Imports System.IO

Partial Class File_Reads
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\")

        cbo_Files.Items.Insert(0, "Please Select A Frame")
        cbo_Files.Items.FindByText("Please Select A Frame").Value = ""
        cbo_Files.SelectedIndex = 0

        With cbo_Files
            .DataSource = dirInfo.GetFiles("*.xls")
            .DataValueField = "Name"
            .DataTextField = "Name"
            .DataBind()
        End With
        articleList.DataSource = dirInfo.GetFiles("*.xls")
        articleList.DataBind()
    End Sub

    Protected Sub cbo_Files_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Files.DataBinding


    End Sub
End Class
