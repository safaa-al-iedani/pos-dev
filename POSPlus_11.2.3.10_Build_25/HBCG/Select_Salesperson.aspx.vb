Imports System.Data.OracleClient

Partial Class Select_Salesperson
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String

        If Not IsPostBack Then
            
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            sql = "SELECT LNAME || ', ' || FNAME As FULL_NAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE HOME_STORE_CD='" & Session("HOME_STORE_CD") & "' AND EMP_CD <> '" & Session("EMP_CD") & "' AND TERMDATE IS NULL ORDER BY LNAME"

            Dim cmd As OracleCommand
            cmd = DisposablesManager.BuildOracleCommand

            Dim ds As New DataSet

            Try
                conn.Open()
                With cmd
                    .Connection = conn
                    .CommandText = sql
                End With

                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
                oAdp.Fill(ds)
                With cbo_salespeople
                    .DataSource = ds
                    .DataValueField = "EMP_CD"
                    .DataTextField = "FULL_NAME"
                    .DataBind()
                End With
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

    End Sub

    Protected Sub cbo_salespeople_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_salespeople.SelectedIndexChanged

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE  EMP_CD = '" & cbo_salespeople.SelectedValue.ToString.Trim & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Session("EMP_CD") = MyDataReader.Item("EMP_CD")
                Session("EMP_FNAME") = MyDataReader.Item("FNAME")
                Session("EMP_LNAME") = MyDataReader.Item("LNAME")
                Session("HOME_STORE_CD") = MyDataReader.Item("HOME_STORE_CD")
                Response.Redirect("newmain.aspx")
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub
End Class
