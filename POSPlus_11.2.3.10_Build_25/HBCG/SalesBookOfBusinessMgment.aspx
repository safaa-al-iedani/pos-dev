﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="True" CodeFile="SalesBookOfBusinessMgment.aspx.vb" Inherits="SalesBookOfBusinessMgment" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script type="text/javascript">
            $(document).ready(function () {
                $(document).on("contextmenu", function () {
                    return false;
                });
            });

            window.name = "ActiveTab"
    </script>
    <style type="text/css">
        .hide {
            display: none;
        }

        .aspGridViewLines {
            color: #003300;
            font-size: 11px;
            font-family: Tahoma;
            font-weight: normal;
        }

        .aspGridViewHdr {
            color: #003300;
            font-size: 11px;
            font-family: Tahoma;
            font-weight: bold;
        }

        .devExpressLinkBreakFix {
            display: inline-table;
        }
    </style>
    <table class="aspGridViewLines" style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr valign="middle" >

            <td colspan="2" valign="middle" ><asp:Label ID="lbl_StoreCds" runat="server" Text="<%$ Resources:LibResources, Label670 %>" Width="150px" CssClass="aspGridViewLines">
            </asp:Label>


                <%--<asp:DropDownList ID="StoreCds" runat="server" AutoPostBack="True" Width="300px" CssClass="aspGridViewLines">
                </asp:DropDownList>--%>
                <dx:ASPxComboBox ID="StoreCds" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="true" EnableViewState="true" OnSelectedIndexChanged="lucy_slspPopulate" />
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="middle" ><asp:Label ID="lbl_Slsps" runat="server" Text="<%$ Resources:LibResources, Label516 %>" Width="150px" CssClass="aspGridViewLines">
            </asp:Label>
                <%--<asp:DropDownList ID="Slsps" runat="server" AutoPostBack="True" Width="250px" CssClass="aspGridViewLines">
                </asp:DropDownList>--%>
                <dx:ASPxComboBox ID="Slsps" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" />
            </td>
        </tr>
        <tr>
            <td valign="middle" colspan="2"> 
                <asp:Label ID="lbl_custTps" runat="server" Text="<%$ Resources:LibResources, Label140 %>" Width="150px" CssClass="aspGridViewLines"></asp:Label>
                <%--<asp:DropDownList ID="CustTps" runat="server" CssClass="aspGridViewLines" AutoPostBack="True">
                    <asp:ListItem Value="Both">B - Both</asp:ListItem>
                    <asp:ListItem Value="C">C - Corporation</asp:ListItem>
                    <asp:ListItem Value="I">I - Individual</asp:ListItem>
                </asp:DropDownList>--%>
                <dx:ASPxComboBox ID="CustTps" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true">
                    <Items>
                        <dx:ListEditItem Value="Both" Text="B - Both" />
                        <dx:ListEditItem Value="C" Text="C - Corporation" />
                        <dx:ListEditItem Value="I" Text="I - Individual" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td>
                <asp:Label ID="lbl_gTot" runat="server" Text="<%$ Resources:LibResources, Label224 %>" CssClass="aspGridViewLines" Width="85px">
                </asp:Label>
                <asp:Label ID="lbl_gTotVal" runat="server" Text="" CssClass="aspGridViewLines" Width="110px">
                </asp:Label>
            </td>
        </tr>
        <tr id="TRConfStatusFrom" runat="server">
            <td colspan="2" valign="middle" ><asp:Label ID="Label1" runat="server" Text="<%$ Resources:LibResources, Label105 %>" Width="150px" CssClass="aspGridViewLines"></asp:Label>
                <%--<asp:DropDownList ID="ddlConfStatusFrom" runat="server" CssClass="aspGridViewLines" AutoPostBack="false">
                </asp:DropDownList>--%>
                <dx:ASPxComboBox ID="ddlConfStatusFrom" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" 
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="0"  CssClass="devExpressLinkBreakFix" />
            </td><%--CssClass="devExpressLinkBreakFix"--%>
        </tr>
        <tr id="TRConfStatusTo" runat="server" >

            <td colspan="2" valign="middle" ><asp:Label ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label106 %>" Width="150px" CssClass="aspGridViewLines"></asp:Label>
                <dx:ASPxComboBox ID="ddlConfStatusTo" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="-1"/>
            </td>

        </tr>
         <%--   Alice add for request 171 on Nov 2019--%>
        <tr id="PU_DEL_Option" runat="server" >
            <td colspan="2" valign="middle" ><asp:Label ID="Label3" runat="server" Text="<%$ Resources:LibResources, Label1035 %>" Width="150px" CssClass="aspGridViewLines"></asp:Label>
                <dx:ASPxComboBox ID="ddlPU_DEL" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="0">
                    <Items>
                        <dx:ListEditItem Value="B" Text="B - Both" />
                        <dx:ListEditItem Value="P" Text="P - Pickup" />
                        <dx:ListEditItem Value="D" Text="D - Delivery" />
                    </Items>
                    </dx:ASPxComboBox>
            </td>
        </tr>
       <%-- request 171 -- End--%>
        <tr>
            <td><dx:ASPxButton ID="btn_LookUp" runat="server" Text="<%$ Resources:LibResources, Label278 %>"></dx:ASPxButton></td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>"></dx:ASPxButton>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" Text="" CssClass="aspGridViewLines" ForeColor="Red" Width="98%">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <br /><asp:Label ID="lbl_asOf" runat="server" Text="" CssClass="aspGridViewLines" >
                </asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="DocGrid" Style="position: relative; top: 1px; left: 1px;" runat="server" OnRowDataBound="DocGrid_OnRowDataBound"
        AutoGenerateColumns="False" CellPadding="3" DataKeyField="del_doc_num"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" AutoGenerateSelectButton="False"
        Font-Underline="False" Height="156px" Width="99%" AllowSorting="True" OnSorting="DocGrid_Sorting">
        <AlternatingRowStyle BackColor="Beige" BorderColor="Green"></AlternatingRowStyle>
        <HeaderStyle HorizontalAlign="Center" Font-Underline="False" CssClass="aspGridViewHdr"></HeaderStyle>
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="del_doc_num" Target="_self" SortExpression="del_doc_num"
                DataTextField="del_doc_num" HeaderText="<%$ Resources:LibResources, Label511 %>"
                DataNavigateUrlFormatString="salesordermaintenance.aspx?referrer=salesbookofbusinessMgment.aspx&del_doc_num={0}&query_returned=Y">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:HyperLinkField>
            <asp:BoundField DataField="del_doc_num" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label511 %>" Visible="False" SortExpression="del_doc_num">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="split_pct" Visible="true" HeaderText="<%$ Resources:LibResources, Label567 %>" SortExpression="split_pct">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="billTo_name" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label138 %>" Visible="True" SortExpression="billTo_name">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="mult_ords" ReadOnly="True" HeaderText=" <%$ Resources:LibResources, Label324 %>" Visible="True" SortExpression="mult_ords">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="CONFIRMED" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label101 %>" Visible="True" SortExpression="CONFIRMED">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="res" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label472 %>" Visible="True" SortExpression="res">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="inv_avail" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label244 %>" Visible="True" SortExpression="inv_avail">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PaidInFull" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label1037 %>" Visible="True" SortExpression="PaidInFull">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="amt" Visible="True" DataFormatString="{0:c2}" HeaderText="<%$ Resources:LibResources, Label250 %>"
                SortExpression="amt">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Right" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" Font-Underline="False" />
            </asp:BoundField>
            <asp:BoundField DataField="pu_del_dt" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label394 %>" Visible="True" DataFormatString="{0:MM/dd/yyyy}" SortExpression="pu_del_dt">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="po_ist" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label420 %>" Visible="True" SortExpression="po_ist">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
             <asp:TemplateField HeaderText="<%$ Resources:LibResources, Label1036 %>" HeaderStyle-ForeColor="Blue" HeaderStyle-Font-Underline="true">
                    <ItemTemplate>
                        <asp:Label ID="lblBackOrder" runat="server" ></asp:Label>
                    </ItemTemplate>
                  <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                </asp:TemplateField>
            <asp:BoundField DataField="row_proc" ShowHeader="true" ItemStyle-HorizontalAlign="Left" Visible="true" ControlStyle-Width="0">
                <ItemStyle CssClass="hide" Width="0" />
                <HeaderStyle Width="0" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    &nbsp;
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    &nbsp;<asp:Image ID="img_warn" runat="server" ImageUrl="images/warning.jpg" Visible="False" />
    &nbsp;
    <dx:ASPxLabel ID="lbl_Warning" Text="" runat="server" Width="100%">
    </dx:ASPxLabel>
</asp:Content>

