Imports HBCG_Utils
Imports System.Data.OracleClient  'lucy add
Imports System.Web.Services

Partial Class Logout
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Daniela add try
        Try
            Dim str_yn As String  'lucy
            SalesUtils.lucy_check_payment(str_yn, Session.SessionID.ToString.Trim)
            'Daniela save to avoid calling twice
            Session("str_yn_paymnt") = str_yn
            If str_yn = "NO" Then
                Exit Sub
            Else
                If Not IsPostBack Then

                    If Session("EMP_CD") & "" = "" Then
                        ClearCache()
                        Response.Redirect("login.aspx")
                    Else
                        theSystemBiz.SaveAuditLogComment("LOGOUT", Session("EMP_CD") & " logged out")
                    End If

                End If  'lucy
            End If

        Catch
            ClearCache()
            Response.Redirect("Login.aspx")
        End Try

    End Sub
    Public Sub lucy_check_commit(ByRef p_yn As String)

        'Daniela comment not needed
        'SalesUtils.lucy_check_payment(p_yn, Session.SessionID.ToString.Trim)

        If p_yn = "NO" Then   'authorization on file

            Dim previousPage As String = Page.Request.UrlReferrer.ToString
            'Daniela prevent logout if payments
            'If InStr(previousPage, "payment") > 0 Or InStr(previousPage, "Order_Stage") > 0 Then

            Session("lucy_from_logout") = "YES"
            Response.Redirect(previousPage)
            'End If


        End If


    End Sub


    Protected Sub lucy_clean_table()


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from lucy_pos_rf_id where session_id='" & Session.SessionID.ToString.Trim & "' or enter_dt <=(sysdate-1) or ((to_char(sysdate,'HH24')*60+to_char(sysdate,'MI'))- (enter_hh*60+ enter_mi)) >60 "
            End With

            cmdDeleteItems.ExecuteNonQuery()
        End If
        conn.Close()

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Dim str_yn As String = Session("str_yn_paymnt") 'Daniela use the value from the session
        'lucy_check_payment(str_yn)
        'Daniela comment Dec 16, no need to check twice
        lucy_check_commit(str_yn)  '15-oct-14, replace lucy_check_payment
        If str_yn = "NO" Then
            Exit Sub
        Else
            Session("lucy_from_logout") = "NO"
            lucy_clean_table() 'lucy
            Try
                Dim strSessionId As String = If(IsNothing(Session.SessionID), "", Session.SessionID.ToString.Trim)
                If (strSessionId.isNotEmpty()) Then
                    lucy_clean_table()  'lucy
                    theSalesBiz.DeleteTempOrder(strSessionId)
                End If


                ' Remove the SALE from the table.
                ' On logout need to remove the doc(s) from the table
                If Not (Session("deleteBrowserID")) Is Nothing Then
                    Dim accessInfo As New AccessControlBiz()
                    accessInfo.DeleteAllRecords()
                End If

                Session.Abandon()
                ClearCache()
                Response.Redirect("Login.aspx")

            Catch
            End Try
        End If  'lucy
    End Sub

    Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton security concern
        Session.Abandon()
        Session.Clear()
        Session.RemoveAll()
        FormsAuthentication.SignOut()
        Response.ClearHeaders()
        Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
        Response.AddHeader("Pragma", "no-cache")

        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Buffer = True
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        Response.Expires = -1000
        Response.CacheControl = "no-cache"

    End Sub

    <WebMethod>
    Public Shared Function LogoutCheck() As Integer
        ''mm -sep 16,2016 - backbutton security concern
        If HttpContext.Current.Session("EMP_CD") Is Nothing Then
            Return 0
        End If
        Return 1
    End Function

End Class
