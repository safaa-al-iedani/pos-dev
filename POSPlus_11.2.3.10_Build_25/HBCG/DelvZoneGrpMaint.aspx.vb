Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports System.Data.OracleClient.OracleConnection
Imports System.Data.Common
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports System.Web.Services
Imports System.Configuration


Partial Class DelvZoneGrpMaint
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsPostBack Then

            If (Not GridView2.DataSource Is Nothing) Then
                GridView2.DataBind()
            End If

        Else
            txt_zone_grp.Items.Clear()
            txt_zone_cd.Items.Clear()
            txt_distr_centre.Items.Clear()
            txt_store_grp_cd.Items.Clear()

            populateZoneGrp()
            PopulateZoneCodes()
            PopulateDistrCentre()
            PopulateStoreGroupCode()

            txt_zone_grp.Enabled = True
            txt_zone_cd.Enabled = True
            txt_distr_centre.Enabled = True
            txt_store_grp_cd.Enabled = True

        End If



    End Sub
    Protected Sub Populate_Results(ByVal p_zone_grp As String, ByVal p_zone_cd As String, ByVal p_distr_centre As String, ByVal p_store_grp_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'sql = "select ZONE_GRP,ZONE_CD,DAY,TIMESLOT,DISTR_CENTRE,rowid row_id "
        'sql = sql & "from ZONE_GRP a "
        'sql = sql & "where a.zone_grp =nvl('" & p_zone_grp & "', a.zone_grp)"
        'sql = sql & "and a.zone_cd =nvl('" & p_zone_cd & "', a.zone_cd)"
        'sql = sql & "and a.distr_centre =nvl('" & p_distr_centre & "', a.distr_centre)"
        'sql = sql & " order by zone_grp, zone_cd,day,timeslot,distr_centre "

        'aug 04,2017-mariam - #3174 - add store group code 
        sql = "select ZONE_GRP,ZONE_CD,DAY,TIMESLOT,DISTR_CENTRE,rowid row_id, STORE_GRP_CD "
        sql = sql & "from ZONE_GRP a "
        sql = sql & "where a.zone_grp =nvl('" & p_zone_grp & "', a.zone_grp)"
        sql = sql & "and a.zone_cd =nvl('" & p_zone_cd & "', a.zone_cd)"
        sql = sql & "and a.distr_centre =nvl('" & p_distr_centre & "', a.distr_centre)"
        sql = sql & "and a.store_grp_cd =nvl('" & p_store_grp_cd & "', a.store_grp_cd)"
        sql = sql & " order by zone_grp, zone_cd,day,timeslot,distr_centre, store_grp_cd "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                Try
                    GridView2.DataSource = ds
                    GridView2.DataBind()

                Catch ex As Exception
                    conn.Close()
                End Try

                MyDataReader.Close()
                txt_zone_grp.Enabled = False
                txt_zone_cd.Enabled = False
                txt_distr_centre.Enabled = False
                txt_store_grp_cd.Enabled = False
            Else
                txt_zone_grp.Enabled = True
                txt_zone_cd.Enabled = True
                txt_distr_centre.Enabled = True
                txt_store_grp_cd.Enabled = True
            End If
            conn.Close()

        Catch ex As Exception

            conn.Close()
            Throw

        End Try




    End Sub
    Private Sub populateZoneGrp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct zone_grp from zone_grp order by zone_grp"

        txt_zone_grp.Items.Insert(0, Resources.LibResources.Label898)
        txt_zone_grp.Items.FindByText(Resources.LibResources.Label898).Value = ""
        txt_zone_grp.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_zone_grp
                .DataSource = ds
                .DataValueField = "zone_grp"
                .DataTextField = "zone_grp"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try



    End Sub
    Private Sub PopulateZoneCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct zone_cd as zone_cd from zone_grp order by zone_cd"

        txt_zone_cd.Items.Insert(0, Resources.LibResources.Label899)
        txt_zone_cd.Items.FindByText(Resources.LibResources.Label899).Value = ""
        txt_zone_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_zone_cd
                .DataSource = ds
                .DataValueField = "zone_cd"
                .DataTextField = "zone_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    Private Sub PopulateDistrCentre()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim v_emp_init As String = Session("emp_init")

        'Dim SQL As String = ""
        'SQL = "SELECT distinct a.store_cd distr_centre from store a "
        'SQL = SQL & " where std_multi_co2.isValidStr('" & v_emp_init & "', a.store_cd ) = 'Y' "
        'SQL = SQL & " order by a.store_cd "

        Dim userType As Double = -1
        Dim SQL As StringBuilder = New StringBuilder("")
        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL.Append("SELECT s.store_cd distr_centre FROM store s order by s.store_cd ")
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            SQL = New StringBuilder(" select distinct s.store_cd distr_centre  ")
            SQL.Append("from MCCL_STORE_GROUP a, store s, store_grp$store b ")
            SQL.Append("where a.user_id = :userType and ")
            SQL.Append("s.store_cd = b.store_cd and ")
            SQL.Append("a.store_grp_cd = b.store_grp_cd ")
            ' Leons Franchise exception
            If isNotEmpty(userType) And (userType = 5 Or userType = 6) Then
                SQL.Append(" and '" & coCd & "' = s.co_cd ")

            End If
            SQL.Append(" order by s.store_cd ")
        End If

        txt_distr_centre.Items.Insert(0, Resources.LibResources.Label900)
        txt_distr_centre.Items.FindByText(Resources.LibResources.Label900).Value = ""
        txt_distr_centre.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL.ToString()
            End With

            ' Parameters only if not Super User
            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetCodes.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetCodes.Parameters(":userType").Value = userType

            End If
            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_distr_centre
                .DataSource = ds
                .DataValueField = "distr_centre"
                .DataTextField = "distr_centre"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    ''----- July 28, 2017 - mariam ---------
    ''3174 -Add new field Store_group_code from Aone_grp table to screen
    <WebMethod()>
    Public Sub PopulateStoreGroupCode()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim v_emp_init As String = HttpContext.Current.Session("emp_init")
        Dim storegroupcodes As New List(Of String)()
        Dim sql As String = String.Empty

        sql = "SELECT store_grp_cd FROM store_grp "
        sql = sql & " order by store_grp_cd"

        Try
            With cmd
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

            With txt_store_grp_cd
                .DataSource = ds
                .DataValueField = "store_grp_cd"
                .DataTextField = "store_grp_cd"
                .DataBind()
            End With

            connErp.Close()

            txt_store_grp_cd.Items.Insert(0, Resources.LibResources.Label528 + " " + Resources.LibResources.Label708)
            txt_store_grp_cd.Items.FindByText(Resources.LibResources.Label528 + " " + Resources.LibResources.Label708).Value = ""
            txt_store_grp_cd.SelectedIndex = 0


        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click


        GridView2.DataSource = ""
        GridView2.DataBind()

        txt_zone_grp.SelectedIndex = 0
        txt_zone_cd.SelectedIndex = 0
        txt_distr_centre.SelectedIndex = 0
        txt_store_grp_cd.SelectedIndex = 0

        txt_zone_grp.Items.Clear()
        txt_zone_cd.Items.Clear()
        txt_distr_centre.Items.Clear()
        txt_store_grp_cd.Items.Clear()

        populateZoneGrp()
        PopulateZoneCodes()
        PopulateDistrCentre()
        PopulateStoreGroupCode()

        txt_zone_grp.Enabled = True
        txt_zone_cd.Enabled = True
        txt_distr_centre.Enabled = True
        txt_store_grp_cd.Enabled = True


    End Sub
    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        Populate_Results(HttpUtility.UrlEncode(txt_zone_grp.Text), HttpUtility.UrlEncode(txt_zone_cd.Text), HttpUtility.UrlEncode(txt_distr_centre.Text), HttpUtility.UrlEncode(txt_store_grp_cd.Text))


    End Sub
    Protected Function isValidZoneGrp(ByVal p_zone_grp As String) As String

        If Len(p_zone_grp) <> 5 Then
            Return "N"
        Else
            Return "Y"
        End If


    End Function
    Protected Function isValidZone(ByVal p_zone As String) As String

        Dim v_ind As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' as AAA FROM zone " &
                            " where zone_cd = '" & p_zone & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If


    End Function
    Protected Function isValidDistrCentre(ByVal p_distr_centre As String) As String

        Dim v_ind As String = String.Empty
        Dim v_emp_init As String = Session("emp_init")
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String =
        "SELECT 'Y' as AAA FROM STORE S " &
        " WHERE S.STORE_CD = '" & p_distr_centre & "'" &
        "   and std_multi_co2.isValidStr6('" & v_emp_init & "','" & p_distr_centre & "') = 'Y' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If



    End Function
    Protected Function isValidStoreGroup(ByVal p_store_grp_cd As String) As String
        'Nov 10, 2017 - mariam - #3174 - store_grp_cd diplay
        Dim v_ind As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' as AAA FROM store_grp " &
                            " where store_grp_cd = '" & p_store_grp_cd & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try

        If v_ind & "" = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If


    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView2_ParseValue(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxParseValueEventArgs)
        Dim hasError As Boolean
        Dim errorMsg As String = ""

        If e.FieldName = "ZONE_GRP" Then
            If e.Value & "" = "" Or isValidZoneGrp(UCase(e.Value)) <> "Y" Then
                hasError = True
                errorMsg = "Invalid Zone Group Please try again."
            End If
        ElseIf e.FieldName = "ZONE_CD" Then
            If e.Value & "" = "" Or isValidZone(UCase(e.Value)) <> "Y" Then
                hasError = True
                errorMsg = "Invalid Zone Code Please try again."
            End If
        ElseIf e.FieldName = "DAY" Then
            If e.Value & "" = "" Or Not IsNumeric(e.Value) Then
                hasError = True
                errorMsg = "Invalid DAY - must be 1-9"
            Else
                If e.Value < 1 Or e.Value > 9 Then
                    hasError = True
                    errorMsg = "Invalid DAY - must be 1-9"
                End If
            End If
        ElseIf e.FieldName = "TIMESLOT" Then
            If e.Value & "" = "" Or Not IsNumeric(e.Value) Then
                hasError = True
                errorMsg = "Invalid TIMESLOT - must be 1 - 99"
            Else
                If e.Value < 1 Or e.Value > 99 Then
                    hasError = True
                    errorMsg = "Invalid TIMESLOT - must be 1 - 99"
                End If
            End If
        ElseIf e.FieldName = "DISTR_CENTRE" Then
            If e.Value & "" = "" Or isValidDistrCentre(UCase(e.Value)) <> "Y" Then
                hasError = True
                'errorMsg = "Invalid Distribution Centre - Please try again."
                errorMsg = Resources.LibResources.Label896
            End If
        ElseIf e.FieldName = "STORE_GRP_CD" Then
            If e.Value & "" = "" Or isValidStoreGroup(UCase(e.Value)) <> "Y" Then
                hasError = True
                errorMsg = "Invalid Store Group Please try again.."
            End If
        End If

        If hasError Then
            Throw New Exception(errorMsg)
        End If
    End Sub


    Protected Sub GridView2_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)


        For Each args In e.InsertValues
            InsertNewItem(args.NewValues)

        Next args

        For Each args In e.UpdateValues
            UpdateItem(args.Keys, args.NewValues, args.OldValues)
            Populate_Results(UCase(args.NewValues("ZONE_GRP")), UCase(args.NewValues("ZONE_CD")), UCase(args.NewValues("DISTR_CENTRE")), UCase(args.NewValues("STORE_GRP_CD")))
        Next args

        For Each args In e.DeleteValues
            DeleteItem(args.Keys, args.Values)

        Next args

        e.Handled = True

        'txt_zone_grp.Items.Clear()
        'txt_zone_cd.Items.Clear()
        'txt_distr_centre.Items.Clear()

        'populateZoneGrp()
        'PopulateZoneCodes()
        'PopulateDistrCentre()

        txt_zone_grp.Enabled = False
        txt_zone_cd.Enabled = False
        txt_distr_centre.Enabled = False
        txt_store_grp_cd.Enabled = False

    End Sub


    Protected Function InsertNewItem(ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into zone_grp (zone_grp,zone_cd,store_grp_cd,day,timeslot,distr_centre) "
        sql = sql & " values ( "
        sql = sql & "'" + UCase(newValues("ZONE_GRP").ToString()) + "',"
        sql = sql & "'" + UCase(newValues("ZONE_CD").ToString()) + "',"
        sql = sql & "'" + UCase(newValues("STORE_GRP_CD").ToString()) + "',"
        sql = sql & newValues("DAY").ToString().ToUpper + ","
        sql = sql & newValues("TIMESLOT").ToString().ToUpper + ","
        sql = sql & "'" + UCase(newValues("DISTR_CENTRE").ToString()) + "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()


        lbl_msg.Text = "Insert is done"
        GridView2.DataBind()

        Return True

    End Function
    Protected Function UpdateItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary, ByVal oldValues As OrderedDictionary) As Boolean


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "UPDATE ZONE_GRP SET "
        sql = sql & " ZONE_GRP = '" + UCase(newValues("ZONE_GRP").ToString()) + "'"
        sql = sql & " ,ZONE_CD = '" + UCase(newValues("ZONE_CD").ToString()) + "'"
        sql = sql & " ,DAY = " + newValues("DAY").ToString()
        sql = sql & " ,TIMESLOT = " + newValues("TIMESLOT").ToString()
        sql = sql & " ,DISTR_CENTRE = '" + UCase(newValues("DISTR_CENTRE").ToString()) + "'"
        sql = sql & " ,STORE_GRP_CD = '" + UCase(newValues("STORE_GRP_CD").ToString()) + "'"
        sql = sql & " WHERE zone_grp= '" + oldValues("ZONE_GRP").ToString() + "'"
        sql = sql & " and ZONE_CD = '" + oldValues("ZONE_CD").ToString() + "'"
        sql = sql & " and day = " + oldValues("DAY").ToString()
        sql = sql & " and timeslot = " + oldValues("TIMESLOT").ToString()
        sql = sql & " and distr_centre = '" + oldValues("DISTR_CENTRE") + "'"
        sql = sql & " and store_grp_cd = '" + oldValues("STORE_GRP_CD") + "'"


        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        lbl_msg.Text = "Batch Updated is done"
        GridView2.DataBind()

        Return True

    End Function
    Protected Function DeleteItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "DELETE ZONE_GRP "
        sql = sql & " WHERE ZONE_GRP = '" + newValues("ZONE_GRP").ToString() + "'"
        sql = sql & " and ZONE_CD = '" + newValues("ZONE_CD").ToString() + "'"
        sql = sql & " and DAY = " + newValues("DAY").ToString()
        sql = sql & " and TIMESLOT = " + newValues("TIMESLOT").ToString()
        sql = sql & " and distr_centre = '" + newValues("DISTR_CENTRE").ToString() + "'"
        sql = sql & " and store_grp_cd = '" + newValues("STORE_GRP_CD").ToString() + "'"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        lbl_msg.Text = "Delete is done"
        GridView2.DataBind()

        Return True

    End Function

    Protected Sub GridView2_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        e.Editor.ReadOnly = False
        e.Editor.ClientEnabled = True
    End Sub
    Protected Sub GridView2_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("ZONE_GRP") = ""
        e.NewValues("ZONE_CD") = ""
        e.NewValues("DAY") = ""
        e.NewValues("TIMESLOT") = ""
        e.NewValues("DISTR_CENTRE") = ""
        e.NewValues("STORE_GRP_CD") = ""
    End Sub


End Class
