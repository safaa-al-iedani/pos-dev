Imports System.IO
Imports System.Data.OracleClient
Imports System.Xml
Imports HBCG_Utils
Imports SD_Utils

Partial Class GEQuickCredit
    Inherits POSBasePage
    Private Const cResponseCriteria As String = "/HandleCode/QuickCredit/ReturnCode/CodeDesc"
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()

    Protected Sub btn_save_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_exit.Click

        'gets invoked when a 'submit' is done
        Submit_Quick_Screen("1")

    End Sub

    Public Sub Lock_Application()

        txt_fname.Enabled = False
        txt_middle_init.Enabled = False
        txt_lname.Enabled = False
        txt_addr1.Enabled = False
        txt_addr2.Enabled = False
        txt_city.Enabled = False
        txt_state.Enabled = False
        txt_zip.Enabled = False
        'txt_home_phone.Enabled = False
        'txt_alt_phone.Enabled = False

    End Sub

    Public Sub Reset_Application()

        txt_fname.Text = ""
        txt_middle_init.Text = ""
        txt_lname.Text = ""
        txt_addr1.Text = ""
        txt_addr2.Text = ""
        txt_city.Text = ""
        txt_state.Text = ""
        txt_zip.Text = ""
        txt_home_phone.Text = ""
        txt_alt_phone.Text = ""
        txt_month.Text = ""
        txt_day.Text = ""
        txt_year.Text = ""
        txt_ssn1.Text = ""
        txt_ssn2.Text = ""
        txt_ssn3.Text = ""
        txt_emp_time_years.Text = ""
        txt_emp_time_months.Text = ""
        txt_emp_phone.Text = ""
        txt_time_at_res_years.Text = ""
        txt_time_at_res_months.Text = ""
        txt_annual_income.Text = ""
        txt_dl_license_no.Text = ""
        txt_dl_license_state.Text = ""
        txt_rel_phone.Text = ""
        txt_auth.Text = ""
        btn_save_exit.Enabled = False
        btn_accept.Enabled = False
        btn_decline.Enabled = False
        txt_cust_cd.Text = ""
        lbl_response.Text = ""
        full_app.Visible = False

    End Sub

    Private Function CheckForRequiredInfo() As String
        'Checks for the required info regardless of whether doing a submit/decline/accept

        Dim error_text As String = ""
        If String.IsNullOrEmpty(txt_fname.Text) Then
            error_text = "You must enter a first name<br />"
            txt_fname.Focus()
        End If
        If String.IsNullOrEmpty(txt_lname.Text) Then
            error_text = error_text & "You must enter a last name<br />"
            txt_lname.Focus()
        End If
        If Not IsNumeric(txt_zip.Text) And Len(txt_zip.Text) <> 5 Then
            error_text = error_text & "Zip code must be numeric and 5 digits long<br />"
            txt_zip.Focus()
        End If
        If (Not String.IsNullOrEmpty(txt_req_amt.Text)) Then
            If IsNumeric(txt_req_amt.Text) Then
                txt_req_amt.Text = Replace(FormatNumber(CDbl(txt_req_amt.Text), 0), ",", "")
            Else
                error_text = "Requested Amount must be numeric<br />"
                txt_req_amt.Focus()
            End If
        End If

        Return error_text
    End Function

    Private Function CheckForAdditionalRequiredInfo() As String
        Dim error_text As String = Nothing

        If Not IsNumeric(txt_rel_phone.Text) Or Len(txt_rel_phone.Text) <> 10 Then
            error_text = error_text & "Relative Phone must be numeric and 10 digits long<br />"
            txt_rel_phone.Focus()
        End If
        If Not IsNumeric(txt_emp_phone.Text) Or Len(txt_emp_phone.Text) <> 10 Then
            error_text = error_text & "Employer Phone must be numeric and 10 digits long<br />"
            txt_emp_phone.Focus()
        End If
        If Not IsNumeric(txt_home_phone.Text) Or Len(txt_home_phone.Text) <> 10 Then
            error_text = error_text & "Home Phone must be numeric and 10 digits long<br />"
            txt_home_phone.Focus()
        End If
        If Not IsNumeric(txt_day.Text) Or Not IsNumeric(txt_month.Text) Or Not IsNumeric(txt_year.Text) Then
            error_text = error_text & "Birthdate must be numeric<br />"
            txt_day.Focus()
        ElseIf Not IsDate(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text) Then
            error_text = error_text & "Birthdate must be a valid date<br />"
            txt_day.Focus()
        End If
        If IsNumeric(txt_annual_income.Text) Then
            txt_annual_income.Text = Replace(FormatNumber(CDbl(txt_annual_income.Text), 0), ",", "")
        Else
            error_text = "Monthly Income must be numeric<br />"
            txt_annual_income.Focus()
        End If
        If Not IsNumeric(txt_ssn1.Text) Or Len(txt_ssn1.Text) <> 3 Then
            error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
            txt_ssn1.Focus()
        ElseIf Not IsNumeric(txt_ssn2.Text) Or Len(txt_ssn2.Text) <> 2 Then
            error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
            txt_ssn2.Focus()
        ElseIf Not IsNumeric(txt_ssn3.Text) Or Len(txt_ssn3.Text) <> 4 Then
            error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
            txt_ssn3.Focus()
        End If
        If Not IsNumeric(txt_time_at_res_months.Text) Then
            error_text = error_text & "Resident months must be numeric<br />"
            txt_time_at_res_months.Focus()
        End If
        If Not IsNumeric(txt_time_at_res_years.Text) Then
            error_text = error_text & "Resident years must be numeric<br />"
            txt_time_at_res_years.Focus()
        End If
        If Not IsNumeric(txt_emp_time_years.Text) Then
            error_text = error_text & "Employment years must be numeric<br />"
            txt_emp_time_years.Focus()
        End If
        If Not IsNumeric(txt_emp_time_months.Text) Then
            error_text = error_text & "Employment months must be numeric<br />"
            txt_emp_time_months.Focus()
        End If
        If Len(txt_time_at_res_months.Text) = 1 Then
            txt_time_at_res_months.Text = "0" & txt_time_at_res_months.Text
        ElseIf txt_time_at_res_months.Text & "" = "" Then
            txt_time_at_res_months.Text = "00"
        End If
        If Len(txt_time_at_res_years.Text) = 1 Then
            txt_time_at_res_years.Text = "0" & txt_time_at_res_years.Text
        ElseIf txt_time_at_res_years.Text & "" = "" Then
            txt_time_at_res_years.Text = "00"
        End If

        If Len(txt_emp_time_years.Text) = 1 Then
            txt_emp_time_years.Text = "0" & txt_emp_time_years.Text
        ElseIf txt_emp_time_years.Text & "" = "" Then
            txt_emp_time_years.Text = "00"
        End If
        If Len(txt_emp_time_months.Text) = 1 Then
            txt_emp_time_months.Text = "0" & txt_emp_time_months.Text
        ElseIf txt_emp_time_months.Text & "" = "" Then
            txt_emp_time_months.Text = "00"
        End If

    End Function


    Private Sub saveApplicationInfo()
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim sql As String

        Try
            conn.Open()

            sql = "INSERT INTO EC_APPLICATION (PRIME_FNAME, PRIME_MIDDLE_INIT, PRIME_LNAME, PRIME_DOB, PRIME_SSN, PRIME_CELL, PRIME_ADDR1, PRIME_ADDR2, PRIME_CITY, "
            sql = sql & "PRIME_STATE, PRIME_ZIP_CD, PRIME_HOME_PHONE, PRIME_TIME_AT_RES, PRIME_HOME_STATUS, PRIME_EMP_TIME, PRIME_EMP_PHONE, "
            sql = sql & "PRIME_MON_INCOME, PRIME_YR_INCOME, RELATIVE_PHONE, CREDIT_PROTECTION, PRIME_DL, PRIME_DL_ST, LANGUAGE_INDICATOR, REQUESTED_AMOUNT, AUTHORIZATION)"
            sql = sql & " VALUES (:PRIME_FNAME, :PRIME_MIDDLE_INIT, :PRIME_LNAME, :PRIME_DOB, :PRIME_SSN, :PRIME_CELL, :PRIME_ADDR1, :PRIME_ADDR2, :PRIME_CITY, "
            sql = sql & ":PRIME_STATE, :PRIME_ZIP_CD, :PRIME_HOME_PHONE, :PRIME_TIME_AT_RES, :PRIME_HOME_STATUS, :PRIME_EMP_TIME, :PRIME_EMP_PHONE, "
            sql = sql & ":PRIME_MON_INCOME, :PRIME_YR_INCOME, :RELATIVE_PHONE, :CREDIT_PROTECTION, :PRIME_DL, :PRIME_DL_ST, :LANGUAGE_INDICATOR, :REQUESTED_AMOUNT, :AUTHORIZATION)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":PRIME_FNAME", OracleType.VarChar)
            objSql.Parameters(":PRIME_FNAME").Value = txt_fname.Text
            objSql.Parameters.Add(":PRIME_MIDDLE_INIT", OracleType.VarChar)
            objSql.Parameters(":PRIME_MIDDLE_INIT").Value = txt_middle_init.Text
            objSql.Parameters.Add(":PRIME_LNAME", OracleType.VarChar)
            objSql.Parameters(":PRIME_LNAME").Value = txt_lname.Text
            objSql.Parameters.Add(":PRIME_DOB", OracleType.DateTime)
            If Not IsDate(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text) Then
                objSql.Parameters(":PRIME_DOB").Value = DBNull.Value
            Else
                objSql.Parameters(":PRIME_DOB").Value = FormatDateTime(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text, DateFormat.ShortDate)
            End If
            objSql.Parameters.Add(":PRIME_SSN", OracleType.VarChar)
            objSql.Parameters(":PRIME_SSN").Value = txt_ssn1.Text & txt_ssn2.Text & txt_ssn3.Text
            objSql.Parameters.Add(":PRIME_CELL", OracleType.VarChar)
            objSql.Parameters(":PRIME_CELL").Value = txt_alt_phone.Text
            objSql.Parameters.Add(":PRIME_ADDR1", OracleType.VarChar)
            objSql.Parameters(":PRIME_ADDR1").Value = txt_addr1.Text
            objSql.Parameters.Add(":PRIME_ADDR2", OracleType.VarChar)
            objSql.Parameters(":PRIME_ADDR2").Value = txt_addr2.Text
            objSql.Parameters.Add(":PRIME_CITY", OracleType.VarChar)
            objSql.Parameters(":PRIME_CITY").Value = txt_city.Text

            objSql.Parameters.Add(":PRIME_STATE", OracleType.VarChar)
            objSql.Parameters(":PRIME_STATE").Value = txt_state.Text
            objSql.Parameters.Add(":PRIME_ZIP_CD", OracleType.VarChar)
            objSql.Parameters(":PRIME_ZIP_CD").Value = txt_zip.Text
            objSql.Parameters.Add(":PRIME_HOME_PHONE", OracleType.VarChar)
            objSql.Parameters(":PRIME_HOME_PHONE").Value = txt_home_phone.Text
            objSql.Parameters.Add(":PRIME_TIME_AT_RES", OracleType.VarChar)
            objSql.Parameters(":PRIME_TIME_AT_RES").Value = txt_time_at_res_months.Text & txt_time_at_res_years.Text
            objSql.Parameters.Add(":PRIME_HOME_STATUS", OracleType.VarChar)
            objSql.Parameters(":PRIME_HOME_STATUS").Value = cbo_res_status.SelectedValue
            objSql.Parameters.Add(":PRIME_EMP_TIME", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_TIME").Value = txt_emp_time_months.Text & txt_emp_time_years.Text
            objSql.Parameters.Add(":PRIME_EMP_PHONE", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_PHONE").Value = txt_emp_phone.Text

            objSql.Parameters.Add(":PRIME_MON_INCOME", OracleType.VarChar)
            objSql.Parameters(":PRIME_MON_INCOME").Value = txt_annual_income.Text
            objSql.Parameters.Add(":PRIME_YR_INCOME", OracleType.VarChar)
            objSql.Parameters(":PRIME_YR_INCOME").Value = ""
            objSql.Parameters.Add(":RELATIVE_PHONE", OracleType.VarChar)
            objSql.Parameters(":RELATIVE_PHONE").Value = txt_rel_phone.Text
            objSql.Parameters.Add(":CREDIT_PROTECTION", OracleType.VarChar)
            objSql.Parameters(":CREDIT_PROTECTION").Value = cbo_credit_protection.SelectedValue
            objSql.Parameters.Add(":PRIME_DL", OracleType.VarChar)
            objSql.Parameters(":PRIME_DL").Value = txt_dl_license_no.Text
            objSql.Parameters.Add(":PRIME_DL_ST", OracleType.VarChar)
            objSql.Parameters(":PRIME_DL_ST").Value = txt_dl_license_state.Text
            objSql.Parameters.Add(":LANGUAGE_INDICATOR", OracleType.VarChar)
            objSql.Parameters(":LANGUAGE_INDICATOR").Value = "E"
            objSql.Parameters.Add(":REQUESTED_AMOUNT", OracleType.VarChar)
            objSql.Parameters(":REQUESTED_AMOUNT").Value = txt_req_amt.Text
            objSql.Parameters.Add(":AUTHORIZATION", OracleType.VarChar)
            objSql.Parameters(":AUTHORIZATION").Value = txt_auth.Text

            objSql.ExecuteNonQuery()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    Public Sub Submit_Quick_Screen(ByVal app_tp As String)
        ' the value of app_tp passed-in can be:
        ' '1' in the case when a 'submit' is done, '2' when an 'accept' and '3' when it's a decline

        'Do validation  
        Dim Validated As Boolean = False
        Dim error_text As String = ""
        lbl_warnings.Text = ""

        'validate all basic required info
        error_text = CheckForRequiredInfo()

        'validate all the other required info when the user wants to except the QuickCredit
        If app_tp = "2" Then
            error_text = CheckForAdditionalRequiredInfo()
        End If

        If error_text & "" <> "" Then
            lbl_warnings.Text = "<font color=red><b>The following items must be corrected before continuing:</b> <br /><br />" & error_text & "</font>"
            Exit Sub
        End If

        'save the info into EC application
        saveApplicationInfo()

        Dim terminalId As String = ""
        Dim processorId As String = ""
        Dim acct_no As String = ""
        Dim store_cd As String = Request("store_cd")
        If store_cd & "" = "" Then
            store_cd = Session("home_store_cd")
        End If

        'first get the provider/ASP code from GP parms that has been setup for GE Quick Credit
        Dim providerCd = GetQuickCreditProviderCode()

        'retrieve the auth provider specific details from the db
        Dim ds As DataSet = GetAuthDetails(providerCd, store_cd, GetCashDrawerCd(store_cd))
        For Each dr In ds.Tables(0).Rows
            terminalId = dr("terminal_id")
            processorId = dr("processor_id")
        Next

        Dim MyDataReader1 As OracleDataReader
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim SDC_Response As String = ""

        Dim sql As String = "SELECT ROW_ID " &
                            "FROM EC_APPLICATION " &
                            "WHERE PRIME_FNAME=:FNAME" &
                            "' AND PRIME_LNAME=:LNAME"
        If app_tp = "2" Then
            sql = sql & "AND PRIME_SSN=:SSN"
        End If

        sql = sql & " ORDER BY ROW_ID DESC"
        Try
            conn.Open()
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.Parameters.Add(":FNAME", OracleType.VarChar)
            objsql2.Parameters(":FNAME").Value = txt_fname.Text
            objsql2.Parameters.Add(":LNAME", OracleType.VarChar)
            objsql2.Parameters(":LNAME").Value = txt_lname.Text
            objsql2.Parameters.Add(":SSN", OracleType.VarChar)
            objsql2.Parameters(":SSN").Value = txt_ssn1.Text & txt_ssn2.Text & txt_ssn3.Text

            MyDataReader1 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader1.Read Then
                SDC_Response = SD_Submit_Query_PL(MyDataReader1.Item("ROW_ID").ToString, terminalId, AUTH_ECA, "BLANK", app_tp, processorId)
                'lbl_warnings.Text = SDC_Response
            Else
                lbl_warnings.Text = "Application Failed"
            End If
        Finally
            conn.Close()
        End Try

        Dim Result_String As String = ""
        Dim SDC_Fields As String() = Nothing
        Dim PB_Response_Cd As String = ""
        Dim Protobase_Response_Msg As String = ""
        Dim Host_Response_Msg As String = ""
        Dim Host_Response_Cd As String = ""
        Dim cc_resp As String = ""

        Dim sep(3) As Char
        sep(0) = Chr(10)
        sep(1) = Chr(12)
        SDC_Fields = SDC_Response.Split(sep)
        Dim s As String
        Dim MsgArray As Array
        For Each s In SDC_Fields
            If InStr(s, ",") > 0 Then
                MsgArray = Split(s, ",")
                Select Case MsgArray(0)
                    Case "1003"
                        PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1004"
                        Result_String = Result_String & "Application Status: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "3165"
                        Result_String = Result_String & "Application Key: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "1004"
                        Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Result_String = Result_String & "GE Response: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "1009"
                        Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1010"
                        Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "3305"
                        Result_String = Result_String & "Account Number: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                        acct_no = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "3306"
                        Result_String = Result_String & "Credit Limit: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "3317"
                        Result_String = Result_String & "Authorization Number: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                        txt_auth.Text = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                End Select
            End If
        Next s
        Result_String = Result_String & "Expiration Date: " & FormatDateTime(Now.Date.AddDays(60).ToString, DateFormat.ShortDate) & "<br />"

        lbl_response.Text = Result_String

        Select Case PB_Response_Cd
            Case "0"
                cc_resp = "APPROVED"
            Case "0000"
                cc_resp = "APPROVED"
            Case "60"
                cc_resp = "DECLINED"
            Case "0060"
                cc_resp = "DECLINED"
            Case Else
                Host_Response_Msg = PB_Response_Cd
                Host_Response_Cd = Protobase_Response_Msg
                cc_resp = "ERROR"
        End Select

        If app_tp = "3" And cc_resp = "APPROVED" Then
            'When doing a decline and the the response is 'approved'
            btn_decline.Enabled = False
            btn_accept.Enabled = True
            Update_QS_Status(txt_cust_cd.Text, "R", txt_auth.Text, store_cd)
        End If
        If app_tp = "1" And cc_resp = "APPROVED" Then
            'When doing a submit and the the response is 'approved'
            btn_accept.Enabled = True
            btn_decline.Enabled = True
            Update_QS_Status(txt_cust_cd.Text, "P", txt_auth.Text, store_cd)
        End If

        If cc_resp = "DECLINED" And Host_Response_Cd = "03" Then
            Update_QS_Status(txt_cust_cd.Text, "N", txt_auth.Text, store_cd)
        ElseIf cc_resp = "DECLINED" And Host_Response_Cd = "04" Then
            Update_QS_Status(txt_cust_cd.Text, "N", txt_auth.Text, store_cd)
        ElseIf cc_resp = "DECLINED" And Host_Response_Cd = "05" Then
            Update_QS_Status(txt_cust_cd.Text, "N", txt_auth.Text, store_cd)
        ElseIf cc_resp = "DECLINED" And Host_Response_Cd = "06" Then
            Update_QS_Status(txt_cust_cd.Text, "N", txt_auth.Text, store_cd)
        ElseIf cc_resp = "DECLINED" Then
            btn_decline.Enabled = False
            btn_accept.Enabled = False
            full_app.Visible = False
            Update_QS_Status(txt_cust_cd.Text, "X", txt_auth.Text, store_cd)
        End If
        If cc_resp = "ERROR" Then
            btn_decline.Enabled = False
            btn_accept.Enabled = False
            Update_QS_Status(txt_cust_cd.Text, "E", txt_auth.Text, store_cd)
        End If
        If cc_resp = "APPROVED" Then
            btn_accept.Enabled = True
            btn_decline.Enabled = True
            full_app.Visible = True
            Lock_Application()
            btn_save_exit.Enabled = False
        End If

        If cc_resp = "APPROVED" And acct_no & "" <> "" And app_tp = "2" Then
            'if the user accepts the QCredit Approval and there is an acct #
            Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

            objConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            objConnection.Open()

            'Use the provider code setup in GP Parms to get the cust code from cust_asp table instead of  'GE money'
            sql = "SELECT CUST_CD FROM CUST_ASP" &
                        " WHERE CUST_CD='" & txt_cust_cd.Text &
                        "' AND AS_CD = '" & providerCd & "'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
            MyDataReader1 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader1.HasRows Then
                sql = "UPDATE CUST_ASP SET AS_CD=' " & providerCd & "', ACCT_CD=:ACCT_CD WHERE CUST_CD=:CUST_CD"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)

                objsql2.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objsql2.Parameters(":CUST_CD").Value = txt_cust_cd.Text
                objsql2.Parameters.Add(":ACCT_CD", OracleType.VarChar)
                objsql2.Parameters(":ACCT_CD").Value = acct_no
            Else
                sql = "INSERT INTO CUST_ASP (CUST_CD, AS_CD, ACCT_CD) "
                sql = sql & "VALUES(:CUST_CD,'" & providerCd & "',:ACCT_CD)"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)

                objsql2.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objsql2.Parameters(":CUST_CD").Value = txt_cust_cd.Text
                objsql2.Parameters.Add(":ACCT_CD", OracleType.VarChar)
                objsql2.Parameters(":ACCT_CD").Value = acct_no
            End If
            objsql2.ExecuteNonQuery()
            objConnection.Close()
            Update_QS_Status(txt_cust_cd.Text, "A", txt_auth.Text, store_cd)
            btn_save_exit.Enabled = False
            btn_accept.Enabled = False
            btn_decline.Enabled = False
            lbl_response.Text = lbl_response.Text & "<br />Account Information Added To GERS"
        End If

    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument = New XmlDocument()
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld.Load(Request.ServerVariables("APPL_PHYSICAL_PATH") & "ADS.xml")
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            ElseIf IsNumeric(m_node.ChildNodes.Item(0).InnerText) And IsNumeric(reasonCd) Then
                If CDbl(m_node.ChildNodes.Item(0).InnerText) = CDbl(reasonCd) Then
                    desc = m_node.ChildNodes.Item(1).InnerText
                    Exit For
                End If
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc
    End Function

    Protected Sub btn_lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_lookup.Click

        Customer_Lookup()

    End Sub

    Public Sub Customer_Lookup()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        sql = "SELECT FNAME, LNAME, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE " &
                    " FROM CUST WHERE CUST_CD='" & txt_cust_cd.Text.ToUpper & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                txt_fname.Text = MyDataReader.Item("FNAME").ToString
                txt_lname.Text = MyDataReader.Item("LNAME").ToString
                txt_addr1.Text = MyDataReader.Item("ADDR1").ToString
                txt_addr2.Text = MyDataReader.Item("ADDR2").ToString
                txt_city.Text = MyDataReader.Item("CITY").ToString
                txt_state.Text = MyDataReader.Item("ST_CD").ToString
                txt_zip.Text = MyDataReader.Item("ZIP_CD").ToString
                txt_home_phone.Text = Replace(MyDataReader.Item("HOME_PHONE").ToString, "-", "")
                txt_alt_phone.Text = Replace(MyDataReader.Item("BUS_PHONE").ToString, "-", "")
                txt_auth.Text = Retrieve_Auth_No(txt_cust_cd.Text)
                If txt_cust_cd.Text & "" <> "" Then
                    Select Case Retrieve_QS_Status(txt_cust_cd.Text)
                        Case "A"
                            btn_accept.Enabled = False
                            btn_decline.Enabled = False
                            full_app.Visible = False
                            Lock_Application()
                            btn_save_exit.Enabled = False
                            lbl_response.Text = "Customer is already approved, cannot submit another Quick Screen"
                        Case "R"
                            btn_accept.Enabled = True
                            btn_decline.Enabled = True
                            full_app.Visible = True
                            Lock_Application()
                            btn_save_exit.Enabled = False
                        Case "D"
                            btn_accept.Enabled = False
                            btn_decline.Enabled = False
                            full_app.Visible = False
                            Lock_Application()
                            btn_save_exit.Enabled = False
                            lbl_response.Text = "Customer has been declined, cannot submit another Quick Screen"
                        Case "X"
                            btn_accept.Enabled = False
                            btn_decline.Enabled = False
                            full_app.Visible = False
                            Lock_Application()
                            btn_save_exit.Enabled = False
                            lbl_response.Text = "Customer has been declined, cannot submit another Quick Screen"
                        Case "P"
                            btn_accept.Enabled = True
                            btn_decline.Enabled = True
                            full_app.Visible = True
                            Lock_Application()
                            btn_save_exit.Enabled = False
                        Case "N"
                            btn_accept.Enabled = True
                            btn_decline.Enabled = True
                            full_app.Visible = True
                            Lock_Application()
                            btn_save_exit.Enabled = False
                        Case Else
                            btn_accept.Enabled = False
                            btn_decline.Enabled = False
                            full_app.Visible = False
                            btn_save_exit.Enabled = True
                    End Select
                End If
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Private Function GetQuickCreditProviderCode() As String

        'Returns the provider that has been setup in  gp_parms to do a GE_QUICK_CREDIT. This code
        ' must match the entry in  the ASP table for that provider.

        Dim providerCd As String = ""
        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            sql = "SELECT value FROM (gp_parms)" &
                        " WHERE key = 'PTS_POS+' " &
                        " AND parm = 'GE_QUICK_CREDIT'"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If MyDataReader.Read Then
                providerCd = MyDataReader.Item("VALUE").ToString
            End If
        Finally
            conn.Close()
        End Try

        Return providerCd.Trim
    End Function

    Protected Sub btn_accept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_accept.Click

        Submit_Quick_Screen("2")

    End Sub

    Protected Sub btn_decline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_decline.Click

        Submit_Quick_Screen("3")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("cust_cd") & "" <> "" Then
                txt_cust_cd.Text = Request("cust_cd")
                Customer_Lookup()
            End If
        End If
        If txt_cust_cd.Text & "" <> "" Then
            Dim qsStatus As String = Retrieve_QS_Status(txt_cust_cd.Text)
            Select Case qsStatus
                Case "A", "D", "X"
                    btn_accept.Enabled = False
                    btn_decline.Enabled = False
                    full_app.Visible = False
                    Lock_Application()
                    btn_save_exit.Enabled = False
                    lbl_response.Text = If("A".Equals(qsStatus),
                                            "Customer is already approved, cannot submit another Quick Screen",
                                            "Customer has been declined, cannot submit another Quick Screen")
                Case "R", "P", "N"
                    btn_accept.Enabled = True
                    btn_decline.Enabled = True
                    full_app.Visible = True
                    Lock_Application()
                    btn_save_exit.Enabled = False
                Case Else
                    btn_accept.Enabled = False
                    btn_decline.Enabled = False
                    full_app.Visible = False
                    Lock_Application()
                    btn_save_exit.Enabled = False
            End Select
        Else
            btn_accept.Enabled = False
            btn_decline.Enabled = False
            full_app.Visible = False
            Lock_Application()
            btn_save_exit.Enabled = False
        End If

    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

    Protected Sub btn_payment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_payment.Click

        Response.Redirect("independentpaymentprocessing.aspx")

    End Sub

    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click

        Reset_Application()

    End Sub

End Class
