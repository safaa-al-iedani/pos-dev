Imports System.Data.OracleClient
Imports SD_Utils
Imports HBCG_Utils

Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp

Partial Class Credit_Card_Processing
    Inherits POSBasePage

    Private thePmtBiz As PaymentBiz = New PaymentBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lucy add the following all lines

        Dim str_mop_cd As String = Request("mop_cd")
        Dim str_pmt_tp As String = Request("pmt_tp_cd")
        If (isEmpty(str_pmt_tp) Or IsDBNull(str_pmt_tp) Or (str_pmt_tp <> "R" And str_pmt_tp <> "PMT")) Then  'basic purchase
            lucy_lbl_text_corr.Visible = False
            lucy_Text_correction.Visible = False
            ASPxLabel4.Visible = False
            txt_auth.Visible = False
            lucy_lbl_plan.Visible = False
            lucy_text_fi_plan.Visible = False
            lucy_text_fi_plan.Text = ""
            lucy_lbl_checkbox.Visible = True
            lucy_CheckBox.Visible = True
            btn_save.Focus()
        ElseIf str_pmt_tp = "R" Then
            If InStr(UCase(Session("lucy_display")), "APPR") > 0 Or Len(Session("lucy_display")) < 2 Then 'lucy approved or first load
                lucy_lbl_msg.Text = " *** Processing a Refund *** "
            End If
            lucy_lbl_msg.ForeColor = Color.Red
            txt_auth.Visible = True
            ASPxLabel4.Visible = True
            lucy_lbl_checkbox.Visible = False
            lucy_CheckBox.Visible = False
            If str_mop_cd = "FV" Then
                If InStr(UCase(Session("lucy_display")), "APPR") > 0 Or Len(Session("lucy_display")) < 2 Then
                    lucy_lbl_msg.Text = " *** Processing a Finance Refund *** "
                End If
                lucy_lbl_plan.Visible = True
                lucy_text_fi_plan.Visible = True
                lucy_Text_correction.Visible = False
                lucy_lbl_text_corr.Visible = False
                Session("lucy_as_cd") = UCase(lucy_text_fi_plan.Text)
                lucy_text_fi_plan.Text = Session("lucy_as_cd")
            Else
                lucy_lbl_text_corr.Visible = True
                lucy_Text_correction.Visible = True
                If IsDBNull(lucy_Text_correction.Text) Or UCase(lucy_Text_correction.Text) <> "Y" Then
                    lucy_Text_correction.Text = "N"
                End If
                lucy_lbl_plan.Visible = False
                lucy_text_fi_plan.Visible = False
                lucy_text_fi_plan.Text = ""
            End If   'R

            If Len(txt_auth.Text) < 2 Then
                txt_auth.Focus()
            End If

            ' ASPxLabel2.Text = "Eenter Auth# and Y to refund"
        ElseIf str_pmt_tp = "PMT" Then


            If str_mop_cd = "FV" Then
                If InStr(UCase(Session("lucy_display")), "APPR") > 0 Or Len(Session("lucy_display")) < 2 Then
                    '  lucy_lbl_msg.Text = " *** Processing a Finance Payment *** "
                End If
                ASPxLabel4.Visible = False
                txt_auth.Visible = False
                lucy_lbl_text_corr.Visible = False
                lucy_Text_correction.Visible = False
                lucy_lbl_plan.Visible = True
                lucy_text_fi_plan.Visible = True
                lucy_text_fi_plan.Text = Session("lucy_as_cd")
                lucy_text_fi_plan.Focus()
            Else
                lucy_lbl_checkbox.Visible = True
                lucy_CheckBox.Visible = True
                lucy_lbl_text_corr.Visible = True
                lucy_Text_correction.Visible = True
                If IsDBNull(lucy_Text_correction.Text) Or UCase(lucy_Text_correction.Text) <> "Y" Then
                    lucy_Text_correction.Text = "N"
                End If

                lucy_lbl_plan.Visible = False
                lucy_text_fi_plan.Visible = False
                lucy_text_fi_plan.Text = ""
                ASPxLabel4.Visible = True
                txt_auth.Visible = True

                btn_save.Focus()
            End If  'pmt
        Else
            lucy_lbl_text_corr.Visible = False
            lucy_Text_correction.Visible = False
            ASPxLabel4.Visible = False
            txt_auth.Visible = False
            btn_save.Focus()    'lucy add
            '  ASPxLabel2.Text = "Click SAVE and SWIPE/INSERT card to make the payment"
        End If

        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btn_save.UniqueID
        lbl_amount.Text = Resources.LibResources.Label387 & " " & FormatCurrency(CDbl(Request("amt")), 2)

        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btn_save.UniqueID
        lbl_amount.Text = Resources.LibResources.Label387 & " " & FormatCurrency(CDbl(Request("amt")), 2)
        TextBox1.Focus()
        ' lucy_lbl_checkbox.Visible = False  ' lucy this needs to be remove when testing E0 
        ' lucy_CheckBox.Visible = False
    End Sub
    Public Function GetIPAddress() As String


        ' lucy created
        Dim strHostName As String

        Dim strIPAddress As String
        Dim str_term_id As String

        strHostName = System.Net.Dns.GetHostName()

        strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(0).ToString()



        Return strIPAddress

        ' MessageBox.Show("Host Name: " & strHostName & "; IP Address: " & strIPAddress)

    End Function

    'Public Function get_term_id() As String
    '    ' Lucy created
    '    Dim connString As String
    '    Dim objConnection As New OracleConnection
    '    Dim x As Exception
    '    Dim Ds As New DataSet()
    '    Dim str_term_id As String
    '    Dim strIP As String

    '    strIP = GetIPAddress() 'does not work after publish 
    '    strIP = Request.ServerVariables("REMOTE_ADDR")  'does not work before publish

    '    connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
    '    objConnection = DisposablesManager.BuildOracleConnection(connString)
    '    objConnection.Open()




    '    Dim myCMD As New OracleCommand()

    '    myCMD.Connection = objConnection
    '    myCMD.CommandText = "std_tenderretail_pos.getTerminal"
    '    myCMD.CommandType = CommandType.StoredProcedure
    '    myCMD.Parameters.Add(New OracleParameter("p_ip", OracleType.Char)).Value = strIP
    '    myCMD.Parameters.Add("p_terminal", OracleType.VarChar, 20).Direction = ParameterDirection.Output

    '    myCMD.ExecuteNonQuery()

    '    str_term_id = myCMD.Parameters("p_terminal").Value


    '    Dim MyDA As New OracleDataAdapter(myCMD)

    '    Try

    '        MyDA.Fill(Ds)
    '        Return str_term_id
    '    Catch x
    '        Throw

    '        objConnection.Close()
    '    End Try
    'End Function



    Protected Sub print_receipt(ByVal p_prt_cmd As String)

        Try

            ' lucy
            Dim str_ip As String
            Dim str_user As String
            Dim str_pswd As String


            str_ip = get_database_ip()
            str_user = get_user()
            str_pswd = get_pswd()

            Dim syclient As New Renci.SshNet.SshClient(str_ip, str_user, str_pswd)

            Dim sycmd As Renci.SshNet.SshCommand
            'Dim str_excep As String
            'Dim str_print_cmd As String

            syclient.Connect()

            sycmd = syclient.RunCommand(p_prt_cmd)
        
            sycmd.Dispose()
            syclient.Disconnect()

        Catch
           
            ' do nothing Jan 25, 2016 prevent payment issues
            'Throw

            'str_excep = Err.Description
        End Try

    End Sub
    Public Function get_pswd()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_pswd As String
        Dim str_pswd As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshPwd"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_pswd", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue






        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_pswd = myCMD.Parameters("p_pswd").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_pswd
    End Function
    Public Function get_user()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_user As String
        Dim p_user As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_user", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try


            myCMD.ExecuteNonQuery()

            str_user = myCMD.Parameters("p_user").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_user
    End Function
    Public Function get_database_ip()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_ip As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()



        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshIP"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_ip", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_ip = myCMD.Parameters("p_ip").Value


        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_ip
    End Function




    Public Function send_out_request() As String
        'Lucy created

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_ack As String

        Dim v_term_id As String

        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_no As String
        Dim v_mop_cd As String
        Dim v_private_lbl_finance_tp As String
        Dim l_filename As String
        Dim l_printcmd As String
        Dim str_sp_approved As String

        ' Daniela Nov 19
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        str_sp_approved = "N"

        Dim str_auth As String
        Dim str_app_cd As String
        Dim str_s_or_m As String = ""
        Dim str_correction As String = UCase(lucy_Text_correction.Text)

        Session("lucy_dsp") = ""
        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""
        If v_term_id = "empty" Then

            lucy_lbl_msg.Text = " No Designated Pinpad"
            Session("lucy_dsp") = " No Designated Pinpad "
            Return " No Designated Pinpad"
            Exit Function
        Else
            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            v_private_lbl_finance_tp = ""   ' this used for finance transaction

            v_sub_type = "8"


            Dim str_del_doc_num As String = Session("del_doc_num")
            Dim str_emp_cd As String = Session("emp_cd")
            If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
                str_del_doc_num = ""
            End If

            Dim sessionid As String = Session.SessionID.ToString.Trim

            v_mop_cd = Request("mop_cd")
            If Len(Trim(v_mop_cd)) < 2 Then
                v_mop_cd = Session("lucy_mop_cd")
            End If
            v_amt = Request("amt")
            If Len(Trim(v_amt)) = 0 Then
                v_amt = Session("lucy_amt")
            End If
            v_sub_type = "8"

            If IsDBNull(txt_auth.Text) Or isEmpty(txt_auth.Text) Then
                str_auth = ""
            Else
                str_auth = txt_auth.Text
            End If

            If IsDBNull(lucy_app_cd.Text) Or isEmpty(lucy_app_cd.Text) Then
                str_app_cd = ""
            Else
                str_app_cd = lucy_app_cd.Text
            End If

            Dim str_pmt_tp As String = Request("pmt_tp_cd")    'R=ARRI PMT=POA
            If (isEmpty(str_pmt_tp) Or IsDBNull(str_pmt_tp) Or (str_pmt_tp <> "R" And str_pmt_tp <> "PMT")) Then   ' PSOE or SOE, not support this transaction
                str_pmt_tp = ""
                If v_mop_cd = "FV" Then
                    lucy_lbl_msg.Text = "Transaction not supported "
                    Session("lucy_dsp") = "Transaction not supported "
                    Exit Function
                End If
            End If

            If v_mop_cd = "FV" Then   'finance void
                v_private_lbl_finance_tp = Session("lucy_as_cd")
            End If

            ' for all (SAL,POA)

            ' If TextBox1.Text & "" = "" And Len(TextBox1.Text) = 16 Then
            If IsDBNull(TextBox1.Text) = False And Len(Trim(TextBox1.Text)) >= 15 Then   'manualy enter card Daniela AMX 15 digits
                str_s_or_m = "M"
                v_card_num = Session("lucy_card_num")
                v_expire_date = MonthLastDay(TextBox2.Text & "/1/" & TextBox3.Text)  'lucy add
                v_expire_date = TextBox2.Text & TextBox3.Text 'lucy add
                If lucy_CheckBox.Checked = True Then str_auth = "E0" ' 24-oct-14
            Else   'swipe
                v_card_num = ""
                v_expire_date = ""
                ' v_auth_no = Session("cust_cd")
                str_s_or_m = "S"
            End If

            ' If v_mop_cd = "DC" And (IsDBNull(txt_auth.Text) = True and Len(txt_auth.Text) < 2) Then

            ' for refund (ARRI and POA)
            If IsDBNull(TextBox1.Text) = False And Len(Trim(TextBox1.Text)) >= 15 Then   'manually enter card Daniela AMEX 15 digits
                str_s_or_m = "M"
                If str_pmt_tp = "R" Then     'refund =ARRI 
                    If Len(str_app_cd) > 1 Then ' not support this transaction
                        Session("lucy_dsp") = "Transaction not supported "
                        Exit Function
                    End If
                End If  'refund =ARRI, purchase correction,manualy enter card


                'If str_pmt_tp = "PMT"     'payment =POA,  manualy enter card

            Else   'swipe
                str_s_or_m = "S"
            End If   'swipe or enter

            If Len(str_app_cd) > 1 And str_pmt_tp = "PMT" And v_mop_cd = "FV" Then ' not support this transaction
                Session("lucy_dsp") = "Transaction not supported "
                Exit Function
            End If

            'Daniela Oct 23, 20114 insert new customer ByVal sessionId As String, ByVal custCd As String, ByVal storeCd As String
            CreateCustomer(sessionid, Session("cust_cd"), Session("store_cd"))

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            Try

                myCMD.Connection = objConnection
                myCMD.CommandText = "std_tenderretail_pos.sendsubtp8_2"
                myCMD.CommandType = CommandType.StoredProcedure
                myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
                myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
                myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
                myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
                myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
                myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
                myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
                myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
                ' Daniela comment
                'myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
                myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
                myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
                myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

                myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
                myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = Session("cust_cd")
                myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
                'select lpad(glpmt_seq.nextval,12,'0') into v_trans_no from dual;
                ' peter will get v_trans_no in his procedure which is a sequence#
                ' myCMD.Parameters.Add(New OracleParameter("p_transTp", OracleType.VarChar)).Value = v_trans_type
                ' myCMD.Parameters.Add(New OracleParameter("p_authNo", OracleType.VarChar)).Value = v_auth_no

                ' Daniela May 12
                myCMD.Parameters.Add("p_mop_cd", OracleType.VarChar, 3).Direction = ParameterDirection.InputOutput
                myCMD.Parameters("p_mop_cd").Value = v_mop_cd
                ' end Daniela

                myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output

                ''If str_auth = "E0" Then
                ''myCMD.Parameters.Add(New OracleParameter("p_aut", OracleType.VarChar)).Value = str_auth
                ''Else

                myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
                ''End If
                ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
                'sabrina changed 5 to 10
                'myCMD.Parameters.Add("p_res", OracleType.VarChar, 5).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
                myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

                ' need str_correction and str_app_cd

                myCMD.ExecuteNonQuery()
                ' DB 20140722 handle invalid card exception
            Catch ex As Exception

                lbl_Full_Name.Text = " ** " & "There is an error processing. Please try again." & " ** " & ex.ToString
                lbl_Full_Name.ForeColor = Color.Red
                ' l_dsp = myCMD.Parameters("p_dsp").Value
                'lbl_Full_Name.Text = l_dsp
                l_dsp = lbl_Full_Name.Text
                Try
                    myCMD.Cancel()
                    myCMD.Dispose()
                    objConnection.Close()
                    objConnection.Dispose()
                Catch
                    'do nothing
                End Try

                Exit Function

                ' Throw
            End Try

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"  ' this is approved auth, using empty to replace null value
            End If


            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces or combination of auth.txt or app_cd with spaces
            v_trans_no = myCMD.Parameters("p_transTp").Value


            lucy_lbl_msg.Text = myCMD.Parameters("p_dsp").Value
            If l_aut <> "empty" Then
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then    'swipe
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    Session("lucy_bnk_card") = Left(l_tr2, 16)
                    TextBox1.Text = Left(l_tr2, 16)
                    l_exp = myCMD.Parameters("p_exp").Value
                    TextBox2.Text = Left(l_exp, 2)
                    TextBox3.Text = Right(l_exp, 2)
                    Session("lucy_as_cd") = ""
                    Session("del_doc_num") = ""
                End If
                l_exp = myCMD.Parameters("p_exp").Value
                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value

                txt_security_code.Text = "000"

                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut              ' 
                Session("approval_cd") = l_aut     '23-jul-14


                Session("lucy_exp_dt") = MonthLastDay(TextBox2.Text & "/1/20" & TextBox3.Text)
                If Left(l_res, 5) = "1[49]" Or Left(l_res, 5) = "1[50]" Then
                    str_sp_approved = "Y"
                End If
                ' Daniela Gift Card not print
                ' print_receipt(l_printcmd)
                If Not LCase(Request("page")) = "wgc_purchasegiftcard.aspx" Then
                    print_receipt(l_printcmd)
                End If

            Else

                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = "" ' used to display before commit
                Session("lucy_card_num") = "" ' Daniela May 21 reset manual card
                l_dsp = myCMD.Parameters("p_dsp").Value

                TextBox1.Text = ""

                TextBox2.Text = ""
                TextBox3.Text = ""
                lucy_lbl_msg.Text = l_dsp  'lucy
                btn_save.Visible = True
            End If
            l_dsp = myCMD.Parameters("p_dsp").Value

            'Daniela Dec 16 save payment status for multiple payments
            If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                Session("cust_display") = Left(l_dsp, 20)
            End If

            Session("lucy_display") = Left(l_dsp, 20) ' used to dispaly before commit

            'Daniela May 12 update right mop code
            Session("out_mop_cd") = myCMD.Parameters("p_mop_cd").Value
            ' End Daniela

            Try
                myCMD.Cancel()
                myCMD.Dispose()
                objConnection.Close()
                objConnection.Dispose()

            Catch ex As Exception
                'do nothing
            End Try
            'comment
            'Return l_dsp

            End If   ' end if of termid condition

            Return l_dsp

    End Function




    Public Function MonthLastDay(ByVal dCurrDate As Date)

        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        Try
            lbl_Full_Name.Text = ""
            btn_save.Enabled = True
            lbl_tran_id.Text = ""

            If InStr(TextBox1.Text, "^") > 0 Then
                lbl_tran_id.Text = AppUtils.Encrypt(TextBox1.Text, "CrOcOdIlE")
                Dim sCC As Array
                sCC = Split(TextBox1.Text.ToString, "^")
                TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
                If Left(TextBox1.Text, 1) = "B" Then TextBox1.Text = Right(TextBox1.Text, Len(TextBox1.Text) - 1)
                TextBox3.Text = Left(sCC(2).Trim, 2)
                TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
                lbl_Full_Name.Text = sCC(1).Trim
                lbl_Full_Name.ForeColor = Color.Black
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
            Else
                If IsNumeric(TextBox1.Text) Then
                    lbl_tran_id.Text = AppUtils.Encrypt(TextBox1.Text, "CrOcOdIlE")
                End If
                TextBox1.Focus()
            End If

            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(TextBox1.Text)

            Dim store_cd As String = ""
            If Request("ip_store_cd") & "" <> "" Then
                store_cd = Request("ip_store_cd")
            Else
                store_cd = Session("store_cd")
            End If

            If (Not thePmtBiz.IsValidCreditCard(store_cd, Request("mop_cd"), TextBox1.Text)) Then
                lbl_Full_Name.Text = " ** Entry does not match selected MOP ** "
                lbl_Full_Name.ForeColor = Color.Red
                TextBox1.Text = ""
                lbl_tran_id.Text = ""
                btn_save.Enabled = False
            End If


            If TextBox1.Text & "" <> "" Then
                Session("lucy_card_num") = Trim(TextBox1.Text)  'lucy add
                Dim v_amt As Double = Request("amt")

                TextBox1.Text = StringUtils.MaskExceptLastFour(TextBox1.Text, "x")
            End If
            lbl_card_type.Text = myTypeValid.CCType
            If String.IsNullOrEmpty(myTypeValid.CCType) Or lbl_card_type.Text = "Unknown" Then
                btn_save.Enabled = False
                lbl_Full_Name.Text = " ** Invalid Credit Card Entry ** "
                lbl_Full_Name.ForeColor = Color.Red
            Else
                TextBox2.Focus()
            End If
        Catch
            lbl_Full_Name.Text = " ** " & Err.Description & " ** "
            lbl_Full_Name.ForeColor = Color.Red
        End Try

    End Sub
    Protected Sub TextBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

        Try
            Dim regex As Regex = New Regex("^[0-9]")
            Dim monthCheck As Match = regex.Match(TextBox2.Text)
            If monthCheck.Success AndAlso TextBox2.Text <> "" AndAlso CInt(TextBox2.Text) >= 1 AndAlso CInt(TextBox2.Text) <= 12 Then
                lbl_Full_Name.Visible = False
                TextBox3.Focus()
            Else
                lbl_Full_Name.Visible = True
                lbl_Full_Name.Text = ""
                lbl_Full_Name.Text = " ** Invalid Month ** "
                lbl_Full_Name.ForeColor = Color.Red
                TextBox2.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub TextBox3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

        Try
            Dim regex1 As Regex = New Regex("^[0-9]")
            Dim yearCheck As Match = regex1.Match(TextBox3.Text)
            If yearCheck.Success AndAlso TextBox3.Text <> "" Then
                lbl_Full_Name.Visible = False
                txt_security_code.Focus()
            Else
                lbl_Full_Name.Visible = True
                lbl_Full_Name.Text = ""
                lbl_Full_Name.Text = " ** Invalid year ** "
                lbl_Full_Name.ForeColor = Color.Red
                TextBox3.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        lbl_card_type.Text = ""
        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox1.Focus()
        btn_save.Enabled = True

    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        ' Daniela May 21
        btn_save.Visible = False
        btn_save.Enabled = False

        Dim connString As String
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim thistransaction As OracleTransaction
        Dim NEW_EXP_DT As String = ""
        Dim sql As String = ""

        TextBox1.Focus()
        ' btn_save.Attributes.Add("OnClick", "this.value='Processiong...'")

        ' lbl_amount.Text = "SWIPE/INSERT"
        btn_save.ForeColor = Color.Gray
        lbl_amount.ForeColor = Color.Purple


        If TextBox1.Text & "" = "" Then
            lbl_Full_Name.Text = "No Credit Card Number Found"
            lbl_Full_Name.Text = "SWIPE/INSERT CARD IN"
            ' lbl_Lucy_Test.Visible = True

            ' Exit Sub
        End If
        Dim dbl_len As Double = Len(Trim(TextBox1.Text))
        Dim str_disp As String = Session("lucy_display")
        If InStr(str_disp, "APPR") = 0 And dbl_len >= 15 And dbl_len < 25 And (TextBox2.Text & "" = "" Or TextBox3.Text & "" = "") Then  'lucy add
            ' Daniela do not save if masked
            If (InStr(TextBox1.Text, "xx") = 0) Then
                Session("lucy_card_num") = Trim(TextBox1.Text)  'lucy add
            End If
            ' lbl_Full_Name.Text = "No Expiration Date Found"  'lucy add
            Exit Sub
        End If
        ' If TextBox1.Text & "" = "" Then
        'lbl_Full_Name.Text = "No Credit Card Number Found"
        ' Exit Sub
        ' End If
        ' If TextBox2.Text & "" = "" Or TextBox3.Text & "" = "" Then
        'lbl_Full_Name.Text = "No Expiration Date Found"
        ' Exit Sub
        'End If
        If Len(Trim(TextBox1.Text)) > 25 Then
            lbl_Full_Name.Text = "Invalid Credit Card"
            Exit Sub
        End If
        'lucy comment the following lines
        ' If' TextBox1.Enabled = True And txt_security_code.Text & "" = "" Then
        ' lbl_Full_Name.Text = "You must enter the security code for non-swipe transactions"
        'txt_security_code.Focus()
        'Exit Sub
        ' ElseIf TextBox1.Enabled = True And Not IsNumeric(txt_security_code.Text) Then
        ' lbl_Full_Name.Text = "Security code must be numeric"
        'txt_security_code.Focus()
        '  Exit Sub
        ' End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        If IsNumeric(TextBox2.Text) = True And Len(TextBox2.Text) = 2 And Len(TextBox3.Text) = 2 And IsNumeric(TextBox3.Text) = True Then    'lucy add
            Try
                NEW_EXP_DT = MonthLastDay(TextBox2.Text & "/1/20" & TextBox3.Text)
            Catch
                lbl_Full_Name.Text = "Invalid Expiry Date"
                Exit Sub
            End Try
            If isNotEmpty(NEW_EXP_DT) Then ' Daniela
                Session("lucy_exp_dt") = NEW_EXP_DT  'lucy add
            End If
        End If

        '  ASPxLabel2.Visible = True
        Dim str_display As String = send_out_request()  'lucy  
        If InStr(str_display, "APPR") = 0 Then  'lucy add
            TextBox1.Text = ""                             'lucy add
            lbl_Full_Name.Visible = True
            If isEmpty(str_display) = False Then
                lbl_Full_Name.Text = str_display
            End If

            Exit Sub

        End If

        ' Fix missing exp dates for Gift Card and Others
        If isEmpty(NEW_EXP_DT) And isNotEmpty(Session("lucy_exp_dt")) Then ' Daniela added exp_date for Swipe transactions
            NEW_EXP_DT = Session("lucy_exp_dt")
        End If

        Dim cc_data As String = ""
        If lbl_tran_id.Text & "" <> "" Then
            cc_data = lbl_tran_id.Text
        Else
            cc_data = TextBox1.Text
        End If

        sql = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP,"
        If IsNumeric(txt_security_code.Text) Then
            sql = sql & " SECURITY_CD,"
        End If
        'jkl
        'sql = sql & " APPROVAL_CD) values ('" & Session.SessionID.ToString.Trim & "','" & Request("MOP_CD") & "','" & Request("AMT") & "','" & Request("DES") & "','" & cc_data & "','" & NEW_EXP_DT & "','BC'"
        sql = sql & " APPROVAL_CD) values (:SESSIONID, :MOP_CD, :AMT, (select des from mop where mop_cd = :MOP_CD), :CCDATA, :NEW_EXP_DT, 'BC'"

        If IsNumeric(txt_security_code.Text) Then
            'jkl
            'sql = sql & "," & txt_security_code.Text
            sql = sql & ", :SEC_CODE"

        End If
        sql = sql & ", :AUTH)"
        txt_auth.Text = Session("approval_cd")  'lucy add
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        thistransaction = objConnection.BeginTransaction
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = sql
            .Parameters.Add(":SESSIONID", OracleType.VarChar)
            .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            .Parameters.Add(":MOP_CD", OracleType.VarChar)
            ' Daniela May 12 use return mop code
            '.Parameters(":MOP_CD").Value = Request("MOP_CD")
            If isNotEmpty(Session("out_mop_cd")) Then
                .Parameters(":MOP_CD").Value = Session("out_mop_cd")
            Else
                .Parameters(":MOP_CD").Value = Request("MOP_CD")
            End If
            ' End Daniela
            .Parameters.Add(":AMT", OracleType.VarChar)
            .Parameters(":AMT").Value = Request("AMT")
            '.Parameters.Add(":DES", OracleType.VarChar)
            '.Parameters(":DES").Value = Request("DES")
            .Parameters.Add(":CCDATA", OracleType.VarChar)
            .Parameters(":CCDATA").Value = cc_data
            .Parameters.Add(":NEW_EXP_DT", OracleType.VarChar)
            .Parameters(":NEW_EXP_DT").Value = NEW_EXP_DT
            .Parameters.Add(":SEC_CODE", OracleType.VarChar)
            .Parameters(":SEC_CODE").Value = txt_security_code.Text
            .Parameters.Add(":AUTH", OracleType.VarChar)
            .Parameters(":AUTH").Value = txt_auth.Text
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()
        objConnection.Close()

        ' Dnaiela May 12 reset mop code
        Session("out_mop_cd") = Nothing

        Session("payment") = True

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        If LCase(Request("page")) = "paymentonaccount.aspx" Then
            Dim URL_String As String = ""
            URL_String = "?ord_total=" & Request("ord_total") & "&csh_pass=" & Request("csh_pass") & "&store_cd=" & Request("store_cd") & "&cust_cd=" & Request("cust_cd") & "&ivc_cd=" & Request("ivc_cd") & "&pmt_tp_cd=" & Request("pmt_tp_cd")
            Response.Write("parentWindow.location.href='" & Request("page") & URL_String & "';")
        ElseIf LCase(Request("page")) = "independentpaymentprocessing.aspx" Then
            Dim query_string As String = ""
            If Request("ip_ivc_cd") & "" <> "" Then
                query_string = query_string & "ip_ivc_cd=" & Request("ip_ivc_cd") & "&"
            End If
            If Request("ip_csh_pass") & "" <> "" Then
                query_string = query_string & "ip_csh_pass=" & Request("ip_csh_pass") & "&"
            End If
            If Request("ip_store_cd") & "" <> "" Then
                query_string = query_string & "ip_store_cd=" & Request("ip_store_cd") & "&"
            End If
            If Request("ip_csh_drw") & "" <> "" Then
                query_string = query_string & "ip_csh_drw=" & Request("ip_csh_drw") & "&"
            End If
            If Request("pmt_tp_cd") & "" <> "" Then
                query_string = query_string & "pmt_tp_cd=" & Request("pmt_tp_cd") & "&"
            End If
            If Request("ip_cust_cd") & "" <> "" Then
                query_string = query_string & "ip_cust_cd=" & Request("ip_cust_cd") & "&"
            End If
            If Request("referrer") & "" <> "" Then
                query_string = query_string & "referrer=" & Request("referrer") & "&"
            End If
            'to indicate to the independentPaymentProcessing page that a refresh is needed since a CC pmt got added
            query_string = query_string & "refresh=true"
            'jkl might want to encrypt variable info before showing them in clear text in URL

            Response.Write("parentWindow.location.href='independentpaymentprocessing.aspx?" & query_string & "';")
            ' Daniela GiftCard
        ElseIf LCase(Request("page")) = "wgc_purchasegiftcard.aspx" Then
            Response.Write("parentWindow.location.href='WGC_PurchaseGiftCard.aspx';")
        Else
            Response.Write("parentWindow.location.href='payment.aspx';")
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Public Sub CreateCustomer(ByVal sessionId As String, ByVal custCd As String, ByVal storeCd As String)

        If custCd = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & sessionId & "' AND CUST_CD='" & custCd & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            custCd = CustomerUtils.Create_CUSTOMER_CODE("", storeCd,
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))

            Session("cust_cd") = custCd

            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = custCd
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & custCd & "' WHERE Session_ID='" & sessionId & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub


End Class
