<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Startup.aspx.vb" Inherits="Startup"
    MasterPageFile="~/Regular.master"%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />       
    <script language="javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        function pageLoad(sender, args) {
            $('input[type=submit], a, button').click(function (event) {
                if ($(this).attr('href') != null) {
                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {
                        $.ajax({
                            type: "POST",
                            url: "Startup.aspx/SetSessionIsFirstVisit",
                            data: "{}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                            }
                        });
                    }
                }
            });
        }    </script>

    <div>
        <table width="99%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <dx:ASPxLabel ID="lblTranDt" runat="server" Text="<%$ Resources:LibResources, Label668 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="lblFollowUpTime" runat="server" Text="<%$ Resources:LibResources, Label608 %>" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <br />
                    <dx:ASPxCalendar ID="txttrandt" runat="server" AutoPostBack="True">
                    </dx:ASPxCalendar>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="txt_appt_time" runat="server" Width="50px" Visible="False" AutoPostBack="true">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbo_am_or_pm" Visible="False" AutoPostBack="True" runat="server"
                                    IncrementalFilteringMode ="StartsWith" ValueType="System.String" Width="45px">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="AM" Value="AM" />
                                        <dx:ListEditItem Text="PM" Value="PM" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbo_call_type" Visible="False" AutoPostBack="True" runat="server"
                                    IncrementalFilteringMode ="StartsWith" ValueType="System.String" Width="120px">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="<%$ Resources:LibResources, Label52 %>" Value="PHONE" />
                                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label238 %>" Value="STORE" />
                                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label237 %>" Value="HOME" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxComboBox ID="cbo_ord_tp_cd" Visible="True" AutoPostBack="True" runat="server"
                        IncrementalFilteringMode ="StartsWith" ValueType ="System.String" Width="279px">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="<%$ Resources:LibResources, Label494 %>" Value="SAL" />
                            <dx:ListEditItem Text="<%$ Resources:LibResources, Label126 %>" Value="CRM" />
                            <dx:ListEditItem Text="<%$ Resources:LibResources, Label295 %>" Value="MCR" />
                            <dx:ListEditItem Text="<%$ Resources:LibResources, Label306 %>" Value="MDB" />
                        </Items>
                    </dx:ASPxComboBox>
                    <br />
                    <dx:ASPxLabel ID="lblStoreCode" runat="server" Text="<%$ Resources:LibResources, Label573 %>">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxComboBox ID="cboStores" Width="280px" AutoPostBack="True" runat="server" 
                        SelectedIndex="0" IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                    <br />
                    <dx:ASPxLabel ID="lbl_cash_drw" runat="server" Text="<%$ Resources:LibResources, Label64 %>" Visible="True">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxComboBox ID="cbo_csh_drw" Width="280px" AutoPostBack="True" runat="server"
                        IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                    <br />
                    <dx:ASPxLabel ID="lblSls1" runat="server" Text="<%$ Resources:LibResources, Label687 %>">
                    </dx:ASPxLabel>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxComboBox ID="cboSalesperson1" Width="202px" AutoPostBack="True" runat="server" 
                                    OnTextChanged="UpdateSalesperson" IncrementalFilteringMode ="StartsWith">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_pct_1" Text="100" runat="server" Width="50px" Visible="True"
                                    AutoPostBack="true">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="lblComm1" runat="server" Text="%">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <dx:ASPxLabel ID="Label1" runat="server" Text="<%$ Resources:LibResources, Label688 %>">
                    </dx:ASPxLabel>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxComboBox ID="cboSalesperson2" Width="202px" AutoPostBack="True" runat="server" 
                                    OnTextChanged="UpdateSalesperson" IncrementalFilteringMode ="StartsWith" ValueType="System.String">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_pct_2" Text="0" runat="server" Width="50px" Visible="True"
                                    AutoPostBack="true">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="lblComm2" runat="server" Text="%">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">&nbsp;
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxComboBox ID="cboPD" Visible="True" AutoPostBack="True" runat="server" 
                                    IncrementalFilteringMode ="StartsWith" ValueType="System.String" Width="80px">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="<%$ Resources:LibResources, Label151 %>" Value="D" />
                                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label392 %>" Value="P" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbo_pd_store_cd" Width="247px" AutoPostBack="True" runat="server"
                                    IncrementalFilteringMode ="StartsWith">
                                </dx:ASPxComboBox>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="padding-top: 5px;">
                                <dx:ASPxLabel ID="lblPostalCd" runat="server" Text="<%$ Resources:LibResources, Label422 %>">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left">
                                <dx:ASPxTextBox ID="txtPostalCd" runat="server" Width="80px" Visible="True" AutoPostBack="true">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <dx:ASPxLabel ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label366 %>">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxComboBox ID="cboOrdSrt" Width="250px" AutoPostBack="True" runat="server"
                        IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                    <br />
                    <dx:ASPxLabel ID="Label3" runat="server" Text="<%$ Resources:LibResources, Label218 %>" Visible="False">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxComboBox ID="cbo_general" Width="250px" AutoPostBack="True" Visible="False"
                        runat="server" IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                </td>
            </tr>
        </table>
    </div>

    <%--<div align="center">
        &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" Height="36px" ImageUrl="~/images/Custom_Fields.PNG"
            OnClick="ImageButton1_Click" Width="36px" AlternateText="Custom Fields" Enabled="False"
            Visible="False" /></div>--%>

    <br />
    <dx:ASPxLabel ID="lbl_warnings" runat="server" ForeColor="#FF0000" Text="" Visible="False"
        Width="100%">
    </dx:ASPxLabel>
    &nbsp;
    <br />
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="ASPxPopupControl1" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label371 %>" Height="290px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px"  >
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <%--<dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
        HeaderText="Custom Fields" Height="170px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="493px" AllowDragging="True" ContentUrl="~/User_Defined_Fields.aspx">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>--%>

    <dxpc:ASPxPopupControl ID="PopupTax" runat="server" HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None"
        CloseAction="CloseButton" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
    <br />
    <asp:Label ID="lbl_Error" runat="server" CssClass="style5" Font-Bold="True" ForeColor="Red"
        Width="392px"></asp:Label>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
