Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'JAM added 20Oct2014
Imports DevExpress.Web.ASPxEditors
Imports System.Data


Partial Class TerminalMaintenance
    Inherits POSBasePage

    Public Const gs_version As String = "Version 1.1"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. Basic terminal maintenance query, insert, update and delete functionality.
    '
    '   RULES
    '   INSERT/UPDATE
    '       - no duplicate pinpad IDs - across the board
    '       - if IP addr exists already, error
    '   UPDATE
    '       - can't change pinpad ID if transactions exist
    '   DELETE
    '       - don't allow delete of pinpad where transactions exist already
    '               
    '   1.1:    Bug fixes and changed logic around pinpad ID being the primary key for terminals (due to 'NA' possibilities).
    '           Since no constraints exist on the GP_PINPAD_MAPPING table, it has been determined that the PC_NM will be used as
    '           a unique value - this has been confirmed with Richard McCoy since Host PCs will require a unique name on the network.
    '           Insert, Update and Delete functions have been modified to reflect this change.
    '
    '   1.1.1:  Modified store drop down list to display stores based on company group code, not company code. Similar to IALQ.
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetAllFields()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Resets/clears all fields.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        'lbl_resultsInfo.Visible = False
        'lbl_resultsInfo.Text = "--"

        'txt_pinpad_id.Text = ""
        'txt_pc_name.Text = ""
        'txt_server_ip.Text = ""
        'txt_server_port.Text = ""
        'txt_printergrpidnm.Text = ""
        'txt_laser_prn_nm.Text = ""
        'txt_refund_limit.Text = ""
        txt_store_cd.Text = ""

        txt_svs_div_cd.Text = ""
        txt_svs_store_num.Text = ""
        txt_td_merchant_num.Text = ""
        txt_td_store_num.Text = ""

        GridViewTerminalDetails.DataSource = ""
        GridViewTerminalDetails.DataBind()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveTerminalHeaderInfo()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Main procedure which populates the store terminal header info based on store code parameter chosen by the user.
    '   Called by:      "Search" button - btn_Search_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveTerminalHeaderInfo(p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT gp_merch.store_cd, gp_merch.svs_div_cd, gp_merch.svs_store_num, gp_merch.td_store_num, gp_merch.td_merchant_num "
        SQL = SQL & "FROM gp_pinpad_merchant gp_merch "
        SQL = SQL & "WHERE gp_merch.store_cd = UPPER(:x_storeCode) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                txt_svs_div_cd.Text = Mydatareader.Item("SVS_DIV_CD").ToString
                txt_svs_store_num.Text = Mydatareader.Item("SVS_STORE_NUM").ToString
                txt_td_store_num.Text = Mydatareader.Item("td_store_num").ToString
                txt_td_merchant_num.Text = Mydatareader.Item("td_merchant_num").ToString

            Else

                Dim s_errorMessage As String = ""
                s_errorMessage = "ERROR: No Data found for store [" + p_storeCode + "]"
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveTerminalDetailInfo()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Main procedure which populates the store terminal detail info based on store code parameter chosen by the user.
    '   Called by:      "Search" button - btn_Search_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveTerminalDetailInfo(p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT gp_map.store_cd, TO_CHAR(gp_map.pinpad_id) as pinpad_id, gp_map.pc_nm as pc_name, gp_map.pc_ipaddr as pc_ip_address, gp_map.printergrpidnm as printer_groupid_name, "
        SQL = SQL & "gp_map.laser_prn_nm as laser_printer_name, gp_map.refund_limit, gp_map.server_ip, gp_map.server_port "
        SQL = SQL & "FROM gp_pinpad_mapping gp_map "
        SQL = SQL & "WHERE gp_map.store_cd = UPPER(:x_storeCode) "
        SQL = SQL & "ORDER BY gp_map.pinpad_id"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Dim ds_GridViewTerminalDetails As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
        oAdp.Fill(ds_GridViewTerminalDetails)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

            Else

                Dim s_errorMessage As String = ""

                s_errorMessage = "ERROR: No Data found for store [" + p_storeCode + "]"

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        GridViewTerminalDetails.DataSource = ds_GridViewTerminalDetails
        GridViewTerminalDetails.DataBind()

        'Dim dv As DataView = New DataView(ds_GridViewTerminalDetails.Tables(0))
        'dv.RowFilter = something
        'For Each drv As DataRowView In dv
        '    MsgBox(" {0} " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des"))
        'Next
        'If b_Continue Then
        'End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL

        Dim s_co_cd As String = Session("CO_CD")
        'Dim s_co_grp_cd As String = Session("CO_GRP_CD")
        Dim s_employee_code As String = Session("EMP_CD")
        'MsgBox("company group code [" + s_co_grp_cd + "]; company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty Then
            'IT Request 2503 remove MCCL 
            'Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store order by store_cd"
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc "
            SQL = SQL & "FROM store s "
            SQL = SQL & "ORDER BY store_cd"

            'SQL = SQL & "FROM store s, co_grp cg "
            'SQL = SQL & "WHERE s.co_cd = cg.co_cd "
            'SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_co_cd) ) "
            'SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)
            'cmdGetCodes.Parameters.Add(":x_co_cd", OracleType.VarChar)
            'cmdGetCodes.Parameters(":x_co_cd").Value = s_co_cd

            txt_store_cd.Items.Insert(0, "Select a Store Code")
            txt_store_cd.Items.FindByText("Select a Store Code").Value = ""
            txt_store_cd.SelectedIndex = 0

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_store_cd
                    .DataSource = ds
                    .DataValueField = "store_cd"
                    .DataTextField = "full_store_desc"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation, Title:="Error")
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'lbl_resultsInfo.Visible = False

        If Not IsPostBack Then
            '    If Find_Security("ISTM", Session("emp_cd")) = "N" Then
            'btn_Lookup.Enabled = False
            'btn_Clear.Enabled = False
            'lbl_Cust_Info.Text = "You don't have the appropriate securities to view Franchise-Store percentages."
            'End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then

                btn_Search.Enabled = False

            Else

                'JAM: populate the store codes drop-down list
                PopulateStoreCodes()

            End If
        End If


        If (Not GridViewTerminalDetails.DataSource Is Nothing) Then
            GridViewTerminalDetails.DataBind()
        End If

        GridViewTerminalDetails.Visible = True

        lbl_versionInfo.Text = gs_version

        'default readonly fields
        'txt_svs_div_cd.ReadOnly = True
        'txt_svs_store_num.ReadOnly = True
        'txt_td_store_num.ReadOnly = True
        'txt_td_merchant_num.ReadOnly = True
        'txt_pinpad_id.ReadOnly = True
        'txt_pc_ip_address.ReadOnly = True
        'txt_pc_name.ReadOnly = True
        'txt_printergrpidnm.ReadOnly = True
        'txt_refund_limit.ReadOnly = True
        'txt_laser_prn_nm.ReadOnly = True
        'txt_server_ip.ReadOnly = True
        'txt_server_port.ReadOnly = True
        lbl_resultsInfo.Visible = False
        lbl_resultsInfo.Text = ""

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_store_cd.Enabled = True

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Clear"
        Response.Redirect("TerminalMaintenance.aspx")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Search_Click()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click

        RetrieveTerminalHeaderInfo(txt_store_cd.Text)
        RetrieveTerminalDetailInfo(txt_store_cd.Text)

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_UpdateStoreInfo_Click()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_UpdateStoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_UpdateStoreInfo.Click

        InsertUpdateTerminalHeaderInfo(txt_store_cd.Text)

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AddError()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub AddError(ByVal errors As Dictionary(Of GridViewColumn, String), ByVal column As GridViewColumn, ByVal errorText As String)
        If errors.ContainsKey(column) Then
            Return
        End If
        errors(column) = errorText
    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           GridViewTerminalDetails_InitNewRow()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub GridViewTerminalDetails_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)
        e.NewValues("STORE_CD") = txt_store_cd.Text
        e.NewValues("PINPAD_ID") = Nothing
        e.NewValues("PC_NAME") = Nothing
        e.NewValues("PC_IP_ADDRESS") = Nothing
        e.NewValues("PRINTER_GROUPID_NAME") = Nothing
        e.NewValues("LASER_PRINTER_NAME") = Nothing
        e.NewValues("REFUND_LIMIT") = Nothing
        e.NewValues("SERVER_IP") = Nothing
        e.NewValues("SERVER_PORT") = Nothing

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           IsValidPrinterGroupID()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates user entered printer group ID
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function IsValidPrinterGroupID(p_printerGroupID As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT printergrpidnm "
        SQL = SQL & "FROM gp_pinpad_printer_grp "
        SQL = SQL & "WHERE printergrpidnm = UPPER(:x_printerGroupID) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_printerGroupID", OracleType.VarChar)
        objsql.Parameters(":x_printerGroupID").Value = p_printerGroupID

        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_Continue

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfSomethingAlreadyExists()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that passed in property already exists
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function checkIfSomethingAlreadyExists(p_tableName As String, p_keyColumnName As String, p_keyColumnValue As String, p_columnName As String, p_columnValue As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM " & p_tableName & " "
        SQL = SQL & "WHERE " & p_keyColumnName & " <> UPPER(:x_keyColumnValue) "
        SQL = SQL & "AND " & p_columnName & " = UPPER(:x_columnValue) "


        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_keyColumnValue", OracleType.VarChar)
        objsql.Parameters(":x_keyColumnValue").Value = p_keyColumnValue

        objsql.Parameters.Add(":x_columnValue", OracleType.VarChar)
        objsql.Parameters(":x_columnValue").Value = p_columnValue


        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfPCNameAlreadyExists()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that passed in PC Name already exists
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function checkIfPCNameAlreadyExists(p_PCName As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM gp_pinpad_mapping "
        SQL = SQL & "WHERE pc_nm = UPPER(:x_pc_name) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        objsql.Parameters(":x_pc_name").Value = p_PCName

        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfPCIPAddressAlreadyExists()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that passed in PC IP Address already exists
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function checkIfPCIPAddressAlreadyExists(p_PCIPAddress As String, p_PCName As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM gp_pinpad_mapping "
        SQL = SQL & "WHERE pc_ipaddr = UPPER(:x_pc_ipaddr) "
        SQL = SQL & "AND pc_nm = UPPER(:x_pc_name) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_pc_ipaddr", OracleType.VarChar)
        objsql.Parameters(":x_pc_ipaddr").Value = p_PCIPAddress

        objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        objsql.Parameters(":x_pc_name").Value = p_PCName


        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfTerminalAlreadyExists()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that passed in pinpad ID has transactions or not
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    'Private Function checkIfTerminalAlreadyExists(p_pinpadID As String, p_PCName As String) As Boolean
    Private Function checkIfTerminalAlreadyExists(p_pinpadID As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM gp_pinpad_mapping "
        SQL = SQL & "WHERE pinpad_id = UPPER(:x_pinpadID) "
        '        SQL = SQL & "AND pc_nm = UPPER(:x_pc_name) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_pinpadID", OracleType.VarChar)
        objsql.Parameters(":x_pinpadID").Value = p_pinpadID

        'objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        'objsql.Parameters(":x_pc_name").Value = p_PCName

        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfTerminalHasTransactions()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that passed in pinpad ID has transactions or not
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function checkIfTerminalHasTransactions(p_pinpadID As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM pos_electronic_transaction pet "
        SQL = SQL & "WHERE pet.trm = UPPER(:x_pinpadID) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_pinpadID", OracleType.VarChar)
        objsql.Parameters(":x_pinpadID").Value = p_pinpadID

        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           checkIfTerminalHeaderInfoExists()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Validates that the terminal header (store) information exists or not
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function checkIfTerminalHeaderInfoExists(p_storeCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_result As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT count(*) as n_count "
        SQL = SQL & "FROM gp_pinpad_merchant "
        SQL = SQL & "WHERE store_cd = UPPER(:x_storeCode) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)

        Try
            'Open Connection 
            conn.Open()

            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            'Store Values in String Variables 
            If Mydatareader.Read() Then
                Dim n_count As Integer = Mydatareader.Item("N_COUNT")
                If n_count > 0 Then
                    b_result = True
                Else
                    b_result = False
                End If
                Mydatareader.Close()
            End If
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_result

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           InsertUpdateTerminalHeaderInfo()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    TODO
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub InsertUpdateTerminalHeaderInfo(p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = True

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim s_errorMessage As String = ""

        If p_storeCode Is Nothing Then
            s_errorMessage = "ERROR: Store Code is required!"
            'Throw New Exception(s_errorMessage)
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            b_Continue = False
        End If

        If (checkIfSomethingAlreadyExists("GP_PINPAD_MERCHANT", "STORE_CD", txt_store_cd.Text, "TD_STORE_NUM", txt_td_store_num.Text)) Then
            s_errorMessage = "ERROR: TD Store # [" + txt_td_store_num.Text + "] already used!"
            'Throw New Exception(s_errorMessage)
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            b_Continue = False
        End If

        If Not (txt_svs_store_num.Text.Equals("N/A")) AndAlso (checkIfSomethingAlreadyExists("GP_PINPAD_MERCHANT", "STORE_CD", txt_store_cd.Text, "SVS_STORE_NUM", txt_svs_store_num.Text)) Then
            s_errorMessage = "ERROR: SVS Store # [" + txt_svs_store_num.Text + "] already used!"
            'Throw New Exception(s_errorMessage)
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            b_Continue = False
        End If

        If (b_Continue) Then

            'check if store data exists first and if not, INSERT, otherwise UPDATE
            If (checkIfTerminalHeaderInfoExists(p_storeCode)) Then

                SQL = "UPDATE gp_pinpad_merchant SET "
                SQL = SQL & "svs_div_cd = UPPER(:x_svs_div_cd), "
                SQL = SQL & "svs_store_num = UPPER(:x_svs_store_num), "
                SQL = SQL & "td_store_num = UPPER(:x_td_store_num), "
                SQL = SQL & "td_merchant_num = UPPER(:x_td_merchant_num) "
                SQL = SQL & "WHERE store_cd = UPPER(:x_storeCode) "

            Else

                SQL = "INSERT INTO gp_pinpad_merchant "
                SQL = SQL & "( store_cd, svs_div_cd, svs_store_num, td_store_num, td_merchant_num ) "
                SQL = SQL & "VALUES "
                SQL = SQL & "( UPPER(:x_storeCode), UPPER(:x_svs_div_cd), UPPER(:x_svs_store_num), UPPER(:x_td_store_num), UPPER(:x_td_merchant_num) ) "

            End If

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode

            objsql.Parameters.Add(":x_svs_div_cd", OracleType.VarChar)
            objsql.Parameters(":x_svs_div_cd").Value = txt_svs_div_cd.Text

            objsql.Parameters.Add(":x_svs_store_num", OracleType.VarChar)
            objsql.Parameters(":x_svs_store_num").Value = txt_svs_store_num.Text

            objsql.Parameters.Add(":x_td_store_num", OracleType.VarChar)
            objsql.Parameters(":x_td_store_num").Value = txt_td_store_num.Text

            objsql.Parameters.Add(":x_td_merchant_num", OracleType.VarChar)
            objsql.Parameters(":x_td_merchant_num").Value = txt_td_merchant_num.Text

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Mydatareader.Close()

            Catch
                'Throw New Exception("Error updating the record. Please try again.")
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If

        If (b_Continue) Then
            'lbl_resultsInfo.Visible = True
            'lbl_resultsInfo.Text = "Store header info updated"
            Dim s_displayMessage = "Store header info updated"
            DisplayMessage(s_displayMessage, "LABEL", "INFO")
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           InsertTerminalDetailInfoLineItem()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    TODO
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Function InsertTerminalDetailInfoLineItem(ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        If newValues("STORE_CD") Is Nothing Then
            Throw New Exception("Store Code is required!")
        End If
        If Not newValues("STORE_CD").Equals(txt_store_cd.Text) Then
            Throw New Exception("Store Code must equal the selected store [" & txt_store_cd.Text & "]!")
        End If


        If newValues("PINPAD_ID") Is Nothing Then
            Throw New Exception("Terminal ID is required!")
        End If
        'If (checkIfTerminalAlreadyExists(newValues("PINPAD_ID"))) Then
        If (checkIfTerminalAlreadyExists(newValues("PINPAD_ID"))) AndAlso Not (newValues("PINPAD_ID").Equals("NA")) Then
            Throw New Exception("Terminal ID [" + newValues("PINPAD_ID") + "] already used!")
        End If

        If newValues("PC_NAME") Is Nothing Then
            Throw New Exception("PC Name is required!")
        End If

        'If (checkIfSomethingAlreadyExists("GP_PINPAD_MAPPING", "PINPAD_ID", newValues("PINPAD_ID"), "PC_NM", newValues("PC_NAME"))) Then
        If (checkIfPCNameAlreadyExists(newValues("PC_NAME"))) Then
            Throw New Exception("PC Name [" + newValues("PC_NAME") + "] already used!")
        End If

        If newValues("PC_IP_ADDRESS") Is Nothing Then
            Throw New Exception("PC IP Address is required!")
        End If
        'If (checkIfTerminalIPAddressAlreadyExists(newValues("PC_IP_ADDRESS"))) Then
        'If (checkIfSomethingAlreadyExists("GP_PINPAD_MAPPING", "PINPAD_ID", newValues("PINPAD_ID"), "PC_IPADDR", newValues("PC_IP_ADDRESS"))) Then
        If (checkIfPCIPAddressAlreadyExists(newValues("PC_IP_ADDRESS"), newValues("PC_NAME"))) Then
            Throw New Exception("IP address [" + newValues("PC_IP_ADDRESS") + "] already used!")
        End If

        If newValues("PRINTER_GROUPID_NAME") Is Nothing Then
            Throw New Exception("Printer GroupID name is required!")
        End If
        If Not (IsValidPrinterGroupID(newValues("PRINTER_GROUPID_NAME"))) Then
            Throw New Exception("Printer GroupID [" + newValues("PRINTER_GROUPID_NAME") + "] is not valid!")
        End If

        If newValues("SERVER_IP") Is Nothing Then
            Throw New Exception("Server IP is required!")
        End If
        If newValues("SERVER_PORT") Is Nothing Then
            Throw New Exception("Server Port is required!")
        End If

        SQL = "INSERT INTO gp_pinpad_mapping "
        SQL = SQL & "( store_cd, pinpad_id, pc_nm, pc_ipaddr, printergrpidnm, laser_prn_nm, refund_limit, server_ip, server_port ) "
        SQL = SQL & "VALUES "
        SQL = SQL & "( UPPER(:x_storeCode), UPPER(:x_pinpadID), UPPER(:x_pc_name), UPPER(:x_pc_ip_address), UPPER(:x_printer_groupid_name), UPPER(:x_laser_printer_name), :x_refund_limit, UPPER(:x_server_ip), :x_server_port ) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = newValues("STORE_CD")


        objsql.Parameters.Add(":x_pinpadID", OracleType.VarChar)
        Dim x_pinpadID As String
        If newValues("PINPAD_ID") Is Nothing Then
            x_pinpadID = ""
        Else
            x_pinpadID = newValues("PINPAD_ID")
        End If
        objsql.Parameters(":x_pinpadID").Value = x_pinpadID

        objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        Dim x_pc_name As String
        If newValues("PC_NAME") Is Nothing Then
            x_pc_name = ""
        Else
            x_pc_name = newValues("PC_NAME")
        End If
        objsql.Parameters(":x_pc_name").Value = x_pc_name

        objsql.Parameters.Add(":x_pc_ip_address", OracleType.VarChar)
        Dim x_pc_ip_address As String
        If newValues("PC_IP_ADDRESS") Is Nothing Then
            x_pc_ip_address = ""
        Else
            x_pc_ip_address = newValues("PC_IP_ADDRESS")
        End If
        objsql.Parameters(":x_pc_ip_address").Value = x_pc_ip_address

        objsql.Parameters.Add(":x_printer_groupid_name", OracleType.VarChar)
        Dim x_printer_groupid_name As String
        If newValues("PRINTER_GROUPID_NAME") Is Nothing Then
            x_printer_groupid_name = ""
        Else
            x_printer_groupid_name = newValues("PRINTER_GROUPID_NAME")
        End If
        objsql.Parameters(":x_printer_groupid_name").Value = x_printer_groupid_name

        objsql.Parameters.Add(":x_laser_printer_name", OracleType.VarChar)
        Dim x_laser_printer_name As String
        If newValues("LASER_PRINTER_NAME") Is Nothing Then
            x_laser_printer_name = ""
        Else
            x_laser_printer_name = newValues("LASER_PRINTER_NAME")
        End If
        objsql.Parameters(":x_laser_printer_name").Value = x_laser_printer_name

        objsql.Parameters.Add(":x_refund_limit", OracleType.Number)
        Dim x_refund_limit As String
        If newValues("REFUND_LIMIT") Is Nothing Then
            objsql.Parameters(":x_refund_limit").Value = DBNull.Value
        Else
            x_refund_limit = newValues("REFUND_LIMIT")
            objsql.Parameters(":x_refund_limit").Value = x_refund_limit
        End If

        objsql.Parameters.Add(":x_server_ip", OracleType.VarChar)
        Dim x_server_ip As String
        If newValues("SERVER_IP") Is Nothing Then
            x_server_ip = ""
        Else
            x_server_ip = newValues("SERVER_IP")
        End If
        objsql.Parameters(":x_server_ip").Value = x_server_ip

        objsql.Parameters.Add(":x_server_port", OracleType.Number)
        Dim x_server_port As String
        If newValues("SERVER_PORT") Is Nothing Then
            x_server_port = ""
        Else
            x_server_port = newValues("SERVER_PORT")
        End If
        objsql.Parameters(":x_server_port").Value = x_server_port

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            'Throw New Exception("Error updating the record. Please try again.")
            Throw
            conn.Close()
        End Try
        conn.Close()

        'lbl_resultsInfo.Visible = True
        'lbl_resultsInfo.Text = "Record Inserted"

        GridViewTerminalDetails.DataBind()

        Return True

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           UpdateTerminalDetailInfoLineItem()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    TODO
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Function UpdateTerminalDetailInfoLineItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary, ByVal oldValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        If newValues("STORE_CD") Is Nothing Then
            Throw New Exception("Store Code is required!")
        End If
        If Not newValues("STORE_CD").Equals(txt_store_cd.Text) Then
            Throw New Exception("Store Code must equal the selected store [" & txt_store_cd.Text & "]!")
        End If

        If newValues("PINPAD_ID") Is Nothing Then
            Throw New Exception("Terminal ID is required!")
        End If

        'If pinpadID has been changed, check to make sure it doesn't have transactions yet
        If newValues("PINPAD_ID") <> oldValues("PINPAD_ID") Then
            If (checkIfTerminalHasTransactions(oldValues("PINPAD_ID"))) Then
                Throw New Exception("Terminal ID [" + oldValues("PINPAD_ID") + "] cannot be changed as it has current transactions!")
            End If
            'If (checkIfTerminalAlreadyExists(newValues("PINPAD_ID"))) Then

            If (Not (newValues("PINPAD_ID").Equals("NA"))) Then
                If (checkIfTerminalAlreadyExists(newValues("PINPAD_ID"))) Then
                    Throw New Exception("Terminal ID [" + newValues("PINPAD_ID") + "] already used!")
                End If
            End If
        End If

        Dim s_usePinpadID As String = newValues("PINPAD_ID")
        Dim s_usePCName As String = oldValues("PC_NAME")
        If newValues("PINPAD_ID").Equals("NA") Then
            'must use oldvalue of pinpad id to do update
            s_usePinpadID = oldValues("PINPAD_ID")
        End If
        If oldValues("PINPAD_ID").Equals("NA") Then
            'must use other fields to update properly
            s_usePinpadID = oldValues("PINPAD_ID")
        End If


        If newValues("PC_NAME") Is Nothing Then
            Throw New Exception("PC Name is required!")
        End If
        'If PC_NAME has been changed, check to make sure it doesn't already exist
        If newValues("PC_NAME") <> oldValues("PC_NAME") Then
            'If (checkIfSomethingAlreadyExists("GP_PINPAD_MAPPING", "PINPAD_ID", newValues("PINPAD_ID"), "PC_NM", newValues("PC_NAME"))) Then
            If (checkIfPCNameAlreadyExists(newValues("PC_NAME"))) Then
                Throw New Exception("PC Name [" + newValues("PC_NAME") + "] already used!")
            End If
        End If

        If newValues("PC_IP_ADDRESS") Is Nothing Then
            Throw New Exception("PC IP Address is required!")
        End If
        If newValues("PC_IP_ADDRESS") <> oldValues("PC_IP_ADDRESS") Then
            'If (checkIfSomethingAlreadyExists("GP_PINPAD_MAPPING", "PINPAD_ID", newValues("PINPAD_ID"), "PC_IPADDR", newValues("PC_IP_ADDRESS"))) Then
            If (checkIfPCIPAddressAlreadyExists(newValues("PC_IP_ADDRESS"), oldValues("PC_NAME"))) Then
                Throw New Exception("IP address [" + newValues("PC_IP_ADDRESS") + "] already used!")
            End If
        End If

        If newValues("PRINTER_GROUPID_NAME") Is Nothing Then
            Throw New Exception("Printer GroupID name is required!")
        End If
        If Not (IsValidPrinterGroupID(newValues("PRINTER_GROUPID_NAME"))) Then
            Throw New Exception("Printer GroupID [" + newValues("PRINTER_GROUPID_NAME") + "] is not valid!")
        End If

        If newValues("SERVER_IP") Is Nothing Then
            Throw New Exception("Server IP is required!")
        End If
        If newValues("SERVER_PORT") Is Nothing Then
            Throw New Exception("Server Port is required!")
        End If

        SQL = "UPDATE gp_pinpad_mapping SET "
        SQL = SQL & "pinpad_id = :x_pinpadID, "
        SQL = SQL & "pc_nm = UPPER(:x_pc_name), "
        SQL = SQL & "pc_ipaddr = UPPER(:x_pc_ip_address), "
        SQL = SQL & "printergrpidnm = UPPER(:x_printer_groupid_name), "
        SQL = SQL & "laser_prn_nm = UPPER(:x_laser_printer_name), "
        SQL = SQL & "refund_limit = :x_refund_limit, "
        SQL = SQL & "server_ip = UPPER(:x_server_ip), "
        SQL = SQL & "server_port = :x_server_port "
        SQL = SQL & "WHERE store_cd = UPPER(:x_storeCode) "
        SQL = SQL & "AND pinpad_id = UPPER(:x_usePinpadID) "
        SQL = SQL & "AND UPPER(pc_nm) = UPPER(:x_usePCName) "


        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = newValues("STORE_CD")


        objsql.Parameters.Add(":x_pinpadID", OracleType.VarChar)
        Dim x_pinpadID As String
        If newValues("PINPAD_ID") Is Nothing Then
            x_pinpadID = ""
        Else
            x_pinpadID = newValues("PINPAD_ID")
        End If
        objsql.Parameters(":x_pinpadID").Value = x_pinpadID

        objsql.Parameters.Add(":x_usePinpadID", OracleType.VarChar)
        objsql.Parameters(":x_usePinpadID").Value = s_usePinpadID

        objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        Dim x_pc_name As String
        If newValues("PC_NAME") Is Nothing Then
            x_pc_name = ""
        Else
            x_pc_name = newValues("PC_NAME")
        End If
        objsql.Parameters(":x_pc_name").Value = x_pc_name

        objsql.Parameters.Add(":x_usePCName", OracleType.VarChar)
        objsql.Parameters(":x_usePCName").Value = s_usePCName

        objsql.Parameters.Add(":x_pc_ip_address", OracleType.VarChar)
        Dim x_pc_ip_address As String
        If newValues("PC_IP_ADDRESS") Is Nothing Then
            x_pc_ip_address = ""
        Else
            x_pc_ip_address = newValues("PC_IP_ADDRESS")
        End If
        objsql.Parameters(":x_pc_ip_address").Value = x_pc_ip_address

        objsql.Parameters.Add(":x_printer_groupid_name", OracleType.VarChar)
        Dim x_printer_groupid_name As String
        If newValues("PRINTER_GROUPID_NAME") Is Nothing Then
            x_printer_groupid_name = ""
        Else
            x_printer_groupid_name = newValues("PRINTER_GROUPID_NAME")
        End If
        objsql.Parameters(":x_printer_groupid_name").Value = x_printer_groupid_name

        objsql.Parameters.Add(":x_laser_printer_name", OracleType.VarChar)
        Dim x_laser_printer_name As String
        If newValues("LASER_PRINTER_NAME") Is Nothing Then
            x_laser_printer_name = ""
        Else
            x_laser_printer_name = newValues("LASER_PRINTER_NAME")
        End If
        objsql.Parameters(":x_laser_printer_name").Value = x_laser_printer_name

        objsql.Parameters.Add(":x_refund_limit", OracleType.Number)
        Dim x_refund_limit As String
        If newValues("REFUND_LIMIT") Is Nothing Then
            objsql.Parameters(":x_refund_limit").Value = DBNull.Value
        Else
            x_refund_limit = newValues("REFUND_LIMIT")
            objsql.Parameters(":x_refund_limit").Value = x_refund_limit
        End If

        objsql.Parameters.Add(":x_server_ip", OracleType.VarChar)
        Dim x_server_ip As String
        If newValues("SERVER_IP") Is Nothing Then
            x_server_ip = ""
        Else
            x_server_ip = newValues("SERVER_IP")
        End If
        objsql.Parameters(":x_server_ip").Value = x_server_ip

        objsql.Parameters.Add(":x_server_port", OracleType.Number)
        Dim x_server_port As String
        If newValues("SERVER_PORT") Is Nothing Then
            x_server_port = ""
        Else
            x_server_port = newValues("SERVER_PORT")
        End If
        objsql.Parameters(":x_server_port").Value = x_server_port

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            'Throw New Exception("Error updating the record. Please try again.")
            Throw
            conn.Close()
        End Try
        conn.Close()

        'lbl_resultsInfo.Visible = True
        'lbl_resultsInfo.Text = "Record(s) Updated"

        GridViewTerminalDetails.DataBind()

        Return True

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DeleteTerminalDetailInfoLineItem()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    TODO
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Function DeleteTerminalDetailInfoLineItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        If (checkIfTerminalHasTransactions(newValues("PINPAD_ID"))) Then
            Throw New Exception("Terminal ID [" + newValues("PINPAD_ID") + "] cannot be deleted as it has current transactions!")
        End If

        SQL = "DELETE "
        SQL = SQL & "FROM gp_pinpad_mapping "
        SQL = SQL & "WHERE store_cd = UPPER(:x_storeCode) "
        SQL = SQL & "AND pinpad_id = UPPER(:x_pinpadID) "
        SQL = SQL & "AND pc_nm = UPPER(:x_pc_name) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = newValues("STORE_CD")

        objsql.Parameters.Add(":x_pinpadID", OracleType.VarChar)
        Dim x_pinpadID As String
        If newValues("PINPAD_ID") Is Nothing Then
            x_pinpadID = ""
        Else
            x_pinpadID = newValues("PINPAD_ID")
        End If
        objsql.Parameters(":x_pinpadID").Value = x_pinpadID

        objsql.Parameters.Add(":x_pc_name", OracleType.VarChar)
        Dim x_pc_name As String
        If newValues("PC_NAME") Is Nothing Then
            x_pc_name = ""
        Else
            x_pc_name = newValues("PC_NAME")
        End If
        objsql.Parameters(":x_pc_name").Value = x_pc_name


        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            'Throw New Exception("Error updating the record. Please try again.")
            Throw
            conn.Close()
        End Try
        conn.Close()

        'lbl_resultsInfo.Visible = True
        'lbl_resultsInfo.Text = "Record Deleted"

        GridViewTerminalDetails.DataBind()

        Return True

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           GridViewTerminalDetails_BatchUpdate()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub GridViewTerminalDetails_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)

        For Each args In e.InsertValues
            InsertTerminalDetailInfoLineItem(args.NewValues)
        Next args
        For Each args In e.UpdateValues
            UpdateTerminalDetailInfoLineItem(args.Keys, args.NewValues, args.OldValues)
            RetrieveTerminalDetailInfo(txt_store_cd.Text)
        Next args
        For Each args In e.DeleteValues
            DeleteTerminalDetailInfoLineItem(args.Keys, args.Values)
            RetrieveTerminalDetailInfo(txt_store_cd.Text)
        Next args

        e.Handled = True

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           GridViewTerminalDetails_CellEditorInitialize()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub GridViewTerminalDetails_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        e.Editor.ReadOnly = False
        e.Editor.ClientEnabled = True
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           grid_HtmlRowPrepared()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub grid_HtmlRowPrepared(ByVal sender As Object, ByVal e As ASPxGridViewTableRowEventArgs)
        Dim hasError As Boolean
        If Not e.GetValue("STORE_CD") Is Nothing Then
            hasError = e.GetValue("STORE_CD").ToString().Length <= 1
        End If
        If hasError Then
            e.Row.ForeColor = System.Drawing.Color.Red
        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           grid_StartRowEditing()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridViewTerminalDetails.IsNewRowEditing) Then
            GridViewTerminalDetails.DoRowValidation()
        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           GridViewTerminalDetails_ParseValue()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Pre-defined
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub GridViewTerminalDetails_ParseValue(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxParseValueEventArgs)

        Dim hasError As Boolean
        Dim errorMsg As String = ""

        If hasError Then
            Throw New Exception(errorMsg)
        End If
    End Sub

End Class
