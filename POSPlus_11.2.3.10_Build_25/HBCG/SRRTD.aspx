<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SRRTD.aspx.vb" Inherits="SRRTD"    %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%--  CodeFile="SRRTD.aspx.vb" Inherits="SRRTD" meta:resourcekey="PageResource1" UICulture="auto" %>--%>
<%--
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>  --%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <table> 
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Store Group/Store" ID="lbl_store_grp_cd" Width="160px"></asp:Label>
                           
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_store" runat="server"></asp:DropDownList>
                            
                        </td>
                         <td align="right">
                         <asp:Label runat="server" Text="Store" ID="lbl_st" Width="160" Visible="False"></asp:Label>
                           
                        </td>

                        <td>
                            <asp:DropDownList ID="dw_st" runat="server" Visible="False"></asp:DropDownList>
                            
                        </td>

                    </tr>
                </table>
            </td>
            </tr>
            <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Written /Delivered  " ID="lbl_w_d"  Width="160px" ></asp:Label>
                          
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_w_d" runat="server"  Width="60px"></asp:DropDownList>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
            <tr>
               
            <td align="left">
                <table>
                    <tr>
                        
                        <td>
                         <asp:Label runat="server" Text="Inventory/Non Inventory" ID="lbl_inv_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_inv_tp" runat="server"  OnSelectedIndexChanged  ="get_inv_itm_tp"    AutoPostBack="True"  Width="60px"></asp:DropDownList>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
             <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Item       Type" ID="lbl_itm_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_itm_tp" runat="server"  Width="60px"></asp:DropDownList>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    
                                   

                    <tr align="right">
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_search" runat="server" Text="Search" onclick="click_btn_search"/> </asp:Button>
                                         
                                    </td>
                                    <td>
                                         <asp:Button ID="btn_clr_screen" runat="server" Text="Clear" onclick="btn_clr_screen_Click" /> </asp:Button>
                                        
                                    </td>
                                   
                                   
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
         <%-- sabrina r3442 --%>
        <tr align="left">
                        <td>
                            <table>
                                <tr>
                                  <td>
                                        <asp:Button ID="btn_slsp" runat="server" Font-Bold="true" Text="SLSP. SUMMARY" Visible="false" onclick="show_slsp_detail"/> </asp:Button>
                                         
                                    </td>   
                                </tr>
                            </table>
                        </td>
            </tr>
        <%-- sabrina R3442 --%>
            <tr>
            <td>
               <asp:DataGrid ID="GV_SRR" runat="server"  Width="1850px" BorderColor="Black" 
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="id"
                  Height="16px" AlternatingItemStyle-BackColor="Beige" ReadOnly ="true" AllowCustomPaging="True"  AllowPaging="True" 
                   Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="#99CCFF">
                   <tr>
                       <td align="center" Width="1567px">
                      ALL PRODUCTS

                       </td>
                       <td align="center" Width="283px">
                       OUTLET/CLEARANCE

                       </td>
                   </tr>

                            </table>'

CaptionAlign="Top">
        
<AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
        
                <Columns>
                     
                    
                 <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkBoxSelectPmt" runat="server" AutoPostBack="true"  OnCheckedChanged="show_detail" HeaderText="Select" CssClass="style5"  Width="10px" BackColor="#CC66FF" /> 
                                        </ItemTemplate>
                 </asp:TemplateColumn>
                   
                
                  <asp:BoundColumn DataField="id" HeaderText="id" Visible="False"></asp:BoundColumn>
                     <asp:BoundColumn DataField="s_f" HeaderText="" Visible="False"></asp:BoundColumn>
             <asp:BoundColumn DataField="store_cd" HeaderText="store"   ReadOnly="True"  HeaderStyle-Width="10" ItemStyle-Width="10" Visible="False">
<HeaderStyle Width="10px"></HeaderStyle>

<ItemStyle Width="10px"></ItemStyle>
                    </asp:BoundColumn> 
                     
             <asp:BoundColumn DataField="gross_sale" HeaderText="Gross Sales"  ItemStyle-Width="140px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle Width="140px"></ItemStyle>
                    </asp:BoundColumn>
                     
             <asp:BoundColumn DataField="void_sale" HeaderText="Credits/Void Sales"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn> 
             <asp:BoundColumn DataField="net_sale" HeaderText="Net Sales"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn> 
                     <asp:BoundColumn DataField="net_mgn" HeaderText="Net Mgn"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                     
                    <asp:BoundColumn DataField="furn_mix" HeaderText="Furn Mix"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:N2}"    >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="furn_net_mgn" HeaderText="Furn Mgn"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="matt_mix" HeaderText="Matt Mix"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="matt_net_mgn" HeaderText="Matt Mgn"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"   >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_mix" HeaderText="Appl Mix" ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_net_mgn" HeaderText="Appl Mgn"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_mix" HeaderText="Elec Mix"  ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_net_mgn" HeaderText="Elec Mgn" ItemStyle-Width="121px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="121px"></ItemStyle>
                    </asp:BoundColumn>
             <asp:BoundColumn DataField="id" HeaderText="ID" Visible="False" ItemStyle-Width="140px" >
<ItemStyle Width="140px"></ItemStyle>
                    </asp:BoundColumn>
             <asp:BoundColumn DataField="out_net_sale" HeaderText="Net Sales" ItemStyle-Width="90px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="90px"></ItemStyle>
                    </asp:BoundColumn> 
                     <asp:BoundColumn DataField="out_net_mgn" HeaderText="Net Mgn" ItemStyle-Width="90px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="90px"></ItemStyle>
                    </asp:BoundColumn>
               <asp:BoundColumn DataField="out_net_qty" HeaderText="Units" ItemStyle-Width="90px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="90px"></ItemStyle>
                    </asp:BoundColumn>
                
        </Columns>
       
                   <PagerStyle NextPageText=" " PrevPageText=" " />
       
    </asp:DataGrid>
                </td>
       </tr>
       
    </table>
     <asp:Label runat="server"   ID="lbl_msg"  Width="100%" ForeColor="Red"></asp:Label>
     <asp:Label runat="server"   ID="lbl_warning"  Width="100%" ForeColor="Red"></asp:Label>
  <%-- <dx:aspxlabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:aspxlabel> --%>
    <br />
    <br />
    <%------------------------------------------- sabrina R3442 ------------------------------ --%>
    <%-- =========== SLSP SUMMARY =======================================   --%> 
     <dxpc:ASPxPopupControl ID="ASPxPopupslsp" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="SALESPEOPLE SUMMARY" Height="90px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="1000px"   
                 CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
     
     <table>
    <tr>
      <td colspan="5">
           <asp:label ID="str_lbl" text="Select a Store:" Font-Bold="true" runat="server"></asp:label>
      </td>
      <td>
           <asp:DropDownList ID="slsp_strs" runat="server"></asp:DropDownList>
      </td>
      <td>
           <asp:Button ID="btn_summary" runat="server" Text="Display Summary" Font-Bold="true" OnClick="populate_slsp_detail" />
      </td>
     </tr>
     </table>
            <br />
            <br />
           
    <%-- Height="12px" AllowCustomPaging="True"  AllowPaging="True" bgcolor="#99CCFF" --%>
        <table>
            <tr>
            <td>
               <asp:DataGrid ID="GV_SLSP" runat="server"  Width="1300px" BorderColor="Black" 
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="id"
                   AlternatingItemStyle-BackColor="LightGray" ReadOnly ="true"  
                   Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="#A9A9A9">
                   <tr>
                       <td align="center" Width="1127px">
                      ALL PRODUCTS

                       </td>
                       <td align="center" Width="243px">
                       OUTLET/CLEARANCE

                       </td>
                   </tr>

                            </table>'

CaptionAlign="Top">
        
<AlternatingItemStyle BackColor="LightGray"></AlternatingItemStyle>
        
                <Columns>
                    
                     <asp:BoundColumn DataField="id" HeaderText="id" Visible="False"></asp:BoundColumn>
                     <asp:BoundColumn DataField="s_f" HeaderText="" Visible="False"></asp:BoundColumn>
                     <asp:BoundColumn DataField="EMP_NM" HeaderText="NAME" ReadOnly="True" HeaderStyle-Width="40" ItemStyle-Wrap="false" ItemStyle-Width="60">
                     
                         <HeaderStyle Width="10px"></HeaderStyle>

                     <ItemStyle Width="10px"></ItemStyle>
                    </asp:BoundColumn> 
                     
             <asp:BoundColumn DataField="gross_sale" HeaderText="Gross Sales"  ItemStyle-Width="120px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle Width="140px"></ItemStyle>
                    </asp:BoundColumn>
                     
             <asp:BoundColumn DataField="void_sale" HeaderText="Credits/Void Sales"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn> 
             <asp:BoundColumn DataField="net_sale" HeaderText="Net Sales"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>


<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn> 
                     <asp:BoundColumn DataField="net_mgn" HeaderText="Net Mgn"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                     
                    <asp:BoundColumn DataField="furn_mix" HeaderText="Furn Mix"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:N2}"    >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="furn_net_mgn" HeaderText="Furn Mgn"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="matt_mix" HeaderText="Matt Mix"  ItemStyle-Width="101px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="matt_net_mgn" HeaderText="Matt Mgn"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"   >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_mix" HeaderText="Appl Mix" ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_net_mgn" HeaderText="Appl Mgn"  ItemStyle-Width="101px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_mix" HeaderText="Elec Mix"  ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_net_mgn" HeaderText="Elec Mgn" ItemStyle-Width="101px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="101px"></ItemStyle>
                    </asp:BoundColumn>
             <asp:BoundColumn DataField="id" HeaderText="ID" Visible="False" ItemStyle-Width="120px" >
<ItemStyle Width="120px"></ItemStyle>
                    </asp:BoundColumn>
             <asp:BoundColumn DataField="out_net_sale" HeaderText="Net Sales" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
                    </asp:BoundColumn> 
                     <asp:BoundColumn DataField="out_net_mgn" HeaderText="Net Mgn" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
                    </asp:BoundColumn>
               <asp:BoundColumn DataField="out_net_qty" HeaderText="Units" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
                    </asp:BoundColumn>
                
        </Columns>
 
        <PagerStyle NextPageText=" " PrevPageText=" "  />
       
    </asp:DataGrid>
                </td>
       </tr>
        </table>
    <%--  --%>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
  <%------------------------------------------- sabrina R3442 ------------------------------ --%>

    <div runat="server" id="div_version">
          
        <asp:label ID="lbl_versionInfo" runat="server" >
       &nbsp;&nbsp;&nbsp; </asp:label>  
    </div>
</asp:Content>   
