Imports System.Data
Imports System.Data.OracleClient
Imports DevExpress.XtraCharts

Partial Class salestrends
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If Not IsPostBack Then
            store_cd_populate()
            'kiosk_cd_populate()
            If Not Is_Manager() Then
                cbo_store.Enabled = False
                txt_start_dt.Enabled = False
            End If
            txt_start_dt.SelectedDate = Today.Date
            populate_relationship_trends()
        End If

    End Sub

    Public Function Is_Manager() As Boolean

        Is_Manager = False
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                             ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.EMP_CD='" & UCase(Session("EMP_CD")) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='SLM'"
        SQL = UCase(SQL)

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Is_Manager = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Sub populate_relationship_trends()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter
        Dim oAdp2 As OracleDataAdapter
        Dim start_timestamp As Date
        Dim timestamp As Date
        start_timestamp = txt_start_dt.SelectedDate
        timestamp = txt_start_dt.SelectedDate
        start_timestamp = timestamp.AddDays(-7)

        ds = New DataSet

        conn.Open()

        sql = "SELECT ORIGINATION, TO_CHAR(TIMESTAMP, 'MM/DD/RRRR') AS KIOSK_DT, COUNT(ORIGINATION) AS ORIG_COUNT "
        sql = sql & "FROM KIOSK_AUDIT "
        sql = sql & "WHERE TIMESTAMP BETWEEN TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') AND TO_DATE('" & timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "AND STORE_CD='" & cbo_store.SelectedValue & "' "
        sql = sql & "AND RESPONSE_MSG<>'NO RESPONSE FROM HOST' "
        sql = sql & "GROUP BY ORIGINATION, TO_CHAR(TIMESTAMP, 'MM/DD/RRRR')"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        Dim dt As DataTable = ds.Tables(0)

        Dim view As New DataView(dt)
        view.Sort = "KIOSK_DT ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl1.DataSource = view

        ' Specify data members to bind the series.
        WebChartControl1.SeriesDataMember = "ORIGINATION"
        WebChartControl1.SeriesTemplate.ArgumentDataMember = "KIOSK_DT"
        WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))

        WebChartControl1.SeriesTemplate.View = New LineSeriesView

        WebChartControl1.DataBind()

        Dim ds2 As New DataSet
        sql = "SELECT RESPONSE_MSG, TO_CHAR(TIMESTAMP, 'MM/DD/YYYY') AS KIOSK_DT, COUNT(RESPONSE_MSG) AS ORIG_COUNT "
        sql = sql & "FROM KIOSK_AUDIT "
        sql = sql & "WHERE TIMESTAMP BETWEEN TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') AND TO_DATE('" & timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "AND STORE_CD='" & cbo_store.SelectedValue & "' "
        sql = sql & "AND RESPONSE_MSG<>'NO RESPONSE FROM HOST' "
        sql = sql & "AND ORIGINATION='APPLY' "
        sql = sql & "GROUP BY RESPONSE_MSG, TO_CHAR(TIMESTAMP, 'MM/DD/YYYY')"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp2 = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp2.Fill(ds2)
        Dim dt2 As DataTable = ds2.Tables(0)

        Dim view2 As New DataView(dt2)
        view2.Sort = "KIOSK_DT ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl2.DataSource = view2

        ' Specify data members to bind the series.
        WebChartControl2.SeriesDataMember = "RESPONSE_MSG"
        WebChartControl2.SeriesTemplate.ArgumentDataMember = "KIOSK_DT"
        WebChartControl2.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))

        WebChartControl2.SeriesTemplate.View = New LineSeriesView

        WebChartControl2.DataBind()

        sql = "SELECT RESPONSE_MSG, TO_CHAR(TIMESTAMP, 'MM/DD/YYYY') AS KIOSK_DT, COUNT(RESPONSE_MSG) AS ORIG_COUNT, SUM(CREDIT_LIMIT) AS CREDIT_LIMIT "
        sql = sql & "FROM KIOSK_AUDIT "
        sql = sql & "WHERE TIMESTAMP BETWEEN TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') AND TO_DATE('" & timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "AND STORE_CD='" & cbo_store.SelectedValue & "' "
        sql = sql & "AND RESPONSE_MSG<>'NO RESPONSE FROM HOST' "
        sql = sql & "AND ORIGINATION='APPLY' "
        sql = sql & "GROUP BY RESPONSE_MSG, TO_CHAR(TIMESTAMP, 'MM/DD/YYYY') "
        sql = sql & "ORDER BY TO_CHAR(TIMESTAMP, 'MM/DD/YYYY')"

        Dim MyDataReader As OracleDataReader
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim Curr_dt As String = ""
        Dim approvals As Integer = 0
        Dim declines As Integer = 0
        Dim html As String = ""
        Dim credit_limit As Integer = 0
        lbl_kiosk_details.Text = ""

        html = "<br /><table width=""100%"">"
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                If Curr_dt & "" = "" Then
                    Curr_dt = MyDataReader.Item("KIOSK_DT").ToString
                End If
                If Curr_dt <> MyDataReader.Item("KIOSK_DT").ToString Then
                    html = html & "<tr>"
                    html = html & "<td>Date: " & Curr_dt & "</td>"
                    html = html & "<td>Approvals: " & approvals & "</td>"
                    html = html & "<td>Declines: " & declines & "</td>"
                    html = html & "<td>Average Credit Limit: " & FormatCurrency(credit_limit / approvals, 0) & "</td>"
                    html = html & "<td>Approval %: " & FormatPercent(approvals / (approvals + declines), 2) & "</td>"
                    html = html & "</tr>"
                    approvals = 0
                    declines = 0
                    credit_limit = 0
                    Curr_dt = MyDataReader.Item("KIOSK_DT").ToString
                End If
                If MyDataReader.Item("RESPONSE_MSG").ToString = "APPROVAL" Then
                    approvals = MyDataReader.Item("ORIG_COUNT").ToString
                    credit_limit = MyDataReader.Item("CREDIT_LIMIT").ToString
                Else
                    declines = MyDataReader.Item("ORIG_COUNT").ToString
                End If
            Loop
            html = html & "<tr>"
            html = html & "<td>Date: " & Curr_dt & "</td>"
            html = html & "<td>Approvals: " & approvals & "</td>"
            html = html & "<td>Declines: " & declines & "</td>"
            html = html & "<td>Average Credit Limit: " & FormatCurrency(credit_limit / approvals, 0) & "</td>"
            html = html & "<td>Approval %: " & FormatPercent(approvals / (approvals + declines), 2) & "</td>"
            html = html & "</tr>"
            html = html & "</table><br /><br />"
            lbl_kiosk_details.Text = html
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub kiosk_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If

        SQL = "SELECT ORIGINATION FROM KIOSK_AUDIT GROUP BY ORIGINATION ORDER BY ORIGINATION"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store
                .DataSource = ds
                .DataValueField = "ORIGINATION"
                .DataTextField = "ORIGINATION"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Function GetFirstDateOfWeek(ByVal pDate As DateTime, ByVal pFirstDayOfWeek As System.Web.UI.WebControls.FirstDayOfWeek) As DateTime

        Dim vFirstDayOfWeek As Short = Convert.ToInt16(pFirstDayOfWeek)
        Dim vDayOfWeek As Short = Convert.ToInt16(pDate.DayOfWeek)  ' 0 = sunday
        If (vDayOfWeek <> vFirstDayOfWeek) Then

            If (vFirstDayOfWeek > vDayOfWeek) Then
                pDate = pDate.AddDays(vFirstDayOfWeek - vDayOfWeek - 7)
            Else
                pDate = pDate.AddDays(vFirstDayOfWeek - vDayOfWeek)
            End If
        End If
        Return pDate

    End Function

    Protected Sub cbo_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store.SelectedIndexChanged

        populate_relationship_trends()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub txt_start_dt_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_start_dt.SelectionChanged

        populate_relationship_trends()

    End Sub
End Class
