<%@ Page Language="VB" AutoEventWireup="false" CodeFile="order_detail.aspx.vb" Inherits="order_detail"
    MasterPageFile="~/Regular.master" validateRequest="false"%>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<%@ Register Src="~/Usercontrols/SummaryPriceChangeApprovalWithComment.ascx" TagPrefix="ucPC" TagName="SummaryPriceChangeApproval" %>
<%@ Register Src="~/Usercontrols/MultiWarranties.ascx" TagPrefix="ucWar" TagName="Warranties" %>
<%@ Register Src="~/Usercontrols/RelatedSKU.ascx" TagPrefix="ucRelatedSku" TagName="RelatedSKUs" %>

<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup2() {
            ASPxPopupControl2.Hide();
        }
        function SelectAndClosePopup3() {
            ASPxPopupControl3.Hide();
        }
        function SelectAndClosePopup5() {
            ASPxPopupControl5.Hide();
        }
        function SelectAndClosePopup10() {
            ASPxPopupControl10.Hide();
        }

        function pageLoad(sender, args) {
            var rowscount = $("#<%=GridView1.ClientID %> tr").length;
                $('input[type=submit], a, button').click(function (event) {
                    if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {
                        if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {
                            $('#hidDestUrl').val($.trim($(this).attr('href')));
                            if ('<% =AppConstants.ManagerOverride.BY_ORDER %>' == '<% =SalesUtils.GetPriceMgrOverrideSetting() %>') {
                                __doPostBack('SummaryPriceChangeApproval', "");

                            }

                            event.preventDefault();
                        }
                    }
            });
            }
        function PriceUpdatedRow(rowNo) {
            if (('<% =AppConstants.ManagerOverride.NONE %>' != '<% =SalesUtils.GetPriceMgrOverrideSetting() %>')) {
                $('#hidUpdLnNos').val($('#hidUpdLnNos').val() + ';' + rowNo);

            }
        }
    </script>

       <%-- MM-19550 --%>
<script type="text/javascript" language="javascript">
    window.onload = function () {
        var GetDocumentScrollTop = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageYOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollTop || document.body.scrollTop;
                return document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        };
        var _aspxGetDocumentScrollTop = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageYOffset;
                else
                    return document.documentElement.scrollTop || document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        }
        if (window._aspxGetDocumentScrollTop) {
            window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
        } else {
            window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
        }
        /* Begin -> Correct ScrollLeft  */
        var GetDocumentScrollLeft = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageXOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
                return document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        };
        var _aspxGetDocumentScrollLeft = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageXOffset;
                else
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        }
        if (window._aspxGetDocumentScrollLeft) {
            window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
        } else {
            window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
        }
        /* End -> Correct ScrollLeft  */
    };
</script>

    <asp:HiddenField ID="hidDestUrl" runat="server" ClientIDMode="Static" />
    <div>
        <table cellspacing="0" cellpadding="0" style="width: 100%">
            <tr>
                <asp:Label ID="lblErrorMsg" runat="server" Visible="false"  ></asp:Label>
            </tr>
            <tr>
                <td valign="top" align="right">
                    <asp:Panel ID="Panel1" runat="server" Width="500px" DefaultButton="cmd_add" Height="30px">
                        <table>
                            <tr>
                                <td valign="top"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label550 %>" />&nbsp;&nbsp;
                                </td>
                                <td valign="top">
                                    <dx:ASPxTextBox ID="txt_SKU" runat="server" Width="170px">
                                    </dx:ASPxTextBox>
                                    &nbsp;
                                      <asp:Label ID="lucy_lbl" runat="server" Text="<%$ Resources:LibResources, Label402 %>" ForeColor="#CC00FF"></asp:Label>
                                </td>
                                <td valign="top">
                                    <dx:ASPxButton ID="cmd_add" runat="server" Text="<%$ Resources:LibResources, Label13 %>">
                                    </dx:ASPxButton>
                                </td>
                                <td valign="top">
                                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                                        Width="19px" ToolTip="<%$ Resources:LibResources, Label291 %>" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td valign="top">
                                    <dx:ASPxButton ID="btn_sku_gen" runat="server" Text="Generate SKU" Visible="False"
                                        OnClick="btn_sku_gen_Click">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                               </td>
            </tr>
            <%--Daniela moved message --%>
            <tr>
            <td valign="top">                 
             <br />
                <div style="text-align: right; width:855px; margin-right:150px "/> 
                    <dx:ASPxLabel ID="lbl_warnings" ForeColor="#FF0066" runat="server" Text="">
                    </dx:ASPxLabel>
             <br />    
            </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chk_slsp_view_all" runat="server" AutoPostBack="true" Text="<%$ Resources:LibResources, Label544 %>"
                                    TextAlign="Left" OnCheckedChanged="chk_slsp_view_all_CheckedChanged" />
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chk_reserve_all" runat="server" AutoPostBack="true" Text="<%$ Resources:LibResources, Label473 %>"
                                    TextAlign="Left" OnCheckedChanged="chk_reserve_all_CheckedChanged" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 1px; left: 1px; margin-top: 0px;" runat="server"
        OnDeleteCommand="Gridview1_DeleteCommand" OnEditCommand="Gridview1_ItemCommand"
        AutoGenerateColumns="False" CellPadding="2" DataKeyField="row_id"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="156px" Width="100%">
        <alternatingitemstyle backcolor="Beige" font-bold="False" font-italic="False" font-overline="False"
            font-strikeout="False" font-underline="False" />
        <columns>
            <asp:BoundColumn DataField="itm_cd" ReadOnly="True" HeaderText="SKU" Visible="False">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="ve_cd" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label640 %>" Visible="False">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label653 %>">
                <ItemTemplate>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <%--<td valign="top" align="left" style="width:150px;">
                                    <asp:Image ID="img_picture" runat="server" Height="100px" ImageUrl="~/images/image_not_found.jpg"
                                        Width="133px" />
                                </td>--%>
								<td valign="top" align="left" style="width:150px;">
                                    <%--Daniela use temp table to get URL--%>
                                   <asp:Image ID="img_picture" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.itm_url") %>'
                                        />
                                </td>                                
								<%--ToolTip ='<%# If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", ","+Environment.NewLine) = "-","",DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", ","+Environment.NewLine)) %>'--%>
                                     <br />
                                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label560 %>" />:<br />
                                    <div  runat="server" id ="divSortCodes" style="overflow:auto; height:50px; text-align:left; width:95%;padding-top:5px;">
                                        <label><%# Replace(If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", "," + Environment.NewLine) = "-", "", DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper()),",",", ")%></label>
                                    </div>
                                </td>
                                <td valign="top" align="left">
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vsn") %>'></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'></asp:Label>
                                    <br />
                                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label550 %>" />
                                    <asp:Label ID="lbl_SKU" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itm_cd") %>'></asp:Label>
                                    &nbsp; &nbsp; &nbsp;&nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label640 %>" />:
                                    <asp:Label ID="lbl_ve_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ve_cd") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lbl_sernum" runat="server" Text='Serial Number:'></asp:Label>
                                    <asp:Label ID="lbl_sernumval" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.serial_num") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lbl_crpt_num" runat="server" Text='Combination Number:'></asp:Label>
                                    <asp:Label ID="lbl_crpt_num_val" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID_NUM") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lbl_outletid" runat="server" Text='Outlet ID:'></asp:Label>
                                   
                                    <asp:Label ID="lbl_outletval" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.out_id") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lbl_warr" runat="server" Text='Warr SKU:'></asp:Label>
                                    <asp:Label ID="lbl_warr_sku" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WARR_ITM_LINK") %>'></asp:Label>
                                    <br />
                                    <asp:ImageButton ID="img_po" runat="server" ImageUrl="images/icons/symbol-delete.png"
                                        Height="20" Width="20" CommandName="remove_po" Visible="False" ToolTip="Remove PO" AlternateText="Remove PO" /><asp:Label ID="lbl_PO"
                                            runat="server" Text=''></asp:Label>
                                    <br />
                                    <asp:ImageButton ID="img_gm" runat="server" ImageUrl="images/icons/money.gif" Height="20"
                                        Width="20" CommandName="find_gm" Visible="True" ToolTip="Show Margin" AlternateText="Show Margin" />
                                    <br />
                                    <%--Daniela Nov 11 hide Leave in Carton--%>
                                    <asp:Label ID="lbl_carton" runat="server" Text="Leave In Carton:" Visible="false"></asp:Label>
                                    <asp:CheckBox ID="chk_carton" runat="server" AutoPostBack="true" OnCheckedChanged="Carton_Update" visible="false"/>
                                    <div id="slsp_section" runat="server" visible="False">
                                        <asp:Label ID="lblSlps1" runat="server" Text='Slsp1:'></asp:Label>
                                        <asp:DropDownList ID="cbo_slsp1" OnSelectedIndexChanged="DoSalesPersonUpdate" AutoPostBack="true" runat="server"
                                            CssClass="style5" Width="180px" Enabled="false">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="slsp1_pct" runat="server" AutoPostBack="true" CssClass="style5"
                                            OnTextChanged="txt_pct_1_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.slsp1") %>'
                                            Width="25" Enabled="false">
                                        </asp:TextBox>%
                                        <br /><asp:Label ID="lblSlps2" runat="server" Text='Slsp2:'></asp:Label>
                                        <asp:DropDownList ID="cbo_slsp2" OnSelectedIndexChanged="DoSalesPersonUpdate" AutoPostBack="true" runat="server"
                                            CssClass="style5" Width="180px" Enabled="false">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="slsp2_pct" runat="server" AutoPostBack="true" CssClass="style5"
                                            OnTextChanged="txt_pct_2_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.slsp2") %>' Width="25" Enabled="false">
                                         </asp:TextBox>%
                                    </div>
                                    <br />
                                    <asp:Label ID="lblAvailDt" runat="server" Text='' CssClass="style5"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_line_desc" runat="server" AutoPostBack="true" CssClass="style5"
                                        Height="50px" OnTextChanged="Line_Update" TextMode="MultiLine" Visible="False"
                                        Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                 <%--Daniela red and smaller font removed <asp:Label ID="lbl_itm_warning" runat="server" Font-Size="Smaller" ForeColor="#C0C000"></asp:Label>--%>
                                    <asp:Label ID="lbl_itm_warning" runat="server" ForeColor="#FF0066"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="images/icons/DocumentsBlack.PNG"
                        Height="20" Width="20" CommandName="inventory_lookup" ToolTip="SKU Details" AlternateText="SKU Details" />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="Image2" runat="server" ImageUrl="images/icons/compare_inventory.gif"
                                    Height="20" Width="20" CommandName="find_inventory" ToolTip="Inventory Availability" AlternateText="Inventory Availability" />
                            </td>
                        </tr>
                        <tr></tr>
                        <tr><td><asp:Label ID="lbl_invcount" runat="server" Visible="false" Text=""></asp:Label></td></tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label572 %>">
                <ItemTemplate>
                    <asp:TextBox ID="store_cd" runat="server" AutoPostBack="true" CssClass="style5" OnTextChanged="Store_Update"
                        Text='<%# DataBinder.Eval(Container, "DataItem.store_cd") %>' Width="20"></asp:TextBox>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label271 %>">
                <ItemTemplate>
                    <asp:TextBox ID="loc_cd" runat="server" AutoPostBack="true" CssClass="style5" OnTextChanged="Loc_Update"
                        Text='<%# DataBinder.Eval(Container, "DataItem.loc_cd") %>' Width="40"></asp:TextBox>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    Fab
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="SelectThis" runat="server" AutoPostBack="true" OnCheckedChanged="Treated__CheckedChanged"
                        CssClass="style5" />
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" ForeColor="Black" HorizontalAlign="Center" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label655 %>">
                <%--<HeaderTemplate>
                    <b>Warr</b>
                </HeaderTemplate>--%>
                <ItemTemplate>
                    <asp:CheckBox ID="SelectWarr" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="CheckWarrClicked"
                        CssClass="style5" />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:CheckBox ID="chk_warr" runat="server" Enabled="false" CssClass="style5" />
                </FooterTemplate>
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" ForeColor="Black" HorizontalAlign="Center" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    T
                </HeaderTemplate>
                <ItemTemplate>
                    <%--Daniela disable TakeWith--%>
                    <asp:CheckBox ID="TakeWith" runat="server" AutoPostBack="true" OnCheckedChanged="Take_With_CheckedChanged"
                        CssClass="style5" Enabled="false"/>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" ForeColor="Black" HorizontalAlign="Center" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="row_id" HeaderText="Row_ID" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label427 %>">
                <ItemTemplate>
                    <asp:TextBox ID="ret_prc" runat="server" AutoPostBack="true" CssClass="style5" OnTextChanged="Price_Update"
                        Text='<%# Replace(FormatNumber(DataBinder.Eval(Container, "DataItem.ret_prc"),2),",","") %>'
                        onchange='<%#"PriceUpdatedRow("+DataBinder.Eval(Container, "DataItem.row_id").ToString()+");" %>' Width="45"></asp:TextBox>
                    <asp:HiddenField id="hidMaxDisc" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.MAX_DISC_PCNT")%>'></asp:HiddenField>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
              <asp:BoundColumn DataField="RelationshipPrice" ReadOnly="True" HeaderText="Reln Price" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>  
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label447 %>">
                <ItemTemplate>
                    <asp:TextBox ID="QTY_TXTBOX" runat="server" AutoPostBack="true" CssClass="style5" OnTextChanged="Qty_Update"
                        Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="23"></asp:TextBox>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:ButtonColumn>
            <asp:BoundColumn DataField="ITM_TP_CD" HeaderText="itm_tp_cd" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TREATABLE" HeaderText="treatable" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TREATED" HeaderText="TREATED" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TAKE_WITH" HeaderText="TAKE_WITH" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="NEW_SPECIAL_ORDER" HeaderText="SPECIAL ORDER" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="PACKAGE_PARENT" HeaderText="PACKAGE PARENT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TAXABLE_AMT" HeaderText="TAXABLE AMOUNT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="LEAVE_CARTON" HeaderText="LEAVE_CARTON" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SLSP1" HeaderText="SLSP1" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SLSP2" HeaderText="SLSP2" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SLSP1_PCT" HeaderText="SLSP1_PCT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SLSP2_PCT" HeaderText="SLSP2_PCT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="BULK_TP_ITM" HeaderText="BULK_TP_ITM" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SERIAL_TP" HeaderText="SERIAL_TP" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TAX_AMT" HeaderText="TAX_AMT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="WARR_ITM_LINK" HeaderText="WARR_ITM_LINK" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="WARR_ROW_LINK" HeaderText="WARR_ROW_LINK" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="WARRANTABLE" HeaderText="Warrantable" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Inventory" HeaderText="Inventory" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Qty" HeaderText="Qty" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AVAIL_DT" HeaderText="AVAIL_DT" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="RES_ID" HeaderText="RES_ID" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>    
        </columns>
        <headerstyle font-bold="True" font-italic="False" font-overline="False" font-strikeout="False"
            font-underline="False" forecolor="Black" />
    </asp:DataGrid>       
    
<br />
    <dx:ASPxLabel ID="lbl_questions" runat="server" Text="" Width="100%" EncodeHtml="False">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="new_special" runat="server" Text="" Visible="False">
    </dx:ASPxLabel>
    &nbsp;
&nbsp;<table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_detail" runat="server" Text="Add Detailed Descriptions" Visible="False"
                    OnClick="btn_detail_Click">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_total_margins" runat="server" Text="<%$ Resources:LibResources, Label545 %>" Visible="False"
                    OnClick="btn_total_margins_Click">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
   <%--Daniela move up <dx:ASPxLabel ID="lbl_warnings" ForeColor="#FF0066" runat="server" Text="" Width="100%">
    </dx:ASPxLabel>--%>
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="Related SKUS" Height="340px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="680px" ClientInstanceName="ASPxPopupControl1">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
        HeaderText="Approval Password" Height="179px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="370px"
        ShowCloseButton="False" ClientInstanceName="ASPxPopupControl2">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl7" runat="server" CloseAction="None" HeaderText="Select Serial Number"
        Height="179px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="370px" ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <dx:ASPxLabel ID="lbl_ser_store_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_loc_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_row_id" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exit Without Selecting Serial #">
                </dx:ASPxButton>
                <dx:ASPxLabel ID="lbl_ser_itm_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="346px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="SER_NUM" VisibleIndex="0" Visible="False" Caption="Serial Number">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="" VisibleIndex="0">
                            <DataItemTemplate>
                                &nbsp;<dx:ASPxHyperLink ID="hpl_serial" runat="server" Text=" Select" Visible="True">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager Visible="False" Mode="ShowAllRecords" RenderMode="Lightweight">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
                <dx:ASPxLabel ID="lbl_ser_err" runat="server" Text="" Visible="True" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl12" runat="server" CloseAction="None"
        HeaderText="Select Outlet ID" Height="179px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="370px"
        ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl12" runat="server">
                <dx:ASPxLabel ID="lbl_out_store_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_loc_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_row_id" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_itm_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" Width="346px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="ID_CD" VisibleIndex="0" Visible="False" Caption="Outlet ID">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Outlet ID" VisibleIndex="0">
                            <DataItemTemplate>
                                &nbsp;<dx:ASPxHyperLink ID="hpl_out" runat="server" Text=" Select" Visible="True">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OUT_CD" VisibleIndex="0" Visible="True" Caption="Outlet Code">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OUT_COMM_CD" VisibleIndex="0" Visible="False" Caption="Outlet ID">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SPIFF" VisibleIndex="0" Visible="False" Caption="Outlet ID">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager Visible="False" Mode="ShowAllRecords" RenderMode="Lightweight">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
                <dx:ASPxLabel ID="lbl_out_err" runat="server" Text="" Visible="True" Width="100%"
                    ForeColor="Red">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl6" runat="server" CloseAction="CloseButton"
        HeaderText="Gross Margin" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Label ID="lbl_gm" runat="server"></asp:Label>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl5" runat="server" CloseAction="CloseButton"
        HeaderText="Detailed Line Descriptions" Height="403px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="593px"
        AllowDragging="True" ShowCloseButton="False" ClientInstanceName="ASPxPopupControl5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btn_close" runat="server" Text="Close Window" CssClass="style5" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center">
                            <br />
                            <asp:DataGrid ID="MyDataGrid" runat="server" Width="100%" BorderColor="Black" CellPadding="3"
                                AutoGenerateColumns="False" DataKeyField="LINE" Height="1px" PageSize="4" CssClass="style5">
                                <Columns>
                                    <asp:BoundColumn HeaderText="Line" DataField="LINE" />
                                    <asp:BoundColumn HeaderText="Item" DataField="VSN" />
                                    <asp:BoundColumn HeaderText="Relationship" DataField="REL_NO" Visible="False" />
                                    <asp:TemplateColumn HeaderText="Detailed Description">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_desc" OnTextChanged="Line_Update" Text='<%# DataBinder.Eval(Container, "DataItem.DESCRIPTION") %>'
                                                Columns="4" runat="server" Width="363px" CssClass="style5" AutoPostBack="True"
                                                Height="59px" TextMode="MultiLine"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                <EditItemStyle CssClass="style5" />
                                <ItemStyle CssClass="style5" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" ClientInstanceName="ASPxPopupControl3" CloseAction="CloseButton"
        HeaderText="Inventory Lookup" Height="500px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="679px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" CloseAction="CloseButton"
        HeaderText="SKU Details" Height="680px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="680px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl8" runat="server" CloseAction="CloseButton"
        HeaderText="SKU Generation" Height="680px" Modal="False" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="776px" AllowDragging="True" AllowResize="True"
        ScrollBars="Auto">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl10" runat="server" ClientInstanceName="ASPxPopupControl10" CloseAction="CloseButton"
        HeaderText="Custom Order Entry" Height="600px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="680px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl11" runat="server" CloseAction="CloseButton"
        HeaderText="Take With Sale" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <asp:Label ID="Label3" runat="server" Text="Your order has been converted to a Take With sale"></asp:Label>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Continue" OnClick="ConvertToCashCarrySale" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupTax" runat="server" HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None"
        CloseAction="CloseButton" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ShowPageScrollbarWhenModal="true" Height="460px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <div style="width: 100%; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
        HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
    <ucPC:SummaryPriceChangeApproval runat="server" ID="ucSummaryPrcChange" ClientIDMode="Static" />
    <asp:HiddenField ID="hidUpdLnNos" runat="server" ClientIDMode="Static" />
    <ucWar:Warranties runat="server" ID="ucWarranties" ClientIDMode="Static" />
    <ucRelatedSku:RelatedSKUs runat="server" ID="ucRelatedSku" ClientIDMode="Static" />
    
</asp:content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_discounts" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hidItemCd" runat="server" />
    <asp:HiddenField ID="hidWarrantyCheck" runat="server" Value="0" />
    <asp:HiddenField ID="hidWarrantyClicked" runat="server" Value="0" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TaxIconHolder" runat="Server">
    <dx:ASPxLabel runat="server" Text="(">
    </dx:ASPxLabel>
    <asp:LinkButton ID="lnkModifyTax" runat="server" ToolTip="Click to modify the tax code." CssClass="dxeBase_Office2010Blue">
        <%= If(IsNothing(Session("TAX_CD")), "None", If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString))%>
    </asp:LinkButton>
    <dx:ASPxLabel runat="server" Text=")">
    </dx:ASPxLabel>
</asp:Content>
