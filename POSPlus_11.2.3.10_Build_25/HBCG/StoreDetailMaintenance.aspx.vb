﻿Imports HBCG_Utils
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Text

Partial Class StoreDetailMaintenance
    Inherits POSBasePage
    '    Inherits System.Web.UI.Page

    Dim footer_message As String
    Public Const gs_version As String = "Version 1.0"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbl_versionInfo.Text = gs_version
        If Not IsPostBack Then
            PopulateStoreCodes()
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : DANIELA
    ''' Dreated Date   : Oct 2017
    ''' Description    : Search button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click

        lbl_message.Text = ""
        footer_message = ""

        GV_store_detail.DataBind()
        GV_store_detail.CancelEdit()
    End Sub

    ''' <summary>
    ''' Created by     : DANIELA
    ''' Dreated Date   : Oct 2017
    ''' Description    : Search button click event 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GV_Store_Detail_DataBind()
        Dim conn As New OracleConnection
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("SELECT rowid, ")
        sqlString.Append("STORE_CD,STORE_NAME,ADDRESS_1,ADDRESS_2,CITY,PROV,PC,EMAIL,PHONE,CO_GRP,CO_NAME, ")
        sqlString.Append("CO_CD,CO_TYPE,LOC_TYPE,BANNER,REGION,METRO_STORE,METRO_AREA, ")
        sqlString.Append("METRO_AREA_DESC,PHYSICAL_LOC,OPEN_DATE,CLOSE_DATE,SAME_STORE, ")
        sqlString.Append("STORE_STATUS,FIN_CO_CD,FIN_BANNER_CD,FIN_REGION_CD,FIN_LOCATION_CD, ")
        sqlString.Append("CREDIT_STORE,PARENT_CO,SUPPLY_POINT,LINEHAUL,FLEET_STORE_NAME,FLEET_STORE_MANAGER, ")
        sqlString.Append("FLEET_STORE_EMAIL,FLEET_STORE_REGION,AM_NAME,AM_SORT_CD,AM_EMAIL,AM_GRP_EMAIL, ")
        sqlString.Append("STORE_LEVEL,STORE_SQFEET,STORE_ATTR1,STORE_ATTR2,STORE_ATTR3,STORE_ATTR4, ")
        sqlString.Append("STORE_ATTR5,STORE_ATTR6,STORE_ATTR7,STORE_ATTR8,STORE_ATTR9,STORE_ATTR10,TRANSIT_NO,ACCT_NO,MARKET,MERCHANT_NO ")
        sqlString.Append("FROM store_detail ")

        If String.IsNullOrEmpty(txt_store_cd.Text) = False Then
            sqlString.Append("WHERE store_cd LIKE :p_store_cd ")
        End If

        sqlString.Append(" Order by STORE_CD ")

        Dim ds As New DataSet
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()

        If String.IsNullOrEmpty(txt_store_cd.Text) = False Then
            objcmd.Parameters.Add(":p_store_cd", OracleType.VarChar)
            objcmd.Parameters(":p_store_cd").Value = txt_store_cd.Text.ToUpper
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)

        Try
            conn.Open()
            objAdaptor.Fill(ds)
            GV_store_detail.DataSource = ds

            conn.Close()
            GV_store_detail.Enabled = True
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    Protected Sub GV_Footer_label_init(sender As Object, e As EventArgs)
        Dim gv_footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        gv_footer_label.Text = footer_message
    End Sub


    Protected Sub GV_row_update(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        'Dim objResult As Object

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If String.IsNullOrEmpty(e.NewValues("STORE_CD")) Then
            Throw New Exception("Store code must be entered")
        End If

        'If e.OldValues("STORE_CD").ToString().ToUpper() <> e.NewValues("STORE_CD").ToString().ToUpper() Then
        'check_duplicate(e.NewValues("STORE_CD").ToString().ToUpper())
        'End If

        conn.Open()

        sqlString = New StringBuilder("UPDATE store_detail SET ")
        sqlString.Append("STORE_CD = :p_store_cd, ")
        sqlString.Append("STORE_NAME = :p_store_name, ")
        sqlString.Append("ADDRESS_1 = :p_address_1, ")
        sqlString.Append("ADDRESS_2 = :p_address_2, ")
        sqlString.Append("CITY = :p_city, ")
        sqlString.Append("PROV = :p_prov, ")
        sqlString.Append("PC = :p_pc, ")
        sqlString.Append("EMAIL = :p_email, ")
        sqlString.Append("PHONE = :p_phone, ")
        sqlString.Append("CO_GRP = :p_co_grp, ")
        sqlString.Append("CO_NAME = :p_co_name, ")
        sqlString.Append("CO_CD = :p_co_cd, ")
        sqlString.Append("CO_TYPE = :p_co_type, ")
        sqlString.Append("LOC_TYPE = :p_loc_type, ")
        sqlString.Append("BANNER = :p_banner, ")
        sqlString.Append("REGION = :p_region, ")
        sqlString.Append("METRO_STORE = :p_metro_store, ")
        sqlString.Append("METRO_AREA = :p_metro_area, ")
        sqlString.Append("METRO_AREA_DESC = :p_metro_area_desc, ")
        sqlString.Append("PHYSICAL_LOC = :p_physical_loc, ")
        sqlString.Append("OPEN_DATE = :p_open_date, ")
        sqlString.Append("CLOSE_DATE = :p_close_date, ")
        sqlString.Append("SAME_STORE = :p_same_store, ")
        sqlString.Append("STORE_STATUS = :p_store_status, ")
        sqlString.Append("FIN_CO_CD = :p_fin_co_cd, ")
        sqlString.Append("FIN_BANNER_CD = :p_fin_banner_cd, ")
        sqlString.Append("FIN_REGION_CD = :p_fin_region_cd, ")
        sqlString.Append("FIN_LOCATION_CD = :p_fin_location_cd, ")
        sqlString.Append("CREDIT_STORE = :p_credit_store, ")
        sqlString.Append("PARENT_CO = :p_parent_co, ")
        sqlString.Append("SUPPLY_POINT = :p_supply_point, ")
        sqlString.Append("LINEHAUL = :p_linehaul, ")
        sqlString.Append("FLEET_STORE_NAME = :p_fleet_store_name, ")
        sqlString.Append("FLEET_STORE_MANAGER = :p_fleet_store_manager, ")
        sqlString.Append("FLEET_STORE_EMAIL = :p_fleet_store_email, ")
        sqlString.Append("FLEET_STORE_REGION = :p_fleet_store_region, ")
        sqlString.Append("AM_NAME = :p_am_name, ")
        sqlString.Append("AM_SORT_CD = :p_am_sort_cd, ")
        sqlString.Append("AM_EMAIL = :p_am_email, ")
        sqlString.Append("AM_GRP_EMAIL = :p_am_grp_email, ")
        sqlString.Append("STORE_LEVEL = :p_store_level, ")
        sqlString.Append("STORE_SQFEET = :p_store_sqfeet, ")
        sqlString.Append("STORE_ATTR1 = :p_store_attr1, ")
        sqlString.Append("STORE_ATTR2 = :p_store_attr2 ,")
        sqlString.Append("STORE_ATTR3 = :p_store_attr3, ")
        sqlString.Append("STORE_ATTR4 = :p_store_attr4, ")
        sqlString.Append("STORE_ATTR5 = :p_store_attr5, ")
        sqlString.Append("STORE_ATTR6 = :p_store_attr6, ")
        sqlString.Append("STORE_ATTR7 = :p_store_attr7, ")
        sqlString.Append("STORE_ATTR8 = :p_store_attr8, ")
        sqlString.Append("STORE_ATTR9 = :p_store_attr9, ")
        sqlString.Append("STORE_ATTR10 = :p_store_attr10, ")
        sqlString.Append("TRANSIT_NO = :p_transit_no, ")
        sqlString.Append("ACCT_NO = :p_acct_no, ")
        sqlString.Append("MARKET = :p_market, ")
        sqlString.Append("MERCHANT_NO = :p_merchant_no ")
        sqlString.Append("WHERE rowid = :p_rowid ")


        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_store_cd", OracleType.VarChar)
        objCmd.Parameters(":p_store_cd").Value = e.NewValues("STORE_CD").ToString().ToUpper()

        objCmd.Parameters.Add(":p_store_name", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_NAME")) Then
            objCmd.Parameters(":p_store_name").Value = ""
        Else
            objCmd.Parameters(":p_store_name").Value = e.NewValues("STORE_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_address_1", OracleType.VarChar)
        If IsNothing(e.NewValues("ADDRESS_1")) Then
            objCmd.Parameters(":p_address_1").Value = ""
        Else
            objCmd.Parameters(":p_address_1").Value = e.NewValues("ADDRESS_1").ToString()
        End If

        objCmd.Parameters.Add(":p_address_2", OracleType.VarChar)
        If IsNothing(e.NewValues("ADDRESS_2")) Then
            objCmd.Parameters(":p_address_2").Value = ""
        Else
            objCmd.Parameters(":p_address_2").Value = e.NewValues("ADDRESS_2").ToString()
        End If

        objCmd.Parameters.Add(":p_city", OracleType.VarChar)
        If IsNothing(e.NewValues("CITY")) Then
            objCmd.Parameters(":p_city").Value = ""
        Else
            objCmd.Parameters(":p_city").Value = e.NewValues("CITY").ToString()
        End If

        objCmd.Parameters.Add(":p_prov", OracleType.VarChar)
        If IsNothing(e.NewValues("PROV")) Then
            objCmd.Parameters(":p_prov").Value = ""
        Else
            objCmd.Parameters(":p_prov").Value = e.NewValues("PROV").ToString()
        End If

        objCmd.Parameters.Add(":p_pc", OracleType.VarChar)
        If IsNothing(e.NewValues("PC")) Then
            objCmd.Parameters(":p_pc").Value = ""
        Else
            objCmd.Parameters(":p_pc").Value = e.NewValues("PC").ToString()
        End If

        objCmd.Parameters.Add(":p_email", OracleType.VarChar)
        If IsNothing(e.NewValues("EMAIL")) Then
            objCmd.Parameters(":p_email").Value = ""
        Else
            objCmd.Parameters(":p_email").Value = e.NewValues("EMAIL").ToString()
        End If

        objCmd.Parameters.Add(":p_phone", OracleType.VarChar)
        If IsNothing(e.NewValues("PHONE")) Then
            objCmd.Parameters(":p_phone").Value = ""
        Else
            objCmd.Parameters(":p_phone").Value = e.NewValues("PHONE").ToString()
        End If

        objCmd.Parameters.Add(":p_co_grp", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_GRP")) Then
            objCmd.Parameters(":p_co_grp").Value = ""
        Else
            objCmd.Parameters(":p_co_grp").Value = e.NewValues("CO_GRP").ToString()
        End If

        objCmd.Parameters.Add(":p_co_name", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_NAME")) Then
            objCmd.Parameters(":p_co_name").Value = ""
        Else
            objCmd.Parameters(":p_co_name").Value = e.NewValues("CO_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_co_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_CD")) Then
            objCmd.Parameters(":p_co_cd").Value = ""
        Else
            objCmd.Parameters(":p_co_cd").Value = e.NewValues("CO_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_co_type", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_TYPE")) Then
            objCmd.Parameters(":p_co_type").Value = ""
        Else
            objCmd.Parameters(":p_co_type").Value = e.NewValues("CO_TYPE").ToString()
        End If

        objCmd.Parameters.Add(":p_loc_type", OracleType.VarChar)
        If IsNothing(e.NewValues("LOC_TYPE")) Then
            objCmd.Parameters(":p_loc_type").Value = ""
        Else
            objCmd.Parameters(":p_loc_type").Value = e.NewValues("LOC_TYPE").ToString()
        End If

        objCmd.Parameters.Add(":p_banner", OracleType.VarChar)
        If IsNothing(e.NewValues("BANNER")) Then
            objCmd.Parameters(":p_banner").Value = ""
        Else
            objCmd.Parameters(":p_banner").Value = e.NewValues("BANNER").ToString()
        End If

        objCmd.Parameters.Add(":p_region", OracleType.VarChar)
        If IsNothing(e.NewValues("REGION")) Then
            objCmd.Parameters(":p_region").Value = ""
        Else
            objCmd.Parameters(":p_region").Value = e.NewValues("REGION").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_store", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_STORE")) Then
            objCmd.Parameters(":p_metro_store").Value = ""
        Else
            objCmd.Parameters(":p_metro_store").Value = e.NewValues("METRO_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_area", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_AREA")) Then
            objCmd.Parameters(":p_metro_area").Value = ""
        Else
            objCmd.Parameters(":p_metro_area").Value = e.NewValues("METRO_AREA").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_area_desc", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_AREA_DESC")) Then
            objCmd.Parameters(":p_metro_area_desc").Value = ""
        Else
            objCmd.Parameters(":p_metro_area_desc").Value = e.NewValues("METRO_AREA_DESC").ToString()
        End If

        objCmd.Parameters.Add(":p_physical_loc", OracleType.VarChar)
        If IsNothing(e.NewValues("PHYSICAL_LOC")) Then
            objCmd.Parameters(":p_physical_loc").Value = ""
        Else
            objCmd.Parameters(":p_physical_loc").Value = e.NewValues("PHYSICAL_LOC").ToString()
        End If

        objCmd.Parameters.Add(":p_open_date", OracleType.DateTime)
        If IsNothing(e.NewValues("OPEN_DATE")) Then
            objCmd.Parameters(":p_open_date").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_open_date").Value = e.NewValues("OPEN_DATE")
        End If

        objCmd.Parameters.Add(":p_close_date", OracleType.DateTime)
        If IsNothing(e.NewValues("CLOSE_DATE")) Then
            objCmd.Parameters(":p_close_date").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_close_date").Value = e.NewValues("CLOSE_DATE")
        End If

        objCmd.Parameters.Add(":p_same_store", OracleType.VarChar)
        If IsNothing(e.NewValues("SAME_STORE")) Then
            objCmd.Parameters(":p_same_store").Value = ""
        Else
            objCmd.Parameters(":p_same_store").Value = e.NewValues("SAME_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_store_status", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_STATUS")) Then
            objCmd.Parameters(":p_store_status").Value = ""
        Else
            objCmd.Parameters(":p_store_status").Value = e.NewValues("STORE_STATUS").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_co_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_CO_CD")) Then
            objCmd.Parameters(":p_fin_co_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_co_cd").Value = e.NewValues("FIN_CO_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_banner_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_BANNER_CD")) Then
            objCmd.Parameters(":p_fin_banner_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_banner_cd").Value = e.NewValues("FIN_BANNER_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_region_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_REGION_CD")) Then
            objCmd.Parameters(":p_fin_region_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_region_cd").Value = e.NewValues("FIN_REGION_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_location_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_LOCATION_CD")) Then
            objCmd.Parameters(":p_fin_location_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_location_cd").Value = e.NewValues("FIN_LOCATION_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_credit_store", OracleType.VarChar)
        If IsNothing(e.NewValues("CREDIT_STORE")) Then
            objCmd.Parameters(":p_credit_store").Value = ""
        Else
            objCmd.Parameters(":p_credit_store").Value = e.NewValues("CREDIT_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_parent_co", OracleType.VarChar)
        If IsNothing(e.NewValues("PARENT_CO")) Then
            objCmd.Parameters(":p_parent_co").Value = ""
        Else
            objCmd.Parameters(":p_parent_co").Value = e.NewValues("PARENT_CO").ToString()
        End If

        objCmd.Parameters.Add(":p_supply_point", OracleType.VarChar)
        If IsNothing(e.NewValues("SUPPLY_POINT")) Then
            objCmd.Parameters(":p_supply_point").Value = ""
        Else
            objCmd.Parameters(":p_supply_point").Value = e.NewValues("SUPPLY_POINT").ToString()
        End If

        objCmd.Parameters.Add(":p_linehaul", OracleType.VarChar)
        If IsNothing(e.NewValues("LINEHAUL")) Then
            objCmd.Parameters(":p_linehaul").Value = ""
        Else
            objCmd.Parameters(":p_linehaul").Value = e.NewValues("LINEHAUL").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_name", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_NAME")) Then
            objCmd.Parameters(":p_fleet_store_name").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_name").Value = e.NewValues("FLEET_STORE_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_manager", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_MANAGER")) Then
            objCmd.Parameters(":p_fleet_store_manager").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_manager").Value = e.NewValues("FLEET_STORE_MANAGER").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_email", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_EMAIL")) Then
            objCmd.Parameters(":p_fleet_store_email").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_email").Value = e.NewValues("FLEET_STORE_EMAIL").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_region", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_REGION")) Then
            objCmd.Parameters(":p_fleet_store_region").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_region").Value = e.NewValues("FLEET_STORE_REGION").ToString()
        End If

        objCmd.Parameters.Add(":p_am_name", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_NAME")) Then
            objCmd.Parameters(":p_am_name").Value = ""
        Else
            objCmd.Parameters(":p_am_name").Value = e.NewValues("AM_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_am_sort_cd", OracleType.Number)
        If IsNothing(e.NewValues("AM_SORT_CD")) Then
            objCmd.Parameters(":p_am_sort_cd").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_am_sort_cd").Value = e.NewValues("AM_SORT_CD")
        End If

        objCmd.Parameters.Add(":p_am_email", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_EMAIL")) Then
            objCmd.Parameters(":p_am_email").Value = ""
        Else
            objCmd.Parameters(":p_am_email").Value = e.NewValues("AM_EMAIL")
        End If

        objCmd.Parameters.Add(":p_am_grp_email", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_GRP_EMAIL")) Then
            objCmd.Parameters(":p_am_grp_email").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_am_grp_email").Value = e.NewValues("AM_GRP_EMAIL")
        End If

        objCmd.Parameters.Add(":p_store_level", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_LEVEL")) Then
            objCmd.Parameters(":p_store_level").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_level").Value = e.NewValues("STORE_LEVEL")
        End If

        objCmd.Parameters.Add(":p_store_sqfeet", OracleType.Number)
        If IsNothing(e.NewValues("STORE_SQFEET")) Then
            objCmd.Parameters(":p_store_sqfeet").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_sqfeet").Value = e.NewValues("STORE_SQFEET")
        End If

        objCmd.Parameters.Add(":p_store_attr1", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR1")) Then
            objCmd.Parameters(":p_store_attr1").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr1").Value = e.NewValues("STORE_ATTR1")
        End If

        objCmd.Parameters.Add(":p_store_attr2", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR2")) Then
            objCmd.Parameters(":p_store_attr2").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr2").Value = e.NewValues("STORE_ATTR2")
        End If

        objCmd.Parameters.Add(":p_store_attr3", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR3")) Then
            objCmd.Parameters(":p_store_attr3").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr3").Value = e.NewValues("STORE_ATTR3")
        End If

        objCmd.Parameters.Add(":p_store_attr4", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR4")) Then
            objCmd.Parameters(":p_store_attr4").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr4").Value = e.NewValues("STORE_ATTR4")
        End If

        objCmd.Parameters.Add(":p_store_attr5", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR5")) Then
            objCmd.Parameters(":p_store_attr5").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr5").Value = e.NewValues("STORE_ATTR5")
        End If

        objCmd.Parameters.Add(":p_store_attr6", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR6")) Then
            objCmd.Parameters(":p_store_attr6").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr6").Value = e.NewValues("STORE_ATTR6")
        End If

        objCmd.Parameters.Add(":p_store_attr7", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR7")) Then
            objCmd.Parameters(":p_store_attr7").Value = ""
        Else
            objCmd.Parameters(":p_store_attr7").Value = e.NewValues("STORE_ATTR7")
        End If

        objCmd.Parameters.Add(":p_store_attr8", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR8")) Then
            objCmd.Parameters(":p_store_attr8").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr8").Value = e.NewValues("STORE_ATTR8")
        End If

        objCmd.Parameters.Add(":p_store_attr9", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR9")) Then
            objCmd.Parameters(":p_store_attr9").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr9").Value = e.NewValues("STORE_ATTR9")
        End If

        objCmd.Parameters.Add(":p_store_attr10", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR10")) Then
            objCmd.Parameters(":p_store_attr10").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr10").Value = e.NewValues("STORE_ATTR10")
        End If

        objCmd.Parameters.Add(":p_transit_no", OracleType.VarChar)
        If IsNothing(e.NewValues("TRANSIT_NO")) Then
            objCmd.Parameters(":p_transit_no").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_transit_no").Value = e.NewValues("TRANSIT_NO")
        End If

        objCmd.Parameters.Add(":p_acct_no", OracleType.VarChar)
        If IsNothing(e.NewValues("ACCT_NO")) Then
            objCmd.Parameters(":p_acct_no").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_acct_no").Value = e.NewValues("ACCT_NO")
        End If

        objCmd.Parameters.Add(":p_market", OracleType.VarChar)
        If IsNothing(e.NewValues("MARKET")) Then
            objCmd.Parameters(":p_market").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_market").Value = e.NewValues("MARKET")
        End If

        objCmd.Parameters.Add(":p_merchant_no", OracleType.VarChar)
        If IsNothing(e.NewValues("MERCHANT_NO")) Then
            objCmd.Parameters(":p_merchant_no").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_merchant_no").Value = e.NewValues("MERCHANT_NO")
        End If

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Store updated successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try


        GV_store_detail.CancelEdit()
        e.Cancel = True
        GV_store_detail.DataBind()
    End Sub


    Protected Sub GV_row_insert(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If String.IsNullOrEmpty(e.NewValues("STORE_CD")) Then
            Throw New Exception("Store code must be entered")
        End If

        'check_duplicate(e.NewValues("STORE_CD").ToString().ToUpper())

        conn.Open()

        sqlString = New StringBuilder("INSERT INTO store_detail ( ")
        sqlString.Append("STORE_CD, STORE_NAME, ADDRESS_1, ADDRESS_2, CITY, PROV, PC, EMAIL, Phone, ")
        sqlString.Append("CO_GRP, CO_NAME, CO_CD, CO_TYPE, LOC_TYPE, BANNER,")
        sqlString.Append("Region, METRO_STORE, METRO_AREA, METRO_AREA_DESC, ")
        sqlString.Append("PHYSICAL_LOC, OPEN_DATE, CLOSE_DATE, SAME_STORE, STORE_STATUS, ")
        sqlString.Append("FIN_CO_CD, FIN_BANNER_CD, FIN_REGION_CD, FIN_LOCATION_CD, CREDIT_STORE, PARENT_CO, ")
        sqlString.Append("SUPPLY_POINT, LINEHAUL, FLEET_STORE_NAME, FLEET_STORE_MANAGER, FLEET_STORE_EMAIL, ")
        sqlString.Append("FLEET_STORE_REGION, AM_NAME, AM_SORT_CD, AM_EMAIL, AM_GRP_EMAIL, STORE_LEVEL, STORE_SQFEET, STORE_ATTR1,")
        sqlString.Append("STORE_ATTR2, STORE_ATTR3, STORE_ATTR4, STORE_ATTR5, STORE_ATTR6, STORE_ATTR7, STORE_ATTR8, STORE_ATTR9,")
        sqlString.Append("STORE_ATTR10, TRANSIT_NO, ACCT_NO, MARKET, MERCHANT_NO )")
        sqlString.Append(" VALUES ( ")
        sqlString.Append(":p_store_cd, :p_store_name, :p_address_1, :p_address_2, :p_city, :p_prov, :p_pc, :p_email, :p_phone,")
        sqlString.Append(":p_co_grp, :p_co_name, :p_co_cd, :p_co_type, :p_loc_type, :p_banner,")
        sqlString.Append(":p_region, :p_metro_store, :p_metro_area, :p_metro_area_desc,")
        sqlString.Append(":p_physical_loc, :p_open_date, :p_close_date, :p_same_store, :p_store_status, ")
        sqlString.Append(":p_fin_co_cd, :p_fin_banner_cd, :p_fin_region_cd, :p_fin_location_cd, :p_credit_store, :p_parent_co, ")
        sqlString.Append(":p_supply_point, :p_linehaul, :p_fleet_store_name, :p_fleet_store_manager, :p_fleet_store_email, ")
        sqlString.Append(":p_fleet_store_region, :p_am_name, :p_am_sort_cd, :p_am_email, :p_am_grp_email, :p_store_level, :p_store_sqfeet,:p_store_attr1,")
        sqlString.Append(":p_store_attr2, :p_store_attr3,:p_store_attr4, :p_store_attr5, :p_store_attr6, :p_store_attr7, :p_store_attr8, :p_store_attr9,")
        sqlString.Append(":p_store_attr10, :p_transit_no, :p_acct_no, :p_market, :p_merchant_no")
        sqlString.Append(")")


        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_store_cd", OracleType.VarChar)
        objCmd.Parameters(":p_store_cd").Value = e.NewValues("STORE_CD").ToString().ToUpper()

        objCmd.Parameters.Add(":p_store_name", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_NAME")) Then
            objCmd.Parameters(":p_store_name").Value = ""
        Else
            objCmd.Parameters(":p_store_name").Value = e.NewValues("STORE_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_address_1", OracleType.VarChar)
        If IsNothing(e.NewValues("ADDRESS_1")) Then
            objCmd.Parameters(":p_address_1").Value = ""
        Else
            objCmd.Parameters(":p_address_1").Value = e.NewValues("ADDRESS_1").ToString()
        End If

        objCmd.Parameters.Add(":p_address_2", OracleType.VarChar)
        If IsNothing(e.NewValues("ADDRESS_2")) Then
            objCmd.Parameters(":p_address_2").Value = ""
        Else
            objCmd.Parameters(":p_address_2").Value = e.NewValues("ADDRESS_2").ToString()
        End If

        objCmd.Parameters.Add(":p_city", OracleType.VarChar)
        If IsNothing(e.NewValues("CITY")) Then
            objCmd.Parameters(":p_city").Value = ""
        Else
            objCmd.Parameters(":p_city").Value = e.NewValues("CITY").ToString()
        End If

        objCmd.Parameters.Add(":p_prov", OracleType.VarChar)
        If IsNothing(e.NewValues("PROV")) Then
            objCmd.Parameters(":p_prov").Value = ""
        Else
            objCmd.Parameters(":p_prov").Value = e.NewValues("PROV").ToString()
        End If

        objCmd.Parameters.Add(":p_pc", OracleType.VarChar)
        If IsNothing(e.NewValues("PC")) Then
            objCmd.Parameters(":p_pc").Value = ""
        Else
            objCmd.Parameters(":p_pc").Value = e.NewValues("PC").ToString()
        End If

        objCmd.Parameters.Add(":p_email", OracleType.VarChar)
        If IsNothing(e.NewValues("EMAIL")) Then
            objCmd.Parameters(":p_email").Value = ""
        Else
            objCmd.Parameters(":p_email").Value = e.NewValues("EMAIL").ToString()
        End If

        objCmd.Parameters.Add(":p_phone", OracleType.VarChar)
        If IsNothing(e.NewValues("PHONE")) Then
            objCmd.Parameters(":p_phone").Value = ""
        Else
            objCmd.Parameters(":p_phone").Value = e.NewValues("PHONE").ToString()
        End If

        objCmd.Parameters.Add(":p_co_grp", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_GRP")) Then
            objCmd.Parameters(":p_co_grp").Value = ""
        Else
            objCmd.Parameters(":p_co_grp").Value = e.NewValues("CO_GRP").ToString()
        End If

        objCmd.Parameters.Add(":p_co_name", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_NAME")) Then
            objCmd.Parameters(":p_co_name").Value = ""
        Else
            objCmd.Parameters(":p_co_name").Value = e.NewValues("CO_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_co_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_CD")) Then
            objCmd.Parameters(":p_co_cd").Value = ""
        Else
            objCmd.Parameters(":p_co_cd").Value = e.NewValues("CO_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_co_type", OracleType.VarChar)
        If IsNothing(e.NewValues("CO_TYPE")) Then
            objCmd.Parameters(":p_co_type").Value = ""
        Else
            objCmd.Parameters(":p_co_type").Value = e.NewValues("CO_TYPE").ToString()
        End If

        objCmd.Parameters.Add(":p_loc_type", OracleType.VarChar)
        If IsNothing(e.NewValues("LOC_TYPE")) Then
            objCmd.Parameters(":p_loc_type").Value = ""
        Else
            objCmd.Parameters(":p_loc_type").Value = e.NewValues("LOC_TYPE").ToString()
        End If

        objCmd.Parameters.Add(":p_banner", OracleType.VarChar)
        If IsNothing(e.NewValues("BANNER")) Then
            objCmd.Parameters(":p_banner").Value = ""
        Else
            objCmd.Parameters(":p_banner").Value = e.NewValues("BANNER").ToString()
        End If

        objCmd.Parameters.Add(":p_region", OracleType.VarChar)
        If IsNothing(e.NewValues("REGION")) Then
            objCmd.Parameters(":p_region").Value = ""
        Else
            objCmd.Parameters(":p_region").Value = e.NewValues("REGION").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_store", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_STORE")) Then
            objCmd.Parameters(":p_metro_store").Value = ""
        Else
            objCmd.Parameters(":p_metro_store").Value = e.NewValues("METRO_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_area", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_AREA")) Then
            objCmd.Parameters(":p_metro_area").Value = ""
        Else
            objCmd.Parameters(":p_metro_area").Value = e.NewValues("METRO_AREA").ToString()
        End If

        objCmd.Parameters.Add(":p_metro_area_desc", OracleType.VarChar)
        If IsNothing(e.NewValues("METRO_AREA_DESC")) Then
            objCmd.Parameters(":p_metro_area_desc").Value = ""
        Else
            objCmd.Parameters(":p_metro_area_desc").Value = e.NewValues("METRO_AREA_DESC").ToString()
        End If

        objCmd.Parameters.Add(":p_physical_loc", OracleType.VarChar)
        If IsNothing(e.NewValues("PHYSICAL_LOC")) Then
            objCmd.Parameters(":p_physical_loc").Value = ""
        Else
            objCmd.Parameters(":p_physical_loc").Value = e.NewValues("PHYSICAL_LOC").ToString()
        End If

        objCmd.Parameters.Add(":p_open_date", OracleType.DateTime)
        If IsNothing(e.NewValues("OPEN_DATE")) Then
            objCmd.Parameters(":p_open_date").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_open_date").Value = e.NewValues("OPEN_DATE")
        End If

        objCmd.Parameters.Add(":p_close_date", OracleType.DateTime)
        If IsNothing(e.NewValues("CLOSE_DATE")) Then
            objCmd.Parameters(":p_close_date").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_close_date").Value = e.NewValues("CLOSE_DATE")
        End If

        objCmd.Parameters.Add(":p_same_store", OracleType.VarChar)
        If IsNothing(e.NewValues("SAME_STORE")) Then
            objCmd.Parameters(":p_same_store").Value = ""
        Else
            objCmd.Parameters(":p_same_store").Value = e.NewValues("SAME_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_store_status", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_STATUS")) Then
            objCmd.Parameters(":p_store_status").Value = ""
        Else
            objCmd.Parameters(":p_store_status").Value = e.NewValues("STORE_STATUS").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_co_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_CO_CD")) Then
            objCmd.Parameters(":p_fin_co_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_co_cd").Value = e.NewValues("FIN_CO_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_banner_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_BANNER_CD")) Then
            objCmd.Parameters(":p_fin_banner_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_banner_cd").Value = e.NewValues("FIN_BANNER_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_region_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_REGION_CD")) Then
            objCmd.Parameters(":p_fin_region_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_region_cd").Value = e.NewValues("FIN_REGION_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_fin_location_cd", OracleType.VarChar)
        If IsNothing(e.NewValues("FIN_LOCATION_CD")) Then
            objCmd.Parameters(":p_fin_location_cd").Value = ""
        Else
            objCmd.Parameters(":p_fin_location_cd").Value = e.NewValues("FIN_LOCATION_CD").ToString()
        End If

        objCmd.Parameters.Add(":p_credit_store", OracleType.VarChar)
        If IsNothing(e.NewValues("CREDIT_STORE")) Then
            objCmd.Parameters(":p_credit_store").Value = ""
        Else
            objCmd.Parameters(":p_credit_store").Value = e.NewValues("CREDIT_STORE").ToString()
        End If

        objCmd.Parameters.Add(":p_parent_co", OracleType.VarChar)
        If IsNothing(e.NewValues("PARENT_CO")) Then
            objCmd.Parameters(":p_parent_co").Value = ""
        Else
            objCmd.Parameters(":p_parent_co").Value = e.NewValues("PARENT_CO").ToString()
        End If

        objCmd.Parameters.Add(":p_supply_point", OracleType.VarChar)
        If IsNothing(e.NewValues("SUPPLY_POINT")) Then
            objCmd.Parameters(":p_supply_point").Value = ""
        Else
            objCmd.Parameters(":p_supply_point").Value = e.NewValues("SUPPLY_POINT").ToString()
        End If

        objCmd.Parameters.Add(":p_linehaul", OracleType.VarChar)
        If IsNothing(e.NewValues("LINEHAUL")) Then
            objCmd.Parameters(":p_linehaul").Value = ""
        Else
            objCmd.Parameters(":p_linehaul").Value = e.NewValues("LINEHAUL").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_name", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_NAME")) Then
            objCmd.Parameters(":p_fleet_store_name").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_name").Value = e.NewValues("FLEET_STORE_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_manager", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_MANAGER")) Then
            objCmd.Parameters(":p_fleet_store_manager").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_manager").Value = e.NewValues("FLEET_STORE_MANAGER").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_email", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_EMAIL")) Then
            objCmd.Parameters(":p_fleet_store_email").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_email").Value = e.NewValues("FLEET_STORE_EMAIL").ToString()
        End If

        objCmd.Parameters.Add(":p_fleet_store_region", OracleType.VarChar)
        If IsNothing(e.NewValues("FLEET_STORE_REGION")) Then
            objCmd.Parameters(":p_fleet_store_region").Value = ""
        Else
            objCmd.Parameters(":p_fleet_store_region").Value = e.NewValues("FLEET_STORE_REGION").ToString()
        End If

        objCmd.Parameters.Add(":p_am_name", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_NAME")) Then
            objCmd.Parameters(":p_am_name").Value = ""
        Else
            objCmd.Parameters(":p_am_name").Value = e.NewValues("AM_NAME").ToString()
        End If

        objCmd.Parameters.Add(":p_am_sort_cd", OracleType.Number)
        If IsNothing(e.NewValues("AM_SORT_CD")) Then
            objCmd.Parameters(":p_am_sort_cd").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_am_sort_cd").Value = e.NewValues("AM_SORT_CD")
        End If

        objCmd.Parameters.Add(":p_am_email", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_EMAIL")) Then
            objCmd.Parameters(":p_am_email").Value = ""
        Else
            objCmd.Parameters(":p_am_email").Value = e.NewValues("AM_EMAIL")
        End If

        objCmd.Parameters.Add(":p_am_grp_email", OracleType.VarChar)
        If IsNothing(e.NewValues("AM_GRP_EMAIL")) Then
            objCmd.Parameters(":p_am_grp_email").Value = ""
        Else
            objCmd.Parameters(":p_am_grp_email").Value = e.NewValues("AM_GRP_EMAIL")
        End If

        objCmd.Parameters.Add(":p_store_level", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_LEVEL")) Then
            objCmd.Parameters(":p_store_level").Value = ""
        Else
            objCmd.Parameters(":p_store_level").Value = e.NewValues("STORE_LEVEL")
        End If

        objCmd.Parameters.Add(":p_store_sqfeet", OracleType.Number)
        If IsNothing(e.NewValues("STORE_SQFEET")) Then
            objCmd.Parameters(":p_store_sqfeet").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_sqfeet").Value = e.NewValues("STORE_SQFEET")
        End If

        objCmd.Parameters.Add(":p_store_attr1", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR1")) Then
            objCmd.Parameters(":p_store_attr1").Value = ""
        Else
            objCmd.Parameters(":p_store_attr1").Value = e.NewValues("STORE_ATTR1")
        End If

        objCmd.Parameters.Add(":p_store_attr2", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR2")) Then
            objCmd.Parameters(":p_store_attr2").Value = ""
        Else
            objCmd.Parameters(":p_store_attr2").Value = e.NewValues("STORE_ATTR2")
        End If

        objCmd.Parameters.Add(":p_store_attr3", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR3")) Then
            objCmd.Parameters(":p_store_attr3").Value = ""
        Else
            objCmd.Parameters(":p_store_attr3").Value = e.NewValues("STORE_ATTR3")
        End If

        objCmd.Parameters.Add(":p_store_attr4", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR4")) Then
            objCmd.Parameters(":p_store_attr4").Value = ""
        Else
            objCmd.Parameters(":p_store_attr4").Value = e.NewValues("STORE_ATTR4")
        End If

        objCmd.Parameters.Add(":p_store_attr5", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR5")) Then
            objCmd.Parameters(":p_store_attr5").Value = ""
        Else
            objCmd.Parameters(":p_store_attr5").Value = e.NewValues("STORE_ATTR5")
        End If

        objCmd.Parameters.Add(":p_store_attr6", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR6")) Then
            objCmd.Parameters(":p_store_attr6").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr6").Value = e.NewValues("STORE_ATTR6")
        End If

        objCmd.Parameters.Add(":p_store_attr7", OracleType.VarChar)
        If IsNothing(e.NewValues("STORE_ATTR7")) Then
            objCmd.Parameters(":p_store_attr7").Value = ""
        Else
            objCmd.Parameters(":p_store_attr7").Value = e.NewValues("STORE_ATTR7")
        End If

        objCmd.Parameters.Add(":p_store_attr8", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR8")) Then
            objCmd.Parameters(":p_store_attr8").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr8").Value = e.NewValues("STORE_ATTR8")
        End If

        objCmd.Parameters.Add(":p_store_attr9", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR9")) Then
            objCmd.Parameters(":p_store_attr9").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr9").Value = e.NewValues("STORE_ATTR9")
        End If

        objCmd.Parameters.Add(":p_store_attr10", OracleType.Number)
        If IsNothing(e.NewValues("STORE_ATTR10")) Then
            objCmd.Parameters(":p_store_attr10").Value = DBNull.Value
        Else
            objCmd.Parameters(":p_store_attr10").Value = e.NewValues("STORE_ATTR10")
        End If

        objCmd.Parameters.Add(":p_transit_no", OracleType.VarChar)
        If IsNothing(e.NewValues("TRANSIT_NO")) Then
            objCmd.Parameters(":p_transit_no").Value = ""
        Else
            objCmd.Parameters(":p_transit_no").Value = e.NewValues("TRANSIT_NO")
        End If

        objCmd.Parameters.Add(":p_acct_no", OracleType.VarChar)
        If IsNothing(e.NewValues("ACCT_NO")) Then
            objCmd.Parameters(":p_acct_no").Value = ""
        Else
            objCmd.Parameters(":p_acct_no").Value = e.NewValues("ACCT_NO")
        End If

        objCmd.Parameters.Add(":p_market", OracleType.VarChar)
        If IsNothing(e.NewValues("MARKET")) Then
            objCmd.Parameters(":p_market").Value = ""
        Else
            objCmd.Parameters(":p_market").Value = e.NewValues("MARKET")
        End If

        objCmd.Parameters.Add(":p_merchant_no", OracleType.VarChar)
        If IsNothing(e.NewValues("MERCHANT_NO")) Then
            objCmd.Parameters(":p_merchant_no").Value = ""
        Else
            objCmd.Parameters(":p_merchant_no").Value = e.NewValues("MERCHANT_NO")
        End If

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Store Added Successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_store_detail.CancelEdit()
        e.Cancel = True
        GV_store_detail.DataBind()
    End Sub

    Protected Sub GV_row_delete(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)

        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("DELETE FROM store_detail WHERE ROWID = :p_rowid ")

        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        conn.Open()
        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Store deleted successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_store_detail.CancelEdit()
        e.Cancel = True
        GV_store_detail.DataBind()
    End Sub

    Protected Sub GVCellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        'If GV_product_attribute.IsNewRowEditing Then
        'Return
        'End If

        e.Editor.ReadOnly = False

    End Sub

    Private Sub check_duplicate(attr_cd As String)
        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim objValidate As Object
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        conn.Open()
        objCmd.Connection = conn
        objCmd.CommandType = CommandType.Text

        sqlString = New StringBuilder(" SELECT store_cd FROM store_detail ")
        sqlString.Append("WHERE store_cd = UPPER(:p_store_cd)")

        objCmd.CommandText = sqlString.ToString()
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_store_cd", OracleType.VarChar)
        objCmd.Parameters(":p_store_cd").Value = attr_cd

        objValidate = objCmd.ExecuteScalar()
        If objValidate <> Nothing Then
            Throw New Exception("Store code must be unique")
        End If
        conn.Close()
    End Sub

    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store_detail order by store_cd"

        txt_store_cd.Items.Insert(0, "Select a Store Code")
        txt_store_cd.Items.FindByText("Select a Store Code").Value = ""
        txt_store_cd.SelectedIndex = 0


        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_store_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Protected Sub GridView2_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        e.Editor.ReadOnly = False
        e.Editor.ClientEnabled = True
    End Sub

End Class



