Imports DevExpress.Web.ASPxFileManager

Partial Class File_Manager_index
    Inherits POSBasePage

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        'HBCG_Utils.Update_Theme()

    End Sub

    Public Sub fileManager_CustomThumbnail(ByVal sender As Object, ByVal e As FileManagerThumbnailCreateEventArgs)

        Dim strTemp As String = ""

        Select Case e.File.Extension
            Case ".xls", ".xlsx"
                strTemp = "xls"
            Case ".asp"
                strTemp = "html"
            Case ".doc", ".rtf", ".docx"
                strTemp = "doc"
            Case ".mdb", ".mdbx"
                strTemp = "mdb"
            Case ".mov"
                strTemp = "mov"
            Case ".mpeg", ".mpg"
                strTemp = "mpeg"
            Case ".pdf"
                strTemp = "pdf"
            Case ".ppt", ".pptx"
                strTemp = "ppt"
            Case ".pub", ".pubx"
                strTemp = "pub"
            Case ".zip", ".rar"
                strTemp = "zip"
            Case ".htm", ".html"
                strTemp = "html"
            Case ".gif", ".jpg", ".png"
                strTemp = "image"
            Case ".txt"
                strTemp = "html"
            Case Else
                strTemp = "image"
        End Select

        e.ThumbnailImage.Url = "../images/icons/" & strTemp & ".jpg"

    End Sub

End Class
