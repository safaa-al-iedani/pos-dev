<%@ Page Language="VB" MasterPageFile="NoWizard3.master" AutoEventWireup="false"
    CodeFile="index.aspx.vb" Inherits="File_Manager_index" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFileManager" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="UploadDetails" visible="False" runat="Server">
        &nbsp;</div>
    <dx:aspxfilemanager runat="server" ID="file_mgr" OnCustomThumbnail="fileManager_CustomThumbnail" Height="641px">
<Settings RootFolder="~\merch_images\File_Manager" AllowedFileExtensions=".XLS, .MDB, .TXT, .PPT, .PPTX, .XLSX, .DOC, .DOCX, .JPG, .JPEG, .MOV, .MPEG, .GIF, .HTML, .PDF, .PUB"></Settings>
        <SettingsToolbar ShowDownloadButton="True" />
        <SettingsEditing AllowDelete="True" AllowMove="True" AllowRename="True" AllowCreate="True" />
</dx:aspxfilemanager>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
