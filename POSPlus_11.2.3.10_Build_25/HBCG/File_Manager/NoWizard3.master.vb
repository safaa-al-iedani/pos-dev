Imports System.Data.OracleClient
Imports ErrorManager

Partial Class NoWizard3
    Inherits System.Web.UI.MasterPage

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=../timeout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=../timeout.aspx")
        End If

        img_logo.ImageUrl = "~/" & ConfigurationManager.AppSettings("company_logo").ToString

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("../login.aspx")
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "* TRAIN MODE *"
            lbl_header.ForeColor = Color.Red
        End If

        If Not IsPostBack Then
            Dim sql As String
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            If Session("EMP_FNAME") & "" = "" Or Session("EMP_LNAME") & "" = "" Then
                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If

                sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("EMP_CD") = MyDataReader.Item("EMP_CD").ToString
                        Session("EMP_FNAME") = MyDataReader.Item("FNAME").ToString
                        Session("EMP_LNAME") = MyDataReader.Item("LNAME").ToString
                        Session("HOME_STORE_CD") = MyDataReader.Item("HOME_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                sql = "SELECT SHIP_TO_STORE_CD FROM STORE WHERE STORE_CD='" & UCase(Session("HOME_STORE_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("pd_store_cd") = MyDataReader.Item("SHIP_TO_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            End If
            If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
                lbl_Header1.Text = "Welcome " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            End If
            If Session("HOME_STORE_CD") & "" <> "" Then
                'lbl_Header2.Text = "Store Code: " & Session("HOME_STORE_CD")
                lbl_Header2.Text = Resources.LibResources.Label573 & ": " & Session("HOME_STORE_CD")
            End If
            If Request("D") = "D" Then
                lblTabID.Text = Session("MP")
            End If
            If Request("D") = "I" Then
                lblTabID.Text = Session("IP")
            End If

            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink1").ToString) Then
                ASPxMenu1.RootItem.Items(4).Text = ConfigurationManager.AppSettings("link1_desc").ToString
                ASPxMenu1.RootItem.Items(4).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink1").ToString
                ASPxMenu1.RootItem.Items(4).Target = "_new"
            End If
            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink2").ToString) Then
                ASPxMenu1.RootItem.Items(5).Text = ConfigurationManager.AppSettings("link2_desc").ToString
                ASPxMenu1.RootItem.Items(5).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink2").ToString
                ASPxMenu1.RootItem.Items(5).Target = "_new"
            End If

            Dim strURL As String
            Dim arrayURL As Array
            Dim pagename As String

            strURL = Request.ServerVariables("SCRIPT_NAME")
            arrayURL = Split(strURL, "/", -1, 1)
            pagename = arrayURL(UBound(arrayURL))

            If LCase(pagename) = "newsmaintenance.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "NEWS AND UPDATES - MAINTENANCE"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "News and Updates - Maintenance"
            End If

            If LCase(pagename) = "admin.aspx" Then
                lblTabID.Text = "SYSTEM PARAMETERS"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Application Maintenance"
            End If

            If LCase(pagename) = "independentpaymentprocessing.aspx" Then
                lblTabID.Text = "PAYMENT PROCESSING"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payment Processing"
                'ASPxMenu1.RootItem.Items(0).Text = ""
                'ASPxMenu1.RootItem.Items(0).NavigateUrl = ""
                'ASPxMenu1.RootItem.Items(1).Text = ""
                'ASPxMenu1.RootItem.Items(1).NavigateUrl = ""
                'ASPxMenu1.RootItem.Items(2).Text = ""
                'ASPxMenu1.RootItem.Items(2).NavigateUrl = ""
                ASPxMenu1.RootItem.Items(3).Text = "Quick Screen"
                ASPxMenu1.RootItem.Items(3).NavigateUrl = "~/GEQuickCredit.aspx"
            End If

            'If Request("referrer") = "IndependentPaymentProcessing.aspx" Then
            '    ASPxMenu1.RootItem.Items(3).Text = "Quick Screen"
            '    ASPxMenu1.RootItem.Items(3).NavigateUrl = "GEQuickCredit.aspx"
            'End If

            If LCase(pagename) = "terminal_setup.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "TERMINAL SETUP"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Terminal Setup"
            End If

            If LCase(pagename) = "terminal_status.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "TERMINAL STATUS"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Terminal Status"
            End If

            If LCase(pagename) = "invoice_triggers.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "INVOICE TRIGGER SETUP"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Invoice Trigger Setup"
            End If

            If LCase(pagename) = "InventoryComparison.aspx" Then
                lblTabID.Text = "MERCHANDISE COMPARISON"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Merchandise Comparison"
            End If

            If LCase(pagename) = "relationship_maintenance.aspx" Then
                lblTabID.Text = Resources.LibResources.Label456
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Maintenance"
            End If

            If LCase(pagename) = "relationship_calendar.aspx" Then
                lblTabID.Text = Resources.LibResources.Label455
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Calendar"
            End If

            If LCase(pagename) = "relationship_reporting.aspx" Then
                lblTabID.Text = "RELATIONSHIP REPORTING"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Reporting"
            End If

            If LCase(pagename) = "campaign_main.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "CAMPAIGN"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Launch Campaign"
            End If

            If LCase(pagename) = "paymentonaccount.aspx" Then
                lblTabID.Text = "PAYMENT ON ACCOUNT"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payment on Account"
            End If

            If LCase(pagename) = "picture_upload.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "UTILITIES : UPLOAD IMAGES"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Upload Images"
            End If

            If LCase(pagename) = "discount_lookup.aspx" Then
                lblTabID.Text = "DISCOUNT LOOKUP"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Discount Lookup"
            End If

            If LCase(pagename) = "main_cust_lookup.aspx" Then
                lblTabID.Text = "CUSTOMER SEARCH"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Customer Search"
            End If

            If LCase(pagename) = "invoice_admin.aspx" Then
                hpl_exit.NavigateUrl = "~/admin.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "INVOICE ASSIGN"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Invoice Assignment"
            End If

            If LCase(pagename) = "testcc.aspx" Then
                hpl_exit.NavigateUrl = "~/admin.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "TEST CC"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Test Credit Cards"
            End If

            If LCase(pagename) = "merchant_info.aspx" Then
                hpl_exit.NavigateUrl = "~/admin.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "MERCHANT INFO"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Merchant Information"
            End If

            If LCase(pagename) = "admin_rights.aspx" Then
                hpl_exit.NavigateUrl = "~/admin.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "ADMINISTRATIVE RIGHTS"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Administrative Rights"
            End If

            If LCase(pagename) = "help_admin.aspx" Then
                hpl_exit.NavigateUrl = "~/admin.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "HELP ADMIN"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Help Page Administration"
            End If

            If LCase(pagename) = "utilities.aspx" Then
                lblTabID.Text = "UTILITIES"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Utilities"
            End If

            If LCase(pagename) = "reporting.aspx" Then
                lblTabID.Text = "REPORTING"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Reporting Functions"
            End If

            If LCase(pagename) = "main_customer.aspx" Then
                lblTabID.Text = "CUSTOMER MAINTENANCE"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Customer Maintenance"
            End If

            If LCase(pagename) = "salesordermaintenance.aspx" Then
                lblTabID.Text = "SALES ORDER MAINTENANCE"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Maintenance"
            End If

            If LCase(pagename) = "main_so_lookup.aspx" Then
                lblTabID.Text = "SALES ORDER SEARCH"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Search"
            End If

            If LCase(pagename) = "salesbookofbusiness.aspx" Then
                lblTabID.Text = "SALES BOOK OF BUSINESS"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Book of Business"
            End If

            If LCase(pagename) = "salestrends.aspx" Then
                lblTabID.Text = "SALES TRENDS"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Trends"
            End If

            If LCase(pagename) = "salesreport.aspx" Then
                lblTabID.Text = "REPORTING :  SALES ORDER REPORT"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Report"
            End If

            If LCase(pagename) = "salesrewrite.aspx" Then
                lblTabID.Text = "SALES ORDER REWRITE"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Rewrite"
            End If

            If LCase(pagename) = "cashdrawerreport.aspx" Then
                lblTabID.Text = "REPORTING :  DAILY RECONCILIATION"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Daily Reconciliation"
            End If

            If LCase(pagename) = "custom_availability.aspx" Then
                hpl_exit.NavigateUrl = "~/custom_availability.aspx"
                hpl_exit.Text = "Lookup Another"
                hpl_exit.Visible = True
                lblTabID.Text = "INVENTORY LOOKUP"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Inventory Lookup"
            End If

            If LCase(pagename) = "restore_saved_order.aspx" Then
                lblTabID.Text = "RESTORE SAVED ORDER"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Restore Saved Order"
            End If

            If LCase(pagename) = "generalinterest.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "GENERAL INTEREST"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "General Interest Categories"
            End If

            If LCase(pagename) = "asp_promo_admin.aspx" Then
                lblTabID.Text = "FINANCE ADDENDUM ADMINISTRATION"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Finance Addendum Selection"
            End If

            If LCase(pagename) = "payment_xref.aspx" Then
                hpl_exit.NavigateUrl = "~/utilities.aspx"
                hpl_exit.Visible = True
                lblTabID.Text = "Payment Setup"
                Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payment Setup"
            End If
        End If

    End Sub
End Class

