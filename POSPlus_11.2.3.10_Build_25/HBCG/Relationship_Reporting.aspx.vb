Imports System.Data.OracleClient


Partial Class Reports_Relationship_Reporting
    Inherits POSBasePage


    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click

        Response.Redirect("newmain.aspx")

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim del_chg As Double = 0
        Dim tax_chg As Double = 0
        Dim setup_chg As Double = 0
        Dim subtotal As Double = 0
        Dim sql As String
        Dim HTML As String = ""

        If Not IsDate(txt_start_dt.Text) Or Not IsDate(txt_end_dt.Text) Then
            lbl_errors.Text = "You must enter a start and end written date."
            Exit Sub
        End If

        conn.Open()

        sql = "Select * from RELATIONSHIP WHERE WR_DT BETWEEN TO_DATE('" & txt_start_dt.Text
        sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
        If cbo_ord_stat.SelectedValue = "C" Then
            sql = sql & "AND CONVERSION_DT IS NOT NULL "
        ElseIf cbo_ord_stat.SelectedValue = "O" Then
            sql = sql & "AND CONVERSION_DT IS NULL "
        End If
        If IsDate(txt_del_start.Text) And IsDate(txt_del_end.Text) Then
            sql = sql & "AND FOLLOW_UP_DT BETWEEN TO_DATE('" & txt_del_start.Text
            sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_del_end.Text & "','mm/dd/RRRR') "
        End If
        If cbo_ord_tp_cd.SelectedValue <> "ALL" Then
            sql = sql & "AND APPT_TYPE = '" & cbo_ord_tp_cd.SelectedValue & "' "
        End If
        If cbo_pd.SelectedValue <> "B" Then
            sql = sql & "AND P_D = '" & cbo_pd.SelectedValue & "' "
        End If
        sql = sql & "AND SLSP1 = '" & Session("EMP_CD") & "' "
        sql = sql & "ORDER BY " & RadioButtonList1.SelectedValue

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.HasRows Then
                HTML = HTML & "<table border=0 width=""100%"" cellpadding=0 cellspacing=0 border=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey align=center><b>RELATIONSHIP REPORT</b></td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=2 bgcolor=lightgrey align=left>Run By: " & Session("EMP_CD") & "</td>"
                HTML = HTML & "<td colspan=3 bgcolor=lightgrey align=right>" & Now() & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5><hr></td>"
                HTML = HTML & "</tr>"
            Else
                HTML = "<br /><br />No records returned.<br /><br />"
            End If
            Do While MyDataReader.Read
                subtotal = 0
                If MyDataReader.Item("DEL_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("DEL_CHG")) Then
                        del_chg = CDbl(MyDataReader.Item("DEL_CHG").ToString)
                    Else
                        del_chg = 0
                    End If
                Else
                    del_chg = 0
                End If
                setup_chg = 0
                If MyDataReader.Item("TAX_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("TAX_CHG")) Then
                        tax_chg = CDbl(MyDataReader.Item("TAX_CHG").ToString)
                    Else
                        tax_chg = 0
                    End If
                Else
                    tax_chg = 0
                End If
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("FNAME").ToString & " " & MyDataReader.Item("LNAME").ToString & "</td>"
                HTML = HTML & "<td align=left>"
                HTML = HTML & "<a href=""Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & MyDataReader.Item("REL_NO") & """>" & MyDataReader.Item("REL_NO") & "</a></td>"
                HTML = HTML & "<td align=left>Customer:&nbsp;<a href=""Main_Customer.aspx?query_returned=Y&cust_cd=" & MyDataReader.Item("CUST_CD") & """>" & MyDataReader.Item("CUST_CD") & "</a></td>"
                'Pickup/Delivery Information
                HTML = HTML & "<td colspan=2 rowspan=4 align=right valign=top>"
                HTML = HTML & "<table border=0 width=""80%"" cellpadding=0 cellspacing=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>&nbsp;&nbsp;"
                Select Case MyDataReader.Item("P_D").ToString
                    Case "P"
                        HTML = HTML & "Pickup"
                    Case "D"
                        HTML = HTML & "Delivery"
                End Select

                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "</table>"
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("ADDR1").ToString & "</td>"
                HTML = HTML & "<td align=left>"
                If IsDate(MyDataReader.Item("CONVERSION_DT").ToString) Then
                    HTML = HTML & "(C)onverted"
                Else
                    HTML = HTML & "(O)pen"
                End If
                HTML = HTML & "</td>"
                HTML = HTML & "<td>&nbsp;</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("CITY").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("ST").ToString & "&nbsp;&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("ZIP") & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("HPHONE").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("BPHONE").ToString & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>&nbsp;</td>"
                'Populate the Sales Order Line information
                HTML = HTML & "<td colspan=4 align=left>"
                sql = "SELECT * FROM RELATIONSHIP_LINES "
                sql = sql & "WHERE REL_NO = '" & MyDataReader.Item("REL_NO").ToString & "' "
                sql = sql & "ORDER BY LINE"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Dim FoundME As Boolean = False
                    If MyDataReader2.Read Then
                        HTML = HTML & "<table width=""100%"" cellpadding=0 cellspacing=0>"
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("LINE").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                        subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("RET_PRC").ToString)
                        HTML = HTML & "<td align=right width=100>" & MyDataReader2.Item("RET_PRC").ToString & "</td>"
                        HTML = HTML & "</tr>"
                        FoundME = True
                    End If
                    Do While MyDataReader2.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("LINE").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                        subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("RET_PRC").ToString)
                        HTML = HTML & "<td align=right width=100>" & MyDataReader2.Item("RET_PRC").ToString & "</td>"
                        HTML = HTML & "</tr>"
                    Loop
                    If FoundME = True Then
                        HTML = HTML & "</table>"
                    End If
                    MyDataReader2.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 align=left><b><u><a href=Relationship_Comments.aspx?del_doc_num=" & MyDataReader.Item("REL_NO").ToString
                HTML = HTML & "&cust_cd=" & MyDataReader.Item("CUST_CD").ToString & ">Comments:</a></u></b></td>"
                HTML = HTML & "</tr>"

                'Add Sales Order Comments
                sql = "Select * from RELATIONSHIP_COMMENTS Where REL_NO = '" & MyDataReader.Item("REL_NO").ToString & "'"
                sql = sql & " ORDER BY SUBMIT_DT ASC"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Dim Repeat_Me As Boolean = True
                Dim comment_data As String = ""
                Dim term_comm As Boolean = False
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If Not MyDataReader2.Read Then
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td colspan=3>&nbsp;</td>"
                    Else
                        comment_data = comment_data & "<tr><td colspan=3 align=left valign=top>"
                        comment_data = comment_data & "<table width=""100%"" celpadding=0 cellspacing=0>"
                        term_comm = True
                    End If
                    Do While MyDataReader2.Read
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td align=left width=70>" & FormatDateTime(MyDataReader2.Item("SUBMIT_DT").ToString, DateFormat.ShortDate)
                        comment_data = comment_data & "</td>"
                        comment_data = comment_data & "<td colspan=2 align=left>" & MyDataReader2.Item("COMMENTS").ToString & "</td>"
                        comment_data = comment_data & "</tr>"
                    Loop
                    MyDataReader2.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                If term_comm = True Then
                    comment_data = comment_data & "</table></td><td colspan=2 valign=top align=right>"
                Else
                    comment_data = comment_data & "<td colspan=2 valign=top align=right>"
                End If

                'Add the balance and payment information
                Dim payment_data As String

                Dim deposit_totals As Double
                Dim grand_total As Double = 0
                deposit_totals = 0

                If IsNumeric(MyDataReader.Item("PROSPECT_TOTAL").ToString) Then
                    grand_total = CDbl(MyDataReader.Item("PROSPECT_TOTAL").ToString)
                Else
                    grand_total = 0
                End If
                payment_data = "<table cellpadding=0 cellspacing=0 border=0>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>SubTotal:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(subtotal, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Delivery:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(del_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Setup:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(setup_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Taxes:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(tax_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Grand Total:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(grand_total, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "</table>"
                HTML = HTML & comment_data & payment_data & "</td></tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey>&nbsp;</td>"
                HTML = HTML & "</tr>"
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
        If InStr(HTML, "No records returned.") < 1 Then
            HTML = HTML & "</table>"
        End If
        main_body.InnerHtml = HTML

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
