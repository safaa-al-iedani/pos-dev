Imports System
Imports System.IO
Imports System.Data.OracleClient
Imports System.Xml
Imports SD_Utils
Imports HBCG_Utils

Partial Class OTB
    Inherits POSBasePage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim cmd As OracleCommand
            Dim rdr As OracleDataReader

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT NAME, AS_CD FROM ASP " &
                            " WHERE  ACTIVE = 'Y' " &
                            " AND  AS_TP_CD = 'F' " &
                            " AND ELECTRONIC_INT= 'Y' " &
                            " AND ELECTRONIC_CR_APP = 'Y' " &
                            " ORDER BY NAME"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                conn.Open()
                rdr = DisposablesManager.BuildOracleDataReader(cmd, CommandBehavior.CloseConnection)    'this will close the connection as soon as the reader is closed

                If (rdr.HasRows) Then
                    cboProviders.Items.Clear()
                    cboProviders.DataSource = rdr
                    cboProviders.DataTextField = "NAME"
                    cboProviders.DataValueField = "AS_CD"
                    cboProviders.DataBind()
                    cboProviders.Items.Insert(0, "Select a Provider")
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "Setup Error", "alert('" + "No ECA providers found in the system.\n Please contact the System Administrator." + "');", True)
                End If

                cboProviders.SelectedIndex = 0
                rdr.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

    End Sub


    Protected Sub btn_save_exit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_save_exit.Click

        'first check if data has been captured
        If (ValidateData()) Then

            Dim terminalId As String = ""
            Dim processorId As String = ""
            Dim SDC_Response As String = ""
            Dim Result_String As String = ""
            Dim SDC_Fields As String() = Nothing

            'retrieve the auth provider specific details from the db
            Dim store As String = Session("HOME_STORE_CD")
            Dim ds As DataSet = GetAuthDetails(cboProviders.SelectedValue, store, GetCashDrawerCd(store))

            For Each dr In ds.Tables(0).Rows
                terminalId = dr("terminal_id")
                processorId = dr("processor_id")
            Next


            'to avoid a null pointer exception when there is no zip, pass-in a zero as the pmt-id to the auth routine
            Dim id As Integer = 0
            If (Not String.IsNullOrEmpty(txt_zip.Text)) Then
                id = Integer.Parse(txt_zip.Text)
            End If

            'build the request and call for auth 
            SDC_Response = SD_Submit_Query_PL(id, terminalId, AUTH_OTB, txt_ssn.Text, txt_account.Text, processorId)

            Dim sep(3) As Char
            Dim s As String
            Dim MsgArray As Array
            sep(0) = Chr(10)
            sep(1) = Chr(12)

            SDC_Fields = SDC_Response.Split(sep)

            For Each s In SDC_Fields
                If InStr(s, ",") > 0 Then
                    MsgArray = Split(s, ",")
                    Select Case MsgArray(0)
                        Case "0631"
                            Dim amt As String = FormatResponseAmtAsCurrency(Left(MsgArray(1), Len(MsgArray(1)) - 1))
                            Result_String = String.Format("{0}Open to Buy: {1}<br />", Result_String, amt)
                        Case "1002"
                            Result_String = String.Format("{0}Card Holder: {1}<br />", Result_String, Left(MsgArray(1), Len(MsgArray(1)) - 1))
                        Case "3305"
                            Dim acctNum As String = MaskAccountNumber(Left(MsgArray(1), Len(MsgArray(1)) - 1))
                            Result_String = String.Format("{0}Account Number: {1}<br />", Result_String, acctNum)
                        Case "3306"
                            Dim amt As String = FormatResponseAmtAsCurrency(Left(MsgArray(1), Len(MsgArray(1)) - 1))
                            Result_String = String.Format("{0}Credit Limit: {1}<br />", Result_String, amt)
                    End Select
                End If
            Next s

            lbl_response.Text = ""
            If (Result_String.isNotEmpty) Then
                txt_account.Text = ""
                txt_ssn.Text = ""
                txt_zip.Text = ""
                lbl_response.Text = String.Format("RESPONSE:<br /> {0} {1} ", System.Environment.NewLine, Result_String)
            End If

        End If

    End Sub


    Private Function ValidateData() As Boolean
        'validates that the data entered  to ensure that at least 2 fields are poulated

        Dim rtnVal As Boolean = True
        Dim nullCount = 0

        If (String.IsNullOrEmpty(txt_ssn.Text)) Then
            nullCount += 1
        End If
        If (String.IsNullOrEmpty(txt_zip.Text)) Then
            nullCount += 1
        End If

        If nullCount > 1 Then
            rtnVal = False
            lbl_response.Text = "The Account Number and at least one other field must be entered."
        End If

        If cboProviders.SelectedIndex = 0 Then
            rtnVal = False
            lbl_response.Text = " Please select a provider."
        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Converts an amount receieved in the auth response to a proper currency format. 
    ''' The amount received is usually padded with zeroes with implied decimals, like
    ''' '0000060000', which really means $60.00.
    ''' 
    ''' </summary>
    ''' <param name="authResponseAmt">the auth text value containing the currency amt to be formatted</param>
    ''' <returns>a properly formatted currency amt</returns>
    Private Function FormatResponseAmtAsCurrency(ByVal authResponseAmt As String) As String
        Dim rtnVal As String = authResponseAmt

        'check if the value is numeric, then convert to a double to divide by 100 and display as currency
        If (authResponseAmt.isNotEmpty AndAlso IsNumeric(authResponseAmt)) Then
            rtnVal = FormatCurrency(CDbl(authResponseAmt) / 100, 2)
        End If

        Return rtnVal
    End Function

End Class
