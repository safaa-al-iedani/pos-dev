﻿Imports System.Collections.Generic
Imports TransportationLibrary
Imports SalesLibrary.OrderInfoUtils
Imports SalesLibrary
Imports System.Data.OracleClient
Imports DevExpress.XtraEditors
Imports StringUtils
Imports System.Web.UI.WebControls


Partial Class SalesConfirmation
    Inherits POSBasePage

    ' when building quasi-bulk queries, how many bound vars to use?  this constant keeps them consistent and thus re-usable in sqlarea
    ' and in testing it is actually faster this way, e.g. 4 queries of 100, 100, 100, 43 + 57 NULL is faster than 
    ' a unique new query for all 343. If there are many keys to query, the very best size, in elapsed-time, seems to be 
    ' about 175 to 200.  But 100 works quite well.  Above 200 it starts to slow down and the shared memory grows disporportionately.
    ' Any list size greater than 1 is faster than querying them all one-by-one.  Any list size between 50 and 200 is faster
    ' than querying 300+ at a time.  The observation of an optimum near 175 may be an artifact of the test case with 343 - it 
    ' has the fewest dead slots with null in the last query - so we'll go with 100 as that is well into the performance band and
    ' saves a bit on sga.
    Private Const NormalNumberOfBoundParameters = AppConstants.AdvancedQueryConstants.NormalNumberOfBoundParameters
    ' but a few queries get GIGANTIC when we do this and we may need to make a tradeoff 
    Private Const LimitedNumberOfBoundParameters = AppConstants.AdvancedQueryConstants.LimitedNumberOfBoundParameters

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private g_SocSes As SocSession
    Dim SecurityCache As New System.Collections.Generic.Dictionary(Of String, Boolean)                  'oct 5,2017-mariam -3250 mccl

    Private Class SoGrid

        Public Const SELECT_COL As Integer = 0
        Public Const DEL_DOC_NUM As Integer = 1
        Public Const SPLIT_PCT As Integer = 2
        Public Const BILL_TO_NAME As Integer = 3
        Public Const MULT_ORDS As Integer = 4
        Public Const CONFIRMED As Integer = 5 '  to be added later; removed for initial phase
        Public Const RESERVED As Integer = 6
        Public Const INV_AVAIL As Integer = 7
        Public Const PAID_IN_FULL As Integer = 8
        Public Const INVOICE_AMT As Integer = 9
        Public Const PU_DEL_DT As Integer = 10
        Public Const PO_IST As Integer = 11
        Public Const ROW_PROC As Integer = 12

    End Class

    Private Class SocSession

        Property zoneCdFromSess As String = ""
        Property zoneCdToSess As String = ""
        Property zoneGroupSess As String = ""
        Property soStoreSess As String = ""
        Property puDelSess As String = ""
        Property puDelDtFromSess As String = ""
        Property puDelDtToSess As String = ""
        Property docNumSess As String = ""
        Property pickDelStoreSess As String = ""
        Property custCdSess As String = ""
        Property confStatusCdSess As String = ""
        Property isPaidSess As String = ""

    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then
            zoneCdPopulate()
            populateZoneGrp()
            'storeCdPopulate()
            store_cd_populate()
            'populateConfirmationRage()
            CustTps.Value = "UNC" ' default to individual
            lbl_msg.Text = ""

            Dim NextDay As Date = Now        ' Current date and time.
            NextDay = NextDay.AddDays(0)

            DelPickDtFrom.Date = NextDay.ToShortDateString

            Dim NextWeek As Date = Now        ' Current date and time.
            NextWeek = NextDay.AddDays(3)

            DelPickDtTo.Date = NextWeek.ToShortDateString


            ' if returning from SOM reset the query info and then
            '   if the prior results are available redisplay them otherwise we may re-query if auto-query
            Dim returnFrom As String = Request("returnFrom")
            Dim didRebind = False
            If Not String.IsNullOrWhiteSpace(returnFrom) Then

                If "SOM".Equals(returnFrom) Then
                    ' get the request from Session
                    Dim req As New SocDtc
                    g_SocSes = Session("g_SocSes")

                    req.zoneCdFrom = g_SocSes.zoneCdFromSess
                    req.zoneCdTo = g_SocSes.zoneCdToSess
                    req.zoneGroup = g_SocSes.zoneGroupSess
                    req.soStore = g_SocSes.soStoreSess
                    req.puDel = g_SocSes.puDelSess
                    req.puDelDtFrom = g_SocSes.puDelDtFromSess
                    req.puDelDtTo = g_SocSes.puDelDtToSess
                    req.docNum = g_SocSes.docNumSess
                    req.pickDelStore = g_SocSes.pickDelStoreSess
                    req.custCd = g_SocSes.custCdSess
                    req.confStatusCd = g_SocSes.confStatusCdSess
                    req.isPaid = g_SocSes.isPaidSess

                    ZoneCdFrom.Value = req.zoneCdFrom
                    ZoneCdTo.Value = req.zoneCdTo
                    ZoneGroup.Value = req.zoneGroup
                    StoreCds.Value = req.soStore
                    DelPick.Value = req.puDel
                    If isNotEmpty(req.puDelDtFrom) Then
                        DelPickDtFrom.Date = req.puDelDtFrom
                    Else : DelPickDtFrom.Date = Nothing
                    End If
                    If isNotEmpty(req.puDelDtTo) Then
                        DelPickDtTo.Date = req.puDelDtTo
                    Else : DelPickDtTo.Date = Nothing
                    End If
                    txt_SalesOrder.Text = req.docNum
                    PickUpDelSt.Value = req.pickDelStore
                    txt_CustCd.Text = req.custCd
                    CustTps.Value = req.confStatusCd
                    isPaid.Value = req.isPaid

                    BindGrid(req)

                End If

            End If

        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Not SystemUtils.valLogin Then
            Response.Redirect("login.aspx")
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub SetSocSessionInfo()

        g_SocSes = New SocSession
        Session("socSess") = g_SocSes

    End Sub


    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_LookUp.Click

        Try

            lbl_msg.Text = ""

            Dim t0 As Date = Now
            Dim dec As Boolean = True
            Dim req As New SocDtc

            If isNotEmpty(ZoneGroup.Value) And (isNotEmpty(ZoneCdFrom.Value) Or isNotEmpty(ZoneCdTo.Value)) Then
                btn_Clear_Click(sender, e)
                'lbl_msg.Text = "Please select zone code range or delivery zone group."
                lbl_msg.Text = Resources.LibResources.Label956
                lbl_msg.Visible = True
                Exit Sub
            End If

            req.empCdOp = Session("EMP_CD")

            If isNotEmpty(StoreCds.Value) Then
                req.soStore = StoreCds.Value.ToString
            End If
            If isNotEmpty(ZoneCdFrom.Value) Then
                req.zoneCdFrom = ZoneCdFrom.Value.ToString
            End If
            If isNotEmpty(ZoneCdTo.Value) Then
                req.zoneCdTo = ZoneCdTo.Value.ToString
            End If
            If isNotEmpty(ZoneGroup.Value) Then
                req.zoneGroup = ZoneGroup.Value.ToString
            End If

            If isNotEmpty(DelPick.Value) Then
                req.puDel = DelPick.Value.ToString
            End If
            If isNotEmpty(DelPickDtFrom.Value) Then
                req.puDelDtFrom = DelPickDtFrom.Value.ToString
            End If
            If isNotEmpty(DelPickDtTo.Value) Then
                req.puDelDtTo = DelPickDtTo.Value.ToString
            End If
            req.docNum = txt_SalesOrder.Text
            If isNotEmpty(PickUpDelSt.Value) Then
                req.pickDelStore = PickUpDelSt.Value.ToString
            End If
            req.custCd = txt_CustCd.Text
            If Not "ALL".Equals(CustTps.Value.ToString) Then
                req.confStatusCd = CustTps.Value.ToString
            End If
            If isNotEmpty(isPaid.Value) Then
                req.isPaid = isPaid.Value.ToString
            End If

            BindGrid(req)
            ' Daniela handle exception
        Catch ex As Exception
            ' user security
            lbl_msg.Text = "System Error: " & ex.Message.ToString
            lbl_msg.Visible = True
        End Try
    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        lbl_msg.Text = ""

        ZoneCdFrom.Value = Nothing
        ZoneCdTo.Value = Nothing
        ZoneGroup.Value = Nothing
        StoreCds.Value = Nothing
        PickUpDelSt.Value = Nothing
        DelPick.Value = Nothing

        CustTps.Value = "UNC" ' default to individual

        Dim NextDay As Date = Now        ' Current date and time.
        NextDay = NextDay.AddDays(0)

        DelPickDtFrom.Date = NextDay.ToShortDateString

        Dim NextWeek As Date = Now        ' Current date and time.
        NextWeek = NextDay.AddDays(3)

        DelPickDtTo.Date = NextWeek.ToShortDateString

        txt_SalesOrder.Text = Nothing

        PickUpDelSt.Value = Nothing
        txt_CustCd.Text = Nothing
        isPaid.Value = Nothing

        Session("ORD_TBL") = Nothing
        DocGrid.DataSource = Nothing
        DocGrid.DataBind()

    End Sub

    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Sub store_cd_populate()

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        ' MCCL DB Nov 9, 2017

        Dim userType As Double = -1
        Dim SQL As String = ""

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
               "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
               "where a.user_id = :userType and " +
               "s.store_cd = b.store_cd and " +
               "a.store_grp_cd = b.store_grp_cd "
            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                SQL = SQL & " and '" & coCd & "' = s.co_cd "

            End If
        End If
        SQL = SQL & " order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            ' Parameters only if not Super User
            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType

            End If

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            With StoreCds
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            With PickUpDelSt
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            oAdp.Dispose()

        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Sub

    Private Sub storeCdPopulate()

        Dim datSet As DataSet = theSalesBiz.GetEmpStores(Session("emp_cd"))

        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

            With StoreCds
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "store_des"
                .DataBind()
            End With

            With PickUpDelSt
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "store_des"
                .DataBind()
            End With

        ElseIf Not IsNothing(Session("HOME_STORE_CD")) Then

            Dim storeRow As StoreData = theSalesBiz.GetStoreInfo(Session("HOME_STORE_CD"))
            'StoreCds.Items.Add(New ListItem(Session("HOME_STORE_CD") + " - " + storeRow.name, Session("HOME_STORE_CD")))  ' store name same format as StoreUtils.getEmpStores: store_cd || ' - ' || store_name AS store_des 
            StoreCds.Items.Add(New DevExpress.Web.ASPxEditors.ListEditItem(Session("HOME_STORE_CD") + " - " + storeRow.name, Session("HOME_STORE_CD"))) ' store name same format as StoreUtils.getEmpStores: store_cd || ' - ' || store_name AS store_des 
            StoreCds.DataBind()
        End If

    End Sub

    Public Sub zoneCdPopulate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetZones As OracleCommand
        cmdGetZones = DisposablesManager.BuildOracleCommand

        Dim stored_zone_cd As String
        Dim SQL As String = ""


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim coCd = Session("CO_CD")

        ' Nov 15 improve performance
        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "select zone_cd, des, zone_cd || ' - ' || z.des full_desc from zone z ORDER BY z.ZONE_CD "
        End If
        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT    z.zone_cd, z.des, z.zone_cd || ' - ' || z.des full_desc " +
                "FROM ZONE z, zone_grp zg " +
                "WHERE z.zone_cd = zg.zone_cd " +
                "and zg.store_grp_cd = (select cz.zone_grp from CO_GRP$ZONE_GRP cz where zg.store_grp_cd = cz.zone_grp and cz.co_cd = :coCd) " +
                "order by z.zone_cd "
        End If

        Try
            With cmdGetZones
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()

            ' Parameters only if not Super User
            If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

                cmdGetZones.Parameters.Add(":coCd", OracleType.VarChar)
                cmdGetZones.Parameters(":coCd").Value = coCd

            End If

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetZones)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If stored_zone_cd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("ZONE_CD")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(stored_zone_cd)
            End If

            With ZoneCdFrom
                .DataSource = ds
                .ValueField = "zone_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            With ZoneCdTo
                .DataSource = ds
                .ValueField = "zone_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Private Sub populateZoneGrp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        'Dim SQL = "SELECT distinct zone_grp from zone_grp order by zone_grp"

        ' MCCL added Nov 21

        Dim coCd As String = Session("CO_CD")
        Dim SQL As String = ""

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT distinct zone_grp from zone_grp order by zone_grp"
        End If
        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "select distinct zone_grp from zone_grp zg where zg.store_grp_cd = (select cz.zone_grp from CO_GRP$ZONE_GRP cz " +
                "where zg.store_grp_cd = cz.zone_grp and cz.co_cd = :coCd) order by zone_grp"
        End If

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()

            ' Parameters only if not Super User
            If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

                cmdGetCodes.Parameters.Add(":coCd", OracleType.VarChar)
                cmdGetCodes.Parameters(":coCd").Value = coCd

            End If

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With ZoneGroup
                .DataSource = ds
                .ValueField = "zone_grp"
                .TextField = "zone_grp"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub


    ''' <summary>
    ''' Sets the value of the special row process (highlight) column in the extended order row, based 
    ''' on the value in the pre-computed dictionary, if the del_doc_num is present there.  
    ''' In addition, if "mto" highlighting option and the process is "high_light" the further check for 
    ''' "not paid in full" is applied first.
    ''' 
    ''' this is the second and final part of a refactoring of the specRowProc, to allow the data to be fetched
    ''' in one query rather than one query per order. 
    ''' </summary>
    ''' <param name="ordRow">the extended order row</param>
    ''' <param name="specRows">sparse pre-computed dictionary of special process codes by order number </param>
    ''' <param name="col">the column name in the order row to set with the process value</param>
    ''' <remarks>MM-9935</remarks>
    Private Sub ApplySpecRowProc(ordRow As DataRow, specRows As Dictionary(Of String, String), col As String)
        Dim isMto As Boolean = (SysPmsConstants.SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS).Equals(SysPms.sbobRowProcess)
        Dim process As String = ""
        specRows.TryGetValue(ordRow("del_doc_num"), process)
        If isMto AndAlso SysPmsConstants.SBOB_ROW_HGHLGHT.Equals(process) Then
            If Not "N".Equals(ordRow("PaidInFull").ToString) Then
                process = ""
            End If
        End If
        ordRow(col) = process
    End Sub

    ''' <summary>
    ''' determines the process to apply to each row when binding the data to the grid, 
    ''' for instance highlighting the order row if the document has a specific order sort code; 
    ''' parameter in GP_PARMS, SBOB_SPECIAL_ROW_PROCESS, value indicates what the conditions are to do the process.
    ''' 
    ''' this is the main part of a refactoring of the specRowProc, to allow the data to be fetched in one query
    ''' rather than one query per order. 
    ''' </summary>
    ''' 
    ''' <param name="conn">caller's oracle connection</param>
    ''' <param name="docNums">list of order numbers</param>
    ''' <param name="st_cd">store code that the list was filtered on</param>
    ''' <returns>returns a SPARSE dictionary of del_doc_num>>process (only HIGH_LIGHT at this time) for all (only) impacted rows</returns>
    ''' <remarks>
    ''' ?This could have an option input that indicated what special processing to do; initially it is only highlighting
    ''' In the case of "mto" highlighting an additional test for "paid in full" needs to be applied later
    ''' MM-9935
    ''' </remarks>
    ''' 
    Private Function FindSpecRowProc(conn As OracleConnection, ByVal docNums As List(Of String), st_cd As String) As Dictionary(Of String, String)

        Dim rowSpecs As New Dictionary(Of String, String)
        Dim process As String = ""
        Dim procAndCond As String = SysPms.sbobRowProcess

        If (Not String.IsNullOrEmpty(procAndCond)) Then

            Dim datSet As New DataSet
            Dim sqlStmtSb As New StringBuilder
            Dim localNumberOfBoundParms = NormalNumberOfBoundParameters
            Dim isMto As Boolean = (SysPmsConstants.SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS).Equals(procAndCond)

            If isMto Then
                ' there is some evidence, sporadically, that the MTO query might get very large in SGA, 
                ' we could try to mitigate that by using fewer parameters
                ' on further review this does not appear to be necessary...
                'localNumberOfBoundParms = LimitedNumberOfBoundParameters
            End If

            Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

                Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, localNumberOfBoundParms)

                If (SysPmsConstants.SBOB_ROW_HGHLGHT_ORDSRT).Equals(procAndCond.Substring(0, 16)) Then  ' if the first part = HIGHLIGHT
                    Dim lastUnder As Integer = procAndCond.LastIndexOf("_")
                    Dim srtCd As String = procAndCond.Substring((lastUnder + 1), (procAndCond.Length - lastUnder - 1))
                    If SystemUtils.isNotEmpty(srtCd) Then
                        sqlStmtSb.Append("SELECT del_Doc_num FROM so WHERE del_Doc_num IN (").Append(inlist).Append(
                            ") AND ord_srt_cd = :srtCd ")
                        cmd.Parameters.Add(":srtCd", OracleType.VarChar)
                        cmd.Parameters(":srtCd").Value = srtCd
                    End If
                ElseIf (SysPmsConstants.SBOB_ROW_HGHLGHT_SPECORD_DROPPED).Equals(procAndCond) Then
                    If SystemUtils.isNotEmpty(SysPms.specialOrderDropCd) Then
                        sqlStmtSb.Append("SELECT sl.del_Doc_num FROM itm, so_ln sl ").Append(
                            "WHERE itm.itm_cd = sl.itm_cd AND sl.del_Doc_num IN (").Append(inlist).Append(
                            ") AND itm.drop_cd = :dropCd AND sl.void_flag != 'Y' ")
                        cmd.Parameters.Add(":dropCd", OracleType.VarChar)
                        cmd.Parameters(":dropCd").Value = SysPms.specialOrderDropCd
                    End If
                ElseIf isMto Then
                    sqlStmtSb.Append(" SELECT trunc(sysdate) systemdate, max(PU_DEL_DT) DelDate,Sum(ITM_SRT_CD)  MTO, DEL_DOC_NUM,  max(TotalLeadDays) TotalLeadDays")
                    sqlStmtSb.Append(" from( SELECT ")
                    sqlStmtSb.Append(" SO.PU_DEL_DT,case when upper(srt.MTO) = 'Y' then 1 else 0 end ITM_SRT_CD,")
                    sqlStmtSb.Append(" SO.DEL_DOC_NUM,bt_adv_res.get_all_lead_days(i.itm_Cd, str.store_Cd, z.zone_Cd) TotalLeadDays ")
                    sqlStmtSb.Append(" FROM STORE STR, SO,so_ln sl,itm i, ITM$ITM_SRT ISRT,itm_srt srt, ZONE z ")
                    sqlStmtSb.Append(" WHERE ")
                    sqlStmtSb.Append(" SO.SO_STORE_CD = STR.STORE_CD and SO.SO_STORE_CD = :stCd ")
                    sqlStmtSb.Append(" and so.del_doc_num = sl.del_doc_num ")
                    sqlStmtSb.Append(" and sl.itm_cd = i.itm_cd and sl.void_flag != 'Y' ")
                    sqlStmtSb.Append(" and ISRT.ITM_CD = I.ITM_CD and srt.ITM_SRT_CD = isrt.ITM_SRT_CD ")
                    sqlStmtSb.Append(" and SO.SHIP_TO_ZONE_CD = Z.ZONE_CD ")
                    sqlStmtSb.Append(" AND SO.DEL_DOC_NUM  IN (").Append(inlist).Append(") ")
                    sqlStmtSb.Append(" ) t ")
                    sqlStmtSb.Append(" Group by DEL_DOC_NUM ")
                    cmd.Parameters.Add(":stCd", OracleType.VarChar)
                    cmd.Parameters(":stCd").Value = st_cd
                End If

                If sqlStmtSb.Length > 0 Then
                    cmd.CommandText = sqlStmtSb.ToString
                    cmd.Connection = conn
                    oDatAdap.SelectCommand = cmd
                    For ix = 0 To docNums.Count - 1 Step localNumberOfBoundParms
                        SystemUtils.FillMore(docNums, ":ddn", cmd, localNumberOfBoundParms, ix)
                        oDatAdap.Fill(datSet)
                        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
                            For Each tmRow In datSet.Tables(0).Rows
                                If isMto Then
                                    If Convert.ToInt32(tmRow("MTO").ToString) > 0 Then
                                        If Convert.ToDateTime(tmRow("DelDate")) <= Convert.ToDateTime(tmRow("systemdate").ToString).AddDays(Convert.ToDouble(tmRow("TotalLeadDays").ToString)) Then
                                            ' MM-9935 with this refactoring we have to defer this "PaidInFull" check until later If "N".Equals(soRow("PaidInFull").ToString) AndAlso 
                                            ' so the result is "provisional" at this point
                                            rowSpecs.Add(tmRow("DEL_DOC_NUM"), SysPmsConstants.SBOB_ROW_HGHLGHT)
                                        End If
                                    End If
                                Else
                                    rowSpecs.Add(tmRow("DEL_DOC_NUM"), SysPmsConstants.SBOB_ROW_HGHLGHT)
                                End If
                            Next
                        End If
                        datSet.Clear()
                    Next
                End If
            End Using
            If datSet IsNot Nothing Then
                datSet.Dispose()
            End If
        End If

        Return rowSpecs

    End Function

    ''' <summary>
    ''' returns the order lines for the del_docs, sum by itm_cd and id_num of the quantity needed
    ''' </summary>
    ''' <param name="conn">caller's connection to oracle</param>
    ''' <param name="docNums">list of all SO numbers of interest</param>
    ''' <returns>ordered set of summed lines</returns>
    ''' <remarks></remarks>
    Private Shared Function getItmQtys(conn As OracleConnection, ByVal docNums As List(Of String)) As DataTable
        'Returns the order line information

        Dim moreResults As New DataSet
        Dim allresults As DataSet = Nothing

        If docNums IsNot Nothing AndAlso docNums.Count > 0 Then

            Dim sqlStmtSb As New StringBuilder
            ' TODO - DSA - if need to exclude tw, may ned to change test of fill_dt to PIcked flag = F
            ' TODO - may need to add in handling of frame SKUs (by id_num)

            Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter(cmd)

                Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, NormalNumberOfBoundParameters)
                sqlStmtSb.Append("SELECT sl.del_doc_num, sl.itm_cd, SUM(qty) AS qty_ord, SUM(CASE WHEN fill_dt IS NOT NULL THEN qty ELSE 0 END) AS qty_res  ").Append(
                                "FROM itm i, so_ln sl ").Append(
                                "WHERE sl.del_doc_num IN (").Append(inlist).Append(") AND i.itm_cd = sl.itm_cd  AND i.inventory = 'Y' AND sl.void_flag != 'Y' ").Append(
                                "GROUP BY sl.del_doc_num, sl.itm_cd ORDER BY sl.del_doc_num, sl.itm_cd ")
                cmd.Connection = conn
                cmd.CommandText = sqlStmtSb.ToString
                For ix As Integer = 0 To docNums.Count - 1 Step NormalNumberOfBoundParameters
                    SystemUtils.FillMore(docNums, ":ddn", cmd, NormalNumberOfBoundParameters, ix)
                    moreResults = New DataSet()
                    oDatAdap.Fill(moreResults)
                    If SystemUtils.dataSetHasRows(moreResults) AndAlso SystemUtils.dataRowIsNotEmpty(moreResults.Tables(0).Rows(0)) Then
                        If allresults Is Nothing Then
                            allresults = moreResults
                        Else
                            allresults.Tables(0).Merge(moreResults.Tables(0))
                            moreResults.Dispose()
                        End If
                    End If
                Next
            End Using
        End If

        If allresults IsNot Nothing AndAlso SystemUtils.dataSetHasRows(allresults) AndAlso SystemUtils.dataRowIsNotEmpty(allresults.Tables(0).Rows(0)) Then
            Return allresults.Tables(0)
        Else
            Return New DataTable
        End If

    End Function

    ''' <summary>
    ''' extract the rows that match some criterion from a datatable
    ''' </summary>
    ''' <param name="t">the complete table </param>
    ''' <param name="col">name of column to filter on</param>
    ''' <param name="val">value to filter on</param>
    ''' <returns>a datatable with just the qualifying rows if any</returns>
    ''' <remarks></remarks>
    Private Function extractRows(t As DataTable, col As String, val As String) As DataTable
        Dim dv As New DataView(t, col + " = '" + val + "'", Nothing, DataViewRowState.CurrentRows)
        Dim rv As DataTable = dv.ToTable()
        dv.Dispose()
        Return rv
    End Function

    ''' <summary>
    ''' looks at available, on-hand inventory using store priority fill (SPFM) or zone priority fill (ZPFM)
    ''' if document is linked to an IST or PO (with open quantity to receive) (but not above) then returns an 'N'
    ''' if document is not linked to either an IST or PO then returns blank 
    ''' </summary>
    ''' <param name="docNum"></param>
    ''' <param name="puDel"></param>
    ''' <param name="zoneCd">this most likely is ship_to_zone_cd but can be any other zone code</param>
    ''' <param name="storeCd">this most likely is pu_del_Store_cd but can be any other store code</param>
    ''' <param name="summedLines">pre-queried table of so lines summed by doc num and sku (MM-9935)</param>
    ''' <returns>N – The order cannot be filled; P – The order can be partially filled; F – The order can be fully filled</returns>
    ''' <remarks></remarks>
    Private Function setInvFill(ByVal docNum As String, ByVal puDel As String, ByVal zoneCd As String, ByVal storeCd As String, summedLines As DataTable) As String

        Dim invFill As String = "N"

        ' extract the inventory type list from so_ln and sum by itm_cd
        Dim soLnItmsTbl As DataTable = extractRows(summedLines, "DEL_DOC_NUM", docNum)

        If soLnItmsTbl IsNot Nothing AndAlso soLnItmsTbl.Rows.Count > 0 Then

            Dim soLnItmRow As DataRow
            Dim priFillInv As DataSet
            Dim priFillrow As DataRow
            Dim remQtyNeeded As Double
            Dim itmInvCnt As Double = soLnItmsTbl.Rows.Count
            Dim itmFilledCnt As Double = 0  ' count for the number of itms filled; if this equals the itmCnt when done looking for inv, then all doc is filled
            Dim pfIndx As Integer = 0
            ' for testing purposes
            'Dim debug As String = ""

            'loop thru the list of summed by qty inventory itms on the so_lns
            For Each soLnItmRow In soLnItmsTbl.Rows

                If soLnItmRow("qty_res") > 0 Then

                    invFill = "P" ' if any line already has reserved, then the doc is partially filled 
                End If
                remQtyNeeded = soLnItmRow("qty_ord") - soLnItmRow("qty_res")
                'debug = soLnItmRow("itm_cd").ToString + " " + soLnItmRow("qty_ord").ToString + " - " + soLnItmRow("qty_res").ToString & " = " & remQtyNeeded

                If remQtyNeeded <= 0 Then

                    itmFilledCnt = itmFilledCnt + 1

                Else
                    priFillInv = theInvBiz.GetInvPriFill(soLnItmRow("Itm_cd"), puDel, zoneCd, storeCd, "N")

                    If SystemUtils.dataSetHasRows(priFillInv) AndAlso SystemUtils.dataRowIsNotEmpty(priFillInv.Tables(0).Rows(0)) Then

                        pfIndx = 0
                        Do Until pfIndx > priFillInv.Tables(0).Rows.Count - 1 OrElse remQtyNeeded <= 0

                            priFillrow = priFillInv.Tables(0).Rows(pfIndx)
                            ' store, loc, qty returned from api
                            'debug = soLnItmRow("itm_cd").ToString + " " + priFillrow("store_cd").ToString + " " + priFillrow("loc_cd").ToString & " " & priFillrow("qty")

                            If priFillrow("qty") > 0 Then

                                invFill = "P" ' if any line has any qty avail, then the doc is partially filled
                            End If

                            remQtyNeeded = remQtyNeeded - priFillrow("qty")
                            pfIndx = pfIndx + 1
                        Loop
                        If remQtyNeeded <= 0 Then

                            itmFilledCnt = itmFilledCnt + 1
                        End If
                    End If
                End If
            Next

            If itmFilledCnt = itmInvCnt Then
                invFill = "F"  ' all line counts are filled
            End If
        End If

        Return invFill

    End Function

    Sub BindGrid(ByRef req As SocDtc)

        Dim ords As DataSet = SocLookupOrders(req, 2)
        Dim countOrders As Integer = 0
        If Not ords Is Nothing Then
            countOrders = ords.Tables(0).Rows.Count
        End If
        If countOrders >= 200 Then
            If isEmpty(lbl_msg.Text) Then
                'lbl_msg.Text = "No results were found using your search criteria."
                lbl_msg.Text = Resources.LibResources.Label957
            End If
            lbl_msg.Visible = True
            Exit Sub
        End If

        If SystemUtils.dataSetHasRows(ords) Then
            Dim grandTot As Double = 0
            Dim ordsTbl As DataTable = ords.Tables(0)

            ordsTbl.Columns.Add("row_proc")  ' used to allow special processing on the row, for instance highlighting
            lbl_msg.Text = ""

            DocGrid.DataSource = ordsTbl
            DocGrid.DataBind()

            ' Save request criteria in session to return from SOM

            g_SocSes = New SocSession

            If isNotEmpty(req.zoneCdFrom) Then
                g_SocSes.ZoneCdFromSess = req.zoneCdFrom
            End If
            If isNotEmpty(req.zoneCdTo) Then
                g_SocSes.ZoneCdToSess = req.zoneCdTo
            End If
            If isNotEmpty(req.zoneGroup) Then
                g_SocSes.ZoneGroupSess = req.zoneGroup
            End If
            If isNotEmpty(req.soStore) Then
                g_SocSes.soStoreSess = req.soStore
            End If
            If isNotEmpty(req.puDel) Then
                g_SocSes.puDelSess = req.puDel
            End If
            If isNotEmpty(req.puDelDtFrom) Then
                g_SocSes.puDelDtFromSess = req.puDelDtFrom
            End If
            If isNotEmpty(req.puDelDtTo) Then
                g_SocSes.puDelDtToSess = req.puDelDtTo
            End If
            If isNotEmpty(req.pickDelStore) Then
                g_SocSes.pickDelStoreSess = req.pickDelStore
            End If
            If isNotEmpty(req.docNum) Then
                g_SocSes.docNumSess = req.docNum
            End If
            If isNotEmpty(req.custCd) Then
                g_SocSes.custCdSess = req.custCd
            End If
            If isNotEmpty(req.confStatusCd) Then
                g_SocSes.confStatusCdSess = req.confStatusCd
            End If
            If isNotEmpty(req.isPaid) Then
                g_SocSes.isPaidSess = req.isPaid
            End If

            ' Save the request in Session to be used returning from SOM+
            Session("g_SocSes") = g_SocSes

        Else

            If isEmpty(lbl_msg.Text) Then
                'lbl_msg.Text = "No results were found using your search criteria."
                lbl_msg.Text = Resources.LibResources.Label953
            End If
            lbl_msg.Visible = True

            DocGrid.DataSource = Nothing
            DocGrid.DataBind()
        End If



    End Sub

    ''' <summary>
    ''' Display or clear the "results as of" message. 
    ''' </summary>
    ''' <remarks>MM-9936</remarks>
    'Private Sub showAsOfMsg()
    '    If g_SbobSes Is Nothing OrElse g_SbobSes.qryAsOf.Ticks = 0 Then
    '        lbl_asOf.Text = ""
    '        lbl_asOf.Visible = False
    '    Else
    '        Dim tzn As String = IIf(TimeZone.CurrentTimeZone.IsDaylightSavingTime(g_SbobSes.qryAsOf), TimeZone.CurrentTimeZone.DaylightName, TimeZone.CurrentTimeZone.StandardName)
    '        Dim tzns() As String = tzn.Split(" ".ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
    '        Dim tz As String = ""
    '        For Each s As String In tzns
    '            tz += s.Substring(0, 1)
    '        Next
    '        lbl_asOf.Text = String.Format("Query results as of: {0:hh:mm:ss tt} {1} {2:MM/dd/yy}", g_SbobSes.qryAsOf, tz, g_SbobSes.qryAsOf)
    '        lbl_asOf.Visible = True
    '    End If
    'End Sub

    ''' <summary>
    ''' Locate and the (first) row in the given DataTable where the value in the given column 
    ''' is equal to the given value.  This enables pre-query in cases where the results needed 
    ''' are more than a single value.
    ''' </summary>
    ''' <param name="tbl"></param>
    ''' <param name="colname"></param>
    ''' <param name="keyval"></param>
    ''' <returns>first matching row or nothing</returns>
    ''' <remarks>MM-9935</remarks>
    Private Function MatchRow(tbl As DataTable, colname As String, keyval As String) As DataRow
        Dim rv As DataRow = Nothing
        For i As Integer = 0 To tbl.Rows.Count - 1
            If tbl.Rows(i)(colname) = keyval Then
                rv = tbl.Rows(i)
                Exit For
            End If
        Next
        Return rv
    End Function

    Protected Sub DocGrid_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)

        'Retrieve the table from the session object.
        Dim datTbl = TryCast(Session("ORD_TBL"), DataTable)

        If datTbl IsNot Nothing Then

            datTbl.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            DocGrid.DataSource = Session("ORD_TBL")
            DocGrid.DataBind()
        End If

    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"
                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection
    End Function

    Protected Sub OnRowDataBound(ByVal source As Object, ByVal e As GridViewRowEventArgs)

        Dim IsValidateFinance As String = "N"        'oct 5, 2017 - 3250 - mariam


        If (e.Row.RowType = DataControlRowType.DataRow) Then

            'Find the DropDownList in the Row
            Dim ddConfStatus As DropDownList = CType(e.Row.FindControl("ddConfStatus"), DropDownList)
            ddConfStatus.DataTextField = "CONF_STAT_CD"
            ddConfStatus.DataValueField = "CONF_STAT_CD"
            ddConfStatus.DataBind()

            'Add Default Item in the DropDownList
            'Select the Country of Customer in DropDownList
            Dim currConfSt As String = CType(e.Row.FindControl("lblConfStatus"), Label).Text
            Dim availInv As String
            Dim res As String
            Dim confUpdateFlag As String

            If (isNotEmpty(currConfSt)) Then
                ddConfStatus.Items.FindByValue(currConfSt).Selected = True
            End If
            If currConfSt = "INP" Then
                ddConfStatus.Enabled = False
            End If

            Dim ordType As String = e.Row.Cells(4).Text
            Dim link As HyperLink = TryCast(e.Row.Cells(0).Controls(0), HyperLink)
            Dim delDocNum As String
            If link IsNot Nothing Then
                ' It worked, so you can safely access the Text property '
                delDocNum = link.Text
            Else
                Exit Sub
                ' Otherwise the cast failed, handle accordingly '
            End If

            If ordType = "CRM" Then
                e.Row.Cells(12).Text = "Y"
            Else
                e.Row.Cells(12).Text = GetReserved(delDocNum)
            End If
            res = e.Row.Cells(12).Text

            If e.Row.Cells(13).Text = "N" Then
                availInv = GetInvAvail(delDocNum)
                If isNotEmpty(availInv) Then
                    e.Row.Cells(14).Text = availInv
                End If
            End If

            ' Check if the COnfirmation Status can be changed
            'The confirmation code field (SO. CONF_STAT_CD) is the only updatable field on this form.  
            'The User can change the confirmation code to 'CON’firm status if the order is RES’d = ‘Y’ fully reserved, 
            'the pickup / delivery date Is within the confirmation period*.  And the delivery document Is considered fully paid “Paid Del Doc” = 'Y’.  
            'If the Then order Is 'CON’firmed and user wants to change it to an ‘UNC’onfirmed then this should be allowed 
            'as long as order has not been trucked (TKM'd).  If order is ‘INP’in process or any other status then no changes are allowed.  
            'Ensure that a SO_CMNT audit record Is inserted when the confirmation status Is changed And saved recording the emp_cd who did the change.  

            Dim isPaid As String = e.Row.Cells(16).Text
            Dim isTKM As String = e.Row.Cells(13).Text

            'change from Unconfirmed to Confirm
            If currConfSt = "UNC" Then
                Dim confPeriodFlag As String = CheckConfirmationPeriod(e.Row.Cells(3).Text, e.Row.Cells(11).Text, "CON", currConfSt)
                ' Dec 6 DB add isPaid parameter
                IsValidateFinance = ValidateFinance(delDocNum, currConfSt, isPaid)                        'oct 5, 2017 - 3250 - mariam
                If (res = "Y" And confPeriodFlag = "Y" And isPaid = "Y") Or (res = "Y" And confPeriodFlag = "Y" And IsValidateFinance = "Y") Then
                    'If res = "Y" And confPeriodFlag = "Y" And isPaid = "Y" Then
                    ddConfStatus.Enabled = True
                Else
                    ddConfStatus.Enabled = False
                    e.Row.Cells(20).Enabled = False
                End If
            End If

            If currConfSt = "CON" Then
                If isTKM = "N" Then
                    ddConfStatus.Enabled = True
                Else
                    ddConfStatus.Enabled = False
                    e.Row.Cells(20).Enabled = False
                End If
            End If

            Dim btn As ImageButton = CType(e.Row.FindControl("AddressDetails"), ImageButton)
            Dim cmndArg = btn.CommandArgument
            Dim cmndName = btn.CommandName

            If cmndName = "SHIP2" And isEmpty(cmndArg) Then
                btn.Visible = False
            End If

        End If

    End Sub

    Protected Sub DocGrid_DataBinding(sender As Object, e As EventArgs) Handles DocGrid.DataBinding
        Dim test As String = e.ToString
    End Sub

    Public Class SocDtc
        '  Selection requirements beyond the basic returned set
        ' Operator employee code for store based security 
        Property empCdOp As String = ""

        '  Order Query criteria, the non-null values will be used to setup the WHERE clause
        '     if the entry includes a '%', then the clause will be 'LIKE'; if not, then '='
        Property zoneCdFrom As String = ""
        Property zoneCdTo As String = ""
        Property zoneGroup As String = ""
        Property soStore As String = ""
        Property puDel As String = ""
        Property puDelDtFrom As String = ""
        Property puDelDtTo As String = ""
        Property docNum As String = ""
        Property pickDelStore As String = ""
        Property custCd As String = ""
        Property confStatusCd As String = ""
        Property isPaid As String = ""

        ''' End criteria

    End Class

    Public Function SocLookupOrders(ByVal req As SocDtc, qoption As Integer) As DataSet

        Dim soDatSet As DataSet

        If IsNothing(req) OrElse (isEmpty(req.zoneCdFrom) AndAlso isEmpty(req.zoneCdTo) AndAlso isEmpty(req.zoneGroup) AndAlso
                                  isEmpty(req.soStore) AndAlso isEmpty(req.docNum) AndAlso isEmpty(req.pickDelStore) AndAlso
                                  isEmpty(req.custCd)) Then
            ' DO NOTHING - NO INPUT
            'lbl_msg.Text = "Please enter additional search criteria."
            lbl_msg.Text = Resources.LibResources.Label954
            lbl_msg.Visible = True
            Return soDatSet
        End If

        Dim sqlSelStmtSb As New StringBuilder
        Dim sqlStmtSb As New StringBuilder
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        'Coumns
        '       DEL_DOC_NUM, SHIP_TO_ZONE_CD, ORD_TP_CD, CUST_CD, CUST_NAME, SHIP_TO_H_PHONE, SHIP_TO_ADDRESS,
        ' MULT_ORDS, PICK_DEL_FLAG, PU_DEL_DT, RESD, TKM, INV_AVAIL, SPEC_ATTR, PAID_DEL_DOC, CONF_STAT_CD

        sqlSelStmtSb.Append("select * from ( ")
        sqlSelStmtSb.Append("select SO.DEL_DOC_NUM,SO.SHIP_TO_ZONE_CD ZONE_CD, ")
        sqlSelStmtSb.Append("SO.ORD_TP_CD,SO.CUST_CD, ")
        sqlSelStmtSb.Append("nvl(SO.SHIP_TO_CORP, SO.SHIP_TO_L_NAME|| ', ' ||SO.SHIP_TO_F_NAME) CUST_NAME, ")
        sqlSelStmtSb.Append("SO.SHIP_TO_H_PHONE, ")
        sqlSelStmtSb.Append("(SHIP_TO_ADDR1|| ', ' ||SHIP_TO_CITY|| ' ' ||SHIP_TO_ZIP_CD) || ' ' || SHIP_TO_ST_CD || ' ' SHIP_TO_ADDRESS, ")
        sqlSelStmtSb.Append("SHIP_TO_ADDR2, ")
        sqlSelStmtSb.Append("  (select decode(count(*), 0, 'N', 'Y') from so a where a.ORD_TP_CD IN ('SAL', 'CRM') AND a.STAT_CD = 'O' ")
        sqlSelStmtSb.Append(" and a.DEL_DOC_NUM <> SO.DEL_DOC_NUM and a.CUST_CD = SO.CUST_CD) MULT_ORDS, ")
        sqlSelStmtSb.Append("SO.PU_DEL PICK_DEL_FLAG, ")
        sqlSelStmtSb.Append("SO.PU_DEL_DT, ")
        sqlSelStmtSb.Append("'Y' RES, ")
        sqlSelStmtSb.Append("(select decode(count(*), 0, 'N','Y')  from TRK_STOP ")
        sqlSelStmtSb.Append("where del_doc_num = SO.DEL_DOC_NUM) TKM, ")
        sqlSelStmtSb.Append("'Y' INV_AVAIL, ")
        sqlSelStmtSb.Append("(select decode(count(*), 0, 'N', 'Y') SpecAttr from so_ln a, ITM$ITM_SRT i, ZONE$PRI c ")
        sqlSelStmtSb.Append("where a.del_doc_num = so.del_doc_num and ")
        sqlSelStmtSb.Append("a.itm_cd = i.itm_cd and ")
        sqlSelStmtSb.Append("c.ZONE_CD = so.SHIP_TO_ZONE_CD and ")
        sqlSelStmtSb.Append("a.VOID_FLAG <> 'Y' and (a.OUT_CD is not null or i.ITM_SRT_CD = 'RTA' or  i.ITM_SRT_CD = MTO_AUTOPO_PPKG.GET_REGIOn(c.BEG_STORE_CD) || 'MT')) ")
        sqlSelStmtSb.Append(" SPEC_ATTR, ")
        sqlSelStmtSb.Append("custom_paid.is_order_paid_in_full(so.ord_tp_cd, so.del_doc_num, so.cust_cd) PAID_DEL_DOC, ")
        sqlSelStmtSb.Append("nvl(SO.CONF_STAT_CD,'UNC') CONF_STAT_CD, SO.SO_WR_DT, SO.SO_STORE_CD, SO.SO_SEQ_NUM ")
        sqlSelStmtSb.Append("FROM SO ")
        sqlSelStmtSb.Append("WHERE rownum <= 200 and SO.ORD_TP_CD in ('SAL', 'CRM') and ")
        sqlSelStmtSb.Append("SO.STAT_CD = 'O' ")

        If req.confStatusCd.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND nvl(SO.CONF_STAT_CD, 'UNC') = :CONFSTATUS ")
            cmd.Parameters.Add(":CONFSTATUS", OracleType.VarChar)
            cmd.Parameters(":CONFSTATUS").Value = UCase(req.confStatusCd)
        End If
        If req.zoneCdFrom.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.SHIP_TO_ZONE_CD >= :ZONE_CD_FROM ")
            cmd.Parameters.Add(":ZONE_CD_FROM", OracleType.VarChar)
            cmd.Parameters(":ZONE_CD_FROM").Value = UCase(req.zoneCdFrom)
        End If
        If req.zoneCdTo.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.SHIP_TO_ZONE_CD <= :ZONE_CD_TO ")
            cmd.Parameters.Add(":ZONE_CD_TO", OracleType.VarChar)
            cmd.Parameters(":ZONE_CD_TO").Value = UCase(req.zoneCdTo)
        End If
        If req.zoneGroup.Trim().isNotEmpty Then
            sqlStmtSb.Append(" and SO.SHIP_TO_ZONE_CD in (select zone_cd from zone_grp g where zone_grp = :ZONE_GRP and SO.SHIP_TO_ZONE_CD = g.ZONE_CD) ")
            cmd.Parameters.Add(":ZONE_GRP", OracleType.VarChar)
            cmd.Parameters(":ZONE_GRP").Value = UCase(req.zoneGroup)
        End If
        If req.soStore.isNotEmpty Then
            sqlStmtSb.Append(" AND SO.SO_STORE_CD ").Append(equalOrLike(req.soStore)).Append(" :soStore ")
            cmd.Parameters.Add(":soStore", OracleType.VarChar)
            cmd.Parameters(":soStore").Value = UCase(req.soStore)
        End If
        If req.puDel.isNotEmpty Then
            sqlStmtSb.Append(" AND SO.PU_DEL ").Append(equalOrLike(req.puDel)).Append(" :puDel ")
            cmd.Parameters.Add(":puDel", OracleType.VarChar)
            cmd.Parameters(":puDel").Value = UCase(req.puDel)
        End If
        If req.docNum.isNotEmpty Then
            sqlStmtSb.Append(" AND SO.DEL_DOC_NUM ").Append(equalOrLike(req.docNum)).Append(" :docNum ")
            cmd.Parameters.Add(":docNum", OracleType.VarChar)
            cmd.Parameters(":docNum").Value = UCase(req.docNum)
        End If
        If req.pickDelStore.isNotEmpty Then
            sqlStmtSb.Append(" AND SO.PU_DEL_STORE_CD ").Append(equalOrLike(req.soStore)).Append(" :pickDelStore ")
            cmd.Parameters.Add(":pickDelStore", OracleType.VarChar)
            cmd.Parameters(":pickDelStore").Value = UCase(req.pickDelStore)
        End If
        If req.custCd.isNotEmpty Then
            sqlStmtSb.Append(" AND SO.CUST_CD ").Append(equalOrLike(req.custCd)).Append(" :custCd ")
            cmd.Parameters.Add(":custCd", OracleType.VarChar)
            cmd.Parameters(":custCd").Value = UCase(req.custCd)
        End If
        If req.puDelDtFrom.isNotEmpty AndAlso IsDate(req.puDelDtFrom) Then
            sqlStmtSb.Append(" AND SO.PU_DEL_DT >= trunc(:puDelDtFrom) ")
            cmd.Parameters.Add(":puDelDtFrom", OracleType.DateTime)
            cmd.Parameters(":puDelDtFrom").Value = UCase(req.puDelDtFrom)
        End If
        If req.puDelDtTo.isNotEmpty AndAlso IsDate(req.puDelDtTo) Then
            sqlStmtSb.Append(" AND SO.PU_DEL_DT <= trunc(:puDelDtTo) ")
            cmd.Parameters.Add(":puDelDtTo", OracleType.DateTime)
            cmd.Parameters(":puDelDtTo").Value = UCase(req.puDelDtTo)
        End If

        sqlStmtSb.Append(" ) ")

        If req.isPaid.isNotEmpty Then
            sqlStmtSb.Append(" WHERE PAID_DEL_DOC = :isPaid  ")
            cmd.Parameters.Add(":isPaid", OracleType.VarChar)
            cmd.Parameters(":isPaid").Value = UCase(req.isPaid)
        Else
            sqlStmtSb.Append(" WHERE 1=1  ")
        End If

        ' MCCL DB Nov 9, 2017

        Dim userType As Double = -1
        Dim SQL As String = ""

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        'sqlStmtSb.Append(" AND STD_MULTI_CO2.ISVALIDSTR6('" & Session("EMP_INIT") & "',SO_STORE_CD)='Y'  ") ' DB Oct 26 moved outside performance

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then
            sqlStmtSb.Append(" AND SO_STORE_CD in (select s.store_cd from store s, MCCL_STORE_GROUP a, store_grp$store b ")
            sqlStmtSb.Append("where SO_STORE_CD = s.store_cd and a.store_grp_cd = b.store_grp_cd and ")
            sqlStmtSb.Append("s.store_cd = b.store_cd and a.user_id = :userType ")

            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                SQL = SQL & " and '" & coCd & "' = s.co_cd "
            End If
            sqlStmtSb.Append(" ) ")
            cmd.Parameters.Add(":userType", OracleType.VarChar)
            cmd.Parameters(":userType").Value = userType
        End If
        sqlStmtSb.Append(" ORDER BY PU_DEL_DT, CUST_CD ")

        soDatSet = New DataSet
        Try
            cmd.CommandText = UCase(sqlSelStmtSb.ToString + sqlStmtSb.ToString)

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                cmd.Connection = conn
                oAdp.Fill(soDatSet)

            End Using
        Finally
            cmd.Dispose()
        End Try


        Return soDatSet
    End Function


    ' GridView.RowUpdating Event 
    Protected Sub DocGrid_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        ' Make the GridView control into edit mode  
        ' for the selected row.  
        'Update 

        Dim ddConfStat As DropDownList = DirectCast(DocGrid.Rows(e.RowIndex).FindControl("ddConfStatus"), DropDownList)
        Dim fld = ddConfStat.DataValueField
        ddConfStat.DataTextField = fld

        Dim docNum As String = DirectCast(DocGrid.Rows(e.RowIndex) _
                                     .FindControl("del_doc_num"), TextBox).Text

        Dim confSt As String = DirectCast(DocGrid.Rows(e.RowIndex) _
                                     .FindControl("ddConfStatus"), TextBox).Text

        Dim ds As New DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim cmd As OracleCommand
        cmd = DisposablesManager.BuildOracleCommand

        cmd.CommandText = "update ..."

        DocGrid.EditIndex = -1
        DocGrid.DataSource = ds
        DocGrid.DataBind()


    End Sub


    Protected Sub DocGrid_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        DocGrid.PageIndex = e.NewPageIndex
        'Bind data to the GridView control.
        DataBind()
    End Sub

    Protected Sub DocGrid_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        'Set the edit index.

        DocGrid.EditIndex = e.NewEditIndex
        Dim check As String = "Y"
        'Dim ddConfStat As DropDownList = e. .Row.FindControl("ddConfStatus")
        Dim ddConfStat As DropDownList = DirectCast(DocGrid.SelectedRow.FindControl("ddConfStatus"), DropDownList)
        ddConfStat.Enabled = True

        'Dim statCd As DropDownList = DirectCast(DocGrid.SelectedRow.FindControl("ddConfStatus"), DropDownList)
        'statCd.Enabled = True
        DataBind()
    End Sub

    Sub DocGrid_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DocGrid.SelectedIndexChanged

        lbl_msg.Text = ""

        ' Retrieve the company name from the appropriate cell.
        DocGrid.SelectedRow.Cells(1).Visible = True

        Dim delDocNum As String

        Dim statCd As DropDownList = DirectCast(DocGrid.SelectedRow.FindControl("ddConfStatus"), DropDownList)

        Dim row As GridViewRow = DocGrid.SelectedRow
        Dim delFlag As String = row.Cells(10).Text
        Dim homePhone As String = row.Cells(7).Text

        'Dim wrDt As String = row.Cells(18).Text
        'Dim soStore As String = row.Cells(19).Text
        'Dim seqNum As String = row.Cells(20).Text

        Dim link = TryCast(DocGrid.SelectedRow.Cells(0).Controls(0), HyperLink)
        If link IsNot Nothing Then
            ' It worked, so you can safely access the Text property '
            delDocNum = link.Text
        Else
            Exit Sub
            ' Otherwise the cast failed, handle accordingly '
        End If

        Dim confSt As String = statCd.SelectedItem.Value

        ' Update Record
        If confSt = "CON" Or confSt = "UNC" Then
            ' Validate that Update is allowed
            Dim confStat As String = "Y"
            If confSt = "CON" Then
                If isEmpty(homePhone) Or LeonsBiz.ValidatePhoneNumber(homePhone) = False Then
                    lbl_msg.Text = "Invalid Home Phone"
                    row.Cells(7).BackColor = System.Drawing.Color.Red
                    lbl_msg.Visible = True
                    statCd.Enabled = False
                    Exit Sub
                End If
                confStat = ValidateFinance(delDocNum, confSt, "Y")
                If confStat = "N" Then
                    lbl_msg.Text = "Finance not approved"
                    lbl_msg.Visible = True
                    statCd.Enabled = False
                    statCd.BackColor = System.Drawing.Color.Red
                    Exit Sub
                End If
            End If
            If confSt = "CON" And delFlag = "D" Then
                confStat = ValidateConfirmationStatus(delDocNum, confSt)
                If confStat = "N" Then
                    lbl_msg.Text = "Invalid Address."
                    lbl_msg.Visible = True
                    row.Cells(8).BackColor = System.Drawing.Color.Red
                    statCd.Enabled = False
                    Exit Sub
                End If
            End If

            UpdateConfirmationStatus(delDocNum, confSt)
            Dim confText As String = "CONFIRMATION STATUS CHANGED TO " & confSt & " BY " & Session("emp_cd")

            theSystemBiz.SaveAuditLogComment("SOC", confText, Session("emp_cd"))

            'InsertSOComment(confText, "S", delDocNum, wrDt, soStore, seqNum)

            statCd.Enabled = True
            statCd.BackColor = System.Drawing.Color.Aqua

            'lbl_msg.Text = "Confirmation Status has been updated."
            If isEmpty(lbl_msg.Text) Then
                lbl_msg.Text = Resources.LibResources.Label955
                lbl_msg.Visible = True
            End If

        Else
            lbl_msg.Text = "No change To " & confSt & " allowed."
            lbl_msg.Visible = True
            statCd.BackColor = System.Drawing.Color.Red
        End If

        'Dim eb As LinkButton = DirectCast(DocGrid.SelectedRow.FindControl(""), LinkButton)
        'eb.Text = "Save"

        ' Change Color for th 

        'DocGrid.SelectedRow.BackColor = System.Drawing.Color.Red

    End Sub

    Protected Sub UpdateConfirmationStatus(ByVal delDocNum As String, ByVal confStatus As String)


        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        'Daniela check for lock before payments are added
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)
        Dim soRowid As String = String.Empty

        Try
            conn.Open()
            dbCmd.Transaction = conn.BeginTransaction()

            soRowid = SalesUtils.LockSoRec(delDocNum, dbCmd)
            dbCmd.Transaction.Commit()
        Catch ex As Exception
            dbCmd.Transaction.Rollback()
            lbl_msg.Text = ex.Message
            lbl_msg.Visible = True
            conn.Close()
            Exit Sub
            'Throw New Exception(String.Format(Resources.POSErrors.ERR0041, delDocNum))
        Finally
            If Not IsNothing(dbCmd.Transaction) Then
                dbCmd.Transaction.Dispose()
            End If
            dbCmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try
        'Daniela end check for lock on so table      
        Dim connUpd As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try
            'UPDATE ONLY THE AMT IN SO table

            connUpd.Open()
            Dim sql As String = "UPDATE SO Set CONF_STAT_CD= :CONF_STAT_CD, emp_cd_op = :EMP_CD WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND nvl(CONF_STAT_CD, 'UNC') <> :CONF_STAT_CD"
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, connUpd)
            cmd.Parameters.Add(":CONF_STAT_CD", OracleType.VarChar)
            cmd.Parameters(":CONF_STAT_CD").Value = confStatus
            cmd.Parameters.Add(":EMP_CD", OracleType.VarChar)
            cmd.Parameters(":EMP_CD").Value = Session("emp_cd")
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            connUpd.Close()
            Throw
        Finally
            connUpd.Close()
        End Try

    End Sub

    Protected Function ValidateConfirmationStatus(ByVal delDocNum As String, ByVal confStatus As String) As String

        Dim confAllowed As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "Select decode(count(*), 1, 'N', 'Y') CONF_STATUS from so " +
            "WHERE del_doc_num = :DEL_DOC_NUM And PU_DEL = 'D' " +
            "And (SHIP_TO_ST_CD Is null Or SHIP_TO_ZIP_CD Is null or not exists (SELECT 1 FROM ZIP WHERE ZIP_CD = SHIP_TO_ZIP_CD) )"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                confAllowed = dbReader.Item("CONF_STATUS").ToString
            Else
                Return "N"
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return confAllowed

    End Function

    Protected Function ValidateFinance(ByVal delDocNum As String, ByVal confStatus As String, ByVal isPaid As String) As String

        Dim confAllowed As String = "Y"
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String
        Dim co_grp_cd As String = Session("str_co_grp_cd")      'Dec 6, 2017
        Dim hasConfirmPIFSecurity As Boolean = False     'mariam oct 5 2017- 3250
        Dim orig_fi_amount As String = String.Empty
        Dim approval_cd As String = String.Empty
        Dim fully_paid As String = String.Empty

        sql = "select ORIG_FI_AMT, APPROVAL_CD,custom_paid.is_order_paid_in_full(so.ord_tp_cd, so.del_doc_num, so.cust_cd) FULLY_PAID from so " +
        "Where del_doc_num = :DEL_DOC_NUM and ORIG_FI_AMT > 0 "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            ' Set confAllowed = N only if ORIG_FI_AMT > 0
            If dbReader.Read() Then
                fully_paid = dbReader.Item("FULLY_PAID").ToString
                orig_fi_amount = dbReader.Item("ORIG_FI_AMT").ToString                                                       'mariam - 3250 - oct 12,2017
                approval_cd = dbReader.Item("APPROVAL_CD").ToString
                ' orig_fi_amount is greater that 0
                If co_grp_cd = "BRK" Then
                    ' Brick not confirm ORIG_FI_AMT has value and approval_cd has no value
                    If isEmpty(approval_cd) Then
                        confAllowed = "N"
                    End If
                Else ' Leons confirm if paid in full
                    confAllowed = IIf(fully_paid = "Y", "Y", "N")
                End If
            End If

            'If not fully paid /finace not approved, check for CONFIRM_PIF  - 3250 - mariam- oct 05, 2017
            If confAllowed = "N" Or isPaid = "N" Then
                hasConfirmPIFSecurity = CheckSecurity(SecurityUtils.OVERRIDE_PAID_IN_FULL, Session("emp_cd"))       'oct 5,2017 - mariam -3250 
                confAllowed = IIf(hasConfirmPIFSecurity = True, "Y", "N")
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return confAllowed

    End Function


    Public Function GetReserved(ByVal delDoc As String) As String
        Dim res As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        'Dim sql As String = "SELECT CUST_CD, FNAME, LNAME FROM CUST WHERE CUST_CD=upper(:CUST_CD)"

        Dim sql As String = "select decode(count(*), 0, 'Y', 'N') RES from itm i, so_ln sl " +
            "WHERE sl.del_doc_num = :DEL_DOC_NUM and i.itm_cd = sl.itm_cd " +
            "and I.ITM_TP_CD = 'INV' and sl.void_flag != 'Y' and SL.STORE_CD is null "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDoc
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                res = dbReader.Item("RES").ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return res

    End Function

    Public Function GetInvAvail(ByVal delDoc As String) As String
        Dim invAvail As String = "Y"
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) inv_count " +
            "from so_ln a, itm i, ZONE$PRI c, so d " +
            "where d.del_doc_num = :DEL_DOC_NUM and " +
            "c.ZONE_CD = d.SHIP_TO_ZONE_CD and c.PD_CD = d.PU_DEL AND " +
            "c.PRIORITY = '1' and " +
            "i.itm_cd = a.itm_cd and i.ITM_TP_CD = 'INV' and " +
            "a.del_doc_num = d.del_doc_num And a.STORE_CD IS NULL and " +
            "BT_ITEM_AVAIL.GET_FIFL_QTY(a.itm_cd, c.BEG_STORE_CD, 'W', null) = 0 "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDoc
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Dim invAvailCount As Integer = dbReader.Item("inv_count")
                If invAvailCount = 0 Then
                    invAvail = "Y"
                End If
                If invAvailCount > 0 Then
                    invAvail = "N"
                End If

            Else
                Return "Y"
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return invAvail

    End Function

    Public Function CheckConfirmationPeriod(ByVal zoneCd As String, ByVal delDate As Date, ByVal newConfCd As String, ByVal origConfCd As String) As String

        Dim confCodeWasChanged As Boolean = Session("confCodeChanged")
        Dim confStatusFlag As String = "Y" ' default allow confirmation
        Dim todaysDate As Date = Date.Now
        Dim zoneLeadDays As Integer = theTMBiz.GetZoneLeadDays(zoneCd)
        Dim daysToConfirm As Integer = SysPms.numDaysToConfirmSO + zoneLeadDays
        Dim isDateWithinRange As Boolean = (todaysDate.ToShortDateString <= delDate AndAlso
                                            todaysDate.ToShortDateString >= delDate.AddDays(-daysToConfirm))

        If isDateWithinRange Then
            confStatusFlag = "Y"
        Else
            confStatusFlag = "N"
        End If

        Return confStatusFlag
    End Function

    Private Sub InsertSOComment(ByVal comment As String, ByVal cmntType As String, ByRef delDocNum As String, ByRef wrDt As String, ByRef soStore As String, ByRef seqNum As String)

        Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
        cmntKey.wrDt = wrDt
        cmntKey.storeCd = soStore
        cmntKey.seqNum = seqNum

        Dim rtnVal As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim intSeqNum As Integer = OrderUtils.getNextSoCmntSeq(cmntKey)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader

        sql.Append(" INSERT INTO SO_CMNT (SO_WR_DT, ")
        sql.Append("                      SO_STORE_CD, ")
        sql.Append("                      SO_SEQ_NUM, ")
        sql.Append("                      SEQ#, ")
        sql.Append("                      DT, ")
        sql.Append("                      TEXT, ")
        sql.Append("                      DEL_DOC_NUM, ")
        sql.Append("                      CMNT_TYPE, ")
        sql.Append("                      PERM, EMP_CD) ")
        sql.Append("      VALUES( :WR_DT, ")
        sql.Append("              :STORE_CD, ")
        sql.Append("              :SEQ_NUM, ")
        sql.Append("              :SEQ#, ")
        sql.Append("              TRUNC(sysdate), ")
        sql.Append("              :CMNT, ")
        sql.Append("              :DEL_DOC_NUM, ")
        sql.Append("              :TYPE, ")
        sql.Append("              'Y', :EMP_CD) ")

        Try
            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":WR_DT", OracleType.DateTime)
            cmd.Parameters(":WR_DT").Value = Date.ParseExact(FormatDateTime(cmntKey.wrDt, DateFormat.ShortDate), "d", Globalization.CultureInfo.CurrentCulture)
            cmd.Parameters.Add(":STORE_CD", OracleType.VarChar)
            cmd.Parameters(":STORE_CD").Value = cmntKey.storeCd
            cmd.Parameters.Add(":SEQ_NUM", OracleType.VarChar)
            cmd.Parameters(":SEQ_NUM").Value = cmntKey.seqNum
            cmd.Parameters.Add(":SEQ#", OracleType.Number)
            cmd.Parameters(":SEQ#").Value = intSeqNum
            cmd.Parameters.Add(":CMNT", OracleType.VarChar)
            cmd.Parameters(":CMNT").Value = comment
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.Parameters.Add(":TYPE", OracleType.VarChar)
            cmd.Parameters(":TYPE").Value = cmntType
            cmd.Parameters.Add(":EMP_CD", OracleType.VarChar)
            cmd.Parameters(":EMP_CD").Value = Session("emp_cd")

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub


    Protected Sub ButtonSoDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent, GridViewRow)
        Dim index As Integer = gvRow.RowIndex

        Dim btn As ImageButton = CType(sender, ImageButton)
        Dim cmndArg = btn.CommandArgument
        Dim cmndName = btn.CommandName

        If cmndName = "so_details" Then
            SoDetailBindData(cmndArg)
            SODetailPopup.ShowOnPageLoad = True
        End If

    End Sub

    Protected Sub AddressDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent, GridViewRow)
        Dim index As Integer = gvRow.RowIndex

        Dim link As HyperLink = TryCast(gvRow.Cells(0).Controls(0), HyperLink)
        Dim delDocNum As String
        If link IsNot Nothing Then
            ' It worked, so you can safely access the Text property '
            delDocNum = link.Text
        Else
            Exit Sub
            ' Otherwise the cast failed, handle accordingly '
        End If
        Dim shipAddr1 As String = ""
        'If isNotEmpty(delDocNum) Then
        'shipAddr1 = GetShipToAddress2(delDocNum)
        'End If

        Dim btn As ImageButton = CType(sender, ImageButton)
        Dim cmndArg = btn.CommandArgument
        Dim cmndName = btn.CommandName

        If cmndName = "SHIP2" Then
            lbl_gm.Text = cmndArg & "<br />"
            PopupAddress.ShowOnPageLoad = True
        End If

    End Sub

    Public Function GetShipToAddress2(ByVal delDoc As String) As String
        Dim shipToAddress2 As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        'Dim sql As String = "SELECT CUST_CD, FNAME, LNAME FROM CUST WHERE CUST_CD=upper(:CUST_CD)"

        Dim sql As String = "select SHIP_TO_ADDR2 FROM SO " +
            "WHERE del_doc_num = :DEL_DOC_NUM "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDoc
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                shipToAddress2 = dbReader.Item("SHIP_TO_ADDR2").ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return shipToAddress2

    End Function

    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = MsgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = MsgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = MsgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_msg.Text = p_messageText
            lbl_msg.ForeColor = color_fontColor
            lbl_msg.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    Sub SoDetailBindData(ByVal delDoc As String)

        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter

        objConnect = DisposablesManager.BuildOracleConnection

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sql = " Select DEL_DOC_LN#, QTY, ITEM, DES, ITM_TP_CD, " +
"(case when drop_cd = 'D' then 'D' " +
        "when drop_cd = 'Q' then " +
        "     nvl((select 'D' " +
       "             From itm$itm_srt isr " +
      "             Where isr.itm_cd = ITEM " +
     "                And isr.itm_srt_cd =  " +
    "                   MTO_AUTOPO_PPKG.GET_REGION(BEG_STORE_CD)||'DI'),'Q') " +
   "     Else null End) DROP_CD, " +
  "ITM_SRT_CD, OUT_CD, " +
    "STORE_CD, " +
    "(select sum(qty) " +
       "              From itm_fifl if " +
      "              Where If.itm_cd = ITEM " +
     "                 And if.store_cd = beg_store_cd " +
    "                  ) ZONE_PRI_AV_QTY, " +
   " to_char((nvl(po_arr_dt, ist_arr_dt)),'DD-MON-YYYY') POIST_LINK_ARR_DT, " +
   "PO_CD, " +
   "PO_LN" +
  "  from(" +
 "Select a.DEL_DOC_LN#, a.QTY, a.ITM_CD ITEM, b.DES, b.ITM_TP_CD, b.DROP_CD, c.ITM_SRT_CD, decode(a.OUT_CD, null, 'N', 'Y') OUT_CD, " +
    "a.STORE_CD, '' ZONE_PRI_AV_QTY, " +
    "(select distinct pl.arrival_dt " +
       "              From so_ln$po_ln sp, po_ln pl " +
      "              Where a.del_doc_num = sp.del_doc_num " +
     "                 And a.del_doc_ln# = sp.del_doc_ln# " +
    "                  And sp.po_cd = pl.po_cd " +
   "                   And sp.ln# = pl.ln# " +
  "                     ) po_arr_dt, " +
        "(SELECT sp.po_cd " +
            "From so_ln$po_ln sp, po_ln pl " +
            "Where a.del_doc_num = sp.del_doc_num " +
                "And a.del_doc_ln#   = sp.del_doc_ln# " +
                "And sp.po_cd        = pl.po_cd " +
                "And sp.ln#          = pl.ln#" +
        ") po_cd," +
    "(SELECT sp.ln# " +
    "From so_ln$po_ln sp, " +
      "po_ln pl " +
    "Where a.del_doc_num = sp.del_doc_num " +
    "And a.del_doc_ln#   = sp.del_doc_ln# " +
    "And sp.po_cd        = pl.po_cd " +
    "And sp.ln#          = pl.ln# " +
    ") po_ln," +
 "            (select ist.transfer_dt " +
"From ist_ln$so_ln isl, ist " +
"Where a.del_doc_num = isl.del_doc_num " +
"And a.del_doc_ln# = isl.del_doc_ln# " +
"And isl.ist_wr_dt = ist.ist_wr_dt " +
"And isl.ist_store_cd = ist.ist_store_cd " +
"And isl.ist_seq_num = ist.ist_seq_num " +
        ") ist_arr_dt  , " +
        "(  select distinct c.BEG_STORE_CD from ZONE$PRI c, so d " +
         "   where c.PRIORITY = '1' and " +
        "          c.ZONE_CD = d.SHIP_TO_ZONE_CD And " +
       "           c.PD_CD = d.PU_DEL And " +
     "             d.del_doc_num = a.del_doc_num) BEG_STORE_CD " +
    "From so_ln a, itm b, ITM$ITM_SRT c Where a.itm_cd = b.itm_cd And b.itm_cd = c.itm_cd(+) And c.ITM_SRT_CD(+) = 'RTA' " +
    "And del_doc_num = '" & delDoc & "'" +
    ") Order By ITEM "


        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

	' Add new column into data table #ITREQUEST-337
        MyTable.Columns.Add(New DataColumn("BOOKING_STATUS"))
	
        ' Process grid data to get booking status
        If numrows > 0 Then
            For Each dataRow As DataRow In MyTable.Rows
                ' Get purchase order line booking status #ITREQUEST-377
                Dim bookingStatus As String = theSalesBiz.getPOLineBookingStatus(dataRow("PO_CD").ToString, dataRow("PO_LN").ToString)

                If Not IsDBNull(bookingStatus) AndAlso Not String.IsNullOrEmpty(bookingStatus) Then
                    ' Set booking status to data table column
                    dataRow("BOOKING_STATUS") = bookingStatus

                    ' Display booking status with "POIST_LINK_ARR_DT"
                    dataRow("POIST_LINK_ARR_DT") = dataRow("POIST_LINK_ARR_DT").ToString & " " & bookingStatus
                End If
            Next
        End If

        'Setup DataView for Sorting
        Source = ds.Tables(0).DefaultView

        If numrows = 0 Then
            SODetail.Visible = False
            lbl_msg.Visible = True
            lbl_msg.Text = Resources.LibResources.Label953
        Else
            SODetail.DataSource = Source
            SODetail.DataBind()
            lbl_msg.Visible = False
        End If
        'Close connection           
        objConnect.Close()

    End Sub


    ''' <summary>
    ''' Used this transient cache to avoid hitting the database multiple times for
    ''' every security and every line placed on the order, mostly during the
    ''' itemdatabound process
    ''' </summary>
    ''' <param name="key">the Security KEY that needs to be read</param>
    ''' <param name="emp">the Employee code to check security for</param>
    ''' <returns>TRUE, if user has the security indicated; FALSE otherwise</returns>
    Private Function CheckSecurity(key As String, emp As String) As Boolean
        Dim catKey As String = key + "~" + emp
        If Not SecurityCache.ContainsKey(catKey) Then
            SecurityCache.Add(catKey, SecurityUtils.hasSecurity(key, emp))
        End If
        Return SecurityCache(catKey)
    End Function

End Class
