<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesOrderComments.aspx.vb"
    Inherits="SalesOrderComments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" height="100%" class="style5">
                <tr>
                    <td valign="top" align="center">
                        <asp:Label ID="lbl_REL_NO" runat="server" Font-Bold="True" Height="24px" Width="169px"></asp:Label>
                        <br />
                        <asp:Button ID="btn_save2" runat="server" Text="Save Changes" Width="119px" CssClass="style5" />
                        &nbsp; &nbsp;&nbsp; &nbsp;
                        <asp:Button ID="btn_exit2" runat="server" Text="Exit" Width="119px" CssClass="style5" />
                        <br />
                        <br />
                        Sales Order Comments:
                        <br />
                        <asp:TextBox ID="txt_comments" runat="server" Rows="3" TextMode="MultiLine" Height="70px"
                            Width="327px" CssClass="style5"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="txt_past_Comments" runat="server" Rows="3" TextMode="MultiLine"
                            Height="66px" Width="327px" CssClass="style5" ReadOnly="True"></asp:TextBox>&nbsp;<br />
                        Delivery Comments:
                        <br />
                        <asp:TextBox ID="txt_del_comments" runat="server" CssClass="style5" Height="70px" Rows="3"
                            TextMode="MultiLine" Width="327px"></asp:TextBox><br />
                        <br />
                        <asp:TextBox ID="txt_del_past_comments" runat="server" CssClass="style5" Height="66px" Rows="3"
                            TextMode="MultiLine" Width="327px" ReadOnly="True"></asp:TextBox><br />
                        Account Comments:
                        <br />
                        <asp:TextBox ID="txt_ar_comments" runat="server" Rows="3" TextMode="MultiLine" Height="70px"
                            Width="327px" CssClass="style5"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="txt_ar_past_comments" runat="server" Rows="3" TextMode="MultiLine"
                            Height="66px" Width="327px" CssClass="style5" ReadOnly="True"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="btn_Save" runat="server" Text="Save Changes" Width="119px" CssClass="style5" />
                        &nbsp; &nbsp;&nbsp; &nbsp;
                        <asp:Button ID="btn_Clear" runat="server" Text="Exit" Width="119px" CssClass="style5" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
