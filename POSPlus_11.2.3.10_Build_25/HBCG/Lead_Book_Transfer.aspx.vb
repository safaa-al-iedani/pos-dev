Imports System.Data
Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Lead_Book_Transfer
    Inherits POSBasePage

    Public Sub populate_slsp()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCashiers As OracleCommand
        cmdGetCashiers = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP,EMP_SLSP WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and (EMP.TERMDATE IS NULL) order by FullNAME"

        Try
            With cmdGetCashiers
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCashiers)
            oAdp.Fill(ds)

            With cbo_new
                .DataSource = ds
                .DataValueField = "EMP_CD"
                .DataTextField = "FullNAME"
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub populate_slsp_old()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCashiers As OracleCommand
        cmdGetCashiers = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP,EMP_SLSP WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and (EMP.TERMDATE IS NOT NULL) order by FullNAME"

        Try
            With cmdGetCashiers
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCashiers)
            oAdp.Fill(ds)

            With cbo_original
                .DataSource = ds
                .DataValueField = "EMP_CD"
                .DataTextField = "FullNAME"
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
        If Not IsPostBack Then
            populate_slsp()
            populate_slsp_old()
            Get_Prospects()
        End If

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As oracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Protected Sub cbo_original_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_original.SelectedIndexChanged

        Get_Prospects()

    End Sub

    Public Sub Get_Prospects()
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        ds = New DataSet
        Dim MyDataReader As OracleDatareader

        'Open Connection 
        conn.Open()

        sql = "SELECT COUNT(REL_NO) As REL_COUNT FROM RELATIONSHIP WHERE SLSP1='" & cbo_original.SelectedValue & "'"
        If chk_converted.Checked = False Then
            sql = sql & " AND CONVERSION_DT IS NULL"
        End If
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                If MyDataReader.Item("REL_COUNT").ToString & "" <> "" Then
                    If CDbl(MyDataReader.Item("REL_COUNT").ToString) > 0 Then
                        lbl_information.Text = MyDataReader.Item("REL_COUNT").ToString & " prospect(s) will be transferred."
                        btn_transfer.Enabled = True
                    Else
                        lbl_information.Text = "No Prospects Found"
                        btn_transfer.Enabled = False
                    End If
                Else
                    lbl_information.Text = "No Prospects Found"
                    btn_transfer.Enabled = False
                End If
            Else
                lbl_information.Text = "No Prospects Found"
                btn_transfer.Enabled = False
            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub chk_converted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_converted.CheckedChanged

        Get_Prospects()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class

