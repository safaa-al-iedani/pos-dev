Imports System.Data.OracleClient

Partial Class Main_Customer
    Inherits POSBasePage

    Dim running_balance As Double = 0
    Dim quote_balance As Double = 0
    Dim theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub GridView2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView2.ItemDataBound

        If IsNumeric(e.Item.Cells(3).Text) Then
            quote_balance = FormatNumber(quote_balance + e.Item.Cells(3).Text, 2)
        End If
        If Len(e.Item.Cells(6).Text) > 10 Then
            Dim hpl_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink2"), HyperLink)
            hpl_ivc.NavigateUrl = "~/Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(6).Text
            hpl_ivc.Text = e.Item.Cells(6).Text
        End If

        txt_quote_summary.Text = FormatCurrency(quote_balance, 2)

    End Sub

    Function SortOrder2(ByVal Field As String) As String

        Dim so As String = Session("SortOrder2")

        If Field = so Then
            SortOrder2 = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder2 = Replace(Field, "desc", "asc")
        Else
            SortOrder2 = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder2") = SortOrder2

    End Function

    Sub MyDataGrid2_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        GridView2.CurrentPageIndex = 0 'To sort from top
        GridView1_Binddata(SortOrder2(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Public Sub GridView1_Binddata(ByVal SortField As String)

        txt_quote_summary.Text = "0.00"
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim Source As DataView
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        ds = New DataSet

        Dim FOLLOW_UP_DT As String = ""

        GridView2.DataSource = ""
        'CustTable.Visible = False
        GridView2.Visible = True

        sql = "SELECT REL_NO, WR_DT, FOLLOW_UP_DT, NVL(PROSPECT_TOTAL,0) as PROSPECT_TOTAL, STORE_CD, DECODE(REL_STATUS,'O','Open','C','Converted','F','Closed','Unknown') As REL_STATUS FROM RELATIONSHIP WHERE "
        sql = sql & "CUST_CD = '" & txt_Cust_cd.Text & "' ORDER BY WR_DT"

        conn.Open()

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim MyTable As DataTable
        Dim numrows As Integer
        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Assign sort expression to Session              
        Session("SortOrder2") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Source.Sort = SortField
                GridView2.DataSource = Source
                GridView2.DataBind()
            Else
                GridView2.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub Get_Comments()

        If Not String.IsNullOrWhiteSpace(txt_Cust_cd.Text) Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim cust_cd As String
            Dim curr_dt As String = ""
            txt_cust_past.Text = ""

            conn = DisposablesManager.BuildOracleConnection

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            cust_cd = txt_Cust_cd.Text
            'Daniela remove < character
            'sql = "SELECT SEQ#, CMNT_DT, TEXT FROM CUST_CMNT WHERE CUST_CD=:CUST_CD ORDER BY SEQ#"
            sql = "SELECT SEQ#, CMNT_DT, translate(TEXT,'<', '-') TEXT FROM CUST_CMNT WHERE CUST_CD=:CUST_CD ORDER BY SEQ#"

            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
            objSql.Parameters(":CUST_CD").Value = cust_cd
            curr_dt = ""

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    curr_dt = MyDataReader.Item("CMNT_DT").ToString
                    txt_cust_past.Text = FormatDateTime(curr_dt, DateFormat.ShortDate) & " - " & txt_cust_past.Text
                    txt_cust_past.Text = txt_cust_past.Text & MyDataReader.Item("TEXT").ToString
                End If

                Do While (MyDataReader.Read())
                    If curr_dt <> MyDataReader.Item("CMNT_DT").ToString Then

                        curr_dt = MyDataReader.Item("CMNT_DT").ToString
                        txt_cust_past.Text = txt_cust_past.Text & vbCrLf & FormatDateTime(MyDataReader.Item("CMNT_DT").ToString, DateFormat.ShortDate) & " - " & MyDataReader.Item("TEXT").ToString
                    Else
                        txt_cust_past.Text = txt_cust_past.Text & MyDataReader.Item("TEXT").ToString
                    End If
                Loop

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw

            Finally
                conn.Close()
            End Try
        End If
    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        DataGrid1.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter

        objConnect = DisposablesManager.BuildOracleConnection

        Dim co_cd = Session("CO_CD")
        Dim co_grp_cd = Session("str_co_grp_cd")

        ' Franchise changes

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'Daniela prevent SOM+ crash for missing so record
        sql = "select a.ar_trn_pk, decode(so.del_doc_num, null, NVL(a.ivc_cd, a.adj_ivc_cd) || '#', NVL(a.ivc_cd, a.adj_ivc_cd)) as ivc_cd, "
        sql = sql & "a.post_dt, a.trn_tp_cd, a.mop_cd, a.stat_cd, "
        sql = sql & "nvl(DECODE(T.DC_CD,'C',-A.AMT,'D',A.AMT,0),0) AS AMT "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a , so "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and a.cust_cd='" & txt_Cust_cd.Text & "' and NVL(a.ivc_cd, a.adj_ivc_cd) = so.del_doc_num(+) "
        'sql = sql & "and so.so_store_cd in (select store_cd from store where co_cd in " ' Daniela MCCL added"
        sql = sql & "and a.co_cd in "
        If co_grp_cd = "LEO" Then
            sql = sql & " ('" & co_cd & "') "
        Else
            sql = sql & " (select co_cd from co_grp where co_grp_cd= '" & co_grp_cd & "') "
        End If

        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows = 0 Then
            DataGrid1.Visible = False
            lbl_Cust_Info.Visible = True
            lbl_Cust_Info.Text = "No records were found matching your search criteria." & vbCrLf
        Else
            Source.Sort = SortField
            DataGrid1.DataSource = Source
            DataGrid1.DataBind()
            lbl_Cust_Info.Visible = False
        End If
        'Close connection           
        objConnect.Close()
        txt_acct_balance.Text = FormatCurrency(running_balance, 2)

    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click

        lbl_Cust_Info.Text = ""

        If txt_Fname.Text & "" <> "" And txt_Lname.Text & "" <> "" Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim sql2 As String
            Dim objSql As OracleCommand

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            'If Request("query_returned") <> "Y" Then
            If txt_Cust_cd.Enabled = True Then

                txt_Cust_cd.Text = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"), txt_addr1.Text & "", Left(UCase(txt_Lname.Text), 20),
                                                          Left(UCase(txt_Fname.Text), 15))

                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, CUST_TP_CD, FNAME, LNAME "
                sql2 = "VALUES('" & txt_Cust_cd.Text & "',TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
                sql2 = sql2 & "'" & cbo_cust_tp.SelectedValue.ToString & "','" & UCase(Replace(txt_Fname.Text, "'", "''")) & "','" & UCase(Replace(txt_Lname.Text, "'", "''")) & "'"
                If txt_corp_name.Text & "" <> "" Then
                    sql = sql & ",CORP_NAME"
                    sql2 = sql2 & ",'" & Replace(txt_corp_name.Text.Trim, "'", "''") & "'"
                End If
                If txt_addr1.Text & "" <> "" Then
                    sql = sql & ",ADDR1"
                    sql2 = sql2 & ",'" & Replace(txt_addr1.Text.Trim, "'", "''") & "'"
                End If
                If txt_addr2.Text & "" <> "" Then
                    sql = sql & ",ADDR2"
                    sql2 = sql2 & ",'" & Replace(txt_addr2.Text.Trim, "'", "''") & "'"
                End If
                If txt_City.Text & "" <> "" Then
                    sql = sql & ",CITY"
                    sql2 = sql2 & ",'" & Replace(txt_City.Text.Trim, "'", "''") & "'"
                End If
                If cbo_State.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ",ST_CD"
                    sql2 = sql2 & ",'" & cbo_State.SelectedValue.ToString & "'"
                End If
                If txt_Zip.Text & "" <> "" Then
                    sql = sql & ",ZIP_CD"
                    sql2 = sql2 & ",'" & txt_Zip.Text.Trim & "'"
                End If
                If txt_county.Text & "" <> "" Then
                    sql = sql & ",COUNTY"
                    sql2 = sql2 & ",'" & Replace(txt_county.Text.Trim, "'", "''") & "'"
                End If
                If txt_country.Text & "" <> "" Then
                    sql = sql & ",COUNTRY"
                    sql2 = sql2 & ",'" & Replace(txt_country.Text.Trim, "'", "''") & "'"
                End If
                If txt_h_phone.Text & "" <> "" Then
                    sql = sql & ",HOME_PHONE"
                    sql2 = sql2 & ",'" & StringUtils.FormatPhoneNumber(txt_h_phone.Text.Trim) & "'"
                End If
                If txt_b_phone.Text & "" <> "" Then
                    sql = sql & ",BUS_PHONE"
                    sql2 = sql2 & ",'" & StringUtils.FormatPhoneNumber(txt_b_phone.Text.Trim) & "'"
                End If
                If txt_Ext.Text & "" <> "" Then
                    sql = sql & ",EXT"
                    sql2 = sql2 & ",'" & txt_Ext.Text.Trim & "'"
                End If
                If txt_Email.Text & "" <> "" Then
                    sql = sql & ",EMAIL_ADDR"
                    sql2 = sql2 & ",'" & txt_Email.Text.Trim & "'"
                End If
                If cbo_Zone_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ",ZONE_CD"
                    sql2 = sql2 & ",'" & cbo_Zone_cd.SelectedValue.ToString & "'"
                End If
                If cbo_se_zone.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ",SE_ZONE_CD"
                    sql2 = sql2 & ",'" & cbo_se_zone.SelectedValue.ToString & "'"
                End If
                If cbo_srt_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ",SRT_CD"
                    sql2 = sql2 & ",'" & cbo_srt_cd.SelectedValue.ToString & "'"
                End If
                If cbo_tet_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ",TET_CD"
                    sql2 = sql2 & ",'" & cbo_tet_cd.SelectedValue.ToString & "'"
                    If txt_tax_exempt_cd.Text.ToString & "" <> "" Then
                        sql = sql & ",TET_ID#"
                        sql2 = sql2 & ",'" & txt_tax_exempt_cd.Text.ToString & "'"
                    End If
                End If

                sql = sql & ")" & sql2 & ")"
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    objSql.ExecuteNonQuery()
                    'Close Connection 
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    lbl_Cust_Info.Text = "ERRORS WERE ENCOUNTERED!"
                End Try
            Else
                sql = "UPDATE CUST SET CUST_TP_CD='" & cbo_cust_tp.SelectedValue.ToString & "' "
                sql = sql & ", FNAME='" & txt_Fname.Text & "' "
                sql = sql & ", LNAME='" & txt_Lname.Text & "' "
                If txt_corp_name.Text & "" <> "" Then
                    sql = sql & ", CORP_NAME='" & Replace(txt_corp_name.Text.Trim, "'", "''") & "' "
                Else
                    sql = sql & ", CORP_NAME=NULL "
                End If
                If txt_addr1.Text & "" <> "" Then
                    sql = sql & ", ADDR1='" & txt_addr1.Text.Trim & "' "
                Else
                    sql = sql & ", ADDR1=NULL "
                End If
                If txt_addr2.Text & "" <> "" Then
                    sql = sql & ", ADDR2='" & txt_addr2.Text.Trim & "' "
                Else
                    sql = sql & ", ADDR2=NULL "
                End If
                If txt_City.Text & "" <> "" Then
                    sql = sql & ", CITY='" & txt_City.Text.Trim & "' "
                Else
                    sql = sql & ", CITY=NULL "
                End If
                If cbo_State.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ", ST_CD='" & cbo_State.SelectedValue.ToString & "' "
                Else
                    sql = sql & ", ST_CD=NULL "
                End If
                If txt_Zip.Text & "" <> "" Then
                    sql = sql & ", ZIP_CD='" & txt_Zip.Text.Trim & "' "
                Else
                    sql = sql & ", ZIP_CD=NULL "
                End If
                If txt_county.Text & "" <> "" Then
                    sql = sql & ", COUNTY='" & txt_county.Text.Trim & "' "
                Else
                    sql = sql & ", COUNTY=NULL "
                End If
                If txt_country.Text & "" <> "" Then
                    sql = sql & ", COUNTRY='" & txt_country.Text.Trim & "' "
                Else
                    sql = sql & ", COUNTRY=NULL "
                End If
                If txt_h_phone.Text & "" <> "" Then
                    sql = sql & ", HOME_PHONE='" & StringUtils.FormatPhoneNumber(txt_h_phone.Text.Trim) & "' "
                Else
                    sql = sql & ", HOME_PHONE=NULL "
                End If
                If txt_b_phone.Text & "" <> "" Then
                    sql = sql & ", BUS_PHONE='" & StringUtils.FormatPhoneNumber(txt_b_phone.Text.Trim) & "' "
                Else
                    sql = sql & ", BUS_PHONE=NULL "
                End If
                If txt_Ext.Text & "" <> "" Then
                    sql = sql & ", EXT='" & txt_Ext.Text.Trim & "' "
                Else
                    sql = sql & ", EXT=NULL "
                End If
                If txt_Email.Text & "" <> "" Then
                    sql = sql & ", EMAIL_ADDR='" & txt_Email.Text.Trim & "' "
                Else
                    sql = sql & ", EMAIL_ADDR=NULL "
                End If
                If cbo_Zone_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ", ZONE_CD='" & cbo_Zone_cd.SelectedValue.ToString & "' "
                End If
                If cbo_se_zone.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ", SE_ZONE_CD='" & cbo_se_zone.SelectedValue.ToString & "' "
                End If
                If cbo_srt_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ", SRT_CD='" & cbo_srt_cd.SelectedValue.ToString & "' "
                End If
                If cbo_tet_cd.SelectedValue.ToString & "" <> "" Then
                    sql = sql & ", TET_CD='" & cbo_tet_cd.SelectedValue.ToString & "' "
                    If txt_tax_exempt_cd.Text.ToString & "" <> "" Then
                        sql = sql & ", TET_ID#='" & txt_tax_exempt_cd.Text.ToString & "' "
                    End If
                Else
                    sql = sql & ", TET_CD=NULL "
                    sql = sql & ", TET_ID#=NULL "
                End If
                sql = sql & "WHERE CUST_CD='" & txt_Cust_cd.Text.ToString & "'"

                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    objSql.ExecuteNonQuery()
                    'Close Connection 
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    lbl_Cust_Info.Text = "ERRORS WERE ENCOUNTERED!"
                    Exit Sub
                End Try

                lbl_Cust_Info.Text = "Customer code was updated."
            End If
        Else
            lbl_Cust_Info.Text = "You must enter a first and last name for your customer."
        End If

    End Sub

    Public Sub se_zone_cd_populate()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetZones As OracleCommand
        cmdGetZones = DisposablesManager.BuildOracleCommand

        Dim stored_zone_cd
        Dim SQL As String
        stored_zone_cd = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT se_zone_cd, des, se_zone_cd || ' - ' || des as full_desc FROM se_zone order by se_zone_cd"
        Try
            With cmdGetZones
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetZones)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If stored_zone_cd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("SE_ZONE_CD")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(stored_zone_cd)
            End If

            With cbo_se_zone
                .DataSource = ds
                .DataValueField = "se_zone_cd"
                .DataTextField = "full_desc"
                If Not (foundrow Is Nothing) Then
                    .SelectedValue = stored_zone_cd
                End If
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub zone_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetZones As OracleCommand
        cmdGetZones = DisposablesManager.BuildOracleCommand

        Dim stored_zone_cd
        Dim SQL As String
        stored_zone_cd = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT zone_cd, des, zone_cd || ' - ' || des as full_desc FROM zone order by zone_cd"
        Try
            With cmdGetZones
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetZones)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If stored_zone_cd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("ZONE_CD")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(stored_zone_cd)
            End If

            With cbo_Zone_cd
                .DataSource = ds
                .DataValueField = "zone_cd"
                .DataTextField = "full_desc"
                If Not (foundrow Is Nothing) Then
                    .SelectedValue = stored_zone_cd
                End If
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub



    Public Sub cust_srt_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetZones As OracleCommand
        cmdGetZones = DisposablesManager.BuildOracleCommand

        Dim stored_zone_cd
        Dim SQL As String
        stored_zone_cd = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT srt_cd, des, srt_cd || ' - ' || des as full_desc FROM cust_srt order by des"

        Try
            With cmdGetZones
                .Connection = conn
                .CommandText = SQL

            End With


            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetZones)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If stored_zone_cd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("srt_cd")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(stored_zone_cd)
            End If

            With cbo_srt_cd
                .DataSource = ds
                .DataValueField = "srt_cd"
                .DataTextField = "full_desc"
                If Not (foundrow Is Nothing) Then
                    .SelectedValue = stored_zone_cd
                End If
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub PopulateTaxExempt()

        Dim ds As DataSet = theSalesBiz.GetTaxExemptCodes()
        Dim stored_zone_cd = ""
        Dim foundrow As DataRow = Nothing

        If stored_zone_cd & "" <> "" Then
            Dim pkColumn(1) As DataColumn
            pkColumn(0) = ds.Tables(0).Columns("tet_cd")
            'set the primary key to the CustomerID column
            ds.Tables(0).PrimaryKey = pkColumn
            foundrow = ds.Tables(0).Rows.Find(stored_zone_cd)
        End If

        With cbo_tet_cd
            .DataSource = ds
            .DataValueField = "tet_cd"
            .DataTextField = "full_desc"
            If Not (foundrow Is Nothing) Then
                .SelectedValue = stored_zone_cd
            End If
            .DataBind()
        End With

    End Sub

    Protected Sub Populate_Results()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT * FROM CUST WHERE CUST_CD=:CUST_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
        objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                txt_Cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                cbo_cust_tp.SelectedValue = MyDataReader.Item("CUST_TP_CD").ToString
                txt_corp_name.Text = MyDataReader.Item("CORP_NAME").ToString
                txt_Fname.Text = MyDataReader.Item("FNAME").ToString
                txt_Lname.Text = MyDataReader.Item("LNAME").ToString
                txt_addr1.Text = MyDataReader.Item("ADDR1").ToString
                txt_addr2.Text = MyDataReader.Item("ADDR2").ToString
                txt_City.Text = MyDataReader.Item("CITY").ToString
                txt_Zip.Text = MyDataReader.Item("ZIP_CD").ToString
                txt_county.Text = MyDataReader.Item("COUNTY").ToString
                txt_country.Text = MyDataReader.Item("COUNTRY").ToString
                txt_h_phone.Text = MyDataReader.Item("HOME_PHONE").ToString
                txt_b_phone.Text = MyDataReader.Item("BUS_PHONE").ToString
                txt_Ext.Text = MyDataReader.Item("EXT").ToString
                txt_Email.Text = MyDataReader.Item("EMAIL_ADDR").ToString
                txt_tax_exempt_cd.Text = MyDataReader.Item("TET_ID#").ToString
                cbo_Zone_cd.SelectedValue = MyDataReader.Item("ZONE_CD").ToString
                cbo_se_zone.SelectedValue = MyDataReader.Item("SE_ZONE_CD").ToString
                cbo_srt_cd.SelectedValue = MyDataReader.Item("SRT_CD").ToString
                cbo_tet_cd.SelectedValue = MyDataReader.Item("TET_CD").ToString
                cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString

                'For a retrieved customer, the  type should never be allowed to be changed, so disable the drop-down. 
                ' However, if the type  'Corporate', then  and the the corp. field should be editable so user cannot change it.
                cbo_cust_tp.Enabled = False
                If (cbo_cust_tp.SelectedValue = "I") Then
                    txt_corp_name.Enabled = False
                Else
                    txt_corp_name.Enabled = True
                End If

                lbl_Cust_Info.Text = ""
                lbl_summary.Text = "<table width=""100%""><tr>"
                'lbl_summary.Text = lbl_summary.Text & "<td>First visit: </td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("ACCT_OPN_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<td>" & Resources.LibResources.Label755 & ": </td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("ACCT_OPN_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"

                MyDataReader.Close()

                sql = "SELECT POST_DT FROM AR_TRN WHERE CUST_CD=:CUST_CD AND CO_CD=:CO_CD GROUP BY POST_DT ORDER BY POST_DT DESC"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("CO_CD"))

                Dim x As Integer = 1

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read
                    If x = 1 Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label756 & ":</td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("POST_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"
                    End If
                    x = x + 1
                Loop
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label757 & ":</td><td align=""right""><font color=""darkorange"">" & vbTab & x & "</font></td></tr>"

                MyDataReader.Close()

                sql = "SELECT SUM(DECODE(AR_TRN_TP.DC_CD,'C',-AR_TRN.AMT,AR_TRN.AMT)) AS AMT "
                sql = sql & "FROM AR_TRN, AR_TRN_TP "
                sql = sql & "WHERE AR_TRN.TRN_TP_CD=AR_TRN_TP.TRN_TP_CD "
                sql = sql & "AND AR_TRN.CUST_CD=:CUST_CD "
                sql = sql & "AND AR_TRN.CO_CD=:CO_CD "
                sql = sql & "AND AR_TRN.TRN_TP_CD IN('SAL','CRM','MCR','MDB') "
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("CO_CD"))

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    If IsNumeric(MyDataReader.Item("AMT").ToString) Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label758 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(MyDataReader.Item("AMT").ToString, 2) & "</font></td></tr>"
                    Else
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label758 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(0, 2) & "</font></td></tr>"
                    End If
                End If
                MyDataReader.Close()

                sql = "SELECT SUM(DECODE(AR_TRN_TP.DC_CD,'C',-AR_TRN.AMT,AR_TRN.AMT)) AS AMT "
                sql = sql & "FROM AR_TRN, AR_TRN_TP "
                sql = sql & "WHERE AR_TRN.TRN_TP_CD=AR_TRN_TP.TRN_TP_CD "
                sql = sql & "AND AR_TRN.CO_CD=:CO_CD "
                sql = sql & "AND AR_TRN.CUST_CD=:CUST_CD "
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("CO_CD"))

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    If IsNumeric(MyDataReader.Item("AMT").ToString) Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label44 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(MyDataReader.Item("AMT").ToString, 2) & "</font></td></tr>"
                    Else
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label44 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(0, 2) & "</font></td></tr>"
                    End If
                End If
                lbl_summary.Text = lbl_summary.Text & "<tr><td colspan=""2""><hr /></td></tr>"

                MyDataReader.Close()

                conn.Close()
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()

                sql = "SELECT CONVERSION_DT, PROSPECT_TOTAL FROM RELATIONSHIP WHERE CUST_CD='" & Request("cust_cd") & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                x = 0
                Dim y As Integer = 0
                Dim z As Integer = 0

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read
                    If IsNumeric(MyDataReader.Item("PROSPECT_TOTAL").ToString) Then
                        y = y + CDbl(MyDataReader.Item("PROSPECT_TOTAL").ToString)
                        If IsDate(MyDataReader.Item("CONVERSION_DT").ToString) Then
                            z = z + CDbl(MyDataReader.Item("PROSPECT_TOTAL").ToString)
                        End If
                    End If
                    x = x + 1
                Loop
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label760 & ":</td><td align=""right""><font color=""darkorange"">" & x & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label761 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(y, 2) & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label762 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(z, 2) & "</font></td></tr>"

                MyDataReader.Close()

                lbl_summary.Text = lbl_summary.Text & "</table>"

            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        txt_Cust_cd.Enabled = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If HBCG_Utils.Find_Security("CUSM", Session("emp_cd")) = "N" Then
                cbo_cust_tp.Enabled = False
                cbo_Zone_cd.Enabled = False
                cbo_se_zone.Enabled = False
                cbo_srt_cd.Enabled = False
                cbo_tet_cd.Enabled = False
                txt_tax_exempt_cd.Enabled = False
                btn_Lookup.Enabled = False
                btn_Submit.Enabled = False
                btn_Clear.Enabled = False
                lbl_Cust_Info.Text = "You don't have the appropriate securities to update customer records."
            End If
            Dim dsStates As DataSet = Nothing
            dsStates = HBCG_Utils.GetStates()
            With cbo_State
                .DataSource = dsStates
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            cbo_State.Items.Insert(0, " ")
            cbo_State.Items.FindByText(" ").Value = ""
            zone_cd_populate()
            se_zone_cd_populate()
            cust_srt_populate()
            PopulateTaxExempt()
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then
                Populate_Results()
                btn_Lookup.Enabled = False
            Else
                cbo_Zone_cd.Enabled = False
                cbo_se_zone.Enabled = False
                cbo_srt_cd.Enabled = False
                cbo_tet_cd.Enabled = False
                txt_tax_exempt_cd.Enabled = False
                lbl_Cust_Info.Text = ""
            End If
        End If
        If cbo_tet_cd.SelectedValue & "" <> "" Then
            txt_tax_exempt_cd.Visible = True
        Else
            txt_tax_exempt_cd.Visible = False
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_Cust_cd.Enabled = True
        txt_Cust_cd.Text = ""
        cbo_cust_tp.SelectedValue = "I"
        txt_corp_name.Text = ""
        txt_Fname.Text = ""
        txt_Lname.Text = ""
        txt_addr1.Text = ""
        txt_addr2.Text = ""
        txt_City.Text = ""
        cbo_State.SelectedValue = ""
        txt_Zip.Text = ""
        txt_county.Text = ""
        txt_country.Text = ""
        txt_h_phone.Text = ""
        txt_b_phone.Text = ""
        txt_Ext.Text = ""
        txt_Email.Text = ""
        cbo_Zone_cd.SelectedValue = ""
        cbo_se_zone.SelectedValue = ""
        cbo_srt_cd.SelectedValue = ""
        cbo_tet_cd.SelectedValue = ""
        txt_tax_exempt_cd.Text = ""
        lbl_Cust_Info.Text = ""
        txt_corp_name.Enabled = True
        Response.Redirect("Main_Customer.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        lbl_Cust_Info.Text = ""
        Response.Redirect("Main_Cust_Lookup.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text) & "&fname=" & HttpUtility.UrlEncode(txt_Fname.Text) _
            & "&lname=" & HttpUtility.UrlEncode(txt_Lname.Text) & "&addr1=" & HttpUtility.UrlEncode(txt_addr1.Text) & "&addr2=" & HttpUtility.UrlEncode(txt_addr2.Text) & "&city=" _
            & HttpUtility.UrlEncode(txt_City.Text) & "&state=" & HttpUtility.UrlEncode(cbo_State.SelectedValue.ToString) & "&zip=" & HttpUtility.UrlEncode(txt_Zip.Text) & "&hphone=" _
            & HttpUtility.UrlEncode(txt_h_phone.Text) & "&bphone=" & HttpUtility.UrlEncode(txt_b_phone.Text) & "&email=" & HttpUtility.UrlEncode(txt_Email.Text) & "&corp_name=" & HttpUtility.UrlEncode(txt_corp_name.Text))

    End Sub

    Protected Sub txt_Zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If btn_Lookup.Enabled = False Then
            Update_City_State()
        End If

    End Sub

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(txt_Zip.Text) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            conn.Open()

            sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & txt_Zip.Text & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    txt_City.Text = MyDataReader.Item("CITY").ToString
                    cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged1(ByVal source As Object, ByVal e As DevExpress.Web.ASPxTabControl.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged

        If txt_Cust_cd.Text & "" <> "" Then
            If ASPxPageControl1.ActiveTabIndex = 2 Then
                'Set up default column sorting   
                If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                    BindData("post_dt asc")
                Else
                    BindData(Session("SortOrder"))
                End If
            ElseIf ASPxPageControl1.ActiveTabIndex = 3 Then
                If IsNothing(Session("SortOrder2")) Or IsDBNull(Session("SortOrder2")) Then
                    GridView1_Binddata("rel_no asc")
                Else
                    GridView1_Binddata(Session("SortOrder2"))
                End If
            ElseIf ASPxPageControl1.ActiveTabIndex = 4 Then
                Get_Comments()
            End If
        End If

    End Sub

    Protected Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound

        If IsNumeric(e.Item.Cells(5).Text) Then
            Dim txt_qty As Label = CType(e.Item.FindControl("lbl_balance"), Label)
            txt_qty.Text = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
            running_balance = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
        End If
        If Len(e.Item.Cells(7).Text) > 10 Then
            Dim hpl_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink1"), HyperLink)
            If e.Item.Cells(2).Text = "SAL" Or e.Item.Cells(2).Text = "CRM" Or e.Item.Cells(2).Text = "MCR" Or e.Item.Cells(2).Text = "MDB" Then
                'Daniela fix SOM+ error
                If InStr(e.Item.Cells(7).Text, "#") > 0 Then
                    hpl_ivc.NavigateUrl = "#"
                Else
                    hpl_ivc.NavigateUrl = "~/SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(7).Text
                End If
            Else
                hpl_ivc.NavigateUrl = "#"
            End If
            hpl_ivc.Text = Replace(e.Item.Cells(7).Text, "#", "")
        End If

    End Sub

    Protected Sub btn_Save_Comments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_comments.Click
        ' saves customer comments

        Dim conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim CUST_CMNT_LENGTH As Integer = 70

        ' TODO - this logic needs to go into customer utility - fixes for date here that aren't in OrderStage and SOM+ - have to handle passing command for transaction
        ' do not attempt to save comments if no cust code 
        If txt_cust_comments.Text & "" <> "" And Not String.IsNullOrWhiteSpace(txt_Cust_cd.Text) Then

            Try
                conn.Open()

                Dim sql As String
                Dim objSql As OracleCommand
                Dim cust_cd As String = txt_Cust_cd.Text

                Dim lngSEQNUM As Integer = HBCG_Utils.Get_next_cust_cmnt_seq(cust_cd)
                Dim strComments As String = txt_cust_comments.Text.ToString
                Dim lngComments As Integer = Len(strComments)
                Dim startChar As Integer = 1
                Dim strMidCmnts As String
                Dim dateTime As String = Now  ' a single comment being split up must have the exact same date and time to be maintained as one comment on query

                If lngComments > 0 Then

                    Do Until startChar > lngComments
                        strMidCmnts = Mid(strComments, startChar, CUST_CMNT_LENGTH)
                        strMidCmnts = Replace(strMidCmnts, "'", "")

                        sql = "INSERT INTO CUST_CMNT (cust_cd, seq#, cmnt_dt, text, emp_cd_op) " &
                              "VALUES('" & cust_cd & "', " & lngSEQNUM & ", TO_DATE('" & dateTime & "', 'MM/DD/RRRR HH:MI:SS PM'), '" &
                              Replace(strMidCmnts, """", """""") & "', '" & Session("EMP_CD") & "')"

                        sql = UCase(sql)

                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql.ExecuteNonQuery()
                        objSql.Cancel()
                        objSql.Dispose()

                        startChar = startChar + CUST_CMNT_LENGTH
                        lngSEQNUM = lngSEQNUM + 1
                    Loop
                End If

            Catch ex As Exception
                'Throw ex
                'Daniela prevent yellow traingle
                lbl_Cust_Info.Text = "Invalid comments. Please validate and try again."
            Finally
                conn.Close()
            End Try

            txt_cust_comments.Text = ""
            txt_cust_past.Text = ""
            Get_Comments()

        Else
            lbl_Cust_Info.Text = "You must save the customer first."
        End If

    End Sub


End Class

