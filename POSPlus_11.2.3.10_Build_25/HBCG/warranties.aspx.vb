Imports System.Data.OracleClient


Partial Class warranties
    Inherits POSBasePage

    Protected Const sameWarrSeparator As String = "%"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim SKUS As String
            SKUS = Request("itm_cd")

            If SKUS & "" = "" Then
                lbl_ext_warr.Text = "No Warranty SKUS were found for this order."
                VSN.Visible = False
                lbl_curr_warr.Visible = False
                ADD_WARR.Visible = False
                Button1.Visible = False
                PrevWarrList.Visible = False
                lbl_ext_warr2.Visible = False
                Button2.Text = "Close Window"

            Else
                bindgrid()
            End If
        End If

    End Sub

    Public Sub bindgrid()

        Dim SKUS As String = Request("itm_cd")
        Dim datSet As DataSet = New DataSet
        ' if there are multiple SKUs in the input, do as before for now
        ' TODO DSA - handle multiple SKUs; assuming one for now
        If SKUS.Contains(",") Then
            Dim tmpNum = 20 / 0    ' force an error for now if multiples  ' TODO - need to resolve multiple in input - really can only get warr for one SKU at a time
        Else
            Dim req4ExtWarr As SalesUtils.GetCalRetPrcReq = New SalesUtils.GetCalRetPrcReq
            req4ExtWarr.itmCd = SKUS
            req4ExtWarr.cashFlg = "N"
            req4ExtWarr.itmRetPrc = Nothing
            If (Not IsNothing(Session("tran_dt"))) AndAlso SystemUtils.isNotEmpty(Session("tran_dt")) AndAlso IsDate(Session("tran_dt")) Then  ' TODO DSA - test the nothings
                req4ExtWarr.effDt = Session("tran_dt")
            Else
                req4ExtWarr.effDt = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
            End If
            If (Not IsNothing(Session("store_cd"))) AndAlso SystemUtils.isNotEmpty(Session("store_cd")) Then
                req4ExtWarr.storeCd = Session("store_cd")
            Else
                req4ExtWarr.storeCd = ""
            End If
            If (Not IsNothing(Session("cust_tp_prc_cd"))) AndAlso SystemUtils.isNotEmpty(Session("cust_tp_prc_cd")) Then
                req4ExtWarr.custTpPrcCd = Session("cust_tp_prc_cd")
            Else
                req4ExtWarr.custTpPrcCd = ""
            End If
            datSet = SalesUtils.GetItmExtWarrs(req4ExtWarr)

            If SysPms.isOne2ManyWarr Then

                Dim prevWarrDatSet As DataSet = New DataSet
                prevWarrDatSet = GetTempWarrExtWarrs()
                Session("PrevWarrList") = prevWarrDatSet
                If SystemUtils.dataSetHasRows(prevWarrDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(prevWarrDatSet.Tables(0).Rows(0)) Then

                    PrevWarrList.DataSource = prevWarrDatSet
                    PrevWarrList.DataValueField = "war_itm_cd"
                    PrevWarrList.DataTextField = "des"
                    PrevWarrList.DataBind()

                Else
                    PrevWarrList.Visible = False
                    lbl_ext_warr2.Visible = False
                End If
            Else
                PrevWarrList.Visible = False
                lbl_ext_warr2.Visible = False
            End If
        End If

        ADD_WARR.DataSource = datSet
        ADD_WARR.DataValueField = "WAR_ITM_CD"
        ADD_WARR.DataTextField = "DES"
        ADD_WARR.DataBind()

        If datSet.ToString & "" <> "" Then

            ' can only get and relate 1 warranty at a time, even if can relate to multiple
            Dim manuWarrs As DataTable = SalesUtils.GetItmManuWarrs(SKUS).Tables(0)

            For Each mWarr As DataRow In manuWarrs.Rows
                If VSN.Text & "" = "" Then
                    VSN.Text = "The item you have selected, " & mWarr.Item("VSN").ToString & ", comes with the following warranties:"
                End If
                lbl_curr_warr.Text = lbl_curr_warr.Text & "<li>" & mWarr.Item("DES").ToString & "</li>" & vbCrLf
            Next
            lbl_curr_warr.Text = "<ul>" & lbl_curr_warr.Text & "</ul>"
        End If
    End Sub

    ''' <summary>
    ''' Extracts temp_warr table entries for the existing temp_itm warranty records that can be applied to the SKU
    ''' </summary>
    Private Function GetTempWarrExtWarrs() As DataSet

        Dim warr As New DataSet
        Dim sql As New StringBuilder
        ' add ln_num to itm_cd to make same SKU different in usage, add % to be able to strip off the trailer and get the SKU
        sql.Append("SELECT itm_cd || '" + sameWarrSeparator + "' || ln_num AS war_itm_cd, TO_CHAR(ret_prc, '$9999.99') || ' -- ' || vsn || ' (' || days || ' DAYS)' AS des, ln_num ").Append(
                   "FROM temp_warr tw ").Append(
                   "WHERE session_id = :sessId ORDER BY days ")
        Try
            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(UCase(sql.ToString), conn), oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                If "".Equals(Request("del_Doc_num") & "") Then
                    'If "".Equals(Request("referrer") & "") Then
                    cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                Else
                    cmd.Parameters(":sessId").Value = Request("del_Doc_num")
                End If

                oAdp.Fill(warr)
            End Using
        Finally
        End Try

        Return warr
    End Function

    ''' <summary>
    ''' Deletes temp_warr table entries before exit
    ''' </summary>
    Private Sub DeleteTempWarr()

        Dim sql As New StringBuilder
        sql.Append("DELETE FROM temp_warr WHERE session_id = :sessId ")

        Try
            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(UCase(sql.ToString), conn)

                conn.Open()

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                'cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                If "".Equals(Request("del_Doc_num") & "") Then
                    'If "".Equals(Request("referrer") & "") Then
                    cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                Else
                    cmd.Parameters(":sessId").Value = Request("del_Doc_num")
                End If

                cmd.ExecuteNonQuery()
            End Using
        Finally
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' button to select a warranty

        lbl_error.Text = ""
        If ADD_WARR.SelectedValue.ToString() & "" = "" AndAlso PrevWarrList.SelectedValue.ToString() & "" = "" Then
            lbl_error.Text = "You must select a valid warranty"
            Exit Sub
        End If

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup9();")

        Dim selWarr As String = ""
        Dim warrItmRowNum As String = ""
        Dim selIndx As Integer = 0

        If ADD_WARR.SelectedValue.ToString() & "" <> "" Then

            selWarr = ADD_WARR.SelectedValue.ToString()
            selIndx = ADD_WARR.SelectedIndex

        ElseIf SysPms.isOne2ManyWarr Then  'If PrevWarrList.SelectedValue.ToString() & "" <> "" Then ' if not, logic problem

            selWarr = PrevWarrList.SelectedValue.ToString()
            selWarr = selWarr.Substring(0, selWarr.IndexOf(sameWarrSeparator))
            selIndx = PrevWarrList.SelectedIndex
            Dim warrSet As DataSet = Session("PrevWarrList")
            Dim warrTbl As DataTable = warrSet.Tables(0)
            Dim warrCnt As Integer = warrTbl.Rows.Count
            Dim warrIndx As Integer = 0
            Dim lnNum As Integer = 0
            Do While warrIndx <= (warrCnt - 1) AndAlso warrIndx <= selIndx

                lnNum = warrTbl.Rows(warrIndx)("ln_num")
                warrIndx = warrIndx + 1
            Loop
            If warrIndx - 1 = selIndx Then
                warrItmRowNum = lnNum.ToString
            End If
        End If

        'warrItmRowNum is filled in if the warranty selected is from the existing list, otherwise the warrItmRowNum will be empty or NONE if select No Thanks
        Dim rtnInfo As New StringBuilder
        rtnInfo.Append("itmCd=" & Request("itm_cd") & "&itmRowNum=" & Request("itmRowNum") & "&warrItmCd=" & selWarr & "&warrItmRowNum=" & warrItmRowNum)
        'If Request("referrer") = "salesordermaintenance.aspx" Then
        rtnInfo.Append("&del_doc_num=" & Request("del_doc_num"))
        'End If
        If Not Request("preventMessage") Is Nothing Then
            rtnInfo.Append("&preventMessage=" & Request("preventMessage") & "")
        Else
            'do nothing
        End If

        DeleteTempWarr()
        If Request("LEAD") = "TRUE" Then
            Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE&" & rtnInfo.ToString & "';" & vbCrLf)
        ElseIf Request("referrer") & "" <> "" Then
            Response.Write("parentWindow.location.href='" & Request("referrer") & "?" & rtnInfo.ToString & "';" & vbCrLf)
            'Else
            '    Response.Write("parentWindow.location.href='order_detail.aspx?" & rtnInfo.ToString & "';" & vbCrLf)
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
        ' button to exit with no warranty

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup9();")
        Dim rtnInfo As New StringBuilder
        rtnInfo.Append("?del_doc_num=" & Request("del_doc_num"))
        'If Request("referrer") = "salesordermaintenance.aspx" Then
        rtnInfo.Append("&warrItmCd=NONE")
        If Not Request("preventMessage") Is Nothing Then
            rtnInfo.Append("&preventMessage=" & Request("preventMessage") & "")
        Else
            'do nothing
        End If

        'End If
        DeleteTempWarr()
        If Request("LEAD") = "TRUE" Then

            Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE';" & vbCrLf)

        ElseIf Request("referrer") & "" <> "" Then
            If Not (Request("del_doc_num") Is Nothing) Then
                Response.Write("parentWindow.location.href='" & Request("referrer") & rtnInfo.ToString & "';" & vbCrLf)
            Else
                Response.Write("parentWindow.location.href='" & Request("referrer") & "?preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
            End If

            'Else
            '    Response.Write("parentWindow.location.href='order_detail.aspx';" & vbCrLf)
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub PrevWarrList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PrevWarrList.SelectedIndexChanged

        If ADD_WARR.SelectedValue.ToString() & "" <> "" Then

            ADD_WARR.ClearSelection()
        End If
    End Sub

    Protected Sub ADD_WARR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ADD_WARR.SelectedIndexChanged

        If PrevWarrList.SelectedValue.ToString() & "" <> "" Then

            PrevWarrList.ClearSelection()
        End If
    End Sub
End Class
