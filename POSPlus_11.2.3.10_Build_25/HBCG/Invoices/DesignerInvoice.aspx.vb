﻿Imports System.Data
Imports System.Data.OracleClient
Imports Email_Functions
Imports System.Xml
Imports System.Web
Imports System.IO
Imports System.Diagnostics
Imports DevExpress.XtraReports.UI
Imports AppExtensions
Imports SalesLibrary.OrderInfoUtils
Imports SalesLibrary


Partial Class Invoices_DesignerInvoice
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim delDocNum As String = Request("DEL_DOC_NUM")
        Dim invoiceName As String = Request("INVOICE_NAME")
        Dim watch As Stopwatch = New Stopwatch()

        If delDocNum.isEmpty OrElse invoiceName.isEmpty Then
            Response.Write("No Order or Invoice specified.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else
            'watch.Start()
            ProcessCustomInvoice(invoiceName, delDocNum, "C:\Invoices")
            'watch.Stop()            
            'watch.Reset()
        End If

    End Sub

    ''' <summary>
    ''' Processes the invoice by first getting all the order info and then saving it as
    ''' a pdf file. It calls an external library to get the order details as an xml document.
    ''' it formats and displays it as per the defined invoice template, saves it as a pdf file 
    ''' and even emails the invoice, if setup to do so.
    ''' </summary>
    ''' <param name="invoiceTemplate">the name of the invoice template to be used for formatting the invoice</param>
    ''' <param name="delDocNum">the current order or del doc number</param>
    ''' <param name="appPath">the path where the invoice is to be written and saved.</param>
    Private Sub ProcessCustomInvoice(ByVal invoiceTemplate As String,
                                     ByVal delDocNum As String,
                                     ByVal appPath As String)


        Dim pdfDocPath As String = appPath & "\" & delDocNum & ".pdf"
        Dim xmlDocPath As String = appPath & "\" & delDocNum & ".xml"
        Dim Report As New DevExpress.XtraReports.UI.XtraReport
        Dim xmlDoc As XmlDocument = New XmlDocument

        'Load the layout of the .repx passed-in from the deployment dir, so it can be bound to the actual data
        Report.LoadLayout(HttpContext.Current.Server.MapPath("~/invoice_templates/") & invoiceTemplate)

        'call the web service using the delDocNum passed-in and save the output as xml doc with that data
        xmlDoc.LoadXml(GetOrderInfo(delDocNum))
        'save the xml output to a file
        xmlDoc.Save(xmlDocPath)
        Report.XmlDataPath = xmlDocPath

        Response.ClearContent()
        Response.ClearHeaders()
        Response.Buffer = True
        Response.Cache.SetCacheability(HttpCacheability.Private)

        'save the report as a pdf file
        Dim ms As New System.IO.MemoryStream()
        Report.ExportToPdf(ms)
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", String.Format("inline;filename=" & delDocNum & ".pdf"))
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        Response.BinaryWrite(ms.ToArray())

        'write & save the actual pdf file with the order invoice
        Dim fs As System.IO.FileStream = System.IO.File.OpenWrite(pdfDocPath)
        fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
        fs.Flush()
        fs.Close()
        ms.Flush()
        ms.Close()
        fs = Nothing
        ms = Nothing

        'set the report to the document
        ReportViewer1.Report = Report
        HttpContext.Current.ApplicationInstance.CompleteRequest()

        'check if invoice needs to be emailed
        Dim emailInvoice As Boolean = (Request("EMAIL") <> "" AndAlso Request("EMAIL") = "True")
        If (emailInvoice) Then
            SendMailMessage(appPath & "\" & delDocNum & ".pdf")
        End If

    End Sub


    ''' <summary>
    ''' Calls an external library to get all the details of the order/del doc passed-in
    ''' and return it as a xml string.
    ''' </summary>
    ''' <param name="delDocNum">the order/del doc num for which to get all the information </param>
    ''' <returns>a xml string that has all the elements for an order.</returns>
    Private Function GetOrderInfo(ByVal delDocNum As String) As String

        Dim orderXml As String = String.Empty
        Dim req As New OrderInfoRequest
        req.puDelStoreDet = True 'puDelStoreDetail
        req.wrStoreDet = True 'wrStoreDetail
        req.empSlsp1HdrDet = True
        req.empSlsp2HdrDet = True
        req.empSlsp1LineDet = True
        req.empSlsp2LineDet = True
        req.empOpDet = True
        req.empKeyerDet = True
        req.billToCustDet = True
        req.billCustCredDet = True
        req.mfgWarrDet = True
        req.itmDet = True
        req.currPrc = True
        req.lineStatusDet = True
        req.lineDiscDet = True
        req.usrFlds = True
        req.saleCmnts = True
        req.delivCmnts = True
        req.autoCmnts = True
        req.rfCmnts = True
        req.arDocList = True       ' true returns the list of AR_TRN SAL/MDB/MCR/CRM entries for the document
        req.arPmtList = True        ' true returns the list of AR_TRN credit card, cash and check entries for the document
        req.arOthDbCrList = True
        req.trkDet = True
        req.financeDet = True
        req.calcTotals = True

        Try
            orderXml = OrderInfoUtils.getOrderDataSetXmlString(delDocNum, False)
        Catch ex As Exception
            Throw
        End Try


        Return orderXml

    End Function


    ''' <summary>
    ''' Converts the object passed-in into an xml string.
    ''' </summary>
    ''' <param name="objectToConvert">the object to convert into xml</param>
    ''' <returns>the xml representation of the object passed-in</returns>
    Private Shared Function ConvertToXML(ByVal objectToConvert As Object) As String

        Dim xmlDoc As New XmlDocument
        Dim xmlSerializer As New System.Xml.Serialization.XmlSerializer(objectToConvert.GetType())
        Dim stream As New System.IO.MemoryStream()

        Using xmlStream As New MemoryStream()
            xmlSerializer.Serialize(xmlStream, objectToConvert)
            xmlStream.Position = 0
            xmlDoc.Load(xmlStream)
            Return xmlDoc.InnerXml
        End Using

    End Function


End Class
