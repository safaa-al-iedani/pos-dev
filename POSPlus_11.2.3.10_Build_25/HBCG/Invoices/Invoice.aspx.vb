﻿Imports System.Data.OracleClient

Partial Class Invoice
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        If DEL_DOC_NUM & "" = "" Then
            DEL_DOC_NUM = txt_DEL_DOC_NUM.Text
        End If
        DEL_DOC_NUM = UCase(DEL_DOC_NUM)

        If DEL_DOC_NUM & "" = "" Then
            invoice_body.Visible = False
            query_screen.Visible = True
        Else
            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader
            Dim sql
            Dim UpPanel As UpdatePanel
            UpPanel = Master.FindControl("UpdatePanel1")

            conn2.Open()

            Dim default_invoice As String = ""

            sql = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & Session("STORE_CD") & "'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            Try
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                Do While MyDatareader2.Read
                    If MyDatareader2.Item("invoice_file").ToString & "" = "" Then
                        default_invoice = MyDatareader2.Item("DEFAULT_FILE").ToString
                    Else
                        default_invoice = MyDatareader2.Item("invoice_file").ToString
                    End If
                Loop
                MyDatareader2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            If String.IsNullOrEmpty(default_invoice) Then
                sql = "select store_cd, invoice_file, default_file from invoice_admin"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                Try
                    MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDatareader2.Read Then
                        default_invoice = MyDatareader2.Item("DEFAULT_FILE").ToString
                    End If
                    MyDatareader2.Close()
                Catch ex As Exception
                    conn2.Close()
                    Throw
                End Try
            End If

            'resolve invoice to print
            If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('" & default_invoice & "?DEL_DOC_NUM=" & DEL_DOC_NUM & "','frmTest1','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('../merch_images/invoice_files/" & DEL_DOC_NUM & ".pdf" & "','frmTest1','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & DEL_DOC_NUM & "&INVOICE_NAME=" & default_invoice & "','frmTest1','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class

