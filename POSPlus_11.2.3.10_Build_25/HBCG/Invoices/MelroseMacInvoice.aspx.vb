Imports System.Data
Imports System.Data.OracleClient
Imports Email_Functions

Partial Class Reports_MelroseMacInvoice
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        Dim EMAIL As String = Request("EMAIL")
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Sales Order Found.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else
            ' Create a report.
            Dim Report As New MMACInvoice

            ' Create a connection.
            Dim Connection As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            Connection.Open()

            Dim sql As String
            sql = "SELECT SO.*, ITM.VSN, ITM.DES, ITM.VE_CD, EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME, EMP.ADDR2, EMP.EMAIL_ADDR, SO_LN.ITM_CD, SO_LN.VOID_FLAG, SO_LN.STORE_CD, SO_LN.LOC_CD, "
            sql = sql & "SO_LN.UNIT_PRC, SO_LN.QTY, SO_LN.SER_NUM, ITM.SIZ, SO_LN.DISC_AMT, SO_LN.DEL_DOC_LN#, SO_LN.QTY*SO_LN.UNIT_PRC As EXT_PRC, "
            sql = sql & "STORE.STORE_NAME, 'Tel. ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 || ', ' || STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS STORE_ADDRESS, "
            sql = sql & "CUST.FNAME || ' ' || CUST.LNAME AS CUST_FULL_NAME, CUST.OPEN_AR_TERM_CD, CUST.ADDR1, CUST.CORP_NAME, CUST.ADDR2 AS BILL_ADDR2, CUST.CITY, CUST.ST_CD, CUST.ZIP_CD, CUST.HOME_PHONE, CUST.BUS_PHONE "
            sql = sql & "FROM SO, SO_LN, ITM, EMP, STORE, STORE_PHONE, CUST WHERE SO.DEL_DOC_NUM='" & DEL_DOC_NUM & "' "
            sql = sql & "AND SO_LN.ITM_CD=ITM.ITM_CD "
            sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD1  "
            sql = sql & "AND SO.SO_STORE_CD=STORE.STORE_CD "
            sql = sql & "AND SO.CUST_CD=CUST.CUST_CD "
            sql = sql & "AND SO.SO_STORE_CD=STORE_PHONE.STORE_CD(+) "
            sql = sql & "AND STORE_PHONE.PHONE_TP_CD(+)='PH' "
            sql = sql & "AND SO_LN.VOID_FLAG(+)='N' "
            sql = sql & "AND SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM(+) "
            sql = sql & "ORDER BY SO.DEL_DOC_NUM, SO_LN.DEL_DOC_LN#"

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            ' Create a data adapter and a dataset.
            objSql = DisposablesManager.BuildOracleCommand(sql, Connection)
            Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, Connection)
            Dim DataSet1 As New DataSet()
            Dim Proceed As Boolean = False

            ' Specify the data adapter and the data source for the report.
            ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
            Report.DataAdapter = Adapter
            Adapter.Fill(DataSet1, "SO")
            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If MyDataReader.Item("DEL_DOC_NUM").ToString & "" <> "" Then Proceed = True
                End If

                'Close Connection 
                MyDataReader.Close()
                Connection.Close()
            Catch ex As Exception
                Connection.Close()
                Throw
            End Try

            If Proceed = False Then
                Response.Write("No Sales Order Found.")
                ReportToolbar1.Enabled = False
                ReportToolbar1.Visible = False
                ReportViewer1.Visible = False
            Else
                Report.DataSource = DataSet1
                Report.DataMember = "SO"
                Report.BindSOVariables()
                Report.Name = DEL_DOC_NUM

                If Not String.IsNullOrEmpty(EMAIL) Then
                    Try
                        Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        If TheFile.Exists Then
                            System.IO.File.Delete("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        End If
                    Catch ex As Exception

                    End Try

                    Dim ms As New System.IO.MemoryStream()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.Private)

                    Report.ExportToPdf(ms)
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", String.Format("inline;filename=" & DEL_DOC_NUM & ".pdf"))
                    ms.Seek(0, System.IO.SeekOrigin.Begin)
                    Response.BinaryWrite(ms.ToArray())
                    Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                    fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                    fs.Flush()
                    fs.Close()
                    ms.Flush()
                    ms.Close()
                    fs = Nothing
                    ms = Nothing
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    If EMAIL = "True" Then
                        SendMailMessage("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                    End If
                End If

                ReportViewer1.Report = Report
                ReportViewer1.WritePdfTo(Page.Response)

            End If

        End If

    End Sub

End Class
