Imports System.Data
Imports System.Data.OracleClient
Imports Email_Functions

Partial Class Invoices_RothmanQuote
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")

        Dim EMAIL As String = Request("EMAIL")
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Relationship Found.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else '
            Dim Report As New RothmanQuote
            Dim Connection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            Dim sql As String
            sql = "SELECT RELATIONSHIP.*, FNAME || ' ' || LNAME AS FULL_NAME, ITM_CD, VSN, DES, LINE, "
            sql = sql & "RET_PRC, QTY, QTY*RET_PRC As EXT_PRC "
            'sql = sql & "FROM RELATIONSHIP, RELATIONSHIP_LINES WHERE RELATIONSHIP.REL_NO='" & DEL_DOC_NUM & "' "
            sql = sql & "FROM RELATIONSHIP, RELATIONSHIP_LINES WHERE RELATIONSHIP.REL_NO=:docNum "
            sql = sql & "AND RELATIONSHIP.REL_NO=RELATIONSHIP_LINES.REL_NO(+) "
            sql = sql & "ORDER BY RELATIONSHIP.REL_NO, RELATIONSHIP_LINES.LINE"
            ' Create a data adapter and a dataset.
            Try
                Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, Connection)
                cmd.Parameters.Clear()
                cmd.Parameters.Add(":docNum", OracleType.VarChar)
                cmd.Parameters(":docNum").Value = DEL_DOC_NUM
                Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                Dim DataSet1 As New DataSet()

                ' Specify the data adapter and the data source for the report.
                ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
                Report.DataAdapter = Adapter
                Adapter.Fill(DataSet1, "SO")
                Report.DataSource = DataSet1
                Report.DataMember = "SO"
                Report.BindSOVariables()
                Report.Name = DEL_DOC_NUM
                If Not String.IsNullOrEmpty(EMAIL) Then
                    Try
                        Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        If TheFile.Exists Then
                            System.IO.File.Delete("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        End If
                    Catch ex As Exception

                    End Try

                    Dim ms As New System.IO.MemoryStream()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.Private)

                    Report.ExportToPdf(ms)
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", String.Format("inline;filename=" & DEL_DOC_NUM & ".pdf"))
                    ms.Seek(0, System.IO.SeekOrigin.Begin)
                    Response.BinaryWrite(ms.ToArray())
                    Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                    fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                    fs.Flush()
                    fs.Close()
                    ms.Flush()
                    ms.Close()
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    If EMAIL = "True" Then
                        SendMailMessage("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                    End If
                End If

            Catch ex As Exception

            Finally
                If Connection.State = ConnectionState.Open Then
                    Connection.Close()
                End If

            End Try

            ReportViewer1.Report = Report
            ReportViewer1.WritePdfTo(Page.Response)

        End If


    End Sub

End Class
