Imports System.Data
Imports System.Data.OracleClient
Imports Email_Functions

Partial Class Reports_OlindesThermal
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        Dim EMAIL As String = Request("EMAIL")
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Sales Order Found.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else
            ' Create a report.
            Dim Report As New ThermalInvoice
            Dim sql As String = InvoiceUtils.GetOrderDataQuery(DEL_DOC_NUM)

            ' Create a connection.
            Dim Connection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Connection.Open()

            ' Create a data adapter and a dataset.
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, Connection)
            Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
            Dim DataSet1 As New DataSet()

            ' Specify the data adapter and the data source for the report.
            ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
            Report.DataAdapter = Adapter
            Adapter.Fill(DataSet1, "SO")
            Report.DataSource = DataSet1
            Report.DataMember = "SO"
            Report.BindSOVariables()
            Dim randObj As New Random

            Report.Name = DEL_DOC_NUM & randObj.Next(50000)

            If Not String.IsNullOrEmpty(EMAIL) Then
                Dim ms As New System.IO.MemoryStream()
                Response.ClearContent()
                Response.ClearHeaders()
                Response.Buffer = True
                Response.Cache.SetCacheability(HttpCacheability.Private)

                Report.ExportToPdf(ms)
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", String.Format("inline;filename=" & DEL_DOC_NUM & ".pdf"))
                ms.Seek(0, System.IO.SeekOrigin.Begin)
                Response.BinaryWrite(ms.ToArray())
                Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\invoices\" & DEL_DOC_NUM & ".pdf")
                fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                fs.Close()
                ms.Close()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                If EMAIL = "True" Then
                    SendMailMessage("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                End If
            End If

            ReportViewer1.Report = Report
            ReportViewer1.WritePdfTo(Page.Response)

        End If

    End Sub

End Class
