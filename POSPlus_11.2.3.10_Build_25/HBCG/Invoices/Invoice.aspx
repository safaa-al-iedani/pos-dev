﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPages/NoWizard2.master"
    CodeFile="Invoice.aspx.vb" Inherits="Invoice" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server" id="query_screen" align="center">
        <br />
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text='Please enter the complete sales order number in the box below and click "Submit"'
            Width="293px"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="txt_DEL_DOC_NUM" runat="server" CssClass="style5"></asp:TextBox>&nbsp;&nbsp;
        <asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="style5" Width="83px" />
        <asp:HyperLink ID="hpl_find_invoice" runat="server" NavigateUrl="../SalesOrderMaintenance.aspx"><img src="../images/icons/find.gif" style="width: 15px; height: 15px" border="0" /></asp:HyperLink>
        <br />
        <br />
        <br />
        <sup>*By clicking the lookup button, you will be taken to the Sales Order Maintenance
            Screen which will allow you to search and print invoices.</sup>
    </div>
    <div runat="server" id="invoice_body">
        &nbsp;&nbsp;&nbsp;&nbsp;<br />
    </div>
</asp:Content>




