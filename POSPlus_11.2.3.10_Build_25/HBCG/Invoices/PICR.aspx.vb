Imports System.Data
Imports System.Data.OracleClient

Partial Class Invoices_PICR
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Sales Order Found.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else
            ' Create a report.
            Dim Report As New PICR

            ' Create a connection.
            Dim Connection As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            Connection.Open()

            Dim sql As String
            sql = "SELECT SO.*, ITM.VSN, ITM.DES, EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME, SO_LN.ITM_CD, SO_LN.VOID_FLAG, SO_LN.STORE_CD, SO_LN.LOC_CD, "
            sql = sql & "SO_LN.UNIT_PRC, SO_LN.QTY, ITM.SIZ, SO_LN.DISC_AMT, SO_LN.DEL_DOC_LN#, SO_LN.QTY*SO_LN.UNIT_PRC As EXT_PRC, "
            sql = sql & "CUST.FNAME, CUST.LNAME, CUST.ADDR1, CUST.ADDR2, CUST.CITY, CUST.ST_CD, CUST.ZIP_CD, CUST.HOME_PHONE, CUST.BUS_PHONE, "
            sql = sql & "STORE.STORE_NAME, 'Tel. ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 || ', ' || STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS STORE_ADDRESS "
            sql = sql & "FROM SO, SO_LN, ITM, EMP, STORE, STORE_PHONE, CUST WHERE SO.DEL_DOC_NUM='" & DEL_DOC_NUM & "' "
            sql = sql & "AND SO_LN.ITM_CD=ITM.ITM_CD "
            sql = sql & "AND CUST.CUST_CD=SO.CUST_CD "
            sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD1  "
            sql = sql & "AND SO.SO_STORE_CD=STORE.STORE_CD "
            sql = sql & "AND SO.SO_STORE_CD=STORE_PHONE.STORE_CD(+) "
            sql = sql & "AND STORE_PHONE.PHONE_TP_CD(+)='PHN' "
            sql = sql & "AND SO_LN.VOID_FLAG(+)='N' "
            sql = sql & "AND SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM(+) "
            sql = sql & "ORDER BY SO.DEL_DOC_NUM, SO_LN.DEL_DOC_LN#"

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            ' Create a data adapter and a dataset.
            objSql = DisposablesManager.BuildOracleCommand(sql, Connection)
            Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
            Dim DataSet1 As New DataSet()
            Dim Proceed As Boolean = False

            ' Specify the data adapter and the data source for the report.
            ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
            Report.DataAdapter = Adapter
            Adapter.Fill(DataSet1, "SO")

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If MyDataReader.Item("DEL_DOC_NUM").ToString & "" <> "" Then Proceed = True
                End If

                'Close Connection 
                MyDataReader.Close()
                Connection.Close()
            Catch ex As Exception
                Connection.Close()
                Throw
            End Try

            If Proceed = False Then
                Response.Write("No Sales Order Found.")
                ReportToolbar1.Enabled = False
                ReportToolbar1.Visible = False
                ReportViewer1.Visible = False
            Else
                Report.DataSource = DataSet1
                Report.DataMember = "SO"
                Report.BindSOVariables()
                Report.Name = DEL_DOC_NUM

                ReportViewer1.Report = Report
                ReportViewer1.WritePdfTo(Page.Response)

            End If

        End If

    End Sub

End Class
