Imports System.Data
Imports System.Data.OracleClient
Imports Email_Functions
Imports onBase

Partial Class Reports_RothmanInvoice
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        Dim EMAIL As String = Request("EMAIL")
        Dim onBase As String = ConfigurationManager.AppSettings("onbase").ToString = "Y"
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Sales Order Found.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
        Else
            ' Create a report.
            Dim Report As New RothmanInvoice
            Report.Landscape = True

            ' Create a connection.
            Dim Connection As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            Connection.Open()

            Dim sqlSb As New StringBuilder
            sqlSb.Append("SELECT SO.*, ITM.VSN, ITM.DES, SO.SHIP_TO_L_NAME || ', ' || SO.SHIP_TO_F_NAME AS SHIP_TO_FULL_NAME, EMP.FNAME || ' ' || EMP.LNAME AS SLSP1_FULL_NAME, ").Append(
                    "SO_LN.ITM_CD, SO_LN.VOID_FLAG, SO_LN.STORE_CD, SO_LN.LOC_CD, e2.FNAME || ' ' || e2.LNAME AS SLSP2_FULL_NAME, srt.des AS srt_des, itm.ve_cd, ").Append(
                    "SO_LN.UNIT_PRC, SO_LN.QTY, ITM.SIZ, NVL(SO_LN.DISC_AMT,0), SO_LN.DEL_DOC_LN#, SO_LN.QTY*SO_LN.UNIT_PRC As EXT_PRC, SO_LN.UNIT_PRC + NVL(SO_LN.DISC_AMT,0) + NVL(SO_LN.PU_DISC_AMT,0) AS PRE_DISC_PRC, ").Append(
                    "CUST.FNAME, CUST.LNAME, CUST.ADDR1, CUST.ADDR2, CUST.CITY, CUST.ST_CD, CUST.ZIP_CD, CUST.HOME_PHONE, CUST.BUS_PHONE, store.co_cd, ").Append(
                    "STORE.STORE_NAME, 'PHONE: ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 AS STORE_ADDR1, STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS store_city_st_zip ").Append(
                "FROM SO, SO_LN, ITM, EMP, STORE, STORE_PHONE, CUST, emp e2, ord_srt_cd srt WHERE SO.DEL_DOC_NUM='" & DEL_DOC_NUM & "' ").Append(
                    "AND SO_LN.ITM_CD=ITM.ITM_CD ").Append(
                    "AND CUST.CUST_CD=SO.CUST_CD ").Append(
                    "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD1  ").Append(
                    "AND E2.EMP_CD (+) = SO.SO_EMP_SLSP_CD2 ").Append(
                    "AND SO.SO_STORE_CD=STORE.STORE_CD ").Append(
                    "AND SO.SO_STORE_CD=STORE_PHONE.STORE_CD(+) ").Append(
                    "AND SO.ord_srt_CD = srt.ord_srt_cd(+) ").Append(
                    "AND STORE_PHONE.PHONE_TP_CD(+)='PH' ").Append(
                    "AND SO_LN.VOID_FLAG(+)='N' ").Append(
                    "AND SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM(+) ").Append(
                    "ORDER BY SO.DEL_DOC_NUM, SO_LN.DEL_DOC_LN#")

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            ' Create a data adapter and a dataset.
            objSql = DisposablesManager.BuildOracleCommand(sqlSb.ToString, Connection)
            Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
            Dim DataSet1 As New DataSet()
            Dim Proceed As Boolean = False

            ' Specify the data adapter and the data source for the report.
            ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
            Report.DataAdapter = Adapter
            Adapter.Fill(DataSet1, "SO")

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If MyDataReader.Item("DEL_DOC_NUM").ToString & "" <> "" Then Proceed = True
                End If

                'Close Connection 
                MyDataReader.Close()
                Connection.Close()
            Catch ex As Exception
                Connection.Close()
                Throw
            End Try

            If Proceed = False Then
                Response.Write("No Sales Order Found.")
                ReportToolbar1.Enabled = False
                ReportToolbar1.Visible = False
                ReportViewer1.Visible = False
            Else
                Report.DataSource = DataSet1
                Report.DataMember = "SO"
                Report.BindSOVariables()
                Report.Name = DEL_DOC_NUM

                If Not String.IsNullOrEmpty(EMAIL) Or ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                    Try
                        Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        If TheFile.Exists Then
                            System.IO.File.Delete("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        End If
                    Catch ex As Exception

                    End Try
                    Dim ms As New System.IO.MemoryStream()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.Private)

                    Report.ExportToPdf(ms)
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", String.Format("inline;filename=" & DEL_DOC_NUM & ".pdf"))
                    ms.Seek(0, System.IO.SeekOrigin.Begin)
                    Response.BinaryWrite(ms.ToArray())
                    Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                    fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                    fs.Close()
                    ms.Close()
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Try
                        If EMAIL = "True" Then
                            SendMailMessage("c:\Invoices\" & DEL_DOC_NUM & ".pdf")
                        End If
                        If ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                            Import_To_Onbase(DEL_DOC_NUM & ".pdf", "invoice", DEL_DOC_NUM)
                        End If
                    Catch ex As Exception

                    End Try
                End If

                ReportViewer1.Report = Report
                ReportViewer1.WritePdfTo(Page.Response)

            End If

        End If

    End Sub

End Class
