﻿Imports DevExpress.Web.Data
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxEditors
Imports System.Web.UI


Imports HBCG_Utils
''' <summary>
''' Jun 20 2018, alice - Website Promotional Pricing Exception
''' </summary>
''' <remarks></remarks>
Partial Class WebsitePromotionalPricingException
    Inherits POSBasePage

    Protected objFrPOBiz As FranchisePOBiz = New FranchisePOBiz()
    Const _MAX_LINE_ITEMS As Int16 = 20

#Region "initialization"

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            SetInitialRow()
        End If

    End Sub
    Public Sub SetInitialRow()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow = Nothing
        dt.Columns.Add(New DataColumn("RowNumber", GetType(String)))
        dt.Columns.Add(New DataColumn("Col1", GetType(String)))
        dt.Columns.Add(New DataColumn("Col2", GetType(String)))
        dt.Columns.Add(New DataColumn("Col3", GetType(String)))
        dt.Columns.Add(New DataColumn("Col4", GetType(String)))
        dt.Columns.Add(New DataColumn("Col5", GetType(String)))
        dt.Columns.Add(New DataColumn("Col6", GetType(String)))
        dt.Columns.Add(New DataColumn("Col7", GetType(String)))
        dt.Columns.Add(New DataColumn("Col8", GetType(String)))
        dt.Columns.Add(New DataColumn("Col9", GetType(String)))
        dt.Columns.Add(New DataColumn("Col10", GetType(String)))
        dt.Columns.Add(New DataColumn("Col11", GetType(String)))
        dt.Columns.Add(New DataColumn("Col12", GetType(String)))
        dt.Columns.Add(New DataColumn("Col13", GetType(String)))
        dt.Columns.Add(New DataColumn("Col14", GetType(String)))
        dt.Columns.Add(New DataColumn("Col15", GetType(String)))
        dt.Columns.Add(New DataColumn("Col16", GetType(String)))


        dr = dt.NewRow()
        dr("RowNumber") = 1
        dr("Col1") = String.Empty
        dr("Col2") = String.Empty
        dr("Col3") = String.Empty
        dr("Col4") = String.Empty
        dr("Col5") = String.Empty
        dr("Col6") = String.Empty
        dr("Col7") = String.Empty
        dr("Col8") = String.Empty
        dr("Col9") = String.Empty
        dr("Col10") = String.Empty
        dr("Col11") = String.Empty
        dr("Col12") = String.Empty
        dr("Col13") = String.Empty
        dr("Col14") = String.Empty
        dr("Col15") = String.Empty
        dr("Col16") = String.Empty

        dt.Rows.Add(dr)

        ViewState("CurrentTable") = dt

        grvItems.DataSource = dt
        grvItems.DataBind()

        Session("linenumber") = 1

    End Sub
#End Region


#Region "create/query radio button function"

    Protected Sub rdbCreate_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("MODE") = "CREATE"
        dvCreate.Style("display") = "block"

        btnSearch.Visible = False
        btnClear.Visible = False
        lblQuery_CompanyCode.Visible = False
        txtQuery_CompanyCode.Visible = False
        lblQuery_promotionCode.Visible = False
        txtQuery_PromotionCode.Visible = False
        lblQuery_sku.Visible = False
        txtQuery_sku.Visible = False
        lblQuery_vsn.Visible = False
        txtQuery_vsn.Visible = False
        lblQuery_eff_dt.Visible = False
        txtQuery_eff_dt.Visible = False
        lblQuery_end_dt.Visible = False
        txtQuery_end_dt.Visible = False

        lblMessage.Text = String.Empty

        dvQuery.Style("display") = "none"


    End Sub


    Protected Sub rdbQuery_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("MODE") = "QUERY"
        dvCreate.Style("display") = "none"

        btnClear_Click(sender, e)

        btnSearch.Visible = True
        btnClear.Visible = True


        lblQuery_CompanyCode.Visible = True
        txtQuery_CompanyCode.Visible = True
        lblQuery_promotionCode.Visible = True
        txtQuery_PromotionCode.Visible = True
        lblQuery_sku.Visible = True
        txtQuery_sku.Visible = True
        lblQuery_vsn.Visible = True
        txtQuery_vsn.Visible = True
        lblQuery_eff_dt.Visible = True
        txtQuery_eff_dt.Visible = True
        lblQuery_end_dt.Visible = True
        txtQuery_end_dt.Visible = True


        dvQuery.Style("display") = "block"

    End Sub

#End Region


#Region "Add new row function"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)

        If Convert.ToInt16(Session("linenumber")) = _MAX_LINE_ITEMS Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Maximum Items to Order is " & _MAX_LINE_ITEMS
        Else
            AddNewRow()
            Session("linenumber") = Convert.ToInt16(Session("linenumber")) + 1
        End If


    End Sub

    Private Sub AddNewRow()
        Dim rowIndex As Integer = 0

        Try
            If Not ViewState("CurrentTable") Is Nothing Then

                Dim dtCurrentTable As System.Data.DataTable = CType(ViewState("CurrentTable"), DataTable)
                Dim drCurrentRow As DataRow = Nothing

                If dtCurrentTable.Rows.Count > 0 Then

                    Dim i As Integer
                    For i = 1 To dtCurrentTable.Rows.Count Step i + 1

                        ''extract the TextBox values

                        Dim box_co_cd As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_co_cd"), TextBox)
                        Dim box_prc_cd As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txt_prc_cd"), TextBox)
                        Dim box_desc As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txt_Desc"), TextBox)
                        Dim box_sku As TextBox = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("txt_SKU"), TextBox)
                        Dim box_svn As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txt_VSN"), TextBox)
                        Dim box_corp As TextBox = CType(grvItems.Rows(rowIndex).Cells(6).FindControl("txt_corporate"), TextBox)
                        Dim box_fran_corp As TextBox = CType(grvItems.Rows(rowIndex).Cells(7).FindControl("txt_franchise_corporate"), TextBox)
                        Dim box_fran As TextBox = CType(grvItems.Rows(rowIndex).Cells(8).FindControl("txt_franchise"), TextBox)
                        Dim box_quebec As TextBox = CType(grvItems.Rows(rowIndex).Cells(9).FindControl("txt_quebec"), TextBox)
                        Dim box_que_fran As TextBox = CType(grvItems.Rows(rowIndex).Cells(10).FindControl("txt_que_fran"), TextBox)
                        Dim box_other As TextBox = CType(grvItems.Rows(rowIndex).Cells(11).FindControl("txt_other"), TextBox)
                        Dim box_eff_date As ASPxDateEdit = CType(grvItems.Rows(rowIndex).Cells(12).FindControl("txt_eff_date"), ASPxDateEdit)
                        Dim box_end_date As ASPxDateEdit = CType(grvItems.Rows(rowIndex).Cells(13).FindControl("txt_end_date"), ASPxDateEdit)
                        Dim box_associate As TextBox = CType(grvItems.Rows(rowIndex).Cells(14).FindControl("txt_associate"), TextBox)
                        Dim box_upd_date As TextBox = CType(grvItems.Rows(rowIndex).Cells(15).FindControl("txt_upd_date"), TextBox)
                        Dim box_upd_time As TextBox = CType(grvItems.Rows(rowIndex).Cells(16).FindControl("txt_upd_time"), TextBox)

                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = i + 1

                        dtCurrentTable.Rows(i - 1)("Col1") = box_co_cd.Text
                        dtCurrentTable.Rows(i - 1)("Col2") = box_prc_cd.Text
                        dtCurrentTable.Rows(i - 1)("Col3") = box_desc.Text
                        dtCurrentTable.Rows(i - 1)("Col4") = box_sku.Text
                        dtCurrentTable.Rows(i - 1)("Col5") = box_svn.Text
                        dtCurrentTable.Rows(i - 1)("Col6") = box_corp.Text
                        dtCurrentTable.Rows(i - 1)("Col7") = box_fran_corp.Text
                        dtCurrentTable.Rows(i - 1)("Col8") = box_fran.Text
                        dtCurrentTable.Rows(i - 1)("Col9") = box_quebec.Text
                        dtCurrentTable.Rows(i - 1)("Col10") = box_que_fran.Text
                        dtCurrentTable.Rows(i - 1)("Col11") = box_other.Text
                        dtCurrentTable.Rows(i - 1)("Col12") = box_eff_date.Text
                        dtCurrentTable.Rows(i - 1)("Col13") = box_end_date.Text
                        dtCurrentTable.Rows(i - 1)("Col14") = box_associate.Text
                        dtCurrentTable.Rows(i - 1)("Col15") = box_upd_date.Text
                        dtCurrentTable.Rows(i - 1)("Col16") = box_upd_time.Text
                        rowIndex = rowIndex + 1

                    Next

                    dtCurrentTable.Rows.Add(drCurrentRow)
                    ViewState("CurrentTable") = dtCurrentTable


                    grvItems.DataSource = dtCurrentTable
                    grvItems.DataBind()

                End If
            Else
                Response.Write("ViewState is null")
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
        SetPreviousData()
    End Sub

    Private Sub SetPreviousData()
        Dim rowIndex As Integer = 0

        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            If dt.Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To dt.Rows.Count - 1 Step i + 1

                    Dim box_co_cd As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_co_cd"), TextBox)
                    Dim box_prc_cd As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txt_prc_cd"), TextBox)
                    Dim box_desc As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txt_Desc"), TextBox)
                    Dim box_sku As TextBox = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("txt_SKU"), TextBox)
                    Dim box_svn As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txt_VSN"), TextBox)
                    Dim box_corp As TextBox = CType(grvItems.Rows(rowIndex).Cells(6).FindControl("txt_corporate"), TextBox)
                    Dim box_corp_fran As TextBox = CType(grvItems.Rows(rowIndex).Cells(7).FindControl("txt_franchise_corporate"), TextBox)
                    Dim box_fran As TextBox = CType(grvItems.Rows(rowIndex).Cells(8).FindControl("txt_franchise"), TextBox)
                    Dim box_que As TextBox = CType(grvItems.Rows(rowIndex).Cells(9).FindControl("txt_quebec"), TextBox)
                    Dim box_que_fran As TextBox = CType(grvItems.Rows(rowIndex).Cells(10).FindControl("txt_que_fran"), TextBox)
                    Dim box_other As TextBox = CType(grvItems.Rows(rowIndex).Cells(11).FindControl("txt_other"), TextBox)
                    Dim box_eff_date As ASPxDateEdit = CType(grvItems.Rows(rowIndex).Cells(12).FindControl("txt_eff_date"), ASPxDateEdit)
                    Dim box_end_date As ASPxDateEdit = CType(grvItems.Rows(rowIndex).Cells(13).FindControl("txt_end_date"), ASPxDateEdit)
                    Dim box_upd_date As TextBox = CType(grvItems.Rows(rowIndex).Cells(15).FindControl("txt_upd_date"), TextBox)
                    Dim box_upd_time As TextBox = CType(grvItems.Rows(rowIndex).Cells(16).FindControl("txt_upd_time"), TextBox)

                    box_co_cd.Text = Session("CO_CD")
                    box_prc_cd.Text = dt.Rows(i)("Col2").ToString()
                    box_desc.Text = dt.Rows(i)("Col3").ToString()
                    box_sku.Text = dt.Rows(i)("Col4").ToString()
                    box_svn.Text = dt.Rows(i)("Col5").ToString()
                    box_corp.Text = dt.Rows(i)("Col6").ToString()
                    box_corp_fran.Text = dt.Rows(i)("Col7").ToString()
                    box_fran.Text = dt.Rows(i)("Col8").ToString()
                    box_que.Text = dt.Rows(i)("Col9").ToString()
                    box_que_fran.Text = dt.Rows(i)("Col10").ToString()
                    box_other.Text = dt.Rows(i)("Col11").ToString()
                    box_eff_date.Text = dt.Rows(i)("Col12").ToString()
                    box_end_date.Text = dt.Rows(i)("Col13").ToString()
                    box_upd_date.Text = Today.Date.ToString("dd-MMM-yyyy")
                    box_upd_time.Text = Format(Now, "HH:mm:ss")

                    rowIndex = rowIndex + 1
                Next
            End If
        End If

    End Sub

#End Region

#Region "gridview function"
    Protected Sub grvItems_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)

        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            Dim drCurrentRow As DataRow = Nothing
            Dim rowIndex As Integer = Convert.ToInt32(e.RowIndex)
            If dt.Rows.Count > 1 Then
                dt.Rows.Remove(dt.Rows(rowIndex))
                drCurrentRow = dt.NewRow()
                ViewState("CurrentTable") = dt
                grvItems.DataSource = dt
                grvItems.DataBind()

                Dim i As Integer
                For i = 0 To grvItems.Rows.Count - 1 - 1 Step i + 1
                    grvItems.Rows(i).Cells(0).Text = Convert.ToString(i + 1)
                Next
                SetPreviousData()
            End If
        End If
    End Sub

    '''''Summary
    ''''' Bind Store Group to dropdownlist, add limitation for date controls
    '''''Summary
    Protected Sub grvItems_OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim Co_cd As TextBox = CType(e.Row.FindControl("txt_co_cd"), TextBox)
            Co_cd.Text = Session("CO_CD")


            'Dim ddlStoreGroup As DropDownList = CType(e.Row.FindControl("ddlStoreGroup"), DropDownList)
            'Dim ds As DataSet = WPPEBiz.GetWPPEStoreGroup(Co_cd.Text)

            'If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
            '    With ddlStoreGroup
            '        .DataSource = ds
            '        .DataValueField = "store_grp_cd"
            '        .DataTextField = "name"
            '        .DataBind()
            '    End With
            'End If

            Dim effective_date As ASPxDateEdit = CType(e.Row.FindControl("txt_eff_date"), ASPxDateEdit)
            effective_date.MinDate = Date.Today.AddDays(1)

            Dim end_date As ASPxDateEdit = CType(e.Row.FindControl("txt_end_date"), ASPxDateEdit)
            end_date.MinDate = Date.Today.AddDays(1)

            Dim updated_date As TextBox = CType(e.Row.FindControl("txt_upd_date"), TextBox)
            updated_date.Text = Today.Date.ToString("dd-MMM-yyyy")

            Dim updated_time As TextBox = CType(e.Row.FindControl("txt_upd_time"), TextBox)
            updated_time.Text = Format(Now, "HH:mm:ss")




        End If
    End Sub
#End Region

#Region "Approver password an commit"

    Public Sub txtApprover_Pswd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim isValid As String = String.Empty

        If txtApprover_Pswd.Text.Length > 0 Then
            isValid = objFrPOBiz.isvalidApproverPassword(txtApprover_Pswd.Text)

            If isValid = "N" Or String.IsNullOrEmpty(isValid) Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "This Approver Password is not Valid or Authorized"
                Return
            Else
                'Get Approver User Init
                'For Each row As GridViewRow In grvItems.Rows
                '    Dim txt_Associate As TextBox = CType(row.FindControl("txt_associate"), TextBox)
                '    txt_Associate.Text = objFrPOBiz.GetApprUserInitByPass(txtApprover_Pswd.Text)
                'Next
            End If
        End If
    End Sub
    'Confirm 
    Public Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmASP.Click

        Dim lstDetails As New List(Of WPPEBiz)
        Try
            'Validate Required Fields
            If IsValidRequiredFields() Then

                If grvItems.Rows.Count > 0 Then

                    For i As Integer = 0 To grvItems.Rows.Count - 1
                        Dim objDetails As WPPEBiz = New WPPEBiz

                        objDetails.co_cd = CType(grvItems.Rows(i).Cells(1).FindControl("txt_co_cd"), TextBox).Text
                        objDetails.prc_cd = CType(grvItems.Rows(i).Cells(2).FindControl("txt_prc_cd"), TextBox).Text
                        objDetails.itm_cd = CType(grvItems.Rows(i).Cells(4).FindControl("txt_SKU"), TextBox).Text
                        objDetails.corp = Decimal.Parse(CType(grvItems.Rows(i).Cells(6).FindControl("txt_corporate"), TextBox).Text)
                        objDetails.corp_fran = Decimal.Parse(CType(grvItems.Rows(i).Cells(7).FindControl("txt_franchise_corporate"), TextBox).Text)
                        objDetails.fran = Decimal.Parse(CType(grvItems.Rows(i).Cells(8).FindControl("txt_franchise"), TextBox).Text)
                        objDetails.que = Decimal.Parse(CType(grvItems.Rows(i).Cells(9).FindControl("txt_quebec"), TextBox).Text)
                        objDetails.que_fran = Decimal.Parse(CType(grvItems.Rows(i).Cells(10).FindControl("txt_que_fran"), TextBox).Text)
                        objDetails.other = Decimal.Parse(CType(grvItems.Rows(i).Cells(11).FindControl("txt_other"), TextBox).Text)
                        objDetails.eff_dt = CType(grvItems.Rows(i).Cells(12).FindControl("txt_eff_date"), ASPxDateEdit).Text
                        objDetails.end_dt = CType(grvItems.Rows(i).Cells(13).FindControl("txt_end_date"), ASPxDateEdit).Text
                        objDetails.emp_cd = objFrPOBiz.GetApprUserInitByPass(txtApprover_Pswd.Text.Trim())
                        objDetails.upd_dt = CType(grvItems.Rows(i).Cells(15).FindControl("txt_upd_date"), TextBox).Text
                        objDetails.upd_time = CType(grvItems.Rows(i).Cells(16).FindControl("txt_upd_time"), TextBox).Text
                        lstDetails.Add(objDetails)
                    Next

                    If WPPEBiz.CreateWPPE(lstDetails) Then
                        lblMessage.ForeColor = Color.Green
                        lblMessage.Text = "Successfully Created Website Promotional Pricing Exception"
                    Else
                        lblMessage.ForeColor = Color.Red
                        lblMessage.Text = "Error While Created Website Promotional Pricing Exception"
                    End If

                Else
                    lblMessage.ForeColor = Color.Red
                    lblMessage.Text = "Error occurred at Confirm click"
                End If

            End If
        Catch Ex As Exception
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Error occurred at Confirm click"
        End Try
    End Sub
    'Validate Required Fields
    Public Function IsValidRequiredFields() As Boolean
        Dim result As Boolean = True
        Dim requiredMsg As String = String.Empty

        Try

            If txtApprover_Pswd.Text.Trim.Length = 0 Then
                requiredMsg = "Approver Password is Mandatory"
                result = False
            End If

            For i As Integer = 0 To grvItems.Rows.Count - 1
                Dim co_cd As String = CType(grvItems.Rows(i).Cells(1).FindControl("txt_co_cd"), TextBox).Text.Trim()
                Dim prc_cd As String = CType(grvItems.Rows(i).Cells(3).FindControl("txt_prc_cd"), TextBox).Text.Trim()
                Dim itm_cd As String = CType(grvItems.Rows(i).Cells(5).FindControl("txt_SKU"), TextBox).Text.Trim()
                Dim eff_dt As String = CType(grvItems.Rows(i).Cells(8).FindControl("txt_eff_date"), ASPxDateEdit).Text.Trim()
                Dim end_dt As String = CType(grvItems.Rows(i).Cells(9).FindControl("txt_end_date"), ASPxDateEdit).Text.Trim()

                If co_cd = String.Empty Then
                    requiredMsg = requiredMsg & "<br/>Company code is required for line " & i + 1
                    result = False
                End If

                If prc_cd = String.Empty Then
                    requiredMsg = requiredMsg & "<br/>Promotion code is required for line " & i + 1
                    result = False
                End If

                If itm_cd = String.Empty Then
                    requiredMsg = requiredMsg & "<br/>SKU is required for line " & i + 1
                    result = False
                End If


                If eff_dt = String.Empty Then
                    requiredMsg = requiredMsg & "<br/>Effective Date is required for line " & i + 1
                    result = False
                End If

                If end_dt = String.Empty Then
                    requiredMsg = requiredMsg & "<br/>Ending Date is required for line " & i + 1
                    result = False
                End If
            Next

        Catch ex As Exception
            result = False
            requiredMsg = "Error occurred at Confirm click"
        End Try
        lblMessage.ForeColor = Color.Red
        lblMessage.Text = requiredMsg
        Return result
    End Function

#End Region

#Region "Query and Clear function"
    Public Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Text = String.Empty
        BindSearchResult()
        dvQuery.Style("display") = "block"
    End Sub

    Private Sub BindSearchResult()

        Dim objTrans As WPPEBiz = New WPPEBiz
        Dim ds As DataSet = New DataSet

        Try

            'Get search criteria from controls
            objTrans.co_cd = txtQuery_CompanyCode.Text.Trim()
            objTrans.prc_cd = txtQuery_PromotionCode.Text.Trim()
            objTrans.itm_cd = txtQuery_sku.Text.Trim()
            objTrans.eff_dt = txtQuery_eff_dt.Text.Trim()
            objTrans.end_dt = txtQuery_end_dt.Text.Trim()
            Dim VSN As String = txtQuery_vsn.Text.Trim()


            ds = WPPEBiz.GetSearchResult(objTrans, VSN)
            gvMain.Visible = True
            gvMain.DataSource = ds
            gvMain.DataBind()



            lbl_pageinfo.Text = "Page " + CStr(gvMain.PageIndex + 1) + " of " + CStr(gvMain.PageCount)
            ' make all the buttons visible if page count is more than 0
            If gvMain.PageCount > 0 Then
                btnFirst.Visible = True
                btnPrev.Visible = True
                btnNext.Visible = True
                btnLast.Visible = True
                lbl_pageinfo.Visible = True
            End If

            ' turn all buttons on by default
            btnFirst.Enabled = True
            btnPrev.Enabled = True
            btnNext.Enabled = True
            btnLast.Enabled = True


            ' then turn off buttons that don't make sense
            If gvMain.PageCount = 1 Then
                ' only 1 page of data
                btnFirst.Enabled = False
                btnPrev.Enabled = False
                btnNext.Enabled = False
                btnLast.Enabled = False
            Else
                If gvMain.PageIndex = 0 Then
                    ' first page
                    btnFirst.Enabled = False
                    btnPrev.Enabled = False
                Else
                    If gvMain.PageIndex = (gvMain.PageCount - 1) Then
                        ' last page
                        btnNext.Enabled = False
                        btnLast.Enabled = False
                    End If
                End If
            End If
            If gvMain.PageCount = 0 Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Sorry, no records found.  Please search again." & vbCrLf & vbCrLf

            End If
        Catch ex As Exception

        End Try
    End Sub
    'Query - Page Navigation
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If gvMain.PageIndex < (gvMain.PageCount - 1) Then
                    gvMain.PageIndex += 1
                End If
            Case "Prev"
                If gvMain.PageIndex > 0 Then
                    gvMain.PageIndex -= 1
                End If
            Case "Last"
                gvMain.PageIndex = gvMain.PageCount - 1
            Case Else
                gvMain.PageIndex = 0
        End Select
        BindSearchResult()

    End Sub


    'Clear Control Text
    Public Sub btnClear_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const _CREATE As String = "CREATE"
        Const _QUERY As String = "QUERY"

        lblMessage.Text = String.Empty
        txtQuery_CompanyCode.Text = String.Empty
        txtQuery_sku.Text = String.Empty
        txtQuery_vsn.Text = String.Empty
        txtQuery_eff_dt.Text = String.Empty
        txtQuery_end_dt.Text = String.Empty

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_QUERY)) Then

            gvMain.DataSource = Nothing
            gvMain.DataBind()
            dvQuery.Style("display") = "none"

        End If

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_CREATE)) Then

            txtApprover_Pswd.Text = String.Empty
            grvItems.DataSource = Nothing
            grvItems.DataBind()
            SetInitialRow()
        End If
    End Sub
#End Region



#Region "Textbox change function"

    Public Sub txtCoCd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty
        Try
            Dim txt As TextBox = (CType((sender), TextBox))
            Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
            Dim rowIndex = gv.RowIndex

            Dim txtCoCd As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_co_cd"), TextBox)
            txtCoCd.Text = txtCoCd.Text.ToString().Trim().ToUpper()

            'Validate Company code, apply MCCL Logic Type 1

            Dim isValidCoCd As String = WPPEBiz.isValid_co_cd(txtCoCd.Text, Session("emp_init"))

            If isValidCoCd = "N" Or String.IsNullOrEmpty(isValidCoCd) Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Company Code - " & txtCoCd.Text & " - is NOT Valid"
                txtCoCd.Text = String.Empty
                Return
            Else
                lblMessage.Text = String.Empty

            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
    Public Sub txt_prc_cd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Get promotion description based on promotion code
        Dim IsValidPromotionCode As String = "N"
        Dim po_cd As String = String.Empty
        Dim ds As DataSet

        lblMessage.Text = String.Empty
        Try
            Dim txt As TextBox = (CType((sender), TextBox))
            Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
            Dim rowIndex = gv.RowIndex


            Dim txtPromotionCode As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txt_prc_cd"), TextBox)
            Dim txtPromotionDesc As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txt_Desc"), TextBox)

            po_cd = txtPromotionCode.Text.ToString().Trim().ToUpper()


            'Validate promotion code
            IsValidPromotionCode = WPPEBiz.isvalidPromotionCode(po_cd)

            If IsValidPromotionCode = "N" Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Promotion Code - " & po_cd & " - is NOT Valid"
                txtPromotionCode.Text = String.Empty
                txtPromotionDesc.Text = String.Empty
                Return
            Else
                ds = WPPEBiz.GetPromotionDesc(po_cd)

                If ds.Tables(0).Rows.Count > 0 Then
                    txtPromotionDesc.Text = ds.Tables(0).Rows(0)("Des")
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    Public Sub txtCorpSKU_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'GetVSN based on Corp SKU#
        Dim itemSKU As String = String.Empty
        Dim ds As DataSet
        Dim strItemSKU As String = "N"
        lblMessage.Text = String.Empty
        Try
            Dim txt As TextBox = (CType((sender), TextBox))
            Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
            Dim rowIndex = gv.RowIndex

            Dim txtCorpSku As TextBox = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("txt_SKU"), TextBox)
            Dim txtvsn As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txt_VSN"), TextBox)

            itemSKU = txtCorpSku.Text.ToString().Trim()


            'Validate Item SKU, apply std_multi_co
            strItemSKU = WPPEBiz.isvalidSKU(itemSKU, Session("emp_init"))

            If strItemSKU = "N" Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "SKU - " & itemSKU & " - is NOT Valid or Authorized"
                txtCorpSku.Text = String.Empty
                txtvsn.Text = String.Empty
                Return
            Else
                ds = objFrPOBiz.GetVSNByItemSKU(itemSKU)      'Get VSN No based on Item SKU typed

                If ds.Tables(0).Rows.Count > 0 Then
                    txtvsn.Text = ds.Tables(0).Rows(0)("VSN")
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    Public Sub txt_corp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_corporate"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Corporate price value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub
    Public Sub txt_corp_fran_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_franchise_corporate"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Franchise using corporate pricing value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub
    Public Sub txt_fran_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_franchise"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Franchise price value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub
    Public Sub txt_que_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_quebec"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Quebec price value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub
    Public Sub txt_que_fran_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_que_fran"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Quebec franchise price value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub
    Public Sub txt_other_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = String.Empty

        Dim txt As TextBox = (CType((sender), TextBox))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        Dim txtPrice As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txt_other"), TextBox)

        If Not IsValidPrice(txtPrice.Text) Then
            lblMessage.ForeColor = Color.Red
            lblMessage.Text = "Other price value should be maximum of 10 characters including up to 2 decimal places"
            txtPrice.Text = String.Empty
            Return
        Else
            lblMessage.Text = String.Empty

        End If

    End Sub

    Public Function IsValidPrice(ByVal strPrice As String) As Boolean
        strPrice = strPrice.Trim()
        Dim strPattern As Regex = New Regex("^(-)?(([1-9]{1}\d*)|([0]{1}))(\.(\d){1,2})?$")
        Dim m As Match = strPattern.Match(strPrice)
        If m.Success Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region



End Class
