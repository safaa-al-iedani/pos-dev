<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" ValidateRequest="false"
    CodeFile="FinanceSellReportCorrection.aspx.vb" Inherits="FinanceSellReportCorrection" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="position: relative; width: 100%; top: 0px;">
        <tr>
            <%-- Finance Selling Report Corrections --%>
            <td style="width: 200px">
                <dx:ASPxLabel ID="ASPxLabel4" Text="<%$ Resources:LibResources, Label941 %>" Font-Bold="true" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
            </td>
            </tr>
        </table>
     <br />
        <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            
            <td>
            <asp:Label ID="Label2" Font-Bold="true"  runat="server" width="110px" style="text-align: start" Text="<%$ Resources:LibResources, Label877 %>" ></asp:Label>
            <asp:Calendar ID="post_dt" Font-Size="6pt" ForeColor="Black" Height="100px" Width="120px" runat="server"></asp:Calendar>
           </td>
                
            
            <td>
                <%-- Company code --%>
                <dx:ASPxLabel ID="ASPxLabel1" Text="<%$ Resources:LibResources, Label100 %>" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
           
                     <asp:DropDownList ID="srch_co_cd" runat="server"  Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>
                        
             <td>
                 <%-- Store  --%>
                <dx:ASPxLabel ID="ASPxLabel2" Text="<%$ Resources:LibResources, Label572 %>" Font-Bold="true" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
           
                <asp:DropDownList ID="srch_str_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">                </asp:DropDownList>
            </td>
               
           
        </tr>
         </table>
   
    <br />
      <table>
        <tr>
           
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label887 %>">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
         
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
            </td>
           
            
        </tr>
    </table>
     <br />
    <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </dx:ASPxLabel>
    <br />
         <dx:ASPxGridView ID="GridView2" runat="server" KeyFieldName="IVC_CD;CUST_CD;HOLD_BNK_CRD_NUM;ROW_ID" AutoGenerateColumns="False" Width="98%" ClientInstanceName="GridView2"  OnBatchUpdate="GridView2_BatchUpdate" OnParseValue="GridView2_ParseValue">
        <SettingsEditing BatchEditSettings-EditMode="row" Mode="Batch"  />
                  <SettingsCommandButton>
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>" ></UpdateButton>
         <CancelButton Text="<%$ Resources:LibResources, Label886 %>"></CancelButton>
         </SettingsCommandButton>
        <Columns>
            <%-- invoice --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label249 %>" FieldName="IVC_CD" ReadOnly="true" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="14"></PropertiesTextEdit>
                      
            </dx:GridViewDataTextColumn>
            <%-- cust cd --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label130 %>" FieldName="CUST_CD" ReadOnly="true" Visible="True" VisibleIndex="2">
                <PropertiesTextEdit  MaxLength="11">  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- cust name --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label138 %>" FieldName="CUST_NAME" ReadOnly="true" Visible="True" VisibleIndex="3">
                <PropertiesTextEdit MaxLength="30"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- orig card# --%>
             <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label935 %>" FieldName="BNK_CRD_NUM" ReadOnly="true" Visible="True" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="16"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- new card# --%>
             <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label936 %>" FieldName="NEW_CRD_NUM" VisibleIndex="5">
                <PropertiesTextEdit Width="160"  MaxLength="16">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d{16}" ErrorText="<%$ Resources:LibResources, Label909 %>" />

                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- Fin Amount --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label937 %>" FieldName="ORIG_FI_AMT" VisibleIndex="6">
                <PropertiesTextEdit Width="100" MaxLength="10">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="^-?\d*\.{0,1}\d+$" ErrorText="<%$ Resources:LibResources, Label909 %>" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- plan# --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label401 %>" FieldName="FIN_CUST_CD" VisibleIndex="7">
                <PropertiesTextEdit Width="50" MaxLength="5">
                    <ValidationSettings>
                        <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- Auth#  --%>
             <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label874 %>" FieldName="APPROVAL_CD" VisibleIndex="8">
                <PropertiesTextEdit Width="90" MaxLength="9">
                    <ValidationSettings>
                        <RequiredField isrequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- Updated by --%>
             <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label938 %>" ReadOnly="true" FieldName="UPD_EMP_INIT" VisibleIndex="9">
                <PropertiesTextEdit MaxLength="10">  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- Approved by --%>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label880 %>" ReadOnly="true" FieldName="APP_EMP_INIT" VisibleIndex="10">
                <PropertiesTextEdit MaxLength="10">  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%-- HOLD rig card# **sy note ** THIS FIELD IS ADDED TO HOLD THE DECRYPTED CARD# --%>
             <dx:GridViewDataTextColumn Caption="" FieldName="HOLD_BNK_CRD_NUM" ReadOnly="true" Visible="false" VisibleIndex="19">
             </dx:GridViewDataTextColumn>
               <%-- ROWID ** sy note** THIS FIELD IS ADDED TO HANDLE IF HOLD-CARD IS NULL (THESE ARE PART OF KEY FIELD)--%>
             <dx:GridViewDataTextColumn Caption="" FieldName="ROW_ID" ReadOnly="true" Visible="false" VisibleIndex="20">
             </dx:GridViewDataTextColumn>
           
                     
            <%--<dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowDeleteButton="True" VisibleIndex="14"/>--%>
                  
        </Columns>
        
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
       <%-- <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="<%$ Resources:LibResources, Label908 %>" />--%>
    </dx:ASPxGridView>
    <br />
  
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
