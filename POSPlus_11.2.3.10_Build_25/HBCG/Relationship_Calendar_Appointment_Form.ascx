<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Relationship_Calendar_Appointment_Form.ascx.vb"
    Inherits="AppointmentForm" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table class="dxscAppointmentForm" cellpadding="0" cellspacing="0" style="width: 100%;
    height: 230px;">
    <tr>
        <td class="dxscDoubleCell" colspan="2">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell">
                        <dxe:ASPxLabel ID="lblSubject" runat="server" AssociatedControlID="tbSubject" Text="Subject:">
                        </dxe:ASPxLabel>
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxTextBox ClientInstanceName="_dx" ID="tbSubject" runat="server" Width="100%"
                            Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).Appointment.Subject%>'
                            BackColor="Ivory" ReadOnly="True" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="dxscSingleCell">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell">
                        <dxe:ASPxLabel ID="ASPxLabel1" runat="server" AssociatedControlID="tbField1" Text="Corporation:">
                        </dxe:ASPxLabel>
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxTextBox ID="tbField1" runat="server" Width="100%" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).CORP_NAME%>'
                            BackColor="Ivory" ReadOnly="True">
                        </dxe:ASPxTextBox>
                    </td>
                </tr>
            </table>
        </td>
        <td class="dxscSingleCell" colspan="2" align="Right">
            <dxe:ASPxLabel ID="tbField4" runat="server" Font-Size="Medium" Text='<%# FormatCurrency((CType(Container, UserAppointmentFormTemplateContainer)).PROSPECT_TOTAL,2) %>'>
            </dxe:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td class="dxscSingleCell">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell">
                        <dxe:ASPxLabel ID="ASPxLabel2" runat="server" AssociatedControlID="tbField1" Text="Customer:">
                        </dxe:ASPxLabel>
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxTextBox ID="tbField2" runat="server" Width="100%" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).FIRST_NAME%>'
                            BackColor="Ivory" ReadOnly="True">
                        </dxe:ASPxTextBox>
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxTextBox ID="tbField3" runat="server" Width="100%" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).LAST_NAME %>'
                            BackColor="Ivory" ReadOnly="True">
                        </dxe:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td>
                                    <dxe:ASPxTextBox ID="tbField5" runat="server" Width="100%" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).ADDR1 %>'
                                        BackColor="Ivory" ReadOnly="True">
                                    </dxe:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2" align="left">
                        <table width="100%">
                            <tr>
                                <td>
                                    <dxe:ASPxTextBox ID="tbField6" runat="server" Width="100%" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).CITY %>'
                                        BackColor="Ivory" ReadOnly="True">
                                    </dxe:ASPxTextBox>
                                </td>
                                <td>
                                    <dxe:ASPxTextBox ID="tbField7" runat="server" Width="30px" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).ST %>'
                                        BackColor="Ivory" ReadOnly="True">
                                    </dxe:ASPxTextBox>
                                </td>
                                <td>
                                    <dxe:ASPxTextBox ID="tbField8" runat="server" Width="50px" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).ZIP %>'
                                        BackColor="Ivory" ReadOnly="True">
                                    </dxe:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td class="dxscSingleCell" colspan="2">
        </td>
    </tr>
    <tr>
        <td class="dxscSingleCell">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell">
                        &nbsp;
                    </td>
                    <td class="dxscControlCell">
                        <table>
                            <tr>
                                <td>
                                    <dxe:ASPxTextBox ClientInstanceName="_dx" ID="tbLocation" runat="server" Width="100%"
                                        Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).Appointment.Location%>'
                                        BackColor="Ivory" ReadOnly="True" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td class="dxscSingleCell">
        </td>
    </tr>
    <tr>
        <td class="dxscSingleCell">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell">
                        <dxe:ASPxLabel ID="lblStartDate" runat="server" AssociatedControlID="edtStartDate"
                            Text="Start time:" Wrap="false">
                        </dxe:ASPxLabel>
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxDateEdit ClientInstanceName="_dx" ID="edtStartDate" runat="server" Width="100%"
                            Date='<%#(CType(Container, UserAppointmentFormTemplateContainer)).Start%>' EditFormat="DateTime"
                            BackColor="Ivory" ReadOnly="True" />
                    </td>
                </tr>
            </table>
        </td>
        <td class="dxscSingleCell">
            <table class="dxscLabelControlPair" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="dxscLabelCell" style="padding-left: 25px;">
                        <dxe:ASPxLabel runat="server" ID="lblEndDate" Text="End time:" Wrap="false" AssociatedControlID="edtEndDate" />
                    </td>
                    <td class="dxscControlCell">
                        <dxe:ASPxDateEdit ID="edtEndDate" runat="server" ClientInstanceName="_dx" Date='<%#(CType(Container, UserAppointmentFormTemplateContainer)).End%>'
                            EditFormat="DateTime" Width="100%" BackColor="Ivory" ReadOnly="True">
                        </dxe:ASPxDateEdit>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="dxscDoubleCell" colspan="2" style="height: 90px;">
            <dxe:ASPxMemo ClientInstanceName="_dx" ID="tbDescription" runat="server" Width="100%"
                Rows="3" Text='<%#(CType(Container, UserAppointmentFormTemplateContainer)).Appointment.Description%>'
                ReadOnly="True" />
            &nbsp;
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%; height: 35px;">
    <tr>
        <td style="width: 100%; height: 100%;" align="center">
            <table style="height: 100%;">
                <tr>
                    <td>
                        <dxe:ASPxButton runat="server" ClientInstanceName="_dx" ID="btnOk" Text="OK" UseSubmitBehavior="false"
                            AutoPostBack="false" EnableViewState="false" Width="91px" />
                    </td>
                    <td>
                        <dxe:ASPxButton runat="server" ID="btn_relationship_open" Text="Open Relationship"
                            AutoPostBack="True" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td style="width: 100%;" align="left">
            &nbsp;</td>
    </tr>
</table>

<script id="dxss_ASPxSchedulerAppoinmentForm" type="text/javascript">
    function OnChkReminderCheckedChanged(s, e) {
        var isReminderEnabled = s.GetValue();
        if (isReminderEnabled)
            _dxAppointmentForm_cbReminder.SetSelectedIndex(3);
        else
            _dxAppointmentForm_cbReminder.SetSelectedIndex(-1);

        _dxAppointmentForm_cbReminder.SetEnabled(isReminderEnabled);

    }
</script>

