Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports DevExpress.Web

Partial Class Franchise
    Inherits POSBasePage

    Dim footer_message As String
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Populate_Results()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "select STORE_CD,TAX_CD,WHSE_STORE_CD,FURN_PCT,APPL_PCT,ELEC_PCT, "
        sql = sql & "MATT_PCT,FURN_TRANS_PCT,APPL_TRANS_PCT,ELEC_TRANS_PCT,MATT_TRANS_PCT,EXCP, CUST_CD, QST, (select st.store_cd from store st where st.co_cd='LFL' and st.store_cd=a.store_num) as store_num "
        sql = sql & "from frtbl a "
        sql = sql & "where a.store_cd =nvl('" & Request("store_cd") & "', a.store_cd)"
        sql = sql & "and a.whse_store_cd =nvl('" & Request("whse_store_cd") & "', a.whse_store_cd)"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            If MyDataReader.Read Then
                Try
                    txt_store_cd.Items.Insert(0, Request("store_cd"))
                    txt_store_cd.SelectedIndex = 0
                    txt_whse_store_cd.Items.Insert(0, Request("whse_store_cd"))
                    txt_whse_store_cd.SelectedIndex = 0
                    txt_tax_cd.Items.Insert(0, MyDataReader.Item("TAX_CD").ToString)
                    txt_tax_cd.SelectedIndex = 0
                Catch ex As Exception
                    conn.Close()
                End Try

                txt_co_cd.Text = getCompanyCode(Request("store_cd"))
                txt_whse_co_cd.Text = getCompanyCode(Request("whse_store_cd"))

                MyDataReader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        GridView2.DataSource = ds
        GridView2.DataBind()

        txt_store_cd.Enabled = False
        txt_whse_store_cd.Enabled = False
        txt_tax_cd.Enabled = False

    End Sub

    Protected Overrides Sub InitializeCulture()

        ' Put the following code before InitializeComponent()
        ' Sets the culture to French (France)
        'Thread.CurrentThread.CurrentCulture = New CultureInfo("fr-FR")
        ' Sets the UI culture to French (France)
        'Thread.CurrentThread.CurrentUICulture = New CultureInfo("fr-FR")

    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Page Load"

        If Not IsPostBack Then
            '    If Find_Security("ISTM", Session("emp_cd")) = "N" Then
            'btn_Lookup.Enabled = False
            'btn_Clear.Enabled = False
            'lbl_Cust_Info.Text = "You don't have the appropriate securities to view Franchise-Store percentages."
            'End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then
                Populate_Results()
                btn_Lookup.Enabled = False
                ' lbl_Cust_Info.Visible = True

            Else
                PopulateStoreCodes()
                PopulateWhseCodes()
                PopulateTaxCodes()
                footer_message = String.Empty

            End If
        End If

        ' lbl_Cust_Info.Visible = True

        'txt_store_cd.SelectedValue = ConfigurationManager.AppSettings("default_store_code").ToString

        If (Not GridView2.DataSource Is Nothing) Then
            GridView2.DataBind()
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_store_cd.Enabled = True

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Clear"
        Response.Redirect("Franchise.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click
        'Dim changesMade As Boolean = False
        'Dim doc As XmlDocument = New XmlDocument()
        'Dim appPath As String = Request.ServerVariables("APPL_PHYSICAL_PATH")
        'Dim settingsFile As String = appPath + "\appsettings.config"
        'doc.Load(settingsFile)
        'Dim appSettings As XmlNodeList = doc.FirstChild.ChildNodes

        'UpdateAppSetting(changesMade, appSettings, "default_store_code", txt_store_cd.SelectedValue.ToString.Trim)
        'txt_store_cd.Text = txt_store_cd.SelectedValue
        'txt_store_cd.Text = txt_store_cd.SelectedValue


        Response.Redirect("Main_Franchise_Lookup.aspx?store_cd=" & HttpUtility.UrlEncode(txt_store_cd.Text) & "&whse_store_cd=" & HttpUtility.UrlEncode(txt_whse_store_cd.Text) _
            & "&tax_cd=" & HttpUtility.UrlEncode(txt_tax_cd.Text))

    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub


    Protected Sub grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        e.RowError = "Start grid_RowValidating."
        For Each column As GridViewColumn In GridView2.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If
            If e.NewValues(dataColumn.FieldName) Is Nothing Then
                e.Errors(dataColumn) = "Value can't be null."
            End If
        Next column
        If e.Errors.Count > 0 Then
            e.RowError = "Please, fill all fields."
        End If
        If e.NewValues("EXCP") IsNot Nothing AndAlso e.NewValues("EXCP").ToString().ToUpper IsNot "E" Then
            AddError(e.Errors, GridView2.Columns("EXCP"), "EXCPT can be 'E' or empty.")
        End If

        Dim age As Integer = 0
        If e.NewValues("Age") Is Nothing Then
            Integer.TryParse(String.Empty, age)
        Else
            Integer.TryParse(e.NewValues("Age").ToString(), age)
        End If
        If age < 18 Then
            AddError(e.Errors, GridView2.Columns("Age"), "Age must be greater than or equal 18.")
        End If

        If String.IsNullOrEmpty(e.RowError) AndAlso e.Errors.Count > 0 Then
            e.RowError = "Please correct all errors."
        End If
    End Sub
    Private Sub AddError(ByVal errors As Dictionary(Of GridViewColumn, String), ByVal column As GridViewColumn, ByVal errorText As String)
        If errors.ContainsKey(column) Then
            Return
        End If
        errors(column) = errorText
    End Sub
    Protected Sub grid_HtmlRowPrepared(ByVal sender As Object, ByVal e As ASPxGridViewTableRowEventArgs)
        Dim hasError As Boolean
        If Not e.GetValue("STORE_CD") Is Nothing Then
            hasError = e.GetValue("STORE_CD").ToString().Length <= 1
        End If
        If Not e.GetValue("WHSE_STORE_CD") Is Nothing Then
            hasError = hasError OrElse e.GetValue("WHSE_STORE_CD").ToString().Length <= 1
        End If
        If hasError Then
            e.Row.ForeColor = System.Drawing.Color.Red
        End If
    End Sub
    Protected Sub grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridView2.IsNewRowEditing) Then
            GridView2.DoRowValidation()
        End If
    End Sub
    Protected Sub GridView2_ParseValue(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxParseValueEventArgs)
        Dim hasError As Boolean
        Dim errorMsg As String = ""
        If e.FieldName.Contains("PCT") Then
            Dim pct As Integer = 0
            If e.Value Is Nothing OrElse (Not Integer.TryParse(e.Value.ToString(), pct)) Then
                hasError = True
                errorMsg = "Invalid Percentage. Please try again."
            End If
        End If

        If hasError Then
            Throw New Exception(errorMsg)
        End If
    End Sub

    Protected Function getCompanyCode(storeCode As String) As String
        Dim co_cd As String = ""
        If storeCode Is Nothing Then
            Return Nothing
        End If

        If storeCode & "" <> "" Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            sql = "select co_cd from store "
            sql = sql & "where STORE_CD=upper('" & storeCode & "') "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    co_cd = Mydatareader.Item("CO_CD").ToString
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If

        Return co_cd

    End Function

    Protected Function getTaxCode(taxCode As String) As String
        Dim tax_cd As String = ""
        If taxCode Is Nothing Then
            Return Nothing
        End If

        If taxCode & "" <> "" Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            sql = "select tax_cd from tax_cd "
            sql = sql & "where tax_cd=upper('" & taxCode & "') "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    tax_cd = Mydatareader.Item("TAX_CD").ToString
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If

        Return tax_cd

    End Function

    Protected Sub GridView2_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)
        'If (Not BatchUpdateCheckBox.Checked) Then
        'Return
        'End If
        'Throw New Exception("Batch Update ")
        For Each args In e.InsertValues
            InsertNewItem(args.NewValues)
        Next args
        For Each args In e.UpdateValues
            UpdateItem(args.Keys, args.NewValues)
            Populate_Results()
        Next args
        For Each args In e.DeleteValues
            DeleteItem(args.Keys, args.Values)
            Populate_Results()
        Next args

        e.Handled = True

        'Throw New Exception("Batch Done " + GridView2.DataSource.Count)
        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Batch Updated"

    End Sub


    Protected Function InsertNewItem(ByVal newValues As OrderedDictionary) As Boolean
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim excp = newValues("EXCP")
        If excp Is Nothing Then
            excp = ""
        End If

        If newValues("QST") Is Nothing Then
            newValues("QST") = ""
        End If

        If Not (excp.Equals("") Or excp.ToString().ToUpper.Equals("E")) Then
            Throw New Exception("EXCPT can only be 'E' or empty.")
        End If

        If newValues("STORE_CD") Is Nothing Then
            Throw New Exception("Store Code is empty")
        End If
        If getCompanyCode(newValues("STORE_CD")) Is Nothing Or getCompanyCode(newValues("STORE_CD")).Equals("") Then
            Throw New Exception("Store Code is not valid")
        End If
        If newValues("CUST_CD") Is Nothing Or LeonsBiz.GetCustomerName(newValues("CUST_CD")).Equals("") Then
            Throw New Exception("Franchise Customer code does not exist")
        End If
        If getCompanyCode(newValues("WHSE_STORE_CD")) Is Nothing Or getCompanyCode(newValues("WHSE_STORE_CD")).Equals("") Then
            Throw New Exception("WHSE Store Code is not valid")
        End If
        If newValues("TAX_CD") Is Nothing Or getTaxCode(newValues("TAX_CD")).Equals("") Then
            Throw New Exception("Tax code does not exist")
        End If

        sql = "Insert into FRTBL (STORE_CD,TAX_CD,WHSE_STORE_CD,FURN_PCT, "
        sql = sql & "APPL_PCT, ELEC_PCT, "
        sql = sql & "MATT_PCT,FURN_TRANS_PCT,APPL_TRANS_PCT, "
        sql = sql & "ELEC_TRANS_PCT,MATT_TRANS_PCT,EXCP,CUST_CD, QST) values ( "
        sql = sql & "'" + newValues("STORE_CD").ToString().ToUpper + "','" + newValues("TAX_CD").ToString().ToUpper + "','" + newValues("WHSE_STORE_CD").ToString().ToUpper
        sql = sql & "'," + newValues("FURN_PCT").ToString() + "," + newValues("APPL_PCT").ToString() + "," + newValues("ELEC_PCT").ToString()
        sql = sql & "," + newValues("MATT_PCT").ToString() + "," + newValues("FURN_TRANS_PCT").ToString()
        sql = sql & "," + newValues("APPL_TRANS_PCT").ToString() + "," + newValues("ELEC_TRANS_PCT").ToString()
        sql = sql & "," + newValues("MATT_TRANS_PCT").ToString() + ",'" + excp + "','" + newValues("CUST_CD").ToString().ToUpper + "','" + newValues("QST").ToString().ToUpper + "')"

        'Throw New Exception("sql ins " + sql)

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            footer_message = "Record(s) Inserted"
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")

            End If
        End Try
        conn.Close()

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Record(s) Inserted"
        'txt_store_cd.Text = newValues("STORE_CD").ToString().ToUpper
        'txt_whse_store_cd.Text = newValues("WHSE_STORE_CD").ToString().ToUpper
        GridView2.DataBind()

        Return True
    End Function

    Protected Sub GV_Footer_label_init(sender As Object, e As EventArgs)
        Dim gv_footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        gv_footer_label.Text = footer_message
    End Sub

    Protected Function UpdateItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim excp = newValues("EXCP")
        If excp Is Nothing Then
            excp = ""
        End If
        If Not (excp.Equals("") Or excp.ToString().ToUpper.Equals("E")) Then
            Throw New Exception("EXCPT can only be 'E' or empty.")
        End If

        If newValues("STORE_CD") Is Nothing Then
            Throw New Exception("Store Code is empty")
        End If
        If getCompanyCode(newValues("STORE_CD")) Is Nothing Or getCompanyCode(newValues("STORE_CD")).Equals("") Then
            Throw New Exception("Store Code is not valid")
        End If
        If newValues("CUST_CD") Is Nothing Or LeonsBiz.GetCustomerName(newValues("CUST_CD")).Equals("") Then
            Throw New Exception("Franchise Customer code does not exist")
        End If
        If getCompanyCode(newValues("WHSE_STORE_CD")) Is Nothing Or getCompanyCode(newValues("WHSE_STORE_CD")).Equals("") Then
            Throw New Exception("WHSE Store Code is not valid")
        End If
        If newValues("TAX_CD") Is Nothing Or getTaxCode(newValues("TAX_CD")).Equals("") Then
            Throw New Exception("Tax code does not exist")
        End If
        If newValues("QST") Is Nothing Then
            newValues("QST") = ""
        End If

        If newValues("STORE_NUM") Is Nothing Then
            newValues("STORE_NUM") = ""
        End If

        sql = "UPDATE FRTBL SET "
        sql = sql & "STORE_CD = '" + newValues("STORE_CD").ToString() + "',"
        sql = sql & "STORE_NUM = '" + newValues("STORE_NUM").ToString() + "',"
        sql = sql & "TAX_CD = '" + newValues("TAX_CD").ToString() + "',"
        sql = sql & "WHSE_STORE_CD = '" + newValues("WHSE_STORE_CD").ToString() + "',"
        sql = sql & "FURN_PCT = " + newValues("FURN_PCT").ToString() + ","
        sql = sql & "APPL_PCT = " + newValues("APPL_PCT").ToString() + ","
        sql = sql & "ELEC_PCT = " + newValues("ELEC_PCT").ToString() + ","
        sql = sql & "MATT_PCT = " + newValues("MATT_PCT").ToString() + ","
        sql = sql & "FURN_TRANS_PCT = " + newValues("FURN_TRANS_PCT").ToString() + ","
        sql = sql & "APPL_TRANS_PCT = " + newValues("APPL_TRANS_PCT").ToString() + ","
        sql = sql & "ELEC_TRANS_PCT = " + newValues("ELEC_TRANS_PCT").ToString() + ","
        sql = sql & "MATT_TRANS_PCT = " + newValues("MATT_TRANS_PCT").ToString() + ","
        sql = sql & "QST = '" + newValues("QST").ToString() + "',"
        sql = sql & "EXCP = '" + excp + "'"
        sql = sql & " WHERE STORE_CD= '" + newValues("STORE_CD").ToString() + "' and WHSE_STORE_CD = '" + newValues("WHSE_STORE_CD").ToString() + "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            footer_message = "Record(s) Updated"
            Mydatareader.Close()
        Catch
            ' Throw New Exception("Error updating the record. Please try again.")
            footer_message = "Error updating the record. Please try again."
            conn.Close()
        End Try
        conn.Close()

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Record(s) Updated"
        GridView2.DataBind()

        Return True
    End Function
    Protected Function DeleteItem(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary) As Boolean
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "DELETE FRTBL "
        sql = sql & " WHERE STORE_CD= '" + newValues("STORE_CD").ToString() + "' and WHSE_STORE_CD = '" + newValues("WHSE_STORE_CD").ToString() + "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            footer_message = "Record(s) Deleted"
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Record(s) Deleted"
        GridView2.DataBind()

        Return True
    End Function

    Protected Sub GridView2_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        Dim IsLeonFrStroreCode As Boolean = IsLeonsFranchiseStoreCode(txt_store_cd.Text)

        If e.Column.FieldName = "STORE_NUM" And IsLeonFrStroreCode Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True

        ElseIf e.Column.FieldName = "STORE_NUM" Then
            e.Editor.ReadOnly = True
        Else

            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        End If


    End Sub

    Public Shared Function IsLeonsFranchiseStoreCode(ByVal p_store_cd As String) As Boolean

        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) as count from store s inner join frtbl f on s.store_cd=f.store_cd  where s.co_cd like 'F%' and s.store_cd='" & p_store_cd & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("count")
            End If
        Catch
            v_value = 0
        End Try
        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Protected Sub GridView2_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)
        e.NewValues("FURN_PCT") = "0"
        e.NewValues("APPL_PCT") = "0"
        e.NewValues("ELEC_PCT") = "0"
        e.NewValues("MATT_PCT") = "0"
        e.NewValues("FURN_TRANS_PCT") = "0"
        e.NewValues("APPL_TRANS_PCT") = "0"
        e.NewValues("ELEC_TRANS_PCT") = "0"
        e.NewValues("MATT_TRANS_PCT") = "0"
    End Sub

    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store where store_cd in (select store_cd from frtbl) order by store_cd"

        txt_store_cd.Items.Insert(0, "Select a Store Code")
        txt_store_cd.Items.FindByText("Select a Store Code").Value = ""
        txt_store_cd.SelectedIndex = 0


        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_store_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Private Sub PopulateWhseCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT store_cd as whse_store_cd, store_name, store_cd || ' - ' || store_name as full_whse_desc from store where store_cd in (select whse_store_cd from frtbl) order by store_cd"

        txt_whse_store_cd.Items.Insert(0, "Select a Warehouse Code")
        txt_whse_store_cd.Items.FindByText("Select a Warehouse Code").Value = ""
        txt_whse_store_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_whse_store_cd
                .DataSource = ds
                .DataValueField = "whse_store_cd"
                .DataTextField = "full_whse_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Private Sub PopulateTaxCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "select tax_cd, des, tax_cd || ' - ' || des as full_tax_desc from TAX_CD where tax_cd in (select tax_cd from frtbl) order by tax_cd"

        txt_tax_cd.Items.Insert(0, "Select a Tax Code")
        txt_tax_cd.Items.FindByText("Select a Tax Code").Value = ""
        txt_tax_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_tax_cd
                .DataSource = ds
                .DataValueField = "tax_cd"
                .DataTextField = "full_tax_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub


    Private Sub UpdateAppSetting(ByRef settingWasUpdated As Boolean,
                             ByRef appSettingList As XmlNodeList,
                             ByVal appSettingKey As String,
                             ByVal newValue As String)

        Dim key As String
        Dim origValue As String

        For Each node As XmlNode In appSettingList
            If (node.Attributes.Count >= 2) Then
                Try
                    key = node.Attributes.GetNamedItem("key").Value
                    origValue = node.Attributes.GetNamedItem("value").Value

                    If (appSettingKey.ToUpper.Equals(key.ToUpper)) Then
                        If (newValue <> origValue) Then
                            node.Attributes.GetNamedItem("value").Value = newValue
                            settingWasUpdated = True
                        End If
                        Exit For
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Next

    End Sub


End Class
