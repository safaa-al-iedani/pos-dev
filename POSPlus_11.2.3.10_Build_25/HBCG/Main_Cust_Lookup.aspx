<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main_Cust_Lookup.aspx.vb"
    Inherits="prs" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
         <dx:ASPxGridView ID="GridView1" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="CUST_CD" EnableCallBacks="False">
            <SettingsPager Visible="False">
            </SettingsPager>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Corp Name" FieldName="CORP_NAME" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Last Name" FieldName="LNAME" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="First Name" FieldName="FNAME" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Address" FieldName="ADDR1" Visible="False"
                    VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="City" FieldName="CITY" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label229 %>" FieldName="HOME_PHONE" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Customer Code" FieldName="CUST_CD" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="6">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="Select" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Font-Size="XX-Small" Width="100%" AllowPaging="True" BorderStyle="None" BorderColor="White"
            BorderWidth="0px" PagerSettings-Visible="false" CssClass="style5">
            <Columns>
                <asp:BoundField DataField="CORP_NAME" HeaderText="CORP NAME" SortExpression="Corp Name">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="LNAME" HeaderText="LAST NAME" SortExpression="Last Name">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="FNAME" HeaderText="FIRST NAME" SortExpression="FIRST NAME">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="ADDR1" HeaderText="ADDRESS" SortExpression="ADDRESS">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="CITY" HeaderText="CITY" SortExpression="CITY">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="home_phone" HeaderText="HOME PHONE" SortExpression="Home Phone">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:BoundField DataField="CUST_CD" HeaderText="CODE" SortExpression="CUSTOMER CODE">
                    <ControlStyle Font-Size="X-Small" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" ButtonType="Button">
                    <ControlStyle CssClass="style5" />
                </asp:CommandField>
            </Columns>
            <PagerSettings Visible="False" />
            <HeaderStyle ForeColor="Black" />
            <AlternatingRowStyle BackColor="Beige" />
        </asp:GridView>--%>
        <table width="98%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:aspxbutton id="btnFirst" onclick="PageButtonClick" runat="server" text="<< First"
                                    tooltip="Go to first page" commandargument="First" visible="false">
                                </dx:aspxbutton>
                            </td>
                            <td width="15%" align="center">
                                <dx:aspxbutton id="btnPrev" onclick="PageButtonClick" runat="server" text="< Prev"
                                    tooltip="Go to the previous page" commandargument="Prev" visible="false">
                                </dx:aspxbutton>
                            </td>
                            <td width="15%" align="center">
                                <dx:aspxbutton id="btnNext" onclick="PageButtonClick" runat="server" text="Next >"
                                    tooltip="Go to the next page" commandargument="Next" visible="False">
                                </dx:aspxbutton>
                            </td>
                            <td width="15%" align="center">
                                <dx:aspxbutton id="btnLast" onclick="PageButtonClick" runat="server" text="Last >>"
                                    tooltip="Go to the last page" commandargument="Last" visible="false">
                                </dx:aspxbutton>
                            </td>
                            <td width="40%" align="center">
                                <dx:aspxbutton id="btn_search_again" runat="server" text="Search Again">
                                </dx:aspxbutton>
                            </td>
                        </tr>
                    </table>
                    <dx:aspxlabel id="lbl_pageinfo" runat="server" text="" visible="false">
                    </dx:aspxlabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="false" CssClass="style5"></asp:Label>
    </div>
</asp:Content>
