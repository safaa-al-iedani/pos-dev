Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic

Partial Class InventoryResults
    Inherits POSBasePage

    '    Protected MyDataGrid As System.Web.UI.WebControls.DataGrid
    Protected CheckBox As System.Web.UI.WebControls.CheckBox
    '   Protected cbo_records As DevExpress.Web.ASPxEditors.ASPxComboBox
    'Protected Confirm As DevExpress.Web.ASPxEditors.ASPxButton
    '  Protected lbl_warning As DevExpress.Web.ASPxEditors.ASPxLabel
    ' Protected OutputMsg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected objConnect As OracleConnection
    Protected myDataAdapter As OracleDataAdapter
    Protected myCommand As OracleCommand
    Protected DS As DataSet
    Protected dgItem As DataGridItem

    Public itemIds As String = ""
    Public sel_itms() As String
    Public itmcd As String
    Public ChkdItems As String
    Public SortField As String = ""
    Public ChkBxIndex As String = ""
    Public BxChkd As Boolean = False
    Public CheckedItems As ArrayList
    Public Results() As String


    Sub Page_Load(ByVal Sender As Object, ByVal E As EventArgs)
        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.Confirm.UniqueID
        objConnect = DisposablesManager.BuildOracleConnection

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If Not IsPostBack Then
            If Request("referrer") & "" <> "" Then
                Confirm.Text = "Use Selected"
            End If
            'Set up default column sorting   
            If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                BindData("vsn asc")
            Else
                BindData(Session("SortOrder"))
            End If
            RePopulateCheckBoxes()
        End If

    End Sub

    Sub MyDataGrid_Page(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        'Get CheckBoxValues before paging occurs        
        GetCheckBoxValues()

        MyDataGrid.CurrentPageIndex = e.NewPageIndex

        BindData(Session("SortOrder"))

        'Populate current DataGrid page with the current page items from Session after databind          
        RePopulateCheckBoxes()

    End Sub

    Sub GetCheckBoxValues() 'As paging occurs store checkbox values
        CheckedItems = New ArrayList

        'Add ArrayList to Session if it doesnt exist  
        If Not IsDBNull(Session("CheckedItems")) Then
            CheckedItems = Session("CheckedItems")
        End If

        'Loop through DataGrid Items            
        For Each dgItem In MyDataGrid.Items

            'Retrieve key value of each record based on DataGrids DataKeyField property    
            ChkBxIndex = MyDataGrid.DataKeys(dgItem.ItemIndex)
            CheckBox = dgItem.FindControl("SelectThis")

            If CheckBox.Checked Then
                BxChkd = True
                'Add to Session if it doesnt already exist 
                If IsNothing(CheckedItems) Then
                    CheckedItems = New ArrayList
                    CheckedItems.Add(ChkBxIndex.ToString())
                Else
                    If Not CheckedItems.Contains(ChkBxIndex) Then
                        CheckedItems.Add(ChkBxIndex.ToString())
                    End If
                End If
            Else
                If Not IsDBNull(CheckedItems) Then
                    If Not IsNothing(CheckedItems) Then
                        'Remove value from Session when unchecked       
                        CheckedItems.Remove(ChkBxIndex.ToString())
                    End If
                End If
            End If
        Next

        If Not IsDBNull(CheckedItems) Then
            If Not IsNothing(CheckedItems) Then
                'Update Session with the list of checked items       
                Session("CheckedItems") = CheckedItems

                'Copy ArrayList to a new array          
                sel_itms = CheckedItems.ToArray(GetType(String))

                'Concatenate ArrayList with comma to properly send for deletion          
                itmcd = String.Join(",", sel_itms)
            End If
        End If

    End Sub

    Sub BindData(ByVal SortField As String)

        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim proceed As Boolean
        Dim LikeQry As String
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objSql As OracleCommand

        MyDataGrid.Visible = True
        If (IsNothing(Source)) Then
            ' Daniela Apr 29 fix price
            'sql = "SELECT itm_cd, ve_cd, vsn, des ,mnr_cd ,cat_cd ,style_cd ,finish ,siz ,cover ,grade ,ret_prc, inventory FROM itm WHERE "
            If Session("store_cd") = Nothing Then
                Session("store_cd") = Session("HOME_STORE_CD")
            End If
            sql = "SELECT itm_cd, ve_cd, vsn, des ,mnr_cd ,cat_cd ,style_cd ,finish ,siz ,cover ,grade ,nvl(current_price_custom(itm_cd,'" & Session("store_cd") &
                "',trunc(sysdate),ret_prc, '" & Session("cust_tp_prc_cd") & "'),ret_prc) ret_prc, inventory FROM itm WHERE "


            If Session("sku") & "" <> "" Then
                If InStr(Session("sku"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                'sql = sql & "itm_cd " & LikeQry & " :ITM_CD "
                sql = sql & "itm_cd " & LikeQry & " :ITM_CD AND std_multi_co.isValidItm2('" & Session("emp_cd") & "', itm_cd) = 'Y' "
                proceed = True
            End If
            ' Daniela added
            If Session("sku") & "" = "" Then
                sql = sql & "std_multi_co.isValidItm2('" & Session("emp_cd") & "', itm_cd) = 'Y' "
                proceed = True
            End If ' Daniela end
            'Daniela limit number of records
            sql = sql & " and rownum <= 100 "
            proceed = True
            'end daniela
            If Session("vendor") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("vendor"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ve_cd " & LikeQry & " :VENDOR "
                proceed = True
            End If
            If Session("vsn") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("vsn"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "vsn " & LikeQry & " :VSN "
                proceed = True
            End If
            If Session("desc") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("desc"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "des " & LikeQry & " :DES "
                proceed = True
            End If
            If Session("minor") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("minor"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "mnr_cd " & LikeQry & " :MINOR "
                proceed = True
            End If
            If Session("category") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("category"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "cat_cd " & LikeQry & " :CATEGORY "
                proceed = True
            End If
            If Session("style") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("style"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "style_cd " & LikeQry & " :STYLE "
                proceed = True
            End If
            If Session("finish") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("finish"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "finish " & LikeQry & " :FINISH "
                proceed = True
            End If
            If Session("size") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("size"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "siz " & LikeQry & " :SIZE "
                proceed = True
            End If
            If Session("cover") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("cover"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "cover " & LikeQry & " :COVER "
                proceed = True
            End If
            If Session("grade") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("grade"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "grade " & LikeQry & " :GRADE "
                proceed = True
            End If
            If Session("price") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Session("price"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ret_prc " & LikeQry & " :RET_PRC "
                proceed = True
            End If
            If Session("dropped") & "" <> "" Then
                If UCase(Session("dropped")) = "TRUE" Then
                    If proceed = True Then sql = sql & "AND "
                    sql = sql & "drop_cd IS NULL "
                    proceed = True
                End If
            End If

            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)

            If Session("sku") & "" <> "" Then
                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = UCase(Session("sku"))
            End If
            If Session("vendor") & "" <> "" Then
                objSql.Parameters.Add(":VENDOR", OracleType.VarChar)
                objSql.Parameters(":VENDOR").Value = UCase(Session("vendor"))
            End If
            If Session("vsn") & "" <> "" Then
                objSql.Parameters.Add(":VSN", OracleType.VarChar)
                objSql.Parameters(":VSN").Value = UCase(Session("vsn"))
            End If
            If Session("desc") & "" <> "" Then
                objSql.Parameters.Add(":DES", OracleType.VarChar)
                objSql.Parameters(":DES").Value = UCase(Session("desc"))
            End If
            If Session("minor") & "" <> "" Then
                objSql.Parameters.Add(":MINOR", OracleType.VarChar)
                objSql.Parameters(":MINOR").Value = UCase(Session("minor"))
            End If
            If Session("category") & "" <> "" Then
                objSql.Parameters.Add(":CATEGORY", OracleType.VarChar)
                objSql.Parameters(":CATEGORY").Value = UCase(Session("category"))
            End If
            If Session("style") & "" <> "" Then
                objSql.Parameters.Add(":STYLE", OracleType.VarChar)
                objSql.Parameters(":STYLE").Value = UCase(Session("style"))
            End If
            If Session("finish") & "" <> "" Then
                objSql.Parameters.Add(":FINISH", OracleType.VarChar)
                objSql.Parameters(":FINISH").Value = UCase(Session("finish"))
            End If
            If Session("size") & "" <> "" Then
                objSql.Parameters.Add(":SIZE", OracleType.VarChar)
                objSql.Parameters(":SIZE").Value = UCase(Session("size"))
            End If
            If Session("cover") & "" <> "" Then
                objSql.Parameters.Add(":COVER", OracleType.VarChar)
                objSql.Parameters(":COVER").Value = UCase(Session("cover"))
            End If
            If Session("grade") & "" <> "" Then
                objSql.Parameters.Add(":GRADE", OracleType.VarChar)
                objSql.Parameters(":GRADE").Value = UCase(Session("grade"))
            End If
            If Session("price") & "" <> "" Then
                objSql.Parameters.Add(":RET_PRC", OracleType.VarChar)
                objSql.Parameters(":RET_PRC").Value = UCase(Session("price"))
            End If

            ds = New DataSet()
            sAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            myDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            If Session("vendor") & "" = "" And Session("vsn") & "" = "" And Session("desc") & "" = "" And Session("minor") & "" = "" And Session("category") & "" = "" And Session("style") & "" = "" And Session("finish") & "" = "" And Session("size") & "" = "" And Session("cover") & "" = "" And Session("grade") & "" = "" And Session("price") & "" = "" Then
                If numrows = 1 And InStr(Session("sku"), "%") = 0 Then
                    Session("itemid") = UCase(Session("sku"))
                    Session("CheckedItems") = System.DBNull.Value
                    If Request("referrer") & "" <> "" Then
                        Session("Comparisons") = Session("Comparisons") & "," & UCase(Session("sku"))
                        ' Response.Redirect(Request("referrer") & "?SKU=" & UCase(Session("sku")) & "," & Request("SKU"))
                    ElseIf Request("LEAD") = "TRUE" Then
                        Response.Redirect("order_detail.aspx?LEAD=TRUE&auto_add=TRUE")
                    Else
                        Response.Redirect("order_detail.aspx?auto_add=TRUE")
                    End If
                End If
            End If

            myDataAdapter.Fill(ds, "MyDataGrid")

            'Assign sort expression to Session              
            Session("SortOrder") = SortField

            'Setup DataView for Sorting              
            Source = ds.Tables(0).DefaultView

            'Insert DataView into Session           
            Session("dgCache") = Source

        End If

        If numrows = 0 Then
            MyDataGrid.Visible = False
            cbo_records.Enabled = False
            Confirm.Visible = False
            lbl_warning.Visible = True
            lbl_warning.Text = "No records were found matching your search criteria." & vbCrLf
        Else
            If Request("referrer") & "" <> "" And Not IsPostBack Then SortField = "vsn asc"
            Source.Sort = SortField
            MyDataGrid.DataSource = Source
            MyDataGrid.DataBind()

            If ConfigurationManager.AppSettings("inv_avail_find_show").ToString = "Y" Then
                Update_Inventory()
            Else
                MyDataGrid.Columns(9).Visible = "False"
                MyDataGrid.Columns(10).Visible = "False"
                MyDataGrid.Columns(11).Visible = "False"
            End If
            lbl_warning.Visible = False
        End If
        'Close connection           
        objConnect.Close()

    End Sub

    Sub RePopulateCheckBoxes()

        CheckedItems = New ArrayList

        'If Session("CheckedItems") & "" <> "" Then
        If Not IsDBNull(Session("CheckedItems")) Then
            If Not IsNothing(Session("CheckedITems")) Then
                CheckedItems = Session("CheckedItems")
            End If
        End If

        If Not IsNothing(CheckedItems) Then
            'Loop through DataGrid Items              
            For Each dgItem In MyDataGrid.Items
                ChkBxIndex = MyDataGrid.DataKeys(dgItem.ItemIndex)

                'Repopulate DataGrid with items found in Session              
                If CheckedItems.Contains(ChkBxIndex) Then
                    CheckBox = CType(dgItem.FindControl("SelectThis"), CheckBox)
                    CheckBox.Checked = True
                End If
            Next

            'Copy ArrayList to a new array          
            Results = CheckedItems.ToArray(GetType(String))

            'Concatenate ArrayList with comma to properly send for deletion          
            itemIds = String.Join(",", Results)
        End If

    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        'To retain checkbox on sorting        
        GetCheckBoxValues()

        MyDataGrid.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid
        'To retain checkbox on sorting          
        RePopulateCheckBoxes()

    End Sub
    Sub ClearDataGrid(ByVal sender As Object, ByVal e As EventArgs)

        'Clear All Session Values        
        Session("sortorder") = System.DBNull.Value
        Session("checkeditems") = System.DBNull.Value
        Session("dgcache") = System.DBNull.Value
        Session("sku") = System.DBNull.Value
        Session("vendor") = System.DBNull.Value
        Session("vsn") = System.DBNull.Value
        Session("desc") = System.DBNull.Value
        Session("minor") = System.DBNull.Value
        Session("category") = System.DBNull.Value
        Session("style") = System.DBNull.Value
        Session("finish") = System.DBNull.Value
        Session("size") = System.DBNull.Value
        Session("cover") = System.DBNull.Value
        Session("grade") = System.DBNull.Value
        Session("price") = System.DBNull.Value
        Session("dropped") = System.DBNull.Value

        'Reset DataGrid to top   
        Dim Query_string As String = ""
        If Request("referrer") & "" <> "" Then
            Query_string = Request.QueryString.ToString()
        End If
        If Request("LEAD") = "TRUE" Then
            Response.Redirect("order.aspx?LEAD=TRUE" & "&" & Query_string)
        Else
            Response.Redirect("order.aspx" & "?" & Query_string)
        End If
    End Sub

    Sub movenextorder(ByVal sender As Object, ByVal e As EventArgs)

        GetCheckBoxValues()
        Session("itemid") = itmcd
        Session("CheckedItems") = System.DBNull.Value

        Dim Query_string As String = Request.QueryString.ToString
        If Request("referrer") & "" <> "" Then
            Session("itemid") = ""
            If Request("referrer") = "InventoryComparison.aspx" Then
                Session("Comparisons") = Session("Comparisons") & "," & itmcd
                Response.Redirect("InventoryComparison.aspx?SKU=" & itmcd & "," & Request("SKU"))
            ElseIf Request("referrer") = "Relationship_Maintenance.aspx" Then
                Response.Redirect("Relationship_Maintenance.aspx?SKU=" & itmcd & "&" & Query_string)
            ElseIf Request("referrer") = "InventoryLookup.aspx" Then
                Response.Redirect("InventoryLookup.aspx?SKU=" & itmcd & "&" & Query_string)
            ElseIf Request("referrer") = "SalesOrderMaintenance.aspx" Then
                Response.Redirect("SalesOrderMaintenance.aspx?SKU=" & itmcd & "&" & Query_string)
            ElseIf Request("referrer") = "CashCarryExpress.aspx" Then
                Response.Redirect("CashCarryExpress.aspx?SKU=" & itmcd & "&refresh=true&" & Query_string)
            End If
        ElseIf Request("LEAD") = "TRUE" Then
            Response.Redirect("order_detail.aspx?LEAD=TRUE")
        Else
            Response.Redirect("order_detail.aspx")
        End If

    End Sub

    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        ElseIf Request("referrer") & "" <> "" Then
            If Request("minimal") = "true" Then
                Page.MasterPageFile = "~/NoContent.Master"
            Else
                Page.MasterPageFile = "~/MasterPages/NoWizard2.Master"
            End If
        End If

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Sub Update_Record_Count(ByVal sender As Object, ByVal e As EventArgs)

        'See if the user clicked anything before changing the page count
        GetCheckBoxValues()

        'Redisplay the data in the requested format
        MyDataGrid.CurrentPageIndex = 0
        MyDataGrid.PageSize = cbo_records.Value
        If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
            BindData("itm_cd asc")
        Else
            BindData(Session("SortOrder"))
        End If

        'Add check marks to any boxes that were previously checked
        RePopulateCheckBoxes()

    End Sub

    Sub Update_Inventory()

        For Each dgItem In MyDataGrid.Items
            Dim txt_qty As Label = CType(dgItem.FindControl("lbl_avail_qty"), Label)
            txt_qty.Text = "0"
            If dgItem.Cells(8).Text = "Y" Then
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim sql As String
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()
                sql = "select SUM(QTY) from ITM_FIFL, LOC WHERE ITM_FIFL.STORE_CD=LOC.STORE_CD and ITM_FIFL.LOC_CD=LOC.LOC_CD AND LOC.LOC_TP_CD='W' AND ITM_CD=:ITM_CD"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = dgItem.Cells(1).Text

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read Then
                        If IsNumeric(MyDataReader.Item(0).ToString) Then
                            txt_qty.Text = MyDataReader.Item(0).ToString
                        Else
                            txt_qty.Text = "0"
                        End If
                    Else
                        txt_qty.Text = "0"
                    End If
                    MyDataReader.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            Else
                txt_qty.Text = "0"
            End If
            Dim txt_shw_qty As Label = CType(dgItem.FindControl("lbl_shw_qty"), Label)
            txt_shw_qty.Text = "0"
            If dgItem.Cells(8).Text = "Y" Then
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim sql As String
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()
                sql = "select SUM(QTY) from ITM_FIFL, LOC WHERE ITM_FIFL.STORE_CD=LOC.STORE_CD and ITM_FIFL.LOC_CD=LOC.LOC_CD AND LOC.LOC_TP_CD='S' AND ITM_CD=:ITM_CD"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = dgItem.Cells(1).Text

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read Then
                        If IsNumeric(MyDataReader.Item(0).ToString) Then
                            txt_shw_qty.Text = MyDataReader.Item(0).ToString
                        Else
                            txt_shw_qty.Text = "0"
                        End If
                    Else
                        txt_shw_qty.Text = "0"
                    End If
                    MyDataReader.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            Else
                txt_shw_qty.Text = "0"
            End If

            'Determine if the PO Available image should display
            Dim po_img As Image = CType(dgItem.FindControl("po_img"), Image)
            po_img.Visible = False
            If dgItem.Cells(8).Text = "Y" Then
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim sql As String
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()
                sql = "select itm.itm_cd, itm.vsn || ' - ' || itm.des vsn, "
                sql = sql & "0 whs_qty, 0 shw_qty, "
                sql = sql & "d.po_cd || ' Ln#' || d.ln# || ': ' || nvl(d.qty_ord - sum(decode(f.po_actn_tp_cd,'RCV',f.qty,0)) - nvl(psq.qty,0),0) po_data, "
                sql = sql & "null ist_data "
                sql = sql & "from itm, po_ln$actn_hst f, po_ln d, po e, "
                sql = sql & "(select a.po_cd, a.ln#, b.qty "
                sql = sql & "from so_ln b, so_ln$po_ln a "
                sql = sql & "where(a.del_doc_num = b.del_doc_num) "
                sql = sql & "and a.del_doc_ln# = b.del_doc_ln# "
                sql = sql & "and b.store_cd is null) psq "
                sql = sql & "where(itm.itm_cd = d.itm_cd) "
                sql = sql & "and d.po_cd = e.po_cd "
                sql = sql & "and d.po_cd = f.po_cd "
                sql = sql & "and d.ln# = f.ln# "
                sql = sql & "and d.po_cd = psq.po_cd (+) "
                sql = sql & "and d.ln# = psq.ln# (+) "
                sql = sql & "and e.stat_cd = 'O' "
                sql = sql & "and itm.itm_cd = :ITM_CD "
                sql = sql & "group by itm.itm_cd, itm.vsn, itm.des, d.po_cd, d.ln#, d.qty_ord, psq.qty "
                sql = sql & "having nvl(d.qty_ord - sum(decode(f.po_actn_tp_cd,'RCV',f.qty,0)) - nvl(psq.qty,0),0) >0 "
                sql = sql & "order by 1"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = UCase(dgItem.Cells(1).Text)

                Dim po_string As String = ""

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    Do While MyDataReader.Read
                        po_string = po_string & MyDataReader.Item("po_data").ToString & vbCrLf
                    Loop
                    MyDataReader.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
                If po_string & "" <> "" Then
                    po_img.Visible = True
                    po_img.AlternateText = po_string
                    po_img.ToolTip = po_string
                End If
            End If
        Next

    End Sub

End Class

