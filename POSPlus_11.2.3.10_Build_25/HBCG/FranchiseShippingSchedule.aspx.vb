﻿Imports DevExpress.Web.Data
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxEditors

Imports System.Web.UI


Imports HBCG_Utils
''' <summary>
''' Apr 2018, mm - FRANCHISE SHIPPING SCHEDULE
''' </summary>
''' <remarks></remarks>
Partial Class FranchiseShippingSchedule
    Inherits POSBasePage

    Protected objFrPOBiz As FranchisePOBiz = New FranchisePOBiz()
    Protected objTransDtc As TransDtc = New TransDtc
    Protected objCartItems As CartItemDtc = New CartItemDtc
    Const _MAX_LINE_ITEMS As Int16 = 20
    Protected _PAGE_VALID As String = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Const Const_TABLENAME As String = "Fr_Shipping_Schedule"
            Const Const_COLUMNNAME As String = "upd_dt"
            Const Const_UniqueSeq_AA As String = "AA"
            Dim res As String = String.Empty

            '  Session("addnewrow") = "n"
            storeCdPopulate()

            SetInitialRow()

            'Get Last Update Date
            res = objFrPOBiz.GetValueByLastRow(Const_COLUMNNAME, Const_TABLENAME)
            If res.Length > 0 Then
                lblDate.Text = DateTime.Parse(res).ToString("dd-MMM-yyyy")
            Else
                lblDate.Text = ""
            End If

        End If

    End Sub


    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub storeCdPopulate()
        Dim s_home_store_code = Session("HOME_STORE_CD")
        Dim userType As Double = -1
        Dim sup_user_flag As String = String.Empty

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            sup_user_flag = Session("str_sup_user_flag")
        End If
        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        Dim datSet As DataSet = objFrPOBiz.GetStores_MCCL1(sup_user_flag, userType, coCd)

        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

            With ddl_StoreCds
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            'ddl_StoreCds.Items.Insert(0, New ListEditItem(" -Select- "))

        End If

        If Not IsNothing(Session("HOME_STORE_CD")) Then

            Try
                ddl_StoreCds.ValueField = Session("HOME_STORE_CD").ToString
            Catch argEx As ArgumentOutOfRangeException
                ' setup is wrong - probably no stores show - never saw this fail but slsp did so playing it safe
            End Try
        End If

    End Sub

    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateOrderFromStoreCode()
        Try

        Catch ex As Exception
            Throw ex
        End Try


    End Sub


    ''' <summary>
    ''' populates Corporate store codes drop down
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CorpstoreCdPopulate()

        Dim datSet As DataSet = objFrPOBiz.GetCorpStores()

        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

            With ddl_StoreCds
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "store_name"
                .DataBind()
            End With

            'ddl_StoreCds.Items.Insert(0, New ListEditItem(" -Select- "))

        End If

        If Not IsNothing(Session("HOME_STORE_CD")) Then

            Try
                ddl_StoreCds.ValueField = Session("HOME_STORE_CD").ToString
            Catch argEx As ArgumentOutOfRangeException
                ' setup is wrong - probably no stores show - never saw this fail but slsp did so playing it safe
            End Try
        End If

    End Sub

    ''' <summary>
    ''' populates Zone Codes drop down by store code;
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetZoneCodesForCorpStore(ByVal p_OrderFromCorpStore As String)
        Try

        Catch argEx As ArgumentOutOfRangeException
            Throw argEx
        End Try
        ' ddlCorpDelZoneCd.Text = "SCA"       'hard coded now

    End Sub
    ''' <summary>
    ''' Jan 10,2018 - mariam
    ''' when franchise store code is selected from dropdown, get franshise coporate store code, franshice customer code,  store name and display
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub cbo_StoreCds_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        litMessage.Text = String.Empty

        Dim s_store_code = ddl_StoreCds.Value

        'Get Franchise Store Details - whse_store_cd, storename, fr_cust_cd
        GetFrStoreDetails(s_store_code)

        'OrderingFromStore Code Populate
        PopulateOrderFromStoreCode()

    End Sub
    Protected Sub grvItems_OnRowCommand(sender As Object, e As GridViewCommandEventArgs)
        'Determine the RowIndex of the Row whose Button was clicked.
        Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)
        Dim CorpStoreCode As String = String.Empty
        'Reference the GridView Row.
        Dim row As GridViewRow = grvItems.Rows(rowIndex)
        'Find the CorpStoreCode DropDownList control.
        Dim ddlCorpStoreCode As DropDownList = TryCast(row.FindControl("ddlCorpStoreCode"), DropDownList)
        CorpStoreCode = ddlCorpStoreCode.SelectedItem.Value
        'Find the DropDownList control.
        Dim ddlDelZoneCode As DropDownList = TryCast(row.FindControl("ddlDelZoneCode"), DropDownList)
        ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Name: " + ddlCorpStoreCode.SelectedItem.Value + "');", True)
    End Sub
    '''Corporate Store Code dropdown 
    Protected Sub ddlCorpStoreCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddl_StoreCds.SelectedIndex <> -1 Then

            Dim ddlCurrentDropDownList As DropDownList = CType(sender, DropDownList)
            Dim grdrDropDownRow As GridViewRow = (CType(ddlCurrentDropDownList.Parent.Parent, GridViewRow))
            Dim CoprStoreCd As String = String.Empty
            CoprStoreCd = ddlCurrentDropDownList.SelectedValue
            'Populate Delivery Zone Code dropdownlist
            Dim ddlDelZoneCd As DropDownList = CType(grdrDropDownRow.FindControl("ddlDelZoneCode"), DropDownList)

            Dim ds As DataSet

            '     If ddl_StoreCds.SelectedIndex <> -1 Then
            ds = objFrPOBiz.GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(CoprStoreCd, ddl_StoreCds.SelectedItem.Value)
            '  Else
            ' ds = objFrPOBiz.GetZonesForCorpStoreCd_Franchise1(CoprStoreCd)
            'End If


            If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
                With ddlDelZoneCd
                    .DataSource = ds
                    .DataValueField = "ZONE_CD"
                    .DataTextField = "full_desc"
                    .DataBind()
                End With
            Else
                ddlDelZoneCd.Items.Clear()
            End If
        Else
            litMessage.Text = "Please select Franchise Store # first"
        End If

    End Sub
    '''Delivery Zone code dropdown 
    Protected Sub ddlDelZoneCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str As String = String.Empty
        ' AddNewRow()
    End Sub
    ''Fill dropdown with Corporate Store Code
    Public Sub PopulateCorpStoreCode(ByRef p_ddlCorpStoreCode As DropDownList)
        Dim datSet As DataSet = objFrPOBiz.GetCorpStores()
        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
            With p_ddlCorpStoreCode
                .DataSource = datSet
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        End If
    End Sub
    Public Sub PopulateDelZoneCode(ByRef p_ddlDelZoneCode As DropDownList, ByVal p_CoprStoreCode As String)
        Dim ds As DataSet = objFrPOBiz.GetZonesForCorpStoreCd_Franchise(p_CoprStoreCode)

        If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
            With p_ddlDelZoneCode
                .DataSource = ds
                .DataValueField = "ZONE_CD"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        End If
    End Sub
    Public Sub PopulateDelZoneCodeByDeliveryStoreCode(ByRef p_ddlDelZoneCode As DropDownList, ByVal p_CoprStoreCode As String)
        Dim ds As DataSet = objFrPOBiz.GetZonesForCorpStoreCd_Franchise1(p_CoprStoreCode)

        If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
            With p_ddlDelZoneCode
                .DataSource = ds
                .DataValueField = "ZONE_CD"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        End If
    End Sub

    'Clear Control Text
    Public Sub btnClear_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const _CREATE As String = "CREATE"
        Const _QUERY As String = "QUERY"

        ASPxDateEdit.Text = String.Empty
        lblFrCustCd.Text = String.Empty
        ddl_StoreCds.SelectedIndex = -1
        'lblCorpFrStoreNo.Text = String.Empty
        'lblCorpFrStoreName.Text = String.Empty
        lblMessage.Text = String.Empty

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_QUERY)) Then

            gvMain.DataSource = Nothing
            gvMain.DataBind()

            dvQuery.Style("display") = "none"

            txtQuery_ConfirmationNum.Text = String.Empty
        End If

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_CREATE)) Then

            txtApprover_Pswd.Text = String.Empty
            txtApprover_User_Init.Text = String.Empty

            grvItems.DataSource = Nothing
            grvItems.DataBind()

            SetInitialRow()
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub rdbCreate_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("MODE") = "CREATE"
        dvCreate.Style("display") = "block"

        btnClear_Click(sender, e)

        lblDate.Visible = True

        lblCity.Visible = True
        lblCityCaption.Visible = True

        lblFrCustCd.Visible = True
        lblFrCustCdCaption.Visible = True

        btnSearch.Visible = False
        btnClear.Visible = False
        lblQuery_ConfirmNum.Visible = False
        txtQuery_ConfirmationNum.Visible = False
        ASPxDateEdit.Visible = False

        dvQuery.Style("display") = "none"


    End Sub
    Protected Sub rdbQuery_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("MODE") = "QUERY"
        dvCreate.Style("display") = "none"

        btnClear_Click(sender, e)

        btnSearch.Visible = True
        btnClear.Visible = True

        lblQuery_ConfirmNum.Visible = True
        txtQuery_ConfirmationNum.Visible = True
        ASPxDateEdit.Visible = True

        lblDate.Visible = False
        lblCity.Visible = False
        lblCityCaption.Visible = False

        lblFrCustCd.Visible = False
        lblFrCustCdCaption.Visible = False
        'lblCorpFrStoreNo.Visible = False
        '  lblCorpFrStoreNoCaption.Visible = False

        dvQuery.Style("display") = "block"

    End Sub
    Public Sub SetInitialRow()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow = Nothing
        dt.Columns.Add(New DataColumn("RowNumber", GetType(String)))
        dt.Columns.Add(New DataColumn("Col1", GetType(String)))
        dt.Columns.Add(New DataColumn("Col2", GetType(String)))
        dt.Columns.Add(New DataColumn("Col3", GetType(String)))
        dt.Columns.Add(New DataColumn("Col4", GetType(String)))

        dr = dt.NewRow()
        dr("RowNumber") = 1
        dr("Col1") = String.Empty
        dr("Col2") = String.Empty
        dr("Col3") = String.Empty
        dr("Col4") = String.Empty

        dt.Rows.Add(dr)

        ViewState("CurrentTable") = dt

        grvItems.DataSource = dt
        grvItems.DataBind()

        Session("linenumber") = 1




    End Sub







    'Private Sub SetPreviousData()

    '    Dim rowIndex As Integer = 0
    '    Dim dayOfWeek_selected As String = String.Empty
    '    Dim corpStoreCode As String = String.Empty
    '    Dim corpStoreCode_Sel As String = String.Empty

    '    If Not ViewState("CurrentTable") Is Nothing Then
    '        Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
    '        If dt.Rows.Count > 0 Then
    '            Dim i As Integer
    '            For i = 0 To dt.Rows.Count - 1 Step i + 1
    '                'Set the Previous Selected Items on Each DropDownList on Postbacks
    '                Dim ddlDaysOfWeek As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlDayofWeek"), DropDownList)
    '                Dim ddlCorpoStoreCode As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlCorpStoreCode"), DropDownList)
    '                Dim ddlDelZoneCode As DropDownList = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlDelZoneCode"), DropDownList)

    '                'Fill the DropDownList with Data
    '                PopulateCorpStoreCode(ddlCorpoStoreCode)

    '                If i < dt.Rows.Count - 1 Then

    '                    dayOfWeek_selected = dt.Rows(i)("Col1").ToString()
    '                    ddlDaysOfWeek.ClearSelection()
    '                    ddlDaysOfWeek.Items.FindByText(dayOfWeek_selected).Selected = True

    '                    corpStoreCode_Sel = dt.Rows(i)("Col2").ToString()
    '                    ddlCorpoStoreCode.ClearSelection()
    '                    ddlCorpoStoreCode.Items.FindByText(corpStoreCode_Sel).Selected = True

    '                    corpStoreCode = corpStoreCode_Sel.Substring(0, corpStoreCode_Sel.IndexOf(" -"))
    '                    PopulateDelZoneCode(ddlDelZoneCode, corpStoreCode)

    '                    ddlDelZoneCode.ClearSelection()
    '                    ddlDelZoneCode.Items.FindByText(dt.Rows(i)("Col3").ToString()).Selected = True

    '                End If

    '                rowIndex = rowIndex + 1
    '            Next
    '        End If
    '    End If

    'End Sub


    Private Sub SetPreviousDataAfterDel()

        Dim rowIndex As Integer = 0
        Dim dayOfWeek_selected As String = String.Empty
        Dim corpStoreCode As String = String.Empty
        Dim corpStoreCode_Sel As String = String.Empty

        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            If dt.Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dt.Rows.Count - 1 Step i + 1
                    'Set the Previous Selected Items on Each DropDownList on Postbacks
                    Dim ddlDaysOfWeek As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlDayofWeek"), DropDownList)
                    Dim ddlCorpoStoreCode As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlCorpStoreCode"), DropDownList)
                    Dim ddlDelZoneCode As DropDownList = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlDelZoneCode"), DropDownList)

                    'Fill the DropDownList with Data
                    PopulateCorpStoreCode(ddlCorpoStoreCode)

                    If i <= dt.Rows.Count - 1 Then

                        dayOfWeek_selected = dt.Rows(i)("Col1").ToString()
                        ddlDaysOfWeek.ClearSelection()
                        ddlDaysOfWeek.Items.FindByText(dayOfWeek_selected).Selected = True

                        corpStoreCode_Sel = dt.Rows(i)("Col2").ToString()
                        ddlCorpoStoreCode.ClearSelection()
                        ddlCorpoStoreCode.Items.FindByText(corpStoreCode_Sel).Selected = True

                        corpStoreCode = corpStoreCode_Sel.Substring(0, corpStoreCode_Sel.IndexOf(" -"))
                        PopulateDelZoneCode(ddlDelZoneCode, corpStoreCode)

                        ddlDelZoneCode.ClearSelection()
                        ddlDelZoneCode.Items.FindByText(dt.Rows(i)("Col3").ToString()).Selected = True

                    End If

                    rowIndex = rowIndex + 1
                Next
            End If
        End If

    End Sub

    Protected Sub grvItems_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        'SetRowData()
        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            Dim drCurrentRow As DataRow = Nothing
            Dim rowIndex As Integer = Convert.ToInt32(e.RowIndex)
            If dt.Rows.Count > 1 Then
                dt.Rows.Remove(dt.Rows(rowIndex))
                drCurrentRow = dt.NewRow()
                ViewState("CurrentTable") = dt
                grvItems.DataSource = dt
                grvItems.DataBind()

                Dim i As Integer
                For i = 0 To grvItems.Rows.Count - 1 - 1 Step i + 1
                    grvItems.Rows(i).Cells(0).Text = Convert.ToString(i + 1)
                Next
                SetPreviousData()
            End If
        End If
    End Sub
    ''''Summary
    '''' Bind Corporate Store codes to dropdownlist
    ''''Summary
    Protected Sub grvItems_OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            'Find the DropDownList in the Row
            Dim ddlCoprStoreCd As DropDownList = CType(e.Row.FindControl("ddlCorpStoreCode"), DropDownList)
            Dim datSet As DataSet = objFrPOBiz.GetCorpStores()

            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
                With ddlCoprStoreCd
                    .DataSource = datSet
                    .DataValueField = "store_cd"
                    .DataTextField = "full_desc"
                    .DataBind()
                End With
            End If

            Dim ddlDelZoneCode As DropDownList = CType(e.Row.FindControl("ddlDelZoneCode"), DropDownList)

            Dim ds As DataSet = objFrPOBiz.GetZonesForCorpStoreCd_Franchise1(ddlCoprStoreCd.SelectedValue)

            If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
                With ddlDelZoneCode
                    .DataSource = ds
                    .DataValueField = "ZONE_CD"
                    .DataTextField = "full_desc"
                    .DataBind()
                End With
            End If

        End If

    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        'lblMessage.Text = String.Empty
        If Convert.ToInt16(Session("linenumber")) = _MAX_LINE_ITEMS Then
            lblMessage.Text = "Maximum Items to Order is " & _MAX_LINE_ITEMS
        Else
            ' Session("addnewrow") = "y"
            AddNewRow()
            Session("linenumber") = Convert.ToInt16(Session("linenumber")) + 1
        End If
    End Sub

    Private Sub AddNewRow()
        Dim rowIndex As Integer = 0

        Try
            If Not ViewState("CurrentTable") Is Nothing Then

                Dim dtCurrentTable As System.Data.DataTable = CType(ViewState("CurrentTable"), DataTable)
                Dim drCurrentRow As DataRow = Nothing

                If dtCurrentTable.Rows.Count > 0 Then

                    Dim i As Integer
                    For i = 1 To dtCurrentTable.Rows.Count Step i + 1

                        'extract the TextBox values

                        Dim box1 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlDayofWeek"), DropDownList)
                        Dim box2 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlCorpStoreCode"), DropDownList)
                        Dim box3 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("ddlDelZoneCode"), DropDownList)

                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = i + 1



                        dtCurrentTable.Rows(i - 1)("Col1") = box1.Text
                        If (box1.Text.Length = 0) Then
                            Return
                        Else
                            dtCurrentTable.Rows(i - 1)("Col2") = box2.Text
                            dtCurrentTable.Rows(i - 1)("Col3") = box3.Text
                            'dtCurrentTable.Rows(i - 1)("Col4") = box4.Text
                            rowIndex = rowIndex + 1
                        End If
                    Next

                    dtCurrentTable.Rows.Add(drCurrentRow)
                    ViewState("CurrentTable") = dtCurrentTable


                    grvItems.DataSource = dtCurrentTable
                    grvItems.DataBind()

                End If
            Else
                Response.Write("ViewState is null")
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
        SetPreviousData()
    End Sub

    Private Sub SetPreviousData()
        Dim rowIndex As Integer = 0

        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            If dt.Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To dt.Rows.Count - 1 Step i + 1

                    Dim box1 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlDayofWeek"), DropDownList)
                    Dim box2 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlCorpStoreCode"), DropDownList)
                    Dim box3 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("ddlDelZoneCode"), DropDownList)

                    box1.Text = dt.Rows(i)("Col1").ToString()
                    box2.Text = dt.Rows(i)("Col2").ToString()

                    Dim ds As DataSet = objFrPOBiz.GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(box2.SelectedValue, ddl_StoreCds.SelectedItem.Value)

                    If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
                        With box3
                            .DataSource = ds
                            .DataValueField = "ZONE_CD"
                            .DataTextField = "full_desc"
                            .DataBind()
                        End With
                    Else
                        box3.Items.Clear()
                    End If


                    ' box3.Text = dt.Rows(i)("Col3").ToString()

                    rowIndex = rowIndex + 1
                Next
            End If
        End If

    End Sub


    Protected Sub GetFrStoreDetails(ByVal p_FrStoreCode As String)
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String
        Dim sqlString As StringBuilder

        sqlString = New StringBuilder("select str.store_cd, str.fran_cust_cd as fr_cust_cd, str.store_name, str.city ")
        sqlString.Append("from store str ")
        sqlString.Append("WHERE UPPER(str.store_cd) =UPPER('" & p_FrStoreCode & "') ")

        sql = sqlString.ToString()

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                'lblCorpFrStoreNo.Text = dbReader.Item("whse_store_cd")
                ' lblCorpFrStoreName.Text = dbReader.Item("store_name")
                lblFrCustCd.Text = dbReader.Item("fr_cust_cd")
                lblCity.Text = dbReader.Item("city")
                Dim CoprStoreCd As String = dbReader.Item("store_cd")

                'bind zone code for selected franchise store

                For i As Integer = 0 To grvItems.Rows.Count - 1
                    Dim ddlCorpStoreCode As DropDownList = CType(grvItems.Rows(i).Cells(1).FindControl("ddlCorpStoreCode"), DropDownList)
                    Dim ddlDelZoneCode As DropDownList = CType(grvItems.Rows(i).Cells(2).FindControl("ddlDelZoneCode"), DropDownList)
                    Dim ds As DataSet = objFrPOBiz.GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(ddlCorpStoreCode.SelectedValue, CoprStoreCd)

                    If SystemUtils.dataSetHasRows(ds) AndAlso SystemUtils.dataRowIsNotEmpty(ds.Tables(0).Rows(0)) Then
                        With ddlDelZoneCode
                            .DataSource = ds
                            .DataValueField = "ZONE_CD"
                            .DataTextField = "full_desc"
                            .DataBind()
                        End With
                    Else
                        ddlDelZoneCode.Items.Clear()
                    End If


                Next



                'ddlCorpStoreCode.SelectedValue = CoprStoreCd



            End If
        Catch ex As Exception
            Dim err As String = ex.ToString()

            'do nothing
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

    End Sub
    Protected Sub grvItems_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'Checking whether the Row is Data Row
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Finding the Dropdown control.
            Dim ctrl As Control = e.Row.FindControl("ddl_CorpStoreCd")
            Dim ddCorpStoreCd As Control = e.Row.FindControl("ddl_CorpStoreCd")

            If Not ctrl Is Nothing Then
                Dim dd As DropDownList = ctrl

            End If
        End If
    End Sub

    'Validate Approver Password
    Public Sub txtApprover_Pswd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim isValid As String = String.Empty

        If txtApprover_Pswd.Text.Length > 0 Then
            isValid = objFrPOBiz.isvalidApproverPassword(txtApprover_Pswd.Text)

            If isValid = "N" Or isValid & "" = "" Then
                lblMessage.Text = "This Approver Password is not Valid or Authorized"
                Return
            Else
                'Get Approver User Init
                txtApprover_User_Init.Text = objFrPOBiz.GetApprUserInitByPass(txtApprover_Pswd.Text)
            End If
        End If
    End Sub
    'Confirm 
    Public Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmASP.Click
        Const Const_Success As String = "SUCCESS"
        Dim objTrans As FRShippingSch_TransDtc = New FRShippingSch_TransDtc
        Dim lstDetails As New List(Of FRShippingSch_DetDtc)

        Dim row As GridViewRow
        Dim result As String = String.Empty
        Dim rowIndex As Int16 = 0
        Dim SKU As String = String.Empty
        Dim strItemSKU As String = String.Empty
        Try
            'Validate Required Fields
            If IsValidRequiredFields() Then

                objTrans.ID = 0
                objTrans.FR_Cust_CD = lblFrCustCd.Text.Trim
                objTrans.FR_CO_CD = ddl_StoreCds.Value.ToString.Trim
                objTrans.FR_Store_Num = IIf(ddl_StoreCds.Text.Length > 0, ddl_StoreCds.Value, "")
                'objTrans.FR_Store_Num = lblCorpFrStoreNo.Text.Trim.Substring(0, 2)
                ''objTrans.Corp_Zone_CD = IIf(ddlCorpDelZoneCd.Text.Length > 0, "", ddlOrdCorpStoreCd.SelectedItem.Text)
                objTrans.City = lblCity.Text.Trim
                objTrans.Upd_By = Session("emp_init")
                objTrans.Upd_Dt = DateTime.Today()

                objTrans.Approval_Pswd = txtApprover_Pswd.Text.Trim
                objTrans.Approval_User_Init = txtApprover_User_Init.Text.Trim

                'Get Line item details
                If Not ViewState("CurrentTable") Is Nothing Then

                    Dim dtCurrentTable As DataTable = CType(ViewState("CurrentTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If dtCurrentTable.Rows.Count > 0 Then

                        Dim i As Integer
                        For i = 1 To dtCurrentTable.Rows.Count Step i + 1
                            Dim objDetails As FRShippingSch_DetDtc = New FRShippingSch_DetDtc

                            ' objDetails.LineNo = i
                            objDetails.Day_Of_Week = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("ddlDayofWeek"), DropDownList).Text
                            objDetails.Corp_Store_CD = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("ddlCorpStoreCode"), DropDownList).Text
                            objDetails.Corp_Del_Zone_CD = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlDelZoneCode"), DropDownList).Text
                            objDetails.Updated_Date = DateTime.Today()

                            lstDetails.Add(objDetails)
                            rowIndex = rowIndex + 1
                        Next
                    End If
                Else
                    lblMessage.Text = "Error occurred at Confirm click"
                End If

                'Generate Confirmation Number

                'Call Sproc to Insert values into table
                result = objFrPOBiz.CreateFranchisePO(lstDetails, objTrans)

                'Display Approver User Initial and confirmation Number
                txtConfirmationNum.Text = objTrans.Confirm_Num

                If result.ToUpper().Contains(Const_Success) Then
                    lblMessage.ForeColor = Color.Green
                    lblMessage.Text = result & " Confirmation # : " & objTrans.Confirm_Num
                Else
                    lblMessage.ForeColor = Color.Red
                    lblMessage.Text = result & " Confirmation # : "
                End If

            End If
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub


    'Validate Required Fields
    Public Function IsValidRequiredFields() As Boolean
        Dim result As Boolean = False
        Dim requiredMsg As String = String.Empty
        requiredMsg = "Following Values are Mandatory - Franchise Store Code,  Ordering From Corporate Store code, Corporate Delivery Zone Code,  Approver Password"
        Try
            If ddl_StoreCds.SelectedIndex = -1 Or
                txtApprover_Pswd.Text.Trim.Length = 0 Then

                lblMessage.Text = requiredMsg
                result = False
            Else
                result = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return result
    End Function

    Public Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Text = String.Empty
        GridView1_Binddata()
        dvQuery.Style("display") = "block"
    End Sub
    'Query - Search initial main result 
    Private Sub GridView1_Binddata()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim sb As StringBuilder = New StringBuilder
        Dim CatchMe As Boolean
        Dim dv As DataView
        Dim proceed As Boolean
        Dim LikeQry As String

        Dim objTrans As FRShippingSch_TransDtc = New FRShippingSch_TransDtc
        Dim MyDataReader As OracleDataReader
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        gvMain.DataSource = ""
        'CustTable.Visible = False
        CatchMe = False

        Try
            'Get search criteria from controls
            objTrans.FR_Cust_CD = lblFrCustCd.Text.Trim
            'Dim a As Integer = ddl_StoreCds.SelectedIndex
            objTrans.FR_Store_Num = If(ddl_StoreCds.SelectedIndex > -1, ddl_StoreCds.Value.ToString.Trim, String.Empty)
            'objTrans.FR_Cust_CD = If(lblCorpFrStoreNo.Text.Length > 0, lblCorpFrStoreNo.Text.Trim.Substring(0, 2), String.Empty)
            objTrans.Confirm_Num = txtQuery_ConfirmationNum.Text.Trim

            If ASPxDateEdit.Value Is Nothing Then
                objTrans.Upd_Dt = DBNull.Value
            Else
                objTrans.Upd_Dt = Convert.ToDateTime(ASPxDateEdit.Text)
            End If

            gvMain.Visible = True
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sb.Append("SELECT FSS.CONFIRM_NUM, FSS.store_num || ' - ' || S.store_NAME as store_full_desc, FSS.co_cd, FSS.cust_cd, FSS.aprv_user_init, FSS.upd_by, FSS.upd_dt  FROM FR_SHIPPING_SCHEDULE fss, ")
            sb.Append(" store S ")
            sb.Append("  where FSS.store_num = S.store_cd AND ")

            sql = sb.ToString()

            ''FR_Store_CD
            'If objTrans.FR_Store_Num & "" <> "" Then
            '    If proceed = True Then sql = sql & "AND "
            '    If InStr(objTrans.FR_Store_Num, "%") > 0 Then
            '        LikeQry = "LIKE"
            '    Else
            '        LikeQry = "="
            '    End If
            '    sql = sql & "STORE_CD " & LikeQry & " '" & Replace(objTrans.FR_Store_Num, "'", "''") & "' "
            '    proceed = True
            'End If
            'FR_Store_Num
            If objTrans.FR_Store_Num & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.FR_Store_Num, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "STORE_NUM " & LikeQry & " '" & Replace(objTrans.FR_Store_Num, "'", "''") & "' "
                proceed = True
            End If


            'Confirmation num
            If objTrans.Confirm_Num & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Confirm_Num, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CONFIRM_NUM " & LikeQry & " '" & Replace(objTrans.Confirm_Num, "'", "''") & "' "
                proceed = True
            End If

            'Create Date
            If objTrans.Upd_Dt & "" <> "" And Not IsNothing(objTrans.Upd_Dt) Then
                If proceed = True Then sql = sql & "AND "
                LikeQry = "="
                'sql = sql & "ENTER_DATE " & LikeQry & " '" & Replace(objTrans.Create_Date, "'", "''") & "' "
                sql = sql & "UPD_DT " & LikeQry & "TO_DATE('" & objTrans.Upd_Dt & "','mm/dd/yyyy') "
                proceed = True
            End If

            If Right(sql, 6) = "WHERE " Then
                sql = Replace(sql, "WHERE", "")
                'lblMessage.Text = "No Search Criteria entered."
                'Exit Sub
            End If

            sql = sql & (" order by FSS.store_num ")
            sql = UCase(sql)

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Dim MyTable As DataTable
            Dim numrows As Integer
            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                If numrows = 1 Then
                    gvMain.DataSource = dv
                    gvMain.DataBind()
                Else
                    gvMain.DataSource = dv
                    gvMain.DataBind()
                End If
            Else
                gvMain.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        lbl_pageinfo.Text = "Page " + CStr(gvMain.PageIndex + 1) + " of " + CStr(gvMain.PageCount)
        ' make all the buttons visible if page count is more than 0
        If gvMain.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If gvMain.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If gvMain.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If gvMain.PageIndex = (gvMain.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If gvMain.PageCount = 0 Then
            lblMessage.Text = "Sorry, no records found.  Please search again." & vbCrLf & vbCrLf
            ' Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub
    'Query - Page Navigation
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If gvMain.PageIndex < (gvMain.PageCount - 1) Then
                    gvMain.PageIndex += 1
                End If
            Case "Prev"
                If gvMain.PageIndex > 0 Then
                    gvMain.PageIndex -= 1
                End If
            Case "Last"
                gvMain.PageIndex = gvMain.PageCount - 1
            Case Else
                gvMain.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    Public Sub GridView1_OnSelectionChanged(ByVal sender As Object, ByVal e As DevExpress.Data.SelectionChangedEventArgs)
        Dim sel As String = String.Empty
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim sb As StringBuilder = New StringBuilder
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet
        Dim LikeQry As String
        Dim objTrans As TransDtc = New TransDtc


        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim id As String = gvMain.DataKeys(e.Row.RowIndex).Value.ToString()
            Dim gvDetLn As GridView = TryCast(e.Row.FindControl("gvDetails_ln"), GridView)

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If


            sb.Append(" SELECT fssl.confirm_num, ")
            sb.Append("(case when fssl.day_of_week = 'M' then 'Monday'  ")
            sb.Append("when fssl.day_of_week = 'T' then 'Tuesday'  ")
            sb.Append("when fssl.day_of_week = 'W' then 'Wednesday'  ")
            sb.Append("when fssl.day_of_week = 'R' then 'Thursday'   ")
            sb.Append("when fssl.day_of_week = 'F' then 'Friday' end) day_of_week, ")
            sb.Append("fssl.corp_store_cd || ' - ' || s.store_name as corp_store_full_desc,  ")
            sb.Append("fssl.del_zone_cd || '-' || z.des as del_zone_full_desc ")
            sb.Append("FROM FR_SHIPPING_SCHEDULE_LN fssl, store s, zone z ")
            sb.Append("WHERE fssl.corp_store_cd = s.store_cd And fssl.del_zone_cd = z.zone_cd and CONFIRM_NUM = '" & id & "'")

            sql = sb.ToString()
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Dim MyTable As DataTable
            Dim numrows As Integer
            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            If numrows > 0 Then
                'Data - details
                gvDetLn.DataSource = dv
                gvDetLn.DataBind()
            End If
        End If


    End Sub
    Protected Sub gvDetails_Ln_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)



        Dim gvDetLn As GridView = TryCast(sender, GridView)

            If gvDetLn.Rows.Count > 0 Then
                Dim ConfirmationNumber As String = gvDetLn.Rows(Convert.ToInt32(e.RowIndex)).Cells(0).Text
            Dim storeCode As String = gvDetLn.Rows(Convert.ToInt32(e.RowIndex)).Cells(2).Text.Substring(0, 2)
            Dim ZoneCode As String = gvDetLn.Rows(Convert.ToInt32(e.RowIndex)).Cells(3).Text.Substring(0, 3)




            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
            Dim sql As String
            Dim sb As StringBuilder = New StringBuilder
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand()

            Try


                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
         ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If

                conn.Open()
                dbCommand.Connection = conn

                dbCommand.Parameters.Clear()
                dbCommand.Parameters.Add(":confirm_num", OracleType.VarChar)
                dbCommand.Parameters(":confirm_num").Value = ConfirmationNumber

                dbCommand.Parameters.Add(":store_cd", OracleType.VarChar)
                dbCommand.Parameters(":store_cd").Value = storeCode

                dbCommand.Parameters.Add(":zone_cd", OracleType.VarChar)
                dbCommand.Parameters(":zone_cd").Value = ZoneCode

                dbCommand.CommandType = CommandType.Text
                dbCommand.CommandText = "DELETE FROM FR_SHIPPING_SCHEDULE_LN WHERE CONFIRM_NUM =:confirm_num and CORP_STORE_CD=:store_cd and DEL_ZONE_CD=:zone_cd"
                dbCommand.ExecuteNonQuery()
                GridView1_Binddata()

                gvDetLn.DataBind()

                'For i As Integer = 0 To gvMain.Rows.Count - 1
                '    If gvMain.DataKeys(i).Value = ConfirmationNumber Then
                '        Dim detailImage As WebControls.Image = CType(gvMain.Rows(i).FindControl("detailImage"), WebControls.Image)
                '        'pnl_Ln.Style("display") = "block"
                '        detailImage.Attributes("src") = "images/minus.png"

                '    End If
                'Next
                'gvDetLn.Style("display") = "block"

                Session("MODE") = "QUERY"
                dvQuery.Style("display") = "block"


            Catch ex As Exception
                dbCommand.Transaction.Rollback()
                Throw
            Finally
                conn.Dispose()
                dbCommand.Dispose()
            End Try
        End If


    End Sub
End Class
