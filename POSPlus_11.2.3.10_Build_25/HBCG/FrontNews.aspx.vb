Imports System.Data.OracleClient

Partial Class FrontNews
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "SELECT COMMENT_TEXT FROM COMMENT_SETUP WHERE COMMENT_TP='MAIN'"
        If Not IsPostBack Then
            Using conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, False), _
                objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn, False)

                conn.Open()
                Using MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objSql, False)
                    If MyDataReader.Read() Then
                        If Not String.IsNullOrEmpty(MyDataReader.Item("COMMENT_TEXT").ToString) Then
                            main_body.InnerHtml = MyDataReader.Item("COMMENT_TEXT").ToString
                        Else
                            main_body.InnerHtml = "<b><u>" & FormatDateTime(Now.Date, DateFormat.ShortDate) & "</u></b><br /><br />No news found"
                        End If
                    Else
                        main_body.InnerHtml = "<b><u>" & FormatDateTime(Now.Date, DateFormat.ShortDate) & "</u></b><br /><br />No news found"
                    End If
                End Using
            End Using
        End If

    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

End Class
