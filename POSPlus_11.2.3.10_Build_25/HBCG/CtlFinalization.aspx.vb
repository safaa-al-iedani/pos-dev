'Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq


Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class CtlFinalization
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If IsPostBack = False Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As StringBuilder
            Dim ds As New DataSet
            Dim Mydatareader As OracleDataReader
            Dim homeStoreCode As String
            Dim companyCode As String
            Dim coGroupCode As String
            ' lbl_versionInfo.Text = gs_version
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            btn_search.Enabled = False
            btn_clr_screen.Enabled = False

            If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
                Return
            End If

            Dim str_emp_init As String = LeonsBiz.GetEmpInit(Session("EMP_CD"))

            sqlString = New StringBuilder("SELECT e.home_store_cd, s.co_cd, cg.co_grp_cd FROM emp e, store s, co_grp cg  ")
            sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd ")
            ' sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd AND e.emp_cd = UPPER(:p_emp_cd)")
            objCmd.CommandText = sqlString.ToString()
            objCmd.Connection = conn
            '  objCmd.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            '   objCmd.Parameters(":p_emp_cd").Value = Session("EMP_CD").ToString().ToUpper()
            Try
                conn.Open()
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)

                If Mydatareader.Read() Then

                    '   txt_company_code.Text = Mydatareader.Item("CO_CD").ToString()
                    '   txt_company_code.Enabled = False
                    companyCode = Mydatareader.Item("CO_CD").ToString()

                    homeStoreCode = Mydatareader.Item("HOME_STORE_CD").ToString()
                    Session("HOME_STORE_CD") = homeStoreCode

                    coGroupCode = Mydatareader.Item("CO_GRP_CD").ToString()
                    Session("CO_GRP_CD") = coGroupCode

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            If String.IsNullOrEmpty(homeStoreCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find home store code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0037
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            ElseIf String.IsNullOrEmpty(coGroupCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find company group code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0038
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            Else

                sqlString = New StringBuilder("SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s   ")
                sqlString.Append("where std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'") 'lucy

                '  If (homeStoreCode = "00" Or homeStoreCode = "10") Then
                sqlString.Append(" UNION SELECT sg.store_grp_cd store_cd, sg.store_grp_cd || ' - ' ||  sg.des as full_desc FROM store_grp sg ")
                sqlString.Append("WHERE EXISTS (SELECT 1 FROM store_grp$store sgs, store s  ")
                sqlString.Append("WHERE sgs.store_grp_cd = sg.store_grp_cd AND sgs.store_cd = s.store_cd ")

                '   sqlString.Append("AND std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'") 'lucy
                '  sqlString.Append(" and length(sg.store_grp_cd) >2 ")
                ' sqlString.Append(" and sg.store_grp_cd <> 'ALL' ")

                '  If Not (homeStoreCode) = "00" Then
                'sqlString.Append(" and sg.store_grp_cd <> 'ALL' ")
                ' End If
                sqlString.Append(")")
                '  End If
                ' If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                'sqlString.Append("and s.co_cd = :co_cd ")
                ' End If
                '  sqlString.Append("AND sgs.store_grp_cd != s.store_cd AND s.co_cd = cg.co_cd AND cg.co_grp_cd = :co_grp_cd)")

                ' sqlString.Append("AND NOT EXISTS (SELECT 1 FROM STORE_GRP$STORE SGS2 WHERE SGS2.STORE_GRP_CD = SG.STORE_GRP_CD ")
                ' sqlString.Append("AND SGS2.STORE_CD NOT IN (SELECT s2.store_cd FROM store s2, co_grp cg  WHERE s2.co_cd = cg.co_cd ")
                ' If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                'sqlString.Append("and s2.co_cd = :co_cd ")
                'End If
                'sqlString.Append("))")
                ' sqlString.Append("AND cg.co_grp_cd = :co_grp_cd))")

                objCmd.CommandText = sqlString.ToString()
                objCmd.Connection = conn
                objCmd.Parameters.Clear()
                '  objCmd.Parameters.Add(":co_grp_cd", OracleType.VarChar)
                '  objCmd.Parameters(":co_grp_cd").Value = coGroupCode
                '  If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                'objCmd.Parameters.Add(":co_cd", OracleType.VarChar)
                ' objCmd.Parameters(":co_cd").Value = companyCode
                ' End If
                objCmd.Parameters.Add(":str_emp_init", OracleType.VarChar)
                objCmd.Parameters(":str_emp_init").Value = str_emp_init

                Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

                Try
                    dataAdapter.Fill(ds)
                Catch
                    Throw
                    conn.Close()
                End Try
                conn.Close()
                If ds.Tables(0).Rows.Count = 0 Then
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
                    lbl_warning.Text = Resources.CustomMessages.MSG0039
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                    Return
                End If
                Dim defaultStoreIndex As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows

                    drpdwn_store.Items.Add(row.ItemArray(1).ToString())
                    dw_st.Items.Add(row.ItemArray(0).ToString())         ' store_grp_cd/store_cd
                    If row.ItemArray(0).ToString = homeStoreCode.ToString() Then
                        defaultStoreIndex = ds.Tables(0).Rows.IndexOf(row)
                    End If
                Next
                'this is defaul to home_store, commented
                drpdwn_store.SelectedIndex = defaultStoreIndex
                dw_st.SelectedIndex = defaultStoreIndex
                Session("defaultStoreIndex") = defaultStoreIndex


            End If
        End If
        drpdwn_p_d.Items.Add("P")
        drpdwn_p_d.Items.Add("D")
        drpdwn_p_d.Items.Add("")

        drpdwn_p_d.SelectedIndex = 2

        populate_enabled()
        populate_min_pick_status()


        

        btn_search.Enabled = True
        btn_clr_screen.Enabled = True

        btn_update.Visible = False
    End Sub
   
   
    Public Sub populate_enabled()
        '  If IsPostBack = False Then
        
        drpdwn_enabled.Items.Clear()
        drpdwn_enabled.Items.Add("Y")
        drpdwn_enabled.Items.Add("N")
        drpdwn_enabled.Items.Add("")

        drpdwn_enabled.SelectedIndex = 2



        '   End If


    End Sub
    Private Sub populate_pd()

        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtPD As String
        Dim txtStore As String
        'iterate through all the rows  
        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)

            drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_pd"), DropDownList)



            drpdwn.Items.Add("P")
            drpdwn.Items.Add("D")


            txtPD = dgItem.Cells(2).Text
            txtStore = dgItem.Cells(1).Text
            drpdwn.Text = txtPD
        Next


    End Sub
    Private Sub populate_yn()

        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtEnabled As String
        Dim txtStore As String
        'iterate through all the rows  
        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)

            drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_yn"), DropDownList)

            drpdwn.Items.Add("Y")
            drpdwn.Items.Add("N")


            txtEnabled = dgItem.Cells(4).Text

            drpdwn.Text = txtEnabled
        Next


    End Sub

    Private Sub populate_min_status()

        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtMinStat As String
        Dim txtStore As String
        'iterate through all the rows  
        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)

            drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_min_status"), DropDownList)


            drpdwn.Items.Add("S")
            drpdwn.Items.Add("P")
            drpdwn.Items.Add("C")


            txtMinStat = dgItem.Cells(3).Text
            txtStore = dgItem.Cells(1).Text
            drpdwn.Text = txtMinStat
        Next


    End Sub
    Public Sub populate_min_pick_status()

        drpdwn_min_pick_status.Items.Add("S")
        drpdwn_min_pick_status.Items.Add("P")
        drpdwn_min_pick_status.Items.Add("C")
        drpdwn_min_pick_status.Items.Add("")

        'this is defaul to Y and N

        drpdwn_min_pick_status.SelectedIndex = 3




    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0079
        _lblHeader.Text = "Minimum pick status delivery store maintenance"
    End Sub



    Protected Sub click_btn_search(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String

        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button



        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value


        lbl_msg.Text = Nothing

        reload()
           
        

    End Sub
    Protected Sub old_click_btn_search(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String

        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button



        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value


        lbl_msg.Text = Nothing

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            ds = New DataSet
            GV_SRR.DataSource = ""



            sqlString = New StringBuilder("SELECT  ")

            sqlString.Append(" rowid,store_cd,pu_del ,min_stat,enabled ")
            sqlString.Append(" FROM ctl_finalization ")
            sqlString.Append(" WHERE store_cd in(select store_cd FROM store_grp$store ")
            sqlString.Append(" WHERE store_grp_cd = :l_store_grp_cd) ")
            If Not isEmpty(l_p_d) Then
                sqlString.Append(" and pu_del = :l_p_d ")

            End If
            If Not isEmpty(l_min_pick_status) Then
                sqlString.Append(" and min_stat = : l_min_pick_status ")

            End If

            If Not isEmpty(l_enabled) Then
                sqlString.Append(" and enabled = : l_enabled ")

            End If
            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()

            objcmd.Parameters.Add(":l_store_grp_cd", OracleType.VarChar)
            objcmd.Parameters(":l_store_grp_cd").Value = l_store_grp_cd
            If Not isEmpty(l_p_d) Then

                objcmd.Parameters.Add(":l_p_d", OracleType.VarChar)
                objcmd.Parameters(":l_p_d").Value = l_p_d
            End If
            If Not isEmpty(l_min_pick_status) Then
                objcmd.Parameters.Add(":l_min_pick_status", OracleType.VarChar)
                objcmd.Parameters(":l_min_pick_status").Value = l_min_pick_status
            End If
            If Not isEmpty(l_enabled) Then
                objcmd.Parameters.Add(":l_enabled", OracleType.VarChar)
                objcmd.Parameters(":l_enabled").Value = l_enabled
            End If

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String

                    If Not IsDBNull(ds.Tables(0).Rows(i)("store_cd")) Then

                        l_st = ds.Tables(0).Rows(i)("store_cd")
                    Else
                        l_st = ""
                    End If



                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("store_cd")) Then
                            err_cnt = 1
                        End If

                        If isNotEmpty(ds.Tables(0).Rows(i)("min_stat")) Then

                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)
                btn_update.Visible = True



            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_update.Visible = False
            Else
                populate_min_status()
                populate_pd()
                populate_yn()

            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub
    Protected Sub reload()

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String

        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button



        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value


        lbl_msg.Text = Nothing

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            ds = New DataSet
            GV_SRR.DataSource = ""



            sqlString = New StringBuilder("SELECT  ")

            sqlString.Append(" rowid,store_cd,pu_del ,min_stat,enabled ")
            sqlString.Append(" FROM ctl_finalization ")
            sqlString.Append(" WHERE store_cd in(select store_cd FROM store_grp$store ")
            sqlString.Append(" WHERE store_grp_cd = :l_store_grp_cd) ")
            If Not isEmpty(l_p_d) Then
                sqlString.Append(" and pu_del = :l_p_d ")

            End If
            If Not isEmpty(l_min_pick_status) Then
                sqlString.Append(" and min_stat = : l_min_pick_status ")

            End If

            If Not isEmpty(l_enabled) Then
                sqlString.Append(" and enabled = : l_enabled ")

            End If
            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()

            objcmd.Parameters.Add(":l_store_grp_cd", OracleType.VarChar)
            objcmd.Parameters(":l_store_grp_cd").Value = l_store_grp_cd
            If Not isEmpty(l_p_d) Then

                objcmd.Parameters.Add(":l_p_d", OracleType.VarChar)
                objcmd.Parameters(":l_p_d").Value = l_p_d
            End If
            If Not isEmpty(l_min_pick_status) Then
                objcmd.Parameters.Add(":l_min_pick_status", OracleType.VarChar)
                objcmd.Parameters(":l_min_pick_status").Value = l_min_pick_status
            End If
            If Not isEmpty(l_enabled) Then
                objcmd.Parameters.Add(":l_enabled", OracleType.VarChar)
                objcmd.Parameters(":l_enabled").Value = l_enabled
            End If

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String

                    If Not IsDBNull(ds.Tables(0).Rows(i)("store_cd")) Then

                        l_st = ds.Tables(0).Rows(i)("store_cd")
                    Else
                        l_st = ""
                    End If



                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("store_cd")) Then
                            err_cnt = 1
                        End If

                        If isNotEmpty(ds.Tables(0).Rows(i)("min_stat")) Then

                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)
                btn_update.Visible = True



            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_update.Visible = False
            Else
                populate_min_status()
                populate_pd()
                populate_yn()

            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub
    

  

  
    Public Sub insert_to_table(ByVal p_store_grp_cd As String, ByVal p_pu_del As String, ByVal p_min_stat As String, ByVal p_enabled As String)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            lbl_msg.Text = Nothing

            sql = "Insert into ctl_finalization "
            sql = sql & " (store_cd,pu_del,min_stat,enabled ) "
            sql = sql & " select store_cd,:pu_del,:min_stat,:enabled  "
            ' sql = sql & " select store_cd  "
            sql = sql & " from store_grp$store"
            sql = sql & " where store_grp_cd=:store_grp_cd"
            sql = sql & " and store_cd not in (select store_cd from ctl_finalization "
            sql = sql & " where pu_del=:pu_del) "
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":store_grp_cd", OracleType.VarChar)
            objsql.Parameters(":store_grp_cd").Value = p_store_grp_cd
            objsql.Parameters.Add(":pu_del", OracleType.VarChar)
            objsql.Parameters(":pu_del").Value = p_pu_del
            objsql.Parameters.Add(":min_stat", OracleType.VarChar)
            objsql.Parameters(":min_stat").Value = p_min_stat
            objsql.Parameters.Add(":enabled", OracleType.VarChar)
            objsql.Parameters(":enabled").Value = p_enabled

            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub click_btn_insert(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String




        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value

        If isEmpty(l_p_d) Then
            lbl_warning.Text = "Please select a value from dropdown Picked/Delivered"
            Exit Sub
        ElseIf isEmpty(l_store_grp_cd) Then
            lbl_warning.Text = "Please select a value from dropdown Store_Group/Store"
            Exit Sub
        ElseIf isEmpty(l_min_pick_status) Then
            lbl_warning.Text = "Please select a value from dropdown Min Pick Status"
            Exit Sub
        ElseIf isEmpty(l_enabled) Then
            lbl_warning.Text = "Please select a value from dropdown Enabled"
            Exit Sub
        End If
        insert_to_table(l_store_grp_cd, l_p_d, l_min_pick_status, l_enabled)


        reload()

    End Sub
    Protected Sub update_record()



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

      
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing


        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtRowId As String
        Dim txtPD As String
        Dim txtStore As String
        Dim txtMinStat As String
        Dim txtEnabled As String
        Try
            'conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            ' conn.Open()

            'iterate through all the rows  
            For i As Integer = 0 To GV_SRR.Items.Count - 1


                dgItem = GV_SRR.Items(i)

                drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_pd"), DropDownList)
                txtPD = drpdwn.Text

                drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_min_status"), DropDownList)

                txtMinStat = drpdwn.Text

                drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_yn"), DropDownList)
                txtEnabled = drpdwn.Text

                txtStore = dgItem.Cells(1).Text
                txtRowId = dgItem.Cells(0).Text



                update_each_record(txtRowId, txtStore, txtPD, txtMinStat, txtEnabled)
            Next

            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub

    Protected Sub update_each_record(ByRef p_rowid As String, ByRef p_store_cd As String, ByRef p_pu_del As String, ByRef p_min_stat As String, ByRef p_yn As String)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing


        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtRowId As String
        Dim txtPD As String
        Dim txtStore As String
        Dim txtMinStat As String
        Dim txtEnabled As String
        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()



            sql = "update ctl_finalization "
            sql = sql & " set min_stat=:txtMinStat,pu_del=:txtPD, enabled=:txtEnabled"
            sql = sql & " where rowid =:txtRowId"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            objsql.Parameters(":txtRowId").Value = p_rowid
            objsql.Parameters.Add(":txtPD", OracleType.VarChar)
            objsql.Parameters(":txtPD").Value = p_pu_del
            objsql.Parameters.Add(":txtMinStat", OracleType.VarChar)
            objsql.Parameters(":txtMinStat").Value = p_min_stat
            objsql.Parameters.Add(":txtEnabled", OracleType.VarChar)
            objsql.Parameters(":txtEnabled").Value = p_yn

                'Execute DataReader 

                objsql.ExecuteNonQuery()




            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub
    Public Sub click_btn_update(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String




        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value


        update_record()

        reload()

    End Sub


     
    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV_SRR.DeleteCommand

        Dim txtRowid As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing

        txtRowid = GV_SRR.DataKeys(e.Item.ItemIndex)

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()



            sql = "delete from ctl_finalization "
            sql = sql & " where rowid =:txtRowId"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            objsql.Parameters(":txtRowId").Value = txtRowid


            'Execute DataReader 

            objsql.ExecuteNonQuery()




            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


        reload()

    End Sub

    Public Sub old_click_btn_update(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_p_d As String
        Dim l_min_pick_status As String
        Dim l_enabled As String




        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_p_d = drpdwn_p_d.SelectedItem.Value
        l_min_pick_status = drpdwn_min_pick_status.SelectedItem.Value
        l_enabled = drpdwn_enabled.SelectedItem.Value


        '  update_record(l_store_grp_cd, l_p_d, l_min_pick_status, l_enabled)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim err_cnt As Integer = 0
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        conn.Open()

        ds = New DataSet
        GV_SRR.DataSource = ""



        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append(" rowid,store_cd,pu_del ,min_stat,enabled ")
        sqlString.Append(" FROM ctl_finalization ")
        sqlString.Append(" WHERE store_cd in(select store_cd FROM store_grp$store ")
        sqlString.Append(" WHERE store_grp_cd = :l_store_grp_cd) ")
        If Not isEmpty(l_p_d) Then
            sqlString.Append(" and pu_del = :l_p_d ")

        End If
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":l_store_grp_cd", OracleType.VarChar)
        objcmd.Parameters(":l_store_grp_cd").Value = l_store_grp_cd

        If Not isEmpty(l_p_d) Then

            objcmd.Parameters.Add(":l_p_d", OracleType.VarChar)
            objcmd.Parameters(":l_p_d").Value = l_p_d
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


        objAdaptor.Fill(ds)


        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count




        If (MyDataReader.Read()) Then
            GV_SRR.Enabled = True
            GV_SRR.Visible = True
            GV_SRR.DataSource = ds
            GV_SRR.DataBind()

            Dim i As Integer

            For i = 0 To numrows - 1

                ds.Tables(0).NewRow()
                Dim l_va As String
                Dim l_st As String

                If Not IsDBNull(ds.Tables(0).Rows(i)("store_cd")) Then

                    l_st = ds.Tables(0).Rows(i)("store_cd")
                Else
                    l_st = ""
                End If
                If Len(l_store_grp_cd) = 2 Then
                    GV_SRR.Columns(0).Visible = True

                Else
                    GV_SRR.Columns(0).Visible = False
                End If
                If i > 0 And err_cnt = 0 Then
                    If isNotEmpty(ds.Tables(0).Rows(i)("store_cd")) Then
                        err_cnt = 1
                    End If


                End If

            Next
            ds.Tables(0).NewRow()
            ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            GV_SRR.DataSource = ds
            GV_SRR.DataBind()
            lbl_msg.Text = "No data found."
            GV_SRR.Enabled = False
            GV_SRR.Visible = False

        End If

        MyDataReader.Close()
        conn.Close()

        If Len(l_store_grp_cd) = 2 Then
            GV_SRR.Columns(0).Visible = True
        Else
            GV_SRR.Columns(0).Visible = False
        End If



    End Sub




    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To Clear Grid, search criteria and remove sessions
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clr_screen_Click(sender As Object, e As EventArgs)
        Dim l_id As String



        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False

        populate_enabled()
        drpdwn_store.SelectedIndex = 0
        drpdwn_p_d.SelectedIndex = 2
        drpdwn_min_pick_status.SelectedIndex = 3
        drpdwn_enabled.SelectedIndex = 2



        lbl_warning.Text = ""
        lbl_msg.Text = ""
    End Sub








End Class