Imports System.Data.OracleClient
Imports System.Data

Partial Class Sales_History
    Inherits POSBasePage

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim sdate As Date = Today.AddDays(-65).Date.ToString
        Dim edate As Date = Today.Date.ToString

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        Try
            conn.Open()

            conn2.Open()

            sql = "DELETE FROM sales_report_archive WHERE wr_dt between TO_DATE('" & sdate & "','mm/dd/RRRR') and TO_DATE('" & edate & "','mm/dd/RRRR')"
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()

            sql = "INSERT INTO sales_report_archive (DEL_DOC_NUM, STORE_CD, WR_DT, CUST_CD, ORD_TP_CD, "
            sql = sql & "ORD_SRT_CD, QTY, UNIT_PRC, SLSP1, SLSP1_PCT, SLSP2, SLSP2_PCT, PROD_GRP_CD, MNR_CD, "
            sql = sql & "TREATABLE, ITM_TP_CD, LINE_TYPE, VE_CD, ITM_CD, TREATED_BY, CAT_CD, INVENTORY)"
            sql = sql & " VALUES(:DEL_DOC_NUM, :STORE_CD, :WR_DT, :CUST_CD, :ORD_TP_CD, "
            sql = sql & ":ORD_SRT_CD, :QTY, :UNIT_PRC, :SLSP1, :SLSP1_PCT, :SLSP2, :SLSP2_PCT, :PROD_GRP_CD, :MNR_CD, "
            sql = sql & ":TREATABLE, :ITM_TP_CD, :LINE_TYPE, :VE_CD, :ITM_CD, :TREATED_BY_ITM_CD, :CAT_CD, :INVENTORY)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":WR_DT", OracleType.DateTime)
            objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
            objSql.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":ORD_SRT_CD", OracleType.VarChar)
            objSql.Parameters.Add(":QTY", OracleType.Number)
            objSql.Parameters.Add(":UNIT_PRC", OracleType.Number)
            objSql.Parameters.Add(":SLSP1", OracleType.VarChar)
            objSql.Parameters.Add(":SLSP1_PCT", OracleType.Number)
            objSql.Parameters.Add(":SLSP2", OracleType.VarChar)
            objSql.Parameters.Add(":SLSP2_PCT", OracleType.Number)
            objSql.Parameters.Add(":PROD_GRP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":TREATABLE", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_TP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":LINE_TYPE", OracleType.VarChar)
            objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":TREATED_BY_ITM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CAT_CD", OracleType.VarChar)
            objSql.Parameters.Add(":INVENTORY", OracleType.VarChar)

            sql = "select so.del_doc_num, so.so_store_cd, so.so_wr_dt, so.cust_cd, so.ord_tp_cd, so.ord_srt_cd, nvl(so_ln.qty,0) as QTY, nvl(so_ln.unit_prc,0) as UNIT_PRC, so_ln.so_emp_slsp_cd1, itm.cat_cd, itm.inventory, "
            sql = sql & "nvl(so_ln.pct_of_sale1,0) as PCT_OF_SALE1, so_ln.so_emp_slsp_cd2, nvl(so_ln.pct_of_sale2,0) as pct_of_sale2, inv_mjr.prod_grp_cd , itm.mnr_cd, itm.treatable, itm.itm_tp_cd, itm.ve_cd, so_ln.itm_cd, so_ln.treated_by_itm_cd, 'W' as line_type "
            sql = sql & "from so, so_ln, itm, inv_mnr, inv_mjr where itm.mnr_cd = inv_mnr.mnr_cd and inv_mnr.mjr_cd = inv_mjr.mjr_cd and "
            sql = sql & "so.del_doc_num = so_ln.del_doc_num and so_ln.itm_cd = itm.itm_cd and so.so_wr_dt between TO_DATE('" & sdate & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & edate & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') and so_ln.addon_wr_dt is null "
            sql = sql & " UNION "
            sql = sql & "select so.del_doc_num, so.so_store_cd, so.so_wr_dt, so.cust_cd, so.ord_tp_cd, so.ord_srt_cd, nvl(so_ln.qty,0) as QTY, nvl(so_ln.unit_prc,0) as UNIT_PRC, so_ln.so_emp_slsp_cd1, itm.cat_cd, itm.inventory, "
            sql = sql & "nvl(so_ln.pct_of_sale1,0) as PCT_OF_SALE1, so_ln.so_emp_slsp_cd2, nvl(so_ln.pct_of_sale2,0) as pct_of_sale2, inv_mjr.prod_grp_cd , itm.mnr_cd, itm.treatable, itm.itm_tp_cd, itm.ve_cd, so_ln.itm_cd, so_ln.treated_by_itm_cd, 'V' as line_type "
            sql = sql & "from so, so_ln, itm, inv_mnr, inv_mjr where itm.mnr_cd = inv_mnr.mnr_cd and inv_mnr.mjr_cd = inv_mjr.mjr_cd and "
            sql = sql & "so.del_doc_num = so_ln.del_doc_num and so_ln.itm_cd = itm.itm_cd and so.final_dt between TO_DATE('" & sdate & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & edate & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') and so.stat_cd = 'V' and so_ln.void_flag = 'N' "
            sql = sql & " UNION "
            sql = sql & "select so.del_doc_num, so.so_store_cd, so.so_wr_dt, so.cust_cd, so.ord_tp_cd, so.ord_srt_cd, nvl(so_ln.qty,0) as QTY, nvl(so_ln.unit_prc,0) as UNIT_PRC, so_ln.so_emp_slsp_cd1, itm.cat_cd, itm.inventory, "
            sql = sql & "nvl(so_ln.pct_of_sale1,0) as PCT_OF_SALE1, so_ln.so_emp_slsp_cd2, nvl(so_ln.pct_of_sale2,0) as pct_of_sale2, inv_mjr.prod_grp_cd, itm.mnr_cd, itm.treatable, itm.itm_tp_cd, itm.ve_cd, so_ln.itm_cd, so_ln.treated_by_itm_cd, 'L' as line_type "
            sql = sql & "from so, so_ln, itm, inv_mnr, inv_mjr where itm.mnr_cd = inv_mnr.mnr_cd and inv_mnr.mjr_cd = inv_mjr.mjr_cd and "
            sql = sql & "so.del_doc_num = so_ln.del_doc_num and so_ln.itm_cd = itm.itm_cd and so_ln.void_dt between TO_DATE('" & sdate & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & edate & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') and so_ln.void_dt is not null "
            sql = sql & "and so_ln.void_flag = 'Y' and so_ln.picked = 'V'"
            sql = sql & " UNION "
            sql = sql & "select so.del_doc_num, so.so_store_cd, so.so_wr_dt, so.cust_cd, so.ord_tp_cd, so.ord_srt_cd, nvl(so_ln.qty,0) as QTY, nvl(so_ln.unit_prc,0) as UNIT_PRC, so_ln.so_emp_slsp_cd1, itm.cat_cd, itm.inventory, "
            sql = sql & "nvl(so_ln.pct_of_sale1,0) as PCT_OF_SALE1, so_ln.so_emp_slsp_cd2, nvl(so_ln.pct_of_sale2,0) as pct_of_sale2, inv_mjr.prod_grp_cd, itm.mnr_cd, itm.treatable, itm.itm_tp_cd, itm.ve_cd, so_ln.itm_cd, so_ln.treated_by_itm_cd, 'A' as line_type "
            sql = sql & "from so, so_ln, itm, inv_mnr, inv_mjr where itm.mnr_cd = inv_mnr.mnr_cd and inv_mnr.mjr_cd = inv_mjr.mjr_cd and "
            sql = sql & "so.del_doc_num = so_ln.del_doc_num and so_ln.itm_cd = itm.itm_cd and so_ln.addon_wr_dt between TO_DATE('" & sdate & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & edate & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') and so_ln.addon_wr_dt is not null "

            Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn2)
            Dim MyDatareader2 As OracleDataReader

            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDatareader2.Read
                objSql.Parameters(":DEL_DOC_NUM").Value = MyDatareader2.Item("DEL_DOC_NUM").ToString
                objSql.Parameters(":STORE_CD").Value = MyDatareader2.Item("SO_STORE_CD").ToString
                objSql.Parameters(":WR_DT").Value = MyDatareader2.Item("SO_WR_DT").ToString
                objSql.Parameters(":CUST_CD").Value = MyDatareader2.Item("CUST_CD").ToString
                objSql.Parameters(":ORD_TP_CD").Value = MyDatareader2.Item("ORD_TP_CD").ToString
                objSql.Parameters(":ORD_SRT_CD").Value = MyDatareader2.Item("ORD_SRT_CD").ToString
                objSql.Parameters(":QTY").Value = CDbl(MyDatareader2.Item("QTY").ToString)
                objSql.Parameters(":UNIT_PRC").Value = CDbl(MyDatareader2.Item("UNIT_PRC").ToString)
                objSql.Parameters(":SLSP1").Value = MyDatareader2.Item("so_emp_slsp_cd1").ToString
                objSql.Parameters(":SLSP1_PCT").Value = CDbl(MyDatareader2.Item("pct_of_sale1").ToString)
                objSql.Parameters(":SLSP2").Value = MyDatareader2.Item("so_emp_slsp_cd2").ToString
                objSql.Parameters(":SLSP2_PCT").Value = CDbl(MyDatareader2.Item("pct_of_sale2").ToString)
                objSql.Parameters(":PROD_GRP_CD").Value = MyDatareader2.Item("PROD_GRP_CD").ToString
                objSql.Parameters(":MNR_CD").Value = MyDatareader2.Item("MNR_CD").ToString
                objSql.Parameters(":ITM_CD").Value = MyDatareader2.Item("ITM_CD").ToString
                objSql.Parameters(":TREATABLE").Value = MyDatareader2.Item("TREATABLE").ToString
                objSql.Parameters(":ITM_TP_CD").Value = MyDatareader2.Item("ITM_TP_CD").ToString
                objSql.Parameters(":LINE_TYPE").Value = MyDatareader2.Item("LINE_TYPE").ToString
                objSql.Parameters(":VE_CD").Value = MyDatareader2.Item("VE_CD").ToString
                objSql.Parameters(":TREATED_BY_ITM_CD").Value = MyDatareader2.Item("TREATED_BY_ITM_CD").ToString
                objSql.Parameters(":CAT_CD").Value = MyDatareader2.Item("CAT_CD").ToString
                objSql.Parameters(":INVENTORY").Value = MyDatareader2.Item("INVENTORY").ToString
                objSql.ExecuteNonQuery()
            Loop
            MyDatareader2.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        conn.Close()
        conn2.Close()

        lbl_status.Text = "Process completed"

    End Sub
End Class
