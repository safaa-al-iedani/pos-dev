



Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Linq
Imports System.Web.UI.WebControls
Imports System.Math
Imports HBCG_Utils
Imports InventoryUtils
Imports SalesUtils
Imports TaxUtils
Imports SD_Utils
Imports jda.mm.order.DataTransferClass


Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp

Imports System.Globalization


Partial Class soCmnt
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
    End Sub


    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init


        Dim del_doc_num As String
        Session("from_soCmnt") = "Y"
        del_doc_num = Request("del_doc_num")
        '  Text_prod_grp.Text = del_doc_num



        reload()

        ' btn_search.Enabled = True
        btn_clr_screen.Enabled = True

        btn_update.Visible = False
    End Sub


    Protected Sub exit_form(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim del_doc_num As String
        Dim cust_cd As String
        Dim previousPage As String = Page.Request.UrlReferrer.ToString

        Session("from_soCmnt") = "Y"
        del_doc_num = Request("del_doc_num")
        cust_cd = Session("sht_cust_cd")
        ' Response.Redirect(previousPage)

        Response.Redirect("SaleHst.aspx?del_doc_num=" & HttpUtility.UrlEncode(del_doc_num) & "&cust_cd=" & HttpUtility.UrlEncode(cust_cd))
    End Sub





    Protected Sub reload()

        Dim l_id As String


        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            ds = New DataSet
            GV_SRR.DataSource = ""

            Dim l_date_r As String
            ' l_date_r = DropDownDt.SelectedItem.Value


            Dim l_table As String
            '  l_table = DropDown_table.SelectedItem.Value

            Dim del_doc_num As String
            Session("from_soCmnt") = "Y"
            del_doc_num = Request("del_doc_num")


            sqlString = New StringBuilder("SELECT distinct  SO_CMNT.rowid ,    ")
            sqlString.Append(" TO_CHAR(SO_CMNT.DT, 'MM/DD/YYYY') AS DT, SO_CMNT.TEXT as CMNTS , SO_CMNT.CMNT_TYPE as TYPE ,seq#")
            sqlString.Append("  from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & del_doc_num & "'")
            sqlString.Append(" AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD  ")
            sqlString.Append(" AND SO_CMNT.SO_SEQ_NUM = SO.SO_SEQ_NUM ")
            sqlString.Append("union  SELECT  distinct  SO_CMNT.rowid ,    ")
            sqlString.Append(" TO_CHAR(SO_CMNT.DT, 'MM/DD/YYYY') AS DT, SO_CMNT.TEXT as CMNTS , null as TYPE,seq# ")
            sqlString.Append("  from SO_CMNT_arc so_cmnt, so_arc SO Where SO.DEL_DOC_NUM = '" & del_doc_num & "'")
            sqlString.Append(" AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD  ")
            sqlString.Append(" AND SO_CMNT.SO_SEQ_NUM = SO.SO_SEQ_NUM ")
            sqlString.Append("  ORDER BY SEQ# ASC ")
            


            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()



            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String


                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)
                btn_update.Visible = True



                    End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_update.Visible = False


                    End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub





     
End Class