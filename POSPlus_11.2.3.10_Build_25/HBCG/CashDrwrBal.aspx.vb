Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'sabrina new class
Partial Class CashDrwrBal
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_dt_fmt As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_msg.Text = ""

        g_dt_fmt = "mm/dd/RRRR"

        If IsPostBack Then

            If (Not GridView2.DataSource Is Nothing) Then
                GridView2.DataBind()
            End If

        End If
    End Sub
    Protected Function isCashDrawerFound(ByVal p_store_cd As String, ByVal p_post_dt As String) As String

        Dim v_found As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' FOUND_IND from CASHDRAWER "
        sql = sql & " where st_cd = '" & p_store_cd & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_found = dbReader.Item("FOUND_IND")
            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_found = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If


    End Function

    Protected Sub Populate_Results(ByVal p_store_cd As String, ByVal p_post_dt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim v_co_cd As String = Session("CO_CD")
        Dim v_emp_init As String = Session("emp_init")
        Dim v_found_ind As String = "N"

        Dim v_mop_sql As String = ""
        Dim v_cshdwr_sql As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
            v_mop_sql = " and m.mop_cd not in ('AX', 'DIS', 'PAD', 'VPY', 'WIR') " 'as per donna for brick exclude these mops
            v_cshdwr_sql = ""
        Else
            v_mop_sql = ""
            v_cshdwr_sql = " and b.csh_dwr_cd not in ('50','51') "   'as per donna for leons exclude 50,51

        End If

        sql = "SELECT MOP_CD,MOP_DES,to_char(CDR_AMT,'9999999999.99') CDR_AMT, "
        sql = sql & " TO_CHAR(ACT_AMT,'9999999999.99') ACT_AMT,TO_CHAR((NVL(ACT_AMT,0) - NVL(CDR_AMT,0)),'9999999999.99') DIFF_AMT FROM "
        sql = sql & "(select m.mop_cd MOP_CD, m.des||' ('||m.mop_cd||') ' MOP_DES, "
        sql = sql & " (select nvl(sum(decode(b.trn_tp_cd,'R',-amt,amt)),0) "
        sql = sql & "   from ar_trn b "
        sql = sql & "  where b.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & "    and b.pmt_store = '" & UCase(p_store_cd) & "'"
        sql = sql & "    and trunc(b.post_dt) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"
        sql = sql & "    and b.mop_cd = m.mop_cd "
        sql = sql & "    and b.trn_tp_cd IN ('R','DEP','PMT') and b.stat_cd != 'A'  "
        sql = sql & v_cshdwr_sql
        sql = sql & " ) CDR_AMT, "
        sql = sql & " (select to_char(nvl(std_cdsas_main.getActual('" & v_emp_init & "','" & UCase(p_store_cd) & "', to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "'),M.mop_cd),0),'9999999999.99') FROM DUAL) ACT_AMT "
        sql = sql & " from mop m "
        sql = sql & " where nvl(m.deposit_flag,'x') = 'Y' "
        sql = sql & "   and std_multi_co.isvalidmop('" & Session("emp_init") & "', m.mop_cd) = 'Y' "
        sql = sql & v_mop_sql
        sql = sql & " ) "
        sql = sql & " union "

        'sql = sql & "SELECT MOP_CD,MOP_DES, to_char(sum(CDR_AMT),'9999999999.99') CDR_AMT, "
        'sql = sql & " TO_CHAR(sum(ACT_AMT),'9999999999.99') ACT_AMT ,TO_CHAR((sum(NVL(ACT_AMT,0)) - sum(NVL(CDR_AMT,0))),'9999999999.99') DIFF_AMT FROM "
        'sql = sql & "(select '' MOP_CD, '***** SubTotal *****' MOP_DES, "
        'sql = sql & " (select to_char(nvl(sum(decode(b.trn_tp_cd,'R',-amt,amt)),0),'9999999999.99') "
        'sql = sql & "    from ar_trn b "
        'sql = sql & "   where b.co_cd = '" & Session("CO_CD") & "'"
        'sql = sql & "     and b.pmt_store = '" & UCase(p_store_cd) & "'"
        'sql = sql & "     and trunc(b.post_dt) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"
        'sql = sql & "     and b.mop_cd = m.mop_cd "
        'sql = sql & "    and b.trn_tp_cd IN ('R','DEP','PMT') and b.stat_cd = 'T' "
        'sql = sql & v_cshdwr_sql
        'sql = sql & " ) CDR_AMT, "
        'sql = sql & " (select to_char(nvl(std_cdsas_main.getActual('" & v_emp_init & "','" & UCase(p_store_cd) & "', to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "'),M.mop_cd),0),'9999999999.99') FROM DUAL) ACT_AMT "
        'sql = sql & " from mop m "
        'sql = sql & " where nvl(m.deposit_flag,'x') = 'Y' "
        'sql = sql & "   and std_multi_co.isvalidmop('" & Session("emp_init") & "', m.mop_cd) = 'Y' "
        'sql = sql & v_mop_sql
        'sql = sql & " ) "
        'sql = sql & " group by MOP_CD,MOP_DES "

        'Alice update on Sep 21, 2018---for request #4089
        sql = sql & "SELECT MOP_CD,MOP_DES, TO_CHAR((sum(NVL(CDR_AMT,0)) - NVL(REFUND_CHQ_AMT,0)),'9999999999.99') CDR_AMT, "
        sql = sql & " TO_CHAR((sum(NVL(ACT_AMT,0)) - NVL(REFUND_CHQ_AMT,0)),'9999999999.99') ACT_AMT ,TO_CHAR((sum(NVL(ACT_AMT,0)) - sum(NVL(CDR_AMT,0))),'9999999999.99') DIFF_AMT FROM "
        sql = sql & "(select '' MOP_CD, '***** SubTotal *****' MOP_DES, "
        sql = sql & " (select to_char(nvl(sum(decode(b.trn_tp_cd,'R',-amt,amt)),0),'9999999999.99') "
        sql = sql & "    from ar_trn b "
        sql = sql & "   where b.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & "     and b.pmt_store = '" & UCase(p_store_cd) & "'"
        sql = sql & "     and trunc(b.post_dt) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"
        sql = sql & "     and b.mop_cd = m.mop_cd "
        sql = sql & "    and b.trn_tp_cd IN ('R','DEP','PMT') and b.stat_cd != 'A' "
        sql = sql & v_cshdwr_sql
        sql = sql & " ) CDR_AMT, "

        sql = sql & "( select to_char(sum(nvl(x.AMT,0)),'9999999999.99') "
        sql = sql & " From AR_TRN x "
        sql = sql & "  Where x.MOP_CD = 'CK' And x.CSH_DWR_CD = 50 "
        sql = sql & "And x.co_cd = '" & Session("CO_CD") & "' and x.pmt_store = '" & UCase(p_store_cd) & "' and trunc(x.post_dt) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "') ) REFUND_CHQ_AMT,  "

        sql = sql & "(select to_char(nvl(std_cdsas_main.getActual('" & v_emp_init & "','" & UCase(p_store_cd) & "', to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "'),M.mop_cd),0),'9999999999.99') FROM DUAL) ACT_AMT "
        sql = sql & " from mop m "
        sql = sql & " where nvl(m.deposit_flag,'x') = 'Y' "
        sql = sql & "   and std_multi_co.isvalidmop('" & Session("emp_init") & "', m.mop_cd) = 'Y' "
        sql = sql & v_mop_sql
        sql = sql & " ) "
        sql = sql & " group by MOP_CD,MOP_DES "


        'group by MOP_CD,MOP_DES " -- IT REQ 4066 July 25, 2018
        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            sql = sql & " UNION "
            sql = sql & " SELECT MOP_CD, MOP_DES, CDR_AMT, nvl(ACT_AMT, '.00') ACT_AMT, nvl(DIFF_AMT, '.00') DIFF_AMT FROM "
            sql = sql & " ( select '' MOP_CD, 'GIFT CARD ACTIVATION' MOP_DES, '' CDR_AMT, "
            sql = sql & " to_char(sum(nvl(GC_ACTIVATION,0)),'9999999999.99') ACT_AMT, to_char(sum(nvl(GC_ACTIVATION,0)),'9999999999.99') DIFF_AMT "
            sql = sql & " FROM CASHDRAWER  where st_cd = '" & UCase(p_store_cd) & "' and trunc(post_day) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "') "
            sql = sql & " )"
            sql = sql & " UNION "
            sql = sql & " SELECT MOP_CD, MOP_DES, nvl(CDR_AMT,'.00') CDR_AMT, nvl(ACT_AMT,'.00') ACT_AMT, DIFF_AMT "
            sql = sql & " FROM ( "
            sql = sql & " select '' MOP_CD, 'H/O REFUND CHEQUES' MOP_DES, to_char(sum(nvl(AMT,0)),'9999999999.99') CDR_AMT, "
            sql = sql & " to_char(sum(AMT),'9999999999.99') ACT_AMT, '' DIFF_AMT  "
            sql = sql & " FROM AR_TRN"
            sql = sql & " WHERE MOP_CD = 'CK' and CSH_DWR_CD = 50 "
            sql = sql & " and co_cd = '" & Session("CO_CD") & "' and pmt_store = '" & UCase(p_store_cd) & "' and trunc(post_dt) = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')   "
            sql = sql & " ) "
        End If
        ' no need to sort after group by
        'sql = sql & " order by MOP_CD,MOP_DES "



        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                Try
                    GridView2.DataSource = ds
                    GridView2.DataBind()
                Catch ex As Exception
                    conn.Close()
                End Try
                MyDataReader.Close()
                txt_store_cd.Text = UCase(p_store_cd)

                txt_store_cd.Enabled = False
                post_dt.Enabled = False
            Else
                txt_store_cd.Enabled = True
                post_dt.Enabled = True
            End If
            conn.Close()

        Catch ex As Exception

            conn.Close()
            Throw

        End Try

        appr_by.Text = ApprovedUsr()
        If getLastUpdDt() & "" <> "" Then
            last_upd_dt.Text = FormatDateTime(getLastUpdDt, DateFormat.ShortDate)
            last_upd_by.Text = UCase(CrUsr)
        End If

        appr_by.Enabled = False
        last_upd_dt.Enabled = False
        last_upd_by.Enabled = False



    End Sub
    Public Function getLastUpdDt() As String

        Dim v_appr_dt As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT cr_app_dt from CASHDRAWER "
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_appr_dt = dbReader.Item("cr_app_dt")
            Else
                v_appr_dt = ""
            End If

        Catch
            v_appr_dt = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_appr_dt

    End Function
    Public Function isValidStore() As String

        Dim v_valid_ind As String = ""

        If txt_store_cd.Text & "" = "" Then
            lbl_msg.Text = Resources.LibResources.Label891
            Return "N"
        ElseIf LeonsBiz.isValidStr2(Session("emp_init"), UCase(txt_store_cd.Text)) <> "Y" Then
            lbl_msg.Text = Resources.LibResources.Label892
            Return "N"
        Else
            txt_store_cd.Text.ToUpper()
            Return "Y"
        End If

    End Function

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        If isValidStore() = "Y" Then

            If post_dt.SelectedDate.Date = DateTime.MinValue Then
                'lbl_msg.Text = "Please select a Date to proceed"
                lbl_msg.Text = Resources.LibResources.Label893
                Exit Sub
            ElseIf FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) > DateTime.Today.AddDays(-1) Then
                'lbl_msg.Text = "Invalid Date - please try again"
                lbl_msg.Text = Resources.LibResources.Label894
                Exit Sub
            End If

            post_dt.Enabled = True
            txt_store_cd.Enabled = True
            Populate_Results(UCase(HttpUtility.UrlEncode(txt_store_cd.Text)), FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate))

        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView2_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)


        Dim v_new_row As String = ""
        Dim v_cnt As OracleNumber = 0
        Dim UpdSql As String = ""
        Dim v_upd_sw As String = ""

        v_upd_sw = "Y"
        If ApprovedUsr() & "" <> "" Then
            e.Handled = True
            e.UpdateValues.Clear()
            'e.cancel = True
            lbl_msg.Text = Resources.LibResources.Label895
        Else

            If e.UpdateValues.Count = 0 Then
                If isCashDrawerFound(txt_store_cd.Text, post_dt.SelectedDate.ToString()) = "Y" Then
                    If isOKToApprove() = "Y" And isAuthToApprove() = "Y" Then
                        UpdateApprUsr()
                    Else
                        'lbl_msg.Text = "You are not Authorized to Approve"
                        lbl_msg.Text = Resources.LibResources.Label889
                        e.Handled = True
                        e.UpdateValues.Clear()
                    End If
                End If
            Else

                For Each args In e.UpdateValues
                    v_cnt = v_cnt + 1
                    If v_cnt = 1 Then
                        If isCashDrawerFound(txt_store_cd.Text, post_dt.SelectedDate.ToString()) = "Y" Then
                            updateCrUsr(txt_store_cd.Text, post_dt.SelectedDate.ToString())
                            v_new_row = "N"
                        Else
                            v_new_row = "Y"
                            insertCashDrawer(txt_store_cd.Text, post_dt.SelectedDate.ToString())
                        End If
                    End If

                    Dim strmopdec As String
                    strmopdec = args.NewValues.Item("MOP_DES").ToString

                    If InStr(args.NewValues.Item("MOP_DES").ToString, "SubTotal") = 0 Then
                         

                        UpdSql = UpdSql & BuildUpdSql(v_new_row, args.Keys, args.NewValues, args.OldValues)
                    Else
                        If e.UpdateValues.Count = 1 Then  'if only subtotal is changed no upd required
                            v_upd_sw = "N"
                        End If
                    End If

                Next args

                If v_upd_sw <> "N" Then
                    UpdateCashDrawer(UpdSql)
                    'lbl_msg.Text = "Update is done"
                    lbl_msg.Text = Resources.LibResources.Label890
                End If

                e.Handled = True
                e.UpdateValues.Clear()


            End If
        End If

        Populate_Results(txt_store_cd.Text, post_dt.SelectedDate.ToString())

        txt_store_cd.Enabled = False
        post_dt.Enabled = False

    End Sub
    Protected Sub insertCashDrawer(ByVal p_store_cd As String, ByVal p_post_dt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into CASHDRAWER (st_cd,post_day) "
        sql = sql & " values ( '" & p_store_cd & "',"
        sql = sql & " to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','" & g_dt_fmt & "'))"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()


    End Sub
    Public Function ApprovedUsr() As String

        Dim v_appr_cd As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT apprv_usr from CASHDRAWER "
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        v_appr_cd = ""

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_appr_cd = dbReader.Item("apprv_usr")
            Else
                v_appr_cd = ""
            End If

        Catch
            v_appr_cd = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_appr_cd


    End Function
    Public Function CrUsr() As String

        Dim v_cr_usr_cd As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT cr_usr from CASHDRAWER "
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        v_cr_usr_cd = ""

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_cr_usr_cd = dbReader.Item("cr_usr")
            Else
                v_cr_usr_cd = ""
            End If

        Catch
            v_cr_usr_cd = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_cr_usr_cd


    End Function
    Public Function isOKToApprove() As String

        Dim v_appr_cd As String = String.Empty
        Dim v_cr_usr_cd As String = CrUsr()

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT apprv_usr from CASHDRAWER "
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_appr_cd = dbReader.Item("apprv_usr")
            Else
                v_appr_cd = ""
            End If

        Catch
            v_appr_cd = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_appr_cd & "" = "" Then
            If v_cr_usr_cd & "" = "" Then
                Return "Y"
            ElseIf v_cr_usr_cd = Session("emp_init") Then
                Return "N"
            Else
                Return "Y"
            End If
        Else
            Return "N"
        End If


    End Function
    Public Function isAuthToApprove() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','CashDrwrBal.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Protected Sub UpdateApprUsr()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim cdrcol As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update CASHDRAWER "
        sql = sql & "set apprv_usr = '" & Session("emp_init") & "'"
        sql = sql & " ,cr_app_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'" & g_dt_fmt & "')"
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

    End Sub
    Public Function getColumn(oldvalues As OrderedDictionary) As String

        Dim updcol As String = ""

        If InStr(oldvalues("MOP_DES").ToString, "(CS)") > 0 Then
            updcol = "cs_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(CK)") > 0 Then
            updcol = "ck_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(FQ)") > 0 Then
            updcol = "fq_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(MO)") > 0 Then
            updcol = "mo_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(CC)") > 0 Then
            updcol = "cq_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(BD)") > 0 Then
            updcol = "bd_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(TC)") > 0 Then
            updcol = "tc_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(VI)") > 0 Then
            updcol = "vs_"
            'ElseIf InStr(oldvalues("MOP_DES").ToString, "(FV)") > 0 Then
            '    updcol = "fv_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(VPY)") > 0 Then
            updcol = "vpy_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(DC)") > 0 Then
            updcol = "dc_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(MC)") > 0 Then
            updcol = "mc_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(AMX)") > 0 Then
            updcol = "ax_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(FP)") > 0 Then
            updcol = "fp_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(FC)") > 0 Then
            updcol = "fc_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(FLX)") > 0 Then
            updcol = "flx_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(ZAP)") > 0 Then
            updcol = "zap_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(ZUP)") > 0 Then
            updcol = "zup_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(ZWC)") > 0 Then
            updcol = "zwc_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(FM)") > 0 Then
            updcol = "fm_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(DCS)") > 0 Then
            updcol = "dcs_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(TDF)") > 0 Then
            updcol = "tdf_"
        ElseIf InStr(oldvalues("MOP_DES").ToString, "(GC)") > 0 Then
            updcol = "gc_"
            ' GIFT CARD ACTIVATION July 25, 2018 IT Req 4066
        ElseIf InStr(oldvalues("MOP_DES").ToString, "GIFT CARD ACTIVATION") > 0 Then
            updcol = "GC_ACTIVATION"
        Else
            updcol = "sv_"
        End If

        Return updcol

    End Function
    Protected Function BuildUpdSql(ByVal p_new_row As String, ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary, ByVal oldValues As OrderedDictionary) As String

        Dim sql As String = ""
        Dim cdrcol As String = ""
        Dim v_comma As String = ""
        Dim v_new_value As String = ""

        ' IT Req 4066, July 25, 2018 - do not update GC_ACTIVATION CDR
        If p_new_row = "Y" Then
            If (getColumn(oldValues) <> "GC_ACTIVATION") Then
                If newValues.Item("CDR_AMT") & "" = "" Then
                    cdrcol = " , " & getColumn(oldValues) & "cdr = 0 "
                Else
                    cdrcol = " , " & getColumn(oldValues) & "cdr = " & newValues.Item("CDR_AMT").ToString
                End If
            End If
        Else
            cdrcol = ""
        End If

        If newValues.Item("ACT_AMT") & "" = "" Then
            sql = " , " & getColumn(oldValues) & "act = 0 "
        Else
            ' IT Req 4066, July 25, 2018
            If (getColumn(oldValues) = "GC_ACTIVATION") Then
                sql = " , " & getColumn(oldValues) & " = " & newValues.Item("ACT_AMT").ToString
            Else
                sql = " , " & getColumn(oldValues) & "act = " & newValues.Item("ACT_AMT").ToString
            End If
        End If

        sql = sql & cdrcol

        Return sql

    End Function
    Protected Function UpdateCashDrawer(ByVal p_sql As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()


        sql = " update CASHDRAWER set cr_usr = '" & Session("emp_init") & "'"
        sql = sql & " ,cr_app_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'" & g_dt_fmt & "')"
        sql = sql & p_sql
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        Return True

    End Function
    Protected Sub updateCrUsr(ByVal p_store_cd As String, ByVal p_post_dt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update CASHDRAWER "
        sql = sql & "set cr_usr = '" & Session("emp_init") & "'"
        sql = sql & " ,cr_app_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'" & g_dt_fmt & "')"
        sql = sql & " where st_cd = '" & UCase(txt_store_cd.Text) & "'"
        sql = sql & " and trunc(post_day) = to_date('" & FormatDateTime(post_dt.SelectedDate, DateFormat.ShortDate) & "','" & g_dt_fmt & "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click


        GridView2.DataSource = ""
        GridView2.DataBind()

        txt_store_cd.Text = ""
        post_dt.SelectedDate = DateTime.MinValue
        appr_by.Text = ""
        last_upd_dt.Text = ""
        last_upd_by.Text = ""
        lbl_msg.Text = ""

        txt_store_cd.Enabled = True
        post_dt.Enabled = True

    End Sub


End Class
