<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LoadLead.aspx.vb" Inherits="LoadLead" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label ID="txt_title" runat="server" Style="position: relative" CssClass="style5">Load Vendor Lead File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload1" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Upload Vendor Lead File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label1" runat="server" ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg1" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:Label ID="txt_itm_lead" runat="server" Style="position: relative" CssClass="style5">Load Item File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload2" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button2" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" 
         Text="Upload Item Lead File " />&nbsp;<br />
        <br />
        <asp:Label ID="Label2" runat="server"  ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg2" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:Label ID="txt_fcm_lead" runat="server" Style="position: relative" CssClass="style5">Load furniture.ca  File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload3" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button3" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" 
         Text="Upload furniture.ca File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label3" runat="server"  ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg3" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="ve_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn HeaderStyle-Width ="60px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center" DataField="ve_cd" headertext="Vendor Code" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                    <asp:BoundColumn HeaderStyle-Width ="60px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center" DataField="mnr_cd" headertext="Minor Code" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                    <asp:BoundColumn HeaderStyle-Width ="75px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="store_grp_cd" headertext="Store Group Code" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn HeaderStyle-Width ="75px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="days_lead" headertext="Days Lead" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn HeaderStyle-Width ="35px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="days_transit" headertext="Days Transit" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
              <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
            </asp:BoundColumn>
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

       <asp:DataGrid ID="Gridview2" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="itm_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="itm_cd" headertext="Sku" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
            <asp:BoundColumn DataField="store_grp_cd" headertext="Store Grp Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="days_lead" headertext="Days Lead " ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="days_transit" headertext="Days Transit " ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="ear_avl_dt" headertext="Earliest Arrival Date" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
            <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
                  </asp:BoundColumn>                      
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

       <asp:DataGrid ID="Gridview3" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="ve_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="ve_cd" headertext="Vendor Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="days_lead" headertext="Days Lead" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
            <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
                     </asp:BoundColumn>        
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

    <br />

     <asp:Button ID="btn_process" runat="server" Text="Process Vendor Lead" Width="200px" Visible="false">
     </asp:Button>
    <asp:Button ID="btn_process_itm_lead" runat="server" Text="Process Item Lead" Width="200px" Visible="false">
     </asp:Button>
    <asp:Button ID="btn_process_FCM_Lead" runat="server" Text="Process FCM Lead" Width="200px" Visible="false">
     </asp:Button>
    <br />
    <br />
    
    <asp:Button ID="btn_clear" runat="server" Text="Clear" Width="100px" Visible="true">
     </asp:Button>
</asp:Content>
