Imports System.Data.OracleClient
Imports System.Security.Cryptography
Imports System.Xml
Imports AppUtils
Imports HBCG_Utils

Partial Class admin
    Inherits POSBasePage


    Protected Sub Page_PreInit(ByVal sender As Object,
                               ByVal e As System.EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            admin_form.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            admin2.InnerHtml = ""
            Exit Sub
        End If

        If Not IsPostBack Then

            'show the warning message up front in red about sys parameter changes
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
            ucMsgPopup.DisplayWarningMsg(Resources.POSMessages.MSG0015)
            Dim panel As DevExpress.Web.ASPxPanel.ASPxPanel = CType(msgPopup.FindControl("Panel1"), DevExpress.Web.ASPxPanel.ASPxPanel)
            Dim msgCompont As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtMsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgCompont.ForeColor = Color.Red
            msgPopup.ShowOnPageLoad = True

            PopulateSortCodes()

            PopulateMgrOverride()

            link1_desc.Text = ConfigurationManager.AppSettings("link1_desc").ToString
            link2_desc.Text = ConfigurationManager.AppSettings("link2_desc").ToString
            txt_hyperlink1.Text = ConfigurationManager.AppSettings("Hyperlink1").ToString
            txt_hyperlink2.Text = ConfigurationManager.AppSettings("Hyperlink2").ToString
            txt_App_Title.Text = ConfigurationManager.AppSettings("app_title").ToString
            txt_logo_image.Text = ConfigurationManager.AppSettings("company_logo").ToString
            txt_co_cd.Text = ConfigurationManager.AppSettings("co_cd").ToString
            txt_cash_carry.Text = ConfigurationManager.AppSettings("cash_carry").ToString
            txt_drop_cd.Text = ConfigurationManager.AppSettings("default_drop_cd").ToString
            txt_COD_MIN.Text = ConfigurationManager.AppSettings("min_deposit").ToString
            txt_swatch_sku.Text = ConfigurationManager.AppSettings("swatch_sku").ToString
            txt_onbase_dir.Text = ConfigurationManager.AppSettings("onbase_dir").ToString
            txt_gift_sku.Text = ConfigurationManager.AppSettings("gift_card_sku").ToString
            txt_margin.Text = ConfigurationManager.AppSettings("margin").ToString
            txt_sku_generation.Text = ConfigurationManager.AppSettings("sku_generation").ToString

            cbo_primary_phone.SelectedValue = ConfigurationManager.AppSettings("primary_phone").ToString
            cbo_secondary_phone.SelectedValue = ConfigurationManager.AppSettings("secondary_phone").ToString
            cbo_fab_populate.SelectedValue = ConfigurationManager.AppSettings("fab_populate").ToString
            cbo_package.SelectedValue = ConfigurationManager.AppSettings("package_breakout").ToString
            cbo_mass.SelectedValue = ConfigurationManager.AppSettings("mass").ToString
            cbo_store_based.SelectedValue = ConfigurationManager.AppSettings("store_based").ToString
            cbo_Delivery_Options.SelectedValue = ConfigurationManager.AppSettings("delivery_options").ToString

            cbo_Special_Orders.SelectedValue = ConfigurationManager.AppSettings("special_orders").ToString

            Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
            ddlMgrOverride.SelectedValue = prcOverride 
            cbo_db_type.SelectedValue = ConfigurationManager.AppSettings("db_type").ToString
            cbo_Allow_COD.SelectedValue = ConfigurationManager.AppSettings("allow_cod").ToString
            cbo_special_drop_required.SelectedValue = ConfigurationManager.AppSettings("require_drop_special").ToString
            cbo_show_marketing.SelectedValue = ConfigurationManager.AppSettings("show_marketing").ToString
            cbo_Special_Form.SelectedValue = ConfigurationManager.AppSettings("use_special_forms").ToString
            cbo_inv_avail.SelectedValue = ConfigurationManager.AppSettings("inv_avail").ToString
            cbo_dep_req.SelectedValue = ConfigurationManager.AppSettings("dep_req").ToString
            cbo_Generic.SelectedValue = ConfigurationManager.AppSettings("use_generic").ToString
            cbo_zip.SelectedValue = ConfigurationManager.AppSettings("validate_zip").ToString
            cbo_avail_details.SelectedValue = ConfigurationManager.AppSettings("avail_details").ToString
            cbo_PFM.SelectedValue = ConfigurationManager.AppSettings("pfm").ToString
            cbo_cc.SelectedValue = ConfigurationManager.AppSettings("cc").ToString
            cbo_finance.SelectedValue = ConfigurationManager.AppSettings("finance").ToString
            cbo_chk_guar.SelectedValue = ConfigurationManager.AppSettings("chk_guar").ToString
            cbo_sys_type.SelectedValue = ConfigurationManager.AppSettings("sys_type").ToString
            cbo_sort_cd.SelectedValue = ConfigurationManager.AppSettings("ord_srt_cd").ToString
            cbo_promotions.SelectedValue = ConfigurationManager.AppSettings("promotions").ToString
            cbo_rel_access.SelectedValue = ConfigurationManager.AppSettings("rel_access").ToString
            cbo_special_sku_limit.SelectedValue = ConfigurationManager.AppSettings("special_order_limits").ToString
            cbo_email.SelectedValue = ConfigurationManager.AppSettings("email_prompt").ToString
            cbo_allow_cash_carry.SelectedValue = ConfigurationManager.AppSettings("allow_cash_carry").ToString
            cbo_xref.SelectedValue = ConfigurationManager.AppSettings("xref").ToString
            cbo_term_lock_down.SelectedValue = ConfigurationManager.AppSettings("term_lock_down").ToString
            cbo_system_mode.SelectedValue = ConfigurationManager.AppSettings("system_mode").ToString
            cbo_lock_converted.SelectedValue = ConfigurationManager.AppSettings("lock_converted").ToString
            cbo_open_zones.SelectedValue = ConfigurationManager.AppSettings("open_zones").ToString
            cbo_dl_verify.SelectedValue = ConfigurationManager.AppSettings("dl_verify").ToString
            cbo_p_or_d.SelectedValue = ConfigurationManager.AppSettings("p_or_d").ToString
            cbo_sig_cap.SelectedValue = ConfigurationManager.AppSettings("sig_cap").ToString
            cbo_cust_tp.SelectedValue = ConfigurationManager.AppSettings("cust_tp").ToString
            cbo_picr.SelectedValue = ConfigurationManager.AppSettings("picr").ToString
            'txt_slsp2.Text = ConfigurationManager.AppSettings("slsp2").ToString
            txt_slsp2.Text = IIf(String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings("slsp2").ToString), 0, ConfigurationManager.AppSettings("slsp2").ToString)
            cbo_cust_creation.SelectedValue = ConfigurationManager.AppSettings("cust_cd_creation").ToString
            cbo_app_theme.SelectedItem = cbo_app_theme.Items.FindByValue(ConfigurationManager.AppSettings("app_theme").ToString)
            cbo_show_cust_relationships.SelectedValue = ConfigurationManager.AppSettings("show_cust_relationships").ToString
            cbo_tax_line.SelectedValue = ConfigurationManager.AppSettings("tax_line").ToString
            cbo_Sort_Code.SelectedValue = ConfigurationManager.AppSettings("default_sort_code").ToString
            cbo_onbase.SelectedValue = ConfigurationManager.AppSettings("onbase").ToString
            cbo_qs.SelectedValue = ConfigurationManager.AppSettings("quick_screen").ToString
            cbo_w_gift_card.SelectedValue = ConfigurationManager.AppSettings("gift_card").ToString
            cbo_inv_avail_find.SelectedValue = ConfigurationManager.AppSettings("inv_avail_find_show").ToString
            cbo_show_po.SelectedValue = ConfigurationManager.AppSettings("show_po").ToString
            cbo_serialization.SelectedValue = ConfigurationManager.AppSettings("enable_serialization").ToString
            cbo_link_po.SelectedValue = ConfigurationManager.AppSettings("link_po").ToString
            cbo_email_freeform.SelectedValue = ConfigurationManager.AppSettings("email_freeform").ToString

            chk_pos.Checked = (ConfigurationManager.AppSettings("pos").ToString = "Y")
            chk_CRM.Checked = (ConfigurationManager.AppSettings("crm").ToString = "Y")
            chk_payment.Checked = (ConfigurationManager.AppSettings("payment").ToString = "Y")
            chk_kiosk.Checked = (ConfigurationManager.AppSettings("kiosk").ToString = "Y")

            Dim PTAG_Access As String = Decrypt(Get_Connection_String("/HandleCode/HBCG/PCF_Config", "Ident_String"),
                                                "CrOcOdIlE")
            If PTAG_Access = "HIGHLANDS" Then
                chk_ptag.Checked = True
            End If

            Dim User_Count As String = Decrypt(Get_Connection_String("/HandleCode/HBCG/DLL_Config", "Ident_String"),
                                               "CrOcOdIlE")
            txt_user_count.Text = User_Count
            txt_current_users.Text = Lookup_User_Count()

            txt_COD_MIN.Enabled = (cbo_Allow_COD.SelectedValue = "Y")
        End If

    End Sub

    'Executes when the 'Save Settings' button is clicked
    Protected Sub btn_System_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_System.Click

        Try
            Dim changesMade As Boolean = False
            Dim doc As XmlDocument = New XmlDocument()
            Dim appPath As String = Request.ServerVariables("APPL_PHYSICAL_PATH")
            Dim settingsFile As String = appPath + "\appsettings.config"
            doc.Load(settingsFile)
            Dim appSettings As XmlNodeList = doc.FirstChild.ChildNodes

            UpdateAppSetting(changesMade, appSettings, "email_freeform", cbo_email_freeform.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "inv_avail_find_show", cbo_inv_avail_find.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "show_po", cbo_show_po.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "enable_serialization", cbo_serialization.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "link_po", cbo_link_po.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "onbase", cbo_onbase.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "quick_screen", cbo_qs.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "gift_card", cbo_w_gift_card.SelectedValue.ToString)
            UpdateAppSetting(changesMade, appSettings, "onbase_dir", txt_onbase_dir.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "gift_card_sku", txt_gift_sku.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "app_title", txt_App_Title.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "swatch_sku", txt_swatch_sku.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "company_logo", txt_logo_image.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "cash_carry", txt_cash_carry.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "link1_desc", link1_desc.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "link2_desc", link2_desc.Text.ToString)
            UpdateAppSetting(changesMade, appSettings, "inv_avail", cbo_inv_avail.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "avail_details", cbo_avail_details.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "store_based", cbo_store_based.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "package_breakout", cbo_package.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "fab_populate", cbo_fab_populate.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "delivery_options", cbo_Delivery_Options.SelectedValue.ToString.Trim)

            UpdateAppSetting(changesMade, appSettings, "special_orders", cbo_Special_Orders.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "supervisor_overrides", ddlMgrOverride.SelectedValue.ToString.Trim)
            'TODO:AXK to be removed to not have 'SQL' as a d.d. option for the db. Hard-coding to 'Oracle' for now
            UpdateAppSetting(changesMade, appSettings, "db_type", "ORACLE")

            UpdateAppSetting(changesMade, appSettings, "allow_cod", cbo_Allow_COD.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "require_drop_special", cbo_special_drop_required.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "default_drop_cd", txt_drop_cd.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "min_deposit", txt_COD_MIN.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "show_marketing", cbo_show_marketing.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "use_special_forms", cbo_Special_Form.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "default_sort_code", cbo_Sort_Code.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "use_generic", cbo_Generic.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "validate_zip", cbo_zip.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "dep_req", cbo_dep_req.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "mass", cbo_mass.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "pfm", cbo_PFM.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "cc", cbo_cc.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "finance", cbo_finance.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "chk_guar", cbo_chk_guar.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "sys_type", cbo_sys_type.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "ord_srt_cd", cbo_sort_cd.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "promotions", cbo_promotions.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "rel_access", cbo_rel_access.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "primary_phone", cbo_primary_phone.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "secondary_phone", cbo_secondary_phone.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "Hyperlink1", txt_hyperlink1.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "Hyperlink2", txt_hyperlink2.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "special_order_limits", cbo_special_sku_limit.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "email_prompt", cbo_email.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "allow_cash_carry", cbo_allow_cash_carry.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "xref", cbo_xref.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "term_lock_down", cbo_term_lock_down.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "system_mode", cbo_system_mode.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "lock_converted", cbo_lock_converted.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "open_zones", cbo_open_zones.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "dl_verify", cbo_dl_verify.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "p_or_d", cbo_p_or_d.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "cust_tp", cbo_cust_tp.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "sig_cap", cbo_sig_cap.SelectedValue.ToString.Trim)
            If (String.IsNullOrWhiteSpace(txt_slsp2.Text)) Then
                UpdateAppSetting(changesMade, appSettings, "slsp2", 0)
            Else
                UpdateAppSetting(changesMade, appSettings, "slsp2", txt_slsp2.Text.ToString.Trim)
            End If
            'UpdateAppSetting(changesMade, appSettings, "slsp2", txt_slsp2.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "picr", cbo_picr.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "cust_cd_creation", cbo_cust_creation.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "app_theme", cbo_app_theme.Value.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "show_cust_relationships", cbo_show_cust_relationships.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "tax_line", cbo_tax_line.SelectedValue.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "margin", txt_margin.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "co_cd", txt_co_cd.Text.ToString.Trim)
            UpdateAppSetting(changesMade, appSettings, "sku_generation", txt_sku_generation.Text.ToString.Trim)

            ' Save the configuration file, only if changes were encountered
            If (changesMade) Then
                doc.Save(settingsFile)

                ' Force a reload of a changed section.
                ConfigurationManager.RefreshSection("appSettings")
                Response.Redirect("logout.aspx")
            Else
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
                ucMsgPopup.DisplayWarningMsg(Resources.POSMessages.MSG0014)
                msgPopup.ShowOnPageLoad = True

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        UpdateUserCount(txt_new_user_count.Text)
        txt_user_count.Text = txt_new_user_count.Text
        ASPxPopupControl7.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_update_users_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl7.ShowOnPageLoad = True

    End Sub

    Protected Sub btn_activate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim xmlDocCounter As New XmlDocument
    xmlDocCounter.Load(Server.MapPath("appTConnConfig.xml"))

        Dim xmlCountnode As XmlNode
        Dim root As XmlNode = xmlDocCounter.DocumentElement

        xmlCountnode = root.SelectSingleNode("//HandleCode/HBCG/PCF_Config/Ident_String")

        xmlCountnode.InnerText = Encrypt("HIGHLANDS", "CrOcOdIlE")
    xmlDocCounter.Save(Server.MapPath("appTConnConfig.xml"))
        ASPxPopupControl2.ShowOnPageLoad = False
        chk_ptag.Checked = True

    End Sub

    Protected Sub btn_activate_ptag_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl2.ShowOnPageLoad = True

    End Sub

    Protected Sub txt_swatch_sku_TextChanged(ByVal sender As Object,
                                             ByVal e As System.EventArgs) Handles txt_swatch_sku.TextChanged

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim oraCmd As OracleCommand
        Dim oraRdr As OracleDataReader
        Dim sql As String = "SELECT ITM_CD FROM ITM WHERE ITM_CD= :sku_Swatch"

        oraCmd = DisposablesManager.BuildOracleCommand(sql, connErp)
        oraCmd.Parameters.Add(":sku_Swatch", OracleType.VarChar)
        oraCmd.Parameters(":sku_Swatch").Value = txt_swatch_sku.Text

        Try
            connErp.Open()
            oraRdr = DisposablesManager.BuildOracleDataReader(oraCmd)


            'check if sku entered exists and is valid
            If Not oraRdr.Read() Then
                txt_swatch_sku.Text = ""
                txt_swatch_sku.Focus()
                lbl_swatch_sku.Text = "<font color=red>The SKU you entered was invalid</font>"
            Else
                lbl_swatch_sku.Text = ""
            End If

            oraRdr.Close()
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub

    Protected Sub txt_slsp2_TextChanged(ByVal sender As Object,
                                        ByVal e As System.EventArgs) Handles txt_slsp2.TextChanged
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
            lbl_slsp_err.Text = "<font color=red>Commission amount must be numeric</font>"
            Exit Sub
        End If

        If CDbl(sender.text) < 0 Or CDbl(sender.text) > 100 Then
            sender.text = "0.00"
            lbl_slsp_err.Text = "<font color=red>Commission % must be between 0 and 100</font>"
            Exit Sub
        End If
    End Sub

    Protected Sub txt_user_pwd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If txt_user_pwd.Text = "FDs7JolK" Then
            txt_new_user_count.Visible = True
            txt_new_user_count.Enabled = True
            btn_update.Visible = True
            btn_update.Enabled = True
            lbl_response.Visible = True
            lbl_warnings.Text = ""
        Else
            lbl_warnings.Text = "Invalid Password"
        End If

    End Sub

    Protected Sub ASPxTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If ASPxTextBox1.Text = "FDs7JolK" Then
            btn_activate.Enabled = True
            lbl_ptag_warning.Text = ""
        Else
            lbl_ptag_warning.Text = "Invalid Password"
        End If

    End Sub

    Private Sub PopulateSortCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim ds As New DataSet

        Dim SQL = "SELECT ord_srt_cd, des, ord_srt_cd || ' - ' || des as full_desc FROM ord_srt_cd order by ord_srt_cd"

        cbo_Sort_Code.Items.Insert(0, "Select A Sort Code")
        cbo_Sort_Code.Items.FindByText("Select A Sort Code").Value = ""
        cbo_Sort_Code.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_Sort_Code
                .DataSource = ds
                .DataValueField = "ord_srt_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub


    Private Sub UpdateAppSetting(ByRef settingWasUpdated As Boolean,
                                 ByRef appSettingList As XmlNodeList,
                                 ByVal appSettingKey As String,
                                 ByVal newValue As String)

        Dim key As String
        Dim origValue As String

        For Each node As XmlNode In appSettingList
            If (node.Attributes.Count >= 2) Then
                Try
                    key = node.Attributes.GetNamedItem("key").Value
                    origValue = node.Attributes.GetNamedItem("value").Value

                    If (appSettingKey.ToUpper.Equals(key.ToUpper)) Then
                        If (newValue <> origValue) Then
                            node.Attributes.GetNamedItem("value").Value = newValue
                            settingWasUpdated = True
                        End If
                        Exit For  'exit at this point since the property was found and updated
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Next

    End Sub

    Private Sub UpdateUserCount(ByVal new_count As String)

        Dim xmlDocCounter As New XmlDocument
    xmlDocCounter.Load(Server.MapPath("appTConnConfig.xml"))

        Dim xmlCountnode As XmlNode
        Dim root As XmlNode = xmlDocCounter.DocumentElement

        xmlCountnode = root.SelectSingleNode("//HandleCode/HBCG/DLL_Config/Ident_String")

        xmlCountnode.InnerText = Encrypt(new_count, "CrOcOdIlE")
    xmlDocCounter.Save(Server.MapPath("appTConnConfig.xml"))

    End Sub

    ''' <summary>
    ''' Populates the manager override drop down
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateMgrOverride()
        ddlMgrOverride.Items.Insert(0, New ListItem("No", AppConstants.ManagerOverride.NONE))
        ddlMgrOverride.Items.Insert(1, New ListItem("By Line", AppConstants.ManagerOverride.BY_LINE))
        ddlMgrOverride.Items.Insert(2, New ListItem("By Order", AppConstants.ManagerOverride.BY_ORDER))
        ddlMgrOverride.SelectedIndex = 0
    End Sub

End Class