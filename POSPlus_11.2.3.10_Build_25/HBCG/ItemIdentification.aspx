<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" 
    CodeFile="ItemIdentification.aspx.vb" Inherits="ItemIdentification" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div id="Div1" runat="server">   
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Item Identification Codes" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 
     <table>
         <tr>
             <td>
                 <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
                    Text="" Width="100%"> 
                 </dx:ASPxLabel>
            </td>
         </tr>
     </table>
    <br />
     <%-- ============================================   --%> 
    <asp:Panel ID="pnl_search" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" visible="true">
      <table>
        <tr>
            <td> 
                    <dx:ASPxLabel ID="ASPxLabel9" Text=""  width="90px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
                <td> 
                    <dx:ASPxLabel ID="ASPxLabel2" Text="Company Code"  width="120px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                </td>
                <td> 
                    <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="145px" EnableViewState="true" ID="srch_company_cd" AutoPostBack="true">                   
                    </asp:dropdownlist> 
                </td>
    
                <td style="text-align:right">
                    <dx:ASPxLabel ID="ASPxLabel8" Text="Identification Code" Width =" 120" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <asp:TextBox ID="srch_ident_cd" Width="140px" runat="server" MaxLength ="3"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td> 
            </td>
            <td > <dx:ASPxLabel ID="ASPxLabel5" Text="Ident. Description" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
                <td colspan ="3"> 
                <asp:TextBox ID="srch_ident_desc" Width="415px" runat="server" MaxLength="40" ></asp:TextBox> 
            </tr>
        <tr>
            <td>

            </td>     
         
            <td>

            </td>   
    
            <td>

            </td> 

           <td>

           </td> 
        </tr>        <%-- SEARCH BUTTON --%>
        <tr>
            <td colspan ="2">

            </td>
            <td colspan ="2">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="Search">
                </dx:ASPxButton>
            </td>     
            <td>
                <dx:ASPxButton ID="btn_new" runat="server" Font-Bold="true" Text="New">
                </dx:ASPxButton>
            </td>
      </tr>
       </table>
       </asp:Panel>

    <asp:Panel ID="pnl_update" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" Visible ="false" >
      <table>
        <tr>
            <td> 
                    <dx:ASPxLabel ID="ASPxLabel1" Text=""  width="90px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
                <td> 
                    <dx:ASPxLabel ID="ASPxLabel3" Text="Company Code"  width="120px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                </td>
                <td> 
                    <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="145px" EnableViewState="true" ID="upd_company_cd" AutoPostBack="true">                   
                    </asp:dropdownlist> 
                </td>
    
                <td style="text-align:right">
                    <dx:ASPxLabel ID="ASPxLabel4" Text="Identification Code" Width =" 120" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <asp:TextBox ID="upd_ident_cd" Width="140px" runat="server" MaxLength="3"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td> 
            </td>
            <td > <dx:ASPxLabel ID="ASPxLabel6" Text="Ident. Description" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
                <td colspan ="3"> <asp:TextBox ID="upd_ident_desc" Width="415px" runat="server" MaxLength ="40"></asp:TextBox> 
            </tr>
        <tr>
            <td>

            </td>     
         
            <td>

            </td>   
    
            <td>

            </td> 

           <td>

           </td> 
        </tr>        <%-- SEARCH BUTTON --%>
        <tr>
            <td colspan ="2">

            </td>
            <td >
                <dx:ASPxButton ID="btn_update" runat="server" Font-Bold="true" Text="Update">
                </dx:ASPxButton>
            </td>     
            <td>
                <dx:ASPxButton ID="btn_delete" runat="server" Font-Bold="true" Text="Delete" Visible ="false">
                </dx:ASPxButton>
            </td>     
             <td>
                <dx:ASPxButton ID="btn_cancel" runat="server" Font-Bold="true" Text="Cancel">
                </dx:ASPxButton>
            </td>     
     </tr>
       </table>
       </asp:Panel>

    <br />
    <br />
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    <%-- -------------------------------------------------- EDIT grid-------------------------------------------- --%>
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    
     <%-- ===========================================================================   --%>  
    <%-- ===== --%>
    <asp:Panel ID="pnl_grid" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
   
    <%-- gridview --%>
        <dx:ASPxGridView ID="GV_ident_codes" runat="server" AutoGenerateColumns="False" KeyFieldName="CO_CD;II_CD" EnableRowsCache="true"
        OnDataBinding="GV_DataBind" Settings-ShowFooter="true" Width="950px" Enabled="true">

        <Settings ShowFooter="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsPager Position="Bottom">
            <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
        </SettingsPager>
          
       <%-- ---------------- COLUMNS ------------------ --%>
        <Columns>
          <dx:GridViewDataTextColumn Caption="Company Code" FieldName="CO_CD" VisibleIndex="1" Width ="100">
              <CellStyle Wrap="False"/>
                <PropertiesTextEdit Width="10"  MaxLength="3">
                <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                    <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Value" />
                </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn Caption="Company Name" FieldName="COMPANY_NAME" VisibleIndex="2" Width ="300">
              <CellStyle Wrap="False"/>
                <PropertiesTextEdit Width="60"  MaxLength="30">
                <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                    <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Value" />
                </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn Caption="Identification Code" FieldName="II_CD" VisibleIndex="3" Width ="120">
                <PropertiesTextEdit Width="30"  MaxLength="3">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                        <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Value" />
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="Identification Description" FieldName="DESCRIPTION" VisibleIndex="4">
                 <PropertiesTextEdit Width="500"  MaxLength="40">
                    <ValidationSettings>
                    <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                 </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Select" Settings-AllowSort="False"
                HeaderStyle-Cursor="default" Width="15%" VisibleIndex="5">
                <CellStyle HorizontalAlign="Center"></CellStyle>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnSelect" runat="server" Text="Select" CommandName="Select" HorizontalAlign ="Left"
                        CommandArgument="<%# Container.ItemIndex %>">
                    </dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
       </Columns>
        
        <SettingsEditing Mode="Inline"></SettingsEditing>
        <Settings UseFixedTableLayout="True" />
        <SettingsDataSecurity AllowDelete="True" />
        <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />
    
    </dx:ASPxGridView>
   
    </asp:Panel>

    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
