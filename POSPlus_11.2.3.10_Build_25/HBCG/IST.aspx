<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="IST.aspx.vb" Inherits="IST" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label253 %>">
                </dx:ASPxLabel>
            </td>
            <td colspan="3" style="width: 338px">
                <asp:TextBox ID="txt_ist" runat="server" Style="position: relative"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label254 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_store_cd" runat="server" Style="position: relative" Width="79px"></asp:TextBox>&nbsp;
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label571 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_status" runat="server" Width="80px" MaxLength="12"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label252 %>" Width="120px">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_dest_store_cd" runat="server" Style="position: relative" MaxLength="15"
                    Width="79px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp; &nbsp;
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label629 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_b_phone" runat="server" Style="left: 0px; position: relative"
                    Width="80px" MaxLength="12"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label685 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_zone" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label628 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_transfer" runat="server" Style="position: relative" MaxLength="20"
                    Width="80px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_wr_dt" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="lbl_seq_num" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
    <br />
    <dx:ASPxGridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="98%">
        <Columns>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label266 %>" FieldName="LN#" Visible="True" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label550 %>" FieldName="ITM_CD" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label653 %>" FieldName="VSN" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label357 %>" FieldName="OLD_STORE_CD" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label356 %>" FieldName="OLD_LOC_CD" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="New Store" FieldName="NEW_STORE_CD" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="New Loc" FieldName="NEW_LOC_CD" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label94 %>" FieldName="CRPT_ID_NUM" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label555 %>" VisibleIndex="10">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_so" runat="server" Text="" Visible="True">
                    </dx:ASPxLabel>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label276 %>">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
