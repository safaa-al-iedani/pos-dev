Imports System.IO
Imports HBCG_Utils
Imports System.Data.OracleClient
Imports System.Data.OleDb

Partial Class Special_Order_Popup
    Inherits POSBasePage

    Public Sub itm_tp_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = "SELECT itm_tp_cd, des FROM itm_tp order by des"
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_ITM_TP_CD
                .DataSource = ds
                .DataValueField = "itm_tp_cd"
                .DataTextField = "des"
                .DataBind()
                .SelectedValue = "INV"
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub mnr_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim sql As String
        sql = "SELECT mnr_cd, inv_mnr.des, mnr_cd || ' - ' || inv_mnr.des || ' (' || inv_mjr.des || ')' as full_desc "
        sql = sql & "FROM inv_mnr, inv_mjr "
        sql = sql & "where inv_mnr.mjr_cd = inv_mjr.mjr_cd "
        sql = sql & "order by inv_mnr.mnr_cd"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_minor
                .DataSource = ds
                .DataValueField = "mnr_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'Populate the item type drop down
            itm_tp_populate()

            'Populate the minor codes
            mnr_populate()
            MinorChanges()
            Populate_Form()
        End If

    End Sub

    Public Sub Populate_Form()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDatareader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim Warnings_Popup As String = ""

        sql = "SELECT * FROM ITM WHERE ITM_CD='" & Request("SKU") & "'"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDatareader.Read() Then
                txt_SKU.Text = MyDatareader.Item("ITM_CD").ToString
                cbo_ITM_TP_CD.SelectedValue = MyDatareader.Item("ITM_TP_CD").ToString
                txt_Vendor.Text = MyDatareader.Item("VE_CD").ToString
                cbo_minor.SelectedValue = MyDatareader.Item("MNR_CD").ToString
                txtVSN.Text = MyDatareader.Item("VSN").ToString
                txtDES.Text = MyDatareader.Item("DES").ToString
                txtCover.Text = MyDatareader.Item("COVER").ToString
                txtRetail.Text = MyDatareader.Item("RET_PRC").ToString
                txtMFG.Text = MyDatareader.Item("PRC1").ToString
                txtGrade.Text = MyDatareader.Item("GRADE").ToString
                txtSize.Text = MyDatareader.Item("SIZ").ToString
                txtFinish.Text = MyDatareader.Item("FINISH").ToString
                'txtComment.Text = MyDatareader.Item("ITM_CD").ToString 
                If Find_Security("CPTR_PCST", Session("emp_cd")) = "Y" Then
                    txt_cost.Text = MyDatareader.Item("IVC_CST").ToString
                Else
                    txt_cost.Enabled = False
                    txt_cost.Text = ""
                End If
                cboStyle.SelectedValue = MyDatareader.Item("STYLE_CD").ToString
                txtFamily.Text = MyDatareader.Item("FAMILY_CD").ToString
                cbo_category.SelectedValue = MyDatareader.Item("CAT_CD").ToString
                txtVolume.Text = MyDatareader.Item("VOL").ToString
                'txtMeasure.Text = MyDatareader.Item("MEASURE").ToString 
                'txtFreight.Text = MyDatareader.Item("FREIGHT").ToString 
                txtStop.Text = MyDatareader.Item("STOP_TIME").ToString
                'txtCarpet.Text = MyDatareader.Item("CARPET").ToString
                cbo_Warr.SelectedValue = MyDatareader.Item("WARRANTABLE").ToString
                txtPad.Text = MyDatareader.Item("AVAIL_PAD_DAYS").ToString
                txtPoints.Text = MyDatareader.Item("POINT_SIZE").ToString
                cboTreatable.SelectedValue = MyDatareader.Item("TREATABLE").ToString
            End If
        Catch
            conn.Close()
            Throw
        End Try

        sql = "SELECT ITM_CD, SEQ#, TEXT FROM ITM_CMNT WHERE ITM_CD='" & Request("SKU") & "' ORDER BY SEQ#"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDatareader.Read()
                lbl_comments.Text = lbl_comments.Text & MyDatareader.Item("TEXT").ToString
            Loop
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

        If Request("SPEC_ORD") = "Y" Then
            txt_SKU.Enabled = True
            cbo_ITM_TP_CD.Enabled = True
            txt_Vendor.Enabled = True
            cbo_minor.Enabled = True
            txtVSN.Enabled = True
            txtDES.Enabled = True
            txtCover.Enabled = True
            txtRetail.Enabled = True
            txtMFG.Enabled = True
            txtGrade.Enabled = True
            txtSize.Enabled = True
            txtFinish.Enabled = True
            cboStyle.Enabled = True
            txtFamily.Enabled = True
            cbo_category.Enabled = True
            txtVolume.Enabled = True
            txtStop.Enabled = True
            cbo_Warr.Enabled = True
            txtPad.Enabled = True
            txtPoints.Enabled = True
            cboTreatable.Enabled = True
            btn_save_exit.Enabled = True
            txtComment.Enabled = True



        End If
        Dim skuList As ArrayList = New ArrayList()
        Try
            skuList.Add(Request("SKU").ToString())
            'test code
            'divSortCodes.InnerHtml = SkuUtils.GetItemSortCodesInString(skuList).ToString()
            'Dim SortCodes As StringBuilder = New StringBuilder()
            'Dim i As Integer = 0
            'For i = 0 To 10
            '    SortCodes.Append(i.ToString()).Append(" - ").Append(i.ToString()).Append(Environment.NewLine())
            'Next
            Dim sku As SKUBiz = New SKUBiz()
            'divSortCodes.InnerHtml = sku.GetItemSortCodesInString(skuList).ToString().Replace(Environment.NewLine(), "<BR/>")
            If Not Session("ord_tp_cd") Is Nothing AndAlso (Session("ord_tp_cd").ToString().Trim().ToUpper() = "SAL" Or Session("ord_tp_cd").ToString().Trim().ToUpper() = "CRM") Then
                txtSortCodes.Text = sku.GetItemSortCodesInString(skuList).ToString()
            Else
                txtSortCodes.Text = ""
            End If



        Catch ex As Exception
            txtSortCodes.Text = ""
        End Try

    End Sub

    Public Sub Add_SKU(Optional ByVal add_another As String = "NO")

        Dim itm_cd As String
        Dim itm_tp_cd As String
        Dim ve_cd As String
        Dim mnr_cd As String
        Dim VSN As String
        Dim DES As String
        Dim Cover As String
        Dim ret_prc As Double
        Dim prc1 As Double
        Dim grade As String
        Dim size As String
        Dim finish As String
        Dim comment As String
        Dim style As String
        Dim family As String
        Dim category As String
        Dim volume As Double
        Dim measure As Double
        Dim freight As Double
        Dim stop_time As Double
        Dim carpet As Double
        Dim warr As String
        Dim pad As Double
        Dim points As Double
        Dim treatable As String

        itm_cd = txt_SKU.Text.Trim
        itm_tp_cd = cbo_ITM_TP_CD.SelectedValue.Trim
        ve_cd = txt_Vendor.Text.Trim
        mnr_cd = cbo_minor.SelectedValue.Trim
        VSN = txtVSN.Text.Trim
        DES = txtDES.Text.Trim
        Cover = txtCover.Text.Trim
        If Not IsNumeric(txtRetail.Text.Trim) Then
            ret_prc = 0
        Else
            ret_prc = txtRetail.Text.Trim
        End If
        If Not IsNumeric(txtMFG.Text.Trim) Then
            prc1 = 0
        Else
            prc1 = txtMFG.Text.Trim
        End If
        grade = txtGrade.Text.Trim
        size = txtSize.Text.Trim
        finish = txtFinish.Text.Trim
        comment = txtComment.Text.Trim
        style = cboStyle.SelectedValue.Trim
        family = txtFamily.Text.Trim
        category = cbo_category.SelectedValue.Trim
        If Not IsNumeric(txtVolume.Text.Trim) Then
            volume = 0
        Else
            volume = txtVolume.Text.Trim
        End If
        If Not IsNumeric(txtMeasure.Text.Trim) Then
            measure = 0
        Else
            measure = txtMeasure.Text.Trim
        End If
        If Not IsNumeric(txtFreight.Text.Trim) Then
            freight = 0
        Else
            freight = txtFreight.Text.Trim
        End If
        If Not IsNumeric(txtStop.Text.Trim) Then
            stop_time = 1
        Else
            If CDbl(txtStop.Text.Trim) > 0 Then
                stop_time = txtStop.Text.Trim
            Else
                stop_time = 1
            End If
        End If
        If Not IsNumeric(txtCarpet.Text.Trim) Then
            carpet = 0
        Else
            carpet = txtCarpet.Text.Trim
        End If
        warr = cbo_Warr.SelectedValue.Trim
        If Not IsNumeric(txtPad.Text.Trim) Then
            pad = 0
        Else
            pad = txtPad.Text.Trim
        End If
        If Not IsNumeric(txtPoints.Text.Trim) Then
            points = 0
        Else
            points = txtPoints.Text.Trim()
        End If
        treatable = cboTreatable.SelectedValue.Trim

        If txt_Vendor.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a vendor code</font>"
            Exit Sub
        End If

        If txtVSN.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a vendor stock number</font>"
            Exit Sub
        End If

        If txtDES.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a description</font>"
            Exit Sub
        End If

        If Not IsNumeric(txtRetail.Text) Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a numeric retail price</font>"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDatareader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim Warnings_Popup As String = ""

        sql = "SELECT VE_CD FROM VE WHERE VE_CD='" & ve_cd & "'"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If Not MyDatareader.Read() Then
                lbl_SKU_Info.Text = "<font color=red>Invalid Vendor Code</font>"
                Exit Sub
            End If
        Catch
            conn.Close()
            Throw
        End Try

        'sql = "UPDATE ITM SET LST_ACT_DT=TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
        'sql = sql & "ITM_TP_CD='" & itm_tp_cd & "', "
        'sql = sql & "MNR_CD='" & mnr_cd & "', "
        'If style & "" <> "" Then
        '    sql = sql & "STYLE_CD='" & style & "', "
        'End If
        'If family & "" <> "" Then
        '    sql = sql & "FAMILY_CD='" & family & "', "
        'End If
        'If VSN & "" <> "" Then
        '    sql = sql & "VSN='" & VSN & "', "
        'End If
        'If DES & "" <> "" Then
        '    sql = sql & "DES='" & DES & "', "
        'End If
        'If Cover & "" <> "" Then
        '    sql = sql & "COVER='" & Cover & "', "
        'End If
        'If ve_cd & "" <> "" Then
        '    sql = sql & "VE_CD='" & ve_cd & "', "
        'End If
        'sql = sql & "RET_PRC=" & ret_prc & ", "
        'sql = sql & "PRC1=" & prc1 & ", "
        'sql = sql & "VOL=" & volume & ", "
        'sql = sql & "STOP_TIME=" & stop_time & ", "
        'sql = sql & "AVAIL_PAD_DAYS=" & pad & ", "
        'sql = sql & "POINT_SIZE=" & points & ", "
        'If grade & "" <> "" Then
        '    sql = sql & "GRADE='" & grade & "', "
        'End If
        'If size & "" <> "" Then
        '    sql = sql & "SIZ='" & size & "', "
        'End If
        'If finish & "" <> "" Then
        '    sql = sql & "FINISH='" & finish & "', "
        'End If
        'If category & "" <> "" Then
        '    sql = sql & "CAT_CD='" & category & "', "
        'End If
        'If warr & "" <> "" Then
        '    sql = sql & "WARRANTABLE='" & warr & "', "
        'End If
        'If treatable & "" <> "" Then
        '    sql = sql & "TREATABLE='" & treatable & "', "
        'End If
        'sql = Left(sql, Len(sql) - 2)
        'sql = sql & " WHERE ITM_CD='" & txt_SKU.Text & "'"

        'sql = UCase(sql)
        sql = "UPDATE ITM SET INVENTORY=:INVENTORY, REPL_CST=:REPL_CST, LST_ACT_DT=:LST_ACT_DT, COMM_CD=:COMM_CD, "
        sql = sql & "ITM_TP_CD=:ITM_TP_CD, MNR_CD=:MNR_CD, STYLE_CD=:STYLE_CD, "
        sql = sql & "FAMILY_CD=:FAMILY_CD, VSN=:VSN, DES=:DES, COVER=:COVER, VE_CD=:VE_CD, RET_PRC=:RET_PRC, PRC1=:PRC1, VOL=:VOL, "
        sql = sql & "STOP_TIME=:STOP_TIME, AVAIL_PAD_DAYS=:AVAIL_PAD_DAYS, POINT_SIZE=:POINT_SIZE, FRAME_TP_ITM=:FRAME_TP_ITM, "
        sql = sql & "GRADE=:GRADE, SIZ=:SIZ, FINISH=:FINISH, CAT_CD=:CAT_CD, WARRANTABLE=:WARRANTABLE, TREATABLE=:TREATABLE "
        sql = sql & "WHERE ITM_CD=:ITM_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
        objSql.Parameters.Add(":INVENTORY", OracleType.VarChar)
        objSql.Parameters.Add(":REPL_CST", OracleType.Number)
        objSql.Parameters.Add(":LST_ACT_DT", OracleType.DateTime)
        objSql.Parameters.Add(":COMM_CD", OracleType.VarChar)
        objSql.Parameters.Add(":ITM_TP_CD", OracleType.VarChar)
        objSql.Parameters.Add(":MNR_CD", OracleType.VarChar)
        objSql.Parameters.Add(":STYLE_CD", OracleType.VarChar)
        objSql.Parameters.Add(":FAMILY_CD", OracleType.VarChar)
        objSql.Parameters.Add(":VSN", OracleType.VarChar)
        objSql.Parameters.Add(":DES", OracleType.VarChar)
        objSql.Parameters.Add(":COVER", OracleType.VarChar)
        objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
        objSql.Parameters.Add(":RET_PRC", OracleType.Number)
        objSql.Parameters.Add(":PRC1", OracleType.Number)
        objSql.Parameters.Add(":VOL", OracleType.Number)
        objSql.Parameters.Add(":STOP_TIME", OracleType.Number)
        objSql.Parameters.Add(":AVAIL_PAD_DAYS", OracleType.Number)
        objSql.Parameters.Add(":POINT_SIZE", OracleType.Number)
        objSql.Parameters.Add(":FRAME_TP_ITM", OracleType.VarChar)
        objSql.Parameters.Add(":GRADE", OracleType.VarChar)
        objSql.Parameters.Add(":SIZ", OracleType.VarChar)
        objSql.Parameters.Add(":FINISH", OracleType.VarChar)
        objSql.Parameters.Add(":CAT_CD", OracleType.VarChar)
        objSql.Parameters.Add(":WARRANTABLE", OracleType.VarChar)
        objSql.Parameters.Add(":TREATABLE", OracleType.VarChar)

        objSql.Parameters(":ITM_CD").Value = UCase(txt_SKU.Text)
        objSql.Parameters(":INVENTORY").Value = "Y"
        objSql.Parameters(":REPL_CST").Value = 0
        objSql.Parameters(":LST_ACT_DT").Value = FormatDateTime(Date.Today, DateFormat.ShortDate)
        objSql.Parameters(":COMM_CD").Value = "NON"
        objSql.Parameters(":ITM_TP_CD").Value = UCase(itm_tp_cd)
        objSql.Parameters(":MNR_CD").Value = UCase(mnr_cd)
        objSql.Parameters(":STYLE_CD").Value = UCase(style)
        objSql.Parameters(":FAMILY_CD").Value = UCase(family)
        objSql.Parameters(":VSN").Value = UCase(VSN)
        objSql.Parameters(":DES").Value = UCase(DES)
        objSql.Parameters(":COVER").Value = UCase(Cover)
        objSql.Parameters(":VE_CD").Value = UCase(ve_cd)
        objSql.Parameters(":RET_PRC").Value = ret_prc
        objSql.Parameters(":PRC1").Value = prc1
        objSql.Parameters(":VOL").Value = volume
        objSql.Parameters(":STOP_TIME").Value = stop_time
        objSql.Parameters(":AVAIL_PAD_DAYS").Value = pad
        objSql.Parameters(":POINT_SIZE").Value = points
        objSql.Parameters(":FRAME_TP_ITM").Value = "N"
        objSql.Parameters(":GRADE").Value = UCase(grade)
        objSql.Parameters(":SIZ").Value = UCase(size)
        objSql.Parameters(":FINISH").Value = UCase(finish)
        objSql.Parameters(":CAT_CD").Value = UCase(category)
        objSql.Parameters(":WARRANTABLE").Value = UCase(warr)
        objSql.Parameters(":TREATABLE").Value = UCase(treatable)

        objSql.ExecuteNonQuery()

        objSql.Parameters.Clear()

        ''Set SQL OBJECT 
        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        'Try
        '    'Execute DataReader 
        '    objSql.ExecuteNonQuery()
        'Catch ex As Exception
        '    conn.Close()
        '    Throw
        'End Try

        Dim lngSEQNUM As Integer = 0

        If comment & "" <> "" Then
            sql = "Select Max(SEQ#) from ITM_CMNT WHERE ITM_CD = '" & itm_cd & "'"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)


            If MyDatareader.Read() Then
                If MyDatareader.IsDBNull(0) = False Then
                    lngSEQNUM = MyDatareader(0)
                    lngSEQNUM = lngSEQNUM + 1
                Else
                    lngSEQNUM = 1
                End If
            Else
                lngSEQNUM = 1
            End If

            'sql = "INSERT INTO ITM_CMNT (ITM_CD, SEQ#, TEXT) VALUES('" & txt_SKU.Text & "'," & lngSEQNUM & ",'" & Left(comment, 70) & "')"
            'sql = UCase(sql)
            sql = "INSERT INTO ITM_CMNT (ITM_CD, SEQ#, TEXT) VALUES(:ITM_CD, :SEQ#, :TEXT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SEQ#", OracleType.Number)
            objSql.Parameters.Add(":TEXT", OracleType.VarChar)

            objSql.Parameters(":ITM_CD").Value = txt_SKU.Text
            objSql.Parameters(":SEQ#").Value = lngSEQNUM
            objSql.Parameters(":TEXT").Value = UCase(Left(comment, 70))

            Try
                'Execute DataReader 
                objSql.ExecuteNonQuery()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        objSql.Parameters.Clear()

        'If it is a new special order, let the salesperson make adjustments to the SKU
        If Request("SPEC_ORD") & "" <> "" And Request("ROW_ID") & "" <> "" Then
            'sql = "UPDATE TEMP_ITM SET ITM_TP_CD='" & itm_tp_cd & "', "
            'sql = sql & "MNR_CD='" & mnr_cd & "', "
            'If style & "" <> "" Then
            '    sql = sql & "STYLE_CD='" & style & "', "
            'End If
            'If IsNumeric(txtRetail.Text) Then
            '    sql = sql & "RET_PRC=" & CDbl(txtRetail.Text) & ", "
            'End If
            'If VSN & "" <> "" Then
            '    sql = sql & "VSN='" & VSN & "', "
            'End If
            'If DES & "" <> "" Then
            '    sql = sql & "DES='" & DES & "', "
            'End If
            'If Cover & "" <> "" Then
            '    sql = sql & "COVER='" & Cover & "', "
            'End If
            'If ve_cd & "" <> "" Then
            '    sql = sql & "VE_CD='" & ve_cd & "', "
            'End If
            'sql = sql & "DEL_PTS=" & points & ", "
            'If grade & "" <> "" Then
            '    sql = sql & "GRADE='" & grade & "', "
            'End If
            'If size & "" <> "" Then
            '    sql = sql & "SIZ='" & size & "', "
            'End If
            'If finish & "" <> "" Then
            '    sql = sql & "FINISH='" & finish & "', "
            'End If
            'If category & "" <> "" Then
            '    sql = sql & "CAT_CD='" & category & "', "
            'End If
            'If treatable & "" <> "" Then
            '    sql = sql & "TREATABLE='" & treatable & "', "
            'End If
            'sql = Left(sql, Len(sql) - 2)
            'sql = sql & " WHERE ROW_ID=" & Request("ROW_ID") & ""

            sql = "UPDATE TEMP_ITM SET RET_PRC=:RET_PRC, STYLE_CD=:STYLE_CD, MNR_CD=:MNR_CD, VSN=:VSN, DES=:DES, "
            sql = sql & "COVER=:COVER, VE_CD=:VE_CD, DEL_PTS=:DEL_PTS, GRADE=:GRADE, SIZ=:SIZ, FINISH=:FINISH, "
            sql = sql & "CAT_CD=:CAT_CD, TREATABLE=:TREATABLE "
            sql = sql & "WHERE ROW_ID=:ITM_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":RET_PRC", OracleType.Number)
            objSql.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objSql.Parameters.Add(":STYLE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":VSN", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":COVER", OracleType.VarChar)
            objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DEL_PTS", OracleType.Number)
            objSql.Parameters.Add(":GRADE", OracleType.VarChar)
            objSql.Parameters.Add(":SIZ", OracleType.VarChar)
            objSql.Parameters.Add(":FINISH", OracleType.VarChar)
            objSql.Parameters.Add(":CAT_CD", OracleType.VarChar)
            objSql.Parameters.Add(":TREATABLE", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_CD", OracleType.Number)

            objSql.Parameters(":RET_PRC").Value = CDbl(txtRetail.Text)
            objSql.Parameters(":MNR_CD").Value = UCase(mnr_cd)
            objSql.Parameters(":STYLE_CD").Value = UCase(style)
            objSql.Parameters(":VSN").Value = UCase(VSN)
            objSql.Parameters(":DES").Value = UCase(DES)
            objSql.Parameters(":COVER").Value = UCase(Cover)
            objSql.Parameters(":VE_CD").Value = UCase(ve_cd)
            objSql.Parameters(":DEL_PTS").Value = points
            objSql.Parameters(":GRADE").Value = UCase(grade)
            objSql.Parameters(":SIZ").Value = UCase(size)
            objSql.Parameters(":FINISH").Value = UCase(finish)
            objSql.Parameters(":CAT_CD").Value = UCase(category)
            objSql.Parameters(":TREATABLE").Value = UCase(treatable)
            objSql.Parameters(":ITM_CD").Value = Request("ROW_ID")

            objSql.ExecuteNonQuery()

            objSql.Parameters.Clear()
        End If

        conn.Close()

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        If Request("LEAD") = "TRUE" Then
            Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE';")
        Else
            Response.Write("parentWindow.location.href='order_detail.aspx';")
        End If
        Response.Write("</script>")

    End Sub

    Public Sub Verify_Vendor()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDatareader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim Warnings_Popup As String = ""

        'jkl
        'sql = "SELECT VE_CD,VE_NAME FROM VE WHERE VE_CD='" & txt_Vendor.Text & "'"
        'sql = UCase(sql)

        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.CommandText = "SELECT VE_CD,VE_NAME FROM VE WHERE VE_CD=:VE_CD"
        objSql.Connection = conn
        objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
        objSql.Parameters(":VE_CD").Value = UCase(txt_Vendor.Text)


        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If Not MyDatareader.Read() Then
                lbl_SKU_Info.Text = "<font color=red>Invalid Vendor Code</font>"
                btn_save_exit.Enabled = False
                txt_Vendor.Text = ""
                txt_Vendor.Focus()
                lbl_ve_label.Text = ""
            Else
                lbl_ve_label.Text = MyDatareader.Item("VE_NAME").ToString
                btn_save_exit.Enabled = True
            End If
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub Create_SKU(ByVal itm_cd As String)

        Dim randObj As New Random
        Dim x As Integer
        Dim Start_Number As Integer
        Dim End_Number As Integer
        Dim test As Integer
        Dim SKU As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim SKU_Limit As Double = CDbl(ConfigurationManager.AppSettings("special_order_limits").ToString)

        If Not IsNumeric(SKU_Limit) Then
            SKU_Limit = 9
        Else
            If SKU_Limit < 1 Then
                SKU_Limit = 9
            End If
        End If
        SKU = "FALSE"
        lbl_SKU_Info.Text = ""
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Do While SKU = "FALSE"
            If Len(itm_cd) <> SKU_Limit Then
                Start_Number = 1
                End_Number = 0
                For x = 1 To (SKU_Limit - Len(itm_cd))
                    Start_Number = Start_Number * 10
                Next
                test = SKU_Limit - Len(itm_cd)
                End_Number = Replace(Start_Number, 0, SKU_Limit)
                End_Number = Right(End_Number, test)
                Start_Number = Left(Start_Number, test)
                SKU = itm_cd & (randObj.Next(Start_Number, End_Number).ToString())
            Else : SKU = itm_cd
            End If

            'jkl

            'Set SQL OBJECT 
            'objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.CommandText = "SELECT ITM_CD FROM ITM WHERE ITM_CD=:SKU"
            objSql.Connection = conn
            objSql.Parameters.Add(":SKU", OracleType.VarChar)
            objSql.Parameters(":SKU").Value = SKU

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If Len(itm_cd) = SKU_Limit Then
                        lbl_SKU_Info.Text = "**THE SKU YOU SELECTED IS NOT AVAILABLE**"
                        SKU = txt_SKU.Text
                        Exit Do
                    End If
                    SKU = "FALSE"
                Else
                    SKU = SKU
                    lbl_SKU_Info.Text = "SKU Created"
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop
        txt_SKU.Text = SKU

    End Sub

    Protected Sub btn_save_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_exit.Click

        Add_SKU("NO")

    End Sub

    Protected Sub cbo_minor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_minor.SelectedIndexChanged

        MinorChanges()

    End Sub

    Public Sub Update_Category_Data()

        If Not String.IsNullOrEmpty(cbo_category.SelectedValue.ToString) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDatareader As OracleDataReader

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            'jkl
            'Dim sql As String
            'sql = "SELECT CAT_POINT_SIZE, STOP_TIME FROM INV_MNR_CAT WHERE CAT_CD='" & cbo_category.SelectedValue & "'"

            'objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.CommandText = "SELECT CAT_POINT_SIZE, STOP_TIME FROM INV_MNR_CAT WHERE CAT_CD=:CAT_CD"
            objsql2.Connection = conn
            objsql2.Parameters.Add(":CAT_CD", OracleType.VarChar)
            objsql2.Parameters(":CAT_CD").Value = cbo_category.SelectedValue

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader.Read()) Then
                    If IsNumeric(MyDatareader.Item("CAT_POINT_SIZE").ToString) Then
                        txtPoints.Text = MyDatareader.Item("CAT_POINT_SIZE").ToString
                    Else
                        txtPoints.Text = "0"
                    End If
                    If IsNumeric(MyDatareader.Item("STOP_TIME").ToString) Then
                        txtStop.Text = MyDatareader.Item("STOP_TIME").ToString
                    Else
                        txtStop.Text = "0"
                    End If
                Else
                    txtStop.Text = "0"
                    txtPoints.Text = "0"
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            'jkl
            'sql = "SELECT TREATMENT_ITM_CD FROM INV_MNR_CAT WHERE MNR_CD = '" & cbo_minor.SelectedValue & "' AND CAT_CD = '" & cbo_category.SelectedValue & "'"
            'objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2 = DisposablesManager.BuildOracleCommand

            objsql2.CommandText = "SELECT TREATMENT_ITM_CD FROM INV_MNR_CAT WHERE MNR_CD = :MNR_CD AND CAT_CD = :CAT_CD"
            objsql2.Connection = conn
            objsql2.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objsql2.Parameters(":MNR_CD").Value = cbo_minor.SelectedValue
            objsql2.Parameters.Add(":CAT_CD", OracleType.VarChar)
            objsql2.Parameters(":CAT_CD").Value = cbo_category.SelectedValue

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader.Read()) Then
                    If Not String.IsNullOrEmpty(MyDatareader.Item("TREATMENT_ITM_CD").ToString) Then
                        cboTreatable.SelectedValue = "Y"
                    Else
                        cboTreatable.SelectedValue = "N"
                    End If
                Else
                    cboTreatable.SelectedValue = "N"
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Public Sub MinorChanges()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand



        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
        Try
            'jkl
            'Dim sql As String
            'sql = "SELECT style_cd, des, style_cd || ' - ' || des as full_desc FROM inv_mnr_style where mnr_cd='" & cbo_minor.SelectedValue & "' order by style_cd"


            cmdGetCodes.CommandText = "SELECT style_cd, des, style_cd || ' - ' || des as full_desc FROM inv_mnr_style where mnr_cd=:MNR_CD order by style_cd"
            cmdGetCodes.Connection = conn
            cmdGetCodes.Parameters.Add(":MNR_CD", OracleType.VarChar)
            cmdGetCodes.Parameters(":MNR_CD").Value = cbo_minor.SelectedValue


            conn.Open()
            oAdp.Fill(ds)

            With cboStyle
                .DataSource = ds
                .DataValueField = "style_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'Sql = "SELECT cat_cd, des, cat_cd || ' - ' || des as full_desc FROM inv_mnr_cat where mnr_cd='" & cbo_minor.SelectedValue & "' order by cat_cd"

        cmdGetCodes.Dispose()
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        ds.Dispose()
        ds = New DataSet

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = "SELECT cat_cd, des, cat_cd || ' - ' || des as full_desc FROM inv_mnr_cat where mnr_cd=:MNR_CD order by cat_cd"
                .Parameters.Add(":MNR_CD", OracleType.VarChar)
                .Parameters(":MNR_CD").Value = cbo_minor.SelectedValue
            End With

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_category
                .DataSource = ds
                .DataValueField = "cat_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
        Update_Category_Data()

    End Sub

    Private Sub Clear_Values()

        'Remove the former values from the screen
        txt_SKU.Text = ""
        txt_Vendor.Text = ""
        txtVSN.Text = ""
        txtDES.Text = ""
        txtCover.Text = ""
        txtRetail.Text = ""
        txtMFG.Text = ""
        txtGrade.Text = ""
        txtSize.Text = ""
        txtFinish.Text = ""
        txtComment.Text = ""
        txtFamily.Text = ""
        txtVolume.Text = ""
        txtMeasure.Text = ""
        txtFreight.Text = ""
        txtStop.Text = ""
        txtCarpet.Text = ""
        cbo_Warr.SelectedIndex = 0
        txtPad.Text = ""
        txtPoints.Text = ""
        cboTreatable.SelectedIndex = 0
        If cbo_Special_Forms.Items.Count > 0 Then cbo_Special_Forms.SelectedIndex = 0
        'cbo_Frames.SelectedIndex = 0
        cbo_Frames.Enabled = False

        'Populate the item type drop down
        itm_tp_populate()

        'Populate the minor codes
        mnr_populate()
        MinorChanges()

    End Sub

    Protected Sub cbo_Special_Forms_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Special_Forms.SelectedIndexChanged

        On Error Resume Next
        Err.Clear()
        hpl_excel.Visible = False
        'img_excel.Visible = False

        If cbo_Special_Forms.SelectedIndex <> 0 Then
            'Remove the former values from the screen
            txt_SKU.Text = ""
            txt_Vendor.Text = ""
            txtVSN.Text = ""
            txtDES.Text = ""
            txtCover.Text = ""
            txtRetail.Text = ""
            txtMFG.Text = ""
            txtGrade.Text = ""
            txtSize.Text = ""
            txtFinish.Text = ""
            txtComment.Text = ""
            txtFamily.Text = ""
            txtVolume.Text = ""
            txtMeasure.Text = ""
            txtFreight.Text = ""
            txtStop.Text = ""
            txtCarpet.Text = ""
            cbo_Warr.SelectedIndex = 0
            txtPad.Text = ""
            txtPoints.Text = ""
            cboTreatable.SelectedIndex = 0

            Dim myDataset As New DataSet()

            ''You can also use the Excel ODBC driver I believe - didn't try though
            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;"""

            ''You must use the $ after the object you reference in the spreadsheet
            Dim myData As New OleDbDataAdapter("SELECT DESCRIPTION FROM [SPECIALS$] GROUP BY DESCRIPTION ORDER BY DESCRIPTION", strConn)
            myData.TableMappings.Add("Table", "ExcelTest")
            myData.Fill(myDataset)
            hpl_excel.NavigateUrl = "merchandising\" & cbo_Special_Forms.SelectedValue
            hpl_excel.Visible = True
            'img_excel.Visible = True

            If Err.Number = 5 Then
                cbo_Frames.Enabled = False
                cbo_Frames.Visible = False

                lbl_SKU_Info.Text = "<font color=red><b>No Special Order Information Found!!!</b></font>"
            Else
                lbl_SKU_Info.Text = "Enter up to 9 characters"
                cbo_Frames.Enabled = True
                cbo_Frames.Visible = True

                cbo_Frames.Items.Clear()
                cbo_Frames.Items.Insert(0, "Please Select A Frame")
                cbo_Frames.Items.FindByText("Please Select A Frame").Value = ""
                cbo_Frames.SelectedIndex = 0

                With cbo_Frames
                    .DataSource = myDataset.Tables(0).DefaultView
                    .DataValueField = "DESCRIPTION"
                    .DataTextField = "DESCRIPTION"
                    .DataBind()
                End With
            End If

        Else
            lbl_SKU_Info.Text = "Enter up to 9 characters"
            cbo_Frames.Enabled = False
        End If

    End Sub

    Protected Sub cbo_Frames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Frames.SelectedIndexChanged

        If cbo_Special_Forms.SelectedIndex <> 0 Then
            'Remove the former values from the screen
            txt_SKU.Text = ""
            txt_Vendor.Text = ""
            txtVSN.Text = ""
            txtDES.Text = ""
            txtCover.Text = ""
            txtRetail.Text = ""
            txtMFG.Text = ""
            txtGrade.Text = ""
            txtSize.Text = ""
            txtFinish.Text = ""
            txtComment.Text = ""
            txtFamily.Text = ""
            txtVolume.Text = ""
            txtMeasure.Text = ""
            txtFreight.Text = ""
            txtStop.Text = ""
            txtCarpet.Text = ""
            cbo_Warr.SelectedIndex = 0
            txtPad.Text = ""
            txtPoints.Text = ""
            cboTreatable.SelectedIndex = 0

            ''You can also use the Excel ODBC driver I believe - didn''t try though
            Dim conn2 = DisposablesManager.BuildOracleConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;""")

            Dim myDataset As New DataSet()
            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;"""

            ''You must use the $ after the object you reference in the spreadsheet
            Dim sql As String = "SELECT * FROM [SPECIALS$] WHERE DESCRIPTION='" & cbo_Frames.SelectedValue & "'"
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader
            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Open Connection 
                conn2.Open()
                'Execute DataReader 
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader2.Read()) Then
                    txt_Vendor.Text = UCase(MyDatareader2(1).ToString)
                    txt_SKU.Text = MyDatareader2(0).ToString
                    txtDES.Text = UCase(MyDatareader2(3).ToString)
                    txtVSN.Text = UCase(MyDatareader2(2).ToString)
                    txtSize.Text = UCase(MyDatareader2(6).ToString)
                    cbo_ITM_TP_CD.SelectedValue = "INV"
                    cbo_minor.SelectedValue = UCase(MyDatareader2(5).ToString)
                    cboStyle.SelectedValue = UCase(MyDatareader2(4).ToString)
                    txtFinish.Text = UCase(MyDatareader2(7).ToString)
                End If

                'Close Connection 
                MyDatareader2.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        Else
            hpl_excel.Visible = False
            'img_excel.Visible = False
        End If

    End Sub

    Protected Sub txt_Vendor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Vendor.TextChanged

        Verify_Vendor()

    End Sub

    Protected Sub cbo_category_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Update_Category_Data()

    End Sub

    Protected Sub btn_ve_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ve_submit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim sql As String
        sql = "SELECT VE_CD, VE_NAME FROM VE WHERE "
        If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
            sql = sql & "VE_CD LIKE :VE_CD "
        End If
        If Not String.IsNullOrEmpty(txt_ve_name.Text) Then
            If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
                sql = sql & "AND "
            End If
            sql = sql & " VE_NAME LIKE :VE_NAME "
        End If
        sql = sql & "ORDER BY VE_NAME"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
            objsql2.Parameters.Add(":VE_CD", OracleType.VarChar)
            objsql2.Parameters(":VE_CD").Value = UCase(txt_ve_cd.Text)
        End If
        If Not String.IsNullOrEmpty(txt_ve_name.Text) Then
            objsql2.Parameters.Add(":VE_NAME", OracleType.VarChar)
            objsql2.Parameters(":VE_NAME").Value = UCase(txt_ve_name.Text)
        End If

        Dim oAdp As OracleDataAdapter
        conn.Open()

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
            oAdp.Fill(ds)

            With ve_grid
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub ve_grid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ve_grid.ItemCommand

        txt_Vendor.Text = e.Item.Cells(0).Text
        lbl_ve_label.Text = e.Item.Cells(1).Text
        ASPxPopupControl4.ShowOnPageLoad = False

    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        ASPxPopupControl4.ShowOnPageLoad = True

    End Sub

    Protected Sub ve_grid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles ve_grid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim myButton As Button = CType(e.Item.Cells(2).Controls(0), Button)
            myButton.CssClass = "style5"
        End If

    End Sub

    Protected Sub mnr_grid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles mnr_grid.ItemCommand

        cbo_minor.SelectedValue = e.Item.Cells(0).Text
        ASPxPopupControl1.ShowOnPageLoad = False
        MinorChanges()

    End Sub

    Protected Sub mnr_grid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles mnr_grid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or _
       e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim myButton As Button = CType(e.Item.Cells(2).Controls(0), Button)
            myButton.CssClass = "style5"
        End If

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub btn_mnr_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_mnr_submit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim sql As String
        sql = "SELECT MNR_CD, inv_mnr.des || ' (' || inv_mjr.des || ')' as full_des "
        sql = sql & "FROM inv_mnr, inv_mjr "
        sql = sql & "where inv_mnr.mjr_cd = inv_mjr.mjr_cd "
        If Not String.IsNullOrEmpty(txt_mnr_search.Text) Then
            sql = sql & "AND MNR_CD LIKE :MNR_CD "
        End If
        If Not String.IsNullOrEmpty(txt_mnr_desc_search.Text) Then
            sql = sql & "AND inv_mnr.DES LIKE :DES "
        End If
        sql = sql & "ORDER BY inv_mnr.DES"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        If Not String.IsNullOrEmpty(txt_mnr_search.Text) Then
            objsql2.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objsql2.Parameters(":MNR_CD").Value = UCase(txt_mnr_search.Text)
        End If
        If Not String.IsNullOrEmpty(txt_mnr_desc_search.Text) Then
            objsql2.Parameters.Add(":DES", OracleType.VarChar)
            objsql2.Parameters(":DES").Value = UCase(txt_mnr_desc_search.Text)
        End If

        Dim oAdp As OracleDataAdapter
        conn.Open()

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
            oAdp.Fill(ds)

            With mnr_grid
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub
End Class
