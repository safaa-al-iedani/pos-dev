<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SRRTD_DETAIL.aspx.vb" Inherits="SRRTD_DETAIL"    %>
<%--  CodeFile="SRRTD.aspx.vb" Inherits="SRRTD" meta:resourcekey="PageResource1" UICulture="auto" %>--%>
<%--
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>  --%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <table> 
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Store Group/Store" ID="lbl_store_grp_cd" Width="160px"></asp:Label>
                           
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_store" runat="server" Width="60px"></asp:DropDownList>
                            
                        </td>
                         <td align="right">
                         <asp:Label runat="server" Text="Store" ID="lbl_st" Width="160px" Visible="False"></asp:Label>
                           
                        </td>

                        <td>
                            <asp:DropDownList ID="dw_st" runat="server" Visible="False" Width="60px"></asp:DropDownList>
                            
                        </td>

                    </tr>
                </table>
            </td>
            </tr>
            <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Written /Delivered  " ID="lbl_w_d"  Width="160px" ></asp:Label>
                          
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_w_d" runat="server" Width="60px"></asp:DropDownList>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
            <tr>
               
            <td align="left">
                <table>
                    <tr>
                        
                        <td>
                         <asp:Label runat="server" Text="Inventory/Non Inventory" ID="lbl_inv_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_inv_tp" runat="server" Width="60px"></asp:DropDownList>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
             <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Item       Type" ID="lbl_itm_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_itm_tp" runat="server" Width="60px"></asp:DropDownList>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    
                                   

                    <tr align="right">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_first_page" runat="server" Text="Go First Page"   Visible="true" onclick="click_btn_first_page"/> </asp:Button>
                                         
                                    </td>
                                    <td>
                                         <asp:Button ID="btn_back_to_total" runat="server" Text="Go Back Summary Page"   Visible="true" onclick="click_btn_back_to_total"/> </asp:Button>
                                        <%-- %> <asp:Button ID="btn_clr_screen" runat="server" Text="Clear"  Visible="False" /> </asp:Button> --%>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        
            <tr>
            <td>
               <asp:DataGrid ID="GV_SRR" runat="server"   Width="1850px" BorderColor="Black" 
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="id"
                  Height="16px" AlternatingItemStyle-BackColor="Beige" ReadOnly ="false"  AllowPaging="False"      OnPageIndexChanged="GridView_PageIndexChanged"
                    Caption='  <table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="#99CCFF">
                   <tr>
                       <td align="center" Width="1567px">
                      ALL PRODUCTS

                       </td>
                       <td align="center" Width="283px">
                       OUTLET/CLEARANCE

                       </td>
                   </tr>

                            </table>'

CaptionAlign="Top">
                   
        
<AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
         
                <Columns>
                     
                      
                
                    <asp:ButtonColumn Text="Click_me" Visible="False"></asp:ButtonColumn>
                     
                             
                  <asp:BoundColumn DataField="id" HeaderText="id" Visible="False"></asp:BoundColumn>
                     <asp:BoundColumn DataField="s_f" HeaderText="" Visible="False"></asp:BoundColumn>
             <asp:BoundColumn DataField="store_cd" HeaderText="Store"   ReadOnly="True"  HeaderStyle-Width="10" ItemStyle-Width="10">
<HeaderStyle Width="10px"></HeaderStyle>

<ItemStyle Width="10px"></ItemStyle>
                    </asp:BoundColumn> 
              <asp:BoundColumn DataField="gross_sale" HeaderText="Gross Sales"  ItemStyle-Width="140px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                     
             <asp:BoundColumn DataField="void_sale" HeaderText="Credits/Void Sales"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn> 
             <asp:BoundColumn DataField="net_sale" HeaderText="Net Sales"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn> 
                     <asp:BoundColumn DataField="net_mgn" HeaderText="Net Mgn"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn>
                     
                    <asp:BoundColumn DataField="furn_mix" HeaderText="Furn Mix"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="furn_net_mgn" HeaderText="Furn Mgn"  ItemStyle-Width="121px"   HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn>
                     <asp:BoundColumn DataField="matt_mix" HeaderText="Matt Mix"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="matt_net_mgn" HeaderText="Matt Mgn"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_mix" HeaderText="Appl Mix" ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="appl_net_mgn" HeaderText="Appl Mgn"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_mix" HeaderText="Elec Mix"  ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="elec_net_mgn" HeaderText="Elec Mgn" ItemStyle-Width="121px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn>
             <asp:BoundColumn DataField="id" HeaderText="ID" Visible="False" ItemStyle-Width="140px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}"></asp:BoundColumn>
             <asp:BoundColumn DataField="out_net_sale" HeaderText="Net Sales" ItemStyle-Width="90px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn> 
                     <asp:BoundColumn DataField="out_net_mgn" HeaderText="Net Mgn" ItemStyle-Width="90px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ></asp:BoundColumn>
               <asp:BoundColumn DataField="out_net_qty" HeaderText="Units" ItemStyle-Width="90px"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0}"></asp:BoundColumn>
        </Columns>
       
                   <PagerStyle BorderStyle="Double" Mode="NumericPages" />
       
    </asp:DataGrid>
                </td>
       </tr>
       
    </table>
     <asp:Label runat="server"   ID="lbl_msg"  Width="100%" ForeColor="Red"></asp:Label>
     <asp:Label runat="server"   ID="lbl_warning"  Width="100%" ForeColor="Red"></asp:Label>
  <%-- <dx:aspxlabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:aspxlabel> --%>
    <br />
    <br />
    
    <div runat="server" id="div_version">
          
        <asp:label ID="lbl_versionInfo" runat="server" >
       &nbsp;&nbsp;&nbsp; </asp:label>  
    </div>
</asp:Content>   
