<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OrderSort_Maint.aspx.vb"
    Inherits="OrderSort_Maint" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="frmsubmit" runat="server">
        <br />
        <table border="0" cellpadding="0" cellspacing="0" width="95%" bgcolor="White">
            <tr>
                <td height="22" align="left" valign="middle" bgcolor="white">
                    <div style="padding-left: 13px; padding-top: 2px; text-align: left;">
                        &nbsp;</div>
                </td>
            </tr>
            <tr>
                <td height="100%" align="left" valign="middle">
                    <div style="padding-left: 18px; padding-top: 10px" id="main_body" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="95%">
                            <tr>
                                <td align="left" valign="middle">
                                    <b>
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Order Sort Code Maintenance">
                                        </dx:ASPxLabel>
                                    </b>&nbsp;</td>
                                <td align="right" valign="middle">
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Filter:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="DropDownList1" runat="server" AutoPostBack="True" IncrementalFilteringMode ="StartsWith">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="No Filter" Value="NONE" />
                                                        <dx:ListEditItem Text="Active" Value="Active" />
                                                        <dx:ListEditItem Text="Inactive" Value="Inactive" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;<dx:ASPxButton
                                        ID="Button1" runat="server" Text="Update Order Sort Codes from GERS">
                                    </dx:ASPxButton>
                                    &nbsp; &nbsp;
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <br />
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td align="center" valign="middle" style="height: 266px">
                                    <asp:DataGrid ID="GridView1" runat="server" AutoGenerateColumns="False" Height="200px"
                                        Width="100%" DataKeyField="ORD_SRT_CD">
                                        <Columns>
                                            <asp:BoundColumn DataField="ORD_SRT_CD" HeaderText="Order Sort Code">
                                                <HeaderStyle Height="10px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DES" HeaderText="Description" />
                                            <asp:TemplateColumn HeaderText="Active">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="Update_Active" />
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ACTIVE" Visible="False" />
                                        </Columns>
                                        <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False"></AlternatingItemStyle>
                                        <HeaderStyle Font-Bold="True" ForeColor="Black" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" />
                                        <SelectedItemStyle BackColor="Silver" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" />
                                    </asp:DataGrid>
                                </td>
                                <td align="left" valign="middle" bgcolor="#ffffff" style="height: 266px">
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="height: 19px">
                    <asp:Label ID="lbl_header" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
</asp:Content>
