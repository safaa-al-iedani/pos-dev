Imports System.Data.OracleClient
Imports System.Web.SessionState
Imports System.Web
Imports System.Configuration
Imports System.IO
Imports System.Text
Imports System.Diagnostics
Imports ErrorManager
Imports HBCG_Utils

Partial Class Error_Handling
    Inherits POSBasePage
    Private mstrErrorAsString As String
    Private mobjException As Exception
    Private mobjHTTPSessionState As HttpSessionState
    Private mobjHTTPRequest As HttpRequest

    Private LeonsBiz As LeonsBiz = New LeonsBiz() 'Daniela

    Private Function GetErrorAsString() As String
        Dim objTempException As Exception
        Dim intExceptionLevel As Integer
        Dim strIndent As String
        Dim objSB As New StringBuilder()
        Dim strFormData As String
        objSB.Append("-----------------" & System.DateTime.Now.ToString & "-----------------" & vbCrLf)
        objSB.Append("SessionID:" & Session.SessionID.ToString & vbCrLf)

        strFormData = vbTab & Me.GetStringFromArray(mobjHTTPRequest.Form.AllKeys).Replace(vbCrLf, vbCrLf & vbTab)
        If strFormData.Length > 1 Then '1 because if the form is empty it will just contain the tab prefixed to the line.
            objSB.Append("Form Data:" & vbCrLf)
            'remove the last tab so it doesn't screw up formatting on the line after it.
            objSB.Append(strFormData.Substring(0, strFormData.Length - 1))
        Else
            objSB.Append("Form Data: No Form Data Found")
        End If

        objTempException = Server.GetLastError
        'objTempException = mobjException
        While Not (objTempException Is Nothing)
            'is this the 1st, 2nd, etc exception in the hierarchy
            intExceptionLevel += 1
            objSB.Append(intExceptionLevel & ": Error Description:" & objTempException.Message & vbCrLf)

            objSB.Append(intExceptionLevel & ": Source:" & Replace(objTempException.Source, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Stack Trace:" & Replace(objTempException.StackTrace, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Target Site:" & Replace(objTempException.TargetSite.ToString, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            'get the next exception to log
            objTempException = objTempException.InnerException

        End While

        On Error Resume Next
        intExceptionLevel += 1

        Dim Sname
        Dim x As Double = 0
        objSB.Append("*** ENVIRONMENTAL VARIABLES ***" & vbCrLf)
        objSB.Append(intExceptionLevel & ":    ")
        For Each Sname In Session.Contents
            x = x + 1
            If x > 5 Then
                objSB.Append(vbCrLf)
                objSB.Append(intExceptionLevel & ":    ")
                x = 1
            End If
            objSB.Append(Sname & " - " & Session.Contents(Sname) & "  |  ")
        Next
        objSB.Append(vbCrLf & vbCrLf)

        Return objSB.ToString()

    End Function

    Private Function GetStringFromArray(ByVal pobjArray As String()) As String

        Dim i As Integer
        Dim objSB As New StringBuilder()
        Dim strKey As String

        For i = pobjArray.GetLowerBound(0) To pobjArray.GetUpperBound(0)
            strKey = CType(pobjArray.GetValue(i), String)
            objSB.Append(strKey & " - " & mobjHTTPRequest.Form.Item(strKey) & vbCrLf)
        Next i
        Return objSB.ToString

    End Function

    ReadOnly Property ErrorAsString() As String

        Get
            'if this text hasn't been set yet, then fill it in.
            If mstrErrorAsString Is Nothing Then mstrErrorAsString = Me.GetErrorAsString
            Return mstrErrorAsString
        End Get

    End Property

    Public Sub Catch_errors()

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Catch_errors()

        Dim myError As System.Exception = Server.GetLastError()

        ' Send the webmaster an email with the error details.
        ' You'll obviously need to modify this code to reflect
        ' the appropriate email addresses and check your SMTP
        ' server configuration before it will work.
        'Dim myMessage As New System.Net.Mail.MailMessage( _
        '   "FROM_EMAIL_ADDRESS", "TO_EMAIL_ADDRESS")
        'myMessage.Subject = "15Seconds.com Sample Error"
        'myMessage.Body = Request.Url.ToString() & vbCrLf _
        ' & myError.ToString() & vbCrLf

        'hpl_referral.NavigateUrl = Request.Url.ToString
        Try
            LogToFile(Request.ServerVariables("APPL_PHYSICAL_PATH") & "HBCG_errors.txt")
        Catch
            'Throw
        End Try

        Try
            LogToEventLog()
        Catch
            'Throw
        End Try

        'main_body.InnerHtml = myError.ToString

        'Dim mySmtpClient = New System.Net.Mail.SmtpClient("localhost")
        'mySmtpClient.Send(myMessage)

        ' You should probably transfer them the user to a custom
        ' error page or else they won't see any error message.
        'Response.Redirect("error.aspx")

        ' Since the original page errored out and I didn't transfer
        ' the user on the line above, if I clear the error then
        ' they'll just get a blank page.  So... I'm not going to
        ' clear the error.  This means that the default ASP.NET
        ' error handler will still fire after this runs.
        'Server.ClearError()

        ' Daniela added
        Dim ip As String = ""
        Try
            SessionID.Text = "SessionID: " & Session.SessionID.ToString
            EmpCd.Text = "Employee Code: " & Session("EMP_CD")
            PrevPath.Text = "Error Path: " & Request("aspxerrorpath")
            ClientIP.Text = "IP " + Session("clientip")
            Dim Version_String As String = Get_Version("/HandleCode/HighlandsPOS", "Version")
            ServerIP.Text = "Version: " + Version_String
        Catch
        End Try
        'End Daniela
    End Sub

    Private Sub LogToFile(ByVal pstrFileName As String)

        Dim objSRLogFile As New StreamWriter(pstrFileName, True)

        Try
            objSRLogFile.Write(Me.ErrorAsString)
            objSRLogFile.Close()
        Catch ex As Exception
            'hopefully you also have the log to event log selected and that may have worked. If not, you will probably
            'not know the error information since the attempt to log to file failed. 
            'It may have failed for some reason writing to a file (this could be because of an exclusive lock on the file
            'or security permissions, aspnet_wp may not have permissions to the file, etc etc. 

            'one option here may be shell off an email or net send, depending on your system requirements.
            Throw ex
        End Try


    End Sub

    Private Sub LogToEventLog()

        Dim pstrEventLogName As String = "Application"
        Dim objEventLog As New EventLog()

        'You may have specific a custom event log, so make sure it exists first
        If (Not EventLog.SourceExists(pstrEventLogName)) Then
            EventLog.CreateEventSource(pstrEventLogName, pstrEventLogName)
        End If

        objEventLog.Source = pstrEventLogName

        Try
            objEventLog.WriteEntry(Me.ErrorAsString, EventLogEntryType.Error)
        Catch ex As Exception
            'unfortunately the write to the event log has failed (it may be full)
            Throw ex
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class
