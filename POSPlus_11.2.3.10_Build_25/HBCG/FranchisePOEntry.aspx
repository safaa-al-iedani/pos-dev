﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" CodeFile="FranchisePOEntry.aspx.vb" 
    Inherits="FranchisePOEntry" culture="auto"  meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
       
        function pageLoad(sender, args) {
            

            $('input[type=submit], a, button').click(function (event) {

                if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {

                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {

                        $('#hidDestUrl').val($.trim($(this).attr('href')));

                        event.preventDefault();
                    }
                }
            });
        }

        function disableButton(id) {
            // Daniela B 20140723 prevent double click
            try {
                var a = document.getElementById(id);
                a.style.display = 'none';
            } catch (err) {
                alert('Error in disableButton ' + err.message);
                return false;
            }
            return true;
        }

        function _getKeyCode(evt) {
            return (typeof (evt.keyCode) != "undefined" && evt.keyCode != 0) ?
                evt.keyCode : evt.charCode;
        }

        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "images/minus.png");
        });
            $("[src*=minus]").live("click", function () {
                $(this).attr("src", "images/plus.png");
                $(this).closest("tr").next().remove();
            });

    </script>

  <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
   <ContentTemplate>
    
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

    <table id="FranchisePOEntry" style="position: relative; width: 1000px; left: 0px; top: 0px;" cellpadding="2px">

       <tr >
                <td style="width:17%">  </td>
                <td style="width:35%"> </td>
                <td style="width:15%">   Date:   </td>
                <td style="width:35%">
                     <dx:ASPxDateEdit ID="ASPxDateEdit" runat="server" Enabled="false"  Width="100px" AllowUserInput="false" style="border:solid;border-width:thin;border-color:steelblue" ToolTip="Select a date to filter the delivery dates.">
                    </dx:ASPxDateEdit>
                </td>
           </tr>    
           
          <tr >
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblCreate" Text="Create/Query"></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:RadioButton ID="rdbCreate" runat="server" Text="Create" Checked="true" GroupName="CreateQuery" OnCheckedChanged="rdbCreate_OnCheckedChanged" AutoPostBack="true" /> &nbsp;
                    <asp:RadioButton ID="rdbQuery" runat="server" Text="Query"  GroupName="CreateQuery" OnCheckedChanged="rdbQuery_OnCheckedChanged" AutoPostBack="true"/>
                </td>
                    <td style="width:15%">
                        <asp:Label runat="server" ID="lblFrCustCdCaption" Text="Franchise Customer Code"></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:Label runat="server" ID="lblFrCustCd" Text=""></asp:Label>
                </td>
           </tr>       
           
            <!-- Row 2 --> 
            <tr>
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblFrStoreCd" Text="Franchise Store Code"></asp:Label>
                </td>
                <td style="width:35%">
                      <dx:ASPxComboBox ID="ddl_StoreCds" runat="server" AutoPostBack="True"   CssClass="style5"  IncrementalFilteringMode="StartsWith" Paddings-PaddingLeft="2px"
                           OnSelectedIndexChanged="cbo_StoreCds_SelectedIndexChanged"   AppendDataBoundItems="True" Width="250px" Height="25px" TabIndex="1" style="border:solid;border-width:thin;border-color:steelblue" EnableViewState="true">
                    </dx:ASPxComboBox>
                    <%--<asp:TextBox runat="server" ID="txtFrStoreName"></asp:TextBox>--%>
                </td>
                    <td style="width:17%">
                        <asp:Label runat="server" ID="lblCorpFrStoreNoCaption" Text="Franchise Store #"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCorpFrStoreNo" Text=""></asp:Label>&nbsp;&nbsp;
                   <asp:Label runat="server" ID="lblCorpFrStoreName" Text=""></asp:Label>
                </td>
            
            </tr>       
           
                        
            <!-- Row 3 --> 
            <tr>
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblBuyerInitial" Text="Buyer's Inititals"></asp:Label>
                </td>
                <td style="width:35%">
                     <dx:ASPxComboBox ID="cbo_Buyers" runat="server"  CssClass="style5"  IncrementalFilteringMode="StartsWith" Height="25px"
                         AppendDataBoundItems="True" Width="134px" TabIndex="1" style="border:solid;border-width:thin;border-color:steelblue" EnableViewState="true">
                    </dx:ASPxComboBox>
                </td>
            </tr>      

         
             <!-- Row 4 --> 
            <tr>
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblVendorCd" Text="Vendor Code"></asp:Label>
                </td>
                <td style="width:35%">
                    <dx:ASPxComboBox runat="server" ID="ddlVendorCd"   width="250px" Height="25px" AutoPostBack="true" style="border:solid;border-width:thin;border-color:steelblue" ></dx:ASPxComboBox>
                    <%--<asp:TextBox runat="server" ID="txtVendorName"></asp:TextBox>--%>
                </td>

               <td style="width:17%">
                          <asp:Label runat="server" ID="lblShipVia" Text="Ship Via" ></asp:Label>
                        
                            <asp:TextBox runat="server" ID="txtShipVia" MaxLength="10" Width="100px"></asp:TextBox>
                 </td>
                <td style="width:35%">     
                                <asp:Label runat="server" ID="lblSortCode" Text="P.O. Sort Code"></asp:Label>
                       
                            <asp:TextBox runat="server" ID="txtPOSortCode" Text="CPO" Width="40px" Enabled="false"></asp:TextBox>
               </td>
            </tr>
       
         
             <!-- Row 5 --> 
            <tr>
                 <td style="width:17%">
                    <asp:Label runat="server" ID="lblOrdCorpStoreCd" Text="Ordering from Corporate Store Code"></asp:Label>
                </td>
                <td style="width:15%">
                     <dx:ASPxComboBox runat="server" CssClass="style5" IncrementalFilteringMode="StartsWith"  ID="ddlOrdCorpStoreCd" Width="250px" Height="25px" style="border:solid;border-width:thin;border-color:steelblue"  OnSelectedIndexChanged="ddlOrdCorpStoreCd_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="True"></dx:ASPxComboBox>
                </td>
                <td style="width:17%">
                        <asp:Label runat="server" ID="lblCoprDevZoneCd" Text="Corporate Delivery Zone Code"  ></asp:Label>
                </td>
                <td style="width:35%">
                    <%--<dx:ASPxComboBox runat="server" ID="ddlCorpDelZoneCd" Width="250px"  Height="25px" style="border:solid;border-width:thin;border-color:steelblue"></dx:ASPxComboBox>--%>
                    <asp:DropDownList ID="ddlCorpDelZoneCd" runat="server" AutoPostBack="true" Width="250px"  Height="25px" style="border:solid;border-width:thin;border-color:steelblue">
                        </asp:DropDownList>

                </td>
            </tr>      

        <!-- Row 6 -Show only when Query is selected -->
        <tr>
             <td style="width:17%">
                    <asp:Label runat="server" ID="lblQuery_DocNum" Text="Del Doc #" Visible="false"></asp:Label>
                </td>
                <td style="width:15%">
                     <asp:TextBox runat="server" ID="txtQuery_DocNum" MaxLength="17" Width="200px" Visible="false"></asp:TextBox>
                </td>
                <td style="width:17%">
                        <asp:Label runat="server" ID="lblQuery_ConfirmNum" Text="Confirmation #" Visible="false" ></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:TextBox runat="server" ID="txtQuery_ConfirmationNum" MaxLength="17" Width="200px" Visible="false"></asp:TextBox>
                </td>
        </tr>
        </table>

       <div style="width:950px;width:95%;text-align:center" >
             <dx:ASPxButton ID="btnSearch" runat="server" Text="Search"  OnClick="btnSearch_Click" Visible="false" >
             </dx:ASPxButton>
            <dx:ASPxButton ID="btnClear" runat="server" OnClick="btnClear_Click"  Text="Clear" Visible="false">
           </dx:ASPxButton>
       </div>
    <hr />

    <div id="dvCreate" runat="server">
    <!-- TABLE COMMENTS 2 to 5 -->
    <div id="dvComment" runat="server" style="text-align:left">
        <table cellpadding="2px">
             <%--<tr>
                <td style="width:10%">
                    <asp:Label runat="server" ID="Label11" Text="Comment 1"></asp:Label>
                </td>
                <td style="width:30%">
                     <asp:TextBox runat="server" ID="txtComment1" MaxLength="20" Width="860px"></asp:TextBox>
                </td>
            </tr>--%>

             <tr>
                 <tr">
                <td style="width:10%">
                    <asp:Label ID="lblComment2" runat="server" Text="Comment 2"></asp:Label>
                </td>
                <td style="width:35%">
                     <asp:TextBox ID="txtComment2" runat="server" MaxLength="20" Width="860px"></asp:TextBox>
                </td>
            </tr">
             </tr>
             <tr>
                 <td style="width:10%">
                     <asp:Label ID="lblComment3" runat="server" Text="Comment 3"></asp:Label>
                 </td>
                 <td style="width:35%">
                     <asp:TextBox ID="txtComment3" runat="server" MaxLength="20" Width="860px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width:10%">
                     <asp:Label ID="lblComment4" runat="server" Text="Comment 4"></asp:Label>
                 </td>
                 <td style="width:35%">
                     <asp:TextBox ID="txtComment4" runat="server" MaxLength="20" Width="860px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width:12%">
                     <asp:Label ID="lblComment5" runat="server" Text="Comment 5"></asp:Label>
                 </td>
                 <td style="width:35%">
                     <asp:TextBox ID="txtComment5" runat="server" MaxLength="20" Width="860px"></asp:TextBox>
                 </td>
             </tr>
        </table>
    </div>
    <br />

    <!-- GRIDVIEW FOR ITEMS -->
    <div id="dvGridItems" runat="server">
        <asp:GridView ID="grvItems" runat="server" ShowFooter="True" AutoGenerateColumns="false"
                    CellPadding="4" ForeColor="#333333"   GridLines="None" OnRowDeleting="grvItems_RowDeleting">
            <Columns>
              <asp:BoundField DataField="RowNumber"  HeaderText="LineNo" />
                <asp:TemplateField HeaderText="Corporate SKU#">
                    <ItemTemplate>
                        <asp:TextBox ID="txtCorpSKU" runat="server"  MaxLength="9" Width="100px" Text='<%# Eval("col1")%>' AutoPostBack="true" ToolTip="Item SKU must be an INVENTORY item with Sort Code 'LFL' and Minor Code NOT Start With '97'" OnTextChanged="txtCorpSKU_TextChanged"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="VSN#">
                    <ItemTemplate>
                        <asp:TextBox ID="txtVSN" runat="server" MaxLength="30" Text='<%# Eval("col2")%>'  Enabled="false"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDesc" runat="server"  MaxLength="30" Width="250px" Text='<%# Eval("col3")%>' Enabled="false"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Quantity">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlQty" runat="server" OnSelectedIndexChanged="ddlQty_SelectedIndexChanged" AutoPostBack="true"  >
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="PO Cost" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <asp:TextBox ID="txtPOCost" runat="server" Text='<%# Eval("col5")%>' Width="70px" Enabled="false" CssClass="rightAlign"  ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Extended PO Cost">
                    <ItemTemplate>
                        <asp:TextBox ID="txtExtPOCost" runat="server" Text='<%# Eval("col6")%>'  Width="100px" Enabled="false"  CssClass="rightAlign" ></asp:TextBox> 
                        <%--<asp:TextBox  ID="txtExtPOCost2" runat="server" Enabled="false" BorderStyle="Groove" Width="100px"  CssClass="rightAlign" ></asp:TextBox>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="|">
                    <ItemTemplate>
                        <asp:Label ID="lblLine" runat="server" Text="|" Enabled="false"></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SO Cost">
                    <ItemTemplate>
                        <asp:TextBox ID="txtSOCost" runat="server" Text='<%# Eval("col7")%>' Enabled="false"  BorderStyle="Groove" Width="70px"  CssClass="rightAlign" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Extended SO Cost">
                    <ItemTemplate>
                         <asp:TextBox ID="txtExtSOCost" runat="server" Text='<%# Eval("col8")%>' Enabled="false"  BorderStyle="Groove" Width="100px"  CssClass="rightAlign" ></asp:TextBox>
                    </ItemTemplate>


                    <FooterStyle HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Button ID="ButtonAdd" runat="server" 

                                Text="Add New Row" OnClick="btnAdd_Click" />
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowDeleteButton="True" />
            </Columns>

            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </div>

<%--    <div class="row" style="width:100%">
    <div class="col-md-4" style="border-right-style:solid;width:45%">
      <table id="Table1" width="45%">
          <tr >
                <td style="width:15%">
                    <asp:Label runat="server" ID="Label1" Text="Create/Query"></asp:Label>
                </td>
                <td style="width:30%">
                    <asp:TextBox runat="server" ID="TextBox1"></asp:TextBox>
                </td>
          </tr> 
          <tr>
                <td style="width:15%">
                        <asp:Label runat="server" ID="Label2" Text="Franchise Customer Code"></asp:Label>
                </td>
                <td style="width:30%">
                    <asp:TextBox runat="server" ID="TextBox2"></asp:TextBox>
                </td>
           </tr>       
      </table>
    </div>--%>

    <!-- SHOW COSTS -->
    <table style="width:100%" cellpadding="2px">
        <tr>
            <td style="width:50%;border-right-style:groove">
                <table style="width:100%;" cellpadding="3px">
                    <tr  style="padding-bottom:20px">
                        <td style="width:45%">
                            <asp:Label runat="server" ID="Label1" Text="Sub-total PO Cost*"></asp:Label>
                        </td>
                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtSubTotPOCost"  Width="200px"  CssClass="rightAlign"  Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="border-right-style:groove">
                        <td style="width:45%">
                            <asp:Label runat="server" ID="Label2" Text="PO Tax Code"></asp:Label>
                            <asp:TextBox runat="server" ID="txtPOTaxCode" Width="35px" Enabled="false" ></asp:TextBox>
                            &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<asp:Label runat="server" ID="Label3" Text="PO Tax Amount"></asp:Label>
                        </td>

                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtPOTaxAmount" Width="200px"  Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td style="width:45%">
                            <asp:Label runat="server" ID="Label4" Text="Total PO Cost*" Width="200px"></asp:Label>
                        </td>
                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtPOTotal"  Width="200px" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>

                </table>
            </td>

            <td style="width:50%;padding-left:20px">
                <table style="width:100%;margin-left:30px"  cellpadding="2px">
                     <tr style="padding-bottom:20px;">
                        <td style="width:45%;" >
                            <asp:Label runat="server" ID="Label5" Text="Sub-total SO Cost*"></asp:Label>
                        </td>
                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtSubTotSOCost" Width="200px" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:45%">
                            <asp:Label runat="server" ID="Label6" Text="SO Tax Code"></asp:Label>
                            <asp:TextBox runat="server" ID="txtSOTaxCode"   Width="35px" Enabled="false" ></asp:TextBox>
                            &nbsp; &nbsp;&nbsp; <asp:Label runat="server" ID="Label7" Text="SO Tax Amount" ></asp:Label>
                        </td>

                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtSOTaxAmount" Width="200px" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td style="width:45%">
                            <asp:Label runat="server" ID="Label8" Text="Total SO Cost*"></asp:Label>
                        </td>
                        <td style="width:60%">
                            <asp:TextBox runat="server" ID="txtSOTotal" Width="200px" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>

       <br />
       <table style="width:100%">
        <tr style="text-align:center">
            <td style="column-span:all;text-align:center">
                <span style="font-style:italic;font-weight:bold;color:red">
                    *Franchise costs are only estimates. Costs may change due to up charge and/or fright factor changes.
                </span>
            </td>
        </tr>
    </table>
   
    <hr />

   <!-- APPROVING AREA -->
    <table style="width:100%" cellpadding="2px">
        <tr>
            <td style="width:10%">
                <asp:Label runat="server" ID="Label9" Text="Approval Password:"></asp:Label>
            </td>
            <td style="width:25%">
                <asp:TextBox runat="server" ID="txtApprover_Pswd" MaxLength="17" Width="300px" AutoPostBack="true" OnTextChanged="txtApprover_Pswd_TextChanged" ></asp:TextBox>
            </td>

            <td style="width:45%">
                <asp:Label runat="server" ID="Label10" Text="Approving Associate's Code:"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtApprover_User_Init" Width="360px" Enabled="false" ></asp:TextBox>
            </td>
           <%-- <td style="width:25%">
                <asp:TextBox runat="server" ID="txtApprover_User_Init" Width="300px"></asp:TextBox>
            </td>--%>
        </tr>

         <tr>
            <td style="width:10%">
               
            </td>
            <td style="width:25%;text-align:right;padding-right:50px">
                <asp:Button ID="btnClearAll" runat="server" Text="CLEAR RECORD TO PROCEED" Click="btnClearAll_Click" Enabled="true" CausesValidation="False" />&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnConfirmASP" runat="server" Text="COMMIT" Click="btnConfirm_Click" Enabled="true" CausesValidation="False" />
               
                <%--<asp:Button ID="btnConfirmASP" runat="server" Text="COMMIT" BorderWidth="5px" BorderStyle="Solid" BorderColor="RoyalBlue" Click="btnConfirm_Click" Enabled="true" />--%>
                <%--<dx:ASPxButton  ID="btnConfirm" runat="server" Text="COMMIT" Click="btnConfirm_Click" Enabled="true" ></dx:ASPxButton>--%>
             </td>

            <td style="width:45%">
                <asp:Label runat="server" ID="lblConfirm" Text="Confirmation #"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox runat="server" ID="txtConfirmationNum" Width="435px" Enabled="false" ></asp:TextBox>
            </td>
           <%-- <td style="width:35%">
                <asp:TextBox runat="server" ID="txtConfirmationNum" Width="300px"></asp:TextBox>
            </td>--%>
        </tr>
    </table>

    </div>

     <div id="dvQuery" runat="server" style="width:98%">
      
         <!-- Gridview Expand -->
         <asp:GridView ID="gvFRPO" runat="server" AutoGenerateColumns="false" CssClass="Grid"
            DataKeyNames="DEL_DOC_NUM"  OnRowDataBound="OnRowDataBound" PageSize="10"  AllowSorting="true" >
            <Columns>
               <%-- <asp:TemplateField>
                    <ItemTemplate>--%>
                       <%-- <img alt = "" style="cursor: pointer" src="images/plus.png" />
                        <asp:Panel ID="pnlFRPO_Ln" runat="server" Style="display: none">
                            <asp:GridView ID="gvFRPO_Ln" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid">
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="150px" DataField="DEL_DOC_NUM" HeaderText="DelDocNum" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="ITM_CD" HeaderText="Item Code" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="QTY" HeaderText="Qty" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="PO_CST" HeaderText="PO Cost" />
                                     <asp:BoundField ItemStyle-Width="150px" DataField="PO_EXT_CST" HeaderText="PO Ext. Cost" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="SO_CST" HeaderText="So Cost" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="SO_EXT_CST" HeaderText="SO Ext. Cost" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
                 --%>
                <asp:BoundField ItemStyle-Width="150px" DataField="DEL_DOC_NUM" HeaderText="DelDocNum" />
                <asp:BoundField ItemStyle-Width="150px" DataField="PO_CD" HeaderText="Confirmation #" />
                <asp:BoundField ItemStyle-Width="150px" DataField="CUST_CD" HeaderText="Customer CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="STORE_CD" HeaderText="Dest Store CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="STORE_NUM" HeaderText="Store #" />
                <asp:BoundField ItemStyle-Width="150px" DataField="BUYER_INIT" HeaderText="Buyer Init" />
                <asp:BoundField ItemStyle-Width="150px" DataField="VE_CD" HeaderText="Vendor CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="SHIP_VIA" HeaderText="Ship Via" />
                <asp:BoundField ItemStyle-Width="150px" DataField="PO_SRT_CD" HeaderText="PO Sort CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="CORP_STORE_CD" HeaderText="Corp Store CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="CORP_ZONE_CD" HeaderText="Corp Zone CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="ENTER_DATE" HeaderText="Created Date" HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
            </Columns>
        </asp:GridView>

           <table width="98%">
            <tr>
                <td align="center">
                    <table width="35%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>

                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>


     </div>
  
      </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>