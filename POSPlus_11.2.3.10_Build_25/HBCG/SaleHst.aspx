<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SaleHst.aspx.vb" Inherits="SaleHst" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />

    <table>
        <tr>
           <td>
               <asp:Label ID="lbl_del" runat="server" Text="Sales_order"></asp:Label>
            </td>  
            <td>
               <dx:ASPxTextBox ID="txt_corp_name" runat="server" Width="170px"  ></dx:ASPxTextBox>
            </td> 
            
            <td>
                      <dx:ASPxLabel ID="ASPxLabel2" runat="server" width="100px">
                      </dx:ASPxLabel>
            </td>
             <td>
           
                    <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="Sku">
                    </dx:ASPxLabel>
             </td>
             <td>
                           
                  <dx:ASPxTextBox ID="txt_acct_balance" runat="server" Width="102px"  ></dx:ASPxTextBox>
             </td>   
                  
         </tr>

        
    </table>



    <table>

                       

                           
                      <tr>
                          <td>
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" width="560px">
                            </dx:ASPxLabel>
                          </td>
                        <td>
                            <dx:ASPxButton ID="btn_Submit" runat="server" visible="true" onclick="btn_submit_click" Text="Search">
                            </dx:ASPxButton>
                        </td>
                     
                        <td>
                            <dx:ASPxButton ID="btn_Exit" runat="server"   onclick="btn_Exit_Click" Text="Back to CPHQ">
                            </dx:ASPxButton>
                       </td>

                      <td align="right">
                
                         <dx:ASPxButton ID="btn_Clear" runat="server" onclick="btn_Clear_Click" Text="Clear">
                         </dx:ASPxButton>
             
                      </td>
                     </tr>
         </table>

                            <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3" DataKeyField="sales_order" Height="16px"
                                OnSortCommand="MyDataGrid_Sort" PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages"
                                Width="99%" Style="position: relative; top: 1px; left: 1px;">
                                <Columns>
                                    <%-- 
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label511 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" visible="false" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                     --%>

                                    <asp:BoundColumn DataField="SALES_ORDER" HeaderText="<%$ Resources:LibResources, Label511 %>"     SortExpression="SALES_ORDER asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>

                                     <asp:BoundColumn DataField="REFERENCE" HeaderText="Reference" SortExpression="REFERENCE asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ORD_TP" HeaderText="<%$ Resources:LibResources, Label944 %>"  SortExpression="ORD_TP asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FINAL_DATE" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label202 %>"
                                        SortExpression="FINAL_DATE asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                              
                                    <asp:BoundColumn DataField="ITM_CD" HeaderText="Sku" SortExpression="ITM_CD asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="QTY" HeaderText="<%$ Resources:LibResources, Label447 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="UNIT_PRC" HeaderText="<%$ Resources:LibResources, Label427 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    
                                    <asp:BoundColumn DataField="OUT_ASIS" HeaderText="<%$ Resources:LibResources, Label750 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="WTY" HeaderText="Wty"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>

                                   

                                    <asp:BoundColumn DataField="SALES_ORDER" HeaderText="<%$ Resources:LibResources, Label511 %>"   visible="false"  SortExpression="SALES_ORDER asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>

                                        

                                      <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal ID="lbl_cmnt"  text="Comments" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                       
                                            <asp:HyperLink ID="HyperLink2" runat="server"  ImageUrl="~/images/icons/Edit24.gif" Height="8" Width="20"  
                                                 VerticalAlign="Top"   ></asp:HyperLink>
                                                 <asp:label ID="lbl_v" runat="server" text=""  Height="8" Width="20"  ></asp:label>   
                                
                                        </ItemTemplate>
                                        
                                    </asp:TemplateColumn>
 
                                              <%--         
                                   <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal ID="Literal1" runat="server"   Visible="false"   Text="<%$ Resources:LibResources, Label44 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_balance" runat="server" Visible="false"   CssClass="style5" Text="0"></asp:Label><br />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                      --%> 
                                           
                                      
                                  <%--  <asp:BoundColumn DataField="sales_order" HeaderText="" Visible="false"></asp:BoundColumn> --%>

                                    
                                    
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="Black" CssClass="style5" />
                            </asp:DataGrid>
              
    <br />
     
    <br />
    <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="ASPxLabel" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel19" runat="server" >
    </dx:ASPxLabel>
</asp:Content>
