
Partial Class Reporting
    Inherits POSBasePage

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Alice updated on Oct 3 for request 3759
        If isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag") = "Y" Then

            ASPxHyperLink10.Enabled = True
            ASPxHyperLink9.Enabled = True
            ASPxHyperLink8.Enabled = True
            ASPxHyperLink20.Enabled = True

            ASPxHyperLink6.Enabled = True
            ASPxHyperLink7.Enabled = True
            ASPxHyperLink12.Enabled = True

            ASPxHyperLink5.Enabled = True
            ASPxHyperLink4.Enabled = True
            ASPxHyperLink1.Enabled = True

            ASPxHyperLink2.Enabled = True
            ASPxHyperLink3.Enabled = True
            ASPxHyperLink11.Enabled = True
            ASPxHyperLink13.Enabled = True

            ASPxHyperLink14.Enabled = True
            ASPxHyperLink15.Enabled = True
            ASPxHyperLink16.Enabled = True
            ASPxHyperLink17.Enabled = True
            ASPxHyperLink18.Enabled = True

        End If

        If isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag") = "N" Then

            ASPxHyperLink10.Enabled = False
            ASPxHyperLink9.Enabled = False
            ASPxHyperLink8.Enabled = False
            ASPxHyperLink20.Enabled = False

            ASPxHyperLink6.Enabled = False
            ASPxHyperLink7.Enabled = False
            ASPxHyperLink12.Enabled = False

            ASPxHyperLink5.Enabled = False
            ASPxHyperLink4.Enabled = False
            ASPxHyperLink1.Enabled = False

            ASPxHyperLink2.Enabled = False
            ASPxHyperLink3.Enabled = False
            ASPxHyperLink11.Enabled = False
            ASPxHyperLink13.Enabled = False

            ASPxHyperLink14.Enabled = False
            ASPxHyperLink15.Enabled = False
            ASPxHyperLink16.Enabled = False
            ASPxHyperLink17.Enabled = False
            ASPxHyperLink18.Enabled = False

        End If


        Dim str_previousPageURL As String = Convert.ToString(Request.UrlReferrer)

        If InStr(str_previousPageURL, "CashDrawerBalancing.aspx") > 0 Then  'lucy to prevent user use report, 23-Jul-15
            Response.Redirect("CashDrawerBalancing.aspx")

        End If

        If (Not IsPostBack) Then
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim previousPageURL As String = Convert.ToString(Request.UrlReferrer)
                If Not String.IsNullOrWhiteSpace(previousPageURL) Then
                    ' For stale data issue
                    ' Need to perform the action when user navigates back to Home page from SOM+, SORW+ page            
                    Dim pageName As String = String.Empty
                    pageName = System.IO.Path.GetFileName(previousPageURL)

                    Dim accessInfo As New AccessControlBiz()
                    accessInfo.DeleteRecordsforPage(pageName)
                End If
            End If
        End If



    End Sub
End Class
