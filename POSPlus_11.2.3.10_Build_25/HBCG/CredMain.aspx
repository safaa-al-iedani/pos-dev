﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CredMain.aspx.vb"
    MasterPageFile="MasterPages/NoWizard2.master"  Inherits="CredMain" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" BorderStyle="none" runat="Server">
        <div runat="server">
        
            <table  width="100%" class="style5">
                <tr>
                    <td align="center" > 
                    <asp:Label ID="ASPxLabel1" runat="server" Text="CRED Tracking Screen" Font-Bold="True" ></asp:Label>
                    </td>
                </tr>
                 <%-- ===============MESSAGES =========lbl_msg =========================   --%> 
            </table>
            <br />
           
            <table>
                  <tr>
                    <td >
                        <asp:TextBox ID="lbl_msg" BorderColor="Brown"  BackColor="WhiteSmoke" forecolor="Blue" font-size="Small" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" ></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
               <%-- =============BUYER, EVENT, NEW EVENT ind, EXCLU IND =========================   --%> 
           <asp:Panel ID="pan" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
              <table class="style5">
                <tr>
                <td>
                <%-- buyer --%>
                  <asp:Label ID="Label1" runat="server" Font-Bold="true" font-size="XX-Small" Text="Buyer "></asp:Label> 
                    <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true"  autopostback="true" OnSelectedIndexChanged="BuyerSelected" ID="buyer_drp_list">                   
                  </asp:dropdownlist>
                </td >

                <td> 
                     &nbsp&nbsp&nbsp&nbsp
                   <asp:Label font-bold="true" ID="lbl_ve_cd" runat="server" Text="Vendor "></asp:Label>  
                   <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" width="100px" autopostback="true" OnSelectedIndexChanged="VeCdSelected" ID="ve_cd_drp_list">                   
                     </asp:dropdownlist>
               </td>

                         <%-- R3053 - add store_grp_cd - sabrina --%>
               <td>
                       &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                   <asp:Label ID="Label10" font-bold="true" runat="server" Text="Store Group "></asp:Label>
                     <asp:DropDownList runat="server" AutoPostBack="True" OnSelectedIndexChanged="StoreGrpChanged" ID="store_grp_list">  
                   </asp:DropDownList>
               </td>

                </tr>
            </table>
            <br />
            
           <%-- ========================== EVENT code and new event code ==================   --%> 
            <table>
                <tr align="char" valign="top">
                   <%-- new event indicator --%>
                   <td >
                        <asp:Label ID="lbl_new_event" font-bold="true"  runat="server"  ForeColor="RED"  Text="New"></asp:Label>
                        <asp:CheckBox ID="new_event_ind"  AutoPostBack="true" runat="server" />
                       </td>
                    <%-- event dropd list --%>
                <td>
                    <asp:Label ID="lbl_event1" runat="server" Font-Bold="true"  font-size="XX-Small" Text="Event CD"></asp:Label> 
                    </td>
                <td>
                    <asp:dropdownlist runat="server" width="160px" Style="position: relative" AppendDataBoundItems="true" autopostback="true" OnSelectedIndexChanged="EventSelected" ID="event_drp_list">                   
                    </asp:dropdownlist>
                </td>
                    <%-- new event --%>
                     <td>
                     <asp:TextBox ID="new_event_cd" Enabled="false" style='text-transform:uppercase' width="160px" Visible="false" runat="server" AutoPostBack="true"  ></asp:TextBox>
                           
                     </td>
                    
                     <%-- exclude franchise 'R2913 - sabrina --%>
               <td>
                       &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                   <asp:Label ID="Label2" font-bold="true" runat="server" Text="Exclude Franchise "></asp:Label>
                     <asp:DropDownList runat="server" AutoPostBack="True" OnSelectedIndexChanged="excludeIndChanged" ID="exclu_fran_list">  
                   </asp:DropDownList>
               </td>

                    <%-- event status --%>
                    <td>
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                         <asp:Label ID="Label4" font-bold="true" runat="server"  font-size="XX-Small" Text="Event Status"></asp:Label>  <asp:dropdownlist runat="server"  Style="position: relative" AppendDataBoundItems="true" width="90px" autopostback="true" OnSelectedIndexChanged="STATSelected" ID="stat_drp_list">                   
                    </asp:dropdownlist>
                </td>

                 
                    </tr>
            </table>
            
             <%-- VCM button --%>
            <table >
                <tr>
                    <td>
                        <asp:Button ID="vcm_btn" runat="server" width="180px" OnClick="vcm_btn_click" font-bold="true" ForeColor="Red" Text=" VCM " />
                    </td>
                </tr>
            </table>
                </asp:Panel>
            <br /> 
           <%-- ========================== EVENT DESCRIPTION ==================   --%> 
             <asp:Panel ID="Panel1" runat="server" BorderStyle="solid"  BorderWidth="1px" >  
            <%--<table>
                <tr align="char" >
                    <td>
                        <asp:Label ID="Label7" runat="server" font-size="XX-Small" Text="Description"></asp:Label> 
                        <asp:TextBox ID="event_des" Width="760px" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table> 
            <br />--%>
              

             <%-- ===========CREDIT TP, VCM sort, store, status =======================================   --%> 
            <table width="100%">
            <tr align="char" valign="top" >
               
               
                <td> <asp:Label ID="Label6"  runat="server"   font-size="XX-Small" Text="Cred TP"></asp:Label>  <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" width="100px" autopostback="true" OnSelectedIndexChanged="CreditTPSelected" ID="credit_tp_drp_list">                   
                     </asp:dropdownlist>
                 </td> 
                 <td> <asp:Label ID="Label8" runat="server"   font-size="XX-Small" Text="VCM Sort"></asp:Label>  <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" width="100px" autopostback="true" OnSelectedIndexChanged="VCMTPSelected" ID="vc_tp_drp_list">                   
                    </asp:dropdownlist>
                 </td>
                  <td> <asp:Label ID="lbl_str" runat="server"    font-size="XX-Small" Text="Store"></asp:Label>  <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" width="90px" autopostback="true" OnSelectedIndexChanged="STRSelected" ID="str_drp_list">                   
                    </asp:dropdownlist> 
                 </td>
                
              </tr>
                </table>
            <br />
        <%-- ========================== COMMENT =============================   --%> 
             <table width="100%"> 
            <tr align="char" valign="top">
                <td> <asp:Label ID="Label3" runat="server"  Text="Comments"></asp:Label>
                     <asp:TextBox ID="cmnt"  Width="760px" runat="server"></asp:TextBox>
                </td>
            </tr>
            </table>
            <br />
        <%-- ========================== DISCOUNT =============================   --%> 
            <table width="100%" >
                 <tr align="char" valign="top">
                 <td> <asp:Label ID="lbl_disc_cd"  runat="server" Text="Discount Code"></asp:Label> 
                  <asp:TextBox  ID="disc_cd" width="90px" style='text-transform:uppercase' runat="server"></asp:TextBox> </td>
                 
                 <td> <asp:Label ID="lbl_disc_pct" runat="server" Text="Claim % of Amount"></asp:Label>  
                    <asp:TextBox ID="disc_pct" width="90px" runat="server"></asp:TextBox> </td>
                 <td> <asp:Label ID="lbl_disc_amt" runat="server" Text="Claim $ of Amount"></asp:Label>  
                   <asp:TextBox  ID="disc_amt"  width="90px" runat="server"></asp:TextBox> </td>
            </tr>   
            </table> 
        <%-- ========================== START AND END DATE OnSelectionChanged="EndDateChanged" =================   --%> 
            <table width="100%">
                <tr align="char" valign="top">
            <td>
            <asp:Label ID="Label5" runat="server" style="text-align: start" Text="START DT" ></asp:Label>
               <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="start_dt" DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
             </td>
            <td>
            <asp:Label ID="lbl_end_dt" runat="server" style="text-align: start" Text="END DT" ></asp:Label>
            <asp:Calendar Font-Size="XX-Small" hieght="1px" Width="1px" 
              DisplayDateFormat="MM/dd/yyyy" ID="end_dt" ForeColor="Black" runat="server">
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>

            </asp:Calendar>
            </td>

          </tr>
          </table>
                </asp:Panel>
          <br />
        <%-- ========================== BUTTONS =============================   --%> 
           
          <table width="100%">
            <tr> 
            <td> <asp:Button ID="search_btn" width="180px" OnClick="search_btn_click" font-bold="true" ForeColor="Red"  runat="server" Text="Search" /> </td>
            <td> <asp:Button ID="save_btn" width="180px" onclick="save_btn_click"  font-bold="true" ForeColor="Red"  runat="server" Text="SAVE" /> </td>
            <td> <asp:Button ID="clear_btn"  width="180px" onclick="clear_btn_click"  font-bold="true" ForeColor="Red"  runat="server" Text="Clear" />
            </td>
                 <td> <asp:Button ID="credsku_btn"  width="180px" onclick="credsku_btn_click"  font-bold="true" ForeColor="Red"  runat="server" Text="Cred Item" />
            </td>
            </tr> 
                
           </table>

        <%-- ===============CREDSKU DETAIL WITH EDIT ===================================   --%>
        <%-- ===========================================================================   --%>  
          <dxpc:ASPxPopupControl ID="ASPxPopupCREDSKU1" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="CREDSKU DETAIL" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
        
          <asp:Label ID="Label7" runat="server" Text="Event Code"></asp:Label>
          <asp:TextBox ID="credsku_event_cd" Enabled="false" ReadOnly="true" runat="server"></asp:TextBox>
             <asp:TextBox ID="lbl_msg2" BorderColor="Brown"  BackColor="WhiteSmoke" forecolor="Blue" font-size="Small" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True"></asp:TextBox>
          <br />
          <br />
          <dx:ASPxGridView ID="GridView1" runat="server" keyfieldname="ITM_CD" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView1" oncelleditorinitialize="GridView1_CellEditorInitialize"  
            OnRowUpdating="ASPxGridView1_RowUpdating" OnRowInserting="ASPxGridView1_RowInserting"  
            OnRowValidating="rowvalidation">
         <SettingsEditing mode="EditForm" NewItemRowPosition="top" />
          
         <Columns>
            <dx:GridViewDataTextColumn Caption="ITM" FieldName="ITM_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  Width="100" MaxLength="10">
                    <ValidationSettings>
                        <RequiredField IsRequired="True"  ErrorText="Required Field"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="Description"  FieldName="ITM_DES" ReadOnly="true" VisibleIndex="2">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="25">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="VSN" FieldName="VSN"  ReadOnly="true" VisibleIndex="3">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="25">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="QTY" FieldName="QTY" ReadOnly="false" VisibleIndex="4">
                <PropertiesTextEdit Width="50"  MaxLength="5">
                    <ValidationSettings>
                       <%--  "^(?=.*\d)[\d ]+$"<RequiredField IsRequired="True" ErrorText="Required Field"/>  --%>
                         <RegularExpression ValidationExpression="^[0-9]{0,5}$" ErrorText="Invalid Value"  />
                    </ValidationSettings>
                </PropertiesTextEdit> 
            </dx:GridViewDataTextColumn>
            <%-- "^-?\d*\.{0,1}\d+$" --%>
            <dx:GridViewDataTextColumn Caption="CREDIT" FieldName="AMT" VisibleIndex="5">
                <PropertiesTextEdit Width="90" MaxLength="9">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Required Field"/>
                         <%--<RegularExpression ValidationExpression="^[0-9]{0,9}$" ErrorText="Invalid Value"  />--%>
                        <RegularExpression ValidationExpression="^-?\d*\.{0,1}\d+$" ErrorText="Invalid Value"  />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="STAT" FieldName="ITM_STAT" VisibleIndex="6">
                <PropertiesTextEdit Width="20" MaxLength="1">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewCommandColumn Shownewbuttoninheader="true" ShowEditButton="true" VisibleIndex="14"/>
          
        </Columns>

        <SettingsBehavior  AllowSort="False" />
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  /> 
       
    </dx:ASPxGridView>
            <%--  --%>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>

     <%-- =========== CREDSKU DETAIL BROWSE ONLY =======================================   --%> 
          <dxpc:ASPxPopupControl ID="ASPxPopupCREDSKU2" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="CREDSKU DETAIL-" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" DefaultButton="btnSubmit" runat="server">
             <%--  --%>
          <asp:Label ID="Label9" runat="server" Text="Event Code"></asp:Label>
          <asp:TextBox ID="credsku_event_cd9" Enabled="false" ReadOnly="true" runat="server"></asp:TextBox>
          <br />
          <br />
          <dx:ASPxGridView ID="GridView9" runat="server" keyfieldname="ITM_CD" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView9">
                  <SettingsCommandButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption="ITM" FieldName="ITM_CD" ReadOnly="true" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  Width="100" MaxLength="10">
                      </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description"  FieldName="ITM_DES" ReadOnly="true" VisibleIndex="2">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="25">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="VSN" FieldName="VSN"  ReadOnly="true" VisibleIndex="3">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="25">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="QTY" FieldName="QTY" ReadOnly="true" VisibleIndex="4">
                <PropertiesTextEdit Width="50" MaxLength="5">
                                    </PropertiesTextEdit> 
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="CREDIT" ReadOnly="true" FieldName="AMT" VisibleIndex="5">
                <PropertiesTextEdit Width="90" MaxLength="9">
                                    </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="STAT" FieldName="ITM_STAT" VisibleIndex="6">
                <PropertiesTextEdit Width="20" MaxLength="1">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
        </Columns>

        <SettingsBehavior  AllowSort="False" />
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
        <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please press 'UPDATE' to confirm and Delete this record" />
    </dx:ASPxGridView>
    <%--  --%>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
    </div>
    <%-- ====================VCM POPUP screen =====================================   --%> 
    <%-- ------------- --%> 
            
    <dxpc:ASPxPopupControl ID="ASPxPopupvcmdetail" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="VCM DETAIL" Height="64px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="494px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl6" DefaultButton="btnSubmit" runat="server">
        <dx:ASPxGridView ID="GridView2" runat="server" keyfieldname="EVENT_CD" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView2" >
         <Columns>
            <dx:GridViewDataTextColumn Caption="Company" FieldName="CO_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="3"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Event Cd." FieldName="EVENT_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="12"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="VCM" FieldName="VC_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="13"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn> 
             <dx:GridViewDataTextColumn Caption="Processed Ind." FieldName="PROCESSED_IND" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="1"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Caption="Process Date" FieldName="PROC_DT" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="15"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Amount" FieldName="AMOUNT" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  MaxLength="10"> </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

      </Columns>
     </dx:ASPxGridView>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    
            <%--  --%>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
            
    
