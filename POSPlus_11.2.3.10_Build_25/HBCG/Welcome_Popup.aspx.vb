Imports System.Data.OleDb

Partial Class Welcome_Popup
    Inherits POSBasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String
        Dim HTML As String
        Dim Proceed As Boolean
        Dim conn2 As OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleClient.OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleClient.OracleDataReader
        Dim x, y, z As Integer

        Proceed = False
        x = 0
        y = 0
        z = 0

        HTML = ""

        sql = "SELECT * FROM RELATIONSHIP WHERE SLSP1='" & UCase(Session("EMP_CD")) & "' AND FOLLOW_UP_DT=TO_DATE('" & FormatDateTime(Today.Date.ToString, 2) & "','mm/dd/RRRR') ORDER BY APPT_TIME"
        'Open Connection 
        conn2.Open()

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If Not MyDataReader2.HasRows Then
                sql = "insert into welcome_view (EMP_CD, DT, VIEWED) VALUES('" & UCase(Session("EMP_CD")) & "',TO_DATE('" & FormatDateTime(Today.Date.ToString, 2) & "','mm/dd/RRRR'),1)"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                objsql2.ExecuteNonQuery()
                main_body.InnerHtml = "<br />No follow up transaction found for today.  This screen will not display again."
            Else
                Proceed = True
                HTML = "<table width=""100%"" cellpadding=""0"" cellspacing=""0"">"
                HTML = HTML & "<tr bgcolor=""lightgrey"">"
                HTML = HTML & "<td>Relationship</td>"
                HTML = HTML & "<td>Customer Code</td>"
                HTML = HTML & "<td>Total</td>"
                HTML = HTML & "<td>Follow Up Date</td>"
                HTML = HTML & "<td>Time</td>"
                HTML = HTML & "<td>Type</td>"
                HTML = HTML & "</tr>"
            End If
            Do While MyDataReader2.Read()
                HTML = HTML & "<tr>"
                HTML = HTML & "<td><a href=""#"" onClick=""window.opener.parent.location='Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & MyDataReader2.Item("REL_NO") & "'"">" & MyDataReader2.Item("REL_NO") & "</a></td>"
                HTML = HTML & "<td>" & MyDataReader2.Item("CUST_CD").ToString & "</td>"
                If IsNumeric(MyDataReader2.Item("PROSPECT_TOTAL").ToString) Then
                    HTML = HTML & "<td>" & FormatCurrency(MyDataReader2.Item("PROSPECT_TOTAL").ToString, 2) & "</td>"
                Else
                    HTML = HTML & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                HTML = HTML & "<td>" & FormatDateTime(MyDataReader2.Item("FOLLOW_UP_DT").ToString, 2) & "</td>"
                If MyDataReader2.Item("APPT_TIME").ToString & "" <> "" Then
                    HTML = HTML & "<td>" & FormatDateTime(MyDataReader2.Item("APPT_TIME").ToString, DateFormat.LongTime) & "</td>"
                Else
                    HTML = HTML & "<td>&nbsp;</td>"
                End If
                HTML = HTML & "<td>" & MyDataReader2.Item("APPT_TYPE").ToString & "</td>"
                HTML = HTML & "</tr>"
                x = x + 1
                If MyDataReader2.Item("APPT_TIME").ToString & "" <> "" Then
                    y = y + 1
                End If
                If IsNumeric(MyDataReader2.Item("PROSPECT_TOTAL").ToString) Then
                    z = z + CDbl(MyDataReader2.Item("PROSPECT_TOTAL").ToString)
                End If
            Loop
            If Proceed = True Then
                HTML = HTML & "</table>"
                If x > 0 Then
                    'Daniela french
                    'HTML = HTML & "<br /><b><u>Daily Summary:</u></b><br />"
                    'HTML = HTML & "Prospect(s): &nbsp; " & x & "<br />"
                    'HTML = HTML & "Appointment(s): &nbsp; " & y & "<br />"
                    'HTML = HTML & "Potential Revenue: &nbsp; " & FormatCurrency(z, 2) & "<br />"
                    HTML = HTML & "<br /><b><u>" & Resources.LibResources.Label780 & ":</u></b><br />"
                    HTML = HTML & Resources.LibResources.Label781 & ": &nbsp; " & x & "<br />"
                    HTML = HTML & Resources.LibResources.Label782 & ": &nbsp; " & y & "<br />"
                    HTML = HTML & Resources.LibResources.Label783 & ": &nbsp; " & FormatCurrency(z, 2) & "<br />"
                End If
                main_body.InnerHtml = HTML
            End If
            'Close Connection 
            MyDataReader2.Close()
            conn2.Close()
        Catch ex As Exception
            'TODO HANDLE EX 
        End Try
    End Sub

    Protected Sub chk_keep_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_keep.CheckedChanged
        Dim sql As String
        Dim conn2 As OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleClient.OracleCommand = DisposablesManager.BuildOracleCommand


        sql = "UPDATE WELCOME_VIEW SET VIEWED=1 WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "' AND DT=TO_DATE('" & FormatDateTime(Today.Date.ToString, 2) & "','mm/dd/RRRR')"
        'Open Connection 
        conn2.Open()
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        main_body.InnerHtml = "<br />Welcome screen will not be displayed again today."
        conn2.Close()
    End Sub
End Class
