Imports System.Data
Imports System.Data.OracleClient
Imports DevExpress.XtraCharts

Partial Class salestrends
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If Not IsPostBack Then
            store_cd_populate()
            cbo_store.SelectedValue = Session("HOME_STORE_CD")
            emp_cd_populate()
            cbo_slsp.SelectedValue = Session("EMP_CD")
            If Not Is_Manager() Then
                cbo_store.Enabled = False
                cbo_slsp.Enabled = False
            End If
            If Not String.IsNullOrEmpty(cbo_slsp.SelectedValue.ToString) Then
                populate_sales_trends()
                ' populate_relationship_trends()
            End If
        End If

    End Sub

    Public Function Is_Manager() As Boolean

        Is_Manager = False
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                             ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.EMP_CD='" & UCase(Session("EMP_CD")) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='SLM'"
        SQL = UCase(SQL)

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Is_Manager = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Sub populate_sales_trends()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim ds4 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim MyDatareader As OracleDataReader

        ds = New DataSet
        ds4 = New DataSet

        conn.Open()

        sql = "SELECT REL_NO, NVL(CORP_NAME,LNAME) AS LNAME, HPHONE, PROSPECT_TOTAL, APPT_TIME, APPT_TYPE, EMAIL_ADDR, CONVERSION_DT FROM RELATIONSHIP WHERE SLSP1='" & UCase(Session("EMP_CD")) & "' AND FOLLOW_UP_DT=TO_DATE('" & FormatDateTime(Today.Date.ToString, 2) & "','mm/dd/RRRR') ORDER BY APPT_TIME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds4)

        ASPxGridView1.DataSource = ds4
        ASPxGridView1.DataBind()

        ds4.Clear()
        ds4.Dispose()

        sql = "SELECT Count(REL_NO) AS REL_NO, Sum(RELATIONSHIP.PROSPECT_TOTAL) AS PROSPECT_TOTAL, TO_CHAR(FOLLOW_UP_DT,'DAY') As DAY_TEST, TO_CHAR(FOLLOW_UP_DT,'fmD') As DAY_NUM, 'Current Week' AS WEEK  "
        sql = sql & "FROM RELATIONSHIP "
        sql = sql & "WHERE SLSP1 = '" & cbo_slsp.SelectedValue & "' "
        sql = sql & "AND CONVERSION_DT IS NULL "
        sql = sql & "AND FOLLOW_UP_DT >= TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday) & "','mm/dd/RRRR') "
        sql = sql & "AND FOLLOW_UP_DT <= TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday).AddDays(6) & "','mm/dd/RRRR') "
        sql = sql & "GROUP BY TO_CHAR(FOLLOW_UP_DT,'DAY'),TO_CHAR(FOLLOW_UP_DT,'fmD') "
        sql = sql & "ORDER BY 4"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        Dim i As Integer = 1
        Dim Missing_String As String = ""
        Dim Curr_day As String = ""

        For Each dr In ds.Tables(0).Rows
            If Curr_day <> dr("DAY_NUM").ToString Then
                If CDbl(dr("DAY_NUM").ToString) <> i Then
                    Do While i < CDbl(dr("DAY_NUM").ToString)
                        Missing_String = Missing_String & i & ","
                        i = i + 1
                    Loop
                End If
            End If
            Curr_day = dr("DAY_NUM").ToString
            i = i + 1
        Next
        If i <= 7 Then
            Do While i <= 7
                Missing_String = Missing_String & i & ","
                i = i + 1
            Loop
        End If

        Dim DayArray As Array

        DayArray = Split(Missing_String, ",")
        Dim x As Integer

        For x = LBound(DayArray) To UBound(DayArray)
            If Not String.IsNullOrEmpty(DayArray(x)) Then
                dr = dt.NewRow
                dr("DAY_NUM") = DayArray(x)
                Select Case DayArray(x)
                    Case 1
                        dr("DAY_TEST") = "SUNDAY"
                    Case 2
                        dr("DAY_TEST") = "MONDAY"
                    Case 3
                        dr("DAY_TEST") = "TUESDAY"
                    Case 4
                        dr("DAY_TEST") = "WEDNESDAY"
                    Case 5
                        dr("DAY_TEST") = "THURSDAY"
                    Case 6
                        dr("DAY_TEST") = "FRIDAY"
                    Case 7
                        dr("DAY_TEST") = "SATURDAY"
                End Select
                dr("REL_NO") = 0
                dr("PROSPECT_TOTAL") = 0
                dr("WEEK") = "Current Week"
                dt.Rows.Add(dr)
            End If
        Next

        sql = "SELECT Count(REL_NO) AS REL_NO, Sum(RELATIONSHIP.PROSPECT_TOTAL) AS PROSPECT_TOTAL, TO_CHAR(FOLLOW_UP_DT,'DAY') As DAY_TEST, TO_CHAR(FOLLOW_UP_DT,'fmD') As DAY_NUM, 'Next Week' AS WEEK  "
        sql = sql & "FROM RELATIONSHIP "
        sql = sql & "WHERE SLSP1 = '" & cbo_slsp.SelectedValue & "' "
        sql = sql & "AND CONVERSION_DT IS NULL "
        sql = sql & "AND FOLLOW_UP_DT >= TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday).AddDays(7) & "','mm/dd/RRRR') "
        sql = sql & "AND FOLLOW_UP_DT <= TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday).AddDays(13) & "','mm/dd/RRRR') "
        sql = sql & "GROUP BY TO_CHAR(FOLLOW_UP_DT,'DAY'),TO_CHAR(FOLLOW_UP_DT,'fmD') "
        sql = sql & "ORDER BY 4"

        Dim ds2 As DataSet
        ds2 = New DataSet

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)

        Dim dt2 As DataTable = ds.Tables(0)
        Dim dr2 As DataRow

        i = 1
        Missing_String = ""
        Curr_day = ""

        For Each dr2 In ds2.Tables(0).Rows
            If Curr_day <> dr2("DAY_NUM").ToString Then
                If CDbl(dr2("DAY_NUM").ToString) <> i Then
                    Do While i < CDbl(dr2("DAY_NUM").ToString)
                        Missing_String = Missing_String & i & ","
                        i = i + 1
                    Loop
                End If
            End If
            Curr_day = dr2("DAY_NUM").ToString
            i = i + 1
        Next
        If i <= 7 Then
            Do While i <= 7
                Missing_String = Missing_String & i & ","
                i = i + 1
            Loop
        End If

        DayArray = Split(Missing_String, ",")

        For x = LBound(DayArray) To UBound(DayArray)
            If Not String.IsNullOrEmpty(DayArray(x)) Then
                dr2 = dt2.NewRow
                dr2("DAY_NUM") = DayArray(x)
                Select Case DayArray(x)
                    Case 1
                        dr2("DAY_TEST") = "SUNDAY"
                    Case 2
                        dr2("DAY_TEST") = "MONDAY"
                    Case 3
                        dr2("DAY_TEST") = "TUESDAY"
                    Case 4
                        dr2("DAY_TEST") = "WEDNESDAY"
                    Case 5
                        dr2("DAY_TEST") = "THURSDAY"
                    Case 6
                        dr2("DAY_TEST") = "FRIDAY"
                    Case 7
                        dr2("DAY_TEST") = "SATURDAY"
                End Select
                dr2("REL_NO") = 0
                dr2("PROSPECT_TOTAL") = 0
                dr2("WEEK") = "Next Week"
                dt2.Rows.Add(dr2)
            End If
        Next

        For Each dr2 In ds2.Tables(0).Rows
            dr = dt.NewRow
            dr("DAY_NUM") = dr2("DAY_NUM")
            dr("DAY_TEST") = dr2("DAY_TEST")
            dr("REL_NO") = dr2("REL_NO")
            dr("PROSPECT_TOTAL") = dr2("PROSPECT_TOTAL")
            dr("WEEK") = dr2("WEEK")
            dt.Rows.Add(dr)
        Next

        For Each dr In ds.Tables(0).Rows
            Select Case dr("DAY_NUM")
                Case 1
                    dr("DAY_TEST") = "SUNDAY"
                Case 2
                    dr("DAY_TEST") = "MONDAY"
                Case 3
                    dr("DAY_TEST") = "TUESDAY"
                Case 4
                    dr("DAY_TEST") = "WEDNESDAY"
                Case 5
                    dr("DAY_TEST") = "THURSDAY"
                Case 6
                    dr("DAY_TEST") = "FRIDAY"
                Case 7
                    dr("DAY_TEST") = "SATURDAY"
            End Select

        Next

        conn.Close()
        Dim view As New DataView(dt)
        view.Sort = "DAY_NUM ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl1.DataSource = view

        ' Specify data members to bind the series.
        WebChartControl1.SeriesDataMember = "WEEK"
        WebChartControl1.SeriesTemplate.ArgumentDataMember = "DAY_TEST"
        WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("REL_NO").ColumnName.ToString))

        WebChartControl1.SeriesTemplate.View = New LineSeriesView

        WebChartControl1.DataBind()

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub emp_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP_CD, LNAME || ', ' || FNAME as full_desc FROM EMP WHERE TERMDATE IS NULL AND HOME_STORE_CD='" & cbo_store.SelectedValue & "' order by LNAME, FNAME"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_slsp
                .DataSource = ds
                .DataValueField = "EMP_CD"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub cbo_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store.SelectedIndexChanged

        emp_cd_populate()

    End Sub

    Public Function GetFirstDateOfWeek(ByVal pDate As DateTime, ByVal pFirstDayOfWeek As System.Web.UI.WebControls.FirstDayOfWeek) As DateTime

        Dim vFirstDayOfWeek As Short = Convert.ToInt16(pFirstDayOfWeek)
        Dim vDayOfWeek As Short = Convert.ToInt16(pDate.DayOfWeek)  ' 0 = sunday
        If (vDayOfWeek <> vFirstDayOfWeek) Then

            If (vFirstDayOfWeek > vDayOfWeek) Then
                pDate = pDate.AddDays(vFirstDayOfWeek - vDayOfWeek - 7)
            Else
                pDate = pDate.AddDays(vFirstDayOfWeek - vDayOfWeek)
            End If
        End If
        Return pDate

    End Function

    Protected Sub cbo_slsp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_slsp.SelectedIndexChanged

        If Not String.IsNullOrEmpty(cbo_slsp.SelectedValue.ToString) Then
            'populate_relationship_trends()
            populate_sales_trends()
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Public Sub populate_relationship_trends()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter

        ds = New DataSet

        conn.Open()

        sql = "select a.del_doc_num, a.so_store_cd, a.final_dt, a.cust_cd, a.ord_tp_cd, a.ord_srt_cd, b.qty, b.unit_prc, b.so_emp_slsp_cd1, b.treated_by_itm_cd, "
        sql = sql & "b.pct_of_sale1, b.so_emp_slsp_cd2, b.pct_of_sale2, e.prod_grp_cd , c.mnr_cd, c.treatable, c.itm_tp_cd, c.ve_cd, b.itm_cd, TO_CHAR(FINAL_DT,'DAY') As DAY_TEST, TO_CHAR(FINAL_DT,'fmD') As DAY_NUM "
        sql = sql & "from so a, so_ln b, itm c, inv_mnr d, inv_mjr e where c.mnr_cd = d.mnr_cd and d.mjr_cd = e.mjr_cd and "
        sql = sql & "a.del_doc_num = b.del_doc_num and b.itm_cd = c.itm_cd and a.final_dt between TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday) & "','mm/dd/yyyy') "
        sql = sql & "and TO_DATE('" & GetFirstDateOfWeek(DateTime.Today, System.Web.UI.WebControls.FirstDayOfWeek.Sunday).AddDays(6) & "','mm/dd/yyyy') and a.ord_tp_cd in ('SAL','CRM','MDB','MCR') and a.stat_cd='F' and b.void_flag='N' "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        Dim i As Integer = 1
        Dim Missing_String As String = ""
        Dim Curr_day As String = ""

        For Each dr In ds.Tables(0).Rows
            If Curr_day <> dr("DAY_NUM").ToString Then
                If CDbl(dr("DAY_NUM").ToString) <> i Then
                    Do While i < CDbl(dr("DAY_NUM").ToString)
                        Missing_String = Missing_String & i & ","
                        i = i + 1
                    Loop
                End If
            End If
            Curr_day = dr("DAY_NUM").ToString
            i = i + 1
        Next
        If i <= 7 Then
            Do While i <= 7
                Missing_String = Missing_String & i & ","
                i = i + 1
            Loop
        End If

        Dim DayArray As Array

        DayArray = Split(Missing_String, ",")
        Dim x As Integer

        For x = LBound(DayArray) To UBound(DayArray)
            If Not String.IsNullOrEmpty(DayArray(x)) Then
                dr = dt.NewRow
                dr("DAY_NUM") = DayArray(x)
                Select Case DayArray(x)
                    Case 1
                        dr("DAY_TEST") = "SUNDAY"
                    Case 2
                        dr("DAY_TEST") = "MONDAY"
                    Case 3
                        dr("DAY_TEST") = "TUESDAY"
                    Case 4
                        dr("DAY_TEST") = "WEDNESDAY"
                    Case 5
                        dr("DAY_TEST") = "THURSDAY"
                    Case 6
                        dr("DAY_TEST") = "FRIDAY"
                    Case 7
                        dr("DAY_TEST") = "SATURDAY"
                End Select
                dr("REL_NO") = 0
                dr("PROSPECT_TOTAL") = 0
                dr("WEEK") = "Current Week"
                dt.Rows.Add(dr)
            End If
        Next

        conn.Close()
        Dim view As New DataView(dt)
        view.Sort = "DAY_NUM ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl1.DataSource = view

        ' Specify data members to bind the series.
        WebChartControl1.SeriesDataMember = "WEEK"
        WebChartControl1.SeriesTemplate.ArgumentDataMember = "DAY_TEST"
        WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("REL_NO").ColumnName.ToString))

        WebChartControl1.SeriesTemplate.View = New LineSeriesView

        WebChartControl1.DataBind()

    End Sub

End Class
