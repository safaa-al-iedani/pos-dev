﻿<%@ Page Title="Cash Drawer Balancing" Language="VB" MasterPageFile="~/POSMaster.master" AutoEventWireup="false" CodeFile="CashDrawerBalancing.aspx.vb" Inherits="CashDrawerBalancing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="Scripts/CashDrawerBalancing.js?12345"></script>
    <script src="Scripts/jquery.printthis.js"></script>
    <script src="Scripts/jquery.bpopup.min.js"></script>

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <style type="text/css">
        .mCSB_container {
            width: 100%;
        }

        .ui-autocomplete {
            max-height: 400px;
            overflow-y: auto;
            overflow-x: hidden;
        }

        /*IE 6 doesn't support max-height
        we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 400px;
        }

        .modalOverlay {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            background-color: rgba(0,0,0,0.3); /* black semi-transparent */
        }
            [disabled], /* Daniela black */
            input[type=button]:disabled,
            input[type=submit]:disabled {
            color: Black;
            cursor: default;
            pointer-events: none;
    }
            
    }
       
    </style>

    <div style="display: block; margin: auto; width: 100%;">
        <div class="divHeader"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label786 %>" /></div>
        <div class="divContents">
            <div id="divCdb" style="display: table; width: 100%;">
                <div style="display: table-row">
                    <label id="lblErrMsg"></label>
                </div>

                <div style="display: table-row;">
                    <div style="display: table; margin-top: 10px; width: 100%;">
                        <div style="display: table-cell; width: 33%;">
                            <label for="txtStore"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label572 %>" /></label>
                            <input type="text" id="txtStore" style="width: 20%" autocomplete="off" />
                            <input type="text" id="txtStoreDesc" class="disable" style="width: 50%;" />
                            <asp:HiddenField ID="hidStoreDtls" runat="server" ClientIDMode="Static" />
                        </div>
                        <div style="display: table-cell; width: 41%;">
                            <label for="txtCashDrawer"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label61 %>" /></label>
                            <input type="text" id="txtCashDrawer" style="width: 20%" autocomplete="off" />
                            <input type="text" id="txtCashDrawerDesc" class="disable" style="width: 45%;" />
                        </div>
                        <div style="display: table-cell; width: 26%;">
                            <label for="txtTransactionDate"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label625 %>" /></label>
                            <input type="text" id="txtTransactionDate" style="width: 85px;" class="dateBox" />
                        </div>
                    </div>
                </div>

                <div style="display: table-row">
                    <div style="display: table; width: 100%; margin-top: 10px;">
                        <div style="display: table-cell; width: 42%;">
                            <label for="ddlCashier"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label65 %>" /></label>
                            <select id="ddlCashier" style="width: 20%;"></select>
                            <input type="text" id="txtCashier" class="disable" style="width: 50%;" />
                            <input type="hidden" id="hidCashierCd" />
                        </div>
                        <div style="display: table-cell; width: 42%;">
                            <label for="ddlCurr"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label128 %>" /></label>
                            <select id="ddlCurr" style="width: 20%;"></select>
                            <input type="text" id="txtCurrDesc" class="disable" style="width: 50%;" />
                        </div>
                        <div style="display: table-cell; text-align: right;">
                            <input type="button" name="btnGo" value="<asp:Localize ID='Localize1' Text='<%$ Resources:LibResources, Label222 %>' runat='server'></asp:Localize>" id="btnGo" title="Go" class="MenuButton" style="width: 100%;" />
                            <input type="hidden" id="hidCurrSymbol" />
                            <input type="hidden" id="hidIsDrawerOpen" />
                            <input type="hidden" id="hidCloseBalance" />
                            <input type="hidden" id="hidIsDrawerClosed" />
                            <input type="hidden" id="hidIsDataRetrived" />
                            <input type="hidden" id="hidIsDataEntered" />
                            <input type="hidden" id="hidDepositSlipNo" />
                            <input type="hidden" id="hidIsUpdateEnabled" />
                        </div>
                    </div>
                </div>

                <div id="divCdbDetail" style="display: table-row">
                    <div style="display: table; width: 100%;">
                        <div style="display: table-row">
                            <div style="display: table; width: 100%; margin-top: 10px;">
                                <div style="display: table-cell; width: 33%;">
                                    <div class="divHeader"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label58 %>" /></div>
                                    <div class="divContents">

                                        <div style="display: table;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell; width: 50%;">
                                                    <label for="txtOpenBal"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label360 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; width: 50%;">
                                                    <input type="text" id="txtOpenBal" class="numeric" style="text-align: right;" value="0.00" />
                                                </div>
                                            </div>

                                            <div id="divCash" style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <div style="display: table;">
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; width: 5%; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; width: 20%; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash1"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label788 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell; width: 25%;">
                                                                <input type="text" id="txtCash1" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash2"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label789 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash2" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash5"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label790 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash5" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash10"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label791 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash10" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash20"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label792 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash20" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash50"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label793 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash50" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash100"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label794 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash100" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: right;">
                                                                <label class="currency"></label>
                                                            </div>
                                                            <div style="display: table-cell; text-align: right; padding-right: 5px;">
                                                                <label for="txtCash1000"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label795 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtCash1000" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div style="display: table-cell;">
                                                    <div style="display: table;">
                                                        <div style="display: table-row; height: 20px;">&nbsp;</div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; width: 25%;">
                                                                <label for="txtPennies" style="padding-left: 3px;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label398 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell; width: 25%;">
                                                                <input type="text" id="txtPennies" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <label for="txtNickels" style="padding-left: 3px;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label339 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtNickels" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <label for="txtDimes" style="padding-left: 3px;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label163 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtDimes" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <label for="txtQuarters" style="padding-left: 3px;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label445 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtQuarters" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <label for="txtHalfDlr" style="padding-left: 3px;">
                                                                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label226 %>" />
                                                                    <label class="currency"></label></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtHalfDlr" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <label for="txtDlrCoin" style="padding-left: 3px;">
                                                                    <label class="currency"></label>
                                                                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label1 %>" /></label>
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="text" id="txtDlrCoin" class="onlynumber" style="text-align: right;" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtCashTotal"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label69 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtCashTotal" style="text-align: right;" class="disable" value="0.00" />
                                                    <asp:HiddenField ID="hidIsInsert" runat="server" ClientIDMode="Static" />
                                                </div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtTransCsTotal"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label626 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtTransCsTotal" style="text-align: right;" class="disable" value="0.00" />
                                                    <input type="hidden" id="hidTransCsTotal" />
                                                </div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtCashOvrShrt"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label376 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtCashOvrShrt" class="numeric" style="text-align: right;" />
                                                    <input type="hidden" id="hidCashOvrShrt" />
                                                </div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell;"></div>
                                                <div style="display: table-cell; text-align: right; padding: 6px;">
                                                    <a id="cashTransDtl" class="ShowDetail" title="Detail Cash Transactions"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label161 %>" /></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div style="display: table-cell; width: 33%; padding-left: 2px;">
                                    <div class="divHeader">
                                        <label class="checkLabel"></label>
                                    </div>
                                    <div id="chkData" class="divContents">
                                        <div style="display: table; width: 100%;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell; text-align: center;">
                                                    <label for="txtChkName" style="font-weight: bold;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label325 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; text-align: center;">
                                                    <label for="txtChkAmt" style="font-weight: bold;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label28 %>" /></label>
                                                </div>
                                            </div>
                                            <div id="chkAddRow" style="display: table-row;">
                                                <div style="display: table-cell; width: 47%;">
                                                    <input type="text" id="txtChkName" maxlength="30" />
                                                </div>
                                                <div style="display: table-cell; width: 47%;">
                                                    <input type="text" id="txtChkAmt" class="numeric" style="text-align: right;" />
                                                </div>
                                                <div style="display: table-cell; width: 6%;">
                                                    <a id="addNewChk" title="Add new Check">+</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="scrollChkData" style="display: block; height: 152px; width: 100%; margin-top: 5px; margin-bottom: 5px;">
                                            <asp:HiddenField ID="hidCheckData" runat="server" ClientIDMode="Static" />
                                            <div id="divAddCheck"></div>
                                        </div>

                                        <div style="display: table;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell; width: 47%;">
                                                    <label for="txtCount"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label114 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; width: 47%;">
                                                    <input type="text" id="txtChkCnt" class="disable" style="text-align: right;" value="0" />
                                                </div>
                                                <div style="display: table-cell; width: 6%;"></div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtChkTotal"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label78 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtChkTotal" class="disable" style="text-align: right;" value="0.00" />
                                                </div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtTransCkTotal"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label626 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtTransCkTotal" style="text-align: right;" class="disable" value="0.00" />
                                                    <input type="hidden" id="hidTransCkTotal" />
                                                </div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <label for="txtAmtOverShrt"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label376 %>" /></label>
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input type="text" id="txtCheckOverShrt" style="text-align: right;" />
                                                    <input type="hidden" id="hidCheckOverShrt" />
                                                </div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;"></div>
                                                <div style="display: table-cell; text-align: right; padding: 6px;">
                                                    <a id="checkTransDtl" class="ShowDetail" title="Detail Check Transactions"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label161 %>" /></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div style="display: table-cell; width: 33%; padding-left: 2px;">
                                    <div class="divHeader"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label373 %>" /></div>
                                    <div id="othrData" class="divContents">
                                        <div style="display: table; width: 100%;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell; text-align: center;">
                                                    <label style="font-weight: bold;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label322 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; text-align: center;">
                                                    <label style="font-weight: bold;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label158 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; text-align: center;">
                                                    <label style="font-weight: bold;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label28 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; width: 6%;"></div>
                                            </div>

                                        </div>

                                        <div id="othorMopScroll" style="display: block; height: 227px; width: 100%; margin-top: 5px; margin-bottom: 5px;font:#ff0000">
                                            <div id="divAddOther"></div>
                                        </div>

                                        <div style="display: table;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell; width: 47%;">
                                                    <label for="txtOtherCount"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label114 %>" /></label>
                                                </div>
                                                <div style="display: table-cell; width: 47%;">
                                                    <input type="text" id="txtOtherCount" class="disable" value="0" style="text-align: right;" />
                                                </div>
                                                <div style="display: table-cell; width: 6%;"></div>
                                            </div>

                                            <div style="display: table-row;">
                                                <div style="display: table-cell; width: 47%;">
                                                    <label for="txtOthrTotal">Total</label>
                                                </div>
                                                <div style="display: table-cell; width: 47%;">
                                                    <input type="text" id="txtOthrTotal" class="disable" style="text-align: right;" value="0.00" />
                                                </div>
                                                <div style="display: table-cell; width: 6%;"></div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;"></div>
                                                <div style="display: table-cell; text-align: right; padding: 6px;">
                                                    <a id="checkOther" class="ShowDetail" title="Detail Cash Transactions"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label161 %>" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="display: table-row">
                            <div style="display: table; margin-left: auto; margin-right: auto; margin-top: 10px; margin-bottom: 20px;">
                                <div style="display: table-cell;">
                                    <label for="txtDepSlip"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label157 %>" /></label>
                                </div>
                                <div style="display: table-cell;">
                                    <input type="text" id="txtDepSlip" class="disable" />
                                </div>
                                <div style="display: table-cell; padding-left: 10px;">
                                    <label for="txtDepBag"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label156 %>" /></label>
                                </div>
                                <div style="display: table-cell;">
                                    <input type="text" id="txtDepBag" />
                                </div>
                                <div style="display: table-cell; text-align: right; font-size: 1em; padding-left: 10px;">
                                    <label><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label433 %>" /></label>
                                    <label id="lblIsPrnt">No</label>&nbsp;
                                    (<label id="lblPrintSlipCnt">0</label>
                                    times)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="divSubBtns" style="display: table-row">
                    <div style="display: table; margin: auto; width: 98%;">
                        <input type="button" name="btnSave" value="<asp:Localize ID='Localize2' Text='<%$ Resources:LibResources, Label520 %>' runat='server'></asp:Localize>" id="btnSave" title="Save" class="MenuButton"
                            style="width: 19%; background-image: url(images/icons/Save24.gif); background-position-x: 18px;" />&nbsp;
                        <input type="button" name="btnClear" value="<asp:Localize ID='Localize3' Text='<%$ Resources:LibResources, Label82 %>' runat='server'></asp:Localize>" id="btnClear" title="Clear" class="MenuButton"
                            style="width: 19%; background-image: url(images/icons/Undo24.gif); background-position-x: 18px;" />&nbsp;
                        <input type="button" name="btnBalance" value="<asp:Localize ID='Localize4' Text='<%$ Resources:LibResources, Label44 %>' runat='server'></asp:Localize>" id="btnBalance" title="Balance" class="MenuButton"
                            style="width: 19%; background-image: url(images/icons/money20px.gif); background-position-x: 18px;" />&nbsp;
                        <input type="button" name="btnClose" value="<asp:Localize ID='Localize5' Text='<%$ Resources:LibResources, Label91 %>' runat='server'></asp:Localize>" id="btnClose" title="Close Drawer" class="MenuButton"
                            style="width: 19%; background-image: url(images/icons/CloseDrawer20px.jpg); background-position-x: 10px;" />&nbsp;
                        <input type="button" name="btnPrintDepSlip" value="<asp:Localize ID='Localize6' Text='<%$ Resources:LibResources, Label432 %>' runat='server'></asp:Localize>" id="btnPrintDepSlip" title="Print Deposit Slip"
                            class="MenuButton" style="width: 19%; background-image: url(images/icons/Print20px.png); background-position-x: 5px;" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popTransDetail" class="PopUp" style="width: 500px;">
        <div id="divTransDtlHeader" class="divHeader" title="Transactions Detail"></div>
        <div style="position: absolute; right: 2px; top: 2px;">
            <img id="aCloseCashDetail" alt="X" title="Close" src="images/icons/exit.gif" style="cursor: pointer;" />
        </div>
        <div class="divContents">
            <div style="display: table; width: 100%;">
                <div style="display: table-row;">
                    <div style="display: block; margin: auto;">
                        <label id="lblTransErr" runat="server" style="color: red;"></label>
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table; margin: auto; margin-left: 0px; width: 94%;">
                        <div style="display: table-row; text-align: center; height: 20px;">
                            <div style="display: table-cell; width: 50px;">
                                <label style="font-weight: bold;">MOP</label>
                            </div>
                            <div style="display: table-cell; text-align: center; width: 90px;">
                                <label style="font-weight: bold;">Invoice#</label>
                            </div>
                            <div style="display: table-cell; text-align: left; width: 90px;">
                                <label style="font-weight: bold;">Post Date</label>
                            </div>
                            <div style="display: table-cell; text-align: left; width: 50px;">
                                <label style="font-weight: bold;">Trn</label>
                            </div>
                            <div style="display: table-cell; text-align: center; width: 50px;">
                                <label style="font-weight: bold;">D/C</label>
                            </div>
                            <div style="display: table-cell; text-align: center; width: 100px;">
                                <label style="font-weight: bold;">Amount</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="display: table-row;">
                    <div id="divCsTransDtl" style="display: block; height: 190px; width: 100%;">
                        <div id="divTransDetail"></div>
                    </div>
                </div>

                <div style="display: table-row;">
                    <div style="display: table; margin: auto; height: 18px; margin-top: 10px; width: 100%;">
                        <div style="display: table-cell; float: left;">
                            <label>Transaction Count:</label>
                            <input type="text" id="txtTransCount" style="text-align: right; width: 50px;" class="disable" value="0" />
                        </div>
                        <div style="display: table-cell; float: right;">
                            <label>Total:</label>
                            <input type="text" id="txtTransTotal" style="text-align: right; width: 100px;" class="disable" value="0.00" />
                        </div>
                    </div>
                </div>
                <div style="display: table-row; text-align: right;">
                    <div style="margin-top: 10px;">
                        <input type="button" name="btnUpdateTrn" value="Save Changes" id="btnUpdateTrn" title="Save Changes"
                            class="MenuButton" />
                        &nbsp;<input type="hidden" id="hidMopTrans" />
                        <input type="button" name="btnResetTrn" value="Reset" id="btnResetTrn" title="Restore the Transcation"
                            class="MenuButton" />
                        &nbsp;
                        <input type="button" name="btnReturnTrn" value="Return to CDB" id="btnReturnTrn" title="Return to CDB"
                            class="MenuButton" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divPopupConfirm" class="PopUp" style="width: 500px;">
        <div id="divConfirmHeader" class="divHeader" title="Save Cash Drawer Balance Information?" style="text-align: left;">Save Cash Drawer Balance Information?</div>
        <div style="position: absolute; right: 2px; top: 2px;">
            <img id="imgCloseConfirm" alt="X" title="Close" src="images/icons/exit.gif" style="cursor: pointer;" />
        </div>
        <div class="divContents">
            <div style="display: table;">
                <div style="display: table-cell; text-align: center; vertical-align: middle; width: 30%;">
                    <img alt="Save Cash Drawer Balance Information?" title="Save Cash Drawer Balance Information?" src="images/warning.jpg" />
                </div>
                <div style="display: table-cell;">
                    <div style="display: table;">
                        <div style="display: table-row;">
                            <p>You are about to clear entered/updated data?</p>
                        </div>
                        <div style="display: table-row; margin: auto;">
                            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;">
                                <input type="button" name="btnConfirmYes" value="Yes, Proceed" id="btnConfirmYes" title="Yes, Save Order"
                                    class="MenuButton" />&nbsp;
                                <input type="button" name="btnConfirmCancel" value="Cancel" id="btnConfirmCancel" title="Cancel"
                                    class="MenuButton" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popOk" class="PopUp" style="width: 800px;">
        <div class="divHeader" title="Error">Error</div>
        <div class="divContents">
            <table border="0" style="width: 100%;">
                <tr>
                    <td>
                        <label>A default currency code must be defined</label></td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <input type="button" name="btnOk" value="Ok" id="btnOk" title="btnOk" onclick="closeErrorPopUp()"
                            class="MenuButton" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="popDepositSlip" class="PopUp" style="width: 800px;">
        <div class="divHeader" title="Deposit Slip">Print Deposit Slip</div>
        <div style="position: absolute; right: 2px; top: 2px;">
            <img id="imgDepositSlipClose" alt="X" title="Close" src="images/icons/exit.gif" style="cursor: pointer;" />
        </div>
        <div class="divContents">
            <div id="divDepositSlip" style="display: block; height: 405px; width: 100%;">
                <div id="divPrintDepositSlip" style="width: 98%;">
                    <div id="depSlipData" style="display: table; width: 100%;">
                        <div id="firstDepSlipData" style="display: table-row; width: 100%;">

                            <table cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid Black; page-break-inside: avoid; -webkit-region-break-inside: avoid;">
                                <tr>
                                    <td colspan="3" style="text-align: center; vertical-align: top;">
                                        <label class="checkLabel" style="font-size: 0.9em; font-weight: bold;"></label>
                                    </td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="5" style="text-align: left; vertical-align: top;">
                                        <label id="lblDepositSlipTitle">&nbsp;</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="5" style="border-bottom: 1px solid Black;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: left;">
                                        <label style="font-size: 0.8em;">Date</label>
                                    </td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left;">
                                        <label style="font-size: 0.8em;">Date</label>
                                    </td>
                                    <td colspan="2" style="text-align: left; border-left: 1px solid black;">
                                        <label style="font-size: 0.8em;">Deposit Bag#</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsCkDate" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;
                                    </td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsCkDate" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;
                                    </td>
                                    <td colspan="2" style="border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label class="dsDepositBagNo" style="font-size: 0.9em; font-weight: bold;">&nbsp;</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 26%; text-align: left;">
                                        <label style="font-size: 0.8em;">Transit No.</label>
                                    </td>
                                    <td colspan="2" style="border-left: 1px solid Black;">
                                        <label style="font-size: 0.8em;">Account No.</label>
                                    </td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left;">
                                        <label style="font-size: 0.8em;">Transit No.</label>
                                    </td>
                                    <td colspan="2" style="border-left: 1px solid Black;">
                                        <label style="font-size: 0.8em;">Account No.</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsTransitNo" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td colspan="2" style="text-align: left; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label class="dsAccountNo" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsTransitNo" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td colspan="2" style="text-align: left; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label class="dsAccountNo" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: left;">
                                        <label style="font-size: 0.8em;">Account Name (please print)</label></td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="5" style="text-align: left;">
                                        <label style="font-size: 0.8em;">Account Name (please print)</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsAccountName" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="5" style="text-align: left; border-bottom: 1px solid Black;">
                                        <label class="dsAccountName" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">Particulars</td>
                                    <td style="width: 17%; text-align: center; border-left: 1px solid Black;">Amount</td>
                                    <td style="width: 7%; border-left: 1px dotted Black;">&nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="width: 11%; text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1"></label>
                                        &nbsp;</td>
                                    <td style="width: 4%; text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="width: 8%; text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">1</td>
                                    <td style="width: 17%; text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1Total"></label>
                                        &nbsp;</td>
                                    <td style="width: 7%; text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid black;">&nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-bottom: 1px solid black;">&nbsp;</td>
                                    <td style="border-left: 1px dotted Black; border-bottom: 1px solid black;">&nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr2"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">2</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr2Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr2TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName1"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt1"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP1"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr5"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">5</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr5Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr5TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName2"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt2"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP2"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr10"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">10</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr10Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr10TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName3"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt3"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP3"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr20"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">20</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr20Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr20TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName4"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt4"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP4"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr50"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">50</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr50Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr50TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName5"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt5"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP5"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr100"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">100</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr100Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr100TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName6"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt6"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP6"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1000"></label>
                                        &nbsp;</td>
                                    <td style="text-align: center; border-bottom: 1px solid Black;">X</td>
                                    <td style="text-align: right; padding-right: 5px; border-bottom: 1px solid Black;">1000</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1000Total"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurr1000TotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName7"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt7"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP7"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">Total&nbsp;<label class="currCode"></label>&nbsp;Cash
                                    </td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurrTotal"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurrTotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName8"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt8"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP8"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">Total Coin
                                    </td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurrCoinTotal"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblPdCurrCoinTotalP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName9"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt9"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP9"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="2" style="text-align: left; border-bottom: 1px solid black; font-size: 0.8em; white-space: nowrap;">Total
                                        <label class="currCode"></label>
                                        <label class="checkLabel"></label>
                                        <%--&nbsp;<label id="pdCsCurr" class="currency"></label>&nbsp;--%>
                                    </td>
                                    <td style="text-align: center; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label class="lblTotalNoChk" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label class="lblTotalChkAmt"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label class="lblTotalChkAmtP"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName10"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt10"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP10"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="2" style="text-align: left; border-bottom: 1px solid Black;">&nbsp;
                                    </td>
                                    <td style="text-align: center; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName11"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt11"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP11"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="2" style="text-align: left; border-bottom: 1px solid Black;">&nbsp;
                                    </td>
                                    <td style="text-align: center; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName12"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt12"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP12"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">&nbsp;
                                    </td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">
                                        <label class="lblPdCkName13"></label>
                                        &nbsp;</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmt13"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblPdCkAmtP13"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">&nbsp;
                                    </td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">Total&nbsp;
                                        <label class="currCode"></label>
                                        &nbsp;
                                        <label class="checkLabel"></label>
                                        &nbsp;Amount</td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblTotalChkAmt" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid black;">
                                        <label class="lblTotalChkAmtP" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                                    <td colspan="3" style="text-align: left; border-bottom: 1px solid Black;">TOTAL
                                    </td>
                                    <td style="text-align: right; border-left: 1px solid Black; border-bottom: 1px solid Black;">
                                        <label id="lblTotalPd" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="text-align: left; border-left: 1px dotted Black; border-bottom: 1px solid Black;">
                                        <label id="lblTotalPdP" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left; border-bottom: 1px solid black;">Total No&nbsp;
                                        <label class="currCode"></label>
                                        &nbsp;
                                        <label class="checkLabel"></label>
                                        &nbsp;
                                    </td>
                                    <td colspan="2" style="text-align: center; border-left: 1px solid Black; border-bottom: 1px solid black;">
                                        <label class="lblTotalNoChk" style="font-size: 0.9em; font-weight: bold;"></label>
                                        &nbsp;</td>
                                    <td style="border-left: 1px solid Black; border-right: 1px solid Black; border-bottom: 1px solid Black;">&nbsp;</td>
                                    <td colspan="5" style="text-align: left; border-bottom: 1px solid Black;">Deposited by:
                                        &nbsp;<label id="lblEmpinit"></label>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="9">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td style="text-align: left;">
                                                    <label class="dsPageNo"></label>
                                                    &nbsp;</td>
                                                <td style="text-align: center;">
                                                    <label class="dsDepositSlip"></label>
                                                    &nbsp;</td>
                                                <td style="text-align: right;">
                                                    <label class="pdDsCopyName"></label>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <div style="display: table-row; height: 20px;">
                                &nbsp;
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div style="display: block; text-align: right; margin-top: 10px;">
                <input type="button" name="btnPrintDepositSlip" value="Print" id="btnPrintDepositSlip" title="Print Deposit Slip"
                    class="MenuButton" style="width: 100px; background-image: url(images/icons/Print20px.png); background-position-x: 5px;" />
            </div>
        </div>
    </div>

    <div id="divTempNextDSPage" style="display: none; width: 100%;">
        <div id="divNextDpSlipData">
            <br style="page-break-before: always" />
            <table cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid Black; page-break-inside: avoid; -webkit-region-break-inside: avoid;">
                <tr>
                    <td style="width: 26%; text-align: left;">
                        <label style="font-size: 0.8em;">Date</label>
                    </td>

                    <td colspan="2" style="text-align: left; border-left: 1px solid black;">
                        <label style="font-size: 0.8em;">Deposit Bag#</label>
                    </td>

                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>

                    <td colspan="3" style="text-align: left;">
                        <div style="padding-left: 10px;">Continued</div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid Black;">
                        <label class="dsCkDate" style="font-size: 0.9em; font-weight: bold; display: inline;"></label>
                        &nbsp;
                    </td>
                    <td colspan="2" style="text-align: left; border-left: 1px solid black; border-bottom: 1px solid Black;">
                        <label class="dsDepositBagNo" style="font-size: 0.9em; font-weight: bold; display: inline"></label>
                        &nbsp;
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td colspan="3" style="border-bottom: 1px solid black;">&nbsp;
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left;">
                        <label style="font-size: 0.8em;">Transit No.</label>
                    </td>

                    <td colspan="2" style="text-align: left; border-left: 1px solid black;">
                        <label style="font-size: 0.8em;">Account No.</label>
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="width: 26%; text-align: center;">
                        <label>Particulars</label>
                    </td>

                    <td style="width: 17%; text-align: center; border-left: 1px solid black;">
                        <label>Amount</label>
                    </td>

                    <td style="width: 6%; border-left: 1px dotted Black;">&nbsp;
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="dsTransitNo" style="font-size: 0.9em; font-weight: bold;"></label>
                        &nbsp;
                    </td>
                    <td colspan="2" style="text-align: left; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="dsAccountNo" style="font-size: 0.9em; font-weight: bold;"></label>
                        &nbsp;
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="border-left: 1px solid black; border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="border-left: 1px dotted black; border-bottom: 1px solid black;">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3" style="text-align: left;">
                        <label style="font-size: 0.8em;">Account Name (please print)</label>
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName30"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt30"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP30"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3" style="text-align: left; border-bottom: 1px solid black;">
                        <label class="dsAccountName" style="font-size: 0.9em; font-weight: bold;"></label>
                        &nbsp;
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName31"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt31"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP31"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: center;">
                        <label>Particulars</label>
                    </td>
                    <td style="width: 17%; text-align: center; border-left: 1px solid black;">
                        <label>Amount</label>
                    </td>
                    <td style="width: 6%; border-left: 1px dotted Black;">&nbsp;
                    </td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName32"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt32"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP32"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="border-left: 1px solid black; border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="border-left: 1px dotted black; border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName33"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt33"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP33"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName1"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt1"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP1"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName34"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt34"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP34"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName2"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt2"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP2"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName35"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt35"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP35"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName3"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt3"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP3"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName36"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt36"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP36"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName4"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt4"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP4"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName37"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt37"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP37"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName5"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt5"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP5"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName38"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt38"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP38"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName6"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt6"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP6"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName39"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt39"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP39"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName7"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt7"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP7"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName40"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt40"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP40"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName8"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt8"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP8"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName41"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt41"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP41"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName9"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt9"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP9"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName42"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt42"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP42"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName10"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt10"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP10"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName43"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt43"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP43"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName11"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt11"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP11"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName44"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt44"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP44"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName12"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt12"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP12"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName45"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt45"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP45"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName13"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt13"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP13"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName46"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt46"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP46"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName14"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt14"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP14"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName47"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt47"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP47"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName15"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt15"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP15"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName48"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt48"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP48"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName16"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt16"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP16"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName49"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt49"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP49"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName17"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt17"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP17"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName50"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt50"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP50"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName18"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt18"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP18"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName51"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt51"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP51"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName19"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt19"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP19"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName52"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt52"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP52"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName20"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt20"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP20"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName53"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt53"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP53"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName21"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt21"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP21"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName54"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt54"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP54"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName22"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt22"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP22"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName55"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt55"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP55"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName23"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt23"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP23"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName56"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt56"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP56"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName24"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt24"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP24"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName57"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt57"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP57"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName25"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt25"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP25"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName58"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt58"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP58"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName26"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt26"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP26"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName59"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt59"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP59"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName27"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt27"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP27"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName60"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt60"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP60"></label>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName28"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt28"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP28"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <div class="divTotalCk" style="display: inline;">
                            Total<label class="currCode"></label>
                            <label class="checkLabel"></label>
                            &nbsp;Amount
                        </div>
                        &nbsp;</td>
                    <td style="text-align: right; vertical-align: middle; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <div class="divTotalCk" style="display: inline;">
                            <label class="lblTotalChkAmt" style="font-size: 0.9em; font-weight: bold;">&nbsp;</label>
                        </div>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <div class="divTotalCk" style="display: inline;">
                            <label class="lblTotalChkAmtP" style="font-size: 0.9em; font-weight: bold;"></label>
                        </div>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <label class="lblPdCkName29"></label>
                        &nbsp;</td>
                    <td style="text-align: right; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmt29"></label>
                        &nbsp;</td>
                    <td style="text-align: left; border-left: 1px dotted black; border-bottom: 1px solid black;">
                        <label class="lblPdCkAmtP29"></label>
                        &nbsp;</td>
                    <td style="border-left: 1px solid Black; border-right: 1px solid Black; border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="text-align: left; border-bottom: 1px solid black;">
                        <div class="divTotalCk" style="display: inline;">
                            Total No<label class="currCode"></label>
                            <label class="checkLabel"></label>
                        </div>
                        &nbsp;</td>
                    <td colspan="2" style="text-align: center; border-left: 1px solid black; border-bottom: 1px solid black;">
                        <div class="divTotalCk" style="display: inline;">
                            <label class="lblTotalNoChk" style="font-size: 0.9em; font-weight: bold;"></label>
                        </div>
                        &nbsp;</td>
                </tr>

                <tr>
                    <td colspan="7">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="text-align: left;">
                                    <label class="dsPageNo"></label>
                                    &nbsp;</td>
                                <td style="text-align: center;">
                                    <label class="dsDepositSlip"></label>
                                    &nbsp;</td>
                                <td style="text-align: right;">
                                    <label class="pdDsCopyName"></label>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>

                </tr>

            </table>

            <div style="display: table-row; height: 20px;">
                &nbsp;
            </div>
        </div>
    </div>

    <%--<div id="dialog-confirm" title="Save Cash Drawer Balance Information?">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            You are about to clear entered/updated data?</p>
    </div>--%>
</asp:Content>
