<%@ Page Language="VB" MasterPageFile="~/Regular.master" AutoEventWireup="false"
    CodeFile="restore_saved_order.aspx.vb" Inherits="restore_saved_order" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
        OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False" CellPadding="2" DataKeyField="session_id"
        Font-Bold="False" Font-Italic="False" Font-Overline="False"
        Font-Size="XX-Small" Font-Strikeout="False" Font-Underline="False" Height="144px"
        Width="100%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
            Font-Bold="False" Height="20px" HorizontalAlign="Left"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="emp_cd" HeaderText="User ID" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="save_date" HeaderText="<%$ Resources:LibResources, Label522 %>" ReadOnly="True" DataFormatString="{0:MM/dd/yyyy}">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="ord_tp_cd" HeaderText="<%$ Resources:LibResources, Label369 %>" ReadOnly="True">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="cust_cd" HeaderText="<%$ Resources:LibResources, Label131 %>" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="fname" HeaderText="<%$ Resources:LibResources, Label211 %>" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="lname" HeaderText="<%$ Resources:LibResources, Label131 %>" ReadOnly="True">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="hphone" HeaderText="<%$ Resources:LibResources, Label229 %>" ReadOnly="True">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="session_id" HeaderText="ID" Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="LEAD" HeaderText="CRM?" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:ButtonColumn HeaderText="<%$ Resources:LibResources, Label150 %>" CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
            <asp:ButtonColumn HeaderText="<%$ Resources:LibResources, Label476 %>" CommandName="Update" Text="&lt;img src='images/icons/icon_proceed.gif' width='24' height='24' border='0'&gt;">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
        </Columns>
        <ItemStyle VerticalAlign="Top" />
    </asp:DataGrid>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
</asp:Content>
