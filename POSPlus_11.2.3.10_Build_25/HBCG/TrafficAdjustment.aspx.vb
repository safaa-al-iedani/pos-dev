﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Partial Class TrafficAdjustment
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_stat As String = ""

        lbl_msg.Text = ""
        lbl_msg2.Text = ""

        If IsPostBack Then
            'do nothing
        Else
            ASPxPopupHourly.ShowOnPageLoad = False
            ASPxPopupSummary.ShowOnPageLoad = False
            lbl1_dt.Text = ""
            lbl1_store_cd.Text = ""
            GridView1.DataSource = ""
            GridView1.DataBind()
            clndr_traffic_dt.SelectedDate = DateTime.Now
            traffic_dt_txt.Text = FormatDateTime(clndr_traffic_dt.SelectedDate, DateFormat.ShortDate)
            clndr_traffic_dt.Visible = False
            populate_store_list()
        End If

    End Sub
    Protected Sub traffic_dt_sel_btn_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles traffic_dt_sel_btn.Click

        clndr_traffic_dt.Visible = True
        clndr_traffic_dt.Focus()
    End Sub
    Protected Sub traffic_dt_changed(ByVal sender As Object, ByVal e As System.EventArgs)

        traffic_dt_txt.Text = FormatDateTime(clndr_traffic_dt.SelectedDate, DateFormat.ShortDate)
        clndr_traffic_dt.Visible = False

    End Sub
    Public Sub populate_store_list()
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        str_drp_list.Items.Clear()

        sql = " select store_cd str_cd, store_cd||' - '||store_name name from store s "
        sql = sql & " where std_multi_co2.isvalidstr('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " and exists (select 'x' from traffic_store t where t.store_cd = s.store_cd "
        sql = sql & "                and t.installed is not null) "
        sql = sql & " order by 1 "

        str_drp_list.Items.Insert(0, "store")
        str_drp_list.Items.FindByText("store").Value = ""
        str_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With str_drp_list
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    Function isLeonStr(ByVal p_str As String) As String

        Dim v_co_cd = LeonsBiz.GetStoreCoCode(p_str)
        Dim v_co_grp = LeonsBiz.GetGroupCode(v_co_cd)

        If LeonsBiz.isLeonCoGrp(v_co_grp) = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Function Reader1Exists() As String


        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT count(*) cnt from traffic_detail  " &
                            " where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                            "   and reader = 1 " &
                            "   and trunc(detail_date) = to_date('" & traffic_dt_txt.Text & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If
    End Function
    Function TrafficSummaryExists() As String


        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT count(*) cnt from traffic_daily_summary " &
                            " where " &
                            "   store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                            "   and trunc(summary_date) = to_date('" & traffic_dt_txt.Text & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If
    End Function
    Function Reader3Exists() As String


        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT count(*) cnt from traffic_detail " &
                            " where reader = 3 " &
                            "   and store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                            "   and trunc(detail_date) = to_date('" & traffic_dt_txt.Text & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If
    End Function
    Protected Function getDay(p_dt As String) As String


        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT ltrim(ltrim(to_char(to_date('" & p_dt & "','mm/dd/RRRR'),'DAY'))) dt from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If
    End Function
    Protected Function getLastWk(p_i As String) As String

        Dim l_day As String = ""
        Dim l_dt As String = ""
        l_dt = FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate)
        l_day = getDay(l_dt)

        Dim v_value As String = ""
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT next_day(add_months(to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                                            " ,-12) , '" & l_day & "' ) - " & p_i & "dt from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If
    End Function
    Protected Function BuildSql() As String

        Dim l_wk1 As String = getLastWk(0)
        Dim l_wk2 As String = getLastWk(7)
        Dim l_wk3 As String = getLastWk(14)
        Dim l_wk4 As String = getLastWk(21)
        Dim l_wk5 As String = getLastWk(28)
        Dim l_wk6 As String = getLastWk(35)
        Dim v_sql As String = ""
        Dim v_hr As String = ""
        Dim v_adj_tab As String = ""

        For i As Integer = 0 To 23
            If i < 10 Then
                v_hr = "0" & i.ToString
            Else
                v_hr = i.ToString
            End If
            
            v_sql = v_sql & " select to_char(" & i & ",'00') HR " &
                           " , to_char(d.HR" & v_hr & ") CURR_CNT " &
                           " ,to_char(nvl((select round(avg(p2.HR" & v_hr & ") ) " &
                           "    from pre_traffic p2, traffic_store t " &
                           "   where p2.store = t.store_id  " &
                           "     and t.store_cd = d.store_cd " &
                           "     and p2.when in ( to_date('" & l_wk1 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & l_wk2 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & l_wk3 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & l_wk4 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & l_wk5 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & l_wk6 & "','mm/dd/RRRR') ) ),0)) AVG_CNT " &
                           " ,to_char((select l.new_cnt " &
                           "    from traffic_trf_adjust l " &
                           "   where l.store_cd = d.store_cd " &
                           "     and trunc(adjust_dt) =  to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                           "     and l.hour_cd = 'HR" & v_hr & "')) NEW_CNT " &
                           " from traffic_detail d " &
                           " where d.store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                           "   and d.detail_date = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                           "   and d.reader = 1 "
            If i < 23 Then
                v_sql = v_sql & " UNION "
            End If
        Next

        Return v_sql

    End Function
    Protected Function get_traffic_summ() As String

        Dim v_summ As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " select nvl(sum_reader_1,0) summ " &
                             "  from traffic_daily_summary " &
                             "  where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                             "    and summary_date =to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_summ = dbReader.Item("summ")
            Else
                v_summ = "0"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_summ & "" = "" Then
            Return "0"
        Else
            Return v_summ
        End If

    End Function
    Protected Sub get_traffic_data(ByVal p_ind As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim l_day As String = ""
        Dim l_dt As String = ""


        GridView1.DataSource = ""
        GridView9.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        ds = New DataSet

        l_dt = FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate)
        l_day = getDay(l_dt)

        objSql.CommandText = BuildSql()

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If p_ind = "E" Then
                    GridView1.DataSource = ds
                    GridView1.DataBind()
                Else
                    GridView9.DataSource = ds
                    GridView9.DataBind()
                End If

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataBind()
                lbl_msg.Text = "No data found for this Store/Date"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub
    Public Shared Function GetMinDetailDt(ByVal p_str As String) As String

        Dim v_dt As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT min(detail_date) dt from traffic_detail " &
                            " where store_cd = '" & p_str & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_dt = dbReader.Item("dt")
            Else
                v_dt = ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_dt & "" = "" Then
            Return ""
        Else
            Return v_dt
        End If

    End Function
    Function GetStoreId() As String

        Dim v_id As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT store_id id from traffic_store " &
                            " where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_id = dbReader.Item("id")
            Else
                v_id = "0"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_id & "" = "" Then
            Return "0"
        Else
            Return v_id
        End If

    End Function
    Function AllowBrickDetailEdit() As String

        Dim v_ind As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        'compare to min date on traffic_detail and min date on brk_pre_traffic
        Dim sql As String = "select 'Y' ind from dual " &
                            " where to_date('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') >=   " &
                            "       NVL(( select min(detail_date) from traffic_detail ),to_date('01/01/2999','mm/dd/RRRR')) " &
                            "  and  to_date('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') < " &
                            "       NVL((select min(when) from brk_pre_traffic),to_date('01/01/1999','mm/dd/RRRR') ) "



        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("ind")
            Else
                v_ind = "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If IsDBNull(v_ind) Then
            Return "N"
        Else
            Return v_ind
        End If


    End Function
    Function AllowBrickSummaryEdit() As String

        Dim v_ind As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        'compare to min date on brk_pre_traffic
        Dim sql As String = "select 'Y' ind from dual " &
                            " where to_date('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') < " &
                            "       nvl((select min(detail_date) " &
                            "              from traffic_detail  " &
                            "             where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                            "            ),to_date('01/01/2999','mm/dd/RRRR') " &
                            "          ) "



        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("ind")
            Else
                v_ind = "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If IsDBNull(v_ind) Then
            Return "N"
        Else
            Return v_ind
        End If


    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub STRSelected(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Sub insert_dummy_reader3_brick()

        Dim v_dt As String = formatDT(FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate))

        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)


        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_trfc_brk.insertDetailCust"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = str_drp_list.SelectedItem.Value.ToString()
            myCMD.Parameters.Add(New OracleParameter("p_dt", OracleType.VarChar)).Value = v_dt
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("std_trfc_brk.insertDetailCust failed")
        End Try



    End Sub
    Sub insert_dummy_reader1_brick()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        Dim v_curr_cnt As Integer = 0
        Dim v_new_cnt As Integer = 0
        Dim v_store_id As Integer = GetStoreId()

        'insert dummy pre_traffic
        conn.Open()
        sql = "Insert into pre_traffic " &
              " values ( " &
              "'" & v_store_id & "' ,1 " &
              " ,to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
              " ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - pre_traffic")
            Else
                Throw New Exception("Error inserting pre_traffic record. Please try again.")
            End If
        End Try
        conn.Close()

        'insert dummy traffic_detail - reader 1 (traffic)
        conn.Open()
        sql = "Insert into traffic_detail " &
              " values ( '" & str_drp_list.SelectedItem.Value.ToString() & "', 1" &
              " ,to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
              " ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - traffic_detail ")
            Else
                Throw New Exception("Error inserting traffic_detail record. Please try again.")
            End If
        End Try
        conn.Close()


    End Sub
    Function getSls(ByVal p_prod_grp_cd As String) As OracleNumber


        Dim v_sls As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "Select nvl(sum(wsa.d_s),0) sls " &
         " from   written_sales_arc wsa " &
        " where   wsa.prod_grp_cd ='" & p_prod_grp_cd & "'" &
        " and    wsa.WHEN = to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
        " and    wsa.store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_sls = dbReader.Item("sls")
            Else
                v_sls = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If IsDBNull(v_sls) Then
            Return 0
        Else
            Return v_sls
        End If

    End Function
    Sub InsertDummySummary()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        Dim v_curr_cnt As Integer = 0
        Dim v_new_cnt As Integer = 0
        Dim v_store_id As Integer = GetStoreId()

        Dim v_furn_sls As OracleNumber = getSls("1FURN")
        Dim v_appl_sls As OracleNumber = getSls("2APPL")
        Dim v_elec_sls As OracleNumber = getSls("3ELEC")
        Dim v_matt_sls As OracleNumber = getSls("4MATT")

        'insert dummy traffic_summary
        conn.Open()
        sql = "Insert into traffic_daily_summary " &
              " values ( '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              " ,to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
              " ,0,0,0,0,0,0,0,0,0)"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - traffic_daily_summary")
            Else
                Throw New Exception("Error inserting traffic_daily_summary record. Please try again.")
            End If
        End Try
        conn.Close()

    End Sub
    Sub CheckReader3AndSummary()

        If TrafficSummaryExists() <> "Y" Then
            InsertDummySummary()
        End If

        If Reader3Exists() <> "Y" Then
            insert_dummy_reader3_brick()
        End If

    End Sub
    Function AllowEdit(ByVal p_request_tp As String) As String

        Dim v_dt_diff As Integer = 0
        Dim v_oldest_detail_date As String = ""
        '------------------------------------------------------------------
        '----------------------- L E O N S---------------------------------
        '------------------------------------------------------------------
        'for leons can edit detail that falls within traffic parm date
        'for leons can edit summary older than 2 weeks
        If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
            v_dt_diff = DateDiff("d", Now, FormatDateTime(clndr_traffic_dt.SelectedDate, DateFormat.ShortDate))
            If p_request_tp = "D" Then
                If v_dt_diff < -14 Then
                    Return "N"
                Else
                    If Reader1Exists() = "Y" Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            Else
                If v_dt_diff < -14 Then
                    If Reader1Exists() = "Y" Then 'if there is detail no summ edit
                        Return "N"
                    Else
                        If TrafficSummaryExists() = "Y" Then
                            Return "Y"
                        Else
                            InsertDummySummary()
                            Return "Y"
                        End If
                    End If
                Else
                    Return "N"
                End If
            End If
        End If


        '    '------------------------------------------------------------------
        '    '-----------------------B R I C K------------------------
        '    '------------------------------------------------------------------
        '    'for brick can edit detail if date is >= min detail_date and < min when on brk_pre_traffic
        '    'for brick can edit summary if no detail exists and older than min detail_date
        If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) <> "Y" Then
            If p_request_tp = "D" Then
                If AllowBrickDetailEdit() = "Y" Then
                    If Reader1Exists() = "Y" Then
                        CheckReader3AndSummary()
                        Return "Y"
                    Else
                        'only for Brick add dummy detail (reader 1, 3)
                        insert_dummy_reader1_brick()
                        CheckReader3AndSummary()
                        Return "Y"
                    End If
                Else
                    Return "N"
                End If
            Else
                If AllowBrickSummaryEdit() = "Y" Then
                    If Reader1Exists() = "Y" Then
                        Return "N"
                    Else
                        If TrafficSummaryExists() = "Y" Then
                            Return "Y"
                        Else
                            InsertDummySummary()
                            Return "Y"
                        End If
                    End If
                Else
                    Return "N"
                End If
            End If
        End If

    End Function

    Protected Sub GetHourlyData()

        If str_drp_list.SelectedIndex < 1 Then
            str_drp_list.Focus()
            lbl_msg.Text = "Please select a store to continue"
            Exit Sub
        End If

        lbl1_dt.Text = "  Traffic Date: " & traffic_dt_txt.Text
        lbl1_store_cd.Text = "Store: " & str_drp_list.SelectedItem.Value.ToString()
        lbl2_dt.Text = "  Traffic Date: " & traffic_dt_txt.Text
        lbl2_store_cd.Text = "Store: " & str_drp_list.SelectedItem.Value.ToString()

        If AllowEdit("D") = "N" Then
            ASPxPopupHourlyBrowse.ShowOnPageLoad = True
            get_traffic_data("B") 'browse only
        Else
            ASPxPopupHourly.ShowOnPageLoad = True
            get_traffic_data("E") 'can edit
        End If


    End Sub
    Protected Sub GetSummaryData()

        Dim v_dt_diff As Integer = 0

        If str_drp_list.SelectedIndex < 1 Then
            str_drp_list.Focus()
            lbl_msg.Text = "Please select a store to continue"
            Exit Sub
        End If


        v_dt_diff = DateDiff("d", Now, FormatDateTime(clndr_traffic_dt.SelectedDate, DateFormat.ShortDate))
        If v_dt_diff < 0 Then
            'continue
        Else
            'future date not allowed
            lbl_msg.Text = "Invalid Date - No traffic data exists for this store/date"
            Exit Sub
        End If

        summ_dt.Text = traffic_dt_txt.Text
        summ_str.Text = str_drp_list.SelectedItem.Value.ToString()
        ASPxPopupSummary.ShowOnPageLoad = True

        If AllowEdit("S") = "Y" Then
            summ_curr_count.Text = get_traffic_summ()
            summ_new_count.Text = ""
            summ_new_count.Enabled = True
            summ_upd_btn.Visible = True
            summ_clear_btn.Visible = True
        Else 'browse only
            If TrafficSummaryExists() = "Y" Then
                summ_curr_count.Text = get_traffic_summ()
            Else
                summ_curr_count.Text = 0
                lbl_msg.Text = "No traffic summary exists for this store-date"
            End If
            summ_new_count.Text = ""
            summ_new_count.Enabled = False
            summ_upd_btn.Visible = False
            summ_clear_btn.Visible = False
        End If


    End Sub
    Protected Function GetAvgCnt(ByVal p_str As String, ByVal p_hr_cd As String, ByVal p_wk1 As String, ByVal p_wk2 As String, ByVal p_wk3 As String, ByVal p_wk4 As String, ByVal p_wk5 As String, ByVal p_wk6 As String) As Integer


        Dim v_avg As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " select round(avg(p2." & p_hr_cd & ") ) avg " &
                           "    from pre_traffic p2 " &
                           "   where p2.store = " & p_str &
                           "     and p2.when in ( to_date('" & p_wk1 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & p_wk2 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & p_wk3 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & p_wk4 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & p_wk5 & "','mm/dd/RRRR'), " &
                           "                      to_date('" & p_wk6 & "','mm/dd/RRRR') )  "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_avg = dbReader.Item("avg")
            Else
                v_avg = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_avg & "" = "" Then
            Return 0
        Else
            Return v_avg
        End If

    End Function
    Protected Function GetCurrCnt(ByVal p_str As String, ByVal p_hr_cd As String) As Integer


        Dim v_cnt As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " select nvl(" & p_hr_cd & ", 0) cnt " &
                           "    from pre_traffic p2 " &
                           "   where p2.store = " & p_str &
                           "   and p2.when = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_cnt & "" = "" Then
            Return 0
        Else
            Return v_cnt
        End If

    End Function
    Protected Function GetOrigSummaryCnt() As Integer


        Dim v_cnt As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " select nvl(new_cnt,0) cnt " &
                           "    from traffic_summ_adjust " &
                           "   where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                           "   and adjust_dt = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_cnt & "" = "" Then
            Return 0
        Else
            Return v_cnt
        End If

    End Function
    Protected Sub hrly_btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        lbl_msg2.Text = ""
        GetHourlyData()

    End Sub
    Protected Sub summ_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lbl3_msg.Text = ""
        GetSummaryData()
    End Sub
    Protected Sub saveAvg_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim v_hr As String = ""
        Dim v_hr_cd As String = ""
        Dim v_new_cnt As String = ""
        Dim v_curr_cnt As String = ""
        Dim v_avg_cnt As String = ""
        Dim v_upd_sql As String = ""
        Dim v_hr_ctr As Integer = 0
        Dim v_update As String = ""
        Dim v_comma As String = " "
        Dim v_ctr As Integer = 0
        Dim v_store_id As String = GetStoreId()

        If GridView1.VisibleRowCount < 1 Then
            lbl_msg.Text = "No Data to update"
            lbl_msg2.Text = "No Data to update"
            Exit Sub
        End If


        For i As Integer = 0 To GridView1.VisibleRowCount - 1
            If i < 10 Then
                v_hr = "0" & i.ToString
            Else
                v_hr = i.ToString
            End If
            v_hr_cd = "HR" & v_hr

            Try
                If GridView1.GetRowValues(i, "AVG_CNT") & "" = "" Then
                    v_avg_cnt = "0"
                Else
                    v_avg_cnt = GridView1.GetRowValues(i, "AVG_CNT")
                End If

                If GridView1.GetRowValues(i, "CURR_CNT") & "" = "" Then
                    v_curr_cnt = "0"
                Else
                    v_curr_cnt = GridView1.GetRowValues(i, "CURR_CNT")
                End If

                v_new_cnt = GridView1.GetRowValues(i, "AVG_CNT")


            Catch ex As Exception
            End Try

            If v_curr_cnt <> v_new_cnt Then
                If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
                    'hr_cd has HR
                    log_dtl_adjustment_leons(v_hr_cd, v_new_cnt)
                Else
                    v_hr_ctr = v_hr_ctr + 1
                    If v_hr_ctr > 1 Then
                        v_comma = " , "
                    End If
                    v_upd_sql = v_upd_sql & v_comma & " " & v_hr_cd & " = nvl(" & v_avg_cnt & ",0) "
                    log_dtl_adjustment_brick(v_hr_cd, v_curr_cnt, v_avg_cnt)
                End If
            End If


        Next

        If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
            lbl_msg2.Text = "Average count is logged and will be applied by tonight's batch run"
            get_traffic_data("E")
            Exit Sub
        End If

        If v_hr_ctr > 0 Then
            v_update = " update traffic_detail set " & v_upd_sql &
                         " where detail_date = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                         "   and reader = 1 " &
                         "   and store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' "
            do_update(v_update)
            v_update = " update pre_traffic set " & v_upd_sql &
                        " where when = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                        "   and store = " & v_store_id
            do_update(v_update)
            Dim v_new_summ As Integer = GetTotalTraffic()
            update_traffic_summary(v_new_summ)

        End If

        get_traffic_data("E")
        lbl_msg2.Text = "**** Traffic Count is Updated **** "



    End Sub
    Protected Sub Clear_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        str_drp_list.SelectedIndex = 0
        clndr_traffic_dt.SelectedDate = DateTime.Now
        traffic_dt_txt.Text = FormatDateTime(clndr_traffic_dt.SelectedDate, DateFormat.ShortDate)

        GridView1.DataSource = ""
        GridView1.DataBind()

        GridView9.DataSource = ""
        GridView9.DataBind()

        ASPxPopupHourly.ShowOnPageLoad = False
        ASPxPopupHourlyBrowse.ShowOnPageLoad = False
        ASPxPopupSummary.ShowOnPageLoad = False

        lbl1_dt.Text = ""
        lbl1_store_cd.Text = ""
        lbl_msg.Text = ""

    End Sub

    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        If e.Column.FieldName = "HR" Or e.Column.FieldName = "CURR_CNT" Or e.Column.FieldName = "AVG_CNT" Then
            e.Editor.ReadOnly = True
        Else
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        End If

    End Sub
    Protected Sub rowvalidation(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        Dim v_hr_cd As String = ""


        v_hr_cd = "HR" & e.OldValues("HR").ToString().Replace(" ", "")
        If e.NewValues("NEW_CNT") Is Nothing Or e.NewValues("NEW_CNT") = " " Then
            If e.OldValues("NEW_CNT") Is Nothing Then
                'do nothing
            Else
                If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
                    delete_adjustment_leons(v_hr_cd)
                End If
            End If
        ElseIf Not IsNumeric(e.NewValues("NEW_CNT").ToString()) Then
            e.RowError = "Invalid Value"
            Exit Sub
        Else
            If e.NewValues("NEW_CNT").ToString() <> e.OldValues("NEW_CNT").ToString() Then
                If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
                    log_dtl_adjustment_leons(v_hr_cd, e.NewValues("NEW_CNT").ToString())
                End If
            End If
        End If

        e.RowError = ""
        e.Keys.Clear()

    End Sub
    Protected Sub GridView1_BatchUpdate(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs)

        Dim v_upd_sql As String = ""
        Dim v_update As String = ""
        Dim v_summ_cnt As Integer = 0
        Dim v_store_id As String = GetStoreId()
        Dim v_hr_cd As String = ""
        Dim v_new_cnt As String = ""
        Dim v_upd_sw As String = ""
        Dim v_ctr As Integer = 0
        Dim v_comma As String = ""

        If isLeonStr(str_drp_list.SelectedItem.Value.ToString()) = "Y" Then
            get_traffic_data("E")
            GridView1.Settings.ShowTitlePanel = True
            GridView1.SettingsText.Title = "Traffic count is logged and will be updated by tonight's Batch run"
            e.Handled = True
            e.UpdateValues.Clear()

        Else
            If e.UpdateValues.Count = 0 Then
                e.Handled = True
                e.UpdateValues.Clear()
                Exit Sub
            End If

            v_upd_sw = "Y"
            For Each args In e.UpdateValues
                v_ctr = v_ctr + 1
                If Len(args.OldValues("HR")) < 2 Then
                    v_hr_cd = "HR0" & Replace(args.OldValues("HR").ToString(), " ", "")
                Else
                    v_hr_cd = "HR" & Replace(args.OldValues("HR").ToString(), " ", "")
                End If

                log_dtl_adjustment_brick(v_hr_cd, args.OldValues("CURR_CNT").ToString(), args.NewValues("NEW_CNT").ToString())

                If v_ctr = 1 Then
                    v_comma = ""
                Else
                    v_comma = ","
                End If

                v_upd_sql = v_upd_sql & v_comma & " " & v_hr_cd & " = nvl(" & args.NewValues("NEW_CNT").ToString & ",0) "

            Next args

            If v_upd_sw = "Y" Then
                v_update = " update traffic_detail set " & v_upd_sql &
                            " where detail_date = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                            "   and reader = 1 " &
                            "   and store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' "
                do_update(v_update)
                v_update = " update pre_traffic set " & v_upd_sql &
                            " where when = to_date ('" & FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
                            "   and store = " & v_store_id
                do_update(v_update)
                Dim v_new_summ As Integer = GetTotalTraffic()
                update_traffic_summary(v_new_summ)

                get_traffic_data("E")
                e.Handled = True
                e.UpdateValues.Clear()
                GridView1.Settings.ShowTitlePanel = True
                GridView1.SettingsText.Title = "*** Traffic count update is complete ***"
            End If

        End If

    End Sub
    Function formatDT(ByVal p_dt As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "select to_char(to_date('" & p_dt & "','mm/dd/RRRR'),'dd-mon-yyyy') dt from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Protected Sub log_dtl_adjustment_leons(ByVal p_hr_cd As String, p_new_cnt As String)

        Dim v_dt As String = formatDT(FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate))
        Dim v_hr_cd As String = p_hr_cd.ToString().Replace(" ", "")
        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_trfc.setFrmStr"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_in", OracleType.VarChar)).Value = str_drp_list.SelectedItem.Value.ToString()
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(setFrmStr)")
        End Try


        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD1 As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD1.Connection = objConnection
            myCMD1.CommandText = "std_trfc.setFrmDt"
            myCMD1.CommandType = CommandType.StoredProcedure
            myCMD1.Parameters.Add(New OracleParameter("p_in", OracleType.VarChar)).Value = v_dt
            myCMD1.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(setFrmDt)")
        End Try

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD2 As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD2.Connection = objConnection
            myCMD2.CommandText = "std_trfc.setFrmHrCd"
            myCMD2.CommandType = CommandType.StoredProcedure
            myCMD2.Parameters.Add(New OracleParameter("p_in", OracleType.VarChar)).Value = v_hr_cd
            myCMD2.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(setFrmHrCd)")
        End Try

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD3 As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD3.Connection = objConnection
            myCMD3.CommandText = "std_trfc.setFrmCnt"
            myCMD3.CommandType = CommandType.StoredProcedure
            myCMD3.Parameters.Add(New OracleParameter("p_in", OracleType.Number)).Value = CDec(p_new_cnt)
            myCMD3.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(setFrmCnt)")
        End Try

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD4 As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD4.Connection = objConnection
            myCMD4.CommandText = "std_trfc.setFrmUsr"
            myCMD4.CommandType = CommandType.StoredProcedure
            myCMD4.Parameters.Add(New OracleParameter("p_in", OracleType.VarChar)).Value = Session("emp_init")
            myCMD4.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(setFrmUsr)")
        End Try

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD5 As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD5.Connection = objConnection
            myCMD5.CommandText = "std_trfc.CreChgLog"
            myCMD5.CommandType = CommandType.StoredProcedure
            myCMD5.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(CreChgLog)")
        End Try


    End Sub
    Protected Sub delete_adjustment_leons(ByVal p_hr_cd As String)

        Dim v_dt As String = formatDT(FormatDateTime(traffic_dt_txt.Text, DateFormat.ShortDate))

        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()


        'note p_flg Y indicates delete audit
        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_traf_audit.updhour"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = str_drp_list.SelectedItem.Value.ToString()
            myCMD.Parameters.Add(New OracleParameter("p_dt", OracleType.VarChar)).Value = v_dt
            myCMD.Parameters.Add(New OracleParameter("p_hr", OracleType.VarChar)).Value = p_hr_cd
            myCMD.Parameters.Add(New OracleParameter("p_flg", OracleType.VarChar)).Value = "Y"   'this flag indicates delete adjustment
            myCMD.Parameters.Add(New OracleParameter("p_nw_value", OracleType.VarChar)).Value = 0
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Adjustment Failed-press clear and try again(updhour)")
        End Try

    End Sub
    Protected Sub do_update(ByVal p_sql As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = p_sql

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error-Update Failed. Please try again.")
            conn.Close()
        End Try
        conn.Close()


    End Sub
    Protected Function GetTotalTraffic() As Integer

        Dim v_sum As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT nvl(hr00,0)+nvl(hr01,0)+nvl(hr02,0)+nvl(hr03,0)+nvl(hr04,0)+ " &
                            "       nvl(hr05,0)+nvl(hr06,0)+nvl(hr07,0)+nvl(hr08,0)+nvl(hr09,0)+ " &
                            "       nvl(hr10,0)+nvl(hr11,0)+nvl(hr12,0)+nvl(hr13,0)+nvl(hr14,0)+ " &
                            "       nvl(hr15,0)+nvl(hr16,0)+nvl(hr17,0)+nvl(hr18,0)+nvl(hr19,0)+ " &
                            "       nvl(hr20,0)+nvl(hr21,0)+nvl(hr22,0)+nvl(hr23,0) trf " &
                            "  from traffic_detail " &
                            " where reader = 1 and store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
                            "   and trunc(detail_date) = to_date('" & traffic_dt_txt.Text & "','mm/dd/RRRR') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_sum = dbReader.Item("trf")
            Else
                v_sum = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_sum & "" = "" Then
            Return 0
        Else
            Return v_sum
        End If

    End Function
    Sub update_traffic_summary(ByVal p_new_summ As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update traffic_daily_summary " &
              "  set sum_reader_1 = nvl(" & p_new_summ & ",0) " &
              " where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              "   and trunc(summary_date) = to_date('" & traffic_dt_txt.Text & "','mm/dd/RRRR') "
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating traffic detail. Please try again.")
            conn.Close()
        End Try
        conn.Close()




    End Sub
    Sub log_dtl_adjustment_brick(ByVal p_hr As String, ByVal p_curr_cnt As String, ByVal p_new_cnt As String)

        If Brick_dtl_log_exists(p_hr, p_curr_cnt, p_new_cnt) = "Y" Then
            update_dtl_log_brick(p_hr, p_curr_cnt, p_new_cnt)
        Else
            insert_dtl_log_brick(p_hr, p_curr_cnt, p_new_cnt)
        End If

    End Sub
    Sub log_summary_adjustment(ByVal p_new_cnt As String)

        If Summary_log_exists(p_new_cnt) = "Y" Then
            update_summary_log(p_new_cnt)
        Else
            insert_summary_log(p_new_cnt)
        End If

    End Sub
    Sub insert_dtl_log_brick(ByVal p_hr As String, ByVal p_curr_cnt As String, ByVal p_new_cnt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim v_hr = p_hr.Replace(" ", "")
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_curr_cnt As Integer = 0
        Dim v_new_cnt As Integer = 0

        If p_curr_cnt Is Nothing Then
            v_curr_cnt = 0
        ElseIf Not IsNumeric(p_curr_cnt) Then
            v_curr_cnt = 0
        Else
            v_curr_cnt = p_curr_cnt
        End If

        If p_new_cnt Is Nothing Then
            v_new_cnt = 0
        ElseIf Not IsNumeric(p_new_cnt) Then
            v_new_cnt = 0
        Else
            v_new_cnt = p_new_cnt
        End If

        sql = "Insert into traffic_trf_adjust (store_cd,adjust_dt,hour_cd,orig_cnt,new_cnt,create_cd,create_dt,update_cd,update_dt)" &
              " values ( " &
              "'" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              " ,to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
              " ,'" & v_hr & "' " &
              " ,nvl(" & v_curr_cnt & ",0), nvl(" & v_new_cnt & ",0) " &
              " ,'" & Session("emp_init") & "' " &
              " ,to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" &
              ", null, null) "


        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - traffic_trf_adjust")
            Else
                Throw New Exception("Error inserting traffic_trf_adjust record. Please try again.")
            End If
        End Try
        conn.Close()

    End Sub
    Sub update_dtl_log_brick(ByVal p_hr As String, ByVal p_curr_cnt As String, ByVal p_new_cnt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim v_curr_cnt As Integer = 0
        Dim v_new_cnt As Integer = 0

        If p_curr_cnt Is Nothing Then
            v_curr_cnt = 0
        ElseIf Not IsNumeric(p_curr_cnt) Then
            v_curr_cnt = 0
        Else
            v_curr_cnt = p_curr_cnt
        End If

        If p_new_cnt Is Nothing Then
            v_new_cnt = 0
        ElseIf Not IsNumeric(p_new_cnt) Then
            v_new_cnt = 0
        Else
            v_new_cnt = p_new_cnt
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update traffic_trf_adjust " &
              "  set orig_cnt = " & v_curr_cnt &
              "    , new_cnt = " & v_new_cnt &
              "    , update_cd = '" & Session("emp_init") & "' " &
              "    , update_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" &
              "  where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              "    and adjust_dt = to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR') " &
              "    and hour_cd = '" & p_hr & "' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating traffic_trf_adjust")
            conn.Close()
        End Try
        conn.Close()


    End Sub
    Function Brick_dtl_log_exists(ByVal p_hr As String, ByVal p_curr_cnt As String, ByVal p_new_cnt As String) As String

        Dim v_cnt As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from traffic_trf_adjust " &
                            " where store_cd = '" & str_drp_list.SelectedItem.Value.ToString & "' " &
                            "   and adjust_dt = to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR') " &
                            "   and hour_cd = '" & p_hr & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_cnt & "" = "" Then
            Return "N"
        ElseIf v_cnt > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Sub update_summary_log(ByVal p_new_cnt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim v_orig_cnt As String = summ_curr_count.Text
        Dim v_new_cnt As Integer = 0

        If p_new_cnt Is Nothing Then
            v_new_cnt = 0
        ElseIf Not IsNumeric(p_new_cnt) Then
            v_new_cnt = 0
        Else
            v_new_cnt = p_new_cnt
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update traffic_summ_adjust " &
              "  set orig_cnt = nvl(" & v_orig_cnt & " ,0) " &
              "    , new_cnt = " & v_new_cnt &
              "    , update_cd = '" & Session("emp_init") & "' " &
              "    , update_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" &
              "  where store_cd = '" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              "    and adjust_dt = to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR') "


        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating traffic_summ_adjust")
            conn.Close()
        End Try
        conn.Close()


    End Sub
    Sub insert_summary_log(ByVal p_new_cnt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_orig_cnt As String = summ_curr_count.Text
        Dim v_curr_cnt As Integer = 0
        Dim v_new_cnt As Integer = 0

        If p_new_cnt Is Nothing Then
            v_new_cnt = 0
        ElseIf Not IsNumeric(p_new_cnt) Then
            v_new_cnt = 0
        Else
            v_new_cnt = p_new_cnt
        End If

        sql = "Insert into traffic_summ_adjust (store_cd,adjust_dt,orig_cnt,new_cnt,create_cd,create_dt,update_cd,update_dt)" &
              " values ( " &
              "'" & str_drp_list.SelectedItem.Value.ToString() & "' " &
              " ,to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR')" &
              " ,nvl(" & v_orig_cnt & ",0), nvl(" & v_new_cnt & ",0) " &
              " ,'" & Session("emp_init") & "' " &
              " ,to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" &
              ", null, null) "


        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - traffic_summ_audit")
            Else
                Throw New Exception("Error inserting traffic_summ_audit record. Please try again.")
            End If
        End Try
        conn.Close()

    End Sub
    Function Summary_log_exists(ByVal p_new_cnt As String) As String

        Dim v_cnt As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from traffic_summ_adjust " &
                            " where store_cd = '" & str_drp_list.SelectedItem.Value.ToString & "' " &
                            "   and adjust_dt = to_date ('" & FormatDateTime(clndr_traffic_dt.SelectedDate, 2) & "','mm/dd/RRRR') "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_cnt & "" = "" Then
            Return "N"
        ElseIf v_cnt > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Sub summ_upd_btn_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles summ_upd_btn.Click

        If summ_new_count.Text & "" = "" Then
            lbl3_msg.Text = "Please enter a value to continue"
            summ_new_count.Focus()
            Exit Sub
        End If

        If Not IsNumeric(summ_new_count.Text) Then
            lbl3_msg.Text = "Invalid Value - please try again"
            summ_new_count.Focus()
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String


        update_traffic_summary(summ_new_count.Text)
        log_summary_adjustment(summ_new_count.Text)


        lbl3_msg.Text = "Traffic Summary is updated"
        summ_curr_count.Text = summ_new_count.Text
        summ_new_count.Text = ""


    End Sub
    Protected Sub summ_clear_btn_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles summ_clear_btn.Click

        summ_new_count.Text = ""
        lbl3_msg.Text = ""

    End Sub


End Class
