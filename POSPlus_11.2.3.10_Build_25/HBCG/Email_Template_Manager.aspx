<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Email_Template_Manager.aspx.vb"
    Inherits="Email_Template_Manager" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="admin_form" runat="server">
        <table>
            <tr>
                <td valign="top" colspan="2" style="height: 24px">
                    <table>
                        <tr>
                            <td valign="top">
                                Template:
                                <asp:DropDownList ID="cbo_template" runat="server" AutoPostBack="True" CssClass="style5"
                                    Width="513px">
                                </asp:DropDownList>
                            </td>
                            <td valign="top">
                                <dx:ASPxButton ID="btn_add_new" runat="server" Text="Add New" OnClick="btn_add_new_Click">
                                </dx:ASPxButton>
                            </td>
                            <td valign="top">
                                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Save Changes" OnClick="btn_submit_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
        <asp:Label ID="lbl_error" runat="server" Width="100%" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; From Field:
                </td>
                <td align="left" valign="top">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; To Field:
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBoxList ID="chk_list_from" runat="server" Width="195px" CssClass="style5">
                        <asp:ListItem Value="SLSP1_EMAIL">Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="SLSP2_EMAIL">Second Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="CUST_EMAIL" Enabled="False">Customer Email</asp:ListItem>
                        <asp:ListItem Value="CUST_SHIP_EMAIL" Enabled="False">Customer Ship To Email</asp:ListItem>
                        <asp:ListItem Value="ALLOW_FREEFORM" Enabled="False">Allow Freeform Entry</asp:ListItem>
                        <asp:ListItem Value="OTHER">Other:</asp:ListItem>
                    </asp:CheckBoxList>
                    &nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_from" runat="server" CssClass="style5" Width="306px"
                        MaxLength="250"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBoxList ID="chk_list_to" runat="server" CssClass="style5">
                        <asp:ListItem Value="SLSP1_EMAIL">Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="SLSP2_EMAIL">Second Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="CUST_EMAIL">Customer Email</asp:ListItem>
                        <asp:ListItem Value="CUST_SHIP_EMAIL">Customer Ship To Email</asp:ListItem>
                        <asp:ListItem Value="ALLOW_FREEFORM">Allow Freeform Entry</asp:ListItem>
                        <asp:ListItem Value="OTHER">Other:</asp:ListItem>
                    </asp:CheckBoxList>
                    &nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_to" runat="server" CssClass="style5" Width="306px"
                        MaxLength="2500"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; CC Field:
                </td>
                <td align="left" valign="top">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; BCC Field:
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBoxList ID="chk_list_cc" runat="server" Width="192px" CssClass="style5"
                        OnSelectedIndexChanged="chk_list_cc_SelectedIndexChanged">
                        <asp:ListItem Value="SLSP1_EMAIL">Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="SLSP2_EMAIL">Second Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="CUST_EMAIL">Customer Email</asp:ListItem>
                        <asp:ListItem Value="CUST_SHIP_EMAIL">Customer Ship To Email</asp:ListItem>
                        <asp:ListItem Value="ALLOW_FREEFORM">Allow Freeform Entry</asp:ListItem>
                        <asp:ListItem Value="DONT_ALLOW">Don't Allow</asp:ListItem>
                        <asp:ListItem Value="OTHER">Other:</asp:ListItem>
                    </asp:CheckBoxList>&nbsp;&nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_cc" runat="server"
                        CssClass="style5" Width="306px" MaxLength="2500"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBoxList ID="chk_list_bcc" runat="server" CssClass="style5" AutoPostBack="true"
                        OnSelectedIndexChanged="chk_list_bcc_SelectedIndexChanged">
                        <asp:ListItem Value="SLSP1_EMAIL">Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="SLSP2_EMAIL">Second Salesperson Email</asp:ListItem>
                        <asp:ListItem Value="CUST_EMAIL">Customer Email</asp:ListItem>
                        <asp:ListItem Value="CUST_SHIP_EMAIL">Customer Ship To Email</asp:ListItem>
                        <asp:ListItem Value="ALLOW_FREEFORM">Allow Freeform Entry</asp:ListItem>
                        <asp:ListItem Value="DONT_ALLOW">Don't Allow</asp:ListItem>
                        <asp:ListItem Value="OTHER">Other:</asp:ListItem>
                    </asp:CheckBoxList>
                    &nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_bcc" runat="server" CssClass="style5" Width="306px"
                        MaxLength="2500"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; Subject: &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<asp:CheckBox
                        ID="chk_subject_allow_ff" runat="server" CssClass="style5" Text="Allow Freeform?" />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_subject" runat="server" CssClass="style5"
                        MaxLength="250" Width="596px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="images/icons/Envelope.png" style="width: 33px; height: 24px" />
                    &nbsp; Body:&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:CheckBox ID="chk_body_allow_ff" runat="server" CssClass="style5" Text="Allow Freeform?" />&nbsp;<br />
                    <dx:ASPxHtmlEditor ID="body_editor" runat="server" Width="630px">
                    </dx:ASPxHtmlEditor>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <dx:ASPxButton ID="btn_submit" runat="server" Text="Save Changes" OnClick="btn_submit_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="Add New Email Template" Height="104px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="515px">
        <ModalBackgroundStyle BackColor="#E0E0E0" />
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Label ID="Label1" runat="server" CssClass="style5" Text="This will add a new email template.  All settings below will be reset for the new template.  Please select cancel to exit out without saving changes.<br />"></asp:Label>
                <br />
                <asp:Label ID="Label4" CssClass="style5" Text="Template Name: &nbsp;" runat="server"></asp:Label>
                <asp:TextBox ID="txt_template_name" runat="server" Width="329px" CssClass="style5"></asp:TextBox>
                <br />
                <br />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button
                    ID="btn_save" runat="server" Text="Save Template" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click" Text="Cancel Template" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</asp:Content>
