Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization


Partial Class LoadADFP
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Load_ADFP(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '           objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
            '              "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
            '             "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
            '            "from ITM_GENERATION_EXT a "
            ' June 27 as per Nick
            objSql.CommandText = "select distinct co_cd,year,promotion_code,flyer_format,flap,page#,sku, " &
                "decode(sku,null,'sku cannot be null.', ' ') || " &
                "decode(sign(length(trim(flyer_format))-10), 1, 'Flyer format exceeds 10 characters', ' ') || " &
                "decode(sign(length(trim(promotion_code))-3), 1, 'promotion_code exceeds 3 characters', ' ') || " &
               "decode(sign(length(trim(co_cd))-3), 1, 'Co code exceeds 3 characters', ' ') || " &
                "decode(sign(length(trim(year))-4), 1, 'Year exceeds 4 numbers', ' ') || " &
                "decode(sign(length(trim(page#))-4), 1, 'Page# exceeds 4 numbers', ' '  ) err_msg " &
                "from ADFP_GENERATION_EXT a "


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            btn_upload.Visible = False
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = True
                lbl_msg.Text = "File Loaded - Please process the Records."

            Else
                btn_process.Visible = False
                btn_upload.Visible = True
                lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process.Visible = False
            btn_upload.Visible = True
            lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Process_Items(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            Dim emp_init As String = Session("emp_init")

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select co_cd,year,promotion_code,flyer_format,flap,page#,sku, NVL(err_msg, ' ') err_msg  from adfp_creation a "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = False
                lbl_msg.Text = "Records's successfully uploaded."

            Else
                btn_process.Visible = False
                btn_upload.Visible = True
                lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub btn_process_Click(sender As Object, e As EventArgs) Handles btn_process.Click
        ' Validate and process
        Dim result = validate_items()

        If result = "Y" Then
            Process_Items(sender, e)
        End If

        If result = "N" Then
            lbl_msg.Text = "Errors found. Please correct the data and uploadPlease process the file file again."
        End If

    End Sub

    Public Function validate_items() As String

        Dim emp_init As String = Session("emp_init")
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_upload_adfp.validate_items"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = emp_init

            myCMD.ExecuteNonQuery()

            'Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

            'MyDA.Fill(Ds)
            'myCMD.Cancel()
            'myCMD.Dispose()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Protected Sub btn_upload_Click(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Try
            If FileUpload1.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "adfp_gen.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadADFP\done\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadADFP\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label1.Text = "File name: " & _
                          FileUpload1.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload1.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadADFP\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        theSystemBiz.SaveAuditLogComment("LOAD_ADFP", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label1.Text = Label1.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        btn_upload.Visible = False

                        ' Display data from external table
                        Load_ADFP(sender, e)

                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label1.Text = "Only .csv files allowed!"
                End If
            Else
                Label1.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_ADFP", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        ' Clear
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)

    End Sub

    'HOW TO USE:

    ' Upload file using FTP
    'UploadFile("c:\UploadFile.doc", "ftp://FTPHostName/UploadPath/UploadFile.doc", "UserName", "Password")

End Class
