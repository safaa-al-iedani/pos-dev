Imports System.Data.OracleClient
Imports System.IO
Imports System.Collections.Generic
Imports DevExpress.XtraReports.UI
Imports HBCG_Utils

Partial Class Price_Tag
    Inherits POSBasePage
    Private salesBiz As SalesBiz = New SalesBiz()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim PTAG_Access As String = AppUtils.Decrypt(Get_Connection_String("/HandleCode/HBCG/PCF_Config", "Ident_String"), "CrOcOdIlE")
            If PTAG_Access <> "HIGHLANDS" Then
                txt_gtag.Enabled = False
                txt_ptag.Enabled = False
                ASPxPageControl1.Enabled = False
                Button1.Enabled = False
                lbl_information.Text = "This functionality is not currently licensed to your site"
                lbl_information.ForeColor = Color.Red
                lbl_information.Font.Size = 12
                Exit Sub
            End If
            LoadCombos()
        End If

    End Sub


    Public Sub GenerateStoreTags()

        If Not String.IsNullOrEmpty(cbo_store_store_cd.Value) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Try
                Dim oAdp As OracleDataAdapter
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim objSql As OracleCommand
                Dim sql As String

                conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
                conn.Open()

                sql = "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC,  " &
                          "NVL(a.RET_PRC,0.00) AS RET_PRC, a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, b.QTY, b.CRPT_ID_NUM AS COMBO_NUM, "
                sql = sql & "b.STORE_CD, b.LOC_CD, a.MNR_CD, a.CAT_CD, a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, "
                sql = sql & "NULL As FEATURES_BENEFITS, a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY, a.PO_LEAD_TIME, b.RCV_DT, "
                sql = sql & "a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, a.SPIFF, a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, "
                sql = sql & "NULL AS IST_PO_CD, NULL AS IST_PO_FINAL_DT, NULL AS IST_PO_SEQ_NO, NULL AS IST_PO_SRT_CD, "
                sql = sql & "NULL AS IST_PO_WR_DT,  NULL AS PROMOTION_CD, NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, "
                sql = sql & "NULL AS OUTLET_NAS_ID, NULL AS ITM_COMMENT,  NULL AS CURRENT_PRC, NULL AS RF_ID_CD "
                sql = sql & "FROM ITM a, ITM_FIFL b "
                sql = GetStoreSortCodeAndPriceQuery(sql)

                If Not String.IsNullOrEmpty(cbo_store_loc_cd.Value) Then
                    sql = sql & "AND b.LOC_CD=:LOC_CD "
                End If
                If Not String.IsNullOrEmpty(txt_store_ve_cd.Text) Then
                    sql = sql & "AND a.VE_CD=:VE_CD "
                End If

                sql = sql & " UNION " &
                "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC, " &
                "NVL(a.RET_PRC,0.00) AS RET_PRC, a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, b.QTY, b.CRPT_ID_NUM AS COMBO_NUM, "
                sql = sql & "b.STORE_CD, b.LOC_CD, a.MNR_CD, a.CAT_CD, a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, "
                sql = sql & "NULL As FEATURES_BENEFITS, a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY, a.PO_LEAD_TIME, b.ENTRY_DT, "
                sql = sql & "a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, a.SPIFF, a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, "
                sql = sql & "NULL AS IST_PO_CD, NULL AS IST_PO_FINAL_DT, NULL AS IST_PO_SEQ_NO, NULL AS IST_PO_SRT_CD, "
                sql = sql & "NULL AS IST_PO_WR_DT, NULL AS PROMOTION_CD, NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, "
                sql = sql & "b.ID_CD AS OUTLET_NAS_ID, NULL AS ITM_COMMENT, NULL AS CURRENT_PRC, NULL AS RF_ID_CD "
                sql = sql & "FROM ITM a, NAS b "
                sql = GetStoreSortCodeAndPriceQuery(sql)

                If Not String.IsNullOrEmpty(cbo_store_loc_cd.Value) Then
                    sql = sql & "AND b.LOC_CD=:LOC_CD "
                End If
                If Not String.IsNullOrEmpty(txt_store_ve_cd.Text) Then
                    sql = sql & "AND a.VE_CD=:VE_CD "
                End If
                sql = sql & " UNION " &
                "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC, " &
                "NVL(a.RET_PRC,0.00) AS RET_PRC, a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, b.QTY, b.CRPT_ID_NUM AS COMBO_NUM, "
                sql = sql & "b.STORE_CD, b.LOC_CD, a.MNR_CD, a.CAT_CD, a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, "
                sql = sql & "NULL As FEATURES_BENEFITS, a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY, a.PO_LEAD_TIME, b.ENTRY_DT, "
                sql = sql & "a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, a.SPIFF, a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, "
                sql = sql & "NULL AS IST_PO_CD, NULL AS IST_PO_FINAL_DT, NULL AS IST_PO_SEQ_NO, NULL AS IST_PO_SRT_CD, "
                sql = sql & "NULL AS IST_PO_WR_DT, NULL AS PROMOTION_CD, NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, "
                sql = sql & "b.ID_CD AS OUTLET_NAS_ID, NULL AS ITM_COMMENT, NULL AS CURRENT_PRC, NULL AS RF_ID_CD "
                sql = sql & "FROM ITM a, OUT b "
                sql = GetStoreSortCodeAndPriceQuery(sql)

                If Not String.IsNullOrEmpty(cbo_store_loc_cd.Value) Then
                    sql = sql & "AND b.LOC_CD=:LOC_CD "
                End If
                If Not String.IsNullOrEmpty(txt_store_ve_cd.Text) Then
                    sql = sql & "AND a.VE_CD=:VE_CD "
                End If

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
                objSql.Parameters(":STORE_CD").Value = cbo_store_store_cd.Value

                If Not String.IsNullOrEmpty(cbo_store_loc_cd.Value) Then
                    objSql.Parameters.Add(":LOC_CD", OracleType.VarChar)
                    objSql.Parameters(":LOC_CD").Value = cbo_store_loc_cd.Value
                End If
                If Not String.IsNullOrEmpty(txt_store_ve_cd.Text) Then
                    objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
                    objSql.Parameters(":VE_CD").Value = UCase(txt_store_ve_cd.Text)
                End If

                BindSortCodeAndPriceParams(objSql, txt_store_sort_cd_from.Text, txt_store_sort_cd_to.Text,
                                                   dt_store_ret_prc_from.Value, dt_store_ret_prc_to.Value,
                                                   dt_store_adv_prc_from.Value, dt_store_adv_prc_to.Value)

                ' ** Prepares to retrieve data
                Dim ds As DataSet = New DataSet
                Dim ds_new As DataSet = New DataSet
                Dim dt, dt_new As DataTable
                Dim dr, dr_new As DataRow

                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds, "PRICE_TAGS")
                oAdp.Fill(ds_new, "PRICE_TAGS")

                dt = ds.Tables(0)
                dt_new = ds_new.Tables(0)
                dt_new.Clear()  'clears it because only the table structure is needed, not the contents

                ' *** Retrieves the addional data for each SKU found above
                Dim count As Integer = 1
                Dim itmText As String = ""
                Dim reqQty As Integer = 0

                For Each dr In ds.Tables(0).Rows
                    ' --- Get the item comments
                    itmText = GetItemComments(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("itm_comment") = itmText
                    End If

                    '--- Get & populate the promotion pricing info
                    salesBiz.getPromoDetails(dr, dt_store_eff_prc.Value)

                    '--- Get the item features info
                    itmText = GetItemFeatures(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("features_benefits") = itmText
                    End If

                    '--- Get the item current effective price from BT API that goes against the price calendar
                    Dim currentPrice As String = GetCurrentPrice(dr("ITM_CD"), cbo_store_store_cd.Value, dt_store_eff_prc.Value)
                    If Not String.IsNullOrEmpty(currentPrice) Then
                        dr("current_prc") = currentPrice
                    End If

                    '--- Gets the RFID values for this line
                    Dim comboNum As String = IIf(IsDBNull(dr("COMBO_NUM")), "", dr("COMBO_NUM"))
                    Dim rfIds As List(Of String) = GetRfIdsForInvLoc(dr("STORE_CD"),
                                                                     dr("LOC_CD"),
                                                                     dr("ITM_CD"),
                                                                     comboNum)
                    If (rfIds.Count > 0) Then
                        dr("RF_ID_CD") = rfIds.Item(0)
                    End If

                    'when qty>1, duplicates the line so that every row has a quantity of 1
                    If CDbl(dr("qty")) > 1 Then
                        For count = 2 To CDbl(dr("qty"))
                            dr_new = dt_new.NewRow
                            dr("qty") = "1"
                            CopyDataRow(dr, dr_new)
                            dr_new("rcv_dt") = dr("rcv_dt")

                            'there may not be enough RFIDs therefore needs to check
                            If ((count - 1) < rfIds.Count) Then
                                dr_new("RF_ID_CD") = rfIds.Item(count - 1)
                            End If

                            dt_new.Rows.Add(dr_new)
                        Next
                    End If
                Next

                'copies the newly added rows (above) into the dataset and displays them
                dt.Merge(dt_new, True)

                PopulateSkuDatagrid(dt)
                GenerateTagsAsXmlFile(ds)

            Catch ex As Exception
                lbl_information.Text = ex.Message
            Finally
                conn.Close()
            End Try
        End If

    End Sub


    Public Sub GeneratePoTags()

        If (Not String.IsNullOrEmpty(txt_po.Text)) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Try
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim objSql As OracleCommand
                Dim oAdp As OracleDataAdapter
                Dim sql As String

                conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
                conn.Open()

                sql = "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC, NVL(a.RET_PRC,0.00) AS RET_PRC, " &
                "a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, c.QTY_ORD AS QTY, c.COMB_NUM AS COMBO_NUM, NULL AS STORE_CD, NULL AS LOC_CD, " &
                "a.MNR_CD, a.CAT_CD, a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, NULL As FEATURES_BENEFITS, " &
                "a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY, a.PO_LEAD_TIME, c.ARRIVAL_DT, a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, " &
                "a.SPIFF, a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, b.PO_CD AS IST_PO_CD, b.FINAL_DT AS IST_PO_FINAL_DT, " &
                "NULL AS IST_PO_SEQ_NO, b.PO_SRT_CD AS IST_PO_SRT_CD, b.WR_DT AS IST_PO_WR_DT,  NULL AS PROMOTION_CD, " &
                "NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, NULL AS OUTLET_NAS_ID, NULL AS ITM_COMMENT,  " &
                "NULL AS CURRENT_PRC, LN# AS LINE_NUM, NULL as RF_ID_CD " &
                    "FROM ITM a, PO b, PO_LN c " &
                    "WHERE a.ITM_CD = c.ITM_CD " &
                           "AND b.PO_CD=:PO_CD " &
                           "AND b.PO_CD=c.PO_CD " &
                    "ORDER BY a.ITM_CD"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":PO_CD", OracleType.VarChar)
                objSql.Parameters(":PO_CD").Value = txt_po.Text

                ' ** Prepares to retrieve data
                Dim ds As DataSet = New DataSet
                Dim ds_new As DataSet = New DataSet
                Dim dt, dt_new As DataTable
                Dim dr, dr_new As DataRow

                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds, "PRICE_TAGS")
                oAdp.Fill(ds_new, "PRICE_TAGS")

                dt = ds.Tables(0)
                dt_new = ds_new.Tables(0)
                dt_new.Clear()  'clears it because only the table structure is needed, not the contents

                ' *** Retrieves the addional data for each SKU found above
                Dim count As Integer = 1
                Dim itmText As String = ""
                Dim reqQty As Integer = 0

                For Each dr In ds.Tables(0).Rows
                    ' --- Get the item comments
                    itmText = GetItemComments(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("itm_comment") = itmText
                    End If

                    ' Set the store code to an empty string(the main query above always sets it to null) to avoid getting the null 
                    ' conversion to string error whenever a query uses it.
                    dr("store_cd") = ""

                    '--- Get & populate the promotion pricing info. 
                    salesBiz.getPromoDetails(dr, dr("ARRIVAL_DT"))

                    '--- Get the item features info
                    itmText = GetItemFeatures(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("features_benefits") = itmText
                    End If

                    '--- Get the item current effective price from BT API that goes against the price calendar
                    Dim currentPrice As String = GetCurrentPrice(dr("ITM_CD"), dr("STORE_CD"), dr("ARRIVAL_DT"))
                    If Not String.IsNullOrEmpty(currentPrice) Then
                        dr("current_prc") = currentPrice
                    End If

                    '--- Gets the RFID values for this line
                    Dim rfIds As List(Of String) = GetRfIdsForPo(dr("IST_PO_CD"), dr("LINE_NUM"))
                    If (rfIds.Count > 0) Then
                        dr("RF_ID_CD") = rfIds.Item(0)
                    End If

                    'when qty>1, duplicates the line so that every row has a quantity of 1
                    If CDbl(dr("qty")) > 1 Then
                        For count = 2 To CDbl(dr("qty"))
                            dr_new = dt_new.NewRow
                            dr("qty") = "1"
                            CopyDataRow(dr, dr_new)
                            dr_new("arrival_dt") = dr("arrival_dt")

                            'there may not be enough RFIDs therefore needs to check
                            If ((count - 1) < rfIds.Count) Then
                                dr_new("RF_ID_CD") = rfIds.Item(count - 1)
                            End If

                            dt_new.Rows.Add(dr_new)
                        Next
                    End If
                Next

                'copies the newly added rows (above) into the dataset and displays them
                dt.Merge(dt_new, True)
                PopulateSkuDatagrid(dt)
                GenerateTagsAsXmlFile(ds)

            Catch ex As Exception
                lbl_information.Text = ex.Message
            Finally
                conn.Close()
            End Try
        End If

    End Sub


    Public Sub GenerateIstTags()

        If (Not String.IsNullOrEmpty(txt_ist.Text)) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Try
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim objSql As OracleCommand
                Dim oAdp As OracleDataAdapter
                Dim sql As String

                conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
                conn.Open()

                sql = "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC, " &
                            "NVL(a.RET_PRC,0.00) AS RET_PRC, a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, c.QTY, " &
                            "c.CRPT_ID_NUM AS COMBO_NUM, c.NEW_STORE_CD AS STORE_CD, c.NEW_LOC_CD AS LOC_CD, a.MNR_CD, a.CAT_CD, " &
                            " a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, " &
                            "NULL As FEATURES_BENEFITS, a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY,  " &
                            "a.PO_LEAD_TIME, NVL(b.TRANSFER_DT,TRUNC(SYSDATE)) AS TRANSFER_DT, a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, a.SPIFF, " &
                            "a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, b.DOC_NUM AS IST_PO_CD, b.FINAL_DT AS IST_PO_FINAL_DT, " &
                            "b.IST_SEQ_NUM AS IST_PO_SEQ_NO, b.SRT_CD AS IST_PO_SRT_CD, b.IST_WR_DT AS IST_PO_WR_DT, " &
                            "NULL AS PROMOTION_CD, NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, " &
                            "c.ID_CD AS OUTLET_NAS_ID, NULL AS ITM_COMMENT, NULL AS CURRENT_PRC, LN# AS LINE_NUM, c.IST_STORE_CD, NULL AS RF_ID_CD " &
                         "FROM ITM a,  IST b,  IST_LN c " &
                         "WHERE a.ITM_CD = c.ITM_CD " &
                                "AND b.DOC_NUM=:IST_CD " &
                                "AND b.IST_WR_DT = c.IST_WR_DT " &
                                "AND b.IST_STORE_CD = c.IST_STORE_CD " &
                                "AND b.IST_SEQ_NUM = c.IST_SEQ_NUM " &
                         "ORDER BY a.ITM_CD"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":IST_CD", OracleType.VarChar)
                objSql.Parameters(":IST_CD").Value = txt_ist.Text

                ' ** Prepares to retrieve data
                Dim ds As DataSet = New DataSet
                Dim ds_new As DataSet = New DataSet
                Dim dt, dt_new As DataTable
                Dim dr, dr_new As DataRow

                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds, "PRICE_TAGS")
                oAdp.Fill(ds_new, "PRICE_TAGS")

                dt = ds.Tables(0)
                dt_new = ds_new.Tables(0)
                dt_new.Clear()  'clears it because only the table structure is needed, not the contents

                ' *** Retrieves the addional data for each SKU found above
                Dim count As Integer = 1
                Dim itmText As String = ""
                Dim reqQty As Integer = 0

                For Each dr In ds.Tables(0).Rows
                    ' --- Get the item comments
                    itmText = GetItemComments(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("itm_comment") = itmText
                    End If

                    ' Set the store code to an empty string(the main query above always sets it to null) to avoid getting the null 
                    ' conversion to string error whenever a query uses it.
                    dr("store_cd") = ""

                    '--- Get & populate the promotion pricing info. 
                    salesBiz.getPromoDetails(dr, dr("TRANSFER_DT"))

                    '--- Get the item features info
                    itmText = GetItemFeatures(dr("ITM_CD"))
                    If Not String.IsNullOrEmpty(itmText) Then
                        dr("features_benefits") = itmText
                    End If

                    '--- Get the item current effective price from BT API that goes against the price calendar
                    Dim currentPrice As String = GetCurrentPrice(dr("ITM_CD"), dr("STORE_CD"), dr("TRANSFER_DT"))
                    If Not String.IsNullOrEmpty(currentPrice) Then
                        dr("current_prc") = currentPrice
                    End If

                    '--- Gets the RFID values for this line
                    Dim rfIds As List(Of String) = GetRfIdsForIst(dr("IST_PO_WR_DT"),
                                                                  dr("IST_STORE_CD"),
                                                                  dr("IST_PO_SEQ_NO"),
                                                                  dr("LINE_NUM"))
                    If (rfIds.Count > 0) Then
                        dr("RF_ID_CD") = rfIds.Item(0)
                    End If

                    'when qty>1, duplicates the line so that every row has a quantity of 1
                    If CDbl(dr("qty")) > 1 Then
                        For count = 2 To CDbl(dr("qty"))
                            dr_new = dt_new.NewRow
                            dr("qty") = "1"
                            CopyDataRow(dr, dr_new)
                            dr_new("transfer_dt") = dr("transfer_dt")

                            'there may not be enough RFIDs therefore needs to check
                            If ((count - 1) < rfIds.Count) Then
                                dr_new("RF_ID_CD") = rfIds.Item(count - 1)
                            End If

                            dt_new.Rows.Add(dr_new)
                        Next
                    End If
                Next

                'copies the newly added rows (above) into the dataset and displays them
                dt.Merge(dt_new, True)
                PopulateSkuDatagrid(dt)
                GenerateTagsAsXmlFile(ds)

            Catch ex As Exception
                lbl_information.Text = ex.Message
            Finally
                conn.Close()
            End Try
        End If

    End Sub


    Public Sub GenerateIndividualTags()

        lbl_information.Text = ""
        If (Not String.IsNullOrEmpty(txt_ind_sku.Text)) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Try
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim objSql As OracleCommand
                Dim oAdp As OracleDataAdapter
                Dim sql As String
                Dim includeStore As Boolean = (String.IsNullOrEmpty(cbo_ind_store_cd.Value) = False)
                Dim includeLocation As Boolean = (String.IsNullOrEmpty(cbo_ind_loc_cd.Value) = False)
                Dim includeComboNum As Boolean = (String.IsNullOrEmpty(txt_ind_combo.Text) = False)

                ' ** Builds the query
                sql = "SELECT a.ITM_CD, a.VSN, a.DES, a.SIZ, NVL(a.PRC1,0.00) AS PRC1, NVL(a.ADV_PRC,0.00) AS ADV_PRC, NVL(a.RET_PRC,0.00) AS RET_PRC, " &
                            "a.VE_CD, NVL(a.REPL_CST,0.00) AS REPL_CST, b.QTY, b.CRPT_ID_NUM AS COMBO_NUM, " &
                            "b.STORE_CD, b.LOC_CD, a.MNR_CD, a.CAT_CD, a.STYLE_CD, a.COVER, a.GRADE, a.FINISH, a.DROP_CD, a.DROP_DT, a.FAMILY_CD, " &
                            "NULL As FEATURES_BENEFITS, a.FRT_FAC, a.IVC_CST, a.LST_ACT_DT, a.PALLET_QTY, a.PO_LEAD_TIME, b.RCV_DT, " &
                            "a.RELATED_ITM_CD, a.RET_PRC_CHNG_DT, a.SPIFF, a.VOL, a.WEIGHT, NULL AS WARRANTY_INFO, a.COMM_CD, " &
                            "NULL AS IST_PO_CD, NULL AS IST_PO_FINAL_DT, NULL AS IST_PO_SEQ_NO, NULL AS IST_PO_SRT_CD, " &
                            "NULL AS IST_PO_WR_DT, NULL AS PROMOTION_CD, NULL AS PROMOTION_DESC, NULL AS PROMOTION_RET_PRC, " &
                            "NULL AS OUTLET_NAS_ID, NULL AS ITM_COMMENT, NULL AS CURRENT_PRC, NULL AS RF_ID_CD " &
                            "FROM ITM a, ITM_FIFL b "
                sql = GetIndividualSortCodeAndPriceQuery(sql)

                If (includeStore) Then
                    sql = sql & "AND b.STORE_CD=:STORE_CD "
                End If
                If (includeLocation) Then
                    sql = sql & "AND b.LOC_CD=:LOC_CD "
                End If
                If (includeComboNum) Then
                    sql = sql & "AND b.CRPT_ID_NUM=:CRPT_ID_NUM "
                End If
                sql = sql & "ORDER BY a.ITM_CD"

                ' ** Establishes connection AND binds the variables needed by the query
                conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
                conn.Open()

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = UCase(txt_ind_sku.Text)

                If (includeStore) Then
                    objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
                    objSql.Parameters(":STORE_CD").Value = cbo_ind_store_cd.Value
                End If
                If (includeLocation) Then
                    objSql.Parameters.Add(":LOC_CD", OracleType.VarChar)
                    objSql.Parameters(":LOC_CD").Value = cbo_ind_loc_cd.Value
                End If
                If (includeComboNum) Then
                    objSql.Parameters.Add(":CRPT_ID_NUM", OracleType.VarChar)
                    objSql.Parameters(":CRPT_ID_NUM").Value = txt_ind_combo.Text
                End If

                'bind the sort code and price variables
                BindSortCodeAndPriceParams(objSql, txt_ind_sort_cd_from.Text, txt_ind_sort_cd_to.Text,
                                                   dt_ind_ret_prc_from.Value, dt_ind_ret_prc_to.Value,
                                                   dt_ind_adv_prc_from.Value, dt_ind_adv_prc_to.Value)

                ' ** Prepares to retrieve data
                Dim ds As DataSet = New DataSet
                Dim ds_new As DataSet = New DataSet
                Dim dt, dt_new As DataTable
                Dim dr, dr_new As DataRow

                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds, "PRICE_TAGS")
                oAdp.Fill(ds_new, "PRICE_TAGS")

                dt = ds.Tables(0)
                dt_new = ds_new.Tables(0)
                dt_new.Clear()  'clears it because only the table structure is needed, not the contents

                ' *** Retrieves the addional data for each SKU found above
                Dim count As Integer = 1
                Dim itmText As String = ""
                Dim qtyRequested As Double = 0
                Dim qtyCurrentRow As Double = 0
                Dim qtyFulfilledSoFar As Double = 0

                If (IsNumeric(txt_ind_qty.Text)) Then
                    qtyRequested = txt_ind_qty.Text
                End If

                If qtyRequested = 0 Then
                    'when a qty is not entered by the user, only ONE tag will get generated
                    qtyRequested = 1
                End If

                For Each dr In ds.Tables(0).Rows
                    qtyCurrentRow = dr("qty")
                    If (qtyFulfilledSoFar < qtyRequested) Then     'means that the requested qty has not been met yet

                        ' --- Get the item comments
                        itmText = GetItemComments(dr("ITM_CD"))
                        If Not String.IsNullOrEmpty(itmText) Then
                            dr("itm_comment") = itmText
                        End If

                        '--- Get & populate the promotion pricing info
                        salesBiz.getPromoDetails(dr, dt_ind_eff_prc.Value)

                        '--- Get the item features info
                        itmText = GetItemFeatures(dr("ITM_CD"))
                        If Not String.IsNullOrEmpty(itmText) Then
                            dr("features_benefits") = itmText
                        End If

                        '--- Get the item current effective price from BT API that goes against the price calendar
                        Dim currentPrice As String = GetCurrentPrice(dr("ITM_CD"), cbo_ind_store_cd.Value, dt_ind_eff_prc.Value)
                        If Not String.IsNullOrEmpty(currentPrice) Then
                            dr("current_prc") = currentPrice
                        End If

                        '--- Gets the RFID values for this line
                        Dim comboNum As String = IIf(IsDBNull(dr("COMBO_NUM")), "", dr("COMBO_NUM"))
                        Dim rfIds As List(Of String) = GetRfIdsForInvLoc(dr("STORE_CD"),
                                                                         dr("LOC_CD"),
                                                                         dr("ITM_CD"),
                                                                         comboNum)

                        ' ---  Consider all the available qty for the row, which may or may not be enough depending on what the user requested
                        For count = 1 To qtyCurrentRow
                            If qtyFulfilledSoFar < qtyRequested Then
                                ' only process as many rows as the user requested even though there may be more qty available. 
                                dr("qty") = "1"
                                dr_new = dt_new.NewRow
                                CopyDataRow(dr, dr_new)
                                dr_new("rcv_dt") = dr("rcv_dt")

                                'there may not be enough RFIDs therefore needs to check
                                If (count - 1 < rfIds.Count) Then
                                    dr_new("RF_ID_CD") = rfIds.Item(count - 1)
                                End If

                                dt_new.Rows.Add(dr_new)
                                qtyFulfilledSoFar += 1
                            Else
                                ' qty has been met, nothing to do
                                Exit For
                            End If
                        Next
                    End If
                Next

                'populates a new table with a subset of the newly added rows (above) and displays them
                PopulateSkuDatagrid(dt_new)
                GenerateTagsAsXmlFile(ds_new)

            Catch ex As Exception
                lbl_information.Text = ex.Message
            Finally
                conn.Close()
            End Try
        Else
            lbl_information.Text = "You must enter a SKU to continue."
        End If

    End Sub


    Private Sub GeneratePackageTags(ByVal pkgSkus As String)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        conn.Open()

        Try
            sql = "SELECT a.ITM_CD AS PARENT_ITM_CD, a.VSN AS PARENT_VSN, a.DES AS PARENT_DES, a.SIZ AS PARENT_SIZ, a.PRC1 AS PARENT_PRC1, "
            sql = sql & "a.ADV_PRC AS PARENT_ADV_PRC, a.RET_PRC AS PARENT_RET_PRC, a.VE_CD AS PARENT_VE_CD, a.REPL_CST AS PARENT_REPL_CST, "
            sql = sql & "'1' AS PARENT_QTY, a.MNR_CD AS PARENT_MNR_CD, a.CAT_CD AS PARENT_CAT_CD, a.STYLE_CD AS PARENT_STYLE_CD, a.COVER AS PARENT_COVER, "
            sql = sql & "a.GRADE AS PARENT_GRADE, a.FINISH AS PARENT_FINISH, a.DROP_CD AS PARENT_DROP_CD, a.DROP_DT AS PARENT_DROP_DT, a.FAMILY_CD AS PARENT_FAMILY_CD, "
            sql = sql & "NULL AS PARENT_FEATURES_BENEFITS, a.FRT_FAC AS PARENT_FRT_FAC, a.IVC_CST AS PARENT_IVC_CST, "
            sql = sql & "a.LST_ACT_DT AS PARENT_LST_ACT_DT, a.PALLET_QTY AS PARENT_PALLET_QTY, a.PO_LEAD_TIME AS PARENT_PO_LEAD_TIME, "
            sql = sql & "a.RELATED_ITM_CD AS PARENT_RELATED_ITM_CD, a.RET_PRC_CHNG_DT AS PARENT_RET_PRC_CHNG_DT, a.SPIFF AS PARENT_SPIFF, "
            sql = sql & "a.VOL AS PARENT_VOL, a.WEIGHT AS PARENT_WEIGHT, NULL AS PARENT_WARRANTY_INFO, a.COMM_CD AS PARENT_COMM_CD, "
            sql = sql & "NULL AS PARENT_PROMOTION_CD, NULL AS PARENT_PROMOTION_DESC, NULL AS PARENT_PROMOTION_RET_PRC, NULL AS PARENT_ITM_COMMENT, "
            sql = sql & "c.ITM_CD AS COMPONENT_ITM_CD, c.VSN AS COMPONENT_VSN, c.DES AS COMPONENT_DES, c.SIZ AS COMPONENT_SIZ, "
            sql = sql & "c.PRC1 AS COMPONENT_PRC1, c.ADV_PRC AS COMPONENT_ADV_PRC, c.RET_PRC AS COMPONENT_RET_PRC, c.VE_CD AS COMPONENT_VE_CD, "
            sql = sql & "c.REPL_CST AS COMPONENT_REPL_CST, b.QTY AS COMPONENT_QTY, "
            sql = sql & "c.MNR_CD AS COMPONENT_MNR_CD, c.CAT_CD AS COMPONENT_CAT_CD, c.STYLE_CD AS COMPONENT_STYLE_CD, c.COVER AS COMPONENT_COVER, "
            sql = sql & "c.GRADE AS COMPONENT_GRADE, c.FINISH AS COMPONENT_FINISH, c.DROP_CD AS COMPONENT_DROP_CD, c.DROP_DT AS COMPONENT_DROP_DT, c.FAMILY_CD AS COMPONENT_FAMILY_CD, "
            sql = sql & "NULL As COMPONENT_FEATURES_BENEFITS, c.FRT_FAC AS COMPONENT_FRT_FAC, c.IVC_CST AS COMPONENT_IVC_CST, "
            sql = sql & "c.LST_ACT_DT AS COMPONENT_LST_ACT_DT, c.PALLET_QTY AS COMPONENT_PALLET_QTY, c.PO_LEAD_TIME AS COMPONENT_PO_LEAD_TIME, "
            sql = sql & "c.RELATED_ITM_CD AS COMPONENT_RELATED_ITM_CD, c.RET_PRC_CHNG_DT AS COMPONENT_RET_PRC_CHNG_DT, c.SPIFF AS COMPONENT_SPIFF, "
            sql = sql & "c.VOL AS COMPONENT_VOL, c.WEIGHT AS COMPONENT_WEIGHT, NULL AS COMPONENT_WARRANTY_INFO, c.COMM_CD AS COMPONENT_COMM_CD, "
            sql = sql & "NULL AS COMPONENT_PROMOTION_CD, NULL AS COMPONENT_PROMOTION_DESC, NULL AS COMPONENT_PROMOTION_RET_PRC, NULL AS COMPONENT_ITM_COMMENT "
            sql = sql & "FROM ITM a, PACKAGE b, ITM c "
            sql = sql & "WHERE a.ITM_CD = b.PKG_ITM_CD AND c.ITM_CD=b.CMPNT_ITM_CD AND b.PKG_ITM_CD IN ('" & pkgSkus & "') "
            sql = sql & "ORDER BY a.ITM_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            ds = New DataSet

            'Set SQL OBJECT 
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds, "PRICE_TAGS")

            PopulatePkgDatagrid(ds.Tables("PRICE_TAGS"))
            GenerateTagsAsXmlFile(ds)

        Catch ex As Exception
            lbl_information.Text = ex.Message
        Finally
            conn.Close()
        End Try

    End Sub


    Protected Sub ASPxPageControl1_ActiveTabChanged(source As Object, e As DevExpress.Web.ASPxTabControl.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged
        ResetCommonComponents()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        On Error Resume Next

        Dim appPath As String = Request.ServerVariables("APPL_PHYSICAL_PATH")
        Dim individualTags As Boolean = txt_ptag.Visible = True
        Dim REPX_Format As String = IIf(individualTags, txt_ptag.Text, txt_gtag.Text)
        If (String.IsNullOrEmpty(REPX_Format)) Then
            lbl_information.Text = IIf(individualTags,
                                       "You must select an Individual Price Tag format to continue",
                                       "You must select a Group Price Tag format to continue")
            Exit Sub
        Else
            REPX_Format = appPath & "\Templates\" & IIf(individualTags, "Individual\", "Group\") & REPX_Format
        End If

        Dim Report As New XtraReport
        Report.LoadLayout(REPX_Format)
        Report.XmlDataPath = appPath & "\Data\" & lbl_tag_id.Text & ".xml"

        Response.ClearContent()
        Response.ClearHeaders()
        Response.Buffer = True
        Response.Cache.SetCacheability(HttpCacheability.Private)

        Dim randObj As New Random
        Dim doc_num As String = randObj.Next(50000)
        Dim ms As New System.IO.MemoryStream()
        Dim fs As System.IO.FileStream
        Dim pdfOptions As New DevExpress.XtraPrinting.PdfExportOptions
        pdfOptions.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Medium
        pdfOptions.Compressed = True
        Report.ExportToPdf(ms, pdfOptions)

        fs = System.IO.File.OpenWrite(appPath & "\Data\" & doc_num & ".pdf")
        fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
        fs.Close()
        ms.Close()

        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "AnyScriptNameYouLike95", "window.open('Data/" & doc_num & ".pdf','frmPTAG" & randObj.Next(1000) & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'this method is executed when 'Generate Tags' button is pressed within the STORE tab
        Try
            DataGridView1.DataSource = ""
            txt_ptag.Visible = True
            txt_gtag.Visible = False
            Label1.Visible = True
            Label2.Visible = False
            ValidateStoreRanges()
            GenerateStoreTags()
        Catch
            lbl_information.Text = Err.Description
        End Try
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'this method is executed when 'Generate Tags' button is pressed within the PO tab
        Try
            DataGridView1.DataSource = ""
            txt_ptag.Visible = True
            txt_gtag.Visible = False
            Label1.Visible = True
            Label2.Visible = False
            GeneratePoTags()
        Catch
            lbl_information.Text = Err.Description
        End Try
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'this method is executed when 'Generate Tags' button is pressed within the IST tab
        Try
            DataGridView1.DataSource = ""
            txt_ptag.Visible = True
            txt_gtag.Visible = False
            Label1.Visible = True
            Label2.Visible = False
            GenerateIstTags()
        Catch
            lbl_information.Text = Err.Description
        End Try
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'this method is executed when 'Generate Tags' button is pressed within the INDIVIDUAL tab
        lbl_information.Text = ""
        Try
            If (String.IsNullOrEmpty(cbo_ind_store_cd.Text)) Then
                lbl_information.Text = System.Environment.NewLine &
                                       "A Store is required for this Action.  Please select one." &
                                       System.Environment.NewLine & System.Environment.NewLine
            Else
                DataGridView1.DataSource = ""
                txt_ptag.Visible = True
                txt_gtag.Visible = False
                Label1.Visible = True
                Label2.Visible = False
                ValidateIndividualRanges()
                GenerateIndividualTags()
            End If
        Catch
            lbl_information.Text = Err.Description
        End Try
    End Sub


    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'this method is executed when 'Generate Tags' button is pressed within the PACKAGE tab
        txt_ptag.Visible = False
        txt_gtag.Visible = True
        Label1.Visible = False
        Label2.Visible = True
        GeneratePackageTags(txt_new_package.Text.ToUpper)
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'this method is executed when 'RESET' button is pressed within the STORE tab
        ResetCommonComponents()
        'sets the cbo objects to an empty string, otherwise if setting 
        'a selected index, it triggers a call to the server
        cbo_store_store_cd.Text = ""
        cbo_store_loc_cd.Text = ""
        txt_store_ve_cd.Text = ""
        txt_store_sort_cd_from.Text = ""
        txt_store_sort_cd_to.Text = ""
        dt_store_ret_prc_from.Value = ""
        dt_store_ret_prc_to.Value = ""
        dt_store_adv_prc_from.Value = ""
        dt_store_adv_prc_to.Value = ""
        dt_store_eff_prc.Value = ""
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'this method is executed when 'RESET' button is pressed within the PO tab
        ResetCommonComponents()
        txt_po.Text = ""
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'this method is executed when 'RESET' button is pressed within the IST tab
        ResetCommonComponents()
        txt_ist.Text = ""
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'this method is executed when 'RESET' button is pressed within the INDIVIDUAL tab
        ResetCommonComponents()
        'sets the cbo objects to an empty string, otherwise if setting 
        'a selected index, it triggers a call to the server
        cbo_ind_store_cd.Text = ""
        cbo_ind_loc_cd.Text = ""
        txt_ind_sku.Text = ""
        txt_ind_qty.Text = ""
        txt_ind_combo.Text = ""
        txt_ind_sort_cd_from.Text = ""
        txt_ind_sort_cd_to.Text = ""
        dt_ind_ret_prc_from.Value = ""
        dt_ind_ret_prc_to.Value = ""
        dt_ind_adv_prc_from.Value = ""
        dt_ind_adv_prc_to.Value = ""
        dt_ind_eff_prc.Value = ""
    End Sub


    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'this method is executed when 'RESET' button is pressed within the PACKAGE tab
        ResetCommonComponents()
        txt_new_package.Text = ""
    End Sub


    Private Sub cbo_store_store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store_store_cd.SelectedIndexChanged

        PopulateLocations(cbo_store_store_cd.Value, cbo_store_loc_cd)

    End Sub


    Private Sub cbo_ind_store_cd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_ind_store_cd.SelectedIndexChanged

        PopulateLocations(cbo_ind_store_cd.Value, cbo_ind_loc_cd)

    End Sub





    Private Function GetItemFeatures(ByVal itemCd As String) As String
        'Retrieves and builds a string of all the item features and benefits for the passed-in item 

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim reader As OracleDataReader
        Dim cmd As OracleCommand
        Dim text As New StringBuilder()
        Dim sql As String

        ' ** Establishes connection AND binds the variables needed by the query
        conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        conn.Open()

        Try
            sql = GetItemFeaturesQuery(itemCd)
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            Do While reader.Read
                'build the item  features text as its own line
                text = text.Append(reader.Item("DES").ToString).Append(System.Environment.NewLine)
            Loop
            reader.Close()
        Catch ex As Exception
            lbl_information.Text = ex.Message
        Finally
            conn.Close()
        End Try

        Return text.ToString
    End Function


    Private Function GetItemComments(ByVal itemCd As String) As String
        'Retrieves and builds a string of all the item comments for the passed-in item 

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim reader As OracleDataReader
        Dim cmd As OracleCommand
        Dim text As New StringBuilder()
        Dim sql As String

        ' ** Establishes connection AND binds the variables needed by the query
        conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        conn.Open()

        Try
            sql = GetItemCommentQuery(itemCd)
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            Do While reader.Read
                'build the item comment as its own line
                text = text.Append(reader.Item("TEXT").ToString).Append(System.Environment.NewLine)
            Loop
            reader.Close()
        Catch ex As Exception
            lbl_information.Text = ex.Message
        Finally
            conn.Close()
        End Try

        Return text.ToString
    End Function


    'effDt passed as object since the date value could be null.  if defined As Date throws an exception
    'when trying to cast the date read from a datarow
    Private Function GetCurrentPrice(ByVal itemCd As String, ByVal storeCd As String, ByVal effDt As Object) As String

        'Returns the retail price from E1 pricing api
        Dim retPrice As Double = 0
        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        conn.Open()

        Try
            sql = "bt_sale_wrapper.get_current_price"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.CommandType = CommandType.StoredProcedure

            objsql.Parameters.Add("returnValue", OracleType.Double, 14).Direction = ParameterDirection.ReturnValue
            objsql.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("eff_date_i", OracleType.DateTime).Direction = ParameterDirection.Input
            objsql.Parameters.Add("cust_tp_prc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input

            '-- default to system date if the date passed-in is null
            If IsDBNull(effDt) Then
                effDt = DateTime.Now
            End If

            objsql.Parameters("itm_cd_i").Value = itemCd
            objsql.Parameters("store_cd_i").Value = storeCd
            objsql.Parameters("eff_date_i").Value = effDt
            objsql.Parameters("cust_tp_prc_cd_i").Value = ""     'not applicable for price tags

            objsql.ExecuteNonQuery()

            'get the retail price from the stored function call
            retPrice = objsql.Parameters(0).Value ' to use this by number, starts at zero and is by add list sequence above

        Catch ex As Exception
            lbl_information.Text = ex.Message
        Finally
            conn.Close()
        End Try

        Return retPrice
    End Function


    Private Sub BindSortCodeAndPriceParams(ByRef objSql As OracleCommand, ByVal sortCodeFrom As String,
                                           ByVal sortCodeTo As String,
                                           ByVal retPriceFrom As Date, ByVal retPriceTo As Date,
                                           ByVal advPriceFrom As Date, ByVal advPriceTo As Date)

        'Defines and adds the sort code, retail and advertised price parameters and values to the query.

        If Not String.IsNullOrEmpty(sortCodeFrom) And Not String.IsNullOrEmpty(sortCodeTo) Then
            objSql.Parameters.Add(":ITM_SRT1", OracleType.VarChar)
            objSql.Parameters(":ITM_SRT1").Value = UCase(sortCodeFrom)
            objSql.Parameters.Add(":ITM_SRT2", OracleType.VarChar)
            objSql.Parameters(":ITM_SRT2").Value = UCase(sortCodeTo)
        End If

        If IsDate(retPriceFrom) And IsDate(retPriceTo) Then
            If (IsDateNull(retPriceFrom) = False And IsDateNull(retPriceTo) = False) Then
                objSql.Parameters.Add(":RET_PRC_FROM", OracleType.DateTime)
                objSql.Parameters(":RET_PRC_FROM").Value = retPriceFrom
                objSql.Parameters.Add(":RET_PRC_TO", OracleType.DateTime)
                objSql.Parameters(":RET_PRC_TO").Value = retPriceTo
            End If
        End If

        If IsDate(advPriceFrom) And IsDate(advPriceTo) Then
            If (IsDateNull(advPriceFrom) = False And IsDateNull(advPriceTo) = False) Then
                objSql.Parameters.Add(":ADV_PRC_FROM", OracleType.DateTime)
                objSql.Parameters(":ADV_PRC_FROM").Value = advPriceFrom
                objSql.Parameters.Add(":ADV_PRC_TO", OracleType.DateTime)
                objSql.Parameters(":ADV_PRC_TO").Value = advPriceTo
            End If
        End If

    End Sub


    Private Function GetItemFeaturesQuery(ByVal itemCd As String) As String
        'Builds and returns the item features query using the passed-in item code

        Dim sql As String = "SELECT DES FROM ITM2FEATURE, FEATURE " &
                            " WHERE ITM_CD = '" & itemCd &
                            "' AND ITM2FEATURE.FEAT_CD = FEATURE.FEAT_CD"
        Return sql
    End Function


    Private Shared Function GetItemCommentQuery(ByVal itemCd As String) As String
        'Builds and returns the item comments query using the passed-in item code

        Dim sql As String = "SELECT TEXT FROM ITM_CMNT " &
                            " WHERE ITM_CD = '" & itemCd &
                            "' ORDER BY SEQ#"
        Return sql
    End Function


    'Private Function GetPromPrcStoreQuery(ByVal itemCd As String, ByVal storeCd As String,
    '                                                                         ByVal effDt As Date) As String

    '    'Builds and returns the promotion price query from store using the passed-in item and store code

    '    ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range against in GetPrcDtRangeQuery; here prom_prc_tp
    '    Dim sql As String = "SELECT ed.DES, s.PRC, ed.PRC_CD  " &
    '                             "FROM PROM_PRC_TP ed, STORE_PROM_PRC s " &
    '                              "WHERE ed.PRC_CD = s.PRC_CD "
    '    sql = sql & " AND ITM_CD = '" & itemCd & "' AND STORE_CD = '" & storeCd & "' " & GetPrcDtRangeQuery(effDt)
    '    sql = sql & " ORDER BY ed.prc_cd "  'order by courtesy of pcr 75020

    '    Return sql
    'End Function


    'Private Function GetPromPrcItmQuery(ByVal itemCd As String, ByVal storeCd As String,
    '                                                                     ByVal effDt As Date) As String

    '    'Builds and returns the promotion price query using the passed-in item and store code from the itm promo calendar

    '    ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range against in GetPrcDtRangeQuery; here store$prom_clndr
    '    Dim sql As String = "SELECT p.des, i.prc, i.prc_cd FROM itm$prom_prc_tp i, store$prom_clndr ed, prom_prc_tp p WHERE ed.prc_cd  = i.prc_cd "

    '    sql = sql & " AND p.prc_cd = i.prc_cd AND p.prc_cd = ed.prc_cd "

    '    sql = sql & "AND ITM_CD = '" & itemCd & "' AND STORE_CD ='" & storeCd & "' " & GetPrcDtRangeQuery(effDt) &
    '        " ORDER BY i.prc_cd " 'order by courtesy of pcr 75020

    '    Return sql
    'End Function


    'Private Function GetPromPrcStoreGrpQuery(ByVal itemCd As String, ByVal storeCd As String,
    '                                                                                ByVal effDt As Date) As String

    '    'Builds and returns the promotion price query from store group using the passed-in item and store code
    '    ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range 
    '    ' against in GetPrcDtRangeQuery; here prom_prc_tp

    '    Dim sql As String = "SELECT ed.DES, s.PRC, ed.PRC_CD " &
    '                        "FROM PROM_PRC_TP ed, STORE_GRP_PROM_PRC s " &
    '                        "WHERE ed.PRC_CD = s.PRC_CD "
    '    sql = sql & " AND ITM_CD = '" & itemCd & "' " & GetPrcDtRangeQuery(effDt) &
    '                        " AND STORE_GRP_CD IN " &
    '                                        "(SELECT sg.STORE_GRP_CD FROM STORE_GRP$STORE sgs, store_grp sg  " &
    '                                         "WHERE sgs.STORE_CD ='" & storeCd &
    '                                         "' AND sg.PRICING_STORE_GRP = 'Y' AND sg.store_grp_cd = sgs.store_grp_cd) "
    '    sql = sql & " ORDER BY ed.prc_cd " 'order by courtesy of pcr 75020

    '    Return sql
    'End Function


    'Private Function GetPrcDtRangeQuery(ByVal effDt As Date) As String

    '    'Builds and returns the date range for pricing

    '    ' PROGRAMMING NOTE: the ed alias must be setup in calling routine for the table to be used for dates
    '    Dim sql As String = " AND ed.eff_dt <= TO_DATE('" & FormatDateTime(effDt, 2) & "','mm/dd/yyyy')" &
    '                        " AND ed.end_dt >= TO_DATE('" & FormatDateTime(effDt, 2) & "','mm/dd/yyyy') "

    '    Return sql
    'End Function


    Private Function GetStoreSortCodeAndPriceQuery(ByVal query As String) As String
        'Builds and returns a query string for generating STORE price tags that is based on whether values have 
        'been defined for the sort code, retail and advertised price fields. Otherwise the original query is returned.

        Dim sql = query

        If Not String.IsNullOrEmpty(txt_store_sort_cd_from.Text) And
           Not String.IsNullOrEmpty(txt_store_sort_cd_to.Text) Then
            sql = sql & ",ITM$ITM_SRT c "
        End If
        sql = sql & "WHERE a.ITM_CD = b.ITM_CD AND b.STORE_CD =:STORE_CD "

        If Not String.IsNullOrEmpty(txt_store_sort_cd_from.Text) And
           Not String.IsNullOrEmpty(txt_store_sort_cd_to.Text) Then
            sql = sql & "AND c.ITM_CD = a.ITM_CD "
            sql = sql & "AND c.ITM_SRT_CD BETWEEN :ITM_SRT1 AND :ITM_SRT2 "
        End If

        If IsDate(dt_store_ret_prc_from.Value) And IsDate(dt_store_ret_prc_to.Value) Then
            If (IsDateNull(dt_store_ret_prc_from.Value) = False And
                IsDateNull(dt_store_ret_prc_to.Value) = False) Then
                sql = sql & "AND a.RET_PRC_CHNG_DT BETWEEN :RET_PRC_FROM AND :RET_PRC_TO "
            End If
        End If

        If IsDate(dt_store_adv_prc_from.Value) And IsDate(dt_store_adv_prc_to.Value) Then
            If (IsDateNull(dt_store_adv_prc_from.Value) = False And
                IsDateNull(dt_store_adv_prc_to.Value) = False) Then
                sql = sql & "AND a.PRC3_CHNG_DT BETWEEN :ADV_PRC_FROM AND :ADV_PRC_TO "
            End If
        End If

        Return sql
    End Function


    Private Function GetIndividualSortCodeAndPriceQuery(ByVal query As String) As String
        'Builds and returns a query string for generating INDIVIDUAL price tags that is based on whether values  
        ' have been defined for the sort code, retail and advertised price fields. Otherwise the original query is returned.

        Dim sql = query

        If Not String.IsNullOrEmpty(txt_ind_sort_cd_from.Text) And
            Not String.IsNullOrEmpty(txt_ind_sort_cd_to.Text) Then
            sql = sql & ",ITM$ITM_SRT c "
        End If
        sql = sql & "WHERE a.ITM_CD = b.ITM_CD AND b.ITM_CD =:ITM_CD "

        If Not String.IsNullOrEmpty(txt_ind_sort_cd_from.Text) And
            Not String.IsNullOrEmpty(txt_ind_sort_cd_to.Text) Then
            sql = sql & "AND c.ITM_CD = a.ITM_CD "
            sql = sql & "AND c.ITM_SRT_CD BETWEEN :ITM_SRT1 AND :ITM_SRT2 "
        End If

        If IsDate(dt_ind_ret_prc_from.Value) And IsDate(dt_ind_ret_prc_to.Value) Then
            If (IsDateNull(dt_ind_ret_prc_from.Value) = False And
                IsDateNull(dt_ind_ret_prc_to.Value) = False) Then
                sql = sql & "AND a.RET_PRC_CHNG_DT BETWEEN :RET_PRC_FROM AND :RET_PRC_TO "
            End If
        End If

        If IsDate(dt_ind_adv_prc_from.Value) And IsDate(dt_ind_adv_prc_to.Value) Then
            If (IsDateNull(dt_ind_adv_prc_from.Value) = False And
                IsDateNull(dt_ind_adv_prc_to.Value) = False) Then
                sql = sql & "AND a.PRC3_CHNG_DT BETWEEN :ADV_PRC_FROM AND :ADV_PRC_TO "
            End If
        End If
        Return sql

    End Function


    Private Shared Function GetDbConnectionString() As String
        Return (ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
    End Function


    'Private Function GetPromPrc(ByVal sql As String, ByRef promPrc As String,
    '                            ByRef promCd As String, ByRef promDes As String) As Boolean

    '    'Runs the sql statement 
    '    Dim resultFound As Boolean = False
    '    Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
    '    Dim rdr As OracleDataReader = Nothing

    '    Try
    '        'create command object to work with SELECT
    '        Dim cmd As New OracleCommand(sql, conn)
    '        cmd.Connection.Open()

    '        'get the DataReader object from command object
    '        rdr = cmd.ExecuteReader

    '        If rdr.HasRows Then
    '            rdr.Read()
    '            promPrc = rdr.Item("PRC")
    '            promCd = rdr.Item("PRC_CD")
    '            promDes = rdr.Item("DES")
    '            resultFound = True
    '        End If
    '    Catch ex As Exception
    '        lbl_information.Text = ex.Message
    '    Finally
    '        If Not IsNothing(rdr) Then
    '            rdr.Close()
    '        End If
    '        conn.Close()
    '    End Try

    '    Return resultFound
    'End Function

    Private Sub GenerateTagsAsXmlFile(ds As DataSet)
        Dim randObj As New Random
        Dim docNum As String = randObj.Next(50000)
        Dim path As String = Request.ServerVariables("APPL_PHYSICAL_PATH") & "\Data\"
        Dim docName As String = (path & docNum & ".xml")

        ds.WriteXml(docName)
        lbl_tag_id.Text = docNum
    End Sub

    Private Sub CopyDataRow(ByRef fromDataRow As DataRow, ByRef toDataRow As DataRow)
        'Copies the data from the passed-in data row to the new data row- this is the common data used by almost all tags

        toDataRow("itm_cd") = fromDataRow("itm_cd")
        toDataRow("vsn") = fromDataRow("vsn")
        toDataRow("des") = fromDataRow("des")
        toDataRow("siz") = fromDataRow("siz")
        toDataRow("prc1") = fromDataRow("prc1")
        toDataRow("adv_prc") = fromDataRow("adv_prc")
        toDataRow("ret_prc") = fromDataRow("ret_prc")
        toDataRow("ve_cd") = fromDataRow("ve_cd")
        toDataRow("repl_cst") = fromDataRow("repl_cst")
        toDataRow("qty") = "1"
        toDataRow("combo_num") = fromDataRow("combo_num")
        toDataRow("store_cd") = fromDataRow("store_cd")
        toDataRow("loc_cd") = fromDataRow("loc_cd")
        toDataRow("mnr_cd") = fromDataRow("mnr_cd")
        toDataRow("cat_cd") = fromDataRow("cat_cd")
        toDataRow("style_cd") = fromDataRow("style_cd")
        toDataRow("cover") = fromDataRow("cover")
        toDataRow("grade") = fromDataRow("grade")
        toDataRow("finish") = fromDataRow("finish")
        toDataRow("drop_cd") = fromDataRow("drop_cd")
        toDataRow("drop_dt") = fromDataRow("drop_dt")
        toDataRow("family_cd") = fromDataRow("family_cd")
        toDataRow("related_itm_cd") = fromDataRow("related_itm_cd")
        toDataRow("ret_prc_chng_dt") = fromDataRow("ret_prc_chng_dt")
        toDataRow("spiff") = fromDataRow("spiff")
        toDataRow("vol") = fromDataRow("vol")
        toDataRow("weight") = fromDataRow("weight")
        toDataRow("warranty_info") = fromDataRow("warranty_info")
        toDataRow("comm_cd") = fromDataRow("comm_cd")
        toDataRow("ist_po_cd") = fromDataRow("ist_po_cd")
        toDataRow("ist_po_final_dt") = fromDataRow("ist_po_final_dt")
        toDataRow("ist_po_seq_no") = fromDataRow("ist_po_seq_no")
        toDataRow("ist_po_srt_cd") = fromDataRow("ist_po_srt_cd")
        toDataRow("ist_po_wr_dt") = fromDataRow("ist_po_wr_dt")
        toDataRow("promotion_cd") = fromDataRow("promotion_cd")
        toDataRow("promotion_desc") = fromDataRow("promotion_desc")
        toDataRow("promotion_ret_prc") = fromDataRow("promotion_ret_prc")
        toDataRow("outlet_nas_id") = fromDataRow("outlet_nas_id")
        toDataRow("itm_comment") = fromDataRow("itm_comment")
        toDataRow("family_cd") = fromDataRow("family_cd")
        toDataRow("features_benefits") = fromDataRow("features_benefits")
        toDataRow("frt_fac") = fromDataRow("frt_fac")
        toDataRow("ivc_cst") = fromDataRow("ivc_cst")
        toDataRow("lst_act_dt") = fromDataRow("lst_act_dt")
        toDataRow("pallet_qty") = fromDataRow("pallet_qty")
        toDataRow("po_lead_time") = fromDataRow("po_lead_time")
        toDataRow("current_prc") = fromDataRow("current_prc")

    End Sub


    ''effDt passed as object since the date value could be null.  if defined As Date throws an exception
    ''when trying to cast the date read from a datarow
    'Private Sub PopulatePromoDetails(ByRef dr As DataRow, ByVal effDt As Object)
    '    ' Obtains promotion price, code and description using the item and store code - no E1 api available for this.
    '    ' Note that it is possible not to have a promotional price for the store and effective date
    '    ' The values found (if any) are set on the passed-in row
    '    Dim promoFound As Boolean = False
    '    Dim promoPrc As String = ""
    '    Dim promoCd As String = ""
    '    Dim promoDes As String = ""
    '    Dim sql As String = ""
    '    Dim itemCd As String = dr("ITM_CD")
    '    Dim storeCd As String = dr("STORE_CD")

    '    If (IsDBNull(effDt)) Then
    '        effDt = Date.Now
    '    End If

    '    If (SysPms.isPrcByStoreGrp) Then
    '        sql = GetPromPrcStoreQuery(itemCd, storeCd, effDt)
    '        promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes)

    '        If Not promoFound Then
    '            sql = GetPromPrcStoreGrpQuery(itemCd, storeCd, effDt)
    '            promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes)
    '        End If
    '    Else
    '        sql = GetPromPrcItmQuery(itemCd, storeCd, effDt)
    '        promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes)
    '    End If

    '    If promoFound Then
    '        dr("promotion_cd") = promoCd
    '        dr("promotion_desc") = promoDes
    '        dr("promotion_ret_prc") = promoPrc
    '    End If
    'End Sub


    Private Sub PopulateLocations(ByVal storeCd As String, ByRef cbo As DevExpress.Web.ASPxEditors.ASPxComboBox)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim ds As New DataSet
        Dim SQL As String = "SELECT loc_cd, des, loc_cd || ' - ' || des as full_desc FROM LOC WHERE STORE_CD='" &
                            storeCd &
                            "' order by loc_cd"
        Try
            conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())

            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds, "SO")

            With cbo
                .DataSource = ds.Tables("SO")
                .ValueField = "loc_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub


    Private Sub PopulateSkuDatagrid(ByRef dt As DataTable)
        'Create a new data set for displaying only a subset of columns retrieved from the db

        Dim displayTable As DataTable = New DataTable
        Dim displayRow As DataRow
        Dim dr As DataRow

        displayTable.Columns.Add("ITEM")
        displayTable.Columns.Add("DESCRIPTION")
        displayTable.Columns.Add("RET PRICE")
        displayTable.Columns.Add("ADV PRICE")
        displayTable.Columns.Add("VENDOR")

        For Each dr In dt.Rows
            displayRow = displayTable.NewRow()
            displayRow("ITEM") = dr("itm_cd")
            displayRow("DESCRIPTION") = dr("des")
            displayRow("RET PRICE") = dr("ret_prc")
            displayRow("ADV PRICE") = dr("adv_prc")
            displayRow("VENDOR") = dr("ve_cd")
            displayTable.Rows.Add(displayRow)
        Next

        DataGridView1.DataSource = displayTable
        DataGridView1.AutoGenerateColumns = True
        DataGridView1.KeyFieldName = "ITEM"
        DataGridView1.DataBind()

    End Sub


    Private Sub PopulatePkgDatagrid(ByRef dt As DataTable)
        'Create a new dataset for displaying only a subset of columns retrieved from the db

        Dim newDs As DataSet = New DataSet
        Dim displayTable As DataTable = New DataTable
        Dim displayRow As DataRow
        Dim dr As DataRow
        Dim pkgSku As String = ""

        'columns to be displayed in the grid
        displayTable.Columns.Add("ITEM")
        displayTable.Columns.Add("DESCRIPTION")
        displayTable.Columns.Add("RET PRICE")
        displayTable.Columns.Add("ADV PRICE")
        displayTable.Columns.Add("VENDOR")

        For Each dr In dt.Rows
            'Adds a package header to better represent the package
            If (dr("parent_itm_cd") <> pkgSku) Then
                displayRow = displayTable.NewRow()
                displayRow("ITEM") = dr("parent_itm_cd")
                displayRow("DESCRIPTION") = dr("parent_des")
                displayRow("VENDOR") = dr("parent_ve_cd")
                displayRow("RET PRICE") = dr("parent_ret_prc")
                displayRow("ADV PRICE") = dr("parent_adv_prc")
                displayTable.Rows.Add(displayRow)
                'next is done to only add one header for the group of components
                pkgSku = dr("parent_itm_cd")
            End If

            'Adds the next component
            displayRow = displayTable.NewRow()
            displayRow("ITEM") = dr("component_itm_cd")
            displayRow("DESCRIPTION") = dr("component_des")
            displayRow("VENDOR") = dr("component_ve_cd")
            displayRow("RET PRICE") = dr("component_ret_prc")
            displayRow("ADV PRICE") = dr("component_adv_prc")
            displayTable.Rows.Add(displayRow)
        Next

        DataGridView1.DataSource = displayTable
        DataGridView1.DataBind()

    End Sub


    Private Sub LoadCombos()
        LoadStoreCombos()
        LoadTemplateCombos()
    End Sub


    Private Sub LoadStoreCombos()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim ds As New DataSet
        Dim SQL As String

        conn = DisposablesManager.BuildOracleConnection(GetDbConnectionString())

        Try
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds, "SO")

            'Populate the stores on STORE/INDIVIDUAL store combo boxes
            With cbo_store_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            With cbo_ind_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

        Catch ex As Exception
            lbl_information.Text = Err.Description
        Finally
            conn.Close()
        End Try
    End Sub


    Private Sub LoadTemplateCombos()
        'Populates the list of Individual and Group templates available
        Dim path As DirectoryInfo

        path = New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Templates\Individual\")
        txt_ptag.Items.Clear()
        txt_ptag.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A File", ""))
        txt_ptag.Items.FindByText("Please Select A File").Value = ""
        txt_ptag.SelectedIndex = 0
        With txt_ptag
            .DataSource = path.GetFiles("*.repx")
            .ValueField = "Name"
            .TextField = "Name"
            .DataBind()
        End With

        path = New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Templates\Group\")
        txt_gtag.Items.Clear()
        txt_gtag.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A File", ""))
        txt_gtag.Items.FindByText("Please Select A File").Value = ""
        txt_gtag.SelectedIndex = 0
        With txt_gtag
            .DataSource = path.GetFiles("*.repx")
            .ValueField = "Name"
            .TextField = "Name"
            .DataBind()
        End With
    End Sub


    Private Sub ResetCommonComponents()
        DataGridView1.DataSource = ""
        DataGridView1.DataBind()
        'clear any error messages that might have previously displayed
        lbl_information.Text = ""
    End Sub


    Private Sub ValidateStoreRanges()

        Dim dtAdvPriceFromIsNull As Boolean
        Dim dtAdvPriceToIsNull As Boolean
        Dim dtRetPriceFromIsNull As Boolean
        Dim dtRetPriceToIsNull As Boolean
        Dim sortCdFromIsNull As Boolean
        Dim sortCdToIsNull As Boolean

        dtAdvPriceFromIsNull = IsDateNull(dt_store_adv_prc_from.Value)
        dtAdvPriceToIsNull = IsDateNull(dt_store_adv_prc_to.Value)
        If (Not (dtAdvPriceFromIsNull = dtAdvPriceToIsNull)) Then
            If (dtAdvPriceFromIsNull) Then
                dt_store_adv_prc_from.Value = dt_store_adv_prc_to.Value
            Else
                dt_store_adv_prc_to.Value = dt_store_adv_prc_from.Value
            End If
        End If

        dtRetPriceFromIsNull = IsDateNull(dt_store_ret_prc_from.Value)
        dtRetPriceToIsNull = IsDateNull(dt_store_ret_prc_to.Value)
        If (Not (dtRetPriceFromIsNull = dtRetPriceToIsNull)) Then
            If (dtRetPriceFromIsNull) Then
                dt_store_ret_prc_from.Value = dt_store_ret_prc_to.Value
            Else
                dt_store_ret_prc_to.Value = dt_store_ret_prc_from.Value
            End If
        End If

        sortCdFromIsNull = String.IsNullOrEmpty(txt_store_sort_cd_from.Text)
        sortCdToIsNull = String.IsNullOrEmpty(txt_store_sort_cd_to.Text)
        If (Not (sortCdFromIsNull = sortCdToIsNull)) Then
            If (sortCdFromIsNull) Then
                txt_store_sort_cd_from.Text = txt_store_sort_cd_to.Text
            Else
                txt_store_sort_cd_to.Text = txt_store_sort_cd_from.Text
            End If
        End If

    End Sub


    Private Sub ValidateIndividualRanges()

        Dim dtAdvPriceFromIsNull As Boolean
        Dim dtAdvPriceToIsNull As Boolean
        Dim dtRetPriceFromIsNull As Boolean
        Dim dtRetPriceToIsNull As Boolean
        Dim sortCdFromIsNull As Boolean
        Dim sortCdToIsNull As Boolean

        dtAdvPriceFromIsNull = IsDateNull(dt_ind_adv_prc_from.Value)
        dtAdvPriceToIsNull = IsDateNull(dt_ind_adv_prc_to.Value)
        If (Not (dtAdvPriceFromIsNull = dtAdvPriceToIsNull)) Then
            If (dtAdvPriceFromIsNull) Then
                dt_ind_adv_prc_from.Value = dt_ind_adv_prc_to.Value
            Else
                dt_ind_adv_prc_to.Value = dt_ind_adv_prc_from.Value
            End If
        End If

        dtRetPriceFromIsNull = IsDateNull(dt_ind_ret_prc_from.Value)
        dtRetPriceToIsNull = IsDateNull(dt_ind_ret_prc_to.Value)
        If (Not (dtRetPriceFromIsNull = dtRetPriceToIsNull)) Then
            If (dtRetPriceFromIsNull) Then
                dt_ind_ret_prc_from.Value = dt_ind_ret_prc_to.Value
            Else
                dt_ind_ret_prc_to.Value = dt_ind_ret_prc_from.Value
            End If
        End If

        sortCdFromIsNull = String.IsNullOrEmpty(txt_ind_sort_cd_from.Text)
        sortCdToIsNull = String.IsNullOrEmpty(txt_ind_sort_cd_to.Text)
        If (Not (sortCdFromIsNull = sortCdToIsNull)) Then
            If (sortCdFromIsNull) Then
                txt_ind_sort_cd_from.Text = txt_ind_sort_cd_to.Text
            Else
                txt_ind_sort_cd_to.Text = txt_ind_sort_cd_from.Text
            End If
        End If

    End Sub


    Private Function IsDateNull(ByVal dateToCheck As DateTime) As Boolean
        'This function checks if the date field passed-in can be treated as being equal to null. 
        'By virtue of being a DateTime type, a date field can never be null- it always has a timestamp 
        'behind the scenes.Therefore, traditional not null checks will always fail. In this case,we have
        'to compare the DateTime.MinValue or 1.1.0001 12:00:00 because even when cleared or "null",
        'the field really contains that value.
        Dim result As Boolean = False
        If DateTime.Compare(dateToCheck, DateTime.MinValue) = 0 Then
            result = True
        End If

        Return result
    End Function


    Private Function GetRfIdsForPo(ByVal poCd As String,
                                   ByVal lnNum As String) As List(Of String)
        'Returns the RFIDs found for the PO code and Line Number passed in, OR 
        ' an empty list if either of the required values passed in is null.

        If (Not (String.IsNullOrEmpty(poCd)) And Not (String.IsNullOrEmpty(lnNum))) Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
            Dim cmd As OracleCommand
            Dim sql As String = "SELECT rf.rf_id_cd FROM po_ln2rf_id_cd rf " &
                                " WHERE po_cd = :poCd AND" &
                                "       ln# = :lnNum " &
                                " ORDER BY rf_id_cd "
            Try
                conn.Open()
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd.Parameters.Add(":poCd", OracleType.VarChar)
                cmd.Parameters(":poCd").Value = poCd            'dr("IST_PO_CD")
                cmd.Parameters.Add(":lnNum", OracleType.Number)
                cmd.Parameters(":lnNum").Value = CInt(lnNum)    'dr("LINE_NUM")

                Return GetRfIds(cmd)

            Catch ex As Exception
                'do nothing an emtpy list will be returned
            Finally
                conn.Close()
            End Try

        End If

        Return (New List(Of String))  'if the call failed in any way, returns an empty list

    End Function


    Private Function GetRfIdsForInvLoc(ByVal storeCd As String,
                                       ByVal locCd As String,
                                       ByVal itmCd As String,
                                       ByVal idCd As String) As List(Of String)
        'Returns the RFIDs found for the values passed in OR an empty list if none are found
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        Dim cmd As OracleCommand
        Dim sql As String = "SELECT ix.rf_id_cd, ix.qty FROM inv_xref ix WHERE store_cd = :storeCd " &
           " AND loc_cd = :locCd AND itm_cd = :itmCd AND del_doc_num IS NULL AND ist_store_cd IS NULL " &
           " AND replaced_by IS NULL "

        If Not String.IsNullOrEmpty(idCd) Then
            sql = sql & " AND crpt_id_num = :idCd "
        End If
        sql = sql & " ORDER BY rf_id_cd "

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":storeCd", OracleType.VarChar)
            cmd.Parameters(":storeCd").Value = storeCd 'dr("STORE_CD")
            cmd.Parameters.Add(":locCd", OracleType.VarChar)
            cmd.Parameters(":locCd").Value = locCd 'dr("LOC_CD")
            cmd.Parameters.Add(":itmCd", OracleType.VarChar)
            cmd.Parameters(":itmCd").Value = itmCd 'dr("ITM_CD")

            If Not String.IsNullOrEmpty(idCd) Then
                cmd.Parameters.Add(":idCd", OracleType.VarChar)
                cmd.Parameters(":idCd").Value = idCd 'dr("COMBO_NUM")
            End If

            Return GetRfIds(cmd)

        Catch ex As Exception
            'do nothing an emtpy list will be returned
        Finally
            conn.Close()
        End Try

        Return (New List(Of String))  'if the call failed in any way, returns an empty list

    End Function


    Private Function GetRfIdsForIst(ByVal istWrDT As Date,
                                    ByVal istStoreCd As String,
                                    ByVal istSeqNum As String,
                                    ByVal lnNum As Double) As List(Of String)
        'Returns the RFIDs found for the values passed in OR an empty list if none are found
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(GetDbConnectionString())
        Dim cmd As OracleCommand
        Dim sql As String = "SELECT ix.rf_id_cd, ix.qty FROM inv_xref ix WHERE ist_wr_dt = :istWrDt" &
           " AND ist_store_cd = :istStoreCd AND ist_seq_num = :istSeqNum AND ist_ln# = :istLnNum AND REPLACED_BY IS NULL " &
           " ORDER BY ix.rf_id_cd "

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":istWrDt", OracleType.DateTime)
            cmd.Parameters(":istWrDt").Value = FormatDateTime(istWrDT.Date, DateFormat.ShortDate) 'dr("IST_PO_WR_DT")
            cmd.Parameters.Add(":istStoreCd", OracleType.VarChar)
            cmd.Parameters(":istStoreCd").Value = istStoreCd 'dr("IST_STORE_CD")
            cmd.Parameters.Add(":istSeqNum", OracleType.VarChar)
            cmd.Parameters(":istSeqNum").Value = istSeqNum ' dr("IST_PO_SEQ_NO")
            cmd.Parameters.Add(":istLnNum", OracleType.Number)
            cmd.Parameters(":istLnNum").Value = CInt(lnNum) 'dr("LINE_NUM")

            Return GetRfIds(cmd)

        Catch ex As Exception
            'do nothing an emtpy list will be returned
        Finally
            conn.Close()
        End Try

        Return (New List(Of String))  'if the call failed in any way, returns an empty list
    End Function


    Private Function GetRfIds(ByRef cmd As OracleCommand) As List(Of String)

        Dim rfIds As List(Of String) = New List(Of String)
        Dim oAdp As OracleDataAdapter
        Dim rfIdsDatSet As DataSet = New DataSet
        Dim dr As DataRow

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(rfIdsDatSet)
            For Each dr In rfIdsDatSet.Tables(0).Rows
                rfIds.Add(dr("rf_id_cd"))
            Next
        Catch ex As Exception
            'do nothing with this exception, let it return an empty list of RF-IDs
        End Try

        Return rfIds
    End Function


End Class
