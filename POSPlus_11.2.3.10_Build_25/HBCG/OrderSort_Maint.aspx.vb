Imports System.Data.OracleClient
Imports System.Data
Imports System.Xml
Imports HBCG_Utils

Partial Class OrderSort_Maint
    Inherits POSBasePage
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        ds = New DataSet

        GridView1.DataSource = ""

        GridView1.Visible = True

        'Open Connection 
        conn.Open()

        sql = "SELECT ORD_SRT_CD, DES, ACTIVE FROM ORD_SRT_MAINT "
        If DropDownList1.Value = "Active" Then
            sql = sql & " WHERE ACTIVE='Y' "
        ElseIf DropDownList1.Value = "Inactive" Then
            sql = sql & " WHERE ACTIVE='N' "
        End If
        sql = sql & "ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        GridView1.Visible = True
        GridView1.DataSource = dv
        GridView1.DataBind()

        'Close Connection 
        conn.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If
        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "<font color=red>* TRAIN MODE *</font>"
        End If

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            Exit Sub
        End If

        If Not IsPostBack Then
            Update_Sort_Codes()
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        Dim CheckBox2 As CheckBox = CType(e.Item.FindControl("CheckBox2"), CheckBox)

        If Not IsNothing(CheckBox2) Then
            If e.Item.Cells(3).Text = "Y" Then
                CheckBox2.Checked = True
            End If
        End If

    End Sub

    Public Sub Update_Active(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE ORD_SRT_MAINT SET ACTIVE='" & ck_value & "' WHERE ORD_SRT_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Update_Sort_Codes()

    End Sub

    Public Sub Update_Sort_Codes()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        SQL = "SELECT * FROM ORD_SRT_MAINT ORDER BY ORD_SRT_CD"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL
            End With


            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
        Catch
            Throw
        End Try

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        Dim match_found As Boolean = False
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn2.Open()

        SQL = "SELECT * FROM ORD_SRT_CD ORDER BY ORD_SRT_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn2)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                match_found = False
                For Each dr In ds.Tables(0).Rows
                    If dr("ORD_SRT_CD") = MyDataReader.Item("ORD_SRT_CD").ToString Then
                        match_found = True
                        Exit For
                    End If
                Next
                If match_found = True Then
                    SQL = "UPDATE ORD_SRT_MAINT SET DES=:DES "
                    SQL = SQL & "WHERE ORD_SRT_CD=:ORD_SRT_CD"
                Else
                    SQL = "INSERT INTO ORD_SRT_MAINT (DES, ORD_SRT_CD) VALUES(:DES, "
                    SQL = SQL & ":ORD_SRT_CD)"
                End If
                cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
                cmdGetCodes.Parameters.Add(":ORD_SRT_CD", OracleType.VarChar)
                cmdGetCodes.Parameters(":ORD_SRT_CD").Value = MyDataReader.Item("ORD_SRT_CD").ToString
                cmdGetCodes.Parameters.Add(":DES", OracleType.VarChar)
                cmdGetCodes.Parameters(":DES").Value = MyDataReader.Item("DES").ToString
                cmdGetCodes.ExecuteNonQuery()
            Loop

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
            conn2.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        bindgrid()

    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged

        bindgrid()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
