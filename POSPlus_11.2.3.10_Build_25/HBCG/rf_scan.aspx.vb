﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic
Imports System.Web.SessionState
Imports HBCG_Utils
Imports SalesUtils
Imports System.Math
Partial Class _Default

    Inherits POSBasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("lucy_rf_scan") = "N"
        Session("lucy_been_to_rf_scan") = "Y"
        txt_itm_rf_scan.Text = ""
        txt_store_cd_rf_scan.Text = ""
        txt_loc_cd_rf_scan.Text = ""
        txt_rf_id.Focus()
    End Sub

    Public Function check_dup_rf(ByRef p_rfid As String) As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()


        sql = "select rf_id_cd from lucy_pos_rf_id where rf_id_cd='" & p_rfid & "' and  SESSION_ID ='" & Session.SessionID.ToString.Trim & "'"
        ' sql = sql & "and store_cd = '" & Session("store_cd") & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                lbl_rf_scan.Text = "RF_ID=" & p_rfid & "  has been scanned "
                txt_rf_id.Text = ""
                Return "N"
                Exit Function
            Else
                Return "Y"

            End If
            MyDataReader.Close()

        Catch
            conn.Close()
            Throw

        End Try


    End Function

    Function check_redp_store(ByRef p_store_cd As String, ByRef p_y_n As String) As String
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_rp_interface.isRPPilotStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.Char)).Value = p_store_cd
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue




        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_y_n = myCMD.Parameters("p_y_n").Value
            p_y_n = str_y_n
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
            str_y_n = "N"
            objConnection.Close()
        End Try
    End Function
    Public Function use_alt_rf_id(ByRef p_rf_id As String, ByRef p_store_cd As String, ByRef p_alt_rf_id As String) As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim x As Exception
        Dim MyDataReader As OracleDataReader
        Dim str_flag As String
        Session("lucy_rf_scan") = "N"
        'Session("store_cd") = "HY"   'need to remove
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()
        sql = "select rf_id_cd from inv_xref   "
        sql = sql & " where alt_rf_id_cd='" & p_alt_rf_id & "' "
        sql = sql & " and store_cd='" & p_store_cd & "'"



        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                p_rf_id = MyDataReader.Item("rf_id_cd")
                str_flag = "Y"
            Else
                str_flag = "N"
                p_rf_id = ""
                lbl_rf_scan.Text = "No record, could be wrong RFID or wrong store code with RFID=" & p_rf_id
            End If
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
            Throw
            conn.Close()
        End Try
        Return str_flag
    End Function


    Public Function get_redp_record(ByRef p_rf_id_cd As String, ByRef p_store_cd As String) As String
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_alt_rf_id As String
        Dim str_err As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_redp_rf.redp_main"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_rf_id_cd", OracleType.Char)).Value = p_rf_id_cd
        myCMD.Parameters.Add(New OracleParameter("p_store_cd", OracleType.Char)).Value = p_store_cd

        myCMD.Parameters.Add("p_itm_cd", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_loc_cd", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_count", OracleType.Number, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("v_err", OracleType.VarChar, 50).Direction = ParameterDirection.Output

        myCMD.ExecuteNonQuery()
        str_err = myCMD.Parameters("v_err").Value
        If str_err <> "N" Then
            lbl_rf_scan.Text = str_err
            Exit Function
        End If
        Session("str_itm_cd") = myCMD.Parameters("p_itm_cd").Value
        Session("str_store_cd") = p_store_cd
        Session("str_loc_cd") = myCMD.Parameters("p_loc_cd").Value

        Session("rf_id_cd") = p_rf_id_cd

        txt_itm_rf_scan.Text = Session("str_itm_cd")
        txt_store_cd_rf_scan.Text = Session("str_store_cd")
        txt_loc_cd_rf_scan.Text = Session("str_loc_cd")


        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try

            MyDA.Fill(Ds)
            Return str_err
        Catch x
            Throw

            objConnection.Close()
        End Try
        objConnection.Close()
    End Function


    Public Sub get_non_redp_record(ByRef p_rfid As String, ByRef p_store_cd As String)
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim str_nas_out_id As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader

        Session("lucy_rf_scan") = "N"
        'Session("store_cd") = "HY"   'need to remove
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()
        sql = "select itm_cd,store_cd,loc_cd,nas_out_id,disposition from inv_xref   "
        sql = sql & " where rf_id_cd='" & p_rfid & "' "
        sql = sql & "and store_cd = '" & p_store_cd & "'"
        'sql = sql & " and disposition <> '" & str_disposition & "' "
        sql = sql & " and disposition in ('FIF','OUT') " ' "
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("str_itm_cd") = MyDataReader.Item("itm_cd")
                Session("str_store_cd") = MyDataReader.Item("store_cd")
                Session("str_loc_cd") = MyDataReader.Item("loc_cd")
                Session("rf_id_cd") = p_rfid
                Session("disposition") = MyDataReader.Item("disposition")
                If MyDataReader.Item("disposition") = "OUT" And IsDBNull(MyDataReader.Item("nas_out_Id")) = False Then
                    Session("nas_out_id") = MyDataReader.Item("nas_out_Id")
                End If
                str_store_cd = MyDataReader.Item("store_cd")

                txt_itm_rf_scan.Text = Session("str_itm_cd")
                txt_store_cd_rf_scan.Text = Session("str_store_cd")
                txt_loc_cd_rf_scan.Text = Session("str_loc_cd")
            Else
                lbl_rf_scan.Text = "No record, could be wrong RFID or wrong store code with RFID=" & txt_rf_id.Text
                txt_rf_id.Text = ""
                Exit Sub
            End If
            MyDataReader.Close()
            txt_rf_id.Text = ""
        Catch
            conn.Close()
            Throw

        End Try
        txt_rf_id.Focus()
        conn.Close()
    End Sub
    Public Sub delete_lucy_pos_rf_id(ByRef p_rfid As String)
        Dim sql As String
        Dim connString As String
        Dim thistransaction As OracleTransaction
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        thistransaction = objConnection.BeginTransaction
        sql = "DELETE FROM lucy_pos_rf_id WHERE rf_id_cd =  '" & p_rfid & "'  AND SESSION_ID <>'" & Session.SessionID.ToString.Trim & "'"

        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = sql
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        objConnection.Close()

    End Sub

    Public Sub rf_scanned()
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim p_alt_rf_id As String
        Dim str_rf_id As String = UCase(Trim(txt_rf_id.Text))
        Dim str_y_n As String
        Session("lucy_rf_scan") = "N"
        lbl_rf_scan.Text = "In progress ..."
        str_store_cd = Session("store_cd")
        If Left(str_rf_id, 1) = "M" Or (Len(str_rf_id) = 11 And Left(str_rf_id, 1) <> "S") Then ' alt rf_id for thr brick

            p_alt_rf_id = Right(str_rf_id, 11)
            str_y_n = use_alt_rf_id(str_rf_id, str_store_cd, p_alt_rf_id)
            If str_y_n = "N" Or Len(str_rf_id) < 10 Then
                lbl_rf_scan.Text = "No record, could be wrong RFID or wrong store code "
                Exit Sub
            End If
        Else
            str_rf_id = Right(str_rf_id, 10)
        End If


        str_rfid = str_rf_id
        delete_lucy_pos_rf_id(str_rfid)
        Dim str_check As String = check_dup_rf(str_rfid)
        If str_check = "N" Then
            lbl_rf_scan.Text = "RF_ID=" & str_rfid & "  has been scanned "
            txt_rf_id.Text = ""
            Exit Sub
        End If

        str_store_cd = Session("store_cd")
        lbl_rf_scan.Text = "In progress ..."

        check_redp_store(str_store_cd, str_y_n)
        If str_y_n <> "Y" Then 'non redp
            get_non_redp_record(str_rfid, str_store_cd)
        Else
            get_redp_record(str_rfid, str_store_cd)   'redp
        End If




        txt_rf_id.Focus()


    End Sub

    Public Sub btn_add_itm_rf_scan_Click(sender As Object, e As EventArgs) Handles btn_add_itm_rf_scan.Click
        Dim str_itm_cd As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader

        Session("lucy_rf_scan") = "N"
        'lbl_rf_scan.Text = "Please scan a RFID"
        lbl_rf_scan.Text = Resources.LibResources.Label409
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()
        Dim str_rfid As String = Right(Trim(txt_rf_id.Text), 10)
        sql = "select itm_cd,store_cd,loc_cd from inv_xref where rf_id_cd='" & str_rfid & "' "
        sql = sql & "and store_cd = '" & Session("store_cd") & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("str_itm_cd") = Trim(MyDataReader.Item("itm_cd"))
                Session("str_store_cd") = MyDataReader.Item("store_cd")
                Session("str_loc_cd") = MyDataReader.Item("loc_cd")

                str_store_cd = MyDataReader.Item("store_cd")

                txt_itm_rf_scan.Text = Session("str_itm_cd")
                txt_store_cd_rf_scan.Text = Session("str_store_cd")
                txt_loc_cd_rf_scan.Text = Session("str_loc_cd")
            Else
                lbl_rf_scan.Text = "No record, could be wrong RFID or wrong store code "
                txt_rf_id.Text = ""
                Exit Sub
            End If

            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
            lbl_rf_scan.Text = Err.Description
        End Try
        conn.Close()

    End Sub
    Protected Sub cp_btn_exit_rf_scan_Click()
        If Session("str_itm_cd") <> "" And Session("str_store_cd") <> "" And Session("str_loc_cd") <> "" Then
            Session("lucy_rf_scan") = "Y"
            Response.Redirect("order_detail.aspx")
        Else
            Session("lucy_rf_scan") = "N"
        End If


    End Sub

    Protected Sub btn_exit_rf_scan_Click(sender As Object, e As EventArgs) Handles btn_exit_rf_scan.Click
        If Session("str_itm_cd") <> "" And Session("str_store_cd") <> "" And Session("str_loc_cd") <> "" Then
            Session("lucy_rf_scan") = "Y"

        Else
            Session("lucy_rf_scan") = "N"
        End If

        Response.Redirect("order_detail.aspx")
    End Sub

    Protected Sub txt_rf_id_TextChanged(sender As Object, e As EventArgs) Handles txt_rf_id.TextChanged
        Dim int_length As Integer = txt_rf_id.Text.Length()
        If txt_rf_id.Text <> "" And int_length >= 10 Then
            lbl_rf_scan.Text = "In progress ...."
            rf_scanned()
            cp_btn_exit_rf_scan_Click()
        End If
    End Sub
End Class
