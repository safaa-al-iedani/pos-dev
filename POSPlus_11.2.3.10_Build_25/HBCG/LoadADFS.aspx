
<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="LoadADFS.aspx.vb"
    Inherits="LoadADFS" MasterPageFile="~/MasterPages/NoWizard2.master" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div id="Div1" runat="server">
            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate> 
            <asp:FileUpload ID="FileUpload1" runat="server" ClientIDMode="Static"/><br />
                </ContentTemplate>
                <Triggers>
                <asp:PostBackTrigger ControlID="btn_upload" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
            <asp:Button ID="btn_upload" runat="server" OnClick="btn_upload_Click" 
             Text="Upload ADFS File" />&nbsp;<br />
            <asp:Label ID="Label1" runat="server"></asp:Label>

            <br />
             <asp:TextBox ID="lbl_msg" runat="server" AutoPostBack="true" Width="100%" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
            <br />
            <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server" 
             AutoGenerateColumns="false" CellPadding="2" DataKeyField="primary_sku"
            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
            <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
                font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
            <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
                font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
            <columns>
                <asp:BoundColumn HeaderStyle-Width ="75px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="promotion_code" headertext="PRC_CD" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn HeaderStyle-Width ="75px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="primary_sku" headertext="ITM_CD" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn HeaderStyle-Width ="35px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="sku_qty" headertext="ITM_CD_QTY" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn HeaderStyle-Width ="35px" ItemStyle-HorizontalAlign ="center" HeaderStyle-HorizontalAlign ="center"  DataField="sku_dollars" headertext="ITM_CD_DOL" ReadOnly="True" Visible="True">                
                        </asp:BoundColumn>
                <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                    <ItemStyle ForeColor="Red"/>
                </asp:BoundColumn>
            </columns> 
            <itemstyle verticalalign="Top" />
            </asp:DataGrid>
            <br />
             <asp:Button ID="btn_process" runat="server" Text="Process ADFS File" Width="200px" Visible="false">
             </asp:Button>
            <br />
            <br />
             <asp:Button ID="btn_clear" runat="server" Text="Clear" Width="100px" Visible="true">
             </asp:Button>
        </div>
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
        <br />
        <br />
        <br />
    </asp:Content>

