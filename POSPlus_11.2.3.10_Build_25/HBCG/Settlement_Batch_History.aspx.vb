Imports System.Data.OracleClient
Imports System.Data

Partial Class Settlement_Batch_History
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            WebDateChooser1.Value = Today.Date.AddDays(-1)
            store_cd_populate()
            If Authorize_User(Session("EMP_CD"), "ADMIN_ACCESS") = False Then
                store_cd.Value = Session("HOME_STORE_CD")
                store_cd.Enabled = False
            End If
            bindgrid("STORE_CD asc")
        End If
        lbl_gdr.Text = "$ 0.00"
        lbl_gdf.Text = "$ 0.00"

    End Sub

    Public Shared Function Retrieve_Stores(ByVal store_cd As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim output_store As String = ""

        conn.Open()

        sql = "select store_cd from payment_xref where merchant_id in (select merchant_id from payment_xref where store_cd='"
        sql = sql & store_cd & "') group by store_cd"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDatareader2.Read
                output_store = output_store & "'" & MyDatareader2.Item("STORE_CD").ToString & "',"
            Loop
        Catch
            conn.Close()
            Throw
        End Try

        If output_store & "" = "" Then Return ""
        output_store = Left(output_store, Len(output_store) - 1)

        Return output_store
        conn.Close()

    End Function

    Public Sub bindgrid(ByVal SortField As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        Dim myDataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter

        Dim Source As DataView
        ds = New DataSet
        Dim store_group As String = Retrieve_Stores(store_cd.Value)

        GridView1.DataSource = ""

        GridView1.Visible = True
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "SELECT ASP_TRN.WR_DT, ASP_TRN.CUST_CD, ASP_TRN.MEDIUM_TP_CD, ASP_TRN.STAT_CD, ASP_TRN.BNK_CRD_NUM, ASP_TRN.STORE_CD, "
        sql = sql & "DECODE(ASP_TRN.MEDIUM_TP_CD,'SAL',ASP_TRN.AMT,'MDB',ASP_TRN.AMT,'CRM',-ASP_TRN.AMT,'MCR',-ASP_TRN.AMT,ASP_TRN.AMT) AS AMT, ASP_TRN.APP_CD, ASP_TRN.REF_NUM, ASP_TRN.AS_TRN_TP, ASP_TRN.DEL_DOC_NUM FROM ASP_TRN "
        sql = sql & "WHERE ASP_TRN.xmit_dt_time BETWEEN TO_DATE('" & FormatDateTime(WebDateChooser1.Value, DateFormat.ShortDate) & " 00:00','MM/DD/YYYY HH24:MI') "
        sql = sql & "AND TO_DATE('" & FormatDateTime(WebDateChooser1.Value, DateFormat.ShortDate) & " 23:59','MM/DD/YYYY HH24:MI') "
        sql = sql & "AND ASP_TRN.XMIT_DT_TIME IS NOT NULL "
        If Not String.IsNullOrEmpty(store_group) Then
            sql = sql & "AND ASP_TRN.STORE_CD IN(" & store_group & ") "
        End If
        If cbo_trn_tp.Value <> "ALL" Then
            If cbo_trn_tp.Value = "GDR" Then
                sql = sql & "AND ASP_TRN.MEDIUM_TP_CD = 'GDR' "
            Else
                sql = sql & "AND ASP_TRN.MEDIUM_TP_CD <> 'GDR' "
            End If
        End If

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows > 0 Then
            lbl_grid_msg.Text = ""
            Gridview1.Visible = True
            Source.Sort = SortField
            Gridview1.DataSource = Source
            Gridview1.DataBind()
        Else
            lbl_grid_msg.Text = "No records found for requested store"
            Gridview1.Visible = False
        End If

        'Close Connection 
        conn.Close()

    End Sub

    Public Shared Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT EMP_CD FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("EMP_CD") & "" <> "" Then
                    Authorize_User = True
                Else
                    Authorize_User = False
                End If
            Else
                Authorize_User = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT STORE_CD, STORE_NAME, STORE_CD || ' - ' || STORE_NAME AS FULL_DESC FROM STORE ORDER BY STORE_CD"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With store_cd
                .DataSource = ds
                .ValueField = "STORE_CD"
                .TextField = "FULL_DESC"
                .DataBind()
            End With
            store_cd.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Store", ""))
            store_cd.Items.FindByText("Please Select A Store").Value = ""
            store_cd.SelectedIndex = 0

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles store_cd.SelectedIndexChanged

        bindgrid(Session("SortOrder"))

    End Sub

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        GridView1.CurrentPageIndex = 0 'To sort from top
        bindgrid(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid
        'To retain checkbox on sorting          
        'RePopulateCheckBoxes()

    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        If IsNumeric(e.Item.Cells(4).Text) Then
            Select Case e.Item.Cells(7).Text
                Case "GDR"
                    lbl_gdr.Text = FormatCurrency(CDbl(lbl_gdr.Text) + CDbl(e.Item.Cells(4).Text), 2)
                Case Else
                    lbl_gdf.Text = FormatCurrency(CDbl(lbl_gdf.Text) + CDbl(e.Item.Cells(4).Text), 2)
            End Select
        Else

        End If

    End Sub

    Protected Sub cbo_trn_tp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        bindgrid(Session("SortOrder"))

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Protected Sub WebDateChooser1_ValueChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles WebDateChooser1.ValueChanged

        bindgrid(Session("SortOrder"))

    End Sub
End Class