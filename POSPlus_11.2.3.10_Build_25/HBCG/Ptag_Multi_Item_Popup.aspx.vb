
Imports HBCG_Utils
Imports DevExpress.Web.Data
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports System.Data.OracleClient


Partial Class Ptag_Multi_Item_Popup
    Inherits POSBasePage
    Private ds As DataSet = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dt As DataTable
        Dim dcItemNo As DataColumn
        Dim dcItemCode As DataColumn
        Dim dcItemDesc As DataColumn

        txt_itm_cd.Focus()

        If (IsNothing(Session("ptag_multi_item"))) Then
            ds = New DataSet()
            dt = New DataTable()
            dcItemNo = New DataColumn("itm_No", Type.GetType("System.String"))
            dcItemCode = New DataColumn("itm_code", Type.GetType("System.String"))
            dcItemDesc = New DataColumn("itm_desc", Type.GetType("System.String"))

            dt.Columns.Add(dcItemNo)
            dt.Columns.Add(dcItemCode)
            dt.Columns.Add(dcItemDesc)
            dt.PrimaryKey = New DataColumn() {dt.Columns("itm_No")}
            ds.Tables.Add(dt)

            GV_MultiItem.DataSource = ds
            GV_MultiItem.DataBind()
            Session("ptag_multi_item") = ds
            Session("ptag_item_count") = 0
        Else
            ds = Session("ptag_multi_item")
            GV_MultiItem.DataSource = ds
            GV_MultiItem.DataBind()
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Add Item button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_add_item_Click(sender As Object, e As EventArgs) Handles btn_add_item.Click
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim sqlString As String
        Dim strItmDesc As String
        lbl_warning.Text = ""
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        sqlString = "SELECT des FROM itm WHERE itm_cd = '" & txt_itm_cd.Text & "'"

        objCmd.CommandText = sqlString
        objCmd.CommandType = CommandType.Text
        objCmd.Connection = conn

        conn.Open()
        Dim objItmDesc As Object
        objItmDesc = objCmd.ExecuteScalar()
        If objItmDesc = Nothing Then
            lbl_warning.Text = String.Format(Resources.CustomMessages.MSG0108, txt_itm_cd.Text)
            txt_itm_cd.Text = ""
            Exit Sub
        Else
            strItmDesc = objItmDesc.ToString()
        End If

        conn.Close()

        Dim dt As DataTable
        Dim dr As DataRow

        ds = Session("ptag_multi_item")
        dt = ds.Tables(0)

        If dt.Rows.Count >= 10 Then
            lbl_warning.Text = Resources.CustomMessages.MSG0113
            txt_itm_cd.Text = ""
            Exit Sub
        End If

        dr = dt.NewRow()
        Session("ptag_item_count") = Session("ptag_item_count") + 1
        dr("itm_No") = Session("ptag_item_count")
        dr("itm_code") = txt_itm_cd.Text
        dr("itm_desc") = strItmDesc

        Try
            dt.Rows.Add(dr)
        Catch ex As System.Data.ConstraintException
            lbl_warning.ForeColor = Color.Red
            lbl_warning.Text = String.Format(Resources.CustomMessages.MSG0114, txt_itm_cd.Text)
        End Try

        GV_MultiItem.DataSource = ds
        GV_MultiItem.DataBind()
        Session("ptag_multi_item") = ds
        txt_itm_cd.Text = ""
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Grid view delete event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_MultiItem_DeleteRow(ByVal sender As Object, ByVal e As ASPxDataDeletingEventArgs)
        Dim i As Integer = GV_MultiItem.FindVisibleIndexByKeyValue(e.Keys(GV_MultiItem.KeyFieldName))
        Dim c As Control = GV_MultiItem.FindDetailRowTemplateControl(i, "ASPxGridView2")

        lbl_warning.Text = ""

        e.Cancel = True
        ds = CType(Session("ptag_multi_item"), DataSet)
        ds.Tables(0).Rows.Remove(ds.Tables(0).Rows.Find(e.Keys(GV_MultiItem.KeyFieldName)))
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class