﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="TrafficSlsp.aspx.vb" Inherits="TrafficSlsp" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 338px">
                   <asp:Label ID="Label3" runat="server" Text="Store Code:"></asp:Label>
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>
        </tr>   
        <tr> 
            <td style="width: 109px">
                <asp:Label ID="Label2" runat="server" Text="Traffic Date ( DD-MON-YYYY ):"></asp:Label>
                <asp:TextBox ID="txt_traffic_dt" runat="server" Width="100px" MaxLength="11" CausesValidation = "true">
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate = "txt_traffic_dt"
                    ValidationExpression="^([012]?\d|3[01])-([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Ll]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$"
                    runat="server" ErrorMessage="Invalid Date format. Valid Date Format DD-MON-YYYY">
                </asp:RegularExpressionValidator>
            </td>
          
        </tr>
    </table>

    <br />

     <asp:DataGrid ID="Gridview1" runat="server" Width="60%" BorderColor="Black" CellPadding="3"
        DataKeyField="start_hr" Height="16px" PageSize="10" AlternatingItemStyle-BackColor="Beige" AutoGenerateColumns="false" ReadOnly ="false">
        <Columns>
          <asp:BoundColumn DataField="start_hr" HeaderText="Start Hour" ReadOnly="True"></asp:BoundColumn> 
          <asp:TemplateColumn HeaderText="SLSP COUNT" >
          <ItemTemplate>
           <asp:TextBox EnableViewState="true" id="slsp_cnt" runat="server" DataField="slsp_cnt" Text='<%# DataBinder.Eval(Container, "DataItem.slsp_cnt")%>'></asp:TextBox>
           <asp:RangeValidator ID="RangeValidator1" Type="Integer" MinimumValue="0" MaximumValue="9999" ControlToValidate="slsp_cnt" runat="server" ErrorMessage="Enter only numbers between 1 and 9999" ForeColor="Red"></asp:RangeValidator>
           </ItemTemplate>
           </asp:TemplateColumn>       
        </Columns>
      
    </asp:DataGrid>

    <table border="0" cellpadding="0" cellspacing="0">
     <tr>
         <td>
           <asp:Button ID="btn_lookup" runat="server" Text="Add Sales People">
           </asp:Button>
         </td>
         <td>
           <asp:Button ID="btn_update" runat="server" Text="Update">
           </asp:Button>
         </td>
         <td>
           <asp:Button ID="btn_clear" runat="server" Text="Clear Screen"></asp:Button>
         </td>
     </tr>
     </table>

    <asp:Label ID="lbl_info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
