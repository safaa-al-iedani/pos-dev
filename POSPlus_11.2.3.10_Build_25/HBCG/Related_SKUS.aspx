<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="Related_SKUS.aspx.vb"
    Inherits="Related_SKUS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Related SKUS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
body {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
}
td {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
	color: #000000;
}
input {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
}
select {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
	color: #000000;
}
</style>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/icons/Symbol-Information.png" style="width: 128px; height: 128px" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td valign="middle" align="center">
                        <asp:Label ID="lbl_Desc" runat="server" Style="position: relative" Text="" Width="304px"></asp:Label>
                        <br />
                        <br />
                        <asp:DataGrid ID="GridView1" runat="server" Height="155px" Style="position: relative;
                            left: 0px; top: 0px;" Width="479px" AutoGenerateColumns="False"
                            DataKeyField="REL_SKU" ShowHeader="False" BorderColor="White" GridLines="None">
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <font face="Webdings" size="4">a</font>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="SelectThis" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="REL_SKU" HeaderText="Related SKU" ReadOnly="True">
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="VSN" HeaderText="VSN" ReadOnly="True">
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="DES" HeaderText="DESC" ReadOnly="True">
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="RET_PRC" HeaderText="Price" ReadOnly="True" DataFormatString="{0:C}">
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="ITM_TP_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="VE_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="MNR_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="CAT_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="SIZ" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="FINISH" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="COVER" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="GRADE" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="STYLE_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="TREATABLE" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="POINT_SIZE" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="REPL_CST" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="COMM_CD" visible="false" ReadOnly="True"/>
                                <asp:BoundColumn DataField="SPIFF" visible="false" ReadOnly="True"/>
                            </Columns>
                            <ItemStyle VerticalAlign="Top" />
                        </asp:DataGrid>
                        <br />
                        <asp:Button ID="Confirm" runat="server" Text="Add to Order" />
                        <asp:Button ID="NoThanks" Text="<%$ Resources:LibResources, Label350 %>" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
