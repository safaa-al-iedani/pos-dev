Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic

Partial Class cust_find
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btn_search_again.UniqueID
        If Request("referrer") & "" <> "" Then
            cmd_new_cust.Visible = False
        End If
        If Not IsPostBack Then
            GridView1_Binddata()
        End If

    End Sub

    Private Sub GridView1_Binddata()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet

        GridView1.DataSource = ""
        CatchMe = False
        Session("cust_zip") = Session("cust_zip_entered")   'marim - Oct 4,2016 - backbutton to retrieve all search result
        GridView1.Visible = True
        If CatchMe = False Then
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            'Daniela restrict number of records
            'sql = "SELECT FNAME, LNAME, CUST_CD, HOME_PHONE, BUS_PHONE, ADDR1, CITY, ST_CD, ZIP_CD, NVL(CORP_NAME,LNAME ||', ' || FNAME) AS CORP_NAME, ST_CD FROM CUST WHERE "
            sql = "SELECT FNAME, LNAME, CUST_CD, HOME_PHONE, BUS_PHONE, ADDR1, CITY, ST_CD, ZIP_CD, NVL(CORP_NAME,LNAME ||', ' || FNAME) AS CORP_NAME, ST_CD FROM CUST WHERE "

            If Session("home_phone") & "" <> "" Then
                sql = sql & "HOME_PHONE LIKE :HOME_PHONE "
                proceed = True
            End If
            If Session("bus_phone") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "BUS_PHONE LIKE :BUS_PHONE "
                proceed = True
            End If
            If Session("addr") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "ADDR1 LIKE :ADDR1 "
                proceed = True
            End If
            If Session("custcd") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "CUST_CD LIKE :CUST_CD "
                proceed = True
            End If
            If Session("cust_tp") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "CUST_TP_CD LIKE :CUST_TP "
                proceed = True
            End If
            If Session("corp_name") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "CORP_NAME LIKE :CORP_NAME "
                proceed = True
            End If
            If Session("fname") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "FNAME LIKE :FNAME "
                proceed = True
            End If
            If Session("lname") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "LNAME LIKE :LNAME "
                proceed = True
            End If
            If Session("cust_city") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "CITY LIKE :CITY "
                proceed = True
            End If
            If Session("cust_state") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "ST_CD LIKE :ST_CD "
                proceed = True
            End If
            If Session("cust_zip") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & "ZIP_CD LIKE :ZIP_CD "
                proceed = True
            End If
            If proceed = True Then
                sql = sql & "AND "
                sql = sql & "CUST_TP_CD NOT IN ('F','V') "
                proceed = True
            Else
                sql = sql & "CUST_TP_CD NOT IN ('F','V') "
                proceed = True
            End If
            'Daniela restrict number of records
            If Right(sql, 6) = "WHERE " Then
                sql = Replace(sql, "WHERE", "WHERE rownum <= 1000")
            Else : sql = sql & " AND rownum <= 1000 "
            End If
            sql = sql & " ORDER BY LNAME, FNAME, ADDR1"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            If Session("home_phone") & "" <> "" Then
                objSql.Parameters.Add(":HOME_PHONE", OracleType.VarChar)
                objSql.Parameters(":HOME_PHONE").Value = Session("home_phone")
            End If
            If Session("bus_phone") & "" <> "" Then
                objSql.Parameters.Add(":BUS_PHONE", OracleType.VarChar)
                objSql.Parameters(":BUS_PHONE").Value = Session("bus_phone")
            End If
            If Session("addr") & "" <> "" Then
                objSql.Parameters.Add(":ADDR1", OracleType.VarChar)
                objSql.Parameters(":ADDR1").Value = UCase(Session("addr"))
            End If
            If Session("custcd") & "" <> "" Then
                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Session("custcd"))
            End If
            If Session("cust_tp") & "" <> "" Then
                objSql.Parameters.Add(":CUST_TP", OracleType.VarChar)
                objSql.Parameters(":CUST_TP").Value = Session("cust_tp")
            End If
            If Session("corp_name") & "" <> "" Then
                objSql.Parameters.Add(":CORP_NAME", OracleType.VarChar)
                objSql.Parameters(":CORP_NAME").Value = UCase(Session("corp_name"))
            End If
            If Session("fname") & "" <> "" Then
                objSql.Parameters.Add(":FNAME", OracleType.VarChar)
                objSql.Parameters(":FNAME").Value = UCase(Session("fname"))
            End If
            If Session("lname") & "" <> "" Then
                objSql.Parameters.Add(":LNAME", OracleType.VarChar)
                objSql.Parameters(":LNAME").Value = UCase(Session("lname"))
            End If
            If Session("cust_city") & "" <> "" Then
                objSql.Parameters.Add(":CITY", OracleType.VarChar)
                objSql.Parameters(":CITY").Value = UCase(Session("cust_city"))
            End If
            If Session("cust_state") & "" <> "" Then
                objSql.Parameters.Add(":ST_CD", OracleType.VarChar)
                objSql.Parameters(":ST_CD").Value = Session("cust_state")
            End If
            If Session("cust_zip") & "" <> "" Then
                objSql.Parameters.Add(":ZIP_CD", OracleType.VarChar)
                objSql.Parameters(":ZIP_CD").Value = UCase(Session("cust_zip"))
            End If

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            dv = ds.Tables(0).DefaultView

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    GridView1.DataSource = dv
                    GridView1.DataBind()
                Else
                    GridView1.Visible = False
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        ' Daniela french
        'lbl_pageinfo.Text = "Page " + CStr(GridView1.PageIndex + 1) + " of " + CStr(GridView1.PageCount)
        lbl_pageinfo.Text = "Page " + CStr(GridView1.PageIndex + 1) + " " + Resources.LibResources.Label686 + " " + CStr(GridView1.PageCount)
        ' make all the buttons visible if page count is more than 0
        If GridView1.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If GridView1.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView1.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView1.PageIndex = (GridView1.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        'Daniela warning
        If GridView1.PageCount >= 100 Then
            Label1.Text = "More than 1000 customers found.  Please, <a href=""Main_Customer.aspx"">search again</a> and use additional search criteria.<br>"
            Label1.Visible = True
        End If
        If GridView1.PageCount = 0 Then
            If Request("LEAD") = "TRUE" Then
                Label1.Text = "Sorry, No customer exists with the supplied information. Click on Add New Customer Button or <a href=""Customer.aspx?LEAD=TRUE"">Search Again</a>"
            Else
                If Request("referrer") & "" <> "" Then
                    Label1.Text = "Sorry, no customer exists with the supplied information.  Please try <a href=""Customer.aspx?referrer=" & Request("referrer") & """>again</a>"
                Else
                    Label1.Text = "Sorry, No customer exists with the supplied information. Click on Add New Customer Button or <a href=""Customer.aspx"">Search Again</a>"
                End If
            End If
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView1.PageIndex < (GridView1.PageCount - 1) Then
                    GridView1.PageIndex += 1
                End If
            Case "Prev"
                If GridView1.PageIndex > 0 Then
                    GridView1.PageIndex -= 1
                End If
            Case "Last"
                GridView1.PageIndex = GridView1.PageCount - 1
            Case Else
                GridView1.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    Protected Sub cmd_add_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmd_add_new.Click

        If Request("LEAD") = "TRUE" Then
            Response.Redirect("cust_edit.aspx?ADD_NEW=TRUE&LEAD=TRUE&custid")
        Else
            Response.Redirect("cust_edit.aspx?ADD_NEW=TRUE&custid")
        End If

    End Sub

    'Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

    '    Dim row As GridViewRow = GridView1.SelectedRow

    '    ' Display the company name from the selected row.
    '    Session("CUST_CD") = row.Cells(6).Text
    '    Session("CUST_LNAME") = row.Cells(1).Text
    '    If Request("LEAD") = "TRUE" Then
    '        Response.Redirect("cust_edit.aspx?Custid=" + Session("CUST_CD") + "&LEAD=TRUE")
    '    Else
    '        If Request("referrer") & "" <> "" Then
    '            Session("cust_cd") = System.DBNull.Value
    '            Session("cust_lname") = System.DBNull.Value
    '            Dim Query_string As String
    '            Query_string = Request.QueryString.ToString()
    '            Response.Redirect(Request("referrer") & "?Cust_cd=" + row.Cells(6).Text & "&" & Query_string)
    '        Else
    '            Response.Redirect("cust_edit.aspx?Custid=" + Session("CUST_CD"))
    '        End If
    '    End If

    'End Sub

    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView1.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If Not IsNothing(e.GetValue("CUST_CD").ToString()) Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)
            ' Gift card added Daniela
            If Request("GIFT") = "TRUE" Then
                ASPxMenu2.RootItem.Items(0).NavigateUrl = "WGC_PurchaseGiftCard.aspx?GIFT=TRUE&Custid=" + e.GetValue("CUST_CD").ToString
                Exit Sub
            End If
            If Not IsNothing(ASPxMenu2) Then
                If Request("LEAD") = "TRUE" Then
                    ASPxMenu2.RootItem.Items(0).NavigateUrl = "cust_edit.aspx?LEAD=TRUE&Custid=" + e.GetValue("CUST_CD").ToString
                Else
                    If Request("referrer") & "" <> "" Then
                        Session("cust_cd") = System.DBNull.Value
                        Session("cust_lname") = System.DBNull.Value
                        Dim Query_string As String
                        Query_string = Request.QueryString.ToString()
                        ASPxMenu2.RootItem.Items(0).NavigateUrl = Request("referrer") & "?Cust_cd=" + e.GetValue("CUST_CD").ToString & "&" & Query_string
                    Else
                        ASPxMenu2.RootItem.Items(0).NavigateUrl = "cust_edit.aspx?Custid=" + e.GetValue("CUST_CD").ToString
                    End If
                End If
            End If
        End If

    End Sub

    Protected Sub cmd_new_cust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmd_new_cust.Click

        If Request("LEAD") = "TRUE" Then
            Response.Redirect("cust_edit.aspx?ADD_NEW=TRUE&LEAD=TRUE")
        Else
            Response.Redirect("cust_edit.aspx?ADD_NEW=TRUE")
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "Regular.Master"
        ElseIf Request("referrer") & "" <> "" Then
            Page.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub cbo_records_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_records.SelectedIndexChanged

        GridView1.PageIndex = 0
        GridView1.SettingsPager.PageSize = cbo_records.SelectedValue
        GridView1_Binddata()

    End Sub

    Protected Sub btn_search_again_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If Request("LEAD") = "TRUE" Then
            Response.Redirect("Customer.aspx?LEAD=TRUE")
        Else
            If Request("referrer") & "" <> "" Then
                Response.Redirect("Customer.aspx?" & Request.QueryString.ToString())
            Else
                Response.Redirect("Customer.aspx")
            End If
        End If

    End Sub
End Class
