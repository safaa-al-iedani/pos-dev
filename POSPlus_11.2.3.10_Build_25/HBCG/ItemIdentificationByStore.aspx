<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" 
    CodeFile="ItemIdentificationByStore.aspx.vb" Inherits="ItemIdentificationByStore" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div id="Div1" runat="server">   
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Item Identification By Store" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 
     <table>
         <tr>
             <td>                 <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
                    Text="" Width="100%"> 
                 </dx:ASPxLabel>
            </td>
         </tr>
     </table>
    <br />
     <%-- ============================================   --%> 
    <asp:Panel ID="pnl_search" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" visible="true">
      <table>
          <tr>
                <td> 
                     <dx:ASPxLabel ID="ASPxLabel9" Text=""  width="180px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                </td>
                 <td style="text-align:left">
                     <dx:ASPxLabel ID="ASPxLabel8" Text="Company Code" Width ="100" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                     </dx:ASPxLabel>
                 </td>
                 <td colspan ="4">
                    <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="410px" EnableViewState="true" ID="srch_company_cd" AutoPostBack="true">                   
                    </asp:dropdownlist> 
                 </td>
            </tr>

           <tr>
                <td> 
                     <dx:ASPxLabel ID="ASPxLabel25" Text=""  width="180px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                </td>
                 <td> 
                     <dx:ASPxLabel ID="ASPxLabel2" Text="Store Code"  width="120px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                 </td>
                 <td colspan ="4"> 
                    <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="410px" EnableViewState="true" ID="srch_str_cd">                   
                    </asp:dropdownlist> 
                 </td>
           </tr>

          <tr>
            <td> 
            </td>
            <td> 
                <dx:ASPxLabel ID="ASPxLabel1" Text="Item Code" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> 
            </td>
             <td> 
                 <asp:TextBox ID="srch_item_code" width="140px" runat="server" MaxLength="9" AutoPostBack="true"></asp:TextBox> 
             </td>
            <td>

            </td> 
           <td>
               <dx:ASPxLabel ID="ASPxLabel4" Text="Item Description" Font-Bold="true" runat="server" > </dx:ASPxLabel> 
           </td> 
            <td> <dx:ASPxLabel ID="srch_item_desc" Width="180px" runat="server" OnTextChanged="UserChanged" MaxLength="30" >
                 </dx:ASPxLabel> 
            </td> 
        </tr>        
        <tr>
            <td> 
            </td>
            <td> 
                <dx:ASPxLabel ID="ASPxLabel3" Text="Identification Code" Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
            <td> 
                <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="145px" EnableViewState="true" ID="srch_ident_cd" AutoPostBack="true">                   
                </asp:dropdownlist> 
            </td> 
            <td> 
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" Text="Ident. Description" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> 
            </td>

            <td>
                <dx:ASPxLabel ID="srch_ident_desc" Width="180px" runat="server" OnTextChanged="UserChanged" MaxLength ="40" ></dx:ASPxLabel> 
            </td> 
      </tr>
          
        <tr>
            <td> 
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel6" Text="Effective Date" Font-Bold="true" VerticalAlign="Top" runat="server" >
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="srch_purch_dt_from" Enabled="true" Width="90px" runat="server" MaxLength="11" AutoPostBack="true"></asp:TextBox>
                <asp:Button ID="dt1x_btn" runat="server" Text="Clear" OnClick="srch_dt1x_btn" />
            </td>
            <td> 
            <dx:ASPxLabel ID="ASPxLabel15" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> 
        </td>
            <%-- Entered dt to  --%>
                <td style="text-align:left">
                    <dx:ASPxLabel ID="ASPxLabel7" Text="Ending Date" Width ="80" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <asp:TextBox ID="srch_purch_dt_to" Enabled="true" Width="90px" runat="server" MaxLength="11" AutoPostBack="true"></asp:TextBox>
                    <asp:Button ID="dt2x_btn" runat="server" Text="Clear" OnClick="srch_dt2x_btn" />
                </td>
        </tr>
        <tr>
            <td> 
            </td>
            <td style="width: 60px;">

            </td>     
         
            <td style="width: 30px;">
                <asp:Calendar Visible="true" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_from" 
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt1_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
            </td>   
    
            <td>

            </td> 
            <td>

            </td>   
           <td>
              <asp:Calendar visible="true" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_to"
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt2_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
           </td> 
        </tr>
        <tr>
            <td>

            </td>     
         
            <td>

            </td>   
    
            <td>

            </td> 

           <td>

           </td> 
        </tr>        <%-- SEARCH BUTTON --%>
        <tr>
            <td colspan ="2">

            </td>
            <td colspan ="3">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="Search">
                </dx:ASPxButton>
            </td>     
            <td >
                <dx:ASPxButton ID="btn_new" runat="server" Font-Bold="true" Text="New">
                </dx:ASPxButton>
            </td>
      </tr>
       </table>
       </asp:Panel>

    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    <%-- -------------------------------------------------- EDIT grid-------------------------------------------- --%>
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    
     <%-- ===========================================================================   --%>  
    <%-- ===== --%>
    <asp:Panel ID="pnl_grid" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
   
    <%-- gridview --%>
        <dx:ASPxGridView ID="GV_ident_codes" runat="server" AutoGenerateColumns="False" KeyFieldName="ST_CD;II_CD" EnableRowsCache="true"
        OnRowUpdating="GV_row_update" OnRowInserting="GV_row_insert" OnDataBinding="GV_DataBind"
        Settings-ShowFooter="true" Width="950px" Enabled="true" SettingsBehavior-ProcessSelectionChangedOnServer="true">

        <SettingsEditing mode ="PopupEditForm" />
        <Settings ShowFooter="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsPager Position="Bottom">
            <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
        </SettingsPager>
          
       <%-- ---------------- COLUMNS ------------------ --%>
        <Columns>

        <dx:GridViewDataComboBoxColumn Caption="II Code" FieldName ="II_CD" VisibleIndex="1">
               <PropertiesComboBox Width="90" DropDownStyle="DropDown" MaxLength="46">
                   <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right">
                    </ValidationSettings>
               </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

        <dx:GridViewDataTextColumn Caption="II Description" FieldName="DESCRIPTION" VisibleIndex="2">
             <PropertiesTextEdit Width="500"  MaxLength="60">
                <ValidationSettings>
                <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                </ValidationSettings>
             </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>

            <dx:GridViewDataComboBoxColumn Caption="Store Code" FieldName ="ST_CD" VisibleIndex="3">
               <PropertiesComboBox Width="360" DropDownStyle="DropDown" MaxLength="46">
                   <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right">
                    </ValidationSettings>
               </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

          <dx:GridViewDataTextColumn Caption="Company Code" FieldName="CO_CD" VisibleIndex="4">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="30">
                </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Item Code" FieldName="ITM_CD" VisibleIndex="5">
                <PropertiesTextEdit Width="90"  MaxLength="9">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Value" />
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="Item Description" FieldName="DES" VisibleIndex="6">
             <PropertiesTextEdit Width="500"  MaxLength="60">
                <ValidationSettings>
                <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                </ValidationSettings>
             </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>

            <dx:GridViewDatadateColumn Caption="Effective Date" FieldName="EFF_DT" VisibleIndex="7">
                  <PropertiesdateEdit DisplayFormatString="MM/dd/yyyy" ReadOnlyStyle-BackColor="Snow">
                  </PropertiesdateEdit> 
             </dx:GridViewDatadateColumn>
              <dx:GridViewDatadateColumn Caption="Ending Date" FieldName="END_DT" VisibleIndex="8">
                  <PropertiesdateEdit DisplayFormatString="MM/dd/yyyy" ReadOnlyStyle-BackColor="Snow">
                  </PropertiesdateEdit> 
             </dx:GridViewDatadateColumn>
            <dx:GridViewDataTextColumn Caption="Select" Settings-AllowSort="False"
                HeaderStyle-Cursor="default" Width="15%" VisibleIndex="9">
                <CellStyle HorizontalAlign="Center"></CellStyle>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnSelect" runat="server" Text="Select" CommandName="Select" HorizontalAlign ="Left"
                        CommandArgument="<%# Container.ItemIndex %>">
                    </dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>

         </Columns>
        
        <SettingsBehavior AllowSort="False"/>
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager" PageSize="10"/>
           
        <SettingsBehavior ConfirmDelete="true"   />
        <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />
    
    </dx:ASPxGridView>
   
    </asp:Panel>

    <asp:Panel ID="pnl_update" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" Visible ="false" >
      <table>
        <tr>
            <td> 
                <dx:ASPxLabel ID="ASPxLabel24" Text=""  width="100px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
           </td>
            <td> 
                <dx:ASPxLabel ID="ASPxLabel16" Text="Identification Code" Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
            <td colspan="3"> 
                <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="220px" EnableViewState="true" ID="upd_ident_cd" AutoPostBack ="true">                   
                </asp:dropdownlist> 

                <asp:HiddenField runat="server" ID="hid_ident_cd" />
            </td>
        </tr>
        <tr>
            <td> 
                    <dx:ASPxLabel ID="ASPxLabel20" Text=""  width="30px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
            <td  style="text-align:left"> 
                <dx:ASPxLabel ID="lbl_ident_cd" Text="Ident. Description" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> 
            </td>
            <td>
                <dx:ASPxLabel ID="upd_ident_desc"  Width ="140" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
            </td>
            <td> 
            </td> 
            <td style="text-align:left">
                    <dx:ASPxLabel ID="lbl_company_cd" Text="Company Code" Width ="100" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                    </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="upd_company_cd"  Width =" 140" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
            </td>
          </tr>
          <tr>
                <td> 
                     <dx:ASPxLabel ID="ASPxLabel10" Text=""  width="90px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                </td>
                 <td> 
                     <dx:ASPxLabel ID="ASPxLabel11" Text="Store Code"  width="120px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
                 </td>
                 <td colspan ="4"> 
                    <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="420px" EnableViewState="true" ID="upd_str_cd" >                   
                    </asp:dropdownlist> 
                     <asp:HiddenField runat="server" ID="hid_str_cd" />
                 </td>
            </tr>
            <tr>
                <td> 
                </td>
                <td> 
                    <dx:ASPxLabel ID="ASPxLabel21" Text="Item Code" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> 
                </td>
                 <td> 
                     <asp:TextBox ID="upd_item_cd" width="140px" runat="server" MaxLength="9" AutoPostBack="true"></asp:TextBox> 
                     <asp:HiddenField runat="server" ID="hid_item_cd" />
                 </td>
                <td> 
                </td>
                <td style="text-align:left"> 
                   <dx:ASPxLabel ID="lbl_item_desc" Text="Item Desc." Font-Bold="true" runat="server" > </dx:ASPxLabel> 
               </td>
                <td>
                    <dx:ASPxLabel ID="upd_item_desc"  Width ="240" runat="server" meta:resourcekey="Label1">
                    </dx:ASPxLabel>
                </td>
            </tr>
          <tr>
            <td> 
            </td>
           <td>
                <dx:ASPxLabel ID="ASPxLabel17" Text="Effective Date" Font-Bold="true" VerticalAlign="Top" runat="server" >
                </dx:ASPxLabel>
           </td>
            <td>
              <asp:TextBox ID="upd_dt_from" Enabled="true" Width="80px" runat="server" MaxLength="11"  AutoPostBack="true"></asp:TextBox>
               <asp:Button ID="Button1" runat="server" Text="Clear" OnClick="upd_dt1x_btn" />
            </td>
            <td> 
            </td>
            <%-- Entered dt to  --%>
             <td style="text-align:left">
                 <dx:ASPxLabel ID="ASPxLabel18" Text="Ending Date" Width ="80" Font-Bold="true" VerticalAlign="Top" runat="server" meta:resourcekey="Label1">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <asp:TextBox ID="upd_dt_to" Enabled="true" Width="80px" runat="server" MaxLength="11"  AutoPostBack="true"></asp:TextBox>
                 <asp:Button ID="Button2" runat="server" Text="Clear" OnClick="upd_dt2x_btn" />
             </td>
        </tr>

        <tr>
            <td> 
            </td>
            <td style="width: 60px;">

            </td>     
         
            <td style="width: 30px;">
                <asp:Calendar Visible="true" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_from2" 
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="upd_dt1_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
            </td>   
            <td> 
                    <dx:ASPxLabel ID="ASPxLabel23" Text=""  width="30px"  Font-Bold="true" runat="server" > </dx:ASPxLabel> 
            </td>
    
            <td>

            </td> 

           <td>
              <asp:Calendar visible="true" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_to2"
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="upd_dt2_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
           </td> 
        </tr>
        <tr>
            <td>

            </td>     
         
            <td>

            </td>   
    
            <td>

            </td> 

           <td>

           </td> 
        </tr>        <%-- SEARCH BUTTON --%>
        <tr>
            <td colspan ="2">

            </td>
            <td>
                <dx:ASPxButton ID="btn_update" runat="server" Font-Bold="true" Text="Update">
                </dx:ASPxButton>
            </td>     
             <td colspan ="2">
                <dx:ASPxButton ID="btn_delete" runat="server" Font-Bold="true" Text="Delete">
                    <ClientSideEvents Click="function(s, e) {
                          e.processOnServer = confirm('Do you wish to delete this record?');          
                     }"/>                 
                </dx:ASPxButton>
            </td>     
           <td style align="left" >
                <dx:ASPxButton ID="btn_cancel" runat="server" Font-Bold="true" Text="Cancel">
                </dx:ASPxButton>
            </td>     
     </tr>
       </table>
       </asp:Panel>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
    <br />
   </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
