<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WGC_PurchaseGiftCard.aspx.vb" Inherits="WGC_PurchaseGiftCard"
    MasterPageFile="~/LeonsCustom.master" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">
        function SelectAndClosePopup1() {
            //ctl00_ASPxRoundPanel4_ContentPlaceHolder1_ASPxPopupControl1.Hide();
           }         
    </script>
      <script language="javascript">
          function disableButton(id) {
              // Daniela B 20140722 Disable the 2 buttons to prevent double click
              try {
                  var a = document.getElementById(id);
                  a.style.display = 'none';
              
              } catch (err) {
                  alert('Error in disableButton ' + err.message);
                  return false;
              }
              return true;
          }
    </script>

    <div width="100%" align="center">
        <br />
        &nbsp;<br />
        <b>
            <asp:Label ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label440 %>">
            </asp:Label>
        </b>
        <table width="80%">
            <tr>
                <td align="left">
                    <asp:Label ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label28 %>">
                    </asp:Label>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_amount" TabIndex="1" runat="server" AutoPostBack="True" Width="200px" CssClass="style5"></asp:TextBox>
                </td>
            </tr>
                 <tr>
                <td align="left">
                    <asp:Label ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                    </asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_cust_cd" TabIndex="2" runat="server" AutoPostBack="True" Enabled="true" Width="200px"></asp:TextBox>
                </td>
                    <%-- sabrina R1922 add hyperlink--%>
                     <td align="left">
                   <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Customer.aspx"  Text="<%$ Resources:LibResources, Label88 %>"> </asp:HyperLink>
                     <%-- sabrina R1922 comment out  
                     <td valign="top">
                                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                                        Width="19px" ToolTip="Customer Lookup" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          --%>
                    </td>
            </tr>
       
            <tr>
                <td align="left">
                     <asp:Label ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label292 %>">
                    </asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="DropDownMop" runat="server" Style="position: relative" AppendDataBoundItems="true" Width="200px">
                     </asp:DropDownList>
                </td>
                <td align="left">
                <asp:Button ID="btnProcessPaymnt" runat="server" Text="<%$ Resources:LibResources, Label439 %>" Width="150px">
                </asp:Button>
                </td>
            </tr>
                <tr>
                <td align="left">
                    <asp:Label ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label221 %>">
                    </asp:Label>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_gift_card" runat="server" AutoPostBack="False" Enabled="false" Width="200px"></asp:TextBox>
                </td>
                <td align="left">
                <asp:Button ID="btnGiftCard" runat="server" Text="<%$ Resources:LibResources, Label583 %>" Width="150px">
                </asp:Button>
            </tr>
        
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lbl_warnings" runat="server" Width="100%">
                    </asp:Label>
                    <br />
                </td>
            </tr>
          

        </table>
        <table width="50%" id="tbl_aruti" runat="server">
          
          
      
          
        </table>
        <table width="60%">
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label440 %>">
                                </asp:Button>
                                <asp:Button ID="btnReload" runat="server" Text="<%$ Resources:LibResources, Label457 %>">
                                </asp:Button>
                            </td>
                            <td>
                                <asp:Button ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lbl_response" runat="server" Width="100%">
                    </asp:Label>
                </td>
            </tr>
        </table>
        <br />
    </div>

       <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
        OnCancelCommand="DoItemCancel" OnEditCommand="DoItemEdit" OnUpdateCommand="DoItemUpdate"
        OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False" CellPadding="2" DataKeyField="row_id"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="mop_cd" HeaderText="SKU" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="des" HeaderText="Payment Type" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" HeaderText="Amount" DataFormatString="{0:0.00}">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="row_id" HeaderText="ID" Visible="False"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Electronic?">
                <ItemTemplate>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:EditCommandColumn CancelText="&lt;img src='images/icons/Undo24.gif' border='0'&gt;"
                UpdateText="&lt;img src='images/icons/Save24.gif' border='0'&gt;" EditText="&lt;img src='images/icons/Edit24.gif' border='0'&gt;"
                HeaderText="Edit">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:EditCommandColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="Delete">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
            <asp:BoundColumn DataField="mop_tp" HeaderText="TP" Visible="False"></asp:BoundColumn>
        </columns>
        <itemstyle verticalalign="Top" />
    </asp:DataGrid>&nbsp;
 
       <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="SWIPE/INSERT CARD IN POS TERMINAL" Height="300px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <br />
   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
