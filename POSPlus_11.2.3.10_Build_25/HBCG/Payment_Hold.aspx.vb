Imports System.Data.OracleClient
Imports System.Xml
Imports AppUtils
Imports SD_Utils
Imports HBCG_Utils
Imports NCT_Utils

Partial Class Payment_Hold
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Dim ds As New DataSet
    Dim partial_amt As Double = 0
    Private Const cResponseCriteria As String = "/HandleCode/ReasonCode/CodeDesc"
    Private Const cReturnCriteria As String = "/HandleCode/ReturnCode/CodeDesc"
    Private Const cAVSCriteria As String = "/HandleCode/AVSResponseCode/CodeDesc"

    Public Sub bindgrid(ByVal del_doc_num As String, Optional ByVal row_id As String = "")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim exp_dt As Date

        btn_Clear.Enabled = True

        ds2 = New DataSet

        If row_id & "" = "" Then
            If cbo_payment_type.SelectedValue & "" <> "" Then
                row_id = cbo_payment_type.SelectedValue
            End If
        End If

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT ROW_ID, DEL_DOC_NUM, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP "
        sql = sql & ",FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD, CHK_NUM, CHK_TP, TRANS_ROUTE, "
        sql = sql & "CHK_ACCOUNT_NO, TRN_TP_CD, PB_RESPONSE_MSG, HOST_RESPONSE_MSG, HOST_RESPONSE_CD, DL_STATE, DL_LICENSE_NO FROM PAYMENT_HOLDING "
        If Not String.IsNullOrEmpty(del_doc_num) Then
            sql = sql & "WHERE DEL_DOC_NUM='" & del_doc_num & "' "
        End If
        If Not String.IsNullOrEmpty(row_id) Then
            If InStr(sql, "WHERE") > 0 Then
                sql = sql & "AND "
            Else
                sql = sql & "WHERE "
            End If
            sql = sql & "ROW_ID=" & row_id
        End If
        sql = sql & " ORDER BY ROW_ID DESC"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        cc.Visible = False
        ck.Visible = False
        fin.Visible = False

        If numrows < 1 Then
            cc.Visible = False
            ck.Visible = False
            fin.Visible = False
            cbo_payment_type.Enabled = False
            Label3.Text = "No records were found"
        Else
            cbo_payment_type.Enabled = True
        End If

        Dim hold_stop As Boolean = False
        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                lbl_del_doc_num.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                lbl_mop_cd.Text = MyDataReader.Item("MOP_CD").ToString
                lbl_amt.Text = MyDataReader.Item("AMT").ToString
                hold_stop = False
                If Not String.IsNullOrEmpty(row_id) Then
                    If row_id.ToString <> MyDataReader.Item("ROW_ID").ToString Then
                        hold_stop = True
                    End If
                End If
                If hold_stop = False Then
                    lbl_host_resp_cd.Text = MyDataReader.Item("HOST_RESPONSE_CD").ToString
                    lbl_host_resp_msg.Text = MyDataReader.Item("HOST_RESPONSE_MSG").ToString
                    lbl_pb_resp.Text = MyDataReader.Item("PB_RESPONSE_MSG").ToString
                    lbl_row_id.Text = MyDataReader.Item("ROW_ID").ToString
                    If MyDataReader.Item("MOP_TP").ToString = "BC" Then
                        cc.Visible = True
                        TextBox1.Text = Decrypt(MyDataReader.Item("BC").ToString, "CrOcOdIlE")
                        lbl_tran_id.Text = ""
                        If InStr(TextBox1.Text, "^") > 0 Then
                            lbl_tran_id.Text = MyDataReader.Item("BC").ToString
                            Dim sCC As Array
                            sCC = Split(TextBox1.Text.ToString, "^")
                            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
                            TextBox3.Text = Left(sCC(2).Trim, 2)
                            TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
                            lbl_Full_Name.Text = sCC(1).Trim
                            TextBox1.Enabled = False
                            TextBox2.Enabled = False
                            TextBox3.Enabled = False
                        Else
                            If IsNumeric(TextBox1.Text) Then
                                lbl_tran_id.Text = Encrypt(TextBox1.Text, "CrOcOdIlE")
                            End If
                            If IsDate(MyDataReader.Item("EXP_DT").ToString) Then
                                exp_dt = MyDataReader.Item("EXP_DT").ToString
                                TextBox3.Text = Right(exp_dt.Year.ToString, 2)
                                TextBox2.Text = exp_dt.Month.ToString
                            End If
                        End If

                        Dim myVerify As New VerifyCC
                        Dim myTypeValid As New VerifyCC.TypeValid
                        myTypeValid = myVerify.GetCardInfo(TextBox1.Text)

                        If TextBox1.Text & "" <> "" Then
                            TextBox1.Text = StringUtils.MaskString(TextBox1.Text, "x", 16, 4) & "'"
                        End If

                        lbl_card_type.Text = myTypeValid.CCType
                        If String.IsNullOrEmpty(myTypeValid.CCType) Then
                            btn_save.Enabled = False
                            lbl_Full_Name.Text = " <font color=""RED"">** Invalid Credit Card Entry **</font> "
                        End If
                        If lbl_host_resp_cd.Text = "PARTIAL AUTHORIZATION" Then
                            btn_save.Text = "Void Transaction"
                            txt_auth.Enabled = False
                            TextBox1.Enabled = False
                            TextBox2.Enabled = False
                            TextBox3.Enabled = False
                            txt_security_code.Enabled = False
                            btn_Clear.Enabled = False
                        Else
                            btn_save.Text = "Submit"
                            txt_auth.Enabled = True
                            TextBox1.Enabled = True
                            TextBox2.Enabled = True
                            TextBox3.Enabled = True
                            txt_security_code.Enabled = True
                        End If
                    ElseIf MyDataReader.Item("MOP_TP") = "CK" Then
                        ck.Visible = True
                        txt_check.Text = MyDataReader.Item("CHK_NUM").ToString
                        TextBox4.Text = MyDataReader.Item("TRANS_ROUTE").ToString
                        TextBox5.Text = MyDataReader.Item("CHK_ACCOUNT_NO").ToString
                        If MyDataReader.Item("CHK_TP").ToString & "" <> "" Then
                            DropDownList2.SelectedValue = MyDataReader.Item("CHK_TP").ToString
                        End If
                        txt_dl_state.Text = MyDataReader.Item("DL_STATE").ToString
                        txt_dl_number.Text = MyDataReader.Item("DL_LICENSE_NO").ToString
                    ElseIf MyDataReader.Item("MOP_TP") = "FI" Then
                        fin.Visible = True
                        PaymentUtils.PopulateFinanceProviders(cbo_store_cd.Value, cbo_finance_company)
                        cbo_finance_company.SelectedValue = MyDataReader.Item("FIN_CO").ToString
                        GetPromoCodes()
                        cbo_promo.SelectedValue = MyDataReader.Item("FIN_PROMO_CD").ToString
                        txt_fi_acct.Text = Decrypt(MyDataReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                    End If
                End If
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    Protected Sub txt_fi_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If InStr(txt_fi_acct.Text, "^") > 0 Then
            Dim sCC As Array
            sCC = Split(txt_fi_acct.Text.ToString, "^")
            txt_fi_acct.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
            'TextBox3.Text = Left(sCC(2).Trim, 2)
            'TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
            'txt_swiped.Text = "Y"
        Else
            TextBox2.Focus()
            'txt_swiped.Text = "N"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            store_cd_populate()
            cbo_store_cd.Value = Session("home_store_cd")
            csh_dwr_populate()
            pull_payments()
            bindgrid(Request("del_doc_num"))
        End If

    End Sub

    Protected Sub cbo_store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store_cd.SelectedIndexChanged

        csh_dwr_populate()

    End Sub

    Public Sub csh_dwr_populate()

        Dim SQL As String
        Dim store_cd As String = ""

        If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objSql2 As OracleCommand
            Dim MyDataReader2 As OracleDataReader

            conn2.Open()

            SQL = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
            SQL = SQL & " AND STORE_CD='" & cbo_store_cd.Value & "' "
            SQL = SQL & " OR TERMINAL_IP = '" & Left(Request.ServerVariables("REMOTE_ADDR"), Find_First_IP(Request.ServerVariables("REMOTE_ADDR"))) & "' "
            SQL = SQL & " AND STORE_CD='" & cbo_store_cd.Value & "' "
            SQL = SQL & "GROUP BY STORE_CD, CASH_DWR_CD"
            SQL = UCase(SQL)

            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(SQL, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    store_cd = store_cd & "'" & MyDataReader2.Item("CASH_DWR_CD").ToString & "',"
                Loop
            Catch
                conn2.Close()
                Throw
            End Try
            conn2.Close()
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If

        SQL = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE store_cd='" & cbo_store_cd.Value & "' "
        If Not String.IsNullOrEmpty(Replace(store_cd, ",", "")) Then
            SQL = SQL & "AND csh_dwr_cd IN(" & Left(store_cd, Len(store_cd) - 1) & ") "
        End If
        SQL = SQL & "ORDER by csh_dwr_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With


            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_csh_drw
                .DataSource = ds
                .ValueField = "csh_dwr_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            cbo_csh_drw.SelectedIndex = 0

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub

    Public Sub pull_payments()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds2 = New DataSet

        conn.Open()

        sql = "SELECT ROW_ID, DEL_DOC_NUM || ' - ' || MOP_CD || '(' || AMT || ')' AS DOC_COUNT FROM PAYMENT_HOLDING WHERE PAYMENT_POST_DT > SYSDATE-1 "
        If Request("del_doc_num") & "" <> "" Then
            sql = sql & "AND DEL_DOC_NUM='" & Request("del_doc_num") & "' "
        End If
        sql = sql & "ORDER BY DEL_DOC_NUM "

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        If numrows < 1 Then
            cbo_payment_type.Enabled = False
            Label3.Text = "No pending payments found"
        ElseIf numrows = 1 Then
            With cbo_payment_type
                .DataSource = ds2
                .DataValueField = "ROW_ID"
                .DataTextField = "DOC_COUNT"
                .DataBind()
                .Enabled = False
            End With
        Else
            With cbo_payment_type
                .DataSource = ds2
                .DataValueField = "ROW_ID"
                .DataTextField = "DOC_COUNT"
                .DataBind()
            End With
        End If
        conn.Close()

    End Sub

    Protected Sub cbo_payment_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_payment_type.SelectedIndexChanged

        bindgrid(Request("del_doc_num"), cbo_payment_type.SelectedValue)

    End Sub

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        btn_save.Enabled = True
        lbl_tran_id.Text = ""
        If InStr(TextBox1.Text, "^") > 0 Then
            lbl_tran_id.Text = Encrypt(TextBox1.Text, "CrOcOdIlE")
            Dim sCC As Array
            sCC = Split(TextBox1.Text.ToString, "^")
            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
            TextBox3.Text = Left(sCC(2).Trim, 2)
            TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
            lbl_Full_Name.Text = sCC(1).Trim
            TextBox1.Enabled = False
            TextBox2.Enabled = False
            TextBox3.Enabled = False
        Else
            If IsNumeric(TextBox1.Text) Then
                lbl_tran_id.Text = Encrypt(TextBox1.Text, "CrOcOdIlE")
            End If
            TextBox2.Focus()
        End If

        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(TextBox1.Text)

        If TextBox1.Text & "" <> "" Then
            TextBox1.Text = StringUtils.MaskString(TextBox1.Text, "x", 16, 4) & "'"
        End If

        lbl_card_type.Text = myTypeValid.CCType
        If String.IsNullOrEmpty(myTypeValid.CCType) Or lbl_card_type.Text = "Unknown" Then
            btn_save.Enabled = False
            lbl_Full_Name.Text = " <font color=""RED"">** Invalid Credit Card Entry **</font> "
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        lbl_card_type.Text = ""
        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox1.Focus()

    End Sub

    Protected Sub btn_save_chk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_chk.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql, sql2 As String
        Dim objSql As OracleCommand

        conn.Open()

        sql = "UPDATE PAYMENT_HOLDING SET CHK_NUM=:CHK_NUM, CHK_TP=:CHK_TP, TRANS_ROUTE=:TRANS_ROUTE, CHK_ACCOUNT_NO=:CHK_ACCOUNT_NO, "
        sql = sql & "APPROVAL_CD=:APPROVAL_CD WHERE ROW_ID=:ROW_ID"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":ROW_ID", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_TP", OracleType.VarChar)
        objSql.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)

        objSql.Parameters(":ROW_ID").Value = lbl_row_id.Text
        objSql.Parameters(":CHK_NUM").Value = txt_check.Text.ToString
        objSql.Parameters(":APPROVAL_CD").Value = txt_appr.Text.ToString
        objSql.Parameters(":CHK_TP").Value = DropDownList2.SelectedValue.ToString
        objSql.Parameters(":TRANS_ROUTE").Value = TextBox4.Text.ToString
        objSql.Parameters(":CHK_ACCOUNT_NO").Value = TextBox5.Text.ToString

        objSql.ExecuteNonQuery()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyDataReader As OracleDataReader
        Dim order_tp As String = ""
        Dim order_tp_cd As String = ""
        Dim Card_Type As String = ""
        Dim exp_dt As String = ""
        Dim ref_num As String = ""
        Dim merchant_id As String = ""
        Dim Insert_Payment As Boolean = False
        Dim err_msg As String = ""
        Dim cc_resp As String = ""
        Dim co_cd As String = ""
        Dim cust_cd As String = ""
        Dim store_cd As String = ""
        Dim Auth_No As String = ""

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn2.Open()

        sql = "SELECT a.CO_CD, b.SO_STORE_CD, b.ORD_TP_CD, b.CUST_CD FROM SO b, AR_TRN a WHERE b.DEL_DOC_NUM='" & lbl_del_doc_num.Text & "' "
        sql = sql & "AND a.IVC_CD=b.DEL_DOC_NUM"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                order_tp_cd = MyDataReader.Item("ORD_TP_CD").ToString
                co_cd = MyDataReader.Item("CO_CD").ToString
                cust_cd = MyDataReader.Item("CUST_CD").ToString
            Loop
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        store_cd = cbo_store_cd.Value

        Select Case order_tp_cd
            Case "SAL"
                order_tp = "DEP"
            Case "MDB"
                order_tp = "DEP"
            Case "CRM"
                order_tp = "REF"
            Case "MCR"
                order_tp = "REF"
        End Select

        merchant_id = Lookup_Merchant("CK", store_cd, lbl_mop_cd.Text)

        cc_resp = NCT_Submit_Query(lbl_row_id.Text, merchant_id)
        If InStr(LCase(cc_resp), "approval") > 0 Then
            Auth_No = Right(cc_resp, cc_resp.IndexOf("Message = ") - 10).Trim
            Insert_Payment = True
            lbl_host_resp_cd.Text = "APPROVED"
            lbl_host_resp_msg.Text = ""
            lbl_pb_resp.Text = ""
        ElseIf InStr(LCase(cc_resp), "denied") > 0 Then
            Update_Payment_Holding(lbl_row_id.Text, "", "", "Check " & txt_check.Text.ToString & " was declined", "DEP", lbl_del_doc_num.Text)
            err_msg = "TRUE"
            Insert_Payment = False
            lbl_host_resp_cd.Text = "DECLINED"
            lbl_host_resp_msg.Text = ""
            lbl_pb_resp.Text = ""
        Else
            Update_Payment_Holding(lbl_row_id.Text, "", "", "Check " & txt_check.Text.ToString & " was not approved. " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5), "DEP", lbl_del_doc_num.Text)
            err_msg = "TRUE"
            Insert_Payment = False
            lbl_host_resp_cd.Text = Right(cc_resp, cc_resp.IndexOf("Message = ") + 5)
            lbl_host_resp_msg.Text = ""
            lbl_pb_resp.Text = ""
        End If
        If Insert_Payment = True Then
            sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, ORIGIN_CD "
            sql2 = " VALUES('" & co_cd & "','" & cust_cd & "','" & lbl_mop_cd.Text & "'"
            sql2 = sql2 & ",'" & store_cd & "','" & order_tp & "'," & lbl_amt.Text & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),'T','O',"
            sql2 = sql2 & "'" & lbl_del_doc_num.Text & "','" & store_cd & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
            If cbo_csh_drw.Value & "" <> "" Then
                sql = sql & ", CSH_DWR_CD"
                sql2 = sql2 & ", '" & cbo_csh_drw.Value & "'"
            Else
                sql = sql & ", CSH_DWR_CD"
                sql2 = sql2 & ", '01'"
            End If
            If Session("emp_cd") & "" <> "" Then
                sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                sql2 = sql2 & ", '" & Session("emp_cd") & "','" & Session("emp_cd") & "'"
            End If
            If txt_check.Text.ToString & "" <> "" Then
                sql = sql & ", CHK_NUM"
                sql2 = sql2 & ", '" & txt_check.Text.ToString & "' "
            End If
            If Auth_No & "" <> "" Then
                sql = sql & ", APP_CD"
                sql2 = sql2 & ",'" & Auth_No & "'"
            End If
            If ref_num & "" <> "" Then
                sql = sql & ", HOST_REF_NUM"
                sql2 = sql2 & ",'" & ref_num & "'"
            End If

            sql = UCase(sql)
            sql2 = UCase(sql2)
            sql = sql & ")"
            sql2 = sql2 & ")"
            sql = sql & sql2

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            objSql.ExecuteNonQuery()
            lbl_host_resp_cd.Text = "APPROVED"
            lbl_host_resp_msg.Text = ""
            lbl_pb_resp.Text = ""
            TextBox4.Text = ""
            TextBox5.Text = ""
            txt_check.Text = ""
            txt_appr.Text = ""

            sql = "DELETE FROM PAYMENT_HOLDING WHERE ROW_ID=" & lbl_row_id.Text

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()

        End If
        conn.Close()
        conn2.Close()

    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim sql2 As String
        Dim objSql As OracleCommand

        conn.Open()

        conn2.Open()

        Dim NEW_EXP_DT As String = ""

        If TextBox1.Text & "" = "" Then
            lbl_Full_Name.Text = "No Credit Card Number Found"
            Exit Sub
        End If
        If TextBox2.Text & "" = "" Or TextBox3.Text & "" = "" Then
            lbl_Full_Name.Text = "No Expiration Date Found"
            Exit Sub
        End If
        If Len(Trim(TextBox1.Text)) > 25 Then
            lbl_Full_Name.Text = "Invalid Credit Card"
            Exit Sub
        End If
        If TextBox1.Enabled = True And txt_security_code.Text & "" = "" Then
            lbl_Full_Name.Text = "You must enter the security code for non-swipe transactions"
            txt_security_code.Focus()
            Exit Sub
        ElseIf TextBox1.Enabled = True And Not IsNumeric(txt_security_code.Text) Then
            lbl_Full_Name.Text = "Security code must be numeric"
            txt_security_code.Focus()
            Exit Sub
        End If

        NEW_EXP_DT = SystemUtils.MonthLastDay(TextBox2.Text & "/1/" & TextBox3.Text)

        Dim cc_data As String = ""
        If lbl_tran_id.Text & "" <> "" Then
            cc_data = lbl_tran_id.Text
        Else
            cc_data = TextBox1.Text
        End If

        sql = "UPDATE PAYMENT_HOLDING SET BC=:BC, EXP_DT=:EXP_DT, SECURITY_CD=:SECURITY_CD, "
        sql = sql & "APPROVAL_CD=:APPROVAL_CD WHERE ROW_ID=:ROW_ID"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":ROW_ID", OracleType.VarChar)
        objSql.Parameters.Add(":BC", OracleType.VarChar)
        objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
        objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
        objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)

        objSql.Parameters(":ROW_ID").Value = lbl_row_id.Text
        objSql.Parameters(":BC").Value = cc_data
        objSql.Parameters(":EXP_DT").Value = NEW_EXP_DT
        If IsNumeric(txt_security_code.Text) Then
            objSql.Parameters(":SECURITY_CD").Value = txt_security_code.Text
        Else
            objSql.Parameters(":SECURITY_CD").Value = System.DBNull.Value
        End If
        objSql.Parameters(":APPROVAL_CD").Value = txt_auth.Text

        objSql.ExecuteNonQuery()

        Dim Auth_No As String = ""
        Dim Trans_Id As String = ""
        Dim Host_Response_Cd As String = ""
        Dim Protobase_Response_Msg As String = ""
        Dim PB_Response_Cd As String = ""
        Dim Host_Response_Msg As String = ""
        Dim bnk_crd_num As String = ""
        Dim SDC_Response As String = ""
        Dim order_tp As String = ""
        Dim order_tp_cd As String = ""
        Dim Card_Type As String = ""
        Dim exp_dt As String = ""
        Dim ref_num As String = ""
        Dim merchant_id As String = ""
        Dim Insert_Payment As Boolean = False
        Dim err_msg As String = ""
        Dim cc_resp As String = ""
        Dim MyDataReader As OracleDataReader
        Dim co_cd As String = ""
        Dim cust_cd As String = ""
        Dim store_cd As String = ""

        sql = "SELECT a.CO_CD, b.SO_STORE_CD, b.ORD_TP_CD, b.CUST_CD FROM SO b, AR_TRN a WHERE b.DEL_DOC_NUM='" & lbl_del_doc_num.Text & "' "
        sql = sql & "AND a.IVC_CD=b.DEL_DOC_NUM"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                order_tp_cd = MyDataReader.Item("ORD_TP_CD").ToString
                co_cd = MyDataReader.Item("CO_CD").ToString
                cust_cd = MyDataReader.Item("CUST_CD").ToString
            Loop
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        store_cd = cbo_store_cd.Value

        Select Case order_tp_cd
            Case "SAL"
                order_tp = "DEP"
            Case "MDB"
                order_tp = "DEP"
            Case "CRM"
                order_tp = "REF"
            Case "MCR"
                order_tp = "REF"
        End Select

        merchant_id = Lookup_Merchant("BC", store_cd, lbl_mop_cd.Text)

        If order_tp = "DEP" Then
            If lbl_host_resp_cd.Text = "PARTIAL AUTHORIZATION" Then
                'Per rules an auth type of '11' or void needs to be issued  for a CC partial auth
                SDC_Response = SD_Submit_Query_CC(lbl_row_id.Text, merchant_id, AUTH_VOID, Request("del_doc_num"))
            Else
                SDC_Response = SD_Submit_Query_CC(lbl_row_id.Text, merchant_id, AUTH_SALE, Request("del_doc_num"))
            End If
        Else
            SDC_Response = SD_Submit_Query_CC(lbl_row_id.Text, merchant_id, AUTH_REFUND, Request("del_doc_num"))
        End If

        Dim SDC_Fields As String() = Nothing
        Dim SDC_BreakOut As String() = Nothing

        If SDC_Response = AUTH_INQUIRY Then
            SDC_Response = SD_Submit_Query_CC(lbl_row_id.Text, merchant_id, AUTH_INQUIRY, Request("del_doc_num"))
        End If

        If InStr(SDC_Response, " this stream") > 0 Then
            If InStr(Decrypt(cc_data, "CrOcOdIlE"), "^") > 0 Then
                Dim sCC As Array
                sCC = Split(Decrypt(cc_data, "CrOcOdIlE"), "^")
                bnk_crd_num = Right(sCC(0).Trim, 4)
            Else
                bnk_crd_num = Decrypt(cc_data, "CrOcOdIlE")
            End If
            err_msg = "TRUE"
            Insert_Payment = False
        ElseIf InStr(SDC_Response, "Protobase is not responding") > 0 Then
            If InStr(Decrypt(cc_data, "CrOcOdIlE"), "^") > 0 Then
                Dim sCC As Array
                sCC = Split(Decrypt(cc_data, "CrOcOdIlE"), "^")
                bnk_crd_num = Right(sCC(0).Trim, 4)
            Else
                bnk_crd_num = Decrypt(cc_data, "CrOcOdIlE")
            End If
            err_msg = "TRUE"
            Insert_Payment = False
        ElseIf SDC_Response & "" = "" Then
            If InStr(Decrypt(cc_data, "CrOcOdIlE"), "^") > 0 Then
                Dim sCC As Array
                sCC = Split(Decrypt(cc_data, "CrOcOdIlE"), "^")
                bnk_crd_num = Right(sCC(0).Trim, 4)
            Else
                bnk_crd_num = Decrypt(cc_data, "CrOcOdIlE")
            End If
            err_msg = "TRUE"
            Insert_Payment = False
        Else
            Dim Host_Merchant_ID As String = ""
            Dim sep(3) As Char
            sep(0) = Chr(10)
            sep(1) = Chr(12)
            SDC_Fields = SDC_Response.Split(sep)
            Dim s As String
            Dim MsgArray As Array
            For Each s In SDC_Fields
                If InStr(s, ",") > 0 Then
                    MsgArray = Split(s, ",")
                    Select Case MsgArray(0).ToString
                        Case "0003"
                            Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "0004"
                            exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                            exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                        Case "0006"
                            Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "0007"
                            ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1000"
                            Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1003"
                            PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1004"
                            Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1005"
                            Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1008"
                            bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                            bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                        Case "1009"
                            Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1010"
                            Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    End Select
                End If
            Next s
            Select Case PB_Response_Cd
                Case "0"
                    cc_resp = "APPROVED"
                Case "0000"
                    cc_resp = "APPROVED"
                Case "60"
                    cc_resp = "DECLINED"
                Case "0060"
                    cc_resp = "DECLINED"
                Case Else
                    Host_Response_Msg = PB_Response_Cd
                    Host_Response_Cd = Protobase_Response_Msg
                    cc_resp = "ERROR"
            End Select
            If InStr(Decrypt(cc_data, "CrOcOdIlE"), "^") > 0 And bnk_crd_num & "" <> "" Then
                Dim sCC As Array
                sCC = Split(Decrypt(cc_data, "CrOcOdIlE"), "^")
                bnk_crd_num = Right(sCC(0).Trim, 4)
            End If
            If cc_resp = "APPROVED" Then
                Insert_Payment = True
                theSystemBiz.SaveAuditLogComment("PAYMENT_SUCCESS", "Sale " & lbl_del_doc_num.Text & " bankcard transaction for " & lbl_amt.Text & " succeeded for card " & Right(bnk_crd_num, 4))
                If ConfigurationManager.AppSettings("partial_pay") = "Y" Then
                    If CDbl(lbl_amt.Text) <> CDbl(partial_amt) Then
                        theSystemBiz.SaveAuditLogComment("PAYMENT_PARTIAL", "Sale " & lbl_del_doc_num.Text & " bankcard transaction for " & lbl_amt.Text & " approved for " & partial_amt & " for card " & Right(bnk_crd_num, 4))
                        Update_Payment_Holding(lbl_row_id.Text, "PARTIAL AUTHORIZATION", "PARTIAL AUTHORIZATION", "EXPECTED:" & lbl_amt.Text & ", ACTUAL:" & partial_amt, "DEP", lbl_del_doc_num.Text)
                        err_msg = "TRUE"
                        sql = "UPDATE PAYMENT_HOLDING SET AMT=:AMT WHERE ROW_ID=:ROW_ID"

                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                        objSql.Parameters.Add(":AMT", OracleType.Number)
                        objSql.Parameters.Add(":ROW_ID", OracleType.Number)

                        objSql.Parameters(":AMT").Value = partial_amt
                        objSql.Parameters(":ROW_ID").Value = lbl_row_id.Text

                        objSql.ExecuteNonQuery()
                    End If
                End If
            Else
                Update_Payment_Holding(lbl_row_id.Text, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", lbl_del_doc_num.Text)
                Insert_Payment = False
                theSystemBiz.SaveAuditLogComment("PAYMENT_FAILURE", "Sale " & lbl_del_doc_num.Text & " bankcard transaction for " & lbl_amt.Text & " failed for card " & Right(bnk_crd_num, 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                err_msg = "TRUE"
            End If
            If Insert_Payment = True Then
                If lbl_host_resp_cd.Text = "PARTIAL AUTHORIZATION" Then
                    'Void the payment from AR_TRN
                    sql = "SELECT SELECT AR_TRN_PK FROM AR_TRN WHERE IVC_CD='" & lbl_del_doc_num.Text & "' AND TRN_TP_CD='DEP' AND POST_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') AND MOP_CD='" & lbl_mop_cd.Text & "' "
                    sql = sql & "AND AMT=" & partial_amt

                    'Set SQL OBJECT 
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn2)

                    Try
                        'Execute DataReader 
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        'Store Values in String Variables 
                        If MyDataReader.Read() Then
                            sql = "UPDATE AR_TRN SET AMT=0, DES='VOIDED PARTIAL' WHERE IVC_CD='" & lbl_del_doc_num.Text & "' AND TRN_TP_CD='DEP' AND POST_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') AND MOP_CD='" & lbl_mop_cd.Text & "' "
                            sql = sql & "AND AMT=" & partial_amt
                            MyDataReader.Close()
                            objSql.Parameters.Clear()
                            objSql.Dispose()

                            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                            objSql.ExecuteNonQuery()
                        End If
                    Catch
                        conn.Close()
                        conn2.Close()
                        Throw
                    End Try
                Else
                    sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, ORIGIN_CD "
                    sql2 = " VALUES('" & co_cd & "','" & cust_cd & "','" & lbl_mop_cd.Text & "'"
                    sql2 = sql2 & ",'" & store_cd & "','" & order_tp & "'," & lbl_amt.Text & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),'T','O',"
                    sql2 = sql2 & "'" & lbl_del_doc_num.Text & "','" & store_cd & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
                    If cbo_csh_drw.Value & "" <> "" Then
                        sql = sql & ", CSH_DWR_CD"
                        sql2 = sql2 & ", '" & cbo_csh_drw.Value & "'"
                    Else
                        sql = sql & ", CSH_DWR_CD"
                        sql2 = sql2 & ", '01'"
                    End If
                    If Session("emp_cd") & "" <> "" Then
                        sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                        sql2 = sql2 & ", '" & Session("emp_cd") & "','" & Session("emp_cd") & "'"
                    End If
                    If cc_data & "" <> "" Then
                        If IsNumeric(bnk_crd_num) Then
                            sql = sql & ", BNK_CRD_NUM"
                            sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                        Else
                            If InStr(Decrypt(cc_data, "CrOcOdIlE"), "^") > 0 Then
                                Dim sCC As Array
                                sCC = Split(Decrypt(cc_data, "CrOcOdIlE"), "^")
                                bnk_crd_num = Right(sCC(0).Trim, 4)
                            Else
                                bnk_crd_num = Decrypt(cc_data, "CrOcOdIlE")
                            End If
                            sql = sql & ", BNK_CRD_NUM"
                            sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                        End If
                    End If
                    If Card_Type & "" <> "" Then
                        sql = sql & ", REF_NUM"
                        sql2 = sql2 & ", '" & Card_Type & "' "
                    End If
                    If exp_dt & "" <> "" Then
                        sql = sql & ", EXP_DT"
                        sql2 = sql2 & ", TO_DATE('" & exp_dt & "','mm/dd/RRRR')"
                    End If
                    If Auth_No & "" <> "" Then
                        sql = sql & ", APP_CD"
                        sql2 = sql2 & ",'" & Auth_No & "'"
                    End If
                    If Host_Merchant_ID & "" <> "" Then
                        sql = sql & ", DES"
                        sql2 = sql2 & ", 'MID:" & Host_Merchant_ID & "' "
                    End If
                    If ref_num & "" <> "" Then
                        sql = sql & ", HOST_REF_NUM"
                        sql2 = sql2 & ",'" & ref_num & "'"
                    End If
                    sql = UCase(sql)
                    sql2 = UCase(sql2)
                    If Trans_Id & "" <> "" Then
                        sql = sql & ", ACCT_NUM"
                        sql2 = sql2 & ",'" & Trans_Id & "'"
                    End If

                    sql = sql & ")"
                    sql2 = sql2 & ")"
                    sql = sql & sql2

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objSql.ExecuteNonQuery()
                End If
                lbl_host_resp_cd.Text = "APPROVED"
                lbl_host_resp_msg.Text = ""
                lbl_pb_resp.Text = ""
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
                txt_security_code.Text = ""
                txt_auth.Text = ""

                sql = "DELETE FROM PAYMENT_HOLDING WHERE ROW_ID=" & lbl_row_id.Text

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.ExecuteNonQuery()

            End If
        End If

        conn.Close()
        conn2.Close()

    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Protected Sub btn_save_fi_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fi_co.Click

        Dim MyDataReader As OracleDataReader
        'Dim MyDataReader2 As OracleDataReader
        Dim co_cd As String = ""
        Dim cust_cd As String = ""
        Dim store_cd As String = ""
        Dim so_seq_num As String = ""
        Dim so_store_cd As String = ""
        Dim so_wr_dt As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim sql2 As String
        Dim objSql As OracleCommand
        'Dim objSql2 As OracleCommand
        Dim order_tp_cd As String = ""
        Dim msg As String = "A problem was encountered while processing your payment(s).  Please review your payment information and re-submit below."

        Label4.Text = msg
        conn.Open()
        Dim dbCmd As OracleCommand

        Try
            conn2.Open()
            dbCmd = DisposablesManager.BuildOracleCommand(conn2)
            dbCmd.Transaction = conn2.BeginTransaction()
            Try
                Dim soRowid = SalesUtils.LockSoRec(lbl_del_doc_num.Text, dbCmd)

            Catch ex As Exception
                Throw ex
            End Try

            sql = "SELECT a.CO_CD, b.SO_STORE_CD, b.ORD_TP_CD, b.CUST_CD, b.so_seq_num, b.so_wr_dt FROM SO b, store a WHERE b.DEL_DOC_NUM='" & lbl_del_doc_num.Text & "' "
            sql = sql & "AND a.store_cd = b.so_store_cd AND rownum < 2 "

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                Do While MyDataReader.Read()
                    order_tp_cd = MyDataReader.Item("ORD_TP_CD").ToString
                    co_cd = MyDataReader.Item("CO_CD").ToString
                    cust_cd = MyDataReader.Item("CUST_CD").ToString
                    so_seq_num = MyDataReader.Item("so_seq_num").ToString
                    so_store_cd = MyDataReader.Item("so_store_cd").ToString
                    so_wr_dt = MyDataReader.Item("so_wr_dt").ToString
                Loop
            Catch
                conn.Close()
                conn2.Close()
                Throw
            End Try

            store_cd = cbo_store_cd.Value

            sql = "UPDATE PAYMENT_HOLDING SET FIN_CO=:FIN_CO, FI_ACCT_NUM=:FI_ACCT_NUM, FIN_PROMO_CD=:FIN_PROMO_CD, APPROVAL_CD=:APPROVAL_CD WHERE ROW_ID=:ROW_ID"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":ROW_ID", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)

            objSql.Parameters(":ROW_ID").Value = lbl_row_id.Text
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            objSql.Parameters(":FI_ACCT_NUM").Value = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
            objSql.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text

            objSql.ExecuteNonQuery()

            Dim Auth_No As String = ""
            Dim Trans_Id As String = ""
            Dim Host_Response_Cd As String = ""
            Dim Protobase_Response_Msg As String = ""
            Dim PB_Response_Cd As String = ""
            Dim Host_Response_Msg As String = ""
            Dim Host_Merchant_ID As String = ""
            Dim bnk_crd_num As String = ""
            Dim SDC_Response As String = ""
            Dim order_tp As String = "DEP"
            Dim Card_Type As String = ""
            Dim exp_dt As String = ""
            Dim ref_num As String = ""
            Dim merchant_id As String = ""
            Dim Insert_Payment As Boolean = True
            Dim err_msg As String = ""
            Dim Transport As String = ""
            Dim cc_resp As String = ""
            Dim plan_desc As String = ""
            Dim rate_info As String = ""
            Dim intro_rate As String = ""
            Dim prepaid_balance As String = ""
            Dim lngSEQNUM As Integer, lngComments As Integer, strSO_SEQ_NUM As String
            Dim strComments As String, strMidCmnts As String
            Dim fin_co As String = ""

            'Check to see if there is a merchant key for the store that the sale was written under
            merchant_id = Lookup_Merchant("FI", store_cd, lbl_mop_cd.Text)

            Insert_Payment = False
            Transport = Lookup_Transport("FI", store_cd, lbl_mop_cd.Text)
            If Transport = "GE_182" Or Transport = "WF_148" Then
                ' 
                'Process Finance Authorization through Protobase Instance
                '
                If order_tp = "DEP" Then
                    If InStr(DP_String_Creation, lbl_mop_cd.Text) > 0 Then
                        SDC_Response = SD_Submit_Query_PL(lbl_row_id.Text, merchant_id, AUTH_SALE, lbl_del_doc_num.Text)
                    Else
                        SDC_Response = SD_Submit_Query_PL(lbl_row_id.Text, merchant_id, AUTH_ONLY, lbl_del_doc_num.Text)
                    End If
                Else
                    SDC_Response = SD_Submit_Query_PL(lbl_row_id.Text, merchant_id, AUTH_REFUND, lbl_del_doc_num.Text)
                End If

                Dim SDC_Fields As String() = Nothing


                If SDC_Response = "22" Then
                    SDC_Response = SD_Submit_Query_PL(lbl_row_id.Text, merchant_id, AUTH_INQUIRY, Request("del_doc_num"))
                End If

                If InStr(SDC_Response, " this stream") > 0 Then
                    Update_Payment_Holding(lbl_row_id.Text, "", "", "Protobase server not responding", "DEP", lbl_del_doc_num.Text)
                    err_msg = "TRUE"
                    Insert_Payment = False
                ElseIf InStr(SDC_Response, "Protobase is not responding") > 0 Then
                    Update_Payment_Holding(lbl_row_id.Text, "", "", "Protobase server not responding", "DEP", lbl_del_doc_num.Text)
                    err_msg = "TRUE"
                    Insert_Payment = False
                ElseIf SDC_Response & "" = "" Then
                    Update_Payment_Holding(lbl_row_id.Text, "", "", "Protobase server not responding", "DEP", lbl_del_doc_num.Text)
                    err_msg = "TRUE"
                    Insert_Payment = False
                Else
                    Dim sep(3) As Char
                    sep(0) = Chr(10)
                    sep(1) = Chr(12)
                    SDC_Fields = SDC_Response.Split(sep)
                    Dim s As String
                    Dim MsgArray As Array
                    For Each s In SDC_Fields
                        If InStr(s, ",") > 0 Then
                            MsgArray = Split(s, ",")
                            Select Case MsgArray(0)
                                Case "0003"
                                    Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "0004"
                                    exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                Case "0006"
                                    Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "0007"
                                    ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    'Case "1000"
                                    '    Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1003"
                                    PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1004"
                                    Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    'Case "1005"
                                    '    Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1008"
                                    bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                Case "1009"
                                    Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1010"
                                    Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "3319"
                                    plan_desc = ""
                                    For x = LBound(MsgArray) To UBound(MsgArray)
                                        If x > 0 Then
                                            plan_desc = plan_desc & MsgArray(x) & ","
                                        End If
                                    Next
                                    plan_desc = Left(plan_desc, Len(plan_desc) - 1)
                                Case "3322"
                                    rate_info = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "3325"
                                    intro_rate = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                            End Select
                        End If
                    Next s
                    Select Case PB_Response_Cd
                        Case "0"
                            cc_resp = "APPROVED"
                        Case "0000"
                            cc_resp = "APPROVED"
                        Case "60"
                            cc_resp = "DECLINED"
                        Case "0060"
                            cc_resp = "DECLINED"
                        Case Else
                            Host_Response_Msg = PB_Response_Cd
                            Host_Response_Cd = Protobase_Response_Msg
                            cc_resp = "ERROR"
                    End Select
                    If cc_resp = "APPROVED" Then
                        Insert_Payment = True
                    Else
                        Update_Payment_Holding(lbl_row_id.Text, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", lbl_del_doc_num.Text)
                        Insert_Payment = False
                        err_msg = "TRUE"
                    End If
                End If
            ElseIf Transport = "ADS" Then
                ' 
                'Process Finance Authorization through Alliance Data Systems
                '
                Dim requestDS As New RequestAllianceDataSet()
                Dim responseDS As ResponseAllianceDataSet
                requestDS.Request.Rows.Add(New Object() {"LEVINS", DateTime.Now, merchant_id, txt_fi_acct.Text, lbl_amt.Text, "N"})
                requestDS.AcceptChanges()
                Dim fiService As New FITransactionService()
                responseDS = fiService.InvokeFITransaction(requestDS)
                Dim reasonDesc As String = ""
                Dim returnDesc As String = ""
                Dim avsDesc As String = ""

                If (Not responseDS Is Nothing) And (responseDS.Response.Rows.Count) > 0 Then
                    If Not IsNothing(responseDS.Response(0).reasonCode) Then
                        reasonDesc = GetDesc(responseDS.Response(0).reasonCode.ToString, cResponseCriteria)
                    End If
                    If Not IsNothing(responseDS.Response(0).returnCode) Then
                        returnDesc = GetDesc(responseDS.Response(0).returnCode.ToString, cReturnCriteria)
                    End If
                    If Not IsNothing(responseDS.Response(0).avsResponse) Then
                        avsDesc = GetDesc(responseDS.Response(0).avsResponse.ToString, cAVSCriteria)
                    End If
                    If Not IsNothing(responseDS.Response(0).authCode) Then
                        Auth_No = responseDS.Response(0).authCode.ToString
                    End If
                    If LCase(returnDesc) <> "approved" Then
                        err_msg = "TRUE"
                        Insert_Payment = False
                    Else
                        Insert_Payment = True
                    End If
                Else
                    err_msg = "TRUE"
                    Insert_Payment = False
                End If
            End If
            If Not String.IsNullOrEmpty(txt_fi_acct.Text) Then
                bnk_crd_num = "FI"
            End If

            If Insert_Payment = True Then
                Insert_Payment = False
                fin_co = cbo_finance_company.SelectedValue
                Dim rate_info_array As Array = Nothing
                Dim intro_rate_array As Array = Nothing
                Dim during_promo_apr As String = String.Empty
                Dim during_promo_apr_flag As String = String.Empty
                Dim after_promo_apr As String = String.Empty
                Dim after_promo_apr_flag As String = String.Empty
                Dim promo_duration As String = String.Empty
                Dim aspPromoCd As String = String.Empty


                If plan_desc.isNotEmpty Then
                    'this is the promo/during promo info
                    If InStr(intro_rate, ";") > 0 Then
                        intro_rate_array = Split(intro_rate, ";")
                        If (Not intro_rate Is Nothing) Then
                            during_promo_apr_flag = intro_rate_array(0)
                            during_promo_apr = intro_rate_array(1)
                            promo_duration = intro_rate_array(4)
                        End If
                    End If

                    'this is the account/after promo info 
                    If InStr(rate_info, ";") > 0 Then
                        rate_info_array = Split(rate_info, ";")
                        If (Not rate_info_array Is Nothing) Then
                            after_promo_apr_flag = rate_info_array(4)
                            after_promo_apr = rate_info_array(6)
                        End If
                    End If

                    sql = "INSERT INTO SETTLEMENT_TERMS (DEL_DOC_NUM, ORD_TP_CD, RATE_DEL_RATE_TYPE, RATE_DEL_APR, INTRO_RATE, INTRO_APR, INTRO_DURATION, PLAN_DESC) "
                    sql = sql & "VALUES (:DEL_DOC_NUM, :ORD_TP_CD, :RATE_DEL_RATE_TYPE, :RATE_DEL_APR, :INTRO_RATE, :INTRO_APR, :INTRO_DURATION, :PLAN_DESC) "
                    'objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

                    dbCmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                    dbCmd.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
                    dbCmd.Parameters.Add(":RATE_DEL_RATE_TYPE", OracleType.VarChar)
                    dbCmd.Parameters.Add(":RATE_DEL_APR", OracleType.VarChar)
                    dbCmd.Parameters.Add(":INTRO_RATE", OracleType.VarChar)
                    dbCmd.Parameters.Add(":INTRO_APR", OracleType.VarChar)
                    dbCmd.Parameters.Add(":INTRO_DURATION", OracleType.VarChar)
                    dbCmd.Parameters.Add(":PLAN_DESC", OracleType.VarChar)

                    dbCmd.Parameters(":DEL_DOC_NUM").Value = lbl_del_doc_num.Text
                    dbCmd.Parameters(":ORD_TP_CD").Value = lbl_mop_cd.Text
                    dbCmd.Parameters(":RATE_DEL_RATE_TYPE").Value = after_promo_apr_flag  'rate_info_array(4).ToString
                    dbCmd.Parameters(":RATE_DEL_APR").Value = after_promo_apr   'rate_info_array(6).ToString
                    dbCmd.Parameters(":INTRO_RATE").Value = during_promo_apr_flag      'intro_rate_array(0).ToString
                    dbCmd.Parameters(":INTRO_APR").Value = during_promo_apr         'intro_rate_array(1).ToString
                    dbCmd.Parameters(":INTRO_DURATION").Value = promo_duration 'intro_rate_array(4).ToString
                    dbCmd.Parameters(":PLAN_DESC").Value = plan_desc.trimString(0, 50)
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                    dbCmd.Parameters.Clear()
                    'objSql2.ExecuteNonQuery()
                Else
                    theSystemBiz.SaveAuditLogComment("FINANCE_PROMO_FAILURE", "Sales Order " & lbl_del_doc_num.Text & " did not retrieve promotional terms")
                End If

                If Is_Finance_DP(lbl_mop_cd.Text) = False Then
                    sql = "SELECT DEL_DOC_NUM FROM SO_ASP WHERE DEL_DOC_NUM = '" & lbl_del_doc_num.Text & "'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


                    '****************** Insert the credit entry details into SO_ASP table, if it does not exist *****************
                    If Not MyDataReader.Read() Then
                        MyDataReader.Close()

                        ''get  first the ASP_PROMO info like promo des, etc.
                        Dim promoDes As String = String.Empty
                        Dim promoDes3 As String = String.Empty
                        Dim ds As DataSet = GetAspPromoDetails(cbo_finance_company.SelectedValue, cbo_promo.SelectedValue)
                        For Each dr In ds.Tables(0).Rows
                            If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                            If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                            If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
                        Next

                        If IsNumeric(during_promo_apr) Then
                            during_promo_apr = during_promo_apr / 10000 & "% "
                        End If
                        If IsNumeric(after_promo_apr) Then
                            after_promo_apr = after_promo_apr / 10000 & "% "
                        End If
                        sql = "INSERT INTO SO_ASP (" &
                                    "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                                    "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                    "ACCT_APR, ACCT_APR_FLAG)" &
                                " VALUES('" & lbl_del_doc_num.Text & "', 'Y', '" & cbo_promo.SelectedValue & "','" & aspPromoCd & "','" & promoDes & "','" & plan_desc & "','" &
                                                       promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                                       after_promo_apr & "','" & after_promo_apr_flag & "')"
                        sql = UCase(sql)
                        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        'objSql.ExecuteNonQuery()
                        dbCmd.CommandText = sql
                        dbCmd.ExecuteNonQuery()
                    End If

                    If String.IsNullOrEmpty(Auth_No) Then
                        If Not String.IsNullOrEmpty(txt_fi_appr.Text) Then Auth_No = txt_fi_appr.Text
                    End If
                    sql = "UPDATE SO SET APPROVAL_CD='" & Auth_No & "' WHERE DEL_DOC_NUM='" & lbl_del_doc_num.Text & "'"
                    sql = UCase(sql)
                    'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()

                ElseIf Is_Finance_DP(lbl_mop_cd.Text) = True Then
                    Insert_Payment = True
                    lngSEQNUM = 1

                    Dim x As Integer

                    x = 1
                    Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
                    cmntKey.wrDt = FormatDateTime(so_wr_dt, DateFormat.ShortDate)
                    cmntKey.storeCd = so_store_cd
                    cmntKey.seqNum = so_seq_num
                    lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey)
                    strSO_SEQ_NUM = so_seq_num
                    'sql = "Select Max(SO_CMNT.SEQ#) from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & lbl_del_doc_num.Text & "'"
                    'sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
                    'sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM"

                    'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    'MyDataReader = objSql.ExecuteReader()
                    'If MyDataReader.Read() Then
                    '    If MyDataReader.IsDBNull(0) = False Then
                    '        lngSEQNUM = MyDataReader(0)
                    '        lngSEQNUM = lngSEQNUM + 1
                    '    Else
                    '        lngSEQNUM = 1
                    '    End If
                    'Else
                    '    lngSEQNUM = 1
                    'End If

                    strMidCmnts = "Promotion-Code Assigned to WDR Deposit = " & cbo_promo.SelectedValue
                    'If Len(lbl_del_doc_num.Text) = 12 Then
                    '    strSO_SEQ_NUM = Mid(lbl_del_doc_num.Text, 8, 4)
                    'Else
                    '    strSO_SEQ_NUM = Right(lbl_del_doc_num.Text, 4)
                    'End If

                    sql = "insert into SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM) Values ( " & _
                                "TO_DATE('" & cmntKey.wrDt & "','mm/dd/RRRR')" & ",'" & so_store_cd & "','" & strSO_SEQ_NUM & "'," & _
                                lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & _
                                strMidCmnts & "','" & lbl_del_doc_num.Text.Trim & "')"  ' doc num stays on dp finance pmt
                    'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    'objSql.ExecuteNonQuery()
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                End If      'end if Is_Finance_DP type

                ' ***Update the AR_TRN with the ref_num used for the FIN auth request 
                sql = "UPDATE AR_TRN  SET REF_NUM = '" & ref_num & "'" & ", HOST_REF_NUM = '" & ref_num & "'" &
                            " WHERE ivc_cd = '" & lbl_del_doc_num.Text & "'"
                'objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql2.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()

                '*** Save the asp response info when an approval is recieved for a finance auth
                SaveAspResponseCodeInfo(fin_co, lbl_del_doc_num.Text)
                sql = "UPDATE SO SET ORIG_FI_AMT=" & lbl_amt.Text & ", APPROVAL_CD='" & Auth_No & "', FIN_CUST_CD='" & cbo_finance_company.SelectedValue & "' WHERE DEL_DOC_NUM='" & lbl_del_doc_num.Text & "'"
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
                sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & txt_fi_acct.Text & "', MOP_CD='FI' WHERE IVC_CD='" & lbl_del_doc_num.Text & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()

                lbl_host_resp_cd.Text = "APPROVED"
                lbl_host_resp_msg.Text = ""
                lbl_pb_resp.Text = ""
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
                txt_security_code.Text = ""
                txt_auth.Text = ""

                sql = "DELETE FROM PAYMENT_HOLDING WHERE ROW_ID=" & lbl_row_id.Text

                'Set SQL OBJECT 
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
            ElseIf Insert_Payment = False And Is_Finance_DP(lbl_mop_cd.Text) = False Then 'MyDataReader2.Item("MOP_CD").ToString <> "WDR" Then
                sql = "UPDATE SO SET ORIG_FI_AMT=NULL, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & lbl_del_doc_num.Text & "'"
                'objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql2.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
                sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE IVC_CD='" & lbl_del_doc_num.Text & "' AND TRN_TP_CD='" & Session("ord_tp_cd") & "'"
                'objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql2.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
            End If       '****end if it's a finance and insert_payment= true
            '****end if it's a finance and insert_payment= true

            If Insert_Payment = True And Is_Finance_DP(lbl_mop_cd.Text) = True Then
                'lbl_mop_cd.Text = "WDR" Then
                sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, ORIGIN_CD "
                sql2 = " VALUES('" & co_cd & "','" & cust_cd & "','" & lbl_mop_cd.Text & "'"
                sql2 = sql2 & ",'" & store_cd & "','" & order_tp & "'," & lbl_amt.Text & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),'T','O',"
                sql2 = sql2 & "'" & lbl_del_doc_num.Text & "','" & store_cd & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
                If cbo_csh_drw.Value & "" <> "" Then
                    sql = sql & ", CSH_DWR_CD"
                    sql2 = sql2 & ", '" & cbo_csh_drw.Value & "'"
                Else
                    sql = sql & ", CSH_DWR_CD"
                    sql2 = sql2 & ", '01'"
                End If
                If Session("emp_cd") & "" <> "" Then
                    sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                    sql2 = sql2 & ", '" & Session("emp_cd") & "','" & Session("emp_cd") & "'"
                End If
                If IsNumeric(txt_fi_acct.Text) Then
                    sql = sql & ", BNK_CRD_NUM"
                    sql2 = sql2 & ", '" & txt_fi_acct.Text & "'"
                End If
                If Card_Type & "" <> "" Then
                    sql = sql & ", REF_NUM"
                    sql2 = sql2 & ", '" & Card_Type & "' "
                End If
                If exp_dt & "" <> "" Then
                    sql = sql & ", EXP_DT"
                    sql2 = sql2 & ", TO_DATE('" & exp_dt & "','mm/dd/RRRR')"
                End If
                If Auth_No & "" <> "" Then
                    sql = sql & ", APP_CD"
                    sql2 = sql2 & ",'" & Auth_No & "'"
                End If
                If Host_Merchant_ID & "" <> "" Then
                    sql = sql & ", DES"
                    sql2 = sql2 & ", 'MID:" & Host_Merchant_ID & "' "
                End If
                If ref_num & "" <> "" Then
                    sql = sql & ", HOST_REF_NUM"
                    sql2 = sql2 & ",'" & ref_num & "'"
                End If
                sql = UCase(sql)
                sql2 = UCase(sql2)
                If Trans_Id & "" <> "" Then
                    sql = sql & ", ACCT_NUM"
                    sql2 = sql2 & ",'" & Trans_Id & "'"
                End If

                sql = sql & ")"
                sql2 = sql2 & ")"
                sql = sql & sql2

                'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
                lbl_host_resp_cd.Text = "APPROVED"
                lbl_host_resp_msg.Text = ""
                lbl_pb_resp.Text = ""
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
                txt_security_code.Text = ""
                txt_auth.Text = ""

                sql = "DELETE FROM PAYMENT_HOLDING WHERE ROW_ID=" & lbl_row_id.Text

                'Set SQL OBJECT 
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()

                'conn.Close()
                'conn2.Close()
            ElseIf Insert_Payment = True Then
                sql = "UPDATE SO SET ORIG_FI_AMT=" & lbl_amt.Text & ", APPROVAL_CD='" & Auth_No & "', FIN_CUST_CD='" & cbo_finance_company.SelectedValue & "' WHERE DEL_DOC_NUM='" & lbl_del_doc_num.Text & "'"
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()

                sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & txt_fi_acct.Text & "', MOP_CD='FI' WHERE IVC_CD='" & lbl_del_doc_num.Text & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
                lbl_host_resp_cd.Text = "APPROVED"
                lbl_host_resp_msg.Text = ""
                lbl_pb_resp.Text = ""
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
                txt_security_code.Text = ""
                txt_auth.Text = ""

                sql = "DELETE FROM PAYMENT_HOLDING WHERE ROW_ID=" & lbl_row_id.Text

                'Set SQL OBJECT 
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                'objSql.ExecuteNonQuery()
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
            End If

            dbCmd.Transaction.Commit()

        Catch ex As Exception
            dbCmd.Transaction.Rollback()
            If ex.Message = String.Format(Resources.POSErrors.ERR0042, lbl_del_doc_num.Text) Then

                btn_save_fi_co.Enabled = False
            End If
            Label4.Text = msg & "<BR /><font color='Red'>" & ex.Message & "</font>"
            Exit Sub

        Finally
            If Not IsNothing(dbCmd.Transaction) Then
                dbCmd.Transaction.Dispose()
            End If
            dbCmd.Dispose()
            conn2.Close()
            conn2.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

    End Sub

    Public Sub store_cd_populate()

        Dim SQL As String
        Dim store_cd As String = ""
        If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objSql2 As OracleCommand
            Dim MyDataReader2 As OracleDataReader

            conn2.Open()

            SQL = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
            SQL = SQL & " OR TERMINAL_IP = '" & Left(Request.ServerVariables("REMOTE_ADDR"), Find_First_IP(Request.ServerVariables("REMOTE_ADDR"))) & "' GROUP BY STORE_CD, CASH_DWR_CD"

            SQL = UCase(SQL)

            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(SQL, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    store_cd = store_cd & "'" & MyDataReader2.Item("STORE_CD").ToString & "',"
                Loop
            Catch
                conn2.Close()
                Throw
            End Try
            conn2.Close()
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT STORE.STORE_CD, STORE.STORE_NAME, STORE.STORE_CD || ' - ' || STORE.STORE_NAME as full_desc FROM STORE, CSH_DWR "
        If Not String.IsNullOrEmpty(Replace(store_cd, ",", "")) Then
            SQL = SQL & "WHERE STORE_CD IN(" & Left(store_cd, Len(store_cd) - 1) & ") "
        End If
        If InStr(SQL, "WHERE") > 0 Then
            SQL = SQL & "AND CSH_DWR.STORE_CD=STORE.STORE_CD "
        Else
            SQL = SQL & "WHERE CSH_DWR.STORE_CD=STORE.STORE_CD "
        End If
        SQL = SQL & "GROUP BY STORE.STORE_CD, STORE.STORE_NAME order by STORE.STORE_CD"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged

        GetPromoCodes()
    End Sub

    ''' <summary>
    ''' Retrieves and populates the promotions for a particular finance provider.
    ''' </summary>
    Private Sub GetPromoCodes()
        PaymentUtils.PopulatePromotions(lbl_mop_cd.Text, cbo_finance_company.SelectedValue, cbo_promo)
    End Sub

End Class
