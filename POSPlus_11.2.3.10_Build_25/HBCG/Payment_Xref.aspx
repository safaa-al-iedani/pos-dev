<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Payment_Xref.aspx.vb" Inherits="Payment_Xref" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
    </div>
    Filter by Store: &nbsp;<asp:DropDownList ID="cbo_filter_store" runat="server" AutoPostBack="True">
    </asp:DropDownList>&nbsp;
    <br />
    Filter by MOP Code:&nbsp;
    <asp:DropDownList ID="cbo_mop_filter" runat="server" AutoPostBack="True">
    </asp:DropDownList>
    <hr />
    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click"
        Text="Add New Payment" /><br />
    <asp:DataGrid Style="position: relative; top: 9px" ID="Gridview1" runat="server"
        Width="668px" Height="156px" Font-Underline="False" Font-Strikeout="False"
        Font-Overline="False" Font-Italic="False" Font-Bold="False" DataKeyField="STORE_CD"
        CellPadding="2" AutoGenerateColumns="False">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
            Font-Bold="True" ForeColor="Black"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="store_cd" HeaderText="Store Code" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="mop_cd" HeaderText="MOP Code" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="mop_tp" HeaderText="MOP Type" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="transport" HeaderText="Transport" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="MERCHANT_ID" HeaderText="Terminal ID">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" />
            </asp:ButtonColumn>
        </Columns>
    </asp:DataGrid>&nbsp;<br />
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" Height="218px" Width="366px"
        CloseAction="CloseButton" HeaderText="Add New Automated Payment" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                &nbsp;Add New:<br />
                <table width="100%">
                    <tr>
                        <td>
                            Store Code:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_new_store" runat="server" Width="221px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            MOP Code:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_mop_cd" runat="server" Width="221px" AutoPostBack="True" OnSelectedIndexChanged="cbo_mop_cd_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Payment Type:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_pmt_tp" runat="server" Enabled="False">
                                <asp:ListItem Text="Bank Card" Value="BC"></asp:ListItem>
                                <asp:ListItem Text="Check" Value="CK"></asp:ListItem>
                                <asp:ListItem Text="Finance" Value="FI"></asp:ListItem>
                                <asp:ListItem Text="Gift Card" Value="GC"></asp:ListItem>
                                <asp:ListItem Text="OnBase" Value="ONB"></asp:ListItem>
                                <asp:ListItem Text="Signature Capture" Value="SIG"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Processor:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_pmt_proc" runat="server" Width="219px">
                                <asp:ListItem Value="ADS">Alliance Data</asp:ListItem>
                                <asp:ListItem Value="NCT">National Check Trust</asp:ListItem>
                                <asp:ListItem Value="SDC">Elavon Credit Cards</asp:ListItem>
                                <asp:ListItem Value="GE_182">GE Money</asp:ListItem>
                                 <asp:ListItem Value="WF_148">Wells Fargo</asp:ListItem>
                                <asp:ListItem Value="WGC">World Gift Card</asp:ListItem>
                                <asp:ListItem Value="TOP">Topaz Signature Capture</asp:ListItem>
                                <asp:ListItem Value="ONB">OnBase</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Terminal ID:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_merchant_id" runat="server" Width="129px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click"
                                Text="Add" />
                            <asp:Label ID="lbl_warnings" runat="server" Width="287px"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
