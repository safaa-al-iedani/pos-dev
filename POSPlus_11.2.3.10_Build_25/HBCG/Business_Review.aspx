<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Business_Review.aspx.vb" Inherits="Business_Review" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Store Group Code:">
                </dx:ASPxLabel>
                &nbsp;
            </td>
            <td>
                <dx:ASPxComboBox ID="cbo_store_Cd" runat="server" Width="400px" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td valign="top">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Written Date Range:">
                </dx:ASPxLabel>
            </td>
            <td valign="top">
                <dx:ASPxDateEdit ID="txt_start_dt" runat="server">
                </dx:ASPxDateEdit>
            </td>
            <td valign="top">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text=" to ">
                </dx:ASPxLabel>
            </td>
            <td valign="top">
                <dx:ASPxDateEdit ID="txt_end_dt" runat="server">
                </dx:ASPxDateEdit>
            </td>
            <td valign="top">
                <dx:ASPxButton ID="btn_submit" runat="server" Text="Generate Sales Data" OnClick="btn_submit_Click">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%">
    </dx:ASPxLabel>
    <br />
    <br />
    <dx:ASPxGridView ID="GridView4" runat="server" AutoGenerateColumns="False" Width="100%">
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Store Code" FieldName="SO_STORE_CD" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="DES" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Sales Revenue" FieldName="SALES_DOL" VisibleIndex="2">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Sales Qty" FieldName="SALES_QTY" VisibleIndex="3">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Avg Sale" VisibleIndex="4">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_avg_sale" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Gross Margin" FieldName="SALES_GM" VisibleIndex="5">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Gross Margin %" VisibleIndex="6">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_SALES_GM_PCT" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <TotalSummary>
            <dx:ASPxSummaryItem ShowInColumn="Sales Revenue" ShowInGroupFooterColumn="Sales Revenue"
                SummaryType="Sum" FieldName="SALES_DOL" />
            <dx:ASPxSummaryItem FieldName="SALES_QTY" ShowInColumn="Sales Qty" ShowInGroupFooterColumn="Sales Qty"
                SummaryType="Sum" />
        </TotalSummary>
        <GroupSummary>
            <dx:ASPxSummaryItem FieldName="SALES_DOL" ShowInColumn="Sales Revenue" ShowInGroupFooterColumn="Sales Revenue"
                SummaryType="Sum" DisplayFormat="{0:c2}" />
            <dx:ASPxSummaryItem FieldName="SALES_QTY" ShowInColumn="Sales Qty" ShowInGroupFooterColumn="Sales Qty"
                SummaryType="Sum" DisplayFormat="{0:c2}" />
        </GroupSummary>
        <Settings ShowFooter="True" />
    </dx:ASPxGridView>
    <br />
    <dx:ASPxGridView ID="DataGrid1" runat="server" AutoGenerateColumns="False" Width="100%">
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="SO_EMP_SLSP_CD1" VisibleIndex="0" Caption="Salesperson">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_DOL" VisibleIndex="1" Caption="Sales Revenue">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_QTY" VisibleIndex="2" Caption="Sales Qty">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Avg Sale" VisibleIndex="3">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_avg_sale" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_GM" VisibleIndex="4" Caption="Gross Margin">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Gross Margin %" VisibleIndex="5">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_SALES_GM_PCT_EMP" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <dx:ASPxGridView ID="DataGrid2" runat="server" AutoGenerateColumns="False" Width="100%">
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="MNR_CD" VisibleIndex="0" Caption="Minor Code">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DES" VisibleIndex="1" Caption="Description">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_DOL" VisibleIndex="2" Caption="Sales Revenue">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_QTY" VisibleIndex="3" Caption="Sales Qty">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SALES_GM" VisibleIndex="4" Caption="Gross Margin">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Gross Margin %" VisibleIndex="5">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_SALES_GM_PCT_MNR" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
