<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Price_Tag.aspx.vb" Inherits="Price_Tag" Title="" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" AutoPostBack="True" width="90%" Height="50%">
        <TabPages>
            <dx:TabPage Text="Store">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width: 527px; height: 130px">
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Store Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxComboBox ID="cbo_store_store_cd" runat="server" ValueType="System.String"
                                                    AutoPostBack="True" Width="170px" IncrementalFilteringMode ="StartsWith">
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Location Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxComboBox ID="cbo_store_loc_cd" runat="server" ValueType="System.String" 
                                                    Width="170px" IncrementalFilteringMode ="StartsWith">
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Vendor Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxTextBox ID="txt_store_ve_cd" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="Item Sort Code" Width="170px">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxTextBox ID="txt_store_sort_cd_from" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="    through    ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_store_sort_cd_to" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Retail Price Changed">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_store_ret_prc_from" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="    through    ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_store_ret_prc_to" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Adv. Price Changed">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_store_adv_prc_from" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="    through    ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_store_adv_prc_to" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Effective Price Date">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_store_eff_prc" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 50px;">
                                <td align="center">
                                    <dx:ASPxButton ID="Button2" runat="server" Text="Generate Data" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                                <td align="left">
                                    <dx:ASPxButton ID="Button3" runat="server" Text="Reset Form" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Purchase Order">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width: 340px; height: 130px">
                            <tr>
                                <td style="width: 140px" align="center">
                                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Purchase Order #  ">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txt_po" runat="server" Width="170px">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 140px">
                                    <dx:ASPxButton ID="Button5" runat="server" Text="Generate Data" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                                <td align="center">
                                    <dx:ASPxButton ID="Button4" runat="server" Text="Reset Form" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Inter-store Transfer">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width: 340px; height: 130px">
                            <tr>
                                <td style="width: 140px" align="center">
                                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Inter-Store Transfer #">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txt_ist" runat="server" Width="170px">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 140px">
                                    <dx:ASPxButton ID="Button7" runat="server" Text="Generate Data" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                                <td align="center">
                                    <dx:ASPxButton ID="Button6" runat="server" Text="Reset Form" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Individual Tags">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width: 340px; height: 130px">
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Store Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxComboBox ID="cbo_ind_store_cd" runat="server" ValueType="System.String" 
                                                    AutoPostBack="True" Width="170px" IncrementalFilteringMode ="StartsWith">
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Location Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxComboBox ID="cbo_ind_loc_cd" runat="server" ValueType="System.String" 
                                                    Width="170px" IncrementalFilteringMode ="StartsWith">
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="SKU">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxTextBox ID="txt_ind_sku" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td style="width: 140px">
                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Quantity">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_ind_qty" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Combination #">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxTextBox ID="txt_ind_combo" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td style="width: 180px">
                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="# Only for Custom Order SKU's">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="Item Sort Code">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxTextBox ID="txt_ind_sort_cd_from" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text=" through ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_ind_sort_cd_to" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel23" runat="server" Text="Retail Price Changed">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_ind_ret_prc_from" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text=" through ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_ind_ret_prc_to" runat="server" Width="170px">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="Adv. Price Changed">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_ind_adv_prc_from" runat="server">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel26" runat="server" Text=" through ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_ind_adv_prc_to" runat="server">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 26px;">
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel27" runat="server" Text="Effective Price Date">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <table>
                                        <tr style="height: 26px;">
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_ind_eff_prc" runat="server">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 50px;">
                                <td align="center">
                                    <dx:ASPxButton ID="Button9" runat="server" Text="Generate Data" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                                <td align="center">
                                    <dx:ASPxButton ID="Button8" runat="server" Text="Reset Form" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Package SKUs">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Please enter your Package SKUs in the grid below.  When you are finished, Press the Generate Data button."
                            Width="496px">
                        </dx:ASPxLabel>
                        <br />
                        <table width="100%">
                            <tr>
                                <td>
                                    <dx:ASPxTextBox ID="txt_new_package" runat="server" Width="170px">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="Button11" runat="server" Text="Generate Data" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="DataGridview2" runat="server" AutoGenerateColumns="False" Width="254px">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Package SKU"></asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="Button10" runat="server" Text="Reset Form" Width="140px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
    <br />
    <dx:ASPxButton ID="Button1" runat="server" Text="View Tags" Width="158px">
    </dx:ASPxButton>
    <br />
    <dx:ASPxLabel ID="lbl_information" runat="server" Text="" Width="100%">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="lbl_tag_id" runat="server" Visible="False">
    </dx:ASPxLabel>
    <br />
    <table>
        <tr style="height: 26px;">
            <td style="width: 140px;">
                <dx:ASPxLabel ID="Label1" runat="server" Text="Individual Template:">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxComboBox ID="txt_ptag" runat="server" ValueType="System.String" Width="386px"
                    IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr style="height: 26px;">
            <td style="width: 140px;">
                <dx:ASPxLabel ID="Label2" runat="server" Text="Group Template:">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxComboBox ID="txt_gtag" runat="server" ValueType="System.String" Width="386px"
                    IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="The following will be displayed on your selected template:">
    </dx:ASPxLabel>
    <br />
    <dx:ASPxGridView ID="DataGridView1" runat="server" Width="70%" AutoGenerateColumns="False" >
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="True" UseFixedTableLayout="True">
        </Settings>
    </dx:ASPxGridView>
    <br />
    <br />
</asp:Content>
