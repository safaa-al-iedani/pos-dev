Imports System.Data.OracleClient
Imports Usercontrols_MessagePopup

Partial Class restore_saved_order
    Inherits POSBasePage

    Private theSalesBiz As New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()

    Protected Sub Gridview1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.ItemCommand

        Dim Passed_Command As String
        Passed_Command = e.CommandName.ToString
        If Passed_Command = "Update" Then
            Update_Current_Sale(e.Item.Cells(7).Text)
        End If

    End Sub

    Public Sub Update_Current_Sale(ByVal session_id As String)

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String
        Dim guid As String = ""
        Dim LEAD As String = ""
        Dim sot As New SavedOrderType
        'Open Connection 
        conn2.Open()

        sql = "SELECT * FROM TEMP_MASTER WHERE SESSION_ID='" & session_id & "'"
        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read Then
                guid = MyDataReader2.Item("SESSION_ID").ToString
                Session("PD") = MyDataReader2.Item("PD").ToString
                Session("store_cd") = MyDataReader2.Item("STORE_CD").ToString
                Session("slsp1") = MyDataReader2.Item("SLSP1").ToString
                Session("pct_1") = MyDataReader2.Item("PCT_1").ToString
                Session("slsp2") = MyDataReader2.Item("SLSP2").ToString
                Session("pct_2") = MyDataReader2.Item("PCT_2").ToString
                Session("ord_tp_cd") = MyDataReader2.Item("ORD_TP_CD").ToString
                Session("ord_srt") = MyDataReader2.Item("ORD_SRT_CD").ToString
                Session("pd_store_cd") = MyDataReader2.Item("PD_STORE_CD").ToString
                Session("CO_CD") = MyDataReader2.Item("CO_CD").ToString
                Session("cust_cd") = MyDataReader2.Item("CUST_CD").ToString
                Session("tax_rate") = MyDataReader2.Item("TAX_RATE").ToString
                Session("tax_cd") = MyDataReader2.Item("TAX_CD").ToString
                Session("no_tax") = MyDataReader2.Item("NO_TAX").ToString
                Session("tet_cd") = MyDataReader2.Item("TET_CD").ToString
                Session("del_chg") = MyDataReader2.Item("DEL_CHG").ToString
                SessVar.discCd = MyDataReader2.Item("DISC_CD").ToString
                Session("disc_tp") = MyDataReader2.Item("DISC_TP").ToString
                Session("disc_amt") = MyDataReader2.Item("DISC_AMT").ToString
                Session("ZIP_CD") = MyDataReader2.Item("ZIP_CD").ToString
                Session("zone_cd") = MyDataReader2.Item("ZONE_CD").ToString
                Session("scomments") = MyDataReader2.Item("SCOMMENTS").ToString
                Session("dcomments") = MyDataReader2.Item("DCOMMENTS").ToString
                Session("arcomments") = MyDataReader2.Item("ARCOMMENTS").ToString
                Session("del_dt") = MyDataReader2.Item("DEL_DT").ToString
                Session("setup_chg") = MyDataReader2.Item("SETUP_CHG").ToString
                sot.OrderTypeCode = MyDataReader2.Item("ORD_TP_CD").ToString
                Session("re_order_tp_cd") = MyDataReader2.Item("ORD_TP_CD").ToString
                LEAD = MyDataReader2.Item("LEAD").ToString
                ViewState("LEAD") = LEAD
                'MM-9227
                If Not IsNothing(MyDataReader2.Item("ORIG_DEL_DOC_NUM")) AndAlso Not String.IsNullOrEmpty(MyDataReader2.Item("ORIG_DEL_DOC_NUM").ToString) Then
                    sot.OrderNumber = MyDataReader2.Item("ORIG_DEL_DOC_NUM").ToString
                End If
                If Not IsNothing(MyDataReader2.Item("CAUSE_CD")) AndAlso Not String.IsNullOrEmpty(MyDataReader2.Item("CAUSE_CD").ToString) Then
                    sot.CauseCode = MyDataReader2.Item("CAUSE_CD").ToString
                End If
            End If
            MyDataReader2.Close()
        Catch
            conn2.Close()
            Throw
        End Try
        Session("SavedOrderType") = sot
        'Verify any line items and denote their presence 
        sql = "SELECT * FROM TEMP_ITM WHERE SESSION_ID='" & session_id & "'"
        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read Then
                Session("itemid") = "NONEENTERED"
            End If
            MyDataReader2.Close()
        Catch
            conn2.Close()
            Throw
        End Try

        If Session("cust_cd") & "" = "" Then
            sql = "SELECT CUST_CD FROM CUST_INFO WHERE SESSION_ID='" & session_id & "'"
            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If MyDataReader2.Read Then
                    Session("cust_cd") = MyDataReader2.Item("CUST_CD").ToString
                End If
                MyDataReader2.Close()
            Catch
                conn2.Close()
                Throw
            End Try
        End If

        'Verify any payments and denote their presence 
        sql = "SELECT SUM(AMT) As Payments FROM PAYMENT WHERE SESSIONID='" & session_id & "'"
        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("Payments").ToString) Then
                    Session("payment") = "TRUE"
                    Session("payments") = MyDataReader2.Item("Payments").ToString
                End If
            End If
            MyDataReader2.Close()
        Catch
            conn2.Close()
            Throw
        End Try

        'Update comments variable to denote comments are present
        If Session("scomments") & "" <> "" Or Session("dcomments") & "" <> "" Or Session("arcomments") & "" <> "" Then
            Session("comments") = "TRUE"
        End If

        sql = "UPDATE PAYMENT SET SESSIONID='" & Session.SessionID.ToString & "' WHERE SESSIONID='" & guid & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        sql = "UPDATE CUST_INFO SET SESSION_ID='" & Session.SessionID.ToString & "' WHERE SESSION_ID='" & guid & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        sql = "UPDATE MULTI_DISC SET SESSION_ID='" & Session.SessionID.ToString & "' WHERE SESSION_ID='" & guid & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        sql = "UPDATE TEMP_ITM SET SESSION_ID='" & Session.SessionID.ToString & "' WHERE SESSION_ID='" & guid & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        sql = "DELETE FROM TEMP_MASTER WHERE SESSION_ID='" & guid & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.ExecuteNonQuery()
        Session("MP") = System.DBNull.Value
        Session("IP") = System.DBNull.Value
        conn2.Close()

        If Session("TAX_CD") & "" <> "" Then
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            'Open Connection 
            conn2.Open()

            sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
            sql = sql & "FROM TAT, TAX_CD$TAT "
            sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
            sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            sql = sql & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
            sql = sql & "AND TAT.DEL_TAX = 'Y' "
            sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "
            'Determine the taxability of the setup and delivery charges
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If MyDataReader2.Read() Then
                    Session("DEL_TAX") = MyDataReader2.Item("SumOfPCT")
                Else
                    Session("DEL_TAX") = "N"
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
            sql = sql & "FROM TAT, TAX_CD$TAT "
            sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
            sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            sql = sql & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
            sql = sql & "AND TAT.SU_TAX = 'Y' "
            sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

            'Determine the taxability of the setup and delivery charges
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If MyDataReader2.Read() Then
                    Session("SU_TAX") = MyDataReader2.Item("SumOfPCT")
                Else
                    Session("SU_TAX") = "N"
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        End If

        HBCG_Utils.calculate_total(Page.Master)

        '*****Validation For Prevent Sale SKU's when restoring to sale ******'
        If Not (Session("itemid") Is Nothing) Then

            Dim droppedSkuResponse As DroppedSkuResponseDtc = theSkuBiz.GetDroppedItemsForSession(Session.SessionID.ToString())
            If (droppedSkuResponse.prevSaleCount >= 1) Then
                'This code is written with reference to Admin.aspx.vb as suggested by Laxmikantha.
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                Dim msg As String = If(droppedSkuResponse.prevSaleCount = 1,
                                       Resources.POSMessages.MSG0018,
                                       Resources.POSMessages.MSG0016)
                ucMsgPopup.DisplayAlertMsg(String.Format(msg, droppedSkuResponse.prevSaleSkus), "")
                Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                msgComponent.ForeColor = Color.Red
                msgPopup.ShowOnPageLoad = True
                'Reference ends here
            Else
                If LEAD = "TRUE" Then
                    Response.Redirect("startup.aspx?LEAD=TRUE", False)
                Else
                    Response.Redirect("startup.aspx?restore_saved_order=True", False)
                End If
            End If
        End If
        '*****Validation For Prevent Sale SKU's when restoring to sale ******'

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        bindgrid()

    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT a.EMP_CD, a.SAVE_DATE, a.CUST_CD, a.SESSION_ID, a.ORD_TP_CD, a.STORE_CD, "
        sql = sql & "NVL(CORP_NAME,LNAME ||', ' || FNAME) AS LNAME, b.FNAME, b.HPHONE, a.LEAD FROM TEMP_MASTER a, CUST_INFO b WHERE EMP_CD='" & Session("EMP_CD") & "' "
        sql = sql & "AND a.SESSION_ID=b.SESSION_ID(+) ORDER BY SAVE_DATE DESC"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim sessionId As String = Gridview1.DataKeys(e.Item.ItemIndex)
        theSalesBiz.DeleteTempOrder(sessionId)
        Response.Redirect("restore_saved_order.aspx")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        If e.Item.Cells(8).Text = "TRUE" Then
            e.Item.Cells(2).Text = "Prospect"
        End If

    End Sub

    Protected Sub ucMsgPopup_Choice(sender As Object, e As EventArgs, choice As Usercontrols_MessagePopup.MessageResponse) Handles ucMsgPopup.Choice
        'OK button clcik event
        If MessageResponse.YES_OK_SELECTION = choice Then
            Dim LEAD As String = If(Not ViewState("LEAD").ToString Is Nothing, ViewState("LEAD").ToString, "False")
            If LEAD = "TRUE" Then
                Response.Redirect("startup.aspx?LEAD=TRUE")
            Else
                Response.Redirect("startup.aspx")
            End If
        End If
    End Sub
End Class
