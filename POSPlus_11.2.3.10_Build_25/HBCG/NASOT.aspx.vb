Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq

Partial Class NASOT
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If IsPostBack = False Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As StringBuilder
            Dim ds As New DataSet
            Dim Mydatareader As OracleDataReader
            Dim homeStoreCode As String
            Dim companyCode As String
            Dim coGroupCode As String
            lbl_versionInfo.Text = gs_version
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            btn_search.Enabled = False
            btn_clr_screen.Enabled = False

            If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
                Return
            End If

            sqlString = New StringBuilder("SELECT e.home_store_cd, s.co_cd, cg.co_grp_cd FROM emp e, store s, co_grp cg  ")
            sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd AND e.emp_cd = UPPER(:p_emp_cd)")
            objCmd.CommandText = sqlString.ToString()
            objCmd.Connection = conn
            objCmd.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            objCmd.Parameters(":p_emp_cd").Value = Session("EMP_CD").ToString().ToUpper()
            Try
                conn.Open()
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)

                If Mydatareader.Read() Then

                    txt_company_code.Text = Mydatareader.Item("CO_CD").ToString()
                    txt_company_code.Enabled = False
                    companyCode = Mydatareader.Item("CO_CD").ToString()

                    homeStoreCode = Mydatareader.Item("HOME_STORE_CD").ToString()
                    Session("HOME_STORE_CD") = homeStoreCode

                    coGroupCode = Mydatareader.Item("CO_GRP_CD").ToString()
                    Session("CO_GRP_CD") = coGroupCode

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            If String.IsNullOrEmpty(homeStoreCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find home store code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0037
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            ElseIf String.IsNullOrEmpty(coGroupCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find company group code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0038
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            Else

                sqlString = New StringBuilder("SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s, co_grp cg  ")
                sqlString.Append("WHERE s.co_cd = cg.co_cd AND cg.co_grp_cd = :co_grp_cd ")
                If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    sqlString.Append("and s.co_cd = :co_cd ")
                End If
                sqlString.Append("UNION SELECT sg.store_grp_cd store_cd, sg.store_grp_cd || ' - ' ||  sg.des as full_desc FROM store_grp sg ")
                sqlString.Append("WHERE EXISTS (SELECT 1 FROM store_grp$store sgs, store s, co_grp cg ")
                sqlString.Append("WHERE sgs.store_grp_cd = sg.store_grp_cd AND sgs.store_cd = s.store_cd ")
                If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    sqlString.Append("and s.co_cd = :co_cd ")
                End If
                sqlString.Append("AND sgs.store_grp_cd != s.store_cd AND s.co_cd = cg.co_cd AND cg.co_grp_cd = :co_grp_cd)")

                sqlString.Append("AND NOT EXISTS (SELECT 1 FROM STORE_GRP$STORE SGS2 WHERE SGS2.STORE_GRP_CD = SG.STORE_GRP_CD ")
                sqlString.Append("AND SGS2.STORE_CD NOT IN (SELECT s2.store_cd FROM store s2, co_grp cg  WHERE s2.co_cd = cg.co_cd ")
                If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    sqlString.Append("and s2.co_cd = :co_cd ")
                End If
                sqlString.Append("AND cg.co_grp_cd = :co_grp_cd))")

                objCmd.CommandText = sqlString.ToString()
                objCmd.Connection = conn
                objCmd.Parameters.Clear()
                objCmd.Parameters.Add(":co_grp_cd", OracleType.VarChar)
                objCmd.Parameters(":co_grp_cd").Value = coGroupCode
                If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    objCmd.Parameters.Add(":co_cd", OracleType.VarChar)
                    objCmd.Parameters(":co_cd").Value = companyCode
                End If


                Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

                Try
                    dataAdapter.Fill(ds)
                Catch
                    Throw
                    conn.Close()
                End Try
                conn.Close()
                If ds.Tables(0).Rows.Count = 0 Then
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
                    lbl_warning.Text = Resources.CustomMessages.MSG0039
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                    Return
                End If
                Dim defaultStoreIndex As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows
                    cbo_store_cd.Items.Add(row.ItemArray(1).ToString(), row.ItemArray(0).ToString())
                    If row.ItemArray(0).ToString = homeStoreCode.ToString() Then
                        defaultStoreIndex = ds.Tables(0).Rows.IndexOf(row)
                    End If
                Next
                cbo_store_cd.SelectedIndex = defaultStoreIndex
                Session("defaultStoreIndex") = defaultStoreIndex
            End If
        End If
        btn_search.Enabled = True
        btn_clr_screen.Enabled = True
        GV_NASOT_SUMMARY.Enabled = False
    End Sub

    ''' <summary>
    ''' To set title for this page.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0079
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Search Button Click event 
    ''' Based on the given search criteria build the summary of items that are not scanned or located during RF cycle counts.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'Code Added by Kumaran for POS+ French Conversion on 02-Sep-2015 - Starts            


        sqlString = New StringBuilder("SELECT res.* FROM (SELECT ix.store_cd, ")
        'Landed Cost Logic Starts
        sqlString.Append("NVL((select distinct first_value(nvl(sgrc2.repl_cst, 0) * (1 + nvl(v.up_chg, 0) / 100) * (1 + nvl((select distinct first_value(f2s.frt_fac) over (order by f2s.dt desc) ")
        sqlString.Append("from frt2store f2s where f2s.store_cd=sgs2.store_cd and f2s.mnr_cd=i1.mnr_cd), 0) / 100))  over(order by sgrc2.dt desc)")
        sqlString.Append("from store_grp_repl_clndr sgrc2, store_grp$store sgs2,store_grp sg2, ve2up_chg v,itm i1 ")
        sqlString.Append("where i1.ve_cd = v.ve_cd(+) AND sgrc2.store_grp_cd = sg2.store_grp_cd AND sgs2.store_grp_cd = sg2.store_grp_cd AND sgs2.store_cd = ix.store_cd ")
        sqlString.Append("AND NVL(sg2.pricing_store_grp, 'N') = 'Y' AND sgrc2.dt <= TRUNC(SYSDATE) AND sgrc2.itm_cd = i1.itm_cd AND i1.itm_cd = ix.itm_cd),0) as Cost,")
        'Landed Cost Logic Ends
        sqlString.Append(" (trunc(sysdate) - trunc(nvl(nst.scan_missed_date, sysdate)))+1 days_in, nvl(ix.nas_out_cd, ACTIVITY) NAS, ix.rf_id_cd, ix.itm_cd, i.")
        sqlString.Append(Resources.CustomMessages.MSG0080)
        sqlString.Append(" des,")
        sqlString.Append("ix.loc_cd, ix.del_doc_num, ix.qty, ix.nas_out_id,  decode(ix.nas_out_cd, 'OOB', nas.cmnt, 'OUB', out.cmnt, null) nas_out_cmnt ,")
        sqlString.Append("TO_CHAR(last_actn_dt, 'DD-MON-YY') last_actn_dt, TO_CHAR(last_cycle_dt, 'DD-MON-YY') last_cycle_dt, ix.last_actn_emp_cd ")
        sqlString.Append("FROM nasot nst, co_grp cg,  store s,itm i, itm$itm_srt iss, inv_xref ix, nas, out   ")
        sqlString.Append("WHERE ix.itm_cd = iss.itm_cd AND ix.itm_cd = i.itm_cd AND (ix.nas_out_cd in ('OOB', 'OUB') OR ix.disposition = 'RES') AND ix.activity = 'NIL' ")
        sqlString.Append("and iss.itm_srt_cd = :p_itm_srt_cd and cg.co_grp_cd = :p_co_grp_cd AND ix.store_cd = s.store_cd AND s.co_cd = cg.co_cd  AND s.store_cd in ")
        sqlString.Append("(SELECT st.store_cd from store st  where st.store_cd = :p_selected_store UNION ")
        sqlString.Append("SELECT sgs.store_cd FROM store_grp$store sgs  WHERE sgs.store_grp_cd = :p_selected_store)  ")
        If Not (Session("HOME_STORE_CD") = "00" Or Session("HOME_STORE_CD") = "10") Then
            sqlString.Append("and s.co_cd = :co_cd ")
        End If
        sqlString.Append("AND ix.rf_id_cd = nst.rf_id_cd(+) AND ix.nas_out_id = nas.id_cd(+) and ix.nas_out_id = out.id_cd(+) ) res  ")
        If cbo_display_days.SelectedItem.Text = "1-7 Days" Then
            sqlString.Append("WHERE days_in BETWEEN 1 AND 7  ")
        ElseIf cbo_display_days.SelectedItem.Text = "8-14 Days" Then
            sqlString.Append("WHERE days_in BETWEEN 8 AND 14  ")
        ElseIf cbo_display_days.SelectedItem.Text = ">14 Days" Then
            sqlString.Append("WHERE days_in > 14  ")
        End If

        Dim ds As New DataSet
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":p_co_grp_cd", OracleType.VarChar)
        objcmd.Parameters(":p_co_grp_cd").Value = Session("CO_GRP_CD").ToString()
        objcmd.Parameters.Add(":p_itm_srt_cd", OracleType.VarChar)
        If Session("CO_GRP_CD").ToString().Equals("LEO") Then
            objcmd.Parameters(":p_itm_srt_cd").Value = "LFL"
        Else
            objcmd.Parameters(":p_itm_srt_cd").Value = "BW1"
        End If

        objcmd.Parameters.Add(":p_selected_store", OracleType.VarChar)
        objcmd.Parameters(":p_selected_store").Value = cbo_store_cd.SelectedItem.Value.ToString()

        If Not (Session("HOME_STORE_CD") = "00" Or Session("HOME_STORE_CD") = "10") Then
            objcmd.Parameters.Add(":co_cd", OracleType.VarChar)
            objcmd.Parameters(":co_cd").Value = txt_company_code.Text
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)

        conn.Open()
        objAdaptor.Fill(ds)
        conn.Close()

        Session("GV_NASOT") = ds.Tables(0)

        GV_NASOT_SUMMARY.DataBind()
        GV_NASOT_SUMMARY.DetailRows.CollapseAllRows()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To Bind the Master Grid from Session whenver Grid events are triggers and requires databind.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_NASOT_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Dim dt As DataTable = Session("GV_NASOT")
        If IsNothing(dt) Then
            GV_NASOT_SUMMARY.DataSource = Nothing
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'lbl_warning.Text = No records found for the given criteria
            lbl_warning.Text = Resources.CustomMessages.MSG0040
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            GV_NASOT_SUMMARY.Enabled = False
        Else
            Dim summary = From dr In dt
                          Order By dr.Item("STORE_CD")
                          Group By STORE_CD = dr.Item("STORE_CD")
                          Into ITEMS = Sum(Convert.ToDecimal(dr.Item("QTY"))), COST = Sum(Convert.ToDecimal(dr.Item("COST")))
                          Order By STORE_CD
                          Select STORE_CD, ITEMS, COST, DAYS = cbo_display_days.SelectedItem.Text.ToString()

            If summary.ToList.Count = 0 Then
                Session("GV_NASOT") = Nothing
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = No records found for the given criteria
                lbl_warning.Text = Resources.CustomMessages.MSG0040
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                GV_NASOT_SUMMARY.Enabled = False
            Else
                GV_NASOT_SUMMARY.DataSource = summary.ToList()
                lbl_warning.Text = ""
                GV_NASOT_SUMMARY.Enabled = True
            End If


        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To query the detail grid based on store whenever user expands the row from master grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub PrepareDetailGrid(ByVal sender As Object, ByVal e As EventArgs)

        Dim dtNasot As DataTable = Session("GV_NASOT")

        If IsNothing(dtNasot) Then
            'do nothing
        Else
            Dim detailGrid As ASPxGridView = CType(sender, ASPxGridView)
            Dim masterKey As String = detailGrid.GetMasterRowKeyValue().ToString()
            Dim detail = From dr In dtNasot.AsEnumerable()
                         Where dr.Field(Of String)("STORE_CD") = masterKey.ToString()
                         Select dr
            Dim view As DataView = detail.AsDataView()
            detailGrid.DataSource = view.ToTable()
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To Clear Grid, search criteria and remove sessions
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clr_screen_Click(sender As Object, e As EventArgs) Handles btn_clr_screen.Click
        GV_NASOT_SUMMARY.Enabled = False
        Session("GV_NASOT") = Nothing
        GV_NASOT_SUMMARY.DataBind()
        cbo_store_cd.SelectedIndex = Convert.ToInt16((Session("defaultStoreIndex")))
        cbo_display_days.SelectedIndex = 0
        lbl_warning.Text = ""
    End Sub

    Protected Sub lbl_sum_of_item_count_load(ByVal sender As Object, ByVal e As EventArgs)
        Dim grid As ASPxGridView = GV_NASOT_SUMMARY
        Dim col As GridViewDataColumn = TryCast(grid.Columns("ITEMS"), GridViewDataColumn)
        Dim summary As ASPxSummaryItem = grid.TotalSummary("ITEMS", DevExpress.Data.SummaryItemType.Sum)
        Dim text As String = summary.GetTotalFooterDisplayText(col, grid.GetTotalSummaryValue(summary))
        Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        label.Text = String.Format("{0}" & Constants.vbCrLf & "(Sum={1})", col.Caption, text)

    End Sub

    Protected Sub lbl_sum_of_item_cost_load(ByVal sender As Object, ByVal e As EventArgs)
        Dim grid As ASPxGridView = GV_NASOT_SUMMARY
        Dim col As GridViewDataColumn = TryCast(grid.Columns("COST"), GridViewDataColumn)
        Dim summary As ASPxSummaryItem = grid.TotalSummary("COST", DevExpress.Data.SummaryItemType.Sum)
        Dim text As String = summary.GetTotalFooterDisplayText(col, grid.GetTotalSummaryValue(summary))
        Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        label.Text = String.Format("{0}" & Constants.vbCrLf & "(Sum={1:$#,##0.00})", col.Caption, text)

    End Sub

End Class