Imports System.Data
Imports System.Data.OracleClient

Partial Class discount_lookup
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        txt_Minors.Text = System.DBNull.Value.ToString
        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader


            ' Daniela add multi company logic
            'Dim emp_init As String
            'If isEmpty(Session("emp_init")) And isNotEmpty(Session("EMP_CD")) Then
            'emp_init = LeonsBiz.GetEmpInit(Session("EMP_CD"))
            'Session("emp_init") = emp_init
            'Else : emp_init = Session("emp_init")
            'End If

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            'sql = "SELECT DISC_CD, DES, TP_CD, AMT FROM DISC WHERE EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY DES"
            ' Daniela add multi company logic
            'sql = "SELECT DISC_CD, DES, TP_CD, AMT FROM DISC WHERE Std_multi_co.isValidDisc(upper('" & emp_init & "'), DISC_CD) = 'Y' and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY DES"
            ' Nov 04 improve performance
            sql = "SELECT DISC_CD, DES, TP_CD, AMT FROM DISC a " +
                    "WHERE exists ( select 1 " +
                   "from   co_grp$disc " +
                   "where  co_grp_cd  = (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & Session("CO_CD") & "'),co_cd)) " +
                   "and    disc_cd     =  a.disc_cd) " +
                   "and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY DES"


            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            disc_cd.Items.Clear()
            ' disc_cd.Items.Insert(0, "Select Discount Code")
            ' disc_cd.Items.FindByText("Select Discount Code").Value = ""
            disc_cd.Items.Insert(0, Resources.LibResources.Label530)
            disc_cd.Items.FindByText(Resources.LibResources.Label530).Value = ""
            disc_cd.SelectedIndex = 0

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read) Then
                    disc_cd.DataSource = MyDataReader
                    disc_cd.DataBind()
                Else
                    disc_cd.Visible = False
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Protected Sub disc_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc_cd.SelectedIndexChanged

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim found_item As String
        found_item = disc_cd.SelectedValue
        txt_Minors.Text = System.DBNull.Value.ToString

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        ' Daniela add multi company logic
        'Dim emp_init As String
        'If isEmpty(Session("emp_init")) Then
        'emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        'Session("emp_init") = emp_init
        'Else : emp_init = Session("emp_init")
        'End If

        'Daniela multi company logic, improve performance
        'sql = "SELECT DISC_CD, DES, TP_CD, AMT FROM DISC WHERE Std_multi_co.isValidDisc('" & emp_init & "', DISC_CD) = 'Y' and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY DES"
        sql = "SELECT DISC_CD, DES, TP_CD, AMT FROM DISC a " +
                   "WHERE exists ( select 1 " +
                  "from   co_grp$disc " +
                  "where  co_grp_cd  = (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & Session("CO_CD") & "'),co_cd)) " +
                  "and    disc_cd     =  a.disc_cd) " +
                  "and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        disc_cd.Items.Clear()
        ' disc_cd.Items.Insert(0, "Select Discount Code")
        'disc_cd.Items.FindByText("Select Discount Code").Value = ""
        disc_cd.Items.Insert(0, Resources.LibResources.Label530)
        disc_cd.Items.FindByText(Resources.LibResources.Label530).Value = ""

        disc_cd.SelectedIndex = 0

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                disc_cd.DataSource = MyDataReader
                disc_cd.DataBind()
            Else
                disc_cd.Visible = False
            End If

            MyDataReader.Close()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Dim DoMe As Boolean
            DoMe = False
            Do While MyDataReader.Read()
                Dim Test As String
                Test = MyDataReader(0).ToString().Trim()
                If Test = found_item Then
                    lblDiscDesc.Text = MyDataReader(0).ToString().Trim() & " - " & MyDataReader(1).ToString().Trim()
                    Select Case MyDataReader(2).ToString().Trim()
                        Case "P"
                            lblDisc.Text = CDbl(MyDataReader(3).ToString().Trim()) & "% Off Retail"
                        Case "C"
                            lblDisc.Text = "Cost Plus " & CDbl(MyDataReader(3).ToString().Trim()) & "%"
                        Case "D"
                            '  lblDisc.Text = "$" & FormatNumber(CDbl(MyDataReader(3).ToString().Trim()), 2) & " Off Total Sale"
                            lblDisc.Text = "$" & FormatNumber(CDbl(MyDataReader(3).ToString().Trim()), 2) & Resources.LibResources.Label354
                        Case Else
                            'lblDisc.Text = "Discount varies by minor code"
                            lblDisc.Text = Resources.LibResources.Label169
                            DoMe = True
                    End Select
                End If
            Loop
            If DoMe = True Then
                MyDataReader.Close()
                sql = "SELECT DISC_MNR.DISC_CD, TP_CD, PCNT, INV_MNR.DES, DISC_MNR.MNR_CD FROM DISC_MNR, INV_MNR WHERE DISC_MNR.MNR_CD=INV_MNR.MNR_CD AND DISC_MNR.DISC_CD='" & found_item & "' ORDER BY DISC_MNR.MNR_CD"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read()
                    Select Case MyDataReader(1).ToString().Trim()
                        Case "R"
                            txt_Minors.Text = txt_Minors.Text & MyDataReader(4).ToString.Trim & " - " & MyDataReader(3).ToString.Trim & " / " & CDbl(MyDataReader(2).ToString().Trim()) & "% Off Retail" & vbCrLf
                        Case "C"
                            txt_Minors.Text = txt_Minors.Text & MyDataReader(4).ToString.Trim & " - " & MyDataReader(3).ToString.Trim & " / " & "Cost Plus " & CDbl(MyDataReader(2).ToString().Trim()) & "%" & vbCrLf
                        Case Else
                            txt_Minors.Text = txt_Minors.Text & "WHAT"
                    End Select
                Loop
                MyDataReader.Close()
            End If

            disc_cd.SelectedIndex = 0

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
