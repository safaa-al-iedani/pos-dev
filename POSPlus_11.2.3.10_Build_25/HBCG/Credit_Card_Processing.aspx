<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Credit_Card_Processing.aspx.vb"
    Inherits="Credit_Card_Processing" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<script language="javascript" type="text/javascript">
    function disableButton(id) {
        // Daniela B 20140723 prevent double click
        try {
            var a = document.getElementById(id);
            a.style.display = 'none';
        } catch (err) {
            alert('Error in disableButton ' + err.message);
            return false;
        }
        return true;
    }
</script>
<head runat="server">
    <title>Credit Card Processing</title>
    </head>
<body>
     <form id="form1" defaultbutton="btn_save" runat="server">
     
          <table>
              <%--<asp:Button ID="btnDisableEnter" runat="server" Text="" OnClientClick="return false;" style="display:none;"/>--%>
            <tr>
                <td valign="middle" align="center">
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td valign="middle" align="center">
                    <table>
                        <tr>
                            <td colspan="4" align="left">
                                <dx:ASPxLabel ID="lbl_amount" runat="server" Text="" Width="100%" Font-Bold="True">
                                </dx:ASPxLabel>
                            </td>
                                  
                            <td colspan="1" align="center">
                                <dx:ASPxLabel ID="lbl_Full_Name" runat="server" Width="202px" Text=" ">
                                </dx:ASPxLabel>
                            </tr>
                         <tr>
                            <td colspan="3" class="auto-style2">
                                <%--Daniela May 22<dx:ASPxButton ID="btn_save" runat="server" Text="Click to Swipe/Insert." Width="200px" UseSubmitBehavior="False" ForeColor="Fuchsia" ClientSideEvents-Click="function(s,e) { if (ASPxClientEdit.AreEditorsValid()) s.SetVisible(false); }">--%>
                                <asp:Button ID="btn_save" runat="server" Text="<%$ Resources:LibResources, Label90 %>" Width="200px" ForeColor="Fuchsia" OnClientClick="return disableButton(this.id);">
                                </asp:Button>
                            </td>
                            <td colspan="4" class="auto-style2">
                                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>" Width="100px" Visible="False">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                          <asp:Label ID="lucy_lbl_msg" runat="server" Text=" " ForeColor="#FF3300"></asp:Label>
                        <tr>
                            <td colspan="7" align="left">
                                <br />
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label124 %>">
                                </dx:ASPxLabel>
                                <dx:ASPxLabel ID="lbl_card_type" runat="server" Text="">
                                </dx:ASPxLabel>
                               
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left">
                                 <asp:TextBox ID="TextBox1" runat="server" Width="226px" BorderColor="#999999" 
                                    BorderStyle="Solid" BorderWidth="1px" AutoPostBack="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label191 %>">
                                </dx:ASPxLabel>
                            </td>
                            <td align="right">
                                <asp:TextBox ID="TextBox2" runat="server" Width="29px" MaxLength="2" AutoPostBack="true">
                                </asp:TextBox>
                            </td>
                           
                            <td>
                                /
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" Width="29px" MaxLength="2" AutoPostBack="true">
                                </asp:TextBox>
                            </td>
                            <td>
                               
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <dx:ASPxTextBox ID="txt_security_code" runat="server" Width="40px" MaxLength="4" Visible="False">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                          <tr>
                               <td colspan="1" align="left" class="auto-style4">
                                    <asp:Label ID="lucy_lbl_checkbox" runat="server" Text="Telephone Purchase"></asp:Label>
                                   </td> 
                              <td>
                             <asp:CheckBox ID="lucy_CheckBox" runat="server" />
                                    </td>
                                 </tr>
                       
                        <tr>
                             <td colspan="1" align="left" class="auto-style4">
                             <asp:Label ID="lucy_lbl_app_cd" runat="server" Text="<%$ Resources:LibResources, Label36 %>" ></asp:Label>
                                  </td>
                            <td colspan="1" align="right" class="auto-style4">
                             <asp:TextBox ID="lucy_app_cd" runat="server" MaxLength="8" Width="49px"></asp:TextBox>
                               
                                </td>
                            <td>
                             <asp:Label ID="lucy_lbl_plan" runat="server" Text="<%$ Resources:LibResources, Label401 %>"></asp:Label>
                                </td>
                            <td>
                             <asp:TextBox ID="lucy_text_fi_plan" runat="server" Width="29px"></asp:TextBox>
                                </td>
                            <td colspan="1" align="right">
                                <dx:ASPxLabel ID="lbl_tran_id" runat="server" Visible="False">
                                </dx:ASPxLabel>
                               
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" align="left" class="auto-style4" >
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label370 %>">
                                </dx:ASPxLabel>
                            </td>
                            <td colspan="1" align="right" >
                                <dx:ASPxTextBox ID="txt_auth" runat="server" autopostback="true"  MaxLength="8" Width="49px">
                                </dx:ASPxTextBox>
                            </td>
                             <td colspan="1" align="left" >
                            <asp:Label ID="lucy_lbl_text_corr" runat="server"   Text="Correction"></asp:Label>
                                 </td>
                             <td colspan="1" align="right" >
                             <asp:TextBox ID="lucy_Text_correction" runat="server" MaxLength="1"  Width="12px" ></asp:TextBox>
                             </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Please swipe or enter the credit card information and click Save when finished"
                        Width="100%" Font-Size="XX-Small" Visible="False">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
