<%@ Page Language="VB" AutoEventWireup="false" CodeFile="customorderentry.aspx.vb"
    Inherits="customorderentry" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom Order Entry</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <b>
                <asp:Label ID="lbl_SKU" runat="server" Style="position: relative"
                    Width="554px"></asp:Label></b><br />
            <b>
                <asp:Label ID="lbl_DESC" runat="server" Style="left: 0px; position: relative"
                    Width="554px"></asp:Label></b><br />
            <br />
            <asp:DataGrid ID="GridView1" runat="server" Width="554px" 
                AutoGenerateColumns="False" PageSize="15"
                DataKeyField="option_cd" CellPadding="2" CellSpacing="4">
                <Columns>
                    <asp:BoundColumn DataField="option_cd" ReadOnly="True" Visible="False">
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="OPTION_VE_CD" ReadOnly="True" Visible="False">
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="OPTION_TP" ReadOnly="True" Visible="False">
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="des" HeaderText="DESC" ReadOnly="True" ItemStyle-Wrap="false">
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                     </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="OPTION VALUE">
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                        <ItemTemplate>
                            <asp:DropDownList ID="cbo_Option" runat="server" Style="position: relative" Width="336px">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:DataGrid><br />
            <table style="width: 554px; height: 1px">
                <tr>
                    <td colspan="2" style="height: 26px; text-align: center" valign="top">
                        <dx:ASPxButton ID="btn_Submit" runat="server" Text="Submit Options">
                        </dx:ASPxButton>
                        <asp:Label ID="lbl_error" runat="server" Font-Bold="True" ForeColor="Red" Style="position: relative"
                            Width="392px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                    <hr />
                        <strong><span>- Use Existing Combination -</span></strong>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="50%" align="right">
                        <asp:TextBox ID="txt_combo" runat="server" Height="20px"></asp:TextBox>
                    </td>
                    <td valign="top" width="50%" align="left">
                        <dx:ASPxButton ID="btn_Combination" runat="server" Text="Submit Existing Combination">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
