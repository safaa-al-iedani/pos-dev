Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Globalization ' Code Added by Kumaran for POS+ French Conversion on - 02-Sep-2015
Imports HBCG_Utils

Partial Class LCOMCommissionDetails
    Inherits POSBasePage

    Public Const gs_version As String = "Version 3.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY - see LCOMCommissionLookup.aspx.vb
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveCommissionDetails()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves and populates the 
    '   Called by:      RetrieveItemInfo() - if the item is found/valid
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function RetrieveCommissionDetails(p_salesperson_code As String, p_del_doc_num As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim SQL As String
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_salesperson_code As String = p_salesperson_code.ToUpper
        Dim s_del_doc_num As String = p_del_doc_num.ToUpper

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        Dim b_Continue As Boolean = False
        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(s_co_cd.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Company code is required - your login did not retrieve a proper company code
            s_errorMessage = Resources.CustomMessages.MSG0041
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False

        End If

        If (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString())) OrElse String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Salesperson and Delivery Document are required
            s_errorMessage = Resources.CustomMessages.MSG0042
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False

        End If
           
        SQL = ""
        SQL = SQL & "SELECT del_doc_ln#, pct_of_sale AS split_perc, so_line_qty AS qty, itm_cd, itm_tp_cd, itm_des, "
        SQL = SQL & "TO_CHAR(unit_price,'999,999.99') AS unit_price, TO_CHAR(ext_price,'999,999.99') AS ext_price, comm_cd, retail_percentage AS commission_perc, "
        SQL = SQL & "TO_CHAR(NVL(commission$,0),'999,999.99') AS commission$, TO_CHAR(NVL(spiff$,0),'999,999.99') AS spiff$ "
        SQL = SQL & "FROM ( "

        '--------------------------------------------------------
        '--QUERY 1 - SO_EMP_SLSP_CD1
        '--------------------------------------------------------
        SQL = SQL & "SELECT s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ) AS finalized_date, sl.so_emp_slsp_cd1 AS emp_slsp_cd, sl.pct_of_sale1 AS pct_of_sale, "
        SQL = SQL & "es.store_cd, sl.del_doc_ln#, DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) AS so_line_qty, "
        SQL = SQL & "sl.itm_cd, i.itm_tp_cd, i."
        SQL = SQL & Resources.CustomMessages.MSG0080
        SQL = SQL & " as itm_des, sl.out_cd, "

        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * ( sl.pct_of_sale1 / 100 ), 2) AS unit_price, "
        SQL = SQL & "ROUND( sl.qty * DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2) AS ext_price, "
        SQL = SQL & "sl.comm_cd, MIN(scr.ret_pct) AS retail_percentage, "
        SQL = SQL & "ROUND( sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (MIN(scr.ret_pct) / 100) * (sl.pct_of_sale1 / 100), 2) AS commission$, "
        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale1 / 100), 2) AS spiff$ "

        SQL = SQL & "FROM itm i, so s, so_ln sl, emp_slsp es, store s, co_grp cg, store_comm_ret scr "
        SQL = SQL & "WHERE s.del_doc_num = sl.del_doc_num "
        SQL = SQL & "AND sl.so_emp_slsp_cd1 = es.emp_cd "
        SQL = SQL & "AND i.itm_cd = sl.itm_cd "
        SQL = SQL & "AND scr.comm_cd = sl.comm_cd "
        SQL = SQL & "AND s.store_cd = s.so_store_cd "
        SQL = SQL & "AND s.co_cd = cg.co_cd "

        SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_co_cd) ) "
        SQL = SQL & "AND scr.store_cd = s.so_store_cd "

        SQL = SQL & "AND sl.VOID_FLAG = 'N' "

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            SQL = SQL & "AND sl.so_emp_slsp_cd1 = :x_salesperson_code "
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            SQL = SQL & "AND sl.del_doc_num = :x_del_doc_num "
        End If

        SQL = SQL & "GROUP BY s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ), sl.so_emp_slsp_cd1, sl.pct_of_sale1, es.store_cd, sl.del_doc_ln#, "
        SQL = SQL & "           DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty), "
        SQL = SQL & "           sl.itm_cd, i.itm_tp_cd, i.des, i.alt_des, sl.out_cd, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2), "
        SQL = SQL & "           ROUND(sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2), "
        SQL = SQL & "           sl.comm_cd, sl.qty, sl.unit_prc, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale1 / 100), 2) "
        SQL = SQL & " "
        SQL = SQL & "UNION "
        SQL = SQL & " "

        '--------------------------------------------------------
        '--QUERY 2 - SO_EMP_SLSP_CD2
        '--------------------------------------------------------

        SQL = SQL & "SELECT s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ) AS finalized_date, sl.so_emp_slsp_cd2 AS emp_slsp_cd, sl.pct_of_sale2 AS pct_of_sale, "
        SQL = SQL & "es.store_cd, sl.del_doc_ln#, DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) AS so_line_qty, "
        SQL = SQL & "sl.itm_cd, i.itm_tp_cd, i."
        SQL = SQL & Resources.CustomMessages.MSG0080
        SQL = SQL & " as itm_des, sl.out_cd, "

        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * ( sl.pct_of_sale2 / 100 ), 2) AS unit_price, "
        SQL = SQL & "ROUND( sl.qty * DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2) AS ext_price, "
        SQL = SQL & "sl.comm_cd, MIN(scr.ret_pct) AS retail_percentage, "
        SQL = SQL & "ROUND( sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (MIN(scr.ret_pct) / 100) * (sl.pct_of_sale2 / 100), 2) AS commission$, "
        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale2 / 100), 2) AS spiff$ "

        SQL = SQL & "FROM itm i, so s, so_ln sl, emp_slsp es, store s, co_grp cg, store_comm_ret scr "
        SQL = SQL & "WHERE s.del_doc_num = sl.del_doc_num "
        SQL = SQL & "AND sl.so_emp_slsp_cd2 = es.emp_cd "
        SQL = SQL & "AND sl.so_emp_slsp_cd2 IS NOT NULL "
        SQL = SQL & "AND i.itm_cd = sl.itm_cd "
        SQL = SQL & "AND scr.comm_cd = sl.comm_cd "
        SQL = SQL & "AND s.store_cd = s.so_store_cd "
        SQL = SQL & "AND s.co_cd = cg.co_cd "

        SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_co_cd) ) "
        SQL = SQL & "AND scr.store_cd = s.so_store_cd "

        SQL = SQL & "AND sl.VOID_FLAG = 'N' "

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            SQL = SQL & "AND sl.so_emp_slsp_cd2 = :x_salesperson_code "
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            SQL = SQL & "AND sl.del_doc_num = :x_del_doc_num "
        End If

        SQL = SQL & "GROUP BY s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ), sl.so_emp_slsp_cd2, sl.pct_of_sale2, es.store_cd, sl.del_doc_ln#, "
        SQL = SQL & "           DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty), "
        SQL = SQL & "           sl.itm_cd, i.itm_tp_cd, i.des, i.alt_des, sl.out_cd, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2), "
        SQL = SQL & "           ROUND(sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2), "
        SQL = SQL & "           sl.comm_cd, sl.qty, sl.unit_prc, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale2 / 100), 2) "

        SQL = SQL & ") "
        SQL = SQL & "ORDER BY del_doc_ln# "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        objsql.Parameters(":x_co_cd").Value = s_co_cd

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            objsql.Parameters.Add(":x_salesperson_code", OracleType.VarChar)
            objsql.Parameters(":x_salesperson_code").Value = s_salesperson_code
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            objsql.Parameters.Add(":x_del_doc_num", OracleType.VarChar)
            objsql.Parameters(":x_del_doc_num").Value = s_del_doc_num
        End If

        Dim ds_GridViewCommissionDetails As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
        oAdp.Fill(ds_GridViewCommissionDetails)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            'Store Values in String Variables 
            If Mydatareader.Read Then
                Try
                    'nothing to do
                Catch ex As Exception
                    conn.Close()
                End Try

                Mydatareader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Dim dv_GridViewCommissionDetails As DataView = New DataView(ds_GridViewCommissionDetails.Tables(0))

        Dim dt_commissionDetails As DataTable = New DataTable

        dt_commissionDetails.Columns.Add("DEL_DOC_LN#", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("SPLIT_PERC", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("QTY", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("ITM_CD", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("ITM_TP_CD", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("ITM_DES", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("UNIT_PRICE", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("EXT_PRICE", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("COMM_CD", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("COMMISSION_PERC", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("COMMISSION$", Type.GetType("System.String"))
        dt_commissionDetails.Columns.Add("SPIFF$", Type.GetType("System.String"))

        For Each drv_GridViewCommissionDetails As DataRowView In dv_GridViewCommissionDetails

            Dim dr_newDataRow As DataRow = dt_commissionDetails.NewRow()
            dr_newDataRow("DEL_DOC_LN#") = drv_GridViewCommissionDetails("DEL_DOC_LN#")
            dr_newDataRow("SPLIT_PERC") = drv_GridViewCommissionDetails("SPLIT_PERC")
            dr_newDataRow("QTY") = drv_GridViewCommissionDetails("QTY")
            dr_newDataRow("ITM_CD") = drv_GridViewCommissionDetails("ITM_CD")
            dr_newDataRow("ITM_TP_CD") = drv_GridViewCommissionDetails("ITM_TP_CD")
            dr_newDataRow("ITM_DES") = drv_GridViewCommissionDetails("ITM_DES")
            dr_newDataRow("UNIT_PRICE") = drv_GridViewCommissionDetails("UNIT_PRICE")
            dr_newDataRow("EXT_PRICE") = drv_GridViewCommissionDetails("EXT_PRICE")
            dr_newDataRow("COMM_CD") = drv_GridViewCommissionDetails("COMM_CD")
            dr_newDataRow("COMMISSION_PERC") = drv_GridViewCommissionDetails("COMMISSION_PERC")
            dr_newDataRow("COMMISSION$") = drv_GridViewCommissionDetails("COMMISSION$")
            dr_newDataRow("SPIFF$") = drv_GridViewCommissionDetails("SPIFF$")

            dt_commissionDetails.Rows.Add(dr_newDataRow)

        Next
        GridViewCommissionDetails.DataSource = dt_commissionDetails
        GridViewCommissionDetails.DataBind()
        GridViewCommissionDetails.Width = 960

        If b_Continue Then
            'do nothing
        End If

        Return True

    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'keep in mind this index gets refreshed everytime the next, prev, first, last buttons are pressed
        'Session("PSQGRID_INDEX") = 1

        Dim s_salesperson_code As String = Request("SALESPERSON_CODE")
        Dim s_del_doc_num As String = Request("DEL_DOC_NUM")

        'If Not IsPostBack = True Then

            If s_salesperson_code IsNot Nothing AndAlso s_del_doc_num IsNot Nothing Then
                If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString())) OrElse String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then

                    RetrieveCommissionDetails(s_salesperson_code, s_del_doc_num)

                End If
            End If

        'End If

        lbl_versionInfo.Text = gs_version

        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'lbl_title_commission_details.Text = "Order Detail for Document - " + s_del_doc_num
        'GridViewCommissionDetails.Visible = True
        lbl_title_commission_details.Text = String.Format(Resources.CustomMessages.MSG0017, s_del_doc_num)
        GridViewCommissionDetails.Visible = True
        btnGoBackToSummary.ToolTip = GetLocalResourceObject("tool_btn_goback_to_summary.Text".ToString())
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

    End Sub

    Public Sub GoBackToSummaryClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Response.Redirect("LCOMCommissionLookup.aspx?back_from_details=Y")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
    End Sub

    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridViewCommissionDetails.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

    End Sub

End Class