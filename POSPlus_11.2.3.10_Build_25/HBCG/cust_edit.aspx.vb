Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils
Imports AppExtensions
Imports System.Linq
Imports SessionIntializer

Partial Class cust_edit
    Inherits POSBasePage

    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theTranBiz As TransportationBiz = New TransportationBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dec 16 disable remove customer button if payments are approved
        If Not IsPostBack Then
            If InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                btn_Remove_Cust.Enabled = False
                removeWarning.Visible = True
            End If
        End If
        If Session("vd_inspmt") = "Y" Then     'lucy
            lucy_btn_inspmt.Visible = True
        Else
            lucy_btn_inspmt.Visible = False

        End If

        'sabrina add for tdpl instore payment
        If Session("td_instpmt") = "Y" Then
            sy_btn_tdpl_inspmt.Visible = True
        Else
            sy_btn_tdpl_inspmt.Visible = False
        End If

        'sabrina add for GC Transfer (gc_transfer)
        If Session("gc_transfer") = "Y" Then
            sy_btn_gc_transfer.Visible = True
        Else
            sy_btn_gc_transfer.Visible = False
        End If

        'Alice add for Other Invoice in Payment Application - Request 167
        If Session("OtherCustCd") = "Y" Then
            sy_btn_pa_other.Visible = True
        Else
            sy_btn_pa_other.Visible = False
        End If


        'sabrina add for payr payment reversal
        If Session("payment_reversal") = "Y" Then
            sy_btn_payr.Visible = True
        Else
            sy_btn_payr.Visible = False
        End If

        'sabrina R1922
        If Session("gift_card_purchase") = "Y" Then
            sy_btn_gc_purchase.Visible = True
        Else
            sy_btn_gc_purchase.Visible = False
        End If

        'sabrina aeroplangoodwillmpc
        If Session("aeroMPCmain") = "Y" Then
            sy_btn_aerompc.Visible = True
        Else
            sy_btn_aerompc.Visible = False
        End If
        ' MM-3179 - Enter key kicks out of session
        ' Me.Form.DefaultButton = btn_Remove_Cust.UniqueID
        Dim rel_check As String
        rel_check = Session("rel_check") & ""

        If Not IsNothing(Session("isCommited")) Then
            IntializeSession()
        End If

        If Not IsPostBack = True Then

            img_warn.Visible = False
            lbl_Warning.Visible = False
            'populate the text boxes
            If get_cust_info() Then
                PopulateData(rel_check)
            End If
        End If
        If Request("del_doc_num") & "" <> "" And rel_check <> "TRUE" And Request("LEAD") <> "TRUE" Then
            CheckForDroppedSkus()
            'convert_relationship()
            Session("rel_check") = "TRUE"
        End If
        If Find_Security("PSOETAXCD", Session("EMP_CD")) = "N" Then
            chk_tax_exempt.Enabled = False
        Else
            chk_tax_exempt.Enabled = True
        End If

        ' May 18, 2018 IT Req 2919
        EmailCheck.Enabled = False
        If (isNotEmpty(Session("HOME_STORE_CD")) And isNotEmpty(Email.Text)) Then
            If (LeonsBiz.ValidateEmail(Email.Text) = True AndAlso
               LeonsBiz.CheckStoreGroup("EER", Session("HOME_STORE_CD"))) Then
                EmailCheck.Enabled = True
            End If
        End If
    End Sub
    Protected Sub lucy_btn_inspmt_Click(sender As Object, e As EventArgs) Handles lucy_btn_inspmt.Click
        If Session("vd_inspmt") = "Y" Then 'lucy

            Response.Redirect("vd_inspmt.aspx")
        End If
    End Sub

    Private Sub PopulateData(rel_check As String)
        state_populate()
        If Session("no_tax") & "" <> "" Then
            chk_tax_exempt.Checked = True
            txt_tax_exempt_id.Visible = True
            cbo_tax_exempt.Visible = True
            PopulateTaxExempt()
            If Session("exempt_id") & "" <> "" Then
                txt_tax_exempt_id.Text = Session("exempt_id")
            End If
        End If
        calculate_total(Page.Master)
        If Request("ADD_NEW") = "TRUE" Then
            Session("rel_check") = "TRUE"
            rel_check = "TRUE"
        End If
        If rel_check <> "TRUE" And ConfigurationManager.AppSettings("show_cust_relationships").ToString = "Y" And Request("LEAD") <> "TRUE" And Session("Converted_REL_NO") & "" = "" And Session("cash_carry") <> "TRUE" Then
            If IsNothing(Session("ShowRelationships")) Then
                ' Daniela June 18 and parameter
                Dim grpCd As String = ""
                If isNotEmpty(Session("CO_CD")) Then
                    grpCd = LeonsBiz.GetGroupCode(Session("CO_CD"))
                    If isNotEmpty(grpCd) Then
                        Populate_Relationships(grpCd)
                    Else
                        Populate_Relationships() ' Should not happen
                    End If
                Else : Populate_Relationships() ' Should not happen
                End If

            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtFName.ClientID + "').focus();$get('" + txtFName.ClientID + "').select();", True)
    End Sub


    Public Sub state_populate()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStates As OracleCommand
        cmdGetStates = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT st_cd, des, st_cd || ' - ' || des as full_desc FROM ST order by st_cd"

        Try
            With cmdGetStates
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStates)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If st_label.Text & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("st_cd")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(st_label.Text)
            End If

            With State
                .DataSource = ds
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                If Not (foundrow Is Nothing) Then
                    .SelectedValue = st_label.Text
                End If
                .DataBind()
            End With
            With bill_state
                .DataSource = ds
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            State.Items.Insert(0, " ")
            State.Items.FindByText(" ").Value = ""
            bill_state.Items.Insert(0, " ")
            bill_state.Items.FindByText(" ").Value = ""

            If bill_st_label.Text & "" <> "" Then
                bill_state.SelectedValue = bill_st_label.Text
            End If
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Function get_cust_info() As Boolean

        Dim hadTaxCode As Boolean = True

        Dim custcd As String
        custcd = Request.QueryString("custid")
        If custcd & "" = "" And Session("cust_cd") & "" <> "" Then
            custcd = Session("cust_cd")
        End If

        If custcd = "" Then
            If Session("cust_zip") & "" <> "" Then
                If Session("cust_city") & "" = "" Or Session("cust_state") & "" = "" Then
                    Zip.Text = Session("cust_zip")
                    Update_City_State()
                End If
            End If
            txt_corp_name.Text = Replace(UCase(Session("corp_name")), "%", "")
            txtLName.Text = Replace(UCase(Session("lname")), "%", "")
            txt_bill_LName.Text = UCase(Session("lname"))
            txtFName.Text = Replace(UCase(Session("fname")), "%", "")
            txt_bill_FName.Text = UCase(Session("fname"))
            txtAddr.Text = Replace(UCase(Session("addr")), "%", "")
            txt_bill_Addr.Text = UCase(Session("addr"))
            If Session("cust_tp") & "" = "" Then
                Session("cust_tp") = ConfigurationManager.AppSettings("cust_tp").ToString
            End If
            drp_cust_tp.SelectedValue = UCase(Session("cust_tp"))
            txtCustCode.Text = "NEW"
            txtBusPhone.Text = Replace(UCase(Session("bus_phone")), "%", "")
            txtHomePhone.Text = Replace(UCase(Session("home_phone")), "%", "")
            City.Text = Replace(UCase(Session("cust_city")), "%", "")
            st_label.Text = Replace(UCase(Session("cust_state")), "%", "")
            Zip.Text = Replace(UCase(Session("cust_zip")), "%", "")
            Session("zip_cd") = UCase(Zip.Text)

            If theInvBiz.RequireZoneCd(Session("PD"), SessVar.isArsEnabled, SessVar.ordTpCd) Then

                Dim puDelZip As String = theTranBiz.GetPuDelZip(Session("PD"),
                                    SessVar.puDelStoreCd, UCase(Zip.Text), Session("ZIP_CD"))
                SessVar.zipCode4Zone = puDelZip
                SessVar.zoneCd = Nothing
            End If

            bill_City.Text = UCase(Session("cust_city"))
            bill_st_label.Text = UCase(Session("cust_state"))
            bill_Zip.Text = UCase(Session("cust_zip"))

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim sql As String
            conn.Open()
            sql = "INSERT INTO CUST_INFO (SESSION_ID, CUST_CD, FNAME, LNAME, CUST_TP, ADDR1, CITY, ST, ZIP, HPHONE, BPHONE, BILL_FNAME, BILL_LNAME, BILL_ADDR1, BILL_CITY, BILL_ST, BILL_ZIP, CORP_NAME) "
            sql = sql & "VALUES(:SESSION_ID, :CUST_CD, :FNAME, :LNAME, :CUST_TP, :ADDR1, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :FNAME, :LNAME, :ADDR1, :CITY, :ST, :ZIP, :CORP_NAME) "

            With cmdDeleteItems
                .Connection = conn
                .CommandText = sql
            End With

            cmdDeleteItems.Parameters.Add(":CORP_NAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CORP_NAME").Value = Left(UCase(txt_corp_name.Text.ToString.Trim), 30)
            cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
            cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
            cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CUST_CD").Value = "NEW"
            cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":FNAME").Value = Session("FNAME").ToString.Trim
            cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":LNAME").Value = Session("LNAME").ToString.Trim
            cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CUST_TP").Value = Session("CUST_TP").ToString.Trim
            cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ADDR1").Value = Session("ADDR").ToString.Trim
            cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CITY").Value = Session("cust_city").ToString.Trim
            cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ST").Value = Session("cust_state").ToString.Trim
            cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ZIP").Value = Session("cust_zip").ToString.Trim
            cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
            cmdDeleteItems.Parameters(":HPHONE").Value = Session("HOME_PHONE").ToString.Trim
            cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
            cmdDeleteItems.Parameters(":BPHONE").Value = Session("BUS_PHONE").ToString.Trim

            Session("CUST_CD") = "NEW"
            cmdDeleteItems.ExecuteNonQuery()
            conn.Close()

            If Zip.Text & "" <> "" Then
                hadTaxCode = determine_tax(Zip.Text)
                calculate_total(Page.Master)
            End If
        Else
            ' get info from the database
            get_cust_info_from_db()
            If Session("no_tax") & "" = "" Then
                If IsNothing(Session("TAX_RATE")) Then
                    hadTaxCode = determine_tax("")
                ElseIf Session("TAX_RATE").ToString & "" = "" Then
                    hadTaxCode = determine_tax("")
                End If
            End If
        End If

        Return hadTaxCode

    End Function

    Public Function determine_tax(ByVal zip_cd As String) As Boolean

        Dim hadTaxCode As Boolean = True

        Dim taxability As String = ""
        Dim SQL As String = ""
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)
        conn.Open()

        If Session("PD") = "P" Then
            If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                zip_cd = store.addr.postalCd
            End If
        Else
            zip_cd = theCustomerBiz.GetCustomerZip(Session.SessionID.ToString, Session("CUST_CD"))
        End If
        zip_cd = UCase(zip_cd)

        '** check if the zip is setup to use written store for it's taxes
        Dim Use_Written As String = "N"
        If zip_cd.isNotEmpty Then
            SQL = "SELECT * FROM ZIP WHERE ZIP_CD='" & zip_cd & "'"
            cmd.CommandText = SQL

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                If MyDataReader.Read() Then
                    If Session("PD") = "P" Then
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                    Else
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                    End If
                End If
                MyDataReader.Close()

                If Use_Written = "Y" Then
                    '** since the written store needs to be used for taxation, get it's zip for retrieving tax codes
                    SQL = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
                    cmd.CommandText = SQL

                    MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                    If MyDataReader.Read() Then
                        zip_cd = MyDataReader.Item("ZIP_CD")
                    End If
                    MyDataReader.Close()
                End If
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            zip_cd = UCase(zip_cd)
            Dim MyTable As New DataTable
            ' ** retrieve tax codes by zip
            Dim ds As DataSet = theSalesBiz.GetTaxCodesByZip(zip_cd)
            MyTable = ds.Tables(0)

            If (MyTable.Rows.Count = 1) Then

                hadTaxCode = True

                Dim taxCd As String = MyTable.Rows(0).Item("TAX_CD").ToString
                Dim taxRate As String = MyTable.Rows(0).Item("SumofPCT").ToString
                Session("TAX_RATE") = taxRate
                Session("TAX_CD") = taxCd

                'Add tax rates to the inventory, if items exist                
                Try
                    'set tax for take-with line to zero
                    SQL = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_AMT=0, TAX_PCT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()

                    '** select and update tax for other non-TW lines
                    SQL = "select row_id, itm_tp_cd, package_parent FROM temp_itm WHERE session_id='" & Session.SessionID.ToString & "'" & " AND take_with IS NULL"
                    cmd.CommandText = SQL
                    MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                    Do While MyDataReader.Read
                        taxability = ""
                        If (Session("TAX_CD") & "" <> "") Then
                            taxability = TaxUtils.Determine_Taxability(MyDataReader.Item("ITM_TP_CD").ToString, Date.Today, Session("TAX_CD"))
                        End If
                        If taxability = "Y" Then
                            If MyDataReader.Item("PACKAGE_PARENT").ToString & "" = "" Then
                                'update the tax amts for a regular sku - not a pkg
                                SQL = "UPDATE TEMP_ITM SET TAXABLE_AMT=RET_PRC, TAX_AMT=ROUND((RET_PRC*(NVL(" & taxRate & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_CD='" & taxCd & "', TAX_PCT=" & taxRate & " WHERE ROW_ID='" & MyDataReader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                            Else
                                'update taxes for the package sku
                                SQL = "UPDATE TEMP_ITM SET TAX_CD='" & Session("TAX_CD").ToString & "', TAX_AMT=ROUND((TAXABLE_AMT*(NVL(" & taxRate & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_PCT=" & taxRate & " WHERE ROW_ID='" & MyDataReader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                            End If
                            cmd.CommandText = SQL
                            cmd.ExecuteNonQuery()
                        End If
                    Loop
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Else
                hadTaxCode = False
                Dim _hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                'Refer MM-7026 JIRA - All Tax codes need to be displayed
                If SecurityUtils.hasSecurity("PSOETAXCD", Session("EMP_CD")) Then
                    _hidZip.Value = String.Empty
                Else
                    _hidZip.Value = zip_cd
                End If

                ucTaxUpdate.LoadTaxData()
                PopupTax.ShowOnPageLoad = True
            End If
            conn.Close()

            '** get and set  the delivery tax 
            Dim sessTaxCd As String = If(IsDBNull(Session("TAX_CD")) OrElse String.IsNullOrEmpty(Session("TAX_CD")), String.Empty, Session("TAX_CD"))
            Session("DEL_TAX") = theSalesBiz.GetTaxPctForDelv(sessTaxCd)

            '** get and set  the setup tax 
            Session("SU_TAX") = theSalesBiz.GetTaxPctForSetup(sessTaxCd)

        End If

        Return hadTaxCode

    End Function

    Public Sub get_cust_info_from_db()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim custcd As String
        Dim warning_list As String
        warning_list = ""
        custcd = UCase(Request.QueryString("custid"))

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn2.Open()
        Dim objsql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader

        'jkl
        'sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & custcd & "'"
        sql = "SELECT * FROM CUST_INFO WHERE Session_ID=:sessionID AND CUST_CD=:custCode"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        'jkl
        objsql2.Parameters.Add(":sessionID", OracleType.VarChar)
        objsql2.Parameters(":sessionID").Value = Session.SessionID.ToString.Trim
        objsql2.Parameters.Add(":custCode", OracleType.VarChar)
        objsql2.Parameters(":custCode").Value = custcd

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then
                If MyDataReader2.Item("CORP_NAME") & "" <> "" Then
                    txt_corp_name.Text = MyDataReader2.Item("CORP_NAME")
                End If
                If MyDataReader2.Item("FNAME") & "" <> "" Then
                    txtFName.Text = MyDataReader2.Item("FNAME")
                End If
                If MyDataReader2.Item("CUST_TP") & "" <> "" Then
                    drp_cust_tp.SelectedValue = MyDataReader2.Item("CUST_TP")
                End If
                If MyDataReader2.Item("BILL_FNAME") & "" <> "" Then
                    txt_bill_FName.Text = MyDataReader2.Item("BILL_FNAME")
                End If
                If MyDataReader2.Item("LNAME") & "" <> "" Then
                    txtLName.Text = MyDataReader2.Item("LNAME")
                End If
                If MyDataReader2.Item("BILL_LNAME") & "" <> "" Then
                    txt_bill_LName.Text = MyDataReader2.Item("BILL_LNAME")
                End If
                If MyDataReader2.Item("HPHONE") & "" <> "" Then
                    txtHomePhone.Text = MyDataReader2.Item("HPHONE")
                End If
                If MyDataReader2.Item("BPHONE") & "" <> "" Then
                    txtBusPhone.Text = MyDataReader2.Item("BPHONE")
                End If
                If MyDataReader2.Item("CUST_CD") & "" <> "" Then
                    txtCustCode.Text = MyDataReader2.Item("CUST_CD")
                End If
                If MyDataReader2.Item("ADDR1") & "" <> "" Then
                    txtAddr.Text = MyDataReader2.Item("ADDR1")
                End If
                If MyDataReader2.Item("BILL_ADDR1") & "" <> "" Then
                    txt_bill_Addr.Text = MyDataReader2.Item("BILL_ADDR1")
                End If
                If MyDataReader2.Item("addr2") & "" <> "" Then
                    txtAddr2.Text = MyDataReader2.Item("addr2")
                End If
                If MyDataReader2.Item("BILL_addr2") & "" <> "" Then
                    txt_bill_Addr2.Text = MyDataReader2.Item("BILL_addr2")
                End If
                If MyDataReader2.Item("city") & "" <> "" Then
                    City.Text = MyDataReader2.Item("city")
                End If
                If MyDataReader2.Item("BILL_city") & "" <> "" Then
                    bill_City.Text = MyDataReader2.Item("BILL_city")
                End If
                If MyDataReader2.Item("ST") & "" <> "" Then
                    st_label.Text = MyDataReader2.Item("ST")
                End If
                If MyDataReader2.Item("BILL_ST") & "" <> "" Then
                    bill_st_label.Text = MyDataReader2.Item("BILL_ST")
                End If
                If MyDataReader2.Item("zip") & "" <> "" Then
                    Zip.Text = MyDataReader2.Item("zip")
                    Session("zip_cd") = UCase(Zip.Text)
                    Session("cust_zip") = UCase(Zip.Text)

                    If theInvBiz.RequireZoneCd(Session("PD"), SessVar.isArsEnabled, SessVar.ordTpCd) Then

                        Dim puDelZip As String = theTranBiz.GetPuDelZip(Session("PD"),
                                            SessVar.puDelStoreCd, UCase(Zip.Text), Session("ZIP_CD"))
                        SessVar.zipCode4Zone = puDelZip
                        SessVar.zoneCd = Nothing
                    End If
                End If
                If MyDataReader2.Item("BILL_ZIP") & "" <> "" Then
                    bill_Zip.Text = MyDataReader2.Item("BILL_ZIP")
                End If
                Email.Text = MyDataReader2.Item("email").ToString
                MyDataReader2.Close()
            Else
                sql = "SELECT FNAME, LNAME, CUST_CD, HOME_PHONE, BUS_PHONE, ADDR1,ADDR2, CITY, ST_CD, ZIP_CD, CUST_TP_CD, EMAIL_ADDR, CORP_NAME, CUST_TP_PRC_CD FROM CUST WHERE cust_cd = :custCode"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":custCode", OracleType.VarChar)
                objSql.Parameters(":custCode").Value = custcd

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        If MyDataReader.Item("FNAME").ToString & "" <> "" Then
                            txtFName.Text = MyDataReader.Item("FNAME").ToString
                            txt_bill_FName.Text = MyDataReader.Item("FNAME").ToString
                        End If
                        If MyDataReader.Item("CORP_NAME").ToString & "" <> "" Then
                            txt_corp_name.Text = MyDataReader.Item("CORP_NAME").ToString
                        End If
                        If MyDataReader.Item("CUST_TP_CD").ToString & "" <> "" Then
                            drp_cust_tp.SelectedValue = MyDataReader.Item("CUST_TP_CD").ToString
                        End If
                        If MyDataReader.Item("LNAME").ToString & "" <> "" Then
                            txtLName.Text = MyDataReader.Item("LNAME").ToString
                            txt_bill_LName.Text = MyDataReader.Item("LNAME").ToString
                        End If
                        If MyDataReader.Item("HOME_PHONE").ToString & "" <> "" Then
                            txtHomePhone.Text = MyDataReader.Item("HOME_PHONE").ToString
                        End If
                        If MyDataReader.Item("BUS_PHONE").ToString & "" <> "" Then
                            txtBusPhone.Text = MyDataReader.Item("BUS_PHONE").ToString
                        End If
                        If MyDataReader.Item("CUST_CD").ToString & "" <> "" Then
                            txtCustCode.Text = MyDataReader.Item("CUST_CD").ToString
                        End If
                        If MyDataReader.Item("ADDR1").ToString & "" <> "" Then
                            txtAddr.Text = MyDataReader.Item("ADDR1").ToString
                            txt_bill_Addr.Text = MyDataReader.Item("ADDR1").ToString
                        End If
                        If MyDataReader.Item("addr2").ToString & "" <> "" Then
                            txtAddr2.Text = MyDataReader.Item("addr2").ToString
                            txt_bill_Addr2.Text = MyDataReader.Item("ADDR2").ToString
                        End If
                        If MyDataReader.Item("city").ToString & "" <> "" Then
                            City.Text = MyDataReader.Item("city").ToString
                            bill_City.Text = MyDataReader.Item("city").ToString
                        End If
                        If MyDataReader.Item("st_cd").ToString & "" <> "" Then
                            st_label.Text = MyDataReader.Item("st_cd").ToString
                            bill_state.Text = MyDataReader.Item("st_cd").ToString
                        End If
                        If MyDataReader.Item("zip_cd").ToString & "" <> "" Then
                            Zip.Text = MyDataReader.Item("zip_cd").ToString
                            bill_Zip.Text = MyDataReader.Item("zip_cd").ToString
                            Session("zip_cd") = UCase(Zip.Text)
                            Session("cust_zip") = UCase(Zip.Text)

                            If theInvBiz.RequireZoneCd(Session("PD"), SessVar.isArsEnabled, SessVar.ordTpCd) Then

                                Dim puDelZip As String = theTranBiz.GetPuDelZip(Session("PD"),
                                                    SessVar.puDelStoreCd, UCase(Zip.Text), Session("ZIP_CD"))
                                SessVar.zipCode4Zone = puDelZip
                                SessVar.zoneCd = Nothing
                            End If
                        End If
                        Email.Text = MyDataReader.Item("email_ADDR").ToString.Trim
                        drp_cust_tp.SelectedValue = MyDataReader.Item("CUST_TP_CD").ToString.Trim
                        Session("cust_tp_prc_cd") = MyDataReader.Item("CUST_TP_PRC_CD").ToString.Trim

                        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


                        sql = "INSERT INTO CUST_INFO (SESSION_ID, CUST_CD, FNAME, LNAME, CUST_TP, ADDR1, ADDR2, CITY, ST, ZIP, HPHONE, BPHONE, EMAIL, BILL_FNAME, BILL_LNAME, BILL_ADDR1, BILL_ADDR2, BILL_CITY, BILL_ST, BILL_ZIP, CORP_NAME) "
                        sql = sql & "VALUES (:SESSION_ID, :CUST_CD, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL, :FNAME, :LNAME, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :CORP_NAME) "

                        With cmdDeleteItems
                            .Connection = conn2
                            .CommandText = sql
                        End With

                        cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
                        cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(txtCustCode.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":FNAME").Value = UCase(txtFName.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":CORP_NAME", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":CORP_NAME").Value = Left(UCase(txt_corp_name.Text.ToString.Trim), 30)
                        cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":LNAME").Value = UCase(txtLName.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":CUST_TP").Value = drp_cust_tp.SelectedValue.ToString.Trim
                        cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":ADDR1").Value = UCase(txtAddr.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":ADDR2").Value = UCase(txtAddr2.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":CITY").Value = UCase(City.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":ST").Value = UCase(st_label.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":ZIP").Value = UCase(Zip.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":HPHONE").Value = UCase(txtHomePhone.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":BPHONE").Value = UCase(txtBusPhone.Text.ToString.Trim)
                        cmdDeleteItems.Parameters.Add(":EMAIL", OracleType.VarChar)
                        cmdDeleteItems.Parameters(":EMAIL").Value = UCase(Email.Text.ToString.Trim)

                        cmdDeleteItems.ExecuteNonQuery()
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

            End If
            'Close Connection 
            conn.Close()
            conn2.Close()

            Session("CUST_CD") = txtCustCode.Text
            Session("CUST_LNAME") = txtLName.Text

        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        If custcd <> "NEW" Then
            'jkl
            'sql = "SELECT DES FROM CUST_WARN, CUST2CUST_WARN WHERE CUST_WARN.CUST_WARN_CD=CUST2CUST_WARN.CUST_WARN_CD AND CUST2CUST_WARN.CUST_CD='" & custcd & "'"
            sql = "SELECT DES FROM CUST_WARN, CUST2CUST_WARN WHERE CUST_WARN.CUST_WARN_CD=CUST2CUST_WARN.CUST_WARN_CD AND CUST2CUST_WARN.CUST_CD=:custCode"


            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            'Add Parameters 
            'objSql.Parameters.AddWithValue("@productid", productid)

            'jkl
            objSql.Parameters.Add(":custCode", OracleType.VarChar)
            objSql.Parameters(":custCode").Value = custcd

            Try
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read()
                    warning_list = warning_list & "- " & MyDataReader.Item("des") & vbCrLf
                Loop
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If warning_list & "" <> "" Then
                img_warn.Visible = True
                lbl_Warning.Visible = True
                lbl_Warning.Text = "PLEASE REVIEW"
                img_warn.ToolTip = warning_list
            Else
                lbl_Warning.Visible = False
                img_warn.Visible = False
            End If
        End If

    End Sub

    Protected Sub btn_Remove_Cust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Remove_Cust.Click

        If Session("CUST_CD") & "" = "" Then
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("customer.aspx?LEAD=TRUE")
            Else
                Response.Redirect("customer.aspx")
            End If
        Else
            If Request("LEAD") = "TRUE" Then
                ASPxPopupControl1.ContentUrl = "remove_customer_verification.aspx?LEAD=TRUE&session_ID=" & Session.SessionID.ToString.Trim & "&CUST_CD=" & txtCustCode.Text.ToString
                ASPxPopupControl1.ShowOnPageLoad = True
            Else
                ASPxPopupControl1.ContentUrl = "remove_customer_verification.aspx?session_ID=" & Session.SessionID.ToString.Trim & "&CUST_CD=" & txtCustCode.Text.ToString
                ASPxPopupControl1.ShowOnPageLoad = True
            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "Regular.Master"
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub Save_Customer_Info(ByVal sender As Object, ByVal e As System.EventArgs) Handles drp_cust_tp.SelectedIndexChanged, txtFName.TextChanged, txtLName.TextChanged, txtAddr.TextChanged, txtAddr2.TextChanged, City.TextChanged, State.SelectedIndexChanged, Zip.TextChanged, txtHomePhone.TextChanged, txtBusPhone.TextChanged, Email.TextChanged, txt_bill_FName.TextChanged, txt_bill_LName.TextChanged, txt_bill_Addr.TextChanged, txt_bill_Addr2.TextChanged, bill_City.TextChanged, bill_state.TextChanged, bill_Zip.TextChanged, txt_corp_name.TextChanged

        If sender.id = "Zip" Then
            Update_City_State()
        End If

        Dim sql As String
        Dim conn3 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim zip_code As Boolean = False
        Dim custcd As String
        custcd = Session("cust_cd")
        Session("CUST_LNAME") = txtLName.Text

        If sender.id = "Zip" Then zip_code = True
        If sender.id = "bill_Zip" Then zip_code = True

        If ConfigurationManager.AppSettings("validate_zip").ToString = "Y" And zip_code = True Then

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn3.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT ZIP_CD FROM ZIP WHERE ZIP_CD = :zipCode"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn3)
            objsql.Parameters.Add(":zipCode", OracleType.VarChar)
            objsql.Parameters(":zipCode").Value = UCase(Zip.Text.ToString.Trim)

            Try
                conn3.Open()
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    If MyDataReader.Item("ZIP_CD") & "" = "" Then
                        lbl_Error.Text = "<br /><font color=red><b>Changes NOT saved. Invalid Ship to Zip Code</b></font><br />"
                        Exit Sub
                    Else
                        Session("zip_cd") = UCase(Zip.Text)
                        lbl_Error.Text = ""
                    End If
                Else
                    lbl_Error.Text = "<br /><font color=red><b>Changes NOT saved. Invalid Ship to Zip Code</b></font><br />"
                    Exit Sub
                End If
                MyDataReader.Close()
            Catch
                conn3.Close()
                Throw
            End Try

            If Session("CUST_CD") = "NEW" And String.IsNullOrEmpty(bill_Zip.Text) Then
                txt_bill_FName.Text = txtFName.Text
                txt_bill_LName.Text = txtLName.Text
                txt_bill_Addr.Text = txtAddr.Text
                txt_bill_Addr2.Text = txtAddr2.Text
                bill_City.Text = City.Text
                bill_state.Text = State.Text
                bill_Zip.Text = Zip.Text
            End If

            sql = "SELECT ZIP_CD FROM ZIP WHERE ZIP_CD = :zipCode"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn3)
            objsql.Parameters.Add(":zipCode", OracleType.VarChar)
            objsql.Parameters(":zipCode").Value = UCase(bill_Zip.Text.ToString.Trim)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    If MyDataReader.Item("ZIP_CD") & "" = "" Then
                        lbl_Error.Text = "<br /><font color=red><b>Changes NOT saved. Invalid Bill to Zip Code</b></font><br />"
                        Exit Sub
                    Else
                        lbl_Error.Text = ""
                    End If
                Else
                    lbl_Error.Text = "<br /><font color=red><b>Changes NOT saved. Invalid Bill to Zip Code</b></font><br />"
                    Exit Sub
                End If
                MyDataReader.Close()
            Catch
                conn3.Close()
                Throw
            End Try
            conn3.Close()

            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn2.Open()
            Dim objsql2 As OracleCommand
            Dim MyDataReader2 As OracleDataReader

            'jkl
            'sql = "SELECT ZIP FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & custcd & "'"
            sql = "SELECT ZIP FROM CUST_INFO WHERE Session_ID=:sessionID AND CUST_CD=:custCode"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            'jkl
            objsql2.Parameters.Add(":sessionID", OracleType.VarChar)
            objsql2.Parameters(":sessionID").Value = Session.SessionID.ToString.Trim
            objsql2.Parameters.Add(":custCode", OracleType.VarChar)
            objsql2.Parameters(":custCode").Value = custcd

            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDataReader2.Read()) Then
                    If MyDataReader2.Item("ZIP") & "" <> "" Then
                        If MyDataReader2.Item("ZIP").ToString.Trim <> Zip.Text.ToString.Trim Then
                            Session("TAX_RATE") = System.DBNull.Value
                        End If
                    ElseIf Zip.Text.ToString.Trim & "" <> "" Then
                        Session("TAX_RATE") = System.DBNull.Value
                    End If
                End If
                MyDataReader2.Close()
            Catch
                conn2.Close()
                Throw
            End Try
            conn2.Close()
        End If

        If chk_billing.Checked = True Then
            txt_bill_FName.Text = txtFName.Text
            txt_bill_LName.Text = txtLName.Text
            txt_bill_Addr.Text = txtAddr.Text
            txt_bill_Addr2.Text = txtAddr2.Text
            bill_City.Text = City.Text
            bill_state.Text = State.Text
            bill_Zip.Text = Zip.Text
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()
        sql = "UPDATE CUST_INFO SET "
        sql = sql & "CORP_NAME=:CORP_NAME,"
        sql = sql & "FNAME=:FNAME,"
        sql = sql & "LNAME=:LNAME,"
        sql = sql & "CUST_TP=:CUST_TP,"
        sql = sql & "ADDR1=:ADDR1,"
        sql = sql & "ADDR2=:ADDR2,"
        sql = sql & "CITY=:CITY,"
        sql = sql & "ST=:ST, "
        sql = sql & "ZIP=:ZIP,"
        sql = sql & "HPHONE=:HPHONE,"
        sql = sql & "BPHONE=:BPHONE,"
        sql = sql & "EMAIL=:EMAIL,"
        sql = sql & "BILL_FNAME=:B_FNAME,"
        sql = sql & "BILL_LNAME=:B_LNAME,"
        sql = sql & "BILL_ADDR1=:B_ADDR1,"
        sql = sql & "BILL_ADDR2=:B_ADDR2,"
        sql = sql & "BILL_CITY=:B_CITY,"
        sql = sql & "BILL_ST=:B_ST,"
        sql = sql & "BILL_ZIP=:B_ZIP "
        sql = sql & "WHERE SESSION_ID=:SESSION_ID "
        sql = sql & "AND CUST_CD=:CUST_CD"

        With cmdDeleteItems
            .Connection = conn
            .CommandText = sql
        End With

        cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
        cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
        cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(custcd)
        cmdDeleteItems.Parameters.Add(":CORP_NAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CORP_NAME").Value = Left(UCase(txt_corp_name.Text.ToString.Trim), 30)
        cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":FNAME").Value = UCase(txtFName.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":LNAME").Value = UCase(txtLName.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CUST_TP").Value = drp_cust_tp.SelectedValue.ToString.Trim
        cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ADDR1").Value = UCase(txtAddr.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ADDR2").Value = UCase(txtAddr2.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CITY").Value = UCase(City.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ST").Value = UCase(State.SelectedValue.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ZIP").Value = UCase(Zip.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
        cmdDeleteItems.Parameters(":HPHONE").Value = UCase(txtHomePhone.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
        cmdDeleteItems.Parameters(":BPHONE").Value = UCase(txtBusPhone.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":EMAIL", OracleType.VarChar)
        cmdDeleteItems.Parameters(":EMAIL").Value = UCase(Email.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_FNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_FNAME").Value = UCase(txt_bill_FName.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_LNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_LNAME").Value = UCase(txt_bill_LName.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_ADDR1", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_ADDR1").Value = UCase(txt_bill_Addr.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_ADDR2", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_ADDR2").Value = UCase(txt_bill_Addr2.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_CITY", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_CITY").Value = UCase(bill_City.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_ST", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_ST").Value = UCase(bill_state.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":B_ZIP", OracleType.VarChar)
        cmdDeleteItems.Parameters(":B_ZIP").Value = UCase(bill_Zip.Text.ToString.Trim)

        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()

        If chk_tax_exempt.Checked = False Then
            If IsNothing(Session("TAX_RATE")) Then
                determine_tax("")
            ElseIf Session("TAX_RATE").ToString & "" = "" Then
                determine_tax("")
            End If
        End If

        Select Case LCase(sender.id)
            Case "txtcustcode"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + drp_cust_tp.ClientID + "').focus();$get('" + drp_cust_tp.ClientID + "').select();", True)
            Case "drp_cust_tp"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txt_corp_name.ClientID + "').focus();$get('" + txt_corp_name.ClientID + "').select();", True)
            Case "txt_corp_name"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtFName.ClientID + "').focus();$get('" + txtFName.ClientID + "').select();", True)
            Case "txtfname"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtLName.ClientID + "').focus();$get('" + txtLName.ClientID + "').select();", True)
            Case "txtlname"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtAddr.ClientID + "').focus();$get('" + txtAddr.ClientID + "').select();", True)
            Case "txtaddr"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtAddr2.ClientID + "').focus();$get('" + txtAddr2.ClientID + "').select();", True)
            Case "txtaddr2"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + City.ClientID + "').focus();$get('" + City.ClientID + "').select();", True)
            Case "city"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + State.ClientID + "').focus();$get('" + State.ClientID + "').select();", True)
            Case "State"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + Zip.ClientID + "').focus();$get('" + Zip.ClientID + "').select();", True)
            Case "zip"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtHomePhone.ClientID + "').focus();$get('" + txtHomePhone.ClientID + "').select();", True)
            Case "txthomephone"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtBusPhone.ClientID + "').focus();$get('" + txtBusPhone.ClientID + "').select();", True)
            Case "txtbusphone"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + Email.ClientID + "').focus();$get('" + Email.ClientID + "').select();", True)
            Case "email"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtCustCode.ClientID + "').focus();$get('" + txtCustCode.ClientID + "').select();", True)
            Case "txt_bill_fname"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txt_bill_LName.ClientID + "').focus();$get('" + txt_bill_LName.ClientID + "').select();", True)
            Case "txt_bill_lname"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txt_bill_Addr.ClientID + "').focus();$get('" + txt_bill_Addr.ClientID + "').select();", True)
            Case "txt_bill_addr"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txt_bill_Addr2.ClientID + "').focus();$get('" + txt_bill_Addr2.ClientID + "').select();", True)
            Case "txt_bill_addr2"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + bill_City.ClientID + "').focus();$get('" + bill_City.ClientID + "').select();", True)
            Case "bill_city"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + bill_state.ClientID + "').focus();$get('" + bill_state.ClientID + "').select();", True)
            Case "bill_state"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + bill_Zip.ClientID + "').focus();$get('" + bill_Zip.ClientID + "').select();", True)
            Case "bill_zip"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JSScript", "ShowBilling();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtCustCode.ClientID + "').focus();$get('" + txtCustCode.ClientID + "').select();", True)
        End Select

    End Sub

    Protected Sub chk_billing_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_billing.CheckedChanged

        If chk_billing.Checked = True Then
            txt_bill_FName.Text = txtFName.Text
            txt_bill_LName.Text = txtLName.Text
            txt_bill_Addr.Text = txtAddr.Text
            txt_bill_Addr2.Text = txtAddr2.Text
            bill_City.Text = City.Text
            bill_state.Text = State.Text
            bill_Zip.Text = Zip.Text
            txt_bill_FName.Enabled = False
            txt_bill_LName.Enabled = False
            txt_bill_Addr.Enabled = False
            txt_bill_Addr2.Enabled = False
            bill_City.Enabled = False
            bill_state.Enabled = False
            bill_Zip.Enabled = False

            Dim custcd As String
            custcd = Session("cust_cd")
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim sql As String

            conn.Open()
            sql = "UPDATE CUST_INFO SET "
            sql = sql & "FNAME=:FNAME,"
            sql = sql & "LNAME=:LNAME,"
            sql = sql & "CUST_TP=:CUST_TP,"
            sql = sql & "ADDR1=:ADDR1,"
            sql = sql & "ADDR2=:ADDR2,"
            sql = sql & "CITY=:CITY,"
            sql = sql & "ST=:ST, "
            sql = sql & "ZIP=:ZIP,"
            sql = sql & "HPHONE=:HPHONE,"
            sql = sql & "BPHONE=:BPHONE,"
            sql = sql & "EMAIL=:EMAIL,"
            sql = sql & "BILL_FNAME=:B_FNAME,"
            sql = sql & "BILL_LNAME=:B_LNAME,"
            sql = sql & "BILL_ADDR1=:B_ADDR1,"
            sql = sql & "BILL_ADDR2=:B_ADDR2,"
            sql = sql & "BILL_CITY=:B_CITY,"
            sql = sql & "BILL_ST=:B_ST,"
            sql = sql & "BILL_ZIP=:B_ZIP "
            sql = sql & "WHERE SESSION_ID=:SESSION_ID "
            sql = sql & "AND CUST_CD=:CUST_CD"

            With cmdDeleteItems
                .Connection = conn
                .CommandText = sql
            End With

            cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
            cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
            cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(custcd)
            cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":FNAME").Value = UCase(txtFName.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":LNAME").Value = UCase(txtLName.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CUST_TP").Value = drp_cust_tp.SelectedValue.ToString.Trim
            cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ADDR1").Value = UCase(txtAddr.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ADDR2").Value = UCase(txtAddr2.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
            cmdDeleteItems.Parameters(":CITY").Value = UCase(City.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ST").Value = UCase(st_label.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
            cmdDeleteItems.Parameters(":ZIP").Value = UCase(Zip.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
            cmdDeleteItems.Parameters(":HPHONE").Value = UCase(txtHomePhone.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
            cmdDeleteItems.Parameters(":BPHONE").Value = UCase(txtBusPhone.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":EMAIL", OracleType.VarChar)
            cmdDeleteItems.Parameters(":EMAIL").Value = UCase(Email.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_FNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_FNAME").Value = UCase(txt_bill_FName.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_LNAME", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_LNAME").Value = UCase(txt_bill_LName.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_ADDR1", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_ADDR1").Value = UCase(txt_bill_Addr.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_ADDR2", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_ADDR2").Value = UCase(txt_bill_Addr2.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_CITY", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_CITY").Value = UCase(bill_City.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_ST", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_ST").Value = UCase(bill_state.Text.ToString.Trim)
            cmdDeleteItems.Parameters.Add(":B_ZIP", OracleType.VarChar)
            cmdDeleteItems.Parameters(":B_ZIP").Value = UCase(bill_Zip.Text.ToString.Trim)

            cmdDeleteItems.ExecuteNonQuery()

            conn.Close()
        Else
            txt_bill_FName.Enabled = True
            txt_bill_LName.Enabled = True
            txt_bill_Addr.Enabled = True
            txt_bill_Addr2.Enabled = True
            bill_City.Enabled = True
            bill_state.Enabled = True
            bill_Zip.Enabled = True
        End If

    End Sub

    Protected Sub chk_tax_exempt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_tax_exempt.CheckedChanged

        If chk_tax_exempt.Checked = True Then
            PopulateTaxExempt()
            cbo_tax_exempt.Visible = "True"
            txt_tax_exempt_id.Visible = "true"
            Session("no_tax") = "True"

        Else
            Session("no_tax") = ""
            Session("tet_cd") = ""
            Session("exempt_id") = ""
            cbo_tax_exempt.Visible = "False"
            txt_tax_exempt_id.Visible = "False"
            determine_tax("")
        End If

    End Sub

    Protected Sub EmailCheck_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EmailCheck.CheckedChanged
        'May 22 add email flag to session IT Req 2919
        If EmailCheck.Checked = True Then
            Session("email_checked") = "Y"
        End If
    End Sub

    Private Sub PopulateTaxExempt()

        Dim ds As DataSet = theSalesBiz.GetTaxExemptCodes()
        With cbo_tax_exempt
            .DataSource = ds
            .DataValueField = "tet_cd"
            .DataTextField = "full_desc"
            .DataBind()
            If Session("tet_cd") & "" <> "" Then
                .SelectedValue = Session("tet_cd")
            End If
        End With
        ' Daniela french
        'cbo_tax_exempt.Items.Insert(0, "Please Select A Tax Exempt Code")
        'cbo_tax_exempt.Items.FindByText("Please Select A Tax Exempt Code").Value = ""
        cbo_tax_exempt.Items.Insert(0, Resources.LibResources.Label414)
        cbo_tax_exempt.Items.FindByText(Resources.LibResources.Label414).Value = ""
        If Session("tet_cd") & "" = "" And cbo_tax_exempt.SelectedValue & "" = "" Then
            cbo_tax_exempt.SelectedIndex = 0
        End If

        If Session("tet_cd") & "" = "" And cbo_tax_exempt.SelectedIndex <> 0 Then
            Session("tet_cd") = cbo_tax_exempt.SelectedValue
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim SQL As String = "SELECT TET_CD, TET_ID# FROM CUST WHERE CUST_CD=:custCode"

        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objsql.Parameters.Add(":custCode", OracleType.VarChar)
        objsql.Parameters(":custCode").Value = UCase(Session("cust_cd"))

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If MyDataReader.Read() Then
                If MyDataReader.Item("TET_ID#").ToString & "" <> "" Then
                    Session("exempt_id") = MyDataReader.Item("TET_ID#").ToString
                    txt_tax_exempt_id.Text = MyDataReader.Item("TET_ID#").ToString
                End If
                If MyDataReader.Item("TET_CD").ToString & "" <> "" Then
                    Session("tet_cd") = MyDataReader.Item("TET_CD").ToString
                    cbo_tax_exempt.SelectedValue = MyDataReader.Item("TET_CD").ToString
                End If
            End If
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub txt_tax_exempt_id_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_tax_exempt_id.TextChanged

        If txt_tax_exempt_id.Text & "" <> "" Then
            Session("exempt_id") = txt_tax_exempt_id.Text
        Else
            Session("exempt_id") = System.DBNull.Value
        End If

    End Sub

    Protected Sub cbo_tax_exempt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_tax_exempt.SelectedIndexChanged

        If cbo_tax_exempt.SelectedIndex <> 0 Then
            Session("tet_cd") = cbo_tax_exempt.SelectedValue
            Session("TAX_RATE") = System.DBNull.Value
            Session("TAX_CD") = System.DBNull.Value
        Else
            Session("tet_cd") = ""
            If IsNothing(Session("TAX_RATE")) Then
                determine_tax("")
            ElseIf Session("TAX_RATE").ToString & "" = "" Then
                determine_tax("")
            End If
        End If

    End Sub

    'Protected Sub Zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Zip.TextChanged

    '    Update_City_State()

    'End Sub

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(Zip.Text) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            End If

            conn.Open()

            'jkl
            'sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & UCase(Zip.Text) & "'"
            sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD=:zipCode"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            'jkl
            objsql.Parameters.Add(":zipCode", OracleType.VarChar)
            objsql.Parameters(":zipCode").Value = UCase(Zip.Text)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    Dim custcd As String = Session("cust_cd")

                    Session("cust_city") = MyDataReader.Item("CITY").ToString
                    Session("cust_state") = MyDataReader.Item("ST_CD").ToString
                    City.Text = MyDataReader.Item("CITY").ToString
                    State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                    st_label.Text = MyDataReader.Item("ST_CD").ToString
                    Session("zip_cd") = UCase(Zip.Text)

                    conn.Close()
                    conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    conn.Open()
                    Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


                    sql = "UPDATE CUST_INFO SET "
                    sql = sql & "FNAME=:FNAME,"
                    sql = sql & "LNAME=:LNAME,"
                    sql = sql & "CUST_TP=:CUST_TP,"
                    sql = sql & "ADDR1=:ADDR1,"
                    sql = sql & "ADDR2=:ADDR2,"
                    sql = sql & "CITY=:CITY,"
                    sql = sql & "ST=:ST, "
                    sql = sql & "ZIP=:ZIP,"
                    sql = sql & "HPHONE=:HPHONE,"
                    sql = sql & "BPHONE=:BPHONE,"
                    sql = sql & "EMAIL=:EMAIL,"
                    sql = sql & "BILL_FNAME=:B_FNAME,"
                    sql = sql & "BILL_LNAME=:B_LNAME,"
                    sql = sql & "BILL_ADDR1=:B_ADDR1,"
                    sql = sql & "BILL_ADDR2=:B_ADDR2,"
                    sql = sql & "BILL_CITY=:B_CITY,"
                    sql = sql & "BILL_ST=:B_ST,"
                    sql = sql & "BILL_ZIP=:B_ZIP "
                    sql = sql & "WHERE SESSION_ID=:SESSION_ID "
                    sql = sql & "AND CUST_CD=:CUST_CD"

                    With cmdDeleteItems
                        .Connection = conn
                        .CommandText = sql
                    End With

                    cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
                    cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(custcd)
                    cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":FNAME").Value = UCase(txtFName.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":LNAME").Value = UCase(txtLName.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":CUST_TP").Value = drp_cust_tp.SelectedValue.ToString.Trim
                    cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":ADDR1").Value = UCase(txtAddr.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":ADDR2").Value = UCase(txtAddr2.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":CITY").Value = UCase(City.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":ST").Value = UCase(st_label.Text)
                    cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":ZIP").Value = UCase(Zip.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":HPHONE").Value = UCase(txtHomePhone.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":BPHONE").Value = UCase(txtBusPhone.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":EMAIL", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":EMAIL").Value = UCase(Email.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_FNAME", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_FNAME").Value = UCase(txt_bill_FName.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_LNAME", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_LNAME").Value = UCase(txt_bill_LName.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_ADDR1", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_ADDR1").Value = UCase(txt_bill_Addr.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_ADDR2", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_ADDR2").Value = UCase(txt_bill_Addr2.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_CITY", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_CITY").Value = UCase(bill_City.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_ST", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_ST").Value = UCase(bill_state.Text.ToString.Trim)
                    cmdDeleteItems.Parameters.Add(":B_ZIP", OracleType.VarChar)
                    cmdDeleteItems.Parameters(":B_ZIP").Value = UCase(bill_Zip.Text.ToString.Trim)

                    cmdDeleteItems.ExecuteNonQuery()
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()

            If chk_tax_exempt.Checked = False Then
                determine_tax(Zip.Text)
                calculate_total(Page.Master)
                'If IsNothing(Session("TAX_RATE")) Then
                '    determine_tax(Zip.Text)
                '    calculate_total(Page.Master)
                'ElseIf Session("TAX_RATE").ToString & "" = "" Then
                '    determine_tax(Zip.Text)
                '    calculate_total(Page.Master)
                'End If
            End If
            txtHomePhone.Focus()

            'MM-5922
            If AppConstants.DELIVERY.Equals(SessVar.puDel) And theInvBiz.RequireZoneCd(SessVar.puDel, SessVar.isArsEnabled, SessVar.ordTpCd) Then
                SessVar.zipCode4Zone = UCase(Zip.Text)
                SessVar.zoneCd = Nothing
            End If
        End If

    End Sub

    Public Sub convert_relationship()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        'Open Connection 
        conn.Open()
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim emp_cd, fname, lname, store_cd, del_doc_num, cust_cd, slsp, IPAD, zip_cd, co_cd As String
        Dim dictCurrentPrice As Dictionary(Of String, Double) = Nothing
        If Not IsNothing(Session("dictCurrentPrice")) Then
            dictCurrentPrice = CType(Session("dictCurrentPrice"), Dictionary(Of String, Double))
        End If
        del_doc_num = Request("del_doc_num")
        emp_cd = Session("EMP_CD")
        slsp = Session("slsp1")
        fname = Session("EMP_FNAME")
        IPAD = Session("IPAD")
        lname = Session("EMP_LNAME")
        store_cd = Session("HOME_STORE_CD")
        cust_cd = Session("cust_cd")
        co_cd = Session("CO_CD")
        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from payment where sessionid=:SESSIONID"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
            cmdDeleteItems.Dispose()
            cmdDeleteItems = Nothing
            cmdDeleteItems = DisposablesManager.BuildOracleCommand

            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from temp_itm where session_id=:SESSIONID"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
            cmdDeleteItems.Dispose()
            cmdDeleteItems = Nothing
            cmdDeleteItems = DisposablesManager.BuildOracleCommand

            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from cust_info where session_id=:SESSIONID"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
        End If
        ' Daniela save clientip and emp_init, super user
        Dim client_IP As String = Session("clientip")
        Dim empInit As String = Session("emp_init")
        Dim supUser As String = Session("str_sup_user_flag")
        Dim coGrpCd As String = Session("str_co_grp_cd")
        Dim userType As String = Session("USER_TYPE")
        Session.Clear()
        Session("clientip") = client_IP
        Session("emp_init") = empInit
        Session("str_sup_user_flag") = supUser ' Daniela
        Session("str_co_grp_cd") = coGrpCd ' Daniela
        Session("USER_TYPE") = userType ' Daniela
        Session("IPAD") = IPAD
        Session("rel_check") = "TRUE"
        Session("CO_CD") = co_cd

        Dim objsql As OracleCommand
        Dim linesDatRdr As OracleDataReader
        Dim sql As String
        Dim objsql2 As OracleCommand
        Dim itmDatRdr As OracleDataReader
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()

        sql = "SELECT * FROM RELATIONSHIP where REL_NO=:delNum"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":delNum", OracleType.VarChar)
        objsql.Parameters(":delNum").Value = del_doc_num

        Try
            linesDatRdr = DisposablesManager.BuildOracleDataReader(objsql)

            Do While linesDatRdr.Read()

                Session("EMP_CD") = emp_cd
                Session("EMP_FNAME") = fname
                Session("EMP_LNAME") = lname
                Session("HOME_STORE_CD") = store_cd
                Dim store As StoreData = theSalesBiz.GetStoreInfo(store_cd)
                If store.puStoreCd.isEmpty Then
                    Session("pd_store_cd") = store_cd
                Else
                    Session("pd_store_cd") = store.puStoreCd
                End If

                Session("store_cd") = store_cd

                If slsp = linesDatRdr.Item("SLSP1").ToString Then
                    Session("slsp1") = slsp
                    Session("pct_1") = "100"
                Else
                    Session("slsp1") = linesDatRdr.Item("SLSP1").ToString
                    Session("pct_1") = "50"
                    Session("slsp2") = slsp
                    Session("pct_2") = "50"
                End If
                zip_cd = linesDatRdr.Item("ZIP").ToString
                Session("PD") = linesDatRdr.Item("P_D").ToString
                Session("LNAME") = linesDatRdr.Item("LNAME").ToString
                Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL
                Session("tran_dt") = FormatDateTime(Today.Date.ToString, 2)
                Session("cust_cd") = linesDatRdr.Item("CUST_CD").ToString
                Session("cust_lname") = linesDatRdr.Item("LNAME").ToString
                Session("scomments") = linesDatRdr.Item("COMMENTS").ToString
                Session("del_chg") = linesDatRdr.Item("DEL_CHG").ToString
                Session("tax_rate") = linesDatRdr.Item("TAX_RATE").ToString
                Session("tax_cd") = linesDatRdr.Item("TAX_CD").ToString
                '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                Session("MANUAL_TAX_CD") = Nothing

                Session("grand_total") = linesDatRdr.Item("PROSPECT_TOTAL").ToString
                'Session("sub_total") = CDbl(lblTotal.Text.ToString) - (CDbl(lblTax.Text.ToString) + CDbl(lblDelivery.Text.ToString))
                Session("Converted_REL_NO") = del_doc_num
            Loop
            linesDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        'updates the comments with the Salesperson Notes found in the relationship
        Session("scomments") = GetRelationshipComments(del_doc_num)

        sql = "SELECT rl.*,rl.RET_PRC + NVL(rl.DISC_AMT,0) as RET_PRC_DISC FROM RELATIONSHIP_LINES rl where rl.REL_NO=:delNum order by rl.line"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":delNum", OracleType.VarChar)
        objsql.Parameters(":delNum").Value = del_doc_num

        Dim conv_qty As Double = 1
        Dim orig_sql As String = ""
        Dim z As Integer = 1
        Dim ln1RowSeqNum As Integer = 0   ' this is the row_id number column from temp_itm
        Dim ln1RowId As String = ""       ' this is the actual Oracle rowid
        Dim RelanPriceChange As New List(Of RelationshipLineDetails)
        Dim retprc As Double
        Dim pkgParentLine As String = String.Empty
        Dim insertSql As New StringBuilder
        Dim ln2disc As DataTable
        Dim strWhereClause As String = String.Empty
        Dim dtLnsFiltered As DataRow()
        Dim needTorecalDiscount As Boolean = False
        Try
            linesDatRdr = DisposablesManager.BuildOracleDataReader(objsql)

            ln2disc = theSalesBiz.GetDiscountsApplied(del_doc_num, SessVar.discCd, False, True) ' FOR SOM POPULATE RESULTS FROM SO_LN OR SO_LN2DISC
            ln2disc.Columns.Add("ROW_ID")
            Do While linesDatRdr.Read()
                retprc = 0.0
                If Not IsNothing(dictCurrentPrice) AndAlso dictCurrentPrice.Count > 0 AndAlso dictCurrentPrice.ContainsKey(linesDatRdr.Item("ITM_CD").ToString) Then
                    retprc = dictCurrentPrice(linesDatRdr.Item("ITM_CD").ToString)
                    needTorecalDiscount = True
                    ViewState("needTorecalDiscount") = needTorecalDiscount
                End If
                conv_qty = 1
                z = 1
                'jkl do we need tobind this too
                Session("itemid") = "NONEENTERED"
                insertSql.Length = 0
                insertSql.Append("INSERT INTO TEMP_ITM (ITM_CD, QTY, RET_PRC, ORIG_PRC, MANUAL_PRC, TREATED, SESSION_ID, DISC_AMT ")
                If IsDBNull(linesDatRdr.Item("PACKAGE_PARENT")) Then
                    pkgParentLine = String.Empty
                    insertSql.Append(")")
                ElseIf Not String.IsNullOrEmpty(pkgParentLine) Then
                    insertSql.Append(", PACKAGE_PARENT)")
                Else
                    insertSql.Append(")")
                End If

                insertSql.Append(" VALUES('" & linesDatRdr.Item("ITM_CD").ToString & "'")
                insertSql.Append("," & linesDatRdr.Item("QTY").ToString & ",:retPrc , :origPrc" & ", :manualPrc" & ",'")
                insertSql.Append(linesDatRdr.Item("TREATED").ToString & "','" & Session.SessionID.ToString() & "',:discAmt")

                If Not IsDBNull(linesDatRdr.Item("PACKAGE_PARENT")) AndAlso Not String.IsNullOrEmpty(pkgParentLine) Then
                    insertSql.Append(",'" & pkgParentLine & "') RETURNING ROW_ID, rowid INTO :INSERTED_ROWID, :row_id ")
                Else
                    insertSql.Append(")RETURNING ROW_ID, rowid INTO :INSERTED_ROWID, :row_id")
                End If

                objsql = DisposablesManager.BuildOracleCommand(insertSql.ToString, conn)
                objsql.Parameters.Add(":retPrc", OracleType.Number).Direction = ParameterDirection.Input
                objsql.Parameters.Add(":origPrc", OracleType.Number).Direction = ParameterDirection.Input
                objsql.Parameters.Add(":manualPrc", OracleType.Number).Direction = ParameterDirection.Input
                objsql.Parameters.Add(":discAmt", OracleType.Number).Direction = ParameterDirection.Input

                If retprc <> 0.0 AndAlso retprc <> linesDatRdr.Item("RET_PRC_DISC") Then
                    objsql.Parameters(":retPrc").Value = retprc
                    objsql.Parameters(":manualPrc").Value = retprc
                    objsql.Parameters(":origPrc").Value = retprc
                    objsql.Parameters(":discAmt").Value = 0.0
                Else
                    objsql.Parameters(":retPrc").Value = linesDatRdr.Item("RET_PRC")
                    objsql.Parameters(":manualPrc").Value = linesDatRdr.Item("RET_PRC_DISC")
                    objsql.Parameters(":origPrc").Value = linesDatRdr.Item("RET_PRC")
                    objsql.Parameters(":discAmt").Value = linesDatRdr.Item("DISC_AMT")
                End If

                objsql.Parameters.Add(":INSERTED_ROWID", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                objsql.Parameters(":INSERTED_ROWID").Value = System.DBNull.Value
                objsql.Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                objsql.Parameters(":row_id").Value = System.DBNull.Value
                objsql.ExecuteNonQuery()
                ln1RowId = objsql.Parameters(":row_id").Value.ToString
                'Added the same if block because rowid will be initialized after the insert
                If retprc <> 0.0 AndAlso retprc <> linesDatRdr.Item("RET_PRC_DISC") Then
                    Dim RelationshipLine As New RelationshipLineDetails()
                    RelationshipLine.ItemCD = linesDatRdr.Item("ITM_CD").ToString
                    RelationshipLine.Price = linesDatRdr.Item("RET_PRC")
                    RelationshipLine.RowId = Convert.ToInt64(objsql.Parameters(":INSERTED_ROWID").Value)
                    RelanPriceChange.Add(RelationshipLine)
                End If
                If Not ln2disc Is Nothing Then
                    strWhereClause = ("LNSEQ_NUM = '" & linesDatRdr.Item("LINE") & "' AND ITM_CD = '" & linesDatRdr.Item("ITM_CD") & "'")
                    If ln2disc.Select(strWhereClause).Length > 0 Then
                        dtLnsFiltered = ln2disc.Select(strWhereClause)
                        For Each discln As DataRow In dtLnsFiltered
                            If discln.Item("LNSEQ_NUM") = linesDatRdr.Item("LINE") AndAlso discln.Item("ITM_CD") = linesDatRdr.Item("ITM_CD") Then
                                discln("ROW_ID") = Convert.ToInt64(objsql.Parameters(":INSERTED_ROWID").Value)
                            End If
                        Next
                    End If
                End If
                If String.Equals(linesDatRdr.Item("ITM_TP_CD").ToString.ToUpper, "PKG") Then
                    pkgParentLine = objsql.Parameters(":INSERTED_ROWID").Value.ToString
                End If
                objsql.Parameters.Clear()

                ' extract the TEMP_ITM.ROW_ID (not the Oracle rowid) in case need to link warranty
                If linesDatRdr("LINE") < 2 Then

                    ln1RowSeqNum = SalesUtils.GetTempItmRowSeqNum(ln1RowId)
                End If

                If IsNumeric(linesDatRdr.Item("QTY").ToString) Then
                    conv_qty = CDbl(linesDatRdr.Item("QTY").ToString)
                End If

                sql = "SELECT * FROM ITM where ITM_CD=:itemCode"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

                objsql2.Parameters.Add(":itemCode", OracleType.VarChar)
                objsql2.Parameters(":itemCode").Value = linesDatRdr.Item("ITM_CD").ToString

                Try
                    'Execute DataReader 
                    itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

                    'Store Values in String Variables 
                    Do While itmDatRdr.Read()
                        If InventoryUtils.IsSerialType(itmDatRdr.Item("SERIAL_TP").ToString) Then
                            If conv_qty > 1 Then
                                For z = 1 To conv_qty - 1
                                    objsql = DisposablesManager.BuildOracleCommand(orig_sql, conn)
                                    objsql.ExecuteNonQuery()
                                Next
                            End If
                        End If
                        sql = "UPDATE TEMP_ITM SET "
                        sql = sql & "VE_CD ='" & itmDatRdr.Item("VE_CD").ToString & "', "
                        If "Y".Equals(itmDatRdr.Item("warrantable").ToString) Then
                            sql = sql & "warrantable ='" & itmDatRdr.Item("warrantable").ToString & "', "
                        End If

                        sql = sql & "warr_itm_link = '" & linesDatRdr.Item("warr_itm_link").ToString & "', "
                        If (Not IsNothing(linesDatRdr.Item("warr_row_link"))) AndAlso
                            linesDatRdr.Item("warr_row_link").ToString.isNotEmpty AndAlso
                            IsNumeric(linesDatRdr.Item("warr_row_link")) Then
                            sql = sql & "warr_row_link = " & (linesDatRdr.Item("warr_row_link").ToString + ln1RowSeqNum - 1) & ", "
                        End If

                        Dim itm_prc As String = ""
                        Dim PrcArray As Array
                        Dim Calc_Ret As Double

                        itm_prc = TaxUtils.ComputeLineTax(linesDatRdr.Item("ITM_CD").ToString, linesDatRdr.Item("RET_PRC").ToString, 1, Session("tax_cd"), Session("store_cd"), Session("PD"), "")
                        PrcArray = Split(itm_prc, ";")
                        If IsNumeric(PrcArray(1)) Then
                            Calc_Ret = PrcArray(1)
                        Else
                            Calc_Ret = 0
                        End If

                        If Session("TAX_CD") & "" <> "" Then
                            sql = sql & "TAX_CD ='" & Session("TAX_CD") & "', "
                        End If
                        If Session("TAX_RATE") & "" <> "" Then
                            sql = sql & "TAX_PCT =" & Session("TAX_RATE") & ", "
                        End If
                        If Calc_Ret > 0 Then
                            sql = sql & "TAXABLE_AMT =" & linesDatRdr.Item("RET_PRC").ToString & ", "
                        End If
                        sql = sql & "TAX_AMT =ROUND(" & Calc_Ret & "," & TaxUtils.Tax_Constants.taxPrecision & "), "
                        If IsNumeric(itmDatRdr.Item("POINT_SIZE").ToString) Then
                            sql = sql & "DEL_PTS =" & itmDatRdr.Item("POINT_SIZE").ToString & ", "
                        End If
                        sql = sql & "COMM_CD ='" & itmDatRdr.Item("COMM_CD").ToString & "', "
                        If IsNumeric(itmDatRdr.Item("SPIFF").ToString) Then
                            sql = sql & "SPIFF=" & itmDatRdr.Item("SPIFF").ToString & ", "
                        End If
                        sql = sql & "MNR_CD ='" & itmDatRdr.Item("MNR_CD").ToString & "', "
                        sql = sql & "CAT_CD ='" & itmDatRdr.Item("CAT_CD").ToString & "', "
                        sql = sql & "VSN ='" & Replace(itmDatRdr.Item("VSN").ToString, "'", "''") & "', "
                        sql = sql & "DES ='" & Replace(itmDatRdr.Item("DES").ToString, "'", "''") & "', "
                        sql = sql & "ITM_TP_CD ='" & itmDatRdr.Item("ITM_TP_CD").ToString & "', "
                        sql = sql & "TREATABLE ='" & itmDatRdr.Item("TREATABLE").ToString & "', "
                        sql = sql & "SLSP1='" & Session("slsp1") & "', "
                        sql = sql & "BULK_TP_ITM ='" & itmDatRdr.Item("BULK_TP_ITM").ToString & "', "
                        sql = sql & "SERIAL_TP ='" & itmDatRdr.Item("SERIAL_TP").ToString & "', "
                        If IsNumeric(Session("pct_1")) Then
                            sql = sql & "SLSP1_PCT=" & Session("pct_1") & " "
                        Else
                            sql = sql & "SLSP1_PCT=100 "
                        End If
                        If InventoryUtils.IsSerialType(itmDatRdr.Item("SERIAL_TP").ToString) Then
                            sql = sql & ", QTY=1 "
                        End If
                        sql = sql & "WHERE SESSION_ID='" & Session.SessionID.ToString() & "' "
                        sql = sql & "AND ITM_CD='" & linesDatRdr.Item("ITM_CD").ToString & "'"
                        sql = sql & "AND VSN IS NULL"
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()
                    Loop
                    itmDatRdr.Close()
                Catch ex As Exception
                    conn.Close()
                    conn2.Close()
                    Throw
                End Try

            Loop
            linesDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        '''''*****************************************************************
        'Insert into MULTI_DISC table
        If ln2disc.Rows.Count > 0 Then
            theSalesBiz.InserttoMultiDisc(Session.SessionID.ToString().Trim, ln2disc)
        End If
        ''''*****************************************************************
        ' TODO - WARNING WARNING - if multiple sessions in here at the same time, results are unpredictable but it wouldn't make sense except in a test environment
        sql = "UPDATE CUST_INFO SET SESSION_ID=:sessionID WHERE CUST_CD=:custCode"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql.Parameters.Add(":sessionID", OracleType.VarChar)
        objsql.Parameters(":sessionID").Value = Session.SessionID.ToString
        objsql.Parameters.Add(":custCode", OracleType.VarChar)
        objsql.Parameters(":custCode").Value = Session("cust_cd")

        objsql.ExecuteNonQuery()

        determine_tax(zip_cd)

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD=:taxCode "

        sql = sql & "AND TAT.DEL_TAX = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        'jkl
        objsql2.Parameters.Add(":taxCode", OracleType.VarChar)
        objsql2.Parameters(":taxCode").Value = Session("TAX_CD")

        Try
            'Execute DataReader 
            itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If itmDatRdr.Read() Then
                Session("DEL_TAX") = itmDatRdr.Item("SumOfPCT")
            Else
                Session("DEL_TAX") = "N"
            End If
            itmDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD=:taxCode "

        sql = sql & "AND TAT.SU_TAX = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        'Determine the taxability of the setup and delivery charges
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        objsql2.Parameters.Add(":taxCode", OracleType.VarChar)
        objsql2.Parameters(":taxCode").Value = Session("TAX_CD")

        Try
            'Execute DataReader 
            itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If itmDatRdr.Read() Then
                Session("SU_TAX") = itmDatRdr.Item("SumOfPCT")
            Else
                Session("SU_TAX") = "N"
            End If
            itmDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        conn.Close()
        conn2.Close()

        Session("LEAD") = ""
        If Not IsNothing(RelanPriceChange) AndAlso RelanPriceChange.Count > 0 Then
            Session("priceChangeDictionary") = RelanPriceChange
        End If
        'If SKU(s) price is updated do you want reapply discount if it is exsists in converting relationship
        If needTorecalDiscount AndAlso Not (ln2disc) Is Nothing Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            Dim errorMsg As String = String.Format(Resources.POSMessages.MSG0053, String.Join(",", dictCurrentPrice.Select(Function(pair) String.Format("{0}", pair.Key)).ToArray()))
            ucMsgPopup.DisplayConfirmMsg(errorMsg, "DISCOUNT")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            msgPopup.ShowOnPageLoad = True
        Else
            Response.Redirect("startup.aspx")
        End If
    End Sub

    Private Function GetRelationshipComments(ByVal relNo As String) As String

        Dim relComments As New StringBuilder(Session("scomments").ToString)
        Dim slspNotes As New StringBuilder()

        'Gathers the Salesperson Notes (if any)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim cmd As OracleCommand
        Dim rdr As OracleDataReader
        Dim sql As String = "SELECT * FROM RELATIONSHIP_COMMENTS where REL_NO='" & relNo & "' ORDER BY SUBMIT_DT ASC"
        Dim curr_dt As String = ""
        Dim cmnt_cnt As Integer = 0
        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            rdr = DisposablesManager.BuildOracleDataReader(cmd)

            Do While (rdr.Read())
                If curr_dt & "" = "" Then curr_dt = rdr.Item("SUBMIT_DT").ToString
                If curr_dt <> rdr.Item("SUBMIT_DT").ToString Then
                    slspNotes.Append(vbCrLf).Append(vbCrLf).Append(rdr.Item("SUBMIT_DT")).Append(": ").Append(rdr.Item("COMMENTS"))
                Else
                    If cmnt_cnt = 0 Then
                        slspNotes.Append(rdr.Item("SUBMIT_DT")).Append(": ").Append(rdr.Item("COMMENTS").ToString)
                    Else
                        slspNotes.Append(rdr.Item("COMMENTS").ToString)
                    End If
                End If
                curr_dt = rdr.Item("SUBMIT_DT").ToString
                cmnt_cnt = cmnt_cnt + 1
            Loop

            rdr.Close()
            cmd.Dispose()
            conn.Close()

        Catch ex As Exception
            conn.Close()
        End Try

        'Concatenates ALL comments for this relationship
        If (slspNotes.ToString.Trim.Length > 0) Then
            relComments.Append("(Salesperson Notes:")
            relComments.Append(slspNotes.ToString.Trim)
            relComments.Append(")")
        End If

        Return relComments.ToString()
    End Function

    Public Sub Populate_Relationships()

        ASPxPopupControl2.ContentUrl = "Sales_Relationship.aspx"

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("SELECT * FROM RELATIONSHIP WHERE CUST_CD = :cust_cd AND REL_STATUS='O' ")

        If (SysPms.numDaysRelship > 0) Then
            sqlStmtSb.Append(" AND ( (follow_up_dt IS NOT NULL AND follow_up_dt + :days2Display >= TRUNC(SYSDATE)) ").Append(
                 " OR ( follow_up_dt IS NULL AND wr_dt IS NOT NULL AND TO_DATE(wr_dt) + :days2Display >= TRUNC(SYSDATE) ) ) ")   ' This logic depends on wr_dt being a date although it is not in the db; confirmed inserting dt wherever created
            ' although we've allowed for null follow_up_dt here, the process will fail if it is null
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
        Dim MyDatareader As OracleDataReader

        Try
            conn.Open()

            cmd.Parameters.Add(":cust_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":cust_cd").Value = Session("cust_cd")

            If (SysPms.numDaysRelship > 0) Then
                cmd.Parameters.Add(":days2Display", OracleType.Number).Direction = ParameterDirection.Input
                cmd.Parameters(":days2Display").Value = SysPms.numDaysRelship
            End If

            MyDatareader = DisposablesManager.BuildOracleDataReader(cmd)

            If MyDatareader.HasRows Then
                ASPxPopupControl2.ShowOnPageLoad = True
            End If
            MyDatareader.Close()
        Finally
            conn.Close()
        End Try
    End Sub

    Public Sub Populate_Relationships(ByVal coGrpCD As String)

        ASPxPopupControl2.ContentUrl = "Sales_Relationship.aspx"

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("SELECT * FROM RELATIONSHIP WHERE CUST_CD = :cust_cd AND REL_STATUS='O' and store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd= '" & coGrpCD & "'))")

        If (SysPms.numDaysRelship > 0) Then
            sqlStmtSb.Append(" AND ( (follow_up_dt IS NOT NULL AND follow_up_dt + :days2Display >= TRUNC(SYSDATE)) ").Append(
                 " OR ( follow_up_dt IS NULL AND wr_dt IS NOT NULL AND TO_DATE(wr_dt) + :days2Display >= TRUNC(SYSDATE) ) ) ")   ' This logic depends on wr_dt being a date although it is not in the db; confirmed inserting dt wherever created
            ' although we've allowed for null follow_up_dt here, the process will fail if it is null
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
        Dim MyDatareader As OracleDataReader

        Try
            conn.Open()

            cmd.Parameters.Add(":cust_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":cust_cd").Value = Session("cust_cd")

            If (SysPms.numDaysRelship > 0) Then
                cmd.Parameters.Add(":days2Display", OracleType.Number).Direction = ParameterDirection.Input
                cmd.Parameters(":days2Display").Value = SysPms.numDaysRelship
            End If

            MyDatareader = DisposablesManager.BuildOracleDataReader(cmd)

            If MyDatareader.HasRows Then
                ASPxPopupControl2.ShowOnPageLoad = True
            End If
            MyDatareader.Close()
        Finally
            conn.Close()
        End Try
    End Sub
    
    'Code change is done for,when relationship# is selected from popup to convert it as sales order.
    'In case if any line item of selected relationship is having dorpped SKU for that, it will validate and gives alert popup
    ' then continue with normal flow

    Public Sub CheckForDroppedSkus()

        Dim droppedSkuResponse As DroppedSkuResponseDtc = theSkuBiz.GetDroppedItemsForRelationship(Request("del_doc_num").ToString)
        If (droppedSkuResponse.prevSaleCount >= 1) Then
            'This code is written with reference to Admin.aspx.vb as suggested by Laxmikantha.
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            Dim msg As String = If(droppedSkuResponse.prevSaleCount = 1,
                                   Resources.POSMessages.MSG0018,
                                   Resources.POSMessages.MSG0016)
            ucMsgPopup.DisplayAlertMsg(String.Format(msg, droppedSkuResponse.prevSaleSkus), "")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = Color.Red
            msgPopup.ShowOnPageLoad = True
            'Reference ends here
            ViewState("redirect") = False
        Else
            CheckRelnPriceChange()
        End If
    End Sub

    Protected Sub ucMsgPopup_Choice(sender As Object, e As EventArgs, choice As Usercontrols_MessagePopup.MessageResponse) Handles ucMsgPopup.Choice
        'OK button clcik event
        If ucMsgPopup.MessageResponse.YES_OK_SELECTION = choice Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            If Not ViewState("needTorecalDiscount") Is Nothing AndAlso ViewState("needTorecalDiscount") = True Then
                ReapplyDiscountsOnCovtoOrder(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
                Response.Redirect("startup.aspx")
            Else
                CheckRelnPriceChange()
            End If
        ElseIf ucMsgPopup.MessageResponse.NO_SELECTION = choice Then
            If Not ViewState("needTorecalDiscount") Is Nothing Then
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                theSalesBiz.DeleteTempLnDiscount(Session.SessionID.ToString.Trim)
                theSalesBiz.ReapplyDiscountForRelConvSOE(Session.SessionID.ToString.Trim)
                Response.Redirect("startup.aspx")
            End If
        End If
    End Sub

    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated
        Dim rel_check As String
        rel_check = Session("rel_check") & ""

        img_warn.Visible = False
        lbl_Warning.Visible = False

        PopulateData(rel_check)

        PopupTax.ShowOnPageLoad = False
    End Sub

    Protected Sub MsgPopupConvertion_Option(sender As Object, e As EventArgs, choice As Usercontrols_MessagePopup.MessageResponse) Handles MsgPopupConvertion.Choice
        'OK button clcik event
        If MsgPopupConvertion.MessageResponse.YES_OK_SELECTION = choice Then
            convert_relationship()
        ElseIf MsgPopupConvertion.MessageResponse.NO_SELECTION = choice Then
            Session("rel_check") = "FALSE"
        End If
        Session("ShowRelationships") = Nothing
    End Sub
    ''' <summary>
    ''' Check the price change for the releationship price to current price before convert to order.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub CheckRelnPriceChange()
        Session("tran_dt") = FormatDateTime(Today.Date.ToString, 2)
        Dim dictCurrentPrice As Dictionary(Of String, Double) = theSkuBiz.CheckPriceChange(Request("del_doc_num"), SessVar.homeStoreCd, SessVar.custTpPrcCd, Session("tran_dt"))
        If Not IsNothing(dictCurrentPrice) AndAlso dictCurrentPrice.Count > 0 Then
            Session("dictCurrentPrice") = dictCurrentPrice
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = MsgPopupConvertion.FindControl("popupmsg")
            Dim errorMsg As String = String.Format(Resources.POSMessages.MSG0048, String.Join(",", dictCurrentPrice.Select(Function(pair) String.Format("{0}", pair.Key)).ToArray()))
            MsgPopupConvertion.DisplayConfirmMsg(errorMsg, "PRICE")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            msgPopup.ShowOnPageLoad = True
        Else
            convert_relationship()
        End If
    End Sub
    ''' <summary>
    ''' Recalculates the discounts (when price is updated while relationship is convert to order)
    ''' </summary>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">the ID for the line that triggered this event</param>
    Private Sub ReapplyDiscountsOnCovtoOrder(ByVal action As ReapplyDiscountRequestDtc.LnChng, ByVal lnSeq As String)

        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd")) Then
            Dim discRequest As ReapplyDiscountRequestDtc
            discRequest = New ReapplyDiscountRequestDtc(action, Session("pd"), Session.SessionID.ToString.Trim, lnSeq)
            Dim discResponse As Boolean = theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest)
        End If
        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub
    Protected Sub sy_btn_tdpl_inspmt_Click(sender As Object, e As EventArgs) Handles sy_btn_tdpl_inspmt.Click
        'sabrina tdpl
        'Alice disabled the ability to make a TD "BRICK PAYMENT" on Mar 20, 2019 for request 4597
        'If Session("td_instpmt") = "Y" Then
        '    Response.Redirect("tdpl.aspx")
        'End If
    End Sub

    Protected Sub sy_btn_gc_transfer_Click(sender As Object, e As EventArgs) Handles sy_btn_gc_transfer.Click
        'sabrina GC transfer
        If Session("gc_transfer") = "Y" Then
            If txtCustCode.Text <> "" Then
                Session("cust_cd") = txtCustCode.Text
            End If
            Response.Redirect("WGC_TRANSFER.aspx")
        End If
    End Sub
    'Alice added for request 167
    Protected Sub sy_btn_pa_other_Click(sender As Object, e As EventArgs) Handles sy_btn_pa_other.Click

        If Session("OtherCustCd") = "Y" Then
            If txtCustCode.Text <> "" Then
                Session("pa_cust_cd") = txtCustCode.Text
            End If
            Response.Redirect("IndependentPaymentProcessing.aspx")
        End If
    End Sub

    Protected Sub sy_btn_payr_Click(sender As Object, e As EventArgs) Handles sy_btn_payr.Click

        'sabrina payr paymanet reversal
        If Session("payment_reversal") = "Y" Then
            If txtCustCode.Text <> "" Then
                Session("cust_cd") = txtCustCode.Text
            End If
            Response.Redirect("paymentreversal.aspx")
        End If
    End Sub

    Protected Sub sy_btn_gc_purchase_Click(sender As Object, e As EventArgs) Handles sy_btn_gc_purchase.Click
        'sabrina R1922
        If Session("gift_card_purchase") = "Y" Then
            If txtCustCode.Text <> "" Then
                Session("cust_cd") = txtCustCode.Text
            End If
            Response.Redirect("WGC_PurchaseGiftCard.aspx")
        End If
    End Sub

    Protected Sub sy_btn_aerompc_Click(sender As Object, e As EventArgs) Handles sy_btn_aerompc.Click
        'sabrina aeroplangoodwillmpc
        If Session("aeroMPCmain") = "Y" Then
            If txtCustCode.Text <> "" Then
                Session("aero_cust_cd") = txtCustCode.Text
            End If
            Response.Redirect("AeroplanMPC.aspx")
        End If
    End Sub
End Class
