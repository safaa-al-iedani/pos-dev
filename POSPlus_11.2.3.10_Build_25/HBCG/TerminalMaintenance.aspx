<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="TerminalMaintenance.aspx.vb" Inherits="TerminalMaintenance" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" style="position: relative; width: 1100px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdddlb" style="width: 400px">
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>

            <td class="class_tdbtn" style="width: 600px" align="right">
                <dx:ASPxButton ID="btn_Search" runat="server" meta:resourcekey="lbl_btn_search">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear_screen">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>

    
    <table border="0" style="position: relative; width: 1100px; left: 0px; top: 0px;">
        <tr><td colspan="9">
        <hr />
        </td></tr>

        <tr>
            <td class="class_tdlbl" Width="120px" align="right">
                <dx:ASPxLabel ID="lbl_svs_div_cd" runat="server" meta:resourcekey="lbl_svs_div_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" Width="120px">
                <asp:TextBox ID="txt_svs_div_cd" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
            </td>         
            <td class="class_tdlbl" Width="120px" align="right">
                <dx:ASPxLabel ID="lbl_svs_store_num" runat="server" meta:resourcekey="lbl_svs_store_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" Width="120px">
                <asp:TextBox ID="txt_svs_store_num" runat="server" Width="100px" MaxLength="15"></asp:TextBox>
            </td> 
            <td class="class_tdlbl" Width="120px" align="right">
                <dx:ASPxLabel ID="lbl_td_store_num" runat="server" meta:resourcekey="lbl_td_store_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" Width="120px">
                <asp:TextBox ID="txt_td_store_num" runat="server" Width="100px" MaxLength="15"></asp:TextBox>
            </td>         
            <td class="class_tdlbl" Width="120px" align="right">
                <dx:ASPxLabel ID="lbl_td_merchant_num" runat="server" meta:resourcekey="lbl_td_merchant_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" Width="120px">
                <asp:TextBox ID="txt_td_merchant_num" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
            </td>
            <td class="class_tdbtn" style="width: 140px" align="right">
                <dx:ASPxButton ID="btn_UpdateStoreInfo" runat="server" meta:resourcekey="lbl_btn_update_store_info">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td colspan="9" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="True" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>

    </table>

    <br />

    <div runat="server" id="div_GridViews">
        
    <dx:ASPxGridView ID="GridViewTerminalDetails" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="PC_NAME"  ClientInstanceName="GridViewTerminalDetails" Visible="true"
        OnInitNewRow="GridViewTerminalDetails_InitNewRow" OnBatchUpdate="GridViewTerminalDetails_BatchUpdate" oncelleditorinitialize="GridViewTerminalDetails_CellEditorInitialize" OnParseValue="GridViewTerminalDetails_ParseValue">
<%--<SettingsEditing Mode="Inline" /> --%>
        <SettingsEditing Mode="Batch" />
        <SettingsCommandButton>
        <UpdateButton Text="Update" ></UpdateButton>
        <CancelButton Text="Cancel" ></CancelButton>
        </SettingsCommandButton>

        <Columns>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label573 %>" FieldName="STORE_CD" VisibleIndex="1" ReadOnly="True">
                <PropertiesTextEdit MaxLength="2">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the Store Code"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Terminal Id" FieldName="PINPAD_ID" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="20">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the Pinpad ID"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="PC Name" FieldName="PC_NAME" VisibleIndex="3">
                <PropertiesTextEdit MaxLength="40">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the PC Name"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="IP Address" FieldName="PC_IP_ADDRESS" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="20">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the PC IP Address"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Printer" FieldName="PRINTER_GROUPID_NAME" VisibleIndex="5">
                <%--<EditItemTemplate>
                <asp:DropDownList ID="ddlb_PrinterGroupIDs" runat="server" DataSourceID="JAMTEST" DataValueField="PRINTER_GROUPID_NAME">
                </asp:DropDownList>
                </EditItemTemplate>
                --%>
               <PropertiesTextEdit MaxLength="20">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the Printer GroupID"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
                
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Laser Printer" FieldName="LASER_PRINTER_NAME" VisibleIndex="6">
                <PropertiesTextEdit MaxLength="20">
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Refund Limit" FieldName="REFUND_LIMIT" VisibleIndex="7">
                <PropertiesTextEdit MaxLength="16">
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Server IP Address" FieldName="SERVER_IP" VisibleIndex="8">
                <PropertiesTextEdit MaxLength="20">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the Server IP Address"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Server Port" FieldName="SERVER_PORT" VisibleIndex="9">
                <PropertiesTextEdit MaxLength="6">
                    <ValidationSettings>
                    <RequiredField IsRequired="True" ErrorText="Please enter the Server Port"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
           <dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowDeleteButton="True" VisibleIndex="10"/>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsText ConfirmDelete="Are you sure you wish to delete this row?" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText ConfirmDelete="Please confirm and then click Update to permanently delete the record." />
    </dx:ASPxGridView>


    </div>

    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
