Imports System.Data.OracleClient
Imports System.IO
Imports HBCG_Utils
Imports System.Collections.Generic
Imports SalesUtils

Partial Class Invoice_Admin
    Inherits POSBasePage



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("OLPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        If Not IsPostBack Then
            'Locate stores added to the system and automatically add them to the the table
            GetStores()
            bindgrid()
        End If

    End Sub


    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim mytable As DataTable
        Dim numrows As Integer

        Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Invoices\")
        Dim dirInfo2 As New DirectoryInfo(Server.MapPath("~/invoice_templates"))
        Dim fileList As New List(Of FileInfo)
        Dim repxFiles() As FileInfo = Nothing

        '** first retrieve and add all the .aspx files to the list
        Dim aspxFiles() As FileInfo = dirInfo.GetFiles("*.aspx")
        For Each File In aspxFiles
            fileList.Add(File)
        Next

        '*** Check for any .repx files & add those 
        Try
            repxFiles = dirInfo2.GetFiles("*.repx")
            For Each File In repxFiles
                fileList.Add(File)
            Next
        Catch ex As System.IO.DirectoryNotFoundException
            'the new invoice files folder for .repx files, does not exist, create it
            Dim di As DirectoryInfo = Directory.CreateDirectory(Server.MapPath("~/invoice_templates"))
        End Try


        cbo_default_file.Items.Clear()
        cbo_default_file.Items.Insert(0, "Please Select A File")
        cbo_default_file.Items.FindByText("Please Select A File").Value = ""
        cbo_default_file.SelectedIndex = 0

        With cbo_default_file
            .DataSource = fileList
            .DataValueField = "Name"
            .DataTextField = "Name"
            .DataBind()
        End With

        ds = New DataSet
        Gridview1.DataSource = ""
        Gridview1.Visible = True
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim sql As String = "SELECT * FROM INVOICE_ADMIN order by STORE_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("DEFAULT_FILE").ToString & "" <> "" Then
                    cbo_default_file.SelectedValue = MyDataReader.Item("DEFAULT_FILE").ToString
                End If
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    Public Sub GetStores()

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim store_listing As String = ""
        Dim default_invoice As String = ""

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader

        sql = "SELECT store_cd, default_file FROM invoice_admin ORDER BY store_cd"
        Try
            conn2.Open()
            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                store_listing = store_listing & MyDataReader2.Item("STORE_CD").ToString & "|"
                default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        If default_invoice & "" = "" Then default_invoice = "GenericQuote.aspx"

        sql = "SELECT STORE_CD FROM STORE ORDER BY STORE_CD"
        Try
            conn.Open()
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                If InStr(UCase(store_listing), UCase(MyDataReader.Item("STORE_CD").ToString)) < 1 Then
                    sql = "INSERT INTO INVOICE_ADMIN (STORE_CD, DEFAULT_FILE) VALUES('" & MyDataReader.Item("STORE_CD").ToString & "','" & default_invoice & "')"
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objSql2.ExecuteNonQuery()
                    lbl_msg.Visible = True
                    lbl_msg.Text = lbl_msg.Text & MyDataReader.Item("STORE_CD").ToString & "<br />"
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        conn.Close()
        conn2.Close()

    End Sub


    Protected Sub PopulateFiles(ByVal sender As Object, ByVal e As DataGridItemEventArgs)

        Dim ddInvoiceFile As DropDownList = e.Item.Cells(1).FindControl("drpInvFile")
        Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Invoices\")
        Dim dirInfo2 As New DirectoryInfo(Server.MapPath("~/invoice_templates"))
        Dim fileList As New List(Of String)

        '*** first retrieve and add all the .aspx files to the list
        Dim aspxFiles() As FileInfo = dirInfo.GetFiles("*.aspx")
        For Each File In aspxFiles
            fileList.Add(File.Name)
        Next

        '*** Check for any .repx files & add those 
        Dim repxFiles() As FileInfo = dirInfo2.GetFiles("*.repx")
        For Each File In repxFiles
            fileList.Add(File.Name)
        Next

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        If (Not ddInvoiceFile Is Nothing) Then

            For Each fileName In fileList
                ddInvoiceFile.Items.Add(fileName)
            Next

            Dim strStCd As String = e.Item.Cells(0).Text
            Dim sql As String = "SELECT invoice_file FROM invoice_admin WHERE store_cd ='" & strStCd & "' "
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            If (MyDataReader.Read()) Then
                If MyDataReader.Item("invoice_file") & "" <> "" Then
                    ddInvoiceFile.Text = MyDataReader.Item("invoice_file")
                End If
                MyDataReader.Close()
            End If
        End If
        conn.Close()

    End Sub

    Protected Sub DoItemSave(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim drpfile As DropDownList
        Dim strFileName As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand


        drpfile = e.Item.FindControl("drpInvFile")
        strFileName = drpfile.SelectedItem.Value

        ' Update TempValue to DB
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE invoice_admin SET invoice_file = '" & strFileName & "'  WHERE store_cd ='" & e.Item.Cells(0).Text & "' "
            .ExecuteNonQuery()
        End With

        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub btn_default_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_default.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand

        conn.Open()
        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE invoice_admin SET default_file = '" & cbo_default_file.SelectedValue.ToString & "'"
            .ExecuteNonQuery()
        End With
        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub


    'Public Sub Upload_Click(ByVal Sender As Object, ByVal e As EventArgs)

    '    ' Display properties of the uploaded file
    '    FileName.InnerHtml = MyFile.PostedFile.FileName
    '    FileContent.InnerHtml = MyFile.PostedFile.ContentType
    '    FileSize.InnerHtml = MyFile.PostedFile.ContentLength
    '    UploadDetails.Visible = True

    '    'Grab the file name from its fully qualified path at client 
    '    Dim strFileName As String = MyFile.PostedFile.FileName

    '    ' only the attched file name not its path
    '    Dim c As String = System.IO.Path.GetFileName(strFileName)

    '    'Save uploaded file to server at LOCAL PATH
    '    Try
    '        MyFile.PostedFile.SaveAs(Request.ServerVariables("APPL_PHYSICAL_PATH") + "Invoices\" + c)
    '        Span1.InnerHtml = "Your file successfully uploaded as: " & _
    '                          Request.ServerVariables("APPL_PHYSICAL_PATH") & "Invoices\" & c
    '    Catch Exp As Exception
    '        Span1.InnerHtml = "An Error occured. Please check the attached  file"
    '        UploadDetails.Visible = False
    '        Span2.Visible = False
    '    End Try

    'End Sub


    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function
End Class
