<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GeneralInterest.aspx.vb"
    Inherits="GeneralInterest" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="admin_form" runat="server">
        <dx:ASPxLabel ID="lbl_title" runat="server" Text="General Interest Categories - Relationships"
            Font-Bold="True">
        </dx:ASPxLabel>
        <br />
        <br />
        <asp:DataGrid ID="grd_gen_int" runat="server" AutoGenerateColumns="False" Width="100%">
            <Columns>
                <asp:BoundColumn DataField="GEN_CAT_ID" Visible="False" />
                <asp:BoundColumn DataField="DES" Visible="False" />
                <asp:BoundColumn DataField="MJR_CD" Visible="False" />
                <asp:BoundColumn DataField="MNR_CD" Visible="False" />
                <asp:BoundColumn DataField="CUSTOM" Visible="False" />
                <asp:TemplateColumn HeaderText="Description">
                    <ItemTemplate>
                        <dx:ASPxLabel ID="lbl_desc" runat="server" Text="">
                        </dx:ASPxLabel>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Major Code">
                    <ItemTemplate>
                        <dx:ASPxLabel ID="lbl_mjr_cd" runat="server" Text="">
                        </dx:ASPxLabel>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Minor Code">
                    <ItemTemplate>
                        <dx:ASPxLabel ID="lbl_mnr_cd" runat="server" Text="">
                        </dx:ASPxLabel>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Custom?">
                    <ItemTemplate>
                        <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" OnCheckedChanged="Custom_Update">
                        </dx:ASPxCheckBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Available?">
                    <ItemTemplate>
                        <dx:ASPxCheckBox ID="ASPxCheckBox2" runat="server" OnCheckedChanged="Deleted_Update">
                        </dx:ASPxCheckBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="DELETED" Visible="False" />
            </Columns>
        </asp:DataGrid>
        <br />
        <dx:ASPxLabel ID="lbl_error" runat="server" Text="" Width="100%">
        </dx:ASPxLabel>
        <table width="98%">
            <tr>
                <td>
                    <dx:ASPxButton ID="btn_add_majors" runat="server" Text="Add Major Codes" Width="135px">
                    </dx:ASPxButton>
                </td>
                <td>
                    <dx:ASPxButton ID="btn_add_minors" runat="server" Text="Add Minor Codes" Width="135px">
                    </dx:ASPxButton>
                </td>
                <td style="text-align: right">
                    <dx:ASPxButton ID="btn_new_custom" runat="server" Text="Add Custom Category" Width="157px">
                    </dx:ASPxButton>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txt_custom_add" runat="server" Width="239px">
                    </dx:ASPxTextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </div>
</asp:Content>
