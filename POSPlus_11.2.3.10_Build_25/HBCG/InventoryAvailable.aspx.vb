Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class InventoryAvailable
    Inherits POSBasePage
    Dim running_balance2 As Double = 0
    Dim running_balance As Double = 0
    Dim aiq_ds As DataSet

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        MyDataGrid.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sqlSb As New StringBuilder
        Dim sAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter

        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter
        Dim proceed As Boolean
        Dim LikeQry As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand


        objConnect = DisposablesManager.BuildOracleConnection

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        ' Daniela rownum < 100 added
        sqlSb.Append("select itm.itm_cd, itm.vsn || ' - ' || itm.des vsn, ").Append(
                     "sum(decode(loc.loc_tp_cd,'W',itm_fifl.qty,0)) whs_qty, ").Append(
                     "sum(decode(loc.loc_tp_cd,'S',itm_fifl.qty,0)) shw_qty, ").Append(
                     "null po_data, ").Append(
                     "null ist_data ").Append(
                "FROM itm, itm_fifl, loc ").Append(
                "WHERE rownum < 100 and itm.itm_cd = itm_fifl.itm_cd ").Append(
                      "and itm_fifl.store_cd = loc.store_cd ").Append(
                      "and itm_fifl.loc_cd = loc.loc_cd ").Append(
                      "AND exists (select 1 from store_grp$store sgs where(sgs.store_cd = itm_fifl.store_cd) and sgs.store_grp_cd = :storeGrpCd) ")

        If txt_sku.Text & "" <> "" Then
            If InStr(txt_sku.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.itm_cd " & LikeQry & " :ITM_CD ")
        End If
        If txt_vsn.Text & "" <> "" Then
            If InStr(txt_vsn.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.VSN " & LikeQry & " :VSN ")
        End If
        ' Daniela rownum < 100 added
        sqlSb.Append("group by itm.itm_cd, itm.vsn, itm.des ").Append(
        "union all ").Append(
        "SELECT itm.itm_cd, itm.vsn || ' - ' || itm.des vsn, ").Append(
             "0 whs_qty, 0 shw_qty, null po_data, ").Append(
             "a.doc_num || ' (' || sum(b.qty) || ')' ist_data ").Append(
        "FROM itm, ist a, ist_ln b ").Append(
        "WHERE rownum < 100 and (a.ist_seq_num = b.ist_seq_num) ").Append(
            "and a.ist_store_cd = b.ist_store_cd ").Append(
            "and a.ist_wr_dt = b.ist_wr_dt ").Append(
            "and itm.itm_cd = b.itm_cd ").Append(
            "AND exists (select 1 from store_grp$store sgs where(sgs.store_cd = a.dest_store_cd) and sgs.store_grp_cd = :storeGrpCd) ").Append(
            "and a.stat_cd='O' ").Append(
            "and a.transfer_dt <= TO_DATE(:needDt,'mm/dd/RRRR') ").Append(
            "and b.old_store_cd is not null and b.dest_flag='W' ").Append(
            "and b.void_flag='N' ")
        If txt_sku.Text & "" <> "" Then
            If InStr(txt_sku.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.itm_cd " & LikeQry & " :ITM_CD ")
        End If
        If txt_vsn.Text & "" <> "" Then
            If InStr(txt_vsn.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.VSN " & LikeQry & " :VSN ")
        End If
        sqlSb.Append("group by itm.itm_cd, itm.vsn, itm.des, a.doc_num ").Append(
        "union all ").Append(
        "SELECT itm.itm_cd, itm.vsn || ' - ' || itm.des vsn, ").Append(
            "0 whs_qty, 0 shw_qty, ").Append(
            "d.po_cd || ' Ln#' || d.ln# || ': ' || nvl(d.qty_ord - sum(decode(f.po_actn_tp_cd,'RCV',f.qty,0)) - nvl(psq.qty,0),0) po_data, ").Append(
            "null ist_data ").Append(
        "FROM itm, po_ln$actn_hst f, po_ln d, po e, ").Append(
            "(select a.po_cd, a.ln#, sum(b.qty) qty ").Append(
            "from so_ln b, so_ln$po_ln a ").Append(
            "where(a.del_doc_num = b.del_doc_num) ").Append(
                 "and a.del_doc_ln# = b.del_doc_ln# ").Append(
                 "and b.store_cd is null  GROUP BY a.po_cd, a.ln#) psq  ").Append(
        "WHERE rownum < 100 and (itm.itm_cd = d.itm_cd) ").Append(
            "and d.po_cd = e.po_cd ").Append(
            "and d.po_cd = f.po_cd ").Append(
            "and d.ln# = f.ln#  ").Append(
            "and d.po_cd = psq.po_cd (+) ").Append(
            "and d.ln# = psq.ln# (+) ").Append(
            "AND exists (select 1 from store_grp$store sgs where(sgs.store_cd = d.dest_store_cd) and sgs.store_grp_cd = :storeGrpCd) ").Append(
            "and e.stat_cd = 'O' ")
        If txt_sku.Text & "" <> "" Then
            If InStr(txt_sku.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.itm_cd " & LikeQry & " :ITM_CD ")
        End If
        If txt_vsn.Text & "" <> "" Then
            If InStr(txt_vsn.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("and itm.VSN " & LikeQry & " :VSN ")
        End If
        sqlSb.Append("group by itm.itm_cd, itm.vsn, itm.des, d.po_cd, d.ln#, d.qty_ord, psq.qty ").Append(
        "having nvl(d.qty_ord - sum(decode(f.po_actn_tp_cd,'RCV',f.qty,0)) - nvl(psq.qty,0),0) >0 ").Append(
        "order by 1")

        objSql = DisposablesManager.BuildOracleCommand(sqlSb.ToString, objConnect)

        If txt_sku.Text & "" <> "" Then
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters(":ITM_CD").Value = UCase(txt_sku.Text)
        End If
        If txt_vsn.Text & "" <> "" Then
            objSql.Parameters.Add(":VSN", OracleType.VarChar)
            objSql.Parameters(":VSN").Value = txt_vsn.Text
        End If
        objSql.Parameters.Add(":storeGrpCd", OracleType.VarChar)
        objSql.Parameters(":storeGrpCd").Value = UCase(cbo_store_grp_cd.Value)
        objSql.Parameters.Add(":needDt", OracleType.VarChar)
        objSql.Parameters(":needDt").Value = FormatDateTime(dt_needed.Value, DateFormat.ShortDate)

        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
        sAdp.Fill(ds)

        aiq_ds = ds

        sqlSb.Clear()
        sqlSb.Append("SELECT itm.itm_cd, itm.vsn || ' - ' || itm.des AS VSN FROM itm WHERE ")

        If txt_sku.Text & "" <> "" Then
            If InStr(txt_sku.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("itm.itm_cd " & LikeQry & " :ITM_CD ")
            proceed = True
        End If
        If txt_vsn.Text & "" <> "" Then
            If proceed = True Then sqlSb.Append("AND ")
            If InStr(txt_vsn.Text, "%") > 0 Then
                LikeQry = "LIKE"
            Else
                LikeQry = "="
            End If
            sqlSb.Append("itm.VSN " & LikeQry & " :VSN ")
            proceed = True
        End If
        'Daniela restrict number of records
        If proceed = True Then sqlSb.Append("AND ")
        If InStr(txt_vsn.Text, "%") > 0 Then

            LikeQry = "LIKE"
        Else
            LikeQry = "="
        End If
        sqlSb.Append("rownum < 1000 ")

        'July 11,2017 - mariam - #3108 - isvalidItem
        sqlSb.Append(" and STD_MULTI_CO.isValidItm2( '" & Session("EMP_CD") & "', itm.itm_cd) = 'Y' ")

        objSql = DisposablesManager.BuildOracleCommand(sqlSb.ToString, objConnect)

        If txt_sku.Text & "" <> "" Then
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters(":ITM_CD").Value = UCase(txt_sku.Text)
        End If
        If txt_vsn.Text & "" <> "" Then
            objSql.Parameters.Add(":VSN", OracleType.VarChar)
            objSql.Parameters(":VSN").Value = txt_vsn.Text
        End If

        Dim ds2 = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
        sAdp.Fill(ds2)
        MyTable = New DataTable
        MyTable = ds2.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds2, "MyDataGrid")

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds2.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows = 0 Then
            lbl_Warning.Visible = True
            'Daniela french
            'lbl_Warning.Text = "No records were found matching your search criteria." & vbCrLf
            lbl_Warning.Text = Resources.LibResources.Label709 & vbCrLf
            btnSubmit.Visible = True
        Else
            Source.Sort = SortField
            MyDataGrid.DataSource = Source
            MyDataGrid.DataBind()
            lbl_Warning.Visible = False
        End If
        'Close connection           
        objConnect.Close()

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If txt_sku.Text & "" = "" And txt_vsn.Text & "" = "" Then
            'Daniela french
            'lbl_errors.Text = "You must enter a SKU or VSN to begin search."
            lbl_errors.Text = Resources.LibResources.Label710
        Else
            BindData("vsn asc")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_errors.Text = ""

        dt_needed.MinDate = Today.Date
        If Not IsPostBack Then
            store_cd_update()
            If isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag") = "Y" Then
                cbo_store_grp_cd.Value = "ALL"
            End If
            dt_needed.Value = Now.Date.AddDays(30)
        End If

    End Sub

    Protected Sub MyDataGrid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles MyDataGrid.ItemCommand

        Select Case e.CommandName
            Case "view_inv"
                ASPxPopupControl3.ContentUrl = "InventoryLookup.aspx?SKU=" & e.Item.Cells(0).Text & "&INV_SELECTION=Y"
                ASPxPopupControl3.ShowOnPageLoad = True
        End Select

    End Sub

    Protected Sub MyDataGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemDataBound

        Dim sql As String
        Dim sAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter

        Dim objConnect As OracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim inv_found As Boolean = False

        objConnect = DisposablesManager.BuildOracleConnection

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnect.Open()

        If cbo_store_grp_cd.Value & "" = "" Then cbo_store_grp_cd.Value = "ALL"

        If e.Item.ItemType = ListItemType.Header Then
            inv_found = True
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            inv_found = True
        End If

        If e.Item.Cells(1).Text & "" <> "" Then
            Dim lbl_vsn As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_vsn"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_vsn) Then
                If e.Item.Cells(1).Text = "&nbsp;" Then
                    lbl_vsn.Text = ""
                Else
                    lbl_vsn.Text = e.Item.Cells(1).Text
                End If
            End If
        End If

        Dim dr As DataRow

        If e.Item.Cells(0).Text & "" <> "" Then
            Dim lbl_sku As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_sku"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_sku) Then
                If e.Item.Cells(0).Text = "&nbsp;" Then
                    lbl_sku.Text = ""
                Else
                    lbl_sku.Text = e.Item.Cells(0).Text
                End If
            End If
            Dim lbl_shw As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_shw"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_shw) And chk_shw.Checked = True Then
                lbl_shw.Text = "0"
                For Each dr In aiq_ds.Tables(0).Rows
                    If e.Item.Cells(0).Text = dr("itm_cd").ToString Then
                        If CDbl(dr("shw_qty").ToString) > 0 Then
                            lbl_shw.Text = CDbl(dr("shw_qty").ToString)
                            inv_found = True
                        End If
                    End If
                Next
                '        sql = "SELECT SUM(ITM_FIFL.QTY) AS WHS_QTY FROM itm_fifl, loc WHERE "
                '        sql = sql & "ITM_FIFL.ITM_CD='" & e.Item.Cells(0).Text & "' "
                '        sql = sql & "AND ITM_FIFL.STORE_CD=LOC.STORE_CD "
                '        sql = sql & "AND ITM_FIFL.LOC_CD=LOC.LOC_CD "
                '        sql = sql & "AND LOC.LOC_TP_CD='S' "
                '        sql = sql & "AND ITM_FIFL.STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_grp_cd.Value & "') "
                '        sql = sql & "GROUP BY ITM_FIFL.ITM_CD"

                '        objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)
                '        Try
                '            MyDataReader = objSql.ExecuteReader
                '            If MyDataReader.Read Then
                '                If IsNumeric(MyDataReader.Item(0).ToString) Then
                '                    lbl_shw.Text = MyDataReader.Item(0).ToString
                '                    inv_found = True
                '                Else
                '                    lbl_shw.Text = "0"
                '                End If
                '            Else
                '                lbl_shw.Text = "0"
                '            End If
                '            MyDataReader.Close()
                '        Catch
                '            objConnect.Close()
                '            Throw
                '        End Try
            ElseIf Not IsNothing(lbl_shw) And chk_shw.Checked = False Then
                lbl_shw.Text = ""
            End If

            Dim lbl_whs As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_whs"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_whs) And chk_whs.Checked = True Then
                lbl_whs.Text = "0"
                For Each dr In aiq_ds.Tables(0).Rows
                    If e.Item.Cells(0).Text = dr("itm_cd").ToString Then
                        If CDbl(dr("whs_qty").ToString) > 0 Then
                            lbl_whs.Text = CDbl(dr("whs_qty").ToString)
                            inv_found = True
                        End If
                    End If
                Next
                '        sql = "SELECT SUM(ITM_FIFL.QTY) AS WHS_QTY FROM itm_fifl, loc WHERE "
                '        sql = sql & "itm_fifl.ITM_CD='" & e.Item.Cells(0).Text & "' "
                '        sql = sql & "AND ITM_FIFL.STORE_CD=LOC.STORE_CD "
                '        sql = sql & "AND ITM_FIFL.LOC_CD=LOC.LOC_CD "
                '        sql = sql & "AND LOC.LOC_TP_CD='W' "
                '        sql = sql & "AND ITM_FIFL.STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_grp_cd.Value & "') "
                '        sql = sql & "GROUP BY itm_fifl.ITM_CD"

                '        objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)
                '        Try
                '            MyDataReader = objSql.ExecuteReader
                '            If MyDataReader.Read Then
                '                If IsNumeric(MyDataReader.Item(0).ToString) Then
                '                    lbl_whs.Text = MyDataReader.Item(0).ToString
                '                    inv_found = True
                '                Else
                '                    lbl_whs.Text = "0"
                '                End If
                '            Else
                '                lbl_whs.Text = "0"
                '            End If
                '            MyDataReader.Close()
                '        Catch
                '            objConnect.Close()
                '            Throw
                '        End Try
            ElseIf Not IsNothing(lbl_whs) And chk_whs.Checked = False Then
                lbl_whs.Text = ""
            End If

            If Not IsDate(dt_needed.Value) Then
                dt_needed.Value = Now.Date.AddDays(30)
            End If

            Dim lbl_po As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_po"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_po) And chk_po.Checked = True Then
                For Each dr In aiq_ds.Tables(0).Rows
                    If e.Item.Cells(0).Text = dr("itm_cd").ToString Then
                        If dr("po_data").ToString & "" <> "" Then
                            lbl_po.Text = lbl_po.Text & dr("po_data").ToString & vbCrLf
                            inv_found = True
                        End If
                    End If
                Next
                '        sql = "SELECT PO.PO_CD, PO.PO_CD || ' (' || SUM(QTY_ORD) || ')' FROM PO, PO_LN WHERE PO.PO_CD=PO_LN.PO_CD "
                '        sql = sql & "AND PO_LN.ITM_CD='" & e.Item.Cells(0).Text & "' "
                '        sql = sql & "AND PO.STAT_CD='O' "
                '        sql = sql & "AND PO_LN.QTY_ORD>0 "
                '        sql = sql & "AND PO_LN.ARRIVAL_DT <= TO_DATE('" & dt_needed.Value & "','mm/dd/RRRR') "
                '        sql = sql & "AND PO_LN.DEST_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_grp_cd.Value & "') "
                '        sql = sql & "GROUP BY PO.PO_CD"

                '        objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)

                '        Try
                '            MyDataReader = objSql.ExecuteReader
                '            Do While MyDataReader.Read
                '                sql = "select PO_CD || ' Ln#' || LN# || ': ' || NVL(sum(TOTAL_INCOMING)-SUM(QTY_RCV+QTY_SOLD),0) FROM "
                '                sql = sql & "( "
                '                sql = sql & "SELECT a.PO_CD, b.LN#, b.QTY_ORD As Total_Incoming, TO_CHAR(b.ARRIVAL_DT, 'MM/DD/RRRR') AS ARRIVAL_DT, 0 AS QTY_RCV, 0 AS QTY_SOLD, b.DEST_STORE_CD  "
                '                sql = sql & "FROM PO a, PO_LN b  "
                '                sql = sql & "WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '" & e.Item.Cells(0).Text & "'  "
                '                sql = sql & "AND a.PO_CD = '" & MyDataReader.Item("PO_CD").ToString & "'  "
                '                sql = sql & "AND b.ARRIVAL_DT <= TO_DATE('" & dt_needed.Value & "','mm/dd/RRRR') "
                '                sql = sql & "union all  "
                '                sql = sql & "SELECT a.PO_CD, b.LN#, 0 As Total_Incoming, TO_CHAR(b.ARRIVAL_DT, 'MM/DD/RRRR') AS ARRIVAL_DT, 0 AS QTY_RCV, d.QTY AS QTY_SOLD, b.DEST_STORE_CD "
                '                sql = sql & "FROM PO a, PO_LN b, SO_LN$PO_LN c, SO_LN d "
                '                sql = sql & "WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '" & e.Item.Cells(0).Text & "'  "
                '                sql = sql & "AND c.PO_CD = b.PO_CD "
                '                sql = sql & "AND b.LN# = c.LN# "
                '                sql = sql & "AND b.ARRIVAL_DT <= TO_DATE('" & dt_needed.Value & "','mm/dd/RRRR') "
                '                sql = sql & "AND c.DEL_DOC_NUM = d.DEL_DOC_NUM "
                '                sql = sql & "AND d.DEL_DOC_LN# = c.DEL_DOC_LN# "
                '                sql = sql & "AND a.PO_CD = '" & MyDataReader.Item("PO_CD").ToString & "'  "
                '                sql = sql & "union all "
                '                sql = sql & "SELECT a.PO_CD, b.LN#, 0 As Total_Incoming, TO_CHAR(b.ARRIVAL_DT, 'MM/DD/RRRR') AS ARRIVAL_DT, c.QTY AS QTY_RCV, 0 AS QTY_SOLD, b.DEST_STORE_CD  "
                '                sql = sql & "FROM PO a, PO_LN b, PO_LN$ACTN_HST c  "
                '                sql = sql & "WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '" & e.Item.Cells(0).Text & "'  "
                '                sql = sql & "AND b.ARRIVAL_DT <= TO_DATE('" & dt_needed.Value & "','mm/dd/RRRR') "
                '                sql = sql & "AND c.PO_CD = b.PO_CD "
                '                sql = sql & "AND b.LN# = c.LN# "
                '                sql = sql & "and c.PO_ACTN_TP_CD = 'RCV' "
                '                sql = sql & "AND a.PO_CD = '" & MyDataReader.Item("PO_CD").ToString & "'  "
                '                sql = sql & ")  "
                '                sql = sql & "GROUP BY PO_CD, LN# "
                '                sql = sql & "HAVING SUM(TOTAL_INCOMING)-SUM(QTY_RCV+QTY_SOLD)>0"

                '                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnect)

                '                MyDataReader2 = objSql2.ExecuteReader
                '                Do While MyDataReader2.Read
                '                    inv_found = True
                '                    lbl_po.Text = lbl_po.Text & MyDataReader2.Item(0).ToString & vbCrLf
                '                Loop
                '                MyDataReader2.Close()
                '            Loop
                '            MyDataReader.Close()
                '        Catch
                '            objConnect.Close()
                '            Throw
                '        End Try
            End If

            Dim lbl_ist As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_ist"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_ist) And chk_ist.Checked = True Then
                For Each dr In aiq_ds.Tables(0).Rows
                    If e.Item.Cells(0).Text = dr("itm_cd").ToString Then
                        If dr("ist_data").ToString & "" <> "" Then
                            lbl_ist.Text = lbl_ist.Text & dr("ist_data").ToString & vbCrLf
                            inv_found = True
                        End If
                    End If
                Next
                '        sql = "SELECT a.DOC_NUM || ' (' || SUM(b.QTY) || ')' FROM IST a, IST_LN b "
                '        sql = sql & "WHERE b.ITM_CD='" & e.Item.Cells(0).Text & "' "
                '        sql = sql & "AND a.STAT_CD='O' AND a.IST_SEQ_NUM = b.IST_SEQ_NUM AND a.IST_STORE_CD = b.IST_STORE_CD "
                '        sql = sql & "AND a.IST_WR_DT = b.IST_WR_DT "
                '        sql = sql & "AND a.TRANSFER_DT <= TO_DATE('" & dt_needed.Value & "','mm/dd/RRRR') "
                '        sql = sql & "AND a.DEST_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_grp_cd.Value & "') "
                '        sql = sql & "AND b.OLD_STORE_CD IS NOT NULL AND b.DEST_FLAG='W' "
                '        sql = sql & "AND b.VOID_FLAG='N' "
                '        sql = sql & "GROUP BY a.DOC_NUM"

                '        objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)
                '        Try
                '            MyDataReader = objSql.ExecuteReader
                '            Do While MyDataReader.Read
                '                inv_found = True
                '                lbl_ist.Text = lbl_ist.Text & MyDataReader.Item(0).ToString & vbCrLf
                '            Loop
                '            MyDataReader.Close()
                '        Catch
                '            objConnect.Close()
                '            Throw
                '        End Try
            End If

            Dim lbl_out As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_out"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_out) And chk_out.Checked = True Then
                sql = "SELECT * FROM OUT WHERE ITM_CD='" & e.Item.Cells(0).Text & "' ORDER BY OUT_CD, ID_CD "

                objSql = DisposablesManager.BuildOracleCommand(sql, objConnect)
                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    Do While MyDataReader.Read
                        inv_found = True
                        lbl_out.Text = lbl_out.Text & MyDataReader.Item("OUT_CD").ToString & " - " & MyDataReader.Item("ID_CD").ToString & vbCrLf
                    Loop
                    MyDataReader.Close()
                Catch
                    objConnect.Close()
                    Throw
                End Try
            End If
        End If
        objConnect.Close()

        If chk_all.Checked = False Then
            If inv_found = True Then
                e.Item.Visible = True
            Else
                e.Item.Visible = False
            End If
        End If

    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        MyDataGrid.DataSource = Nothing
        MyDataGrid.DataBind()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Public Sub store_cd_update()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'MCCL Dec 8, 2017
        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s order by store_cd "
        End If
        If (isNotEmpty(userType) And userType > 0) Then
            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
              "from MCCL_STORE_GROUP a, store s, store_grp$store b "
            If isNotEmpty(userType) And userType = 5 Then
                'MCCL 8 validation Franchise leons 
                SQL = SQL & "where a.user_id in (3,:userType) and "
            Else
                SQL = SQL & "where a.user_id = :userType and "
            End If
            SQL = SQL & "s.store_cd = b.store_cd and " +
              "a.store_grp_cd = b.store_grp_cd "
            SQL = SQL & " order by store_cd"
        End If

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            If (isNotEmpty(userType) And userType > 0) Then
                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType
            End If
           
            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_grp_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
                .SelectedIndex = 0
            End With
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

End Class
