<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Order.aspx.vb" Inherits="Order"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label640 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtvendor" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right;">
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:LibResources, Label550 %>"></asp:Label>
            </td>
            <td width="66">
                <asp:TextBox ID="txtsku" runat="server" Width="65px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LibResources, Label653 %>"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtvsn" runat="server" Width="250px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:LibResources, Label158 %>"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtdesc" runat="server" Width="250px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:LibResources, Label427 %>"></asp:Label>
            </td>
            <td style="width: 48px">
                <asp:TextBox ID="txtprice" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right;">
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:LibResources, Label293 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtminor" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:LibResources, Label70 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtcategory" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right;">
                <asp:Label ID="Label8" runat="server" Text="<%$ Resources:LibResources, Label575 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtstyle" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:LibResources, Label209 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtfinish" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right;">
                <asp:Label ID="Label10" runat="server" Text="<%$ Resources:LibResources, Label549 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsize" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="Label11" runat="server" Text="<%$ Resources:LibResources, Label116 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtcover" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right;">
                <asp:Label ID="Label12" runat="server" Text="<%$ Resources:LibResources, Label223 %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtgrade" runat="server" Width="47px" CssClass="style5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <dx:ASPxButton ID="btnsubmit" runat="server" Text="Lookup Inventory">
                </dx:ASPxButton>
            </td>
            <td align="left" colspan="2">
                <dx:ASPxButton ID="btn_Special" runat="server" Text="<%$ Resources:LibResources, Label566 %>">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="4">
                <dx:ASPxCheckBox ID="chk_dropped" runat="server" Checked="true" Text="<%$ Resources:LibResources, Label171 %>">
                </dx:ASPxCheckBox>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_Errors" runat="server" BackColor="White" Font-Bold="True" ForeColor="Red"
        Style="position: relative" Width="304px"></asp:Label><br />
    <br />
    <br />
</asp:Content>
