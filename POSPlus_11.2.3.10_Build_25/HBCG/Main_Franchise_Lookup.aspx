<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main_Franchise_Lookup.aspx.vb"
    Inherits="Main_Franchise_Lookup" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Div1" runat="server">
        <dx:ASPxGridView ID="GridView1" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="STORE_CD;WHSE_STORE_CD">
            <SettingsPager Visible="False">
            </SettingsPager>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Store Cd" FieldName="STORE_CD" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
             <%--   <dx:GridViewDataTextColumn Caption="Comp Cd" FieldName="CO_CD" VisibleIndex="1">
                </dx:GridViewDataTextColumn> --%>
                <dx:GridViewDataTextColumn Caption="Tax Cd" FieldName="TAX_CD" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Whse Store Cd" FieldName="WHSE_STORE_CD" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
            <%--    <dx:GridViewDataTextColumn Caption="Whse Comp Cd" FieldName="WHSE_CO_CD" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                --%>
                <dx:GridViewDataTextColumn Caption="Excpt" FieldName="EXCP" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="6">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="Select" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <table width="98%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="40%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="Search Again">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div2">
        <dx:ASPxLabel ID="Label1" runat="server" Text="" Visible="false">
        </dx:ASPxLabel>
    </div>
</asp:Content>
