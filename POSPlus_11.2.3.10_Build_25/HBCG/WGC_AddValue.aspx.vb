Imports System.Xml
Imports System.Data.OracleClient
Imports World_Gift_Utils
Imports HBCG_Utils

Partial Class WGC_AddValue
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theCustBiz As CustomerBiz = New CustomerBiz()

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.Theme = ConfigurationManager.AppSettings("app_theme").ToString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        TextBox1.Focus()   'this is the Account field
        PopulateTranTypes()
        PopulateStores()
        If Find_Security("ARUTI", Session("emp_cd")) = "N" Then
            cbo_trn_tp.Enabled = False
            txt_cust_cd.Enabled = False
            txt_description.Enabled = False
            cbo_store_cd.Enabled = False
            tbl_aruti.Visible = False
        End If
    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        ClearObjects()
        lbl_response.Text = ""

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Not IsNumeric(txt_amount.Text) Then
            lbl_response.Text = "You must enter a valid amount"
            Exit Sub
        ElseIf TextBox1.Text & "" = "" Then
            lbl_response.Text = "You must enter a valid account"
            Exit Sub
        ElseIf txt_del_doc_num.Text & "" = "" And txt_cust_cd.Text & "" = "" Then
            lbl_response.Text = "You must enter a valid sales order or customer code to continue."
            Exit Sub
        End If

        If txt_cust_cd.Text & "" <> "" Then
            If cbo_trn_tp.Value = "" Then
                lbl_response.Text = "You must select a transaction type code."
                Exit Sub
            End If
            If cbo_store_cd.Value = "" Then
                lbl_response.Text = "You must select a store code."
                Exit Sub
            End If
        End If

        Dim merchant_id As String = Lookup_Merchant("GC", Session("home_store_cd"), "GC")
        If String.IsNullOrEmpty(merchant_id) Then
            lbl_response.Text = "No Gift Card Merchant ID found for your home store code"
            Exit Sub
        End If

        Dim xml_response As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id, "addvalue", TextBox1.Text, txt_amount.Text)
        If IsNumeric(xml_response.InnerText.Trim) Then
            theSystemBiz.SaveAuditLogComment("GC_BALANCE_UPDATE", TextBox1.Text & " New Card Balance: " & FormatCurrency(CDbl(xml_response.InnerText), 2))
            lbl_response.Text = "New Card Balance: " & FormatCurrency(CDbl(xml_response.InnerText), 2)
        Else
            theSystemBiz.SaveAuditLogComment("GC_BALANCE_FAILURE", TextBox1.Text & " Transaction Failed: " & xml_response.InnerText)
            lbl_response.Text = "Transaction Failed: " & xml_response.InnerText
            Exit Sub
        End If

        '==== if reached this point, then the authorization completed, saves record of that ====
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
        Try
            dbConnection.Open()

            If txt_del_doc_num.Text & "" <> "" Then
                dbCommand.CommandText = "UPDATE SO SET USR_FLD_5='X' WHERE DEL_DOC_NUM=:DEL_DOC"
                dbCommand.Parameters.Add(":DEL_DOC", OracleType.VarChar)
                dbCommand.Parameters(":DEL_DOC").Value = UCase(txt_del_doc_num.Text)
                dbCommand.ExecuteNonQuery()
                theSystemBiz.SaveAuditLogComment("GC_SALE_ACTIVATION", TextBox1.Text & " Updated SO#: " & txt_del_doc_num.Text)
            ElseIf txt_cust_cd.Text & "" <> "" Then
                Dim sql, sql2 As String
                sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, EMP_CD_OP, ACCT_NUM, DES)"
                sql2 = " VALUES('" & txt_comp_cd.Text & "','" & txt_cust_cd.Text
                sql2 = sql2 & "','" & cbo_store_cd.Value & "','" & cbo_trn_tp.Value & "', "
                sql2 = sql2 & CDbl(FormatNumber(txt_amount.Text, 2)) & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),"
                sql2 = sql2 & "'T', 'O', '" & Session("emp_cd") & "','" & TextBox1.Text & "'"
                sql2 = sql2 & ",'" & Replace(txt_description.Text, "'", "''") & "')"
                sql = sql & sql2
                dbCommand.CommandText = UCase(sql)
                dbCommand.ExecuteNonQuery()
                theSystemBiz.SaveAuditLogComment("GC_AR_ACTIVATION", TextBox1.Text & " Updated Cust: " & txt_description.Text & "(" & cbo_trn_tp.Value & ") for " & txt_amount.Text)
            End If
            ClearObjects()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
    End Sub

    Private Sub ClearObjects()
        TextBox1.Text = ""
        txt_swiped.Text = "N"
        txt_amount.Text = ""
        txt_amount.Enabled = True
        cbo_trn_tp.Value = ""
        txt_del_doc_num.Text = ""
        txt_cust_cd.Text = ""
        txt_description.Text = ""
        txt_comp_cd.Text = ""
        cbo_store_cd.Value = ""
        TextBox1.Enabled = True
        TextBox1.Focus()
    End Sub

    ''' <summary>
    ''' Populates the transaction types
    ''' </summary>
    Public Sub PopulateTranTypes()

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()
        Dim trnTps As New DataSet

        sql.Append("SELECT trn_tp_cd, des || ' (' || trn_tp_cd || ')' as full_desc")
        sql.Append(" FROM ar_trn_tp WHERE user_defined='Y' AND DC_CD='C' ")
        sql.Append(" ORDER BY des")

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql.ToString(), dbConnection)
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(trnTps)
            With cbo_trn_tp
                .DataSource = trnTps
                .ValueField = "trn_tp_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            dbAdapter.Dispose()
            dbCommand.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Populates the Stores drop-down
    ''' </summary>
    Public Sub PopulateStores()
        Try
            Dim stores As DataSet = theSalesBiz.GetStores()
            With cbo_store_cd
                .DataSource = stores
                .ValueField = "store_cd"
                .TextField = "store_des"
                .DataBind()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Makes sure the customer code passed in, corresponds to a customer in the system
    ''' </summary>
    ''' <param name="custCd">the customer to check if it exists in the system</param>
    ''' <returns>The name of the customer if found or an empty string if no matches found</returns>
    Private Function GetCustomerName(ByVal custCd As String) As String

        Dim custName As String = String.Empty

        Dim custInfo As CustomerDtc = theCustBiz.GetCustomer(custCd)
        If (custInfo.custCd.isNotEmpty()) Then
            custName = custInfo.lname & " " & custInfo.custCd
        Else
            lbl_response.Text = "Invalid Customer Code."
        End If
        Return custName
    End Function

    ''' <summary>
    ''' Finds out if the document passed is considered to be paid in full
    ''' </summary>
    ''' <param name="delDocNum">the document to find out if paid in full</param>
    ''' <param name="ordTpCd">the type of order this document represents</param>
    ''' <param name="custCd">the customer code attached to this document</param>
    ''' <param name="storeCd">the written store code where order was keyed</param>
    ''' <param name="finCo">the finance company code</param>
    ''' <param name="finApprover">the approver of the finance</param>
    ''' <param name="finAmt">the finance amount </param>
    ''' <returns>TRUE if order is considered paid in full; FALSE otherwise</returns>
    Private Function IsPaidInFull(ByVal delDocNum As String, ByVal ordTpCd As String, ByVal custCd As String,
                                  ByVal storeCd As String, ByVal finCo As String, ByVal finApprover As String,
                                  ByVal finAmt As Double) As Boolean
        Dim pifReq As New PaidInFullReq
        pifReq.delDocNum = delDocNum
        pifReq.ordTpCd = ordTpCd
        pifReq.custCd = custCd
        pifReq.writtenStoreCd = storeCd
        pifReq.finCoCd = finCo
        pifReq.finApp = finApprover
        pifReq.origFinAmt = finAmt
        Dim paidInFull As Boolean = theSalesBiz.IsPaidInFull(pifReq)

        Return paidInFull
    End Function

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        If InStr(TextBox1.Text, "?") > 0 Then
            Dim sCC As Array = Split(TextBox1.Text.ToString, "?")
            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 1)
            TextBox1.Enabled = False
            txt_swiped.Text = "Y"
        Else
            txt_swiped.Text = "N"
        End If
    End Sub

    Protected Sub txt_del_doc_num_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_del_doc_num.TextChanged

        Dim validDelDocNum As Boolean = False
        Dim delDocNum As String = UCase(txt_del_doc_num.Text)
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbSql As String = "SELECT STAT_CD, ORD_TP_CD, CUST_CD, SO_STORE_CD, FIN_CUST_CD, APPROVAL_CD, NVL(ORIG_FI_AMT,0) AS ORIG_FI_AMT FROM SO WHERE DEL_DOC_NUM = :delDocNum"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)

            '--- First finds out if the Order is Final and Paid in Full, if not gift cards CANNOT be activated
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then

                Dim statCd As String = If(IsDBNull(dbReader.Item("STAT_CD")), "", dbReader.Item("STAT_CD").ToString)
                Dim typeCd As String = If(IsDBNull(dbReader.Item("ORD_TP_CD")), "", dbReader.Item("ORD_TP_CD").ToString)
                Dim custCd As String = If(IsDBNull(dbReader.Item("CUST_CD")), "", dbReader.Item("CUST_CD").ToString)
                Dim storCd As String = If(IsDBNull(dbReader.Item("SO_STORE_CD")), "", dbReader.Item("SO_STORE_CD").ToString)
                Dim finCo As String = If(IsDBNull(dbReader.Item("FIN_CUST_CD")), "", dbReader.Item("FIN_CUST_CD").ToString)
                Dim finApp As String = If(IsDBNull(dbReader.Item("APPROVAL_CD")), "", dbReader.Item("APPROVAL_CD").ToString)
                Dim finAmt As Double = If(IsDBNull(dbReader.Item("ORIG_FI_AMT")), 0, CDbl(dbReader.Item("ORIG_FI_AMT")))

                If (AppConstants.Order.STATUS_FINAL <> statCd) Then
                    lbl_response.Text = "This order cannot be used because it is not in a FINAL state yet."

                ElseIf (Not IsPaidInFull(delDocNum, typeCd, custCd, storCd, finCo, finApp, finAmt)) Then
                    lbl_response.Text = "This order cannot be used because it is not Paid in Full yet."

                Else
                    dbSql = "SELECT NVL(SUM(QTY),0) AS QTY, NVL(SUM(UNIT_PRC),0) AS GIFT_TOTAL FROM SO a, SO_LN b "
                    dbSql = dbSql & "WHERE a.DEL_DOC_NUM = b.DEL_DOC_NUM AND b.VOID_FLAG='N' AND a.STAT_CD <> 'V' "
                    dbSql = dbSql & "AND b.ITM_CD = :itmCd AND NVL(a.USR_FLD_5,'E') != 'X' AND a.DEL_DOC_NUM = :delDocNum"

                    dbCommand.CommandText = dbSql
                    dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
                    dbCommand.Parameters(":itmCd").Value = ConfigurationManager.AppSettings("gift_card_sku").ToString()

                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If dbReader.Read() Then
                        If (CInt(dbReader.Item("QTY")) = 0) Then
                            lbl_response.Text = "No valid gift card sales on the provided sales order."
                        Else
                            txt_amount.Text = dbReader.Item("GIFT_TOTAL").ToString
                            txt_amount.Enabled = False
                            lbl_response.Text = "Sales Order " & delDocNum & " found with $" & txt_amount.Text & " in sales"
                            validDelDocNum = True
                        End If
                    End If
                End If
            Else
                lbl_response.Text = "This order cannot be found in the system."
            End If

            dbReader.Close()
            dbCommand.Dispose()

            If (Not validDelDocNum) Then
                txt_del_doc_num.Text = String.Empty
                txt_del_doc_num.Focus()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try
    End Sub

    Protected Sub txt_cust_cd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cust_cd.TextChanged
        txt_amount.Enabled = True
        Dim custName As String = GetCustomerName(txt_cust_cd.Text)
        If (String.IsNullOrEmpty(custName)) Then
            txt_description.Text = String.Empty
            txt_cust_cd.Text = String.Empty
            txt_cust_cd.Focus()
        Else
            txt_description.Text = custName
        End If
    End Sub

    Protected Sub cbo_trn_tp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_trn_tp.SelectedIndexChanged
        txt_amount.Enabled = True
        txt_del_doc_num.Text = String.Empty
    End Sub

    Protected Sub cbo_store_cd_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store_cd.SelectedIndexChanged

        txt_amount.Enabled = True
        If cbo_store_cd.Value & "" <> "" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String = "SELECT CO_CD FROM STORE WHERE STORE_CD=:STORE_CD"
            Try
                dbConnection.Open()
                dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                dbCommand.Parameters.Add(":STORE_CD", OracleType.VarChar)
                dbCommand.Parameters(":STORE_CD").Value = cbo_store_cd.Value
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If dbReader.Read() Then
                    txt_comp_cd.Text = dbReader.Item("CO_CD").ToString
                Else
                    txt_comp_cd.Text = String.Empty
                End If
                dbReader.Close()
                dbCommand.Dispose()
            Catch ex As Exception
                Throw ex
            Finally
                dbConnection.Close()
            End Try
        End If
    End Sub

End Class
