<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="testmac.aspx.vb" Inherits="testmac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="text-align: center" id="login_body">
        &nbsp;&nbsp;
        <img src="images/warning.jpg" style="font-size: 16pt" />
        <span style="font-size: 16pt">&nbsp;
            <br />
            &nbsp;&nbsp;System Setup Issue
            <br />
            <br />
            <span style="font-size: 10pt">The system has either not been initially setup or a system
                configuration has taken place that was unexpected. Please call RedPrairie support
                to rectify the issue.
                <br />
                <br />
                <asp:TextBox ID="TextBox1" runat="server" Height="22px" TextMode="Password"></asp:TextBox>
                <asp:Button ID="btn_unlock" runat="server" Text="Unlock" /></span></span>
    </div>
    <div id="auth_body">
        <asp:DropDownList ID="cbo_NICS" runat="server" AutoPostBack="true">
        </asp:DropDownList>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
