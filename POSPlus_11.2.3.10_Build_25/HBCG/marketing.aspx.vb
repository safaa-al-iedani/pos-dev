Imports System.Data
Imports System.Data.OracleClient

Partial Class discount
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = True Then
            If ck_No_Mark.Checked = False Then
                If Session("mkt_sub_cd") & "" <> "" And Session("mark_cd") <> 5959 Then
                    populate_gridview()
                    ck_No_Mark.Checked = False
                End If
            End If
        Else
            If Session("mkt_sub_cd") & "" <> "" And Session("mark_cd") <> 5959 Then
                populate_gridview()
                ck_No_Mark.Checked = False
            End If
        End If
        If Not IsPostBack = True Then
            ' populate the Market Category drop down
            Mrkt_Cat_Populate()
        End If

    End Sub

    Public Sub Mrkt_Cat_Populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim MyDataReader As OracleDataReader

        If ck_No_Mark.Checked = True Then
            Session("mark_cd") = 5959
        Else
            Session("mark_cd") = mark_cd.SelectedIndex
            mark_cd.Enabled = True
        End If
        If Session("mark_cd") = 5959 Then
            mark_cd.Enabled = False
        Else
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT MKT_CAT, DES FROM MKT_CAT ORDER BY MKT_CAT"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Session("mark_cd") = mark_cd.SelectedIndex

            mark_cd.Items.Clear()

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    mark_cd.DataSource = ds
                    mark_cd.DataValueField = "MKT_CAT"
                    mark_cd.DataBind()
                Else
                    mark_cd.Enabled = False
                    mark_cd.Visible = False
                    ck_No_Mark.Checked = True
                    ck_No_Mark.Enabled = False
                End If
                If Session("mark_cd") & "" = "" Then
                    mark_cd.Items.Insert(0, "Select Marketing Code")
                    mark_cd.Items.FindByText("Select Marketing Code").Value = ""
                    mark_cd.SelectedIndex = 0
                Else
                    mark_cd.SelectedIndex = Session("mark_cd")
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Public Sub mark_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mark_cd.SelectedIndexChanged

        'Populate 2nd drop down with the selected value from Mkt_Cat dropdown
        Dim mkt_cat As String
        mkt_cat = mark_cd.SelectedValue
        Session("mkt_cat") = mkt_cat & "," & Session("mkt_cat")
        Mrkt_Sub_Cat_Populate(mark_cd.SelectedValue)

    End Sub

    Public Sub Mrkt_Sub_Cat_Populate(ByVal mark_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT MKT_CD,DES FROM MKT_CD where mkt_Cat = '" & mark_cd & "' ORDER BY MKT_CAT"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                mark_sub_cat.Enabled = True
                mark_sub_cat.DataSource = ds
                mark_sub_cat.DataValueField = "MKT_CD"
                mark_sub_cat.DataBind()
                mark_sub_cat.Items.Insert(0, "Select Category Code")
                mark_sub_cat.Items.FindByText("Select Category Code").Value = ""
                mark_sub_cat.SelectedIndex = 0
            Else
                mark_sub_cat.Enabled = False
            End If


            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub ck_No_Mark_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ck_No_Mark.CheckedChanged

        If ck_No_Mark.Checked = True Then
            If mark_cd.Enabled = True Then
                mark_cd.SelectedIndex = 0
            End If
            If mark_sub_cat.Enabled = True Then
                mark_sub_cat.SelectedIndex = 0
            End If
            mark_cd.Enabled = False
            mark_sub_cat.Enabled = False
            Session("mkt_sub_cd") = ""
            Session("mkt_cat") = ""
            Gridview1.Visible = False
        Else
            If ck_No_Mark.Checked = False Then
                mark_cd.Enabled = True
                Mrkt_Cat_Populate()
                Mrkt_Sub_Cat_Populate(mark_cd.SelectedValue)
            End If
        End If

    End Sub

    Protected Sub mark_sub_cat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mark_sub_cat.SelectedIndexChanged

        'insert values into temp table
        Get_Mkt_Cd()

    End Sub

    Public Sub Get_Mkt_Cd()

        Dim mkt_cd As String
        mkt_cd = mark_sub_cat.SelectedValue
        Session("mkt_sub_cd") = mkt_cd & "," & Session("mkt_sub_cd")
        'Session("mkt_sub_cd") = "'" + Session("mkt_sub_cd").replace(",", "','") + "'"
        'Session("mkt_sub_cd") = "(" & Session("mkt_sub_cd") & ")"
        If mark_sub_cat.SelectedIndex = 0 Then
            ' do nothing
        Else
            mark_cd.SelectedIndex = 0
            mark_sub_cat.SelectedIndex = 0
            populate_gridview()
        End If

    End Sub

    Public Sub populate_gridview()
        Dim mkt_Cd As String
        Dim mkt_cat As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        ds = New DataSet
        Gridview1.Visible = True
        mkt_Cd = Session("mkt_sub_cd")
        mkt_Cd = "'" & Replace(mkt_Cd, ",", "','") & "'"
        mkt_cat = Session("mkt_cat")
        mkt_cat = "'" & Replace(mkt_cat, ",", "','") & "'"
        Dim Marketing_Code
        Marketing_Code = Split(mkt_Cd, ",")
        Dim Category_Code
        Category_Code = Split(mkt_cat, ",")
        Dim x As Integer

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = ""
        For x = LBound(Marketing_Code) To UBound(Marketing_Code) - 1
            If x = 0 Then
                sql = "SELECT MKT_CD.MKT_CAT, MKT_CD.MKT_CD, MKT_CD.DES, MKT_CAT.DES As MKT_DES FROM MKT_CD, MKT_CAT where MKT_CD.mkt_cd = " & Marketing_Code(x) & " and MKT_CD.MKT_CAT=MKT_CAT.MKT_CAT and MKT_CD.mkt_cat = " & Category_Code(x)
            Else
                If Marketing_Code(x) & "" <> "''" Then
                    sql = sql & " UNION SELECT MKT_CD.MKT_CAT, MKT_CD.MKT_CD, MKT_CD.DES, MKT_CAT.DES As MKT_DES FROM MKT_CD, MKT_CAT where MKT_CD.mkt_cd = " & Marketing_Code(x) & " and MKT_CD.MKT_CAT=MKT_CAT.MKT_CAT and MKT_CD.mkt_cat = " & Category_Code(x)
                End If
            End If
        Next    'x
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        dv = ds.Tables(0).DefaultView
        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = dv
                Gridview1.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim deletecd As String
        Dim mkt_cd As String

        deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
        mkt_cd = Session("mkt_sub_cd")
        mkt_cd = Replace(mkt_cd, deletecd, "")
        mkt_cd = Replace(mkt_cd, ",,", ",")
        Session("mkt_sub_cd") = mkt_cd
        If mkt_cd = "," Then
            Gridview1.Visible = False
        Else
            populate_gridview()
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub
End Class
