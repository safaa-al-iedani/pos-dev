Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils
Imports System.Security.Cryptography
Imports System.Text.Encoding
Imports System.String
Imports System.Web.HttpUtility


Partial Class LeonsCustom
    Inherits POSBasePage

    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theCustBiz As CustomerBiz = New CustomerBiz()
    Private theSalBiz As SalesBiz = New SalesBiz()
    Private theTranBiz As TransportationBiz = New TransportationBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim helper As GridViewHelper = New GridViewHelper(sender)
        'helper.RegisterGroup("SUB_SYS", true, true);
        'helper.RegisterGroup("PRG_NM", true, true);
        'helper.ApplyGroupSort();

        If Not IsPostBack Then
            GridView1_Binddata()
            GridView2_Binddata()
        End If

    End Sub

    Private Sub GridView1_Binddata()

        ASPlblError.Visible = False

        Dim row As GridViewRow = GridView1.SelectedRow

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                              ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 

        Dim empCd = Session("emp_cd")

        ' March 07 distinct added

        SQL = "select distinct * from ( select prg_nm, sub_sys, des from priv_web "
        SQL = SQL & "where std_web_security.isOKtoAccess('" & empCd & "',prg_nm) = 'Y' and priv_tp = 'ACC' and LINK_EXT_FLAG='N' "
        SQL = SQL & "ORDER BY LINK_EXT_FLAG ,SUB_SYS, des )"

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)


        Dim ds1 As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds1)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            If MyDataReader.Read Then
                Try

                Catch ex As Exception
                    conn.Close()
                End Try

                MyDataReader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        GridView1.DataSource = ds1

        GridView1.Visible = True

        If Not GridView1.DataSource Is Nothing Then
            GridView1.DataBind()
        End If

        If GridView1.PageCount = 0 Then
            ASPlblError.Text = "Sorry, no records found in PRIV_WEB table."
        End If

    End Sub

    Private Sub GridView2_Binddata()

        ASPlblError.Visible = False

        Dim row As GridViewRow = GridView2.SelectedRow

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                              ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 

        Dim empCd = Session("emp_cd")

        SQL = "select prg_nm, sub_sys, des from priv_web "
        SQL = SQL & "where std_web_security.isOKtoAccess('" & empCd & "',prg_nm) = 'Y' and priv_tp = 'ACC' and LINK_EXT_FLAG='Y' "
        SQL = SQL & "ORDER BY LINK_EXT_FLAG ,SUB_SYS, des "

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)


        Dim ds2 As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            If MyDataReader.Read Then
                Try

                Catch ex As Exception
                    conn.Close()
                End Try

                MyDataReader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try


        'Apply the new security key to link to LFL - mariam - July 11, 2016
        Dim lfl_desc As String
        Dim tablerow() As Data.DataRow

        If ds2.Tables(0).Rows.Count > 0 Then
            lfl_desc = ConfigurationManager.AppSettings("lfleb_desc")
            tablerow = ds2.Tables(0).Select("DES='" & lfl_desc & "'")
            If Not tablerow.Length = 0 Then
                tablerow(0)("prg_nm") = GetSecuredUrltoLFLEB()
            End If
        End If

        'July 11, 2016 - mariam - END

        GridView2.DataSource = ds2

        GridView2.Visible = True

        If Not GridView2.DataSource Is Nothing Then
            GridView2.DataBind()
        End If

        If GridView2.PageCount = 0 Then
            ASPlblError.Text = "Sorry, no records found in PRIV_WEB table."
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView1.PageIndex < (GridView1.PageCount - 1) Then
                    GridView1.PageIndex += 1
                End If
            Case "Prev"
                If GridView1.PageIndex > 0 Then
                    GridView1.PageIndex -= 1
                End If
            Case "Last"
                GridView1.PageIndex = GridView1.PageCount - 1
            Case Else
                GridView1.PageIndex = 0
        End Select

        GridView1_Binddata()

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

        Dim row As GridViewRow = GridView1.SelectedRow

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()
        SQL = "select prg_nm, des from priv_web "
        SQL = SQL & "ORDER BY SUB_SYS "

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            If MyDataReader.Read() Then

            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()


    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        End If
        If Session("MP") & "" <> "" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Function GetSecuredUrltoLFLEB() As String
        'July 11, 2016 - mariam -START
        'Secured Link to new LFL link 
        Dim user As String = String.Empty
        Dim url As String = String.Empty
        Dim urllfleb As String = String.Empty
        Dim currentUTCtime As String = DateTime.UtcNow.ToString()

        ' The secret key known by both parties
        Dim secretKey As String = String.Empty

        user = Session("EMP_INIT")
        secretKey = ConfigurationManager.AppSettings("secretkey_to_lfleb").ToString()

        ' The content to sign is the identity token (the username) appended with the timestamp and secret key
        Dim contentToSignString As String = user & currentUTCtime & secretKey

        ' Create the MD5 hasher and a UTF8Encoding class
        Dim hasher = New MD5CryptoServiceProvider()
        Dim encoder = New UTF8Encoding()

        ' Create the hash
        Dim contentToSignData As Byte() = encoder.GetBytes(contentToSignString)
        Dim signatureData As Byte() = hasher.ComputeHash(contentToSignData)
        Dim signatureString As String = System.Convert.ToBase64String(signatureData)

        urllfleb = ConfigurationManager.AppSettings("urllfleb").ToString()
        url = String.Format(urllfleb & "?user={0}&utcTime={1}&encryptedstring={2}", HtmlEncode(user), HtmlEncode(currentUTCtime), HtmlEncode(signatureString))
        Return url

    End Function



End Class
