<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PtagMaintenance.aspx.vb" Inherits="PtagMaintenance" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table width="850px">
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="right" style="width: 135px">
                            <dx:ASPxLabel ID="lbl_itm_cd" runat="server" meta:resourcekey="lbl_itm_cd"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_itm_cd" runat="server" Width="150PX" MaxLength="9"></dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_search" runat="server" Text="Search" Width="100px"></dx:ASPxButton>
                        </td>
                        <td align="left" valign="bottom" style="width: 2px">
                            <dx:ASPxButton ID="btn_dummy" runat="server" Text="" Width="1px" Height="1px" Theme="Glass"></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_clear" runat="server" Text="Clear" Width="100"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="2"
                    AutoPostBack="True" Width="850px">
                    <TabPages>
                        <dx:TabPage Text="PRTAG">
                            <ContentCollection>
                                <dx:ContentControl runat="server" Width="800px">
                                    <table>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_origin" runat="server" meta:resourcekey="lbl_origin"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_origin" runat="server" Width="120px" MaxLength="8"></dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_date" runat="server" meta:resourcekey="lbl_date"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dt_date" runat="server" Width="120px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                                    AutoPostBack="True" />
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_size" runat="server" meta:resourcekey="lbl_size"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_size" runat="server" Width="166px" MaxLength="13"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>

                                        <tr>
                                            <td rowspan="2" valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_material" runat="server" meta:resourcekey="lbl_material" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td rowspan="2" colspan="3">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_material1" runat="server" Width="344px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_material2" runat="server" Width="344px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_Vol" runat="server" meta:resourcekey="lbl_Vol" Width="70px"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_vol" runat="server" Width="166px" MaxLength="13">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_weight" runat="server" meta:resourcekey="lbl_weight"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_weight" runat="server" Width="50px" MaxLength="3"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_packaging" runat="server" meta:resourcekey="lbl_packaging"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_packaging" runat="server" Width="50px" MaxLength="1"></dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_stackable" runat="server" meta:resourcekey="lbl_stackable"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_stackable" runat="server" Width="50px" MaxLength="1"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_warranty" runat="server" meta:resourcekey="lbl_warranty" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="7">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_warranty1" runat="server" Width="450px" MaxLength="48"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_warranty2" runat="server" Width="450px" MaxLength="48"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_care" runat="server" meta:resourcekey="lbl_care" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="7">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_care1" runat="server" Width="480px" MaxLength="54"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_care2" runat="server" Width="480px" MaxLength="54"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_protection" runat="server" meta:resourcekey="lbl_protection" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="7">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_protection1" runat="server" Width="235px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_space" runat="server" Text="" Width="10px"></dx:ASPxLabel>
                                                        </td>

                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_protection2" runat="server" Width="235px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_features" runat="server" meta:resourcekey="lbl_features" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="7">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features1" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features2" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features3" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features4" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features5" runat="server" Width="550PX" MaxLength="60"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_comments" runat="server" meta:resourcekey="lbl_comments" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="7">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments1" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments2" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments3" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments4" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btn_save" runat="server" Text="Save" Width="100"></dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_warning_prtag" runat="server" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="PRTAG French">
                            <ContentCollection>
                                <dx:ContentControl runat="server" Width="800px">
                                    <table>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_origin_fr" runat="server" meta:resourcekey="lbl_origin"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_origin_fr" runat="server" Width="120px" MaxLength="8"></dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_date_fr" runat="server" meta:resourcekey="lbl_date"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="3">
                                                <dx:ASPxDateEdit ID="dt_date_fr" runat="server" Width="120px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                                    AutoPostBack="True" />
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_size_fr" runat="server" meta:resourcekey="lbl_size"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_size_fr" runat="server" Width="166px" MaxLength="13"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_material_fr" runat="server" meta:resourcekey="lbl_material" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td rowspan="2" colspan="5">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_material1_fr" runat="server" Width="344px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_material2_fr" runat="server" Width="344px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_Vol_fr" runat="server" meta:resourcekey="lbl_Vol" Width="70px"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_vol_fr" runat="server" Width="50px" MaxLength="3">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_weight_fr" runat="server" meta:resourcekey="lbl_weight"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_weight_fr" runat="server" Width="50px" MaxLength="3"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_packaging_fr" runat="server" meta:resourcekey="lbl_packaging"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_packaging_fr" runat="server" Width="50px" MaxLength="1"></dx:ASPxTextBox>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lbl_stackable_fr" runat="server" meta:resourcekey="lbl_stackable"></dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_stackable_fr" runat="server" Width="50px" MaxLength="1"></dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_warranty_fr" runat="server" meta:resourcekey="lbl_warranty" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="9">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_warranty1_fr" runat="server" Width="450px" MaxLength="48"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_warranty2_fr" runat="server" Width="450px" MaxLength="48"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_care_fr" runat="server" meta:resourcekey="lbl_care" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="9">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_care1_fr" runat="server" Width="480px" MaxLength="54"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_care2_fr" runat="server" Width="480px" MaxLength="54"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_protection_fr" runat="server" meta:resourcekey="lbl_protection" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="9">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_protection1_fr" runat="server" Width="235px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_space_fr" runat="server" Text="" Width="10px"></dx:ASPxLabel>
                                                        </td>

                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_protection2_fr" runat="server" Width="235px" MaxLength="28"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_features_fr" runat="server" meta:resourcekey="lbl_features" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="9">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features1_fr" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features2_fr" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features3_fr" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features4_fr" runat="server" Width="550PX" MaxLength="80"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_features5_fr" runat="server" Width="550PX" MaxLength="60"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <dx:ASPxLabel ID="lbl_comments_fr" runat="server" meta:resourcekey="lbl_comments" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                            <td colspan="9">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments1_fr" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments2_fr" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments3_fr" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_comments4_fr" runat="server" Width="600px" MaxLength="100"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="10">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btn_save_fr" runat="server" Text="Save" Width="100"></dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10">
                                                <dx:ASPxLabel ID="lbl_warning_prtag_fr" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="ITEM OPTIONS">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxGridView ID="GV_item_options" runat="server" AutoGenerateColumns="False" KeyFieldName="ROWID" EnableRowsCache="true"
                                                                OnRowUpdating="GV_row_update" OnRowInserting="GV_row_insert" OnDataBinding="GV_item_option_DataBind"
                                                                OnCellEditorInitialize="GVCellEditorInitialize" Width="790px" Enabled="false">
                                                                <Columns>
                                                                    <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true"></dx:GridViewCommandColumn>
                                                                    <dx:GridViewDataTextColumn Caption="" FieldName="ROWID" Visible="False" VisibleIndex="0">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataComboBoxColumn Caption="TYPE" FieldName="RELATED_TYPE" Visible="True" VisibleIndex="0">
                                                                        <PropertiesComboBox ValueField="RELATED_TYPE">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="CHOICES" Value="CHOICES" Selected="true" />
                                                                                <dx:ListEditItem Text="VTY" Value="VTY" />
                                                                                <dx:ListEditItem Text="WTY" Value="WTY" />
                                                                                <dx:ListEditItem Text="VAP" Value="VAP" />
                                                                            </Items>
                                                                        </PropertiesComboBox>
                                                                    </dx:GridViewDataComboBoxColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Quantity" FieldName="QUANTITY"
                                                                        Visible="True" VisibleIndex="0" EditFormSettings-Visible="False" PropertiesTextEdit-MaxLength="10">
                                                                        <EditFormSettings Visible="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Related Item Code" FieldName="RELATED_ITM_CD" Visible="True" VisibleIndex="0">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="Start Date" FieldName="START_DATE" Visible="True" VisibleIndex="0">
                                                                        <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"></PropertiesDateEdit>
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataDateColumn Caption="End Date" FieldName="END_DATE" Visible="True" VisibleIndex="0">
                                                                        <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"></PropertiesDateEdit>
                                                                    </dx:GridViewDataDateColumn>
                                                                </Columns>
                                                                <SettingsEditing Mode="Inline"></SettingsEditing>
                                                                <Settings UseFixedTableLayout="True" />
                                                                <Settings ShowFooter="true" />
                                                                <SettingsPager Position="Bottom">
                                                                    <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                                                                </SettingsPager>
                                                                <SettingsDataSecurity AllowDelete="False" />
                                                                <Templates>
                                                                    <FooterRow>
                                                                        <dx:ASPxLabel ID="GV_Footer_label_io" runat="server" ClientInstanceName="GV_Footer_label_io" OnInit="GV_Footer_label_init_io" ForeColor="Red" Text=" "></dx:ASPxLabel>
                                                                    </FooterRow>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="FEATURES AND BENEFITS">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxGridView ID="GV_feature_benefits" runat="server" AutoGenerateColumns="False" KeyFieldName="ROWID" EnableRowsCache="true"
                                                                OnRowUpdating="GV_fb_row_update" OnRowInserting="GV_fb_row_insert" OnRowDeleting="GV_fb_row_delete"
                                                                OnCellEditorInitialize="GVFBCellEditorInitialize" Width="790px" Enabled="false" OnDataBinding="GV_feature_benefits_DataBind">
                                                                <Columns>
                                                                    <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true"></dx:GridViewCommandColumn>
                                                                    <dx:GridViewDataTextColumn Caption="" FieldName="ROWID" Visible="False" VisibleIndex="0" Width="250px">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Item Code" FieldName="ITM_CD" Visible="True" VisibleIndex="1" Width="200px" PropertiesTextEdit-MaxLength="0">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataComboBoxColumn Caption="Feature Code" FieldName="FEAT_CD" Visible="True" 
                                                                        VisibleIndex="2" Width="350px">
                                                                        <PropertiesComboBox ValueField="FEAT_CD" TextField="FEAT_CD" IncrementalFilteringMode="Contains">                                                                           
                                                                        </PropertiesComboBox>
                                                                    </dx:GridViewDataComboBoxColumn>
                                                                </Columns>
                                                                <SettingsEditing Mode="Inline"></SettingsEditing>
                                                                <Settings UseFixedTableLayout="True" />
                                                                <SettingsDataSecurity AllowDelete="True" />
                                                                <Settings ShowFooter="true" />
                                                                <SettingsBehavior ConfirmDelete="true" />
                                                                <SettingsPager Position="Bottom">
                                                                    <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <FooterRow>
                                                                        <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" Text="" OnInit="GV_Footer_label_init" ForeColor="Red"></dx:ASPxLabel>
                                                                    </FooterRow>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" ClientInstanceName="clientLabel" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="DIMENSIONS">
                            <ContentCollection>
                                <dx:ContentControl runat="server" Width="800px">
                                    <table width="650px">
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <dx:ASPxLabel ID="lbl_title_product_dimensions" runat="server" EncodeHtml="false" meta:resourcekey="lbl_product_dimensions"></dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_dim_size" runat="server" meta:resourcekey="lbl_dim_size" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_size" runat="server" Width="120px" MaxLength="30"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_dim_cover" runat="server" meta:resourcekey="lbl_cover" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_cover" runat="server" Width="120px" MaxLength="30"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_dim_grade" runat="server" meta:resourcekey="lbl_grade" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_grade" runat="server" Width="120px" MaxLength="30"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <dx:ASPxLabel ID="lbl_shipping_dimensions" runat="server" EncodeHtml="false" meta:resourcekey="lbl_shipping_dimensions"></dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_size_id" runat="server" meta:resourcekey="lbl_size_id" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_size_id" runat="server" Width="120px" MaxLength="12"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_cover_id" runat="server" meta:resourcekey="lbl_cover_id" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_cover_id" runat="server" Width="120px" MaxLength="12"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 100px">
                                                            <dx:ASPxLabel ID="lbl_grade_id" runat="server" meta:resourcekey="lbl_grade_id" EncodeHtml="false"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_dim_grade_id" runat="server" Width="120px" MaxLength="12"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    &nbsp;
                                    <table>
                                        <tr>
                                            <td align="center" style="width: 700px">
                                                <dx:ASPxLabel ID="lbl_dim_msg" runat="server" EncodeHtml="false" meta:resourcekey="lbl_dim_msg" ForeColor="#1061A5"></dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxCheckBox ID="chk_NPT" runat="server" EncodeHtml="false" meta:resourcekey="chk_NPT"></dx:ASPxCheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btn_di_save" runat="server" Width="100" meta:resourcekey="lbl_btn_save"></dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="lbl_warning_dim" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
            </td>
        </tr>
    </table>
</asp:Content>
