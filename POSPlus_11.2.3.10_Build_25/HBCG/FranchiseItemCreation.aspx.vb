Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils


Partial Class FranchiseItemCreation
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_old_crd_num As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_msg.Text = ""

        If isBuyer() = "Y" Then
            BuyerMode()
            If Not IsPostBack Then
                ShowExistingSKUs()
            End If
        Else
            NotaBuyerMode()
        End If

        If IsPostBack Then
            If itm_tp_drp_list.SelectedIndex > 0 Then
                If itm_tp_drp_list.SelectedItem.Value.ToString = "PKG" Then
                    populate_compsku()
                    GridView1.DataBind()
                    GridView2.DataBind()
                End If
            End If

        Else
            new_sku.Visible = False
            new_sku_lbl.Visible = False

            buyer_app_ind_list.Items.Add("")
            buyer_app_ind_list.Items.Add("Y")
            buyer_app_ind_list.Items.Add("N")

            select_option_list.Items.Add("-")
            select_option_list.Items.Add("Create New SKU")
            select_option_list.Items.Add("Edit Existing SKUs")

            populateCoCd()
            populateStrCd()

            srch_str_cd.Enabled = True
            srch_co_cd.Enabled = True
            btn_comp_sku.Visible = False
            populate_ALL_lists()

            spiff_eff_dt.SelectedDate = convStrToDt(Today)
            spiff_eff_dt.VisibleDate = convStrToDt(Today)

            spiff_end_dt.SelectedDate = convStrToDt("31-DEC-2049")
            spiff_end_dt.VisibleDate = convStrToDt("31-DEC-2049")

            If isBuyer() = "Y" Then
                sku_drp_lbl.Visible = True
                sku_drp_list.Visible = True
            Else
                sku_drp_lbl.Visible = False
                sku_drp_list.Visible = False
                buyer_app_ind_list.Visible = False
                buyer_app_lbl.Visible = False
                buyer_cmnt.Visible = False
                buyer_cmnt_lbl.Visible = False
            End If

            btn_save.Visible = False

        End If



    End Sub
    Function isBuyer() As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_tab = " buyer$name "
        Else
            v_tab = " brick_buyer$name "
        End If

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from " & v_tab
        sql = sql & " where emp_cd = '" & Session("emp_cd") & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function getBuyerCd() As String

        Dim v_value As String = ""
        Dim v_tab As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_tab = " buyer$name "
        Else
            v_tab = " brick_buyer$name "
        End If

        Dim sql As String = "SELECT buyer "
        sql = sql & " from " & v_tab
        sql = sql & " where emp_cd = '" & Session("emp_cd") & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("buyer")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Private Sub Enable_all_fields()

        srch_co_cd.Enabled = True
        srch_str_cd.Enabled = True
        ve_cd_drp_list.Enabled = True
        mjr_cd_drp_list.Enabled = True
        mnr_cd_drp_list.Enabled = True
        cat_cd_drp_list.Enabled = True
        style_drp_list.Enabled = True
        itm_tp_drp_list.Enabled = True
        label_drp_list.Enabled = True
        warrant_drp_list.Enabled = True
        treat_drp_list.Enabled = False
        comm_cd_drp_list.Enabled = True

        itm_des.Enabled = True
        vsn.Enabled = True
        grp_name.Enabled = True
        family_cd.Enabled = True
        price_tag_des.Enabled = True
        weight.Enabled = True
        repl_cost.Enabled = True
        retail_price.Enabled = True
        spiff_amt.Enabled = True
        spiff_eff_dt.Enabled = True
        spiff_end_dt.Enabled = True

    End Sub

    Private Sub Disable_all_fields()

        srch_co_cd.Enabled = False
        srch_str_cd.Enabled = False
        ve_cd_drp_list.Enabled = False
        mjr_cd_drp_list.Enabled = False
        mnr_cd_drp_list.Enabled = False
        cat_cd_drp_list.Enabled = False
        style_drp_list.Enabled = False
        itm_tp_drp_list.Enabled = False
        label_drp_list.Enabled = False
        warrant_drp_list.Enabled = False
        treat_drp_list.Enabled = False
        comm_cd_drp_list.Enabled = False

        itm_des.Enabled = False
        vsn.Enabled = False
        grp_name.Enabled = False
        family_cd.Enabled = False
        price_tag_des.Enabled = False
        weight.Enabled = False
        repl_cost.Enabled = False
        retail_price.Enabled = False
        spiff_amt.Enabled = False
        spiff_eff_dt.Enabled = False
        spiff_end_dt.Enabled = False

    End Sub
    Private Sub BuyerMode()

        select_option_lbl.Visible = False
        select_option_list.Visible = False

        buyer_app_ind_list.Visible = True
        buyer_app_lbl.Visible = True
        buyer_cmnt.Visible = True
        buyer_cmnt_lbl.Visible = True

        sku_drp_lbl.Visible = True
        sku_drp_list.Visible = True


    End Sub
    Private Sub NotaBuyerMode()

        buyer_app_ind_list.Enabled = False
        buyer_cmnt.Enabled = False

    End Sub
    Private Sub CreateNewSKU()

        new_sku.Visible = True
        new_sku_lbl.Visible = True

        sku_drp_lbl.Visible = False
        sku_drp_list.Visible = False

        buyer_app_ind_list.Visible = False
        buyer_app_lbl.Visible = False
        buyer_cmnt.Visible = False
        buyer_cmnt_lbl.Visible = False

        ve_cd_drp_list.SelectedIndex = 0
        mjr_cd_drp_list.SelectedIndex = 0
        itm_tp_drp_list.SelectedIndex = 0
        comm_cd_drp_list.SelectedIndex = 0
        mnr_cd_drp_list.Items.Clear()
        cat_cd_drp_list.Items.Clear()
        style_drp_list.Items.Clear()

        'clear the screen
        new_sku.Text = ""
        new_sku2.Text = ""
        itm_des.Text = ""
        price_tag_des.Text = ""
        vsn.Text = ""
        family_cd.Text = ""
        grp_name.Text = ""
        weight.Text = ""
        repl_cost.Text = ""
        retail_price.Text = ""
        spiff_amt.Text = ""
        spiff_eff_dt.SelectedDate = convStrToDt(Today)
        spiff_eff_dt.VisibleDate = convStrToDt(Today)
        spiff_end_dt.SelectedDate = convStrToDt("31-DEC-2049")
        spiff_end_dt.VisibleDate = convStrToDt("31-DEC-2049")
        Enable_all_fields()
        'clear the screen

        btn_save.Visible = True

    End Sub
    Private Sub ShowExistingSKUs()

        new_sku.Visible = False
        new_sku_lbl.Visible = False

        sku_drp_lbl.Visible = True
        sku_drp_list.Visible = True

        buyer_app_ind_list.Visible = True
        buyer_app_lbl.Visible = True
        buyer_cmnt.Visible = True
        buyer_cmnt_lbl.Visible = True

        populate_SKU_list()

        If sku_drp_list.Items.Count > 1 Then

            If isBuyer() = "Y" Then
                lbl_msg.Text = "Select SKU from the list to Approve/Reject"
            Else
                btn_save.Visible = True
            End If
        Else
            lbl_msg.Text = "No SKUs FOUND"
            btn_save.Visible = False
        End If

    End Sub
    Protected Sub SelectOptionChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If select_option_list.SelectedItem.Value.ToString() = "Create New SKU" Then
            CreateNewSKU()
        ElseIf select_option_list.SelectedItem.Value.ToString() = "Edit Existing SKUs" Then
            ShowExistingSKUs()
        Else
            new_sku.Visible = False
            sku_drp_list.Visible = False
            new_sku_lbl.Visible = False
            sku_drp_lbl.Visible = False

            buyer_app_ind_list.Visible = False
            buyer_app_lbl.Visible = False
            buyer_cmnt.Visible = False
            buyer_cmnt_lbl.Visible = False
            clear_the_screen()
        End If

    End Sub
    Protected Sub ItmTpChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If isBuyer() = "Y" Then
            Exit Sub
        End If

        If itm_tp_drp_list.SelectedIndex > 0 Then
            If itm_tp_drp_list.SelectedItem.Value.ToString() = "PKG" Then
                btn_comp_sku.Visible = True
            Else
                btn_comp_sku.Visible = False
            End If
        Else
            btn_comp_sku.Visible = False
        End If

    End Sub
    Private Function getSQL() As String

        Dim sql As String = ""
        Dim v_tab As String = ""
        Dim v_buyer As String = getBuyerCd()
        Dim v_buyer_sql As String = ""

        If v_buyer & "" = "" Then
            v_buyer_sql = " m.buyer is null ) "
        Else
            v_buyer_sql = " m.buyer = '" & v_buyer & "' ) "
        End If

        If isBuyer() = "Y" Then
            If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
                v_tab = " buyer$mnr "
            Else
                v_tab = " brick_buyer$mnr "
            End If
            sql = "SELECT distinct itm_cd "
            sql = sql & " from fritm_sku "
            sql = sql & " where substr(mnr_cd,1,2) "
            sql = sql & " in (select m.mnr_cd from " & v_tab & " m "
            sql = sql & "     where " & v_buyer_sql
            sql = sql & " and std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd ) = 'Y' "
            sql = sql & " order by itm_cd "
        Else
            sql = " select distinct itm_cd "
            sql = sql & " from fritm_sku "
            sql = sql & " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd) = 'Y' "
            sql = sql & " order by itm_cd "
        End If

        Return sql

    End Function
    Private Sub populate_SKU_list()

        Dim v_where As String = ""

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim v_buyer As String = getBuyerCd
        Dim v_tab As String = ""

        Dim sql As String = getSQL()

        sku_drp_list.Items.Clear()
        sku_drp_list.Items.Insert(0, "SKU")
        sku_drp_list.Items.FindByText("SKU").Value = ""
        sku_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With sku_drp_list
                .DataSource = ds
                .DataValueField = "itm_cd"
                .DataTextField = "itm_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Function getValue(ByVal p_col As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT " & p_col & " col "
        sql = sql & " from fritm_sku "
        sql = sql & " where itm_cd = '" & sku_drp_list.SelectedItem.Value.ToString() & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("col")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Protected Sub Retrieve_data_part1()

        Dim v_co_cd As String = getValue("co_cd")
        srch_co_cd.Items.FindByValue(v_co_cd)
        srch_co_cd.SelectedValue = v_co_cd

        Dim v_str_cd As String = getValue("store_cd")
        srch_str_cd.Items.FindByValue(v_str_cd)
        srch_str_cd.SelectedValue = v_str_cd

        Dim v_ve_cd As String = getValue("ve_cd")
        ve_cd_drp_list.Items.FindByValue(v_ve_cd).Selected = True
        ve_cd_drp_list.SelectedValue = v_ve_cd

        Dim v_mjr_cd As String = getValue("mjr_cd")
        mjr_cd_drp_list.Items.FindByValue(v_mjr_cd).Selected = True
        mjr_cd_drp_list.SelectedValue = v_mjr_cd

        populate_mnr()
        Dim v_mnr_cd As String = getValue("mnr_cd")
        mnr_cd_drp_list.Items.FindByValue(v_mnr_cd).Selected = True
        mnr_cd_drp_list.SelectedValue = v_mnr_cd

        populate_cat()
        populate_style()
        Retrieve_data_part2()

    End Sub
    Protected Sub Retrieve_data_part2()


        Dim v_cat_cd As String = getValue("cat_cd")
        cat_cd_drp_list.Items.FindByValue(v_cat_cd).Selected = True
        cat_cd_drp_list.SelectedValue = v_cat_cd

        Dim v_style_cd As String = getValue("style_cd")
        style_drp_list.Items.FindByValue(v_style_cd).Selected = True
        style_drp_list.SelectedValue = v_style_cd

        Dim v_label_cd As String = getValue("label_cd")
        label_drp_list.Items.FindByValue(v_label_cd).Selected = True
        label_drp_list.SelectedValue = v_label_cd

        Dim v_itm_tp_cd As String = getValue("itm_tp_cd")
        itm_tp_drp_list.Items.FindByValue(v_itm_tp_cd).Selected = True
        itm_tp_drp_list.SelectedValue = v_itm_tp_cd
        If v_itm_tp_cd & "" = "" Or isEmpty(v_itm_tp_cd) Then
            btn_comp_sku.Visible = False
        ElseIf v_itm_tp_cd = "PKG" Then
            populate_compsku()
            btn_comp_sku.Visible = True
        Else
            btn_comp_sku.Visible = False
        End If

        Dim v_warrant As String = getValue("warrant_ind")
        warrant_drp_list.Items.FindByValue(v_warrant).Selected = True
        warrant_drp_list.SelectedValue = v_warrant

        Dim v_treat As String = getValue("treat_ind")
        treat_drp_list.Items.FindByValue(v_treat).Selected = True
        treat_drp_list.SelectedValue = v_treat

        Dim v_comm_cd As String = getValue("comm_cd")
        comm_cd_drp_list.Items.FindByValue(v_comm_cd).Selected = True
        comm_cd_drp_list.SelectedValue = v_comm_cd

        grp_name.Text = getValue("group_name")
        itm_des.Text = getValue("itm_des")
        vsn.Text = getValue("vsn")
        family_cd.Text = getValue("family_cd")
        price_tag_des.Text = getValue("prc_tag_des")
        weight.Text = getValue("weight")
        spiff_amt.Text = getValue("spiff_amt")
        retail_price.Text = getValue("rtl_price")
        repl_cost.Text = getValue("repl_cost")
        buyer_cmnt.Text = getValue("buyer_cmnt")

        Dim v_buyer_app As String = getValue("buyer_app_ind")
        buyer_app_ind_list.Items.FindByValue(v_buyer_app).Selected = True
        buyer_app_ind_list.SelectedValue = v_buyer_app

        Dim v_dt As String = getValue("spiff_eff_dt")
        If v_dt & "" = "" Then
            spiff_eff_dt.SelectedDate = DateTime.MinValue

        Else
            spiff_eff_dt.SelectedDate = v_dt
            spiff_eff_dt.VisibleDate = v_dt

        End If

        v_dt = getValue("spiff_end_dt")
        If v_dt & "" = "" Then
            spiff_end_dt.SelectedDate = DateTime.minvalue

        Else
            spiff_end_dt.SelectedDate = v_dt
            spiff_end_dt.VisibleDate = v_dt

        End If

    End Sub
    Protected Sub SkuSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim v_app_ind As String = ""

        If sku_drp_list.SelectedIndex < 1 Then
            clear_the_screen()
        Else
            Retrieve_data_part1()
            v_app_ind = getValue("buyer_app_ind")
            If isBuyer() = "Y" Then
                Disable_all_fields()
                If v_app_ind & "" = "" Then
                    buyer_app_ind_list.Enabled = True
                    buyer_cmnt.Enabled = True
                    buyer_cmnt.Text = ""
                    btn_save.Visible = True
                Else
                    buyer_app_ind_list.Enabled = False
                    buyer_cmnt.Enabled = False
                    buyer_cmnt.Text = getValue("buyer_cmnt")
                    btn_save.Visible = False
                End If
            Else
                buyer_app_ind_list.Enabled = False
                buyer_cmnt.Enabled = False
                If v_app_ind & "" = "" Then
                    Enable_all_fields()
                    btn_save.Visible = True
                Else
                    Disable_all_fields()
                    btn_save.Visible = False
                End If
            End If

        End If

    End Sub
    Private Sub populateCoCd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        srch_co_cd.Items.Clear()

        Dim SQL = "SELECT distinct co_cd co_cd from store " &
                  " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "', store_cd) = 'Y' "

        srch_co_cd.Items.Insert(0, "CO")
        srch_co_cd.Items.FindByText("CO").Value = ""
        srch_co_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_co_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "co_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populateStrCd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        srch_str_cd.Items.Clear()

        Dim SQL = "SELECT distinct store_cd str_cd from store " &
                  " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "', store_cd) = 'Y' "

        srch_str_cd.Items.Insert(0, "STR")
        srch_str_cd.Items.FindByText("STR").Value = ""
        srch_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "str_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try



    End Sub
    Protected Sub populate_ALL_lists()

        populate_ve_cd()
        populate_mjr()
        populate_itm_tp()
        populate_label()

        warrant_drp_list.Items.Clear()
        treat_drp_list.Items.Clear()

        warrant_drp_list.Items.Add("Y")
        warrant_drp_list.Items.Add("N")

        treat_drp_list.Items.Add("N")
        treat_drp_list.Items.Add("Y")

        populate_comm()

    End Sub
    Private Sub populate_label()

        label_drp_list.Items.Clear()
        label_drp_list.Items.Add("SHW")
        label_drp_list.Items.Add("WHS")

    End Sub
    Private Sub populate_ve_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        ve_cd_drp_list.Items.Clear()

        Dim SQL = "SELECT ve_cd from ve v " &
                  " where std_multi_co2.isvalidvdr('" & Session("emp_init") & "', ve_cd) = 'Y' " &
                  " and exists (select 'x' from ve$ve_tp t " &
                  "                where t.ve_cd = v.ve_cd   " &
                  "                  and t.ve_tp_cd = 'MER') " &
                  "  order by 1 "
        ve_cd_drp_list.Items.Insert(0, "VE:")
        ve_cd_drp_list.Items.FindByText("VE:").Value = ""
        ve_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With ve_cd_drp_list
                .DataSource = ds
                .DataValueField = "ve_cd"
                .DataTextField = "ve_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try



    End Sub
    Private Sub populate_mjr()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        mjr_cd_drp_list.Items.Clear()

        Dim SQL = "SELECT mjr_cd from inv_mjr " &
                  " order by 1 "

        mjr_cd_drp_list.Items.Insert(0, "MJR")
        mjr_cd_drp_list.Items.FindByText("MJR").Value = ""
        mjr_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With mjr_cd_drp_list
                .DataSource = ds
                .DataValueField = "mjr_cd"
                .DataTextField = "mjr_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populate_mnr()


        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        mnr_cd_drp_list.Items.Clear()

        Dim SQL = "SELECT mnr_cd from inv_mnr " &
                  " where mjr_cd = '" & mjr_cd_drp_list.SelectedValue.ToString() & "' " &
                  " order by 1 "

        mnr_cd_drp_list.Items.Insert(0, "MNR")
        mnr_cd_drp_list.Items.FindByText("MNR").Value = ""
        mnr_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With mnr_cd_drp_list
                .DataSource = ds
                .DataValueField = "mnr_cd"
                .DataTextField = "mnr_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populate_cat()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        cat_cd_drp_list.Items.Clear()

        Dim SQL = "SELECT cat_cd from inv_mnr_cat " &
                  " where mnr_cd = '" & mnr_cd_drp_list.SelectedValue.ToString() & "' " &
                  " order by 1 "

        cat_cd_drp_list.Items.Insert(0, "CAT")
        cat_cd_drp_list.Items.FindByText("CAT").Value = ""
        cat_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cat_cd_drp_list
                .DataSource = ds
                .DataValueField = "cat_cd"
                .DataTextField = "cat_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populate_style()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        style_drp_list.Items.Clear()

        Dim SQL = "SELECT style_cd, style_cd||' - '||des des from inv_mnr_style " &
                  " where mnr_cd = '" & mnr_cd_drp_list.SelectedValue.ToString() & "' " &
                  " order by 1 "

        style_drp_list.Items.Insert(0, "STYLE")
        style_drp_list.Items.FindByText("STYLE").Value = ""
        style_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With style_drp_list
                .DataSource = ds
                .DataValueField = "style_cd"
                .DataTextField = "des"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populate_itm_tp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        itm_tp_drp_list.Items.Clear()

        Dim SQL = "SELECT itm_tp_cd,itm_tp_cd||' - '||des des from itm_tp " &
                  " where itm_tp_cd in ('FAB','INV','LAB','MIL','NLN','NLT','PKG')" &
                  " order by 1 "

        itm_tp_drp_list.Items.Insert(0, "TYPE")
        itm_tp_drp_list.Items.FindByText("TYPE").Value = ""
        itm_tp_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With itm_tp_drp_list
                .DataSource = ds
                .DataValueField = "itm_tp_cd"
                .DataTextField = "des"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populate_comm()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        comm_cd_drp_list.Items.Clear()

        Dim SQL = "SELECT comm_cd, comm_cd||' - '||des des from comm_cd " &
                  " order by 1 "

        comm_cd_drp_list.Items.Insert(0, "COMM")
        comm_cd_drp_list.Items.FindByText("COMM").Value = ""
        comm_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With comm_cd_drp_list
                .DataSource = ds
                .DataValueField = "comm_cd"
                .DataTextField = "des"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Protected Sub veCdChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If mjr_cd_drp_list.SelectedIndex > 0 Then
            populate_mnr()
        Else
            mnr_cd_drp_list.Items.Clear()
            cat_cd_drp_list.Items.Clear()
            style_drp_list.Items.Clear()
        End If

    End Sub
    Protected Sub mjrCdChangeded(ByVal sender As Object, ByVal e As System.EventArgs)

        If ve_cd_drp_list.SelectedIndex > 0 Then
            populate_mnr()
        Else
            mnr_cd_drp_list.Items.Clear()
            cat_cd_drp_list.Items.Clear()
            style_drp_list.Items.Clear()
        End If

    End Sub
    Protected Sub mnrcdChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        populate_cat()
        populate_style()
    End Sub
    Protected Sub warrantSelected(ByVal sender As Object, ByVal e As System.EventArgs)

        If warrant_drp_list.SelectedValue.ToString() = "Y" Then
            treat_drp_list.Items.Clear()
            treat_drp_list.Items.Add("N")
        Else
            treat_drp_list.Items.Clear()
            treat_drp_list.Items.Add("Y")
        End If
    End Sub
    Protected Sub clear_the_screen()

        srch_str_cd.Enabled = True
        srch_co_cd.Enabled = True

        If sku_drp_list.SelectedIndex > 0 Then
            sku_drp_list.SelectedIndex = 0
        End If

        ve_cd_drp_list.SelectedIndex = 0
        mjr_cd_drp_list.SelectedIndex = 0
        itm_tp_drp_list.SelectedIndex = 0
        comm_cd_drp_list.SelectedIndex = 0

        mnr_cd_drp_list.Items.Clear()
        cat_cd_drp_list.Items.Clear()
        style_drp_list.Items.Clear()

        new_sku.Text = ""
        new_sku2.Text = ""
        itm_des.Text = ""
        price_tag_des.Text = ""
        vsn.Text = ""
        family_cd.Text = ""
        grp_name.Text = ""
        weight.Text = ""
        repl_cost.Text = ""
        retail_price.Text = ""
        spiff_amt.Text = ""

        spiff_eff_dt.SelectedDate = convStrToDt(Today)
        spiff_eff_dt.VisibleDate = convStrToDt(Today)

        spiff_end_dt.SelectedDate = convStrToDt("31-DEC-2049")
        spiff_end_dt.VisibleDate = convStrToDt("31-DEC-2049")

        If (Not GridView1.DataSource Is Nothing) Then
            GridView1.DataSource = ""
            GridView1.DataBind()
        End If
        If (Not GridView2.DataSource Is Nothing) Then
            GridView2.DataSource = ""
            GridView2.DataBind()
        End If

        select_option_list.SelectedIndex = 0

        If isBuyer() = "N" Then
            new_sku.Visible = False
            sku_drp_list.Visible = False
            new_sku_lbl.Visible = False
            sku_drp_lbl.Visible = False
            
            buyer_app_ind_list.Visible = False
            buyer_app_lbl.Visible = False
            buyer_cmnt.Visible = False
            buyer_cmnt_lbl.Visible = False

            Enable_all_fields()
        Else
            sku_drp_list.Visible = True
            sku_drp_lbl.Visible = True
            buyer_cmnt.Text = ""
            buyer_app_ind_list.SelectedIndex = 0
            populate_SKU_list()
            Disable_all_fields()
        End If

        btn_comp_sku.Visible = False
        ASPxPopupPkgComp.ShowOnPageLoad = False
        btn_save.Visible = True

    End Sub
    Protected Function new_sku_is_valid() As String

        Dim v_sku As String = ""

        If new_sku.Text & "" = "" Then
            v_sku = getNextSKUnumber(ve_cd_drp_list.SelectedItem.Value.ToString())
            If v_sku & "" = "" Or isEmpty(v_sku) Then
                lbl_msg.Text = "Error - SKU number is not generated correctly - please enter a SKU number"
                new_sku.Focus()
                Return "N"
            Else
                new_sku.Text = v_sku
            End If
        Else
            If Len(new_sku.Text) <> 8 Then
                lbl_msg.Text = "Suggested SKU must be 8 characters long - please try again"
                new_sku.Focus()
                Return "N"
            ElseIf SKUalreadyexists() = "Y" Then
                lbl_msg.Text = "Suggest SKU number already exists - please try again"
                new_sku.Focus()
                Return "N"
            End If
        End If

        Return "Y"

    End Function
    Protected Sub btn_reset_dates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset_dates.Click

        spiff_eff_dt.SelectedDate = convStrToDt(Today)
        spiff_eff_dt.VisibleDate = convStrToDt(Today)

        spiff_end_dt.SelectedDate = convStrToDt(Today)
        spiff_end_dt.VisibleDate = convStrToDt(Today)

    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        clear_the_screen()

    End Sub
    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        If select_option_list.SelectedItem.Value.ToString() = "Create New SKU" Then
            If data_is_valid() = "N" Then
                Exit Sub
            Else
                If new_sku_is_valid() = "N" Then
                    Exit Sub
                Else
                    InsertNewSKU()
                End If
            End If
        Else
            If sku_drp_list.SelectedIndex < 1 Then
                lbl_msg.Text = "Select a SKU to proceed"
                sku_drp_list.Focus()
                Exit Sub
            End If
            If isBuyer() = "Y" Then
                If BuyerDataisValid() = "Y" Then
                    BuyerApproveReject()
                Else
                    Exit Sub
                End If
            Else
                UpdateSKU()
            End If

        End If

    End Sub
    Function BuyerDataisValid() As String

        If buyer_app_ind_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select 'Y' to Approve 'N' to Reject this SKU"
            buyer_app_ind_list.Focus()
            Return "N"
        ElseIf buyer_cmnt.Text & "" = "" Or isEmpty(buyer_cmnt.Text) Then
            lbl_msg.Text = "Please enter a comment"
            buyer_cmnt.Focus()
            Return "N"
        Else
            If buyer_app_ind_list.SelectedItem.Value.ToString() = "Y" Then
                If itm_tp_drp_list.SelectedItem.Value.ToString() = "PKG" Then

                    If PKG_has_2COMPSKU() <> "Y" Then
                        lbl_msg.Text = "This Package SKU must have at least two Comp. SKUs to be approved"
                        sku_drp_list.Focus()
                        Return "N"
                    End If

                    If COMPSKUS_R_APPROVED() <> "Y" Then
                        lbl_msg.Text = "All Component SKUs must be Approved first"
                        sku_drp_list.Focus()
                        Return "N"
                    End If

                End If
            End If
        End If

        Return "Y"

    End Function
    Function AddToItemTable(ByVal p_itm As String) As String

        Dim v_res As String
        Dim v_cre_usr As String = getValue("ITM_CRE_USR")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_fritm_sku.ProcessItem"
        sycmd.CommandType = CommandType.StoredProcedure

        sycmd.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
        sycmd.Parameters("p_itm").Size = 10
        sycmd.Parameters.Add("p_usr", OracleType.VarChar).Direction = ParameterDirection.Input
        sycmd.Parameters("p_usr").Size = 10
        sycmd.Parameters.Add("p_res", OracleType.VarChar).Direction = ParameterDirection.Output

        sycmd.Parameters("p_res").Size = 40

        sycmd.Parameters.Add(New OracleParameter("p_itm", OracleType.VarChar)).Value = p_itm
        sycmd.Parameters.Add(New OracleParameter("p_usr", OracleType.VarChar)).Value = v_cre_usr

        Try
            syda.Fill(syds)

        Catch ex As Exception
            Throw New Exception("Error in AddtoItem ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
                conn.Dispose()
            End If
        End Try

        v_res = sycmd.Parameters("p_res").Value.ToString
        sycmd.Cancel()
        sycmd.Dispose()

        If v_res = "" Then
            Return ""
        Else
            Return v_res
        End If


    End Function
    Protected Sub BuyerApproveReject()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_buyer As String = getBuyerCd()
        If v_buyer & "" = "" Or isEmpty(v_buyer) Then
            lbl_msg.Text = "Invalid Buyer - no buyer code exists "
            Exit Sub
        End If

        sql = " update fritm_sku " +
        " set buyer = '" & v_buyer & "'" +
        "    ,buyer_emp_cd = '" & Session("emp_cd") & "' " +
        "    ,buyer_app_ind = '" & buyer_app_ind_list.SelectedItem.Value.ToString() & "' " +
        "    ,buyer_cmnt = '" & UCase(buyer_cmnt.Text) & "' " +
        "    ,buyer_upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" +
        "  where itm_cd = '" & sku_drp_list.SelectedItem.Value.ToString() & "' " +
        "    and std_multi_co2.isvalidstr2('" & Session("emp_Init") & "', store_cd) = 'Y' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        btn_save.Visible = False
        If buyer_app_ind_list.SelectedItem.Value.ToString() = "Y" Then
            Dim v_res As String
            v_res = AddToItemTable(sku_drp_list.SelectedItem.Value.ToString())
            If v_res & "" = "" Then
                lbl_msg.Text = "SKU " & sku_drp_list.SelectedItem.Value.ToString() & " is Approved and added to ITEM"
            Else
                lbl_msg.Text = "Note - " & Left(v_res, 40)
            End If
        Else
            lbl_msg.Text = "SKU " & sku_drp_list.SelectedItem.Value.ToString() & " is marked as Rejected"
        End If

    End Sub

    Function data_is_valid() As String

        If srch_co_cd.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select CO_CD to to continue"
            srch_co_cd.Focus()
            Return "N"
        End If

        If srch_str_cd.SelectedIndex < 1 Then
            lbl_msg.Text = "please select a STORE to continue"
            srch_str_cd.Focus()
            Return "N"
        End If

        If ve_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Vendor Code"
            ve_cd_drp_list.Focus()
            Return "N"
        End If

        If mjr_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Major Code"
            mjr_cd_drp_list.Focus()
            Return "N"
        End If

        If mnr_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Minor Code"
            mnr_cd_drp_list.Focus()
            Return "N"
        End If

        If cat_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Category Code"
            cat_cd_drp_list.Focus()
            Return "N"
        End If

        If itm_tp_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select Item Type"
            itm_tp_drp_list.Focus()
            Return "N"
        End If

        If comm_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Commission Code"
            comm_cd_drp_list.Focus()
            Return "N"
        End If

        If repl_cost.Text & "" = "" Or isEmpty(repl_cost.Text) Then
            lbl_msg.Text = "Invalid Replacement Cost1"
            repl_cost.Focus()
            Return "N"
        Else
            If Not IsNumeric(repl_cost.Text) Then
                lbl_msg.Text = "Invalid Replacement Cost2"
                repl_cost.Focus()
                Return "N"
            Else
                If repl_cost.Text < 1 Or repl_cost.Text > 100000 Then
                    lbl_msg.Text = "Invalid Replacement Cost3"
                    repl_cost.Focus()
                    Return "N"
                End If
            End If
        End If

        If retail_price.Text & "" = "" Or isEmpty(retail_price.Text) Then
            lbl_msg.Text = "Please enter Retail Price"
            retail_price.Focus()
            Return "N"
        Else
            If Not IsNumeric(retail_price.Text) Then
                retail_price.Text = ""
                lbl_msg.Text = "Retail Price is invalid - please correct"
                retail_price.Focus()
                Return "N"
            Else
                If retail_price.Text < 1 Or retail_price.Text > 100000 Then
                    lbl_msg.Text = "Retail Price is invalid - please correct"
                    retail_price.Focus()
                    Return "N"
                End If
            End If
        End If

        If itm_des.Text & "" = "" Or isEmpty(itm_des.Text) Then
            lbl_msg.Text = "Item Description is missing"
            itm_des.Focus()
            Return "N"
        End If

        If vsn.Text & "" = "" Or isEmpty(vsn.Text) Then
            lbl_msg.Text = "VSN is missing"
            vsn.Focus()
            Return "N"
        Else
            If VSNalreadyexists() = "Y" Then
                lbl_msg.Text = "VSN is invalid - it already exists"
                vsn.Focus()
                Return "N"
            End If
        End If

        If price_tag_des.Text & "" = "" Or isEmpty(price_tag_des.Text) Then
            lbl_msg.Text = "Price Tag Description is required"
            price_tag_des.Focus()
            Return "N"
        End If

        If spiff_amt.Text & "" = "" Or isEmpty(spiff_amt.Text) Then
            spiff_eff_dt.SelectedDate = DateTime.MinValue
            spiff_end_dt.SelectedDate = DateTime.minvalue
            Return "Y"
        End If

        If spiff_amt.Text > 0 Then
            If Not IsNumeric(spiff_amt.Text) Then
                lbl_msg.Text = "Spiff Amount is invalid - please correct"
                spiff_amt.Focus()
                Return "N"
            Else
                If spiff_amt.Text < 0 Or spiff_amt.Text > 100000 Then
                    lbl_msg.Text = "Spiff Amount is invalid - please correct"
                    spiff_amt.Focus()
                    Return "N"
                End If
            End If

            If spiff_eff_dt.SelectedDate.Date = DateTime.MinValue Then

                lbl_msg.Text = "Please select Spiff Effective Date"
                spiff_eff_dt.Focus()
                Return "N"
            Else
                Dim v_dt_diff As String = ""
                v_dt_diff = DateDiff(DateInterval.Day, spiff_eff_dt.SelectedDate, Now)

                If v_dt_diff > 0 Then
                    lbl_msg.Text = "Invalid Spiff Effective Date - must be equal to or greater than today's date"
                    spiff_eff_dt.Focus()
                    Return "N"
                End If
            End If

            Dim v_dt_diff1 As String = ""
            v_dt_diff1 = DateDiff(DateInterval.Day, spiff_end_dt.SelectedDate, spiff_eff_dt.SelectedDate)
            If v_dt_diff1 > 0 Then
                lbl_msg.Text = "Invalid Spiff End Date - must be equal to or greater than Spiff Eff. date"
                spiff_end_dt.Focus()
                Return "N"
            End If
            'End If
        End If

        Return "Y"

    End Function

    Protected Sub InsertNewSKU()

        Dim v_style As String = ""
        Dim v_family As String = ""
        Dim v_spiff As String = ""
        Dim v_grp As String = ""
        Dim v_weight As String = ""
        Dim v_repl_cost As String = ""
        Dim v_rtl_prc As String = ""
        Dim v_spiff_amt As String = ""


        If style_drp_list.SelectedIndex < 1 Then
            v_style = "''"
        Else
            v_style = " '" & style_drp_list.SelectedItem.Value.ToString() & "' "
        End If

        If family_cd.Text & "" = "" Or isEmpty(family_cd.Text) Then
            v_family = "''"
        Else
            v_family = "'" & UCase(family_cd.Text) & "' "
        End If

        If weight.Text & "" = "" Or isEmpty(weight.Text) Then
            v_weight = "''"
        Else
            v_weight = "'" & UCase(weight.Text) & "' "
        End If

        If grp_name.Text & "" = "" Or isEmpty(grp_name.Text) Then
            v_grp = "''"
        Else
            v_grp = "'" & UCase(grp_name.Text) & "'"
        End If


        If spiff_amt.Text & "" = "" Or isEmpty(spiff_amt.Text) Then
            v_spiff = 0 & ",null,null "
        Else
            v_spiff_amt = Replace(spiff_amt.Text, "$", "")
            v_spiff = v_spiff_amt &
            " ,to_date('" & FormatDateTime(spiff_eff_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')" &
            " ,to_date('" & FormatDateTime(spiff_end_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        End If

        v_repl_cost = Replace(repl_cost.Text, "$", "")
        v_rtl_prc = Replace(retail_price.Text, "$", "")

        If itm_tp_drp_list.SelectedValue.ToString() = "PKG" Then
            If PKG_has_2COMPSKU() <> "Y" Then
                new_sku2.Text = UCase(new_sku.Text)
                ASPxPopupPkgComp.ShowOnPageLoad = True
                lbl_msg2.Text = "Must add at least 2 items to this package SKU"
                Exit Sub
            End If
        End If

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- insert new sku -------------------------------------
        '---------------------------------------------------------------------------------------------

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into fritm_sku (co_cd,store_cd,itm_cd,itm_des,vsn,ve_cd,mjr_cd,mnr_cd,cat_cd,style_cd," &
          "itm_tp_cd,label_cd,warrant_ind,treat_ind,comm_cd,group_name,family_cd,prc_tag_des,weight, " &
          "spiff_amt,spiff_eff_dt,spiff_end_dt,repl_cost,rtl_price,itm_cre_dt,itm_cre_usr) " &
          "values ('" & srch_co_cd.SelectedItem.Value.ToString() & "' " &
          " , '" & srch_str_cd.SelectedItem.Value.ToString() & "' " &
          " , '" & UCase(new_sku.Text) & "' " &
          " , '" & UCase(itm_des.Text) & "' " &
          " , '" & UCase(vsn.Text) & "' " &
          " , '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & mjr_cd_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & mnr_cd_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & cat_cd_drp_list.SelectedItem.Value.ToString() & "' " &
          " , " & v_style &
          " , '" & itm_tp_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & label_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & warrant_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & treat_drp_list.SelectedItem.Value.ToString() & "' " &
          " , '" & comm_cd_drp_list.SelectedItem.Value.ToString() & "' " &
          " , " & v_grp &
          " , " & v_family &
          " , '" & UCase(price_tag_des.Text) & "' " &
          " , " & v_weight &
          " , " & v_spiff &
          " , " & v_repl_cost &
          " , " & v_rtl_prc &
          " , to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') " &
          " , '" & UCase(Session("emp_init")) & "' )"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        btn_save.Visible = False

        If itm_tp_drp_list.SelectedValue.ToString() <> "PKG" Then
            lbl_msg.Text = "SKU " & new_sku.Text & " is added"
        Else
            lbl_msg.Text = "Package SKU " & new_sku.Text & " is added"
        End If

    End Sub
    Protected Sub UpdateSKU()

        If data_is_valid() = "N" Then
            Exit Sub
        End If

        Dim v_style As String = ""
        Dim v_family As String = ""
        Dim v_spiff As String = ""
        Dim v_grp As String = ""
        Dim v_weight As String = ""
        Dim v_repl_cost As String = ""
        Dim v_rtl_prc As String = ""
        Dim v_eff_dt As String = ""
        Dim v_end_dt As String = ""


        If style_drp_list.SelectedIndex < 1 Then
            v_style = "''"
        Else
            v_style = " '" & style_drp_list.SelectedItem.Value.ToString() & "' "
        End If

        If family_cd.Text & "" = "" Or isEmpty(family_cd.Text) Then
            v_family = "''"
        Else
            v_family = "'" & UCase(family_cd.Text) & "' "
        End If

        If weight.Text & "" = "" Or isEmpty(weight.Text) Then
            v_weight = "''"
        Else
            v_weight = "'" & UCase(weight.Text) & "' "
        End If

        If grp_name.Text & "" = "" Or isEmpty(grp_name.Text) Then
            v_grp = "''"
        Else
            v_grp = "'" & UCase(grp_name.Text) & "'"
        End If

        If spiff_amt.Text & "" = "" Or isEmpty(spiff_amt.Text) Then
            v_spiff = "null"
            v_eff_dt = "''"
            v_end_dt = "''"
        Else
            v_spiff = Replace(spiff_amt.Text, "$", "")
            v_eff_dt = " to_date('" & FormatDateTime(spiff_eff_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
            v_end_dt = " to_date('" & FormatDateTime(spiff_end_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        End If

        v_repl_cost = Replace(repl_cost.Text, "$", "")
        v_rtl_prc = Replace(retail_price.Text, "$", "")

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- update sku -------------------------------------
        '---------------------------------------------------------------------------------------------
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update fritm_sku " +
        " set co_cd = '" & srch_co_cd.SelectedItem.Value.ToString() & "'" +
        "    ,store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "'" +
        "    ,itm_des = '" & UCase(itm_des.Text) & "'" +
        "    ,vsn = '" & UCase(vsn.Text) & "'" +
        "    ,ve_cd = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,mjr_cd = '" & mjr_cd_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,mnr_cd = '" & mnr_cd_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,cat_cd = '" & cat_cd_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,style_cd = " & v_style +
        "    ,itm_tp_cd = '" & itm_tp_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,label_cd = '" & label_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,warrant_ind = '" & warrant_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,treat_ind = '" & treat_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,comm_cd = '" & comm_cd_drp_list.SelectedItem.Value.ToString() & "'" +
        "    ,group_name = " & v_grp +
        "    ,family_cd = " & v_family +
        "    ,prc_tag_des = '" & UCase(price_tag_des.Text) & "'" &
        "    ,weight = " & v_weight +
        "    ,spiff_amt = " & v_spiff +
        "    ,spiff_eff_dt = " & v_eff_dt +
        "    ,spiff_end_dt = " & v_end_dt +
        "    ,repl_cost = " & v_repl_cost +
        "    ,rtl_price = " & v_rtl_prc +
        "    ,itm_upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" +
        "    ,itm_upd_usr = '" & Session("emp_init") & "'" +
        "  where itm_cd = '" & sku_drp_list.SelectedItem.Value.ToString() & "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        btn_save.Visible = False
        lbl_msg.Text = "SKU " & sku_drp_list.SelectedItem.Value.ToString() & " is updated"


    End Sub
    Function PKG_has_2COMPSKU() As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = get_current_item()

        If v_sku & "" = "" Or isEmpty(v_sku) Then
            Return "N"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from fritm_comp_sku "
        sql = sql & " where itm_cd = '" & v_sku & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 1 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function COMPSKUS_R_APPROVED() As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = get_current_item()

        If v_sku & "" = "" Or isEmpty(v_sku) Then
            Return "N"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        'check if any comp. item is not approved 
        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from fritm_sku a "
        sql = sql & " where nvl(a.buyer_app_ind,'x') <> 'Y' "
        sql = sql & "   and a.itm_cd in "
        sql = sql & "  (select b.comp_sku "
        sql = sql & "     from fritm_comp_sku b "
        sql = sql & "    where b.itm_cd = '" & v_sku & "')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "Y"
        ElseIf isEmpty(v_value) = True Then
            Return "Y"
        ElseIf v_value > 0 Then       'if any compsku not approved
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function getNextSKUnumber(ByVal p_ve_cd As String) As String

        Dim v_sku As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT std_fritm_sku.getSKUnum('" & p_ve_cd & "') itm from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_sku = dbReader.Item("itm")
            Else
                v_sku = ""
            End If
        Catch
            v_sku = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_sku

    End Function
    Function VSNalreadyexists() As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from itm "
        sql = sql & " where vsn = '" & vsn.Text & "'"
        'sql = sql & " and std_multi_co.isValiditm('" & Session("emp_init") & "',itm_cd) "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"            ' does not exist
        ElseIf v_value > 0 Then
            Return "Y"            ' vsn already exists
        Else
            Return "N"
        End If

    End Function
    Function SKUalreadyexists() As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from itm "
        sql = sql & " where itm_cd = '" & new_sku.Text & "' "
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"            ' does not exist
        ElseIf v_value > 0 Then
            Return "Y"            ' sku already exists
        Else
            Return "N"
        End If

    End Function

    Protected Function isvaliditm(ByVal p_itm As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " SELECT std_multi_co.isValiditm('" & Session("emp_init") & "','" & p_itm & "') v_data "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = "N"
            End If

        Catch
            v_value = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        If isBuyer() = "Y" Then
            e.Editor.ReadOnly = True
        Else

            If e.Column.FieldName = "COMP_SKU_EXT_RTL" Then
                e.Editor.ReadOnly = True
            Else
                e.Editor.ReadOnly = False
                e.Editor.ClientEnabled = True
            End If
        End If


    End Sub
    Protected Sub populate_compsku()


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_sku As String = get_current_item()

        GridView1.DataSource = ""
        GridView2.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = "SELECT COMP_SKU,COMP_SKU_QTY,COMP_SKU_RTL,COMP_SKU_EXT_RTL " &
                             "  from fritm_comp_sku " &
                             " where co_cd = '" & srch_co_cd.SelectedItem.Value.ToString() & "' " &
                             "   and store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "' " &
                             "   and itm_cd = '" & v_sku & "' " &
                             " order by 1 "

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView2.DataSource = ds
                GridView2.DataBind()
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView2.DataSource = ds
                GridView2.DataBind()
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

        If isBuyer() = "Y" Then
            GridView1.Visible = False
            GridView2.Visible = True
        ElseIf getValue("buyer_app_ind") & "" = "" Then
            GridView1.Visible = True
            GridView2.Visible = False
        Else
            GridView1.Visible = False
            GridView2.Visible = True
        End If

    End Sub

    Protected Function isValidItmTp(ByVal p_itm As String) As String

        Dim v_cnt As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " SELECT count(*) cnt from itm where itm_cd = '" & p_itm & "' and itm_tp_cd = 'PKG' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If

        Catch
            v_cnt = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_cnt & "" = "" Then
            Return "N"
        Else
            If v_cnt = 0 Then
                Return "N"
            Else
                Return "Y"
            End If
        End If

    End Function
    Function COMPSKU_isvalid(ByVal p_itm As String) As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from dual "
        sql = sql & " where '" & p_itm & "' in (select itm_cd from fritm_sku) "
        sql = sql & "    or '" & p_itm & "' in (select itm_cd from itm) "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function COMPSKU_validVE(ByVal p_itm As String) As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from dual "
        sql = sql & " where '" & p_itm & "' in (select itm_cd from fritm_sku where ve_cd = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "') "
        sql = sql & "    or '" & p_itm & "' in (select itm_cd from itm where ve_cd = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function COMPSKU_validMNR(ByVal p_itm As String) As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from dual "
        sql = sql & " where '" & p_itm & "' in (select itm_cd from fritm_sku where mnr_cd = '" & mnr_cd_drp_list.SelectedItem.Value.ToString() & "') "
        sql = sql & "    or '" & p_itm & "' in (select itm_cd from itm where mnr_cd = '" & mnr_cd_drp_list.SelectedItem.Value.ToString() & "') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function COMPSKU_validTP(ByVal p_itm As String) As String

        Dim v_value As String = ""
        Dim v_tab As String = ""
        Dim v_sku As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT itm_tp_cd tp "
        sql = sql & " from  itm where itm_cd = '" & p_itm & "'"
        sql = sql & " union "
        sql = sql & " select itm_tp_cd tp "
        sql = sql & "   from fritm_sku "
        sql = sql & "  where itm_cd = '" & p_itm & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("tp")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf isEmpty(v_value) = True Then
            Return "N"
        ElseIf v_value = "FAB" Or v_value = "INV" Or v_value = "LAB" Or v_value = "MIL" Or v_value = "NLN" Or v_value = "NLT" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function get_current_item() As String

        Dim v_sku As String = ""

        If select_option_list.SelectedItem.Value.ToString() = "Create New SKU" Then
            If new_sku.Text & "" <> "" Then
                v_sku = new_sku.Text
            Else
                v_sku = ""
            End If
        Else
            If sku_drp_list.SelectedIndex > 0 Then
                v_sku = sku_drp_list.SelectedItem.Value.ToString()
            Else
                v_sku = ""
            End If
        End If

        Return v_sku

    End Function
    Function COMPSKU_isDUP(ByVal p_itm As String) As String

        Dim v_value As Integer = 0
        Dim v_tab As String = ""
        Dim v_sku As String = ""

        v_sku = get_current_item()

        If v_sku & "" = "" Or isEmpty(v_sku) Then
            Return "N"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from fritm_comp_sku "
        sql = sql & " where comp_sku = '" & p_itm & "' "
        sql = sql & " and itm_cd = '" & v_sku & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Sub btn_comp_sku_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_comp_sku.Click

        Dim v_itm As String = ""
        v_itm = get_current_item()

        new_sku2.Text = v_itm
        ASPxPopupPkgComp.ShowOnPageLoad = True

    End Sub
    Protected Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("COMP_SKU") = ""
        e.NewValues("COMP_SKU_QTY") = ""
        e.NewValues("COMP_SKU_RTL") = ""
        e.NewValues("COMP_SKU_EXT_RTL") = ""

    End Sub
    Protected Sub rowvalidation(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        e.RowError = ""

        If isBuyer() = "Y" Then
            e.RowError = "No updates allowed!!! - please press Cancel"
            e.Keys.Clear()
            Exit Sub
        End If

        Dim v_isapproved As String = getValue("buyer_app_ind")
        If v_isapproved & "" <> "" Then
            If v_isapproved = "Y" Then
                e.RowError = "No updates allowed - this Package is already Approved"
            Else
                e.RowError = "No updates allowed - this Package is Rejected"
            End If
            e.Keys.Clear()
            Exit Sub
        End If

        If e.IsNewRow Then
            If e.NewValues("COMP_SKU") & "" = "" Then
                e.RowError = "Item code is required"
                Exit Sub
            ElseIf COMPSKU_isvalid(e.NewValues("COMP_SKU")) <> "Y" Then
                e.RowError = "Invalid Item - please try again"
                Exit Sub
            ElseIf COMPSKU_validVE(e.NewValues("COMP_SKU")) <> "Y" Then
                e.RowError = "Item must belong to Vendor " & ve_cd_drp_list.SelectedItem.Value.ToString() & " - please try again"
                Exit Sub
            ElseIf COMPSKU_validTP(e.NewValues("COMP_SKU")) <> "Y" Then
                e.RowError = "Invalid Item Type - please try again"
                Exit Sub
            ElseIf COMPSKU_isDUP(e.NewValues("COMP_SKU")) = "Y" Then
                e.RowError = "Invalid Item - Duplicate Comp. SKU not allowed"
                Exit Sub
            End If
            
        End If

        If e.NewValues("COMP_SKU_QTY") & "" = "" Or isEmpty(e.NewValues("COMP_SKU_QTY")) Then
            e.RowError = "QTY is required "
            Exit Sub
        ElseIf Not IsNumeric(e.NewValues("COMP_SKU_QTY")) Then
            e.RowError = "QTY is invalid - please try again"
            Exit Sub
        ElseIf e.NewValues("COMP_SKU_QTY") < 1 Then
            e.RowError = "QTY must be 1 or greater"
            Exit Sub
        End If

        If e.NewValues("COMP_SKU_RTL") & "" = "" Or isEmpty(e.NewValues("COMP_SKU_RTL")) Then
            e.RowError = "Retail amount is required "
            Exit Sub
        ElseIf Not IsNumeric(e.NewValues("COMP_SKU_RTL")) Then
            e.RowError = "Retail amount is invalid - please try again"
            Exit Sub
        ElseIf e.NewValues("COMP_SKU_RTL") < 1 Then
            e.RowError = "Retail amount must be 1 or greater"
            Exit Sub
        End If

        e.RowError = ""
        e.Keys.Clear()


    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        Dim hasError As Boolean = False
        Dim errorMsg As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_ext_rtl As Integer = 0
        Dim v_qty As Integer = 0
        Dim v_rtl As Integer = 0
        Dim v_itm As String = get_current_item()

        If Not isEmpty(e.NewValues("COMP_SKU_QTY")) Then
            v_qty = e.NewValues("COMP_SKU_QTY").ToString()
        Else
            v_qty = e.OldValues("COMP_SKU_QTY").ToString()
        End If

        If Not isEmpty(e.NewValues("COMP_SKU_RTL")) Then
            v_rtl = e.NewValues("COMP_SKU_RTL").ToString()
        Else
            v_rtl = e.OldValues("COMP_SKU_RTL").ToString()
        End If

        v_ext_rtl = v_qty * v_rtl

        sql = "UPDATE fritm_comp_sku SET "
        sql = sql & " comp_sku_qty = " + e.NewValues("COMP_SKU_QTY").ToString()
        sql = sql & " ,comp_sku_rtl = " + e.NewValues("COMP_SKU_RTL").ToString()
        sql = sql & " ,comp_sku_ext_rtl = " & v_ext_rtl
        sql = sql & " ,comp_sku_upd_usr = '" & Session("emp_init") & "'"
        sql = sql & " ,comp_sku_upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') "
        sql = sql & "  WHERE itm_cd = '" & v_itm & "'"
        sql = sql & " and comp_sku = '" & e.OldValues("COMP_SKU").ToString() & "' "


        Dim v_val As String = ""

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        populate_compsku()
        GridView1.DataBind()

        e.Cancel = True
        GridView1.CancelEdit()

        Throw New Exception("Comp. SKU " & e.OldValues("COMP_SKU").ToString() & " is Updated")

    End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim v_ext_rtl As Integer = 0
        Dim v_itm As String = get_current_item()


        v_ext_rtl = e.NewValues("COMP_SKU_QTY") * e.NewValues("COMP_SKU_RTL")

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into fritm_comp_sku (co_cd,store_cd,itm_cd,comp_sku,comp_sku_qty,comp_sku_rtl,comp_sku_ext_rtl, "
        sql = sql & "comp_sku_cre_usr, comp_sku_cre_dt) values ("
        sql = sql & "'" & srch_co_cd.SelectedItem.Value.ToString() & "', "
        sql = sql & "'" & srch_str_cd.SelectedItem.Value.ToString & "', "
        sql = sql & "'" & v_itm & "'"
        sql = sql & ",'" & e.NewValues("COMP_SKU").ToString() & "', "
        sql = sql & e.NewValues("COMP_SKU_QTY").ToString() & " ,"
        sql = sql & e.NewValues("COMP_SKU_RTL").ToString() & " ,"
        sql = sql & v_ext_rtl & ", "
        sql = sql & "'" & Session("emp_init") & "' ,"
        sql = sql & "  to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') )"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Error - Duplicate Item")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        e.Cancel = True
        GridView1.CancelEdit()

        populate_compsku()
        GridView1.DataBind()
        ASPxPopupPkgComp.ShowOnPageLoad = True

        Throw New Exception("Comp. SKU " & e.NewValues("COMP_SKU").ToString() & " is added")

    End Sub
    Protected Sub ASPxGridView1_Rowdeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim v_itm As String = get_current_item()

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "delete fritm_comp_sku "
        sql = sql & " where co_cd      = '" & srch_co_cd.SelectedItem.Value.ToString() & "' "
        sql = sql & "  and  store_cd   = '" & srch_str_cd.SelectedItem.Value.ToString() & "' "
        sql = sql & "  and  comp_sku   = '" & e.Values("COMP_SKU") & "'  "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw New Exception("Error deleting the record. Please try again.")
        End Try
        conn.Close()

        populate_compsku()
        GridView1.DataBind()
        e.Cancel = True
        GridView1.CancelEdit()

        lbl_msg2.Text = "Component Item is removed"

    End Sub
   
End Class
