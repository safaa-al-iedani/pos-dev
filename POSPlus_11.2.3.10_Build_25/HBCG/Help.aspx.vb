Imports System.IO
Imports System.Data.OracleClient

Partial Class Help
    Inherits POSBasePage
    Dim htmlfile As String

    Function LoadTextFile(ByVal FilePath As String) As String

        Dim sr As System.IO.StreamReader

        FilePath = Request.ServerVariables("APPL_PHYSICAL_PATH") & FilePath
        Try
            sr = New System.IO.StreamReader(FilePath)
            LoadTextFile = sr.ReadToEnd()
        Finally
            If Not sr Is Nothing Then sr.Close()
        End Try

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Load_Help_File(Request("pagename"))
        If htmlfile & "" = "" Then
            main_body.InnerHtml = "<br />We're sorry, no help file was located for the page you selected." & vbCrLf
        Else
            main_body.InnerHtml = htmlfile
        End If

    End Sub

    Private Sub Load_Help_File(ByVal pagename As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT HELP_BODY FROM HELP_FILES WHERE HTML_FILE = '" & LCase(pagename) & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                htmlfile = MyDataReader.Item("HELP_BODY").ToString
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

End Class
