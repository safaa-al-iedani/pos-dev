Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Xml
Imports System.Collections.Generic
Imports System.Linq
Imports DevExpress.Web.ASPxGridView.Rendering
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports AppUtils
Imports HBCG_Utils
Imports InventoryUtils
Imports SD_Utils
Imports NCT_Utils
Imports World_Gift_Utils

Partial Class CashCarryExpress
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private thePmtBiz As PaymentBiz = New PaymentBiz()
    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()

    Public preventMessageFlag As Boolean '------ this flag is used for error message for prevent SKU's
    Public warrantiePopupURL As String '------- it will hold the url and append error message to it.
    Public strPreventMessage As String
    Public intRowID As Integer
    Public isWarrantieFlag As Boolean

    'Dim GM_Sec As String = ""
    'Dim FAB_FOUND As String
    Dim Insert_Payment As Boolean = True
    Dim partial_amt As Double = 0
    Private Const cResponseCriteria As String = "/HandleCode/ReasonCode/CodeDesc"
    Private Const cReturnCriteria As String = "/HandleCode/ReturnCode/CodeDesc"
    Private Const cAVSCriteria As String = "/HandleCode/AVSResponseCode/CodeDesc"
    Dim g_PkgItems As String
    Dim slsp_ds As New DataSet
    Dim Session_String As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        check_details.Visible = False
        finance_details.Visible = False
        cash_details.Visible = False
        gift_Card_Details.Visible = False
        payment_header.Visible = False

        If Request("refresh") = "true" Then
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.top; ")
            Response.Write("parentWindow.location.href='CashCarryExpress.aspx?SKU=" & Request("SKU") & "';")
            Response.Write("</script>")
            Response.End()
        End If

        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTax")) OrElse ViewState("CanModifyTax").ToString = String.Empty Then
            ViewState("CanModifyTax") = SecurityUtils.hasSecurity(SecurityUtils.OVERRIDE_TAXCD_ENTRY, Session("EMP_CD"))
        End If

        Dim lbl_warnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")

        If Not IsPostBack Then
            If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "C" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" Then
                lbl_warnings.Text = "This terminal has been closed.  You cannot submit orders or payments at this time."
                Exit Sub
            Else
                lbl_warnings.Text = ValStoreAndDt()
                If lbl_warnings.Text.isNotEmpty Then
                    Exit Sub
                End If
            End If

            Dim emp_cd, fname, lname, store_cd, IPAD, co_cd, SKU, tax_Cd As String
            emp_cd = Session("EMP_CD")
            co_cd = Session("CO_CD")
            fname = Session("EMP_FNAME")
            lname = Session("EMP_LNAME")
            store_cd = Session("HOME_STORE_CD")
            IPAD = Session("IPAD")
            SKU = Request("SKU")
            tax_Cd = Session("TAX_CD")
            ' Daniela save clientip, emp_init, super user
            Dim client_IP As String = Session("clientip")
            Dim empInit As String = Session("EMP_INIT")
            Dim supUser As String = Session("str_sup_user_flag")
            Dim coGrpCd As String = Session("str_co_grp_cd")
            Dim userType As String = Session("USER_TYPE")

            Session.Clear()

            Session("clientip") = client_IP
            Session("str_sup_user_flag") = supUser ' Daniela
            Session("str_co_grp_cd") = coGrpCd ' Daniela
            Session("EMP_INIT") = empInit
            Session("USER_TYPE") = userType ' Daniela
            ' TODO - this is mostly the set of info that sb saved as order header session data
            Session("itemid") = SKU
            Session("EMP_CD") = emp_cd
            Session("CO_CD") = co_cd
            Session("EMP_FNAME") = fname
            Session("EMP_LNAME") = lname
            Session("HOME_STORE_CD") = store_cd
            Session("IPAD") = IPAD
            Session("PD") = "P"
            Session("del_dt") = FormatDateTime(Today.Date, 2)
            Session("tran_dt") = FormatDateTime(Today.Date, 2)
            Session("store_cd") = Session("HOME_STORE_CD")
            Session("loc_cd") = GetDefaultLocation(Session("STORE_CD")) ' the default location code from the STORE table for the inventory

            ActivateCashier(emp_cd)

            ' emp_cd is nothing if do not come thru Login
            If (Not IsNothing(emp_cd)) AndAlso (Session("store_cd") = "" OrElse Session("loc_cd") = "") Then
                lbl_warnings.Text = "Employee Home Store and/or Store Sales Location is missing or invalid.  You cannot submit orders or payments at this time."
                Exit Sub
            End If

            Session("slsp1") = UCase(Session("emp_cd"))
            Session("pct_1") = "100"
            Session("comments") = "Y"
            Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL
            Session("cash_carry") = "TRUE"
            Session("TAX_CD") = tax_Cd
            Session("cust_cd") = ConfigurationManager.AppSettings("cash_carry").ToString

            GetCashCarryCustomer()

            Dim txPcts As TaxUtils.taxPcts = SetTxPcts(Session("tran_dt"), tax_Cd) ' local pass thru

            If Not (IsNothing(Request("warrItmCd")) OrElse "NONE".Equals(Request("warrItmCd") + "") OrElse IsNothing(Request("itmRowNum"))) Then
                ' if warrItmRowNum is empty, then user has selected to create a new warranty line, otherwise they've
                '     selected to link this warrantable to an existing warrantable line (warrItmRowNum)
                If IsNothing(Request("warrItmRowNum")) OrElse Request("warrItmRowNum") + "" = "" Then
                    Dim createLnReq As HBCG_Utils.CreateTempItmReq = initTempItmReq()
                    createLnReq.itmCd = Request("warrItmCd")
                    createLnReq.warrItmLink = Request("itmCd")
                    createLnReq.warrRowLink = CInt(Request("itmRowNum"))

                    HBCG_Utils.CreateWarrantyLn(createLnReq)
                Else
                    HBCG_Utils.UpdtWarrLnk(Request("warrItmCd"), Request("warrItmRowNum"), Request("itmRowNum"))
                End If
            End If

            If Request("swatch") <> "TRUE" And Session("itemid") & "" <> "" Then
                InsertItems()
            End If

            If ConfigurationManager.AppSettings("default_sort_code").ToString & "" <> "" Then
                Session("ord_srt") = ConfigurationManager.AppSettings("default_sort_code").ToString
            End If

            If Request("SWATCH") = "TRUE" Then
                Session("ord_srt") = "FAB"
            End If

            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim sql As String
            conn.Open()

            If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
                Dim objSql2 As OracleCommand
                Dim MyDataReader2 As OracleDataReader
                sql = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
                sql = UCase(sql)
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        Session("store_cd") = MyDataReader2.Item("STORE_CD").ToString
                        Session("csh_dwr_cd") = MyDataReader2.Item("CASH_DWR_CD").ToString
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            Else
                Try
                    Dim ds As DataSet = theSystemBiz.GetCashDrawers(store_cd)
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Session("csh_dwr_cd") = ds.Tables(0).Rows(0).Item("CSH_DWR_CD").ToString()
                    End If
                Catch ex As Exception
                    Throw
                End Try
            End If

            Dim store As StoreData = theSalesBiz.GetStoreInfo(UCase(Session("store_cd").ToString))
            If Session("PD") = "P" Then
                Session("pd_store_cd") = store.puStoreCd
            Else
                Session("pd_store_cd") = store.shipToStoreCd
            End If

            Session("CO_CD") = store.coCd
            bindgrid()

            Dim storePostalZip As String = String.Empty
            If Not IsNothing(store.addr) Then
                storePostalZip = store.addr.postalCd
            End If

            determine_tax(storePostalZip)
            calculate_total()
        End If

        If (Request("__EVENTTARGET") IsNot Nothing AndAlso Request("__EVENTTARGET").ToUpper.ToString.Contains("SELECTWARR")) And _
            (Request("__EVENTARGUMENT") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Request("__EVENTARGUMENT").ToString())) Then
            Dim str As String = Request("__EVENTTARGET").Replace("ASPxRoundPanel2_GridView1_cell", "").Split(New Char() {"_"c})(0)
            Dim isWarrCheck As Boolean = False
            Dim rowIndex As Integer = 0

            If Integer.TryParse(str, rowIndex) Then
                Dim row As DataRow = GetRowInfo(rowIndex.ToString)

                If Boolean.TryParse(Request("__EVENTARGUMENT"), isWarrCheck) AndAlso isWarrCheck Then
                    If SalesUtils.hasWarranty(row("ITM_CD").ToString()) Then ShowWarrantyPopup(row("row_id").ToString())
                Else
                    lbl_warnings.Text = RemoveWarrLink(row("WARR_ROW_LINK").ToString(), row("row_id").ToString(), Session.SessionID.ToString())
                End If

                bindgrid()
                calculate_total()
            End If
        End If

        Dim Term_IP As String = Request.ServerVariables("REMOTE_ADDR")

        If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
            lbl_Header1.Text = Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
        End If

        If Session("HOME_STORE_CD") & "" <> "" Then
            lbl_Header2.Text = "Store Code: " & Session("HOME_STORE_CD") & vbCrLf & "Terminal: " & Term_IP
        End If

        GridView1.Visible = True

        ''this code change is done to retain error message for prevented sale in case of multiple 
        ''SKU are entered in search box.
        ''and for any SKU entered is having warranty  or related SKU then this flag is checked.
        If Not Request("preventMessage") Is Nothing Then
            lbl_warnings.Text = Request("preventMessage").ToString
        Else
            'do nothing
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
    End Sub

    Private Function ValStoreAndDt() As String
        ' validate SHU has not been run for the date (today) and also the store (emp home store) is not closed for the date (today)
        Dim ShuDt As String = SessVar.shuDt
        Dim todayDate As String = FormatDateTime(Today.Date, DateFormat.ShortDate)

        If IsDate(ShuDt) AndAlso DateDiff("d", todayDate, ShuDt) >= 0 Then
            ValStoreAndDt = "Sales History (SHU) has already been run for today.  You cannot submit orders or payments at this time."
        Else
            ValStoreAndDt = theSalesBiz.ValidateStoreClosedDate(Session("HOME_STORE_CD"), todayDate)
        End If
    End Function

    Public Sub Add_Finance_Company()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' ORDER BY NAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "AS_CD"
                cbo_finance_company.DataTextField = "NAME"
                cbo_finance_company.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    Protected Sub txt_pct_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim txt_pct_1 As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txt_pct_1.NamingContainer, DataGridItem)
        Dim txt_pct_2 As TextBox
        txt_pct_2 = CType(dgItem.FindControl("slsp2_pct"), TextBox)
        Dim slsp1 As DropDownList
        slsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        Dim slsp2 As DropDownList
        slsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)
        Dim lbl_warnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")

        If IsNumeric(txt_pct_1.Text) Then
            If CDbl(txt_pct_1.Text) < 100 And CDbl(txt_pct_1.Text) > 0 Then
                txt_pct_2.Text = 100 - CDbl(txt_pct_1.Text)
            Else
                lbl_warnings.Text = "Commission percentages must be between 1 and 100"
                If IsNumeric(txt_pct_2.Text) Then
                    txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                Else
                    txt_pct_1.Text = 100
                End If
            End If
        Else
            lbl_warnings.Text = "Commission percentages must be numeric"
            Exit Sub
        End If

        With cmdUpdateItems
            .Connection = conn
            'jkl
            '.CommandText = "update temp_itm set slsp1=" & slsp1.SelectedValue & ", slsp2=" & slsp2.SelectedValue & ", slsp1_pct=" & txt_pct_1.Text & ", slsp2_pct=" & txt_pct_2.Text & " where row_id=" & dgItem.Cells(9).Text

            .CommandText = "update temp_itm set slsp1=:SLSP1, slsp2=:SLSP2, slsp1_pct=:TXTPCT1, slsp2_pct=:TXTPCT21 where row_id=:DGITEM"
            .Parameters.Add(":SLSP1", OracleType.VarChar)
            .Parameters(":SLSP1").Value = slsp1.SelectedValue
            .Parameters.Add(":SLSP2", OracleType.VarChar)
            .Parameters(":SLSP2").Value = slsp2.SelectedValue
            .Parameters.Add(":TXTPCT1", OracleType.VarChar)
            .Parameters(":TXTPCT1").Value = txt_pct_1.Text
            .Parameters.Add(":TXTPCT2", OracleType.VarChar)
            .Parameters(":TXTPCT2").Value = txt_pct_2.Text
            .Parameters.Add(":DGITEM", OracleType.VarChar)
            .Parameters(":DGITEM").Value = dgItem.Cells(9).Text

            .ExecuteNonQuery()
        End With
    End Sub

    Protected Sub Line_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        conn.Open()

        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        If textdata.Text & "" <> "" Then
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            'jkl
            'sql = "SELECT * FROM RELATIONSHIP_LINES_DESC WHERE REL_NO='" & Session.SessionID.ToString & "' AND LINE='" & dgItem.Cells(9).Text & "'"
            sql = "SELECT * FROM RELATIONSHIP_LINES_DESC WHERE REL_NO=:SESSIONID AND LINE=:DGITEM"


            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            'jkl
            objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
            objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString
            objSql.Parameters.Add(":DGITEM", OracleType.VarChar)
            objSql.Parameters(":DGITEM").Value = dgItem.Cells(9).Text

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    sql = "UPDATE RELATIONSHIP_LINES_DESC SET DESCRIPTION=:DESCRIPTION WHERE REL_NO='" & Session.SessionID.ToString & "' AND LINE='" & dgItem.Cells(9).Text & "'"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":DESCRIPTION", OracleType.Clob)

                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.ExecuteNonQuery()
                Else
                    sql = "INSERT INTO RELATIONSHIP_LINES_DESC (REL_NO, LINE, DESCRIPTION) VALUES(:REL_NO, :LINE, :DESCRIPTION)"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":REL_NO", OracleType.Clob)
                    objSql.Parameters.Add(":LINE", OracleType.VarChar)
                    objSql.Parameters.Add(":DESCRIPTION", OracleType.VarChar)

                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.Parameters(":LINE").Value = dgItem.Cells(9).Text
                    objSql.Parameters(":REL_NO").Value = Session.SessionID.ToString
                    objSql.ExecuteNonQuery()
                End If
            Catch
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()
    End Sub

    Protected Sub Loc_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String

        conn.Open()

        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        Dim txt_store_cd As TextBox
        txt_store_cd = CType(dgItem.FindControl("store_cd"), TextBox)

        lbl_ser_store_cd.Text = ""
        lbl_ser_loc_cd.Text = ""
        lbl_ser_row_id.Text = ""
        lbl_ser_itm_cd.Text = ""

        If Request("LEAD") <> "TRUE" And ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" And textdata.Text <> "" Then
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn2.Open()
            lbl_ser_store_cd.Text = txt_store_cd.Text
            lbl_ser_loc_cd.Text = textdata.Text
            lbl_ser_row_id.Text = dgItem.Cells(9).Text
            lbl_ser_itm_cd.Text = dgItem.Cells(0).Text

            sql = "SELECT SERIAL_TP FROM ITM where ITM_CD='" & dgItem.Cells(0).Text & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDatareader.Read() Then
                    If IsSerialType(MyDatareader.Item("SERIAL_TP").ToString) Then
                        ASPxPopupControl7.ShowOnPageLoad = True
                        Serial_Bindgrid()
                    End If
                End If
                MyDatareader.Close()
                conn2.Close()
            Catch ex As Exception
                conn.Close()
                conn2.Close()
                Throw
            End Try
        End If

        If textdata.Text & "" <> "" Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = UCase("update temp_itm set loc_cd = '" & textdata.Text & "' where row_id=" & dgItem.Cells(9).Text)
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = UCase("update temp_itm set loc_cd=NULL, SERIAL_NUM=NULL where row_id=" & dgItem.Cells(9).Text)
            End With
        End If

        cmdDeleteItems.ExecuteNonQuery()
        Dim txt_ret_prc As TextBox
        txt_ret_prc = CType(dgItem.FindControl("ret_prc"), TextBox)
        ClientScript.RegisterStartupScript(Me.GetType(), "selectAndFocus", "$get('" + txt_ret_prc.ClientID + "').focus();$get('" + txt_ret_prc.ClientID + "').select();", True)
        conn.Close()
    End Sub

    Public Sub Serial_Bindgrid()
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim sql As String
        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        Try
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn2.Open()

            sql = "select ser_num from inv_xref where itm_cd='" & lbl_ser_itm_cd.Text & "' and store_cd='" & lbl_ser_store_cd.Text & "' "
            sql = sql & " and loc_cd='" & lbl_ser_loc_cd.Text & "' and disposition='FIF' order by ser_num"

            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            ASPxGridView1.DataSource = ds
            ASPxGridView1.DataBind()
            conn2.Close()

            If ds.Tables(0).Rows.Count < 1 Then
                lbl_ser_err.Text = "No items were found for the selected store and location."
                If lbl_ser_row_id.Text & "" <> "" Then
                    conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    conn.Open()

                    With cmdDeleteItems
                        .Connection = conn
                        .CommandText = "update temp_itm set store_cd=NULL, loc_cd=NULL where row_id=" & lbl_ser_row_id.Text
                    End With
                    cmdDeleteItems.ExecuteNonQuery()
                    bindgrid()
                    calculate_total()
                    conn.Close()
                End If
            End If
        Catch
            conn2.Close()
            conn.Close()
        End Try
    End Sub

    Protected Sub Check_Clicked(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set treated = 'Y' where row_id=" & dgItem.Cells(9).Text
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set treated = 'N' where row_id=" & dgItem.Cells(9).Text
            End With
        End If

        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()
    End Sub

    Protected Sub Price_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbl_warnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")
        lbl_warnings.Text = String.Empty

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim Ret_prc As TextBox = CType(sender, TextBox)
        Dim parent As DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow = CType(Ret_prc.Parent.Parent.Parent.Parent, DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow)
        Dim rowIndex As Integer = parent.VisibleIndex

        Dim blas As String = parent.Cells(0).ToString() & ", " & parent.Cells(0).ToString()

        Dim row As String = GetRowID(rowIndex.ToString)

        If Ret_prc.Text & "" <> "" And IsNumeric(row) Then
            'If IsNumeric(Qty.Text) Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set ret_prc = '" & Math.Abs(CDbl(Ret_prc.Text)) & "' where row_id=" & row.ToString
            End With

            cmdDeleteItems.ExecuteNonQuery()
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "Update temp_itm set taxable_amt = " & Math.Abs(CDbl(Ret_prc.Text)) & " where tax_pct > 0 and row_id = " & row.ToString
            End With

            cmdDeleteItems.ExecuteNonQuery()

            calculate_total()
            'End If
        End If

        conn.Close()
    End Sub

    Protected Sub Qty_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim Qty As TextBox = CType(sender, TextBox)
        Dim parent As DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow = CType(Qty.Parent.Parent.Parent.Parent, DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow)
        Dim rowIndex As Integer = parent.VisibleIndex
        Dim row As DataRow = GetRowInfo(rowIndex.ToString)  'GetRowID(rowIndex.ToString)

        If SystemUtils.dataRowIsNotEmpty(row) Then
            Dim rowIdSeqNum As String = row("ROW_ID")
            Dim lbl_warnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")

            lbl_warnings.Text = OrderUtils.ValAndFormatQty(Qty.Text, row("QTY"), row("ITM_TP_CD"), row("WARR_ROW_LINK") & "", row("OUT_ID") & "")
            If lbl_warnings.Text.isNotEmpty Then
                lbl_warnings.Visible = True
            End If
            ' below will also work but above is simpler
            'IIf(SystemUtils.isEmpty(row("WARR_ROW_LINK")), "", row("warr_row_link")),
            'IIf(SystemUtils.isEmpty(row("OUT_ID")), "", row("OUT_ID")))

            ' TODO - why is below isNumeric check commented out - is there a failure if an error???
            If Qty.Text & "" <> "" AndAlso IsNumeric(rowIdSeqNum) Then

                'If IsNumeric(Qty.Text) Then
                With cmdDeleteItems
                    .Connection = conn
                    .CommandText = "update temp_itm set qty = '" & Math.Abs(CDbl(Qty.Text)) & "' where row_id=" & rowIdSeqNum.ToString
                End With

                cmdDeleteItems.ExecuteNonQuery()
                calculate_total()
                'End If
            End If

            conn.Close()
            'else this would be a logic problem
        End If
    End Sub

    Protected Sub Delete_Items(ByVal sender As Object, ByVal e As EventArgs)
        Dim imagButton As ImageButton = CType(sender, ImageButton)
        Dim parent As DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow = CType(imagButton.Parent.Parent.Parent.Parent, DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataRow)
        Dim rowIndex As Integer = parent.VisibleIndex
        Dim delRowIdSeqNum As String = GetRowID(rowIndex.ToString)

        HBCG_Utils.DeleteTempItmAndLinked(delRowIdSeqNum, Session.SessionID.ToString.Trim)  ' currently not updating message ???

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim datRdr As OracleDataReader
        Dim PACKAGE_PARENT As String = ""
        Dim TAXABLE_AMOUNT As String = ""

        conn.Open()

        Dim sql As String = "SELECT PACKAGE_PARENT, TAXABLE_AMT FROM TEMP_ITM WHERE ROW_ID=" & delRowIdSeqNum
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        datRdr = DisposablesManager.BuildOracleDataReader(cmd)


        Try
            If (datRdr.Read()) Then

                PACKAGE_PARENT = datRdr.Item("PACKAGE_PARENT").ToString
                TAXABLE_AMOUNT = datRdr.Item("TAXABLE_AMT").ToString
            End If
            datRdr.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        ' TODO - have to genericize Modify_Package_Price before can move this too
        Dim ret_prc As Double = 0
        If IsNumeric(PACKAGE_PARENT.ToString) Then
            If IsNumeric(TAXABLE_AMOUNT.ToString) Then
                'With cmdDeleteItems
                '    .Connection = conn
                '    .CommandText = "update temp_itm set ret_prc=ret_prc-" & TAXABLE_AMOUNT & " where ret_prc > 0 and row_id=" & PACKAGE_PARENT
                'End With
                'cmdDeleteItems.ExecuteNonQuery()
                sql = "SELECT RET_PRC FROM TEMP_ITM WHERE row_id=" & PACKAGE_PARENT
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    datRdr = DisposablesManager.BuildOracleDataReader(cmd)

                    If datRdr.Read Then
                        Modify_Package_Price(PACKAGE_PARENT, delRowIdSeqNum, datRdr.Item("RET_PRC").ToString)
                    End If
                Catch
                    Throw
                End Try
            End If
        End If

        conn.Close()

        Response.Redirect("CashCarryExpress.aspx")
    End Sub

    Protected Function GetRowID(ByVal RowIndex As String) As String
        Dim rowId As String = ""

        If IsNumeric(RowIndex) Then
            Dim tempItmRow As DataRow = GetRowInfo(RowIndex)

            If SystemUtils.dataRowIsNotEmpty(tempItmRow) Then

                rowId = tempItmRow("ROW_ID")
            End If
        End If

        Return rowId
    End Function

    Protected Function GetRowInfo(ByVal RowIndex As String) As DataRow
        Dim tempItmRow As DataRow = Nothing

        If SystemUtils.isNumber(RowIndex) Then
            Dim tempItmTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Session.SessionID.ToString.Trim, temp_itm_key_tp.tempItmBySessionId)

            If tempItmTbl.Rows.Count > 0 Then
                Dim RowIndexDbl As Double = (CDbl(RowIndex)) + 1
                Dim currLoopIndx As Integer = 1
                Do While currLoopIndx <= RowIndexDbl AndAlso currLoopIndx <= tempItmTbl.Rows.Count

                    If RowIndexDbl = currLoopIndx Then

                        tempItmRow = tempItmTbl.Rows(currLoopIndx - 1)
                    End If
                    currLoopIndx = currLoopIndx + 1
                Loop
            End If
        End If

        Return tempItmRow
    End Function

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ASPxPopupControl7.ShowOnPageLoad = False
        Response.Redirect("CashCarryExpress.aspx")
    End Sub

    Protected Sub Store_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        If textdata.Text & "" <> "" Then
            Dim txt_loc_cd As TextBox
            txt_loc_cd = CType(dgItem.FindControl("loc_cd"), TextBox)

            Dim txt_store_cd As TextBox
            txt_store_cd = CType(dgItem.FindControl("store_cd"), TextBox)

            lbl_ser_store_cd.Text = ""
            lbl_ser_loc_cd.Text = ""
            lbl_ser_row_id.Text = ""
            lbl_ser_itm_cd.Text = ""

            If Request("LEAD") <> "TRUE" And ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" And txt_loc_cd.Text <> "" Then
                Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim MyDatareader As OracleDataReader
                Dim objsql As OracleCommand
                Dim sql As String
                conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                conn2.Open()
                lbl_ser_store_cd.Text = txt_store_cd.Text
                lbl_ser_loc_cd.Text = textdata.Text
                lbl_ser_row_id.Text = dgItem.Cells(9).Text
                lbl_ser_itm_cd.Text = dgItem.Cells(0).Text

                sql = "SELECT SERIAL_TP FROM ITM where ITM_CD='" & dgItem.Cells(0).Text & "'"

                'Set SQL OBJECT 
                objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

                Try
                    MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If MyDatareader.Read() Then
                        If IsSerialType(MyDatareader.Item("SERIAL_TP").ToString) Then
                            ASPxPopupControl7.ShowOnPageLoad = True
                            Serial_Bindgrid()
                        End If
                    End If
                    MyDatareader.Close()
                    conn2.Close()
                Catch ex As Exception
                    conn.Close()
                    conn2.Close()
                    Throw
                End Try
            End If

            With cmdDeleteItems
                .Connection = conn
                .CommandText = UCase("update temp_itm set store_cd = '" & textdata.Text & "' where row_id=" & dgItem.Cells(9).Text)
            End With

            ClientScript.RegisterStartupScript(Me.GetType(), "selectAndFocus", "$get('" + txt_loc_cd.ClientID + "').focus();$get('" + txt_loc_cd.ClientID + "').select();", True)
            'dgItem.Cells(6).Focus()
        Else
            Dim txt_ret_prc As TextBox
            txt_ret_prc = CType(dgItem.FindControl("ret_prc"), TextBox)

            With cmdDeleteItems
                .Connection = conn
                .CommandText = UCase("update temp_itm set store_cd=NULL, loc_cd=NULL, SERIAL_NUM=NULL where row_id=" & dgItem.Cells(9).Text)
            End With

            dgItem.Cells(6).Text = " "
            ClientScript.RegisterStartupScript(Me.GetType(), "selectAndFocus", "$get('" + txt_ret_prc.ClientID + "').focus();$get('" + txt_ret_prc.ClientID + "').select();", True)
            'dgItem.Cells(10).Focus()
        End If

        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()
    End Sub

    Protected Sub txt_pct_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim txt_pct_2 As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txt_pct_2.NamingContainer, DataGridItem)
        Dim txt_pct_1 As TextBox
        txt_pct_1 = CType(dgItem.FindControl("slsp1_pct"), TextBox)
        Dim slsp1 As DropDownList
        slsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        Dim slsp2 As DropDownList
        slsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If IsNumeric(txt_pct_2.Text) Then
            If CDbl(txt_pct_2.Text) < 100 And CDbl(txt_pct_2.Text) > 0 Then
                txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
            Else
                If IsNumeric(txt_pct_1.Text) And CDbl(txt_pct_2.Text) > 0 Then
                    txt_pct_2.Text = 100 - CDbl(txt_pct_1.Text)
                Else
                    txt_pct_1.Text = 100
                    txt_pct_2.Text = 0
                    slsp2.SelectedValue = ""
                End If
            End If
        Else
            txt_pct_2.Text = 0
            txt_pct_1.Text = 100
            slsp2.SelectedValue = ""
        End If

        With cmdUpdateItems
            .Connection = conn
            .CommandText = "update temp_itm set slsp1='" & slsp1.SelectedValue & "', slsp2='" & slsp2.SelectedValue & "', slsp1_pct=" & txt_pct_1.Text & ", slsp2_pct=" & txt_pct_2.Text & " where row_id=" & dgItem.Cells(9).Text
            .ExecuteNonQuery()
        End With
    End Sub

    Protected Sub Salesperson_Update(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim txt_pct_2 As TextBox
        txt_pct_2 = CType(dgItem.FindControl("slsp2_pct"), TextBox)
        Dim txt_pct_1 As TextBox
        txt_pct_1 = CType(dgItem.FindControl("slsp1_pct"), TextBox)
        Dim slsp1 As DropDownList
        slsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        Dim slsp2 As DropDownList
        slsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If slsp2.SelectedValue <> "" Then
            If slsp2.SelectedValue = slsp1.SelectedValue Then
                slsp2.SelectedIndex = 0
            Else
                If Not IsNumeric(ConfigurationManager.AppSettings("slsp2")) Then
                    txt_pct_1.Text = "50"
                    txt_pct_2.Text = "50"
                Else
                    txt_pct_2.Text = ConfigurationManager.AppSettings("slsp2")
                    txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                End If
            End If
        End If

        With cmdUpdateItems
            .Connection = conn
            .CommandText = "update temp_itm set slsp1='" & slsp1.SelectedValue & "', slsp2='" & slsp2.SelectedValue & "', slsp1_pct=" & txt_pct_1.Text & ", slsp2_pct=" & txt_pct_2.Text & " where row_id=" & dgItem.Cells(9).Text
            .ExecuteNonQuery()
        End With
    End Sub

    Protected Sub Carton_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim chk_carton As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chk_carton.NamingContainer, DataGridItem)
        If chk_carton.Checked = True Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set leave_carton = 'Y' where row_id=" & dgItem.Cells(9).Text
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set leave_carton = 'N' where row_id=" & dgItem.Cells(9).Text
            End With
        End If
        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub Modify_Package_Price(ByVal row_id As String, ByVal itm_cd As String, ByVal package_retail As Double, Optional ByVal change_parent As String = "NO")
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim retprc As Double
        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim PKG_GO As Boolean
        PKG_GO = False
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'If we are to populate the retail amounts, we need to calculate based on cost percentages
        sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd='" & itm_cd & "' "
        sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


        Do While MyDataReader.Read()
            g_PkgItems = g_PkgItems & ",'" & MyDataReader.Item("ITM_CD").ToString & "'"
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST,ROW_ID) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & "," & row_id & ")"
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
            PKG_GO = True
        Loop

        MyDataReader.Close()
        'Insert new retail price
        thistransaction = objConnection.BeginTransaction

        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & package_retail & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id
        End With

        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd='" & itm_cd & "' and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


        'Calculate the total cost
        Do While MyDataReader.Read()
            thistransaction = objConnection.BeginTransaction

            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST").ToString & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD").ToString & "' AND ROW_ID=" & row_id
            End With

            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
            PKG_GO = True
        Loop

        MyDataReader.Close()

        Dim PERCENT_OF_TOTAL As Double
        Dim NEW_RETAIL As Double
        Dim TOTAL_NEW_RETAIL As Double
        Dim TOTAL_RETAIL As Double
        Dim LAST_SKU As String
        Dim LAST_PKG_SKU As String

        If PKG_GO = True Then
            'Calculate the percentage based on cost/total cost
            sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id & " ORDER BY PKG_SKU"
            objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)


            Do While MyDataReader2.Read()
                If IsNumeric(MyDataReader2.Item("COST").ToString) And IsNumeric(MyDataReader2.Item("TOTAL_COST").ToString) Then
                    PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                Else
                    PERCENT_OF_TOTAL = CDbl(0)
                End If

                If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                NEW_RETAIL = PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                thistransaction = objConnection.BeginTransaction

                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND ROW_ID=" & row_id
                End With

                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
            Loop

            MyDataReader2.Close()
            objSql2.Dispose()
        End If

        sql = "SELECT NEW_RETAIL, COMPONENT_SKU, ROW_ID FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & itm_cd & "'"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)


        Do While MyDataReader2.Read()
            retprc = MyDataReader2.Item("NEW_RETAIL")
            If Not IsNumeric(retprc) Then retprc = 0
            sql = "update temp_itm set taxable_amt=" & retprc & "/qty WHERE PACKAGE_PARENT=" & row_id & " AND ITM_CD='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            thistransaction = objConnection.BeginTransaction

            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With

            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
        Loop

        MyDataReader2.Close()
        objSql2.Dispose()

        If change_parent <> "NO" Then
            'Update the taxable amounts for the newly updated Component SKUS
            sql = "update temp_itm set taxable_amt=ret_prc WHERE PACKAGE_PARENT=" & row_id & " AND SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAX_PCT IS NOT NULL"
            thistransaction = objConnection.BeginTransaction

            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With

            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

            'Update the Parent Package SKU retail to 0.00
            sql = "update temp_itm set ret_prc=0 WHERE ROW_ID=" & row_id
            thistransaction = objConnection.BeginTransaction

            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With

            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
        End If

        'We're done, delete out all of the records to avoid duplication
        thistransaction = objConnection.BeginTransaction
        sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"

        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = sql
        End With

        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        conn.Close()
        objConnection.Close()
    End Sub

    Public Sub bindgrid()
        GridView1.DataSource = Nothing
        GridView1.Visible = True

        Dim sql As New StringBuilder
        sql.Append("SELECT ITM_CD, VE_CD, VSN, DES, SLSP1, SLSP2, STORE_CD, LOC_CD, ROW_ID, NVL(RET_PRC,0) AS RET_PRC, NVL(QTY,0) AS QTY, ").Append(
                "ITM_TP_CD, TREATABLE, TREATED, TAKE_WITH, NEW_SPECIAL_ORDER, PACKAGE_PARENT, NVL(TAXABLE_AMT,0) AS TAXABLE_AMT, ").Append(
                "LEAVE_CARTON, NVL(SLSP1_PCT,0) AS SLSP1_PCT, NVL(SLSP2_PCT,0) AS SLSP2_PCT, SERIAL_TP, BULK_TP_ITM, WARR_ROW_LINK, WARR_ITM_LINK, ").Append(
                "WARRANTABLE,ITM_TP_CD AS TYPECODE ").Append(
                "FROM TEMP_ITM WHERE SESSION_ID = :SESSID ORDER BY ROW_ID")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        Dim ds As DataSet = New DataSet
        Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter


        cmd.Parameters.Add(":SESSID", System.Data.OracleClient.OracleType.VarChar)
        cmd.Parameters(":SESSID").Value = Session.SessionID.ToString().Trim

        Try
            conn.Open()

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

            If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'Removing the unselect warranties
                Dim drWarrs() As DataRow = ds.Tables(0).Select("ITM_TP_CD = '" & AppConstants.Sku.TP_WAR & "'")
                Dim drFilterWarr() As DataRow = Nothing
                For Each drWarr As DataRow In drWarrs
                    drFilterWarr = ds.Tables(0).Select("WARR_ROW_LINK = '" & drWarr("ROW_ID") & "'")
                    If drFilterWarr.Length < 1 Then
                        HBCG_Utils.DeleteTempItmAndLinked(drWarr("ROW_ID"), Session.SessionID.ToString.Trim)
                        ds.Tables(0).Rows.Remove(drWarr)
                    End If
                Next

                GridView1.DataSource = ds
                GridView1.DataBind()
            Else
                Session("itemid") = System.DBNull.Value
                Session("sub_total") = 0
                chk_slsp_view_all.Visible = False
                lbl_slsp_breakout.Visible = False
            End If

            oAdp.Dispose()
            conn.Close()
            'calculate_total(Page.Master)
        Catch ex As Exception
            oAdp.Dispose()
            conn.Close()
            Throw
        End Try
    End Sub

    Protected Sub ASPxGridView1_HtmlRowPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs)
        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        Dim lblWarrable As Label = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_warrable"), Label)
        Dim lblWarrSku As Label = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_warr_sku"), Label)
        Dim lblWarr As Label = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_warr"), Label)
        'Dim warrCb As CheckBox = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "SelectWarr"), CheckBox)
        Dim warrCb As ASPxCheckBox = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "SelectWarr"), ASPxCheckBox)

        If lblWarrable.Text = "Y" AndAlso e.GetValue("ITM_TP_CD") <> SkuUtils.ITM_TP_PKG Then
            warrCb.Enabled = True

            If lblWarrSku.Text <> "" Then
                lblWarr.Visible = True
                lblWarrSku.Visible = True
                warrCb.Checked = True
            Else
                lblWarr.Visible = False
                lblWarrSku.Visible = False
                warrCb.Checked = False
            End If
        Else ' not warrantable
            lblWarr.Visible = False
            lblWarrSku.Visible = False
            warrCb.Checked = False
            warrCb.Enabled = False
        End If
    End Sub

    Public Sub cashier1_populate()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdGetCashiers As OracleCommand
        cmdGetCashiers = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY FullNAME"

        Try
            With cmdGetCashiers
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCashiers)
            oAdp.Fill(slsp_ds)

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    Protected Sub chk_slsp_view_all_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindgrid()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sql As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim frt_factor As Double = 0
        Dim repl_cst As Double = 0
        Dim ret_prc As Double = 0
        Dim qty As Double = 0
        Dim total_ret_prc As Double = 0
        Dim total_repl_cst As Double = 0

        'Open Connection 
        conn.Open()
        conn2.Open()

        sql = "select ret_prc, itm_cd, qty from temp_itm where session_id='" & Session.SessionID.ToString & "'"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            Do While MyDataReader2.Read()
                If IsNumeric(MyDataReader2.Item("RET_PRC").ToString) Then ret_prc = CDbl(MyDataReader2.Item("RET_PRC").ToString)
                If IsNumeric(MyDataReader2.Item("QTY").ToString) Then qty = CDbl(MyDataReader2.Item("QTY").ToString)

                sql = "select repl_cst, frt_fac from itm where itm_cd='" & MyDataReader2.Item("ITM_CD").ToString & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                    If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                    If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                End If
                MyDataReader.Close()
                total_ret_prc = total_ret_prc + (ret_prc * qty)
                total_repl_cst = total_repl_cst + (repl_cst * qty)
            Loop
            MyDataReader2.Close()
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        conn.Close()
        conn2.Close()

        lbl_gm.Text = "Retail: " & FormatCurrency(total_ret_prc, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(total_ret_prc - total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((total_ret_prc - total_repl_cst) / total_ret_prc, 2) & "<br />"
        ASPxPopupControl6.ShowOnPageLoad = True
    End Sub

    Private Function initTempItmReq() As HBCG_Utils.CreateTempItmReq
        Dim tempItmReq As HBCG_Utils.CreateTempItmReq = New HBCG_Utils.CreateTempItmReq()

        tempItmReq.sessId = Session.SessionID.ToString.Trim
        tempItmReq.empCd = Session("emp_cd")
        tempItmReq.tranDt = Session("tran_dt")
        tempItmReq.defLocCd = Session("loc_cd")
        tempItmReq.newSpecOrd = False
        tempItmReq.takeWith = True
        tempItmReq.warrRowLink = 0
        tempItmReq.warrItmLink = ""
        tempItmReq.soHdrInf.isCashNCarry = "TRUE"
        tempItmReq.soHdrInf.writtenDt = Session("tran_dt")
        tempItmReq.soHdrInf.writtenStoreCd = Session("store_cd")
        tempItmReq.soHdrInf.custTpPrcCd = IIf(IsNothing(Session("cust_tp_prc_cd")), "", Session("cust_tp_prc_cd"))
        tempItmReq.soHdrInf.orderTpCd = AppConstants.Order.TYPE_SAL
        tempItmReq.soHdrInf.slsp1 = Session("slsp1")
        tempItmReq.soHdrInf.slsp2 = IIf(IsNothing(Session("slsp2")), "", Session("slsp2"))
        tempItmReq.soHdrInf.slspPct1 = Session("pct_1")
        tempItmReq.soHdrInf.slspPct2 = Session("pct_2")
        tempItmReq.soHdrInf.taxExemptCd = IIf(IsNothing(Session("tet_cd")), "", Session("tet_cd"))
        tempItmReq.soHdrInf.taxCd = IIf(IsNothing(Session("tax_cd")), "", Session("tax_cd"))

        Dim taxPcts As TaxUtils.taxPcts = SetTxPcts(tempItmReq.soHdrInf.writtenDt, tempItmReq.soHdrInf.taxCd)

        If taxPcts IsNot Nothing Then
            tempItmReq.soHdrInf.taxRates = taxPcts.itmTpPcts
        End If

        Return tempItmReq
    End Function

    Private Function insertTmpItemRec(ByVal sku As String) As HBCG_Utils.CreateTempItmResp
        Dim createResp As New HBCG_Utils.CreateTempItmResp

        If SystemUtils.isNotEmpty(sku) Then
            Dim row_id_seq_num As String = ""
            Dim tempItmReq As HBCG_Utils.CreateTempItmReq = initTempItmReq()
            tempItmReq.itmCd = sku

            ' CREATE the TEMP_ITM line
            createResp = HBCG_Utils.CreateTempItm(tempItmReq)

            If createResp.errMsg.isNotEmpty Then
                'lbl_warnings.Text = createResp.errMsg
                'Exit Function
                GoTo TheExit
            ElseIf createResp.numRows < 1 Then
                createResp.errMsg = "An invalid SKU has been entered."
                'lbl_warnings.Text = "An invalid SKU has been entered."
                'Exit Function
                GoTo TheExit
            End If

            ' extract the TEMP_ITM.ROW_ID (not the Oracle rowid) in case linking a treatment or a warranty
            If (createResp.isInventory AndAlso createResp.isWarrantable) OrElse
                (ConfigurationManager.AppSettings("fab_populate").ToString = "Y" AndAlso createResp.isTreatable) Then

                row_id_seq_num = SalesUtils.GetTempItmRowSeqNum(createResp.insertedSkuRowId)
            Else
                ' MM-6305
                ' for the package SKU need the row id number
                row_id_seq_num = SalesUtils.GetTempItmRowSeqNum(createResp.insertedSkuRowId)
                createResp.rowIdSequenceNumber = row_id_seq_num
            End If

            ' check if need to apply warranty to the selectedSKU
            If createResp.isInventory AndAlso createResp.isWarrantable Then
                isWarrantieFlag = True
                'Check_Warranty(createResp.selectedSku, row_id_seq_num)
                intRowID = row_id_seq_num
            End If

            'Check the INV_MNR_CAT table for fab protection to auto-insert, if system parameters allow it
            If createResp.isTreatable AndAlso ConfigurationManager.AppSettings("fab_populate").ToString = "Y" AndAlso _
                createResp.catCd.isNotEmpty Then
                sku = SkuUtils.GetTreatmentItmCd(createResp.mnrCd, createResp.catCd)

                If sku.isNotEmpty Then
                    ' use the same session variable as previous
                    tempItmReq.itmCd = sku

                    ' CREATE the treatment TEMP_ITM line
                    Dim createRespFab As HBCG_Utils.CreateTempItmResp = HBCG_Utils.CreateTempItm(tempItmReq)

                    If createRespFab.errMsg.isNotEmpty Then
                        'lbl_warnings.Text = createRespFab.errMsg
                        'Exit Function
                        GoTo TheExit
                        ' no need to test for invalid SKU, we selected this one out of the database
                    End If

                    createResp.User_Questions = createResp.User_Questions & createRespFab.User_Questions & vbCrLf
                    createResp.dropped_dialog = createResp.dropped_dialog & createRespFab.dropped_dialog & vbCrLf
                    'FAB SKU cannot be frame or pkg 
                    ' no check on warranty, this is a FAB type SKU; E1 allows warranties on FAB but POS+ has not yet

                    '  Update the treatment link information (temp_itm.row_id) on the treatable SKU
                    Dim updtReq As New TempItmUpdateRequest
                    updtReq.tmpItm = New TempItm
                    row_id_seq_num = SalesUtils.GetTempItmRowSeqNum(createRespFab.insertedSkuRowId)
                    updtReq.tmpItm.treatedBy = row_id_seq_num

                    If updtReq.tmpItm.treatedBy.isNotEmpty Then
                        updtReq.tmpItm.treated = "Y"
                    Else
                        updtReq.tmpItm.treated = ""
                    End If

                    updtReq.treatedByUpdt = True
                    updtReq.treatedUpdt = True
                    updtReq.tmpItm.rowId = createResp.insertedSkuRowId
                    updtReq.UpdtBy = temp_itm_key_tp.tempItmByOraRowId
                    ' UPDATE the TEMP_ITM line with the treatment link
                    HBCG_Utils.UpdtTempItm(updtReq)
                End If
            End If
            'Session("itemid") = "N/A"  ' moved to above - watch this for a while
        Else
            createResp.errMsg = "Invalid SKU"
        End If
TheExit: Return createResp
    End Function

    Private Sub InsertItems()
        Dim objCreateTempItmResp As New HBCG_Utils.CreateTempItmResp
        Dim _lblWarnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")
        Dim itemcd As String = String.Empty
        Dim sbUserQuestions As New StringBuilder
        Dim sbCustomSKUs As New StringBuilder
        Dim sbPrevent As New StringBuilder
        Dim cntPrevent As Integer
        Dim totalSKU As Integer = 0

        If Not IsNothing(Session("itemid")) AndAlso (Session("itemid") & "" <> "" And Session("itemid") <> "N/A") Then
            Dim showWarrPopup As Boolean = False

            itemcd = Session("itemid")
            Dim itms As String() = Array.ConvertAll(itemcd.TrimStart(",").TrimEnd(",").Split(","c), Function(p) p.Trim())
            Dim validItems As String() = itms

            For Each itm As String In itms
                If theSkuBiz.IsSellable(itm) Then
                    objCreateTempItmResp = insertTmpItemRec(itm)

                    If objCreateTempItmResp.errMsg.isNotEmpty Then
                        _lblWarnings.Text = objCreateTempItmResp.errMsg
                        validItems = validItems.Where(Function(w) w <> itm).ToArray()
                        'Exit Sub
                    End If

                    If objCreateTempItmResp.User_Questions & "" <> "" Then
                        sbUserQuestions.Append(objCreateTempItmResp.User_Questions)
                    End If

                    If objCreateTempItmResp.PKG_SKUS & "" <> "" Then
                        Dim errorMessage As String = String.Empty
                        Dim isWarrantableComps As Boolean = AddPackages(objCreateTempItmResp.PKG_SKUS.ToString, errorMessage)
                        If Not showWarrPopup AndAlso isWarrantableComps Then showWarrPopup = isWarrantableComps
                        ' MM-6305 
                        ' When there are no components for the Package SKU then an exception will occur  
                        ' Package Parent item needs to be removed from the TEMP_ITM
                        If Not (String.IsNullOrWhiteSpace(errorMessage)) Then
                            HBCG_Utils.deleteTempItmByRowId(objCreateTempItmResp.rowIdSequenceNumber)
                            _lblWarnings.Text = errorMessage
                            validItems = validItems.Where(Function(w) w <> itm).ToArray()
                            'Exit Sub
                        Else
                            _lblWarnings.Text = String.Empty
                        End If
                    End If

                    If objCreateTempItmResp.CUSTOM_SKUS & "" <> "" Then
                        sbCustomSKUs.Append(objCreateTempItmResp.CUSTOM_SKUS)
                    End If
                Else
                    preventMessageFlag = True

                    If cntPrevent > 0 Then
                        sbPrevent = sbPrevent.Append(",")
                    End If

                    sbPrevent = sbPrevent.Append(itm)
                    totalSKU = totalSKU + 1
                    cntPrevent = cntPrevent + 1
                End If

                If Not showWarrPopup Then showWarrPopup = Not preventMessageFlag _
                    AndAlso isWarrantieFlag AndAlso SalesUtils.hasWarranty(itm)
            Next

            If Not sbPrevent Is Nothing And Not sbPrevent.ToString.Trim = String.Empty Then
                If totalSKU > 1 Then
                    strPreventMessage = String.Format(Resources.POSMessages.MSG0016, UCase(sbPrevent.ToString))
                Else
                    strPreventMessage = String.Format(Resources.POSMessages.MSG0018, UCase(sbPrevent.ToString))
                End If

                _lblWarnings.Text = strPreventMessage
            End If

            hidIsWarrChecked.Value = False
            If showWarrPopup Then ShowWarrantyPopup(String.Empty)

            'Related SKU popup show
            hidItemCd.Value = String.Join(",", validItems.AsEnumerable())

            If Not String.IsNullOrEmpty(g_PkgItems) Then
                hidItemCd.Value = hidItemCd.Value & g_PkgItems
            End If

            Dim hasRelatedSKU As Boolean = False

            If hidIsWarrChecked.Value = False Then
                hasRelatedSKU = True
                Dim relatedSku As String = hidItemCd.Value
                hidItemCd.Value = String.Empty

                ShowRelatedSkuPopUp(relatedSku)
            End If

            'Open up a window for the custom order SKUS so the user can select the appropriate options
            If sbCustomSKUs.ToString <> "" Then
                Dim customSkus As String = Left(sbCustomSKUs.ToString, Len(sbCustomSKUs.ToString) - 1)
                Dim strScript As String =
                    "<script type='text/javascript'>window.open('customorderentry.aspx?itm_cd=" & customSkus &
                    "','frmTest','height=390px,width=480px,top=50,left=50,status=no,toolbar=no,menubar=no,scrollbars=yes');</script>"
                ClientScript.RegisterStartupScript(Me.GetType(), "AvisoScript", strScript)
            End If

            Session("itemid") = ""
            'Session("itemid") = "N/A"  ' moved to above - watch this for a while
            If sbUserQuestions.ToString & "" <> "" Then
                lbl_questions.Text = sbUserQuestions.ToString
            End If
        End If

        calculate_total()

        Dim _txtSKU As DevExpress.Web.ASPxEditors.ASPxTextBox = ASPxRoundPanel2.FindControl("txt_SKU")
        _txtSKU.Focus()
    End Sub

    Public Sub determine_tax(store_postalCd As String)
        'Determine the appropriate tax code
        Dim tax_cd As String
        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim itm_prc As String = ""
        Dim PrcArray As Array
        Dim Calc_Ret As Double

        If Session("TAX_CD") & "" <> "" Then
            Exit Sub
        End If

        Dim theZipCd = theCustomerBiz.GetCustomerZip(Session.SessionID.ToString, Session("CUST_CD"))
        tax_cd = TaxUtils.Get_Tax_CD(Session("pd_store_cd"), theZipCd, "N", Session("store_cd"), Session("tran_dt"), Session("PD"))

        If String.IsNullOrEmpty(tax_cd & "") OrElse InStr(tax_cd, ";") > 0 Then
            'show the tax window to allow user to choose the tax code
            Dim _hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
            _hidZip.Value = store_postalCd

            ucTaxUpdate.LoadTaxData()
            PopupTax.ShowOnPageLoad = True
        Else
            Session("TAX_RATE") = ""
            Session("TAX_CD") = tax_cd
            '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
            '** Since the system is auto assigning the tax, maunal code should be set to nothing.
            Session("MANUAL_TAX_CD") = Nothing

            'Add tax rates to the inventory, if items exist
            Dim connlocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


            connlocal.Open()
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            Dim objlocal As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim localdatareader As OracleDataReader

            SQL = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
            SQL = SQL & "FROM TAT, TAX_CD$TAT, TAX_CD "
            SQL = SQL & "WHERE TAT.EFF_DT <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            SQL = SQL & "AND TAX_CD$TAT.TAX_CD = TAX_CD.TAX_CD "
            SQL = SQL & "AND TAX_CD.TAX_CD IN ('" & Replace(tax_cd, ";", "','") & "') "
            SQL = SQL & "GROUP BY TAX_CD$TAT.TAX_CD, TAX_CD.DES"
            Dim taxability As String = ""
            Dim MyDataReader As OracleDataReader
            Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim dv As DataView
            Dim MyTable As DataTable
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            ds = New DataSet

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    Session("TAX_RATE") = MyDataReader.Item("SumofPCT").ToString

                    SQL = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_PCT=0, TAX_AMT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
                    With cmdDeleteItems
                        .Connection = connlocal
                        .CommandText = SQL
                    End With
                    cmdDeleteItems.ExecuteNonQuery()

                    SQL = "select row_id, itm_tp_cd, package_parent from temp_itm where session_id='" & Session.SessionID.ToString & "'"
                    objlocal = DisposablesManager.BuildOracleCommand(SQL, connlocal)

                    Try
                        localdatareader = DisposablesManager.BuildOracleDataReader(objlocal)

                        Do While localdatareader.Read
                            If (Session("TAX_CD") & "" <> "") Then
                                taxability = TaxUtils.Determine_Taxability(localdatareader.Item("ITM_TP_CD").ToString, Date.Today, Session("TAX_CD"))
                            End If
                            If taxability = "Y" Then
                                If localdatareader.Item("PACKAGE_PARENT").ToString & "" = "" Then
                                    SQL = "UPDATE TEMP_ITM SET TAXABLE_AMT=RET_PRC, TAX_AMT=ROUND((RET_PRC*(NVL(" & MyDataReader.Item("SumofPCT").ToString & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_CD='" & MyDataReader.Item("TAX_CD").ToString & "', TAX_PCT=" & MyDataReader.Item("SumofPCT").ToString & " WHERE ROW_ID='" & localdatareader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                                Else
                                    SQL = "UPDATE TEMP_ITM SET TAX_CD='" & MyDataReader.Item("TAX_CD").ToString & "', TAX_AMT=ROUND((TAXABLE_AMT*(NVL(" & MyDataReader.Item("SumofPCT").ToString & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_PCT=" & MyDataReader.Item("SumofPCT").ToString & " WHERE ROW_ID='" & localdatareader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                                End If
                                With cmdDeleteItems
                                    .Connection = connlocal
                                    .CommandText = SQL
                                End With
                                cmdDeleteItems.ExecuteNonQuery()
                            End If
                        Loop
                        connlocal.Close()
                    Catch ex As Exception
                        conn.Close()
                        Throw
                    End Try
                End If
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()

    End Sub

    Protected Sub cmd_add_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txt_SKU As DevExpress.Web.ASPxEditors.ASPxTextBox = ASPxRoundPanel2.FindControl("txt_SKU")
        Dim lbl_warnings As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel2.FindControl("lbl_warnings")
        If txt_SKU.Text & "" <> "" Then

            If SystemUtils.isEmpty(Session("itemid")) Then
                'isnothing(session("itemid")) orelse (session("itemid") + "").isempty then
                Session("itemid") = UCase(txt_SKU.Text)
            Else
                Session("itemid") = Session("itemid") & "," & UCase(txt_SKU.Text)
            End If
            'add the sku lines
            'insert_records()
            InsertItems()
            'populate datagrid with new records
            bindgrid()
        Else
            lbl_warnings.Text = "please enter a sku"
        End If
        txt_SKU.Text = ""
        GridView1.Visible = True
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl3.ShowOnPageLoad = True

    End Sub

    ''' <summary>
    ''' Retrieves from the CUST table, the customer that matches the customer code to be used
    ''' for Cash-Carry transaction (as defined in the app properties) and if found, inserts
    ''' a record in the CUST_INFO table, to identify the customer being used this session
    ''' </summary>
    Private Sub GetCashCarryCustomer()
        Dim ccCustCd As String = ConfigurationManager.AppSettings("cash_carry")
        If (ccCustCd.isNotEmpty) Then
            'retrieves the cash-carry customer from the CUST table
            Dim ccCustomer = theCustomerBiz.GetCustomer(ConfigurationManager.AppSettings("cash_carry"))
            If (ccCustomer.custCd.isNotEmpty) Then
                'inserts a record in the CUST_INFO table with the current session
                theCustomerBiz.InsertCustomer(Session.SessionID.ToString, ccCustomer)
            End If
        End If
    End Sub


    Public Sub calculate_total()

        ' This logic is not exactly the same as the one in HBCG_Utils. - no setup/del charge logic here or wrong, no tax exempt, other stuff
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim numrows As Integer
        Dim total As Double
        Dim tax As Double

        Dim Session_String As String

        Session_String = HttpContext.Current.Session.SessionID

        If ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED" And HttpContext.Current.Session("PD") = "D" Then
            Dim Delivery_Amount As Double
            Dim objsql As OracleCommand
            sql = "select sum(ret_prc*qty) As Total_Order from temp_itm where itm_tp_cd = 'INV' AND Session_id='" & Session_String & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If MyDataReader.Read Then
                If MyDataReader.Item(0).ToString & "" <> "" Then
                    Delivery_Amount = CDbl(MyDataReader.Item(0).ToString)
                End If
            End If
            MyDataReader.Close()
            sql = "select p_or_d, amount from tiered_delivery where lower_limit < " & Delivery_Amount & " AND upper_limit >= " & Delivery_Amount
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If MyDataReader.Read Then
                If MyDataReader.Item(0) = "D" Then
                    HttpContext.Current.Session("DEL_CHG") = CDbl(MyDataReader.Item(1).ToString)
                Else
                    HttpContext.Current.Session("DEL_CHG") = (Delivery_Amount * (CDbl(MyDataReader.Item(1).ToString) / 100))
                End If
            End If
        End If

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "select sum(ret_prc*qty) As Total_Order, sum(tax_amt*qty) As Total_Tax from temp_itm where session_id='" & Session_String & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        Try
            If IsNumeric(MyTable.Rows(0).Item("Total_Order")) Then
                total = total + MyTable.Rows(0).Item("Total_Order")
            Else
                total = total + 0
            End If
            If IsNumeric(MyTable.Rows(0).Item("Total_Tax")) Then
                tax = tax + MyTable.Rows(0).Item("Total_Tax")
            Else
                tax = tax + 0
            End If

            '
            ' Changed to reflect the session variable which is calculated at the POS
            '

            Dim del_tax As Double = 0
            Dim setup_tax As Double = 0
            Dim del_chg As Double = 0
            Dim setup_chg As Double = 0
            Dim grandtotal As Double = 0

            del_tax = HttpContext.Current.Session("DEL_TAX")    ' TODO - these are not used ???  maybe just not application to Cash and Carry
            setup_tax = HttpContext.Current.Session("SU_TAX")   ' TODO - these are not used ???

            ' TODO - someday this needs to be based on tax code tax method
            If "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                'If Header tax Then

                ' need the written date and for new sales, this is the written date logic from Order Stage
                Dim wr_Dt As Date
                If IsDate(HttpContext.Current.Session("tran_dt")) Then
                    wr_Dt = FormatDateTime(HttpContext.Current.Session("tran_dt"), DateFormat.ShortDate)
                Else
                    wr_Dt = FormatDateTime(Today.Date, DateFormat.ShortDate)
                End If

                ' TODO - someday when CCExpress is also single transaction, this can be pulled and just pass atomic command to the tax calc
                Dim connTax As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

                cmd.Connection = connTax
                Try
                    connTax.Open()
                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session_String, HttpContext.Current.Session("tax_cd"), wr_Dt, setup_chg, del_chg, OrderUtils.tempItmSelection.All)
                    cmd.Dispose()
                    connTax.Close()

                Catch ex As Exception
                    cmd.Dispose()
                    connTax.Close()
                    Throw
                End Try
            End If

            lblSubtotal.Text = FormatCurrency(CDbl(total))
            lblDelivery.Text = FormatCurrency(CDbl(del_chg) + CDbl(setup_chg))

            If Not String.IsNullOrEmpty(HttpContext.Current.Session("tet_cd")) Then
                tax = 0
            End If

            lblTotal.Text = FormatCurrency(CDbl(total) + CDbl(tax) + CDbl(del_chg) + CDbl(setup_chg), 2)
            grandtotal = FormatCurrency(CDbl(total) + CDbl(tax) + CDbl(del_chg) + CDbl(setup_chg), 2)
            HttpContext.Current.Session("grand_total") = grandtotal

            If IsNumeric(tax) Then
                HttpContext.Current.Session("TAX_CHG") = tax
                lblTax.Text = FormatCurrency(CDbl(tax))
            End If

            If IsNumeric(total) Then
                HttpContext.Current.Session("sub_total") = CDbl(total)
            End If

            sql = "SELECT sum(amt) As Payment FROM payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session.SessionID.ToString.Trim & "'"

            'Set SQL OBJECT 
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            Dim Payment_total As Double = 0
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If IsNumeric(MyDataReader.Item("Payment").ToString) Then
                        Payment_total = CDbl(MyDataReader.Item("Payment").ToString.Trim)
                    End If
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If HttpContext.Current.Session("payment") & "" <> "" Then
                lbl_Payments.Text = FormatCurrency(CDbl(Payment_total), 2)
            Else
                lbl_Payments.Text = FormatCurrency(0, 2)
            End If

            If IsNumeric(lbl_Payments.Text) And IsNumeric(grandtotal) Then
                lbl_balance.Text = FormatCurrency(CDbl(grandtotal) - CDbl(lbl_Payments.Text), 2)
            Else
                lbl_balance.Text = FormatCurrency(0, 2)
            End If

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub

    ''' <summary>
    ''' create an object of tax percentages by item type as the taxes are applied by tax date and tax code; if the session variable object
    '''   already exists and was created for thee input tax date and tax code, there is no reason to re-acquire - save some db hits
    ''' </summary>
    Private Function SetTxPcts(ByVal tax_dt As String, ByVal tax_cd As String) As TaxUtils.taxPcts

        Dim txPcts As TaxUtils.taxPcts
        If tax_cd.isNotEmpty AndAlso tax_dt.isNotEmpty AndAlso IsDate(tax_dt) Then

            If IsNothing(Session("taxPcts")) Then

                txPcts = TaxUtils.SetTaxPcts(tax_dt, tax_cd)
                Session("taxPcts") = txPcts

            Else
                txPcts = Session("taxPcts")

                If txPcts.taxCd.isNotEmpty AndAlso txPcts.taxCd = tax_cd AndAlso
                     (Not IsNothing(txPcts.taxDt)) AndAlso txPcts.taxDt = CDate(tax_dt) Then

                    txPcts = Session("taxPcts")

                Else
                    txPcts = TaxUtils.SetTaxPcts(tax_dt, tax_cd)
                    Session("taxPcts") = txPcts
                End If
            End If
        End If

        Return txPcts
    End Function

    'Private Sub Check_Warranty(ByVal itm_cd As String, ByVal row_id_seq_num As String)

    '    Dim strScript As String

    '    If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then

    '        Dim warranty_skus As Boolean = SalesUtils.hasWarranty(itm_cd)

    '        If warranty_skus = True Then

    '            If SysPms.isOne2ManyWarr Then

    '                HBCG_Utils.CreateTempWarrFromTempItm(Session.SessionID.ToString.Trim, itm_cd)
    '            End If
    '            ASPxPopupControl9.ContentUrl = "Warranties.aspx?referrer=CashCarryExpress.aspx&itm_cd=" & itm_cd & "&itmRowNum=" & row_id_seq_num
    '            warrantiePopupURL = ASPxPopupControl9.ContentUrl
    '            If preventMessageFlag = True And isWarrantieFlag = True Then
    '                ASPxPopupControl9.ContentUrl = "" & warrantiePopupURL & "&preventMessage=" & strPreventMessage & ""
    '            Else
    '                'do nothing
    '            End If
    '            'strScript = "<script type='text/javascript'>window.open('Warranties.aspx?itm_cd=" & itm_cd & "','frmTest" & itm_cd & "','height=390px,width=560px,top=50,left=50,status=no,toolbar=no,menubar=no,scrollbars=yes');</script>"
    '            'ClientScript.RegisterStartupScript(Me.GetType(), "AvisoScript" & itm_cd, strScript)
    '        Else
    '            If preventMessageFlag = True And isWarrantieFlag = True Then
    '                ASPxPopupControl9.ContentUrl = "" & warrantiePopupURL & "&preventMessage=" & strPreventMessage & ""
    '            Else
    '                'do nothing
    '            End If
    '        End If
    '        ASPxPopupControl9.ShowOnPageLoad = True
    '    End If

    'End Sub

    Public Function Find_Promotions(ByVal sku As String)

        Dim conn6 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim MyDatareader6 As OracleDataReader
        Dim objsql6 As OracleCommand

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn6.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'Open Connection 
        conn6.Open()

        sql = "SELECT PRC FROM PROM_PRC_TP, STORE_PROM_PRC WHERE PROM_PRC_TP.PRC_CD=STORE_PROM_PRC.PRC_CD "
        'jkl
        'sql = sql & "AND ITM_CD='" & sku & "' AND STORE_CD='" & Session("store_cd") & "' AND EFF_DT<=TO_DATE('"
        sql = sql & "AND ITM_CD=:SKU AND STORE_CD=:STORE_CD AND EFF_DT<=TO_DATE('"

        sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
        sql = sql & "ORDER BY PRC"

        'Set SQL OBJECT 
        objsql6 = DisposablesManager.BuildOracleCommand(sql, conn6)
        'jkl
        objsql6.Parameters.Add(":SKU", OracleType.VarChar)
        objsql6.Parameters(":SKU").Value = sku
        objsql6.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objsql6.Parameters(":STORE_CD").Value = Session("store_cd")

        Try
            'Execute DataReader 
            MyDatareader6 = DisposablesManager.BuildOracleDataReader(objsql6)


            If MyDatareader6.Read Then
                Find_Promotions = MyDatareader6.Item("PRC").ToString
                conn6.Close()
                Exit Function
            End If
            MyDatareader6.Close()
        Catch
            conn6.Close()
            Throw
        End Try

        sql = "SELECT PRC FROM PROM_PRC_TP, STORE_GRP_PROM_PRC  WHERE PROM_PRC_TP.PRC_CD=STORE_GRP_PROM_PRC .PRC_CD "
        sql = sql & "AND ITM_CD=:SKU AND STORE_GRP_CD IN (SELECT STORE_GRP_CD FROM STORE_GRP$STORE WHERE STORE_CD=:STORE_CD) AND EFF_DT<=TO_DATE('"
        sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
        sql = sql & "ORDER BY PRC"

        'Set SQL OBJECT 
        objsql6 = DisposablesManager.BuildOracleCommand(sql, conn6)
        'jkl
        objsql6.Parameters.Add(":SKU", OracleType.VarChar)
        objsql6.Parameters(":SKU").Value = sku
        objsql6.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objsql6.Parameters(":STORE_CD").Value = Session("store_cd")

        Try
            'Execute DataReader 
            MyDatareader6 = DisposablesManager.BuildOracleDataReader(objsql6)


            If MyDatareader6.Read Then
                Find_Promotions = MyDatareader6.Item("PRC").ToString
                conn6.Close()
                Exit Function
            End If
            MyDatareader6.Close()
        Catch
            conn6.Close()
            Throw
        End Try
        conn6.Close()
        Find_Promotions = ""

    End Function

    Public Function AddPackages(ByVal pkgSkus As String, Optional ByRef errorMessage As String = "") As Boolean
        Dim showWarrPopup As Boolean = False
        ' create the object for header and transaction info     
        Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                               Session("slsp1"), Session("slsp2"),
                                                               Session("pct_1"),
                                                               IIf(IsNothing(Session("pct_2")), 0, Session("pct_2")),
                                                               Session("tet_cd"), Session("TAX_CD"),
                                                               Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                               Session("PD"), Session("pd_store_cd"),
                                                               Session("cash_carry"))

        If Session("TAX_CD") IsNot Nothing OrElse Not String.IsNullOrEmpty(Session("TAX_CD")) Then
            headerInfo.writtenDt = Session("tran_dt")
            Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(headerInfo.writtenDt, headerInfo.taxCd)
            headerInfo.taxRates = taxPcts.itmTpPcts
        End If

        ' create pkg components in temp_itm
        Dim tmpCmpnts As DataTable = theSalesBiz.AddPkgCmpntsToTempItm(headerInfo,
                                                                       pkgSkus, Session.SessionID.ToString.Trim, Session("store_cd"), errorMessage, False)

        Dim drFilter() As DataRow = tmpCmpnts.Select("WARRANTABLE = 'Y' AND INVENTORY = 'Y'")
        'showWarrPopup = drFilter.Length > 0
        For Each drFil As DataRow In drFilter
            If Not showWarrPopup Then showWarrPopup = SalesUtils.hasWarranty(drFil("ITM_CD"))
        Next

        ' related SKUs uses the component list and requires a comma delimited string of single quoted item codes
        ' do not allow duplicates in the component list  - happens when qty > 1
        Dim cmpnts As DataTable = tmpCmpnts.DefaultView.ToTable(True, "itm_cd")
        Dim cmpntList As New StringBuilder
        For Each datRow In cmpnts.Rows
            cmpntList.Append(",'" & datRow("itm_cd") & "'")
        Next
        ' set into page global for use by related SKUs
        g_PkgItems = cmpntList.ToString
        Return showWarrPopup
    End Function

    Protected Sub ASPxMenu4_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu4.ItemClick

        Dim ipad As String = Session("IPAD")
        Dim empCd As String = Session("EMP_CD")
        Dim empFname As String = Session("EMP_FNAME")
        Dim empLname As String = Session("EMP_LNAME")
        Dim storeCd As String = Session("HOME_STORE_CD")
        Dim ordTpCd As String = Session("ORD_TP_CD")
        Dim companyCd As String = Session("CO_CD")

        ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CLEANUP     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        Dim strSessionId As String = If(IsNothing(Session.SessionID), "", Session.SessionID.ToString.Trim)
        If (strSessionId.isNotEmpty()) Then
            theSalesBiz.DeleteTempOrder(strSessionId)
        End If
        ' Daniela save clientip
        Dim client_IP As String = Session("clientip")
        Dim empInit As String = Session("EMP_INIT")
        Dim supUser As String = Session("str_sup_user_flag")
        Dim coGrpCd As String = Session("str_co_grp_cd")
        Session.Clear()
        Session("clientip") = client_IP
        Session("str_sup_user_flag") = supUser ' Daniela
        Session("str_co_grp_cd") = coGrpCd ' Daniela
        Session("EMP_INIT") = empInit
        Session("IPAD") = ipad
        Session("EMP_CD") = empCd
        Session("EMP_FNAME") = empFname
        Session("EMP_LNAME") = empLname
        Session("HOME_STORE_CD") = storeCd
        Session("ORD_TP_CD") = ordTpCd
        Session("CO_CD") = companyCd

        Response.Redirect("CashCarryExpress.aspx")

    End Sub

    'Protected Overrides Sub Finalize()
    '    MyBase.Finalize()
    'End Sub

    Protected Sub ASPxMenu5_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu5.ItemClick

        If CDbl(lbl_balance.Text) <> 0 Then
            txt_Follow_up.Text = "You must not have a balance on this order prior to commit."
            Exit Sub
        End If

        Perform_Commit_Audit(2)

        'MSC 8/5/13
        'The system is finding that there are no items but it is continuing to let the order save.  Adding logic to stop this.
        '
        If ASPxMenu5.Enabled = False Then Exit Sub

        If Session("cust_cd") = "NEW" Then
            Create_Customer()
        End If

        Dim tax_chg As Double
        If String.IsNullOrEmpty(Session("tet_cd")) Then
            tax_chg = 0
        End If

        If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
        Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("store_cd").ToString, Session("tran_dt"))

        If Not (soKeyInfo Is Nothing OrElse soKeyInfo.soDocNum Is Nothing OrElse soKeyInfo.soDocNum.delDocNum Is Nothing) Then
            If Len(soKeyInfo.soDocNum.delDocNum) <> 11 Then
                lbl_Status.Text = "<font color=red>Could not create order, please try again</font>"
                Exit Sub
            End If
        End If

        SaveOrder(tax_chg, soKeyInfo)

    End Sub


    Private Sub SaveOrder(ByVal tax_chg As Double, ByVal soKeys As OrderUtils.SalesOrderKeys)

        Session("err") = ""   ' need to clear this or remains from previous

        ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
        ' a single transaction to be able to rollback all changes if something fails
        Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql, sql2 As String
        Dim lngHowLong As Long = DateDiff("s", FormatDateTime(Today.Date, 2) & " 00:00:00", Now)

        Dim seq_num As Integer = 0        ' used for seq# on so_cmnt comment processing to break up to lines
        Dim doc_processing As Integer = 1 ' num of docs to create, used when processing to determine which; if 2, processing TW doc of a split; if 1, may be cash and carry or pu/del
        Dim soLn_num As Integer = 1       ' set to 1 for each document and incremented for each SO_LN for the del_doc_ln# assignment
        Dim del_doc_num As String = soKeys.soDocNum.delDocNum
        Dim err_msg As String = ""
        Dim isSplitTw As Boolean = False
        Dim split_order As Boolean = False
        Dim split_amt As Double = 0
        Dim ttax As String = "", ttax_cd As String = ""
        Dim newtotal As Double = 0
        Dim default_invoice As String = ""

        ' To make these double, either need to add 'as Double' for each, initialize with decimals or do not initialize in the same statement, 
        '    otherwise only the last entry ends up as double
        Dim del_chg = 0.0, setup_chg = 0.0, del_tax = 0.0, setup_tax As Double = 0
        Dim sub_total = 0.0, delivery = 0.0, tax = 0.0, grand_total As Double = 0

        Dim finAmt As Double = 0
        Dim finCo As String = ""
        Dim finPromoCd As String = ""
        Dim finAcctNum As String = ""
        Dim approvalCd As String = ""
        Dim wdr As Boolean = False
        Dim wdr_promo As String = ""
        Dim dpString As String = ""

        Try
            dbConnection.Open()

            ' ---- this update doesn't have to be part of the single transaction operation
            sql = "UPDATE TEMP_ITM SET TAKE_WITH='N' WHERE TAKE_WITH IS NULL AND Session_ID='" & Session.SessionID.ToString.Trim & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.ExecuteNonQuery()

            ' ---- determines if the order needs to be split
            sql = "SELECT sum(qty*ret_prc) as ord_sum, sum(qty*tax_amt) as tax_sum, tax_cd FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH='Y' group by tax_cd"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                If IsNumeric(dbReader.Item("ord_sum").ToString) Then
                    If CDbl(dbReader.Item("ord_sum").ToString) = CDbl(Session("sub_total")) Then
                        Session("PD") = "P"
                        Session("del_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
                        Session("zone_cd") = System.DBNull.Value
                        Session("del_chg") = System.DBNull.Value
                        Session("cash_carry") = "TRUE"
                    Else
                        doc_processing = doc_processing + 1
                        split_order = True
                        ttax = dbReader.Item("tax_sum").ToString
                        ttax_cd = dbReader.Item("tax_cd").ToString
                        newtotal = Session("sub_total")
                        Session("sub_total") = dbReader.Item("ord_sum").ToString
                    End If
                End If
            End If

            ' ---- Gathers Finance details
            sql = "SELECT AMT AS FIN_AMT, FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD FROM PAYMENT WHERE MOP_TP='FI' AND SessionID='" & Session.SessionID.ToString.Trim & "'"
            dpString = DP_String_Creation()
            If dpString & "" <> "" Then sql = sql & " AND MOP_CD NOT IN (" & dpString & ")"

            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                ' There will only be values/records found if this is a finance payment and either:
                ' 1) there were no 'DP' type finance providers setup in the system OR 
                ' 2) that there are some defined in the sytmem as 'DP' but the one used on this finance payment is not 'DP'
                If Not String.IsNullOrEmpty(dbReader.Item("FIN_CO").ToString) Then finCo = dbReader.Item("FIN_CO").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("APPROVAL_CD").ToString) Then approvalCd = dbReader.Item("APPROVAL_CD").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("FIN_PROMO_CD").ToString) Then finPromoCd = dbReader.Item("FIN_PROMO_CD").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("FI_ACCT_NUM").ToString) Then finAcctNum = Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                If IsNumeric(dbReader.Item("FIN_AMT").ToString.Trim) Then finAmt = CDbl(dbReader.Item("FIN_AMT").ToString.Trim)
            End If
            dbReader.Close()
        Catch ex As Exception
            dbConnection.Close()
            Throw
        End Try

        ' >>>>>>>>>>>>>>>>>>>>  Gets the CUSTOMER details to update the CUST table  <<<<<<<<<<<<<<<<<<<<
        ' ----- NOTE:  The CUSTOMER will always be updated even if the order fails to commit -----
        Dim customer As CustomerDtc = theCustomerBiz.GetCustomer(Session.SessionID.ToString, Session("cust_cd").ToString)
        Dim dLicense As DriverLicenseDtc = theCustomerBiz.GetDriverLicense(Session.SessionID.ToString)
        theCustomerBiz.UpdateCustomer(customer, dLicense, Session("tet_cd"), Session("exempt_id"))

        ' --- EVERY operation queued on the dbAtomicCommand is saved at once, to avoid orphan data related to the order
        Try
            dbAtomicConnection.Open()
            dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction

            Do While doc_processing > 0

                ' if processing a Delivery sale, then Checks Delivery Availability
                If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso doc_processing = 1 AndAlso Session("PD") = "D" Then
                    ' finds out the delivery points for the order being processed
                    Dim delPoints = theTMBiz.GetDeliveryPoints(Session.SessionID.ToString.Trim)
                    Dim isDelDtAvailable = False

                    sql = "SELECT DEL_DT, DAY, DEL_STOPS, ACTUAL_STOPS, CLOSED, (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS "
                    sql = sql & "FROM DEL "
                    sql = sql & "WHERE (ZONE_CD = '" & Session("ZONE_CD") & "') AND (DEL_DT = TO_DATE('" & Session("del_dt") & "','mm/dd/RRRR')) "
                    sql = sql & "ORDER BY DEL_DT"
                    dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If (dbReader.Read()) Then
                        If CDbl(FormatNumber(dbReader.Item("REMAINING_STOPS").ToString)) >= CDbl(FormatNumber(delPoints)) Then
                            sql = "UPDATE DEL SET ACTUAL_STOPS=" & CDbl(FormatNumber(dbReader.Item("ACTUAL_STOPS"))) + delPoints
                            sql = sql & " WHERE (ZONE_CD = '" & Session("ZONE_CD") & "') AND (DEL_DT = TO_DATE('" & Session("del_dt") & "','mm/dd/RRRR')) "
                            dbAtomicCommand.CommandText = sql
                            dbAtomicCommand.ExecuteNonQuery()
                            isDelDtAvailable = True
                        End If
                        dbReader.Close()
                    End If
                    If (Not isDelDtAvailable) Then
                        lbl_Status.Text = "The delivery date you have chosen is not available"
                        Exit Sub
                    End If
                End If

                If doc_processing = 1 Then ' cash and carry OR non-take-with portion of split OR just a regular P/D
                    If split_order AndAlso isSplitTw Then isSplitTw = False
                    If Len(del_doc_num) <> 11 Then del_doc_num = Left(del_doc_num, 11)
                    lbl_del_doc_num.Text = del_doc_num

                    ' --- calculates total tax
                    If HttpContext.Current.Session("sub_total") & "" <> "" Then
                        If IsNumeric(HttpContext.Current.Session("DEL_CHG")) Then
                            del_chg = CDbl(HttpContext.Current.Session("DEL_CHG"))
                            If IsNumeric(HttpContext.Current.Session("DEL_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                del_tax = CDbl(HttpContext.Current.Session("DEL_CHG")) * (HttpContext.Current.Session("DEL_TAX") / 100)
                            End If
                        End If
                        If IsNumeric(HttpContext.Current.Session("SETUP_CHG")) Then
                            setup_chg = CDbl(HttpContext.Current.Session("SETUP_CHG"))
                            If IsNumeric(HttpContext.Current.Session("SU_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                setup_tax = CDbl(HttpContext.Current.Session("SETUP_CHG")) * (HttpContext.Current.Session("SU_TAX") / 100)
                            End If
                        End If
                    End If

                    sql = "SELECT sum(qty*tax_amt) as tax_sum FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH <> 'Y'"
                    dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If (dbReader.Read()) Then
                        If IsNumeric(dbReader.Item("tax_sum").ToString) And String.IsNullOrEmpty(Session("tet_cd")) Then
                            ttax = dbReader.Item("tax_sum").ToString
                            ttax_cd = Session("tax_cd")
                        Else
                            ttax = 0
                        End If
                    End If
                    dbReader.Close()

                    If Not IsNumeric(ttax) Then ttax = 0
                    If String.IsNullOrEmpty(Session("tet_cd")) Then
                        ttax = Math.Round(CDbl(ttax + del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
                    Else
                        ttax = 0
                    End If

                    ' ---- adjusts order subtotal
                    If newtotal <> 0 Then Session("sub_total") = newtotal - CDbl(Session("sub_total"))

                ElseIf doc_processing = 2 Then  ' the take-with doc on a split
                    del_doc_num = del_doc_num & "A"
                    isSplitTw = True
                End If

                If Session("sub_total") & "" <> "" Then
                    sub_total = FormatCurrency(CDbl(Session("sub_total")))
                    If doc_processing <> 2 Then
                        If IsNumeric(Session("DEL_CHG")) Then
                            delivery = FormatNumber(CDbl(Session("DEL_CHG")), 2)
                        Else
                            delivery = FormatNumber(0, 2)
                        End If
                        If IsNumeric(Session("SETUP_CHG")) Then
                            setup_chg = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
                        Else
                            setup_chg = FormatNumber(0, 2)
                        End If
                    End If
                    If IsNumeric(ttax) And String.IsNullOrEmpty(Session("tet_cd")) Then
                        If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                            Dim taxDt As String = Session("tran_dt")
                            If "CRM".Equals(Session("ord_tp_cd")) AndAlso Session("adj_ivc_cd") & "" <> "" Then
                                If (Not Session("origSoWrDt") Is Nothing) AndAlso (Session("origSoWrDt").ToString + "").isNotEmpty AndAlso IsDate(Session("origSoWrDt").ToString) Then
                                    taxDt = FormatDateTime(Session("origSoWrDt").ToString, DateFormat.ShortDate)
                                Else
                                    Dim origSo As DataRow = OrderUtils.Get_Orig_Del_Doc(Session("adj_ivc_cd"))
                                    taxDt = origSo("so_wr_dt")  ' if there is an original sale for the return, then the tax should be based on the original sale tax
                                End If
                            End If
                            If isSplitTw Then
                                tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, 0.0, 0.0, OrderUtils.tempItmSelection.TakeWith)
                            ElseIf split_order Then
                                tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.NonTakeWith)
                            Else
                                tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.All)
                            End If
                        Else ' line tax calc left as was above
                            tax = CDbl(ttax)
                        End If
                    End If
                    grand_total = FormatCurrency((CDbl(Session("sub_total"))) + FormatNumber(CDbl(tax), 2) + FormatNumber(CDbl(delivery), 2) + FormatNumber(CDbl(setup_chg), 2), 2)
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO INFO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                Dim storeCd, soSeqNum As String
                sql = "INSERT INTO SO (ORIGIN_CD, DEL_DOC_NUM, CUST_CD, PU_DEL_STORE_CD, EMP_CD_OP, ORD_TP_CD, SO_STORE_CD, SO_EMP_SLSP_CD1, PCT_OF_SALE1, SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, PU_DEL, PU_DEL_DT, MASF_FLAG, SO_WR_SEC "
                sql2 = "VALUES('POS','" & del_doc_num & "','" & Session("cust_cd") & "','"
                If doc_processing = 2 Then
                    storeCd = Session("store_cd")
                Else
                    storeCd = Session("pd_store_cd")
                End If
                sql2 = sql2 & storeCd & "','"
                sql2 = sql2 & Session("emp_cd") & "','"
                sql2 = sql2 & Session("ord_tp_cd") & "','"
                sql2 = sql2 & Session("store_cd") & "','"
                sql2 = sql2 & Session("slsp1") & "','"
                sql2 = sql2 & Session("pct_1") & "','"
                sql2 = sql2 & Left(del_doc_num, 11) & "',"
                If IsDate(Session("tran_dt")) Then
                    Session("tran_dt") = FormatDateTime(Session("tran_dt"), DateFormat.ShortDate)
                Else
                    Session("tran_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
                End If
                sql2 = sql2 & "TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),'"

                soSeqNum = soKeys.soKey.seqNum
                sql2 = sql2 & soSeqNum & "','"
                If doc_processing = 2 Then
                    sql2 = sql2 & "P',"
                    sql2 = sql2 & "TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),'"
                Else
                    sql2 = sql2 & Session("PD") & "',"
                    sql2 = sql2 & "TO_DATE('" & Session("del_dt") & "','mm/dd/RRRR'),'"
                End If
                sql2 = sql2 & ConfigurationManager.AppSettings("mass").ToString & "','" & lngHowLong & "'"
                If doc_processing <> 2 Then
                    If IsNumeric(Session("DEL_CHG")) Then
                        sql = sql & ", DEL_CHG "
                        sql2 = sql2 & ", " & Session("DEL_CHG")
                    End If
                    If IsNumeric(Session("SETUP_CHG")) Then
                        sql = sql & ", SETUP_CHG "
                        sql2 = sql2 & ", " & Session("SETUP_CHG")
                    End If
                    If finAmt > 0 Then
                        sql = sql & ", ORIG_FI_AMT "
                        sql2 = sql2 & ", " & finAmt
                    End If
                    If Not String.IsNullOrEmpty(approvalCd) Then
                        sql = sql & ", APPROVAL_CD "
                        sql2 = sql2 & ", '" & approvalCd & "' "
                    End If
                    If Not String.IsNullOrEmpty(finCo) Then
                        sql = sql & ", FIN_CUST_CD "
                        sql2 = sql2 & ", '" & finCo & "'"
                    End If
                End If
                If Session("PO") & "" <> "" And ConfigurationManager.AppSettings("show_po").ToString = "Y" Then
                    sql = sql & ", ORDER_PO_NUM "
                    sql2 = sql2 & ", '" & Session("PO") & "'"
                End If
                If Session("USR_FLD_1") & "" <> "" Then
                    sql = sql & ", USR_FLD_1 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_1") & "'"
                End If
                If Session("USR_FLD_2") & "" <> "" Then
                    sql = sql & ", USR_FLD_2 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_2") & "'"
                End If
                If Session("USR_FLD_3") & "" <> "" Then
                    sql = sql & ", USR_FLD_3 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_3") & "'"
                End If
                If Session("USR_FLD_4") & "" <> "" Then
                    sql = sql & ", USR_FLD_4 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_4") & "'"
                End If
                If Session("USR_FLD_5") & "" <> "" Then
                    sql = sql & ", USR_FLD_5 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_5") & "'"
                End If
                If Session("tet_cd") & "" <> "" Then
                    sql = sql & ", TET_CD "
                    sql2 = sql2 & ", '" & Session("TET_CD") & "'"
                End If
                If doc_processing <> 2 And Session("cash_carry") <> "TRUE" Then
                    If Session("ZONE_CD") & "" <> "" Then
                        sql = sql & ", SHIP_TO_ZONE_CD "
                        sql2 = sql2 & ", '" & Session("ZONE_CD") & "'"
                    End If
                    sql = sql & ", STAT_CD "
                    sql2 = sql2 & ", 'O'"
                Else
                    sql = sql & ", CASH_AND_CARRY "
                    sql2 = sql2 & ", 'Y'"
                    sql = sql & ", STAT_CD "
                    sql2 = sql2 & ", 'F'"
                    sql = sql & ", FINAL_STORE_CD "
                    sql2 = sql2 & ", '" & Session("store_cd") & "'"
                    sql = sql & ", FINAL_DT "
                    sql2 = sql2 & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"
                End If
                If AppConstants.Order.TYPE_SAL <> Session("ord_tp_cd") AndAlso Session("crm_cause_cd") & "" <> "" Then
                    sql = sql & ", CAUSE_CD "
                    sql2 = sql2 & ", '" & Session("crm_cause_cd") & "'"
                End If
                If AppConstants.Order.TYPE_SAL <> Session("ord_tp_cd") AndAlso Session("adj_ivc_cd") & "" <> "" Then
                    sql = sql & ", ORIG_DEL_DOC_NUM "
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "'"
                End If
                If Session("slsp2") & "" <> "" Then
                    sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                    sql2 = sql2 & ", '" & Session("slsp2") & "','" & Session("pct_2") & "'"
                End If
                If Session("TAX_CD") & "" <> "" And Session("tet_cd") & "" = "" Then
                    sql = sql & ", TAX_CD "
                    sql2 = sql2 & ", '" & Session("TAX_CD") & "'"
                End If
                If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                    If IsNumeric(tax) Then
                        sql = sql & ", TAX_CHG"
                        sql2 = sql2 & ", " & tax & ""
                    Else
                        If Not IsNumeric(Session("SU_TAX")) Then Session("SU_TAX") = 0
                        If Not IsNumeric(Session("DEL_TAX")) Then Session("DEL_TAX") = 0
                        sql = sql & ", TAX_CHG"
                        sql2 = sql2 & ", " & CDbl(Session("SU_TAX")) + CDbl(Session("DEL_TAX"))
                    End If
                Else
                    sql = sql & ", TAX_CHG"
                    sql2 = sql2 & ", " & Math.Round(CDbl(del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
                End If
                If Session("ord_srt") & "" <> "" Then
                    sql = sql & ", ORD_SRT_CD"
                    sql2 = sql2 & ", '" & Session("ord_srt") & "'"
                End If
                If Session("cust_cd") & "" <> "" Then
                    sql = sql & ", SHIP_TO_CUST_CD"
                    sql2 = sql2 & ", '" & Replace(Session("cust_cd"), "'", "''") & "'"
                End If
                If customer.corpName & "" <> "" Then
                    sql = sql & ", SHIP_TO_CORP"
                    sql2 = sql2 & ", '" & Replace(customer.corpName, "'", "''") & "'"
                End If
                If customer.fname & "" <> "" Then
                    sql = sql & ", SHIP_TO_F_NAME"
                    sql2 = sql2 & ", '" & Replace(customer.fname, "'", "''") & "'"
                End If
                If customer.lname & "" <> "" Then
                    sql = sql & ", SHIP_TO_L_NAME"
                    sql2 = sql2 & ", '" & Replace(customer.lname, "'", "''") & "'"
                End If
                If customer.addr1 & "" <> "" Then
                    sql = sql & ", SHIP_TO_ADDR1"
                    sql2 = sql2 & ", '" & Replace(customer.addr1, "'", "''") & "'"
                End If
                If customer.addr2 & "" <> "" Then
                    sql = sql & ", SHIP_TO_ADDR2"
                    sql2 = sql2 & ", '" & Replace(customer.addr2, "'", "''") & "'"
                End If
                If customer.city & "" <> "" Then
                    sql = sql & ", SHIP_TO_CITY"
                    sql2 = sql2 & ", '" & Replace(customer.city, "'", "''") & "'"
                End If
                If customer.state & "" <> "" Then
                    sql = sql & ", SHIP_TO_ST_CD"
                    sql2 = sql2 & ", '" & customer.state & "'"
                End If
                If customer.zip & "" <> "" Then
                    sql = sql & ", SHIP_TO_ZIP_CD"
                    sql2 = sql2 & ", '" & customer.zip & "'"
                End If
                If customer.hphone & "" <> "" Then
                    sql = sql & ", SHIP_TO_H_PHONE"
                    sql2 = sql2 & ", '" & customer.hphone & "'"
                End If
                If customer.bphone & "" <> "" Then
                    sql = sql & ", SHIP_TO_B_PHONE"
                    sql2 = sql2 & ", '" & customer.bphone & "'"
                End If

                'Code changes for MM-4069
                'sql = sql & ")"
                'sql2 = sql2 & ")"
                sql = sql & ", CONF_STAT_CD)"
                sql2 = sql2 & ", '" & theSalesBiz.GetFirstConfirmationCode() & "')"
                'till here

                sql = UCase(sql & sql2)
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves a record to the CRM.AUDIT_LOG table
                theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "SALE_CREATED", del_doc_num & " was created via Sales Order Entry")

                'now save the marketing info, if any, to  the SO marketing tables
                theSalesBiz.SaveSOMarketingInfo(dbAtomicCommand, Session("tran_dt"), storeCd, soSeqNum, Session("mkt_cat"), Session("mkt_sub_cd"))

                If doc_processing = 1 And newtotal <> 0 Then
                    lbl_Status.Text = lbl_Status.Text & "<b> / " & del_doc_num & "</b>"
                Else
                    lbl_Status.Text = "<br /><b>" & Resources.LibResources.Label121 & " " & del_doc_num & "</b>"
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO_CMNT     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                ' Add auto comemnt for tax code change only when the user has actually gone in to change it
                If Not (IsNothing(Session("MANUAL_TAX_CD"))) AndAlso
                    Session("MANUAL_TAX_CD").ToString.isNotEmpty AndAlso
                    Session("MANUAL_TAX_CD") <> Session("TAX_CD") Then
                    SaveAutoComment(dbAtomicCommand, del_doc_num, Session("tran_dt"), "TCD", Session("MANUAL_TAX_CD"), Session("TAX_CD"), Session("EMP_CD"))
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO_LN INFO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                Dim itmInfo As DataRow
                Dim cst As Double = 0
                Dim pkg_itm_cd As String = ""
                Dim itmTrnInsReq As InventoryUtils.ItmTrnInsertReq
                Dim invResReq As InventoryUtils.Inv_Reserve_Req
                Dim isInvErrOnDoc As Boolean = False
                Dim isInvErrOnThisLine As Boolean

                Dim dbAdapter As OracleDataAdapter
                Dim firstRowId As Integer = 0  ' used to create offset for warranty line pointers; will only work as long as order is by row_id asc
                Dim tmpItmDs As New DataSet
                Dim tmpItmTbl As New DataTable
                Dim tmpItm() As DataRow    ' internal SQL
                Dim where As String = ""
                Dim offSet As Integer = 0

                If doc_processing = 2 Then
                    soLn_num = 1
                    sql = "SELECT * FROM temp_itm where take_with='Y' and session_id='" & Session.SessionID.ToString.Trim & "' order by row_id"
                Else
                    sql = "SELECT * FROM temp_itm where session_id='" & Session.SessionID.ToString.Trim & "' order by row_id"
                End If

                'this select statement has to be in the single transaction, otherwise when having a split order, the main document ends up
                'with ALL the lines (included the ones that are placed in document 'A' and SO_LN ends up with more lines than originally 
                'were keyed.   ex: having an order with 2 lines (one TW), main doc has 2 lines, 'A' doc has 1 line, SO_LN has 3 lines
                dbAtomicCommand.CommandText = sql

                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbAtomicCommand)
                dbAdapter.Fill(tmpItmDs)
                tmpItmTbl = tmpItmDs.Tables(0)

                dbReader = DisposablesManager.BuildOracleDataReader(dbAtomicCommand)

                Do While dbReader.Read()
                    isInvErrOnThisLine = False
                    If firstRowId = 0 Then firstRowId = CInt(dbReader.Item("ROW_ID").ToString)
                    itmInfo = SkuUtils.GetItemInfo(dbReader("ITM_CD").ToString())

                    sql = "INSERT INTO SO_LN (DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, COMM_CD, SO_DOC_NUM, QTY, UNIT_PRC, PICKED, VOID_FLAG "
                    sql2 = " VALUES('" & del_doc_num & "'," & soLn_num & ",'" & Replace(dbReader.Item("ITM_CD").ToString, "'", "''") & "','" & Replace(dbReader.Item("COMM_CD").ToString, "'", "''") & "'"
                    sql2 = sql2 & ",'" & Left(del_doc_num, 11) & "'," & dbReader.Item("QTY").ToString & "," & dbReader.Item("RET_PRC").ToString & ","

                    'Adding logic to force inventory to recognize that nothing was there but we still want to finalize  MSC 8/6/2013
                    If "Y".Equals(itmInfo("inventory").ToString) Then
                        sql2 = sql2 & "'N'"  ' store and loc will be set when inventory updated
                    Else
                        sql2 = sql2 & "'X'"  ' non-inventory
                    End If
                    sql2 = sql2 & ",'N'" ' void flag

                    'when saving PKG Components, the PKG Itm-cd is needed
                    If (dbReader.Item("ITM_TP_CD").ToString = "PKG") Then pkg_itm_cd = dbReader.Item("ITM_CD").ToString

                    If (dbReader.Item("PACKAGE_PARENT").ToString).isNotEmpty Then
                        sql = sql & ", PKG_SOURCE"
                        sql2 = sql2 & ", '" & pkg_itm_cd & "'"
                    End If

                    Dim pct1 As Double = 0
                    If dbReader.Item("SLSP1").ToString & "" <> "" Then
                        sql = sql & ", SO_EMP_SLSP_CD1, PCT_OF_SALE1"
                        sql2 = sql2 & ", '" & Replace(dbReader.Item("SLSP1").ToString, "'", "''") & "','"
                        If IsNumeric(dbReader.Item("SLSP1_PCT").ToString) Then
                            pct1 = dbReader.Item("SLSP1_PCT").ToString
                        Else
                            pct1 = Session("pct_1")
                        End If
                        sql2 = sql2 & pct1.ToString & "'"
                    End If

                    If dbReader.Item("SLSP2").ToString & "" <> "" Then
                        sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                        sql2 = sql2 & ", '" & Replace(dbReader.Item("SLSP2").ToString, "'", "''") & "','"
                        sql2 = sql2 & (100 - pct1).ToString & "'"
                    End If

                    If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                        If Session("TAX_CD") & "" <> "" And Session("tet_cd") & "" = "" Then
                            sql = sql & ", TAX_CD "
                            sql2 = sql2 & ", '" & Session("TAX_CD") & "'"
                        End If
                        sql = sql & ", TAX_RESP"
                        sql2 = sql2 & ", 'C' "
                        If IsNumeric(dbReader.Item("TAXABLE_AMT").ToString) Then
                            sql = sql & ", TAX_BASIS"
                            sql2 = sql2 & ", " & dbReader.Item("TAXABLE_AMT").ToString & ""
                            If IsNumeric(dbReader.Item("TAX_PCT").ToString) Then
                                sql = sql & ", CUST_TAX_CHG"
                                sql2 = sql2 & ", " & Replace(FormatNumber((CDbl(dbReader.Item("TAXABLE_AMT").ToString) * CDbl(dbReader.Item("QTY").ToString)) * (CDbl(dbReader.Item("TAX_PCT").ToString) / 100), 2), ",", "") & " "
                            End If
                        End If
                    End If

                    sql = sql & ", LV_IN_CARTON"
                    sql2 = sql2 & ", '" & IIf((dbReader.Item("LEAVE_CARTON").ToString = "Y"), dbReader.Item("LEAVE_CARTON").ToString, "N") & "'"

                    sql = sql & ", COMM_ON_DEL_CHG"
                    sql2 = sql2 & IIf(SysPms.commissionOnDelvCharge, ", 'Y'", ", 'N'")

                    sql = sql & ", COMM_ON_SETUP_CHG"
                    sql2 = sql2 & IIf(SysPms.commissionOnSetupCharge, ", 'Y'", ", 'N'")

                    If IsNumeric(dbReader.Item("SPIFF").ToString) AndAlso CDbl(dbReader.Item("SPIFF").ToString) > 0 Then
                        sql = sql & ", SPIFF"
                        sql2 = sql2 & ", " & CDbl(dbReader.Item("SPIFF").ToString)
                    End If

                    If dbReader.Item("TREATED").ToString = "Y" Then
                        If IsNumeric(dbReader.Item("TREATED_BY").ToString) AndAlso CDbl(dbReader.Item("TREATED_BY").ToString) > 0 Then
                            sql = sql & ", TREATED_BY_LN#"
                            sql2 = sql2 & ", " & Math.Abs(CDbl(dbReader.Item("TREATED_BY").ToString) - CDbl(dbReader.Item("ROW_ID").ToString)) + soLn_num
                            If Get_Temp_ITM_CD(dbReader.Item("TREATED_BY").ToString) & "" <> "" Then
                                sql = sql & ", TREATED_BY_ITM_CD"
                                sql2 = sql2 & ", '" & Get_Temp_ITM_CD(dbReader.Item("TREATED_BY").ToString) & "' "
                            End If
                        Else
                            Dim firstFab = Get_First_Fab()
                            If IsNumeric(firstFab) Then
                                sql = sql & ", TREATED_BY_LN#"
                                sql2 = sql2 & ", " & Math.Abs(CDbl(firstFab.ToString) - CDbl(dbReader.Item("ROW_ID").ToString)) + soLn_num
                                If Get_Temp_ITM_CD(firstFab) & "" <> "" Then
                                    sql = sql & ", TREATED_BY_ITM_CD"
                                    sql2 = sql2 & ", '" & Get_Temp_ITM_CD(firstFab) & "' "
                                End If
                            End If
                        End If
                    End If

                    If IsNumeric(dbReader.Item("DEL_PTS").ToString) AndAlso CDbl(dbReader.Item("DEL_PTS").ToString) > 0 Then
                        sql = sql & ", DELIVERY_POINTS"
                        sql2 = sql2 & ", " & CDbl(dbReader.Item("DEL_PTS").ToString)
                    End If

                    If dbReader.Item("ID_NUM").ToString & "" <> "" Then
                        sql = sql & ", ID_NUM"
                        sql2 = sql2 & ", '" & dbReader.Item("ID_NUM").ToString & "'"
                    End If
                    'removing constraints or checks for setting take with flag-all cash and carry orders should be set to "Y" - MSC 8/6
                    sql = sql & ", TAKEN_WITH"
                    sql2 = sql2 & ", 'Y'"
                    sql = sql & ", OOC_QTY"
                    sql2 = sql2 & ", " & dbReader.Item("QTY").ToString

                    ' pu_del_dt is only maintained on SO_LN in E1 for RES prior to sale conversion where will split to multi docs; 
                    '  multi pu_del_dt on SO is not allowed and this value is not filled in in E1 - per DSA
                    sql = sql & ")"
                    sql2 = sql2 & ")"
                    sql = UCase(sql & sql2)

                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.ExecuteNonQuery()

                    If dbReader.Item("warr_row_link").ToString.isNotEmpty AndAlso
                        IsNumeric(dbReader.Item("warr_row_link").ToString) AndAlso
                        dbReader.Item("warr_row_link").ToString > 0 AndAlso
                        dbReader.Item("warr_itm_link").ToString.isNotEmpty Then

                        If dbReader.Item("warrantable").ToString = "Y" Then

                            Dim soLnWarr As New SalesUtils.SoLnWarrDtc
                            soLnWarr.docNum = del_doc_num
                            soLnWarr.docLnNum = soLn_num
                            soLnWarr.warrDocNum = del_doc_num
                            where = "row_id >= " + firstRowId.ToString + " AND row_id <= " + dbReader.Item("warr_row_link").ToString
                            tmpItm = tmpItmTbl.Select(where)
                            offSet = tmpItm.GetUpperBound(0)
                            soLnWarr.warrDocLnNum = offSet + 1
                            SalesUtils.InsertSoLnWarr(soLnWarr, dbAtomicCommand)
                            dbAtomicCommand.Parameters.Clear()
                        End If
                    End If

                    'Build the SO_LN correlation
                    sql = "UPDATE TEMP_ITM SET SO_LN_NO=" & soLn_num & " WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND ROW_ID=" & dbReader.Item("ROW_ID")
                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.ExecuteNonQuery()

                    'Update inventory if allocated on the sales order line
                    If "Y".Equals(itmInfo("inventory").ToString) Then

                        invResReq = New InventoryUtils.Inv_Reserve_Req
                        invResReq.qty = dbReader.Item("QTY").ToString
                        invResReq.del_Doc_Num = del_doc_num
                        invResReq.ln_Num = soLn_num
                        invResReq.itm_Cd = dbReader.Item("ITM_CD").ToString
                        invResReq.pu_Del = Session("PD")
                        invResReq.ser_Num = dbReader.Item("SERIAL_NUM").ToString
                        invResReq.out_Id = dbReader.Item("OUT_ID").ToString
                        invResReq.fifo_Cst = 0.0
                        invResReq.out_Cd = ""
                        If dbReader.Item("STORE_CD").ToString & "" = "" OrElse dbReader.Item("LOC_CD").ToString & "" = "" Then
                            invResReq.store_Cd = Session("STORE_CD")
                            invResReq.loc_Cd = Session("loc_cd")
                        Else
                            invResReq.store_Cd = dbReader.Item("STORE_CD").ToString
                            invResReq.loc_Cd = dbReader.Item("LOC_CD").ToString
                        End If
                        InventoryUtils.Debit_Inventory(invResReq, dbAtomicCommand)
                        dbAtomicCommand.CommandType = CommandType.Text
                        dbAtomicCommand.Parameters.Clear()

                        If InStr(Session("err"), "Insufficient") > 0 Then ' TODO - if ever change debit_inventory messages then have to change this check for inventory update error

                            'Force values in for items that inventory was not found - MSC 8/6
                            sql = "UPDATE SO_LN SET STORE_CD='" & invResReq.store_Cd & "', LOC_CD='" & invResReq.loc_Cd & "', FIFL_DT=NULL, FIFO_DT=NULL, FIFO_CST=NULL, picked = 'N' WHERE DEL_DOC_NUM='" & del_doc_num.ToString.Trim & "' AND DEL_DOC_LN#=" & soLn_num
                            dbAtomicCommand.CommandText = sql
                            dbAtomicCommand.ExecuteNonQuery()

                            isInvErrOnThisLine = True
                            isInvErrOnDoc = True
                            Session("err") = "" ' do not want to see the message since finalizing anyway
                        End If

                        If (doc_processing = 2 And (Not isInvErrOnThisLine)) OrElse Session("cash_carry") = "TRUE" Then

                            itmTrnInsReq = New InventoryUtils.ItmTrnInsertReq
                            itmTrnInsReq.delDocNum = del_doc_num
                            itmTrnInsReq.lnNum = soLn_num
                            itmTrnInsReq.qty = CDbl(dbReader.Item("QTY").ToString)
                            itmTrnInsReq.itmCd = dbReader.Item("ITM_CD").ToString
                            itmTrnInsReq.oldStoreCd = dbReader.Item("STORE_CD").ToString
                            itmTrnInsReq.oldLocCd = dbReader.Item("LOC_CD").ToString
                            itmTrnInsReq.serNum = dbReader.Item("SERIAL_NUM").ToString
                            itmTrnInsReq.cst = 0
                            itmTrnInsReq.fifoDt = ""
                            itmTrnInsReq.fiflDt = ""
                            If (dbReader.Item("OUT_ID").ToString + "").isNotEmpty Then
                                itmTrnInsReq.idCd = dbReader.Item("OUT_ID").ToString
                                itmTrnInsReq.oldOutCd = invResReq.out_Cd
                            Else
                                itmTrnInsReq.idCd = ""
                                itmTrnInsReq.oldOutCd = ""
                            End If
                            PopulateFIFOdetails(itmTrnInsReq, del_doc_num, soLn_num, itmInfo("REPL_CST"))
                            SaveFinalDocItmTrn(dbAtomicCommand, itmTrnInsReq, Session("ord_tp_cd"), Session("tran_dt"), Session("EMP_CD"))

                            If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso (Not isInvErrOnThisLine) AndAlso
                               (ConfigurationManager.AppSettings("xref") = "Y" OrElse ConfigurationManager.AppSettings("enable_serialization") = "Y") AndAlso
                               (itmInfo("BULK_TP_ITM").ToString = "N") Then

                                HBCG_Utils.Finalize_Inv_Xref(del_doc_num, soLn_num, dbReader.Item("SERIAL_NUM").ToString, dbAtomicCommand)
                            End If
                        End If
                    End If

                    'Add code for PEN recod in INV_XREF when a CRM is created.  NO Cash&Carry CRM at this time.  MSC 6/3/2012
                    soLn_num = soLn_num + 1   ' get a new del_doc_ln# for next line
                Loop
                dbReader.Close()
                dbAtomicCommand.Parameters.Clear()
                dbAtomicCommand.CommandType = CommandType.Text
                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    END OF SO_LN CREATE     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                If (doc_processing = 2 OrElse Session("cash_carry") = "TRUE") AndAlso (Not isInvErrOnDoc) Then ' no GL_DOC_TRN if inventory update not complete even iff CC
                    ' I don't know where it comes from then because CCFR doesn't create
                    sql = "INSERT INTO GL_DOC_TRN (DOC_NUM, CO_CD, POST_DT, ORIGIN_CD, PROCESSED) "
                    sql = sql & "VALUES (:DOC_NUM, :CO_CD, SYSDATE, :ORIGIN_CD, :PROCESSED)"

                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.Parameters.Add(":DOC_NUM", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":PROCESSED", OracleType.VarChar)

                    dbAtomicCommand.Parameters(":DOC_NUM").Value = del_doc_num
                    dbAtomicCommand.Parameters(":CO_CD").Value = Session("CO_CD")
                    dbAtomicCommand.Parameters(":ORIGIN_CD").Value = "PSOE"
                    dbAtomicCommand.Parameters(":PROCESSED").Value = "N"
                    dbAtomicCommand.ExecuteNonQuery()
                    dbAtomicCommand.Parameters.Clear()
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE AR_TRN INFO FOR SO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing = 2 Then split_amt = CDbl(FormatNumber(grand_total, 2))

                'Create Sales Transaction
                sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, PMT_STORE, WR_DT, ORIGIN_CD "
                sql2 = " VALUES('" & Session("CO_CD") & "','" & Session("cust_cd")
                sql2 = sql2 & "','" & Session("store_cd") & "','" & Session("ord_tp_cd") & "'," & CDbl(FormatNumber(grand_total, 2)) & ", TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),"
                If (doc_processing = 2 AndAlso (Not isInvErrOnDoc)) OrElse Session("cash_carry") = "TRUE" Then ' CC always 'T' cause final
                    sql2 = sql2 & "'T',"
                Else
                    sql2 = sql2 & "'A',"
                End If
                sql2 = sql2 & "'O',"
                sql2 = sql2 & "'" & Session("store_cd") & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
                If Session("slsp1") & "" <> "" Then
                    sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                    sql2 = sql2 & ", '" & Session("slsp1") & "','" & Session("slsp1") & "'"
                End If
                If Not String.IsNullOrEmpty(finAcctNum) Then
                    sql = sql & ", BNK_CRD_NUM, MOP_CD"
                    sql2 = sql2 & ", '" & finAcctNum & "', 'FI'"
                End If
                If Session("adj_ivc_cd") & "" <> "" AndAlso AppConstants.Order.TYPE_SAL <> Session("ord_tp_cd") Then
                    sql = sql & ", IVC_CD, ADJ_IVC_CD"
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "','" & del_doc_num & "'"
                ElseIf Session("adj_ivc_cd") & "" <> "" AndAlso AppConstants.Order.TYPE_MCR = Session("ord_tp_cd") Then ' TODO MDB is adjusted, not ivc
                    sql = sql & ", IVC_CD, ADJ_IVC_CD"
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "','" & del_doc_num & "'"
                ElseIf Session("ord_tp_cd") = "CRM" Then
                    sql = sql & ", ADJ_IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                ElseIf Session("ord_tp_cd") = "MCR" Then
                    sql = sql & ", ADJ_IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                Else
                    sql = sql & ", IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                End If
                sql = sql & ")"
                sql2 = sql2 & ")"
                sql = UCase(sql & sql2)

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE AR_TRN DEPOSITS/REFUNDS     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                Dim PB_Response_Cd As String = ""
                Dim Protobase_Response_Msg As String = ""
                Dim SDC_Response As String = ""
                Dim Host_Response_Msg As String = ""
                Dim Host_Response_Cd As String = ""
                Dim Host_Merchant_ID As String = ""
                Dim merchant_id As String = ""
                Dim plan_desc As String = ""
                Dim rate_info As String = ""
                Dim intro_rate As String = ""
                Dim bnk_crd_num As String = ""
                Dim Auth_No As String = ""
                Dim cc_resp As String = ""
                Dim Card_Type As String = ""
                Dim exp_dt As String = ""
                Dim ref_num As String = ""
                Dim prepaid_balance As String = ""
                Dim Trans_Id As String = ""
                Dim order_tp As String = IIf((AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") OrElse
                                              AppConstants.Order.TYPE_MDB = Session("ord_tp_cd")), "DEP", "R")

                ' if split doc, Inserts Payments first, then split to TW if necessary 
                If doc_processing <> 2 Then

                    sql = "SELECT * FROM payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session.SessionID.ToString.Trim & "' order by ROW_ID"
                    dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    Do While (dbReader.Read())
                        plan_desc = ""        'this represents the  promo info verbiage in field 3319
                        rate_info = ""        'this represents the  "after promo" or "acct apr" info in field 3322
                        intro_rate = ""       'this represents the  "during promo" or "promo apr" info in field 3325
                        merchant_id = ""      'merchant key for the store that the sale was written under
                        bnk_crd_num = ""
                        Auth_No = ""
                        Trans_Id = ""
                        Host_Merchant_ID = ""
                        Host_Response_Cd = ""
                        Host_Response_Msg = ""
                        Protobase_Response_Msg = ""
                        PB_Response_Cd = ""
                        ref_num = ""
                        Card_Type = ""
                        cc_resp = ""
                        prepaid_balance = ""
                        partial_amt = 0
                        Insert_Payment = True

                        'Check to see if there is a merchant key for the store that the sale was written under
                        If Not String.IsNullOrEmpty(dbReader.Item("MOP_TP").ToString) Then
                            merchant_id = Lookup_Merchant(dbReader.Item("MOP_TP").ToString, Session("store_cd"), dbReader.Item("MOP_CD").ToString)
                        End If

                        If Not String.IsNullOrEmpty(merchant_id) Then
                            '
                            'Process Check Guarantees through National Check Trust
                            '
                            If dbReader.Item("MOP_TP").ToString = "CK" AndAlso ConfigurationManager.AppSettings("chk_guar") = "Y" Then
                                cc_resp = NCT_Submit_Query(dbReader.Item("ROW_ID").ToString, merchant_id)
                                If InStr(LCase(cc_resp), "approval") > 0 Then
                                    Auth_No = Right(cc_resp, cc_resp.IndexOf("Message = ") - 10).Trim
                                    Auth_No = Replace(Auth_No, "=", "")
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " succeeded for check " & dbReader.Item("CHK_NUM").ToString & ". Authorization #:" & Auth_No)
                                    Insert_Payment = True
                                ElseIf InStr(LCase(cc_resp), "denied") > 0 Then
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Check " & dbReader.Item("CHK_NUM").ToString & " was declined", "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " was declined for check " & dbReader.Item("CHK_NUM").ToString)
                                    Insert_Payment = False
                                Else
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Check " & dbReader.Item("CHK_NUM").ToString & " was not approved. " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5), "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " was not approved for check " & dbReader.Item("CHK_NUM").ToString & ". " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5))
                                    Insert_Payment = False
                                End If
                            End If
                            '
                            'Process Gift Cards through World Gift Card
                            '
                            If dbReader.Item("MOP_TP").ToString = "GC" AndAlso ConfigurationManager.AppSettings("gift_card") = "Y" Then
                                Dim gcAmt As String = dbReader.Item("AMT").ToString
                                Dim xml_document As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id,
                                                                                                "sale",
                                                                                                dbReader.Item("BC").ToString,
                                                                                                IIf(order_tp = "DEP", gcAmt, CDbl(gcAmt) * -1))
                                cc_resp = xml_document.InnerText.ToString
                                If IsNumeric(cc_resp) Then
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " gift card transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & dbReader.Item("BC").ToString & ".")
                                    Insert_Payment = True
                                Else
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", cc_resp, "PMT", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " gift card transaction for " & dbReader.Item("AMT").ToString & " failed for card " & dbReader.Item("BC").ToString & ".  " & cc_resp)
                                    Insert_Payment = False
                                End If
                            End If
                            '
                            'Process Credit Cards through Southern Datacomm
                            '
                            If dbReader.Item("BC").ToString & "" <> "" AndAlso dbReader.Item("MOP_TP").ToString = "BC" AndAlso
                               ConfigurationManager.AppSettings("cc") = "Y" Then

                                Insert_Payment = False
                                SDC_Response = SD_Submit_Query_CC(dbReader.Item("ROW_ID").ToString,
                                                                  merchant_id,
                                                                  IIf(order_tp = "DEP", AUTH_SALE, AUTH_REFUND),
                                                                  del_doc_num)
                                If SDC_Response = AUTH_INQUIRY Then
                                    SDC_Response = SD_Submit_Query_CC(dbReader.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, del_doc_num)
                                End If

                                If (InStr(SDC_Response, " this stream") > 0 OrElse
                                    InStr(SDC_Response, "Protobase is not responding") > 0 OrElse
                                    SDC_Response & "" = "") Then

                                    Dim theBankCardNumber As String = Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE")
                                    If InStr(theBankCardNumber, "^") > 0 Then
                                        Dim sCC As Array = Split(theBankCardNumber, "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    Else
                                        bnk_crd_num = theBankCardNumber
                                    End If
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " failed.  Protobase server did not respond for card " & Right(bnk_crd_num, 4))

                                Else
                                    Dim s As String
                                    Dim MsgArray As Array
                                    Dim sep(3) As Char
                                    sep(0) = Chr(10)
                                    sep(1) = Chr(12)
                                    Dim SDC_Fields As String() = SDC_Response.Split(sep)

                                    For Each s In SDC_Fields
                                        If InStr(s, ",") > 0 Then
                                            MsgArray = Split(s, ",")
                                            Select Case MsgArray(0)
                                                Case "0002"
                                                    partial_amt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0003"
                                                    Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0004"
                                                    exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    exp_dt = MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                                Case "0006"
                                                    Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0007"
                                                    ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0632"
                                                    prepaid_balance = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1000"
                                                    Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1003"
                                                    PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1004"
                                                    Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1005"
                                                    Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1008"
                                                    bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                                Case "1009"
                                                    Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1010"
                                                    Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            End Select
                                        End If
                                    Next s

                                    If bnk_crd_num & "" <> "" AndAlso InStr(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                        Dim sCC As Array = Split(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    End If

                                    Select Case PB_Response_Cd
                                        Case "0", "0000"
                                            cc_resp = "APPROVED"
                                        Case "60", "0060"
                                            cc_resp = "DECLINED"
                                        Case Else
                                            Host_Response_Msg = PB_Response_Cd
                                            Host_Response_Cd = Protobase_Response_Msg
                                            cc_resp = "ERROR"
                                    End Select

                                    If cc_resp = "APPROVED" Then
                                        Insert_Payment = True
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & Right(bnk_crd_num, 4))

                                        If ConfigurationManager.AppSettings("partial_pay") = "Y" AndAlso CDbl(dbReader.Item("AMT").ToString) <> CDbl(partial_amt) Then

                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_PARTIAL", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " approved for " & partial_amt & " for card " & Right(bnk_crd_num, 4))
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "PARTIAL AUTHORIZATION", "PARTIAL AUTHORIZATION", "EXPECTED:" & dbReader.Item("AMT").ToString & ", ACTUAL:" & partial_amt, "DEP", del_doc_num)
                                            err_msg = "TRUE"

                                            sql = "UPDATE PAYMENT_HOLDING SET AMT=:AMT WHERE ROW_ID=:ROW_ID"
                                            dbAtomicCommand.CommandText = sql
                                            dbAtomicCommand.Parameters.Add(":AMT", OracleType.Number)
                                            dbAtomicCommand.Parameters.Add(":ROW_ID", OracleType.Number)
                                            dbAtomicCommand.Parameters(":AMT").Value = partial_amt
                                            dbAtomicCommand.Parameters(":ROW_ID").Value = dbReader.Item("ROW_ID").ToString
                                            dbAtomicCommand.ExecuteNonQuery()
                                            dbAtomicCommand.Parameters.Clear()

                                        End If
                                    Else
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " failed for card " & Right(bnk_crd_num, 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", del_doc_num)
                                        err_msg = "TRUE"
                                    End If
                                End If
                            End If
                            '
                            'Process Finance
                            '
                            If dbReader.Item("MOP_TP").ToString = "FI" AndAlso ConfigurationManager.AppSettings("finance") = "Y" Then

                                Insert_Payment = False

                                Dim provider As String = Lookup_Transport(dbReader.Item("MOP_TP").ToString, Session("store_cd"), dbReader.Item("MOP_CD").ToString)
                                If provider = "GE_182" OrElse provider = "WF_148" Then
                                    ' 
                                    'Process Finance Authorization through  Protobase Instance
                                    '
                                    If order_tp = "DEP" Then
                                        'if this finance mop cd is found as FINANCE_DEPOSIT_TP='Y' in the settlment_mop table, treat it as a auth sale -02
                                        SDC_Response = SD_Submit_Query_PL(dbReader.Item("ROW_ID").ToString,
                                                                    merchant_id,
                                                                    IIf(InStr(DP_String_Creation, dbReader.Item("MOP_CD").ToString) > 0, AUTH_SALE, AUTH_ONLY),
                                                                    del_doc_num)
                                    Else
                                        'SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_REFUND, del_doc_num)
                                        'Commenting out; we do not want to refund the customer until the CRM is finalized
                                    End If

                                    If SDC_Response = AUTH_INQUIRY Then
                                        SDC_Response = SD_Submit_Query_PL(dbReader.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, del_doc_num)
                                    End If

                                    If InStr(SDC_Response, " this stream") > 0 OrElse
                                       InStr(SDC_Response, "Protobase is not responding") > 0 OrElse SDC_Response & "" = "" Then

                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", del_doc_num)
                                        err_msg = "TRUE"
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " failed.  Protobase server did not respond for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4))

                                    ElseIf order_tp = "R" Then
                                        'Do nothing; we want to bypass things for CRM documents
                                    Else
                                        Dim s As String
                                        Dim MsgArray As Array
                                        Dim sep(3) As Char
                                        sep(0) = Chr(10)
                                        sep(1) = Chr(12)
                                        Dim SDC_Fields As String() = SDC_Response.Split(sep)

                                        For Each s In SDC_Fields
                                            If InStr(s, ",") > 0 Then
                                                MsgArray = Split(s, ",")
                                                Select Case MsgArray(0)
                                                    Case "0003"
                                                        Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "0004"
                                                        exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                        exp_dt = MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                                    Case "0006"
                                                        Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "0007"
                                                        ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1003"
                                                        PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1004"
                                                        Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1008"
                                                        bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                        bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                                    Case "1009"
                                                        Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1010"
                                                        Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "3319"
                                                        plan_desc = ""
                                                        For seq_num = LBound(MsgArray) To UBound(MsgArray)
                                                            If seq_num > 0 Then plan_desc = plan_desc & MsgArray(seq_num) & ","
                                                        Next
                                                        plan_desc = Left(plan_desc, Len(plan_desc) - 1)
                                                    Case "3322"
                                                        rate_info = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "3325"
                                                        intro_rate = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                End Select
                                            End If
                                        Next s

                                        Select Case PB_Response_Cd
                                            Case "0", "0000"
                                                cc_resp = "APPROVED"
                                            Case "60", "0060"
                                                cc_resp = "DECLINED"
                                            Case Else
                                                Host_Response_Msg = PB_Response_Cd
                                                Host_Response_Cd = Protobase_Response_Msg
                                                cc_resp = "ERROR"
                                        End Select

                                        If cc_resp = "APPROVED" Then
                                            Insert_Payment = True
                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4))
                                        Else
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", del_doc_num)
                                            err_msg = "TRUE"
                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " was declined for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                                        End If
                                    End If

                                ElseIf provider = "ADS" Then
                                    ' 
                                    'Process Finance Authorization through Alliance Data Systems
                                    '
                                    Dim requestDS As New RequestAllianceDataSet()
                                    requestDS.Request.Rows.Add(New Object() {"LEVINS", DateTime.Now, merchant_id, Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), dbReader.Item("AMT").ToString, "N"})
                                    requestDS.AcceptChanges()
                                    Dim fiService As New FITransactionService()
                                    Dim responseDS As ResponseAllianceDataSet = fiService.InvokeFITransaction(requestDS)
                                    Dim reasonDesc As String = ""
                                    Dim returnDesc As String = ""
                                    Dim avsDesc As String = ""

                                    If (Not responseDS Is Nothing) And (responseDS.Response.Rows.Count) > 0 Then
                                        If Not IsNothing(responseDS.Response(0).reasonCode) Then
                                            reasonDesc = GetDesc(responseDS.Response(0).reasonCode.ToString, cResponseCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).returnCode) Then
                                            returnDesc = GetDesc(responseDS.Response(0).returnCode.ToString, cReturnCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).avsResponse) Then
                                            avsDesc = GetDesc(responseDS.Response(0).avsResponse.ToString, cAVSCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).authCode) Then
                                            Auth_No = responseDS.Response(0).authCode.ToString
                                        End If
                                        If LCase(returnDesc) <> "approved" Then
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, returnDesc, reasonDesc, avsDesc, "FI", del_doc_num)
                                            err_msg = "TRUE"
                                            lbl_fin_co.Text = ""
                                        Else
                                            Insert_Payment = True
                                        End If
                                    Else
                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "No response from ADS", "", "", "FI", del_doc_num)
                                        lbl_fin_co.Text = ""
                                        err_msg = "TRUE"
                                    End If
                                End If
                                If Not String.IsNullOrEmpty(dbReader.Item("FI_ACCT_NUM").ToString) Then bnk_crd_num = "FI"

                            End If   '***end a finance mop type 

                        End If   ' TODO- add an else here to display a message if terminal/merchant id is null in payment_xref table

                        If dbReader.Item("MOP_TP").ToString = "FI" AndAlso Insert_Payment = True AndAlso order_tp = "DEP" Then

                            Insert_Payment = False
                            finCo = dbReader.Item("FIN_CO").ToString
                            Dim rate_info_array As Array = Nothing
                            Dim intro_rate_array As Array = Nothing
                            Dim during_promo_apr As String = String.Empty
                            Dim during_promo_apr_flag As String = String.Empty
                            Dim after_promo_apr As String = String.Empty
                            Dim after_promo_apr_flag As String = String.Empty
                            Dim promo_duration As String = String.Empty
                            Dim aspPromoCd As String = String.Empty

                            If plan_desc.isEmpty Then
                                theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "FINANCE_PROMO_FAILURE", "Sales Order " & del_doc_num & " did not retrieve promotional terms")

                            Else
                                'this is the promo/during promo info
                                If InStr(intro_rate, ";") > 0 Then
                                    intro_rate_array = Split(intro_rate, ";")
                                    If (Not intro_rate Is Nothing) Then
                                        during_promo_apr_flag = intro_rate_array(0)
                                        during_promo_apr = intro_rate_array(1)
                                        promo_duration = intro_rate_array(4)
                                    End If
                                End If

                                'this is the account/after promo info 
                                If InStr(rate_info, ";") > 0 Then
                                    rate_info_array = Split(rate_info, ";")
                                    If (Not rate_info_array Is Nothing) Then
                                        after_promo_apr_flag = rate_info_array(4)
                                        after_promo_apr = rate_info_array(6)
                                    End If
                                End If

                                sql = "INSERT INTO SETTLEMENT_TERMS (DEL_DOC_NUM, ORD_TP_CD, RATE_DEL_RATE_TYPE, RATE_DEL_APR, INTRO_RATE, INTRO_APR, INTRO_DURATION, PLAN_DESC) "
                                sql = sql & "VALUES (:DEL_DOC_NUM, :ORD_TP_CD, :RATE_DEL_RATE_TYPE, :RATE_DEL_APR, :INTRO_RATE, :INTRO_APR, :INTRO_DURATION, :PLAN_DESC) "
                                dbAtomicCommand.CommandText = UCase(sql)

                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RATE_DEL_RATE_TYPE", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RATE_DEL_APR", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_RATE", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_APR", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_DURATION", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":PLAN_DESC", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = del_doc_num
                                dbAtomicCommand.Parameters(":ORD_TP_CD").Value = dbReader.Item("MOP_CD").ToString
                                dbAtomicCommand.Parameters(":RATE_DEL_RATE_TYPE").Value = after_promo_apr_flag  'rate_info_array(4).ToString
                                dbAtomicCommand.Parameters(":RATE_DEL_APR").Value = after_promo_apr   'rate_info_array(6).ToString
                                dbAtomicCommand.Parameters(":INTRO_RATE").Value = during_promo_apr_flag      'intro_rate_array(0).ToString
                                dbAtomicCommand.Parameters(":INTRO_APR").Value = during_promo_apr         'intro_rate_array(1).ToString
                                dbAtomicCommand.Parameters(":INTRO_DURATION").Value = promo_duration 'intro_rate_array(4).ToString
                                dbAtomicCommand.Parameters(":PLAN_DESC").Value = plan_desc.trimString(0, 50)

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()
                            End If

                            If Is_Finance_DP(dbReader.Item("MOP_CD").ToString) = False Then

                                '****************** Insert the credit entry details into SO_ASP table, if it does not exist *****************
                                If Not theSalesBiz.ExistsInSO_ASP(del_doc_num) Then

                                    ''get  first the ASP_PROMO info like promo des, etc.
                                    Dim promoDes As String = String.Empty
                                    Dim promoDes3 As String = String.Empty
                                    Dim ds As DataSet = GetAspPromoDetails(dbReader.Item("FIN_CO"), finPromoCd)
                                    For Each dr In ds.Tables(0).Rows
                                        If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                                        If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                                        If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
                                    Next

                                    If IsNumeric(during_promo_apr) Then during_promo_apr = during_promo_apr / 10000 & "% "
                                    If IsNumeric(after_promo_apr) Then after_promo_apr = after_promo_apr / 10000 & "% "

                                    sql = "INSERT INTO SO_ASP (" &
                                                "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                                                "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                                "ACCT_APR, ACCT_APR_FLAG)" &
                                            " VALUES('" & del_doc_num & "', 'Y', '" & finPromoCd & "','" & aspPromoCd & "','" & promoDes & "','" & plan_desc & "','" &
                                                                   promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                                                   after_promo_apr & "','" & after_promo_apr_flag & "')"
                                    dbAtomicCommand.CommandText = sql
                                    dbAtomicCommand.ExecuteNonQuery()
                                End If

                                If (String.IsNullOrEmpty(Auth_No) AndAlso Not String.IsNullOrEmpty(approvalCd)) Then Auth_No = approvalCd

                                sql = "UPDATE SO SET APPROVAL_CD='" & Auth_No & "' WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                                sql = UCase(sql)
                                dbAtomicCommand.CommandText = sql
                                dbAtomicCommand.ExecuteNonQuery()

                            Else   'Is_Finance_DP type = true
                                Insert_Payment = True

                                wdr = True
                                wdr_promo = dbReader.Item("FIN_PROMO_CD").ToString
                                theSalesBiz.SaveSOFinanceComments(dbAtomicCommand, soKeys, del_doc_num, wdr_promo, Session("STORE_CD"), Session("tran_dt"))

                            End If      'end if Is_Finance_DP type

                            ' ***Update the AR_TRN with the ref_num used for the FIN auth request 
                            sql = "UPDATE AR_TRN  SET REF_NUM = '" & ref_num & "'" & ", HOST_REF_NUM = '" & ref_num & "'" &
                                        " WHERE ivc_cd = '" & del_doc_num & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            '*** Save the asp response info when an approval is recieved for a finance auth
                            SaveAspResponseCodeInfo(dbAtomicCommand, finCo, del_doc_num)

                            'now delete the payment record
                            sql = "DELETE FROM PAYMENT WHERE ROW_ID=" & dbReader.Item("ROW_ID").ToString
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        ElseIf dbReader.Item("MOP_TP").ToString = "FI" And Insert_Payment = False And Is_Finance_DP(dbReader.Item("MOP_CD").ToString) = False And order_tp = "DEP" Then 'MyDataReader2.Item("MOP_CD").ToString <> "WDR" Then
                            sql = "UPDATE SO SET ORIG_FI_AMT=NULL, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & Session("ord_tp_cd") & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        ElseIf order_tp = "R" Then
                            sql = "INSERT INTO SO_ASP (DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD)" &
                                              " VALUES('" & del_doc_num & "', 'Y', '" & lbl_fin_co.Text & "','" & lbl_fin_co.Text & "')"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        End If       '****end if it's a finance and insert_payment= true

                        If Insert_Payment = True Then
                            'means this could be any type of pmt - CHK, CC, FI DP, GC etc. 
                            If IsNumeric(prepaid_balance) Then Session(del_doc_num & "_CMNT") = Session(del_doc_num & "_CMNT") & "Gift Card -" & Right(bnk_crd_num.Trim, 4) & " new balance: " & FormatCurrency(CDbl(prepaid_balance), 2) & vbCrLf
                            sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, ORIGIN_CD "
                            sql2 = " VALUES('" & Session("CO_CD") & "','" & Session("cust_cd") & "','" & dbReader.Item("MOP_CD").ToString & "'"
                            sql2 = sql2 & ",'" & Session("store_cd") & "','" & order_tp & "',"

                            If ConfigurationManager.AppSettings("partial_pay") = "Y" AndAlso partial_amt > 0 AndAlso CDbl(dbReader.Item("AMT").ToString) <> CDbl(partial_amt) Then
                                sql2 = sql2 & partial_amt
                            Else
                                sql2 = sql2 & dbReader.Item("AMT").ToString
                            End If
                            sql2 = sql2 & ", TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),'T','O',"
                            sql2 = sql2 & "'" & del_doc_num & "','" & Session("store_cd") & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
                            If Session("csh_dwr_cd") & "" <> "" Then
                                sql = sql & ", CSH_DWR_CD"
                                sql2 = sql2 & ", '" & Session("csh_dwr_cd") & "'"
                            End If
                            If Session("slsp1") & "" <> "" Then
                                sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                                sql2 = sql2 & ", '" & Session("slsp1") & "','" & Session("slsp1") & "'"
                            End If
                            If bnk_crd_num = "FI" Then
                                sql = sql & ", BNK_CRD_NUM"
                                sql2 = sql2 & ", '" & Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE") & "'"
                            ElseIf dbReader.Item("MOP_TP").ToString = "GC" Then
                                sql = sql & ", ACCT_NUM"
                                sql2 = sql2 & ", '" & dbReader.Item("BC").ToString & "'"
                            ElseIf dbReader.Item("BC").ToString & "" <> "" Then
                                If IsNumeric(bnk_crd_num) Then
                                    sql = sql & ", BNK_CRD_NUM"
                                    sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                                Else
                                    If InStr(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                        Dim sCC As Array = Split(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    Else
                                        bnk_crd_num = Right(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), 4)
                                    End If
                                    sql = sql & ", BNK_CRD_NUM"
                                    sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                                End If
                            End If
                            If Card_Type & "" <> "" Then
                                sql = sql & ", REF_NUM"
                                sql2 = sql2 & ", '" & Card_Type & "' "
                            End If
                            If dbReader.Item("CHK_NUM").ToString() & "" <> "" Then
                                sql = sql & ", CHK_NUM"
                                sql2 = sql2 & ", '" & dbReader.Item("CHK_NUM").ToString() & "' "
                            End If
                            Dim expDt As String = dbReader.Item("EXP_DT").ToString
                            If IsDate(expDt) Then
                                sql = sql & ", EXP_DT"
                                sql2 = sql2 & ", TO_DATE('" & expDt & "','mm/dd/RRRR')"
                            End If
                            If Host_Merchant_ID & "" <> "" Then
                                sql = sql & ", DES"
                                sql2 = sql2 & ", 'MID:" & Host_Merchant_ID & "' "
                            End If
                            If Auth_No & "" <> "" Then
                                sql = sql & ", APP_CD"
                                sql2 = sql2 & ",'" & Auth_No & "'"
                            End If
                            sql = UCase(sql)
                            sql2 = UCase(sql2)
                            If ref_num & "" <> "" Then
                                sql = sql & ", HOST_REF_NUM"
                                sql2 = sql2 & ",'" & ref_num & "'"
                            End If
                            If Trans_Id & "" <> "" Then
                                sql = sql & ", ACCT_NUM"
                                sql2 = sql2 & ",'" & Trans_Id & "'"
                            End If

                            sql = sql & ")"
                            sql2 = sql2 & ")"
                            sql = sql & sql2

                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "AR_TRN(" & del_doc_num & ") " & dbReader.Item("MOP_CD").ToString & " transaction for " & dbReader.Item("AMT").ToString & " committed.")
                        End If

                    Loop  'processing payments
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE COMMENTS     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing <> 2 Then
                    'Insert Sales "S" Comments
                    theSalesBiz.SaveSalesComments(dbAtomicCommand, soKeys, Session("scomments"), Session("STORE_CD"), Session("tran_dt"))

                    'Insert Delivery "D" Comments
                    theSalesBiz.SaveDeliveryComments(dbAtomicCommand, soKeys, Session("dcomments"), Session("STORE_CD"), Session("tran_dt"))

                    'Insert A/R Comments
                    theSalesBiz.SaveARComments(dbAtomicCommand, Session("EMP_CD"), Session("cust_cd"), Session("arcomments"))
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    START CLEANUP    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing = 2 Then
                    sql = "DELETE FROM TEMP_ITM WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH='Y'"
                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.ExecuteNonQuery()
                End If

                If default_invoice & "" = "" Then default_invoice = theSalesBiz.GetDefaultInvoiceName(Session("STORE_CD"))

                doc_processing = doc_processing - 1
            Loop    ' Do While doc_processing > 0

            If (Not (Session("Converted_REL_NO") Is Nothing) AndAlso Session("Converted_REL_NO").ToString().isNotEmpty) Then
                sql = "UPDATE RELATIONSHIP SET CONVERSION_DT=TO_DATE('" & FormatDateTime(DateTime.Today, DateFormat.ShortDate) & "','mm/dd/RRRR'), REL_STATUS='C' WHERE REL_NO='" & Session("Converted_REL_NO") & "'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()
            End If

            If Session("ord_tp_cd") = "CRM" AndAlso (Find_Security("C1REFUND", Session("emp_cd")) = "Y" OrElse Find_Security("ARRI", Session("emp_cd")) = "Y") Then
                ASPxPopupControl3.ShowOnPageLoad = True
            End If

            'caches some of the relevant SESSION values
            Dim IPAD As String = Session("IPAD")
            Dim empCd As String = Session("EMP_CD")
            Dim empFName As String = Session("EMP_FNAME")
            Dim empLName As String = Session("EMP_LNAME")
            Dim orderTpCd As String = Session("ord_tp_cd")
            Dim homeStoreCd As String = Session("HOME_STORE_CD")
            Dim companyCd As String = Session("CO_CD")

            If Not String.IsNullOrEmpty(err_msg) Then
                ASPxPopupControl2.ContentUrl = "Payment_Hold.aspx?del_doc_num=" & del_doc_num
                ASPxPopupControl2.ShowOnPageLoad = True
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<  COMMITS ALL CHANGES QUEUED DURING THIS WHOLE SAVE PROCESS >>>>>>>>>>>>>>>>>>>>>>>>
            ' FINANCE and XFER LIABILITY is OUTSIDE THE TRANSACTION FOR NOW, HAVE TO CHECK WITH DSA 
            dbAtomicCommand.Transaction.Commit()
            ' <<<<<<<<<<<<  WHAT HAPPENS BELOW THIS POINT, IS NOT WITHIN THE SINGLE TRANSACTION OBJECT >>>>>>>>>>>>>>>>>

            lbl_Follow_Up.Text = "Error Messages: " & vbCrLf

            If Session("cash_carry") = "TRUE" Then
                Xfer_fi_Liability(del_doc_num, dbConnection)
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    PRINTS INVOICE   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            If String.IsNullOrEmpty(err_msg) Then
                If Not String.IsNullOrEmpty(lbl_fin_co.Text.ToString) Then
                    Dim fin_invoice As String = theSalesBiz.GetAddendumInvoiceName(lbl_fin_co.Text, finCo, wdr)
                    ClientScript.RegisterStartupScript(Me.GetType(), "AnyScriptNameYouLike87", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                End If

                If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & del_doc_num & "&EMAIL=','frmTestfgd543g','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & del_doc_num & "&INVOICE_NAME=" & default_invoice & "','frmInvoice','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                End If
            End If

            If Not String.IsNullOrEmpty(err_msg) Then
                lbl_default_invoice.Text = default_invoice
                lbl_del_doc_num.Text = del_doc_num
                lbl_split.Text = split_order
                lbl_fin.Text = finCo
                lbl_wdr.Text = wdr
                ASPxButton1.Visible = True
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CLEANUP     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                sql = "delete from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
                dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                dbCommand.ExecuteNonQuery()

                sql = "delete from temp_itm where session_id='" & Session.SessionID.ToString.Trim & "'"
                dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                dbCommand.ExecuteNonQuery()
            End If

            lbl_Follow_Up.ForeColor = Color.Red
            lbl_Follow_Up.Text = lbl_Follow_Up.Text & Session("err") & vbCrLf
            If Len(lbl_Follow_Up.Text) = 20 Then lbl_Follow_Up.Visible = False

            ' restores the Session values from the cached values above to ready for the next transaction
            Dim partial_data As String = Session(del_doc_num & "_CMNT")
            ' Daniela save clientip
            Dim client_IP As String = Session("clientip")
            Dim empInit As String = Session("EMP_INIT")
            Dim supUser As String = Session("str_sup_user_flag")
            Dim coGrpCd As String = Session("str_co_grp_cd")
            Session.Clear()
            Session("clientip") = client_IP
            Session("str_sup_user_flag") = supUser ' Daniela
            Session("str_co_grp_cd") = coGrpCd ' Daniela
            Session("EMP_INIT") = empInit
            Session(del_doc_num & "_CMNT") = partial_data
            Session("IPAD") = IPAD
            Session("EMP_CD") = empCd
            Session("EMP_FNAME") = empFName
            Session("EMP_LNAME") = empLName
            Session("HOME_STORE_CD") = homeStoreCd
            Session("store_cd") = homeStoreCd
            Session("CO_CD") = companyCd

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "refresh", "setTimeout('window.location.href=\'CashCarryExpress.aspx\'', 3000);", True)

        Catch ex As Exception
            dbAtomicCommand.Transaction.Rollback()
            Throw
        Finally
            dbAtomicCommand.Dispose()
            dbAtomicConnection.Close()
            dbConnection.Close()
        End Try

    End Sub

    Public Sub Finance_Split_Print(ByVal del_doc_num As String, ByVal promo_cd As String, ByVal as_cd As String, Optional ByVal wdr As Boolean = False)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        'Open Connection 
        conn.Open()
        Dim fin_invoice As String = ""
        sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & as_cd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                If wdr = False Then
                    fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                Else
                    fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                End If
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'If InStr(as_cd, "GE") > 0 Then
        ClientScript.RegisterStartupScript(Me.GetType(), "AnyScriptNameYouLike354543" & del_doc_num, "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance9505954" & del_doc_num & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

        'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike354543" & del_doc_num, "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance9505954" & del_doc_num & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        'Else
        '    ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike354543" & del_doc_num, "window.open('FI_Addendums/ADS.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance34500404" & del_doc_num & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        'End If

    End Sub

    Sub Create_Customer()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim fname As String = ""
        Dim lname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim city As String = ""
        Dim state As String = ""
        Dim zip As String = ""
        Dim hphone As String = ""
        Dim bphone As String = ""
        Dim cust_tp_cd As String = ""
        Dim email As String = ""
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader

        sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
        'Open Connection 
        conn2.Open()

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then
                fname = MyDataReader2.Item("BILL_FNAME").ToString.Trim
                If fname & "" = "" Then fname = MyDataReader2.Item("FNAME").ToString.Trim
                lname = MyDataReader2.Item("BILL_LNAME").ToString.Trim
                If lname & "" = "" Then lname = MyDataReader2.Item("LNAME").ToString.Trim
                cust_tp_cd = MyDataReader2.Item("CUST_TP").ToString.Trim
                addr1 = MyDataReader2.Item("BILL_ADDR1").ToString.Trim
                If addr1 & "" = "" Then addr1 = MyDataReader2.Item("ADDR1").ToString.Trim
                addr2 = MyDataReader2.Item("BILL_ADDR2").ToString.Trim
                If addr2 & "" = "" Then addr2 = MyDataReader2.Item("ADDR2").ToString.Trim
                city = MyDataReader2.Item("BILL_CITY").ToString.Trim
                If city & "" = "" Then city = MyDataReader2.Item("CITY").ToString.Trim
                state = MyDataReader2.Item("BILL_ST").ToString.Trim
                If state & "" = "" Then state = MyDataReader2.Item("ST").ToString.Trim
                zip = MyDataReader2.Item("BILL_ZIP").ToString.Trim
                If zip & "" = "" Then zip = MyDataReader2.Item("ZIP").ToString.Trim
                hphone = StringUtils.FormatPhoneNumber(MyDataReader2.Item("HPHONE").ToString.Trim)
                bphone = StringUtils.FormatPhoneNumber(MyDataReader2.Item("BPHONE").ToString.Trim)
                email = MyDataReader2.Item("EMAIL").ToString.Trim
            End If

            'Close Connection 
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Session("cust_cd") = "NEW" Then
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")
            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"), UCase(addr1), Left(UCase(new_cust_lname), 20),
                                          Left(UCase(new_cust_fname), 15))

            sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
            sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
            objSql.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
            objSql.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
            objSql.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
            objSql.Parameters.Add(":FNAME", OracleType.VarChar)
            objSql.Parameters(":FNAME").Value = UCase(fname)
            objSql.Parameters.Add(":LNAME", OracleType.VarChar)
            objSql.Parameters(":LNAME").Value = UCase(lname)
            objSql.Parameters.Add(":CUST_TP", OracleType.VarChar)
            objSql.Parameters(":CUST_TP").Value = cust_tp_cd
            objSql.Parameters.Add(":ADDR1", OracleType.VarChar)
            objSql.Parameters(":ADDR1").Value = UCase(addr1)
            objSql.Parameters.Add(":ADDR2", OracleType.VarChar)
            objSql.Parameters(":ADDR2").Value = UCase(addr2)
            objSql.Parameters.Add(":CITY", OracleType.VarChar)
            objSql.Parameters(":CITY").Value = UCase(city)
            objSql.Parameters.Add(":ST", OracleType.VarChar)
            objSql.Parameters(":ST").Value = UCase(state)
            objSql.Parameters.Add(":ZIP", OracleType.VarChar)
            objSql.Parameters(":ZIP").Value = zip
            objSql.Parameters.Add(":HPHONE", OracleType.VarChar)
            objSql.Parameters(":HPHONE").Value = hphone
            objSql.Parameters.Add(":BPHONE", OracleType.VarChar)
            objSql.Parameters(":BPHONE").Value = bphone
            objSql.Parameters.Add(":EMAIL", OracleType.VarChar)
            objSql.Parameters(":EMAIL").Value = UCase(email)

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                objSql.ExecuteNonQuery()
                'Close Connection 
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            Try
                objsql2.ExecuteNonQuery()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        End If

    End Sub

    Private Sub Xfer_fi_Liability(ByVal del_doc_num As String, ByRef conn As OracleConnection)
        ' TODO - when make order save atomic transaction- need to pass in command and use in call to xfer liability below

        'Dim conn As OracleConnection = HBCG_Utils.GetConn(HBCG_Utils.Connection_Constants.CONN_ERP)
        Dim is_financed As Boolean = False

        ' SO is not committed yet so a new connection will not find this info
        Dim sql = "SELECT orig_fi_amt, fin_cust_cd, orig_del_doc_num, ord_tp_cd, so.stat_cd, final_dt, so_store_cd, so.cust_cd, s.co_cd, bnk_crd_num " +
           " FROM  ar_trn ar, store s, so WHERE ar.ivc_cd = so.del_doc_num AND s.store_cd = so.so_store_cd and ar.co_cd = s.co_cd " +
           " AND ar.trn_tp_cd = so.ord_tp_cd AND so.del_doc_num = '" & del_doc_num & "' AND so.fin_cust_cd IS NOT NULL and orig_fi_amt > 0 "

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim datRdr As OracleDataReader

        Try
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.Read() Then
                If datRdr.IsDBNull(0) = False Then
                    is_financed = True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        If is_financed Then

            ' TODO - when this becomes mostly one big atomic transaction then this does not need to be one, just pass cmd to util
            Dim connAtomic As OracleConnection = HBCG_Utils.GetConn(HBCG_Utils.Connection_Constants.CONN_ERP)
            Dim cmdA As OracleCommand
            Dim trans As OracleTransaction

            cmdA = DisposablesManager.BuildOracleCommand(connAtomic)

            Try
                connAtomic.Open()
                trans = connAtomic.BeginTransaction()
                cmdA.Transaction = trans

                HBCG_Utils.xferCustLiability(del_doc_num, datRdr("ORIG_DEL_DOC_NUM").ToString, datRdr("ORD_TP_CD"), datRdr("STAT_CD"), datRdr("FINAL_DT"), datRdr("SO_STORE_CD"),
                                  CDbl(datRdr("ORIG_FI_AMT").ToString), datRdr("CUST_CD"), datRdr("FIN_CUST_CD"), Session("emp_cd"), datRdr("CO_CD"),
                                  "POS", "O", datRdr("BNK_CRD_NUM"), cmdA)

                trans.Commit()

            Catch ex As Exception
                Try
                    trans.Rollback()

                Catch ex2 As Exception
                    ' This catch block will handle any errors that may have occurred
                    ' on the server that would cause the rollback to fail, such as
                    ' a closed connection.
                    Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                    Console.WriteLine("  Message: {0}", ex2.Message)
                End Try
                Throw ex

            Finally
                connAtomic.Close()
            End Try
        End If
    End Sub

    Public Sub Perform_Commit_Audit(Optional ByVal Num_Passes As Double = 1)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim custcd As String = ""
        Dim summary_txt As String = ""
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim Status As String = ""
        Dim Status_Proceed As String = ""

        Dim Take_deposit As Double = 0

        If Session("tet_cd") & "" = "" Then
            Take_deposit = Calculate_Take_With()
        Else
            Take_deposit = CDbl(Session("Take_Deposit"))
        End If

        txt_Follow_up.Text = ""

        If (Not IsDBNull(Session("CUST_CD")) AndAlso Not String.IsNullOrWhiteSpace(Session("CUST_CD"))) Then
            custcd = Session("CUST_CD").ToString
            'If custcd & "" <> "" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            If Num_Passes = 1 Then
                sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & custcd & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        summary_txt = "You have successfully completed the sales module.  Please review the data listed below before committing your order.<br /><br />"
                        summary_txt = summary_txt & MyDataReader.Item("FNAME").ToString & " " & MyDataReader.Item("LNAME").ToString & "<br />"
                        summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("ADDR1").ToString & "<br />"
                        summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("CITY").ToString & " " & MyDataReader.Item("ST").ToString & ", " & MyDataReader.Item("ZIP").ToString & "<br />"
                        summary_txt = summary_txt & Resources.LibResources.Label999 & StringUtils.FormatPhoneNumber(MyDataReader.Item("HPHONE").ToString) & "<br />"  ' lucy 21-Jul-18
                        summary_txt = summary_txt & "(C) " & StringUtils.FormatPhoneNumber(MyDataReader.Item("BPHONE").ToString) & "<br />"
                        If Session("zip_cd") & "" = "" Then
                            Session("zip_cd") = MyDataReader.Item("ZIP").ToString
                        End If
                        If String.IsNullOrEmpty(MyDataReader.Item("EMAIL").ToString) And ConfigurationManager.AppSettings("email_prompt").ToString = "Y" And Not IsPostBack Then
                            ASPxPopupControl1.ShowOnPageLoad = True
                        Else
                            summary_txt = summary_txt & MyDataReader.Item("EMAIL").ToString & "<br />"
                        End If
                        If String.IsNullOrEmpty(MyDataReader.Item("HPHONE").ToString) And ConfigurationManager.AppSettings("primary_phone").ToString = "Y" Then
                            Status = Status & "You must enter a primary phone #" & vbCrLf
                            Status_Proceed = "R"
                        End If
                        If String.IsNullOrEmpty(MyDataReader.Item("FNAME").ToString) Or String.IsNullOrEmpty(MyDataReader.Item("LNAME").ToString) Then
                            Status = Status & "You must enter a customer first and last name." & vbCrLf
                            Status_Proceed = "R"
                        End If
                        If String.IsNullOrEmpty(MyDataReader.Item("BPHONE").ToString) And ConfigurationManager.AppSettings("secondary_phone").ToString = "Y" Then
                            Status = Status & "You must enter a secondary phone #" & vbCrLf
                            Status_Proceed = "R"
                        End If
                    End If

                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
                txt_Follow_up.Text = summary_txt
            End If


            Dim conn2 As OracleConnection
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader

            conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            sql = "SELECT SUM(QTY) FROM TEMP_ITM WHERE session_id='" & Session.SessionID.ToString() & "'" ' AND ITM_TP_CD='INV'"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Open Connection 
                conn2.Open()
                'Execute DataReader 
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader2.Read()) Then
                    If MyDatareader2(0).ToString & "" <> "" Then
                        bindgrid()
                    End If
                Else
                    Status = Status & "No items have been placed on the order" & vbCrLf
                    Status_Proceed = "R"
                End If

                'Close Connection 
                MyDatareader2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            sql = "SELECT ITM_CD, STORE_CD, LOC_CD, TAKE_WITH FROM TEMP_ITM WHERE session_id='" & Session.SessionID.ToString() & "' AND ITM_TP_CD='INV'"

            Dim Item_Take_With As Integer = 0
            Dim invalidStores As Integer = 0
            Dim storeCd, locCd, itmCd As String

            Try
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)


                Do While MyDatareader2.Read()

                    storeCd = MyDatareader2.Item("STORE_CD").ToString
                    locCd = MyDatareader2.Item("LOC_CD").ToString
                    itmCd = MyDatareader2.Item("ITM_CD").ToString

                    If (String.IsNullOrEmpty(storeCd)) Then
                        'stores are mandatory for take-withs and cash-carry lines 
                        If MyDatareader2.Item("TAKE_WITH").ToString = "Y" Or Session("cash_carry") = "TRUE" Then
                            Item_Take_With = Item_Take_With + 1
                        End If
                    Else
                        'If ConfigurationManager.AppSettings("pfm") = "Y" Then
                        '    'when there is a store present makes sure the store is valid
                        '    If (Not HBCG_Utils.Validate_Store(storeCd, locCd, Session("PD_STORE_CD"), Session("PD"))) Then
                        '        Status = Status & String.Format("The Store {0} and Location {1} for item {2} are not valid for your P/D store.",
                        '                                        storeCd, locCd, MyDatareader2.Item("ITM_CD").ToString) & vbCrLf
                        '        invalidStores = invalidStores + 1
                        '    End If
                        'End If
                    End If
                Loop

                'Close Connection 
                MyDatareader2.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
            'If Item_Take_With > 0 Then
            '    Status = Status & "Take with items must have a store code and location" & vbCrLf
            'End If
            'If Item_Take_With > 0 Or invalidStores > 0 Then
            '    Status_Proceed = "R"
            'End If

            If Session("csh_act_init") = "INVALIDCASHIER" Then
                Status = Status & "There is a problem with your cashier activation (not activated, invalid post date)" & vbCrLf
                Status_Proceed = "R"
            End If

            If Session("del_dt") & "" = "" And Session("ord_tp_cd") = "MCR" Then
                Session("PD") = "P"
                Session("del_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
            End If
            If Session("del_dt") & "" = "" And Session("ord_tp_cd") = "MDB" Then
                Session("PD") = "P"
                Session("del_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
            End If
            If Session("del_dt") & "" <> "" Then
                If Not IsDate(Session("del_dt")) Then
                    Status = Status & "Pickup/Delivery date not selected" & vbCrLf
                    Status_Proceed = "R"
                End If
            Else
                Status = Status & "Pickup/Delivery date not selected" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("tet_cd") & "" <> "" Then
                If Session("exempt_id") & "" = "" Then
                    Status = Status & "Sale has been identified as tax exempt, but no tax id was found." & vbCrLf
                    Status_Proceed = "Y"
                End If
            End If
            If Session("cust_cd") & "" = "" Then
                Status = Status & Resources.LibResources.Label136 & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("ord_srt") & "" = "" And ConfigurationManager.AppSettings("ord_srt_cd").ToString = "Y" Then
                Status = Status & "You must select a sort code to proceed" & vbCrLf
                Status_Proceed = "R"
            End If
            'If Session("zip_cd") & "" = "" Then
            '    Status = Status & "Customer must have a zip code." & vbCrLf
            '    Status_Proceed = "R"
            'End If

            ds = New DataSet

            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT sum(amt) As Payment FROM payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session.SessionID.ToString.Trim & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            Dim Payment_total As Double
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If IsNumeric(MyDataReader.Item("Payment").ToString) Then
                        Payment_total = CDbl(MyDataReader.Item("Payment").ToString.Trim)
                    End If
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            If Session("ord_tp_cd") <> "CRM" Then
                If Session("ord_tp_cd") <> "MCR" Then
                    If Session("payment") & "" <> "" Then
                        If Session("cash_carry") = "TRUE" Then
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & "Cash and Carry orders must be paid in full.  Your balance is " & FormatCurrency(CDbl(Session("Grand_Total")) - CDbl(Payment_total), 2) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("allow_cod").ToString = "N" Then
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & "COD Balances are not allowed.  Your balance is " & FormatCurrency(CDbl(Session("Grand_Total")) - CDbl(Payment_total), 2) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("min_deposit").ToString <> "0" Then
                            If FormatNumber(CDbl(Payment_total), 2) - FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) < 0 Then
                                If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                                    Status = Status & "The minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ".  You need an additional payment of " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) - FormatNumber(CDbl(Payment_total), 2) & "." & vbCrLf
                                    Status_Proceed = "R"
                                Else
                                    Status = Status & "The recommended minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ".  You need an additional payment of " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * CDbl(Session("Grand_Total")) - CDbl(Payment_total)), 2) & "." & vbCrLf
                                    If Status_Proceed <> "R" Then
                                        Status_Proceed = "Y"
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If Session("cash_carry") = "TRUE" Then
                            ds = New DataSet
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & "Cash and Carry orders must be paid in full.  Your balance is " & CDbl(Session("Grand_Total")) - CDbl(Payment_total) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("allow_cod").ToString = "N" Then
                            Status = Status & "COD Balances are not allowed.  Your balance is " & CDbl(Session("Grand_Total")) & "" & vbCrLf
                            Status_Proceed = "R"
                        ElseIf ConfigurationManager.AppSettings("min_deposit").ToString <> "0" Then
                            If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                                Status = Status & "The minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & "." & vbCrLf
                                Status_Proceed = "R"
                            Else
                                Status = Status & "The recommended minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ".  You need an additional payment of " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & "." & vbCrLf
                                If Status_Proceed <> "R" Then
                                    Status_Proceed = "Y"
                                End If
                            End If
                        Else
                            Status = Status & "Payment has not been entered" & vbCrLf
                            If Status_Proceed <> "R" Then
                                Status_Proceed = "Y"
                            End If
                        End If
                    End If
                Else
                    If CDbl(Session("Grand_Total")) - CDbl(Payment_total) < 0 Then
                        Status = Status & "Refund payments cannot be greater than the return amount.  Your finance amount has been automatically updated." & vbCrLf
                        sql = "UPDATE PAYMENT SET AMT=" & CDbl(Session("Grand_Total")) & " where SESSIONID='" & Session.SessionID.ToString.Trim & "' AND MOP_TP='FI'"

                        'Set SQL OBJECT 
                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql.ExecuteNonQuery()
                    End If
                End If
            Else
                If CDbl(Session("Grand_Total")) - CDbl(Payment_total) < 0 Then
                    Status = Status & "Refund payments cannot be greater than the return amount.  Your finance amount has been automatically updated." & vbCrLf
                    sql = "UPDATE PAYMENT SET AMT=" & CDbl(Session("Grand_Total")) & " where SESSIONID='" & Session.SessionID.ToString.Trim & "' AND MOP_TP='FI'"

                    'Set SQL OBJECT 
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()
                End If
            End If
            If Session("comments") & "" = "" Then
                Status = Status & "No comments have been entered" & vbCrLf
                If Status_Proceed <> "R" Then
                    Status_Proceed = "Y"
                End If
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "Y" Then
                If Session("mark_cd") & "" = "" Then
                    Status = Status & "No marketing code has been selected" & vbCrLf
                    If Status_Proceed <> "R" Then
                        Status_Proceed = "Y"
                    End If
                End If
            End If
            If Session("csh_dwr_cd") & "" = "" Then
                Status = Status & "Cash Drawer Code has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("tran_dt") & "" = "" Then
                Status = Status & "Transaction date has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("store_cd") & "" = "" Then
                Status = Status & "Your store code has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If SalesUtils.Validate_Slsp(Session("slsp1")) = False Then
                Status = Status & "Salesperson 1 is invalid" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("PD") & "" = "" Then
                Status = Status & "Pickup/Delivery flag has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("tax_cd") & "" = "" And Session("tet_cd") & "" = "" Then
                Status = Status & "You must select a tax code or tax exempt status to continue" & vbCrLf
                Status_Proceed = "R"
            End If
            conn.Close()
        Else
            Status = Status & Resources.LibResources.Label136 & vbCrLf
            Status_Proceed = "R"
        End If
        txt_Follow_up.Text = txt_Follow_up.Text & Status
        If Status_Proceed = "Y" Then
            If txt_Follow_up.Text.Trim & "" = "" Then
                lbl_Status.Text = ""
                ASPxMenu5.Enabled = True
            Else
                lbl_Status.Text = "There are some items that have not been completed, but are not required to commit. Please verify the items listed below"
                ASPxMenu5.Enabled = True
            End If
        ElseIf Status_Proceed = "R" Then
            lbl_Status.Text = "There are some items that must be completed before committing. Please rectify the items listed below"
            ASPxMenu5.Enabled = False
        End If

    End Sub

    Public Function Get_Temp_ITM_CD(ByVal row_id As String)

        Get_Temp_ITM_CD = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        sql = "SELECT ITM_CD FROM TEMP_ITM WHERE ROW_ID=" & row_id

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDatareader2.Read() Then
                Get_Temp_ITM_CD = MyDatareader2.Item("ITM_CD").ToString
            End If
            MyDatareader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Function Get_First_Fab()

        Get_First_Fab = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        sql = "SELECT ROW_ID FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND ITM_TP_CD='FAB' ORDER BY ROW_ID"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDatareader2.Read() Then
                Get_First_Fab = MyDatareader2.Item("ROW_ID").ToString
            End If
            MyDatareader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Shared Sub SaveFinalDocItmTrn(ByRef dbAtomicCommand As OracleCommand,
                                   ByVal insReq As InventoryUtils.ItmTrnInsertReq,
                                   ByVal orderTp As String,
                                   ByVal transDt As Date,
                                   ByVal empCd As String)

        If AppConstants.Order.TYPE_SAL = orderTp Then

            insReq.ittCd = AppConstants.Inv.ITM_TRN_TP_SOF

        ElseIf AppConstants.Order.TYPE_CRM = orderTp Then

            insReq.ittCd = AppConstants.Inv.ITM_TRN_TP_CMF
            ' input put into old set - OK if SAL but need to fix if CRM
            insReq.newStoreCd = insReq.oldStoreCd
            insReq.newLocCd = insReq.oldLocCd
            insReq.oldStoreCd = ""
            insReq.oldLocCd = ""

        End If

        insReq.empCdOp = empCd
        insReq.trnDt = transDt  ' TODO DSA - check if this needs to be short or set to date - ditto fifl/fifo dates
        insReq.originCd = "POS"
        insReq.postedToGl = "N"
        insReq.confLabels = "N"

        InventoryUtils.InsertItmTrn(insReq, dbAtomicCommand)
        ' TODO - updt 00c, serial num and rf_id_cd
    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Public Function MonthLastDay(ByVal dCurrDate As Date)

        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Public Sub txt_fi_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_fi_acct.TextChanged

        lbl_GE_tran_id.Text = ""

        If IsSwiped(txt_fi_acct.Text) Then
            'first encrypt and set the swiped track info  without any parsing
            lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")

            'get the parsed acct# and exp date from the swiped track info 
            Dim swipedData = txt_fi_acct.Text
            txt_fi_acct.Text = GetAccountNumber(swipedData)
            txt_fi_exp_dt.Text = GetExpirationDate(swipedData)
        Else
            'encrypt entered acct number and set it 
            If IsNumeric(txt_fi_acct.Text) Then
                lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
            End If
        End If

        ' verify the card/acct number 
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)

    End Sub

    Protected Sub btn_save_chk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_chk.Click

        If txt_check.Text & "" = "" Then
            lbl_chk_warnings.Text = "Check # cannot be blank"
            Exit Sub
        End If
        If ConfigurationManager.AppSettings("chk_guar") = "Y" Then
            If TextBox2.Text & "" = "" Then
                lbl_chk_warnings.Text = "Routing # cannot be blank"
                Exit Sub
            End If
            If TextBox3.Text & "" = "" Then
                lbl_chk_warnings.Text = "Account # cannot be blank"
                Exit Sub
            End If
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If
        If ConfigurationManager.AppSettings("dl_verify") = "Y" Then
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & mop_drp.Value & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, CHK_NUM, CHK_TP, TRANS_ROUTE, CHK_ACCOUNT_NO, APPROVAL_CD, DL_STATE, DL_LICENSE_NO)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :CHK_NUM, :CHK_TP, :TRANS_ROUTE, :CHK_ACCOUNT_NO, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_TP", OracleType.VarChar)
            objSql.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = mop_drp.Value
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":CHK_NUM").Value = txt_check.Text.ToString
            objSql.Parameters(":APPROVAL_CD").Value = txt_appr.Text.ToString
            objSql.Parameters(":CHK_TP").Value = DropDownList2.SelectedValue.ToString
            objSql.Parameters(":TRANS_ROUTE").Value = TextBox2.Text.ToString
            objSql.Parameters(":CHK_ACCOUNT_NO").Value = TextBox3.Text.ToString
            objSql.Parameters(":DL_STATE").Value = txt_dl_state.Text.ToString
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_dl_number.Text.ToString

            objSql.ExecuteNonQuery()

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Session("payment") = True
        calculate_total()
        check_details.Visible = False

    End Sub

    Protected Sub btn_save_fi_co_Click(sender As Object, e As System.EventArgs) Handles btn_save_fi_co.Click

        Dim merchant_id As String = Lookup_Merchant("FI", Session("store_cd"), mop_drp.Value)
        If ConfigurationManager.AppSettings("finance") = "N" Then merchant_id = ""

        If (Not String.IsNullOrEmpty(merchant_id)) Then
            '' verify the card/acct number 
            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)
            If String.IsNullOrEmpty(myTypeValid.CCType) Or myTypeValid.CCType = "Unknown" Then
                lbl_fi_warning.Text = "Invalid account #"
                Exit Sub
            End If
            If String.IsNullOrEmpty(txt_fi_acct.Text) Then
                lbl_fi_warning.Text = "You must enter an account #"
                Exit Sub
            End If
            If Not IsNumeric(txt_fi_acct.Text) Then
                lbl_fi_warning.Text = "Account # must be numeric"
                Exit Sub
            End If
            If String.IsNullOrEmpty(cbo_promo.SelectedValue) Then
                lbl_fi_warning.Text = "You must select a promotion code"
                Exit Sub
            End If
            If cbo_promo.SelectedIndex = 0 Then
                lbl_fi_warning.Text = "You must select a promotion code"
                Exit Sub
            End If
            If ConfigurationManager.AppSettings("dl_verify") = "Y" Then
                If String.IsNullOrEmpty(txt_fi_dl_st.Text) Then
                    lbl_fi_warning.Text = "Must enter driver's license and state"
                    Exit Sub
                End If
                If txt_fi_dl_no.Text & "" = "" Then
                    lbl_fi_warning.Text = "Must enter driver's license and state"
                    Exit Sub
                End If
            End If
        End If

        If lbl_GE_tran_id.Text & "" = "" Then lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & mop_drp.Value & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        'If mop_drp.Value = "WDR" Then
        If Is_Finance_DP(mop_drp.Value) = True Then
            bc = "FI"
        End If

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = mop_drp.Value
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            objSql.Parameters(":EXP_DT").Value = txt_fi_exp_dt.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = "FI"
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            objSql.Parameters(":FI_ACCT_NUM").Value = lbl_GE_tran_id.Text
            objSql.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            objSql.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            objSql.ExecuteNonQuery()
            'End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Session("payment") = True
        calculate_total()
        finance_details.Visible = False
        payment_header.Visible = False

    End Sub

    Protected Sub btn_cash_submit_Click(sender As Object, e As System.EventArgs) Handles btn_cash_submit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & mop_drp.Value & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = mop_drp.Value
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc

            objSql.ExecuteNonQuery()

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Session("payment") = True
        calculate_total()
        cash_details.Visible = False
        payment_header.Visible = False

    End Sub

    Protected Sub ASPxMenu7_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu7.ItemClick

        check_details.Visible = True
        payment_header.Visible = True
        mop_drp_Populate("CK")
        lbl_pmt_amt.Text = lbl_balance.Text

    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu1.ItemClick

        cash_details.Visible = True
        payment_header.Visible = True
        mop_drp_Populate("CS")
        lbl_pmt_amt.Text = lbl_balance.Text

    End Sub

    Protected Sub ASPxMenu8_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu8.ItemClick

        finance_details.Visible = True
        payment_header.Visible = True
        mop_drp_Populate("FI")
        Add_Finance_Company()
        get_promo_codes()
        lbl_pmt_amt.Text = lbl_balance.Text

    End Sub

    Private Sub PopulateFIFOdetails(ByRef itmTrnInsReq As InventoryUtils.ItmTrnInsertReq,
                                    ByVal delDocNum As String,
                                    ByVal soLnNum As String,
                                    ByVal itmRplCost As Object)

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbSql As String

        dbSql = "SELECT FIFO_CST, FIFO_DT, FIFL_DT FROM SO_LN where DEL_DOC_NUM='" & delDocNum & "' and DEL_DOC_LN#=" & soLnNum

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Dim cst As Double = 0
                If IsNumeric(dbReader("FIFO_CST").ToString) Then
                    cst = dbReader("FIFO_CST")
                ElseIf IsNumeric(itmRplCost.ToString) Then
                    cst = itmRplCost
                End If
                itmTrnInsReq.cst = CDbl(cst.ToString)
                itmTrnInsReq.fifoDt = dbReader.Item("FIFO_DT").ToString
                itmTrnInsReq.fiflDt = dbReader.Item("FIFL_DT").ToString
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub
    Public Sub get_promo_codes()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        sql = "select NVL(SHORT_DESC,LONG_DESC) AS DES, PROMO_CD "
        sql = sql & "FROM PROMO_ADDENDUMS "
        sql = sql & "WHERE PLAN_ACTIVE='Y' "
        sql = sql & "AND AS_CD='" & cbo_finance_company.SelectedValue & "' "
        sql = sql & "AND EFF_DT <= SYSDATE "
        If theSystemBiz.HasPageAccess(Session("EMP_CD"), "ADMIN_PAGE") = False Then
            If Is_Finance_DP(mop_drp.Value) = True Then
                sql = sql & "AND WDR_CODE = 'Y' "
            Else
                sql = sql & "AND WDR_CODE = 'N' "
            End If
        End If
        sql = sql & "AND END_DT >= SYSDATE "
        sql = sql & "ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        If ds.Tables(0).Rows.Count < 1 Then
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            sql = "SELECT PROMO_CD AS PROMO_CD, DES FROM ASP_PROMO WHERE AS_CD='" & cbo_finance_company.SelectedValue & "' AND EFF_DT <= SYSDATE AND END_DT >= SYSDATE"

            ds.Clear()

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
        End If

        cbo_promo.Items.Clear()

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                cbo_promo.DataSource = ds
                cbo_promo.DataValueField = "PROMO_CD"
                cbo_promo.DataTextField = "DES"
                cbo_promo.DataBind()
            End If

            cbo_promo.Items.Insert(0, "Please select promotion code")
            'cbo_promo.Items.FindByValue("Please select promotion code").Value = ""
            cbo_promo.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_gc_Submit_Click(sender As Object, e As System.EventArgs) Handles btn_gc_Submit.Click

        If txt_gc.Text & "" = "" Then
            ASPxLabel15.Text = "Account # cannot be blank"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & mop_drp.Value & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, BC)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :BC)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = mop_drp.Value
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":BC").Value = txt_gc.Text.ToString

            objSql.ExecuteNonQuery()

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Session("payment") = True
        calculate_total()
        gift_Card_Details.Visible = False
        payment_header.Visible = False

    End Sub

    Protected Sub ASPxMenu3_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu3.ItemClick

        gift_Card_Details.Visible = True
        payment_header.Visible = True
        mop_drp_Populate("GC")
        lbl_pmt_amt.Text = lbl_balance.Text

    End Sub

    Protected Sub txt_gc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim merchant_id As String = ""
        merchant_id = Lookup_Merchant("GC", Session("store_cd"), "GC")

        If String.IsNullOrEmpty(merchant_id) Then
            ASPxLabel15.Text = "No Gift Card Merchant ID found for your home store code"
            Exit Sub
        End If

        Dim xml_response As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id, "balance", txt_gc.Text)
        ASPxLabel15.Text = "Response Received: " & Now.Date & vbCrLf
        If IsNumeric(xml_response.InnerText) Then
            ASPxLabel15.Text = "Card Balance: $" & FormatNumber(CDbl(xml_response.InnerText), 2) & vbCrLf
            If FormatNumber(CDbl(xml_response.InnerText), 2) < CDbl(lbl_pmt_amt.Text) Then
                lbl_pmt_amt.Text = FormatNumber(CDbl(xml_response.InnerText), 2)
                ASPxLabel15.Text = "Payment Amount was changed!  The balance on this gift card was less than the amount requested for payment."
            End If
        Else
            ASPxLabel15.Text = xml_response.InnerText
        End If

    End Sub

    Public Sub mop_drp_Populate(mop_tp As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim mop_found As Boolean = False
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','TC','(TRANSFER CREDIT)','IN','(FINANCE INSTALLMENT)','RV','(REVOLVING)','OP','(OPEN A/R)','LW','(LAYAWAY)','FI','(FINANCE)','') AS DES "
        sql = sql & "FROM SETTLEMENT_MOP "
        sql = sql & "WHERE MOP_ACTIVE='Y' "
        'If lbl_fin_exists.Text & "" <> "" Then
        '    If IsNumeric(lbl_fin_exists.Text) Then
        '        If CDbl(lbl_fin_exists.Text) <> 0 Then
        '            sql = sql & "AND MOP_TP != 'FI' "
        '        End If
        '    Else
        '        sql = sql & "AND MOP_TP != 'FI' "
        '    End If
        'End If
        If mop_tp <> "CS" Then
            sql = sql & "AND MOP_TP = '" & mop_tp & "' AND MOP_ACTIVE='Y'"
        Else
            sql = sql & "OR MOP_TP IS NULL AND MOP_ACTIVE='Y' OR MOP_TP='CS' "
        End If
        sql = sql & "AND MOP_ACTIVE='Y' "
        sql = sql & "ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mop_drp.Items.Clear()


        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                mop_found = True
                mop_drp.DataSource = ds
                mop_drp.ValueField = "MOP_CD"
                mop_drp.TextField = "DES"
                mop_drp.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()

            If mop_found = False Then
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

                sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','TC','(TRANSFER CREDIT)','IN','(FINANCE INSTALLMENT)','RV','(REVOLVING)','OP','(OPEN A/R)','LW','(LAYAWAY)','FI','(FINANCE)','') AS DES "
                sql = sql & "FROM MOP WHERE (MOP_TP != 'CD' or MOP_TP IS NULL) "
                If mop_tp <> "CS" Then
                    sql = sql & "AND MOP_TP = '" & mop_tp & "' "
                Else
                    sql = sql & "OR MOP_TP IS NULL OR MOP_TP='CS' "
                End If

                sql = sql & "ORDER BY DES"

                conn.Open()
                'Execute DataReader 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds)
                mop_drp.Items.Clear()

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    mop_found = True
                    mop_drp.DataSource = ds
                    mop_drp.ValueField = "MOP_CD"
                    mop_drp.TextField = "DES"
                    mop_drp.DataBind()
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            End If

            ' put the default first item
            mop_drp.SelectedIndex = 0

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub ASPxMenu6_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu6.ItemClick

        cc_details.Visible = True
        payment_header.Visible = True
        mop_drp_Populate("BC")
        lbl_pmt_amt.Text = lbl_balance.Text

    End Sub

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        Try
            btn_save.Enabled = True
            lbl_tran_id.Text = ""
            If InStr(TextBox1.Text, "^") > 0 Then
                lbl_tran_id.Text = Encrypt(TextBox1.Text, "CrOcOdIlE")
                Dim sCC As Array
                sCC = Split(TextBox1.Text.ToString, "^")
                TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
                If Left(TextBox1.Text, 1) = "B" Then TextBox1.Text = Right(TextBox1.Text, Len(TextBox1.Text) - 1)
                TextBox3.Text = Left(sCC(2).Trim, 2)
                TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
                lbl_Full_Name.Text = sCC(1).Trim
                lbl_Full_Name.ForeColor = Color.Black
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
            Else
                If IsNumeric(TextBox1.Text) Then
                    lbl_tran_id.Text = Encrypt(TextBox1.Text, "CrOcOdIlE")
                End If
                TextBox2.Focus()
            End If

            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(TextBox1.Text)

            Dim store_cd As String = ""
            If Request("ip_store_cd") & "" <> "" Then
                store_cd = Request("ip_store_cd")
            Else
                store_cd = Session("store_cd")
            End If

            If (Not thePmtBiz.IsValidCreditCard(store_cd, Request("mop_cd"), TextBox1.Text)) Then
                lbl_Full_Name.Text = " ** Entry does not match selected MOP ** "
                lbl_Full_Name.ForeColor = Color.Red
                TextBox1.Text = ""
                lbl_tran_id.Text = ""
                btn_save.Enabled = False
            End If

            If TextBox1.Text & "" <> "" Then
                TextBox1.Text = StringUtils.MaskString(TextBox1.Text, "x", 16, 4) & "'"
            End If

            lbl_card_type.Text = myTypeValid.CCType
            If String.IsNullOrEmpty(myTypeValid.CCType) Or lbl_card_type.Text = "Unknown" Then
                btn_save.Enabled = False
                lbl_Full_Name.Text = " ** Invalid Credit Card Entry ** "
                lbl_Full_Name.ForeColor = Color.Red
            End If
        Catch
            lbl_Full_Name.Text = " ** " & Err.Description & " ** "
            lbl_Full_Name.ForeColor = Color.Red
        End Try
        cc_details.Visible = True
        payment_header.Visible = True

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        lbl_card_type.Text = ""
        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox1.Focus()
        btn_save.Enabled = True

    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim thistransaction As OracleTransaction
        Dim NEW_EXP_DT As String = ""
        Dim sql As String = ""

        If TextBox1.Text & "" = "" Then
            lbl_Full_Name.Text = "No Credit Card Number Found"
            Exit Sub
        End If
        If ASPxTextBox1.Text & "" = "" Or ASPxTextBox2.Text & "" = "" Then
            lbl_Full_Name.Text = "No Expiration Date Found"
            Exit Sub
        End If
        If Len(Trim(TextBox1.Text)) > 25 Then
            lbl_Full_Name.Text = "Invalid Credit Card"
            Exit Sub
        End If
        If TextBox1.Enabled = True And txt_security_code.Text & "" = "" Then
            lbl_Full_Name.Text = "You must enter the security code for non-swipe transactions"
            txt_security_code.Focus()
            Exit Sub
        ElseIf TextBox1.Enabled = True And Not IsNumeric(txt_security_code.Text) Then
            lbl_Full_Name.Text = "Security code must be numeric"
            txt_security_code.Focus()
            Exit Sub
        End If

        NEW_EXP_DT = MonthLastDay(ASPxTextBox1.Text & "/1/" & ASPxTextBox2.Text)

        Dim cc_data As String = ""
        If lbl_tran_id.Text & "" <> "" Then
            cc_data = lbl_tran_id.Text
        Else
            cc_data = TextBox1.Text
        End If

        sql = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP,"
        If IsNumeric(txt_security_code.Text) Then
            sql = sql & " SECURITY_CD,"
        End If
        'jkl
        'sql = sql & " APPROVAL_CD) values ('" & Session.SessionID.ToString.Trim & "','" & Request("MOP_CD") & "','" & Request("AMT") & "','" & Request("DES") & "','" & cc_data & "','" & NEW_EXP_DT & "','BC'"
        sql = sql & " APPROVAL_CD) values (:SESSIONID, :MOP_CD, :AMT, :DES, :CCDATA, :NEW_EXP_DT, 'BC'"


        If IsNumeric(txt_security_code.Text) Then
            'jkl
            'sql = sql & "," & txt_security_code.Text
            sql = sql & ", :SEC_CODE"

        End If
        sql = sql & ", :AUTH)"

        objConnection.Open()
        thistransaction = objConnection.BeginTransaction
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = sql
            .Parameters.Add(":SESSIONID", OracleType.VarChar)
            .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            .Parameters.Add(":MOP_CD", OracleType.VarChar)
            .Parameters(":MOP_CD").Value = mop_drp.Value
            .Parameters.Add(":AMT", OracleType.VarChar)
            .Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            .Parameters.Add(":DES", OracleType.VarChar)
            .Parameters(":DES").Value = mop_drp.Text
            .Parameters.Add(":CCDATA", OracleType.VarChar)
            .Parameters(":CCDATA").Value = cc_data
            .Parameters.Add(":NEW_EXP_DT", OracleType.VarChar)
            .Parameters(":NEW_EXP_DT").Value = NEW_EXP_DT
            .Parameters.Add(":SEC_CODE", OracleType.VarChar)
            .Parameters(":SEC_CODE").Value = txt_security_code.Text
            .Parameters.Add(":AUTH", OracleType.VarChar)
            .Parameters(":AUTH").Value = txt_auth.Text
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()
        objConnection.Close()

        Session("payment") = True
        calculate_total()
        cc_details.Visible = False
        payment_header.Visible = False

    End Sub

    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged

        get_promo_codes()
        finance_details.Visible = True
        payment_header.Visible = True

    End Sub

    Private Function GetDefaultLocation(ByVal store_cd As String) As String

        Dim defaultLoc As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim Mydatareader As OracleDataReader
        Dim sql As String = "SELECT DEFAULT_LOC FROM STORE WHERE STORE_CD='" & store_cd & "'"

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            If Mydatareader.Read() Then
                If Mydatareader.Item("DEFAULT_LOC").ToString & "" <> "" Then
                    defaultLoc = Mydatareader.Item("DEFAULT_LOC").ToString
                End If
            End If
            conn.Close()
        Catch
            conn.Close()
            Throw
        End Try
        Return defaultLoc

    End Function

    Protected Sub ASPxButton2_Click(sender As Object, e As System.EventArgs) Handles ASPxButton2.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        lbl_card_type.Text = ""
        TextBox1.Enabled = True
        ASPxTextBox1.Enabled = True
        ASPxTextBox2.Enabled = True
        TextBox1.Focus()
        btn_save.Enabled = True
        cc_details.Visible = True
        payment_header.Visible = True

    End Sub

    ''' <summary>
    ''' Executes when the user clicks on the button to modify the tax code.
    ''' </summary>
    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkModifyTax.Click

        If (Not IsNothing(ViewState("CanModifyTax")) AndAlso Convert.ToBoolean(ViewState("CanModifyTax"))) Then

            Dim _hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
            _hidIsInvokeManually.Value = "true"

            Dim _hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
            _hidTaxDt.Value = Session("tran_dt").ToString

            ucTaxUpdate.LoadTaxData()
            PopupTax.ShowOnPageLoad = True
        Else
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
            ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
            msgPopup.ShowOnPageLoad = True
        End If

    End Sub

    ''' <summary>
    ''' Responds as the subscriber to the tax update user control event. It will hide the popup
    ''' and recalculate amounts based on the new tax info.
    ''' </summary>
    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated
        PopupTax.ShowOnPageLoad = False
        lbl_Status.Text = String.Empty
        ASPxMenu5.Enabled = True
        calculate_total()
    End Sub

    Private Sub ShowWarrantyPopup(focusedRowId As String)
        If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then
            Dim itmCd As String
            Dim isInventory As Boolean
            Dim isWarrantable As Boolean

            Dim dtWarrantable As New DataTable
            dtWarrantable.Columns.Add("ROW_ID", GetType(String))
            dtWarrantable.Columns.Add("ITM_CD", GetType(String))
            dtWarrantable.Columns.Add("ITM_TP_CD", GetType(String))
            dtWarrantable.Columns.Add("WARR_ROW_ID", GetType(String))
            dtWarrantable.Columns.Add("WARR_ITM_CD", GetType(String))
            dtWarrantable.Columns.Add("WARR_ITM_TEXT", GetType(String))
            dtWarrantable.Columns.Add("IS_PREV_SEL", GetType(Boolean))
            Dim drWarrantable As DataRow

            Dim dtSaleItems As DataTable = theSalesBiz.GetSalesItemForSOE(Session.SessionID)

            For Each dr As DataRow In dtSaleItems.Rows
                itmCd = dr("ITM_CD")
                isInventory = IIf("Y".Equals(dr.Item("INVENTORY") + ""), True, False)
                isWarrantable = IIf("Y".Equals(dr.Item("WARRANTABLE") + ""), True, False)

                ' check if need to apply warranty to the selectedSKU
                If isInventory AndAlso isWarrantable Then
                    Dim warranty_skus As Boolean = SalesUtils.hasWarranty(itmCd)
                    If warranty_skus = True Then
                        Dim discReCal As New SessVar.DiscountReCalcObj
                        discReCal = SessVar.discReCalc
                        discReCal.maybeWarr = True
                        SessVar.discReCalc = discReCal

                        If SysPms.isOne2ManyWarr Then
                            HBCG_Utils.CreateTempWarrFromTempItm(Session.SessionID.ToString.Trim, itmCd)
                        End If

                        drWarrantable = dtWarrantable.NewRow()
                        drWarrantable("ROW_ID") = dr("ROW_ID")
                        drWarrantable("ITM_CD") = itmCd
                        drWarrantable("ITM_TP_CD") = dr("ITM_TP_CD")
                        drWarrantable("WARR_ROW_ID") = dr("WARR_ROW_ID")
                        drWarrantable("WARR_ITM_CD") = dr("WARR_ITM_CD")

                        If dtWarrantable.Rows.Count > 0 Then
                            Dim filterExp As String = "IS_PREV_SEL = FALSE AND WARR_ROW_ID = '" & dr("WARR_ROW_ID") & "' AND WARR_ITM_CD = '" & dr("WARR_ITM_CD") & "'"
                            Dim drFiltered As DataRow() = dtWarrantable.Select(filterExp)
                            drWarrantable("IS_PREV_SEL") = drFiltered.Length > 0
                        Else
                            drWarrantable("IS_PREV_SEL") = False
                        End If

                        dtWarrantable.Rows.Add(drWarrantable)
                    End If
                End If
            Next

            If dtWarrantable.Rows.Count > 0 Then
                hidIsWarrChecked.Value = True

                Dim _popupWarranties As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMultiWarranties.FindControl("popupWarranties")
                _popupWarranties.Modal = True

                Dim _hidDelDocNum As HiddenField = _popupWarranties.FindControl("hidDelDocNum")
                _hidDelDocNum.Value = String.Empty

                Dim _hidFocusItem As HiddenField = _popupWarranties.FindControl("hidFocusItem")
                _hidFocusItem.Value = focusedRowId

                _popupWarranties.ShowOnPageLoad = True
                ucMultiWarranties.LoadWarranties(dtWarrantable)
            End If
        End If
    End Sub

    Protected Sub ucMultiWarranties_WarrantyAdded(sender As Object, e As EventArgs) Handles ucMultiWarranties.WarrantyAdded
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarrInfo As DataTable = CType(Session("WARRINFO"), DataTable)
            Dim sortExp As String = "WARR_ROW_ID, IS_PREV_SEL"
            Dim dt As DataTable = dtWarrInfo.Select(Nothing, sortExp).CopyToDataTable()

            Dim warrRowIdSeqNum As String = String.Empty
            Dim warrItmCd As String
            Dim drWarrRowId As DataRow()

            For Each dr As DataRow In dt.Rows
                warrItmCd = String.Empty

                'Need to fill the WARR_ROW_ID from warrRowIdSeqNum
                If Not IsDBNull(dr("IS_PREV_SEL")) AndAlso dr("IS_PREV_SEL") = True Then
                    warrItmCd = dr("WARR_ITM_CD")
                    drWarrRowId = dt.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" + warrItmCd + "'")

                    If drWarrRowId.Length > 0 Then
                        dr("WARR_ROW_ID") = drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID")
                    Else
                        dr("WARR_ROW_ID") = String.Empty
                    End If
                End If

                '***Check for Warranties being added or not after returning from warranties popup *******
                If Not IsDBNull(dr("WARR_ITM_CD")) AndAlso Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                    If String.IsNullOrEmpty(dr("WARR_ROW_ID")) Then
                        '*** Implies that a new warranty sku is being added after the popup selection. In this case a new 
                        '*** line for the warranty sku needs to be added. Then, the link info on the warrantable sku 
                        '*** also needs to be updated.
                        If Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                            '--means a warrantable sku was selected
                            Dim createLnReq As HBCG_Utils.CreateTempItmReq = initTempItmReq()
                            createLnReq.itmCd = dr("WARR_ITM_CD")
                            createLnReq.warrItmLink = dr("ITM_CD")
                            createLnReq.warrRowLink = CInt(dr("ROW_ID"))

                            warrRowIdSeqNum = HBCG_Utils.CreateWarrantyLn(createLnReq)
                            dr("WARR_ROW_ID") = warrRowIdSeqNum
                        End If
                    Else
                        '*** Means that an existing warranty was selected, so just update the link info on the warrantable
                        '*** sku. No need to add a separate warranty line.
                        UpdtWarrLnk(dr("WARR_ITM_CD"), dr("WARR_ROW_ID"), dr("ROW_ID"))
                    End If
                End If
            Next

            bindgrid()
            calculate_total()
        End If

        hidIsWarrChecked.Value = False

        If Not String.IsNullOrEmpty(hidItemCd.Value) Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty
            ShowRelatedSkuPopUp(relatedSku)
        End If

        Session.Remove("WARRINFO")
    End Sub

    Protected Sub ucMultiWarranties_NoWarrantyAdded(sender As Object, e As EventArgs) Handles ucMultiWarranties.NoWarrantyAdded
        ' if related SKUs then go get any
        If Not String.IsNullOrEmpty(hidItemCd.Value) Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
            ' if we might add more records from warr or related, then re-calc discount later
        Else
            bindgrid()  ' needed to show discounted prices
            calculate_total()
        End If

        hidIsWarrChecked.Value = False
    End Sub

    Private Sub ShowRelatedSkuPopUp(relatedSku As String)
        If Not String.IsNullOrEmpty(relatedSku) And Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
            ucRelatedSKU.LoadRelatedSKUData(relatedSku.Replace("'", String.Empty))
            hidIsWarrChecked.Value = False
        End If
    End Sub

    Protected Sub ucRelatedSKU_AddSelection(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSKU.AddSelection
        Session.Remove("itemid")
        Dim skus As String = String.Empty

        For Each obj As Object In selectedRelSKUs
            If Not IsNothing(obj) AndAlso Not String.IsNullOrEmpty(obj) Then
                skus = skus & "," & UCase(obj.ToString())
            End If
        Next

        If Not String.IsNullOrEmpty(skus) Then
            Session("ITEMID") = skus

            'Add the SKU
            InsertItems()

            'Populate Datagrid with new records
            bindgrid()
            calculate_total()
        End If
    End Sub

    Protected Sub ucRelatedSKU_NoneAdded(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSKU.NoneAdded
        Session.Remove("itemid")
        bindgrid() ' needed to show discounted prices
        calculate_total()
    End Sub
End Class
