Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization


Partial Class LoadItemsBrick
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Load_Items(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '           objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
            '              "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
            '             "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
            '            "from ITM_GENERATION_EXT a "
            ' June 27 as per Nick
            objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
                "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
                "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
                "from ITM_GENERATION_EXT_BRICK a Where rownum < 11 "


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            Button1.Visible = False
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg1.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = True
                lbl_msg1.Text = "No Error Found - Please Process the file."
                lbl_msg1.ForeColor = Color.Blue

            Else
                btn_process.Visible = False
                Button1.Visible = True
                lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process.Visible = False
            Button1.Visible = True
            lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Load_Items_Price(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview2.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '           objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
            '              "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
            '             "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
            '            "from ITM_GENERATION_EXT a "
            ' June 27 as per Nick
            objSql.CommandText = "select * from ITM_PRICE_EXT_BRICK a Where rownum < 11 order by 1, 2"


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        '       If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        Button1.Visible = False
                        Button2.Visible = True
                        Button3.Visible = False
                        'err_cnt = 1
                        'End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()
                lbl_msg2.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_price.Visible = True
                btn_process.Visible = False
                btn_process_warr.Visible = False
                lbl_msg2.Text = "No Error Found - Please Process the file."
                lbl_msg2.ForeColor = Color.Blue
            Else
                btn_process_price.Visible = False
                Button2.Visible = True
                lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process_price.Visible = False
            Button2.Visible = True
            lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Load_Warranty(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview3.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '           objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
            '              "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
            '             "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
            '            "from ITM_GENERATION_EXT a "
            ' June 27 as per Nick
            objSql.CommandText = "select * from ITM_WARRANTY_EXT_BRICK a Where rownum < 11 order by 1, 2, 3"


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        '    If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        'Button1.Visible = False
                        'err_cnt = 1
                        'End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()
                lbl_msg3.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_warr.Visible = True
                btn_process_price.Visible = False
                btn_process.Visible = False
                lbl_msg3.Text = "No Error Found - Please Process the file."
                lbl_msg3.ForeColor = Color.Blue

            Else
                btn_process_warr.Visible = False
                Button3.Visible = True
                lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process_warr.Visible = False
            Button3.Visible = True
            lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Process_Items(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select itm_cd, ve_cd, mnr_cd, des, NVL(err_msg, ' ') err_msg  from itm_code_creation_brick a where rownum < 11 "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg1.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = False
                lbl_msg1.Text = "ITEM's successfully uploaded."
                lbl_msg1.ForeColor = Color.Blue
            Else
                btn_process.Visible = False
                Button1.Visible = True
                lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg1.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub Process_Items_Price(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview2.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select * from ITM_PRICE_EXT_BRICK a where rownum < 11 "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        ' If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        'err_cnt = 1
                        'End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()
                lbl_msg2.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_price.Visible = False
                lbl_msg2.Text = "ITEM Price successfully uploaded."
                lbl_msg2.ForeColor = Color.Blue
            Else
                btn_process_price.Visible = False
                Button2.Visible = True
                lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg2.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub Process_Warranty(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview3.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select * from ITM_WARRANTY_EXT_BRICK a where rownum < 11 "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        '   If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        'err_cnt = 1
                        'End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()
                lbl_msg3.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_warr.Visible = False
                lbl_msg3.Text = "Warranties  successfully uploaded."
                lbl_msg3.ForeColor = Color.Blue

            Else
                btn_process_warr.Visible = False
                Button3.Visible = True
                lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg3.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub btn_process_Click(sender As Object, e As EventArgs) Handles btn_process.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_items()

        If result = "Y" Then
            Process_Items(sender, e)
        End If

        If result = "N" Then
            lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Protected Sub btn_process_price_Click(sender As Object, e As EventArgs) Handles btn_process_price.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_items_price()

        If result = "Y" Then
            Process_Items_Price(sender, e)
        End If

        If result = "N" Then
            lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Protected Sub btn_process_warr_Click(sender As Object, e As EventArgs) Handles btn_process_warr.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_items_warr()

        If result = "Y" Then
            Process_Warranty(sender, e)
        End If

        If result = "N" Then
            lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        ' Clear
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)

    End Sub

    Public Function validate_items() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "upload_items_brick.validate_items"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.ExecuteNonQuery()

            'Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

            'MyDA.Fill(Ds)
            'myCMD.Cancel()
            'myCMD.Dispose()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function validate_items_price() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "upload_items_brick.validate_price"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.ExecuteNonQuery()

            'Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

            'MyDA.Fill(Ds)
            'myCMD.Cancel()
            'myCMD.Dispose()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label2.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function validate_items_warr() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "upload_items_brick.validate_warranty"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.ExecuteNonQuery()

            'Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

            'MyDA.Fill(Ds)
            'myCMD.Cancel()
            'myCMD.Dispose()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label3.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = True
            Gridview2.Visible = False
            Gridview3.Visible = False
            If FileUpload1.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "itm_generation_brick.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label1.Text = "File name: " & _
                          FileUpload1.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload1.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_ITEMS_BRICK", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label1.Text = Label1.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button1.Visible = False

                        ' Display data from external table
                        Load_Items(sender, e)

                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label1.Text = "Only .csv files allowed!"
                End If
            Else
                Label1.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_ITEMS_BRICK", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = False
            Gridview2.Visible = True
            Gridview3.Visible = False
            If FileUpload2.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload2.FileName)
                Dim filePath = FileUpload2.PostedFile.FileName
                Dim fileNameUnix = "itm_price_brick.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload2.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload2.FileName)
                        FileUpload2.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload2.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label2.Text = "File name: " & _
                          FileUpload2.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload2.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload2.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_ITEMS", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label2.Text = Label1.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button2.Visible = False

                        ' Display data from external table
                        Load_Items_Price(sender, e)

                    Catch ex As Exception
                        Label2.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label2.Text = "Only .csv files allowed!"
                End If
            Else
                Label2.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_PRICE_BRICK", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label2.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = False
            Gridview2.Visible = False
            Gridview3.Visible = True
            If FileUpload3.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload3.FileName)
                Dim filePath = FileUpload3.PostedFile.FileName
                Dim fileNameUnix = "itm_warranty.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload3.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload3.FileName)
                        FileUpload3.SaveAs("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload3.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label3.Text = "File name: " & _
                          FileUpload3.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload3.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadItemsBrick\done\" & FileUpload3.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_WARR_BRICK", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label3.Text = Label1.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button3.Visible = False

                        ' Display data from external table
                        Load_Warranty(sender, e)

                    Catch ex As Exception
                        Label3.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label3.Text = "Only .csv files allowed!"
                End If
            Else
                Label3.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_WARR", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label3.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    'HOW TO USE:

    ' Upload file using FTP
    'UploadFile("c:\UploadFile.doc", "ftp://FTPHostName/UploadPath/UploadFile.doc", "UserName", "Password")

End Class
