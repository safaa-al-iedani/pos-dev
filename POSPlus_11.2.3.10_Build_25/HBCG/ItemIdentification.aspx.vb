Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTreeList
Imports DevExpress.Web

Partial Class ItemIdentification
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_custmpc_id As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If Find_Security("POSIICM", Session("EMP_CD")) = "Y" OrElse isSuperUser(Session("EMP_CD")) = "Y" Then
        Else
            Div1.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        Dim v_name As String = ""
        lbl_msg.Text = ""

        If isAuthToUpd() = "Y" Then
            btn_new.Visible = True
        Else
            btn_new.Visible = False
        End If


        If IsPostBack Then

        Else
            btn_search_Click(sender, e)
            btn_search.Visible = True
            Populate_company()
        End If

    End Sub

    Public Function isAuthToUpd() As String
        Return "Y"

        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','ItemIdentification.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function

    Public Function isAuthToAccess() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoAccess('" & Session("emp_cd") & "','ItemIdentification.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Protected Sub btn_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_new.Click
        pnl_search.Visible = False
        pnl_update.Visible = True
        pnl_grid.Visible = False
        btn_update.Text = "Create New"
    End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim v_msg As String = ""

        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        GV_ident_codes.DataBind()
        GV_ident_codes.CancelEdit()
    End Sub

    Function validate_update_fields() As String
        If upd_company_cd.Text & "" = "" Then
            Return "Please input company code"
        ElseIf upd_company_cd.Text.Length <> 3 Then
            Return "Company code shoud be 3 characters"
        ElseIf Not company_exists(upd_company_cd.Text) Then
            Return "Company code does not exist"
        End If

        If upd_ident_cd.Text & "" = "" Then
            Return "Please input identification code"
        ElseIf upd_ident_cd.Text.Length <> 3 Then
            Return "Identification code shoud be 3 characters"
        End If


        If upd_ident_desc.Text & "" = "" Then
            Return "Please input identification description"
        ElseIf upd_ident_desc.Text.Length > 40 Then
            Return "Identification code shoud be 3 characters"
        End If

        Return ""
    End Function

    Protected Sub populate_gridview()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_upd_usr As String = isAuthToUpd()

        Dim v_comp_cd_sql As String = ""
        Dim v_ident_cd_sql As String = ""
        Dim v_ident_desc_sql As String = ""

        If srch_company_cd.Text & "" = "ALL" Or isEmpty(srch_company_cd.Text) Then
            v_comp_cd_sql = ""
        Else
            v_comp_cd_sql = " and upper(a.CO_CD) like '" & UCase(srch_company_cd.Text) & "%'"
        End If

        If srch_ident_cd.Text & "" = "" Then
            v_ident_cd_sql = ""
        Else
            v_ident_cd_sql = " and upper(a.II_CD) like '" & UCase(srch_ident_cd.Text) & "%'"
        End If

        If srch_ident_desc.Text & "" = "" Then
            v_ident_desc_sql = ""
        Else
            v_ident_desc_sql = " and upper(a.DESCRIPTION) like '" & UCase(srch_ident_desc.Text) & "%'"
        End If

        GV_ident_codes.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        Dim sql As String = " select a.CO_CD, a.II_CD, a.DESCRIPTION, b.DES company_name from IICT a INNER JOIN CO b ON a.CO_CD = b.CO_CD "

        If v_comp_cd_sql <> "" Then
            sql &= v_comp_cd_sql
        End If

        If v_ident_cd_sql <> "" Then
            sql &= v_ident_cd_sql
        End If

        If v_ident_desc_sql <> "" Then
            sql &= v_ident_desc_sql
        End If

        sql &= " where std_multi_co2.isvalidco3('" & Session("emp_init") & "', a.CO_CD) = 'Y' "

        sql &= " order by CO_CD, II_CD"

        objSql.CommandText = sql

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GV_ident_codes.DataSource = ds
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_ident_codes.DataSource = ds
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub

    Protected Sub GV_DataBind()
        populate_gridview()
    End Sub

    Public Function getStores() As DataSet

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        sql = " select store_cd str_cd, store_cd||' - '||store_name str_nm from store "
        sql = sql & " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " order by 1 "

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        Return ds

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView1_CommandButtonInitialize(sender As Object, e As ASPxGridViewCommandButtonEventArgs)
        If e.VisibleIndex < 0 Then
            Exit Sub
        End If
    End Sub

    Private Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim v_msg As String = ""

        v_msg = validate_update_fields()
        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        If btn_update.Text = "Create New" Then
            v_msg = create()
        Else
            v_msg = update()
        End If

        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        Response.Redirect("ItemIdentification.aspx")
    End Sub

    Function company_exists(ByVal company_code As String) As Boolean

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from store " &
                            " where CO_CD = '" & UCase(company_code) & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return False
        ElseIf v_value = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Function create() As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand
        Try
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            objsql2 = DisposablesManager.BuildOracleCommand
            objsql2.CommandText = "INSERT INTO IICT (CO_CD, II_CD, DESCRIPTION) VALUES(:CO_CD, :II_CD, :DESCRIPTION)"
            objsql2.Connection = conn
            conn.Open()
            objsql2.Parameters.Add(":CO_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":II_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":DESCRIPTION", OracleType.VarChar)
            objsql2.Parameters(":CO_CD").Value = upd_company_cd.Text
            objsql2.Parameters(":II_CD").Value = upd_ident_cd.Text
            objsql2.Parameters(":DESCRIPTION").Value = upd_ident_desc.Text

            objsql2.ExecuteNonQuery()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Function

    Function update() As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand
        Try
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            objsql2 = DisposablesManager.BuildOracleCommand
            objsql2.CommandText = "update IICT Set DESCRIPTION = :DESCRIPTION where CO_CD = :CO_CD and II_CD = :II_CD"
            objsql2.Connection = conn
            conn.Open()
            objsql2.Parameters.Add(":CO_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":II_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":DESCRIPTION", OracleType.VarChar)
            objsql2.Parameters(":CO_CD").Value = upd_company_cd.Text
            objsql2.Parameters(":II_CD").Value = upd_ident_cd.Text
            objsql2.Parameters(":DESCRIPTION").Value = upd_ident_desc.Text

            objsql2.ExecuteNonQuery()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Function

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        Response.Redirect("ItemIdentification.aspx")
    End Sub

    Protected Sub GV_ident_codes_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles GV_ident_codes.RowCommand

        If e.CommandArgs.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)
            Dim fieldValues As List(Of Object) = New List(Of Object)

            upd_company_cd.Text = GV_ident_codes.GetRowValues(index, "CO_CD").ToString()
            upd_ident_cd.Text = GV_ident_codes.GetRowValues(index, "II_CD").ToString()
            upd_ident_desc.Text = GV_ident_codes.GetRowValues(index, "DESCRIPTION").ToString()

            pnl_search.Visible = False
            pnl_update.Visible = True
            pnl_grid.Visible = False
            btn_delete.Visible = False
            upd_company_cd.Enabled = False
            upd_ident_cd.Enabled = False
            btn_update.Text = "Update"
        End If
    End Sub

    Public Sub Populate_company()
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_company_cd.Items.Clear()

        sql = " select co_cd, co_cd||' - '||DES name from CO "
        sql &= " where std_multi_co2.isvalidco3('" & Session("emp_init") & "', co_cd) = 'Y' "
        sql = sql & " order by 2 "

        If isSuperUser(Session("EMP_CD")) = "Y" Then
            srch_company_cd.Items.Insert(0, "All companies")
            srch_company_cd.Items.FindByText("All companies").Value = "ALL"
            srch_company_cd.SelectedIndex = 0
            upd_company_cd.Items.Insert(0, "All companies")
            upd_company_cd.Items.FindByText("All companies").Value = "ALL"
            upd_company_cd.SelectedIndex = 0
        End If


        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_company_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            With upd_company_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Public Shared Function isSuperUser(ByRef p_emp_cd As String)
        Dim p_sup_flag As String = "N'"
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add("p_co_grp_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_co_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If Not IsDBNull(myCMD.Parameters("p_ind").Value) Then
                p_sup_flag = myCMD.Parameters("p_ind").Value
            End If

        Catch x As Exception
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_sup_flag
    End Function

End Class

