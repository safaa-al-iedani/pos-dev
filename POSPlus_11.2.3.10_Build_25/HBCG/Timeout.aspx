<%@ Page Language="VB" CodeFile="Timeout.aspx.vb" Inherits="Timeout" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
   #progressBackgroundFilter {
    position:fixed;
    top:0px;
    bottom:0px;
    left:0px;
    right:0px;
    overflow:hidden;
    padding:0;
    margin:0;
    background-color:#000;
    filter:alpha(opacity=50);
    opacity:0.5;
    z-index:1000;
}#processMessage {
    position:fixed;
    top:30%;
    left:30%;
    padding:10px;
    width:14%;
    z-index:1001;
    background-color:#fff;
    border:solid 1px #000;
}
 </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="Scriptmanager1" runat="server" AsyncPostBackTimeout="0">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="padding-left: 0px; padding-top: 3px">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>&nbsp;</td>
                            <td width="810px" align="left" valign="middle">
                                <table width="810px" border="1" cellspacing="0" cellpadding="0" bgcolor="White">
                                    <tr>
                                        <td height="56">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" valign="top" width="60%">
                                                        <div style="padding-left: 8px;">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <img src="<% = ConfigurationManager.AppSettings("company_logo").ToString %>" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxLabel ID="lbl_header" runat="server" Text="<%$ Resources:LibResources, Label515 %>"></dx:ASPxLabel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <br />
                                                    </td>
                                                    <td align="right" valign="top" style="width: 222px">
                                                        <div style="padding-right: 13px; padding-top: 8px">
                                                            <dx:ASPxLabel ID="lbl_Header1" runat="server" Text=""></dx:ASPxLabel>
                                                            <br />
                                                            <dx:ASPxLabel ID="lbl_Header2" runat="server" Text=""></dx:ASPxLabel>
                                                        </div>
                                                    </td>
                                                    <td align="right" valign="top">
                                                        <div style="padding-right: 13px; padding-top: 8px">
                                                            <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                                                                <Items>
                                                                    <dx:MenuItem Text="<%$ Resources:LibResources, Label273 %>" NavigateUrl="Logout.aspx">
                                                                        <Image Url="~/images/login_icon.gif" Width="25px"></Image>
                                                                    </dx:MenuItem>
                                                                </Items>
                                                            </dx:ASPxMenu>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="32" align="left">
                                            <dx:ASPxMenu ID="ASPxMenu1" runat="server" Width="100%">
                                                <Items>
                                                    <dx:MenuItem Text="<%$ Resources:LibResources, Label228 %>" NavigateUrl="~/newmain.aspx">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Text="Reporting" NavigateUrl="~/Reporting.aspx" Visible="false">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Text="Utilities" NavigateUrl="~/Utilities.aspx" Visible="false">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Text="">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Text="">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Text="">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </dx:MenuItem>
                                                </Items>
                                            </dx:ASPxMenu>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="810px" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td height="22" align="left" valign="top">
                                                        <div style="padding-left: 13px; padding-top: 4px">
                                                            <dx:ASPxLabel ID="lblTabID" runat="server" Text="<%$ Resources:LibResources, Label796 %>">
                                                            </dx:ASPxLabel>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:HyperLink ID="hpl_exit" runat="server" CssClass="style5" Visible="False"><< Back</asp:HyperLink></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div style="padding-left: 5px; padding-top: 4px">
                                                            <div align="center">
                                                                <br /><br /><br /><br />
                                                                <img src="images/warning.jpg" />
                                                                <br /><br />
                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="value set in the code behind"></dx:ASPxLabel>
                                                                <br /><br />
                                                                <dx:ASPxHyperLink ID="ASPxHyperLink1" NavigateUrl="javascript:history.back();" runat="server" Text="<%$ Resources:LibResources, Label798 %>"></dx:ASPxHyperLink>
                                                                <br /><br /><br /><br /><br /><br />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="left" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="46" align="center" valign="top">
                                            <div style="padding-left: 0px; padding-top: 18px">
                                                <dx:ASPxHyperLink ID="ASPxHyperLink22" NavigateUrl="newmain.aspx" runat="server" Text="<%$ Resources:LibResources, Label228 %>"></dx:ASPxHyperLink>
                                                <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif" width="12" height="1">
                                                <dx:ASPxHyperLink ID="ASPxHyperLink23" NavigateUrl="reporting.aspx" runat="server" Text="Reporting" visible="false"></dx:ASPxHyperLink>
                                                <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif" width="12" height="1">
                                                <dx:ASPxHyperLink ID="ASPxHyperLink24" NavigateUrl="utilities.aspx" runat="server" Text="Utilities" visible="false"></dx:ASPxHyperLink>
                                                <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif" width="12" height="1">
                                                <dx:ASPxHyperLink ID="ASPxHyperLink25" NavigateUrl="salestrends.aspx" runat="server" Text="Sales Trends" Visible="false"></dx:ASPxHyperLink>
                                                <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif" width="12" height="1">
                                                <dx:ASPxHyperLink ID="ASPxHyperLink27" NavigateUrl="logout.aspx" runat="server" Text="<%$ Resources:LibResources, Label273 %>"></dx:ASPxHyperLink>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="22" align="center" valign="middle">
                                            <dx:ASPxHyperLink ID="ASPxHyperLink28" NavigateUrl="http://www.redprairie.com"
                                                runat="server" Text="Copyright &copy; 2011-2013. All Rights Reserved">
                                            </dx:ASPxHyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td width="694" align="left" valign="middle">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
