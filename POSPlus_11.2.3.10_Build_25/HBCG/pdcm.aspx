﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pdcm.aspx.vb" Inherits="Default2" %><%@ Register assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <%--

       <dx:ASPxGridView ID="GridView1" runat="server" AutoGenerateColumns="False"
        Width="100%" Mode="ShowAllRecords" KeyFieldName="seq">
        <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Caption="" Width="10px">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="SEQ" FieldName="seq" VisibleIndex="1" Width="20px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="COMMENT" FieldName="cmnt" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
             
        </Columns>
        <SettingsPager Visible="False">
        </SettingsPager>
    </dx:ASPxGridView>

--%>
         
        
         
    </div>
        <dx:ASPxGridView ID="GridView1" runat="server" AutoGenerateColumns="False"   Width="500px" Mode="ShowAllRecords" KeyFieldName="SEQ">
            <Columns>
                <dx:GridViewCommandColumn Caption="<%$ Resources:LibResources, Label528 %>" ShowSelectCheckbox="True"   VisibleIndex="0" Width="30px" >
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label697 %>" VisibleIndex="2" Width="500px" FieldName="CMNT" ReadOnly="True">
                    <PropertiesTextEdit MaxLength="72">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Seq" FieldName="SEQ" Visible="False" VisibleIndex="1" Width="20px">
                    <PropertiesTextEdit EnableFocusedStyle="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="40">
            </SettingsPager>
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Styles>
                <SelectedRow ForeColor="#66FF66">
                </SelectedRow>
            </Styles>
        </dx:ASPxGridView>
        <asp:Button ID="Button1" runat="server"  Text="<%$ Resources:LibResources, Label89 %>" Width="500px" ForeColor="#CC33FF" />
    </form>
</body>
</html>
