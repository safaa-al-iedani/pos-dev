<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="CtlFinalization.aspx.vb" Inherits="CtlFinalization"    %>
<%--  CodeFile="SRRTD.aspx.vb" Inherits="SRRTD" meta:resourcekey="PageResource1" UICulture="auto" %>--%>
<%--
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>  --%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <table> 
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Store Group/Store" ID="lbl_store_grp_cd" Width="160px"></asp:Label>
                           
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_store" runat="server"></asp:DropDownList>
                            
                        </td>
                         <td align="right">
                         <asp:Label runat="server" Text="Store" ID="lbl_st" Width="160" Visible="False"></asp:Label>
                           
                        </td>

                        <td>
                            <asp:DropDownList ID="dw_st" runat="server" Visible="False"></asp:DropDownList>
                            
                        </td>

                    </tr>
                </table>
            </td>
            </tr>
            <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Picked /Delivered  " ID="lbl_w_d"  Width="160px" ></asp:Label>
                          
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_p_d" runat="server"  Width="60px"></asp:DropDownList>
                            
                        </td>
                        
                        
                    </tr>
                </table>
            </td>
        </tr>
            <tr>
               
            <td align="left">
                <table>
                    <tr>
                        
                        <td>
                         <asp:Label runat="server" Text="Min Pick Status" ID="lbl_inv_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_min_pick_status" runat="server"      AutoPostBack="True"  Width="60px"></asp:DropDownList>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
             <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Enabled" ID="lbl_itm_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:DropDownList ID="drpdwn_enabled" runat="server"  Width="60px"></asp:DropDownList>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    
                                   

                    <tr align="right">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_search" runat="server" Text="Search" onclick="click_btn_search" Width="60px"/> </asp:Button>
                                         
                                    </td>
                                    <td>
                                         <asp:Button ID="btn_clr_screen" runat="server" Text="Clear" onclick="btn_clr_screen_Click" Width="60px"/> </asp:Button>
                                        
                                    </td>
                                    <td  align="right">

                            <asp:Button ID="btn_new" runat="server" Text="NEW"  onclick="click_btn_insert" Width="60px"/> </asp:Button>

                        </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
         
        
            <tr>
            <td>
                <asp:DataGrid ID="GV_SRR" runat="server"  Width="580px" BorderColor="Black"  runat="server" AutoPostBack="true"
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="rowid"   OnDeleteCommand="DoItemDelete"
                  Height="16px" AlternatingItemStyle-BackColor="Beige"  PagerStyle-NextPageText=" " PagerStyle-PrevPageText=" ">            
                    
               
                <Columns>
                    
                   
                
                  <asp:BoundColumn DataField="rowid" HeaderText="Rowid" Visible="False"   ReadOnly="True" /> 
                  <asp:BoundColumn DataField="store_cd" HeaderText="P/U Delivery Store" ReadOnly="True" Visible="True"  HeaderStyle-Width="145px" ItemStyle-HorizontalAlign="Center" />    
                  <asp:BoundColumn DataField="pu_del" HeaderText="P/U Delivery Flag"    ReadOnly="True" Visible="False" />
                  <asp:BoundColumn DataField="min_stat" HeaderText="Min Pick Status"    ReadOnly="True" Visible="False" />
                  <asp:BoundColumn DataField="enabled" HeaderText="Enabled"             ReadOnly="True" Visible="False" />   

                    <asp:TemplateColumn HeaderText="P/U Delivery Flag" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:DropDownList ID="drpdwn_pd"  runat="server" AutoPostBack="true"  ></asp:DropDownList>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                  <asp:TemplateColumn HeaderText="Min Pick Status" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:DropDownList ID="drpdwn_min_status"  runat="server" AutoPostBack="true"  ></asp:DropDownList>
                     </ItemTemplate>
                   </asp:TemplateColumn>  
                    
                       <asp:TemplateColumn HeaderText="Enabled" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:DropDownList ID="drpdwn_yn"  runat="server" AutoPostBack="true"  ></asp:DropDownList>
                     </ItemTemplate>
                   </asp:TemplateColumn> 
                 
       
                
                       <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="<%$ Resources:LibResources, Label150 %>">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>

                </Columns>

    </asp:DataGrid>
                
                </td>
                <td>
                 <asp:Button ID="btn_update" runat="server" Text="Update" onclick="click_btn_update" Width="60px"/> </asp:Button>
                    </td>
       </tr>
       
    </table>
     <asp:Label runat="server"   ID="lbl_msg"  Width="100%" ForeColor="Red"></asp:Label>
     <asp:Label runat="server"   ID="lbl_warning"  Width="100%" ForeColor="Red"></asp:Label>
  <%-- <dx:aspxlabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:aspxlabel> --%>
    <br />
    <br />
    
    <div runat="server" id="div_version">
          
        <asp:label ID="lbl_versionInfo" runat="server" >
       &nbsp;&nbsp;&nbsp; </asp:label>  
    </div>
</asp:Content>   
