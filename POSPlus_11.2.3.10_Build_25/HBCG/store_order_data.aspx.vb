
Partial Class store_order_data
    Inherits POSBasePage

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        If Request("LEAD") = "TRUE" Then
            Response.Write("parentWindow.location.href='newmain.aspx?LEAD=TRUE&save_order_info=TRUE';" & vbCrLf)
        Else
            Response.Write("parentWindow.location.href='newmain.aspx?save_order_info=TRUE';" & vbCrLf)
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        Response.Write("parentWindow.location.href='newmain.aspx?save_order_info=FALSE';" & vbCrLf)
        Response.Write("</script>")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btn_Return_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Return.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        If Request("LEAD") = "TRUE" Then
            Response.Write("parentWindow.location.href='startup.aspx?LEAD=TRUE';" & vbCrLf)
        Else
            Response.Write("parentWindow.location.href='startup.aspx';" & vbCrLf)
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
