Imports System.Data.OracleClient

Partial Class Sales_Relationship
    Inherits POSBasePage

    Private LeonsBiz As LeonsBiz = New LeonsBiz() 'Daniela June 18

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim sql As String
        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim HTML As String = ""
        Dim bgcolor As String = "bgcolor=beige"
        Dim sqlStmtSb As New StringBuilder
        Session("ShowRelationships") = False

        ' Daniela June 18 MCCL added
        'sqlStmtSb.Append("SELECT * FROM RELATIONSHIP WHERE CUST_CD = :cust_cd AND REL_STATUS='O' AND CONVERSION_DT IS NULL ")

        ' Daniela June 18 and parameter
        Dim grpCd As String = ""
        If isNotEmpty(Session("CO_CD")) Then
            grpCd = LeonsBiz.GetGroupCode(Session("CO_CD"))
            If isNotEmpty(grpCd) Then
                sqlStmtSb.Append("SELECT * FROM RELATIONSHIP WHERE CUST_CD = :cust_cd AND REL_STATUS='O' AND CONVERSION_DT IS NULL and store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd= '" & grpCd & "'))")
            End If
        End If

        If isEmpty(Session("CO_CD")) Then
            sqlStmtSb.Append("SELECT * FROM RELATIONSHIP WHERE CUST_CD = :cust_cd AND REL_STATUS='O' AND CONVERSION_DT IS NULL ")
        End If
        If "Y".Equals(ConfigurationManager.AppSettings("show_cust_relationships").ToString & "") AndAlso "Y".Equals(ConfigurationManager.AppSettings("rel_access").ToString & "") Then
            sqlStmtSb.Append(" And EMP_CD = : emp_cd")
        End If

        ' End Daniela

        If (SysPms.numDaysRelship > 0) Then
            sqlStmtSb.Append(" AND ( (follow_up_dt IS NOT NULL AND follow_up_dt + :days2Display >= TRUNC(SYSDATE)) ").Append(
                 " OR ( follow_up_dt IS NULL AND wr_dt IS NOT NULL AND TO_DATE(wr_dt) + :days2Display >= TRUNC(SYSDATE) ) ) ")   ' This logic depends on wr_dt being a date although it is not in the db; confirmed inserting dt wherever created
            ' although we've allowed for null follow_up_dt here, the process will fail if it is null
        End If

        conn2.Open()
        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn2)

        objsql.Parameters.Add(":cust_cd", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters(":cust_cd").Value = Session("cust_cd")
        If "Y".Equals(ConfigurationManager.AppSettings("show_cust_relationships").ToString & "") AndAlso "Y".Equals(ConfigurationManager.AppSettings("rel_access").ToString & "") Then
            objsql.Parameters.Add(":emp_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters(":emp_cd").Value = Session("EMP_CD")
        End If
        If (SysPms.numDaysRelship > 0) Then

            objsql.Parameters.Add(":days2Display", OracleType.Number).Direction = ParameterDirection.Input
            objsql.Parameters(":days2Display").Value = SysPms.numDaysRelship
        End If

        HTML = "<font color=""black"">The following open relationships have been found for the customer you selected.  To convert one of the relationships below; "
        HTML = HTML & "click on the relationship number and the data will be automatically added to your order.</font><br /><br />"

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

            HTML = HTML & "<table class=""style5"" width=""100%"" border=0 cellpadding=0 cellspacing=0>"
            'Daniela french
            Do While MyDatareader.Read()
                ''HTML = HTML & "<tr><td colspan=7 height=""10"">&nbsp;</td></tr>"

                HTML = HTML & "<tr>"
                HTML = HTML & "<td rowspan=2><img src=""images/icons/relationship.png"" width=""50"" height=""50""></td>"
                HTML = HTML & "<td>" & Resources.LibResources.Label459 & ":</td>"
                HTML = HTML & "<td colspan=2 align=""left""><a href=cust_edit.aspx?del_doc_num=" & MyDatareader.Item("REL_NO").ToString & "&custid=" & Session("cust_cd") & " class=style5 target=_parent>" & MyDatareader.Item("REL_NO").ToString & "</a></td>"
                HTML = HTML & "<td>" & Resources.LibResources.Label668 & ":</td>"
                HTML = HTML & "<td colspan=2 align=""left"">" & FormatDateTime(MyDatareader.Item("WR_DT").ToString, DateFormat.ShortDate) & "</td>"
                HTML = HTML & "<td>&nbsp;</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td valign=""top"">" & Resources.LibResources.Label212 & ":</td>"
                HTML = HTML & "<td valign=""top"" colspan=2 align=""left"">" & FormatDateTime(MyDatareader.Item("FOLLOW_UP_DT").ToString, DateFormat.ShortDate) & "</td>"
                HTML = HTML & "<td valign=""top"">" & Resources.LibResources.Label696 & ":</td>"
                HTML = HTML & "<td valign=""top"" colspan=2 align=""left"">" & FormatCurrency(MyDatareader.Item("PROSPECT_TOTAL").ToString, 2) & "</td>"
                HTML = HTML & "<td>&nbsp;</td>"
                HTML = HTML & "</tr>"

                sql = "SELECT * FROM RELATIONSHIP_LINES WHERE REL_NO='" & MyDatareader.Item("REL_NO").ToString & "'"

                'Set SQL OBJECT 
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

                Try
                    MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    Do While MyDatareader2.Read()
                        If String.IsNullOrEmpty(bgcolor) Then
                            bgcolor = "bgcolor=beige"
                        Else
                            bgcolor = ""
                        End If
                        HTML = HTML & "<tr " & bgcolor & ">"
                        HTML = HTML & "<td>" & MyDatareader2.Item("LINE").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDatareader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDatareader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDatareader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDatareader2.Item("DES").ToString & "</td>"
                        HTML = HTML & "<td>" & CDbl(MyDatareader2.Item("QTY").ToString) & "</td>"
                        HTML = HTML & "<td>" & FormatCurrency(MyDatareader2.Item("RET_PRC").ToString, 2) & "</td>"
                        HTML = HTML & "</tr>"
                    Loop
                Catch
                    conn2.Close()
                    Throw
                End Try
                MyDatareader2.Close()
                HTML = HTML & "<tr><td colspan=7 background=""images/rep_1.jpg"">&nbsp;</td></tr>"
                HTML = HTML & "<tr><td colspan=7>&nbsp;</td></tr>"
            Loop
            MyDatareader.Close()
        Catch
            conn2.Close()
            Throw
        End Try
        HTML = HTML & "</table>"
        conn2.Close()
        main.InnerHtml = HTML
    End Sub

End Class
