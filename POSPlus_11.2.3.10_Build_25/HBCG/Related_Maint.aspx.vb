Imports System.Data.OracleClient
Imports System.Net.NetworkInformation
Imports HBCG_Utils

Partial Class Related_Maint
    Inherits POSBasePage
    Dim ds As New DataSet

    Public Sub bindgrid()
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds2 = New DataSet

        Gridview1.DataSource = ""

        Gridview1.Visible = True
        conn.Open()

        sql = "SELECT REL_CD, REL_TP, ITM_CD FROM RELATED_SKUS "
        sql = sql & "ORDER BY REL_TP, REL_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds2
                Gridview1.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("MCM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            Button1.Enabled = False
            Exit Sub
        Else
            Button1.Enabled = True
        End If

        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If txt_sku.Text & "" = "" Then
            lbl_warnings.Text = "You must enter a SKU"
            Exit Sub
        End If
        If txt_mjr_mnr.Text & "" = "" Then
            lbl_warnings.Text = "You must enter a major or minor code"
            Exit Sub
        End If
        lbl_warnings.Text = ""

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        sql = "INSERT INTO RELATED_SKUS (REL_CD, REL_TP, ITM_CD) "
        sql = sql & "VALUES(:REL_CD, :REL_TP, :ITM_CD)"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":REL_CD", OracleType.VarChar)
        objSql.Parameters.Add(":REL_TP", OracleType.VarChar)
        objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)

        objSql.Parameters(":REL_CD").Value = txt_mjr_mnr.Text
        objSql.Parameters(":REL_TP").Value = cbo_mjr_mnr.SelectedValue
        objSql.Parameters(":ITM_CD").Value = txt_sku.Text

        objSql.ExecuteNonQuery()
        conn.Close()
        bindgrid()

    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String

        conn.Open()
        'deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
        deletecd = e.Item.Cells(0).Text.ToString

        'We need to find any fabric protection if linked to ensure that we delete those records as well.

        With cmdDeleteItems
            .Connection = conn
            .CommandText = "delete from related_skus where rel_tp='" & e.Item.Cells(0).Text.ToString & "' and rel_cd='" & e.Item.Cells(1).Text.ToString & "' and itm_cd='" & e.Item.Cells(2).Text.ToString & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()

        cmdDeleteItems.Dispose()
        conn.Close()

        Response.Redirect("Related_Maint.aspx")

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
