<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="CommRate.aspx.vb" Inherits="CommRate"    %>
<%--  CodeFile="SRRTD.aspx.vb" Inherits="SRRTD" meta:resourcekey="PageResource1" UICulture="auto" %>--%>
<%--
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>  --%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <table> 
             <tr>
            <td align="left">
              <table>
                    <tr>
                        <td align="left">

                            <asp:Label runat="server" Text="Current/Expired " ID="Label7" Width="160px"></asp:Label>
                           
                        </td>

                        <td>
                            <asp:DropDownList ID="DropDownDt" runat="server"></asp:DropDownList>
                        </td>

                         
                          
                  </tr>
             </table>
            </td>
          </tr>

  
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">
                         <asp:Label runat="server" Text="Product Group/Major  " ID="lbl_w_d"  Width="160px" ></asp:Label>
                        </td>
                        <td>
                           <asp:textbox ID="Text_prod_grp" runat="server"      AutoPostBack="True"  Width="60px"></asp:textbox>
                         </td>
                     </tr>
                </table>
            </td>
        </tr>
                  
          <tr>
            <td align="left">
              <table>
                    <tr>
                        <td align="left">

                            <asp:Label runat="server" Text="Price Type" ID="Label4" Width="160px"></asp:Label>
                           
                        </td>

                        <td>
                            <asp:DropDownList ID="DropDown_table" runat="server"></asp:DropDownList>
                        </td>

                        <td >
                            <asp:CheckBox ID="CheckBox_end_dt" runat="server" Text="Search end_date as 12/31/2049 only" Visible="False" ></asp:CheckBox>

                        </td>
                          
                  </tr>
             </table>
            </td>
          </tr>

        <tr>
            <td align="left">
                <table>
         <tr>
                        <td align="left">
                         <asp:Label runat="server" Text="Minimum Percent  " ID="Label6"  Width="160px"   ></asp:Label>
                        </td>
         <td>
         
             <asp:TextBox ID="text_min_pcnt" runat="server" AutoPostBack="True"  Width="40px" DataFormatString="{0:0.00}"    ></asp:TextBox>
       </td>
 </tr>
    </td>
                          
                  </tr>
             </table>
         <tr>

            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Start Date" ID="Label1" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             
                        <asp:Label ID="Label5" runat="server" style="text-align: start" Text="" ></asp:Label>
               <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="can_eff_date" DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       
         <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="End Date" ID="Label2" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             
                        <asp:Label ID="Label3" runat="server" style="text-align: start" Text="" ></asp:Label>
               <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="can_end_date" DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       
         
        <tr>
            <td align="right">
                <table>
                    
                                   

                    <tr align="right">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_search" runat="server" Text="Search" onclick="click_btn_search" Width="60px"/> </asp:Button>
                                         
                                    </td>
                                    <td>
                                         <asp:Button ID="btn_clr_screen" runat="server" Text="Clear" onclick="btn_clr_screen_Click" Width="60px"/> </asp:Button>
                                        
                                    </td>
                                    <td  align="right">

                                 <asp:Button ID="Btn_new" runat="server" Text="Insert" onclick="click_btn_insert" Width="60px"/> </asp:Button>        

                        </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    
        
            <tr>
            <td>
                <asp:DataGrid ID="GV_SRR" runat="server"  Width="680px" BorderColor="Black"  runat="server" AutoPostBack="true"
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="rowid"   OnDeleteCommand="DoItemDelete"
                   Height="16px" AlternatingItemStyle-BackColor="Beige"  PagerStyle-NextPageText=" " PagerStyle-PrevPageText=" " CaptionAlign="Top" Captioncolor="RED">            
                    
               
                <Columns>
                                      
                
                  <asp:BoundColumn DataField="rowid" HeaderText="Rowid" Visible="False"   ReadOnly="True" /> 
                   <asp:BoundColumn DataField="prod_grp" HeaderText="Prod_grp"    ReadOnly="True" Visible="true" />
                  
                  <asp:BoundColumn DataField="min_percent" HeaderText="Min Percent"    DataFormatString="{0:0.00}" ReadOnly="True" Visible="False" />
                   
                  <asp:BoundColumn DataField="start_date" HeaderText="Start_date"             ReadOnly="false" Visible="False" />   
                     <asp:BoundColumn DataField="End_date" HeaderText="End_date"             ReadOnly="false" Visible="False" />   
                        
                   
                     
                    <asp:TemplateColumn HeaderText="Min_percent" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_min_pcnt"  runat="server" AutoPostBack="true" DataFormatString="{0:0.00}"   OnTextChanged="txt_min_pcnt_textchanged"  Width="60px"></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                     <asp:TemplateColumn HeaderText="Start_date (mm/dd/yyyy)" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_start_date"  runat="server" AutoPostBack="true"    OnTextChanged="txt_start_date_TextChanged"   ></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                     <asp:TemplateColumn HeaderText="End_date (mm/dd/yyyy)" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_end_date"  runat="server" AutoPostBack="true"    OnTextChanged="txt_end_date_TextChanged" ></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                         
  
                
                       <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="<%$ Resources:LibResources, Label150 %>">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>

                </Columns>

    </asp:DataGrid>
                
                </td>
                <td>
                    <asp:Button ID="btn_update" runat="server" Text="Update" OnClick="click_btn_update" Width="60px" />
                  
                    </td>
       </tr>
       
    </table>
     <asp:Label runat="server"   ID="lbl_msg"  Width="100%" ForeColor="Red"></asp:Label>
     <asp:Label runat="server"   ID="lbl_warning"  Width="100%" ForeColor="Red"></asp:Label>
  <%-- <dx:aspxlabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:aspxlabel> --%>
    <br />
    <br />
    
    <div runat="server" id="div_version">
          
        <asp:label ID="lbl_versionInfo" runat="server" >
       &nbsp;&nbsp;&nbsp; </asp:label>  
    </div>
</asp:Content>   
