<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WGC_Reconcile.aspx.vb" Inherits="WGC_Reconcile"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div width="100%" align="center">
        <br />
        &nbsp;<br />
        <b>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Gift Card Reconciliation">
            </dx:ASPxLabel>
        </b>
        <table>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Company Code:">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_co_cd" runat="server" Width="34px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Dates:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="cbo_start_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text=" through ">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="cbo_end_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Card Number:">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Run Report" OnClick="btnSubmit_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_post_gl" runat="server" Text="Post to GL" Enabled="False" OnClick="btn_post_gl_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_response" runat="server" Width="100%">
                    </dx:ASPxLabel>
                    &nbsp;&nbsp;<dx:ASPxGridView ID="DataGrid1" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <SettingsPager Mode="ShowAllRecords" Visible="False">
                        </SettingsPager>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="POST_DT" VisibleIndex="0" Caption="Post Date">
                                <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="IVC_CD" VisibleIndex="1" Caption="Sales Order">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ACCT_NUM" VisibleIndex="2" Caption="Account Number">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="AMT" VisibleIndex="3" Caption="Amount">
                                <HeaderStyle HorizontalAlign="Right" />
                                <PropertiesTextEdit DisplayFormatString="{0:c2}">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Original Amount" VisibleIndex="4">
                                <DataItemTemplate>
                                    <dx:ASPxLabel ID="lbl_original" runat="server" Text="">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" />
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Original Transaction" VisibleIndex="4">
                                <DataItemTemplate>
                                    <dx:ASPxLabel ID="lbl_original_tran" runat="server" Text="">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" />
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Width="100%" Text="To be posted:" Font-Bold="True">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Width="45%" Text="1335-00 (GCA): ">
                    </dx:ASPxLabel><dx:ASPxLabel ID="lbl_1335" runat="server" Width="45%" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Width="45%" Text="1336-00 (GCD): ">
                    </dx:ASPxLabel><dx:ASPxLabel ID="lbl_1336" runat="server" Width="45%" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Width="45%" Text="1337-00 (GCP): ">
                    </dx:ASPxLabel><dx:ASPxLabel ID="lbl_1337" runat="server" Width="45%" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Width="45%" Text="1338-00 (Purchased): ">
                    </dx:ASPxLabel><dx:ASPxLabel ID="lbl_1338" runat="server" Width="45%" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Width="100%" Text="Posted GL Transactions">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%">
                        <SettingsPager Mode="ShowAllRecords" Visible="False">
                        </SettingsPager>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="GL_ACCT_CD" VisibleIndex="1" Caption="GL Account Code">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DC_CD" VisibleIndex="2" Caption="Debit/Credit">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="AMT" VisibleIndex="3" Caption="Amount">
                                <PropertiesTextEdit DisplayFormatString="{0:c2}">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsText EmptyDataRow="No Transactions Found" />
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
