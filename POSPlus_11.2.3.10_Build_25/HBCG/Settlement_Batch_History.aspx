<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement_Batch_History.aspx.vb" Inherits="Settlement_Batch_History" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td height="100%" align="left" valign="middle">
                    <div style="padding-left: 18px; padding-top: 10px" id="main_body" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="95%" class="style5">
                            <tr>
                                <td align="left" valign="middle" style="height: 13px">
                                    <b>
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Batch History">
                                        </dx:ASPxLabel>
                                    </b>
                                </td>
                                <td align="right" style="height: 13px">
                                    &nbsp;</td>
                            </tr>
                        </table>
                        <hr />
                        <br />
                        <table border="0" cellpadding="0" cellspacing="0" width="95%" class="style5">
                            <tr>
                                <td align="left" valign="top">
                                    &nbsp;<dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Select Batch Date:">
                                    </dx:ASPxLabel>
                                </td>
                                <td align="left" valign="top" style="width: 602px">
                                    <dx:ASPxDateEdit ID="WebDateChooser1" runat="server">
                                    </dx:ASPxDateEdit>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 111px">
                                    &nbsp;<dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Select Store Code:">
                                    </dx:ASPxLabel>
                                </td>
                                <td align="left" valign="top" style="width: 602px">
                                    <dx:ASPxComboBox ID="store_cd" runat="server" AutoPostBack="True" IncrementalFilteringMode ="StartsWith">
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    &nbsp;<dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Order Type:">
                                    </dx:ASPxLabel>
                                </td>
                                <td align="left" valign="top">
                                    <dx:ASPxComboBox ID="cbo_trn_tp" runat="server" AutoPostBack="True" 
                                        OnSelectedIndexChanged="cbo_trn_tp_SelectedIndexChanged" IncrementalFilteringMode ="StartsWith">
                                        <Items>
                                            <dx:ListEditItem Selected="True" Text="All Transactions" Value="ALL" />
                                            <dx:ListEditItem Text="Finance Transactions" Value="GDF" />
                                            <dx:ListEditItem Text="Down Payments" Value="GDR" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        <tr>
            <td align="right" valign="top" colspan="2">
                &nbsp; &nbsp; &nbsp;
                <dx:ASPxLabel ID="Label2" runat="server" Text="Financed Totals:" Width="154px">
                </dx:ASPxLabel>
                &nbsp; &nbsp;&nbsp; &nbsp;<dx:ASPxLabel ID="lbl_gdf" runat="server" Text="$ 0.00">
                </dx:ASPxLabel>
                <br />
                &nbsp; &nbsp; &nbsp;
                <dx:ASPxLabel ID="Label1" runat="server" Text="Down Payment Totals:" Width="154px">
                </dx:ASPxLabel>
                &nbsp; &nbsp;&nbsp; &nbsp;<dx:ASPxLabel ID="lbl_gdr" runat="server" Text="$ 0.00">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
                &nbsp;
                <dx:ASPxLabel ID="lbl_grid_msg" runat="server" Text="" Width="100%">
                </dx:ASPxLabel>
                <asp:DataGrid ID="Gridview1" OnSortCommand="MyDataGrid_Sort" AllowSorting="True"
                    runat="server" AutoGenerateColumns="False" CssClass="style5" Height="200px" Width="100%">
                    <Columns>
                        <asp:BoundColumn DataField="WR_DT" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
                            <HeaderStyle Height="10px" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CUST_CD" HeaderText="Bill to Name" SortExpression="CUST_CD">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BNK_CRD_NUM" HeaderText="Account Number" SortExpression="BNK_CRD_NUM">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="STORE_CD" HeaderText="Store" SortExpression="STORE_CD"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Amount" DataField="AMT" DataFormatString="{0:C2}" SortExpression="AMT">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APP_CD" HeaderText="Auth Code" SortExpression="APP_CD"></asp:BoundColumn>
                        <asp:BoundColumn DataField="REF_NUM" HeaderText="Plan Code" SortExpression="REF_NUM">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Type" DataField="MEDIUM_TP_CD" SortExpression="MEDIUM_TP_CD">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DEL_DOC_NUM" HeaderText="Sales Order" SortExpression="DEL_DOC_NUM">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Released"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Confirmed"></asp:BoundColumn>
                    </Columns>
                    <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                    <EditItemStyle CssClass="style5" />
                    <ItemStyle CssClass="style5" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                    <SelectedItemStyle BackColor="Silver" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" />
                </asp:DataGrid><br />
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" style="height: 19px">
                <asp:Label ID="lbl_header" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
