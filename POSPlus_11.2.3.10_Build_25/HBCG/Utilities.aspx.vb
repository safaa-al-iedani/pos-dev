
Partial Class Utilities
    Inherits POSBasePage

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim str_previousPageURL As String = Convert.ToString(Request.UrlReferrer)

        If InStr(str_previousPageURL, "CashDrawerBalancing.aspx") > 0 Then  'lucy to prevent user use utiltities, 23-Jul-15
            Response.Redirect("CashDrawerBalancing.aspx")

        End If

        If (Not IsPostBack) Then
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim previousPageURL As String = Convert.ToString(Request.UrlReferrer)
                If Not String.IsNullOrWhiteSpace(previousPageURL) Then
                    ' For stale data issue
                    ' Need to perform the action when user navigates back to Home page from SOM+, SORW+ page            
                    Dim pageName As String = String.Empty
                    pageName = System.IO.Path.GetFileName(previousPageURL)

                    Dim accessInfo As New AccessControlBiz()
                    accessInfo.DeleteRecordsforPage(pageName)
                End If
            End If
        End If
    End Sub
End Class
