<%@ Page Language="VB" AutoEventWireup="false" CodeFile="marketing.aspx.vb" Inherits="discount"
    MasterPageFile="~/Regular.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div>
            Please select a marketing code, or select the<br />
            "No Marketing Code" option below<br />
            <br />
            <asp:DropDownList ID="mark_cd" runat="server" AutoPostBack="True" Width="270px" AppendDataBoundItems="True"
                DataTextField="DES" DataValueField="MKT_CAT" CssClass="style5">
            </asp:DropDownList>
            <br />
            <asp:DropDownList ID="mark_sub_cat" runat="server" Width="270px" AutoPostBack="True" DataTextField="DES" Enabled="False" CssClass="style5">
            </asp:DropDownList><br />
            <asp:Label ID="lblMark" runat="server" Width="271px"></asp:Label>&nbsp;<br />
            <asp:DataGrid ID="Gridview1" Style="position: relative;" runat="server" 
            AutoGenerateColumns="False" CellPadding="0" DataKeyField="mkt_Cd" Height="80px" Width="514px" OnDeleteCommand="DoItemDelete">
            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
            <HeaderStyle Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="True" VerticalAlign="Top"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="mkt_cat" HeaderText="Category" ReadOnly="True" Visible="False">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="mkt_cd" HeaderText="Code" ReadOnly="True" Visible="False">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="mkt_des" HeaderText="Marketing Category" ReadOnly="True">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="des" HeaderText="Marketing Code" ReadOnly="True">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"></asp:ButtonColumn>
            </Columns>
        </asp:DataGrid>
            <br />
            <asp:CheckBox ID="ck_No_Mark" runat="server" AutoPostBack="True" Checked="True" CssClass="style5" /> No Marketing Code
        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
<br />
<br />
<br />
</asp:Content>