Imports System.Data.OracleClient

Partial Class CPHQ
    Inherits POSBasePage

    Dim running_balance As Double = 0
    Dim quote_balance As Double = 0
    Dim theSalesBiz As SalesBiz = New SalesBiz()



    Function SortOrder2(ByVal Field As String) As String

        Dim so As String = Session("SortOrder2")

        If Field = so Then
            SortOrder2 = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder2 = Replace(Field, "desc", "asc")
        Else
            SortOrder2 = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder2") = SortOrder2

    End Function

    Sub MyDataGrid2_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        '  GridView2.CurrentPageIndex = 0 'To sort from top
        '  GridView1_Binddata(SortOrder2(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Public Sub GridView1_Binddata(ByVal SortField As String)

        '  txt_quote_summary.Text = "0.00"
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim Source As DataView
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        ds = New DataSet
        Dim emp_init As String = Session("emp_init")
        Dim FOLLOW_UP_DT As String = ""

        '   GridView2.DataSource = ""
        'CustTable.Visible = False
        '   GridView2.Visible = True

        sql = "SELECT SALES_ORDER, REFERENCE,ORD_TP,FINAL_DATE, ITM_CD,QTY,UNIT_PRC, OUT_ASIS,WTY FROM cust_buy_hst WHERE "
        sql = sql & "and CUST_CD = '" & txt_Cust_cd.Text & "' ORDER BY sales_order"

        conn.Open()

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim MyTable As DataTable
        Dim numrows As Integer
        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Assign sort expression to Session              
        Session("SortOrder2") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Source.Sort = SortField
                ' GridView2.DataSource = Source
                ' GridView2.DataBind()
            Else
                ' GridView2.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub Get_Comments()

        If Not String.IsNullOrWhiteSpace(txt_Cust_cd.Text) Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim cust_cd As String
            Dim curr_dt As String = ""
            txt_cust_past.Text = ""

            conn = DisposablesManager.BuildOracleConnection

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            cust_cd = txt_Cust_cd.Text
            'Daniela remove < character
            'sql = "SELECT SEQ#, CMNT_DT, TEXT FROM CUST_CMNT WHERE CUST_CD=:CUST_CD ORDER BY SEQ#"
            sql = "SELECT SEQ#, CMNT_DT, translate(TEXT,'<', '-') TEXT FROM CUST_CMNT WHERE CUST_CD=:CUST_CD ORDER BY SEQ#"

            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
            objSql.Parameters(":CUST_CD").Value = cust_cd
            curr_dt = ""

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    curr_dt = MyDataReader.Item("CMNT_DT").ToString
                    txt_cust_past.Text = FormatDateTime(curr_dt, DateFormat.ShortDate) & " - " & txt_cust_past.Text
                    txt_cust_past.Text = txt_cust_past.Text & MyDataReader.Item("TEXT").ToString
                End If

                Do While (MyDataReader.Read())
                    If curr_dt <> MyDataReader.Item("CMNT_DT").ToString Then

                        curr_dt = MyDataReader.Item("CMNT_DT").ToString
                        txt_cust_past.Text = txt_cust_past.Text & vbCrLf & FormatDateTime(MyDataReader.Item("CMNT_DT").ToString, DateFormat.ShortDate) & " - " & MyDataReader.Item("TEXT").ToString
                    Else
                        txt_cust_past.Text = txt_cust_past.Text & MyDataReader.Item("TEXT").ToString
                    End If
                Loop

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw

            Finally
                conn.Close()
            End Try
        End If
    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        DataGrid1.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter

        objConnect = DisposablesManager.BuildOracleConnection

        Dim co_cd = Session("co_cd")
        Dim co_grp_cd = Session("str_co_grp_cd")

        ' Franchise changes

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'Daniela prevent SOM+ crash for missing so record

        sql = "SELECT SALES_ORDER, REFERENCE,ORD_TP,FINAL_DATE, ITM_CD,QTY,UNIT_PRC, OUT_ASIS,WTY FROM cust_buy_hst WHERE "
        sql = sql & "CUST_CD = '" & txt_Cust_cd.Text & "' ORDER BY sales_order"

        ' sql = "select a.ar_trn_pk, decode(so.del_doc_num, null, NVL(a.ivc_cd, a.adj_ivc_cd) || '#', NVL(a.ivc_cd, a.adj_ivc_cd)) as ivc_cd, "
        ' sql = sql & "a.post_dt, a.trn_tp_cd, a.mop_cd, a.stat_cd, "
        ' sql = sql & "nvl(DECODE(T.DC_CD,'C',-A.AMT,'D',A.AMT,0),0) AS AMT "
        ' sql = sql & "from "
        'sql = sql & "ar_trn_tp t "
        'sql = sql & ", ar_trn a , so "
        'sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        'sql = sql & "and a.cust_cd='" & txt_Cust_cd.Text & "' and NVL(a.ivc_cd, a.adj_ivc_cd) = so.del_doc_num(+) "

        '  sql = sql & "and a.co_cd in "
        '  If co_grp_cd = "LEO" Then
        'sql = sql & " ('" & co_cd & "') "
        ' Else
        ' sql = sql & " (select co_cd from co_grp where co_grp_cd= '" & co_grp_cd & "') "
        '  End If

        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows = 0 Then
            DataGrid1.Visible = False
            lbl_Cust_Info.Visible = True
            lbl_Cust_Info.Text = "No records were found matching your search criteria." & vbCrLf
        Else
            Source.Sort = SortField
            DataGrid1.DataSource = Source
            DataGrid1.DataBind()
            lbl_Cust_Info.Visible = False
        End If
        'Close connection           
        objConnect.Close()
        ' txt_acct_balance.Text = FormatCurrency(running_balance, 2)

    End Sub


    Private Function cust_srt_cd(ByRef p_srt_cd As String) As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String

        Dim str_des As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        sql = "select des FROM cust_srt WHERE srt_cd='" & p_srt_cd & "'"
        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                str_des = MyDataReader.Item("DES").ToString

            End If
            MyDataReader.Close()

            Return str_des
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()


    End Function


    Protected Sub Populate_Results()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT * FROM CUST WHERE CUST_CD=:CUST_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
        objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                txt_Cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                cbo_cust_tp.SelectedValue = MyDataReader.Item("CUST_TP_CD").ToString
                txt_corp_name.Text = MyDataReader.Item("CORP_NAME").ToString
                txt_cust_tp.Text = MyDataReader.Item("CUST_TP_CD").ToString
                If txt_cust_tp.Text = "C" Then
                    txt_tp_nm.Text = "Corporation"
                Else
                    txt_tp_nm.Text = "Individual"
                End If
                txt_alt_cust_cd.Text = MyDataReader.Item("alt_cust_cd").ToString
                txt_Fname.Text = MyDataReader.Item("FNAME").ToString
                txt_Lname.Text = MyDataReader.Item("LNAME").ToString
                txt_addr1.Text = MyDataReader.Item("ADDR1").ToString
                txt_addr2.Text = MyDataReader.Item("ADDR2").ToString
                txt_City.Text = MyDataReader.Item("CITY").ToString
                txt_prv.Text = MyDataReader.Item("ST_CD").ToString
                txt_Zip.Text = MyDataReader.Item("ZIP_CD").ToString
                txt_county.Text = MyDataReader.Item("COUNTY").ToString
                txt_country.Text = MyDataReader.Item("COUNTRY").ToString
                txt_h_phone.Text = MyDataReader.Item("HOME_PHONE").ToString
                txt_b_phone.Text = MyDataReader.Item("BUS_PHONE").ToString
                txt_Ext.Text = MyDataReader.Item("EXT").ToString
                txt_Email.Text = MyDataReader.Item("EMAIL_ADDR").ToString
                '      txt_tax_exempt_cd.Text = MyDataReader.Item("TET_ID#").ToString
                '    cbo_Zone_cd.SelectedValue = MyDataReader.Item("ZONE_CD").ToString
                '    cbo_se_zone.SelectedValue = MyDataReader.Item("SE_ZONE_CD").ToString
                '   cbo_srt_cd.SelectedValue = MyDataReader.Item("SRT_CD").ToString
                '  cbo_tet_cd.SelectedValue = MyDataReader.Item("TET_CD").ToString
                '  cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString

                txt_tax_exempt.Text = MyDataReader.Item("TET_ID#").ToString   'lucy
                ' ASPxLabel20.Text = MyDataReader.Item("SRT_CD").ToString
                txt_srt_cd.Text = MyDataReader.Item("SRT_CD").ToString
                If Not isEmpty(txt_srt_cd.Text) Then
                    txt_srt_desc.Text = cust_srt_cd(txt_srt_cd.Text)
                End If
                'For a retrieved customer, the  type should never be allowed to be changed, so disable the drop-down. 
                ' However, if the type  'Corporate', then  and the the corp. field should be editable so user cannot change it.
                cbo_cust_tp.Enabled = False
                If (cbo_cust_tp.SelectedValue = "I") Then
                    txt_alt_cust_cd.Enabled = False
                Else
                    txt_alt_cust_cd.Enabled = True
                End If

                lbl_Cust_Info.Text = ""
                lbl_summary.Text = "<table width=""100%""><tr>"
                'lbl_summary.Text = lbl_summary.Text & "<td>First visit: </td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("ACCT_OPN_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<td>" & Resources.LibResources.Label755 & ": </td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("ACCT_OPN_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"

                MyDataReader.Close()

                sql = "SELECT POST_DT FROM AR_TRN WHERE CUST_CD=:CUST_CD AND CO_CD=:CO_CD GROUP BY POST_DT ORDER BY POST_DT DESC"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("co_cd"))

                Dim x As Integer = 1

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read
                    If x = 1 Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label756 & ":</td><td align=""right""><font color=""darkorange"">" & FormatDateTime(MyDataReader.Item("POST_DT").ToString, DateFormat.ShortDate) & "</font></td></tr>"
                    End If
                    x = x + 1
                Loop
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label757 & ":</td><td align=""right""><font color=""darkorange"">" & vbTab & x & "</font></td></tr>"

                MyDataReader.Close()

                sql = "SELECT SUM(DECODE(AR_TRN_TP.DC_CD,'C',-AR_TRN.AMT,AR_TRN.AMT)) AS AMT "
                sql = sql & "FROM AR_TRN, AR_TRN_TP "
                sql = sql & "WHERE AR_TRN.TRN_TP_CD=AR_TRN_TP.TRN_TP_CD "
                sql = sql & "AND AR_TRN.CUST_CD=:CUST_CD "
                sql = sql & "AND AR_TRN.CO_CD=:CO_CD "
                sql = sql & "AND AR_TRN.TRN_TP_CD IN('SAL','CRM','MCR','MDB') "
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("co_cd"))

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    If IsNumeric(MyDataReader.Item("AMT").ToString) Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label758 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(MyDataReader.Item("AMT").ToString, 2) & "</font></td></tr>"
                    Else
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label758 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(0, 2) & "</font></td></tr>"
                    End If
                End If
                MyDataReader.Close()

                sql = "SELECT SUM(DECODE(AR_TRN_TP.DC_CD,'C',-AR_TRN.AMT,AR_TRN.AMT)) AS AMT "
                sql = sql & "FROM AR_TRN, AR_TRN_TP "
                sql = sql & "WHERE AR_TRN.TRN_TP_CD=AR_TRN_TP.TRN_TP_CD "
                sql = sql & "AND AR_TRN.CO_CD=:CO_CD "
                sql = sql & "AND AR_TRN.CUST_CD=:CUST_CD "
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters(":CUST_CD").Value = UCase(Request("cust_cd"))
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters(":CO_CD").Value = UCase(Session("co_cd"))

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    If IsNumeric(MyDataReader.Item("AMT").ToString) Then
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label44 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(MyDataReader.Item("AMT").ToString, 2) & "</font></td></tr>"
                    Else
                        lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label44 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(0, 2) & "</font></td></tr>"
                    End If
                End If
                lbl_summary.Text = lbl_summary.Text & "<tr><td colspan=""2""><hr /></td></tr>"

                MyDataReader.Close()

                conn.Close()
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()

                sql = "SELECT CONVERSION_DT, PROSPECT_TOTAL FROM RELATIONSHIP WHERE CUST_CD='" & Request("cust_cd") & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                x = 0
                Dim y As Integer = 0
                Dim z As Integer = 0

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read
                    If IsNumeric(MyDataReader.Item("PROSPECT_TOTAL").ToString) Then
                        y = y + CDbl(MyDataReader.Item("PROSPECT_TOTAL").ToString)
                        If IsDate(MyDataReader.Item("CONVERSION_DT").ToString) Then
                            z = z + CDbl(MyDataReader.Item("PROSPECT_TOTAL").ToString)
                        End If
                    End If
                    x = x + 1
                Loop
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label760 & ":</td><td align=""right""><font color=""darkorange"">" & x & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label761 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(y, 2) & "</font></td></tr>"
                lbl_summary.Text = lbl_summary.Text & "<tr><td>" & Resources.LibResources.Label762 & ":</td><td align=""right""><font color=""darkorange"">" & FormatCurrency(z, 2) & "</font></td></tr>"

                MyDataReader.Close()

                lbl_summary.Text = lbl_summary.Text & "</table>"

            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        txt_Cust_cd.Enabled = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str_from As String = Session("from_customerinqiry")

        If Not isEmpty(Session("sht_cust_cd")) Then
            txt_Cust_cd.Text = Session("sht_cust_cd")
            Session("sht_cust_cd") = ""

            lucy_btn_Lookup_Click()
         
        End If
        If Not IsPostBack Then
            If Request("cust_cd") & "" <> "" Then
                '   If Not isEmpty(str_from) & "" <> "" & str_from = "Y" Then
                If Session("from_customerinqiry") = "Y" Then
                    '  txt_Cust_cd.Text = Request("cust_cd")
                    If isEmpty(txt_Cust_cd.Text) Then

                        txt_Cust_cd.Text = Request("cust_cd")

                        lucy_btn_Lookup_Click()
                        Session("from_customerinqiry") = "N"

                    End If
                End If


            End If


            Dim dsStates As DataSet = Nothing
            dsStates = HBCG_Utils.GetStates()
            With cbo_State
                .DataSource = dsStates
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            cbo_State.Items.Insert(0, " ")
            cbo_State.Items.FindByText(" ").Value = ""
            '     zone_cd_populate()
            '   se_zone_cd_populate()
            '  cust_srt_populate()
            ' PopulateTaxExempt()

            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then
                Populate_Results()
                btn_Lookup.Enabled = False
            Else
                '   cbo_Zone_cd.Enabled = False
                '   cbo_se_zone.Enabled = False
                '    cbo_srt_cd.Enabled = False
                '   cbo_tet_cd.Enabled = False
                '   txt_tax_exempt_cd.Enabled = False
                lbl_Cust_Info.Text = ""
            End If



        End If


    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_Cust_cd.Enabled = True
        txt_Cust_cd.Text = ""
        cbo_cust_tp.SelectedValue = "I"
        txt_alt_cust_cd.Text = ""
        txt_Fname.Text = ""
        txt_Lname.Text = ""
        txt_addr1.Text = ""
        txt_addr2.Text = ""
        txt_City.Text = ""
        cbo_State.SelectedValue = ""
        txt_Zip.Text = ""
        txt_county.Text = ""
        txt_country.Text = ""
        txt_h_phone.Text = ""
        txt_b_phone.Text = ""
        txt_Ext.Text = ""
        txt_Email.Text = ""
        '   cbo_Zone_cd.SelectedValue = ""
        '  cbo_se_zone.SelectedValue = ""
        '  cbo_srt_cd.SelectedValue = ""
        '   cbo_tet_cd.SelectedValue = ""
        '   txt_tax_exempt_cd.Text = ""
        lbl_Cust_Info.Text = ""
        txt_alt_cust_cd.Enabled = True

        Response.Redirect("CPHQ.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        lbl_Cust_Info.Text = ""
        Response.Redirect("CPHQ_cust_Lookup.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text) & "&fname=" & HttpUtility.UrlEncode(txt_Fname.Text) _
            & "&lname=" & HttpUtility.UrlEncode(txt_Lname.Text) & "&addr1=" & HttpUtility.UrlEncode(txt_addr1.Text) & "&addr2=" & HttpUtility.UrlEncode(txt_addr2.Text) & "&city=" _
            & HttpUtility.UrlEncode(txt_City.Text) & "&state=" & HttpUtility.UrlEncode(cbo_State.SelectedValue.ToString) & "&zip=" & HttpUtility.UrlEncode(txt_Zip.Text) & "&hphone=" _
            & HttpUtility.UrlEncode(txt_h_phone.Text) & "&bphone=" & HttpUtility.UrlEncode(txt_b_phone.Text) & "&email=" & HttpUtility.UrlEncode(txt_Email.Text) & "&alt_cust_cd=" & HttpUtility.UrlEncode(txt_alt_cust_cd.Text))


    End Sub
    Protected Sub lucy_btn_Lookup_Click()

        Session("from_customerinqiry") = "N"
        lbl_Cust_Info.Text = ""
        Response.Redirect("CPHQ_cust_Lookup.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text) & "&fname=" & HttpUtility.UrlEncode(txt_Fname.Text) _
            & "&lname=" & HttpUtility.UrlEncode(txt_Lname.Text) & "&addr1=" & HttpUtility.UrlEncode(txt_addr1.Text) & "&addr2=" & HttpUtility.UrlEncode(txt_addr2.Text) & "&city=" _
            & HttpUtility.UrlEncode(txt_City.Text) & "&state=" & HttpUtility.UrlEncode(cbo_State.SelectedValue.ToString) & "&zip=" & HttpUtility.UrlEncode(txt_Zip.Text) & "&hphone=" _
            & HttpUtility.UrlEncode(txt_h_phone.Text) & "&bphone=" & HttpUtility.UrlEncode(txt_b_phone.Text) & "&email=" & HttpUtility.UrlEncode(txt_Email.Text) & "&alt_cust_cd=" & HttpUtility.UrlEncode(txt_alt_cust_cd.Text))


    End Sub

    Protected Sub txt_Zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If btn_Lookup.Enabled = False Then
            Update_City_State()
        End If

    End Sub

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(txt_Zip.Text) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            conn.Open()

            sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & txt_Zip.Text & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    txt_City.Text = MyDataReader.Item("CITY").ToString
                    cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If


    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged1(ByVal source As Object, ByVal e As DevExpress.Web.ASPxTabControl.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged
    'Protected Sub ASPxPageControl1_ActiveTabChanged1(ByVal source As Object, ByVal e As DevExpress.Web.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged




        If txt_Cust_cd.Text & "" <> "" Then
            If ASPxPageControl1.ActiveTabIndex = 1 Then
                Response.Redirect("SaleHst.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text))   ' link
                'Set up default column sorting   

            ElseIf ASPxPageControl1.ActiveTabIndex = 2 Then
                Get_Comments()
            ElseIf ASPxPageControl1.ActiveTabIndex = 3 Then
                '  Response.Redirect("CustomerInquiry.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text) & "&fname=" & HttpUtility.UrlEncode(txt_Fname.Text) _
                ' & "&lname=" & HttpUtility.UrlEncode(txt_Lname.Text))
                Session("from_CPHQ") = "Y"
                Response.Redirect("CustomerInquiry.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text))
                '  Response.Redirect("CustInq.aspx?cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text))
            ElseIf ASPxPageControl1.ActiveTabIndex = 0 Then
                lucy_btn_Lookup_Click()
            Else
                If IsNothing(Session("SortOrder2")) Or IsDBNull(Session("SortOrder2")) Then
                    GridView1_Binddata("sales_order asc")
                Else
                    GridView1_Binddata(Session("SortOrder2"))
                End If
            End If
        End If
    End Sub

    Protected Sub lucy_page()

        If txt_Cust_cd.Text & "" <> "" Then

            'Set up default column sorting   
            If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                BindData("sales_order asc")
            Else
                BindData(Session("SortOrder"))
            End If

        End If
    End Sub




    Function print_sls_hst(ByRef p_prg_nm As String, ByRef p_printer_nm As String, ByRef p_cust_cd As String, ByRef p_y_n As String) As String
        Dim str_emp_cd As String = Session("emp_cd")
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_cust_pur_hst.print_cphq"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_prg_nm", OracleType.Char)).Value = p_prg_nm
        myCMD.Parameters.Add(New OracleParameter("p_spooler", OracleType.Char)).Value = p_printer_nm
        myCMD.Parameters.Add(New OracleParameter("p_session_id", OracleType.Char)).Value = p_cust_cd
        myCMD.Parameters.Add(New OracleParameter("p_emp_id", OracleType.Char)).Value = str_emp_cd
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 200).Direction = ParameterDirection.ReturnValue

        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try
            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_y_n = myCMD.Parameters("p_y_n").Value
            p_y_n = str_y_n

            objConnection.Close()
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
            str_y_n = "N"
            objConnection.Close()
        End Try


    End Function

    Private Function get_default_printer() As String
        Dim str_emp_cd As String = Session("emp_cd")
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select E.LPTQ_NM as PRINT_Q from  emp e   "
        ' sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.emp_cd = '" & str_emp_cd & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Protected Sub btn_print_sls_hst__Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_print_sls_hst.Click
        Dim str_emp_cd As String = Session("emp_cd")
        Dim str_printer_nm As String
        Dim str_y_n As String
        str_printer_nm = get_default_printer()
        print_sls_hst("CPHQ", str_printer_nm, txt_Cust_cd.Text, str_y_n)

    End Sub


    Protected Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound

        'lucy 


        If IsNumeric(e.Item.Cells(5).Text) Then
            Dim txt_qty As Label = CType(e.Item.FindControl("lbl_balance"), Label)
            txt_qty.Text = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
            running_balance = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
        End If
        If Len(e.Item.Cells(9).Text) > 10 Then
            Dim hpl_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink1"), HyperLink)
            Dim hp2_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink2"), HyperLink)
            If e.Item.Cells(2).Text = "SAL" Or e.Item.Cells(2).Text = "CRM" Or e.Item.Cells(2).Text = "MCR" Or e.Item.Cells(2).Text = "MDB" Then
                'Daniela fix SOM+ error
                If InStr(e.Item.Cells(9).Text, "#") > 0 Then
                    hpl_ivc.NavigateUrl = "#"
                Else

                    ' hpl_ivc.NavigateUrl = "~/so_comments.aspx?query_returned=Y&del_doc_num =" & e.Item.Cells(9).Text & "&Cust_cd =" & txt_Cust_cd.Text   'here
                    hpl_ivc.NavigateUrl = "~/soCmnt.aspx?query_returned=Y&del_doc_num =" & e.Item.Cells(9).Text
                    ' hp2_ivc.NavigateUrl = "~/SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(9).Text
                    Session("cphq_cust_cd") = txt_Cust_cd.Text
                End If
            Else
                hpl_ivc.NavigateUrl = "#"
                hp2_ivc.NavigateUrl = "#"
            End If
            hpl_ivc.Text = Replace(e.Item.Cells(9).Text, "#", "")
            hp2_ivc.Text = Replace(e.Item.Cells(9).Text, "#", "")
        End If

    End Sub

    Protected Sub btn_Save_Comments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_comments.Click
        ' saves customer comments

        Dim conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim CUST_CMNT_LENGTH As Integer = 70

        ' TODO - this logic needs to go into customer utility - fixes for date here that aren't in OrderStage and SOM+ - have to handle passing command for transaction
        ' do not attempt to save comments if no cust code 
        ' If txt_cust_comments.Text & "" <> "" And Not String.IsNullOrWhiteSpace(txt_Cust_cd.Text) Then
        If Not String.IsNullOrWhiteSpace(txt_Cust_cd.Text) Then
            Try
                conn.Open()

                Dim sql As String
                Dim objSql As OracleCommand
                Dim cust_cd As String = txt_Cust_cd.Text

                Dim lngSEQNUM As Integer = HBCG_Utils.Get_next_cust_cmnt_seq(cust_cd)
                '  Dim strComments As String = txt_cust_comments.Text.ToString
                Dim strComments As String
                Dim lngComments As Integer = Len(strComments)
                Dim startChar As Integer = 1
                Dim strMidCmnts As String
                Dim dateTime As String = Now  ' a single comment being split up must have the exact same date and time to be maintained as one comment on query

                If lngComments > 0 Then

                    Do Until startChar > lngComments
                        strMidCmnts = Mid(strComments, startChar, CUST_CMNT_LENGTH)
                        strMidCmnts = Replace(strMidCmnts, "'", "")

                        sql = "INSERT INTO CUST_CMNT (cust_cd, seq#, cmnt_dt, text, emp_cd_op) " &
                              "VALUES('" & cust_cd & "', " & lngSEQNUM & ", TO_DATE('" & dateTime & "', 'MM/DD/RRRR HH:MI:SS PM'), '" &
                              Replace(strMidCmnts, """", """""") & "', '" & Session("EMP_CD") & "')"

                        sql = UCase(sql)

                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql.ExecuteNonQuery()
                        objSql.Cancel()
                        objSql.Dispose()

                        startChar = startChar + CUST_CMNT_LENGTH
                        lngSEQNUM = lngSEQNUM + 1
                    Loop
                End If

            Catch ex As Exception
                'Throw ex
                'Daniela prevent yellow traingle
                lbl_Cust_Info.Text = "Invalid comments. Please validate and try again."
            Finally
                conn.Close()
            End Try

            '   txt_cust_comments.Text = ""
            txt_cust_past.Text = ""
            Get_Comments()

        Else
            lbl_Cust_Info.Text = "You must save the customer first."
        End If

    End Sub


End Class

