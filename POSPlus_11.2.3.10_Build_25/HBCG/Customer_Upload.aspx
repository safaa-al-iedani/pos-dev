<%@ Page Language="VB" MasterPageFile="~/NoWizardNoAjax.master" AutoEventWireup="false"
    CodeFile="Customer_Upload.aspx.vb" Inherits="Customer_Upload" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="800px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table style="width: 796px">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Excel File:">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        &nbsp;
                                        <dx:ASPxComboBox ID="cbo_excel_files" runat="server" Width="182px" AutoPostBack="True"
                                            IncrementalFilteringMode ="StartsWith">
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Excel Worksheet:">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        &nbsp;
                                        <dx:ASPxComboBox ID="cbo_excel_worksheet" runat="server" Width="182px"
                                            IncrementalFilteringMode ="StartsWith">
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                            <dx:ASPxButton ID="btn_preview" runat="server" OnClick="ASPxButton1_Click" Text="Preview">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxUploadControl ID="ASPxUploadControl1" runat="server" ShowProgressPanel="True"
                                ShowUploadButton="True" FileUploadMode="OnPageLoad">
                            </dx:ASPxUploadControl>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" Width="561px">
                                <Settings VerticalScrollBarStyle="Virtual" />
                                <SettingsPager Visible="False">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
        <HeaderTemplate>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Step 1: Upload File or Use Existing">
            </dx:ASPxLabel>
        </HeaderTemplate>
    </dx:ASPxRoundPanel>
    <br />
    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="800px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table width="600px">
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="GERS Field">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Excel Column">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Override Value">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="FIRST NAME:   *">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_fname" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_fname_override" runat="server" Width="170px" MaxLength="15">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="LAST NAME:   *">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_lname" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_lname_override" runat="server" Width="170px" MaxLength="20">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="ADDRESS 1:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_addr1" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_addr1_override" runat="server" Width="170px" MaxLength="30">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="ADDRESS 2:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_addr2" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_addr2_override" runat="server" Width="170px" MaxLength="30">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="CITY:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_city" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_city_override" runat="server" Width="170px" MaxLength="20">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="STATE:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_state" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_state_override" runat="server" Width="170px" MaxLength="2">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="ZIP CODE:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_zip" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_zip_override" runat="server" Width="170px" MaxLength="10">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="EMAIL ADDRESS:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_email" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_email_override" runat="server" Width="170px" MaxLength="60">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 37px">
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="HOME PHONE:">
                            </dx:ASPxLabel>
                        </td>
                        <td style="height: 37px">
                            <dx:ASPxComboBox ID="cbo_hphone" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td style="height: 37px">
                            <dx:ASPxTextBox ID="txt_hphone_override" runat="server" Width="170px" MaxLength="12">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="BUSINESS PHONE:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_bphone" runat="server" ValueType="System.String" Width="70px" SelectedIndex="0"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="NONE" Value="100" />
                                    <dx:ListEditItem Text="A" Value="0" />
                                    <dx:ListEditItem Text="B" Value="1" />
                                    <dx:ListEditItem Text="C" Value="2" />
                                    <dx:ListEditItem Text="D" Value="3" />
                                    <dx:ListEditItem Text="E" Value="4" />
                                    <dx:ListEditItem Text="F" Value="5" />
                                    <dx:ListEditItem Text="G" Value="6" />
                                    <dx:ListEditItem Text="H" Value="7" />
                                    <dx:ListEditItem Text="I" Value="8" />
                                    <dx:ListEditItem Text="J" Value="9" />
                                    <dx:ListEditItem Text="K" Value="10" />
                                    <dx:ListEditItem Text="L" Value="11" />
                                    <dx:ListEditItem Text="M" Value="12" />
                                    <dx:ListEditItem Text="N" Value="13" />
                                    <dx:ListEditItem Text="O" Value="14" />
                                    <dx:ListEditItem Text="P" Value="15" />
                                    <dx:ListEditItem Text="Q" Value="16" />
                                    <dx:ListEditItem Text="R" Value="17" />
                                    <dx:ListEditItem Text="S" Value="18" />
                                    <dx:ListEditItem Text="T" Value="19" />
                                    <dx:ListEditItem Text="U" Value="20" />
                                    <dx:ListEditItem Text="V" Value="21" />
                                    <dx:ListEditItem Text="W" Value="22" />
                                    <dx:ListEditItem Text="X" Value="23" />
                                    <dx:ListEditItem Text="Y" Value="24" />
                                    <dx:ListEditItem Text="Z" Value="25" />
                                    <dx:ListEditItem Text="AA" Value="26" />
                                    <dx:ListEditItem Text="AB" Value="27" />
                                    <dx:ListEditItem Text="AC" Value="28" />
                                    <dx:ListEditItem Text="AD" Value="29" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_bphone_override" runat="server" Width="170px" MaxLength="12">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table><dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="* REQUIRED FIELDS - MUST BE MAPPED" Font-Size="0.65em">
                </dx:ASPxLabel>
                <br />
                <br />
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Insert Customers">
                </dx:ASPxButton><dx:ASPxLabel ID="lbl_error" runat="server" ForeColor="Red">
                </dx:ASPxLabel>
                <br />
                <dx:ASPxMemo ID="txt_log" runat="server" Height="71px" Width="100%">
                </dx:ASPxMemo>
            </dx:PanelContent>
        </PanelCollection>
        <HeaderTemplate>
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Step 2: Align Fields">
            </dx:ASPxLabel>
        </HeaderTemplate>
    </dx:ASPxRoundPanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
