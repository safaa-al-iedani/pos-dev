<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="CashDrwrBal.aspx.vb" Inherits="CashDrwrBal" meta:resourcekey="PageResource1" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="text-align: center" >
        <tr>
         <td>
             <%--<b><u>--%>
            <asp:Label ID="ASPxLabel1" runat="server"  Text="<%$ Resources:LibResources, Label875 %>" Font-Bold="True" Font-Size="small"></asp:Label>
           <%--</u></b>&nbsp; &nbsp;--%>
         </td>
        </tr>
    </table>
    &nbsp;
    &nbsp;
    &nbsp;
        &nbsp;
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
          <tr>
            <td>
                <asp:Label ID="Label1" runat="server" width="110px" Font-Bold="true" style="text-align: start" Text="<%$ Resources:LibResources, Label876 %>" ></asp:Label>
                <asp:TextBox ID="txt_store_cd" MaxLength="2" Width="100px" style='text-transform:uppercase' runat="server"></asp:TextBox>
            </td>
           </tr>
        <tr>             
          <td>
            <asp:Label ID="Label2" Font-Bold="true"  runat="server" width="110px" style="text-align: start" Text="<%$ Resources:LibResources, Label877 %>" ></asp:Label>
            <%--<asp:TextBox ID="post_dt" type="date" runat="server"></asp:TextBox>--%>
                <asp:Calendar ID="post_dt" Font-Size="6pt" ForeColor="Black" Height="100px" Width="120px" runat="server"></asp:Calendar>
           </td>
        </tr>
        
            &nbsp;
            &nbsp;

         <tr>
            <td>
            <asp:Label ID="Label3" runat="server" width="110px" style="text-align: start"  Text="<%$ Resources:LibResources, Label878 %>" ></asp:Label>
            <asp:TextBox ID="last_upd_dt" readonly="true" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            <asp:Label ID="Label5" runat="server" width="110px" style="text-align: start"  Text="<%$ Resources:LibResources, Label879 %>" ></asp:Label>
            <asp:TextBox ID="last_upd_by" readyonly="true" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
            <asp:Label ID="Label4" runat="server" width="110px" style="text-align: start"  Text="<%$ Resources:LibResources, Label880 %>" ></asp:Label>
            <asp:TextBox ID="appr_by" readyonly="true" runat="server"></asp:TextBox>
            </td>
        </tr>
             
    </table>

    <br />
     <%--<dx:ASPxGridView ID="GridView2" runat="server" keyfieldname="MOP_DES" enablecallbacks="false" AutoGenerateColumns="False"  Width="98%" ClientInstanceName="GridView2" OnRowValidating="gridview2_validate" OnBatchUpdate="GridView2_BatchUpdate" >--%>
    <dx:ASPxGridView ID="GridView2" runat="server" keyfieldname="MOP_DES" enablecallbacks="false" AutoGenerateColumns="False"  Width="98%" ClientInstanceName="GridView2" OnBatchUpdate="GridView2_BatchUpdate" >     
    <SettingsEditing Mode="Batch" />
         <SettingsCommandButton>
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>"  ></UpdateButton>
        <CancelButton Text="<%$ Resources:LibResources, Label886 %>" ></CancelButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption=" " FieldName="MOP_CD" ReadOnly="true" Visible="FALSE" VisibleIndex="1"> </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption=" " FieldName="MOP_DES" ReadOnly="true" Visible="True" VisibleIndex="2"> </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label881 %>"  FieldName="CDR_AMT"  ReadOnly="true" VisibleIndex="3"> 
                          </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label882 %>"  FieldName="ACT_AMT" VisibleIndex="4"> 
                <PropertiesTextEdit >
                    <ValidationSettings>
                        <%--<RequiredField IsRequired="True" ErrorText="Required Field"/>--%>
                    <%--<RegularExpression ValidationExpression="[0-9]*\.?[0-9]*" ErrorText="Invalid Value"  />--%>
                        <RegularExpression ValidationExpression="^-?\d*\.{0,1}\d+$" ErrorText="Invalid Value"  />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
              
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label883 %>"  readonly="true" FieldName="DIFF_AMT" VisibleIndex="5"> </dx:GridViewDataTextColumn>
          
        </Columns>
        
        <SettingsBehavior AllowSort="False" />
              <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <%-- <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please press 'UPDATE' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>
    <br />
     <table>
         <tr>
             <td>

             </td>
         </tr>

     </table>
   
    <table>
        <tr>
           
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label887 %>" >
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
         
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label888 %>" >
                </dx:ASPxButton>
            </td>
           
            
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
