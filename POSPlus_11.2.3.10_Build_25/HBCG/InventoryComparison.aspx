<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InventoryComparison.aspx.vb"
    Inherits="InventoryComparison" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" Width="333px" DefaultButton="btn_add">
            Please type in a SKU or click the "Find" icon to use a broader search criteria
            <br />
            <br />
            <asp:TextBox ID="TextBox1" runat="server" Height="17px" CssClass="style5">
            </asp:TextBox>
            <asp:Button ID="btn_add" runat="server" Text="Add to Comparison" CssClass="style5" />
            <asp:HyperLink ID="hpl_find_merch" runat="server" NavigateUrl="order.aspx?referrer=InventoryComparison.aspx">
        <img src="images/icons/find.gif" style="width: 15px; height: 15px" border="0" alt="Lookup Merchandise" /></asp:HyperLink>
        </asp:Panel>
        <dx:ASPxLabel ID="lbl_error" runat="server" Text="">
        </dx:ASPxLabel>
    </div>
    <br />
    <div id="main_body" runat="server">
    </div>
</asp:Content>
