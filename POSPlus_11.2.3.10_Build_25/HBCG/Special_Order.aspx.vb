Imports System.IO
Imports System.Data.OracleClient


Partial Class Special_Order
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btn_save_exit.UniqueID
        If Not IsPostBack Then
            'Populate the item type drop down
            itm_tp_populate()

            'Populate the minor codes
            mnr_populate()
            MinorChanges()
            If ConfigurationManager.AppSettings("use_special_forms") = "Y" Then
                cbo_Special_Forms.Enabled = True
                cbo_Special_Forms.Visible = True
                cbo_Frames.Enabled = False
                cbo_Frames.Visible = False
                Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\")

                cbo_Special_Forms.Items.Clear()
                cbo_Special_Forms.Items.Insert(0, "Please Select A Frame")
                cbo_Special_Forms.Items.FindByText("Please Select A Frame").Value = ""
                cbo_Special_Forms.SelectedIndex = 0

                With cbo_Special_Forms
                    .DataSource = dirInfo.GetFiles("*.xls")
                    .DataValueField = "Name"
                    .DataTextField = "Name"
                    .DataBind()
                End With
            Else
                cbo_Special_Forms.Enabled = False
                cbo_Special_Forms.Visible = False
                cbo_Frames.Enabled = False
                cbo_Frames.Visible = False
            End If

        End If

    End Sub


    Public Sub itm_tp_populate()

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = "SELECT itm_tp_cd, des FROM itm_tp WHERE inventory = 'Y' order by des"
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_ITM_TP_CD
                .DataSource = ds
                .DataValueField = "itm_tp_cd"
                .DataTextField = "des"
                .DataBind()
                .SelectedValue = "INV"
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub mnr_populate()

        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As New DataSet

        Dim sql As String = "SELECT mnr_cd, inv_mnr.des, mnr_cd || ' - ' || inv_mnr.des || ' (' || inv_mjr.des || ')' as full_desc " &
                             "FROM inv_mnr, inv_mjr " &
                             "WHERE inv_mnr.mjr_cd = inv_mjr.mjr_cd " &
                             "AND inv_mjr.spec_ord_comm_cd IS NOT NULL " &
                             "ORDER BY inv_mnr.mnr_cd"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_minor
                .DataSource = ds
                .DataValueField = "mnr_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click

        If Request("LEAD") = "TRUE" Then
            Response.Redirect("Order.aspx?LEAD=TRUE")
        Else
            Response.Redirect("Order.aspx")
        End If

    End Sub


    Private Sub Add_SKU(Optional ByVal add_another As String = "NO")

        Dim itm_cd As String
        Dim itm_tp_cd As String
        Dim ve_cd As String
        Dim mnr_cd As String
        Dim VSN As String
        Dim DES As String
        Dim Cover As String
        Dim ret_prc As Double
        Dim prc1 As Double
        Dim grade As String
        Dim size As String
        Dim finish As String
        Dim comment As String
        Dim style As String
        Dim family As String
        Dim category As String
        Dim volume As Double
        Dim measure As Double
        Dim freight As Double
        Dim stop_time As Double
        Dim carpet As Double
        Dim warr As String
        Dim pad As Double
        Dim points As Double
        Dim treatable As String

        itm_cd = txt_SKU.Text.Trim
        itm_tp_cd = cbo_ITM_TP_CD.SelectedValue.Trim
        ve_cd = txt_Vendor.Text.Trim
        mnr_cd = cbo_minor.SelectedValue.Trim
        VSN = txtVSN.Text.Trim
        DES = txtDES.Text.Trim
        Cover = txtCover.Text.Trim
        If Not IsNumeric(txtRetail.Text.Trim) Then
            ret_prc = 0
        Else
            ret_prc = txtRetail.Text.Trim
        End If
        If Not IsNumeric(txtMFG.Text.Trim) Then
            prc1 = 0
        Else
            prc1 = txtMFG.Text.Trim
        End If
        grade = txtGrade.Text.Trim
        size = txtSize.Text.Trim
        finish = txtFinish.Text.Trim
        comment = txtComment.Text.Trim
        style = cboStyle.SelectedValue.Trim
        family = txtFamily.Text.Trim
        category = cbo_category.SelectedValue.Trim
        If Not IsNumeric(txtVolume.Text.Trim) Then
            volume = 0
        Else
            volume = txtVolume.Text.Trim
        End If
        If Not IsNumeric(txtMeasure.Text.Trim) Then
            measure = 0
        Else
            measure = txtMeasure.Text.Trim
        End If
        If Not IsNumeric(txtFreight.Text.Trim) Then
            freight = 0
        Else
            freight = txtFreight.Text.Trim
        End If
        If Not IsNumeric(txtStop.Text.Trim) Then
            stop_time = 1
        Else
            If CDbl(txtStop.Text.Trim) > 0 Then
                stop_time = txtStop.Text.Trim
            Else
                stop_time = 1
            End If
        End If
        If Not IsNumeric(txtCarpet.Text.Trim) Then
            carpet = 0
        Else
            carpet = txtCarpet.Text.Trim
        End If
        warr = cbo_Warr.SelectedValue.Trim
        If Not IsNumeric(txtPad.Text.Trim) Then
            pad = 0
        Else
            pad = txtPad.Text.Trim
        End If
        If Not IsNumeric(txtPoints.Text.Trim) Then
            points = 0
        Else
            points = txtPoints.Text.Trim()
        End If
        treatable = cboTreatable.SelectedValue.Trim

        If txt_Vendor.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a vendor code</font>"
            Exit Sub
        End If

        If txtVSN.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a vendor stock number</font>"
            Exit Sub
        End If

        If txtDES.Text & "" = "" Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a description</font>"
            Exit Sub
        End If

        If Not IsNumeric(txtRetail.Text) Then
            lbl_SKU_Info.Text = "<font color=red>You must enter a numeric retail price</font>"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDatareader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim Warnings_Popup As String = ""

        sql = "SELECT VE_CD FROM VE WHERE VE_CD='" & ve_cd & "'"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If Not MyDatareader.Read() Then
                lbl_SKU_Info.Text = "<font color=red>Invalid Vendor Code</font>"
                conn.Close()
                Exit Sub
            End If
        Catch
            conn.Close()
            Throw
        End Try

        Create_SKU(itm_cd)
        If lbl_SKU_Info.Text = "SKU Created" Then
            If VSN & "" = "" Then
                Warnings_Popup = Warnings_Popup & "Vendor Stock Number must be entered!<br />" & vbCrLf
            End If
            If ve_cd & "" = "" Then
                Warnings_Popup = Warnings_Popup & "Vendor must be entered!" & vbCrLf
            End If
            If Warnings_Popup & "" <> "" Then
                lbl_SKU_Info.Text = "<font color=red>" & Warnings_Popup & "</font>"
                Exit Sub
            End If

            'retrieve comm code for the minor 
            Dim commCode As String = GetCommCode(UCase(mnr_cd))

            sql = "INSERT INTO ITM (ITM_CD,INVENTORY, REPL_CST, LST_ACT_DT, CREATE_DT, COMM_CD, ITM_TP_CD, MNR_CD, STYLE_CD"
            sql = sql & ", FAMILY_CD, VSN, DES, COVER, VE_CD, RET_PRC, PRC1, VOL, STOP_TIME, AVAIL_PAD_DAYS, POINT_SIZE, FRAME_TP_ITM "
            sql = sql & ", GRADE, SIZ, FINISH, CAT_CD, WARRANTABLE, TREATABLE, DROP_CD, DROP_DT, SPEC_ORD_FLAG, PTAG_PRINT_QTY, "
            sql = sql & "SMR_PCT, RCV_LABEL_CD, SETUP_REQ, IN_CARTON, VSAL_QTY, CALC_AVAIL, ALLOW_CREATE_CUST_ORD_OPTION) "
            sql = sql & "VALUES (:ITM_CD, :INVENTORY, :REPL_CST, :LST_ACT_DT, :CREATE_DT, :COMM_CD, :ITM_TP_CD, :MNR_CD, :STYLE_CD"
            sql = sql & ", :FAMILY_CD, :VSN, :DES, :COVER, :VE_CD, :RET_PRC, :PRC1, :VOL, :STOP_TIME, :AVAIL_PAD_DAYS, :POINT_SIZE, :FRAME_TP_ITM "
            sql = sql & ", :GRADE, :SIZ, :FINISH, :CAT_CD, :WARRANTABLE, :TREATABLE, :DROP_CD, SYSDATE+:NUM_DAYS, 'Y', 1, "
            sql = sql & "1, 'WHS', 'N', 'N', 1, 'N', 'N') "

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":INVENTORY", OracleType.VarChar)
            objSql.Parameters.Add(":REPL_CST", OracleType.Number)
            objSql.Parameters.Add(":LST_ACT_DT", OracleType.DateTime)
            objSql.Parameters.Add(":CREATE_DT", OracleType.DateTime)
            objSql.Parameters.Add(":COMM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_TP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objSql.Parameters.Add(":STYLE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":FAMILY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":VSN", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":COVER", OracleType.VarChar)
            objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":RET_PRC", OracleType.Number)
            objSql.Parameters.Add(":PRC1", OracleType.Number)
            objSql.Parameters.Add(":VOL", OracleType.Number)
            objSql.Parameters.Add(":STOP_TIME", OracleType.Number)
            objSql.Parameters.Add(":AVAIL_PAD_DAYS", OracleType.Number)
            objSql.Parameters.Add(":POINT_SIZE", OracleType.Number)
            objSql.Parameters.Add(":FRAME_TP_ITM", OracleType.VarChar)
            objSql.Parameters.Add(":GRADE", OracleType.VarChar)
            objSql.Parameters.Add(":SIZ", OracleType.VarChar)
            objSql.Parameters.Add(":FINISH", OracleType.VarChar)
            objSql.Parameters.Add(":CAT_CD", OracleType.VarChar)
            objSql.Parameters.Add(":WARRANTABLE", OracleType.VarChar)
            objSql.Parameters.Add(":TREATABLE", OracleType.VarChar)
            objSql.Parameters.Add(":DROP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":NUM_DAYS", OracleType.Number)

            objSql.Parameters(":ITM_CD").Value = UCase(txt_SKU.Text)
            objSql.Parameters(":INVENTORY").Value = "Y"
            objSql.Parameters(":REPL_CST").Value = 0
            objSql.Parameters(":LST_ACT_DT").Value = FormatDateTime(Date.Today, DateFormat.ShortDate)
            objSql.Parameters(":CREATE_DT").Value = FormatDateTime(Date.Today, DateFormat.ShortDate)
            objSql.Parameters(":COMM_CD").Value = commCode
            objSql.Parameters(":ITM_TP_CD").Value = UCase(itm_tp_cd)
            objSql.Parameters(":MNR_CD").Value = UCase(mnr_cd)
            objSql.Parameters(":STYLE_CD").Value = UCase(style)
            objSql.Parameters(":FAMILY_CD").Value = UCase(family)
            objSql.Parameters(":VSN").Value = UCase(VSN)
            objSql.Parameters(":DES").Value = UCase(DES)
            objSql.Parameters(":COVER").Value = UCase(Cover)
            objSql.Parameters(":VE_CD").Value = UCase(ve_cd)
            objSql.Parameters(":RET_PRC").Value = ret_prc
            objSql.Parameters(":PRC1").Value = prc1
            objSql.Parameters(":VOL").Value = volume
            objSql.Parameters(":STOP_TIME").Value = stop_time
            objSql.Parameters(":AVAIL_PAD_DAYS").Value = pad
            objSql.Parameters(":POINT_SIZE").Value = points
            objSql.Parameters(":FRAME_TP_ITM").Value = "N"
            objSql.Parameters(":GRADE").Value = UCase(grade)
            objSql.Parameters(":SIZ").Value = UCase(size)
            objSql.Parameters(":FINISH").Value = UCase(finish)
            objSql.Parameters(":CAT_CD").Value = UCase(category)
            objSql.Parameters(":WARRANTABLE").Value = UCase(warr)
            objSql.Parameters(":TREATABLE").Value = UCase(treatable)
            objSql.Parameters(":DROP_CD").Value = UCase(ConfigurationManager.AppSettings("default_drop_cd").ToString)
            objSql.Parameters(":NUM_DAYS").Value = SysPms.specialOrderNumDays

            Try
                objSql.ExecuteNonQuery()
            Catch
                conn.Close()
                Throw
            End Try
            objSql.Parameters.Clear()

            If comment & "" <> "" Then
                Dim lngSEQNUM As Integer, lngComments As Integer, x As Integer
                Dim strComments As String, strMidCmnts As String
                lngSEQNUM = 1
                lngComments = Len(txtComment.Text)
                strComments = txtComment.Text

                If lngComments > 0 Then
                    x = 1
                    Do Until x > lngComments
                        strMidCmnts = Mid(strComments, x, 70)
                        sql = "INSERT INTO ITM_CMNT (ITM_CD, SEQ#, TEXT) VALUES(:ITM_CD, :SEQ#, :TEXT)"

                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                        objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                        objSql.Parameters.Add(":SEQ#", OracleType.Number)
                        objSql.Parameters.Add(":TEXT", OracleType.VarChar)

                        objSql.Parameters(":ITM_CD").Value = txt_SKU.Text
                        objSql.Parameters(":SEQ#").Value = lngSEQNUM
                        objSql.Parameters(":TEXT").Value = UCase(strMidCmnts)

                        Try
                            'Execute DataReader 
                            objSql.ExecuteNonQuery()
                        Catch ex As Exception
                            conn.Close()
                            Throw
                        End Try
                        x = x + 70
                        lngSEQNUM = lngSEQNUM + 1
                    Loop
                End If
            End If
            If Session("itemid") & "" <> "" Then
                Session("itemid") = Session("itemid") & "," & txt_SKU.Text
            Else
                Session("itemid") = txt_SKU.Text
            End If
            conn.Close()
            'Remove the former values from the screen
            Clear_Values()
        Else
            lbl_SKU_Info.Text = "<font color=red>The SKU you selected already exists</font>"
            Exit Sub
        End If
        If add_another = "NO" Then
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("order_detail.aspx?LEAD=TRUE&special_added=TRUE")
            Else
                Response.Redirect("order_detail.aspx?special_added=TRUE")
            End If
        End If

    End Sub

    ''' <summary>
    ''' Retrieves the commission code for a special order sku based on the item's 
    ''' associated major amd minor code.
    ''' </summary>
    ''' <param name="minorCd">the minor code entered for the special order sku for which to get the commission code</param>
    ''' <returns>the special order commission code</returns>
    Private Function GetCommCode(ByVal minorCd As String) As String

        Dim commCode As String = String.Empty
        Dim sql As String
        Dim cmd As OracleCommand
        Dim dataReader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()
        sql = "SELECT spec_ord_comm_cd FROM inv_mjr WHERE mjr_cd = " &
                            "(SELECT mjr_cd FROM inv_mnr WHERE MNR_CD = :MNR_CD) "

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":MNR_CD", OracleType.VarChar)
        cmd.Parameters(":MNR_CD").Value = minorCd
        Try
            dataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (dataReader.Read()) Then
                commCode = dataReader.Item("spec_ord_comm_cd")
            End If

            dataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Return commCode
    End Function

    ''' <summary>
    ''' Verifies that the vendor entered is a valid one, otherewise displays a message.
    ''' </summary>
    Private Sub Verify_Vendor()

        Dim objSql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim Warnings_Popup As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()
        Dim sql As String = UCase("SELECT VE_CD, VE_NAME FROM VE WHERE VE_CD='" & txt_Vendor.Text & "'")
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try

            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            If Not MyDatareader.Read() Then
                lbl_SKU_Info.Text = "<font color=red>Invalid Vendor Code</font>"
                btn_save_exit.Enabled = False
                btn_save_add.Enabled = False
                txt_Vendor.Text = ""
                txt_Vendor.Focus()
                lbl_ve_label.Text = ""
            Else
                lbl_ve_label.Text = MyDatareader.Item("VE_NAME").ToString
                btn_save_exit.Enabled = True
                btn_save_add.Enabled = True
            End If
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub


    Private Sub Create_SKU(ByVal itm_cd As String)

        Dim randObj As New Random
        Dim x As Integer
        Dim Start_Number As Integer
        Dim End_Number As Integer
        Dim test As Integer
        Dim SKU As String
        Dim sql As String
        Dim objSql As OracleCommand
        Dim Try_Count As Integer = 1
        Dim MyDataReader As OracleDataReader
        Dim SKU_Limit As Double = CDbl(ConfigurationManager.AppSettings("special_order_limits").ToString)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        If Not IsNumeric(SKU_Limit) Then
            SKU_Limit = 9
        Else
            If SKU_Limit < 1 Then
                SKU_Limit = 9
            End If
        End If
        SKU = "FALSE"

        If Len(itm_cd) > SKU_Limit Then
            lbl_SKU_Info.Text = "The SKU you entered is greater than the maximum allowable length (Allowed:" & SKU_Limit & ")"
            Exit Sub
        End If

        lbl_SKU_Info.Text = ""

        Do While SKU = "FALSE"
            If Try_Count >= 20 Then
                lbl_SKU_Info.Text = "The system was unable to generate a SKU automatically."
                Exit Sub
            End If
            If Len(itm_cd) <> SKU_Limit Then
                Start_Number = 1
                End_Number = 0
                For x = 1 To (SKU_Limit - Len(itm_cd))
                    Start_Number = Start_Number * 10
                Next
                test = SKU_Limit - Len(itm_cd)
                End_Number = Replace(Start_Number, 0, SKU_Limit)
                End_Number = Right(End_Number, test)
                Start_Number = Left(Start_Number, test)
                SKU = itm_cd & (randObj.Next(Start_Number, End_Number).ToString())
            Else : SKU = itm_cd
            End If
            sql = "SELECT ITM_CD FROM ITM WHERE ITM_CD='" & UCase(SKU) & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                conn.Open()
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read() Then
                    If Len(itm_cd) = SKU_Limit Then
                        lbl_SKU_Info.Text = "**THE SKU YOU SELECTED IS NOT AVAILABLE**"
                        SKU = txt_SKU.Text
                        Exit Do
                    End If
                    SKU = "FALSE"
                Else
                    SKU = SKU
                    lbl_SKU_Info.Text = "SKU Created"
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            Try_Count = Try_Count + 1
        Loop
        txt_SKU.Text = UCase(SKU)

    End Sub

    Protected Sub btn_save_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_exit.Click

        Add_SKU("NO")

    End Sub

    Protected Sub cbo_minor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_minor.SelectedIndexChanged

        MinorChanges()

    End Sub

    Public Sub Update_Category_Data()

        If Not String.IsNullOrEmpty(cbo_category.SelectedValue.ToString) Then

            Dim objsql2 As OracleCommand
            Dim MyDatareader As OracleDataReader
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            Dim sql As String = "SELECT CAT_POINT_SIZE, STOP_TIME FROM INV_MNR_CAT " &
                                 "WHERE CAT_CD='" & cbo_category.SelectedValue & "'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                conn.Open()
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader.Read()) Then
                    If IsNumeric(MyDatareader.Item("CAT_POINT_SIZE").ToString) Then
                        txtPoints.Text = MyDatareader.Item("CAT_POINT_SIZE").ToString
                    Else
                        txtPoints.Text = "0"
                    End If
                    If IsNumeric(MyDatareader.Item("STOP_TIME").ToString) Then
                        txtStop.Text = MyDatareader.Item("STOP_TIME").ToString
                    Else
                        txtStop.Text = "0"
                    End If
                Else
                    txtStop.Text = "0"
                    txtPoints.Text = "0"
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            sql = "SELECT TREATMENT_ITM_CD FROM INV_MNR_CAT WHERE MNR_CD = '" & cbo_minor.SelectedValue & "' AND CAT_CD = '" & cbo_category.SelectedValue & "'"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader.Read()) Then
                    If Not String.IsNullOrEmpty(MyDatareader.Item("TREATMENT_ITM_CD").ToString) Then
                        cboTreatable.SelectedValue = "Y"
                    Else
                        cboTreatable.SelectedValue = "N"
                    End If
                Else
                    cboTreatable.SelectedValue = "N"
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Public Sub MinorChanges()

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = "SELECT style_cd, des, style_cd || ' - ' || des as full_desc " &
                            "FROM inv_mnr_style where mnr_cd='" & cbo_minor.SelectedValue &
                            "' ORDER BY style_cd"

        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            oAdp.Fill(ds)

            With cboStyle
                .DataSource = ds
                .DataValueField = "style_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        sql = "SELECT cat_cd, des, cat_cd || ' - ' || des as full_desc FROM inv_mnr_cat where mnr_cd='" & cbo_minor.SelectedValue & "' order by cat_cd"

        cmdGetCodes.Dispose()
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        ds.Dispose()
        ds = New DataSet

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = sql
            End With

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_category
                .DataSource = ds
                .DataValueField = "cat_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
        Update_Category_Data()

    End Sub

    Protected Sub btn_save_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_add.Click

        'Add the SKU to the order screen
        Add_SKU("YES")

    End Sub

    Private Sub Clear_Values()

        'Remove the former values from the screen
        txt_SKU.Text = ""
        txt_Vendor.Text = ""
        txtVSN.Text = ""
        txtDES.Text = ""
        txtCover.Text = ""
        txtRetail.Text = ""
        txtMFG.Text = ""
        txtGrade.Text = ""
        txtSize.Text = ""
        txtFinish.Text = ""
        txtComment.Text = ""
        txtFamily.Text = ""
        txtVolume.Text = ""
        txtMeasure.Text = ""
        txtFreight.Text = ""
        txtStop.Text = ""
        txtCarpet.Text = ""
        cbo_Warr.SelectedIndex = 0
        txtPad.Text = ""
        txtPoints.Text = ""
        cboTreatable.SelectedIndex = 0
        If cbo_Special_Forms.Items.Count > 0 Then cbo_Special_Forms.SelectedIndex = 0
        'cbo_Frames.SelectedIndex = 0
        cbo_Frames.Enabled = False

        'Populate the item type drop down
        itm_tp_populate()

        'Populate the minor codes
        mnr_populate()
        MinorChanges()

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Clear_Values()
        btn_save_add.Enabled = True
        btn_save_exit.Enabled = True

    End Sub

    Protected Sub cbo_Special_Forms_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Special_Forms.SelectedIndexChanged

        On Error Resume Next
        Err.Clear()
        hpl_excel.Visible = False
        img_excel.Visible = False

        If cbo_Special_Forms.SelectedIndex <> 0 Then
            'Remove the former values from the screen
            txt_SKU.Text = ""
            txt_Vendor.Text = ""
            txtVSN.Text = ""
            txtDES.Text = ""
            txtCover.Text = ""
            txtRetail.Text = ""
            txtMFG.Text = ""
            txtGrade.Text = ""
            txtSize.Text = ""
            txtFinish.Text = ""
            txtComment.Text = ""
            txtFamily.Text = ""
            txtVolume.Text = ""
            txtMeasure.Text = ""
            txtFreight.Text = ""
            txtStop.Text = ""
            txtCarpet.Text = ""
            cbo_Warr.SelectedIndex = 0
            txtPad.Text = ""
            txtPoints.Text = ""
            cboTreatable.SelectedIndex = 0

            Dim myDataset As New DataSet()

            ''You can also use the Excel ODBC driver I believe - didn't try though
            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;"""

            ''You must use the $ after the object you reference in the spreadsheet
            Dim myData As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter("SELECT DESCRIPTION FROM [SPECIALS$] GROUP BY DESCRIPTION ORDER BY DESCRIPTION", strConn)
            myData.TableMappings.Add("Table", "ExcelTest")
            myData.Fill(myDataset)
            hpl_excel.NavigateUrl = "merchandising\" & cbo_Special_Forms.SelectedValue
            hpl_excel.Visible = True
            img_excel.Visible = True

            If Err.Number = 5 Then
                cbo_Frames.Enabled = False
                cbo_Frames.Visible = False

                lbl_SKU_Info.Text = "<font color=red><b>No Special Order Information Found!!!</b></font>"
            Else
                lbl_SKU_Info.Text = "Enter up to 9 characters"
                cbo_Frames.Enabled = True
                cbo_Frames.Visible = True

                cbo_Frames.Items.Clear()
                cbo_Frames.Items.Insert(0, "Please Select A Frame")
                cbo_Frames.Items.FindByText("Please Select A Frame").Value = ""
                cbo_Frames.SelectedIndex = 0

                With cbo_Frames
                    .DataSource = myDataset.Tables(0).DefaultView
                    .DataValueField = "DESCRIPTION"
                    .DataTextField = "DESCRIPTION"
                    .DataBind()
                End With
            End If

        Else
            lbl_SKU_Info.Text = "Enter up to 9 characters"
            cbo_Frames.Enabled = False
        End If

    End Sub

    Protected Sub cbo_Frames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Frames.SelectedIndexChanged

        If cbo_Special_Forms.SelectedIndex <> 0 Then
            'Remove the former values from the screen
            txt_SKU.Text = ""
            txt_Vendor.Text = ""
            txtVSN.Text = ""
            txtDES.Text = ""
            txtCover.Text = ""
            txtRetail.Text = ""
            txtMFG.Text = ""
            txtGrade.Text = ""
            txtSize.Text = ""
            txtFinish.Text = ""
            txtComment.Text = ""
            txtFamily.Text = ""
            txtVolume.Text = ""
            txtMeasure.Text = ""
            txtFreight.Text = ""
            txtStop.Text = ""
            txtCarpet.Text = ""
            cbo_Warr.SelectedIndex = 0
            txtPad.Text = ""
            txtPoints.Text = ""
            cboTreatable.SelectedIndex = 0

            ''You can also use the Excel ODBC driver I believe - didn''t try though
            Dim conn2 = DisposablesManager.BuildOracleConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;""")

            Dim myDataset As New DataSet()
            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "merchandising\" & cbo_Special_Forms.SelectedValue & ";Extended Properties=""Excel 8.0;"""

            ''You must use the $ after the object you reference in the spreadsheet
            Dim sql As String = "SELECT * FROM [SPECIALS$] WHERE DESCRIPTION='" & cbo_Frames.SelectedValue & "'"
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader
            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Open Connection 
                conn2.Open()
                'Execute DataReader 
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader2.Read()) Then
                    txt_Vendor.Text = UCase(MyDatareader2(1).ToString)
                    txt_SKU.Text = MyDatareader2(0).ToString
                    txtDES.Text = UCase(MyDatareader2(3).ToString)
                    txtVSN.Text = UCase(MyDatareader2(2).ToString)
                    txtSize.Text = UCase(MyDatareader2(6).ToString)
                    cbo_ITM_TP_CD.SelectedValue = "INV"
                    cbo_minor.SelectedValue = UCase(MyDatareader2(5).ToString)
                    cboStyle.SelectedValue = UCase(MyDatareader2(4).ToString)
                    txtFinish.Text = UCase(MyDatareader2(7).ToString)
                End If

                'Close Connection 
                MyDatareader2.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        Else
            hpl_excel.Visible = False
            img_excel.Visible = False
        End If

    End Sub

    Protected Sub txt_Vendor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Vendor.TextChanged

        Verify_Vendor()

    End Sub

    Protected Sub cbo_category_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Update_Category_Data()

    End Sub

    Protected Sub btn_ve_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ve_submit.Click

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)


        Dim sql As String = "SELECT VE.VE_CD, VE.VE_NAME FROM VE, VE$VE_TP WHERE "
        If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
            sql = sql & "VE.VE_CD LIKE :VE_CD "
        End If
        If Not String.IsNullOrEmpty(txt_ve_name.Text) Then
            If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
                sql = sql & "AND "
            End If
            sql = sql & " VE.VE_NAME LIKE :VE_NAME "
        End If
        If Not String.IsNullOrEmpty(txt_ve_name.Text) Or Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
            sql = sql & "AND "
        End If
        sql = sql & " ve.ve_cd=ve$ve_tp.ve_cd and ve$ve_tp.ve_tp_cd in('MER','MED') "
        sql = sql & "group by ve.ve_cd, ve.ve_name "
        sql = sql & "ORDER BY VE.VE_NAME"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        If Not String.IsNullOrEmpty(txt_ve_cd.Text) Then
            objsql2.Parameters.Add(":VE_CD", OracleType.VarChar)
            objsql2.Parameters(":VE_CD").Value = UCase(txt_ve_cd.Text)
        End If
        If Not String.IsNullOrEmpty(txt_ve_name.Text) Then
            objsql2.Parameters.Add(":VE_NAME", OracleType.VarChar)
            objsql2.Parameters(":VE_NAME").Value = UCase(txt_ve_name.Text)
        End If

        Dim oAdp As OracleDataAdapter
        conn.Open()

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
            oAdp.Fill(ds)

            With ve_grid
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub ve_grid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ve_grid.ItemCommand

        txt_Vendor.Text = e.Item.Cells(0).Text
        lbl_ve_label.Text = e.Item.Cells(1).Text
        btn_save_exit.Enabled = True
        btn_save_add.Enabled = True
        lbl_SKU_Info.Text = ""
        Dim UpPanel As UpdateProgress
        UpPanel = Master.FindControl("UpdateProgress1")
        UpPanel.Visible = False

        ''ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "Rel_Prospect", "SelectAndClosePopup4();", True)
        'Dim scriptKey As String = "ClosePopupScript"
        'Dim script As System.Text.StringBuilder = New System.Text.StringBuilder()
        ''script.Append("SelectAndClosePopup4();")
        'script.Append("ctl00_ContentPlaceHolder1_ASPxPopupControl4.GetPopupControlCollection().HideAllWindows();")
        'System.Web.UI.ScriptManager.RegisterStartupScript(UpPanel, UpPanel.GetType(), scriptKey, script.ToString(), True)

        ASPxPopupControl4.ShowOnPageLoad = False

    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        ASPxPopupControl4.ShowOnPageLoad = True

    End Sub

    Protected Sub ve_grid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles ve_grid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim myButton As Button = CType(e.Item.Cells(2).Controls(0), Button)
            myButton.CssClass = "style5"
        End If

    End Sub

    Protected Sub mnr_grid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles mnr_grid.ItemCommand

        cbo_minor.SelectedValue = e.Item.Cells(0).Text
        ASPxPopupControl1.ShowOnPageLoad = False
        MinorChanges()

    End Sub

    Protected Sub mnr_grid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles mnr_grid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or _
       e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim myButton As Button = CType(e.Item.Cells(2).Controls(0), Button)
            myButton.CssClass = "style5"
        End If

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub btn_mnr_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_mnr_submit.Click

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        Dim sql As String = "SELECT MNR_CD, inv_mnr.des || ' (' || inv_mjr.des || ')' as full_des "
        sql = sql & "FROM inv_mnr, inv_mjr "
        sql = sql & "where inv_mnr.mjr_cd = inv_mjr.mjr_cd "
        If Not String.IsNullOrEmpty(txt_mnr_search.Text) Then
            sql = sql & "AND MNR_CD LIKE :MNR_CD "
        End If
        If Not String.IsNullOrEmpty(txt_mnr_desc_search.Text) Then
            sql = sql & "AND inv_mnr.DES LIKE :DES "
        End If
        sql = sql & "ORDER BY inv_mnr.DES"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        If Not String.IsNullOrEmpty(txt_mnr_search.Text) Then
            objsql2.Parameters.Add(":MNR_CD", OracleType.VarChar)
            objsql2.Parameters(":MNR_CD").Value = UCase(txt_mnr_search.Text)
        End If
        If Not String.IsNullOrEmpty(txt_mnr_desc_search.Text) Then
            objsql2.Parameters.Add(":DES", OracleType.VarChar)
            objsql2.Parameters(":DES").Value = UCase(txt_mnr_desc_search.Text)
        End If

        Dim oAdp As OracleDataAdapter
        conn.Open()

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
            oAdp.Fill(ds)

            With mnr_grid
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub btn_sku_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sku_search_submit.Click

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        Dim sql As String = "SELECT ITM_CD, VSN, DES " &
                             "FROM ITM WHERE "
        If Not String.IsNullOrEmpty(txt_sku_search.Text) Then
            sql = sql & "ITM_CD LIKE :ITM_CD "
        End If
        If Not String.IsNullOrEmpty(txt_vsn_search.Text) Then
            If Right(sql, 5) <> "WHERE " Then
                sql = sql & "AND "
            End If
            sql = sql & "VSN LIKE :VSN "
        End If
        If Not String.IsNullOrEmpty(txt_des_search.Text) Then
            If Right(sql, 5) <> "WHERE " Then
                sql = sql & "AND "
            End If
            sql = sql & "VSN LIKE :VSN "
        End If
        sql = sql & "ORDER BY VSN"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        If Not String.IsNullOrEmpty(txt_sku_search.Text) Then
            objsql2.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objsql2.Parameters(":ITM_CD").Value = UCase(txt_sku_search.Text)
        End If
        If Not String.IsNullOrEmpty(txt_vsn_search.Text) Then
            objsql2.Parameters.Add(":VSN", OracleType.VarChar)
            objsql2.Parameters(":VSN").Value = UCase(txt_vsn_search.Text)
        End If
        If Not String.IsNullOrEmpty(txt_des_search.Text) Then
            objsql2.Parameters.Add(":DES", OracleType.VarChar)
            objsql2.Parameters(":DES").Value = UCase(txt_des_search.Text)
        End If

        Dim oAdp As OracleDataAdapter
        conn.Open()

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
            oAdp.Fill(ds)

            With itm_grd
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub itm_grd_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles itm_grd.ItemCommand

        ASPxPopupControl2.ShowOnPageLoad = False
        SKU_Update(e.Item.Cells(0).Text)

    End Sub

    Public Sub SKU_Update(ByVal itm_cd As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = "SELECT * FROM ITM WHERE ITM_CD=:ITM_CD"
        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.Parameters.Add(":ITM_CD", OracleType.VarChar)
        objsql2.Parameters(":ITM_CD").Value = UCase(itm_cd)

        Try

            conn.Open()
            'Execute DataReader 
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDatareader2.Read()) Then
                cbo_ITM_TP_CD.SelectedValue = MyDatareader2.Item("ITM_TP_CD").ToString
                txt_Vendor.Text = MyDatareader2.Item("VE_CD").ToString
                cbo_minor.SelectedValue = MyDatareader2.Item("MNR_CD").ToString
                txtVSN.Text = MyDatareader2.Item("VSN").ToString
                txtDES.Text = MyDatareader2.Item("DES").ToString
                txtCover.Text = MyDatareader2.Item("COVER").ToString
                txtRetail.Text = MyDatareader2.Item("RET_PRC").ToString
                txtMFG.Text = MyDatareader2.Item("PRC1").ToString
                txtGrade.Text = MyDatareader2.Item("GRADE").ToString
                txtSize.Text = MyDatareader2.Item("SIZ").ToString
                txtFinish.Text = MyDatareader2.Item("FINISH").ToString
                cboStyle.SelectedValue = MyDatareader2.Item("STYLE_CD").ToString
                txtFamily.Text = MyDatareader2.Item("FAMILY_CD").ToString
                cbo_category.SelectedValue = MyDatareader2.Item("CAT_CD").ToString
                txtVolume.Text = MyDatareader2.Item("VOL").ToString
                txtFreight.Text = MyDatareader2.Item("FRT_FAC").ToString
                txtStop.Text = MyDatareader2.Item("STOP_TIME").ToString
                txtCarpet.Text = MyDatareader2.Item("ITM_CD").ToString
                cbo_Warr.SelectedValue = MyDatareader2.Item("WARRANTABLE").ToString
                txtPad.Text = MyDatareader2.Item("AVAIL_PAD_DAYS").ToString
                txtPoints.Text = MyDatareader2.Item("POINT_SIZE").ToString
                cboTreatable.SelectedValue = MyDatareader2.Item("TREATABLE").ToString
            End If

            'Close Connection 
            MyDatareader2.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try

    End Sub

    Protected Sub btn_sku_copy_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl2.ShowOnPageLoad = True

    End Sub
End Class
