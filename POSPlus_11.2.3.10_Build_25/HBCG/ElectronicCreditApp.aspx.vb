Imports System.IO
Imports System.Data.OracleClient
Imports System.Xml
Imports HBCG_Utils
Imports SD_Utils


Partial Class ElectronicCreditApp
    Inherits POSBasePage
    Private Const cResponseCriteria As String = "/HandleCode/QuickCredit/ReturnCode/CodeDesc"
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim cmd As OracleCommand
            Dim rdr As OracleDataReader

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT NAME, AS_CD FROM ASP" &
                        " WHERE  ACTIVE = 'Y' " &
                        " AND  AS_TP_CD = 'F' AND ELECTRONIC_INT= 'Y'  " &
                        " AND ELECTRONIC_CR_APP = 'Y'  ORDER BY NAME"
            'Set SQL OBJECT 
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)

            'initialize app type to 'Single' and disable co-applicant validators
            cbo_app_type.SelectedValue = 1
            SetEnabledCoApplicantValidators(False)

            Try
                conn.Open()
                rdr = DisposablesManager.BuildOracleDataReader(cmd, CommandBehavior.CloseConnection)    'this will close the connection as soon as the reader is closed

                If (rdr.HasRows) Then
                    cboProviders.Items.Clear()
                    cboProviders.DataSource = rdr
                    cboProviders.DataTextField = "NAME"
                    cboProviders.DataValueField = "AS_CD"
                    cboProviders.DataBind()
                    cboProviders.Items.Insert(0, "Select a Provider")
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "Setup Error", "alert('" + "No ECA providers found in the system.\n Please contact the System Administrator." + "');", True)
                End If

                cboProviders.SelectedIndex = 0
                rdr.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

    End Sub


    'Excutes on the 'submit' button click to submit an ECA auth
    Protected Sub btn_save_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_exit.Click

        'Do validation  
        Dim error_text As String = ""
        lbl_response.Text = ""

        'check for provider selection
        If cboProviders.SelectedIndex = 0 Then
            error_text = " Please select a provider."
        Else
            ' Check for primary applicant info
            error_text = CheckForPrimaryApplicantInfo()

            'validate co-applicant info
            If cbo_app_type.SelectedValue = 2 Then
                error_text = CheckForSecondaryApplicantInfo()
            End If
        End If

        If error_text & "" <> "" Then
            lbl_response.Text = "<b>The following items must be corrected before continuing:</b> <br /><br />" & error_text
            Exit Sub
        End If

        'validate all the other data for length, etc.
        ValidateData()

        'save the info into EC application
        SaveApplicationInfo()

        Dim conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim sql As String = "SELECT ROW_ID FROM EC_APPLICATION WHERE PRIME_FNAME='" & txt_fname.Text & "' AND PRIME_LNAME='" & txt_lname.Text & "' " &
                                                 "AND PRIME_SSN='" & txt_ssn1.Text & txt_ssn2.Text & txt_ssn3.Text & "' ORDER BY ROW_ID DESC"

        Dim MyDataReader1 As OracleDataReader
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader1 = DisposablesManager.BuildOracleDataReader(objsql2)

        Dim terminalId As String = ""
        Dim processorId As String = ""
        Dim SDC_Response As String = ""
        'merchant_id = Lookup_Merchant("ECA", Session("home_store_cd"), "ECA")

        'retrieve the auth provider specific details from the db
        Dim store As String = Session("HOME_STORE_CD")
        Dim ds As DataSet = GetAuthDetails(cboProviders.SelectedValue, store, GetCashDrawerCd(store))

        For Each dr In ds.Tables(0).Rows
            terminalId = dr("terminal_id")
            processorId = dr("processor_id")
        Next

        'build the request and call for auth 
        If MyDataReader1.Read Then
            SDC_Response = SD_Submit_Query_PL(MyDataReader1.Item("ROW_ID").ToString, terminalId, AUTH_ECA, "BLANK", "0", processorId)
            'process and display the response from PBase
            lbl_response.Text = processAuthResult(SDC_Response)
        Else
            lbl_response.Text = "Application Failed"
        End If
        conn.Close()

    End Sub


    Private Function processAuthResult(ByVal response As String) As String

        Dim result As String = ""
        Dim fields As String() = Nothing

        Dim sep(3) As Char
        sep(0) = Chr(10)
        sep(1) = Chr(12)
        fields = response.Split(sep)
        Dim s As String
        Dim MsgArray As Array
        For Each s In fields
            If InStr(s, ",") > 0 Then
                MsgArray = Split(s, ",")
                Select Case MsgArray(0)
                    Case "1004"
                        result = result & "Application Status: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "3165"
                        result = result & "Application Key: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "3305"
                        result = result & "Account Number: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                    Case "3306"
                        'check if the value is numeric, then convert to a double to divide by 100 and display as currency
                        Dim amt As String = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        If (IsNumeric(amt)) Then
                            amt = FormatCurrency(CDbl(amt), 2)
                        End If
                        result = result & "Credit Limit: " & amt & "<br />"
                End Select
            End If
        Next s

        Return result
    End Function


    ''' <summary>
    ''' Checks for the required primary applicant info
    ''' </summary>
    ''' <returns>the error text, if any</returns>
    Private Function CheckForPrimaryApplicantInfo() As String

        Dim error_text As String = ""
        If (txt_day.Text.isEmpty Or txt_month.Text.isEmpty Or txt_year.Text.isEmpty) Then
            error_text = error_text & "Birthdate is required..<br />"
        Else
            If Not IsNumericValue(txt_day.Text) Or Not IsNumericValue(txt_month.Text) Or Not IsNumericValue(txt_year.Text) Then
                error_text = error_text & "Birthdate must be numeric<br />"
                txt_co_day.Focus()
            ElseIf Not IsDate(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text) Then
                error_text = error_text & "Birthdate must be a date<br />"
                txt_co_day.Focus()
            End If
        End If

        If (txt_ssn1.Text.isEmpty Or txt_ssn2.Text.isEmpty Or txt_ssn3.Text.isEmpty) Then
            error_text = error_text & "Social Security number is required.<br />"
        Else
            If Not IsNumericValue(txt_ssn1.Text) Or Len(txt_ssn1.Text) <> 3 Then
                error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
                txt_ssn1.Focus()
            ElseIf Not IsNumericValue(txt_ssn2.Text) Or Len(txt_ssn2.Text) <> 2 Then
                error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
                txt_ssn2.Focus()
            ElseIf Not IsNumericValue(txt_ssn3.Text) Or Len(txt_ssn3.Text) <> 4 Then
                error_text = error_text & "Social Security must be numeric and 9 digits long<br />"
                txt_ssn3.Focus()
            End If
        End If

        Return error_text
    End Function

    ''' <summary>
    ''' Checks for the required secondary applicant info
    ''' </summary>
    ''' <returns>the error text, if any</returns>
    Private Function CheckForSecondaryApplicantInfo() As String

        Dim error_text As String = ""
        If Not IsNumericValue(txt_co_day.Text) Or Not IsNumericValue(txt_co_month.Text) Or Not IsNumericValue(txt_co_year.Text) Then
            error_text = error_text & "Co-Applicant Birthdate must be numeric<br />"
            txt_co_day.Focus()
        ElseIf Not IsDate(txt_co_month.Text & "/" & txt_co_day.Text & "/" & txt_co_year.Text) Then
            error_text = error_text & "Co-Applicant Birthdate must be a date<br />"
            txt_co_day.Focus()
        End If

        If Not IsNumericValue(txt_co_ssn1.Text) Or Len(txt_co_ssn1.Text) <> 3 Then
            error_text = error_text & "Co-Applicant Social Security must be numeric and 9 digits long<br />"
            txt_co_ssn1.Focus()
        ElseIf Not IsNumericValue(txt_co_ssn2.Text) Or Len(txt_co_ssn2.Text) <> 2 Then
            error_text = error_text & "Co-Applicant Social Security must be numeric and 9 digits long<br />"
            txt_co_ssn2.Focus()
        ElseIf Not IsNumeric(txt_co_ssn3.Text) Or Len(txt_co_ssn3.Text) <> 4 Then
            error_text = error_text & "Co-Applicant Social Security must be numeric and 9 digits long<br />"
            txt_co_ssn3.Focus()
        End If

        Return error_text
    End Function

    ''' <summary>
    ''' Checks if the passed-in value is not null/empty and numeric.
    ''' </summary>
    ''' <param name="input">the value to check</param>
    ''' <returns>True to indicate that the passed-in value is a valid numeric value</returns>
    Private Function IsNumericValue(ByVal input As String) As Boolean

        Dim rtnVal As Boolean = False

        If input.isNotEmpty And IsNumeric(input) Then
            rtnVal = True
        End If

        Return rtnVal
    End Function


    Private Sub ValidateData()

        If IsNumeric(txt_req_amt.Text) Then
            txt_req_amt.Text = CInt(txt_req_amt.Text)
        End If

        If Len(txt_time_at_res_months.Text) = 1 Then
            txt_time_at_res_months.Text = "0" & txt_time_at_res_months.Text
        ElseIf txt_time_at_res_months.Text & "" = "" Then
            txt_time_at_res_months.Text = "00"
        End If
        If Len(txt_time_at_res_years.Text) = 1 Then
            txt_time_at_res_years.Text = "0" & txt_time_at_res_years.Text
        ElseIf txt_time_at_res_years.Text & "" = "" Then
            txt_time_at_res_years.Text = "00"
        End If
        If Len(txt_co_time_at_res_months.Text) = 1 Then
            txt_co_time_at_res_months.Text = "0" & txt_co_time_at_res_months.Text
        ElseIf txt_co_time_at_res_months.Text & "" = "" Then
            txt_co_time_at_res_months.Text = "00"
        End If
        If Len(txt_co_time_at_res_years.Text) = 1 Then
            txt_co_time_at_res_years.Text = "0" & txt_co_time_at_res_years.Text
        ElseIf txt_co_time_at_res_years.Text & "" = "" Then
            txt_co_time_at_res_years.Text = "00"
        End If

        If Len(txt_emp_time_years.Text) = 1 Then
            txt_emp_time_years.Text = "0" & txt_emp_time_years.Text
        ElseIf txt_emp_time_years.Text & "" = "" Then
            txt_emp_time_years.Text = "00"
        End If
        If Len(txt_emp_time_months.Text) = 1 Then
            txt_emp_time_months.Text = "0" & txt_emp_time_months.Text
        ElseIf txt_emp_time_months.Text & "" = "" Then
            txt_emp_time_months.Text = "00"
        End If
        If Len(txt_co_emp_time_years.Text) = 1 Then
            txt_co_emp_time_years.Text = "0" & txt_co_emp_time_years.Text
        ElseIf txt_co_emp_time_years.Text & "" = "" Then
            txt_co_emp_time_years.Text = "00"
        End If
        If Len(txt_co_emp_time_months.Text) = 1 Then
            txt_co_emp_time_months.Text = "0" & txt_co_emp_time_months.Text
        ElseIf txt_co_emp_time_months.Text & "" = "" Then
            txt_co_emp_time_months.Text = "00"
        End If

    End Sub

    ''' <summary>
    '''  'saves the EC application and applicant info to the db
    ''' </summary>
    Private Sub SaveApplicationInfo()

        Dim sql As String
        Dim objSql As OracleCommand
        Dim conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO EC_APPLICATION (PRIME_FNAME, PRIME_MIDDLE_INIT, PRIME_LNAME, PRIME_DOB, PRIME_SSN, PRIME_CELL, PRIME_ADDR1, "
            sql = sql & "PRIME_ADDR2, PRIME_CITY, PRIME_STATE, PRIME_ZIP_CD, PRIME_HOME_PHONE, PRIME_TIME_AT_RES, PRIME_HOME_STATUS, "
            sql = sql & "PRIME_EMP_PHONE, PRIME_EMP_TIME,  PRIME_OCCUPATION, PRIME_EMP_NAME, PRIME_EMP_CITY, PRIME_EMP_STATE,  "

            sql = sql & "PRIME_MON_INCOME, PRIME_YR_INCOME, JOINT_FNAME, JOINT_MIDDLE_INIT, JOINT_LNAME, JOINT_DOB, JOINT_SSN, JOINT_CITY, JOINT_STATE, "
            sql = sql & "JOINT_ZIP, JOINT_ADDR1, JOINT_ADDR2, JOINT_HOME, JOINT_CELL, JOINT_HOME_STATUS, JOINT_TIME_AT_RES, JOINT_EMP_TIME, JOINT_WORK, "
            sql = sql & "JOINT_MON_INCOME, JOINT_DL, JOINT_DL_ST, RELATIVE_PHONE, CREDIT_PROTECTION, JOINT_APPLICATION, PRIME_DL, PRIME_DL_ST, "
            'sql = sql & "LANGUAGE_INDICATOR, REQUESTED_AMOUNT, )"
            sql = sql & "LANGUAGE_INDICATOR, REQUESTED_AMOUNT,  JOINT_OCCUPATION, JOINT_EMP_NAME, JOINT_EMP_CITY,  JOINT_EMP_STATE, JOINT_YR_INCOME )"

            sql = sql & " VALUES (:PRIME_FNAME, :PRIME_MIDDLE_INIT, :PRIME_LNAME, :PRIME_DOB, :PRIME_SSN, :PRIME_CELL, :PRIME_ADDR1,  "
            sql = sql & ":PRIME_ADDR2, :PRIME_CITY, :PRIME_STATE, :PRIME_ZIP_CD, :PRIME_HOME_PHONE, :PRIME_TIME_AT_RES, :PRIME_HOME_STATUS,  "
            sql = sql & " :PRIME_EMP_PHONE, :PRIME_EMP_TIME,  :PRIME_OCCUPATION,  :PRIME_EMP_NAME, :PRIME_EMP_CITY, :PRIME_EMP_STATE, "
            sql = sql & ":PRIME_MON_INCOME, :PRIME_YR_INCOME, :JOINT_FNAME, :JOINT_MIDDLE_INIT, :JOINT_LNAME, :JOINT_DOB, :JOINT_SSN, :JOINT_CITY, :JOINT_STATE, "
            sql = sql & ":JOINT_ZIP, :JOINT_ADDR1, :JOINT_ADDR2, :JOINT_HOME, :JOINT_CELL, :JOINT_HOME_STATUS, :JOINT_TIME_AT_RES, :JOINT_EMP_TIME, :JOINT_WORK, "
            sql = sql & ":JOINT_MON_INCOME, :JOINT_DL, :JOINT_DL_ST, :RELATIVE_PHONE, :CREDIT_PROTECTION, :JOINT_APPLICATION, :PRIME_DL, :PRIME_DL_ST,  "
            sql = sql & ":LANGUAGE_INDICATOR, :REQUESTED_AMOUNT,  :JOINT_OCCUPATION, :JOINT_EMP_NAME, :JOINT_EMP_CITY,  :JOINT_EMP_STATE, :JOINT_YR_INCOME)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":PRIME_FNAME", OracleType.VarChar)
            objSql.Parameters(":PRIME_FNAME").Value = txt_fname.Text
            objSql.Parameters.Add(":PRIME_MIDDLE_INIT", OracleType.VarChar)
            objSql.Parameters(":PRIME_MIDDLE_INIT").Value = txt_middle_init.Text
            objSql.Parameters.Add(":PRIME_LNAME", OracleType.VarChar)
            objSql.Parameters(":PRIME_LNAME").Value = txt_lname.Text
            objSql.Parameters.Add(":PRIME_DOB", OracleType.DateTime)
            If Not IsDate(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text) Then
                objSql.Parameters(":PRIME_DOB").Value = ""
            Else
                objSql.Parameters(":PRIME_DOB").Value = FormatDateTime(txt_month.Text & "/" & txt_day.Text & "/" & txt_year.Text, DateFormat.ShortDate)
            End If
            objSql.Parameters.Add(":PRIME_SSN", OracleType.VarChar)
            objSql.Parameters(":PRIME_SSN").Value = txt_ssn1.Text & txt_ssn2.Text & txt_ssn3.Text
            objSql.Parameters.Add(":PRIME_CELL", OracleType.VarChar)
            objSql.Parameters(":PRIME_CELL").Value = txt_alt_phone.Text
            objSql.Parameters.Add(":PRIME_ADDR1", OracleType.VarChar)
            objSql.Parameters(":PRIME_ADDR1").Value = txt_addr1.Text
            objSql.Parameters.Add(":PRIME_ADDR2", OracleType.VarChar)
            objSql.Parameters(":PRIME_ADDR2").Value = txt_addr2.Text
            objSql.Parameters.Add(":PRIME_CITY", OracleType.VarChar)
            objSql.Parameters(":PRIME_CITY").Value = txt_city.Text

            objSql.Parameters.Add(":PRIME_STATE", OracleType.VarChar)
            objSql.Parameters(":PRIME_STATE").Value = txt_state.Text
            objSql.Parameters.Add(":PRIME_ZIP_CD", OracleType.VarChar)
            objSql.Parameters(":PRIME_ZIP_CD").Value = txt_zip.Text
            objSql.Parameters.Add(":PRIME_HOME_PHONE", OracleType.VarChar)
            objSql.Parameters(":PRIME_HOME_PHONE").Value = txt_home_phone.Text
            objSql.Parameters.Add(":PRIME_TIME_AT_RES", OracleType.VarChar)
            objSql.Parameters(":PRIME_TIME_AT_RES").Value = txt_time_at_res_months.Text & txt_time_at_res_years.Text
            objSql.Parameters.Add(":PRIME_HOME_STATUS", OracleType.VarChar)
            objSql.Parameters(":PRIME_HOME_STATUS").Value = cbo_res_status.SelectedValue

            objSql.Parameters.Add(":PRIME_EMP_TIME", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_TIME").Value = txt_emp_time_months.Text & txt_emp_time_years.Text
            objSql.Parameters.Add(":PRIME_EMP_PHONE", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_PHONE").Value = txt_emp_phone.Text
            objSql.Parameters.Add(":PRIME_OCCUPATION", OracleType.VarChar)
            objSql.Parameters(":PRIME_OCCUPATION").Value = cbo_occupation.SelectedValue
            objSql.Parameters.Add(":PRIME_EMP_NAME", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_NAME").Value = txt_emp_name.Text
            objSql.Parameters.Add(":PRIME_EMP_CITY", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_CITY").Value = txt_emp_city.Text
            objSql.Parameters.Add(":PRIME_EMP_STATE", OracleType.VarChar)
            objSql.Parameters(":PRIME_EMP_STATE").Value = txt_emp_state.Text


            objSql.Parameters.Add(":PRIME_MON_INCOME", OracleType.VarChar)
            objSql.Parameters(":PRIME_MON_INCOME").Value = txt_monthly_income.Text
            objSql.Parameters.Add(":PRIME_YR_INCOME", OracleType.VarChar)
            objSql.Parameters(":PRIME_YR_INCOME").Value = txt_annual_income.Text
            objSql.Parameters.Add(":JOINT_FNAME", OracleType.VarChar)
            objSql.Parameters(":JOINT_FNAME").Value = txt_co_fname.Text
            objSql.Parameters.Add(":JOINT_MIDDLE_INIT", OracleType.VarChar)
            objSql.Parameters(":JOINT_MIDDLE_INIT").Value = txt_co_middle_init.Text
            objSql.Parameters.Add(":JOINT_LNAME", OracleType.VarChar)
            objSql.Parameters(":JOINT_LNAME").Value = txt_co_lname.Text
            objSql.Parameters.Add(":JOINT_DOB", OracleType.DateTime)
            If Not IsDate(txt_co_month.Text & "/" & txt_co_day.Text & "/" & txt_co_year.Text) Then
                objSql.Parameters(":JOINT_DOB").Value = OracleDateTime.Null
            Else
                objSql.Parameters(":JOINT_DOB").Value = FormatDateTime(txt_co_month.Text & "/" & txt_co_day.Text & "/" & txt_co_year.Text, DateFormat.ShortDate)
            End If
            objSql.Parameters.Add(":JOINT_SSN", OracleType.VarChar)
            objSql.Parameters(":JOINT_SSN").Value = txt_co_ssn1.Text & txt_co_ssn2.Text & txt_co_ssn3.Text
            objSql.Parameters.Add(":JOINT_CITY", OracleType.VarChar)
            objSql.Parameters(":JOINT_CITY").Value = txt_co_City.Text
            objSql.Parameters.Add(":JOINT_STATE", OracleType.VarChar)
            objSql.Parameters(":JOINT_STATE").Value = txt_co_state.Text

            objSql.Parameters.Add(":JOINT_ZIP", OracleType.VarChar)
            objSql.Parameters(":JOINT_ZIP").Value = txt_co_zip.Text
            objSql.Parameters.Add(":JOINT_ADDR1", OracleType.VarChar)
            objSql.Parameters(":JOINT_ADDR1").Value = txt_co_addr1.Text
            objSql.Parameters.Add(":JOINT_ADDR2", OracleType.VarChar)
            objSql.Parameters(":JOINT_ADDR2").Value = txt_co_addr2.Text
            objSql.Parameters.Add(":JOINT_HOME", OracleType.VarChar)
            objSql.Parameters(":JOINT_HOME").Value = txt_co_home_phone.Text
            objSql.Parameters.Add(":JOINT_CELL", OracleType.VarChar)
            objSql.Parameters(":JOINT_CELL").Value = txt_co_alt_phone.Text
            objSql.Parameters.Add(":JOINT_HOME_STATUS", OracleType.VarChar)
            objSql.Parameters(":JOINT_HOME_STATUS").Value = cbo_co_res_status.SelectedValue
            objSql.Parameters.Add(":JOINT_TIME_AT_RES", OracleType.VarChar)
            objSql.Parameters(":JOINT_TIME_AT_RES").Value = txt_co_time_at_res_months.Text & txt_co_time_at_res_years.Text
            objSql.Parameters.Add(":JOINT_EMP_TIME", OracleType.VarChar)
            objSql.Parameters(":JOINT_EMP_TIME").Value = txt_co_emp_time_months.Text & txt_co_emp_time_years.Text
            objSql.Parameters.Add(":JOINT_WORK", OracleType.VarChar)
            objSql.Parameters(":JOINT_WORK").Value = txt_co_emp_phone.Text

            objSql.Parameters.Add(":JOINT_OCCUPATION", OracleType.VarChar)
            objSql.Parameters(":JOINT_OCCUPATION").Value = cbo_coapp_occupation.SelectedValue
            objSql.Parameters.Add(":JOINT_EMP_NAME", OracleType.VarChar)
            objSql.Parameters(":JOINT_EMP_NAME").Value = txt_co_emp_name.Text
            objSql.Parameters.Add(":JOINT_EMP_CITY", OracleType.VarChar)
            objSql.Parameters(":JOINT_EMP_CITY").Value = txt_co_emp_city.Text
            objSql.Parameters.Add(":JOINT_EMP_STATE", OracleType.VarChar)
            objSql.Parameters(":JOINT_EMP_STATE").Value = txt_co_emp_state.Text

            objSql.Parameters.Add(":JOINT_MON_INCOME", OracleType.VarChar)
            objSql.Parameters(":JOINT_MON_INCOME").Value = txt_co_monthly_income.Text
            objSql.Parameters.Add(":JOINT_YR_INCOME", OracleType.VarChar)
            objSql.Parameters(":JOINT_YR_INCOME").Value = txt_co_annual_income.Text
            objSql.Parameters.Add(":JOINT_DL", OracleType.VarChar)
            objSql.Parameters(":JOINT_DL").Value = txt_co_dl_license_no.Text
            objSql.Parameters.Add(":JOINT_DL_ST", OracleType.VarChar)
            objSql.Parameters(":JOINT_DL_ST").Value = txt_co_dl_license_state.Text
            objSql.Parameters.Add(":RELATIVE_PHONE", OracleType.VarChar)
            objSql.Parameters(":RELATIVE_PHONE").Value = txt_rel_phone.Text
            objSql.Parameters.Add(":CREDIT_PROTECTION", OracleType.VarChar)
            objSql.Parameters(":CREDIT_PROTECTION").Value = cbo_credit_protection.SelectedValue
            objSql.Parameters.Add(":JOINT_APPLICATION", OracleType.VarChar)
            objSql.Parameters(":JOINT_APPLICATION").Value = cbo_app_type.SelectedValue
            objSql.Parameters.Add(":PRIME_DL", OracleType.VarChar)
            objSql.Parameters(":PRIME_DL").Value = txt_dl_license_no.Text
            objSql.Parameters.Add(":PRIME_DL_ST", OracleType.VarChar)
            objSql.Parameters(":PRIME_DL_ST").Value = txt_dl_license_state.Text
            objSql.Parameters.Add(":LANGUAGE_INDICATOR", OracleType.VarChar)
            objSql.Parameters(":LANGUAGE_INDICATOR").Value = "E"
            objSql.Parameters.Add(":REQUESTED_AMOUNT", OracleType.VarChar)
            objSql.Parameters(":REQUESTED_AMOUNT").Value = txt_req_amt.Text

            objSql.ExecuteNonQuery()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_lookup.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        Dim custCd As String = txt_cust_cd.Text
        If (Not String.IsNullOrEmpty(custCd)) Then
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            sql = "SELECT FNAME, INIT, LNAME, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE " &
                        " FROM CUST WHERE CUST_CD ='" & custCd.ToUpper & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    txt_fname.Text = MyDataReader.Item("FNAME").ToString
                    txt_middle_init.Text = MyDataReader.Item("INIT").ToString
                    txt_lname.Text = MyDataReader.Item("LNAME").ToString
                    txt_addr1.Text = MyDataReader.Item("ADDR1").ToString
                    txt_addr2.Text = MyDataReader.Item("ADDR2").ToString
                    txt_city.Text = MyDataReader.Item("CITY").ToString
                    txt_state.Text = MyDataReader.Item("ST_CD").ToString
                    txt_zip.Text = MyDataReader.Item("ZIP_CD").ToString
                    txt_home_phone.Text = Replace(MyDataReader.Item("HOME_PHONE").ToString, "-", "")
                    txt_alt_phone.Text = Replace(MyDataReader.Item("BUS_PHONE").ToString, "-", "")
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

    End Sub

    Protected Sub cbo_app_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_app_type.SelectedIndexChanged

        Dim enable As Boolean = False

        'disable or enable the co-app validators based on whether the app is 'single or 'joint'
        If cbo_app_type.SelectedValue = 2 Then      'joint app
            enable = True
        End If
        SetEnabledCoApplicantValidators(enable)

    End Sub

    ''' <summary>
    ''' Enables or disables the co applicant validators based on the passed-in flag
    ''' </summary>
    ''' <param name="enable">True to enable the validators; false otherwise</param>
    Private Sub SetEnabledCoApplicantValidators(ByVal enable As Boolean)

        RequiredCoFNameValidator.Enabled = enable
        RequiredCoLNameFieldValidator.Enabled = enable
        RequiredCoAddr1FieldValidator.Enabled = enable
        RequiredCoCityFieldValidator.Enabled = enable
        RequiredCoStateFieldValidator.Enabled = enable
        RequiredCoHPhoneFieldValidator.Enabled = enable
        RequiredCoZipFieldValidator.Enabled = enable
        CoAltPhoneExpressionValidator.Enabled = enable
        CoEmpMonthRangeValidator.Enabled = enable
        CoAppAnnualIncomeValidator.Enabled = enable
        CoAppMonthlyIncomeValidator.Enabled = enable

    End Sub


    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(Request.ServerVariables("APPL_PHYSICAL_PATH") & "ADS.xml")
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            ElseIf IsNumeric(m_node.ChildNodes.Item(0).InnerText) And IsNumeric(reasonCd) Then
                If CDbl(m_node.ChildNodes.Item(0).InnerText) = CDbl(reasonCd) Then
                    desc = m_node.ChildNodes.Item(1).InnerText
                    Exit For
                End If
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc
    End Function

    Public Function SendRequestAndGetResponse(ByRef requestDS As QuickCreditDataRequest) 'As XmlDocument

        Dim ADS_Connection_String As String = Get_Connection_String("/HandleCode/ADS/QuickCredit", "PrimaryConnection")
        Dim oWebReq As System.Net.HttpWebRequest
        oWebReq = CType(System.Net.HttpWebRequest.Create(ADS_Connection_String), System.Net.HttpWebRequest)

        Dim xml_string As String = ""

        xml_string = "<?xml version=""1.0"" encoding=""utf-8""?>"
        xml_string = xml_string & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
        xml_string = xml_string & "<soap:Body>"
        xml_string = xml_string & "<ns1:quickCredit30 xmlns:ns1=""http://axes.alldata.net/web-services"" version=""3.0"">"
        xml_string = xml_string & "<request>"
        xml_string = xml_string & "<clientHeader>"
        xml_string = xml_string & "<clientName>LEVINS</clientName>"
        xml_string = xml_string & "<timestamp>" & FormatDateTime(Now.Date.ToString, DateFormat.ShortDate) & " " & FormatDateTime(Now.TimeOfDay.ToString, DateFormat.LongTime) & "</timestamp>"
        xml_string = xml_string & "</clientHeader>"
        xml_string = xml_string & "<storeNumber>" & Session("HOME_STORE_CD") & "</storeNumber>"
        'xml_string = xml_string & "<etailId>" & requestDS.QuickCreditRequest(0).etailId & "</etailId>"
        If requestDS.QuickCreditRequest(0).referenceNumber & "" <> "" Then
            xml_string = xml_string & "<referenceNumber>" & requestDS.QuickCreditRequest(0).referenceNumber & "</referenceNumber>"
        End If
        If requestDS.QuickCreditRequest(0).firstName & "" <> "" Then
            xml_string = xml_string & "<firstName>" & requestDS.QuickCreditRequest(0).firstName & "</firstName>"
        End If
        If requestDS.QuickCreditRequest(0).lastName & "" <> "" Then
            xml_string = xml_string & "<lastName>" & requestDS.QuickCreditRequest(0).lastName & "</lastName>"
        End If
        If requestDS.QuickCreditRequest(0).houseNumber & "" <> "" Then
            xml_string = xml_string & "<houseNumber>" & requestDS.QuickCreditRequest(0).houseNumber & "</houseNumber>"
        End If
        If requestDS.QuickCreditRequest(0).zipCode & "" <> "" Then
            xml_string = xml_string & "<zipCode>" & requestDS.QuickCreditRequest(0).zipCode & "</zipCode>"
        End If
        If requestDS.QuickCreditRequest(0).primaryPhoneNumber & "" <> "" Then
            xml_string = xml_string & "<homePhoneNumber>" & requestDS.QuickCreditRequest(0).primaryPhoneNumber & "</homePhoneNumber>"
        End If
        If requestDS.QuickCreditRequest(0).birthdayDate & "" <> "" Then
            xml_string = xml_string & "<birthdayDate>" & requestDS.QuickCreditRequest(0).birthdayDate & "</birthdayDate>"
        End If
        If requestDS.QuickCreditRequest(0).ssn & "" <> "" Then
            xml_string = xml_string & "<ssn>" & requestDS.QuickCreditRequest(0).ssn & "</ssn>"
        End If
        If requestDS.QuickCreditRequest(0).prescreenId & "" <> "" Then
            xml_string = xml_string & "<prescreenId>" & requestDS.QuickCreditRequest(0).prescreenId & "</prescreenId>"
        End If
        'xml_string = xml_string & "<swipeInd>" & requestDS.QuickCreditRequest(0).swipeInd & "</swipeInd>"
        xml_string = xml_string & "<posType>" & requestDS.QuickCreditRequest(0).posType & "</posType>"
        xml_string = xml_string & "<recordType>" & requestDS.QuickCreditRequest(0).recordType & "</recordType>"
        xml_string = xml_string & "</request>"
        xml_string = xml_string & "</ns1:quickCredit30>"
        xml_string = xml_string & "</soap:Body>"
        xml_string = xml_string & "</soap:Envelope>"

        _requestXML = New XmlDataDocument
        _requestXML.RemoveAll()
        _requestXML.LoadXml(xml_string)

        Dim reqBytes As Byte()
        reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
        oWebReq.ContentLength = reqBytes.Length
        oWebReq.Method = "POST"
        oWebReq.ContentType = "text/xml"
        oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
        Try
            Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
            oReqStream.Write(reqBytes, 0, reqBytes.Length)
            Dim responseBytes As Byte()
            Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
            If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
                responseBytes = ProcessResponse(oWebResp)
                BuildResponseDocument(responseBytes)
            End If
            oWebResp.Close()
        Catch ex As Exception
            If Err.Number = 5 Then
                Return _responseXML
                Exit Function
            End If
            Throw
        End Try
        Return _responseXML

    End Function

    Public Sub BuildResponseDocument(ByRef responseBytes As Byte())

        Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
        _responseXML.RemoveAll()
        _responseXML.LoadXml(strResp)

    End Sub

    Private Function ProcessResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()

        Dim memStream As New System.IO.MemoryStream()
        Const BUFFER_SIZE As Integer = 4096
        Dim iRead As Integer = 0
        Dim idx As Integer = 0
        Dim iSize As Int64 = 0
        memStream.SetLength(BUFFER_SIZE)
        While (True)
            Dim respBuffer(BUFFER_SIZE) As Byte
            Try
                iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
            Catch e As System.Exception
                Throw e
            End Try
            If (iRead = 0) Then
                Exit While
            End If
            iSize += iRead
            memStream.SetLength(iSize)
            memStream.Write(respBuffer, 0, iRead)
            idx += iRead
        End While

        Dim content As Byte() = memStream.ToArray()
        memStream.Close()
        Return content
    End Function

    Public Function ParseResponseAndFillDS(ByRef responseXML As XmlDocument)

        Dim rsaDS As New DataSet()
        Dim ResponseDS As New QuickCreditDataResponse
        rsaDS.ReadXml(New XmlNodeReader(responseXML))
        Dim responseAllianceDSDataRow As QuickCreditDataResponse.QuickCreditResponseRow
        If (Not rsaDS Is Nothing) And (rsaDS.Tables.Count > 0) Then
            If rsaDS.Tables(0).Rows.Count > 0 Then
                responseAllianceDSDataRow = ResponseDS.QuickCreditResponse.NewQuickCreditResponseRow
                Dim dr As DataRow = rsaDS.Tables(2).Rows(0)
                If dr.Table.Columns.Contains("correlationData") Then
                    responseAllianceDSDataRow.correlationData = dr("correlationData")
                End If
                If dr.Table.Columns.Contains("etailId") Then
                    responseAllianceDSDataRow.etailId = dr("etailId")
                End If
                If dr.Table.Columns.Contains("returnCode") Then
                    responseAllianceDSDataRow.returnCode = dr("returnCode")
                End If
                If dr.Table.Columns.Contains("returnMessage") Then
                    responseAllianceDSDataRow.returnMessage = dr("returnMessage")
                End If
                If dr.Table.Columns.Contains("applicationId") Then
                    responseAllianceDSDataRow.applicationId = dr("applicationId")
                End If
                If dr.Table.Columns.Contains("creditLimit") Then
                    responseAllianceDSDataRow.creditLimit = dr("creditLimit")
                End If
                If dr.Table.Columns.Contains("accountNumber") Then
                    responseAllianceDSDataRow.accountNumber = dr("accountNumber")
                End If
                If dr.Table.Columns.Contains("firstName") Then
                    responseAllianceDSDataRow.firstName = dr("firstName")
                End If
                If dr.Table.Columns.Contains("middleInitial") Then
                    responseAllianceDSDataRow.middleInitial = dr("middleInitial")
                End If
                If dr.Table.Columns.Contains("lastName") Then
                    responseAllianceDSDataRow.lastName = dr("lastName")
                End If
                If dr.Table.Columns.Contains("address1") Then
                    responseAllianceDSDataRow.address1 = dr("address1")
                End If
                If dr.Table.Columns.Contains("address2") Then
                    responseAllianceDSDataRow.address2 = dr("address2")
                End If
                If dr.Table.Columns.Contains("City") Then
                    responseAllianceDSDataRow.City = dr("City")
                End If
                If dr.Table.Columns.Contains("State") Then
                    responseAllianceDSDataRow.State = dr("State")
                End If
                If dr.Table.Columns.Contains("zipCode") Then
                    responseAllianceDSDataRow.zipCode = dr("zipCode")
                End If
                If dr.Table.Columns.Contains("creditCardType") Then
                    responseAllianceDSDataRow.creditCardType = dr("creditCardType")
                End If
                If dr.Table.Columns.Contains("creditCardDescription") Then
                    responseAllianceDSDataRow.creditCardDescription = dr("creditCardDescription")
                End If
                If dr.Table.Columns.Contains("creditTerm") Then
                    responseAllianceDSDataRow.creditTerm = dr("creditTerm")
                End If
                ResponseDS.QuickCreditResponse.Rows.Add(responseAllianceDSDataRow)
            End If
        End If

        Return ResponseDS
    End Function


End Class
