<%@ Application Language="VB" %>
<%@ Import Namespace = System.Web %>
<%@ Import Namespace = System.Web.SessionState %>
<%@ Import Namespace = System.Security.Principal %>
<%@ Import Namespace = ErrorManager %>

<script runat="server">

    Private theSystemBiz As SystemBiz = New SystemBiz()

    ' Code that runs on application startup
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        theSystemBiz.LoadSysPms()
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    
        'Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        ''log the error to the event log, database, and/or a file. The web.config specifies where to log it
        ''and this class will read those settings to determine that. 
        'IMSLogError.LogError()
        
        'Response.Redirect("Error_handling.aspx")

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>