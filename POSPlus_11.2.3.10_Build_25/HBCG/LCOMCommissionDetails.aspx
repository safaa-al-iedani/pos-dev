<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
<%--<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="true"
    CodeFile="LCOMCommissionDetails.aspx.vb" Inherits="LCOMCommissionDetails"  %> --%>
<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="true"
    CodeFile="LCOMCommissionDetails.aspx.vb" Inherits="LCOMCommissionDetails"  UICulture ="auto" %>
<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">

    <table border="0" style="position: relative; width: 960px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="1" align="center">
                <dx:ASPxLabel ID="lbl_title_commission_details" runat="server" meta:resourcekey="lbl_title_commission_details" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan="1" align="center">
                <dx:ASPxLabel ID="lbl_title_commission_details2" runat="server" Text="" Visible="True" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan="1" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>

    <dx:ASPxGridView ID="GridViewCommissionDetails" runat="server" AutoGenerateColumns="False"  Width="100%" KeyFieldName="DEL_DOC_LN#"  ClientInstanceName="GridViewCommissionDetails" Visible="False">
    <SettingsPager Position="TopAndBottom">
        <PageSizeItemSettings Items="5, 10, 20" Visible="true" ShowAllItem="true" />
        </SettingsPager>

        <Columns>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%-- <dx:GridViewDataTextColumn Caption="LN#" FieldName="DEL_DOC_LN#" VisibleIndex="1" Width="40px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Split%" FieldName="SPLIT_PERC" VisibleIndex="2" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Qty" FieldName="QTY" VisibleIndex="3" Width="40px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Sku #" FieldName="ITM_CD" VisibleIndex="4" Width="100px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Type" FieldName="ITM_TP_CD" VisibleIndex="5" Width="40px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="ITM_DES" VisibleIndex="6" Width="350px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Unit Price" FieldName="UNIT_PRICE" VisibleIndex="7" Width="70px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Ext Price" FieldName="EXT_PRICE" VisibleIndex="8" Width="70px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Comm Cd" FieldName="COMM_CD" VisibleIndex="9" Width="50px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Comm %" FieldName="COMMISSION_PERC" VisibleIndex="10" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Comm $" FieldName="COMMISSION$" VisibleIndex="11" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Spiff $" FieldName="SPIFF$" VisibleIndex="12" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn> --%>
           <dx:GridViewDataTextColumn  FieldName="DEL_DOC_LN#" VisibleIndex="1" Width="40px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" meta:resourcekey="gv_col_ln">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_split" FieldName="SPLIT_PERC" VisibleIndex="2" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_qty" FieldName="QTY" VisibleIndex="3" Width="40px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_sku" FieldName="ITM_CD" VisibleIndex="4" Width="100px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_type" FieldName="ITM_TP_CD" VisibleIndex="5" Width="40px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_descrition" FieldName="ITM_DES" VisibleIndex="6" Width="350px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_unit_price" FieldName="UNIT_PRICE" VisibleIndex="7" Width="70px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_ext_price" FieldName="EXT_PRICE" VisibleIndex="8" Width="70px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_comm_cd"  FieldName="COMM_CD" VisibleIndex="9" Width="50px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_comm_price" FieldName="COMMISSION_PERC" VisibleIndex="10" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_commission" FieldName="COMMISSION$" VisibleIndex="11" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_spiff" FieldName="SPIFF$" VisibleIndex="12" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
            </dx:GridViewDataTextColumn>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
        </Columns>
        <SettingsBehavior AllowSort="False" />
<%--        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
--%>
        </dx:ASPxGridView>

    
<%--
        <table width="100%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
        </table>
--%>
    <table border="0" style="position: relative; width: 960px; left: 0px; top: 0px;">
        <tr>
            <td colspan="1" align="center">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxButton ID="btnGoBackToSummary" OnClick="GoBackToSummaryClick" runat="server" Text="Back to Summary" ToolTip="Go back to summary page" CommandArgument="Back" Visible="True"> --%>
                <dx:ASPxButton ID="btnGoBackToSummary"  OnClick="GoBackToSummaryClick" runat="server" Text="Back to Summary" CommandArgument="Back" Visible="True" meta:resourcekey="lbl_btn_back_to_summary">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
              </dx:ASPxButton>
            </td>
        </tr>
    </table>


    </div>
<%--
    <div runat="server" id="div_cbo_records">
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;">
            <asp:Label ID="lbl_resultsInfo2" runat="server" Text="" Visible="False"></asp:Label>
            Records to Show:&nbsp;<asp:DropDownList ID="cbo_records" runat="server" AutoPostBack="True"
                CssClass="style5" Style="position: relative">
                <asp:ListItem Selected="True" Value="5">5</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="100">100</asp:ListItem>
                <asp:ListItem Value="9999">ALL</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
--%>
    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>
