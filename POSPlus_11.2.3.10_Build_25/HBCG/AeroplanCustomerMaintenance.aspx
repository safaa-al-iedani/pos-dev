<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="AeroplanCustomerMaintenance.aspx.vb" Inherits="AeroplanCustomerMaintenance" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Aeroplan Customer Maintenance" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
       <br />
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 
     <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
             </tr>
     </table>
    <br />
     <%-- ===============search criteria CUST_CD, AEROPLAN#, ORDER# ==========================================   --%> 
    <asp:Panel ID="Panel5" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >
        <table style="table-layout:fixed" cellpadding="4" cellspacing="1"  >
          <tr>
               <td> 
                <%-- CUST_CD_list --%>
                <dx:ASPxLabel ID="cust_cd_lbl" text="Customer Code" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
              </td>
              <td>
                   <asp:TextBox ID="srch_cust_cd" runat="server"></asp:TextBox>
                </td>
           </tr>
        <tr>
            <td style="width:60px" >
                <%-- AEROPLAN# --%>
                <dx:ASPxLabel ID="ASPxLabel1" Text="Aeroplan Number" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
                 </td>
              <td>
                <asp:TextBox ID="srch_aeroplan_crd_num" runat="server"></asp:TextBox>
                </td>
             </tr>
        <tr>
            <td>
             <%-- ORDER  --%>
                <dx:ASPxLabel ID="ASPxLabel2" Text="Sales Order" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
             </td>
              <td>
              <asp:TextBox ID="srch_order_no" runat="server"></asp:TextBox>
            </td>
            <td>
             <%-- contract no.  --%>
                <dx:ASPxLabel ID="ASPxLabel3" Text="Contract No." Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
             </td>
              <td>
              <asp:TextBox ID="srch_contract" runat="server"></asp:TextBox>
            </td>
        </tr>
            <%-- SEARCH BUTTON --%>
            <tr>
         <td style="width: 60px;">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="Search">
                </dx:ASPxButton>
         </td>
         <%-- CLEAR BUTTON --%>
         <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Font-Bold="true" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
         </td>          
        </tr>
       </table>
       </asp:Panel>
    <br />
    <br />
    <table >
        <tr>
            <%-- ADD NEW ORDER --%>
           
            <td style="width: 100px;" align="right" > 
                <dx:ASPxButton ID="btn_add_order" runat="server" ForeColor="red" Font-Bold="true" Text="Add Order">
                </dx:ASPxButton>
            </td>
            
        </tr>
    </table>
       <%-- =====cust_cd, del_doc_num,AEROPLAN_CRD_NUM,purch_dt,sls_amt,MPC_CODE,base_miles,bonus miles,finaldt,status --%>
    <asp:Panel ID="Panel3" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  

    <%-- gridview --%>
     <%--  sabrina Oct,2016 R2670 <dx:ASPxGridView ID="GridView1" Settings-ShowFooter="true" runat="server" keyfieldname="DEL_DOC_NUM;ITM_CD;SLS_AMT;ROW_ID" AutoGenerateColumns="False" --%>
           <dx:ASPxGridView ID="GridView1" Settings-ShowFooter="true" runat="server" keyfieldname="DEL_DOC_NUM;ITM_CD;CONTRACT;SLS_AMT;MILES;STAT_CD;ROW_ID" AutoGenerateColumns="False"
          Width="98%" ClientInstanceName="GridView1" oncelleditorinitialize="GridView1_CellEditorInitialize"   
            OnRowUpdating="ASPxGridView1_RowUpdating" OnRowValidating="rowvalidation" >
        <SettingsEditing mode="EditForm" />
        <Columns>
          <dx:GridViewDataTextColumn Caption="Customer" FieldName="CUST_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100"  MaxLength="10">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Order" FieldName="DEL_DOC_NUM" VisibleIndex="3">
                <PropertiesTextEdit Width="160" MaxLength="14">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Contract"  FieldName="CONTRACT" VisibleIndex="4">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" MaxLength="12">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Aeroplan#" FieldName="AEROPLAN_CRD_NUM" VisibleIndex="5">
                <PropertiesTextEdit Width="90"  MaxLength="9">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="<%$ Resources:LibResources, Label909 %>" />
                    </ValidationSettings>
                </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
          <dx:GridViewDatadateColumn Caption="Purchase DT" FieldName="SO_WR_DT" VisibleIndex="6">
          <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy" >  
           <ValidationSettings>
           <RequiredField ErrorText="Please select a Date" IsRequired="true" /> 
           </ValidationSettings> 
           </PropertiesDateEdit>
         </dx:GridViewDatadateColumn>
         <dx:GridViewDataTextColumn Caption="Item CD" FieldName="ITM_CD" VisibleIndex="6">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" Width="150" MaxLength="12">
             </PropertiesTextEdit>
             <CellStyle Wrap="False" />
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="MPC CD" FieldName="MPC_CODE" VisibleIndex="8">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="4">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Sales Amt." FieldName="SLS_AMT" VisibleIndex="9">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="15">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Miles" FieldName="MILES" VisibleIndex="10">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="12">
                </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <%--<dx:GridViewDataTextColumn Caption="Bonus Miles" FieldName="BNS_MILES" VisibleIndex="10">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="12">
         </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>--%>
         <dx:GridViewDataTextColumn Caption="Final DT" FieldName="FINAL_DT" VisibleIndex="12">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" DisplayFormatString="MM/dd/yyyy"  MaxLength="12">
              </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Status" FieldName="STAT_CD" VisibleIndex="13">
                <PropertiesTextEdit Width="20"  MaxLength="1">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
        <%-- ROWID ** sy note** THIS FIELD IS ADDED TO HANDLE IF mpc IS NULL (THESE ARE PART OF KEY FIELD)--%>
             <dx:GridViewDataTextColumn Caption="" FieldName="ROW_ID" ReadOnly="true" Visible="false" VisibleIndex="17">
             </dx:GridViewDataTextColumn>
           

          <dx:GridViewCommandColumn ShowEditButton="true"  VisibleIndex="15"/>
        </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager  Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
        <%--<SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>


           <%-- ----------------------BROWSE grid-------------------------------------------- --%>

           <%-- gridview --%>
      <%-- sabrina Oct,2016 R2670 <dx:ASPxGridView ID="GridView9" Settings-ShowFooter="true" runat="server" keyfieldname="DEL_DOC_NUM;ITM_CD;SLS_AMT;ROW_ID" --%>
            <dx:ASPxGridView ID="GridView9" Settings-ShowFooter="true" runat="server" keyfieldname="DEL_DOC_NUM;ITM_CD;CONTRACT;SLS_AMT;MILES;STAT_CD;ROW_ID"
           AutoGenerateColumns="False" Width="99%" ClientInstanceName="GridView9">
        <Columns>
          <dx:GridViewDataTextColumn Caption="Customer" FieldName="CUST_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100"  MaxLength="10"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Order" FieldName="DEL_DOC_NUM" VisibleIndex="3">
                <PropertiesTextEdit Width="160" MaxLength="14"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Contract"  FieldName="CONTRACT" VisibleIndex="4">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" MaxLength="12">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Aeroplan#" FieldName="AEROPLAN_CRD_NUM" VisibleIndex="5">
                <PropertiesTextEdit Width="90"  MaxLength="9">   </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Purchase DT" FieldName="SO_WR_DT" VisibleIndex="6">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" DisplayFormatString="MM/dd/yyyy" MaxLength="12">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Item CD" FieldName="ITM_CD" VisibleIndex="6">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" Width="150" MaxLength="12" >
                </PropertiesTextEdit>
             <CellStyle Wrap="False" />
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="MPC CD" FieldName="MPC_CODE" VisibleIndex="8">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="4">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Sales Amt." FieldName="SLS_AMT" VisibleIndex="9">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="15">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Miles" FieldName="MILES" VisibleIndex="10">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver"  MaxLength="12">
                </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Final DT" FieldName="FINAL_DT" VisibleIndex="12">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Silver" DisplayFormatString="MM/dd/yyyy"  MaxLength="12">
              </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Status" FieldName="STAT_CD" VisibleIndex="13">
                <PropertiesTextEdit Width="20"  MaxLength="1">   </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
        <%-- ROWID ** sy note** THIS FIELD IS ADDED TO HANDLE IF mpc IS NULL (THESE ARE PART OF KEY FIELD)--%>
             <dx:GridViewDataTextColumn Caption="" FieldName="ROW_ID" ReadOnly="true" Visible="false" VisibleIndex="17">
             </dx:GridViewDataTextColumn>
          </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager  Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
      
    </dx:ASPxGridView>
    </asp:Panel>


<%-- ===============POPUP===========add new order=========================================   --%>  
  <dxpc:ASPxPopupControl ID="ASPxPopupAddOrder" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Add Order" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
         <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
        <asp:Panel ID="Panel1" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
            <br />
            <br />
            <br />
<%-- message --%>
       <table>
         <dx:ASPxLabel ID="lbl_msg2" Font-Bold="True" Font-Size="Small" BackColor="White" runat="server"
        Text="" Width="100%"> 
        </dx:ASPxLabel>
        </table> 
            <br />
            <br />
<%-- checkbox --%>
      <table>
         <tr>
          <td>
           <asp:Label ID="Label4" runat="server" Text="Search for Order?"></asp:Label>
            <asp:CheckBox ID="chkbox_srch" AutoPostBack="true" OnCheckedChanged="DoSearchOrder" runat="server" />
          </td>
        </tr>
      </table>
           <br />
            <br />
<%-- NEW ORDER, CUST --%>
   <table>
    <tr>
        <td style="width: 60px;">
         <asp:Label ID="Label3" runat="server" Text="Customer Code"></asp:Label>
         <asp:TextBox ID="new_cust_cd" ReadOnly="false" Enabled="true" runat="server"></asp:TextBox>
        </td>
       <td style="width: 60px;">
         <asp:Label ID="Label6" runat="server" Text="Aeroplan No."></asp:Label>
         <asp:TextBox ID="new_aeroplan_crd_num" ReadOnly="false" Enabled="true" runat="server"></asp:TextBox>
       </td>
       <td style="width: 60px;">
         <asp:Label ID="Label2" runat="server" Text="Sales Order"></asp:Label>
         <asp:TextBox ID="new_del_doc_num" Width="200px" ReadOnly="false" Enabled="true" runat="server"></asp:TextBox>
         <asp:DropDownList ID="new_del_doc_drp_list" Width="200px" runat="server" AutoPostBack="True" AppendDataBoundItems="true" >                
         </asp:DropDownList>
      </td>
    </tr>

     <tr>
<%-- search BUTTON --%>
         <td style="width: 60px;">
           <dx:ASPxButton ID="btn2_search" runat="server" Text="Search">
           </dx:ASPxButton>
         </td>
<%-- Addorder BUTTON --%>
        <td style="width: 60px;">
           <dx:ASPxButton ID="btn2_add" runat="server" Text="Add Order">
           </dx:ASPxButton>
         </td>
<%-- CLEAR BUTTON --%>
         <td>
           <dx:ASPxButton ID="btn2_clear" runat="server" Text="Clear">
           </dx:ASPxButton>
         </td>                       
      </tr>
     </table>
     </asp:Panel>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
    <div>
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
