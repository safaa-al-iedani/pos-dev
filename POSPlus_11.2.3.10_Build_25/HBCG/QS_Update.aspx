<%@ Page Language="VB" AutoEventWireup="false" CodeFile="QS_Update.aspx.vb" Inherits="QS_Update"
    MasterPageFile="~/Regular.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="delivery" runat="server">
        <asp:Label ID="lblDesc" runat="server" Text="Please enter the date range you would like to view Pending Quick Screens for."
            Width="508px" Height="9px"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="txt_sdate" runat="server" Width="75px" CssClass="style5"></asp:TextBox>
        through
        <asp:TextBox ID="txt_edate" runat="server" Width="75px" CssClass="style5"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="style5" OnClick="btnSubmit_Click" />
        <br />
        <br />
        <asp:DataGrid ID="GridView1" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Height="280px" Width="100%" Font-Size="XX-Small" BorderStyle="None" BorderColor="White"
            BorderWidth="0px" CaptionAlign="Left">
            <Columns>
                <asp:BoundColumn DataField="EMP_CD" SortExpression="Emp Code" HeaderText="Emp Code">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CUST_CD" SortExpression="Customer Code" HeaderText="Customer Code">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="STORE_CD" SortExpression="Store Code" HeaderText="Store Code">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TRAN_DT" SortExpression="TRAN_DT" HeaderText="Transaction Date" DataFormatString="{0:MM/dd/yyyy}">
                </asp:BoundColumn>
                 <asp:TemplateColumn HeaderText="GE Account">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:TextBox ID="txt_ge_acct" runat="server" CssClass="style5" BackColor="Transparent" BorderStyle="None" ReadOnly="True" Width="90px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
                <ItemTemplate>
                    <asp:Button ID="txt_update" runat="server" Enabled="False" Text='Update To Approved' Width="137px" OnClick="Update_Approved" CssClass="style5"></asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
            
            </Columns>
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
        </asp:DataGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
