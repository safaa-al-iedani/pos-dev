﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.Business
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Script.Services

Partial Class CashDrawerBalancing
    Inherits POSBasePage

    <WebMethod> _
    Public Shared Function HasSecurityAccess(secCd As String, empCd As String) As Boolean

        Return SecurityUtils.hasSecurity(secCd, empCd)

    End Function

    <WebMethod> _
    Public Shared Function GetGpParmLabel(parm As String) As String

        Dim theSystemBiz As New SystemBiz()
        Return theSystemBiz.GetGpParm(SysPmsConstants.PTS_POS_KEY, parm)
    End Function

    <WebMethod> _
    Public Shared Function GetGpParmDepositSlipCopyName(parm As String) As String()

        Dim theSystemBiz As New SystemBiz()
        Return theSystemBiz.GetGpParmValues(SysPmsConstants.PTS_POS_KEY, parm)
    End Function

    <WebMethod> _
    Public Shared Function LoadCDB(empCd As String) As CashDrawerBalDtc
        Dim obj As New CashDrawerBalDtc

        obj.cdbAccess = New CashDrawerAccessDtc()
        obj.cdbAccess.empCd = empCd
        obj.cdbAccess.isCDBAccess = SecurityUtils.hasSecurity("CDB", empCd)
        obj.cdbAccess.isAllCshrAccess = SecurityUtils.hasSecurity("CDBALLCSHR", empCd)
        obj.cdbAccess.isDwrUpdAccess = SecurityUtils.hasSecurity("CDB_DWRUPD", empCd)
        obj.cdbAccess.isDetailAccess = SecurityUtils.hasSecurity("CDB_DETAIL", empCd)
        obj.cdbAccess.isUpdateAccess = SecurityUtils.hasSecurity("CDB_UPDATE", empCd)
        obj.cdbAccess.isCloseAccess = SecurityUtils.hasSecurity("DWR_CLOSE", empCd)
        obj.cdbAccess.isRePrtAccess = SecurityUtils.hasSecurity("CDB_REPRT", empCd)
        obj.cdbAccess.isBalDiffAccess = True 'SecurityUtils.hasSecurity("CDB_DIFF", empCd)
        obj.cdbAccess.isBalAccess = True 'SecurityUtils.hasSecurity("CDBBAL", empCd)
        obj.cdbAccess.isClsBalAccess = True 'SecurityUtils.hasSecurity("CDBCLSBAL", empCd)

        Dim objStoreBusiness As New StoreBusiness
        obj.storeList = objStoreBusiness.GetAllStore(empCd, Not SystemUtils.hasAllStoreAccess(empCd), String.Empty)

        Return obj
    End Function

    <WebMethod> _
    Public Shared Function GetStoreList(empCd As String, filterText As String) As StoreDtc()
        Dim objStoreBusiness As New StoreBusiness

        Return objStoreBusiness.GetAllStore(empCd, Not SystemUtils.hasAllStoreAccess(empCd), filterText)

    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function LoadStoreData(empCd As String, store As StoreDtc) As StoreDtc

       



        Dim str_co_grp_cd As String
        Dim str_co_cd As String
        Dim str_sup_user_flag As String
        Dim str_flag As String
        Dim p_co_cd As String
        str_flag = LeonsBiz.lucy_check_sup_user(UCase(empCd), str_co_grp_cd, str_co_cd, str_sup_user_flag) 'lucy use the function even not check super user

        p_co_cd = str_co_cd
        Dim objCashDrawerBusiness As New CashDrawerBalBusiness
        store.CashDrawerList = objCashDrawerBusiness.GetAllCashDrawer(store.storeCd, String.Empty)

        Dim objEmpBusiness As New EmpBusiness
        'store.CashierList = objEmpBusiness.GetAllCashier(empCd, SecurityUtils.hasSecurity("CDBALLCSHR", empCd))
        store.CashierList = objEmpBusiness.lucy_GetAllCashier(empCd, SecurityUtils.hasSecurity("CDBALLCSHR", empCd), p_co_cd) 'lucy

        Dim objCurrBusines As New CurrencyBusiness
        store.CurrencyList = objCurrBusines.GetAllCurrency(store.multiCurrency, store.defCurrCd)

        For Each curr As CurrencyDtc In store.CurrencyList
            curr.currencySymbol = CurrencyUtils.GetSymbol(curr.currencyCd)
        Next



        Return store
    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function GetSecurityAccess(cdbAccess As CashDrawerAccessDtc) As CashDrawerAccessDtc

        cdbAccess.isCDBAccess = SecurityUtils.hasSecurity("CDB", cdbAccess.empCd)
        cdbAccess.isAllCshrAccess = SecurityUtils.hasSecurity("CDBALLCSHR", cdbAccess.empCd)
        cdbAccess.isDwrUpdAccess = SecurityUtils.hasSecurity("CDB_DWRUPD", cdbAccess.empCd)
        cdbAccess.isDetailAccess = SecurityUtils.hasSecurity("CDB_DETAIL", cdbAccess.empCd)
        cdbAccess.isUpdateAccess = SecurityUtils.hasSecurity("CDB_UPDATE", cdbAccess.empCd)
        cdbAccess.isCloseAccess = SecurityUtils.hasSecurity("CDB_CLOSE", cdbAccess.empCd)
        cdbAccess.isRePrtAccess = SecurityUtils.hasSecurity("CDB_REPRT", cdbAccess.empCd)
        cdbAccess.isBalDiffAccess = True 'SecurityUtils.hasSecurity("CDB_DIFF", cdbAccess.empCd)
        cdbAccess.isBalAccess = True 'SecurityUtils.hasSecurity("CDBBAL", cdbAccess.empCd)
        cdbAccess.isClsBalAccess = True 'SecurityUtils.hasSecurity("CDBCLSBAL", cdbAccess.empCd)

        Return cdbAccess
    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function GetCashDrawerData(cashDrawer As CashDrawerBalDtc) As CashDrawerBalDtc

        Dim objCdbBusiness As New CdbBusiness

        If SysPms.isCashierActiv Then
            objCdbBusiness.IsCashDrawerOpen(cashDrawer)
        Else
            cashDrawer.isOpen = True
        End If

        objCdbBusiness.GetOpenBalance(cashDrawer)

        If ((Not cashDrawer.empCd.ToString.Equals(cashDrawer.loggedEmpCd.ToString)) And SecurityUtils.hasSecurity("CDBALLCSHR", cashDrawer.loggedEmpCd)) Then
            cashDrawer.isOpen = True
        End If

        Dim CashDrawerDepositBusiness As New CashDrawerDepositBusiness
        CashDrawerDepositBusiness.GetCashDrawerCash(cashDrawer)

        Dim objCsDwrDepCkBusiness As New CashDrawerDepositCkBusiness

        If SecurityUtils.hasSecurity("CDB_DETAIL", cashDrawer.loggedEmpCd.ToString) Then
            objCsDwrDepCkBusiness.GetCashDrawerCk(cashDrawer)
        End If

        Dim objTransactionBusiness As New TransactionBusiness

        objTransactionBusiness.GetCashDrawerOther(cashDrawer)
        objTransactionBusiness.GetTranscationTotal(cashDrawer)

        Dim objCdbPrintBusiness As New CashDrawerPrintBusiness
        objCdbPrintBusiness.GetPrintCount(cashDrawer)

        Return cashDrawer
    End Function

    '<WebMethod> _
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    'Public Shared Sub SaveOpenBalance(objCdbBal As CashDrawerBalDtc)
    '    Dim objCdbBusiness As New CdbBusiness
    '    objCdbBusiness.SaveOpenBalance(objCdbBal)
    'End Sub

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function GetTranscationBalance(objCdbBal As CashDrawerBalDtc, empCd As String) As TransactionDtc()

        Dim objTrans As New TransactionBusiness
        Return objTrans.GetTransactionBal(objCdbBal)

    End Function

    <WebMethod> _
    Public Shared Function GetMOPList() As MopDtc()

        Dim objMopBusiness As New MopBusiness

        Return objMopBusiness.GetMopList().ToArray()

    End Function

    <WebMethod> _
    Public Shared Function GetTransactionDetail(coCd As String, storeCd As String, CashDrawerCd As String, transDate As Date, mop As String) As TransactionDtc()

        Dim objTransactionBusiness As New TransactionBusiness

        Return objTransactionBusiness.GetTransactionDetail(coCd, storeCd, CashDrawerCd, transDate, mop)

    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Sub UpdateTransactionDetail(objTransList As List(Of TransactionDtc), empCd As String)
        If SecurityUtils.hasSecurity("CDB_UPDATE", empCd) Then
            Dim objTransactionBusiness As New TransactionBusiness
            objTransactionBusiness.UpdateTransactionDetail(objTransList)
        End If

    End Sub

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function InsertCashDrawerCash(objCashDrawerDepositDtc As CashDrawerDepositDtc, isInsert As String) As CashDrawerDepositDtc

        Dim CashDrawerDepositBusiness As New CashDrawerDepositBusiness

        Return CashDrawerDepositBusiness.InsertCashDrawerCash(objCashDrawerDepositDtc)

    End Function

    '<WebMethod> _
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    'Public Shared Sub InsertCashDrawerCheck(empCd As String, objCdbCkList As List(Of CashDrawerDepositCkDtc))

    '    Dim CashDrawerDepositCkBusiness As New CashDrawerDepositCkBusiness

    '    If SecurityUtils.hasSecurity("CDB_UPDATE", empCd) Then

    '        CashDrawerDepositCkBusiness.DeleteCashDrawerCheck(objCdbCkList(0))

    '        If Not (objCdbCkList.Count = 1 AndAlso objCdbCkList(0).seqenceNo = -1) Then
    '            CashDrawerDepositCkBusiness.InsertCashDrawerCheck(objCdbCkList)
    '        End If
    '    End If

    'End Sub

    '<WebMethod> _
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    'Public Shared Function SaveCDB(objCdbBal As Integer) As Integer
    '    Return objCdbBal
    'End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function SaveCDB(objCdbBal As CashDrawerBalDtc) As CashDrawerBalDtc

        Dim objCdbBusiness As New CdbBusiness
        objCdbBusiness.SaveOpenBalance(objCdbBal)

        Dim CashDrawerDepositBusiness As New CashDrawerDepositBusiness
        CashDrawerDepositBusiness.InsertCDBCash(objCdbBal)

        Dim CashDrawerDepositCkBusiness As New CashDrawerDepositCkBusiness

        'If SecurityUtils.hasSecurity("CDB_UPDATE", objCdbBal.empCd) Then

        CashDrawerDepositCkBusiness.DeleteCDBCheck(objCdbBal)

        If Not (objCdbBal.depositCk.Count = 1 AndAlso objCdbBal.depositCk(0).seqenceNo = -1) Then
            CashDrawerDepositCkBusiness.InsertCDBCheck(objCdbBal)
        End If
        'End If

        Return objCdbBal
    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Sub CloseCashDrawer(objCashDrawerBal As CashDrawerBalDtc, cashTotal As Decimal, cashOvrSht As Decimal, checkTotal As Decimal, checkOvrSht As Decimal)

        Dim objCashDrawerBalBusiness As New CashDrawerBalBusiness()

        objCashDrawerBalBusiness.UpdateCloseDrawerBalance(objCashDrawerBal)

        objCashDrawerBal.mopCd = "CS"
        objCashDrawerBal.mopAmount = cashTotal
        objCashDrawerBal.overShortAmt = cashOvrSht
        objCashDrawerBalBusiness.InsertCashDrawerBalMop(objCashDrawerBal)

        objCashDrawerBal.mopCd = "CK"
        objCashDrawerBal.mopAmount = checkTotal
        objCashDrawerBal.overShortAmt = checkOvrSht
        objCashDrawerBalBusiness.InsertCashDrawerBalMop(objCashDrawerBal)

        Dim objTransactionBusiness As New TransactionBusiness()
        objTransactionBusiness.BalanceTranscation(objCashDrawerBal.companyCd, objCashDrawerBal.storeCd, objCashDrawerBal.cashDrawerCd, objCashDrawerBal.transactionDate)
    End Sub

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function SavePrintData(cashDrawer As CashDrawerBalDtc) As CashDrawerBalDtc
        Dim objCdbPrintBusiness As New CashDrawerPrintBusiness
        objCdbPrintBusiness.SavePrintData(cashDrawer)

        Return cashDrawer
    End Function

    <WebMethod> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function PrintCashDrawerData(cashDrawer As CashDrawerBalDtc) As CashDrawerBalDtc

        Dim CashDrawerDepositBusiness As New CashDrawerDepositBusiness
        CashDrawerDepositBusiness.GetCashDrawerCash(cashDrawer)

        Dim objCsDwrDepCkBusiness As New CashDrawerDepositCkBusiness

        If SecurityUtils.hasSecurity("CDB_DETAIL", cashDrawer.loggedEmpCd.ToString) Then
            objCsDwrDepCkBusiness.GetCashDrawerCk(cashDrawer)
        End If

        'Dim objCdbPrintBusiness As New CashDrawerPrintBusiness
        'objCdbPrintBusiness.SavePrintData(cashDrawer)

        Return cashDrawer
    End Function

End Class
