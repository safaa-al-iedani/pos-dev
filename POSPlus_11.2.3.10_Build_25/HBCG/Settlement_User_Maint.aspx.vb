Imports System.Data
Imports System.Data.OracleClient
Imports System.Xml
Imports HBCG_Utils


Partial Class Settlement_User_Maint
    Inherits POSBasePage

    Protected Sub btn_add_user_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add_user.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim Proceed As Boolean = False

        conn.Open()

        Dim sql As String
        sql = "SELECT EMP_CD FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & cbo_new_users.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDatareader.Read()) Then
                If String.IsNullOrEmpty(MyDatareader.Item("EMP_CD").ToString) Then
                    Proceed = True
                End If
            Else
                Proceed = True
            End If
            MyDatareader.Close()
            If Proceed = True Then
                sql = "INSERT INTO SETTLEMENT_ADMIN (EMP_CD) VALUES('" & cbo_new_users.Value & "')"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objsql2.ExecuteNonQuery()
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Proceed = False Then
            lbl_errors.Text = "User already exists"
        Else
            lbl_errors.Text = "User " & cbo_new_users.Value & " was successfully added."
        End If
        bindgrid()
        conn.Close()

    End Sub

    Public Sub Update_User(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_ADMIN SET USER_MAINT='" & ck_value & "' WHERE EMP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_MOP(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_ADMIN SET MOP_MAINT='" & ck_value & "' WHERE EMP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_Promo(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_ADMIN SET PROMO_MAINT='" & ck_value & "' WHERE EMP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_Admin(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_ADMIN SET ADMIN_ACCESS='" & ck_value & "' WHERE EMP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT * FROM SETTLEMENT_ADMIN order by EMP_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub Populate_New_Adds()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "SELECT EMP_CD, LNAME || ', ' || FNAME AS FULL_DESC FROM EMP WHERE TERMDATE IS NULL ORDER BY LNAME, FNAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        With cbo_new_users
            .DataSource = ds
            .ValueField = "EMP_CD"
            .TextField = "FULL_DESC"
            .DataBind()
        End With
        conn.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("EMPM", Session("emp_cd")) = "N" Then
            lbl_errors.Text = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            cbo_new_users.Visible = False
            btn_add_user.Visible = False
            Exit Sub
        Else
            cbo_new_users.Visible = True
            btn_add_user.Visible = True
        End If
        If Not IsPostBack Then
            Populate_New_Adds()
            bindgrid()
        End If

    End Sub

    Public Shared Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT EMP_CD FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("EMP_CD") & "" <> "" Then
                    Authorize_User = True
                Else
                    Authorize_User = False
                End If
            Else
                Authorize_User = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String
        Dim intRow As Integer = 0
        Dim intRows As Integer = Gridview1.Items.Count - 1
        Dim bln_match As Boolean = False

        lbl_errors.Text = ""

        conn.Open()
        deletecd = Gridview1.DataKeys(e.Item.ItemIndex)

        With cmdDeleteItems
            .Connection = conn
            .CommandText = "DELETE FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & deletecd & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()

        cmdDeleteItems.Dispose()
        conn.Close()

        Response.Redirect("settlement_user_maint.aspx")

    End Sub

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim CheckBox1 As CheckBox = CType(e.Item.FindControl("CheckBox1"), CheckBox)
        Dim CheckBox2 As CheckBox = CType(e.Item.FindControl("CheckBox2"), CheckBox)
        Dim CheckBox3 As CheckBox = CType(e.Item.FindControl("CheckBox3"), CheckBox)
        Dim CheckBox4 As CheckBox = CType(e.Item.FindControl("CheckBox4"), CheckBox)

        If Not IsNothing(CheckBox1) And Not IsNothing(CheckBox2) Then
            If e.Item.Cells(5).Text = "Y" Then
                CheckBox1.Checked = True
            End If
            If e.Item.Cells(6).Text = "Y" Then
                CheckBox2.Checked = True
            End If
            If e.Item.Cells(7).Text = "Y" Then
                CheckBox3.Checked = True
            End If
            If e.Item.Cells(8).Text = "Y" Then
                CheckBox4.Checked = True
            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
