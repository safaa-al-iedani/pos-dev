Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Business_Review
    Inherits POSBasePage

    Public Sub store_cd_update()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store "
            SQL = SQL & "WHERE STORE_CD IN(select store_grp$store.store_cd "
            SQL = SQL & "from store_grp$store, emp2store_grp "
            SQL = SQL & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
            SQL = SQL & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            SQL = SQL & " order by store_cd"
        Else
            SQL = "SELECT store_grp_cd as store_cd, des, store_grp_cd || ' - ' || des as full_desc FROM store_grp order by store_grp_cd"
        End If

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_Cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
                .SelectedIndex = 0
            End With
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub Populate_Sales()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim sql As String
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim oAdp As OracleDataAdapter

        Try
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn2.Open()

            'sql = "SELECT  so.so_wr_dt written_dt, "
            'sql = sql & "so.so_store_cd store_cd, "
            'sql = sql & "COUNT(DISTINCT(so.del_doc_num)) transactions, "
            'sql = sql & "SUM(DECODE(so.ord_tp_cd, "
            'sql = sql & "'MCR',((sl.qty * sl.unit_prc)*-1), "
            'sql = sql & "'CRM',((sl.qty * sl.unit_prc)*-1), "
            'sql = sql & "sl.qty*sl.unit_prc)) amount, "
            'sql = sql & "SUM(DECODE(so.ord_tp_cd, "
            'sql = sql & "'CRM',(NVL(sl.unit_prc*sl.qty,0)- "
            'sql = sql & "NVL(NVL(sl.fifo_cst,itm.repl_cst)*sl.qty,0))*-1, "
            'sql = sql & "'MCR',NVL(sl.unit_prc*sl.qty,0)*-1, "
            'sql = sql & "'MDB',NVL(sl.unit_prc*sl.qty,0), "
            'sql = sql & "(NVL(sl.unit_prc*sl.qty,0)-NVL(NVL(sl.fifo_cst,itm.repl_cst)"
            'sql = sql & "*sl.qty,0)))) gross_margin "
            'sql = sql & "FROM  so, so_ln sl, itm, store "
            'sql = sql & "WHERE  so.del_doc_num = sl.del_doc_num (+) "
            'sql = sql & "AND  sl.itm_cd      = itm.itm_cd(+) "
            'sql = sql & "AND  so.ord_tp_cd IN ('SAL','MCR','MDB','CRM')                 "
            'sql = sql & "AND so.SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD=:v_store_grp_cd) "
            'sql = sql & "AND STORE.STORE_CD(+)=SO.SO_STORE_CD "
            'sql = sql & "AND  nvl(addon_wr_dt,so.so_wr_dt )BETWEEN :v_tmp_beg_wr_dt "
            'sql = sql & "AND  :v_tmp_end_wr_dt  "
            'sql = sql & "GROUP BY  so.so_wr_dt, so.so_store_cd"

            'sql = "select so_store_cd, DES, COUNT(DISTINCT SALES_QTY) AS SALES_QTY, sum(SALES_DOL) AS SALES_DOL, SALES_GM, NULL AS SALES_GM_PCT FROM "
            'sql = sql & "("
            sql = "select so.so_store_cd, STORE.STORE_NAME AS DES, COUNT(DISTINCT SO.DEL_DOC_NUM) AS SALES_QTY, SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(so_ln.unit_prc,0)) as SALES_DOL, " 'SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(so_ln.unit_prc,0))-SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(DECODE(so_ln.fifo_cst,NULL,itm.REPL_CST,so_ln.fifo_cst),0)) as SALES_GM, "
            sql = sql & "NULL AS SALES_GM_PCT, "
            sql = sql & "SUM(DECODE(so.ord_tp_cd, "
            sql = sql & "'CRM',(NVL(so_ln.unit_prc*so_ln.qty,0)- "
            sql = sql & "NVL(NVL(so_ln.fifo_cst,itm.repl_cst)*so_ln.qty,0))*-1, "
            sql = sql & "'MCR',NVL(so_ln.unit_prc*so_ln.qty,0)*-1, "
            sql = sql & "'MDB',NVL(so_ln.unit_prc*so_ln.qty,0), "
            sql = sql & "(NVL(so_ln.unit_prc*so_ln.qty,0)-NVL(NVL(so_ln.fifo_cst,itm.repl_cst)"
            sql = sql & "*so_ln.qty,0)))) SALES_GM "
            sql = sql & "from so, so_ln, itm, store where so.del_doc_num = so_ln.del_doc_num and so.so_wr_dt between TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') "
            sql = sql & "AND ITM.ITM_CD=SO_LN.ITM_CD AND SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_Cd.Value & "') "
            sql = sql & "AND STORE.STORE_CD(+)=SO.SO_STORE_CD "
            'sql = sql & "GROUP BY SO.SO_STORE_CD "
            'sql = sql & "union all "
            'sql = sql & "SELECT so.so_store_cd, STORE.STORE_NAME AS DES, 0 AS SALES_QTY,       "
            'sql = sql & "DECODE(so.ord_tp_cd,'MCR',((sl.qty * sl.unit_prc)*-1), "
            'sql = sql & "'CRM',((sl.qty * sl.unit_prc)*-1),sl.qty*sl.unit_prc) SALES_DOL, "
            'sql = sql & "NULL AS SALES_GM_PCT, "
            'sql = sql & "NULL AS SALES_GM "
            'sql = sql & "FROM SO_LN SL,  SO ,ITM, STORE "
            'sql = sql & "WHERE SO.DEL_DOC_NUM = SL.DEL_DOC_NUM(+) "
            'sql = sql & "AND  sl.itm_cd      = itm.itm_cd(+) "
            'sql = sql & "AND (SO.STAT_CD = 'V')  "
            'sql = sql & "AND STORE.STORE_CD(+)=SO.SO_STORE_CD "
            'sql = sql & "AND (SO.FINAL_DT BETWEEN TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') "
            'sql = sql & "and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
            'sql = sql & "AND SL.VOID_DT IS NULL) "
            'sql = sql & "GROUP BY SO.SO_STORE_CD "
            'sql = sql & "union all "
            'sql = sql & "SELECT so.so_store_cd, STORE.STORE_NAME AS DES, 0 AS SALES_QTY, "
            'sql = sql & "DECODE(so.ord_tp_cd,'MCR',((sl.qty * sl.unit_prc)*-1), 'CRM',((sl.qty * sl.unit_prc)*-1),sl.qty*sl.unit_prc) SALES_DOL, "
            'sql = sql & "NULL AS SALES_GM_PCT, "
            'sql = sql & "NULL AS SALES_GM "
            'sql = sql & "FROM SO_LN SL,  SO ,ITM, STORE "
            'sql = sql & "WHERE SO.DEL_DOC_NUM = SL.DEL_DOC_NUM(+) "
            'sql = sql & "AND  sl.itm_cd      = itm.itm_cd(+) "
            'sql = sql & "AND ( SL.VOID_FLAG ='Y')  "
            'sql = sql & "AND STORE.STORE_CD(+)=SO.SO_STORE_CD "
            'sql = sql & "AND (SL.VOID_DT BETWEEN TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') "
            'sql = sql & "and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR')) "
            'sql = sql & "GROUP BY SO.SO_STORE_CD "
            'sql = sql & ") "
            sql = sql & "GROUP BY SO_STORE_CD, STORE.STORE_NAME ORDER BY SO_STORE_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            'objSql.Parameters.Add(":v_store_grp_cd", OracleType.VarChar)
            'objSql.Parameters(":v_store_grp_cd").Value = cbo_store_Cd.Value
            'objSql.Parameters.Add(":v_tmp_beg_wr_dt", OracleType.DateTime)
            'objSql.Parameters(":v_tmp_beg_wr_dt").Value = txt_start_dt.Text
            'objSql.Parameters.Add(":v_tmp_end_wr_dt", OracleType.DateTime)
            'objSql.Parameters(":v_tmp_end_wr_dt").Value = txt_end_dt.Text

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            GridView4.DataSource = ds
            GridView4.DataBind()

            sql = "select SO_EMP_SLSP_CD1, COUNT(DISTINCT DEL_DOC_NUM) AS SALES_QTY, sum(unit_prc*qty) AS SALES_DOL,  sum(unit_prc*qty)-SUM(SALES_GM*qty) as SALES_GM, NULL AS SALES_GM_PCT FROM "
            sql = sql & "("
            sql = sql & "select so.SO_EMP_SLSP_CD1, nvl(so_ln.unit_prc,0)*(nvl(so.pct_of_sale1,0)/100) as unit_prc, nvl(DECODE(so_ln.fifo_cst,NULL,itm.REPL_CST,so_ln.fifo_cst),0)*(nvl(so.pct_of_sale1,0)/100) as SALES_GM, DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty) as qty, so.pct_of_sale1, so.del_Doc_num, ord_tp_cd, so.stat_cd "
            sql = sql & "from so, SO_LN, itm "
            sql = sql & "where so_wr_dt between TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') and so_ln.void_flag='N' "
            sql = sql & "and so.del_doc_num=so_ln.del_doc_num AND ITM.ITM_CD=SO_LN.ITM_CD AND SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_Cd.Value & "') and so.stat_cd <> 'V' "
            sql = sql & "union all "
            sql = sql & "select so.SO_EMP_SLSP_CD2 AS SO_EMP_SLSP_CD1, nvl(so_ln.unit_prc,0)*(nvl(so.pct_of_sale2,0)/100) as unit_prc, nvl(DECODE(so_ln.fifo_cst,NULL,itm.REPL_CST,so_ln.fifo_cst),0)*(nvl(so.pct_of_sale2,0)/100) as SALES_GM, DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty) as qty, so.pct_of_sale2, so.del_Doc_num, ord_tp_cd, so.stat_cd "
            sql = sql & "from so, SO_LN, itm "
            sql = sql & "where so_wr_dt between TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') and so_ln.void_flag='N' and so.SO_EMP_SLSP_CD2 IS NOT NULL AND NVL(SO.PCT_OF_SALE2,0) > 0 and so.stat_cd <> 'V' "
            sql = sql & "and so.del_doc_num=so_ln.del_doc_num AND ITM.ITM_CD=SO_LN.ITM_CD AND SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_Cd.Value & "') and SO_LN.VOID_FLAG='N' "
            sql = sql & ") "
            sql = sql & "GROUP BY SO_EMP_SLSP_CD1"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds1)
            DataGrid1.DataSource = ds1
            DataGrid1.DataBind()

            sql = "select itm.mnr_cd, INV_MNR.DES AS DES, COUNT(DISTINCT SO.DEL_DOC_NUM) AS SALES_QTY, SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(so_ln.unit_prc,0)) as SALES_DOL, SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(so_ln.unit_prc,0))-SUM(DECODE(so.ord_tp_cd,'SAL',so_ln.qty,'MDB',so_ln.qty,'MCR',-so_ln.qty,'CRM',-so_ln.qty,so_ln.qty)*nvl(DECODE(so_ln.fifo_cst,NULL,itm.REPL_CST,so_ln.fifo_cst),0)) as SALES_GM, NULL AS SALES_GM_PCT "
            sql = sql & "from so, so_ln, itm, inv_mnr where so.del_doc_num = so_ln.del_doc_num and so.so_wr_dt between TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') "
            sql = sql & "and TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') and so.ord_tp_cd in ('SAL','CRM','MDB','MCR') "
            sql = sql & "AND ITM.ITM_CD=SO_LN.ITM_CD AND SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD='" & cbo_store_Cd.Value & "') "
            sql = sql & "AND INV_MNR.MNR_CD(+)=ITM.MNR_CD AND SO_LN.VOID_FLAG='N' "
            sql = sql & "GROUP BY itm.mnr_cd, INV_MNR.DES ORDER BY itm.mnr_cd"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds2)
            DataGrid2.DataSource = ds2
            DataGrid2.DataBind()
        Catch
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("WBQ", Session("emp_cd")) = "N" Then
            btn_submit.Enabled = False
            lbl_warning.Text = "You do not have access to this page."
            Exit Sub
        End If
        If Not IsPostBack Then
            txt_start_dt.Text = FormatDateTime(Now.Date, DateFormat.ShortDate)
            txt_end_dt.Text = FormatDateTime(Now.Date, DateFormat.ShortDate)
            store_cd_update()
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Populate_Sales()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Protected Sub GridView4_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView4.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        Dim GM_Sec As String = ""
        If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))

        If GM_Sec = "N" Then
            e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
        End If

        If IsNumeric(e.GetValue("SALES_DOL").ToString()) And IsNumeric(e.GetValue("SALES_GM").ToString()) Then
            Dim lbl_store_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView4.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_SALES_GM_PCT"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_store_cd) Then
                lbl_store_cd.Text = FormatPercent(1 - (CDbl(e.GetValue("SALES_DOL").ToString()) - CDbl(e.GetValue("SALES_GM").ToString())) / CDbl(e.GetValue("SALES_DOL").ToString()), 2)
            End If
        End If
        If IsNumeric(e.GetValue("SALES_DOL").ToString()) And IsNumeric(e.GetValue("SALES_QTY").ToString()) Then
            Dim lbl_avg_sale As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView4.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_avg_sale"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_avg_sale) Then
                lbl_avg_sale.Text = FormatNumber((CDbl(e.GetValue("SALES_DOL").ToString()) / CDbl(e.GetValue("SALES_QTY").ToString())), 2)
            End If
        End If

    End Sub

    Protected Sub DataGrid1_HtmlRowPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles DataGrid1.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        Dim GM_Sec As String = ""
        If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))

        If GM_Sec = "N" Then
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
        End If

        If IsNumeric(e.GetValue("SALES_DOL").ToString()) And IsNumeric(e.GetValue("SALES_GM").ToString()) Then
            Dim lbl_store_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(DataGrid1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_SALES_GM_PCT_EMP"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_store_cd) Then
                lbl_store_cd.Text = FormatPercent(1 - (CDbl(e.GetValue("SALES_DOL").ToString()) - CDbl(e.GetValue("SALES_GM").ToString())) / CDbl(e.GetValue("SALES_DOL").ToString()), 2)
            End If
        End If
        If IsNumeric(e.GetValue("SALES_DOL").ToString()) And IsNumeric(e.GetValue("SALES_QTY").ToString()) Then
            Dim lbl_avg_sale As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(DataGrid1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_avg_sale"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_avg_sale) Then
                lbl_avg_sale.Text = FormatNumber((CDbl(e.GetValue("SALES_DOL").ToString()) / CDbl(e.GetValue("SALES_QTY").ToString())), 2)
            End If
        End If

    End Sub

    Protected Sub DataGrid2_HtmlRowPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles DataGrid2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        Dim GM_Sec As String = ""
        If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))

        If GM_Sec = "N" Then
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
        End If

        If IsNumeric(e.GetValue("SALES_DOL").ToString()) And IsNumeric(e.GetValue("SALES_GM").ToString()) Then
            Dim lbl_store_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(DataGrid2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_SALES_GM_PCT_MNR"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_store_cd) Then
                lbl_store_cd.Text = FormatPercent(1 - (CDbl(e.GetValue("SALES_DOL").ToString()) - CDbl(e.GetValue("SALES_GM").ToString())) / CDbl(e.GetValue("SALES_DOL").ToString()), 2)
            End If
        End If

    End Sub
End Class
