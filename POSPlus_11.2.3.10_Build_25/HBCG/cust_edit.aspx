<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cust_edit.aspx.vb" Inherits="cust_edit"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function ShowSearch() {
            document.getElementById('ShippingInfo').style.display = '';
            document.getElementById('DownArrow').style.display = 'none';
            document.getElementById('UpArrow').style.display = '';
            document.getElementById('UpArrow').style.cursor = 'hand';
        }
        function HideSearch() {
            document.getElementById('ShippingInfo').style.display = 'none';
            document.getElementById('DownArrow').style.display = '';
            document.getElementById('DownArrow').style.cursor = 'hand';
            document.getElementById('UpArrow').style.display = 'none';
        }

        function ShowBilling() {
            document.getElementById('BillingInfo').style.display = '';
            document.getElementById('DownArrow1').style.display = 'none';
            document.getElementById('UpArrow1').style.display = '';
            document.getElementById('UpArrow1').style.cursor = 'hand';
        }
        function HideBilling() {
            document.getElementById('BillingInfo').style.display = 'none';
            document.getElementById('DownArrow1').style.display = '';
            document.getElementById('DownArrow1').style.cursor = 'hand';
            document.getElementById('UpArrow1').style.display = 'none';
        }

        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
    </script>

    <div>
        <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label543 %>" />
        <img id="DownArrow" title="Click here to expand shipping information" style="display: none; cursor: pointer;"
            alt="DownArrow" onclick="ShowSearch()" src="Images/icons/down_arrow.png" />&nbsp;
        <img id="UpArrow" title="Click here to hide shipping information" style="cursor: pointer;" alt="UpArrow"
            onclick="HideSearch()" src="Images/icons/up_arrow.png" />
        <div id="ShippingInfo">
            <table cellspacing="0" cellpadding="0" id="CustTable" runat="server" width="80%">
                <tr>
                    <td style="height: 17px" colspan="2">
                        <asp:CheckBox ID="chk_billing" runat="server" Width="23px" AutoPostBack="True" CssClass="style5" />
                        <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label48 %>" />
                    </td>
                    <td valign="middle" align="center" rowspan="10">
                        <asp:Image ID="img_warn" runat="server" ImageUrl="images/warning.jpg" /><asp:Label
                            ID="lbl_Warning" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label132 %>" />
                    </td>
                    <td style="height: 17px"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label140 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtCustCode" runat="server" Width="100px" ReadOnly="True" TabIndex="1"
                            CssClass="style5">
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="drp_cust_tp" runat="server" Enabled ="false"  TabIndex="2" CssClass="style5"
                            AutoPostBack="True">
                            <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label236 %>"></asp:ListItem>
                            <asp:ListItem Value="C" Text="<%$ Resources:LibResources, Label112 %>"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label112 %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txt_corp_name" runat="server" Width="250px" TabIndex="5" AutoPostBack="True"
                            MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label211 %>" />
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label263 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtFName" runat="server" Width="100px" TabIndex="3" CssClass="style5"
                            AutoPostBack="True" MaxLength="15"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="txtLName" runat="server" Width="100px" TabIndex="4" CssClass="style5"
                            AutoPostBack="True" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label22 %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtAddr" runat="server" Width="250px" TabIndex="5" CssClass="style5"
                            AutoPostBack="True" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtAddr2" runat="server" Width="250px" TabIndex="6" CssClass="style5"
                            AutoPostBack="True" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label79 %>" />
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label693 %>" />
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label683 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="City" runat="server" Width="100px" TabIndex="8" CssClass="style5"
                            AutoPostBack="True" MaxLength="20"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="State" runat="server" Width="100px" TabIndex="9" CssClass="style5"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="st_label" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Zip" runat="server" Width="100px" AutoPostBack="True" TabIndex="7"
                            CssClass="style5" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label229 %>" />
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label27 %>" />
                    </td>
                    <td valign="middle" align="center" rowspan="4"></td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtHomePhone" runat="server" Width="100px" TabIndex="10" CssClass="style5"
                            AutoPostBack="True" MaxLength="12"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBusPhone" runat="server" Width="100px" TabIndex="11" CssClass="style5"
                            AutoPostBack="True" MaxLength="12"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label178 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="Email" runat="server" Width="250px" TabIndex="12" CssClass="style5"
                            AutoPostBack="True" MaxLength="60"></asp:TextBox>
                    </td>
                    <td style="height: 17px">
                        <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label181 %>" />
                        <asp:CheckBox ID="EmailCheck" runat="server" Width="23px" AutoPostBack="True" CssClass="style5" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div>
        <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label47 %>" />&nbsp;&nbsp;&nbsp;&nbsp;
        <img id="DownArrow1" title="Click here to expand Billing Information" style="cursor: pointer;"
            alt="DownArrow1" onclick="ShowBilling()" src="Images/icons/down_arrow.png" />&nbsp;
        <img id="UpArrow1" title="Click here to hide billing information" style="cursor: pointer; display: none;"
            alt="UpArrow1" onclick="HideBilling()" src="Images/icons/up_arrow.png" />
        <div id="BillingInfo" style="display: none">
            <table width="80%" cellspacing="0" cellpadding="0">
                <tr>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label211 %>" />
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label263 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txt_bill_FName" runat="server" Width="100px" CssClass="style5" AutoPostBack="True"
                            MaxLength="15"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="txt_bill_LName" runat="server" Width="100px" CssClass="style5" AutoPostBack="True"
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label22 %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txt_bill_Addr" runat="server" Width="250px" CssClass="style5" AutoPostBack="True"
                            MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txt_bill_Addr2" runat="server" Width="250px" CssClass="style5" AutoPostBack="True"
                            MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label79 %>" />
                    </td>
                    <td>State
                    </td>
                    <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label683 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="bill_City" runat="server" Width="100px" CssClass="style5" AutoPostBack="True"
                            MaxLength="20"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="bill_state" runat="server" Width="100px" CssClass="style5"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="bill_st_label" runat="server" Visible="false" AutoPostBack="True"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="bill_Zip" runat="server" Width="100px" CssClass="style5" AutoPostBack="True"
                            MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:CheckBox ID="chk_tax_exempt" runat="server" Text="<%$ Resources:LibResources, Label590 %>" AutoPostBack="true"
        CssClass="style5" />
    <asp:DropDownList ID="cbo_tax_exempt" runat="server" Width="280px" AutoPostBack="true"
        Visible="false" CssClass="style5">
    </asp:DropDownList>
    <br />
    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label589 %>" />
    <asp:TextBox ID="txt_tax_exempt_id" maxlength="20" runat="server" Width="100px" Visible="false"
        AutoPostBack="True">
    </asp:TextBox>
    <br />
    <asp:Label ID="lbl_Error" runat="server" Text=""></asp:Label>&nbsp;
<br />
    <dx:ASPxButton ID="btn_Remove_Cust" runat="server" Text="<%$ Resources:LibResources, Label458 %>">
    </dx:ASPxButton>
    <br />
    <br />
    <%--Hide remove button warning --%>
     <asp:Label ID="removeWarning" runat="server" Text="<%$ Resources:LibResources, Label873 %>" Visible="False" forecolor="red"/>
     <asp:Button ID="lucy_btn_inspmt" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label814 %>" Visible="False" />
    <%--sabrina tdpl --%>
     <asp:Button ID="sy_btn_tdpl_inspmt" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label815 %>" Visible="False" />
     <%--sabrina gc transfer --%>
     <asp:Button ID="sy_btn_gc_transfer" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label816 %>" Visible="False" />
    <%--sabrina payr payment reversal --%>
     <asp:Button ID="sy_btn_payr" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label817 %>" Visible="False" />
     <%--sabrina R1922 gift card purchase --%>
     <asp:Button ID="sy_btn_gc_purchase" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label818 %>" Visible="False" />
    <%--sabrina AeroplanGoodwillMPC --%>
     <asp:Button ID="sy_btn_aerompc" runat="server" ForeColor="#CC00FF" Text="Select Cust" Visible="False" />

     <%--Alice added for request 167 -- for other invoice in Payment Application --%>
     <asp:Button ID="sy_btn_pa_other" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label1025 %>" Visible="False" />

    <br />
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="Reselect Customer?" Height="161px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px"
        CssClass="style5" ClientInstanceName="ASPxPopupControl1">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
   
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label694 %>" Height="413px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="676px"
        CssClass="style5">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <br />
    <dxpc:ASPxPopupControl ID="PopupTax" runat="server" HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None"
        CloseAction="CloseButton" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div align="center">
        &nbsp; &nbsp;
    </div>
    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
    <br />
    <br />
     <ucMsg:MsgPopup runat="server" ID="MsgPopupConvertion" />
    <br />
    <br />
</asp:Content>
