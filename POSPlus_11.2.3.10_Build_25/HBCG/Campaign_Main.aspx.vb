Imports System.Data.OracleClient

Partial Class Campaign_Campaign_Main
    Inherits POSBasePage

    Public Sub campaign_populate()
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        SQL = "SELECT CAMPAIGN_CD, DES FROM CAMPAIGN_TYPES WHERE AUDIT_TP = 'C' ORDER BY DES"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            Dim pkColumn(1) As DataColumn
            pkColumn(0) = ds.Tables(0).Columns("CAMPAIGN_CD")
            'set the primary key to the CustomerID column
            ds.Tables(0).PrimaryKey = pkColumn

            cbo_campaign.Items.Insert(0, "Please Select A Campaign")
            cbo_campaign.Items.FindByText("Please Select A Campaign").Value = ""

            With cbo_campaign
                .DataSource = ds
                .DataValueField = "CAMPAIGN_CD"
                .DataTextField = "DES"
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            campaign_populate()
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim Mydatareader As OracleDatareader
        Dim objsql As OracleCommand
        Dim SQL As String

        conn.Open()

        SQL = "SELECT DOC_LOCATION FROM CAMPAIGN WHERE CAMPAIGN_CD = :campaignValue"
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objsql.Parameters.Add(":campaignValue", OracleType.VarChar)
        objsql.Parameters(":campaignValue").Value = cbo_campaign.SelectedValue.ToString

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            If (Mydatareader.Read()) Then
                Response.Redirect(Mydatareader.Item("DOC_LOCATION").ToString)
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
