<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="CustomerInquiry.aspx.vb" Inherits="CustomerInquiry" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table id="CustTable" runat="server" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td colspan="1">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                </dx:ASPxLabel>
            </td>
            <td colspan="2">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label100 %>">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="1" align="left">
                <dx:ASPxTextBox ID="txtCustCode" runat="server" Width="100px">
                </dx:ASPxTextBox>
            </td>
            <td valign="top" colspan="2" align="left">
                <dx:ASPxTextBox ID="txt_co_cd" runat="server" Width="100px" ReadOnly="true">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label110 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label211%>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label263 %>">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <dx:ASPxTextBox ID="txt_company" runat="server" Width="301px">
                </dx:ASPxTextBox>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtFName" runat="server" MaxLength="15" Width="100px">
                </dx:ASPxTextBox>
            </td>
            <td valign="top">
                <dx:ASPxTextBox ID="txtLName" runat="server" MaxLength="20" Width="100px">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label22 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label229 %>">
                </dx:ASPxLabel>
                &nbsp;</td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label27 %>">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="txtAddr" runat="server" MaxLength="30" Width="250px">
                </dx:ASPxTextBox>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtHomePhone" runat="server" MaxLength="12" Width="100px">
                </dx:ASPxTextBox>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtBusPhone" runat="server" MaxLength="12" Width="100px">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label80 %>">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txt_city" runat="server" MaxLength="20" Width="169px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_state" runat="server" MaxLength="2" Width="25px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_zip" runat="server" MaxLength="10" Width="49px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label275 %>">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_clear" runat="server" Text="<%$ Resources:LibResources, Label84 %>" OnClick="btn_clear_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <br />
                <dx:ASPxLabel ID="lbl_errors" runat="server" Width="100%">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label6 %>">
                </dx:ASPxLabel>
                &nbsp;<dx:ASPxTextBox ID="txt_acct_balance" runat="server" ReadOnly="True" Width="102px"
                    Text="$0.00">
                </dx:ASPxTextBox>
            </td>
            <td>
            <dx:ASPxButton ID="btn_cphq"  runat="server"  Text="Back to CPHQ" >
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <asp:DataGrid ID="MyDataGrid" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
        AutoGenerateColumns="False" BorderColor="Black" CellPadding="3" DataKeyField="ar_trn_pk"
        Height="16px" OnSortCommand="MyDataGrid_Sort" PagerStyle-HorizontalAlign="Right"
        PagerStyle-Mode="NumericPages" Width="100%">
        <Columns>
            <asp:TemplateColumn SortExpression="ivc_cd asc" HeaderText="<%$ Resources:LibResources, Label249 %>">
                <ItemTemplate>
                    <dx:ASPxHyperLink ID="HyperLink1" runat="server" Text="">
                    </dx:ASPxHyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="post_dt" Visible="False" HeaderText="Date" SortExpression="post_dt asc"
                DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
            <asp:BoundColumn DataField="TRN_TP_CD" Visible="False" HeaderText="Trans" SortExpression="trn_tp_cd asc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="STAT_CD" Visible="False" HeaderText="Status" SortExpression="stat_cd asc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="mop_cd" Visible="False" HeaderText="MOP" SortExpression="mop_cd asc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" Visible="False" DataFormatString="{0:N2}" HeaderText="<%$ Resources:LibResources, Label28 %>"
                SortExpression="amt asc">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Right" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Right" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="plan" Visible="False" HeaderText="<%$ Resources:LibResources, Label400 %>" SortExpression="plan asc">
            </asp:BoundColumn>
            <asp:TemplateColumn SortExpression="post_dt asc" HeaderText="<%$ Resources:LibResources, Label143 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_date" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="trn_tp_cd asc" HeaderText="<%$ Resources:LibResources, Label624 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_trans" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="stat_cd asc" HeaderText="<%$ Resources:LibResources, Label571 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_status" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="mop_cd asc" HeaderText="<%$ Resources:LibResources, Label322 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_mop" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="amt asc" HeaderText="<%$ Resources:LibResources, Label28 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_amt" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="<%$ Resources:LibResources, Label44 %>" Font-Bold="True">
                    </dx:ASPxLabel>
                </HeaderTemplate>
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_balance" runat="server" Text="0">
                    </dx:ASPxLabel>
                    <br />
                </ItemTemplate>
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Right" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Right" />
            </asp:TemplateColumn>
             <%--' Daniela added Des for TDF transactions--%>
           <asp:TemplateColumn SortExpression="plan asc" HeaderText="<%$ Resources:LibResources, Label400 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_plan" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
           </asp:TemplateColumn>
            <asp:BoundColumn DataField="ivc_cd" Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="adj_ivc_cd" Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="trn_tp_cd" Visible="False"></asp:BoundColumn>            
        </Columns>
        <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
        <AlternatingItemStyle BackColor="Gainsboro" Font-Bold="False" Font-Italic="False"
            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" ForeColor="Black" />
    </asp:DataGrid>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    &nbsp;<asp:Image ID="img_warn" runat="server" ImageUrl="images/warning.jpg" Visible="False" />
    &nbsp;
    <dx:ASPxLabel ID="lbl_Warning" Text="" runat="server" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
