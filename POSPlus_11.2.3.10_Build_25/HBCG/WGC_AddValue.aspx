<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WGC_AddValue.aspx.vb" Inherits="WGC_AddValue"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div width="100%" align="center">
        <br />
        &nbsp;<br />
        <b>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Add Value to Gift Card">
            </dx:ASPxLabel>
        </b>
        <table width="50%">
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Card Number:">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Amount:">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_amount" runat="server" AutoPostBack="False" Width="194px" CssClass="style5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Swiped?">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_swiped" runat="server" AutoPostBack="false" Enabled="false"
                        Width="10px">N</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="border-right: gray thin solid; border-top: gray thin solid;
                    border-left: gray thin solid; border-bottom: gray thin solid; text-align: center">
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Customer Purchased Gift Card">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Sales Order:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_del_doc_num" runat="server" AutoPostBack="True" Enabled="true"
                        Width="194px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <br />
                </td>
            </tr>
        </table>
        <table width="50%" id="tbl_aruti" runat="server">
            <tr>
                <td align="left" colspan="2" style="border-right: gray thin solid; border-top: gray thin solid;
                    border-left: gray thin solid; border-bottom: gray thin solid; text-align: center">
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Non-Purchase Transaction">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Transaction Type:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <dx:ASPxComboBox ID="cbo_trn_tp" runat="server" Width="250px" AutoPostBack="True"
                        IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Internal Customer Code:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_cust_cd" runat="server" AutoPostBack="True" Enabled="true" Width="194px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Description:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_description" runat="server" MaxLength="40" Enabled="true" Width="194px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Store Code:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <dx:ASPxComboBox ID="cbo_store_cd" runat="server" Width="250px" AutoPostBack="True"
                        IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Company Code:">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_comp_cd" runat="server" MaxLength="40" Enabled="False" Width="44px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table width="50%">
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Add Value To Card">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <dx:ASPxLabel ID="lbl_response" runat="server" Width="100%"></dx:ASPxLabel>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
