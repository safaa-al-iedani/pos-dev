﻿Imports System.Collections.Generic
Imports TransportationLibrary
Imports SalesLibrary.OrderInfoUtils
Imports SalesLibrary
Imports System.Data.OracleClient
Imports DevExpress.XtraEditors

Partial Class SalesBookOfBusiness
    Inherits POSBasePage

    ' when building quasi-bulk queries, how many bound vars to use?  this constant keeps them consistent and thus re-usable in sqlarea
    ' and in testing it is actually faster this way, e.g. 4 queries of 100, 100, 100, 43 + 57 NULL is faster than 
    ' a unique new query for all 343. If there are many keys to query, the very best size, in elapsed-time, seems to be 
    ' about 175 to 200.  But 100 works quite well.  Above 200 it starts to slow down and the shared memory grows disporportionately.
    ' Any list size greater than 1 is faster than querying them all one-by-one.  Any list size between 50 and 200 is faster
    ' than querying 300+ at a time.  The observation of an optimum near 175 may be an artifact of the test case with 343 - it 
    ' has the fewest dead slots with null in the last query - so we'll go with 100 as that is well into the performance band and
    ' saves a bit on sga.
    Private Const NormalNumberOfBoundParameters = AppConstants.AdvancedQueryConstants.NormalNumberOfBoundParameters
    ' but a few queries get GIGANTIC when we do this and we may need to make a tradeoff 
    Private Const LimitedNumberOfBoundParameters = AppConstants.AdvancedQueryConstants.LimitedNumberOfBoundParameters

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private g_SbobSes As SbobSession

    Private Class SoGrid

        Public Const SELECT_COL As Integer = 0
        Public Const DEL_DOC_NUM As Integer = 1
        Public Const SPLIT_PCT As Integer = 2
        Public Const BILL_TO_NAME As Integer = 3
        Public Const MULT_ORDS As Integer = 4
        Public Const CONFIRMED As Integer = 5 '  to be added later; removed for initial phase
        Public Const RESERVED As Integer = 6
        Public Const INV_AVAIL As Integer = 7
        Public Const PAID_IN_FULL As Integer = 8
        Public Const INVOICE_AMT As Integer = 9
        Public Const PU_DEL_DT As Integer = 10
        Public Const PO_IST As Integer = 11
        Public Const ROW_PROC As Integer = 12

    End Class

    Private Class SbobSession

        Property empCd As String
        Property allSlspPriv As Boolean
        Property noAutoQryPriv As Boolean
        Property validSlsp As Boolean
        Property hasSomAccess As Boolean
        Property qryStore As String
        Property qrySlspCd As String
        Property qryCustTp As String
        Property qryStatusFrom As String
        Property qryStatusTo As String
        Property grandTot As Double
        Property qryAsOf As Date
        'Alice added for request 171 on Nov 2019
        Property qryPU_DEL As String
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("SbobSess")) Then
            SetSbobSessionInfo()
        Else
            g_SbobSes = Session("SbobSess")
            If g_SbobSes.empCd <> Session("emp_cd") Then
                SetSbobSessionInfo()
            End If
        End If

        If Not IsPostBack Then
            storeCdPopulate()
            populateConfirmationRage()
            slspPopulate(True)
            CustTps.Value = "I" ' default to individual
            lbl_msg.Text = ""

            ' if returning from SOM reset the query info and then
            '   if the prior results are available redisplay them otherwise we may re-query if auto-query
            Dim returnFrom As String = Request("returnFrom")
            Dim didRebind = False
            If Not String.IsNullOrWhiteSpace(returnFrom) Then
                CustTps.Value = g_SbobSes.qryCustTp
                Slsps.Value = g_SbobSes.qrySlspCd
                StoreCds.Value = g_SbobSes.qryStore
                ddlConfStatusFrom.Value = g_SbobSes.qryStatusFrom
                ddlConfStatusTo.Value = g_SbobSes.qryStatusTo
                ddlPU_DEL.Value = g_SbobSes.qryPU_DEL 'Alice added for request 171 on Nov 2019

                If "SOM".Equals(returnFrom) AndAlso SecurityUtils.hasSecurity("SBOB", g_SbobSes.empCd) Then
                    ' we don't come through here on a postback e.g. click of the Lookup button
                    ' so it is "ok" that the condition we are testing remains true forever if 
                    ' we ever go to SOM and back.  
                    ' NB: this is relying on the fact that the DataTable is being retained in 
                    ' the session state.  That is the way the program WAS working already. 
                    ' It is not necesarily the best possible thing to do.   
                    ' If we later decide this should be improved, the solution here is not 
                    ' complicated: define a new "data class" to hold the fields we need for the
                    ' grid, build a List of those from the results of the queries, change the 
                    ' grid binding logic to use that List, and save the *list* in session.
                    Dim ordsTbl As DataTable = Nothing
                    Try
                        ordsTbl = Session("ORD_TBL")
                    Catch ex As Exception
                    End Try
                    If ordsTbl IsNot Nothing AndAlso ordsTbl.Rows.Count > 0 Then
                        didRebind = True
                        Dim datVu As DataView = ordsTbl.DefaultView()
                        DocGrid.DataSource = datVu
                        DocGrid.DataBind()
                        lbl_gTotVal.Text = FormatCurrency(g_SbobSes.grandTot)
                        showAsOfMsg()
                    Else
                        'Daniela memory error fix
                        btn_Lookup_Click(Nothing, Nothing)
                    End If
                End If
            End If

            ' no automatic lookup if not slsp and have access to ALL
            If (Not didRebind) AndAlso (Not g_SbobSes.noAutoQryPriv) AndAlso g_SbobSes.validSlsp Then
                ' do not go thru the auto-query if the user is not able to view this page
                If Not SecurityUtils.hasSecurity("SBOB", g_SbobSes.empCd) Then
                    Response.Redirect("newmain.aspx")
                End If

                btn_Lookup_Click(Nothing, Nothing)
            End If

        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Not SystemUtils.valLogin Then
            Response.Redirect("login.aspx")
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub SetSbobSessionInfo()
        g_SbobSes = New SbobSession
        g_SbobSes.empCd = Session("emp_cd").ToString
        g_SbobSes.allSlspPriv = SecurityUtils.hasSecurity("SBOB_ALL", Session("emp_cd"))
        g_SbobSes.noAutoQryPriv = SecurityUtils.hasSecurity("SBOBNOQRY", Session("emp_cd"))
        g_SbobSes.validSlsp = SalesUtils.Validate_Slsp(Session("emp_cd"))
        Session("sbobSess") = g_SbobSes
        If SecurityUtils.hasSecurity(SecurityUtils.SOM_ACCESS, g_SbobSes.empCd) Then
            g_SbobSes.hasSomAccess = True
        Else
            g_SbobSes.hasSomAccess = False
        End If
    End Sub

    Protected Sub SetSbobSessionQry()

        g_SbobSes = Session("sbobSess")
        g_SbobSes.qryCustTp = CustTps.Value.ToString
        g_SbobSes.qrySlspCd = Slsps.Value.ToString
        g_SbobSes.qryStore = StoreCds.Value.ToString
        g_SbobSes.qryStatusFrom = ddlConfStatusFrom.Value
        g_SbobSes.qryStatusTo = ddlConfStatusTo.Value
        g_SbobSes.qryPU_DEL = ddlPU_DEL.Value.ToString 'Alice add for request 171 on Nov 2019
        Session("sbobSess") = g_SbobSes
    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_LookUp.Click

        Try 'Daniela 

            Dim t0 As Date = Now
            Dim dec As Boolean = True
            Dim req As New OrderRequestDtc
            'No confirmation code exists, normal flow should exist
            If Boolean.Parse(ViewState("RowsExists")) = False Then
                dec = True
                'mm-4077 added new properies
                req.orderQueryRequestFrom = "0"
                req.orderQueryRequestFrom = "0"
                'mm-4077 
            ElseIf ddlConfStatusFrom.SelectedIndex = 0 AndAlso ddlConfStatusTo.SelectedIndex = 0 Then 'if none of the drop downs are selected, then normal flow
                dec = True
                'mm-4077 added new properies
                req.orderQueryRequestFrom = "0"
                req.orderQueryRequestFrom = "0"
                'mm-4077
            ElseIf ddlConfStatusFrom.SelectedIndex * ddlConfStatusTo.SelectedIndex = 0 Then 'if one drop down is selected and the other is not selected then error messge
                dec = False
            ElseIf ddlConfStatusFrom.SelectedIndex > ddlConfStatusTo.SelectedIndex Then
                dec = False
            Else
                dec = True
                'mm-4077 added new properies
                req.orderQueryRequestFrom = ddlConfStatusFrom.Value
                req.orderQueryRequestTo = ddlConfStatusTo.Value
                'mm-4077 
            End If
            'End If

            If dec = True Then
                req.empCdOp = Session("EMP_CD")
                req.statCd = "O"
                req.ordTp = "SAL"
                req.soStore = StoreCds.Value.ToString
                If Not "Both".Equals(CustTps.Value.ToString) Then
                    req.custTpCd = CustTps.Value.ToString
                End If

                'Alice added for request 171 on Nov 2019
                If Not "B".Equals(ddlPU_DEL.Value.ToString) Then
                    req.puDel = ddlPU_DEL.Value.ToString
                Else
                    req.puDel = String.Empty
                End If

                ' set the selection values so can restore when return from SOM
                SetSbobSessionQry()

                ' if selected value is empty, then user isn't a slsp but doesn't have access to ALL slsp so cannot query for them
                If Slsps.Value.ToString.isNotEmpty Then

                    If Not "ALL".Equals(Slsps.Value.ToString) Then

                        Dim slspItem As String = Slsps.SelectedItem.Text
                        Dim dashIndx As Integer = slspItem.IndexOf("-")
                        If dashIndx <> -1 Then

                            req.allForSlspCd = theSystemBiz.GetEmployeeCode(UCase(slspItem.Substring(0, (dashIndx - 1))))   ' NEED TO KEEP THE ENTRIES AS init || ' - ' || name
                        End If
                    End If
                    req.billToCustName = True
                    req.puDelInfo = True
                    req.slspPcts = True
                    req.slspCds = True
                    req.finInfo = True
                    req.shipToZone = True
                    req.queryPaidInFull = True
                    BindGrid(req)
                Else
                    lbl_msg.Text = "SECURITY VIOLATION"
                    lbl_msg.Visible = True
                End If
            Else
                'Display Error Message
                'invalid range selected.
                lbl_msg.Text = "Invalid range for confirmation codes, Please select valid range."
            End If
            Dim t1 As Date = Now
            Dim iv1 As TimeSpan = t1.Subtract(t0)
            Dim txt As String = String.Format("btn_Lookup_Click elapsed: {0}  ", iv1.ToString)
            System.Diagnostics.Debug.WriteLine(txt)

            ' Daniela handle exception
        Catch
            ' user security
            lbl_msg.Text = "SECURITY VIOLATION. Please login as a sales person or manager and try again."
            lbl_msg.Visible = True
        End Try
    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        StoreCds.Value = Session("HOME_STORE_CD").ToString
        slspPopulate(False)
        CustTps.Value = "I"
        lbl_msg.Text = ""
        lbl_msg.Visible = False
        lbl_gTotVal.Text = FormatCurrency(0, 2)

        Session("ORD_TBL") = Nothing
        DocGrid.DataSource = Nothing
        DocGrid.DataBind()

        g_SbobSes.qryAsOf = New Date(0)
        showAsOfMsg()
    End Sub

    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub storeCdPopulate()

        Dim datSet As DataSet = theSalesBiz.GetEmpStores(Session("emp_cd"))

        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

            With StoreCds
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "store_des"
                .DataBind()
            End With

        ElseIf Not IsNothing(Session("HOME_STORE_CD")) Then

            Dim storeRow As StoreData = theSalesBiz.GetStoreInfo(Session("HOME_STORE_CD"))
            'StoreCds.Items.Add(New ListItem(Session("HOME_STORE_CD") + " - " + storeRow.name, Session("HOME_STORE_CD")))  ' store name same format as StoreUtils.getEmpStores: store_cd || ' - ' || store_name AS store_des 
            StoreCds.Items.Add(New DevExpress.Web.ASPxEditors.ListEditItem(Session("HOME_STORE_CD") + " - " + storeRow.name, Session("HOME_STORE_CD"))) ' store name same format as StoreUtils.getEmpStores: store_cd || ' - ' || store_name AS store_des 
            StoreCds.DataBind()
        End If

        If Not IsNothing(Session("HOME_STORE_CD")) Then

            Try
                StoreCds.Value = Session("HOME_STORE_CD").ToString
            Catch argEx As ArgumentOutOfRangeException
                ' setup is wrong - probably no stores show - never saw this fail but slsp did so playing it safe
            End Try
        End If

    End Sub

    ''' <summary>
    ''' sets the salesperson drop down and default
    ''' </summary>
    ''' <param name="getNewList">if set to true, extract and fill slsp list from db; false, then just use existing list and set default</param>
    ''' <remarks></remarks>
    Private Sub slspPopulate(ByVal getNewList As Boolean)

        Dim thisEmpOnly As String = ""    ' set to blank, populate with all salespersons

        ' if the employee has access to view all other salespersons info, then query all slsp; if not, then just list one row for the employee info
        If Not g_SbobSes.allSlspPriv Then

            thisEmpOnly = Session("emp_cd").ToString
        End If

        If getNewList Then

            Dim datSet As DataSet = OrderUtils.getSalespersonList(False, thisEmpOnly)

            With Slsps
                .DataSource = datSet
                .ValueField = "emp_cd"
                .TextField = "full_name"
                .DataBind()
            End With
        End If

        If Not IsNothing(Session("emp_cd")) Then

            Try
                Slsps.Value = Session("emp_cd").ToString
            Catch argEx As ArgumentOutOfRangeException
                ' employee not a slsp - the above fails the 2nd time executed, not the 1st ???; do nothing
            End Try

            ' if access to ALL, then if user not slsp, default is ALL
            If thisEmpOnly.isEmpty AndAlso Slsps.Value <> Session("emp_cd").ToString Then
                Slsps.Value = "ALL"
                ' if the selected value is not equal, then user not a slsp; if thisEmpOnly is not empty, then doesn't have access to all
            End If
        End If
    End Sub
    ''' <summary>
    ''' sets the Confirmation range from and to dropdown lists
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateConfirmationRage()


        If Not IsNothing(Session("emp_cd")) Then

            Try
                Dim confirmCodes As DataSet = theSalesBiz.GetConfirmationCodes()
                If confirmCodes.Tables.Count > 0 AndAlso confirmCodes.Tables(0).Rows.Count > 0 Then
                    'Fill Range from drop down
                    ddlConfStatusFrom.DataSource = confirmCodes
                    ddlConfStatusFrom.TextField = "Description"
                    ddlConfStatusFrom.ValueField = "Code"

                    ddlConfStatusFrom.DataBind()
                    ddlConfStatusFrom.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Select", "0"))
                    ddlConfStatusFrom.Items(0).Selected = True
                    'Fill Range To Drop Down
                    ddlConfStatusTo.DataSource = confirmCodes
                    ddlConfStatusTo.TextField = "Description"
                    ddlConfStatusTo.ValueField = "Code"
                    ddlConfStatusTo.DataBind()
                    ddlConfStatusTo.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Select", "0"))

                    ddlConfStatusTo.Items(0).Selected = True
                    ViewState("RowsExists") = True
                Else
                    TRConfStatusTo.Visible = False
                    TRConfStatusFrom.Visible = False
                    ddlConfStatusFrom.Enabled = False
                    ddlConfStatusTo.Enabled = False
                    ViewState("RowsExists") = False

                End If

            Catch argEx As Exception
                ' employee not a slsp - the above fails the 2nd time executed, not the 1st ???; do nothing
            End Try


        End If
    End Sub



    ''' <summary>
    ''' Sets the value of the special row process (highlight) column in the extended order row, based 
    ''' on the value in the pre-computed dictionary, if the del_doc_num is present there.  
    ''' In addition, if "mto" highlighting option and the process is "high_light" the further check for 
    ''' "not paid in full" is applied first.
    ''' 
    ''' this is the second and final part of a refactoring of the specRowProc, to allow the data to be fetched
    ''' in one query rather than one query per order. 
    ''' </summary>
    ''' <param name="ordRow">the extended order row</param>
    ''' <param name="specRows">sparse pre-computed dictionary of special process codes by order number </param>
    ''' <param name="col">the column name in the order row to set with the process value</param>
    ''' <remarks>MM-9935</remarks>
    Private Sub ApplySpecRowProc(ordRow As DataRow, specRows As Dictionary(Of String, String), col As String)
        Dim isMto As Boolean = (SysPmsConstants.SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS).Equals(SysPms.sbobRowProcess)
        Dim process As String = ""
        specRows.TryGetValue(ordRow("del_doc_num"), process)
        If isMto AndAlso SysPmsConstants.SBOB_ROW_HGHLGHT.Equals(process) Then
            If Not "N".Equals(ordRow("PaidInFull").ToString) Then
                process = ""
            End If
        End If
        ordRow(col) = process
    End Sub

    ''' <summary>
    ''' determines the process to apply to each row when binding the data to the grid, 
    ''' for instance highlighting the order row if the document has a specific order sort code; 
    ''' parameter in GP_PARMS, SBOB_SPECIAL_ROW_PROCESS, value indicates what the conditions are to do the process.
    ''' 
    ''' this is the main part of a refactoring of the specRowProc, to allow the data to be fetched in one query
    ''' rather than one query per order. 
    ''' </summary>
    ''' 
    ''' <param name="conn">caller's oracle connection</param>
    ''' <param name="docNums">list of order numbers</param>
    ''' <param name="st_cd">store code that the list was filtered on</param>
    ''' <returns>returns a SPARSE dictionary of del_doc_num>>process (only HIGH_LIGHT at this time) for all (only) impacted rows</returns>
    ''' <remarks>
    ''' ?This could have an option input that indicated what special processing to do; initially it is only highlighting
    ''' In the case of "mto" highlighting an additional test for "paid in full" needs to be applied later
    ''' MM-9935
    ''' </remarks>
    ''' 
    Private Function FindSpecRowProc(conn As OracleConnection, ByVal docNums As List(Of String), st_cd As String) As Dictionary(Of String, String)

        Dim rowSpecs As New Dictionary(Of String, String)
        Dim process As String = ""
        Dim procAndCond As String = SysPms.sbobRowProcess

        If (Not String.IsNullOrEmpty(procAndCond)) Then

            Dim datSet As New DataSet
            Dim sqlStmtSb As New StringBuilder
            Dim localNumberOfBoundParms = NormalNumberOfBoundParameters
            Dim isMto As Boolean = (SysPmsConstants.SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS).Equals(procAndCond)

            If isMto Then
                ' there is some evidence, sporadically, that the MTO query might get very large in SGA, 
                ' we could try to mitigate that by using fewer parameters
                ' on further review this does not appear to be necessary...
                'localNumberOfBoundParms = LimitedNumberOfBoundParameters
            End If

            Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

                Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, localNumberOfBoundParms)

                If (SysPmsConstants.SBOB_ROW_HGHLGHT_ORDSRT).Equals(procAndCond.Substring(0, 16)) Then  ' if the first part = HIGHLIGHT
                    Dim lastUnder As Integer = procAndCond.LastIndexOf("_")
                    Dim srtCd As String = procAndCond.Substring((lastUnder + 1), (procAndCond.Length - lastUnder - 1))
                    If SystemUtils.isNotEmpty(srtCd) Then
                        sqlStmtSb.Append("SELECT del_Doc_num FROM so WHERE del_Doc_num IN (").Append(inlist).Append(
                            ") AND ord_srt_cd = :srtCd ")
                        cmd.Parameters.Add(":srtCd", OracleType.VarChar)
                        cmd.Parameters(":srtCd").Value = srtCd
                    End If
                ElseIf (SysPmsConstants.SBOB_ROW_HGHLGHT_SPECORD_DROPPED).Equals(procAndCond) Then
                    If SystemUtils.isNotEmpty(SysPms.specialOrderDropCd) Then
                        sqlStmtSb.Append("SELECT sl.del_Doc_num FROM itm, so_ln sl ").Append(
                            "WHERE itm.itm_cd = sl.itm_cd AND sl.del_Doc_num IN (").Append(inlist).Append(
                            ") AND itm.drop_cd = :dropCd AND sl.void_flag != 'Y' ")
                        cmd.Parameters.Add(":dropCd", OracleType.VarChar)
                        cmd.Parameters(":dropCd").Value = SysPms.specialOrderDropCd
                    End If
                ElseIf isMto Then
                    sqlStmtSb.Append(" SELECT trunc(sysdate) systemdate, max(PU_DEL_DT) DelDate,Sum(ITM_SRT_CD)  MTO, DEL_DOC_NUM,  max(TotalLeadDays) TotalLeadDays")
                    sqlStmtSb.Append(" from( SELECT ")
                    sqlStmtSb.Append(" SO.PU_DEL_DT,case when upper(srt.MTO) = 'Y' then 1 else 0 end ITM_SRT_CD,")
                    sqlStmtSb.Append(" SO.DEL_DOC_NUM,bt_adv_res.get_all_lead_days(i.itm_Cd, str.store_Cd, z.zone_Cd) TotalLeadDays ")
                    sqlStmtSb.Append(" FROM STORE STR, SO,so_ln sl,itm i, ITM$ITM_SRT ISRT,itm_srt srt, ZONE z ")
                    sqlStmtSb.Append(" WHERE ")
                    sqlStmtSb.Append(" SO.SO_STORE_CD = STR.STORE_CD and SO.SO_STORE_CD = :stCd ")
                    sqlStmtSb.Append(" and so.del_doc_num = sl.del_doc_num ")
                    sqlStmtSb.Append(" and sl.itm_cd = i.itm_cd and sl.void_flag != 'Y' ")
                    sqlStmtSb.Append(" and ISRT.ITM_CD = I.ITM_CD and srt.ITM_SRT_CD = isrt.ITM_SRT_CD ")
                    sqlStmtSb.Append(" and SO.SHIP_TO_ZONE_CD = Z.ZONE_CD ")
                    sqlStmtSb.Append(" AND SO.DEL_DOC_NUM  IN (").Append(inlist).Append(") ")
                    sqlStmtSb.Append(" ) t ")
                    sqlStmtSb.Append(" Group by DEL_DOC_NUM ")
                    cmd.Parameters.Add(":stCd", OracleType.VarChar)
                    cmd.Parameters(":stCd").Value = st_cd
                End If

                If sqlStmtSb.Length > 0 Then
                    cmd.CommandText = sqlStmtSb.ToString
                    cmd.Connection = conn
                    oDatAdap.SelectCommand = cmd
                    For ix = 0 To docNums.Count - 1 Step localNumberOfBoundParms
                        SystemUtils.FillMore(docNums, ":ddn", cmd, localNumberOfBoundParms, ix)
                        oDatAdap.Fill(datSet)
                        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
                            For Each tmRow In datSet.Tables(0).Rows
                                If isMto Then
                                    If Convert.ToInt32(tmRow("MTO").ToString) > 0 Then
                                        If Convert.ToDateTime(tmRow("DelDate")) <= Convert.ToDateTime(tmRow("systemdate").ToString).AddDays(Convert.ToDouble(tmRow("TotalLeadDays").ToString)) Then
                                            ' MM-9935 with this refactoring we have to defer this "PaidInFull" check until later If "N".Equals(soRow("PaidInFull").ToString) AndAlso 
                                            ' so the result is "provisional" at this point
                                            rowSpecs.Add(tmRow("DEL_DOC_NUM"), SysPmsConstants.SBOB_ROW_HGHLGHT)
                                        End If
                                    End If
                                Else
                                    rowSpecs.Add(tmRow("DEL_DOC_NUM"), SysPmsConstants.SBOB_ROW_HGHLGHT)
                                End If
                            Next
                        End If
                        datSet.Clear()
                    Next
                End If
            End Using
            If datSet IsNot Nothing Then
                datSet.Dispose()
            End If
        End If

        Return rowSpecs

    End Function

    ''' <summary>
    ''' returns the order lines for the del_docs, sum by itm_cd and id_num of the quantity needed
    ''' </summary>
    ''' <param name="conn">caller's connection to oracle</param>
    ''' <param name="docNums">list of all SO numbers of interest</param>
    ''' <returns>ordered set of summed lines</returns>
    ''' <remarks></remarks>
    Private Shared Function getItmQtys(conn As OracleConnection, ByVal docNums As List(Of String)) As DataTable
        'Returns the order line information

        Dim moreResults As New DataSet
        Dim allresults As DataSet = Nothing

        If docNums IsNot Nothing AndAlso docNums.Count > 0 Then

            Dim sqlStmtSb As New StringBuilder
            ' TODO - DSA - if need to exclude tw, may ned to change test of fill_dt to PIcked flag = F
            ' TODO - may need to add in handling of frame SKUs (by id_num)

            Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter(cmd)

                Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, NormalNumberOfBoundParameters)
                 'IT Request 2581
                'sqlStmtSb.Append("SELECT sl.del_doc_num, sl.itm_cd, SUM(qty) AS qty_ord, SUM(CASE WHEN fill_dt IS NOT NULL THEN qty ELSE 0 END) AS qty_res  ").Append(
                sqlStmtSb.Append("SELECT sl.del_doc_num, sl.itm_cd, SUM(qty) AS qty_ord, SUM(CASE WHEN sl.store_cd  IS NOT NULL THEN qty ELSE 0 END) AS qty_res  ").Append(
		                "FROM itm i, so_ln sl ").Append(
                                "WHERE sl.del_doc_num IN (").Append(inlist).Append(") AND i.itm_cd = sl.itm_cd  AND i.inventory = 'Y' AND sl.void_flag != 'Y' ").Append(
                                "GROUP BY sl.del_doc_num, sl.itm_cd ORDER BY sl.del_doc_num, sl.itm_cd ")
                cmd.Connection = conn
                cmd.CommandText = sqlStmtSb.ToString
                For ix As Integer = 0 To docNums.Count - 1 Step NormalNumberOfBoundParameters
                    SystemUtils.FillMore(docNums, ":ddn", cmd, NormalNumberOfBoundParameters, ix)
                    moreResults = New DataSet()
                    oDatAdap.Fill(moreResults)
                    If SystemUtils.dataSetHasRows(moreResults) AndAlso SystemUtils.dataRowIsNotEmpty(moreResults.Tables(0).Rows(0)) Then
                        If allresults Is Nothing Then
                            allresults = moreResults
                        Else
                            allresults.Tables(0).Merge(moreResults.Tables(0))
                            moreResults.Dispose()
                        End If
                    End If
                Next
            End Using
        End If

        If allresults IsNot Nothing AndAlso SystemUtils.dataSetHasRows(allresults) AndAlso SystemUtils.dataRowIsNotEmpty(allresults.Tables(0).Rows(0)) Then
            Return allresults.Tables(0)
        Else
            Return New DataTable
        End If

    End Function

    ''' <summary>
    ''' extract the rows that match some criterion from a datatable
    ''' </summary>
    ''' <param name="t">the complete table </param>
    ''' <param name="col">name of column to filter on</param>
    ''' <param name="val">value to filter on</param>
    ''' <returns>a datatable with just the qualifying rows if any</returns>
    ''' <remarks></remarks>
    Private Function extractRows(t As DataTable, col As String, val As String) As DataTable
        Dim dv As New DataView(t, col + " = '" + val + "'", Nothing, DataViewRowState.CurrentRows)
        Dim rv As DataTable = dv.ToTable()
        dv.Dispose()
        Return rv
    End Function

    ''' <summary>
    ''' looks at available, on-hand inventory using store priority fill (SPFM) or zone priority fill (ZPFM)
    ''' if document is linked to an IST or PO (with open quantity to receive) (but not above) then returns an 'N'
    ''' if document is not linked to either an IST or PO then returns blank 
    ''' </summary>
    ''' <param name="docNum"></param>
    ''' <param name="puDel"></param>
    ''' <param name="zoneCd">this most likely is ship_to_zone_cd but can be any other zone code</param>
    ''' <param name="storeCd">this most likely is pu_del_Store_cd but can be any other store code</param>
    ''' <param name="summedLines">pre-queried table of so lines summed by doc num and sku (MM-9935)</param>
    ''' <returns>N – The order cannot be filled; P – The order can be partially filled; F – The order can be fully filled</returns>
    ''' <remarks></remarks>
    Private Function setInvFill(ByVal docNum As String, ByVal puDel As String, ByVal zoneCd As String, ByVal storeCd As String, summedLines As DataTable) As String

        Dim invFill As String = "N"

        ' extract the inventory type list from so_ln and sum by itm_cd
        Dim soLnItmsTbl As DataTable = extractRows(summedLines, "DEL_DOC_NUM", docNum)

        If soLnItmsTbl IsNot Nothing AndAlso soLnItmsTbl.Rows.Count > 0 Then

            Dim soLnItmRow As DataRow
            Dim priFillInv As DataSet
            Dim priFillrow As DataRow
            Dim remQtyNeeded As Double
            Dim itmInvCnt As Double = soLnItmsTbl.Rows.Count
            Dim itmFilledCnt As Double = 0  ' count for the number of itms filled; if this equals the itmCnt when done looking for inv, then all doc is filled
            Dim pfIndx As Integer = 0
            ' for testing purposes
            'Dim debug As String = ""

            'loop thru the list of summed by qty inventory itms on the so_lns
            For Each soLnItmRow In soLnItmsTbl.Rows

                If soLnItmRow("qty_res") > 0 Then

                    invFill = "P" ' if any line already has reserved, then the doc is partially filled 
                End If
                remQtyNeeded = soLnItmRow("qty_ord") - soLnItmRow("qty_res")
                'debug = soLnItmRow("itm_cd").ToString + " " + soLnItmRow("qty_ord").ToString + " - " + soLnItmRow("qty_res").ToString & " = " & remQtyNeeded

                If remQtyNeeded <= 0 Then

                    itmFilledCnt = itmFilledCnt + 1

                Else
                    priFillInv = theInvBiz.GetInvPriFill(soLnItmRow("Itm_cd"), puDel, zoneCd, storeCd, "N")

                    If SystemUtils.dataSetHasRows(priFillInv) AndAlso SystemUtils.dataRowIsNotEmpty(priFillInv.Tables(0).Rows(0)) Then

                        pfIndx = 0
                        Do Until pfIndx > priFillInv.Tables(0).Rows.Count - 1 OrElse remQtyNeeded <= 0

                            priFillrow = priFillInv.Tables(0).Rows(pfIndx)
                            ' store, loc, qty returned from api
                            'debug = soLnItmRow("itm_cd").ToString + " " + priFillrow("store_cd").ToString + " " + priFillrow("loc_cd").ToString & " " & priFillrow("qty")

                            If priFillrow("qty") > 0 Then

                                invFill = "P" ' if any line has any qty avail, then the doc is partially filled
                            End If

                            remQtyNeeded = remQtyNeeded - priFillrow("qty")
                            pfIndx = pfIndx + 1
                        Loop
                        If remQtyNeeded <= 0 Then

                            itmFilledCnt = itmFilledCnt + 1
                        End If
                    End If
                End If
            Next

            If itmFilledCnt = itmInvCnt Then
                invFill = "F"  ' all line counts are filled
            End If
        End If

        Return invFill

    End Function

    Sub BindGrid(ByRef req As OrderRequestDtc)
        Dim ObjSalesBiz As SalesBiz = New SalesBiz
        Dim ords As DataSet = ObjSalesBiz.LookupOrders(req, 2)

        If SystemUtils.dataSetHasRows(ords) Then
            Dim grandTot As Double = 0
            Dim ordsTbl As DataTable = ords.Tables(0)
            ordsTbl.Columns.Add("split_pct")
            ordsTbl.Columns.Add("mult_ords")
            ordsTbl.Columns.Add("confirm")
            ordsTbl.Columns.Add("res")
            ordsTbl.Columns.Add("inv_avail")
            'ordsTbl.Columns.Add("paid")
            ordsTbl.Columns.Add("amt", GetType(Double))
            ordsTbl.Columns.Add("po_ist")
            ordsTbl.Columns.Add("row_proc")  ' used to allow special processing on the row, for instance highlighting
            lbl_msg.Text = ""

            ' code left from 3 diff perf modified logic sets - chose 182
            '	18 has one connect loop but a massive sql statement and it cannot limit the queries if not applicable or limit to just one row
            'If "18".Equals(SystemUtils.GetPosGpParm("SBOB_PERF_TST")) Then

            '    grandTot = Process18(req.allForSlspCd, ordsTbl)

            '	15 does separate connects and calls but by doing so, some operations/hits just don’t happen because the logic can check and not execute them if not applicable or it can just get the first row
            'ElseIf "15".Equals(SystemUtils.GetPosGpParm("SBOB_PERF_TST")) Then

            '    grandTot = Process15(req.allForSlspCd, ordsTbl)

            'ElseIf "182".Equals(SystemUtils.GetPosGpParm("SBOB_PERF_TST")) Then
            '	182 is a modified 18 with one connect loop but I pulled out the stuff from the massive sql where I could limit the separate process to just one row
            grandTot = SetSbobDetails(req.allForSlspCd, ordsTbl, req.soStore)
            'End If
            'Dim rcount As String = ordsTbl.Rows.Count.ToString
            'Daniela Oct 15 avoid memory errors TDB
            Session("ORD_TBL") = ordsTbl
            g_SbobSes.grandTot = grandTot
            lbl_gTotVal.Text = FormatCurrency(grandTot)

            Dim datVu As DataView
            datVu = ords.Tables(0).DefaultView
            DocGrid.DataSource = datVu
            DocGrid.DataBind()
            g_SbobSes.qryAsOf = Now
        Else
            g_SbobSes.qryAsOf = New Date(0)
            lbl_msg.Text = "No results were found using your search criteria."
            lbl_msg.Visible = True
            lbl_gTotVal.Text = FormatCurrency(0, 2)
            DocGrid.DataSource = Nothing
            DocGrid.DataBind()
        End If
        showAsOfMsg()
    End Sub

    ''' <summary>
    ''' Display or clear the "results as of" message. 
    ''' </summary>
    ''' <remarks>MM-9936</remarks>
    Private Sub showAsOfMsg()
        If g_SbobSes Is Nothing OrElse g_SbobSes.qryAsOf.Ticks = 0 Then
            lbl_asOf.Text = ""
            lbl_asOf.Visible = False
        Else
            Dim tzn As String = IIf(TimeZone.CurrentTimeZone.IsDaylightSavingTime(g_SbobSes.qryAsOf), TimeZone.CurrentTimeZone.DaylightName, TimeZone.CurrentTimeZone.StandardName)
            Dim tzns() As String = tzn.Split(" ".ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
            Dim tz As String = ""
            For Each s As String In tzns
                tz += s.Substring(0, 1)
            Next
            lbl_asOf.Text = String.Format("Query results as of: {0:hh:mm:ss tt} {1} {2:MM/dd/yy}", g_SbobSes.qryAsOf, tz, g_SbobSes.qryAsOf)
            lbl_asOf.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Locate and the (first) row in the given DataTable where the value in the given column 
    ''' is equal to the given value.  This enables pre-query in cases where the results needed 
    ''' are more than a single value.
    ''' </summary>
    ''' <param name="tbl"></param>
    ''' <param name="colname"></param>
    ''' <param name="keyval"></param>
    ''' <returns>first matching row or nothing</returns>
    ''' <remarks>MM-9935</remarks>
    Private Function MatchRow(tbl As DataTable, colname As String, keyval As String) As DataRow
        Dim rv As DataRow = Nothing
        For i As Integer = 0 To tbl.Rows.Count - 1
            If tbl.Rows(i)(colname) = keyval Then
                rv = tbl.Rows(i)
                Exit For
            End If
        Next
        Return rv
    End Function

    ''' <summary>
    ''' Fills in the extended orders table with the additional details needed to populate 
    ''' the grid on the SBOB page.
    ''' 
    ''' </summary>
    ''' <param name="slspCd">saleperson code</param>
    ''' <param name="ordsTbl">extended table of order data with incomplete details</param>
    ''' <param name="stCd">store code</param>
    ''' <returns>total value of all orders; side effects filling in values in the ordsTbl</returns>
    ''' <remarks>NB: in any mods, make sure we have NO literal keys in any queries</remarks>
    Private Function SetSbobDetails(ByVal slspCd As String, ByRef ordsTbl As DataTable, stCd As String) As Double
        '	182 is a modified 18 with one connect loop but I pulled out the stuff from the massive sql where I could limit the separate process to just one row

        Dim grandTot As Double = 0
        Dim delDoc As String = ""
        Dim coCd As String = ""
        Dim zoneCd As String = ""
        Dim soDatSet As DataSet
        Dim addlSoInfo As DataRow
        Dim gotData As Boolean

        ' first we will pre-query the extra data, as much as possible, using queries with IN (...) expressions to 
        ' scoop up all of them in each query rather running separate queries for each order
        ' for that we need a list of unique doc nums.  a hashset ensured the list has only unique values
        ' with no extra effort by us. but did not preserve the order.
        ' we also need a co_cd value for one of the pre-query routines. and can still use a hashset for that

        Dim tmpAllDocNums As New HashSet(Of String)
        Dim tmpAllCoCds As New HashSet(Of String)
        Dim allDocNums As New List(Of String)
        For Each salOrdRow In ordsTbl.Rows
            delDoc = salOrdRow("del_Doc_num")
            If Not tmpAllDocNums.Contains(delDoc) Then
                tmpAllDocNums.Add(delDoc) ' this serves as an index to keep it unique
                allDocNums.Add(delDoc) ' this preserves the order from the query
            End If
            tmpAllCoCds.Add(salOrdRow("co_cd"))
        Next
        If tmpAllCoCds.Count > 1 Then
            ' this will never happen
            lbl_msg.Text = "Internal error: condition of single co_cd not satisfied."
            lbl_msg.Visible = True
        End If
        Dim theCoCd As String = (New List(Of String)(tmpAllCoCds))(0)
        tmpAllDocNums = Nothing
        tmpAllCoCds = Nothing

        ' then we can loop through the orders and fill in the extra fields, mostly from those bulk queries
        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
            cmd As New OracleCommand, oDatAdap As New OracleDataAdapter(cmd)

            Dim istKeys As Dictionary(Of String, InventoryUtils.WrDtStoreSeqKey) = InventoryUtils.isOnIst(allDocNums, NormalNumberOfBoundParameters)
            Dim poLinks As Dictionary(Of String, InventoryUtils.PoLinkResponse) = InventoryUtils.getPoInfo(allDocNums, NormalNumberOfBoundParameters)

            Dim summedLines As DataTable = getItmQtys(conn, allDocNums)
            Dim specRow As Dictionary(Of String, String) = FindSpecRowProc(conn, allDocNums, stCd)
            Dim unfilledOrders As HashSet(Of String) = FindUnfilledOrders(conn, allDocNums)

            soDatSet = GetOrderData(conn, allDocNums, theCoCd)
            gotData = SystemUtils.dataSetHasRows(soDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(soDatSet.Tables(0).Rows(0))

            ' build a short list that includes only orders that are not "obviously" split
            Dim shortList As New List(Of String)
            For Each salOrdRow In ordsTbl.Rows
                If SystemUtils.isEmpty(salOrdRow("hdr_slsp_cd2") + "") Then
                    shortList.Add(salOrdRow("del_Doc_num"))
                End If
            Next
            ' and use that pre-query for split orders
            Dim splitOrders As HashSet(Of String) = FindSplitOrders(conn, shortList) ' rework to stepwise: DONE
            shortList = Nothing

            For Each salOrdRow In ordsTbl.Rows

                delDoc = salOrdRow("del_Doc_num")
                coCd = salOrdRow("co_cd")

                If gotData Then addlSoInfo = MatchRow(soDatSet.Tables(0), "del_Doc_num", delDoc) Else addlSoInfo = Nothing ' the else will "never" happen 

                If addlSoInfo IsNot Nothing Then

                    'set slsp pct - if 2 on header, can't all be for same, otherwise have to check the lines too
                    If SystemUtils.isNotEmpty(salOrdRow("hdr_slsp_cd2") + "") OrElse splitOrders.Contains(delDoc) Then ' Not isOnlySlsp(salOrdRow("hdr_slsp_cd1" + ""), delDoc)
                        salOrdRow("split_pct") = "SPLIT"
                    Else
                        salOrdRow("split_pct") = FormatPercent("1", 0)
                    End If
                    If SystemUtils.isNotEmpty(addlSoInfo("hasOtherOpenDocs")) AndAlso "Y".Equals(addlSoInfo("hasOtherOpenDocs")) Then

                        salOrdRow("mult_ords") = "Y"
                    Else
                        salOrdRow("mult_ords") = ""
                    End If
                    salOrdRow("confirm") = ""
                    If Not unfilledOrders.Contains(delDoc) Then 'isOrdFilled(delDoc) Then
                        salOrdRow("res") = "Y"
                    Else
                        salOrdRow("res") = "N"
                    End If
                    If SystemUtils.isNotEmpty(addlSoInfo("getSaleAmount")) Then
                        salOrdRow("amt") = addlSoInfo("getSaleAmount")
                    Else
                        salOrdRow("amt") = 0.0
                    End If
                    'If isPaidInFull(addlSoInfo("ar_bal"), salOrdRow("fi_Amt"), salOrdRow("approval_cd") + "", salOrdRow("fin_cust_cd") + "", salOrdRow("stat_cd"), salOrdRow("ord_tp_cd")) Then

                    '    salOrdRow("paid") = "Y"
                    'Else
                    '    salOrdRow("paid") = "N"
                    'End If
                    'salOrdRow("po_ist") = schedOnIstOrPo(addlSoInfo)
                    'salOrdRow("paid") = salOrdRow("paidinfull").ToString()
                Else
                    salOrdRow("split_pct") = "LOGIC PROBLEM"
                End If
                If "N".Equals(salOrdRow("res")) Then

                    Dim storeCd As String = (salOrdRow("pu_del_store_cd") + "").ToString
                    If storeCd.isEmpty Then

                        storeCd = salOrdRow("so_store_cd")
                    End If
                    zoneCd = (salOrdRow("ship_to_zone_cd") + "").ToString
                    If zoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone Then

                        zoneCd = theInvBiz.GetPriFillZoneCode(salOrdRow("pu_del" + ""), (salOrdRow("pu_del_store_cd") + "").ToString, (salOrdRow("ship_to_zip_cd") + "").ToString, "", salOrdRow("so_store_cd"))
                    End If
                    salOrdRow("inv_avail") = setInvFill(delDoc, salOrdRow("pu_del" + ""), zoneCd, storeCd, summedLines)
                Else
                    salOrdRow("inv_avail") = "F"
                End If

                salOrdRow("po_ist") = schedOnIstOrPo(delDoc, poLinks, istKeys)
                ApplySpecRowProc(salOrdRow, specRow, "row_proc")

                grandTot = grandTot + salOrdRow("amt")
            Next

        End Using

        If soDatSet IsNot Nothing Then soDatSet.Dispose()

        Return grandTot
    End Function

    Protected Sub DocGrid_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)

        'Retrieve the table from the session object.
        Dim datTbl = TryCast(Session("ORD_TBL"), DataTable)

        If datTbl IsNot Nothing Then

            datTbl.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            DocGrid.DataSource = Session("ORD_TBL")
            DocGrid.DataBind()
        End If

    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"
                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection
    End Function

    Protected Sub DocGrid_OnRowDataBound(ByVal source As Object, ByVal e As GridViewRowEventArgs)

        'MM-
        If e.Row.RowType = DataControlRowType.DataRow Then
            '

            Dim rowProc As String = e.Row.Cells(SoGrid.ROW_PROC).Text

            'Alice add for request 151--start
            Dim rowView As DataRowView = e.Row.DataItem
            Dim del_doc_num As String = rowView("del_doc_num")
            Dim backorder As String = String.Empty

            If Not String.IsNullOrEmpty(del_doc_num) Then
                backorder = theInvBiz.GetBackOrder(del_doc_num)
            End If
            Dim lblbackorder As Label = CType(e.Row.FindControl("lblBackOrder"), Label)
            lblbackorder.Text = backorder
            '---151 end'

            'Dim rowProc As String = CType(Session("ORD_TBL"), DataTable).Rows(e.Row.RowIndex)("row_proc").ToString
            If SystemUtils.isNotEmpty(rowProc) AndAlso rowProc = SysPmsConstants.SBOB_ROW_HGHLGHT Then

                e.Row.BackColor = System.Drawing.Color.Aqua
                '    'e.Row.ForeColor = System.Drawing.Color.Red
            End If
            'If Not SecurityUtils.hasSecurity(SecurityUtils.SOM_ACCESS, Session("EMP_CD").ToString) Then
            If Not g_SbobSes.hasSomAccess Then  ' changed above to global to avoid db hits for each SO line on page

                Dim viewHpl As HyperLink = CType(e.Row.Cells(SoGrid.SELECT_COL).Controls(0), HyperLink)
                viewHpl.Enabled = False
            End If
        End If
        'MM-4077 
        'Hide if there are no rows for confirmation
        If Boolean.Parse(ViewState("RowsExists")) = False Then
            e.Row.Cells(5).Visible = False ' make it hidden
        End If
    End Sub


    ''' <summary>
    ''' return the doc num in the list iff the header slsp1 code is not the only slsp on the order lines (excluding void lines)
    ''' more efficient than calling Oracle hundreds of times one-by-one
    ''' </summary>
    ''' <param name="docNums">
    ''' list of all doc nums of interest. 
    ''' this list includes only orders that are not "obviously" split, i.e. orders with a NULL slsp_cd2 on the header</param>
    ''' <returns>list of all those that are split</returns>
    ''' <remarks>MM-9935</remarks>
    Private Function FindSplitOrders(conn As OracleConnection, ByVal docNums As List(Of String)) As HashSet(Of String)

        Dim onlySplitDocNums As New HashSet(Of String)

        If docNums Is Nothing OrElse docNums.Count = 0 Then Return onlySplitDocNums

        Dim sqlStmtSb As New StringBuilder

        Using cmd As New OracleCommand()

            Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, NormalNumberOfBoundParameters)

            ' in a general case, this query would be suspect.  but we call it with a list of del_doc_num 
            ' all of which have NULL in so.so_emp_slsp_cd2 
            'Daniela fix SPLIT issue
            'sqlStmtSb.Append("SELECT sl.del_doc_num FROM so_ln sl, so WHERE sl.del_doc_num IN (").Append(inlist).Append(
            '    ") AND sl.del_doc_num = so.del_doc_num AND sl.void_flag != 'Y' ").Append(
            '    " AND (sl.so_emp_slsp_cd1 != so.so_emp_slsp_cd1 OR (sl.so_emp_slsp_cd2 IS NOT NULL AND sl.so_emp_slsp_cd2 != so.so_emp_slsp_cd1)) ")

            sqlStmtSb.Append("SELECT sl.del_doc_num FROM so_ln sl, so WHERE sl.del_doc_num IN (").Append(inlist).Append(
                ") AND sl.del_doc_num = so.del_doc_num AND sl.void_flag != 'Y' ").Append(
                " AND (sl.so_emp_slsp_cd1 != so.so_emp_slsp_cd1 OR (nvl(sl.so_emp_slsp_cd2, '0') <> '0' AND sl.so_emp_slsp_cd2 != so.so_emp_slsp_cd1)) ")

            cmd.Connection = conn
            cmd.CommandText = sqlStmtSb.ToString

            FillByStepwiseQuery(cmd, docNums, ":ddn", NormalNumberOfBoundParameters, onlySplitDocNums, "DEL_DOC_NUM")
        End Using

        Return onlySplitDocNums

    End Function


    ''' <summary>
    ''' determine which of the orders identified in the list of doc nums are unfilled
    ''' uses the step-wise bulk query of up to 100 at a time, for re-use (inside Oracle)
    ''' and effiency e.g. reducing the number of round-trips to the database
    ''' </summary>
    ''' <param name="conn">shared oracle connection</param>
    ''' <param name="docNums">list of del_doc_num to consider</param>
    ''' <returns>hash set of del_doc_num of all the orders that are not filled</returns>
    ''' <remarks>MM-9935</remarks>
    Private Function FindUnfilledOrders(conn As OracleConnection, ByVal docNums As List(Of String)) As HashSet(Of String)

        Dim unfilled As New HashSet(Of String)

        If docNums Is Nothing OrElse docNums.Count = 0 Then Return unfilled

        Dim sqlStmtSb As New StringBuilder

        Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

            Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, NormalNumberOfBoundParameters)

            '  look for an unfilled inventory line
            sqlStmtSb.Append("SELECT distinct sl.del_doc_num FROM itm i, so_ln sl WHERE del_doc_num IN (").Append(inlist).Append(
                                ") AND void_flag != 'Y' AND i.itm_cd = sl.itm_cd ").Append(
                                "AND i.inventory = 'Y' and sl.store_cd IS NULL ")
            cmd.Connection = conn
            cmd.CommandText = sqlStmtSb.ToString

            FillByStepwiseQuery(cmd, docNums, ":ddn", NormalNumberOfBoundParameters, unfilled, "DEL_DOC_NUM")
        End Using

        Return unfilled

    End Function

    ''' <summary>
    ''' executes the parameterized command, filling in the IN (...) params in groups until all have been processed,
    ''' returing the desired result column in the prodvided collection.  
    ''' </summary>
    ''' <param name="cmd">Oracle command with the sql, connection, and bind parameters all set</param>
    ''' <param name="keys">list of key values to query on</param>
    ''' <param name="prefix">locally unique string used in constructing the bind parameter names</param>
    ''' <param name="numParms">number of bind parameters in the command in the IN ( ) phrase</param>
    ''' <param name="results">Collection to hold the results. to get unique results, you can pass a HashSet</param>
    ''' <param name="colName"></param>
    ''' <remarks>MM-9935</remarks>
    ''' 
    Private Sub FillByStepwiseQuery(cmd As OracleCommand, keys As List(Of String), prefix As String, numParms As Integer, results As System.Collections.Generic.ICollection(Of String), colName As String)
        Dim datSet As New DataSet
        Using oDatAdap As New OracleDataAdapter()
            oDatAdap.SelectCommand = cmd
            For ix As Integer = 0 To keys.Count - 1 Step numParms
                SystemUtils.FillMore(keys, prefix, cmd, numParms, ix)
                datSet.Clear()
                oDatAdap.Fill(datSet)
                If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
                    For Each tmRow In datSet.Tables(0).Rows
                        results.Add(tmRow(colName))
                    Next
                End If
            Next
        End Using
        datSet.Dispose()
    End Sub

    ''' <summary>
    ''' builds and execture the query to extract most of the data for each and every document;  
    ''' the core logic was previously described as 
    '''     extracted from original massive combined SQL and removed logic that could be limited outside of query but not in query (ROWNUM); 
    '''     left as separate routine in case have to make some more tweaks.
    ''' 
    ''' new variation (MM-9935), builds sql using a WHERE ... IN (...) construct, with a fixed-length list, to query all the orders in
    ''' one (or a few) invocations of Oracle and return the actual dataset rather than just the query text
    ''' 
    ''' </summary>
    ''' <param name="conn"></param>
    ''' <param name="docNums"></param>
    ''' <param name="co_cd"></param>
    ''' <returns>dataset with most of the details for the SBOB report</returns>
    ''' <remarks></remarks>
    Private Function GetOrderData(conn As OracleConnection, docNums As List(Of String), co_cd As String) As DataSet

        If docNums Is Nothing OrElse docNums.Count = 0 Then Return New DataSet()

        Dim allResults As DataSet = Nothing
        Dim moreResults As DataSet = Nothing

        Dim sqlStmtSb As New StringBuilder

        Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()
            Dim listSql As String = SystemUtils.BuildValueList(":ddn", cmd, NormalNumberOfBoundParameters)
            sqlStmtSb.Append("SELECT so.del_doc_num, so.cust_cd, ").Append(
                "NVL(a.amt, 0.0) AS getSaleAmount, ").Append(
             "   bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '', so.ord_tp_cd) ar_bal, ").Append(
                    "NVL((SELECT /*+ index(so so4) */ 'Y' ").Append(
                        "FROM so oo ").Append(
                        "WHERE oo.del_doc_num != so.del_doc_num ").Append(
                        "AND oo.stat_cd || '' = 'O' ").Append(
                        "AND oo.cust_cd = so.cust_cd ").Append(
                        "AND oo.ord_tp_cd IN ('SAL', 'CRM') ").Append(
                        "AND ROWNUM < 2), 'N') hasOtherOpenDocs ").Append(
            " FROM ").Append(
            "   (SELECT ivc_cd, cust_cd, co_cd, amt ").Append(
            "      FROM ar_trn ar ").Append(
            "      WHERE trn_tp_cd = 'SAL' ) a, ").Append(
            "cust c, so ").Append(
            "  WHERE ").Append(
            "     so.del_doc_num = a.ivc_cd (+) ").Append(
            "     AND a.cust_cd (+) = so.cust_cd ").Append(
            "     AND c.cust_cd = so.cust_cd ").Append(
            "     AND a.co_cd (+) = :coCd AND so.del_Doc_num in ( ").Append(listSql).Append(")")

            cmd.Connection = conn
            cmd.CommandText = sqlStmtSb.ToString
            cmd.Parameters.Add(":coCd", OracleType.VarChar)
            cmd.Parameters(":coCd").Value = co_cd
            oDatAdap.SelectCommand = cmd

            For ix As Integer = 0 To docNums.Count - 1 Step NormalNumberOfBoundParameters
                SystemUtils.FillMore(docNums, ":ddn", cmd, NormalNumberOfBoundParameters, ix)
                moreResults = New DataSet()
                oDatAdap.Fill(moreResults)
                If SystemUtils.dataSetHasRows(moreResults) AndAlso SystemUtils.dataRowIsNotEmpty(moreResults.Tables(0).Rows(0)) Then
                    If allResults Is Nothing Then
                        allResults = moreResults
                    Else
                        allResults.Tables(0).Merge(moreResults.Tables(0))
                        moreResults.Dispose()
                    End If
                End If
            Next

        End Using
        Return allResults

    End Function

    ''' <summary>
    ''' if document is linked to an IST scheduled on a truck OR the document is linked to a PO 
    '''      with open quantity to receive that has been confirmed in RCM, then returns a 'Y'
    ''' if document is linked to an IST or PO (with open quantity to receive) (but not above) then returns an 'N'
    ''' if document is not linked to either an IST or PO then returns blank 
    ''' </summary>
    ''' <param name="docNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function schedOnIstOrPo(ByVal docNum As String, poLinks As Dictionary(Of String, InventoryUtils.PoLinkResponse), istKeys As Dictionary(Of String, InventoryUtils.WrDtStoreSeqKey)) As String

        Dim dispIstPoInfo As String = ""

        Dim poLinkResp As InventoryUtils.PoLinkResponse = Nothing
        poLinks.TryGetValue(docNum, poLinkResp) 'InventoryUtils.getPoInfo(docNum, True)
        'If poLinkResp Is Nothing Then
        '    ' cross-check for test
        '    'poLinkResp = InventoryUtils.getPoInfo(docNum, True)
        'Else
        '    System.Diagnostics.Debug.WriteLine("got a polr on " + docNum)
        'End If
        If (Not IsNothing(poLinkResp)) AndAlso SystemUtils.isNotEmpty(poLinkResp.poCd) Then

            If (Not "Y".Equals(SysPms.sbobPoRcmConfirm)) OrElse theInvBiz.IsPOConfirmed(poLinkResp.poCd) Then
                dispIstPoInfo = "P"
            End If
        End If

        ' changed to always get IST info
        Dim istKey As InventoryUtils.WrDtStoreSeqKey = Nothing 'InventoryUtils.isOnIst(docNum, True)
        istKeys.TryGetValue(docNum, istKey)
        If (Not IsNothing(istKey)) AndAlso SystemUtils.isNotEmpty(istKey.seqNum) Then
            If InventoryUtils.isIstSchedOnTruck(istKey) Then
                If dispIstPoInfo.isNotEmpty Then
                    dispIstPoInfo = dispIstPoInfo + ", I"
                Else
                    dispIstPoInfo = "I"
                End If
            End If
        End If

        Return dispIstPoInfo

    End Function


#Region "various collections of historical and development code, all commented out"

#Region " ----> Original logic functions - put together for perf into one big query but these may be useful as utilities sometime"
    ' ''' <summary>
    ' ''' returns true if input slsp code is the only slsp on the order lines (excluding void lines); false otherwise
    ' ''' </summary>
    ' ''' <param name="slspCd1"></param>
    ' ''' <param name="docNum"></param>
    ' ''' <returns></returns>
    ' ''' <remarks>don't delete this one - needed for reference because the query was changed</remarks>
    'Private Function isOnlySlsp(ByVal slspCd1 As String, ByVal docNum As String) As Boolean

    '    Dim onlySlsp As Boolean = True

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT 1 FROM so_ln sl WHERE del_doc_num = :docNum AND void_flag != 'Y' ").Append(
    '        " AND (sl.so_emp_slsp_cd1 != :slspCd OR (sl.so_emp_slsp_cd2 IS NOT NULL AND sl.so_emp_slsp_cd2 != :slspCd)) ").Append(
    '        " AND ROWNUM < 2 ")

    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum
    '        cmd.Parameters.Add(":slspCd", OracleType.VarChar)
    '        cmd.Parameters(":slspCd").Value = slspCd1

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) Then

    '        onlySlsp = False
    '    End If

    '    Return onlySlsp

    'End Function

    ' ''' <summary>
    ' ''' returns true if this customer has other open orders in the system; false otherwise
    ' ''' </summary>
    ' ''' <param name="custCd"></param>
    ' ''' <param name="docNum"></param>
    ' ''' <remarks></remarks>
    'Private Function hasOtherOpenDocs(ByVal custCd As String, ByVal docNum As String) As Boolean

    '    Dim otherDocs As Boolean = False

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT 1 FROM so WHERE del_doc_num != :docNum AND stat_cd || '' = 'O' AND cust_cd = :custCd ").Append(
    '                        "AND ORD_TP_CD IN ('SAL', 'CRM') AND ROWNUM < 2 ")

    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum
    '        cmd.Parameters.Add(":custCd", OracleType.VarChar)
    '        cmd.Parameters(":custCd").Value = custCd

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) Then

    '        otherDocs = True
    '    End If

    '    Return otherDocs

    'End Function

    ' ''' <summary>
    ' ''' returns true if all non-void inventory rows on this document are filled; false otherwise
    ' ''' </summary>
    ' ''' <param name="docNum"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function isOrdFilled(ByVal docNum As String) As Boolean

    '    Dim ordFilled As Boolean = True

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    '  look for an unfilled inventory line
    '    sqlStmtSb.Append("SELECT 1 FROM itm i, so_ln sl WHERE del_doc_num = :docNum AND void_flag != 'Y' AND i.itm_cd = sl.itm_cd ").Append(
    '                        "AND i.inventory = 'Y' and sl.store_cd IS NULL AND ROWNUM < 2 ")

    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) Then

    '        ordFilled = False
    '    End If

    '    Return ordFilled

    'End Function

    '' not used by final chosen approach but would be good util method 
    ' ''' <summary>
    ' ''' returns amount of the sale from AR_TRN
    ' ''' </summary>
    ' ''' <param name="custCd"></param>
    ' ''' <param name="docNum"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function getSaleAmount(ByVal custCd As String, ByVal docNum As String) As Double

    '    Dim amt As Double = 0

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT amt FROM ar_trn ar, store sr, so WHERE so.del_Doc_num = :docNum AND ar.ivc_cd = so.del_Doc_num ").Append(
    '        "AND ar.cust_cd = :custCd AND ar.co_cd = sr.co_cd AND sr.store_cd = so.so_store_cd AND trn_tp_cd = 'SAL' ")
    '    'sqlStmtSb.Append("SELECT amt FROM store sr, ar_trn ar WHERE ivc_cd = :docNum AND ar.cust_cd = :custCd AND ar.co_cd = sr.co_cd AND sr.store_cd = ar.origin_store AND trn_tp_cd = 'SAL' ")


    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum
    '        cmd.Parameters.Add(":custCd", OracleType.VarChar)
    '        cmd.Parameters(":custCd").Value = custCd

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

    '        amt = datSet.Tables(0).Rows(0)("amt")
    '    End If

    '    Return amt

    'End Function

    ' '' not used by final chosen approach but would be good util method - need to compare to paymentUtils.getBalance
    ' ''' <summary>
    ' ''' returns true if document has a zero balance which includes financing if the financing is approved by a specific finance company; false otherwise
    ' ''' </summary>
    ' ''' <param name="custCd"></param>
    ' ''' <param name="docNum"></param>
    ' ''' <param name="salAmt"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function isPaidInFull(ByVal custCd As String, ByVal docNum As String, ByVal salAmt As Double) As Boolean

    '    ' TODO - this should be changed to use the new PaymentUtils.GetBalance
    '    Dim pdInFull As Boolean = False

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT NVL(orig_fi_amt, 0) AS fi_amt, approval_cd, fin_cust_cd, ").Append(
    '        "bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '', so.ord_tp_cd) ar_bal ").Append(
    '                "FROM so WHERE del_doc_num = :docNum ")

    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) Then

    '        Dim pdRow As DataRow = datSet.Tables(0).Rows(0)
    '        Dim balAmt As Double = CDbl(pdRow("ar_bal"))
    '        If balAmt <= 0.00001 Then

    '            pdInFull = True

    '        ElseIf CDbl(pdRow("fi_amt")) > 0 AndAlso pdRow("approval_cd").ToString.isNotEmpty AndAlso pdRow("fin_cust_cd").ToString.isNotEmpty Then

    '            balAmt = balAmt - CDbl(pdRow("fi_amt"))

    '            If balAmt <= 0.00001 Then

    '                pdInFull = True
    '            End If
    '        End If
    '    End If

    '    Return pdInFull

    'End Function

    ' BELOW function should be in library somewhere - this is a standard E1 paid in full determination if no data available but delDoc
    ' ''' <summary>
    ' ''' returns true if document has a zero balance which includes financing if the financing is approved by a specific finance company; false otherwise
    ' ''' </summary>
    ' ''' <param name="docNum"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function isPaidInFullB(ByVal docNum As String) As Boolean

    '    ' TODO - this should be changed to use the new PaymentUtils.GetBalance
    '    Dim pdInFull As Boolean = False

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT NVL(orig_fi_amt, 0) AS fi_amt, approval_cd, fin_cust_cd, ").Append(
    '        "bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '', so.ord_tp_cd) ar_bal ").Append(
    '                "FROM so WHERE del_doc_num = :docNum ")

    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '          cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters(":docNum").Value = docNum

    '        oDatAdap.Fill(datSet)
    '    End Using

    '    If SystemUtils.dataSetHasRows(datSet) Then

    '        Dim pdRow As DataRow = datSet.Tables(0).Rows(0)
    '        Dim balAmt As Double = CDbl(pdRow("ar_bal"))
    '        If balAmt <= 0.00001 Then

    '            pdInFull = True

    '        ElseIf CDbl(pdRow("fi_amt")) > 0 AndAlso pdRow("approval_cd").ToString.isNotEmpty AndAlso pdRow("fin_cust_cd").ToString.isNotEmpty Then

    '            balAmt = balAmt - CDbl(pdRow("fi_amt"))

    '            If balAmt <= 0.00001 Then

    '                pdInFull = True
    '            End If
    '        End If
    '    End If

    '    Return pdInFull

    'End Function
#End Region

#Region "Functions replaced with pre-query implementations"
    ' ''' <summary>
    ' ''' if document is linked to an IST scheduled on a truck OR the document is linked to a PO 
    ' '''      with open quantity to receive that has been confirmed in RCM, then returns a 'Y'
    ' ''' if document is linked to an IST or PO (with open quantity to receive) (but not above) then returns an 'N'
    ' ''' if document is not linked to either an IST or PO then returns blank 
    ' ''' </summary>
    ' ''' <param name="addInfo"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function schedOnIstOrPo(ByVal addInfo As DataRow) As String

    '    Dim dispIstPoInfo As String = ""

    '    If SystemUtils.isNotEmpty(addInfo("po_Cd") + "") Then

    '        If (Not "Y".Equals(SysPms.sbobPoRcmConfirm) OrElse theInvBiz.IsPOConfirmed(addInfo("po_Cd"))) Then

    '            dispIstPoInfo = "P"
    '        End If
    '    End If

    '    ' changed to always get IST info
    '    If "Y".Equals(addInfo("hasIstLink")) AndAlso
    '        Not "N".Equals(addInfo("isSchedOnTrunk")) Then

    '        If dispIstPoInfo.isNotEmpty Then

    '            dispIstPoInfo = dispIstPoInfo + ", I"

    '        Else
    '            dispIstPoInfo = "I"
    '        End If
    '    End If

    '    Return dispIstPoInfo

    'End Function

    ' ''' <summary>
    ' ''' returns process to do to the row when binding the data to the grid, for instance highlighting the order row if the document has a specific order sort code; 
    ' ''' parameter in GP_PARMS, SBOB_SPECIAL_ROW_PROCESS, value indicates what the conditions are to do the process
    ' ''' </summary>
    ' ''' <param name="docNum"></param>
    ' ''' <param name="soRow"></param>
    ' ''' <returns>Constant string indicating which process to do to this row when displaying, for instance HIGH_LIGHT</returns>
    ' ''' <remarks>This could have an option input that indicated what special processing to do; initially it is only highlighting</remarks>
    'Private Function specRowProc(ByVal docNum As String, ByVal soRow As DataRow) As String

    '    Dim process As String = ""
    '    Dim procAndCond As String = SysPms.sbobRowProcess
    '    If (Not String.IsNullOrEmpty(procAndCond)) Then

    '        Dim datSet As New DataSet
    '        Dim sqlStmtSb As New StringBuilder

    '        If (SysPmsConstants.SBOB_ROW_HGHLGHT_ORDSRT).Equals(procAndCond.Substring(0, 16)) Then  ' if the first part = HIGHLIGHT

    '            Dim lastUnder As Integer = procAndCond.LastIndexOf("_")
    '            Dim srtCd As String = procAndCond.Substring((lastUnder + 1), (procAndCond.Length - lastUnder - 1))

    '            If SystemUtils.isNotEmpty(srtCd) Then

    '                sqlStmtSb.Append("SELECT 1 FROM so WHERE del_Doc_num = :docNum AND ord_srt_cd = :srtCd ")

    '                Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '                      cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '                    cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '                    cmd.Parameters(":docNum").Value = docNum
    '                    cmd.Parameters.Add(":srtCd", OracleType.VarChar)
    '                    cmd.Parameters(":srtCd").Value = srtCd

    '                    oDatAdap.Fill(datSet)
    '                End Using
    '                If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) AndAlso
    '                    datSet.Tables(0).Rows(0)(0) = 1 Then

    '                    process = SysPmsConstants.SBOB_ROW_HGHLGHT
    '                End If

    '            End If

    '        ElseIf (SysPmsConstants.SBOB_ROW_HGHLGHT_SPECORD_DROPPED).Equals(procAndCond) Then

    '            If SystemUtils.isNotEmpty(SysPms.specialOrderDropCd) Then

    '                sqlStmtSb.Append("SELECT 1 FROM itm, so_ln sl ").Append(
    '                    "WHERE itm.itm_cd = sl.itm_cd AND sl.del_Doc_num = :docNum AND itm.drop_cd = :dropCd AND sl.void_flag != 'Y' ")

    '                Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '                      cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '                    cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '                    cmd.Parameters(":docNum").Value = docNum
    '                    cmd.Parameters.Add(":dropCd", OracleType.VarChar)
    '                    cmd.Parameters(":dropCd").Value = SysPms.specialOrderDropCd

    '                    oDatAdap.Fill(datSet)
    '                End Using
    '                If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) AndAlso
    '                    datSet.Tables(0).Rows(0)(0) = 1 Then

    '                    process = SysPmsConstants.SBOB_ROW_HGHLGHT
    '                End If

    '            End If

    '        ElseIf (SysPmsConstants.SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS).Equals(procAndCond) Then
    '            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '                    cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '                '' if not fully paid AND MTO, highlight
    '                'If "N".Equals(soRow("paid")) Then

    '                '    sqlStmtSb.Append("SELECT 1 FROM itm$itm_srt iis, so_ln sl ").Append(
    '                '        "WHERE iis.itm_cd = sl.itm_cd AND sl.del_Doc_num = :docNum AND iis.itm_srt_cd = :srtCd AND sl.void_flag != 'Y' ")

    '                '    'Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '                '    '      cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '                '    cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '                '    cmd.Parameters(":docNum").Value = docNum
    '                '    cmd.Parameters.Add(":srtCd", OracleType.VarChar)
    '                '    cmd.Parameters(":srtCd").Value = "MTO"   ' TODO - make the routine extract the sort code from the constant end or ??? - new sort logic, not yet implemented in E1

    '                '    oDatAdap.Fill(datSet)
    '                '    '                    End Using
    '                '    If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) AndAlso
    '                '        datSet.Tables(0).Rows(0)(0) = 1 Then

    '                '        process = SysPmsConstants.SBOB_ROW_HGHLGHT
    '                '    End If
    '                'End If

    '                Dim SqlQry As StringBuilder = New StringBuilder()
    '                SqlQry.Append(" SELECT trunc(sysdate) systemdate, max(PU_DEL_DT) DelDate,Sum(ITM_SRT_CD)  MTO, DEL_DOC_NUM,  max(TotalLeadDays) TotalLeadDays")
    '                SqlQry.Append(" from( SELECT ")
    '                'SqlQry.Append(" select case when (select value from GP_PARMS where parm='PAID_IN_FULL_CALC') <>  'CUSTOM' ")
    '                'SqlQry.Append(" then ")
    '                'SqlQry.Append("    bt_sales_util.is_paid_in_full(so.del_Doc_Num,SO.CUST_CD,so.so_store_cd, so.ord_tp_cd,so.orig_fi_amt,so.FIN_CUST_CD,So.approval_cd)  ")
    '                'SqlQry.Append(" else custom_paid.is_order_paid_in_full( SO.ORD_TP_CD,SO.DEL_DOC_NUM,SO.CUST_CD) end PaidInFull , ")
    '                SqlQry.Append(" SO.PU_DEL_DT,case when upper(srt.MTO) = 'Y' then 1 else 0 end ITM_SRT_CD,")
    '                SqlQry.Append(" SO.DEL_DOC_NUM,bt_adv_res.get_all_lead_days(i.itm_Cd, str.store_Cd, z.zone_Cd) TotalLeadDays ")
    '                SqlQry.Append(" FROM STORE STR, SO,so_ln sl,itm i, ITM$ITM_SRT ISRT,itm_srt srt, ZONE z ")
    '                SqlQry.Append(" WHERE ")
    '                SqlQry.Append(" SO.SO_STORE_CD = STR.STORE_CD and SO.SO_STORE_CD ='").Append(soRow("SO_STORE_CD").ToString).Append("' ")
    '                SqlQry.Append(" and so.del_doc_num = sl.del_doc_num ")
    '                SqlQry.Append(" and sl.itm_cd = i.itm_cd and sl.void_flag != 'Y' ")
    '                SqlQry.Append(" and ISRT.ITM_CD = I.ITM_CD and srt.ITM_SRT_CD = isrt.ITM_SRT_CD ")
    '                SqlQry.Append(" and SO.SHIP_TO_ZONE_CD = Z.ZONE_CD ")
    '                SqlQry.Append(" AND SO.DEL_DOC_NUM =:docNum  ")
    '                SqlQry.Append(" ) t ")
    '                SqlQry.Append(" Group by DEL_DOC_NUM ")
    '                cmd.CommandText = SqlQry.ToString()
    '                cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '                cmd.Parameters(":docNum").Value = docNum
    '                oDatAdap.Fill(datSet)
    '                ''NOTE: in the above Query. this can be used for future implementation
    '                ''here id paid conditions is not considared since it is already handled in the prevoius IF statement, Still sql statement is having a logic implemented in DB. in future, we can use this to validate in DB part as well.
    '                If datSet.Tables(0).Rows.Count > 0 Then
    '                    'if the SO is not paid full and MTO > 0 meaning, atleast one of the line item is MTO
    '                    '
    '                    ' If "N".Equals(soRow("paid")) AndAlso Convert.ToInt32(datSet.Tables(0).Rows(0)("MTO").ToString) > 0 AndAlso Convert.ToDateTime(datSet.Tables(0).Rows(0)("DelDate")) <= Convert.ToDateTime(datSet.Tables(0).Rows(0)("systemdate").ToString).AddDays(Convert.ToDouble(datSet.Tables(0).Rows(0)("TotalLeadDays").ToString)) Then
    '                    'If "N".Equals(datSet.Tables(0).Rows(0)("PaidInFull").ToString) AndAlso Convert.ToInt32(datSet.Tables(0).Rows(0)("MTO").ToString) > 0 AndAlso Convert.ToDateTime(datSet.Tables(0).Rows(0)("DelDate")) <= Convert.ToDateTime(datSet.Tables(0).Rows(0)("systemdate").ToString).AddDays(Convert.ToDouble(datSet.Tables(0).Rows(0)("TotalLeadDays").ToString)) Then
    '                    If "N".Equals(soRow("PaidInFull").ToString) AndAlso Convert.ToInt32(datSet.Tables(0).Rows(0)("MTO").ToString) > 0 AndAlso Convert.ToDateTime(datSet.Tables(0).Rows(0)("DelDate")) <= Convert.ToDateTime(datSet.Tables(0).Rows(0)("systemdate").ToString).AddDays(Convert.ToDouble(datSet.Tables(0).Rows(0)("TotalLeadDays").ToString)) Then
    '                        process = SysPmsConstants.SBOB_ROW_HGHLGHT
    '                    End If

    '                End If

    '            End Using

    '        End If
    '    End If

    '    Return process

    'End Function

    ' ''' <summary>
    ' ''' returns the order line for a del_doc, sum of the quantity needed by itm_cd and id_num
    ' ''' </summary>
    ' ''' <param name="delDocNum"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Shared Function getItmQty(ByVal delDocNum As String) As DataSet
    '    'Returns the order line information

    '    Dim datSet As New DataSet

    '    If Not String.IsNullOrWhiteSpace(delDocNum) Then

    '        Dim sqlStmtSb As New StringBuilder
    '        ' TODO - DSA - if need to exclude tw, may ned to change test of fill_dt to PIcked flag = F
    '        ' TODO - may need to add in handling of frame SKUs (by id_num)
    '        sqlStmtSb.Append("SELECT sl.itm_cd, SUM(qty) AS qty_ord, SUM(CASE WHEN fill_dt IS NOT NULL THEN qty ELSE 0 END) AS qty_res  ").Append(
    '                        "FROM itm i, so_ln sl ").Append(
    '                        "WHERE sl.del_doc_num = :delDocNum AND i.itm_cd = sl.itm_cd  AND i.inventory = 'Y' AND sl.void_flag != 'Y' ").Append(
    '                        "GROUP BY sl.itm_cd ORDER BY sl.itm_cd ")

    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

    '            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
    '            cmd.Parameters(":delDocNum").Value = delDocNum


    '            oDatAdap.Fill(datSet) 'TODO: possibly convert this, but it is more complicated because it returns multiple rows with a sum
    '        End Using

    '    End If

    '    Return datSet

    'End Function

    ' ''' <summary>
    ' ''' creates the sql statement to extract most of the data for each document; compiled together in limited sql for perforance enhancement; 
    ' ''' extracted from original massive combined SQL and removed logic that could be limited outside of query but not in query (ROWNUM); 
    ' ''' left as separate routine in case have to make some more tweaks
    ' ''' </summary>
    ' ''' <returns>sql statement to extract most of the details for the SBOB report</returns>
    ' ''' <remarks></remarks>
    'Private Function createSqlStmt() As String

    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT so.del_doc_num, so.cust_cd, ").Append(
    '        "NVL(a.amt, 0.0) AS getSaleAmount, ").Append(
    '     "   bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '', so.ord_tp_cd) ar_bal, ").Append(
    '            "NVL((SELECT 'Y' ").Append(
    '                "FROM so oo ").Append(
    '                "WHERE oo.del_doc_num != so.del_doc_num ").Append(
    '                "AND oo.stat_cd || '' = 'O' ").Append(
    '                "AND oo.cust_cd = so.cust_cd ").Append(
    '                "AND oo.ord_tp_cd IN ('SAL', 'CRM') ").Append(
    '                "AND ROWNUM < 2), 'N') hasOtherOpenDocs ").Append(
    '    " FROM ").Append(
    '    "   (SELECT ivc_cd, cust_cd, co_cd, amt ").Append(
    '    "      FROM ar_trn ar ").Append(
    '    "      WHERE trn_tp_cd = 'SAL' ) a, ").Append(
    '    "cust c, so ").Append(
    '    "  WHERE ").Append(
    '    "     so.del_doc_num = a.ivc_cd (+) ").Append(
    '    "     AND a.cust_cd (+) = so.cust_cd ").Append(
    '    "     AND c.cust_cd = so.cust_cd ").Append(
    '    "     AND a.co_cd (+) = :coCd AND so.del_Doc_num = :docNum ")

    '    Return sqlStmtSb.ToString

    'End Function
#End Region

#Region "Other functions that were unused, of possible historical interest"

    ' ''' <summary>
    ' ''' returns true if document has a zero balance which includes financing if the financing is approved by a specific finance company; false otherwise
    ' ''' </summary>
    ' ''' <param name="balAmt">the balance for a document from AR, typically from bt_ar_trn.bal_due</param>
    ' ''' <param name="fiAmt">the finance amount on the document</param>
    ' ''' <param name="appCd">the finance approval on the document</param>
    ' ''' <param name="finCoCd">the finance company code (as_cd) on the document</param>
    ' ''' <param name="statCd">the status code of the document</param>
    ' ''' <param name="ordTpCd">the order type code of the document</param>
    ' ''' <returns>true if the order is paid in full, false otherwise</returns>
    'Private Function isPaidInFull(ByVal balAmt As Double, ByVal fiAmt As Double, ByVal appCd As String,
    '                              ByVal finCoCd As String, ByVal statCd As String, ByVal ordTpCd As String) As Boolean

    '    Dim pdInFull As Boolean = False

    '    balAmt = PaymentUtils.CalcBal(balAmt, fiAmt, appCd, finCoCd, statCd, ordTpCd)

    '    If balAmt <= 0.00001 Then

    '        pdInFull = True
    '    End If

    '    Return pdInFull
    'End Function

    '' massive sql statement from most of the detail acquisition - used by approach 18 - not final choice
    ' ''' <summary>
    ' ''' creates the sql statement to extract most of the data for each document; compiled together in limited sql for perforance enhancement
    ' ''' </summary>
    ' ''' <returns>sql statement to extract most of the details for the SBOB report</returns>
    ' ''' <remarks></remarks>
    'Private Function createSqlStmt() As String

    '    Dim sqlStmtSb As New StringBuilder

    '    sqlStmtSb.Append("SELECT so.del_doc_num, so.cust_cd, NVL(os.isOnlySlsp, 'Y') AS isOnlySlsp, NVL(i.hasIstlink, 'N') AS hasIstLink, ").Append(
    '        "NVL(f.isOrdFilled, 'Y') AS isOrdFilled, NVL(a.amt, 0.0) AS getSaleAmount, NVL(p.hasPoLink, 'N') AS hasPoLink, p.po_cd, ").Append(
    '     "   bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '', so.ord_tp_cd) ar_bal, NVL(i.isSchedOnTruck, 'N') AS isSchedOnTrunk, ").Append(
    '            "NVL((SELECT 'Y' ").Append(
    '                "FROM so oo ").Append(
    '                "WHERE oo.del_doc_num != so.del_doc_num ").Append(
    '                "AND oo.stat_cd || '' = 'O' ").Append(
    '                "AND oo.cust_cd = so.cust_cd ").Append(
    '                "AND oo.ord_tp_cd IN ('SAL', 'CRM') ").Append(
    '                "AND ROWNUM < 2), 'N') hasOtherOpenDocs ").Append(
    '    " FROM ").Append(
    '        "(SELECT distinct del_doc_num, 'N' isOnlySlsp ").Append(
    '            "FROM so_ln sl ").Append(
    '    "      WHERE void_flag != 'Y' ").Append(
    '    "      AND (sl.so_emp_slsp_cd1 != :slspCd ").Append(
    '    "       OR (sl.so_emp_slsp_cd2 IS NOT NULL AND sl.so_emp_slsp_cd2 != :slspCd)) ").Append(
    '    "      ) os, ").Append(
    '    "   (SELECT distinct sl.del_doc_num, 'N' isOrdFilled ").Append(
    '    "      FROM itm i, so_ln sl ").Append(
    '    "      WHERE void_flag != 'Y' ").Append(
    '    "      AND i.itm_cd = sl.itm_cd ").Append(
    '    "      AND i.inventory = 'Y' ").Append(
    '    "      AND sl.store_cd IS NULL ").Append(
    '    "      ) f, ").Append(
    '    "   (SELECT ivc_cd, cust_cd, co_cd, amt ").Append(
    '    "      FROM ar_trn ar ").Append(
    '    "      WHERE trn_tp_cd = 'SAL' ) a, ").Append(
    '    "   (SELECT del_doc_num, po_cd, 'Y' hasPoLink ").Append(
    '        "   FROM so_ln$po_ln sp WHERE 0.0001 < (SELECT SUM(DECODE(po_actn_tp_cd, 'INI', NVL(qty,0), 'QOC', NVL(qty,0), 0)) ").Append(
    '        "   - SUM(DECODE(po_actn_tp_cd, 'RCV', NVL(qty,0), 'QRC', NVL(qty,0), 0)) ").Append(
    '        "   FROM po_ln$actn_hst pa WHERE pa.po_cd = sp.po_cd AND pa.ln# = sp.ln# ").Append(
    '        "   AND pa.po_actn_tp_cd IN ('INI', 'QOC', 'RCV', 'QRC'))) p, ").Append(
    '    "   (SELECT iso.del_Doc_num, i.stat_cd, 'Y' AS hasIstLink, trk_num AS isSchedOnTruck ").Append(
    '        "   FROM trk_stop ts, ist i, ist_ln$so_ln iso WHERE ").Append(
    '        "   ts.ist_wr_dt (+) = i.ist_wr_dt AND ts.ist_store_cd (+) = i.ist_store_cd AND ts.ist_seq_num (+) = i.ist_seq_num ").Append(
    '        "   AND i.ist_wr_dt = iso.ist_wr_dt AND i.ist_store_cd = iso.ist_store_cd AND i.ist_seq_num = iso.ist_seq_num) i, ").Append(
    '    "cust c, so ").Append(
    '    "  WHERE so.del_doc_num = os.del_doc_num (+) ").Append(
    '    "     AND so.del_doc_num = f.del_doc_num (+) ").Append(
    '    "     AND so.del_doc_num = p.del_doc_num (+) ").Append(
    '    "     AND so.del_doc_num = i.del_doc_num (+) ").Append(
    '    "     AND so.del_doc_num = a.ivc_cd (+) ").Append(
    '    "     and a.cust_cd (+) = so.cust_cd ").Append(
    '    "     AND c.cust_cd = so.cust_cd ").Append(
    '    "     AND a.co_cd (+) = :coCd AND so.del_Doc_num = :docNum ")

    '    Return sqlStmtSb.ToString

    'End Function

    'Private Function Process18(ByVal slspCd As String, ByRef ordsTbl As DataTable) As Double
    ' one of 3 approaches to improve logic - this one had poor perf
    '    Dim grandTot As Double = 0
    '    Dim delDoc As String = ""
    '    Dim coCd As String = ""
    '    Dim soDatSet As DataSet
    '    Dim addlSoInfo As DataRow
    '    Dim sqlStmt = createSqlStmt()
    '    Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '        cmd As New OracleCommand, oDatAdap As New OracleDataAdapter(cmd)
    '        cmd.Parameters.Add(":docNum", OracleType.VarChar)
    '        cmd.Parameters.Add(":slspCd", OracleType.VarChar)
    '        cmd.Parameters.Add(":coCd", OracleType.VarChar)
    '        cmd.CommandText = sqlStmt
    '        cmd.Connection = conn

    '        For Each salOrdRow In ordsTbl.Rows

    '            delDoc = salOrdRow("del_Doc_num")
    '            coCd = salOrdRow("co_cd")

    '            soDatSet = New DataSet
    '            cmd.Parameters(":docNum").Value = delDoc
    '            cmd.Parameters(":slspCd").Value = slspCd 'req.allForSlspCd
    '            cmd.Parameters(":coCd").Value = coCd

    '            oDatAdap.Fill(soDatSet)

    '            If SystemUtils.dataSetHasRows(soDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(soDatSet.Tables(0).Rows(0)) Then

    '                addlSoInfo = soDatSet.Tables(0).Rows(0)
    '                'set slsp pct - if 2 on header, can't all be for same, otherwise have to check the lines too
    '                If SystemUtils.isNotEmpty(addlSoInfo("isOnlySlsp")) AndAlso "N".Equals(addlSoInfo("isOnlySlsp")) Then

    '                    salOrdRow("split_pct") = "SPLIT"
    '                Else
    '                    salOrdRow("split_pct") = FormatPercent("1", 0)
    '                End If
    '                If SystemUtils.isNotEmpty(addlSoInfo("hasOtherOpenDocs")) AndAlso "Y".Equals(addlSoInfo("hasOtherOpenDocs")) Then

    '                    salOrdRow("mult_ords") = "Y"
    '                Else
    '                    salOrdRow("mult_ords") = ""
    '                End If
    '                salOrdRow("confirm") = ""
    '                If SystemUtils.isNotEmpty(addlSoInfo("isOrdFilled")) AndAlso "N".Equals(addlSoInfo("isOrdFilled")) Then

    '                    salOrdRow("res") = "N"
    '                Else
    '                    salOrdRow("res") = "Y"
    '                End If
    '                If SystemUtils.isNotEmpty(addlSoInfo("getSaleAmount")) Then

    '                    salOrdRow("amt") = addlSoInfo("getSaleAmount")
    '                Else
    '                    salOrdRow("amt") = 0.0
    '                End If
    '                If isPaidInFull(addlSoInfo("ar_bal"), salOrdRow("fi_Amt"), salOrdRow("approval_cd") + "", salOrdRow("fin_cust_cd") + "", salOrdRow("stat_cd"), salOrdRow("ord_tp_cd")) Then

    '                    salOrdRow("paid") = "Y"
    '                Else
    '                    salOrdRow("paid") = "N"
    '                End If
    '                salOrdRow("po_ist") = schedOnIstOrPo(addlSoInfo)

    '            Else
    '                salOrdRow("split_pct") = "LOGIC PROBLEM"
    '            End If
    '            If "N".Equals(salOrdRow("res")) Then

    '                Dim storeCd As String = (salOrdRow("pu_del_store_cd") + "").ToString
    '                If storeCd.isEmpty Then

    '                    storeCd = salOrdRow("so_store_cd")
    '                End If
    '                salOrdRow("inv_avail") = setInvFill(delDoc, salOrdRow("pu_del" + ""), storeCd)
    '            Else
    '                salOrdRow("inv_avail") = "F"
    '            End If
    '            salOrdRow("row_proc") = specRowProc(delDoc, salOrdRow)

    '            grandTot = grandTot + salOrdRow("amt")
    '        Next
    '    End Using

    '    Return grandTot
    'End Function


    'Private Function Process15(ByVal slspCd As String, ByRef ordsTbl As DataTable) As Double
    '    ' one of 3 approaches to improve perf - this one was close to being equal to 182 which was chose
    '    Dim grandTot As Double = 0
    '    Dim delDoc As String = ""

    '    For Each soRow In ordsTbl.Rows

    '        delDoc = soRow("del_Doc_num" + "")
    '        'set slsp pct
    '        If SystemUtils.isNotEmpty(soRow("hdr_slsp_cd2") + "") OrElse Not isOnlySlsp(soRow("hdr_slsp_cd1" + ""), delDoc) Then

    '            soRow("split_pct") = "SPLIT"
    '        Else
    '            soRow("split_pct") = FormatPercent("1", 0)
    '        End If
    '        If SystemUtils.isNotEmpty(soRow("Cust_cd") + "") AndAlso hasOtherOpenDocs(soRow("cust_cd" + ""), delDoc) Then

    '            soRow("mult_ords") = "Y"
    '        Else
    '            soRow("mult_ords") = ""
    '        End If
    '        soRow("confirm") = ""
    '        If isOrdFilled(delDoc) Then

    '            soRow("res") = "Y"
    '        Else
    '            soRow("res") = "N"
    '        End If
    '        If "N".Equals(soRow("res")) Then

    '            Dim storeCd As String = (soRow("pu_del_store_cd") + "").ToString
    '            If storeCd.isEmpty Then

    '                storeCd = soRow("so_store_cd")
    '            End If
    '            soRow("inv_avail") = setInvFill(delDoc, soRow("pu_del" + ""), storeCd)
    '        Else
    '            soRow("inv_avail") = "F"
    '        End If
    '        soRow("amt") = getSaleAmount(soRow("Cust_cd") + "", delDoc)
    '        If isPaidInFull(soRow("Cust_cd") + "", delDoc, soRow("amt")) Then

    '            soRow("paid") = "Y"
    '        Else
    '            soRow("paid") = "N"
    '        End If
    '        soRow("po_ist") = schedOnIstOrPo(delDoc)
    '        soRow("row_proc") = specRowProc(delDoc, soRow)

    '        grandTot = grandTot + soRow("amt")
    '    Next

    '    Return grandTot
    'End Function
#End Region

#Region "development / diagnostic / performance comparison routines and fragments"

    'Private Sub dumpColValue(description As String, col As String, tbl As DataTable)
    '    System.Diagnostics.Debug.WriteLine("")
    '    System.Diagnostics.Debug.WriteLine("")
    '    System.Diagnostics.Debug.WriteLine(description)

    '    For Each r As DataRow In tbl.Rows
    '        System.Diagnostics.Debug.WriteLine(r(col))
    '    Next
    'End Sub

    ' ''' <summary>
    ' ''' development routine to explore the timing of different numbers of bind variables.
    ' ''' two passes give us the first time/re-use comparison
    ' ''' </summary>
    ' ''' <param name="conn"></param>
    ' ''' <param name="docNums"></param>
    ' ''' <remarks></remarks>
    'Private Sub BenchUnfilledOrders(conn As OracleConnection, ByVal docNums As List(Of String), mxs As Integer, mns As Integer, ss As Integer)

    '    Dim unfilled As New HashSet(Of String)

    '    If docNums Is Nothing OrElse docNums.Count = 0 Then Return

    '    Dim sqlStmtSb As New StringBuilder
    '    Dim t1 As Date
    '    Dim t2 As Date
    '    Dim iv1 As TimeSpan

    '    If ss > 0 Then ss *= -1

    '    For pass As Integer = 0 To 1
    '        For chunksize As Integer = mxs To mns Step ss
    '            unfilled.Clear()
    '            sqlStmtSb.Clear()
    '            t1 = Now
    '            Using cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '                Dim inlist As String = SystemUtils.BuildValueList(":ddn", cmd, chunksize)

    '                '  look for an unfilled inventory line
    '                sqlStmtSb.Append("SELECT distinct sl.del_doc_num FROM itm i, so_ln sl WHERE del_doc_num IN (").Append(inlist).Append(
    '                                    ") AND void_flag != 'Y' AND i.itm_cd = sl.itm_cd ").Append(
    '                                    "AND i.inventory = 'Y' and sl.store_cd IS NULL ")
    '                cmd.Connection = conn
    '                cmd.CommandText = sqlStmtSb.ToString

    '                FillByStepwiseQuery(cmd, docNums, ":ddn", chunksize, unfilled, "DEL_DOC_NUM")
    '            End Using
    '            t2 = Now
    '            iv1 = t2.Subtract(t1)
    '            Dim txt As String = String.Format("pass: {0}  chunksize: {1} elapsed: {2} docs: {3} results: {4}", pass.ToString, chunksize.ToString, iv1.ToString, docNums.Count.ToString, unfilled.Count.ToString)
    '            System.Diagnostics.Debug.WriteLine(txt)
    '        Next
    '    Next
    '    Return

    'End Sub


    'Private Sub TestQueryVariations(ByVal docNums As List(Of String))


    '    Dim unfilled As New HashSet(Of String)

    '    If docNums Is Nothing OrElse docNums.Count < 200 Then Return

    '    Dim datSet As New DataSet
    '    Dim sqlStmtSb As New StringBuilder
    '    Dim tmRow As DataRow

    '    Dim t1a As New Date
    '    Dim t2a As New Date
    '    Dim t3a As New Date
    '    Dim t4a As New Date
    '    Dim t5a As New Date
    '    Dim t6a As New Date
    '    Dim t7a As New Date
    '    Dim t8b As New Date
    '    Dim t1b As New Date
    '    Dim t2b As New Date
    '    Dim t3b As New Date
    '    Dim t4b As New Date
    '    Dim t5b As New Date
    '    Dim t6b As New Date
    '    Dim t7b As New Date
    '    Dim t8a As New Date
    '    Dim sql As String

    '    Dim docs2 As New List(Of String)
    '    docs2.Add(docNums(0))
    '    docs2.Add(docNums(1))
    '    Dim docs3 As New List(Of String)(docs2)
    '    docs3.Add(docNums(2))


    '    '  look for an unfilled inventory line
    '    sqlStmtSb.Append("SELECT distinct sl.del_doc_num FROM itm i, so_ln sl WHERE del_doc_num IN (").Append("{0}").Append(
    '                        ") AND void_flag != 'Y' AND i.itm_cd = sl.itm_cd ").Append(
    '                        "AND i.inventory = 'Y' and sl.store_cd IS NULL ")

    '    Dim sqlBase As String = sqlStmtSb.ToString
    '    Try

    '        Dim howMany As Integer = 101

    '        ' first time with 100 (ish: varied to make the 1st-time an actual parse)
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '              cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildAndBindValueList(docNums, ":ddn", cmd, howMany)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd
    '            t1a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t1b = Date.Now


    '            ' 2nd time with 100
    '            SystemUtils.FillMore(docNums, ":ddn", cmd, howMany, howMany)
    '            t2a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t2b = Date.Now


    '        End Using

    '        ' 3rd time with 2 plus nulls
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildAndBindValueList(docs2, ":ddn", cmd, howMany)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t3a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t3b = Date.Now

    '        End Using

    '        ' first time with 2
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildAndBindValueList(docs2, ":ddn", cmd, 2)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t4a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t4b = Date.Now

    '        End Using

    '        ' second time with 2
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildAndBindValueList(docs2, ":ddn", cmd, 2)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t5a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t5b = Date.Now

    '        End Using

    '        ' first time with 3
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildAndBindValueList(docs3, ":ddn", cmd, 3)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t6a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t6b = Date.Now

    '        End Using

    '        ' 1st time with all
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildValueList(docNums, ":ddn", cmd)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t7a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t7b = Date.Now

    '        End Using

    '        ' 1st time with all, they chance to be the same size
    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand(), oDatAdap As New OracleDataAdapter()

    '            Dim inlist As String = SystemUtils.BuildValueList(docNums, ":ddn", cmd)
    '            sql = String.Format(sqlBase, inlist)
    '            cmd.Connection = conn
    '            cmd.CommandText = sql
    '            oDatAdap.SelectCommand = cmd

    '            t8a = Date.Now
    '            oDatAdap.Fill(datSet)
    '            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then
    '                For Each tmRow In datSet.Tables(0).Rows
    '                    unfilled.Add(tmRow("DEL_DOC_NUM"))
    '                Next
    '            End If
    '            t8b = Date.Now

    '        End Using


    '        'Catch ex As Exception
    '        'System.Diagnostics.Debug.WriteLine(ex.ToString + ex.StackTrace)
    '    Finally

    '        Dim iv1 As TimeSpan = t1b.Subtract(t1a)
    '        Dim iv2 As TimeSpan = t2b.Subtract(t2a)
    '        Dim iv3 As TimeSpan = t3b.Subtract(t3a)
    '        Dim iv4 As TimeSpan = t4b.Subtract(t4a)
    '        Dim iv5 As TimeSpan = t5b.Subtract(t5a)
    '        Dim iv6 As TimeSpan = t6b.Subtract(t6a)
    '        Dim iv7 As TimeSpan = t7b.Subtract(t7a)
    '        Dim iv8 As TimeSpan = t8b.Subtract(t8a)

    '        System.Diagnostics.Debug.WriteLine("interval 1 [ 100 full 1st    ] " + iv1.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 2 [ 100 full 2nd    ] " + iv2.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 3 [ 2 plus 98 null ] " + iv3.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 4 [ 2 only 1st      ] " + iv4.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 5 [ 2 only 2nd      ] " + iv5.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 6 [ 3 only          ] " + iv6.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 7 [ 1st time all    ] " + iv7.ToString)
    '        System.Diagnostics.Debug.WriteLine("interval 8 [ 2nd time all    ] " + iv8.ToString)
    '        System.Diagnostics.Debug.WriteLine("all is " + docNums.Count.ToString)
    '    End Try

    '    Return

    'End Sub

    ' fragments from SetSbobDetails giving example of callingn some diagnostics and making timing comparisons in the IDE
    ' run the query timings
    'TestQueryVariations(listOfDocNum)
    'BenchUnfilledOrders(conn, listOfDocNum, 300, 100, -25)

    'Dim t1 As Date = Now
    'Dim specRow As Dictionary(Of String, String) = FindSpecRowProc(listOfDocNum, stCd)
    'Dim t2 As Date = Now
    'Dim specRow2 As Dictionary(Of String, String) = FindSpecRowProc(conn, listOfDocNum, stCd)
    'Dim t3 As Date = Now
    'Dim iv1 As TimeSpan = t2.Subtract(t1)
    'Dim iv2 As TimeSpan = t3.Subtract(t2)
    'System.Diagnostics.Debug.WriteLine("interval 1 [ spec full qry ] " + iv1.ToString)
    'System.Diagnostics.Debug.WriteLine("interval 2 [ spec stepwise ] " + iv2.ToString)


#End Region

#End Region




End Class
