Imports System.IO
Imports ErrorManager

''' <summary>
''' ==================================================
''' MASTER PAGE FOR SALES AND RELATIONSHIP/QUOTE ENTRY 
''' ==================================================
''' (NoWizard2 for rest of pages except file uploads)
''' </summary>

Partial Class Regular
    Inherits System.Web.UI.MasterPage

    Private salesBiz As New SalesBiz()

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub


    Private Sub PopulateTotals()

        Dim dblGiftCardBalance As Double = If(Not IsNothing(Session("gift_card_balance")) AndAlso IsNumeric(Session("gift_card_balance")), CDbl(Session("gift_card_balance")), 0)
        Dim dblSubtotal As Double = If(Not IsNothing(Session("subtotal")) AndAlso IsNumeric(Session("subtotal")), CDbl(Session("subtotal")), 0)
    
        lblSubtotal.Text = FormatCurrency(dblSubtotal, 2)

        If dblGiftCardBalance <> 0 Then
            lbl_balance.Text = FormatCurrency(dblGiftCardBalance, 2)
        Else
            lbl_balance.Text = FormatCurrency(dblSubtotal, 2)
        End If

    End Sub


    Private Sub Set_Links(ByVal enableLinks As Boolean)

        '  HyperLink2.Enabled = enableLinks
        '  HyperLink2.Visible = enableLinks

    End Sub

    Private Sub DisableSaleLinks()
        Set_Links(False)
        Session("SAL_LNKS_DISABLED") = "Y"
    End Sub


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("SAL_LNKS_DISABLED") = "N"

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "* TRAIN MODE *"
            lbl_header.ForeColor = Color.Red
        End If

        PopulateTotals()

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("logout.aspx")
        Else
            ''mm -swp,20,2016 - back button security concern
            Response.ClearHeaders()
            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
            Response.AddHeader("Pragma", "no-cache")
        End If

        Dim ProgressCount As Integer
        Dim strURL As String
        Dim arrayURL As Array
        Dim pagename As String
        Dim HEADER As String
        Dim LEAD As String
        LEAD = Request("LEAD")

        If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
            HEADER = Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            If Session("CUST_LNAME") & "" <> "" Then
                HEADER = HEADER & " working with customer " & Left(Session("CUST_LNAME"), 1) & LCase(Right(Session("CUST_LNAME"), Len(Session("CUST_LNAME")) - 1))
            End If
            lbl_Header1.Text = HEADER
        End If

        strURL = Request.ServerVariables("SCRIPT_NAME")
        arrayURL = Split(strURL, "/", -1, 1)
        pagename = arrayURL(UBound(arrayURL))

      
        ASPxMenu1.RootItem.Items(0).NavigateUrl = "~/newmain.aspx?verify_save=TRUE"

        If LEAD = "TRUE" Then
       
            '   HyperLink2.NavigateUrl = "customer.aspx?LEAD=TRUE&GIFT=TRUE"
   
            'Display should change to handle data necessary for a prospect entry ONLY!
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Entry"
   
            ASPxMenu1.RootItem.Items(1).NavigateUrl = "help.aspx?pagename=" & pagename

            ProgressCount = 0
            If Session("cust_cd") & "" <> "" Then
                '       img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
                ProgressCount = ProgressCount + 1
            Else
                '       img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
            End If
        Else
            'Data validation and screen displays that are pertinent to a sale entry
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Entry"
   
            Select Case LCase(pagename)
            End Select
            ProgressCount = 0
            If Session("cust_cd") & "" <> "" Then
                '       img_cust.ImageUrl = "images/icons/customerCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                '        img_cust.ImageUrl = "images/icons/customer.gif"
            End If
            
        End If

    End Sub

    Protected Sub ASPxRoundPanel1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxRoundPanel1.Init

        Dim LEAD As String = Request("LEAD")
        Dim strURL As String = Request.ServerVariables("SCRIPT_NAME")
        Dim arrayURL As Array = Split(strURL, "/", -1, 1)
        Dim pagename As String = arrayURL(UBound(arrayURL))
        Dim roundHdrLbl As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel4.FindControl("lbl_Round_Header")

        If LEAD = "TRUE" Then
            Select Case LCase(pagename)
                Case "customer.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : CUSTOMER : FIND CUSTOMER"
                Case "prs.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : CUSTOMER : SEARCH RESULTS"
                Case "cust_edit.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : CUSTOMER : EDIT/DETAILS"
                Case "lead_stage.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : COMPLETION"
            End Select
        Else
            Select Case LCase(pagename)
                Case "startup.aspx"
                    roundHdrLbl.Text = "SALE : CUSTOMER : STARTUP"
                Case "customer.aspx"
                    roundHdrLbl.Text = "SALE : CUSTOMER : FIND CUSTOMER"
                Case "prs.aspx"
                    roundHdrLbl.Text = "SALE : CUSTOMER : SEARCH RESULTS"
                Case "cust_edit.aspx"
                    roundHdrLbl.Text = "SALE : CUSTOMER : EDIT/DETAILS"
                Case "order.aspx"
                    roundHdrLbl.Text = "SALE : ORDER : FIND MERCHANDISE"
                Case "order_detail.aspx"
                    roundHdrLbl.Text = "SALE : ORDER : SHOPPING BAG"
                Case "discount.aspx"
                    roundHdrLbl.Text = "SALE : ORDER : DISCOUNT"
                Case "marketing.aspx"
                    roundHdrLbl.Text = "SALE : ORDER : MARKETING"
                Case "payment.aspx"
                    roundHdrLbl.Text = "SALE : PAYMENT : ADD/CHANGE"
                Case "inventoryresults.aspx"
                    roundHdrLbl.Text = "SALE : ORDER : SEARCH RESULTS"
                Case "comments.aspx"
                    roundHdrLbl.Text = "SALE : OTHER : COMMENTS"
                Case "delivery.aspx"
                    If Session("PD") = "P" Then
                        roundHdrLbl.Text = "SALE : OTHER : PICKUP DATE"
                    Else
                        roundHdrLbl.Text = "SALE : OTHER : DELIVERY ZONE"
                    End If
                Case "deliverydate.aspx"
                    If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Then
                        roundHdrLbl.Text = "SALE : OTHER : DELIVERY/PICKUP CHARGES"
                    Else
                        roundHdrLbl.Text = "SALE : OTHER : DELIVERY DATE"
                    End If
                Case "order_stage.aspx"
                    roundHdrLbl.Text = "SALE : ORDER COMPLETION"
            End Select
            Select Case Session("ord_tp_cd")
                Case "CRM"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "RETURN")
                    roundHdrLbl.ForeColor = Color.Red
                Case "MCR"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "MISCELLANEOUS CREDIT")
                    roundHdrLbl.ForeColor = Color.Red
                Case "MDB"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "MISCELLANEOUS DEBIT")
                    roundHdrLbl.ForeColor = Color.Red
                Case Else
                    roundHdrLbl.ForeColor = Color.Black
            End Select
        End If

    End Sub

End Class

