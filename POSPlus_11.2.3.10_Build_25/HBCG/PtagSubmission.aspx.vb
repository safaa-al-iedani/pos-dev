Imports System.Collections.Generic
Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient


Partial Class PtagSubmission
    Inherits POSBasePage
    Dim footer_message As String
    Public Const gs_version As String = "Version 1.0"

    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of Product Price tag Generation screen. 
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If IsPostBack = False Then
            Session("ptag_multi_item") = Nothing
            'ClientEnabled = "false" DB April 6, 2018 prevent page freeze
            cbx_multi_itm_cd.ClientEnabled = False
            Dim commandBtnPrint As GridViewCommandColumnCustomButton = New GridViewCommandColumnCustomButton()
            commandBtnPrint.ID = "print"
            commandBtnPrint.Text = Resources.CustomMessages.MSG0085
            commandBtnPrint.Visibility = GridViewCustomButtonVisibility.BrowsableRow

            Dim commandColPrint As GridViewCommandColumn = New GridViewCommandColumn(Resources.CustomMessages.MSG0085)
            commandColPrint.Name = Resources.CustomMessages.MSG0085
            commandColPrint.CustomButtons.Add(commandBtnPrint)
            GV_Pricetags.Columns.Add(commandColPrint)

            'Populate Store Code
            PopulateStoreCodes()

            printer_drp.Visible = False
            btn_print_summary.Visible = False
            btn_multi_item.Enabled = False
            cbo_report_type.Focus()
            'DB April 25 disable IT Req 3759
            'cbx_multi_itm_cd.ClientEnabled = True

        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To set title for this page.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0091

        'IT Req 3759
        cbx_multi_itm_cd.ClientEnabled = True

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Multi Item Checkbox click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_multi_item_Click(sender As Object, e As EventArgs) Handles btn_multi_item.Click
        popup_multi_item.ContentUrl = "Ptag_Multi_Item_Popup.aspx"
        popup_multi_item.ShowOnPageLoad = True
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Multi Item Checkbox changed event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cbx_multi_itm_cd_CheckedChanged(sender As Object, e As EventArgs) Handles cbx_multi_itm_cd.CheckedChanged
        If cbx_multi_itm_cd.Checked = True Then
            btn_multi_item.Enabled = True
            txt_itm_cd.Enabled = False
            txt_itm_cd.Text = ""
            popup_multi_item.ContentUrl = "Ptag_Multi_Item_Popup.aspx"
            popup_multi_item.ShowOnPageLoad = True
        Else
            btn_multi_item.Enabled = False
            Session("PtagSessionId") = ""

            txt_itm_cd.Enabled = True
            txt_itm_cd.Text = ""
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : GenerateTag button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_generate_tags_Click(sender As Object, e As EventArgs) Handles btn_generate_tags.Click
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim sqlString As String
        Dim seqString As String
        Dim QString As StringBuilder

        GV_Pricetags.DataSource = Nothing
        GV_Pricetags.DataBind()

        lbl_warning.ForeColor = Color.Red
        lbl_warning.Text = ""
        lbl_ps_message.Text = ""
        Validate_Fields()
        If String.IsNullOrEmpty(lbl_warning.Text) = False Then
            lbl_warning.Text = Resources.CustomMessages.MSG0092 + lbl_warning.Text
            Return
        End If
        Try


            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            ' sqlString = "select SEQ_BQORD.nextval from dual"

            'Alice udpate to avoid the duplicated session id for request #3759.
            sqlString = "SELECT OPS$DAEMON.PTAG_RPT_SEQ.NEXTVAL FROM DUAL"

            objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)

            objCmd.CommandText = sqlString
            objCmd.CommandType = CommandType.Text

            conn.Open()
            seqString = objCmd.ExecuteScalar()

            conn.Close()


            seqString = "BPTAG." + seqString



            objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
            objCmd.CommandText = "OPS$DAEMON.custom_brick_ptag_ppkg.ptaginputparam"

            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.Parameters.Clear()

            objCmd.Parameters.Add("sstorecd", OracleType.VarChar).Value = cbo_store_cd.SelectedItem.Value.ToString

            If String.IsNullOrEmpty(dt_eff_date.Text) Then
                objCmd.Parameters.Add("deffectivedate", OracleType.DateTime).Value = DBNull.Value
            Else
                objCmd.Parameters.Add("deffectivedate", OracleType.DateTime).Value = dt_eff_date.Value
            End If

            objCmd.Parameters.Add("spromocode", OracleType.VarChar).Value = txt_promo_cd.Text

            If String.IsNullOrEmpty(dt_promo_end_dt.Text) Then
                objCmd.Parameters.Add("dpromoenddt", OracleType.DateTime).Value = DBNull.Value
            Else
                objCmd.Parameters.Add("dpromoenddt", OracleType.DateTime).Value = dt_promo_end_dt.Value
            End If


            If String.IsNullOrEmpty(dt_update_from.Text) Then

                objCmd.Parameters.Add("dupdatedatefrom", OracleType.DateTime).Value = DBNull.Value

            Else
                objCmd.Parameters.Add("dupdatedatefrom", OracleType.DateTime).Value = dt_update_from.Value

            End If

            If String.IsNullOrEmpty(dt_update_to.Text) Then

                objCmd.Parameters.Add("dupdatedateto", OracleType.DateTime).Value = DBNull.Value

            Else
                objCmd.Parameters.Add("dupdatedateto", OracleType.DateTime).Value = dt_update_to.Value

            End If

            objCmd.Parameters.Add("sreporttype", OracleType.VarChar).Value = cbo_report_type.SelectedItem.Value.ToString


            If cbx_fm_loc.Value = True Then
                objCmd.Parameters.Add("sfloormodel", OracleType.VarChar).Value = "Y"

            Else
                objCmd.Parameters.Add("sfloormodel", OracleType.VarChar).Value = "N"

            End If

            objCmd.Parameters.Add("slocation", OracleType.VarChar).Value = txt_location_cd.Text

            objCmd.Parameters.Add("sgroupfrom", OracleType.VarChar).Value = txt_prod_grp_from.Text
            objCmd.Parameters.Add("sgroupto", OracleType.VarChar).Value = txt_prod_grp_to.Text

            objCmd.Parameters.Add("smajorfrom", OracleType.VarChar).Value = txt_mjr_code_from.Text
            objCmd.Parameters.Add("smajorto", OracleType.VarChar).Value = txt_mjr_code_to.Text

            objCmd.Parameters.Add("sminorfrom", OracleType.VarChar).Value = txt_mnr_code_from.Text
            objCmd.Parameters.Add("sminorto", OracleType.VarChar).Value = txt_mnr_code_to.Text

            If cbx_print_drop_flags.Value = True Then
                objCmd.Parameters.Add("sdropped", OracleType.VarChar).Value = "Y"

            Else
                objCmd.Parameters.Add("sdropped", OracleType.VarChar).Value = "N"

            End If

            objCmd.Parameters.Add("sinputitem", OracleType.VarChar).Value = txt_itm_cd.Text

            Dim tmpList As New List(Of String)

            If cbx_multi_itm_cd.Value = True Then
                Dim ds_multi_item As DataSet = Session("ptag_multi_item")
                Dim dt_multi_item As DataTable = ds_multi_item.Tables(0)

                For i As Integer = 0 To dt_multi_item.Rows.Count - 1
                    tmpList.Add(dt_multi_item.Rows(i)("itm_code").ToString)
                Next i

                objCmd.Parameters.Add("smultiitems", OracleType.VarChar)
                objCmd.Parameters("smultiitems").Value = String.Join("@@@", tmpList.ToArray)
            Else

                objCmd.Parameters.Add("smultiitems", OracleType.VarChar)
                objCmd.Parameters("smultiitems").Value = ""

            End If

            objCmd.Parameters.Add("sjobname", OracleType.VarChar).Value = seqString

            Dim ParamPtagSessionId As OracleParameter = New OracleParameter("ssessionid", OracleType.VarChar, 255)
            ParamPtagSessionId.Direction = ParameterDirection.Output

            Dim ParamErrorCd As OracleParameter = New OracleParameter("nreterrcd", OracleType.Int16)
            ParamErrorCd.Direction = ParameterDirection.Output

            Dim ParamErrorDesc As OracleParameter = New OracleParameter("sreterrmsg", OracleType.VarChar, 1000)
            ParamErrorDesc.Direction = ParameterDirection.Output

            objCmd.Parameters.Add(ParamPtagSessionId)
            objCmd.Parameters.Add(ParamErrorCd)
            objCmd.Parameters.Add(ParamErrorDesc)

            conn.Open()
            objCmd.ExecuteNonQuery()

            Session("PtagSessionId") = ParamPtagSessionId.Value.ToString()

            objCmd.Parameters.Clear()
            objCmd.CommandText = "OPS$DAEMON.custom_brick_ptag_ppkg.generateandloadptag"
            objCmd.CommandType = CommandType.StoredProcedure

            objCmd.Parameters.Add("ssessionid", OracleType.VarChar).Value = ParamPtagSessionId.Value.ToString()

            objCmd.Parameters.Add("ssource", OracleType.VarChar).Value = "POS+"

            Dim PtagErrorCd As OracleParameter = New OracleParameter("nreterrcd", OracleType.Int16)
            PtagErrorCd.Direction = ParameterDirection.Output

            Dim PtagErrorDesc As OracleParameter = New OracleParameter("sreterrmsg", OracleType.VarChar, 1000)
            PtagErrorDesc.Direction = ParameterDirection.Output

            objCmd.Parameters.Add(PtagErrorCd)
            objCmd.Parameters.Add(PtagErrorDesc)

            objCmd.ExecuteNonQuery()

            conn.Close()

            QString = New StringBuilder("SELECT prh.output_file_smt, ")
            QString.Append("prh.output_file_rfl, ")
            QString.Append("prh.output_file_trk, ")
            QString.Append("prh.output_file_hpt ")
            QString.Append("FROM ptag_rpt_run_hdr prh ")
            QString.Append("WHERE prh.session_id = '" & ParamPtagSessionId.Value.ToString() & "' AND EXISTS ")
            QString.Append("(SELECT 1 FROM ptag_rpt_run_dtls prd  ")
            QString.Append("WHERE prd.session_id= prh.session_id ) ")

            objCmd = DisposablesManager.BuildOracleCommand(QString.ToString, conn)
            objCmd.CommandText = QString.ToString
            objCmd.CommandType = CommandType.Text

            conn.Open()

            Dim datRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(objCmd)

            Dim ds As DataSet = New DataSet()
            Dim dt As DataTable
            Dim dr As DataRow

            dt = New DataTable()
            dt.Columns.Add("price_tags")

            If datRdr.Read() Then

                If Not IsDBNull(datRdr("output_file_smt")) Then
                    dr = dt.NewRow()
                    dr("price_tags") = "SMT"
                    dt.Rows.Add(dr)
                End If

                If Not IsDBNull(datRdr("output_file_rfl")) Then
                    dr = dt.NewRow()
                    dr("price_tags") = "RFL"
                    dt.Rows.Add(dr)
                End If

                If Not IsDBNull(datRdr("output_file_trk")) Then
                    dr = dt.NewRow()
                    dr("price_tags") = "TRK"
                    dt.Rows.Add(dr)
                End If

                If Not IsDBNull(datRdr("output_file_hpt")) Then
                    dr = dt.NewRow()
                    dr("price_tags") = "HPT"
                    dt.Rows.Add(dr)
                End If

                ds.Tables.Add(dt)

                GV_Pricetags.DataSource = ds
                GV_Pricetags.DataBind()

                If dt.Rows.Count > 0 Then
                    printer_drp.Visible = True
                    btn_print_summary.Visible = True
                    sy_printer_drp_Populate()

                End If

            Else
                GV_Pricetags.SettingsText.EmptyDataRow = Resources.CustomMessages.MSG0094
                printer_drp.Visible = False
                btn_print_summary.Visible = False

            End If

        Catch ex As Exception
            Throw
        End Try

        ' lbl_ps_message.Text = String.Format(Resources.CustomMessages.MSG0115, seqString)   'mariam - get this msg from wes then uncomment

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Clear button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        cbo_report_type.Value = ""
        cbx_fm_loc.Checked = False
        txt_promo_cd.Text = ""
        txt_prod_grp_from.Text = ""
        txt_prod_grp_to.Text = ""
        txt_mjr_code_from.Text = ""
        txt_mjr_code_to.Text = ""
        txt_mnr_code_from.Text = ""
        txt_mnr_code_to.Text = ""
        cbx_multi_itm_cd.Checked = False
        txt_itm_cd.Enabled = True
        Session("PtagSessionId") = Nothing
        txt_itm_cd.Text = ""
        cbo_store_cd.SelectedIndex = Convert.ToInt16((Session("defaultStoreIndex")))

        dt_eff_date.Text = ""
        dt_update_from.Text = ""
        dt_update_to.Text = ""
        btn_multi_item.Enabled = False
        dt_promo_end_dt.Text = ""
        lbl_warning.Text = ""
        lbl_ps_message.Text = ""
        footer_message = " "

        Session("ptag_multi_item") = Nothing

        GV_Pricetags.DataSource = Nothing
        GV_Pricetags.DataBind()

        cbo_report_type.Focus()

        printer_drp.Visible = False
        btn_print_summary.Visible = False


    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Grid view call back event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_Pricetags_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs) Handles GV_Pricetags.CustomButtonCallback
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ptagSessionId As String = Session("PtagSessionId")
        Dim emp_id As String = Session("EMP_CD")
        lbl_warning.Text = ""
        lbl_ps_message.Text = ""
        footer_message = " "
        Dim GV_grid As ASPxGridView = CType(sender, ASPxGridView)
        Dim GV_Footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = GV_grid.FindFooterRowTemplateControl("GV_Footer_label")

        If (e.ButtonID.Equals("print")) Then

            If printer_drp.SelectedItem.Value = Resources.CustomMessages.MSG0087 Then
                footer_message = Resources.CustomMessages.MSG0088
                GV_Footer_label.Text = footer_message
                Return
            End If

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            Dim keyValue = GV_Pricetags.GetRowValues(e.VisibleIndex, GV_Pricetags.KeyFieldName)

            objCmd.Connection = conn
            objCmd.CommandText = "OPS$DAEMON.custom_brick_ptag_ppkg.print_pricetag"

            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.Parameters.Clear()

            objCmd.Parameters.Add("p_prg_nm", OracleType.VarChar).Value = "PRINTBTN"
            objCmd.Parameters.Add("p_spooler", OracleType.VarChar).Value = printer_drp.SelectedItem.ToString
            objCmd.Parameters.Add("p_store_cd", OracleType.VarChar).Value = cbo_store_cd.SelectedItem.Value.ToString
            objCmd.Parameters.Add("p_session_id", OracleType.VarChar).Value = ptagSessionId
            objCmd.Parameters.Add("p_rpt_type", OracleType.VarChar).Value = keyValue
            objCmd.Parameters.Add("p_emp_id", OracleType.VarChar).Value = emp_id

            conn.Open()
            Try
                objCmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw
            End Try

            conn.Close()
            'GV_Footer_label.Text = String.Format(Resources.CustomMessages.MSG0095, ptagSessionId) 

            'Alice update on Sep24,2018 for request #3759
            GV_Footer_label.Text = String.Format(Resources.CustomMessages.MSG0095, ptagSessionId) + " " + ptagSessionId

        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Validate Ptag submission fields.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub Validate_Fields()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim sqlString As String
        Dim objValidate As Object
        lbl_warning.Text = ""
        lbl_ps_message.Text = ""

        printer_drp.Visible = False
        btn_print_summary.Visible = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()
        objCmd.Connection = conn
        objCmd.CommandType = CommandType.Text

        'Field Validation

        ''''' mar - feb 23,2017 - storecode, effectivedt, update range from, upd range to
        If cbo_store_cd.SelectedItem.Text.Length > 0 And
            Not String.IsNullOrEmpty(dt_eff_date.Text) And
             Not String.IsNullOrEmpty(dt_update_from.Text) And
                Not String.IsNullOrEmpty(dt_update_to.Text) Then

            'Effective Date Validate
            If String.IsNullOrEmpty(dt_eff_date.Text) = True Then
                lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0109
                Return
            End If

            'Update Date Range Validation
            If String.IsNullOrEmpty(dt_update_from.Text) = False Or String.IsNullOrEmpty(dt_update_to.Text) = False Then

                If String.IsNullOrEmpty(dt_update_from.Text) = True And String.IsNullOrEmpty(dt_update_to.Text) = False Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0110
                    Return
                End If

                If String.IsNullOrEmpty(dt_update_from.Text) = False And String.IsNullOrEmpty(dt_update_to.Text) = True Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0111
                    Return
                End If

                If String.IsNullOrEmpty(dt_update_to.Text) = False And dt_update_to.Value < dt_update_from.Value Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0112
                    Return
                End If

            End If
        Else
            If String.IsNullOrEmpty(txt_promo_cd.Text) = True And
                String.IsNullOrEmpty(txt_prod_grp_from.Text) = True And String.IsNullOrEmpty(txt_prod_grp_to.Text) = True And
                String.IsNullOrEmpty(txt_mjr_code_from.Text) = True And String.IsNullOrEmpty(txt_mjr_code_to.Text) = True And
                String.IsNullOrEmpty(txt_mnr_code_from.Text) = True And String.IsNullOrEmpty(txt_mnr_code_to.Text) = True And
                String.IsNullOrEmpty(txt_itm_cd.Text) = True And cbx_multi_itm_cd.Checked = False Then
                lbl_warning.Text = lbl_warning.Text + "</br>" + Resources.CustomMessages.MSG0096

            End If

            'Promotion Code Validation        
            If String.IsNullOrEmpty(txt_promo_cd.Text) = False Then
                sqlString = "SELECT prc_cd FROM prom_prc_tp WHERE prc_cd = '" & txt_promo_cd.Text & "'"

                objCmd.CommandText = sqlString

                objValidate = objCmd.ExecuteScalar()
                If objValidate = Nothing Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0097, txt_promo_cd.Text)

                End If
            End If

            'Product Group Validation
            If String.IsNullOrEmpty(txt_prod_grp_from.Text) = False Or String.IsNullOrEmpty(txt_prod_grp_to.Text) = False Then

                If String.IsNullOrEmpty(txt_prod_grp_from.Text) = True And String.IsNullOrEmpty(txt_prod_grp_to.Text) = False Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0098
                    Return
                End If

                If String.IsNullOrEmpty(txt_prod_grp_from.Text) = False Then
                    sqlString = "SELECT prod_grp_cd FROM prod_grp WHERE prod_grp_cd = '" & txt_prod_grp_from.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0099, txt_prod_grp_from.Text)
                        Return
                    End If
                End If
                If String.IsNullOrEmpty(txt_prod_grp_to.Text) = False And String.Compare(txt_prod_grp_from.Text, txt_prod_grp_to.Text) <> 0 Then
                    sqlString = "SELECT prod_grp_cd FROM prod_grp WHERE prod_grp_cd = '" & txt_prod_grp_to.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0100, txt_prod_grp_to.Text)

                    End If

                End If
            End If

            'Product Major Validation
            If String.IsNullOrEmpty(txt_mjr_code_from.Text) = False Or String.IsNullOrEmpty(txt_mjr_code_to.Text) = False Then

                If String.IsNullOrEmpty(txt_mjr_code_from.Text) = True And String.IsNullOrEmpty(txt_mjr_code_to.Text) = False Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0101
                    Return
                End If

                If String.IsNullOrEmpty(txt_mjr_code_from.Text) = False Then
                    sqlString = "SELECT mjr_cd FROM inv_mjr WHERE mjr_cd = '" & txt_mjr_code_from.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0102, txt_mjr_code_from.Text)
                        Return
                    End If
                End If
                If String.IsNullOrEmpty(txt_mjr_code_to.Text) = False And String.Compare(txt_mjr_code_from.Text, txt_mjr_code_to.Text) <> 0 Then
                    sqlString = "SELECT mjr_cd FROM inv_mjr WHERE mjr_cd = '" & txt_mjr_code_to.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0103, txt_mjr_code_to.Text)
                    End If

                End If
            End If

            'Product Minor Validation
            If String.IsNullOrEmpty(txt_mnr_code_from.Text) = False Or String.IsNullOrEmpty(txt_mnr_code_to.Text) = False Then

                If String.IsNullOrEmpty(txt_mnr_code_from.Text) = True And String.IsNullOrEmpty(txt_mnr_code_to.Text) = False Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0104
                    Return
                End If

                If String.IsNullOrEmpty(txt_mnr_code_from.Text) = False Then
                    sqlString = "SELECT mnr_cd FROM inv_mnr WHERE mnr_cd = '" & txt_mnr_code_from.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0105, txt_mnr_code_from.Text)
                        Return
                    End If
                End If
                If String.IsNullOrEmpty(txt_mnr_code_to.Text) = False And String.Compare(txt_mnr_code_from.Text, txt_mnr_code_to.Text) <> 0 Then
                    sqlString = "SELECT mnr_cd FROM inv_mnr WHERE mnr_cd = '" & txt_mnr_code_to.Text & "'"
                    objCmd.CommandText = sqlString

                    objValidate = objCmd.ExecuteScalar()
                    If objValidate = Nothing Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0106, txt_mnr_code_to.Text)

                    End If

                End If
            End If

            'Multiple Items checking
            If cbx_multi_itm_cd.Checked = True Then
                If IsNothing(Session("ptag_multi_item")) Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0107

                Else
                    Dim ds As DataSet = Session("ptag_multi_item")
                    If ds.Tables(0).Rows.Count = 0 Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0107
                    End If
                    If ds.Tables(0).Rows.Count = 1 Then
                        lbl_warning.Text = lbl_warning.Text + "<br/>" + Resources.CustomMessages.MSG0093
                    End If
                End If
            End If
            If String.IsNullOrEmpty(txt_itm_cd.Text) = False Then
                sqlString = "SELECT itm_cd FROM itm WHERE itm_cd = '" & txt_itm_cd.Text & "'"
                objCmd.CommandText = sqlString

                objValidate = objCmd.ExecuteScalar()
                If objValidate = Nothing Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0108, txt_itm_cd.Text)

                End If
            End If

            'Location Code Validation - Dec 30, 2016        
            If String.IsNullOrEmpty(txt_location_cd.Text) = False Then
                sqlString = "SELECT  loc_cd FROM loc WHERE loc_cd = '" & txt_location_cd.Text & "'"

                objCmd.CommandText = sqlString

                objValidate = objCmd.ExecuteScalar()
                If objValidate = Nothing Then
                    lbl_warning.Text = lbl_warning.Text + "<br/>" + String.Format(Resources.CustomMessages.MSG0115, txt_promo_cd.Text)

                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To populate printer list in combo box
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub sy_printer_drp_Populate()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd)

        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = Resources.CustomMessages.MSG0087
        End If

        Session("default_prt") = v_default_printer

        ' Dec 27, 2016 - MARIAM -  Change SQL to include Invoice as well as Regular Report Printers
        ' sql = "select pi.printer_nm PRINT_Q "
        ' sql = sql & "from  printer_info pi "
        ' sql = sql & "where pi.store_cd = (select home_store_cd "
        ' sql = sql & "from emp "
        ' sql = sql & "where emp_init = :v_user_init) "
        ' sql = sql & "and   pi.printer_tp = 'INVOICE'"

        sql = "select c.print_q from print_queue c where SUBSTR(c.print_q,1,2)=(select home_store_cd from emp where emp_init = :v_user_init) "
        sql = sql & " union "
        sql = sql & " select pi.printer_nm PRINT_Q from  printer_info pi "
        sql = sql & " where pi.store_cd = (select home_store_cd from emp where emp_init =:v_user_init)"
        sql = sql & " and   pi.printer_tp = 'INVOICE' "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub
    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To employee intials
    ''' </summary>
    ''' <remarks></remarks>
    Private Function sy_get_emp_init(ByVal p_user As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader As OracleDataReader

        Try
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDatareader.Read Then
                Return MyDatareader.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To get default printer
    ''' </summary>
    ''' <param name="p_user"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function sy_get_default_printer_id(ByVal p_user As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Print Summary button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_print_summary_Click(sender As Object, e As EventArgs) Handles btn_print_summary.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ptagSessionId As String = Session("PtagSessionId")
        Dim emp_id As String = Session("EMP_CD")
        lbl_ps_message.Text = ""

        If printer_drp.SelectedItem.Value = Resources.CustomMessages.MSG0087 Then
            lbl_warning.Text = Resources.CustomMessages.MSG0088
            Return
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        objCmd.Connection = conn
        objCmd.CommandText = "OPS$DAEMON.custom_brick_ptag_ppkg.print_pricetag"

        objCmd.CommandType = CommandType.StoredProcedure
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add("p_prg_nm", OracleType.VarChar).Value = "SUMMARY"
        objCmd.Parameters.Add("p_spooler", OracleType.VarChar).Value = printer_drp.SelectedItem.ToString
        objCmd.Parameters.Add("p_store_cd", OracleType.VarChar).Value = cbo_store_cd.SelectedItem.Value.ToString
        objCmd.Parameters.Add("p_session_id", OracleType.VarChar).Value = Session("PtagSessionId")
        objCmd.Parameters.Add("p_rpt_type", OracleType.VarChar).Value = cbo_report_type.SelectedItem.Value.ToString
        objCmd.Parameters.Add("p_emp_id", OracleType.VarChar).Value = Session("EMP_CD")

        conn.Open()
        objCmd.ExecuteNonQuery()
        conn.Close()

        lbl_ps_message.Text = Resources.CustomMessages.MSG0089

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To populate store code
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        If (Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty Then

            SQL = "SELECT s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_store_desc "
            SQL = SQL & "FROM store s, store_grp$store sgs, store_grp sg "
            SQL = SQL & "WHERE s.store_cd = sgs.store_cd "
            SQL = SQL & "AND sgs.store_grp_cd = sg.store_grp_cd "
            SQL = SQL & "AND NVL(sg.pricing_store_grp,'N') = 'Y' "
            SQL = SQL & "AND co_cd = UPPER(:x_co_cd) "
            SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)
            cmdGetCodes.Parameters.Add(":x_co_cd", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_co_cd").Value = s_co_cd

            Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)

            Try
                dataAdapter.Fill(ds)

            Catch
                Throw
                connErp.Close()
            End Try
            connErp.Close()

            If ds.Tables(0).Rows.Count = 0 Then
                lbl_warning.Text = Resources.CustomMessages.MSG0039
                Return
            End If

            Dim defaultStoreIndex As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                cbo_store_cd.Items.Add(row.ItemArray(2).ToString(), row.ItemArray(0).ToString(), row.ItemArray(1).ToString())
            Next

            cbo_store_cd.SelectedIndex = defaultStoreIndex
            Session("defaultStoreIndex") = defaultStoreIndex

        End If
    End Sub

End Class