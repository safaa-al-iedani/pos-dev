Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization


Partial Class LoadEHPriceItems
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Load_Items(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()
            objSql.CommandText = "select distinct ITM_CD,PRC,TERM,UNIT_PRC, IND_PRC," &
                "decode(ITM_CD,null,'SKU cannot be null.', ' ') || " &
                "decode(UPLOAD_ITEMS.is_number(TRIM(PRC)) * UPLOAD_ITEMS.is_number(TRIM(TERM)) * " &
                "UPLOAD_ITEMS.is_number(Trim(UNIT_PRC)) " &
                ", 0, 'Invalid number in item price', ' ') err_msg from EHPRC_GENERATION_EXT a "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer
                For i = 0 To numrows - 1
                    If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        err_cnt = 1
                    End If
                Next
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No data found. Please upload the file."

            ElseIf err_cnt = 0 Then
                btn_upload.Visible = False
                btn_process.Visible = True
                lbl_msg.Text = "File Loaded - Please process the Items."
                Label1.Text = Label1.Text & "<br>" &
                            "Upload Easy Home Pricing Successful. Please Validate and Process."
            Else
                btn_upload.Visible = True
                btn_process.Visible = False
                lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_upload.Visible = True
            btn_process.Visible = False
            lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Function validate_items(ByVal sender As Object, ByVal e As System.EventArgs) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim err_cnt As Integer = 0

        Try
            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select a.ITM_CD ITM_CD, a.PRC, a.TERM, a.UNIT_PRC, a.IND_PRC, ' ' err_msg, " +
                "(select count(itm_cd) from itm where itm.itm_cd = a.ITM_CD) ITM_CNT, " +
                "(select count(itm_cd) from itm$itm_srt where itm$itm_srt.itm_cd = a.ITM_CD and itm_srt_cd = 'LCE') SRT_CD_CNT " +
                "from ehprc_generation a"
            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count
            Dim ITM_CD As String

            If ds.Tables(0).Rows.Count = 0 Then
                lbl_msg.Text = "No data found. Please upload the file."
            Else
                Dim i As Integer

                For i = 0 To numrows - 1
                    Dim ITM_CNT As String = ds.Tables(0).Rows(i)("ITM_CNT").ToString
                    If String.IsNullOrEmpty(ITM_CNT) OrElse Integer.Parse(ITM_CNT) < 1 Then
                        err_cnt += 1
                        ds.Tables(0).Rows(i)("err_msg") += "Item does not exist in ITEM table. "
                    End If
                Next

                For i = 0 To numrows - 1
                    Dim SRT_CD_CNT As String = ds.Tables(0).Rows(i)("SRT_CD_CNT").ToString

                    If String.IsNullOrEmpty(SRT_CD_CNT) OrElse Integer.Parse(SRT_CD_CNT) < 1 Then
                        err_cnt += 1
                        ds.Tables(0).Rows(i)("err_msg") += "Item does not have a LCE sort code. Adjust in ITEM and reload. "
                    End If
                Next

                For i = 0 To numrows - 1
                    ITM_CD = ds.Tables(0).Rows(i)("ITM_CD").ToString

                    If ITM_CD.Length > 9 Then
                        err_cnt += 1
                        ds.Tables(0).Rows(i)("err_msg") += "Item code cannot exceed 9 characters. "
                    End If
                Next

                If err_cnt = 0 Then
                    For i = 0 To numrows - 1
                        ITM_CD = ds.Tables(0).Rows(i)("ITM_CD").ToString
                        For j = i + 1 To numrows - 1
                            If ITM_CD = ds.Tables(0).Rows(j)("ITM_CD") Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Item is duplicate. "
                            End If
                        Next
                    Next
                End If

                If err_cnt = 0 Then
                    For i = 0 To numrows - 1
                        If String.IsNullOrEmpty(ds.Tables(0).Rows(i)("PRC").ToString) OrElse Not IsNumeric(ds.Tables(0).Rows(i)("PRC").ToString) Then
                            err_cnt += 1
                            ds.Tables(0).Rows(i)("err_msg") += "Weekly (PRC) value must be numeric: Adjust and reload. "
                        ElseIf ds.Tables(0).Rows(i)("PRC").ToString.Contains(".") _
                        AndAlso ds.Tables(0).Rows(i)("PRC").ToString.Split(".")(1).Length > 2 Then
                            err_cnt += 1
                            ds.Tables(0).Rows(i)("err_msg") += "Weekly (PRC) value cannot have more than 2 decimal places. "
                        End If
                    Next
                End If

                If err_cnt = 0 Then
                    For i = 0 To numrows - 1
                        If Not String.IsNullOrEmpty(ds.Tables(0).Rows(i)("TERM").ToString) Then
                            If Not IsNumeric(ds.Tables(0).Rows(i)("TERM").ToString) Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Lease (TERM) value must be numeric: Adjust and reload. "
                            ElseIf ds.Tables(0).Rows(i)("TERM").ToString.Contains(".") _
                            AndAlso ds.Tables(0).Rows(i)("TERM").ToString.Split(".")(1).Length > 2 Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Lease (TERM) value cannot have more than 2 decimal places. "
                            End If
                        End If
                    Next
                End If

                If err_cnt = 0 Then
                    For i = 0 To numrows - 1
                        If Not String.IsNullOrEmpty(ds.Tables(0).Rows(i)("UNIT_PRC").ToString) Then
                            If Not IsNumeric(ds.Tables(0).Rows(i)("UNIT_PRC").ToString) Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Unit Price (UNIT_PRC) value must be numeric: Adjust and reload. "
                            ElseIf ds.Tables(0).Rows(i)("UNIT_PRC").ToString.Contains(".") _
                            AndAlso ds.Tables(0).Rows(i)("UNIT_PRC").ToString.Split(".")(1).Length > 2 Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Unit Price (UNIT_PRC) value cannot have more than 2 decimal places. "
                            End If
                        End If
                    Next
                End If

                If err_cnt = 0 Then
                    For i = 0 To numrows - 1
                        If Not String.IsNullOrEmpty(ds.Tables(0).Rows(i)("IND_PRC").ToString) Then
                            If Not IsNumeric(ds.Tables(0).Rows(i)("IND_PRC").ToString) Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Individual Price (IND_PRC) value must be numeric: Adjust and reload. "
                            ElseIf ds.Tables(0).Rows(i)("IND_PRC").ToString.Contains(".") _
                            AndAlso ds.Tables(0).Rows(i)("IND_PRC").ToString.Split(".")(1).Length > 2 Then
                                err_cnt += 1
                                ds.Tables(0).Rows(i)("err_msg") += "Individual Price (IND_PRC) value cannot have more than 2 decimal places. "
                            End If
                        End If
                    Next
                End If

                Gridview1.DataSource = ds
                Gridview1.DataBind()
            End If

            If err_cnt = 0 Then
                Return "Y"
            Else
                Return "N"
            End If
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        Finally
            conn.Close()
        End Try

    End Function

    Protected Sub btn_process_Click(sender As Object, e As EventArgs) Handles btn_process.Click
        ' Validate and process
        Label1.Text = ""
        Dim valid, staged As String

        staged = stage_items()
        If staged = "Y" Then
            valid = validate_items(sender, e)

            If valid = "Y" Then
                If process_items() = "Y" Then
                    btn_upload.Visible = False
                    btn_process.Visible = False
                    lbl_msg.Text = "ITEM's successfully uploaded."
                    Return
                End If
            End If
        End If
        btn_upload.Visible = True
        btn_process.Visible = False
        lbl_msg.Text = "Errors found. Please correct the data and upload file again."

    End Sub

    Public Function stage_items() As String
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim myCMD As OracleCommand = Nothing

        Dim x As Exception

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            myCMD = DisposablesManager.BuildOracleCommand()

            myCMD.Transaction = objConnection.BeginTransaction

            myCMD.Connection = objConnection
            myCMD.CommandType = CommandType.Text

            myCMD.CommandText = "DELETE EHPRC_GENERATION"

            myCMD.ExecuteNonQuery()

            myCMD.CommandText = "INSERT INTO EHPRC_GENERATION SELECT DISTINCT LPAD(TRIM(ITM_CD), 8, '0'),TRIM(PRC),TRIM(TERM), " +
                                "TRIM(UNIT_PRC), TRIM(IND_PRC), '' ERR_MSG FROM EHPRC_GENERATION_EXT a WHERE ITM_CD IS NOT NULL"

            myCMD.ExecuteNonQuery()

            myCMD.CommandText = "UPDATE EHPRC_GENERATION  SET PRC = '0' WHERE PRC IS NULL"

            myCMD.ExecuteNonQuery()

            myCMD.CommandText = "UPDATE EHPRC_GENERATION  SET IND_PRC = UNIT_PRC WHERE IND_PRC IS NULL"

            myCMD.ExecuteNonQuery()

            myCMD.Transaction.Commit()

            Return "Y"

        Catch x
            myCMD.Transaction.Rollback()
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function process_items() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim myCMD As OracleCommand = Nothing

        Dim x As Exception

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            myCMD = DisposablesManager.BuildOracleCommand()

            myCMD.Transaction = objConnection.BeginTransaction

            myCMD.Connection = objConnection
            myCMD.CommandType = CommandType.Text

            myCMD.CommandText = "DELETE EZSTORE_RET_CLNDR"

            myCMD.ExecuteNonQuery()

            myCMD.CommandText = "INSERT INTO EZSTORE_RET_CLNDR SELECT DISTINCT 1 EZ_PRC_ZONE, TRIM(ITM_CD),(sysdate -1) EFF_DT, " +
                                "'31-DEC-2049' END_DT, TRIM(PRC),TRIM(TERM), TRIM(UNIT_PRC), TRIM(IND_PRC) FROM EHPRC_GENERATION"

            myCMD.ExecuteNonQuery()

            myCMD.Transaction.Commit()

            Return "Y"

        Catch x
            myCMD.Transaction.Rollback()
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Protected Sub btn_upload_Click(ByVal sender As Object,
      ByVal e As System.EventArgs)
        Try
            If FileUpload1.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "ehprc_generation.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadEHPrc\done\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadEHPrc\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label1.Text = "File name: " &
                          FileUpload1.PostedFile.FileName & "<br>" &
                          "File Size: " &
                          FileUpload1.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadEHPrc\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        theSystemBiz.SaveAuditLogComment("LOAD_EHPRC", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        ' Display data from external table
                        Load_Items(sender, e)

                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label1.Text = "Only .csv files allowed!"
                End If
            Else
                Label1.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_EHPRC", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" &
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        ' Clear
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Private Sub LoadEHPriceItems_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If isAuthToAccess() = "Y" OrElse isSuperUser(Session("EMP_CD")) = "Y" Then
        Else
            Div1.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
    End Sub

    Public Function isAuthToAccess() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoAccess('" & Session("emp_cd") & "','LoadEHPriceItems.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Public Shared Function isSuperUser(ByRef p_emp_cd As String)
        Dim p_sup_flag As String = "N'"
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add("p_co_grp_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_co_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If Not IsDBNull(myCMD.Parameters("p_ind").Value) Then
                p_sup_flag = myCMD.Parameters("p_ind").Value
            End If

        Catch x As Exception
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_sup_flag
    End Function

End Class
