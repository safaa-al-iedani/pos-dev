Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils

Partial Class PaymentServerQueryGridResults
    Inherits POSBasePage

    Public Const gs_version As String = "Version 4.1"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY - see PaymentServerQuery.aspx.vb
    '
    '------------------------------------------------------------------------------------------------------------------------------------


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'keep in mind this index gets refreshed everytime the next, prev, first, last buttons are pressed
        Session("PSQGRID_INDEX") = 1

        If Not IsPostBack = True Then
            GridView1_Binddata()
        End If

        lbl_versionInfo.Text = gs_version

        'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'Tooltip for Button's
        btnFirst.ToolTip = GetLocalResourceObject(("tool_btnFirst.Text").ToString())
        btnLast.ToolTip = GetLocalResourceObject(("tool_btnLast.Text").ToString())
        btnPrev.ToolTip = GetLocalResourceObject(("tool_btnPrev.Text").ToString())
        btnNext.ToolTip = GetLocalResourceObject(("tool_btnNext.Text").ToString())
        'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
    End Sub

    Protected Sub cbo_records_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_records.SelectedIndexChanged
        Session("SOCACHED") = Nothing
        GridView.PageIndex = 0
        GridView.SettingsPager.PageSize = cbo_records.SelectedValue
        GridView1_Binddata()

    End Sub

    Private Sub GridView1_Binddata()

        GridView.DataSource = ""
        GridView.Visible = True

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView()

        If (ds Is Nothing OrElse Not ds.IsInitialized) Then
            'do nothing
        Else
            dv = New DataView(ds.Tables(0))
        End If

        GridView.DataSource = dv
        GridView.DataBind()

        GridView.Width = 1400
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'lbl_pageinfo.Text = "Page " + CStr(GridView.PageIndex + 1) + " of " + CStr(GridView.PageCount)
        lbl_pageinfo.Text = String.Format(Resources.CustomMessages.MSG0059, CStr(GridView.PageIndex + 1), CStr(GridView.PageCount))
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
        ' make all the buttons visible if page count is more than 0
        If GridView.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If GridView.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView.PageIndex = (GridView.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView.PageCount = 0 Then
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'Label1.Text = "Sorry, no records found.  Please, <a href=""PaymentServerQuery.aspx"">search again</a>"
            Label1.Text = Resources.CustomMessages.MSG0048 + "<a href=""PaymentServerQuery.aspx"">search again</a>"
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Dim n_records_per_page = cbo_records.SelectedValue
        'Dim n_blahindex = Session("PSQGRID_INDEX")

        Select Case strArg
            Case "Next"
                If GridView.PageIndex < (GridView.PageCount - 1) Then
                    GridView.PageIndex += 1
                    Session("PSQGRID_INDEX") += (GridView.PageIndex * n_records_per_page)

                End If
            Case "Prev"
                If GridView.PageIndex > 0 Then
                    GridView.PageIndex -= 1
                    Session("PSQGRID_INDEX") += (GridView.PageIndex * n_records_per_page)

                End If
            Case "Last"
                GridView.PageIndex = GridView.PageCount - 1
                Session("PSQGRID_INDEX") += (GridView.PageCount - 1) * n_records_per_page

            Case Else
                GridView.PageIndex = 0
                Session("PSQGRID_INDEX") = 1

        End Select

        'n_blahindex = Session("PSQGRID_INDEX")
        GridView1_Binddata()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
    End Sub

    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If Not IsNothing(e.GetValue("PET_PS_INVOICE").ToString()) Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridView.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)

            Dim n_index As Integer = 0

            If Not IsNothing(ASPxMenu2) Then

                n_index = Session("PSQGRID_INDEX")

                ASPxMenu2.RootItem.Items(0).NavigateUrl = "PaymentServerQuery.aspx?ROW_NUM=" & e.GetValue("ROW_NUM").ToString() & "&POSITION_INDEX=" & n_index.ToString() & "&selected_row=Y"
                'MsgBox("position=" + e.GetValue("COLUMN_ROWID").ToString())
                'MsgBox("blah " + e.KeyValue)

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'ASPxMenu2.RootItem.Items(0).ToolTip = "Select the row and view the details of this transaction"
                ASPxMenu2.RootItem.Items(0).ToolTip = Resources.CustomMessages.MSG0049
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                n_index += 1
                Session("PSQGRID_INDEX") = n_index

            End If
        End If
    End Sub

    Protected Sub grid_DataBinding(ByVal sender As Object, ByVal e As EventArgs)

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView()

        If (ds Is Nothing OrElse Not ds.IsInitialized) Then
            'do nothing
        Else
            dv = New DataView(ds.Tables(0))
        End If

        GridView.DataSource = dv
    End Sub

    ''mariam - Oct 18, 2017
    Protected Sub GridView_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles GridView.CustomColumnDisplayText
        Const SECURITY_GIFTCARD As String = "PSQPOSGC"
        Const SECURITY_POSPSQCARD As String = "POSPSQCARD"

        Dim colnames As String() = {}
        Dim val As Object

        Dim ccType As String = String.Empty
        Dim ccNo As String = String.Empty
        Dim ccNoMasked As String = String.Empty

        'Oct 13, 2017 - mariam - 3324
        'Mask card no for - visa, master card, american express
        'Gift card - if user has security then display card no else mask it
        'Debit card and TDF - display card number
        'for others - if user have security to see then display else mask

        If e.Column.FieldName = "PET_CARD_NUMBER_MASKED" Then
            ccType = If(IsDBNull(e.GetFieldValue("PET_CARD_TYPE")), String.Empty, e.GetFieldValue("PET_CARD_TYPE").ToString)
            ccNo = If(IsDBNull(e.GetFieldValue("PET_CARD_NUMBER")), String.Empty, e.GetFieldValue("PET_CARD_NUMBER").ToString)
            ccNoMasked = If(IsDBNull(e.GetFieldValue("PET_CARD_NUMBER_MASKED")), String.Empty, e.GetFieldValue("PET_CARD_NUMBER_MASKED").ToString)

            If ccNo.Length > 4 Then
                If InStr(ccType, "VISA") > 0 Or InStr(ccType, "AMERICAN EXPRESS") > 0 Or InStr(ccType, "MASTER CARD") > 0 Then
                    e.DisplayText = ccNoMasked
                ElseIf InStr(ccType, "GIFT CARD") > 0 Then
                    e.DisplayText = If(SecurityUtils.hasSecurity(SECURITY_GIFTCARD, Session("emp_init")) = True, ccNo, ccNoMasked)
                ElseIf InStr(ccType, "DEBIT") > 0 Or InStr(ccType, "TDF") > 0 Then
                    e.DisplayText = ccNo
                Else
                    If SecurityUtils.hasSecurity(SECURITY_POSPSQCARD, Session("emp_init")) Then
                        e.DisplayText = ccNo
                    Else
                        'if NOT Debit card nor TDF or user have NO security for other cards then MASK card no
                        e.DisplayText = ccNo.Substring(ccNo.Length - 4).PadLeft(ccNo.Length, "X")
                    End If
                End If
            Else
                e.DisplayText = ccNo
            End If
        End If
    End Sub

End Class
