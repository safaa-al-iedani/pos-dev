<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement_Release_Hold.aspx.vb" Inherits="Settlement_Release_Hold" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="100%" align="left" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td align="left" valign="middle">
                            <b>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Release Hold Transactions">
                                </dx:ASPxLabel>
                                &nbsp;</b>
                        </td>
                        <td valign="middle">
                            <dx:ASPxCheckBox ID="chk_errors" runat="server" Text="Show Errors Only" AutoPostBack="True">
                            </dx:ASPxCheckBox>
                        </td>
                        <td valign="middle">
                            <dx:ASPxCheckBox ID="chk_deleted" runat="server" Text="Show Deleted Only" AutoPostBack="True">
                            </dx:ASPxCheckBox>
                        </td>
                    </tr>
                </table>
                <hr />
                <br />
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle" style="height: 48px">
                            &nbsp;<dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Transactions Selected:">
                            </dx:ASPxLabel>
                            &nbsp;<dx:ASPxLabel ID="lbl_tran_count" runat="server" Text="">
                            </dx:ASPxLabel>
                            <br />
                            <br />
                            <dx:ASPxComboBox ID="store_cd" runat="server" AutoPostBack="True" Width="316px"
                                IncrementalFilteringMode ="StartsWith">
                            </dx:ASPxComboBox>
                        </td>
                        <td align="right" valign="middle" style="width: 602px; height: 48px;">
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Transaction Amount Selected: $">
                            </dx:ASPxLabel>
                            <dx:ASPxLabel ID="lbl_tran_amt" runat="server">
                            </dx:ASPxLabel>
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                            &nbsp;
                            <dx:ASPxButton ID="btn_release" runat="server" Text="Release Transactions">
                            </dx:ASPxButton>
                            <br />
                            <dx:ASPxLabel ID="lbl_grid_msg" runat="server" Width="100%" ForeColor="Red">
                            </dx:ASPxLabel>
                            <br />
                            <asp:DataGrid ID="GridView1" runat="server" AutoGenerateColumns="False" Height="200px"
                                Width="100%" AllowSorting="True" OnSortCommand="MyDataGrid_Sort">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="SelectThis" runat="server" AutoPostBack="true" OnCheckedChanged="Update_Checked_Items">
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Date" SortExpression="WR_DT">
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_wr_dt" runat="server" Text='<%# FormatDateTime(DataBinder.Eval(Container, "DataItem.WR_DT"),vbShortDate) %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn HeaderText="Customer" SortExpression="CUST_CD">
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_cust_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CUST_CD") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Account Number" SortExpression="BNK_CRD_NUM">
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_bnk_crd_num" runat="server" Width="160px" MaxLength="16" AutoPostBack="true" OnTextChanged="BNK_CRD_UPDATE" Text='<%# DataBinder.Eval(Container, "DataItem.BNK_CRD_NUM") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BNK_CRD_NUM" HeaderText="Account Number" SortExpression="BNK_CRD_NUM"
                                        Visible="False">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Store" SortExpression="STORE_CD">
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_store_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.STORE_CD") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Amount" DataField="AMT" Visible="False" SortExpression="AMT"
                                        DataFormatString="{0:F2}"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Amount" SortExpression="AMT">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_amt" runat="server" Width="70px" MaxLength="8" AutoPostBack="true" OnTextChanged="AMT_UPDATE" Text='<%# DataBinder.Eval(Container, "DataItem.AMT") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="APP_CD" HeaderText="Auth Code" SortExpression="APP_CD"
                                        Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Auth Code" SortExpression="APP_CD">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_auth_cd" runat="server" Width="70px" MaxLength="6" AutoPostBack="true" OnTextChanged="AUTH_CD_UPDATE" Text='<%# DataBinder.Eval(Container, "DataItem.APP_CD") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Plan Code" DataField="REF_NUM" SortExpression="REF_NUM"
                                        Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Plan Code" SortExpression="REF_NUM">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_promo_cd" runat="server" Width="40px" MaxLength="5" AutoPostBack="true" OnTextChanged="PROMO_CD_UPDATE" Text='<%# DataBinder.Eval(Container, "DataItem.REF_NUM") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Type" SortExpression="MEDIUM_TP_CD">
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_med_tp_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MEDIUM_TP_CD") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Sales Order" SortExpression="DEL_DOC_NUM">
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_del_doc_num" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DEL_DOC_NUM") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <dx:ASPxButton ID="Button1" runat="server" Text="Remove">
                                            </dx:ASPxButton>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MEDIUM_TP_CD" HeaderText="Sales Order" SortExpression="ORD_TP_CD"
                                        Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="STAT_CD" HeaderText="Sales Order" SortExpression="STAT_CD"
                                        Visible="False"></asp:BoundColumn>
                                </Columns>
                                <SelectedItemStyle BackColor="Silver" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                            </asp:DataGrid><br />
                            <dx:ASPxButton ID="btn_release2" runat="server" Text="Release Transactions">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" style="height: 19px">
                <asp:Label ID="lbl_header" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
