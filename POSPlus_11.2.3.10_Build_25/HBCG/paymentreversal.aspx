﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="paymentreversal.aspx.vb" Inherits="paymentreversal" Title="Payment Reversal"%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
       <div width="120%" align="center">
        <br />
        &nbsp;<br />
        <table>
            <tr>
                <td> 
            <asp:Label ID="ASPxLabel1" runat="server" font="large" Text="Brick Payment Reversal" Font-Bold="True" Font-Size="Large"></asp:Label>
                    </td>
                </tr>
            </table>
    <table style="position: relative; width: 100%; left: 0px; top: -2px; height: 323px;">
        <tr>
            <td style="width: 200px">
                
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="" >
                </dx:ASPxLabel>
                
            </td>
                                    
        </tr>
  <tr>
      <td>
            <dx:ASPxLabel ID="id1" runat="server" Text="Customer Code" Font-Bold="True">
                </dx:ASPxLabel>
      </td>
      <td style="width: 150px">
          <asp:TextBox ID="cust_cd" runat="server" Width="133px"></asp:TextBox>
           <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Customer.aspx">Find A Customer</asp:HyperLink>
      </td>
  </tr>
        <tr>
      <%-- </td><td style="height: 13px"> --%>
            <td>
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="FROM DATE" Font-Bold="True">
                </dx:ASPxLabel>
      </td>
      <%--<td style="width: 150px; height: 13px">--%>
            <td>
          <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="TO DATE" Font-Bold="True">
                </dx:ASPxLabel>
      </td>
  </tr>
        <tr>
             <%--<td style="width: 202px; height: 218px;">--%>
                <td>
                                <asp:Calendar ID="from_dt" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" style="margin-top: 42px">
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#808080" />
                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                </asp:Calendar>
                </td>
               
                          

            <td>
                               <asp:Calendar ID="to_dt" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" style="margin-top: 42px">
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#808080" />
                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                </asp:Calendar>
            </td>
            <td style="width: 202px;">
                              <dx:ASPxButton ID="search_btn" runat="server" Text="Lookup Payment" Font-Bold="True">
                </dx:ASPxButton>
             </td>
        </tr>
   
         
    </table>
    
           <table>
                <tr>
                    <td style="width: 727px">
                        <asp:TextBox ID="lbl_msg" runat="server" AutoPostBack="true" Width="658px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
                    </td>
                </tr>
            </table>
    <br />
    <br />
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
        OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False" CellPadding="2" DataKeyField="ivc_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="ivc_cd" HeaderText="Invoice" ReadOnly="True" Visible="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="trn_tp_cd" HeaderText="Transaction Type" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="mop_cd" HeaderText="MOP" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <%-- sabrina R1910 --%>
            <asp:BoundColumn DataField="db_card_no" HeaderText="DB CARD#" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
             <%-- END sabrina R1910 --%>
            <asp:BoundColumn DataField="post_dt" HeaderText="Post Date" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" HeaderText="Amount" DataFormatString="{0:0.00}">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="td_card_no" HeaderText="TD Card#" DataFormatString="{0:0.00}">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="Reverse Payment">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
          
        </columns>
        <itemstyle verticalalign="Top" />
    </asp:DataGrid>&nbsp;
       <%--' payment popup start   --%>
    <dxpc:ASPxPopupControl ID="ASPxPopuppaymentreversal" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Processing Payment Reversal" Height="99px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Middle" Width="437px"
        CssClass="style5" BackColor="#99CCFF" Font-Bold="True" ForeColor="Red">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl6" DefaultButton="btnSubmit" runat="server">
                <table class="style5">
                   <tr>
                       <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Amount:" Font-Bold="True">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                       <td align="left" style="width: 538px">
                            <asp:TextBox ID="payr_amt" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="false" ReadOnly="True" BackColor="#99CCFF" BorderStyle="None"></asp:TextBox>
                        </td>
                   </tr>
                     <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Debit Card:" Font-Bold="True">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left" style="width: 538px">
                            <asp:TextBox ID="debit_card_no" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                        
                    </tr>
                         
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="commit_btn" runat="server" Text="Payment Reversal" Font-Bold="True" >
                                        </dx:ASPxButton>
                                    </td>
                                    
                                </tr>
                            </table>
                            <asp:TextBox ID="popup_msg" runat="server" AutoPostBack="true" Width="610px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="False" Font-Size="Medium" ForeColor="Red" BackColor="#99CCFF" Height="22px"></asp:TextBox>
                                                       
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <%--' popup end  --%>
            
            </div>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

