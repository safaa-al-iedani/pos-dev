Imports System.Data.OracleClient

Partial Class SaleHst
    Inherits POSBasePage

    Dim running_balance As Double = 0
    Dim quote_balance As Double = 0
    Dim theSalesBiz As SalesBiz = New SalesBiz()

    

    Function SortOrder2(ByVal Field As String) As String

        Dim so As String = Session("SortOrder2")

        If Field = so Then
            SortOrder2 = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder2 = Replace(Field, "desc", "asc")
        Else
            SortOrder2 = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder2") = SortOrder2

    End Function

    

   



    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("sal_SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("sal_SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        DataGrid1.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter
        Dim emp_init As String = Session("emp_init")
        objConnect = DisposablesManager.BuildOracleConnection

        Dim co_cd = Session("co_cd")
        Dim co_grp_cd = Session("str_co_grp_cd")

        Dim strCust_cd As String

        ' Franchise changes

        If Session("from_soCmnt") = "Y" Then
            strCust_cd = Session("sht_cust_cd")
            Session("from_soCmnt") = "N"
        ElseIf Len(Session("sht_cust_cd")) > 9 Then
            strCust_cd = Session("sht_cust_cd")
        Else
            strCust_cd = Request("cust_cd")
            Session("sht_cust_cd") = strCust_cd
        End If
        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'Daniela prevent SOM+ crash for missing so record

        sql = "SELECT SALES_ORDER, REFERENCE,ORD_TP,FINAL_DATE, ITM_CD,QTY,UNIT_PRC, OUT_ASIS,WTY FROM cust_buy_hst WHERE "
        sql = sql & "CUST_CD = '" & strCust_cd & "'  "
        sql = sql & " and std_multi_co2.isvalidco('" & emp_init & "', cust_buy_hst.co_cd) = 'Y'"
        sql = sql & " and std_multi_co.isvaliditm('" & emp_init & "', itm_cd) = 'Y'"
        sql = sql & " and std_multi_co2.isvalidstr7('" & emp_init & "', so_store_cd) = 'Y'"


        If Not isEmpty(txt_corp_name.Text) And Len(txt_corp_name.Text) > 1 Then
            sql = sql & " and sales_order like '" & UCase(txt_corp_name.Text) & "'"

        End If

        If Not isEmpty(txt_acct_balance.Text) And Len(txt_acct_balance.Text) > 0 Then
            sql = sql & " and itm_cd like '" & UCase(txt_acct_balance.Text) & "'"

        End If
        sql = sql & " ORDER BY FINAL_DATE desc ,sales_order,ITM_CD"



        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

        'Assign sort expression to Session              
        Session("sal_SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows = 0 Then
            DataGrid1.Visible = False
            lbl_Cust_Info.Visible = True
            lbl_Cust_Info.Text = "No records were found matching your search criteria." & vbCrLf
        Else
            '  Source.Sort = SortField
            DataGrid1.DataSource = Source
            DataGrid1.DataBind()
            lbl_Cust_Info.Visible = False
        End If
        'Close connection           
        objConnect.Close()
        '  txt_acct_balance.Text = FormatCurrency(running_balance, 2)

    End Sub

    Protected Sub btn_submit_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        '  If Session("from_soCmnt") = "Y" Then
        ' Dim strcust_cd As String = Session("sht_cust_cd")
        '  txt_acct_balance.Text = Request("del_doc_num")

        ' End If

        lucy_page()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            Dim dsStates As DataSet = Nothing



            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then

                lbl_Cust_Info.Text = ""
            Else

                lbl_Cust_Info.Text = ""
            End If

            If Session("from_soCmnt") = "Y" Then
                Dim strcust_cd As String = Session("sht_cust_cd")
            Else
                Session("hst_cust_cd") = Request("cust_cd")

            End If

        End If

        lucy_page()
    End Sub

    Protected Sub btn_Exit_Click()
        Dim strcust_cd As String = Session("sht_cust_cd")
        lbl_Cust_Info.Text = ""
        Response.Redirect("CPHQ.aspx?cust_cd=" & HttpUtility.UrlEncode(strcust_cd))
        Session("from_sht") = "Y"
    End Sub

    Protected Sub btn_Clear_Click()
        txt_acct_balance.Text = ""
        txt_corp_name.Text = ""

        lucy_page()
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If



    End Sub



    Protected Sub lucy_page()

        '  If txt_Cust_cd.Text & "" <> "" Then

        'Set up default column sorting   
        If IsNothing(Session("sal_SortOrder")) Or IsDBNull(Session("sal_SortOrder")) Then
            BindData("sales_order asc")
        Else
            BindData(Session("sal_SortOrder"))
        End If

        ' End If
    End Sub


    Protected Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound

        'lucy 
        Dim str_c As String
         

        If IsNumeric(e.Item.Cells(5).Text) Then
            '     Dim txt_qty As Label = CType(e.Item.FindControl("lbl_balance"), Label)
            Dim txt_qty As Label
            ' txt_qty.Text = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
            ' running_balance = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
        End If
        If Len(e.Item.Cells(0).Text) > 10 Then
            '  Dim hpl_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink1"), HyperLink)
            Dim hp2_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink2"), HyperLink)
            If e.Item.Cells(2).Text = "SAL" Or e.Item.Cells(2).Text = "CRM" Or e.Item.Cells(2).Text = "MCR" Or e.Item.Cells(2).Text = "MDB" Then

                If InStr(e.Item.Cells(0).Text, "#") > 0 Then
                    ' hpl_ivc.NavigateUrl = "#"
                    hp2_ivc.NavigateUrl = "#"
                Else


                    '  hpl_ivc.NavigateUrl = "~/soCmnt.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(9).Text   'link
                    hp2_ivc.NavigateUrl = "~/soCmnt.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(0).Text



                End If
            Else
                ' hpl_ivc.NavigateUrl = "#"
                hp2_ivc.NavigateUrl = "#"
            End If
            ' hpl_ivc.Text = Replace(e.Item.Cells(9).Text, "#", "")
            hp2_ivc.Text = Replace(e.Item.Cells(9).Text, "#", "")
        End If

    End Sub




End Class

