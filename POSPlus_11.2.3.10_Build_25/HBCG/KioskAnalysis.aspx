<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="KioskAnalysis.aspx.vb" Inherits="salestrends" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0">
        <tr>
            <td align="right">
                <asp:DropDownList ID="cbo_store" runat="server" Width="193px" CssClass="style5" AutoPostBack="true">
                </asp:DropDownList>&nbsp;<br />
                <dx:ASPxCalendar ID="txt_start_dt" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                    Width="192px" ShowWeekNumbers="False" SelectedDate="2010-11-07">
                    <FooterStyle Spacing="3px"></FooterStyle>
                    <FastNavFooterStyle Spacing="3px">
                    </FastNavFooterStyle>
                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                    </FastNavStyle>
                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ValidationSettings>
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                        </ErrorFrameStyle>
                    </ValidationSettings>
                    <HeaderStyle Spacing="8px"></HeaderStyle>
                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                </dx:ASPxCalendar>
            </td>
        </tr>
    </table>
    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="213px" Width="686px">
        <seriestemplate>
            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</PointOptionsSerializable>
            <LabelSerializable>
<cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized">
                <FillStyle >
                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                </FillStyle>
            </cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</LegendPointOptionsSerializable>
            <ViewSerializable>
<cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
            </cc1:SideBySideBarSeriesView>
</ViewSerializable>
        </seriestemplate>
        <fillstyle>
            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
        </fillstyle>
        <titles>
            <cc1:ChartTitle Text="Kiosk Activity Break-Out">
            </cc1:ChartTitle>
        </titles>
    </dxchartsui:WebChartControl>
    <br />
    <br />
    <dxchartsui:WebChartControl ID="WebChartControl2" runat="server" Height="213px" Width="686px">
        <seriestemplate>
            <ViewSerializable>
<cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
            </cc1:SideBySideBarSeriesView>
</ViewSerializable>
            <LabelSerializable>
<cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                <FillStyle >
                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                </FillStyle>
            </cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</PointOptionsSerializable>
            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</LegendPointOptionsSerializable>
        </seriestemplate>
        <fillstyle>
            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
        </fillstyle>
        <titles>
            <cc1:ChartTitle Text="Kiosk New Application Break-Out">
            </cc1:ChartTitle>
        </titles>
    </dxchartsui:WebChartControl>
    <br />
    <asp:Label ID="lbl_kiosk_details" runat="server" Text="" Width="683px" CssClass="style5">
    </asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
