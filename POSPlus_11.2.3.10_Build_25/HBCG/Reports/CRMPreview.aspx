<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CRMPreview.aspx.vb" Inherits="Reports_CRMPreview"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="mainbody" runat="server">
        <br />
        <table width="100%">
            <tr>
                <td width="100%" valign="top">
                    <div>
                        <table>
                            <tr>
                                <td style="text-align: right">
                                    Customer:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_company" runat="server" Width="560px" CssClass="style5">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    Salesperson:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_slsp" runat="server" Width="560px" CssClass="style5">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    Relationship<br />
                                    Date:
                                </td>
                                <td nowrap>
                                    <table>
                                        <tr>
                                            <td style="width: 3px">
                                                <dx:ASPxCalendar ID="txt_start_dt" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                                    Width="192px" ShowWeekNumbers="False">
                                                    <FooterStyle Spacing="3px"></FooterStyle>
                                                    <FastNavFooterStyle Spacing="3px">
                                                    </FastNavFooterStyle>
                                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                                    </FastNavStyle>
                                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ValidationSettings>
                                                        <ErrorFrameStyle ImageSpacing="4px">
                                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                                        </ErrorFrameStyle>
                                                    </ValidationSettings>
                                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                                </dx:ASPxCalendar>
                                            </td>
                                            <td>
                                                &nbsp;through&nbsp;
                                            </td>
                                            <td>
                                                <dx:ASPxCalendar ID="txt_end_dt" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                                    Width="192px" ShowWeekNumbers="False">
                                                    <FooterStyle Spacing="3px"></FooterStyle>
                                                    <FastNavFooterStyle Spacing="3px">
                                                    </FastNavFooterStyle>
                                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                                    </FastNavStyle>
                                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ValidationSettings>
                                                        <ErrorFrameStyle ImageSpacing="4px">
                                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                                        </ErrorFrameStyle>
                                                    </ValidationSettings>
                                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                                </dx:ASPxCalendar>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btn_submit" runat="server" Text="Run Report" Width="120px" CssClass="style5"
                                        OnClick="btn_submit_Click" />
                                    &nbsp;
                                    <input id="Reset1" style="width: 120px" type="reset" value="Reset Options" onclick="return Reset1_onclick()"
                                        class="style5" />
                                    &nbsp;
                                    <asp:Button ID="btn_exit" runat="server" Text="Clear Report" Width="120px" CssClass="style5" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                        BorderColor="White" BorderStyle="None" BorderWidth="0" PagerSettings-Visible="false"
                        PageSize="10" Width="100%">
                        <PagerSettings Visible="False" />
                        <Columns>
                            <asp:BoundField DataField="REL_NO" HeaderText="Relationship" SortExpression="Delivery Document">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WR_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Written Date"
                                HtmlEncode="False" SortExpression="Written Date">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CORP_NAME" HeaderText="Corporation" SortExpression="Corporation">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LNAME" HeaderText="Last Name" SortExpression="Last Name">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CITY" HeaderText="City" SortExpression="City" Visible="false">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HPHONE" HeaderText="Home Phone" SortExpression="Home Phone">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PROSPECT_TOTAL" HeaderText="Total" SortExpression="Total">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="STORE_CD" HeaderText="Store" SortExpression="Store Code">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                                <ControlStyle CssClass="style5" />
                            </asp:CommandField>
                        </Columns>
                        <HeaderStyle Font-Bold="True" ForeColor="Black" />
                        <AlternatingRowStyle BackColor="Beige" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td align="center" style="width: 700px; height: 35px">
                    <asp:Button ID="btnFirst" runat="server" CommandArgument="First" CssClass="style5"
                        OnClick="PageButtonClick" Text="<< First" ToolTip="Go to first page" Visible="false" />
                    &nbsp;<asp:Button ID="btnPrev" runat="server" CommandArgument="Prev" CssClass="style5"
                        OnClick="PageButtonClick" Text="< Prev" ToolTip="Go to the previous page" Visible="false" />
                    &nbsp;<asp:Button ID="btnNext" runat="server" CommandArgument="Next" CssClass="style5"
                        OnClick="PageButtonClick" Text="Next >" ToolTip="Go to the next page" Visible="False" />
                    &nbsp;<asp:Button ID="btnLast" runat="server" CommandArgument="Last" CssClass="style5"
                        OnClick="PageButtonClick" Text="Last >>" ToolTip="Go to the last page" Visible="false" />
                    &nbsp; &nbsp; &nbsp;
                    <asp:Label ID="lbl_pageinfo" runat="server" Text="Label" Visible="false"></asp:Label>
                    <div id="Div1" runat="server">
                        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px">
                            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
