Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Reports_SalesRegisterReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack = True Then
            If Session("EMP_CD") & "" = "" Then
                Response.Redirect("login.aspx")
                Exit Sub
            End If
            ' populate the store drop down
            store_cd_populate()
        End If

    End Sub

    Public Sub store_cd_populate()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store "
            SQL = SQL & "WHERE STORE_CD IN(select store_grp$store.store_cd "
            SQL = SQL & "from store_grp$store, emp2store_grp "
            SQL = SQL & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
            SQL = SQL & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            SQL = SQL & " order by store_cd"
        Else
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store order by store_cd"
        End If

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click
        Response.Redirect("../newmain.aspx")
    End Sub
End Class
