<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesRegisterReport.aspx.vb"
    Inherits="Reports_SalesRegisterReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Register Report</title>
    <link href="../report_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <br />
            <table width="100%">
                <tr>
                    <td valign="middle" align="center">
                        <table width="565" border="0" cellpadding="0" cellspacing="0" background="../images/wideBG.gif">
                            <tr>
                                <td>
                                    <img src="../images/wideTOP.jpg" width="565" height="25"></td>
                            </tr>
                            <tr>
                                <td align="center" class="header">
                                    <asp:Label ID="lblHeaderDesc" runat="server" Text="Sales Register Report"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="../images/wideformTOP.gif" width="565" height="20"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" background="../images/wideformBG.gif">
                                        <tr>
                                            <td>
                                                <img src="../images/shim.gif" width="41" height="1"></td>
                                            <td width="100%" valign="top">
                                                <div style="height=333">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 117px; text-align: right">
                                                                Store Code:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cbo_store_cd" runat="server" Width="285px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 117px; text-align: right">
                                                                Transaction Type:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cbo_tran_tp" runat="server" Width="285px">
                                                                    <asp:ListItem Selected="True" Value="W">Written</asp:ListItem>
                                                                    <asp:ListItem Value="D">Delivered</asp:ListItem>
                                                                    <asp:ListItem Value="B">Both</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 117px; text-align: right">
                                                                Written/Finalized Date:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txt_start_dt" runat="server" Width="80px"></asp:TextBox>&nbsp;
                                                                through
                                                                <asp:TextBox ID="txt_end_dt" runat="server" Width="80px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" style="width: 117px; text-align: right">
                                                                Sort By:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                                                    <asp:ListItem Selected="True" Value="TRAN_DT">Date</asp:ListItem>
                                                                    <asp:ListItem Value="STORE_CD">Store Code</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Button ID="btn_submit" runat="server" Text="Run Report" Width="120px" />
                                                                &nbsp;
                                                                <input id="Reset1" style="width: 120px" type="reset" value="Reset Options" />
                                                                &nbsp;
                                                                <asp:Button ID="btn_exit" runat="server" Text="Exit Report" Width="120px" />
                                                                </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td>
                                                <img src="../images/shim.gif" width="30" height="1">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="../images/wideformBASE.gif" width="565" height="20">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 24px">
                                    <img src="../images/wideBASE.jpg" width="565" height="22">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
