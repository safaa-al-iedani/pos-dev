Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Reports_SalesReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack = True Then
            ' populate the store drop down
            store_cd_populate()
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store "
            SQL = SQL & "WHERE STORE_CD IN(select store_grp$store.store_cd "
            SQL = SQL & "from store_grp$store, emp2store_grp "
            SQL = SQL & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
            SQL = SQL & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            SQL = SQL & " order by store_cd"
        Else
            SQL = "SELECT store_grp_cd, des, store_grp_cd || ' - ' || des as full_desc FROM store_grp order by store_grp_cd"
        End If

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .DataValueField = "store_grp_cd"
                .DataTextField = "full_desc"
                .DataBind()
                .SelectedValue = "ALL"
            End With
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click
        Response.Redirect("../newmain.aspx")
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim del_chg As Double = 0
        Dim tax_chg As Double = 0
        Dim setup_chg As Double = 0
        Dim subtotal As Double = 0
        Dim sql As String
        Dim HTML As String = ""

        If Not IsDate(txt_start_dt.Text) And Not IsDate(txt_end_dt.Text) Then
            lbl_errors.Text = "You must enter a start and end written date."
        End If
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Select * from SO WHERE SO_WR_DT BETWEEN TO_DATE('" & txt_start_dt.Text
        sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
        sql = sql & "AND SO_STORE_CD IN(SELECT STORE_CD FROM STORE_GRP$STORE WHERE STORE_GRP_CD ='"
        sql = sql & cbo_store_cd.SelectedValue & "') "
        If cbo_ord_stat.SelectedValue <> "B" Then
            sql = sql & "AND STAT_CD = '" & cbo_ord_stat.SelectedValue & "' "
        End If
        If IsDate(txt_del_start.Text) And IsDate(txt_del_end.Text) Then
            sql = sql & "AND PU_DEL_DT BETWEEN TO_DATE('" & txt_del_start.Text
            sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_del_end.Text & "','mm/dd/RRRR') "
        End If
        If cbo_ord_tp_cd.SelectedValue <> "ALL" Then
            sql = sql & "AND ORD_TP_CD = '" & cbo_ord_tp_cd.SelectedValue & "' "
        End If
        If cbo_pd.SelectedValue <> "B" Then
            sql = sql & "AND PU_DEL = '" & cbo_pd.SelectedValue & "' "
        End If
        sql = sql & "ORDER BY " & RadioButtonList1.SelectedValue

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                HTML = "<div align=center>"
                HTML = HTML & "<table border=0 width=""97%"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey align=center><b>SALES ORDER REPORT</b></td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=2 bgcolor=lightgrey align=left>Run By: " & Session("EMP_CD") & "</td>"
                HTML = HTML & "<td colspan=3 bgcolor=lightgrey align=right>" & Now() & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5><hr></td>"
                HTML = HTML & "</tr>"
            Else
                HTML = "<br />No records returned.<br /><br /><br />"
            End If
            Do While MyDataReader.Read
                subtotal = 0
                If MyDataReader.Item("DEL_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("DEL_CHG")) Then
                        del_chg = CDbl(MyDataReader.Item("DEL_CHG").ToString)
                    Else
                        del_chg = 0
                    End If
                Else
                    del_chg = 0
                End If
                If MyDataReader.Item("SETUP_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("SETUP_CHG")) Then
                        setup_chg = CDbl(MyDataReader.Item("SETUP_CHG").ToString)
                    Else
                        setup_chg = 0
                    End If
                Else
                    setup_chg = 0
                End If
                If MyDataReader.Item("TAX_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("TAX_CHG")) Then
                        tax_chg = CDbl(MyDataReader.Item("TAX_CHG").ToString)
                    Else
                        tax_chg = 0
                    End If
                Else
                    tax_chg = 0
                End If
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("SHIP_TO_F_NAME").ToString & " " & MyDataReader.Item("SHIP_TO_L_NAME").ToString & "</td>"
                HTML = HTML & "<td align=left>"
                HTML = HTML & "<a href=""../SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & MyDataReader.Item("DEL_DOC_NUM") & """>" & MyDataReader.Item("DEL_DOC_NUM") & "</a></td>"
                HTML = HTML & "<td align=left>Customer:&nbsp;<a href=""../Main_Customer.aspx?query_returned=Y&cust_cd=" & MyDataReader.Item("CUST_CD") & """>" & MyDataReader.Item("CUST_CD") & "</a></td>"
                'Pickup/Delivery Information
                HTML = HTML & "<td colspan=2 rowspan=4 align=right valign=top>"
                HTML = HTML & "<table border=0 width=""80%"" cellpadding=0 cellspacing=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>&nbsp;&nbsp;"
                Select Case MyDataReader.Item("PU_DEL").ToString
                    Case "P"
                        HTML = HTML & "Pickup - "
                    Case "D"
                        HTML = HTML & "Delivery - "
                End Select
                If IsDate(MyDataReader.Item("PU_DEL_DT").ToString) Then
                    HTML = HTML & FormatDateTime(MyDataReader.Item("PU_DEL_DT").ToString, DateFormat.ShortDate) & "&nbsp;&nbsp;<br />"
                Else
                    HTML = HTML & MyDataReader.Item("PU_DEL_DT").ToString & "<br />"
                End If
                HTML = HTML & "&nbsp;&nbsp;FROM: " & MyDataReader.Item("PU_DEL_STORE_CD").ToString & "<br />"
                HTML = HTML & "&nbsp;&nbsp;ZONE: " & MyDataReader.Item("SHIP_TO_ZONE_CD").ToString & "<br />"
                HTML = HTML & "&nbsp;&nbsp;TRK#: "

                'Display delivery truck information, if necessary
                If MyDataReader.Item("PU_DEL").ToString = "D" Then
                    sql = "Select TRK_NUM, STOP#, SEQ# from TRK_STOP Where DEL_DOC_NUM = '" & MyDataReader.Item("DEL_DOC_NUM").ToString & "'"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    Try
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                        If MyDataReader2.Read Then
                            HTML = HTML & MyDataReader2.Item("TRK_NUM").ToString & " - " & MyDataReader2.Item("STOP#").ToString & " - " & MyDataReader2.Item("SEQ#").ToString
                        End If
                        MyDataReader2.Close()
                    Catch
                        Throw
                    End Try
                End If

                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "</table>"
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("SHIP_TO_ADDR1").ToString & "</td>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("ORD_TP_CD").ToString & "&nbsp;&nbsp;(" & MyDataReader.Item("STAT_CD").ToString & ")"
                Select Case MyDataReader.Item("STAT_CD").ToString
                    Case "O"
                        HTML = HTML & "pen"
                    Case "F"
                        HTML = HTML & "inalized"
                    Case "V"
                        HTML = HTML & "oided"
                End Select
                HTML = HTML & "</td>"
                HTML = HTML & "<td>&nbsp;</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("SHIP_TO_CITY").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("SHIP_TO_ST_CD").ToString & "&nbsp;&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("SHIP_TO_ZIP_CD") & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("SHIP_TO_H_PHONE").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("SHIP_TO_B_PHONE").ToString & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>&nbsp;</td>"
                'Populate the Sales Order Line information
                HTML = HTML & "<td colspan=4 align=left>"
                sql = "SELECT SO_LN.DEL_DOC_LN#, SO_LN.VOID_FLAG, SO_LN.QTY, ITM.VE_CD, ITM.VSN, SO_LN.ITM_CD, SO_LN.STORE_CD, SO_LN.LOC_CD, SO_LN.UNIT_PRC FROM SO_LN, ITM "
                sql = sql & "WHERE SO_LN.DEL_DOC_NUM = '" & MyDataReader.Item("DEL_DOC_NUM").ToString & "' "
                sql = sql & "AND SO_LN.ITM_CD=ITM.ITM_CD ORDER BY DEL_DOC_LN#"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Dim FoundME As Boolean = False
                    If MyDataReader2.Read Then
                        HTML = HTML & "<table width=""100%"" cellpadding=0 cellspacing=0>"
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("DEL_DOC_LN#").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td align=left width=20>" & MyDataReader2.Item("STORE_CD").ToString & "</td>"
                        HTML = HTML & "<td align=left width=20>" & MyDataReader2.Item("LOC_CD").ToString & "</td>"
                        If MyDataReader2.Item("VOID_FLAG").ToString = "Y" Then
                            HTML = HTML & "<td width=10>0</td>"
                        Else
                            HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                            If MyDataReader.Item("ORD_TP_CD").ToString = "CRM" Or MyDataReader.Item("ORD_TP_CD").ToString = "MCR" Then
                                subtotal = subtotal + -CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("UNIT_PRC").ToString)
                            Else
                                subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("UNIT_PRC").ToString)
                            End If
                        End If
                        If MyDataReader.Item("ORD_TP_CD").ToString = "CRM" Or MyDataReader.Item("ORD_TP_CD").ToString = "MCR" Then
                            HTML = HTML & "<td align=right width=100>" & -MyDataReader2.Item("UNIT_PRC").ToString & "</td>"
                        Else
                            HTML = HTML & "<td align=right width=100>" & MyDataReader2.Item("UNIT_PRC").ToString & "</td>"
                        End If
                        HTML = HTML & "</tr>"
                        FoundME = True
                    End If
                    Do While MyDataReader2.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("DEL_DOC_LN#").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td align=left width=20>" & MyDataReader2.Item("STORE_CD").ToString & "</td>"
                        HTML = HTML & "<td align=left width=20>" & MyDataReader2.Item("LOC_CD").ToString & "</td>"
                        If MyDataReader2.Item("VOID_FLAG").ToString = "Y" Then
                            HTML = HTML & "<td width=10>0</td>"
                        Else
                            HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                            If MyDataReader.Item("ORD_TP_CD").ToString = "CRM" Or MyDataReader.Item("ORD_TP_CD").ToString = "MCR" Then
                                subtotal = subtotal + -CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("UNIT_PRC").ToString)
                            Else
                                subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("UNIT_PRC").ToString)
                            End If
                        End If
                        If MyDataReader.Item("ORD_TP_CD").ToString = "CRM" Or MyDataReader.Item("ORD_TP_CD").ToString = "MCR" Then
                            HTML = HTML & "<td align=right width=100>" & -MyDataReader2.Item("UNIT_PRC").ToString & "</td>"
                        Else
                            HTML = HTML & "<td align=right width=100>" & MyDataReader2.Item("UNIT_PRC").ToString & "</td>"
                        End If
                        HTML = HTML & "</tr>"
                    Loop
                    If FoundME = True Then
                        HTML = HTML & "</table>"
                    End If
                    MyDataReader2.Close()
                Catch
                    Throw
                End Try
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 align=left><b><u><a href=../SalesOrderComments.aspx?del_doc_num=" & MyDataReader.Item("del_doc_num").ToString
                HTML = HTML & "&cust_cd=" & MyDataReader.Item("CUST_CD").ToString & ">Comments:</a></u></b></td>"
                HTML = HTML & "</tr>"

                'Add Sales Order Comments
                sql = "Select DT, TEXT, CMNT_TYPE from SO_CMNT Where SO_SEQ_NUM = '" & MyDataReader.Item("SO_SEQ_NUM").ToString & "'"
                sql = sql & " AND SO_WR_DT=TO_DATE('" & FormatDateTime(MyDataReader.Item("SO_WR_DT").ToString, DateFormat.ShortDate) & "','mm/dd/RRRR') AND SO_STORE_CD = '" & MyDataReader.Item("SO_STORE_CD").ToString
                sql = sql & "' ORDER BY SEQ#"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Dim Repeat_Me As Boolean = True
                Dim comment_data As String = ""
                Dim term_comm As Boolean = False
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If Not MyDataReader2.Read Then
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td colspan=3>&nbsp;</td>"
                    Else
                        comment_data = comment_data & "<tr><td colspan=3 align=left valign=top>"
                        comment_data = comment_data & "<table width=""100%"" celpadding=0 cellspacing=0>"
                        term_comm = True
                    End If
                    Do While MyDataReader2.Read
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td align=left width=70>" & FormatDateTime(MyDataReader2.Item("DT").ToString, DateFormat.ShortDate)
                        comment_data = comment_data & "&nbsp;(" & MyDataReader2.Item("CMNT_TYPE").ToString & ")</td>"
                        comment_data = comment_data & "<td colspan=2 align=left>" & MyDataReader2.Item("TEXT").ToString & "</td>"
                        comment_data = comment_data & "</tr>"
                    Loop
                    MyDataReader2.Close()
                Catch
                    Throw
                End Try
                If term_comm = True Then
                    comment_data = comment_data & "</table></td><td colspan=2 valign=top align=right>"
                Else
                    comment_data = comment_data & "<td colspan=2 valign=top align=right>"
                End If

                'Add the balance and payment information
                Dim payment_data As String

                If MyDataReader.Item("ORD_TP_CD").ToString <> "SAL" Then
                    sql = "SELECT TRN_TP_CD, AMT FROM AR_TRN WHERE ADJ_IVC_CD = '" & MyDataReader.Item("DEL_DOC_NUM").ToString & "'"
                Else
                    sql = "SELECT TRN_TP_CD, AMT FROM AR_TRN WHERE IVC_CD = '" & MyDataReader.Item("DEL_DOC_NUM").ToString & "'"
                End If

                'Set SQL OBJECT 
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Dim deposit_totals As Double
                Dim grand_total As Double = 0
                deposit_totals = 0

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Do While MyDataReader2.Read()
                        If MyDataReader2.Item("TRN_TP_CD") = "SAL" Then
                            grand_total = MyDataReader2.Item("AMT").ToString
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "CRM" Then
                            grand_total = -MyDataReader2.Item("AMT").ToString
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "MCR" Then
                            grand_total = -MyDataReader2.Item("AMT").ToString
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "MDB" Then
                            grand_total = MyDataReader2.Item("AMT").ToString
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "DEP" Then
                            deposit_totals = deposit_totals + FormatCurrency(MyDataReader2.Item("AMT").ToString, 2)
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "PMT" Then
                            deposit_totals = deposit_totals + FormatCurrency(MyDataReader2.Item("AMT").ToString, 2)
                        ElseIf MyDataReader2.Item("TRN_TP_CD") = "REF" Then
                            deposit_totals = deposit_totals - FormatCurrency(MyDataReader2.Item("AMT").ToString, 2)
                        End If
                    Loop
                    'Close Connection 
                    MyDataReader2.Close()
                Catch ex As Exception
                    Throw
                End Try
                payment_data = "<table cellpadding=0 cellspacing=0 border=0>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>SubTotal:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(subtotal, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Delivery:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(del_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Setup:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(setup_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Taxes:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(tax_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Grand Total:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(grand_total, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Payments:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(deposit_totals, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Balance:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency((grand_total - deposit_totals), 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "</table>"
                HTML = HTML & comment_data & payment_data & "</td></tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey>&nbsp;</td>"
                HTML = HTML & "</tr>"
            Loop
            HTML = HTML & "</table>"
            MyDataReader.Close()
        Catch ex As Exception
            Throw
        End Try
        If InStr(HTML, "No records returned") > 0 Then HTML = Replace(HTML, "</table>", "")
        main_body.InnerHtml = HTML
        btn_create_new.Visible = True

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_create_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_create_new.Click

        Response.Redirect("SalesReport.aspx")

    End Sub

End Class
