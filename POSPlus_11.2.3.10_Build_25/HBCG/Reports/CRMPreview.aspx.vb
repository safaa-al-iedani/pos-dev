Imports System.Data.OracleClient

Partial Class Reports_CRMPreview
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ' populate the store drop down
            store_cd_populate()
            ' populate the company drop down
            company_cd_populate()
            txt_start_dt.SelectedDate = Today.Date.AddDays(-14)
            txt_end_dt.SelectedDate = Today.Date.AddDays(14)
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If

        SQL = "SELECT DISTINCT CUST_CD, CORP_NAME || ' ' || LNAME ||', ' || FNAME AS FULL_DESC FROM RELATIONSHIP "
        SQL = SQL & "ORDER BY CORP_NAME || ' ' || LNAME ||', ' || FNAME "

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_company
                .DataSource = ds
                .DataValueField = "CUST_CD"
                .DataTextField = "FULL_DESC"
                .DataBind()
            End With
            cbo_company.Items.Insert(0, "Please Select A Company")
            cbo_company.Items.FindByText("Please Select A Company").Value = ""
            If cbo_company.SelectedValue & "" = "" Then
                cbo_company.SelectedIndex = 0
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub company_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY FullNAME"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_slsp
                .DataSource = ds
                .DataValueField = "EMP_CD"
                .DataTextField = "FullNAME"
                .DataBind()
            End With
            cbo_slsp.Items.Insert(0, "Please Select A Salesperson")
            cbo_slsp.Items.FindByText("Please Select A Salesperson").Value = ""
            If cbo_slsp.SelectedValue & "" = "" Then
                cbo_slsp.SelectedIndex = 0
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click

        Response.Redirect("../newmain.aspx")

    End Sub

    Public Sub GridView1_Binddata()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet

        Dim FOLLOW_UP_DT As String = ""

        GridView1.DataSource = ""
        'CustTable.Visible = False
        GridView1.Visible = True

        sql = "SELECT * FROM RELATIONSHIP WHERE "

        sql = sql & "WR_DT BETWEEN TO_DATE('" & txt_start_dt.SelectedDate & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.SelectedDate & "','mm/dd/RRRR') "
        proceed = True

        If cbo_company.SelectedValue & "" <> "" Then
            If proceed = True Then sql = sql & "AND "
            sql = sql & "CUST_CD = '" & cbo_company.SelectedValue & "' "
            proceed = True
        End If
        If cbo_slsp.SelectedValue & "" <> "" Then
            If proceed = True Then sql = sql & "AND "
            sql = sql & "SLSP1 = '" & cbo_slsp.SelectedValue & "' "
            proceed = True
        End If
        If Right(sql, 6) = "WHERE " Then
            sql = Replace(sql, "WHERE", "")
        End If
        sql = UCase(sql)

        sql = Replace(sql, "*", "%")
        sql = Replace(sql, "SELECT %", "SELECT *")

        conn.Open()

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim MyTable As DataTable
        Dim numrows As Integer
        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                GridView1.DataSource = dv
                GridView1.DataBind()
            Else
                GridView1.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        lbl_pageinfo.Text = "Page " + CStr(GridView1.PageIndex + 1) + " of " + CStr(GridView1.PageCount)
        ' make all the buttons visible if page count is more than 0
        If GridView1.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If GridView1.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView1.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView1.PageIndex = (GridView1.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView1.PageCount = 0 Then
            Label1.Text = "Sorry, no relationship records found.  Please search again"
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView1.PageIndex < (GridView1.PageCount - 1) Then
                    GridView1.PageIndex += 1
                End If
            Case "Prev"
                If GridView1.PageIndex > 0 Then
                    GridView1.PageIndex -= 1
                End If
            Case "Last"
                GridView1.PageIndex = GridView1.PageCount - 1
            Case Else
                GridView1.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

        Dim row As GridViewRow = GridView1.SelectedRow

        ' Display the customer name from the selected row.
        Response.Redirect("../Relationship_Maintenance.aspx?del_doc_num=" & row.Cells(0).Text & "&query_returned=Y")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        GridView1_Binddata()

    End Sub
End Class
