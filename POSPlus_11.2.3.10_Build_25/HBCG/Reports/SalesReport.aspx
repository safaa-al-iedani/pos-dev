<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesReport.aspx.vb" Inherits="Reports_SalesReport"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="main_body" runat="server">
        <br />
        <table width="100%">
            <tr>
                <td style="text-align: right">
                    Store Code:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_store_cd" runat="server" Width="285px" CssClass="style5">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Transaction Type:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_ord_tp_cd" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="SAL">SALE</asp:ListItem>
                        <asp:ListItem Value="CRM">Returns</asp:ListItem>
                        <asp:ListItem Value="MCR">Credits</asp:ListItem>
                        <asp:ListItem Value="MDB">Debits</asp:ListItem>
                        <asp:ListItem Selected="True" Value="ALL">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Order Status:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_ord_stat" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="O">Open</asp:ListItem>
                        <asp:ListItem Value="F">Finalized</asp:ListItem>
                        <asp:ListItem Value="V">Voided</asp:ListItem>
                        <asp:ListItem Selected="True" Value="B">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Written/Finalized Date:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="txt_start_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                &nbsp;through&nbsp;
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="txt_end_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Pickup/Delivery:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_pd" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="D">Delivery</asp:ListItem>
                        <asp:ListItem Value="P">Pickup</asp:ListItem>
                        <asp:ListItem Selected="True" Value="B">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Pickup/Delivery Date:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="txt_del_start" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                &nbsp;through&nbsp;
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="txt_del_end" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="text-align: right">
                    Sort By:
                </td>
                <td style="text-align: left">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="SO_WR_DT">Written Date</asp:ListItem>
                        <asp:ListItem Value="DEL_DOC_NUM">Sales Order</asp:ListItem>
                        <asp:ListItem Value="SO_STORE_CD">Store Code</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Label ID="lbl_errors" runat="server" Text="" Width="285px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btn_submit" runat="server" Text="Run Report" Width="120px" CssClass="style5" />
                    &nbsp;
                    <input id="Reset1" style="width: 120px" type="reset" value="Reset Options" class="style5" />
                    &nbsp;
                    <asp:Button ID="btn_exit" runat="server" Text="Exit Report" Width="120px" CssClass="style5" />
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
        <dx:ASPxButton ID="btn_create_new" runat="server" Text="Create New Report" Visible="False">
        </dx:ASPxButton>
    </div>
</asp:Content>
