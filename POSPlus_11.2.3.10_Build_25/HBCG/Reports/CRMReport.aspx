<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CRMReport.aspx.vb" Inherits="Reports_SalesReport"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="main_body" runat="server">
        <br />
        <table width="100%">
            <tr>
                <td style="text-align: right">
                    Store Code:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_store_cd" runat="server" Width="285px" CssClass="style5">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Order Status:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_ord_stat" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="O">Open</asp:ListItem>
                        <asp:ListItem Value="C">Converted</asp:ListItem>
                        <asp:ListItem Value="F">Finalized</asp:ListItem>
                        <asp:ListItem Value="V">Voided</asp:ListItem>
                        <asp:ListItem Selected="True" Value="B">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Written/Finalized Date:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxCalendar ID="txt_start_dt" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                    Width="192px" ShowWeekNumbers="False">
                                    <FooterStyle Spacing="3px"></FooterStyle>
                                    <FastNavFooterStyle Spacing="3px">
                                    </FastNavFooterStyle>
                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                    </FastNavStyle>
                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings>
                                        <ErrorFrameStyle ImageSpacing="4px">
                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                        </ErrorFrameStyle>
                                    </ValidationSettings>
                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                </dx:ASPxCalendar>
                            </td>
                            <td>
                                &nbsp;through&nbsp;
                            </td>
                            <td>
                                <dx:ASPxCalendar ID="txt_end_dt" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                    Width="192px" ShowWeekNumbers="False">
                                    <FooterStyle Spacing="3px"></FooterStyle>
                                    <FastNavFooterStyle Spacing="3px">
                                    </FastNavFooterStyle>
                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                    </FastNavStyle>
                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings>
                                        <ErrorFrameStyle ImageSpacing="4px">
                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                        </ErrorFrameStyle>
                                    </ValidationSettings>
                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                </dx:ASPxCalendar>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Follow Up Date:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxCalendar ID="txt_del_start" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                    Width="192px" ShowWeekNumbers="False">
                                    <FooterStyle Spacing="3px"></FooterStyle>
                                    <FastNavFooterStyle Spacing="3px">
                                    </FastNavFooterStyle>
                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                    </FastNavStyle>
                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings>
                                        <ErrorFrameStyle ImageSpacing="4px">
                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                        </ErrorFrameStyle>
                                    </ValidationSettings>
                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                </dx:ASPxCalendar>
                            </td>
                            <td>
                                &nbsp;through&nbsp;
                            </td>
                            <td>
                                <dx:ASPxCalendar ID="txt_del_end" runat="server" CssFilePath="~/App_Themes/Youthful/{0}/styles.css"
                                    CssPostfix="Youthful" Height="149px" SpriteCssFilePath="~/App_Themes/Youthful/{0}/sprite.css"
                                    Width="192px" ShowWeekNumbers="False">
                                    <FooterStyle Spacing="3px"></FooterStyle>
                                    <FastNavFooterStyle Spacing="3px">
                                    </FastNavFooterStyle>
                                    <FastNavStyle ImageSpacing="14px" MonthYearSpacing="1px">
                                    </FastNavStyle>
                                    <LoadingPanelImage Url="~/App_Themes/Youthful/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings>
                                        <ErrorFrameStyle ImageSpacing="4px">
                                            <ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
                                        </ErrorFrameStyle>
                                    </ValidationSettings>
                                    <HeaderStyle Spacing="8px"></HeaderStyle>
                                    <SettingsLoadingPanel Text=""></SettingsLoadingPanel>
                                </dx:ASPxCalendar>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="text-align: right">
                    Sort By:
                </td>
                <td style="text-align: left">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="WR_DT">Written Date</asp:ListItem>
                        <asp:ListItem Value="REL_NO">Sales Order</asp:ListItem>
                        <asp:ListItem Value="STORE_CD">Store Code</asp:ListItem>
                        <asp:ListItem Value="SLSP1">Salesperson</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Label ID="lbl_errors" runat="server" Text="" Width="285px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btn_submit" runat="server" Text="Run Report">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="Reset1" runat="server" Text="Reset Options" Autopostback="False" UseSubmitBehavior="False">
                                    <ClientSideEvents Click="function(s, e) {
     main_body.reset();
            }" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_exit" runat="server" Text="Exit Report">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
        <dx:ASPxButton ID="btn_create_new" runat="server" Text="Create New Report" Visible="False">
        </dx:ASPxButton>
    </div>
</asp:Content>
