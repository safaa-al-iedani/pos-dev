Imports HBCG_Utils

Partial Class Reports_Audit_logs
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            ASPxLabel2.Text = "We're sorry, you don't have access to this page.  If you feel you've reached this message in error, please contact your system administrator."
            ASPxDateEdit1.Enabled = False
            ASPxDateEdit2.Enabled = False
            Exit Sub
        End If

    End Sub

End Class
