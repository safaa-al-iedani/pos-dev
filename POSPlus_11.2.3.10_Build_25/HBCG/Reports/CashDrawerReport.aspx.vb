Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Reports_CashDrawerReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ' populate the store drop down
            store_cd_populate()
            ' populate the company drop down
            company_cd_populate()
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store "
            SQL = SQL & "WHERE STORE_CD IN(select store_grp$store.store_cd "
            SQL = SQL & "from store_grp$store, emp2store_grp "
            SQL = SQL & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
            SQL = SQL & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            SQL = SQL & " order by store_cd"
        Else
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store order by store_cd"
        End If
        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub company_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT co_cd, des, co_cd || ' - ' || des as full_desc FROM CO order by co_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_company
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "des"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click

        Response.Redirect("../newmain.aspx")

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql, PMT_FOUND, REF_FOUND As String
        Dim HTML
        PMT_FOUND = "N"
        REF_FOUND = "N"

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Select IVC_CD, CUST_CD, MOP_CD, DECODE(TRN_TP_CD,'PMT',AMT,'DEP',AMT,'R',-AMT,AMT) AS AMT from AR_TRN WHERE POST_DT BETWEEN TO_DATE('" & txt_start_dt.Text
        sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
        sql = sql & "AND TRN_TP_CD = 'PMT' AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "'"
        sql = sql & " OR POST_DT BETWEEN TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') "
        sql = sql & "AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') AND TRN_TP_CD = 'DEP'"
        sql = sql & " AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "' OR POST_DT BETWEEN "
        sql = sql & "TO_DATE('" & txt_start_dt.Text & "','mm/dd/RRRR') AND "
        sql = sql & "TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') AND TRN_TP_CD = 'R' "
        sql = sql & "AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "' "
        sql = sql & "ORDER BY IVC_CD, MOP_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.HasRows Then
                HTML = "<div align=center>"
                HTML = HTML & "<table width=""450"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey><b>DETAILED PAYMENT RECONCILIATION</b></td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td><b>Sales Order</b></td>"
                HTML = HTML & "<td><b>Customer Code</b></td>"
                HTML = HTML & "<td><b>Payment Type</b></td>"
                HTML = HTML & "<td><b>Amount</b></td>"
                HTML = HTML & "<td><b>Balance?</b></td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5>&nbsp;</td>"
                HTML = HTML & "</tr>"
            Else
                HTML = "No records returned."
            End If
            Do While MyDataReader.Read
                If MyDataReader.Item("MOP_CD").ToString = "R" Then
                    REF_FOUND = "Y"
                Else
                    PMT_FOUND = "Y"
                End If
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>" & MyDataReader.Item("IVC_CD") & "</td>"
                HTML = HTML & "<td>" & MyDataReader.Item("CUST_CD") & "</td>"
                HTML = HTML & "<td>" & MyDataReader.Item("MOP_CD") & "</td>"
                HTML = HTML & "<td align=right>" & FormatNumber(MyDataReader.Item("AMT"), 2) & "</td>"
                HTML = HTML & "<td><input type=checkbox></td>"
                HTML = HTML & "</tr>"
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        If HTML <> "No records returned." Then
            HTML = HTML & "</table>"
        End If
        If PMT_FOUND = "Y" Then
            sql = "Select MOP_CD, SUM(DECODE(TRN_TP_CD,'PMT',AMT,'DEP',AMT,'R',-AMT,AMT)) AS PMT_AMT from AR_TRN WHERE POST_DT BETWEEN TO_DATE('" & txt_start_dt.Text
            sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
            sql = sql & "AND TRN_TP_CD IN('PMT','DEP','R') AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "'"
            sql = sql & "GROUP BY MOP_CD "
            sql = sql & "ORDER BY MOP_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.HasRows Then
                    HTML = HTML & "<div align=center><br><br>"
                    HTML = HTML & "<table width=""450"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 bgcolor=lightgrey><b>PAYMENT RECONCILIATION</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td width=10>&nbsp;</td>"
                    HTML = HTML & "<td align=left><b>Payment Type</b></td>"
                    HTML = HTML & "<td align=right><b>Amount</b></td>"
                    HTML = HTML & "<td><b>Balance?</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                End If
                Do While MyDataReader.Read
                    If MyDataReader.Item("MOP_CD").ToString = "R" Then
                        REF_FOUND = "Y"
                    Else
                        PMT_FOUND = "Y"
                    End If
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td width=10>&nbsp;</td>"
                    HTML = HTML & "<td align=left>" & MyDataReader.Item("MOP_CD") & "</td>"
                    HTML = HTML & "<td align=right>" & FormatNumber(MyDataReader.Item("PMT_AMT"), 2) & "</td>"
                    HTML = HTML & "<td><input type=checkbox></td>"
                    HTML = HTML & "</tr>"
                Loop
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        If REF_FOUND = "Y" Then
            sql = "Select SUM(AMT) As PMT_AMT, MOP_CD from AR_TRN WHERE POST_DT BETWEEN TO_DATE('" & txt_start_dt.Text
            sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
            sql = sql & "AND TRN_TP_CD = 'R' AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "'"
            sql = sql & "GROUP BY MOP_CD "
            sql = sql & "ORDER BY MOP_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.HasRows Then
                    HTML = HTML & "<div align=center><br><br>"
                    HTML = HTML & "<table width=""450"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 bgcolor=lightgrey><b>REFUND RECONCILIATION</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td width=10>&nbsp;</td>"
                    HTML = HTML & "<td><b>Payment Type</b></td>"
                    HTML = HTML & "<td><b>Amount</b></td>"
                    HTML = HTML & "<td><b>Balance?</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                End If
                Do While MyDataReader.Read
                    If MyDataReader.Item("MOP_CD").ToString = "R" Then
                        REF_FOUND = "Y"
                    Else
                        PMT_FOUND = "Y"
                    End If
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td width=10>&nbsp;</td>"
                    HTML = HTML & "<td>" & MyDataReader.Item("MOP_CD") & "</td>"
                    HTML = HTML & "<td align=right>-" & FormatNumber(MyDataReader.Item("PMT_AMT"), 2) & "</td>"
                    HTML = HTML & "<td><input type=checkbox></td>"
                    HTML = HTML & "</tr>"
                Loop
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        If HTML <> "No records returned." Then
            HTML = HTML & "</table>"
        End If
        objSql.Dispose()
        sql = "Select * from AR_TRN WHERE POST_DT BETWEEN TO_DATE('" & txt_start_dt.Text
        sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.Text & "','mm/dd/RRRR') "
        sql = sql & "AND TRN_TP_CD IN ('SAL','CRM','MCR','MDB') AND PMT_STORE = '" & cbo_store_cd.SelectedValue & "' AND CO_CD='" & cbo_company.SelectedValue & "'"
        sql = sql & "ORDER BY IVC_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            HTML = HTML & "<div align=center><br><br>"
            HTML = HTML & "<table width=""450"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
            HTML = HTML & "<tr>"
            HTML = HTML & "<td colspan=5 bgcolor=lightgrey><b>SALES RECONCILIATION</b></td>"
            HTML = HTML & "</tr>"
            HTML = HTML & "<tr>"
            HTML = HTML & "<td><b>Sales Order</b></td>"
            HTML = HTML & "<td><b>Customer Code</b></td>"
            HTML = HTML & "<td><b>Transaction Type</b></td>"
            HTML = HTML & "<td><b>Amount</b></td>"
            HTML = HTML & "<td><b>Balance?</b></td>"
            HTML = HTML & "</tr>"
            HTML = HTML & "<tr>"
            HTML = HTML & "<td colspan=5>&nbsp;</td>"
            HTML = HTML & "</tr>"
            If Not MyDataReader.HasRows Then
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>No records returned.</td>"
                HTML = HTML & "</tr>"
            End If
            Do While MyDataReader.Read
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>" & MyDataReader.Item("IVC_CD") & "</td>"
                HTML = HTML & "<td>" & MyDataReader.Item("CUST_CD") & "</td>"
                HTML = HTML & "<td>" & MyDataReader.Item("TRN_TP_CD") & "</td>"
                HTML = HTML & "<td align=right>"
                If MyDataReader.Item("TRN_TP_CD") = "CRM" Or MyDataReader.Item("TRN_TP_CD") = "MCR" Then
                    HTML = HTML & "-" & FormatNumber(MyDataReader.Item("AMT"), 2)
                Else
                    HTML = HTML & FormatNumber(MyDataReader.Item("AMT"), 2)
                End If
                HTML = HTML & "</td>"
                HTML = HTML & "<td><input type=checkbox></td>"
                HTML = HTML & "</tr>"
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If HTML <> "No records returned." Then
            HTML = HTML & "</table>"
        End If
        objSql.Dispose()
        conn.Close()

        HTML = HTML & "<br />"

        mainbody.InnerHtml = HTML
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
