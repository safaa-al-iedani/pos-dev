<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Best_Sellers.aspx.vb" Inherits="Reports_Best_Sellers"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frm_submit" runat="server">
        <table width="100%">
            <tr>
                <td>
                    &nbsp;&nbsp;<dx:ASPxLabel ID="Label1" runat="server" CssClass="style5" Text="Store Code:"
                        Width="68px">
                    </dx:ASPxLabel>
                    <asp:DropDownList ID="cbo_vendor" runat="server" CssClass="style5" Width="427px">
                    </asp:DropDownList><br />
                    <br />
                    &nbsp;
                    <dx:ASPxLabel ID="Label2" runat="server" CssClass="style5" Text="Time frame:" Width="86px">
                    </dx:ASPxLabel>
                    &nbsp;<asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5" Width="89px">
                        <asp:ListItem Value="30">30 days</asp:ListItem>
                        <asp:ListItem Value="60">60 days</asp:ListItem>
                        <asp:ListItem Value="90">90 days</asp:ListItem>
                        <asp:ListItem Value="120">120 days</asp:ListItem>
                        <asp:ListItem Value="180">180 days</asp:ListItem>
                    </asp:DropDownList>
                    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="60%">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    &nbsp;
                    <table width="96%">
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="Label3" runat="server" CssClass="style5" Text="Determine top 10 by: "
                                    Width="125px">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" CssClass="style5" Text="View: " Width="125px">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" CssClass="style5" Text="Category Code: "
                                    Width="125px">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxRadioButton ID="rdo_revenue" runat="server" Checked="True" GroupName="top_10"
                                    Text="Revenue">
                                </dx:ASPxRadioButton>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbo_inv_type" runat="server" IncrementalFilteringMode ="StartsWith">
                                    <Items>
                                        <dx:ListEditItem Text="All Inventory" Value="ALL" Selected="True" />
                                        <dx:ListEditItem Text="Inventory Only" Value="Y" />
                                        <dx:ListEditItem Text="Non-Inventory Only" Value="N" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbo_category" runat="server" Width="280px" IncrementalFilteringMode ="StartsWith">
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxRadioButton ID="rdo_merch" runat="server" Checked="False" GroupName="top_10"
                                    Text="Merchandise Count">
                                </dx:ASPxRadioButton>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="middle" align="center">
                    <dx:ASPxButton ID="btn_submit" runat="server" Text="Generate Report">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <hr />
        <dx:ASPxLabel ID="lbl_vendor_info" runat="server" Width="100%" EncodeHtml="False">
        </dx:ASPxLabel>
        <br />
        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="210px" Width="683px">
            <seriestemplate><ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>
            <fillstyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</fillstyle>
        </dxchartsui:WebChartControl>
        &nbsp;<br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
