Imports System.Data.OracleClient
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraCharts

Partial Class Reports_Best_Sellers
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            MinorChanges()
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()
            Dim objSql2 As OracleCommand
            Dim MyDataReader2 As OracleDataReader
            Dim ds2 As DataSet
            Dim oAdp2 As OracleDataAdapter
            Dim dv2 As DataView
            ds2 = New DataSet

            sql = "SELECT store_grp_cd as store_cd, des, store_grp_cd || ' - ' || des as full_desc FROM store_grp order by store_grp_cd"
            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp2 = DisposablesManager.BuildOracleDataAdapter(objSql2)
            oAdp2.Fill(ds2)

            dv2 = ds2.Tables(0).DefaultView
            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                'Store Values in String Variables 
                If MyDataReader2.Read() Then
                    cbo_vendor.DataSource = dv2
                    cbo_vendor.DataValueField = "STORE_CD"
                    cbo_vendor.DataTextField = "FULL_DESC"
                    cbo_vendor.DataBind()
                    cbo_vendor.Items.Insert(0, "Please Select A Store")
                    cbo_vendor.Items.FindByText("Please Select A Store").Value = ""
                    cbo_vendor.SelectedIndex = 0
                End If
                'Close Connection 
                MyDataReader2.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Public Function CreateBoundLabel(ByVal PropertyName As String, ByVal DataSource As DataSet, ByVal DataMember As String)
        ' Create a label.
        Dim Label As New XRLabel()

        ' Bind a label to the specified data field.
        Label.DataBindings.Add(PropertyName, DataSource, DataMember)

        Return Label
    End Function

    Public Function CreateGroupField(ByVal GroupFieldName As String) As GroupField
        ' Create a GroupField object.
        Dim groupField As New GroupField()

        ' Set its field name.
        groupField.FieldName = GroupFieldName

        Return groupField
    End Function

    Public Sub Publish_Report()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim start_timestamp As Date
        start_timestamp = Now.AddDays(-DropDownList1.SelectedValue)

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn2.Open()
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "SELECT * FROM ("
        sql = sql & "SELECT ITM_SLS_HST_DAY.ITM_CD, SUM(ITM_SLS_HST_DAY.QTY) AS ORIG_COUNT, "
        sql = sql & "SUM(ITM_SLS_HST_DAY.QTY*ABS(ITM_SLS_HST_DAY.RET)) AS SALE_COUNT, ITM.VSN, ITM.RET_PRC, ITM.ADV_PRC, ITM.PRC1 "
        sql = sql & "FROM ITM_SLS_HST_DAY, store_grp$store sgs, ITM "
        sql = sql & "WHERE DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "and sgs.store_cd = ITM_SLS_HST_DAY.store_cd "
        sql = sql & "and ITM.ITM_CD = ITM_SLS_HST_DAY.ITM_CD AND ITM_SLS_HST_DAY.sls_wr_cd = 'D' "   ' only extract delivered business
        If cbo_vendor.SelectedValue <> "" Then
            sql = sql & "AND sgs.store_grp_cd = '" & cbo_vendor.SelectedValue & "' "
        End If
        If cbo_category.Value & "" <> "" Then
            sql = sql & "AND ITM.CAT_CD='" & cbo_category.Value & "' "
        End If
        If cbo_inv_type.Value = "Y" Then
            sql = sql & "AND ITM.INVENTORY='Y' "
        ElseIf cbo_inv_type.Value = "N" Then
            sql = sql & "AND ITM.INVENTORY='N' "
        End If
        sql = sql & "GROUP BY ITM_SLS_HST_DAY.ITM_CD, ITM.VSN, ITM.RET_PRC, ITM.ADV_PRC, ITM.PRC1 "
        If rdo_merch.Checked = True Then
            sql = sql & "ORDER BY 2 DESC "
        Else
            sql = sql & "ORDER BY 3 DESC "
        End If
        sql = sql & ") WHERE ROWNUM < 11"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                lbl_vendor_info.Text = "<table width=""100%"">"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>&nbsp;</td><td><b>Description</b></td><td><b>Retail</b></td><td><b>Advertised</b></td><td><b>Manufacturer</b></td><td align=""center""><b>Item Count</b></td><td align=""right""><b>Revenue</b></td></tr>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                If IsNumeric(MyDataReader2.Item("RET_PRC").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("RET_PRC").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                If IsNumeric(MyDataReader2.Item("ADV_PRC").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("ADV_PRC").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                If IsNumeric(MyDataReader2.Item("PRC1").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("PRC1").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td align=""center"">" & MyDataReader2.Item("ORIG_COUNT").ToString & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td align=""right"">" & FormatCurrency(MyDataReader2.Item("SALE_COUNT").ToString, 2) & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "</tr>"
            Else
                lbl_vendor_info.Text = "<table width=""100%"">"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>No records found...</td></tr>"
            End If
            Do While MyDataReader2.Read()
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                If IsNumeric(MyDataReader2.Item("RET_PRC").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("RET_PRC").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                If IsNumeric(MyDataReader2.Item("ADV_PRC").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("ADV_PRC").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                If IsNumeric(MyDataReader2.Item("PRC1").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(MyDataReader2.Item("PRC1").ToString, 2) & "</td>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td align=""center"">" & MyDataReader2.Item("ORIG_COUNT").ToString & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<td align=""right"">" & FormatCurrency(MyDataReader2.Item("SALE_COUNT").ToString, 2) & "</td>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "</tr>"
            Loop
            lbl_vendor_info.Text = lbl_vendor_info.Text & "</table><br />"
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try
        conn2.Close()

        Dim ds As DataSet
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        sql = "SELECT * FROM ("
        sql = sql & "SELECT TO_CHAR(DT,'MM/dd') AS WR_DT, ITM_SLS_HST_DAY.ITM_CD, SUM(ITM_SLS_HST_DAY.QTY) AS ORIG_COUNT, "
        sql = sql & "SUM(ITM_SLS_HST_DAY.QTY*ABS(ITM_SLS_HST_DAY.RET)) AS SALE_COUNT, ITM_SLS_HST_DAY.STORE_CD "
        sql = sql & "FROM ITM_SLS_HST_DAY, store_grp$store sgs, ITM "
        sql = sql & "WHERE ITM_SLS_HST_DAY.DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "and sgs.store_cd = ITM_SLS_HST_DAY.store_cd "
        sql = sql & "and ITM.ITM_CD = ITM_SLS_HST_DAY.ITM_CD AND ITM_SLS_HST_DAY.sls_wr_cd = 'D' "   ' only extract delivered business
        If cbo_vendor.SelectedValue <> "" Then
            sql = sql & "AND sgs.store_grp_cd = '" & cbo_vendor.SelectedValue & "' "
        End If
        If cbo_category.Value & "" <> "" Then
            sql = sql & "AND ITM.CAT_CD='" & cbo_category.Value & "' "
        End If
        If cbo_inv_type.Value = "Y" Then
            sql = sql & "AND ITM.INVENTORY='Y' "
        ElseIf cbo_inv_type.Value = "N" Then
            sql = sql & "AND ITM.INVENTORY='N' "
        End If
        sql = sql & "GROUP BY ITM_SLS_HST_DAY.ITM_CD, TO_CHAR(DT,'MM/dd'), ITM_SLS_HST_DAY.STORE_CD "
        If rdo_merch.Checked = True Then
            sql = sql & "ORDER BY 3 DESC "
        Else
            sql = sql & "ORDER BY 4 DESC "
        End If
        sql = sql & ") WHERE ROWNUM < 11"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        Dim dt As DataTable = ds.Tables(0)

        Dim view As New DataView(dt)
        view.Sort = "WR_DT ASC"

        WebChartControl1.ClearSelection()

        ' Generate a data table and bind the series to it.
        WebChartControl1.DataSource = view

        ' Specify data members to bind the series.
        WebChartControl1.Titles.Clear()
        Dim headTitle As New ChartTitle
        headTitle.Font = New System.Drawing.Font("Tahoma", 16)

        WebChartControl1.SeriesDataMember = "ITM_CD"
        WebChartControl1.SeriesTemplate.ArgumentDataMember = "WR_DT"
        If rdo_merch.Checked = True Then
            WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))
            headTitle.Text = "Sales Count by SKU "
        Else
            WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("SALE_COUNT").ColumnName.ToString))
            headTitle.Text = "Revenue by SKU "
        End If
        WebChartControl1.Titles.Add(headTitle)
        WebChartControl1.SeriesTemplate.View = New SideBySideBarSeriesView

        WebChartControl1.DataBind()

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Publish_Report()

    End Sub

    Public Sub MinorChanges()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim sql As String
        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)

        sql = "SELECT cat_cd, des, cat_cd || ' - ' || des as full_desc FROM inv_mnr_cat order by cat_cd"

        cmdGetCodes.Dispose()
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        ds.Dispose()
        ds = New DataSet

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = sql
            End With

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_category
                .DataSource = ds
                .ValueField = "cat_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            cbo_category.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Category", ""))
            cbo_category.Items.FindByText("Please Select A Category").Value = ""

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

End Class

