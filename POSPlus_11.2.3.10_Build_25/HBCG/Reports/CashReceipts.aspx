<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CashReceipts.aspx.vb" Inherits="Reports_CashReceipts"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frm_submit" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Daily Cash Reciepts" CssClass="style5"></asp:Label><br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Post Date:" Width="67px" CssClass="style5"></asp:Label>
        <dx:ASPxDateEdit ID="txttrandt" runat="server">
        </dx:ASPxDateEdit>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Store Code:" Width="98px" CssClass="style5"></asp:Label><br />
        <asp:DropDownList ID="store_cd" runat="server" CssClass="style5" Width="280px" AutoPostBack="true">
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Cash Drawer:" Width="98px" CssClass="style5"></asp:Label><br />
        <asp:DropDownList ID="cbo_csh_drw" runat="server" CssClass="style5" Width="280px">
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Salesperson:" Width="98px" CssClass="style5"></asp:Label><br />
        <asp:DropDownList ID="cbo_slsp" runat="server" CssClass="style5" Width="280px" AutoPostBack="True">
        </asp:DropDownList>&nbsp;
        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" CssClass="style5"
            Enabled="False" Text="Select all stores for this salesperson" /><br />
        <br />
        <asp:Button ID="btn_submit" runat="server" Text="Generate Report" CssClass="style5" />
        <br />
        <br />
    </div>
    &nbsp;
</asp:Content>
