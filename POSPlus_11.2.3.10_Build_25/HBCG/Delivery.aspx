<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Delivery.aspx.vb" Inherits="Delivery"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="delivery" runat="server">
        <dx:ASPxLabel ID="lblDesc" runat="server" Text="<%$ Resources:LibResources, Label405 %>">
        </dx:ASPxLabel>
        <br />
        
        <br />
        <asp:Panel ID="pnlZipCode" runat="server" DefaultButton="btnSubmit">
            <table>
                <tr>
                    <td>
                        <dx:ASPxTextBox ID="txtZipCode" runat="server">
                        </dx:ASPxTextBox>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Height="192px" Width="98%" AllowPaging="True" PageSize="7"
            BorderStyle="None" BorderColor="White" BorderWidth="0px" PagerSettings-Visible="false"
            CaptionAlign="Left">
            <PagerSettings Visible="False"></PagerSettings>
            <Columns>
                <asp:BoundField DataField="ZONE_CD" SortExpression="Zone Code" HeaderText="<%$ Resources:LibResources, Label684 %>"></asp:BoundField>
                <asp:BoundField DataField="DES" SortExpression="Description" HeaderText="<%$ Resources:LibResources, Label158 %>"></asp:BoundField>
                <asp:BoundField DataField="DELIVERY_STORE_CD" SortExpression="Store" HeaderText="<%$ Resources:LibResources, Label572 %>"></asp:BoundField>
                <asp:BoundField DataField="DEFAULT_DEL_CHG" HeaderText="<%$ Resources:LibResources, Label151 %>"></asp:BoundField>
                <asp:BoundField DataField="DEFAULT_SETUP_CHG" HeaderText="<%$ Resources:LibResources, Label541 %>"></asp:BoundField>
                <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="<%$ Resources:LibResources, Label528 %>"></asp:CommandField>
            </Columns>
            <SelectedRowStyle BackColor="#FFFFC0"></SelectedRowStyle>
            <AlternatingRowStyle BackColor="Beige" BorderColor="Green"></AlternatingRowStyle>
            <HeaderStyle Font-Bold="True" ForeColor="Black" />
        </asp:GridView>
        <table id="nav_table" runat="server" width="100%">
            <tr>
                <td align="center">
                    <table width="65%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label210 %>"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label425 %>"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label337 %>"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label261 %>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="40%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="<%$ Resources:LibResources, Label527 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div id="Pickup" runat="server">
         <table width="100%">
            <tr>
              <td width="50%" valign ="top"  ><dx:ASPxCalendar ID="Calendar1" runat="server" AutoPostBack="True" HighlightWeekends="False"
            ShowWeekNumbers="False" ShowClearButton="False">
        </dx:ASPxCalendar></td>
              <td width="50%" valign ="top"  ><asp:Label id="lblZoneCd" runat ="server" Text =""  ></asp:Label></td>
            </tr>
         </table>
        
        <br />
        
    </div>
    <div id="divError" runat="server">
        <dx:ASPxLabel ID="ASPlblError" runat="server" Text=""  ForeColor ="Red">
                    </dx:ASPxLabel>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="lbl_curr_data" runat="server" Text="" Width="100%">
    </dx:ASPxLabel>
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
