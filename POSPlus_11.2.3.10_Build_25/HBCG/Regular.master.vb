Imports System.IO
Imports ErrorManager

''' <summary>
''' ==================================================
''' MASTER PAGE FOR SALES AND RELATIONSHIP/QUOTE ENTRY 
''' ==================================================
''' (NoWizard2 for rest of pages except file uploads)
''' </summary>

Partial Class Regular
    Inherits System.Web.UI.MasterPage

    Private salesBiz As New SalesBiz()

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub


    Private Sub PopulateTotals()

        Dim dblPayments As Double = If(Not IsNothing(Session("payments")) AndAlso IsNumeric(Session("payments")), CDbl(Session("payments")), 0)
        Dim dblSubtotal As Double = If(Not IsNothing(Session("sub_total")) AndAlso IsNumeric(Session("sub_total")), CDbl(Session("sub_total")), 0)
        Dim dblDeliv As Double = If(Not IsNothing(Session("DEL_CHG")) AndAlso IsNumeric(Session("DEL_CHG")), CDbl(Session("DEL_CHG")), 0)
        Dim dblSetup As Double = If(Not IsNothing(Session("SETUP_CHG")) AndAlso IsNumeric(Session("SETUP_CHG")), CDbl(Session("SETUP_CHG")), 0)
        Dim dblTax As Double = If(Not IsNothing(Session("TAX_CHG")) AndAlso
                                  IsNumeric(Session("TAX_CHG")) AndAlso
                                  String.IsNullOrEmpty(Session("tet_cd")), CDbl(Session("TAX_CHG")), 0)
        Dim dblTotal As Double = dblSubtotal + dblDeliv + dblSetup + dblTax


        lblSubtotal.Text = FormatCurrency(dblSubtotal, 2)
        lblDelivery.Text = FormatCurrency(dblDeliv, 2)
        lblSetup.Text = FormatCurrency(dblSetup, 2)
        lblTax.Text = FormatCurrency(dblTax, 2)
        lblTotal.Text = FormatCurrency(dblTotal, 2)
        lbl_Payments.Text = FormatCurrency(dblPayments, 2)
        lbl_balance.Text = FormatCurrency(dblTotal - dblPayments, 2)

        Session("grand_total") = lblTotal.Text

    End Sub


    Private Sub Set_Links(ByVal enableLinks As Boolean)

        next_btn.Enabled = enableLinks
        save_btn.Enabled = enableLinks
        HyperLink2.Enabled = enableLinks
        HyperLink3.Enabled = enableLinks
        HyperLink4.Enabled = enableLinks
        HyperLink5.Enabled = enableLinks
        HyperLink6.Enabled = enableLinks
        HyperLink7.Enabled = enableLinks
        hlOrder.Enabled = enableLinks

        next_btn.Visible = enableLinks
        save_btn.Visible = enableLinks
        HyperLink2.Visible = enableLinks
        HyperLink3.Visible = enableLinks
        HyperLink4.Visible = enableLinks
        HyperLink5.Visible = enableLinks
        HyperLink6.Visible = enableLinks
        HyperLink7.Visible = enableLinks
        hlOrder.Visible = enableLinks
    End Sub

    Private Sub DisableSaleLinks()
        Set_Links(False)
        Session("SAL_LNKS_DISABLED") = "Y"
    End Sub


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '  Response.Cache.SetCacheability(HttpCacheability.NoCache)
      
        Session("SAL_LNKS_DISABLED") = "N"

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "* TRAIN MODE *"
            lbl_header.ForeColor = Color.Red
        End If

        PopulateTotals()

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("logout.aspx")
        Else
            ''mm -swp,20,2016 - back button security concern
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetNoStore()
        End If

        Dim ProgressCount As Integer
        Dim strURL As String
        Dim arrayURL As Array
        Dim pagename As String
        Dim HEADER As String
        Dim LEAD As String
        LEAD = Request("LEAD")

        If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
            HEADER = Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            If Session("CUST_LNAME") & "" <> "" Then
                'Daniela french
                'HEADER = HEADER & " working with customer " & Left(Session("CUST_LNAME"), 1) & LCase(Right(Session("CUST_LNAME"), Len(Session("CUST_LNAME")) - 1))
                HEADER = HEADER & " " & Resources.LibResources.Label663 & " " & Left(Session("CUST_LNAME"), 1) & LCase(Right(Session("CUST_LNAME"), Len(Session("CUST_LNAME")) - 1))
            End If
            lbl_Header1.Text = HEADER
        End If

        strURL = Request.ServerVariables("SCRIPT_NAME")
        arrayURL = Split(strURL, "/", -1, 1)
        pagename = arrayURL(UBound(arrayURL))

        If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "startup.aspx") > 0 Then
            back_btn.Visible = False
        End If

        ASPxMenu1.RootItem.Items(0).NavigateUrl = "~/newmain.aspx?verify_save=TRUE"

        If LEAD = "TRUE" Then
            HyperLink7.Enabled = False
            HyperLink7.Visible = False
            img_payment.Visible = False

            HyperLink1.NavigateUrl = "startup.aspx?LEAD=TRUE"
            HyperLink2.NavigateUrl = "customer.aspx?LEAD=TRUE"
            hlOrder.NavigateUrl = "order_detail.aspx?LEAD=TRUE"
            HyperLink3.NavigateUrl = "Discount.aspx?LEAD=TRUE"
            HyperLink4.NavigateUrl = "marketing.aspx?LEAD=TRUE"
            HyperLink5.NavigateUrl = "comments.aspx?LEAD=TRUE"
            HyperLink6.NavigateUrl = "delivery.aspx?LEAD=TRUE"
            save_btn.NavigateUrl = "lead_stage.aspx?LEAD=TRUE"

            back_btn.ImageUrl = "images/icons/backblue.gif"
            next_btn.ImageUrl = "images/icons/nextblue.gif"
            save_btn.ImageUrl = "images/icons/submitblue.gif"
            save_btn.Width = "24"
            save_btn.Height = "24"

            'Display should change to handle data necessary for a prospect entry ONLY!
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Entry"
            save_btn.Visible = True

            ASPxMenu1.RootItem.Items(1).NavigateUrl = "help.aspx?pagename=" & pagename

            If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "startup.aspx?LEAD=TRUE") > 0 Then
                back_btn.Visible = False
            End If
            If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "delivery.aspx?LEAD=TRUE") > 0 Then
                next_btn.Visible = False
            End If
            Select Case LCase(pagename)
                Case "startup.aspx"
                    next_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                    If Session("wr_dt") & "" = "" Then
                        save_btn.Visible = False
                    End If
                Case "customer.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                Case "special_order.aspx"
                    next_link.Visible = False
                    back_link.Visible = False
                Case "prs.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                Case "cust_edit.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                Case "order.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                Case "order_detail.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                Case "discount.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        next_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    Else
                        next_link.NavigateUrl = "Marketing.aspx?LEAD=TRUE"
                    End If
                    back_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                Case "inventoryresults.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                Case "marketing.aspx"
                    next_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                Case "comments.aspx"
                    next_link.NavigateUrl = "Delivery.aspx?LEAD=TRUE"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        back_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    Else
                        back_link.NavigateUrl = "Marketing.aspx?LEAD=TRUE"
                    End If
                Case "delivery.aspx"
                    next_link.NavigateUrl = "Lead_Stage.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                Case "deliverydate.aspx"
                    next_link.NavigateUrl = "Order_Stage.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                Case "lead_stage.aspx"
                    back_link.NavigateUrl = "Delivery.aspx?LEAD=TRUE"
                    next_link.Enabled = False
                    back_link.Enabled = True
                    save_btn.Enabled = False
                    next_link.Visible = False
                    save_btn.Visible = False
            End Select
            hlOrder.NavigateUrl = "order_detail.aspx?LEAD=TRUE"
            ProgressCount = 0
            If (Not SessVar.discCd.isEmpty) Then
                img_disc.ImageUrl = "images/lead_icons/interest-icon.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_disc.ImageUrl = "images/lead_icons/interest-icon.gif"
            End If
            If Session("PD") = "D" Then
                If Session("zone_cd") & "" <> "" Then
                    If Session("zone_cd") & "" = "" Then
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                    Else
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                        ProgressCount = ProgressCount + 1
                    End If
                Else
                    img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                End If
            Else
                If Session("del_dt") & "" <> "" Then
                    If Session("del_dt") & "" = "" Then
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                    Else
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                        ProgressCount = ProgressCount + 1
                    End If
                Else
                    img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                End If
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                img_mrkt.ImageUrl = "images/icons/marketing-grey.gif"
                ProgressCount = ProgressCount + 1
            Else
                If Session("mark_cd") & "" <> "" Then
                    img_mrkt.ImageUrl = "images/icons/marketingCheck.gif"
                    ProgressCount = ProgressCount + 1
                Else
                    img_mrkt.ImageUrl = "images/icons/marketing.gif"
                End If
            End If
            If Session("cust_cd") & "" <> "" Then
                img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
            End If
            If Session("itemid") & "" <> "" Then
                img_order.ImageUrl = "images/lead_icons/shopping-cart.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_order.ImageUrl = "images/lead_icons/shopping-cart.gif"
            End If
            If Session("comments") & "" <> "" Then
                img_cmnts.ImageUrl = "images/lead_icons/sales-order-icon.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cmnts.ImageUrl = "images/lead_icons/sales-order-icon.gif"
            End If
            If Session("tran_dt") & "" <> "" And Session("store_cd") & "" <> "" And Session("slsp1") & "" <> "" And Session("pct_1") & "" <> "" And Session("PD") & "" <> "" And Session("ord_srt") & "" <> "" Then
                ProgressCount = ProgressCount + 4
                img_start.ImageUrl = "images/lead_icons/school-bag-icon.gif"
            Else
                img_start.ImageUrl = "images/lead_icons/school-bag-icon.gif"
            End If
        Else
            'Data validation and screen displays that are pertinent to a sale entry
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Entry"
            save_btn.Visible = True
            save_btn.ImageUrl = "images/icons/submit.gif"
            save_btn.Width = "24"
            save_btn.Height = "24"

            Select Case LCase(pagename)
                Case "startup.aspx"
                    next_link.NavigateUrl = "Customer.aspx"
                    ' do not allow navigation unless there is a valid written date 
                    Dim errTxt As String = salesBiz.ValidateStoreClosedDate(Session("store_cd"), Session("tran_dt"))
                    If errTxt.isNotEmpty Then
                        DisableSaleLinks()
                        next_link.NavigateUrl = "startup.aspx"
                    End If
                Case "customer.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                Case "prs.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                Case "special_order.aspx"
                    next_link.Visible = False
                    back_link.Visible = False
                Case "cust_edit.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                Case "order.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                Case "order_detail.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                Case "discount.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        next_link.NavigateUrl = "Comments.aspx"
                    Else
                        next_link.NavigateUrl = "Marketing.aspx"
                    End If
                    back_link.NavigateUrl = "Order_detail.aspx"
                Case "marketing.aspx"
                    next_link.NavigateUrl = "Comments.aspx"
                    back_link.NavigateUrl = "Discount.aspx"
                Case "payment.aspx"
                    next_link.NavigateUrl = "Order_Stage.aspx"
                    back_link.NavigateUrl = "Delivery.aspx"
                Case "inventoryresults.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                Case "comments.aspx"
                    next_link.NavigateUrl = "Delivery.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        back_link.NavigateUrl = "Discount.aspx"
                    Else
                        back_link.NavigateUrl = "Marketing.aspx"
                    End If
                Case "delivery.aspx"
                    next_link.NavigateUrl = "Payment.aspx"
                    back_link.NavigateUrl = "Comments.aspx"
                Case "deliverydate.aspx"
                    next_link.NavigateUrl = "Payment.aspx"
                    back_link.NavigateUrl = "Comments.aspx"
                Case "order_stage.aspx"
                    back_link.NavigateUrl = "Payment.aspx"
                    next_link.Enabled = False
                    save_btn.Enabled = False
                    next_link.Visible = False
                    save_btn.Visible = False
            End Select
            ProgressCount = 0
            If (Not SessVar.discCd.isEmpty) Then
                img_disc.ImageUrl = "images/icons/discountCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_disc.ImageUrl = "images/icons/discount.gif"
            End If
            If Session("del_dt") & "" <> "" Then
                If Not IsDate(Session("del_dt")) Then
                    img_del_dt.ImageUrl = "images/icons/delivery.gif"
                Else
                    img_del_dt.ImageUrl = "images/icons/deliveryCheck.gif"
                    ProgressCount = ProgressCount + 1
                End If
            Else
                img_del_dt.ImageUrl = "images/icons/delivery.gif"
            End If
            If Session("cust_cd") & "" <> "" Then
                img_cust.ImageUrl = "images/icons/customerCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cust.ImageUrl = "images/icons/customer.gif"
            End If
            If Session("itemid") & "" <> "" Then
                img_order.ImageUrl = "images/icons/orderCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_order.ImageUrl = "images/icons/order.gif"
            End If
            If Session("comments") & "" <> "" Then
                img_cmnts.ImageUrl = "images/icons/commentsCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cmnts.ImageUrl = "images/icons/comments.gif"
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                img_mrkt.ImageUrl = "images/icons/marketing-grey.gif"
                ProgressCount = ProgressCount + 1
            Else
                If Session("mark_cd") & "" <> "" Then
                    img_mrkt.ImageUrl = "images/icons/marketingCheck.gif"
                    ProgressCount = ProgressCount + 1
                Else
                    img_mrkt.ImageUrl = "images/icons/marketing.gif"
                End If
            End If
            If Session("payment") & "" <> "" Then
                img_payment.ImageUrl = "images/icons/paymentCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_payment.ImageUrl = "images/icons/payment.gif"
            End If
            If Session("tran_dt") & "" <> "" And Session("store_cd") & "" <> "" And Session("slsp1") & "" <> "" And Session("pct_1") & "" <> "" And Session("PD") & "" <> "" Then
                ProgressCount = ProgressCount + 2
                img_start.ImageUrl = "images/icons/creditCheck.gif"
            Else
                img_start.ImageUrl = "images/icons/credit.gif"
            End If

        End If

    End Sub

    Protected Sub ASPxRoundPanel1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxRoundPanel1.Init

        Dim LEAD As String = Request("LEAD")
        Dim strURL As String = Request.ServerVariables("SCRIPT_NAME")
        Dim arrayURL As Array = Split(strURL, "/", -1, 1)
        Dim pagename As String = arrayURL(UBound(arrayURL))
        Dim roundHdrLbl As DevExpress.Web.ASPxEditors.ASPxLabel = ASPxRoundPanel4.FindControl("lbl_Round_Header")
        'Daniela french
        If LEAD = "TRUE" Then
            Select Case LCase(pagename)
                Case "startup.aspx"
                    'roundHdrLbl.Text = "RELATIONSHIP : CUSTOMER : STARTUP"
                    roundHdrLbl.Text = Resources.LibResources.Label465
                Case "customer.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label463
                Case "prs.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label464
                Case "cust_edit.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label462
                Case "order.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : ORDER : FIND MERCHANDISE"
                Case "order_detail.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label731
                Case "discount.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label730
                Case "inventoryresults.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : ORDER : SEARCH RESULTS"
                Case "marketing.aspx"
                    roundHdrLbl.Text = "RELATIONSHIP : OTHER : MARKETING"
                Case "comments.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label732
                Case "delivery.aspx"
                    If Session("PD") = "P" Then
                        roundHdrLbl.Text = Resources.LibResources.Label733
                    Else
                        roundHdrLbl.Text = Resources.LibResources.Label466
                    End If
                Case "deliverydate.aspx"
                    If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Then
                        roundHdrLbl.Text = "RELATIONSHIP : OTHER : DELIVERY/PICKUP CHARGES"
                    Else
                        roundHdrLbl.Text = "RELATIONSHIP : OTHER : DELIVERY DATE"
                    End If
                Case "lead_stage.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label461
            End Select
        Else
            ' Daniea added french
            Select Case LCase(pagename)
                Case "startup.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label499
                Case "customer.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label497
                Case "prs.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label498
                Case "cust_edit.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label496
                Case "order.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label502
                Case "order_detail.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label503
                Case "discount.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label501
                Case "marketing.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label494 & " : " & "ORDER : MARKETING"
                Case "payment.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label507
                Case "inventoryresults.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label498
                Case "comments.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label504
                Case "delivery.aspx"
                    If Session("PD") = "P" Then
                        roundHdrLbl.Text = Resources.LibResources.Label506
                    Else
                        roundHdrLbl.Text = Resources.LibResources.Label505
                    End If
                Case "deliverydate.aspx"
                    If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Then
                        roundHdrLbl.Text = Resources.LibResources.Label494 & " : " & "OTHER : DELIVERY/PICKUP CHARGES"
                    Else
                        roundHdrLbl.Text = Resources.LibResources.Label494 & " : " & "OTHER : DELIVERY DATE"
                    End If
                Case "order_stage.aspx"
                    roundHdrLbl.Text = Resources.LibResources.Label500
            End Select
            Select Case Session("ord_tp_cd")
                Case "CRM"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "RETURN")
                    roundHdrLbl.ForeColor = Color.Red
                Case "MCR"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "MISCELLANEOUS CREDIT")
                    roundHdrLbl.ForeColor = Color.Red
                Case "MDB"
                    roundHdrLbl.Text = Replace(roundHdrLbl.Text, "SALE", "MISCELLANEOUS DEBIT")
                    roundHdrLbl.ForeColor = Color.Red
                Case Else
                    roundHdrLbl.ForeColor = Color.Black
            End Select
        End If

    End Sub

End Class

