<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PO.aspx.vb" Inherits="PO" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label418 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_po" runat="server" Style="position: relative"></asp:TextBox>&nbsp;
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:LibResources, Label571 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_status" runat="server" Width="80px" MaxLength="1"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label573 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_store_cd" runat="server" Style="position: relative" Width="79px"></asp:TextBox>&nbsp;
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label668 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_written" runat="server" Width="80px" MaxLength="12"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label642 %>" Width="120px">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_vendor" runat="server" Style="position: relative" MaxLength="15"
                    Width="79px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp; &nbsp;
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label202 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_finalized" runat="server" Style="left: 0px; position: relative"
                    Width="80px" MaxLength="12"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label596 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_terms_pct" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label262 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_last_chg" runat="server" Style="position: relative" MaxLength="20"
                    Width="80px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label596 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="TextBox1" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
            <td style="width: 109px">
                &nbsp;
            </td>
            <td style="width: 338px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label597 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_terms_days" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label421 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_srt_cd" runat="server" MaxLength="20" Width="79px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label145 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_days_net" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label51 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_buyer" runat="server" Style="position: relative" MaxLength="20"
                    Width="79px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_warning" runat="server" ForeColor="red" Text="">
    </dx:ASPxLabel>
    <br />
    <dx:ASPxGridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="98%">
        <Columns>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label266 %>" FieldName="LN#" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label363 %>" FieldName="QTY_ORD" VisibleIndex="2">
            </dx:GridViewDataTextColumn>           
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label450 %>" VisibleIndex="3">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_rcv" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label550 %>" FieldName="ITM_CD" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label653 %>" FieldName="VSN" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label159 %>" FieldName="DEST_STORE_CD" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label548 %>" FieldName="SHIP_DT" VisibleIndex="7">
                <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label37 %>" FieldName="ARRIVAL_DT" VisibleIndex="8">
                <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label113 %>" FieldName="UNIT_CST" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label555 %>" FieldName="DEL_DOC_NUM" VisibleIndex="10">
            </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Ack" VisibleIndex="11">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_ack" runat="server" Text="">
                    </dx:ASPxLabel>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label571 %>" FieldName="BOOKING_STATUS" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label279 %>">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
