<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Relationship_Copy.aspx.vb"
    Inherits="Relationship_Copy" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Relationship Copy</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="style5" width="100%">
                <tr>
                    <td style="text-align: center">
                        <img height="60" src="images/Rename.png" />
                    </td>
                    <td valign="top" align="left" style="width: 420px">
                        &nbsp;<span style="font-size: 10pt">Relationship Copy -</span>
                        <br />
                        <br />
                        This utility takes an existing relationship and copies select attributes to another
                        relationship.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        Relationship to copy:
                    </td>
                    <td style="width: 420px">
                        <asp:Label ID="lbl_rel_no" runat="server" Text="" Width="100%"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        New Customer Code:
                    </td>
                    <td style="width: 420px">
                        <asp:TextBox ID="txt_cust_cd" runat="server" AutoPostBack="True" CssClass="style5"
                            Width="108px"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="images/icons/find.gif"
                            Width="20px" />&nbsp;
                        <asp:Label ID="lbl_cust_cd" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        New Store Code:
                    </td>
                    <td style="width: 420px">
                        <asp:DropDownList ID="store_cd" runat="server" Width="280px" CssClass="style5">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 420px">
                        <dx:ASPxCheckBox ID="chk_crm_comments" runat="server" Text="Copy Relationship Comments">
                        </dx:ASPxCheckBox>
                        <dx:ASPxCheckBox ID="chk_line_comments" runat="server" Text="Copy Relationship Line Comments">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btn_copy" runat="server" Text="Copy Relationship">
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btn_open_new" runat="server" Text="Go To Relationship" Enabled="False">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lbl_new_rel_no" runat="server" Visible="False" Width="100%"></asp:Label><br />
                        <br />
                    </td>
                </tr>
                <tr bgcolor="Linen">
                    <td style="height: 140px">
                        Status:
                    </td>
                    <td style="width: 420px; height: 140px;">
                        <asp:TextBox ID="txt_status" runat="server" Height="124px" TextMode="MultiLine" Width="414px"
                            CssClass="style5"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
