Imports System.Data
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports SkuUtils
Imports SessionIntializer
Imports System.Collections.Generic
Imports System.Linq
Imports OrderUtils

Partial Class Startup
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private theTranBiz As TransportationBiz = New TransportationBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Daniela comment
        If isEmpty(Session("CO_CD")) Or isEmpty(Session("str_co_grp_cd")) Then

            Dim str_co_grp_cd As String
            Dim str_co_cd As String
            Dim str_sup_user_flag As String
            Dim str_flag As String

            str_flag = LeonsBiz.lucy_check_sup_user(UCase(Session("EMP_CD")), str_co_grp_cd, str_co_cd, str_sup_user_flag) 'lucy use the function even not check super user
            Session("CO_CD") = str_co_cd
            Session("str_co_grp_cd") = str_co_grp_cd
        End If
        Dim ShuDt As String = SessVar.shuDt
        Dim sot As New SavedOrderType
        If Not IsNothing(Session("SavedOrderType")) Then
            sot = Session("SavedOrderType")
        End If

        If Request("LEAD") <> "TRUE" Then
            txttrandt.MinDate = Today.Date.AddDays(-SysPms.numDaysPostDtTrans)
            txttrandt.MaxDate = Today.Date.AddDays(SysPms.numDaysPostDtTrans)
            ' cannot enter anything before SHU date so SHU date is minimum 
            If IsDate(ShuDt) Then
                If DateDiff("d", txttrandt.MinDate, ShuDt) >= 0 Then
                    txttrandt.MinDate = Date.Parse(ShuDt).AddDays(1)
                End If
            End If

            'If the System Parameter for disallowing post date transactions is set to "Y" then lock down the calendar
            If SysPms.skipTransDate Then txttrandt.ReadOnly = True
        End If

        lbl_warnings.Text = ""

        If Not IsPostBack = True Then

            'stopgap fix so that the PD store selected by the user is kept
            'otherwise it gets overriden in the multiple times in the code below
            Dim pdStore As String = If(Session("pd_store_cd") Is Nothing, "", Session("pd_store_cd"))
            '**** IMPORTANT: This has to be before the InitializeSession call below.  ******
            '**** The above cached value, replaces ALL changes made by MM-4601

            If Request("ISSOInitialization") = "yes" OrElse Not IsNothing(Session("isCommited")) Then
                IntializeSession()
            End If

            '''''This code is moved from start page to this as per JIRA-2964&3923
            If Request("LEAD") <> "TRUE" AndAlso
               ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" AndAlso
               Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "C" Then

                lbl_Error.Text = "This terminal has been closed for the day; you will not be able to commit sales."
            End If

            lbl_warnings.Text = ""

            'Daniela IT Request 2475
            If (Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") Then
                If isEmpty(Session("DEL_CHG")) Then
                    Session("DEL_CHG") = 0
                End If
                If isEmpty(Session("SETUP_CHG")) Then
                    Session("SETUP_CHG") = 0
                End If
                If isEmpty(Session("ORIG_DEL_CHG")) Then
                    Session("ORIG_DEL_CHG") = 0
                End If
                If isEmpty(Session("ORIG_SETUP_CHG")) Then
                    Session("ORIG_SETUP_CHG") = 0
                End If
            End If

            If (Request("adj_ivc_cd") & "" <> "" OrElse Request("crm_cause_cd") & "" <> "") Then
                'End
                Session("adj_ivc_cd") = Request("adj_ivc_cd")
                Session("crm_cause_cd") = Request("crm_cause_cd")
                cboSalesperson1.Enabled = False
                cboSalesperson2.Enabled = False
                txt_pct_1.Enabled = False
                txt_pct_2.Enabled = False
                ' if there is no orig doc, then clear the session variable
                If (Request("adj_ivc_cd") & "").isEmpty Then
                    Session("orig_del_doc_num") = String.Empty
                End If
                If Not isEmpty(Request("adj_ivc_cd")) Then 'June 3 no need to call if null
                    PopulateFromOrigDoc(UCase(Request("adj_ivc_cd")))
                End If
            End If

            If ConfigurationManager.AppSettings("lock_converted").ToString = "Y" AndAlso
               Session("Converted_REL_NO") & "" <> "" Then

                cboSalesperson1.Enabled = False
                cboSalesperson2.Enabled = False
                txt_pct_1.Enabled = False
                txt_pct_2.Enabled = False
            End If

            If Request("LEAD") = "TRUE" Then
                txttrandt.MinDate = Today.Date
                'Daniela french
                'lblTranDt.Text = "Follow-Up Date"
                lblTranDt.Text = "Follow-Up Date"
                lblTranDt.Text = Resources.LibResources.Label212
                Label1.Visible = False
                Label2.Visible = False
                cboSalesperson2.Visible = False
                txt_pct_1.Visible = False
                txt_pct_2.Visible = False
                lblComm1.Visible = False
                lblComm2.Visible = False
                cboOrdSrt.Visible = False
                cbo_csh_drw.Items.Clear()
                cbo_csh_drw.Visible = False
                cbo_ord_tp_cd.Visible = False
                cbo_call_type.Visible = True
                cbo_am_or_pm.Visible = True
                cbo_general.Visible = True
                txt_appt_time.Visible = True
                Label3.Visible = True
                lblFollowUpTime.Visible = True
                PopulateGenlIntCategories()
            End If
            'MM-12412
            'if the relationship is converted to order, then initilize the session
            If Not Session("One_Time_Conversion") & "" = String.Empty Then
                Dim activatedStore As CashierInfo = HBCG_Utils.GetActivatedStoreCode(Session("EMP_CD"))
                'activatedStore = If(activatedStore.Equals(String.Empty), Session("Home_Store_cd"), activatedStore)
                If activatedStore.ActivatedStoreCode.Equals(String.Empty) Then
                    activatedStore.ActivatedStoreCode = Session("Home_Store_cd")
                Else
                    Session("csh_dwr_cd") = activatedStore.ActivatedCashDrawerCode
                End If
                'Assign the store code to the session, it should be activated store code OR home store code.
                Session("store_cd") = activatedStore.ActivatedStoreCode
            End If

            PopulateStores()
            PopulateSalespersons()
            PopulateOrderSortCodes()

            If Request("LEAD") = "TRUE" Then
                If Session("appt_time") & "" <> "" Then
                    txt_appt_time.Text = Session("appt_time")
                End If
                If Session("call_type") & "" <> "" Then
                    cbo_call_type.Value = Session("call_type")
                End If
                If Session("am_or_pm") & "" <> "" Then
                    cbo_am_or_pm.Value = Session("am_or_pm")
                End If
                If Session("tran_dt") & "" <> "" Then
                    txttrandt.SelectedDate = Session("tran_dt")
                Else
                    txttrandt.SelectedDate = DateTime.Today.AddDays(30)
                    Session("tran_dt") = DateTime.Today.AddDays(30)
                End If
            Else
                If Session("tran_dt") & "" <> "" Then
                    txttrandt.SelectedDate = Session("tran_dt")
                Else
                    txttrandt.SelectedDate = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
                    Session("tran_dt") = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
                End If
                txt_pct_2.Text = If(Session("pct_2") & "" <> "", Session("pct_2"), "")
                CheckSalesEntrySecurities()
            End If

            If Session("ord_tp_cd") & "" <> "" Then
                cbo_ord_tp_cd.Value = Session("ord_tp_cd")
            Else
                Session("ord_tp_cd") = cbo_ord_tp_cd.Value
            End If

            If Session("PD") & "" = "" Then
                Session("PD") = ConfigurationManager.AppSettings("p_or_d").ToString
            End If

            If Session("store_cd") & "" <> "" Then
                cboStores.SelectedItem = cboStores.Items.FindByValue(Session("store_cd"))
            Else
                Session("store_cd") = cboStores.Value
            End If
            If cboStores.Value = "" Then cboStores.SelectedIndex = 0

            If Session("pct_1") & "" <> "" Then
                txt_pct_1.Text = Session("pct_1")
            Else
                Session("pct_1") = txt_pct_1.Text
            End If

            If Session("pct_2") & "" <> "" Then
                txt_pct_2.Text = Session("pct_2")
            Else
                Session("pct_2") = txt_pct_2.Text
            End If

            'Alice added start on Feb 27, 2019 for request 4477
            If Session("Orig_pct1_forExchangeSo") IsNot Nothing Then
                If Not IsDBNull(Session("Orig_pct1_forExchangeSo")) Then
                    txt_pct_1.Text = Session("Orig_pct1_forExchangeSo")
                    Session("pct_1") = txt_pct_1.Text
                End If
            End If

            If Session("Orig_pct2_forExchangeSo") IsNot Nothing Then
                If Not IsDBNull(Session("Orig_pct2_forExchangeSo")) Then
                    txt_pct_2.Text = Session("Orig_pct2_forExchangeSo")
                    Session("pct_2") = txt_pct_2.Text
                End If
            End If

            'Alice added end on Feb 27, 2019 for request 4477


            If Session("SlspInfo") IsNot Nothing Then Session.Remove("SlspInfo")

            If Request("LEAD") <> "TRUE" Then
                'load with current SO header emp info before any changes
                Dim slspInfo As New OrderUtils.SlspCommissionInfo
                slspInfo.slsp1 = If(Session("slsp1") IsNot Nothing, Session("slsp1"), "")
                slspInfo.slsp2 = If(Session("slsp2") IsNot Nothing, Session("slsp2"), "")
                slspInfo.pct1 = If(Session("pct_1") IsNot Nothing AndAlso IsNumeric(Session("pct_1")), Session("pct_1"), 0)
                slspInfo.pct2 = If(Session("pct_2") IsNot Nothing AndAlso IsNumeric(Session("pct_2")), Session("pct_2"), 0)
                Session("SlspInfo") = slspInfo
                ' TODO - need to replace all the individual session variables to one object w/o header stuff as needed for new create process
                '   there may be an alternate approach also but if so, then can replace this session object; still need to gather these all
                '   into one place
                'Dim soHdrInf As OrderUtils.soHdrSessionInfo = OrderUtils.getInitSoHdrInfo
                'soHdrInf.soWrDt = Session("tran_dt")
                'soHdrInf.soStoreCd = Session("store_cd")
                'soHdrInf.ordTpCd = Session("ord_tp_cd")
                'soHdrInf.puDel = Session("PD")
                'soHdrInf.origDelDocNum = Session("orig_del_doc_num") + ""
                'soHdrInf.slspInfo = slspInfo
                'Session("soHdrSessInfo") = soHdrInf
            End If

            If Session("csh_dwr_cd") & "" <> "" Then
                cbo_csh_drw.SelectedItem = cbo_csh_drw.Items.FindByValue(Session("csh_dwr_cd"))
            Else
                Session("csh_dwr_cd") = cbo_csh_drw.Value
            End If
            If cbo_csh_drw.Value = "" Then cbo_csh_drw.SelectedIndex = 0

            If (ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" AndAlso
                Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O") Then

                Dim sql As String
                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()

                sql = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
                sql = UCase(sql)
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                    If MyDataReader.Read Then
                        cboStores.Value = MyDataReader.Item("STORE_CD").ToString
                        cbo_csh_drw.Items.Clear()
                        cbo_csh_drw.Value = MyDataReader.Item("CASH_DWR_CD").ToString
                        cboStores.Enabled = False
                        cbo_csh_drw.Enabled = False
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                Session("store_cd") = cboStores.Text
                Session("csh_dwr_cd") = cbo_csh_drw.Value
                conn.Close()
            End If

            'this only is true when coming from Relationship Maint when user clicks "convert"
            If Session("One_Time_Conversion") = "TRUE" Then
                Session("One_Time_Conversion") = ""
                determine_tax()
            End If

            If Request("LEAD") <> "TRUE" Then
                If SysPms.isCashierActiv Then       ' if cashier activation enabled
                    ActivateCashier(Session("emp_cd"))
                    If Session("csh_act_init") = "INVALIDCASHIER" Then
                        lbl_warnings.Text = "No records found for this cashier"
                        lbl_warnings.Visible = True
                    Else
                        cbo_csh_drw.SelectedItem = cbo_csh_drw.Items.FindByValue(Session("csh_dwr_cd"))
                        cboStores.SelectedItem = cboStores.Items.FindByValue(Session("store_cd"))
                        txttrandt.SelectedDate = Session("tran_dt")
                        cbo_csh_drw.Enabled = False
                        cboStores.Enabled = False
                        txttrandt.Enabled = False
                        UpdatePickupDelStoreInfo()
                    End If
                    If IsDate(Session("tran_dt")) Then
                        ' Appears if one value is date, then this comparison works fine, not if both are strings
                        If FormatDateTime(Session("tran_dt")) < Today.Date.AddDays(-SysPms.numDaysPostDtTrans) Then
                            Session("csh_act_init") = "INVALIDCASHIER"
                            lbl_warnings.Text = "Your post date is before than the earliest allowed transaction date"
                            lbl_warnings.Visible = True
                        ElseIf FormatDateTime(Session("tran_dt")) > Today.Date.AddDays(SysPms.numDaysPostDtTrans) Then
                            Session("csh_act_init") = "INVALIDCASHIER"
                            lbl_warnings.Text = "Your post date is later than the latest allowed transaction date"
                            lbl_warnings.Visible = True
                        ElseIf IsDate(ShuDt) AndAlso DateDiff("d", FormatDateTime(Session("tran_dt"), DateFormat.ShortDate), ShuDt) >= 0 Then
                            Session("csh_act_init") = "INVALIDCASHIER"
                            lbl_warnings.Text = "Your post date must be later than the SHU date --  Please activate a cash drawer for a new date."
                            lbl_warnings.Visible = True
                        Else
                            lbl_warnings.Text = theSalesBiz.ValidateStoreClosedDate(Session("store_cd"), Session("tran_dt"))
                            If lbl_warnings.Text.isNotEmpty Then
                                lbl_warnings.Text = lbl_warnings.Text + " --  Please activate a cash drawer for a new date."
                                lbl_warnings.Visible = True
                            End If
                        End If
                    End If
                Else
                    lbl_warnings.Text = theSalesBiz.ValidateStoreClosedDate(Session("store_cd"), Session("tran_dt"))
                    If lbl_warnings.Text.isNotEmpty Then
                        lbl_warnings.Text = lbl_warnings.Text
                        lbl_warnings.Visible = True
                    End If
                End If
            ElseIf Session("csh_act_init") & "" <> "" Then
                cbo_csh_drw.Enabled = False
                cboStores.Enabled = False
                '  txttrandt.Enabled = False  Alice update on Oct 21, 2019 for request 445
            End If

            If CheckCalculateTotal() Then
                'In the loading time this will not be called.
                calculate_total(Page.Master)
            End If
            'Restoring original value here, PD-STORE gets altered multiple times above
            'This change to use the cached value, replaces ALL changes made by MM-4601
            Session("pd_store_cd") = If(pdStore.isEmpty, cbo_pd_store_cd.Value, pdStore)
            cbo_pd_store_cd.Value = Session("pd_store_cd")
            SetPickupDeliveryZipNZone()
            CheckforExpDiscount()
        End If
        If String.IsNullOrEmpty(Request("restore_saved_order")) AndAlso (IsNothing(Session("Proceed")) OrElse String.IsNullOrEmpty(Session("Proceed"))) AndAlso Session("Proceed") <> True Then
            sot.OrderTypeCode = ""
        End If

        If Not String.IsNullOrEmpty(sot.OrderTypeCode) AndAlso AppConstants.Order.TYPE_SAL <> sot.OrderTypeCode AndAlso cbo_ord_tp_cd.Value <> AppConstants.Order.TYPE_SAL Then
            ASPxPopupControl1.ContentUrl = "CreditMemoPopulate.aspx"
            ASPxPopupControl1.ShowOnPageLoad = True
        End If
    End Sub

    ''' <summary>
    ''' Checks if the employee logged in has the necessary securities to allow for the creation of different
    ''' types of transactions. These securities include the SAL_CREATE, CRM_CREATE, MCR_CREATE and MDB_CREATE
    ''' and will only be added to the 'order type' drop-down if the employee has securities for them.
    ''' </summary>
    '''
    Private Sub CheckSalesEntrySecurities()

        'If user is setup to override all securities, then simply return and exit
        If SystemUtils.HasSecurityOverride(Session("emp_cd")) Then
            Exit Sub
        End If

        ' Check the security group, & emp privs for the user & only add those options that user has securities for.
        If Find_Security("SAL_CREATE", Session("EMP_CD")) = "N" Then
            cbo_ord_tp_cd.Items.Remove(cbo_ord_tp_cd.Items.FindByValue(AppConstants.Order.TYPE_SAL))
        End If
        If Find_Security("CRM_CREATE", Session("EMP_CD")) = "N" Then
            cbo_ord_tp_cd.Items.Remove(cbo_ord_tp_cd.Items.FindByValue(AppConstants.Order.TYPE_CRM))
        End If
        If Find_Security("MCR_CREATE", Session("EMP_CD")) = "N" Then
            cbo_ord_tp_cd.Items.Remove(cbo_ord_tp_cd.Items.FindByValue(AppConstants.Order.TYPE_MCR))
        End If
        If Find_Security("MDB_CREATE", Session("EMP_CD")) = "N" Then
            cbo_ord_tp_cd.Items.Remove(cbo_ord_tp_cd.Items.FindByValue(AppConstants.Order.TYPE_MDB))
        End If

    End Sub


    Private Sub determine_tax(Optional ByVal zip_cd As String = "")

        Dim taxability As String = ""
        Dim SQL As String = ""
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)
        conn.Open()

        If Session("PD") = "P" Then
            If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                zip_cd = store.addr.postalCd
            End If
        Else
            zip_cd = theCustomerBiz.GetCustomerZip(Session.SessionID.ToString, Session("CUST_CD"))
        End If
        zip_cd = UCase(zip_cd)

        '** check if the zip is setup to use written store for it's taxes
        Dim Use_Written As String = "N"
        If zip_cd.isNotEmpty Then
            SQL = "SELECT * FROM ZIP WHERE ZIP_CD='" & zip_cd & "'"
            cmd.CommandText = SQL

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)
                If MyDataReader.Read() Then
                    If Session("PD") = "P" Then
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                    Else
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                    End If
                End If
                MyDataReader.Close()

                If Use_Written = "Y" Then
                    '** since the written store needs to be used for taxation, get it's zip for retrieving tax codes
                    SQL = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
                    cmd.CommandText = SQL

                    MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)
                    If MyDataReader.Read() Then
                        zip_cd = MyDataReader.Item("ZIP_CD")
                    End If
                    MyDataReader.Close()
                End If
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            zip_cd = UCase(zip_cd)
            Dim MyTable As New DataTable
            ' ** retrieve tax codes by zip
            Dim ds As DataSet = theSalesBiz.GetTaxCodesByZip(zip_cd)
            MyTable = ds.Tables(0)

            If (MyTable.Rows.Count = 1) Then

                Dim taxCd As String = MyTable.Rows(0).Item("TAX_CD").ToString
                Dim taxRate As String = MyTable.Rows(0).Item("SumofPCT").ToString
                Session("TAX_RATE") = taxRate
                Session("TAX_CD") = taxCd
                '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                Session("MANUAL_TAX_CD") = Nothing

                'Add tax rates to the inventory, if items exist                
                Try
                    'set tax for take-with line to zero
                    SQL = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_AMT=0, TAX_PCT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()

                    '** select and update tax for other non-TW lines
                    SQL = "select row_id, itm_tp_cd, package_parent FROM temp_itm WHERE session_id='" & Session.SessionID.ToString & "'" & " AND take_with IS NULL"
                    cmd.CommandText = SQL
                    MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)
                    Do While MyDataReader.Read
                        taxability = ""
                        If (Session("TAX_CD") & "" <> "") Then
                            taxability = TaxUtils.Determine_Taxability(MyDataReader.Item("ITM_TP_CD").ToString, Date.Today, Session("TAX_CD"))
                        End If
                        If taxability = "Y" Then
                            If MyDataReader.Item("PACKAGE_PARENT").ToString & "" = "" Then
                                'update the tax amts for a regular sku - not a pkg
                                SQL = "UPDATE TEMP_ITM SET TAXABLE_AMT=RET_PRC, TAX_AMT=ROUND((RET_PRC*(NVL(" & taxRate & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_CD='" & taxCd & "', TAX_PCT=" & taxRate & " WHERE ROW_ID='" & MyDataReader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                            Else
                                'update taxes for the package sku
                                SQL = "UPDATE TEMP_ITM SET TAX_CD='" & Session("TAX_CD").ToString & "', TAX_AMT=ROUND((TAXABLE_AMT*(NVL(" & taxRate & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_PCT=" & taxRate & " WHERE ROW_ID='" & MyDataReader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                            End If
                            cmd.CommandText = SQL
                            cmd.ExecuteNonQuery()
                        End If
                    Loop
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Else
                Dim _hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                _hidZip.Value = zip_cd

                ucTaxUpdate.LoadTaxData()
                PopupTax.ShowOnPageLoad = True
            End If
            conn.Close()

            '** get and set  the delivery tax 
            Session("DEL_TAX") = theSalesBiz.GetTaxPctForDelv(Session("TAX_CD"))

            '** get and set  the setup tax 
            Session("SU_TAX") = theSalesBiz.GetTaxPctForSetup(Session("TAX_CD"))

        End If
    End Sub

    Private Sub PopulateFromOrigDoc(ByVal orig_del_Doc_num As String)
        Dim listCreditInfo As New List(Of CreditInfoDtc)
        Dim fi_amt As Double = 0
        Dim fin_co As String = ""
        Dim fin As String = ""
        Dim origSoWrDt As String = ""
        Dim lnQty As Double = 0.0
        Dim lnRet As Double = 0.0
        Dim firstRowLn As Integer = 0
        Dim rowLn As Integer = 0
        Dim voidedLines As Integer = 0
        ' the process is already using a session variable ("adj_ivc_cd") for the original del doc but that is the opposite of what it is and there is no time to track down where set, used and reset to 
        '    fix the confusion so I'm creating new one;  Hopefully at some time in the future, we can fix this
        Session("orig_del_doc_num") = orig_del_Doc_num

        fin = Request("fin")

        Dim sql As String
        Dim sqlSb As New StringBuilder
        sqlSb.Append(" SELECT SO.CUST_CD, SO.FIN_CUST_CD, SO_LN.ITM_CD, SO_LN.QTY, NVL(SO_LN.CUST_TAX_CHG,0) AS CUST_TAX_CHG, NVL(SO_LN.TAX_BASIS,0) AS TAX_BASIS, SO.SO_EMP_SLSP_CD1, SO.PCT_OF_SALE1, SO.SO_EMP_SLSP_CD2, SO.PCT_OF_SALE2, ")
        sqlSb.Append(" ITM.VE_CD, ITM.MNR_CD, ITM.CAT_CD, SO_LN.UNIT_PRC, ITM.VSN, ITM.DES, ITM.ITM_TP_CD, so.so_wr_dt, del_doc_ln#, inventory, ")
        sqlSb.Append(" ITM.TREATABLE, NVL(SO.DEL_CHG,0) AS DEL_CHG, NVL(ORIG_FI_AMT,0) AS ORIG_FI_AMT, SO_LN.COMM_CD, ITM.POINT_SIZE, SO.STAT_CD, NVL(SO.TAX_CHG,0) AS TAX_CHG, SO.TAX_CD, SO.PU_DEL, SO_LN.OUT_ID_CD, SO_LN.SER_NUM, ")
        sqlSb.Append(" so_ln.so_emp_slsp_cd1 as ln_slsp1, so_ln.so_emp_slsp_cd2 as ln_slsp2, so_ln.pct_of_sale1 as ln_pct1, so_ln.pct_of_sale2 as ln_pct2, so.setup_chg,SO_LN.VOID_FLAG,ITM.WARRANTABLE,SO_LN.SPIFF ")
        sqlSb.Append(" FROM SO_LN, ITM, SO ")
        sqlSb.Append(" WHERE SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM AND SO_LN.ITM_CD=ITM.ITM_CD ")
        'sqlSb.Append("AND SO.DEL_DOC_NUM= :docNum AND SO_LN.VOID_FLAG='N' AND SO.ORD_TP_CD='SAL' Order By SO_LN.DEL_DOC_LN#")
        sqlSb.Append(" AND SO.DEL_DOC_NUM= :docNum AND SO.ORD_TP_CD='SAL' Order By SO_LN.DEL_DOC_LN#")


        Dim connERP As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdERP As OracleCommand = DisposablesManager.BuildOracleCommand(sqlSb.ToString, connERP)
        Dim MyDataReader2 As OracleDataReader
        connERP.Open()

        Dim cmdLOC As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim connLOC As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connLOC.Open()

        'Set SQL OBJECT 
        Dim oAdp As OracleDataAdapter
        'Dim dt As New DataTable
        Dim dt As DataTable
        cmdERP.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
        cmdERP.Parameters(":docNum").Value = UCase(orig_del_Doc_num)
        If (AppConstants.Order.TYPE_MCR.Equals(Session("ord_tp_cd"))) Then
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmdERP)
            dt = New DataTable()
            oAdp.Fill(dt)
            oAdp.Dispose()
            'Else
            '    dt.Dispose()
        End If

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmdERP)

            If Not MyDataReader2.HasRows Then
                lbl_warnings.Text = "The sale you entered was not found."
                connERP.Close()
                connLOC.Close()
                Exit Sub
            End If

            If (AppConstants.Order.TYPE_MCR.Equals(Session("ord_tp_cd"))) Then
                If (Not IsNothing(dt)) Then
                    Dim itemDetails = From row In dt
                                      Group row By ItemCode = row.Field(Of String)("ITM_CD") Into SaleData = Group
                                      Select New With {
                                        Key ItemCode,
                                        .UnitPrice = SaleData.Sum(Function(r) r.Field(Of Decimal)("UNIT_PRC"))
                                          }

                    Dim itemTotal As New Dictionary(Of String, Decimal)

                    For Each row In itemDetails
                        itemTotal.Add(row.ItemCode, row.UnitPrice)
                    Next
                End If
            End If

            Dim taxability As String = ""
            Dim isSlsp2OnLine As Boolean
            Dim maxRetDatSet As DataSet
            If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), UCase(orig_del_Doc_num)) Then
                If (AppConstants.Order.TYPE_MCR <> Session("ord_tp_cd")) Then
                    maxRetDatSet = OrderUtils.getMaxCreditListByLn(UCase(orig_del_Doc_num))
                End If
            End If

            Do While MyDataReader2.Read()
                If "Y".Equals(MyDataReader2.Item("VOID_FLAG") & "") Then
                    voidedLines = voidedLines + 1
                    Continue Do
                End If
                If MyDataReader2.Item("STAT_CD") <> AppConstants.Order.STATUS_FINAL And Session("ord_tp_cd") = AppConstants.Order.TYPE_CRM Then
                    lbl_warnings.Text = "You must select an order that has been finalized to write a credit memo against"
                    lbl_warnings.Visible = True
                    cbo_ord_tp_cd.Value = AppConstants.Order.TYPE_SAL
                    Session("adj_ivc_cd") = ""
                    Session("crm_cause_cd") = ""
                    Session("origSoWrDt") = ""
                    Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL
                    connLOC.Close()
                    connERP.Close()
                    Exit Sub
                ElseIf MyDataReader2.Item("STAT_CD") <> AppConstants.Order.STATUS_FINAL And Session("ord_tp_cd") = AppConstants.Order.TYPE_MCR Then
                    lbl_warnings.Text = "You must select an order that has been finalized to write a miscellaneous credit against"
                    lbl_warnings.Visible = True
                    cbo_ord_tp_cd.Value = AppConstants.Order.TYPE_SAL
                    Session("adj_ivc_cd") = ""
                    Session("crm_cause_cd") = ""
                    Session("origSoWrDt") = ""
                    Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL
                    connLOC.Close()
                    connERP.Close()
                    Exit Sub
                End If

                If (AppConstants.Order.TYPE_MCR.Equals(Session("ord_tp_cd"))) Then
                    If (listCreditInfo.Count > 0) Then
                        Dim filteredRows = (From itemInfo In listCreditInfo
                                            Where itemInfo.ItemCode.Equals(MyDataReader2.Item("ITM_CD").ToString)
                                            Select itemInfo).ToList()

                        Dim itemDetails = filteredRows.SingleOrDefault()

                        If (filteredRows.Count <= 0) Then
                            maxRetDatSet = OrderUtils.GetMaxCreditforMCRItem(MyDataReader2.Item("ITM_CD").ToString, MyDataReader2.Item("CUST_CD").ToString, UCase(orig_del_Doc_num))
                            listCreditInfo.Add(New CreditInfoDtc With {.ItemCode = MyDataReader2.Item("ITM_CD").ToString, .SalUnitPrc = maxRetDatSet.Tables(0).Rows(0)("SALUNITPRC"), .SalTot = maxRetDatSet.Tables(0).Rows(0)("SALTOT"), .AvalCred = maxRetDatSet.Tables(0).Rows(0)("AVAL_CREDIT"), .SalQty = maxRetDatSet.Tables(0).Rows(0)("SALQTY"), .CrmQty = maxRetDatSet.Tables(0).Rows(0)("CRMQTY")})

                            Dim linePercentage As Decimal
                            If maxRetDatSet.Tables(0).Rows(0)("SALTOT") <> 0 Then
                                linePercentage = Convert.ToDecimal((CDbl(MyDataReader2.Item("unit_prc").ToString)) / maxRetDatSet.Tables(0).Rows(0)("SALTOT")) * 100
                            End If
                            lnRet = Convert.ToDecimal(maxRetDatSet.Tables(0).Rows(0)("AVAL_CREDIT") * linePercentage) / 100
                            Session("MCRData") = listCreditInfo
                        Else
                            Dim linePercentage As Decimal
                            If itemDetails.SalTot <> 0 Then
                                linePercentage = Convert.ToDecimal((CDbl(MyDataReader2.Item("unit_prc").ToString)) / itemDetails.SalTot * 100)
                            End If
                            lnRet = Convert.ToDecimal(itemDetails.AvalCred * linePercentage) / 100
                        End If
                    Else
                        maxRetDatSet = OrderUtils.GetMaxCreditforMCRItem(MyDataReader2.Item("ITM_CD").ToString, MyDataReader2.Item("CUST_CD").ToString, UCase(orig_del_Doc_num))
                        listCreditInfo.Add(New CreditInfoDtc With {.ItemCode = MyDataReader2.Item("ITM_CD").ToString, .SalUnitPrc = maxRetDatSet.Tables(0).Rows(0)("SALUNITPRC"), .SalTot = maxRetDatSet.Tables(0).Rows(0)("SALTOT"), .AvalCred = maxRetDatSet.Tables(0).Rows(0)("AVAL_CREDIT"), .SalQty = maxRetDatSet.Tables(0).Rows(0)("SALQTY"), .CrmQty = maxRetDatSet.Tables(0).Rows(0)("CRMQTY")})

                        Dim linePercentage As Decimal
                        If maxRetDatSet.Tables(0).Rows(0)("SALTOT") <> 0 Then
                            linePercentage = Convert.ToDecimal((CDbl(MyDataReader2.Item("unit_prc").ToString)) / maxRetDatSet.Tables(0).Rows(0)("SALTOT")) * 100
                        End If
                        lnRet = Convert.ToDecimal(maxRetDatSet.Tables(0).Rows(0)("AVAL_CREDIT") * linePercentage) / 100
                        Session("MCRData") = listCreditInfo
                    End If
                End If

                If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), UCase(orig_del_Doc_num)) Then

                    Dim maxCred As New OrderUtils.MaxCredResponse

                    If (AppConstants.Order.TYPE_MCR.Equals(Session("ord_tp_cd"))) Then
                        lnQty = CDbl(MyDataReader2.Item("qty").ToString)
                    Else
                        maxCred = OrderUtils.getLnMaxCredit(MyDataReader2("itm_cd"), MyDataReader2("del_doc_ln#"), MyDataReader2("unit_prc"), Session("ord_tp_cd"), maxRetDatSet)
                        lnQty = maxCred.maxQty
                        lnRet = maxCred.maxUnitRetail
                    End If
                Else
                    lnQty = CDbl(MyDataReader2.Item("qty").ToString)
                    lnRet = CDbl(MyDataReader2.Item("unit_prc").ToString)
                End If

                ' TODO code to limit the CRM maximum finance credit available  - needs some work
                If IsNumeric(MyDataReader2.Item("ORIG_FI_AMT").ToString) Then
                    fi_amt = CDbl(MyDataReader2.Item("ORIG_FI_AMT").ToString)
                End If

                If fi_amt > 0 Then
                    fin = "True"
                Else
                    fin = "False"
                End If

                If MyDataReader2.Item("FIN_CUST_CD").ToString & "" <> "" Then
                    fin_co = MyDataReader2.Item("FIN_CUST_CD").ToString
                End If

                Session("TAX_CD") = MyDataReader2.Item("TAX_CD").ToString
                Session("MANUAL_TAX_CD") = Nothing
                Session("origSoWrDt") = MyDataReader2.Item("so_wr_dt").ToString
                determine_crm_tax(MyDataReader2.Item("so_wr_dt").ToString)
                If Not IsNumeric(Session("TAX_RATE")) Then Session("TAX_RATE") = 0
                sqlSb.Clear()
                ' Daniela fix sql error
                'sqlSb.Append("INSERT INTO TEMP_ITM (SESSION_ID, ITM_CD, QTY, max_crm_qty, VE_CD, MNR_CD, CAT_CD, RET_PRC, max_ret_prc, ORIG_PRC, MANUAL_PRC, orig_so_ln_seq, VSN, DES, ITM_TP_CD, TREATABLE, inventory, ").Append(
                '    "DEL_PTS, COMM_CD, TAX_CD, TAX_PCT, TAX_AMT, TAXABLE_AMT, OUT_ID, SERIAL_NUM, slsp1, slsp1_pct,warrantable ) ")
                sqlSb.Append("INSERT INTO TEMP_ITM (SESSION_ID, ITM_CD, QTY, max_crm_qty, VE_CD, MNR_CD, CAT_CD, RET_PRC, max_ret_prc, ORIG_PRC, MANUAL_PRC, orig_so_ln_seq, VSN, DES, ITM_TP_CD, TREATABLE, inventory, ").Append(
                    "DEL_PTS, COMM_CD, TAX_CD, TAX_PCT, TAX_AMT, TAXABLE_AMT, OUT_ID, SERIAL_NUM, slsp1, slsp1_pct ")
                isSlsp2OnLine = False
                If Not IsDBNull(MyDataReader2.Item("ln_slsp2")) Then

                    sqlSb.Append(", slsp2, slsp2_pct ")
                    isSlsp2OnLine = True
                End If
                ' max_crm_qty is being set to the sales line orig qty if MCR - that's good because we need it to confirm max extended since max_ret_prc is per unit
                sqlSb.Append(",warrantable, SPIFF ) ")
                ' " VALUES('" & Session.SessionID.ToString & "','" & MyDataReader2.Item("ITM_CD").ToString & "'," & lnQty & ", " & lnQty & ",'" & MyDataReader2.Item("VE_CD").ToString & "'").Append(
                ' ",'" & MyDataReader2.Item("MNR_CD").ToString & "','" & MyDataReader2.Item("CAT_CD").ToString & "'," & lnRet & ", " & lnRet & ", " & lnRet & ", " & lnRet & ", " & MyDataReader2.Item("del_doc_ln#").ToString).Append(
                '",'" & MyDataReader2.Item("VSN").ToString & "','" & MyDataReader2.Item("DES").ToString & "','" & MyDataReader2.Item("ITM_TP_CD").ToString & "'").Append(
                ' ",'" & MyDataReader2.Item("TREATABLE").ToString & "', '" & MyDataReader2.Item("inventory").ToString & "' ")
                ' Daniela Nov 20 fix single quote bug
                sqlSb.Append(" VALUES('" & Session.SessionID.ToString & "','" & MyDataReader2.Item("ITM_CD").ToString & "'," & lnQty & ", " & lnQty & ",'" & MyDataReader2.Item("VE_CD").ToString & "'").Append(
                ",'" & MyDataReader2.Item("MNR_CD").ToString & "','" & MyDataReader2.Item("CAT_CD").ToString & "'," & lnRet & ", " & lnRet & ", " & lnRet & ", " & lnRet & ", " & MyDataReader2.Item("del_doc_ln#").ToString).Append(
                ",:vsn,:des,'" & MyDataReader2.Item("ITM_TP_CD").ToString & "'").Append(
                ",'" & MyDataReader2.Item("TREATABLE").ToString & "', '" & MyDataReader2.Item("inventory").ToString & "' ")

                If IsNumeric(MyDataReader2.Item("POINT_SIZE").ToString) Then
                    sqlSb.Append("," & MyDataReader2.Item("POINT_SIZE").ToString)
                Else
                    sqlSb.Append(",0")
                End If
                If Not IsDBNull(MyDataReader2.Item("COMM_CD")) Then
                    sqlSb.Append(",'").Append(MyDataReader2.Item("COMM_CD").ToString).Append("'")
                Else
                    sqlSb.Append(",NULL")
                End If

                If CDbl(MyDataReader2.Item("CUST_TAX_CHG").ToString) > 0 Then
                    sqlSb.Append(",'" & MyDataReader2.Item("TAX_CD").ToString & "', " & Session("TAX_RATE") & ", " & MyDataReader2.Item("CUST_TAX_CHG").ToString & "," & MyDataReader2.Item("UNIT_PRC").ToString & " ")

                ElseIf (Session("TAX_CD") & "" <> "") AndAlso
                       TaxUtils.Determine_Taxability(MyDataReader2.Item("ITM_TP_CD").ToString, Date.Today, Session("TAX_CD")) = "Y" AndAlso String.IsNullOrEmpty(Session("tet_cd")) Then

                    sqlSb.Append(",'" & MyDataReader2.Item("TAX_CD").ToString & "', " & Session("TAX_RATE") & ", " & Replace(FormatNumber(CDbl(MyDataReader2.Item("UNIT_PRC").ToString) * (CDbl(Session("TAX_RATE")) / 100), TaxUtils.Tax_Constants.taxPrecision), ",", "") & "," & MyDataReader2.Item("UNIT_PRC").ToString & " ")
                Else
                    sqlSb.Append(",'" & MyDataReader2.Item("TAX_CD").ToString & "', " & Session("TAX_RATE") & ", 0, 0 ")
                End If
                If MyDataReader2.Item("OUT_ID_CD").ToString & "" <> "" Then
                    sqlSb.Append(", '" & MyDataReader2.Item("OUT_ID_CD").ToString & "' ")
                Else
                    sqlSb.Append(",NULL")
                End If
                If MyDataReader2.Item("SER_NUM").ToString & "" <> "" Then
                    sqlSb.Append(", '" & MyDataReader2.Item("SER_NUM").ToString & "' ")
                Else
                    sqlSb.Append(",NULL")
                End If
                sqlSb.Append(", :lnSlsp1, :lnSlsp1Pct ")

                'sqlSb.Append(",'" & If(String.IsNullOrEmpty(MyDataReader2.Item("warrantable") & ""), "N", MyDataReader2.Item("warrantable")) & "'")

                If isSlsp2OnLine Then
                    sqlSb.Append(", :lnSlsp2, :lnSlsp2Pct ")
                End If
                'Daniela Oct 02
                sqlSb.Append(",'" & If(String.IsNullOrEmpty(MyDataReader2.Item("warrantable") & ""), "N", MyDataReader2.Item("warrantable")) & "'")
                If Not IsDBNull(MyDataReader2.Item("SPIFF")) Then
                    sqlSb.Append(",'").Append(MyDataReader2.Item("SPIFF").ToString).Append("'")
                Else
                    sqlSb.Append(",NULL")
                End If
                sqlSb.Append(" ) RETURNING ROW_ID INTO :ROW_ID")
                sql = sqlSb.ToString

                cmdLOC = DisposablesManager.BuildOracleCommand(sql, connLOC)
                ' Daniela add parameters
                cmdLOC.Parameters.Add("vsn", System.Data.OracleClient.OracleType.VarChar)
                cmdLOC.Parameters("vsn").Value = MyDataReader2.Item("VSN").ToString
                cmdLOC.Parameters.Add("des", System.Data.OracleClient.OracleType.VarChar)
                cmdLOC.Parameters("des").Value = MyDataReader2.Item("DES").ToString
                ' End Daniela
                cmdLOC.Parameters.Add("lnSlsp1", System.Data.OracleClient.OracleType.VarChar)
                cmdLOC.Parameters("lnSlsp1").Value = MyDataReader2.Item("ln_slsp1").ToString
                cmdLOC.Parameters.Add("lnSlsp1Pct", System.Data.OracleClient.OracleType.Number)
                cmdLOC.Parameters("lnSlsp1Pct").Value = CDbl(MyDataReader2.Item("ln_pct1").ToString)
                If isSlsp2OnLine Then
                    cmdLOC.Parameters.Add(":lnSlsp2", System.Data.OracleClient.OracleType.VarChar)
                    cmdLOC.Parameters(":lnSlsp2").Value = MyDataReader2.Item("ln_slsp2").ToString
                    cmdLOC.Parameters.Add(":lnSlsp2Pct", System.Data.OracleClient.OracleType.Number)

                    If IsNumeric(MyDataReader2.Item("ln_pct2").ToString) Then

                        cmdLOC.Parameters(":lnSlsp2Pct").Value = CDbl(MyDataReader2.Item("ln_pct2").ToString)
                    Else
                        cmdLOC.Parameters(":lnSlsp2Pct").Value = 100 - CDbl(MyDataReader2.Item("ln_pct1").ToString)
                    End If
                End If
                Session("CUST_CD") = MyDataReader2.Item("CUST_CD").ToString
                Session("PD") = MyDataReader2.Item("PU_DEL").ToString
                Session("TAX_CHG") = MyDataReader2.Item("TAX_CHG").ToString
                Session("DEL_CHG") = MyDataReader2.Item("DEL_CHG").ToString
                Session("SETUP_CHG") = MyDataReader2.Item("SETUP_CHG").ToString

                'Daniela IT Request 2475
                If (Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") Then
                    'save orig setup to validate limit
                    If IsNumeric(Session("DEL_CHG")) Then
                        Session("ORIG_DEL_CHG") = Session("DEL_CHG")
                    Else
                        Session("ORIG_DEL_CHG") = 0
                    End If
                    Session("DEL_CHG") = 0
                    If IsNumeric(Session("SETUP_CHG")) Then
                        Session("ORIG_SETUP_CHG") = Session("SETUP_CHG")
                    Else
                        Session("ORIG_SETUP_CHG") = 0
                    End If
                    Session("SETUP_CHG") = 0
                End If
                ' End IT Request 2475

                Session("itemid") = "NONEENTERED"
                Session("slsp1") = MyDataReader2.Item("SO_EMP_SLSP_CD1").ToString
                Session("slsp2") = MyDataReader2.Item("SO_EMP_SLSP_CD2").ToString
                If IsNumeric(MyDataReader2.Item("PCT_OF_SALE1").ToString) Then
                    Session("pct_1") = MyDataReader2.Item("PCT_OF_SALE1").ToString
                Else
                    Session("pct_1") = CDbl(0)
                End If
                If IsNumeric(MyDataReader2.Item("PCT_OF_SALE2").ToString) Then
                    Session("pct_2") = MyDataReader2.Item("PCT_OF_SALE2").ToString
                Else
                    Session("pct_2") = CDbl(0)
                End If


                cboSalesperson1.Enabled = False
                cboSalesperson2.Enabled = False
                txt_pct_1.Enabled = False
                txt_pct_2.Enabled = False

                cmdLOC.Parameters.Add(":ROW_ID", OracleType.Int32).Direction = ParameterDirection.Output
                cmdLOC.Parameters(":ROW_ID").Value = System.DBNull.Value
                cmdLOC.ExecuteNonQuery()
                rowLn = cmdLOC.Parameters(":ROW_ID").Value
                If firstRowLn = 0 Then
                    firstRowLn = rowLn
                End If
                If "Y".Equals(MyDataReader2.Item("WARRANTABLE").ToString()) Then
                    theSalesBiz.UpdateWarrDetailsForNonSale(cmdLOC, Session.SessionID.ToString, rowLn, voidedLines, orig_del_Doc_num, MyDataReader2("del_doc_ln#") & "", firstRowLn)
                End If
            Loop
            MyDataReader2.Close()
            ''Added as part of MM-14627.If the sales order has warranty associated with multiple warranty links and If Sales person splits the sales order
            '' using SORW screen and moves few items to 'A' ticket the we need to adjust the rowId of the Warranty link item.

            'Alice updated on Feb 27,2019 for request 4564
            ' theSalesBiz.UpdateWarrLinkForSplitSales(cmdLOC, Session.SessionID.ToString, orig_del_Doc_num, firstRowLn)
            UpdateWarrRowLinkForSplitSales(Session.SessionID.ToString)

        Catch ex As Exception
            connLOC.Close()
            connERP.Close()
            Throw
        End Try

        If fin = "True" Then
            Dim fi_acct_num As String = ""
            ' Daniela fix CRM encrypt issue June 23
            'sql = "SELECT BNK_CRD_NUM FROM AR_TRN, SO WHERE AR_TRN.IVC_CD=SO.DEL_DOC_NUM AND AR_TRN.CUST_CD=SO.CUST_CD AND SO.DEL_DOC_NUM='" & UCase(orig_del_Doc_num) & "' AND TRN_TP_CD='SAL'"
            sql = "SELECT std_des3.decryptCrdNum(BNK_CRD_NUM) BNK_CRD_NUM FROM AR_TRN, SO WHERE AR_TRN.IVC_CD=SO.DEL_DOC_NUM AND AR_TRN.CUST_CD=SO.CUST_CD AND SO.DEL_DOC_NUM='" & UCase(orig_del_Doc_num) & "' AND TRN_TP_CD='SAL'"
            'Set SQL OBJECT 
            cmdERP = DisposablesManager.BuildOracleCommand(sql, connERP)
            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmdERP)
                'Store Values in String Variables 
                Do While MyDataReader2.Read()
                    fi_acct_num = MyDataReader2.Item("BNK_CRD_NUM").ToString
                Loop
            Catch
                Throw
            End Try

            Dim promo_cd As String = ""

            sql = "SELECT PROMO_CD FROM SO_ASP WHERE DEL_DOC_NUM='" & UCase(orig_del_Doc_num) & "'"
            'Set SQL OBJECT 
            cmdERP = DisposablesManager.BuildOracleCommand(sql, connERP)
            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmdERP)
                'Store Values in String Variables 
                Do While MyDataReader2.Read()
                    promo_cd = MyDataReader2.Item("promo_cd").ToString
                Loop
            Catch
                Throw
            End Try

            Try
                sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD)"
                sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD)"

                cmdLOC = DisposablesManager.BuildOracleCommand(sql, connLOC)

                cmdLOC.Parameters.Add(":sessionid", OracleType.VarChar)
                cmdLOC.Parameters.Add(":MOP_CD", OracleType.VarChar)
                cmdLOC.Parameters.Add(":DES", OracleType.VarChar)
                cmdLOC.Parameters.Add(":AMT", OracleType.Number)
                cmdLOC.Parameters.Add(":MOP_TP", OracleType.VarChar)
                cmdLOC.Parameters.Add(":FIN_CO", OracleType.VarChar)
                cmdLOC.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
                cmdLOC.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)

                cmdLOC.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
                cmdLOC.Parameters(":MOP_CD").Value = "FI"
                cmdLOC.Parameters(":AMT").Value = fi_amt
                cmdLOC.Parameters(":DES").Value = "Automated Credit"
                cmdLOC.Parameters(":MOP_TP").Value = "FI"
                cmdLOC.Parameters(":FIN_CO").Value = fin_co
                cmdLOC.Parameters(":FI_ACCT_NUM").Value = AppUtils.Encrypt(fi_acct_num, "CrOcOdIlE")
                cmdLOC.Parameters(":FIN_PROMO_CD").Value = promo_cd

                cmdLOC.ExecuteNonQuery()
                'End If
            Catch
                Throw
            End Try
            Session("payments") = fi_amt
            Session("payment") = True
        End If

        sql = "SELECT FNAME, LNAME, CUST_CD, HOME_PHONE, BUS_PHONE, ADDR1,ADDR2, CITY, ST_CD, ZIP_CD, CUST_TP_CD, EMAIL_ADDR FROM CUST WHERE cust_cd = '" & Session("cust_cd") & "'"

        'Set SQL OBJECT 
        cmdERP = DisposablesManager.BuildOracleCommand(sql, connERP)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmdERP)
            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then

                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                ' Daniela May 06 fix address with single quotes issues
                sql = "INSERT INTO CUST_INFO (SESSION_ID, CUST_CD, FNAME, LNAME, CUST_TP, ADDR1, ADDR2, CITY, ST, ZIP, HPHONE, BPHONE, EMAIL, BILL_FNAME, BILL_LNAME, BILL_ADDR1, BILL_ADDR2, BILL_CITY, BILL_ST, BILL_ZIP) "
                sql = sql & "VALUES("
                sql = sql & ":SESSION_ID, :CUST_CD, :FNAME, :LNAME, :CUST_TP_CD, :ADDR1, :ADDR2, :CITY, :ST_CD, :ZIP_CD, :HOME_PHONE, :BUS_PHONE, :EMAIL_ADDR, :FNAME, :LNAME, :ADDR1, :ADDR2, :CITY, :ST_CD, :ZIP_CD)"

                'Start Daniela May 06 fix address with single quotes issues
                cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
                cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
                cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(Session("cust_cd"))
                cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
                cmdDeleteItems.Parameters(":FNAME").Value = UCase(MyDataReader2.Item("FNAME").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
                cmdDeleteItems.Parameters(":LNAME").Value = UCase(MyDataReader2.Item("LNAME").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":CUST_TP_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":CUST_TP_CD").Value = UCase(MyDataReader2.Item("CUST_TP_CD").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ADDR1").Value = UCase(MyDataReader2.Item("ADDR1").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ADDR2").Value = UCase(MyDataReader2.Item("ADDR2").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
                cmdDeleteItems.Parameters(":CITY").Value = UCase(MyDataReader2.Item("CITY").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ST_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ST_CD").Value = UCase(MyDataReader2.Item("ST_CD").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ZIP_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ZIP_CD").Value = UCase(MyDataReader2.Item("ZIP_CD").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":HOME_PHONE", OracleType.VarChar)
                cmdDeleteItems.Parameters(":HOME_PHONE").Value = UCase(MyDataReader2.Item("HOME_PHONE").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":BUS_PHONE", OracleType.VarChar)
                cmdDeleteItems.Parameters(":BUS_PHONE").Value = UCase(MyDataReader2.Item("BUS_PHONE").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":EMAIL_ADDR", OracleType.VarChar)
                cmdDeleteItems.Parameters(":EMAIL_ADDR").Value = UCase(MyDataReader2.Item("EMAIL_ADDR").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
                cmdDeleteItems.Parameters(":FNAME").Value = UCase(MyDataReader2.Item("FNAME").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
                cmdDeleteItems.Parameters(":LNAME").Value = UCase(MyDataReader2.Item("LNAME").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ADDR1").Value = UCase(MyDataReader2.Item("ADDR1").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ADDR2").Value = UCase(MyDataReader2.Item("ADDR2").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
                cmdDeleteItems.Parameters(":CITY").Value = UCase(MyDataReader2.Item("CITY").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ST_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ST_CD").Value = UCase(MyDataReader2.Item("ST_CD").ToString.Trim)
                cmdDeleteItems.Parameters.Add(":ZIP_CD", OracleType.VarChar)
                cmdDeleteItems.Parameters(":ZIP_CD").Value = UCase(MyDataReader2.Item("ZIP_CD").ToString.Trim)

                'End Daniela May 06 fix address with single quotes issues

                'sql = sql & "'" & Session.SessionID.ToString.Trim & "',"
                'sql = sql & "'" & UCase(Session("cust_cd")) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("FNAME").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("LNAME").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("CUST_TP_CD").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ADDR1").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ADDR2").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("CITY").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ST_CD").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ZIP_CD").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("HOME_PHONE").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("BUS_PHONE").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("EMAIL_ADDR").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("FNAME").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("LNAME").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ADDR1").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ADDR2").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("CITY").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ST_CD").ToString.Trim) & "',"
                'sql = sql & "'" & UCase(MyDataReader2.Item("ZIP_CD").ToString.Trim) & "')"



                With cmdDeleteItems
                    .Connection = connLOC
                    .CommandText = sql
                End With



                cmdDeleteItems.ExecuteNonQuery()
            End If
            'Close Connection 
            MyDataReader2.Close()
            connLOC.Close()
            connERP.Close()
        Catch ex As Exception
            connLOC.Close()
            connERP.Close()
            Throw
        End Try

        calculate_total(Page.Master)

    End Sub

    'Alice added on Feb 28, 2019 for request 4564
    Private Sub UpdateWarrRowLinkForSplitSales(ByVal sessionId As String)

        Dim SQL As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)
        conn.Open()
        Try

            SQL = "declare w_row_id integer; w_itm_cd varchar(12); "
            SQL = SQL & "cursor warrCursor is select row_id, itm_cd from temp_itm where itm_tp_cd='WAR'; "
            SQL = SQL & "begin open warrCursor; LOOP "
            SQL = SQL & "fetch warrCursor into w_row_id, w_itm_cd; "
            SQL = SQL & "update temp_itm set WARR_ROW_LINK = w_row_id where warr_itm_link = w_itm_cd and session_id ='" & sessionId & "'; "
            SQL = SQL & "exit when warrCursor%NOTFOUND; "
            SQL = SQL & "END LOOP; CLOSE warrCursor;END;"

            cmd.CommandText = SQL
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    Public Sub determine_crm_tax(ByVal taxDt_inp As String)
        ' most likely used by line tax method only

        Dim sql As String
        Dim cmd As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        ' TODO - need to do later, loop thru lines and get by itm_tp_cd - call taxUtils.getTaxPct for each line
        Dim taxDtStr As String
        If (Not taxDt_inp Is Nothing) AndAlso taxDt_inp.isNotEmpty AndAlso IsDate(taxDt_inp) Then
            taxDtStr = taxDt_inp
        Else
            taxDtStr = Now()
        End If
        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= TO_DATE('" & FormatDateTime(taxDtStr, DateFormat.ShortDate) & "','mm/dd/RRRR') And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= TO_DATE('" & FormatDateTime(taxDtStr, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND  TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD"

        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        'Set SQL OBJECT 
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Set SQL OBJECT 
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 

            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)
            'Store Values in String Variables 
            If (MyDataReader.Read()) And numrows = 1 Then
                Session("TAX_RATE") = MyDataReader.Item("SumofPCT")
            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Populates the sort code drop down with sort codes setup in the system.
    ''' </summary>
    Private Sub PopulateOrderSortCodes()

        Try
            Dim ds As DataSet = theSalesBiz.GetOrderSortCodes()
            With cboOrdSrt
                .DataSource = ds
                .ValueField = "ord_srt_cd"
                .TextField = "DES"
                .DataBind()
            End With

            If Session("ord_srt") & "" <> "" Then
                cboOrdSrt.Value = Session("ord_srt")
            ElseIf ConfigurationManager.AppSettings("default_sort_code").ToString & "" <> "" Then
                cboOrdSrt.Value = ConfigurationManager.AppSettings("default_sort_code").ToString
                Session("ord_srt") = ConfigurationManager.AppSettings("default_sort_code").ToString
            End If

            cboOrdSrt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Sort Code", ""))
            cboOrdSrt.Items.FindByText("Please Select A Sort Code").Value = ""
            If Session("ord_srt") & "" = "" And cboOrdSrt.Value & "" = "" Then
                cboOrdSrt.SelectedIndex = 0
            End If

            If Session("ord_srt") & "" = "" And cboOrdSrt.SelectedIndex > 0 Then
                Session("ord_srt") = cboOrdSrt.Value
            End If

            If Session("PD") = "P" Or Session("PD") = "D" Then
                cboPD.Value = Session("PD")
            Else
                Session("PD") = "D"
            End If

        Catch ex As Exception
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Populates the store drop down and also any cash drawer and other info associated with it.
    ''' </summary>
    Private Sub PopulateStores()

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim sessionStoreCd As String
        Dim sql As String = ""

        If Session("store_cd") & "" <> "" Then
            sessionStoreCd = Session("store_Cd")
        Else
            sessionStoreCd = Session("HOME_STORE_CD")
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        ' lucy
        Dim str_co_grp_cd As String = Nothing
        Dim str_co_cd As String
        Dim str_sup_user_flag As String
        Dim str_flag As String

        'Daniela Nov 2 improve performance
        'Franchise changes
        If (isNotEmpty(Session("CO_CD")) And isEmpty(Session("str_co_grp_cd"))) Then
            str_co_grp_cd = LeonsBiz.GetGroupCode(Session("CO_CD"))
            Session("str_co_grp_cd") = str_co_grp_cd
        End If

        ' MCCL DB Nov 9, 2017

        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            sql = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            sql = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
               "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
               "where a.user_id = :userType and " +
               "s.store_cd = b.store_cd and " +
               "a.store_grp_cd = b.store_grp_cd "
            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                sql = sql & " and '" & coCd & "' = s.co_cd "

            End If
        End If
        sql = sql & " order by store_cd"


        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = sql
            End With

            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType

            End If


            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            With cbo_pd_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            ds.Clear()
            cmdGetStores.Parameters.Clear()

            sql = "SELECT STORE.STORE_CD, STORE.STORE_NAME, STORE.STORE_CD || ' - ' || STORE.STORE_NAME as full_desc FROM STORE, CSH_DWR "
            sql = sql & " WHERE CSH_DWR.STORE_CD=STORE.STORE_CD "
            If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
                sql = sql & "AND STORE.STORE_CD IN(select store_grp$store.store_cd "
                sql = sql & "from store_grp$store, emp2store_grp "
                sql = sql & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
                sql = sql & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            End If
            sql = sql & "GROUP BY STORE.STORE_CD, STORE.STORE_NAME order by STORE.STORE_CD"

            With cmdGetStores
                .Connection = conn
                .CommandText = sql
            End With

            Dim ds2 As New DataSet
            Dim oAdp2 = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp2.Fill(ds2)

            Dim foundrow As DataRow = Nothing
            If sessionStoreCd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds2.Tables(0).Columns("STORE_CD")
                'set the primary key to the CustomerID column
                ds2.Tables(0).PrimaryKey = pkColumn
                foundrow = ds2.Tables(0).Rows.Find(sessionStoreCd)
            End If

            With cboStores
                .DataSource = ds2
                .TextField = "FULL_DESC"
                .ValueField = "STORE_CD"
                .DataBind()
                If (Not IsNothing(foundrow)) Then
                    .SelectedItem = .Items.FindByValue(sessionStoreCd)
                End If
            End With
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        If cboStores.Value = "" Then cboStores.SelectedIndex = 0
        UpdateStoreInfo()

    End Sub

    Private Sub PopulateSalespersons()

        Dim ds As New DataSet
        Dim fname As String
        Dim lname As String
        Dim fullname As String
        Dim sfullname As String
        Dim empcd As String = ""
        Dim empcd2 As String = ""

        'Repopulate the drop down boxes based on any previous stored data

        If Session("slsp1") & "" <> "" Then
            empcd = Session("slsp1")
        Else
            empcd = UCase(Session("emp_cd"))
        End If

        If Session("slsp2") & "" <> "" Then
            empcd2 = Session("slsp2")
        End If

        'Alice added start on Feb 25, 2019 for request 4477
        If Session("Orig_slsp1_forExchangeSo") IsNot Nothing Then
            If Session("Orig_slsp1_forExchangeSo").ToString <> "" Then
                empcd = Session("Orig_slsp1_forExchangeSo")
            End If
        End If

        If Session("Orig_slsp2_forExchangeSo") IsNot Nothing Then
            If Session("Orig_slsp2_forExchangeSo").ToString <> "" Then
                empcd2 = Session("Orig_slsp2_forExchangeSo")
            End If
        End If
        'Alice added end on Feb 25, 2019 for request 4477


        ' Daniela
        'ds = SalesUtils.RetrieveSalespersons()
        'ds = LeonsBiz.lucy_RetrieveSalespersons(empcd)
        'Franchise changes

        'Alice updated On May 13, 2019 for request 81 
        If Session("str_co_grp_cd").ToString = "BRK" Then
            If (Request("adj_ivc_cd") & "" <> "" OrElse Request("crm_cause_cd") & "" <> "") Then
                Dim co_cd As String = GetCompanyCode(empcd)
                ds = LeonsBiz.lucy_RetrieveSalespersons(empcd, co_cd, Session("ord_tp_cd"))
            Else
                ds = LeonsBiz.lucy_RetrieveSalespersons(empcd, Session("CO_CD"), Session("ord_tp_cd"))
            End If
        Else
            ds = LeonsBiz.lucy_RetrieveSalespersons(empcd, Session("CO_CD"))
        End If



        Dim foundrow As DataRow = Nothing
        If empcd & "" <> "" Then
            Dim pkColumn(1) As DataColumn
            pkColumn(0) = ds.Tables(0).Columns("EMP_CD")
            'set the primary key to the CustomerID column
            ds.Tables(0).PrimaryKey = pkColumn
            foundrow = ds.Tables(0).Rows.Find(empcd)

        End If

        Dim dept_cd As String = GetDeptCode(Session("emp_cd"))

        With cboSalesperson1
            .DataSource = ds
            .ValueField = "EMP_CD"
            .TextField = "FullNAME"


            If Session("ord_tp_cd").ToString = AppConstants.Order.TYPE_SAL Then 'Alice udpated on May 22, 2019 for request 81
                If (Not (foundrow Is Nothing)) And (dept_cd <> "CSR") Then
                    .Value = empcd
                    Session("slsp1") = empcd
                End If

            Else

                If Session("str_co_grp_cd") = "BRK" Then
                    If Session("OriginalOrderExist") = "True" Then
                        If IsSalePersonWithTerm(Session("OriginalDocNum").ToString, 1) Then
                            .Value = "HOU" & Session("store_cd").ToString.Trim
                            Session("slsp1") = "HOU" & Session("store_cd").ToString.Trim
                        Else
                            .Value = empcd
                            Session("slsp1") = empcd
                        End If

                    Else
                        If (dept_cd <> "CSR") Then
                            .Value = Session("emp_cd")
                            Session("slsp1") = Session("emp_cd")
                        End If
                    End If

                Else
                    If Not (foundrow Is Nothing) Then
                        .Value = empcd
                        Session("slsp1") = empcd
                    End If
                End If
            End If
            .DataBind()
        End With

        fname = Session("emp_fname")
        lname = Session("emp_lname")
        fullname = lname + ", " + fname
        sfullname = cboSalesperson1.Text

        If sfullname <> fullname Then
            'Daniela french
            'cboSalesperson1.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Salesperson", ""))
            'cboSalesperson1.Items.FindByText("Please Select A Salesperson").Value = ""
            cboSalesperson1.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label692, ""))
            cboSalesperson1.Items.FindByText(Resources.LibResources.Label692).Value = ""
            If (foundrow Is Nothing) Then
                cboSalesperson1.SelectedIndex = 0
            End If
        End If

        foundrow = Nothing
        If empcd2 & "" <> "" Then
            Dim pkColumn(1) As DataColumn
            pkColumn(0) = ds.Tables(0).Columns("EMP_CD")
            'set the primary key to the CustomerID column
            ds.Tables(0).PrimaryKey = pkColumn
            foundrow = ds.Tables(0).Rows.Find(empcd2)
        End If

        With cboSalesperson2
            .DataSource = ds
            .ValueField = "EMP_CD"
            .TextField = "FullNAME"

            'If Not (foundrow Is Nothing) Then
            '    .Value = empcd2
            'End If

            If Session("ord_tp_cd").ToString = AppConstants.Order.TYPE_SAL Then 'Alice udpated on Jun 25, 2019 for request 81
                If (Not (foundrow Is Nothing)) And (dept_cd <> "CSR") Then
                    .Value = empcd2
                End If

            Else

                If Session("str_co_grp_cd") = "BRK" Then
                    If Session("OriginalOrderExist") = "True" Then
                        If IsSalePersonWithTerm(Session("OriginalDocNum").ToString, 2) Then
                            .Value = "HOU" & Session("store_cd").ToString.Trim
                            Session("slsp2") = "HOU" & Session("store_cd").ToString.Trim
                        Else
                            .Value = empcd2
                            Session("slsp2") = empcd2
                        End If
                    End If

                Else
                    If Not (foundrow Is Nothing) Then
                        .Value = empcd2
                        Session("slsp2") = empcd2
                    End If
                End If
            End If

            .DataBind()
        End With

        cboSalesperson2.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label692, ""))
        cboSalesperson2.Items.FindByText(Resources.LibResources.Label692).Value = ""
        If (foundrow Is Nothing) Then
            cboSalesperson2.SelectedIndex = 0
            txt_pct_1.Enabled = False
        End If

    End Sub


    Private Sub PopulateGenlIntCategories()
        Dim ds As New DataSet
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim SQL As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        SQL = "SELECT GEN_CAT_ID, DES FROM RELATIONSHIP_GEN_INT WHERE DELETED IS NULL ORDER BY DES"

        Try
            With cmd
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If Session("gen_int") & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("GEN_CAT_ID")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(Session("gen_int"))
            End If

            cbo_general.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Category", ""))
            cbo_general.Items.FindByText("Please Select A Category").Value = ""

            With cbo_general
                .DataSource = ds
                .ValueField = "GEN_CAT_ID"
                .TextField = "DES"
                If Not (foundrow Is Nothing) Then
                    .Value = Session("gen_int")
                End If
                .DataBind()
            End With
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub UpdateSalesperson(ByVal sender As Object, ByVal e As System.EventArgs)

        If cboSalesperson1.Value <> "" Then
            Session("slsp1") = cboSalesperson1.Value
        End If
        If cboSalesperson2.Value & "" <> "" Then
            txt_pct_2.Enabled = True
            If cboSalesperson2.Value = cboSalesperson1.Value Then
                cboSalesperson2.SelectedIndex = 0
            Else
                txt_pct_1.Enabled = True
                If Not IsNumeric(ConfigurationManager.AppSettings("slsp2")) Then
                    txt_pct_1.Text = "100"
                    txt_pct_2.Text = "0"
                Else
                    'MM-6865
                    txt_pct_2.Text = IIf(IsNumeric(ConfigurationManager.AppSettings("slsp2")), ConfigurationManager.AppSettings("slsp2"), 0)
                    txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                End If
                Session("pct_1") = txt_pct_1.Text
                Session("pct_2") = txt_pct_2.Text
                Session("slsp2") = cboSalesperson2.Value
            End If
        Else
            txt_pct_2.Enabled = False
            txt_pct_2.Text = 0
            txt_pct_1.Text = 100
            Session("slsp2") = ""
            Session("pct_1") = 100
            'MM-6865
            Session("pct_2") = CDbl(0)
            cboSalesperson2.Value = ""
            txt_pct_1.Enabled = False
        End If

        If Request("LEAD") <> "TRUE" AndAlso Session("SlspInfo") IsNot Nothing Then

            Dim slspInfo As New OrderUtils.SlspCommissionInfo
            slspInfo.slsp1 = Session("slsp1")
            slspInfo.slsp2 = Session("slsp2")
            slspInfo.pct1 = IIf(IsNumeric(Session("pct_1")), Session("pct_1"), 0)
            slspInfo.pct2 = IIf(IsNumeric(Session("pct_2")), Session("pct_2"), 0)
            Slsp_Temp_Line_Update(Session.SessionID.ToString.Trim, slspInfo, Session("SlspInfo"))
            Session("SlspInfo") = slspInfo ' set to current new values

        End If

    End Sub

    Private Sub Slsp_Temp_Line_Update(ByVal sessId As String, ByVal newSlspInfo As OrderUtils.SlspCommissionInfo, ByVal origSlspInfo As OrderUtils.SlspCommissionInfo)

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("UPDATE TEMP_ITM SET slsp1 = :slsp1, slsp1_pct = :pct1, slsp2 = :slsp2, slsp2_pct = :pct2 WHERE SESSION_ID= :sessionId AND ").Append(
            " NVL(slsp1, ' ') = NVL(:origSlsp1, ' ') AND NVL(slsp2, ' ') = NVL(:origSlsp2, ' ') AND NVL(slsp1_pct, 0) = NVL(:origPct1, 0) AND ").Append(
            " NVL(slsp2_pct, 0) = NVL(:origPct2, 0) ")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

        cmd.Parameters.Add(":sessionId", OracleType.VarChar)
        cmd.Parameters(":sessionId").Value = sessId
        cmd.Parameters.Add(":slsp1", OracleType.VarChar)
        cmd.Parameters(":slsp1").Value = UCase(newSlspInfo.slsp1)
        cmd.Parameters.Add(":slsp2", OracleType.VarChar)
        cmd.Parameters(":slsp2").Value = UCase(newSlspInfo.slsp2)
        cmd.Parameters.Add(":origSlsp1", OracleType.VarChar)
        cmd.Parameters(":origSlsp1").Value = UCase(origSlspInfo.slsp1)
        cmd.Parameters.Add(":origSlsp2", OracleType.VarChar)
        cmd.Parameters(":origSlsp2").Value = UCase(origSlspInfo.slsp2)
        cmd.Parameters.Add(":pct1", OracleType.Number)
        cmd.Parameters(":pct1").Value = newSlspInfo.pct1
        cmd.Parameters.Add(":pct2", OracleType.Number)
        cmd.Parameters(":pct2").Value = newSlspInfo.pct2
        cmd.Parameters.Add(":origPct1", OracleType.Number)
        cmd.Parameters(":origPct1").Value = origSlspInfo.pct1
        cmd.Parameters.Add(":origPct2", OracleType.Number)
        cmd.Parameters(":origPct2").Value = origSlspInfo.pct2

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
        Finally
            conn.Close()
        End Try
    End Sub


    ''' <summary>
    ''' Updates the related written store info such as cash drawers and pickup-delv store
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateStoreInfo()

        If cboStores.Value <> "" Then

            If cboStores.Value <> Session("store_cd") Then Session("csh_dwr_cd") = ""

            SessVar.wrStoreCd = cboStores.Value

            Dim sessionCashDwr As String = Session("csh_dwr_cd")

            Dim ds As New DataSet
            Try
                ds = theSystemBiz.GetCashDrawers(cboStores.Value)
                Dim foundrow As DataRow
                If sessionCashDwr & "" <> "" Then
                    Dim pkColumn(1) As DataColumn
                    pkColumn(0) = ds.Tables(0).Columns("csh_dwr_cd")
                    'set the primary key to the CustomerID column
                    ds.Tables(0).PrimaryKey = pkColumn
                    foundrow = ds.Tables(0).Rows.Find(sessionCashDwr)
                End If

                With cbo_csh_drw
                    .DataSource = ds
                    .ValueField = "csh_dwr_cd"
                    .TextField = "full_desc"
                    .DataBind()
                    If Not IsNothing(cbo_csh_drw.Items.FindByValue(sessionCashDwr)) Then
                        .Value = sessionCashDwr
                    Else
                        .SelectedIndex = 0
                    End If
                End With
            Catch ex As Exception
                Throw
            End Try

            Session("csh_dwr_cd") = cbo_csh_drw.Value

            UpdatePickupDelStoreInfo()

        End If

    End Sub

    ''' <summary>
    ''' Retrieves the store info for the selected written store and updates the pickup/delivery store data.
    ''' </summary>
    Private Sub UpdatePickupDelStoreInfo()

        ' TODO - half of the data updated here is related to written store, not pu_del store but because of the convoluted nature of this process, I'm afraid to reorg it now
        '    the written store info sb set when the written store is set (updateStoreInfo) and/or modified and the pu_del store info sb set when pu_del, or cust zip changes;
        ' or set it when ready to use it and omit the complications of updating 
        If cboStores.Value <> "" Then

            Dim store As StoreData = theSalesBiz.GetStoreInfo(cboStores.Value.ToString.Trim)
            SessVar.wrStoreCd = cboStores.Value
            ' TODO consider saving store object for session; probably written and puDel - at least a subset
            '' MCCL nov 27, 2017
            'Session("CO_CD") = store.coCd

            '*****TODO:AXK -- figure out how to indicate that the store is setup for enable_ARS, for now put in the session
            ' TODO comment - looks like store init here and then again if cashier activation
            SessVar.isArsEnabled = store.enableARS

            txtPostalCd.Visible = store.enableARS And cboPD.Value = "D"
            lblPostalCd.Visible = txtPostalCd.Visible
            Dim puDelStore As String = theTranBiz.GetPuDelStore(cboStores.Value, SessVar.puDel, SessVar.zoneCd, store)
            SessVar.puDelStoreCd = puDelStore   ' TODO consider saving store object for session; probably written and puDel - at least a subset
            cbo_pd_store_cd.Value = puDelStore

            If IsPostBack Then
                ' TODO - since store date is not checked when populate stores, then probably need to do this on load too so in all cases but confirm first
                ValidateWrStoreNotClosed(store)
            End If

        End If
    End Sub
    ''' <summary>
    ''' Gets fired when the pickup/delivery store drop down changes. 
    ''' </summary>
    Protected Sub cbo_pd_store_cd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_pd_store_cd.SelectedIndexChanged

        ' TODO - seems like this field should only be avail if pickup but not ??? 
        If cbo_pd_store_cd.Value <> "" Then

            Session("pd_store_cd") = cbo_pd_store_cd.Value
            SetPickupDeliveryZipNZone()

            RecalculateTaxes()
            calculate_total(Page.Master)
        End If

    End Sub


    ''' <summary>
    ''' Fires when the order sort code value in the d.d. changes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cboOrdSrt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOrdSrt.SelectedIndexChanged

        If cboOrdSrt.Value <> "" Then
            Session("ord_srt") = cboOrdSrt.Value
        End If

    End Sub

    Protected Sub cboPD_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPD.SelectedIndexChanged

        Session("PD") = cboPD.Value

        If cboPD.Value = "P" Then
            Session("del_chg") = System.DBNull.Value.ToString
            Session("del_dt") = System.DBNull.Value.ToString
        End If

        SessVar.zoneCd = Nothing
        SessVar.zipCode4Zone = Nothing

        UpdatePickupDelStoreInfo()
        SetPickupDeliveryZipNZone()

        RecalculateTaxes()
        If CheckCalculateTotal() Then
            'In the startup page if change in the sale type is changed it will not be called.
            calculate_total(Page.Master)
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub txt_appt_time_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_appt_time.TextChanged
        Try
            If Len(txt_appt_time.Text) = 4 Then txt_appt_time.Text = "0" & txt_appt_time.Text
            If DateTime.TryParseExact(txt_appt_time.Text, "HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, New DateTime) Then
                If Left(txt_appt_time.Text, 2) > 12 Then
                    txt_appt_time.Text = CDbl(Left(txt_appt_time.Text, 2)) - 12 & Right(txt_appt_time.Text, Len(txt_appt_time.Text) - 2)
                    cbo_am_or_pm.Value = "PM"
                End If
                Session("appt_time") = txt_appt_time.Text
                Session("am_or_pm") = cbo_am_or_pm.Value
                Session("call_type") = cbo_call_type.Value
                'MM-5670
                cbo_am_or_pm.Focus()
            Else
                lbl_warnings.Text = "Invalid Time Entered"
                lbl_warnings.Visible = True
                txt_appt_time.Text = ""
                'MM-5670
                txt_appt_time.Focus()
            End If
        Catch
            Session("appt_time") = ""
            Session("am_or_pm") = ""
            Session("call_type") = ""
            lbl_warnings.Text = "Invalid appointment time entered, please try again."
            'MM-5670
            txt_appt_time.Focus()
        End Try
    End Sub


    Protected Sub cbo_am_or_pm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_am_or_pm.SelectedIndexChanged

        Session("am_or_pm") = cbo_am_or_pm.Value

    End Sub

    Protected Sub cbo_ord_tp_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_ord_tp_cd.SelectedIndexChanged

        Session("ord_tp_cd") = cbo_ord_tp_cd.Value
        If AppConstants.Order.TYPE_SAL <> cbo_ord_tp_cd.Value Then

            Dim strSessionId As String = If(IsNothing(Session.SessionID), "", Session.SessionID.ToString.Trim)
            If (strSessionId.isNotEmpty()) Then
                theSalesBiz.DeleteTempOrder(strSessionId)
                'Setting Session("DeleteTempOrder") value True if items are deleted from the Shoping bag
                Session("DeleteTempOrder") = True
            End If

            Session("payment") = System.DBNull.Value
            ASPxPopupControl1.ContentUrl = "CreditMemoPopulate.aspx"
            ASPxPopupControl1.ShowOnPageLoad = True
        End If

    End Sub

    Protected Sub txt_pct_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_pct_1.TextChanged

        If IsNumeric(txt_pct_1.Text) Then
            If CDbl(txt_pct_1.Text) <= 100 And CDbl(txt_pct_1.Text) > 0 Then
                'valid values, so just default the other pct field based on this one
                Session("pct_1") = txt_pct_1.Text
                txt_pct_2.Text = 100 - CDbl(txt_pct_1.Text)
                Session("pct_2") = txt_pct_2.Text
            Else
                lbl_warnings.Text = "Commission percentages must be between 1 and 100"
                If IsNumeric(txt_pct_2.Text) Then
                    txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                    Session("pct_1") = txt_pct_1.Text
                Else
                    txt_pct_1.Text = 100
                    Session("slsp2") = ""
                    Session("pct_1") = 100
                    'MM-6865
                    Session("pct_2") = CDbl(0)
                End If
            End If
        Else
            txt_pct_1.Text = 100
            txt_pct_2.Text = 0
            lbl_warnings.Text = "Commission percentages must be numeric"
        End If

        If Request("LEAD") <> "TRUE" AndAlso Session("SlspInfo") IsNot Nothing Then
            Dim slspInfo As New OrderUtils.SlspCommissionInfo
            slspInfo.slsp1 = Session("slsp1")
            slspInfo.slsp2 = Session("slsp2")
            slspInfo.pct1 = IIf(IsNumeric(Session("pct_1")), Session("pct_1"), 0)
            slspInfo.pct2 = IIf(IsNumeric(Session("pct_2")), Session("pct_2"), 0)
            Slsp_Temp_Line_Update(Session.SessionID.ToString.Trim, slspInfo, Session("SlspInfo"))
            Session("SlspInfo") = slspInfo ' set to current new values
            'OrderUtils.setSessSlsps(slspInfo)  ' TODO later - see OrderUtils cmnts

        End If
    End Sub

    Protected Sub txt_pct_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_pct_2.TextChanged

        If IsNumeric(txt_pct_2.Text) And cboSalesperson2.Value & "" <> "" Then
            If CDbl(txt_pct_2.Text) < 100 And CDbl(txt_pct_2.Text) > 0 Then
                Session("pct_2") = txt_pct_2.Text
                txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                Session("pct_1") = txt_pct_1.Text
            Else
                If IsNumeric(txt_pct_1.Text) And CDbl(txt_pct_2.Text) > 0 Then
                    txt_pct_2.Text = 100 - CDbl(txt_pct_1.Text)
                    Session("pct_2") = txt_pct_2.Text
                Else
                    txt_pct_1.Text = 100
                    txt_pct_2.Text = 0
                    Session("slsp2") = ""
                    Session("pct_1") = 100
                    'MM-6865
                    Session("pct_2") = CDbl(0)
                    cboSalesperson2.Value = ""
                End If
            End If
        Else
            txt_pct_2.Text = 0
            txt_pct_1.Text = 100
            Session("slsp2") = ""
            Session("pct_1") = 100
            'MM-6865
            Session("pct_2") = CDbl(0)
            cboSalesperson2.Value = ""
        End If

        If Request("LEAD") <> "TRUE" AndAlso Session("SlspInfo") IsNot Nothing Then

            Dim slspInfo As New OrderUtils.SlspCommissionInfo
            slspInfo.slsp1 = Session("slsp1")
            slspInfo.slsp2 = Session("slsp2")
            slspInfo.pct1 = IIf(IsNumeric(Session("pct_1")), Session("pct_1"), 0)
            slspInfo.pct2 = IIf(IsNumeric(Session("pct_2")), Session("pct_2"), 0)
            Slsp_Temp_Line_Update(Session.SessionID.ToString.Trim, slspInfo, Session("SlspInfo"))
            Session("SlspInfo") = slspInfo ' set to current new values

        End If
    End Sub

    Protected Sub cbo_general_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_general.SelectedIndexChanged

        If cbo_general.SelectedIndex = 0 Then
            Session("gen_int") = ""
        Else
            Session("gen_int") = cbo_general.Value
        End If

    End Sub

    Protected Sub cboStores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStores.SelectedIndexChanged

        UpdateStoreInfo()

        ' Since the store code changed, we need to recalculate taxes
        RecalculateTaxes()
        calculate_total(Page.Master)

    End Sub

    ''' <summary>
    ''' Determines the tax rate and code based on the current store code and it's zip code. 
    ''' </summary>
    Private Sub RecalculateTaxes()

        Dim isCRM As Boolean = Session("ord_tp_cd") IsNot Nothing AndAlso AppConstants.Order.TYPE_CRM.Equals(Session("ord_tp_cd"))
        Dim hasOrigSoWrDt As Boolean = Session("origSoWrDt") IsNot Nothing

        ' if processing a return to original document, we already have tax from orig, even store change doesn't impact
        If Not (isCRM AndAlso hasOrigSoWrDt) Then
            determine_tax()
        End If

    End Sub

    Protected Sub cbo_csh_drw_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_csh_drw.SelectedIndexChanged

        Session("csh_dwr_cd") = cbo_csh_drw.Value

    End Sub

    Protected Sub cbo_call_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_call_type.SelectedIndexChanged

        Session("call_type") = cbo_call_type.Value

    End Sub

    Protected Sub txttrandt_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txttrandt.SelectionChanged

        Dim StartDate As DateTime
        Dim trnDt As DateTime
        Dim span As TimeSpan
        Dim errFound As Boolean = False

        If Not IsDate(txttrandt.SelectedDate.ToString) Then ' TODO - is this check needed, there is no way to enter the date - it comes from a calendar
            If Request("LEAD") = "TRUE" Then
                lbl_warnings.Text = "You've entered an invalid follow-up date."
            Else
                lbl_warnings.Text = "You've entered an invalid transaction date."
            End If
            errFound = True
        End If

        trnDt = FormatDateTime(txttrandt.SelectedDate.ToString, 2)

        If (Not errFound) Then
            StartDate = FormatDateTime(Today.Date, 2)
            span = StartDate.Subtract(trnDt)

            If span.Days > 0 And Request("LEAD") = "TRUE" Then
                lbl_warnings.Text = "Your follow-up date must be greater than or equal to the current date."
                errFound = True
            End If
        End If

        If (Not errFound) Then
            ' cannot enter anything before store closed date 
            If Request("LEAD") <> "TRUE" Then
                Dim store As StoreData = theSalesBiz.GetStoreInfo(cboStores.Value.ToString.Trim)
                ' choose to not reset min date for every store change
                ValidateWrStoreNotClosed(store)

                If lbl_warnings.Text.isNotEmpty Then
                    errFound = True
                End If
            End If
        End If
        ' errors didn't show because it didn't get here
        If errFound Then
            If lbl_warnings.Text & "" <> "" Then lbl_warnings.Visible = True
            Exit Sub
        End If

        Session("tran_dt") = txttrandt.SelectedDate.ToString
    End Sub

    ''' <summary>
    ''' validate the transaction date and the written store and make sure the navigation in the
    ''' master page is available or not depending on the current status
    ''' </summary>
    Private Sub ValidateWrStoreNotClosed(ByVal storeInfo As StoreData)

        ' SAL_LNKS_DISABLED = Y - means that sale links are disabled and unable to navigate forward out of the startup; 
        ' SAL_LNKS_DISABLED = N - means that sale links are enabled  
        ' checking this session variable allows us to avoid redirecting if not necessary
        ' if was disabled and now want to enable, must redirect or icons won't be available
        ' if was enabled and want to disable, redirect to make that happen

        lbl_warnings.Text = theSalesBiz.ValidateStoreClosedDate(cboStores.Value, FormatDateTime(txttrandt.SelectedDate.ToString, 2), storeInfo)

        If lbl_warnings.Text.isNotEmpty Then

            ' if sale links have not been disabled, then disable them
            If Session("SAL_LNKS_DISABLED") = "N" Then

                Session("tran_dt") = txttrandt.SelectedDate.ToString
                Response.Redirect("Startup.aspx") ' if the date has now become valid, need to make all navigation available 
            End If

            ' if sale links have been disabled, then re-enable them
        ElseIf Session("SAL_LNKS_DISABLED") = "Y" Then

            Session("tran_dt") = txttrandt.SelectedDate.ToString
            Response.Redirect("Startup.aspx") ' if the date has now become valid, need to make all navigation available 
        End If
    End Sub

    ''' <summary>
    ''' if the input customer code is empty, then clear related session zip variables; if there is a 
    '''  customer code, the if there is a customer postal code, then return that cust postal code;
    '''  used as part of process to reset session variables   (?????)
    ''' </summary>
    ''' <param name="custCd">code for the customer in processing or blank if have none</param>
    ''' <param name="custZipCd">customer zip code if processing customer</param>
    ''' <returns>if passed a customer and postal code, then will return the customer postal code entered</returns>
    Private Function GetnSetCustZipCd(ByVal custCd As Object, ByVal custZipCd As Object) As String

        Dim rtnVal As String = ""

        If SystemUtils.isEmpty(custCd) Then

            Session("ZIP_CD") = Nothing
            Session("cust_zip") = Nothing
            rtnVal = ""

        ElseIf Not SystemUtils.isEmpty(custZipCd) Then

            rtnVal = UCase(custZipCd)
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' sets the pickup/delivery postal code and the pickup/delivery/inventory zone code session variables
    '''   and page values based on standard logic and the existing entries available; if the zone code is set for 
    '''   a delivery, then it is possible that the pickup/delivery store code will change to the delivery store
    '''   code on the zone
    ''' </summary>
    Private Sub SetPickupDeliveryZipNZone()

        If cboPD.Value <> "" AndAlso cboStores.Value <> "" Then

            Dim reqZoneCd As Boolean = theInvBiz.RequireZoneCd(cboPD.Value.ToString().ToUpper, SessVar.isArsEnabled, SessVar.ordTpCd)

            lblPostalCd.Visible = reqZoneCd
            txtPostalCd.Visible = reqZoneCd
            If SystemUtils.isEmpty(Session("CUST_CD")) Then

                txtPostalCd.Enabled = True
            Else
                txtPostalCd.Enabled = False
            End If

            If reqZoneCd Then

                Dim custZipCd As String = GetnSetCustZipCd(Session("CUST_CD"), Session("cust_ZIP"))
                Dim puDelZip As String = theTranBiz.GetPuDelZip(cboPD.Value.ToString().ToUpper(), cbo_pd_store_cd.Value.ToString.Trim, custZipCd, Session("ZIP_CD"))

                txtPostalCd.Text = puDelZip
                SessVar.zipCode4Zone = puDelZip
                SessVar.puDelStoreCd = cbo_pd_store_cd.Value.ToString.Trim

                Dim zoneFromZipResp As ZoneFromZipResp = theTranBiz.GetZoneCd4Zip(SessVar.zipCode4Zone)
                SessVar.zoneCd = zoneFromZipResp.zoneCd
                SessVar.ZoneDes = zoneFromZipResp.zoneCd + " - " + zoneFromZipResp.zoneDes
                ' if it's a delivery, need to reset the store code now that have the zone code to delivery store on the zone
                If cboPD.Value.ToString().ToUpper().isNotEmpty AndAlso cboPD.Value.ToString().ToUpper() = AppConstants.DELIVERY AndAlso zoneFromZipResp.zoneCd.isNotEmpty Then

                    Dim puDelStore As String = theTranBiz.GetPuDelStore(cboStores.Value, cboPD.Value.ToString().ToUpper(), zoneFromZipResp.zoneCd)
                    If puDelStore.isNotEmpty Then
                        SessVar.puDelStoreCd = puDelStore
                        cbo_pd_store_cd.Value = puDelStore
                        ' TODO future - if delivery then probably not reasonable to access pickup/del store and should probably be same here
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub txtPostalCd_TextChanged(sender As Object, e As EventArgs) Handles txtPostalCd.TextChanged

        Dim x As String = txtPostalCd.Text

        Dim hadCustDelZipCd As Boolean = (Not IsNothing(Session("PD")) AndAlso Session("PD").toupper().Equals("D")) And Session("CUST_CD") IsNot Nothing
        Dim wrStoreObj As StoreData = theSalesBiz.GetStoreInfo(cboStores.Value.ToString.Trim)

        ' TODO DSA - why are we letting this go blank - if they clear it and it's required, then we re-default
        If theInvBiz.RequireZoneCd(cboPD.Value.ToString().ToUpper(), SessVar.isArsEnabled, SessVar.ordTpCd) Then

            Dim custZipCd As String = GetnSetCustZipCd(Session("CUST_CD"), Session("cust_ZIP"))
            Dim puDelZip As String = theTranBiz.GetPuDelZip(cboPD.Value.ToString().ToUpper(),
                                cbo_pd_store_cd.Value.ToString.Trim, custZipCd, SessVar.aZipCd
                                )
            SessVar.zipCode4Zone = puDelZip
            Dim zoneFromZipResp As ZoneFromZipResp = theTranBiz.GetZoneCd4Zip(SessVar.zipCode4Zone)
            SessVar.zoneCd = zoneFromZipResp.zoneCd
            SessVar.ZoneDes = zoneFromZipResp.zoneCd + " - " + zoneFromZipResp.zoneDes
            ' if it's a delivery, need to reset the store code now that have the zone code
            Dim puDelStore As String = ""
            If cboPD.Value.ToString().ToUpper().isNotEmpty AndAlso cboPD.Value.ToString().ToUpper() = AppConstants.DELIVERY AndAlso
                zoneFromZipResp.zoneCd.isNotEmpty Then

                puDelStore = theTranBiz.GetPuDelStore(cboStores.Value, cboPD.Value.ToString().ToUpper(), zoneFromZipResp.zoneCd)
            End If
            If puDelStore.isNotEmpty Then
                SessVar.puDelStoreCd = puDelStore
            End If
        End If
        If SessVar.zipCode4Zone.isEmpty AndAlso
            Not String.IsNullOrEmpty(txtPostalCd.Text.Trim()) Then

            SessVar.zipCode4Zone = UCase(txtPostalCd.Text)
        Else
            txtPostalCd.Text = SessVar.zipCode4Zone
        End If
    End Sub

    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated
        calculate_total(Page.Master)
        PopupTax.ShowOnPageLoad = False
    End Sub

    ''' <summary>
    ''' Check  for discount applied is expired or not
    ''' </summary>
    Private Sub CheckforExpDiscount()
        Dim discLn As DataTable = theSalesBiz.GetDiscountsAppliedForSOCreation(Session.SessionID.ToString().Trim)
        Dim strWhereClause As String = String.Empty
        Dim dtLnsFiltered As DataRow()
        Dim lstDiscCdExp As New ArrayList()
        Dim lstDiscCdNotExp As New ArrayList()
        Dim strDiscountCd As String = String.Empty
        If (discLn.Rows.Count > 0) Then
            strWhereClause = ("DISC_EXP = 'Y'")
            If discLn.Select(strWhereClause).Length > 0 Then
                dtLnsFiltered = discLn.Select(strWhereClause)
                For Each discrw As DataRow In dtLnsFiltered
                    If Not lstDiscCdExp.Contains(discrw.Item("DISC_CD")) Then
                        lstDiscCdExp.Add(discrw("DISC_CD"))
                    End If
                Next
            End If
            strWhereClause = ("DISC_EXP = 'N'")
            If discLn.Select(strWhereClause).Length > 0 Then
                dtLnsFiltered = discLn.Select(strWhereClause)
                Dim discCode As String = dtLnsFiltered(0)("DISC_CD").ToString
                SessVar.discCd = If(SysPms.allowMultDisc, SysPms.multiDiscCd, discCode)
            End If

        End If
        If lstDiscCdExp.Count > 0 Then
            Dim result As Boolean = theSalesBiz.DeleteTempLnDiscount(Session.SessionID.ToString().Trim, lstDiscCdExp)
            'Dim errorMsg As String = String.Format(Resources.POSMessages.MSG0048, String.Join(",", dictCurrentPrice.Select(Function(pair) String.Format("{0}", pair.Key)).ToArray()))
            Dim errorMsg As String = String.Format(Resources.POSErrors.ERR0039, String.Join(",", lstDiscCdExp.ToArray()))
            If result Then
                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)

                lbl_Error.Text = errorMsg
            End If
        End If
    End Sub

    ''' <summary>
    ''' Recalculates the discounts (when applicable)
    ''' </summary>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">the ID for the line that triggered this event</param>
    Private Sub ReapplyDiscounts(ByVal action As ReapplyDiscountRequestDtc.LnChng, ByVal lnSeq As String)

        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd")) Then

            Dim discRequest As ReapplyDiscountRequestDtc
            discRequest = New ReapplyDiscountRequestDtc(action, Session("pd"), Session.SessionID.ToString.Trim, lnSeq)
            Dim discResponse As Boolean = theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest)
            If Not discResponse Then
                Dim discLn As DataTable = theSalesBiz.GetDiscountsAppliedForSOCreation(Session.SessionID.ToString().Trim)
                If discLn.Rows.Count = 0 Then
                    theSalesBiz.ReapplyDiscountForRelConvSOE(Session.SessionID.ToString.Trim)
                End If
            End If
        End If
        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub


    ''' <summary>
    ''' Set the session value to true when user leaves out of startup screen.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()>
    Public Shared Function SetSessionIsFirstVisit() As Boolean
        HttpContext.Current.Session("isFirstVisit") = True
        Return True
    End Function

    ''' <summary>
    ''' Check weather to call the calculate method or not. 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckCalculateTotal() As Boolean
        If (AppConstants.Order.TYPE_SAL.Equals(Session("ord_tp_cd")) AndAlso Not IsNothing(HttpContext.Current.Session("isFirstVisit"))) OrElse Not AppConstants.Order.TYPE_SAL.Equals(Session("ord_tp_cd")) OrElse Request("restore_saved_order") & "" <> "" OrElse Not IsNothing(Session("Converted_REL_NO")) Then
            'Return true in 1.Re-visited statup page or order type is not sale i.e(MCR,MDB,CRM),or save order is converted to order or relationship is converted sale.
            Return True
        Else
            Return False
        End If
    End Function
    'Alice added on May 17, 2019 for request 81
    Private Function GetCompanyCode(ByVal p_usr As String) As String


        Dim v_str As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT co_cd from store where store_cd in (select home_store_cd FROM EMP " &
                            " where emp_cd = '" & p_usr & "')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_str = dbReader.Item("co_cd")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            v_str = String.Empty
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return v_str

    End Function

    'Alice added on May 22, 2019 for request 81
    Private Function GetDeptCode(ByVal p_usr As String) As String


        Dim v_str As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT SLSP_DEPT_CD from EMP_SLSP where emp_cd = '" & p_usr & "'"


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_str = dbReader.Item("SLSP_DEPT_CD")
            Else
                Return ""
            End If

        Catch ex As Exception
            v_str = String.Empty

        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return v_str

    End Function

    ''' <summary>
    ''' Alice added on Jun 25, 2019 for request 81
    ''' For the Brick CRM/MCR/MDB  transaction,find out if original order has sale person with EMP_SLSP.SLSP_DEPT_CD = 'TERM' 
    ''' </summary>
    ''' <param name="delDocNum">the document number to find</param>

    Public Function IsSalePersonWithTerm(ByVal delDocNum As String, ByVal SalePersonNum As Integer) As Boolean


        Dim existsInDB As Boolean = False
        Dim dbSql As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        If SalePersonNum = 1 Then
                dbSql = "select * from so WHERE so_emp_slsp_cd1 in (select emp_cd from emp_slsp where slsp_dept_cd='TERM') and del_doc_num=:delDocNum"
            Else
                dbSql = "select * from so WHERE so_emp_slsp_cd2 in (select emp_cd from emp_slsp where slsp_dept_cd='TERM') and del_doc_num=:delDocNum"
            End If
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                existsInDB = True
            End If

        Catch ex As Exception
            existsInDB = False

        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return existsInDB

    End Function
End Class
