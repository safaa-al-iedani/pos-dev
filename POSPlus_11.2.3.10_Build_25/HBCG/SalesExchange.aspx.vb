Imports System.Collections.Generic
Imports System.Linq
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports TaxUtils
Imports SalesUtils
Imports DevExpress.Web

Partial Class SalesExchange
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private gPointCnt As String = "0"
    Dim SecurityCache As New System.Collections.Generic.Dictionary(Of String, Boolean)
    'Private remainingPointOrStop As String = "0"
    Private Exmpt As String = String.Empty

    Private Class DelDtGrid

        Public Const DEL_DT As Integer = 0
        Public Const DAY As Integer = 1
        Public Const DEL_STOPS As Integer = 2
        Public Const ACTUAL_STOPS As Integer = 3
        Public Const REMAINING_STOPS As Integer = 4
        Public Const SELECT_BUTTON As Integer = 5
    End Class

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim co_grp_cd As String = Session("str_co_grp_cd")

        If txt_del_doc_num.Text = String.Empty AndAlso (ddlReturnReason.SelectedIndex = 0 OrElse ddlReturnReason.Enabled = False) Then
            lblErrorMessage.Visible = True
            lbl_error.Text = ""
            lblErrorMessage.Text = Resources.POSErrors.ERR0061
            Exit Sub
        ElseIf ddlReturnReason.SelectedIndex = 0 OrElse ddlReturnReason.Enabled = False Then
            lblErrorMessage.Visible = True
            lbl_error.Text = ""
            lblErrorMessage.Text = Resources.POSErrors.ERR0038
            ddlReturnReason.Focus()
            Exit Sub
        ElseIf txt_del_doc_num.Text = String.Empty Then
            lblErrorMessage.Visible = True
            lbl_error.Text = ""
            lblErrorMessage.Text = Resources.POSErrors.ERR0060
            txt_del_doc_num.Focus()
            Exit Sub
            'MCCL security validate -- Alice added on Jan 08, 2019 for request 2260
        ElseIf Not theSalesBiz.Validate_MCCL_SO_Access(txt_del_doc_num.Text.Trim) Then
            lblErrorMessage.Visible = True
            lbl_error.Text = ""
            lblErrorMessage.Text = Resources.SalesExchange.Label15
            txt_del_doc_num.Focus()
            Exit Sub

        ElseIf btn_submit.Enabled = True Then
            pnlExchange.Enabled = True
            Populate_Results()
            If lbl_cust_cd.Text & "" <> "" Then
                btn_Clear.Visible = True
                btn_Save.Visible = True
                cboOrdSrt.Enabled = True
            End If
        End If


        validateIsActivateCashier()

        If System.Web.HttpContext.Current.Session("store_cd") & "" = String.Empty Then
            System.Web.HttpContext.Current.Session("store_cd") = System.Web.HttpContext.Current.Session("HOME_STORE_CD")
        End If

        Dim store_cd As String = System.Web.HttpContext.Current.Session("store_cd")

        If co_grp_cd = "BRK" And Not IsValidHouseSalesPerson(store_cd) Then  'Alice added on Feb 21, 2019 for request 4477

            lblErrorMessage.Visible = True
            lbl_error.Text = ""
            lblErrorMessage.Text = Resources.SalesExchange.Label68
            txt_del_doc_num.Focus()
            Exit Sub

        End If


        If Not String.IsNullOrEmpty(txt_store_cd.Text) AndAlso Not txt_store_cd.Text.Equals(Session("store_cd").ToString.Trim) AndAlso hidIsZoneSelected.Value.Equals("False") AndAlso hidIsCashierActivated.Value.Equals("True") Then
            hidIsZoneSelected.Value = True
        End If
    End Sub

    'Alice added on Feb 21, 2019 for request 4477
    Private Function IsValidHouseSalesPerson(ByVal store_cd As String) As Boolean
        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) as count from EMP_SLSP where EMP_CD = 'HOU'|| '" & store_cd & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("count")
            End If
        Catch
            v_value = 0
        End Try

        If v_value > 0 Then
            Return True
        Else
            Return False
        End If

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

    End Function


    ''' <summary>
    ''' checks if cashier is activated
    ''' </summary>
    Private Sub validateIsActivateCashier()

        ActivateCashier(Session("emp_cd"))
        If System.Web.HttpContext.Current.Session("store_cd") & "" = String.Empty Then
            System.Web.HttpContext.Current.Session("store_cd") = System.Web.HttpContext.Current.Session("HOME_STORE_CD")
        End If
        If SysPms.isCashierActiv Then
            validateCashier()
        ElseIf Not Session("store_cd") = Nothing And Not Session("tran_dt") = Nothing Then
            Dim msg As String = theSalesBiz.ValidateStoreClosedDate(Session("store_cd"), Session("tran_dt"))
            If Not String.IsNullOrEmpty(msg) Then
                lbl_error.Text = msg
                btn_Save.Enabled = False
                hidIsCashierActivated.Value = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' validates cashier post date.
    ''' </summary>
    Private Sub validateCashier()

        If Session("csh_act_init") = "INVALIDCASHIER" Then
            lbl_error.Text = Resources.POSErrors.ERR0054
        ElseIf FormatDateTime(Session("tran_dt")) < Today.Date.AddDays(-SysPms.numDaysPostDtTrans) Then
            Session("csh_act_init") = "INVALIDCASHIER"
            lbl_error.Text = Resources.POSErrors.ERR0056
        ElseIf FormatDateTime(Session("tran_dt")) > Today.Date.AddDays(SysPms.numDaysPostDtTrans) Then
            Session("csh_act_init") = "INVALIDCASHIER"
            lbl_error.Text = Resources.POSErrors.ERR0057
        End If
        If Session("csh_act_init") = "INVALIDCASHIER" Then
            btn_Save.Enabled = False
            hidIsCashierActivated.Value = False
        End If
    End Sub


    ''' <summary>
    ''' Populates the Return Cause Code(s) in to drop downlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateReturnCauseCodes()
        Dim returnCauseCodes As DataTable = theSalesBiz.GetCRMCauseCodes()
        If (Not IsNothing(returnCauseCodes)) AndAlso (returnCauseCodes.Rows.Count > 0) Then
            With ddlReturnReason
                .DataSource = returnCauseCodes
                .ValueField = "CRM_CAUSE_CD"
                .TextField = "FULL_DESC"
                .DataBind()
            End With
            lblErrorMessage.Visible = False
            ddlReturnReason.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.SalesExchange.Label17, ""))
            ddlReturnReason.Items.FindByText(Resources.SalesExchange.Label17).Value = ""
        Else
            ddlReturnReason.SelectedIndex = 0
            ddlReturnReason.Enabled = False
            lblErrorMessage.Visible = True
            lblErrorMessage.Text = Resources.POSMessages.MSG0040
        End If
    End Sub

    Private Sub PopulateOrderSortCodes()

        Dim objSal As New SalesBiz

        Dim ds As DataSet = objSal.GetOrderSortCodes()

        With cboOrdSrt
            .DataSource = ds
            .ValueField = "ord_srt_cd"
            .TextField = "full_desc"
            .DataBind()
        End With


        'Alice updated on Jan 07, 2018 for request 2260
        If Session("ord_srt_eex") & "" <> "" Then
            cboOrdSrt.Value = Session("ord_srt_eex")
        Else
            cboOrdSrt.Value = "EXN"
            Session("ord_srt_eex") = "EXN"
        End If

        cboOrdSrt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Sort Code", ""))
        cboOrdSrt.Items.FindByText("Please Select A Sort Code").Value = ""
        If Session("ord_srt_eex") & "" = "" And cboOrdSrt.Value & "" = "" Then
            cboOrdSrt.SelectedIndex = 0
        End If

        If Session("ord_srt_eex") & "" = "" And cboOrdSrt.SelectedIndex > 0 Then
            Session("ord_srt_eex") = cboOrdSrt.Value
        End If

    End Sub

    Private Sub bindStates()
        Dim dsStates As DataSet = Nothing
        dsStates = HBCG_Utils.GetStates()
        With cbo_State
            .DataSource = dsStates
            .DataValueField = "st_cd"
            .DataTextField = "full_desc"
            .DataBind()
        End With

        With cboShipToState
            .DataSource = dsStates
            .DataValueField = "st_cd"
            .DataTextField = "full_desc"
            .DataBind()
        End With


        cbo_State.Items.Insert(0, " ")
        cbo_State.Items.FindByText(" ").Value = ""
        cboShipToState.Items.Insert(0, " ")
        cboShipToState.Items.FindByText(" ").Value = ""
    End Sub

    Private Sub bindCustomerDetails(ByVal odr As OracleDataReader)
        bindStates()
        ''Customer Billing Information from Cust table
        txt_corp_name.Text = odr.Item("BILL_TO_CORP").ToString
        txt_f_name.Text = odr.Item("BILL_TO_F_NAME").ToString
        txt_l_name.Text = odr.Item("BILL_TO_L_NAME").ToString
        txt_addr1.Text = odr.Item("BILL_TO_ADDR1").ToString
        txt_addr2.Text = odr.Item("BILL_TO_ADDR2").ToString
        txt_h_phone.Text = odr.Item("BILL_TO_H_PHONE").ToString
        txt_b_phone.Text = odr.Item("BILL_TO_B_PHONE").ToString
        txt_city.Text = odr.Item("BILL_TO_CITY").ToString
        cbo_State.SelectedValue = odr.Item("BILL_TO_ST_CD").ToString
        txt_zip.Text = odr.Item("BILL_TO_ZIP_CD").ToString
        txt_email.Text = odr.Item("EMAIL_ADDR").ToString

        ''Customer Shipping Information from SO table
        txtShippingCorpName.Text = odr.Item("SHIP_TO_CORP").ToString
        txtShipToFName.Text = odr.Item("SHIP_TO_F_NAME").ToString
        txtShipToLname.Text = odr.Item("SHIP_TO_L_NAME").ToString
        txtShipToPO.Text = odr.Item("ORDER_PO_NUM").ToString
        txtShipToAdd1.Text = odr.Item("SHIP_TO_ADDR1").ToString
        txtShipToAdd2.Text = odr.Item("SHIP_TO_ADDR2").ToString
        txtShipToHomePhone.Text = odr.Item("SHIP_TO_H_PHONE").ToString
        txtShipToAlternatePhone.Text = odr.Item("SHIP_TO_B_PHONE").ToString
        txtShipToCity.Text = odr.Item("SHIP_TO_CITY").ToString
        cboShipToState.SelectedValue = odr.Item("SHIP_TO_ST_CD").ToString
        txtShipToZip.Text = odr.Item("SHIP_TO_ZIP_CD").ToString
        txtShipToEmail.Text = odr.Item("EMAIL_ADDR").ToString

    End Sub

    Private Sub Populate_Results()

        Dim sql As String
        Dim cmd As OracleCommand
        Dim ds As DataSet
        ds = New DataSet
        Dim MyDataReader As OracleDataReader
        Dim SO_DOC_NUM As String = ""

        txt_del_doc_num.Enabled = True
        btn_submit.Visible = True

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        'Alice add TED_CD in the below sql query for tax exempt sales on Mar 08, 2019
        sql = "Select SO.*, SO.TET_CD as so_tet_cd, CUST.EMAIL_ADDR, CUST.TET_ID#, tax_method, tax.des as tax_des, s.zip_cd AS store_zip, " &
                " CUST.CORP_NAME AS BILL_TO_CORP,  " &
                " CUST.FNAME AS BILL_TO_F_NAME,  " &
                " CUST.LNAME AS BILL_TO_L_NAME,  " &
                " CUST.ADDR1 AS BILL_TO_ADDR1,  " &
                " CUST.ADDR2 AS BILL_TO_ADDR2,  " &
                " CUST.HOME_PHONE AS BILL_TO_H_PHONE,  " &
                " CUST.BUS_PHONE AS BILL_TO_B_PHONE,  " &
                " CUST.CITY AS BILL_TO_CITY,  " &
                " CUST.ST_CD AS BILL_TO_ST_CD,  " &
                " CUST.ZIP_CD AS BILL_TO_ZIP_CD  " &
                "FROM CUST, tax_cd tax, store s, SO " &
                "WHERE DEL_DOC_NUM= :docNum AND STAT_CD='F' AND ORD_TP_CD='SAL' AND SO.CUST_CD = CUST.CUST_CD " &
                "AND S.STORE_CD =  SO.PU_DEL_STORE_CD " &
                " AND SO.tax_cd = tax.tax_cd (+) "

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Clear()
        cmd.Parameters.Add(":docNum", OracleType.VarChar)
        cmd.Parameters(":docNum").Value = UCase(txt_del_doc_num.Text)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.Read) Then
                lbl_error.Text = String.Empty
                pnlExchange.Enabled = True
                SO_DOC_NUM = MyDataReader.Item("SO_DOC_NUM").ToString

                'Alice added for tax exempt sales on Mar 08, 2019, start
                Session("tet_cd") = MyDataReader.Item("so_tet_cd").ToString
                txt_exempt_id.Text = MyDataReader.Item("TET_ID#").ToString
                'Alice added for tax exempt sales on Mar 08, 2019, end

                txt_del_doc_num.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                txt_del_doc_num.Enabled = False
                txt_del_doc_existing.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                txt_del_doc_existing.Enabled = False
                txt_ord_tp_cd.Text = MyDataReader.Item("ORD_TP_CD").ToString
                txt_tax_cd.Text = MyDataReader.Item("TAX_CD").ToString
                lbl_tax_cd.Text = txt_tax_cd.Text
                lnk_origsale_TaxCode.Text = txt_tax_cd.Text
                lbl_fname.Text = MyDataReader.Item("SHIP_TO_F_NAME").ToString
                lbl_lname.Text = MyDataReader.Item("SHIP_TO_L_NAME").ToString
                txt_zone_cd.Text = MyDataReader.Item("SHIP_TO_ZONE_CD").ToString
                txtOriginalZone.Text = MyDataReader.Item("SHIP_TO_ZONE_CD").ToString
                txtNewZone.Text = txt_zone_cd.Text
                cbo_PD.SelectedValue = MyDataReader.Item("PU_DEL").ToString
                '  cboOrdSrt.Value = MyDataReader.Item("ORD_SRT_CD").ToString
                hidWrittenStoreCode.Value = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                txtTaxCd.Text = MyDataReader.Item("TAX_CD").ToString
                ' delivery charge and setup charges should default to what is assigned in the zone code 

                If (cbo_PD.SelectedValue.Equals(AppConstants.DELIVERY)) Then
                    If MyDataReader.Item("DEL_CHG") & "" <> "" Then
                        txtOriginalDelChgs.Text = FormatNumber(MyDataReader.Item("DEL_CHG").ToString, 2)
                    Else
                        txtOriginalDelChgs.Text = FormatNumber(0, 2)
                    End If
                    txt_del_chg.Enabled = True
                    txtNewDeliveryChgs.Enabled = True
                Else
                    txtOriginalDelChgs.Text = FormatNumber(0, 2)
                    txt_del_chg.Enabled = False
                    txtNewDeliveryChgs.Enabled = False
                End If

                If MyDataReader.Item("SETUP_CHG") & "" <> "" Then
                    txtOriginalSetupChgs.Text = FormatNumber(MyDataReader.Item("SETUP_CHG").ToString, 2)
                Else
                    txtOriginalSetupChgs.Text = FormatNumber(0, 2)
                End If

                txt_setup.Text = FormatNumber(0, 2)
                txt_del_chg.Text = FormatNumber(0, 2)

                txtNewSetupChg.Text = FormatNumber(0, 2)
                txtNewDeliveryChgs.Text = FormatNumber(0, 2)
                'If it is PICKUP then no need to check the security and disable the delivery charge field
                If (cbo_PD.SelectedValue.Equals(AppConstants.DELIVERY)) Then
                    If CheckSecurity(SecurityUtils.SOM_DEL_CHG, Session("EMP_CD")) AndAlso AppConstants.DELIVERY.Equals(cbo_PD.SelectedValue) Then
                        'txt_del_chg.Enabled = True
                    Else
                        txt_del_chg.Enabled = False
                        txtNewDeliveryChgs.Enabled = False
                    End If
                Else
                    txt_del_chg.Enabled = False
                    txtNewDeliveryChgs.Enabled = False
                End If

                If CheckSecurity(SecurityUtils.SOM_SETUP_CHG, Session("EMP_CD")) Then
                    txt_setup.Enabled = True
                    txtNewSetupChg.Enabled = True
                Else
                    txt_setup.Enabled = False
                    txtNewSetupChg.Enabled = False
                End If

                txt_zip_cd.Text = MyDataReader.Item("SHIP_TO_ZIP_CD").ToString
                lbl_cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                txt_store_cd.Text = MyDataReader.Item("SO_STORE_CD").ToString
                txt_new_pd_store_cd.Text = txt_store_cd.Text
                '***** set if store is ARS enabled
                Dim store As StoreData = theSalesBiz.GetStoreInfo(txt_store_cd.Text)
                Session("STORE_ENABLE_ARS") = store.enableARS

                cbo_pd_date.MinDate = Today.Date
                cbo_pd_date.Value = Today.Date
                hidOriginalDate.Value = MyDataReader.Item("PU_DEL_DT").ToString
                If (cbo_PD.SelectedValue.Equals("P")) Then
                    cbo_pd_date.Enabled = True
                    If (SysPms.isPriorityFillByZone) Then
                        SetZoneCodeVisibility(True)
                    Else
                        SetZoneCodeVisibility(False)
                    End If
                Else
                    'For DELIVERY
                    'E1 SYSPM �Sales 13. Use delivery maintenance in sales order entry?�  
                    If (SysPms.isDelvSchedEnabled) Then
                        SetZoneSelectionVisibility(True)
                        SetZoneCodeVisibility(True)
                    Else
                        SetZoneSelectionVisibility(False)
                        SetZoneCodeVisibility(True)
                    End If
                End If

                txt_store_cd.Enabled = False

                Dim theSoHdrInfo As SoHdrExchangeInfo = New SoHdrExchangeInfo()
                theSoHdrInfo.writtenDt = MyDataReader("SO_WR_DT").ToString
                theSoHdrInfo.delChgTaxOnOrig = 0.0    ' txt_del_chg.Text
                theSoHdrInfo.setupChgTaxOnOrig = 0.0  ' txt_setup.Text
                theSoHdrInfo.delChgTaxOnNew = 0.0    ' txt_del_chg.Text
                theSoHdrInfo.setupChgTaxOnNew = 0.0  ' txt_setup.Text
                theSoHdrInfo.taxCd = txt_tax_cd.Text
                theSoHdrInfo.slsp1 = MyDataReader("SO_EMP_SLSP_CD1")
                theSoHdrInfo.slspPct1 = CDbl(MyDataReader("PCT_OF_SALE1"))
                theSoHdrInfo.slsp2 = MyDataReader("SO_EMP_SLSP_CD2") + ""
                If IsDBNull(MyDataReader("PCT_OF_SALE2")) OrElse (MyDataReader("PCT_OF_SALE2").ToString + "").isEmpty Then
                    theSoHdrInfo.slspPct2 = 0
                Else
                    theSoHdrInfo.slspPct2 = CDbl(MyDataReader("PCT_OF_SALE2"))
                End If

                txt_finance.Text = MyDataReader.Item("ORIG_FI_AMT").ToString
                txt_total_finance.Text = MyDataReader.Item("ORIG_FI_AMT").ToString
                If Not IsNumeric(MyDataReader.Item("ORIG_FI_AMT").ToString) Then
                    txt_finance.Enabled = False
                    txt_finance.Text = "0.00"
                Else
                    txt_finance.Enabled = True
                    If CDbl(MyDataReader.Item("ORIG_FI_AMT").ToString) = 0 Then
                        txt_finance.Enabled = False
                    End If
                End If

                cmd.Parameters.Clear()

                If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Or ("L".Equals(ViewState("TaxMethod"))) Then
                    sql = "SELECT NVL(SUM(CUST_TAX_CHG),0) AS CUST_TAX_CHG FROM SO_LN " &
                        "WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "' AND VOID_FLAG='N'"

                    cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                    Using Odr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)
                        If Odr.Read Then
                            If IsNumeric(Odr.Item("CUST_TAX_CHG").ToString) Then
                                txt_crm_taxes.Text = FormatNumber(CDbl(Odr.Item("CUST_TAX_CHG").ToString), 2)
                            Else
                                txt_crm_taxes.Text = FormatNumber(0, 2)
                            End If
                        Else
                            txt_crm_taxes.Text = FormatNumber(0, 2)
                        End If
                    End Using
                    If IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                        txt_crm_taxes.Text = CDbl(txt_crm_taxes.Text) + CDbl(MyDataReader.Item("TAX_CHG").ToString)
                    End If
                ElseIf IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                    txt_crm_taxes.Text = FormatNumber(MyDataReader.Item("TAX_CHG").ToString, 2)
                Else
                    txt_crm_taxes.Text = FormatNumber(0, 2)
                End If

                bindCustomerDetails(MyDataReader)
                Session("soHdrInfo") = theSoHdrInfo
            Else
                lbl_error.Text = Resources.SalesExchange.Label16
                conn.Close()
                Exit Sub
            End If

            MyDataReader.Close()
            cmd.Dispose()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

        PopulateTaxExempt()
        bindgrid()
        calculate_total(CalculateTotal.OriginalSale)
        btn_submit.Enabled = False
        lbl_old_or_new.Text = "NEW"
    End Sub

    ''' <summary>
    ''' Hide and Show of Pickup or Delivery date calendar
    ''' </summary>
    ''' <param name="isVisible">isVisible to show or hide the field</param>
    ''' <remarks></remarks>
    Private Sub SetZoneSelectionVisibility(ByVal isVisible As Boolean)
        btnDateLookup.Visible = isVisible
        cbo_pd_date.Enabled = Not isVisible
    End Sub

    ''' <summary>
    ''' This is will set the visibility of the Zone Code field on the screen
    ''' </summary>
    ''' <param name="isVisible">isVisible to show or hide the field</param>
    ''' <remarks></remarks>
    Private Sub SetZoneCodeVisibility(ByVal isVisible As Boolean)
        txt_zone_cd.Visible = isVisible
        lblZone.Visible = isVisible
        txtNewZone.Visible = isVisible
        lblNewZone.Visible = isVisible
        lbl_original_zone.Visible = isVisible
        txtOriginalZone.Visible = isVisible
    End Sub

    Public Sub bindgrid()
        Dim credDs As DataSet
        Dim salDs As DataSet
        Dim DEL_DOC_NUM As String
        Dim isFoundAndInProcess As Boolean = False
        Dim qty As Integer

        ' if we have already queried (and maybe even updated, then use the session variable, else query the data
        If Not Session("ORIG_SAL_LN") Is Nothing Then
            credDs = Session("ORIG_SAL_LN")

            If (credDs.Tables(0).Rows(0)(0).ToString() <> UCase(txt_del_doc_num.Text)) Or Request("RELATED_SO") & "" <> "" Then
                isFoundAndInProcess = False
                credDs.Clear()
                Session("ORIG_SAL_LN") = credDs

                If Not Session("SAL_LN") Is Nothing Then
                    salDs = Session("SAL_LN")
                    salDs.Clear()
                    Session("SAL_LN") = salDs
                End If
            Else
                isFoundAndInProcess = True
                credDs = Session("ORIG_SAL_LN")
                orig_Sale.DataSource = credDs
                orig_Sale.DataBind()

                salDs = Session("SAL_LN")
                'Removing the unselect warranties
                Dim drWarrs() As DataRow = salDs.Tables(0).Select("ITM_TP_CD = '" & AppConstants.Sku.TP_WAR & "'")
                Dim drFilterWarr() As DataRow = Nothing

                For Each drWarr As DataRow In drWarrs
                    qty = 0
                    drFilterWarr = salDs.Tables(0).Select("WARR_BY_LN = '" & drWarr("DEL_DOC_LN#") & "'")
                    If drFilterWarr.Length < 1 Then
                        'salDs.Tables(0).Rows.Remove(drWarr)
                        If Integer.TryParse(drWarr("QTY"), qty) AndAlso qty > 0 Then
                            qty = qty - 1
                        End If
                        'drWarr("QTY") = CDbl(drWarr("QTY")) - 1
                        drWarr("QTY") = qty

                        If CDbl(drWarr("QTY")) = 0 Then
                            drWarr("STORE_CD") = System.DBNull.Value
                            drWarr("LOC_CD") = System.DBNull.Value
                            drWarr("ITM_CD") = System.DBNull.Value
                            drWarr("UNIT_PRC") = 0.0
                            drWarr("ITM_SRT_DESC") = System.DBNull.Value
                        End If
                    End If
                Next

                grd_lines_new.DataSource = salDs
                grd_lines_new.DataBind()
            End If
        End If

        If isFoundAndInProcess = False Then
            credDs = New DataSet
            salDs = New DataSet
            DEL_DOC_NUM = UCase(txt_del_doc_num.Text)

            orig_Sale.DataSource = ""
            orig_Sale.DataBind()

            orig_Sale.Visible = True
            grd_lines_new.Visible = True

            credDs = theSalesBiz.GetOriginalItemsForExchange(DEL_DOC_NUM)
            salDs = credDs.Copy()
            'this new column will hold the retail price of the newly added item
            If Not salDs.Tables(0).Columns.Contains("OriginalRetailPrice") Then
                salDs.Tables(0).Columns.Add("OriginalRetailPrice", Type.GetType("System.String"))
                salDs.Tables(0).Columns.Add("IsApproved", Type.GetType("System.String"))
                salDs.Tables(0).Columns("IsApproved").DefaultValue = "Y"
            End If
            Session("ORIG_SAL_LN") = credDs
            Dim returnRowFound As Boolean = False

            If (Not IsNothing(credDs) AndAlso credDs.Tables(0).Rows.Count > 0) Then
                Dim dt As DataTable = credDs.Tables(0)
                Dim dr As DataRow
                Dim maxRetDatSet As DataSet = OrderUtils.getMaxCreditListByLn(DEL_DOC_NUM)
                Dim maxCred As New OrderUtils.MaxCredResponse
                ' 07/xx/13 DSA: set the qty available to return or exchange to the max of the original sale quantity minus returned quantity
                '     but since EEX's are created as SAL/CRM, have to add back in the sal side of the eex
                ' 08/08/13 DSA: by including the eex's, it is also including items that were added to outgoing (S that match itm_cd's on the original sale;
                '     Decided that the exchange should really just allow access to items that were not previously returned or exchanged;  if exchanged 
                '     items on the orig sale need further return or exchange, then they will have to be done against the new sale (portion of the exchange)
                For Each dr In dt.Rows
                    maxCred = OrderUtils.getLnMaxCredit(dr("itm_cd").ToString, dr("del_doc_ln#"), dr("unit_prc"), AppConstants.Order.TYPE_CRM, maxRetDatSet)
                    dr("UNIT_PRC") = maxCred.maxUnitRetail
                    dr("qty") = maxCred.maxQty
                    If dr("qty") > 0 Then
                        returnRowFound = True
                    End If
                Next

                ViewState("ItemTable") = dt
                Session("ORIG_SAL_LN") = credDs
                orig_Sale.DataSource = credDs
                orig_Sale.DataBind()

                dt = salDs.Tables(0)

                For Each dr In dt.Rows
                    dr("ITM_CD") = System.DBNull.Value
                    dr("QTY") = 0
                    dr("STORE_CD") = System.DBNull.Value
                    dr("LOC_CD") = System.DBNull.Value
                    dr("UNIT_PRC") = 0.0
                    dr("cust_tax_chg") = 0.0
                    dr("ITM_SRT_Desc") = String.Empty
                    dr("WARR_BY_LN") = System.DBNull.Value
                    dr("WARR_BY_SKU") = System.DBNull.Value
                    dr("WARR_BY_DOC_NUM") = System.DBNull.Value
                Next

                Session("SAL_LN") = salDs
                'Removing the unselect warranties
                Dim drWarrs() As DataRow = salDs.Tables(0).Select("ITM_TP_CD = '" & AppConstants.Sku.TP_WAR & "'")
                Dim drFilterWarr() As DataRow = Nothing

                For Each drWarr As DataRow In drWarrs
                    qty = 0
                    drFilterWarr = salDs.Tables(0).Select("WARR_BY_LN = '" & drWarr("DEL_DOC_LN#") & "'")
                    If drFilterWarr.Length < 1 Then
                        'salDs.Tables(0).Rows.Remove(drWarr)
                        If Integer.TryParse(drWarr("QTY"), qty) AndAlso qty > 0 Then
                            qty = qty - 1
                        End If
                        'drWarr("QTY") = CDbl(drWarr("QTY")) - 1
                        drWarr("QTY") = qty

                        If CDbl(drWarr("QTY")) = 0 Then
                            drWarr("STORE_CD") = System.DBNull.Value
                            drWarr("LOC_CD") = System.DBNull.Value
                            drWarr("ITM_CD") = System.DBNull.Value
                            drWarr("UNIT_PRC") = 0.0
                            drWarr("ITM_SRT_DESC") = System.DBNull.Value
                        End If
                    End If
                Next

                grd_lines_new.DataSource = salDs
                grd_lines_new.DataBind()
            Else
                orig_Sale.Visible = False
            End If
        End If
    End Sub
    Protected Function GetMovedQuantity(ByVal lineNo As String) As Double
        Dim drNew As DataRow
        Dim newLines As New DataSet
        newLines = Session("SAL_LN")
        Dim movedQuantity As Double = 0
        For Each drNew In newLines.Tables(0).Rows
            If drNew("DEL_DOC_LN#") = lineNo Then
                movedQuantity = drNew("QTY")
            End If
        Next
        Return movedQuantity
    End Function
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Session("ORIG_SAL_LN") = Nothing
        Session("SAL_LN") = Nothing
        Session("STORE_ENABLE_ARS") = False
        Session("soHdrInfo") = Nothing
        Response.Redirect("SalesExchange.aspx")

    End Sub

    Protected Sub cboOrdSrt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOrdSrt.SelectedIndexChanged

        If cboOrdSrt.Value <> "" Then
            Session("ord_srt_eex") = cboOrdSrt.Value
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            If Find_Security("POSEXC", Session("emp_cd")) = "N" Then
                txt_del_doc_num.Enabled = False
                btn_submit.Enabled = False
                btn_Save.Enabled = False
                btn_Clear.Enabled = False
                lbl_error.Text = Resources.SalesExchange.Label02
                Exit Sub
            End If
            If IsNothing(ViewState("CanModifyTax")) OrElse ViewState("CanModifyTax").ToString = String.Empty Then
                ViewState("CanModifyTax") = CheckSecurity(SecurityUtils.OVERRIDE_TAXCD_MGMT, Session("EMP_CD"))
            End If
            lbl_error.Text = ""
            cbo_pd_date.MinDate = Today.Date
            cbo_pd_date.MaxDate = "12/31/2049"
            PopulateOrderSortCodes()
            cboOrdSrt.Enabled = False

            If Request("RELATED_SO") & "" <> "" Then
                txt_del_doc_num.Text = Request("RELATED_SO")
            End If

            If Request("query_returned") = "Y" Then
                txt_del_doc_num.Text = Request("del_doc_num")
                pnlExchange.Enabled = True
                Populate_Results()
                If lbl_cust_cd.Text & "" <> "" Then
                    btn_Clear.Visible = True
                    btn_Save.Visible = True
                End If
            End If
            pnlExchange.Enabled = False
            popupMgrOverride.ShowOnPageLoad = False


        End If
    End Sub

    Private Function CRMPointsCalculation() As Integer
        Dim points As Integer
        If SysPms.crmAffectDelAvail Then
            points = 2
        Else
            points = 1
        End If
        Return points
    End Function


    ''' <summary>
    ''' Used this transient cache to avoid hitting the database multiple times for
    ''' every security and every line placed on the order, mostly during the
    ''' itemdatabound process
    ''' </summary>
    ''' <param name="key">the Security KEY that needs to be read</param>
    ''' <param name="emp">the Employee code to check security for</param>
    ''' <returns>TRUE, if user has the security indicated; FALSE otherwise</returns>
    Private Function CheckSecurity(key As String, emp As String) As Boolean
        Dim catKey As String = key + "~" + emp
        If Not SecurityCache.ContainsKey(catKey) Then
            SecurityCache.Add(catKey, SecurityUtils.hasSecurity(key, emp))
        End If
        Return SecurityCache(catKey)
    End Function


    Enum CalculateTotal
        NewSale
        CRM
        Both
        OriginalSale
    End Enum

    Public Sub calculate_total(Optional ByVal CalculateTotal As CalculateTotal = CalculateTotal.Both)

        Dim sql As String = ""
            Dim tax As Double = 0
            Dim theSoHdrInfo As SoHdrExchangeInfo = Nothing

            Dim taxCode As String = String.Empty
            If String.IsNullOrEmpty(lnkUpdatedTax.Text) Then
                taxCode = lnkTaxCode.Text.Trim
            Else
                taxCode = lnkUpdatedTax.Text.Trim
            End If

        'If cbo_tax_exempt.SelectedIndex = 0 AndAlso (Not String.IsNullOrWhiteSpace(lbl_tax_cd.Text)) Then
        Dim taxDt As DateTime = FormatDateTime(Today.Date, DateFormat.ShortDate)   ' for uneven new sale lines
            Dim credTaxDt As DateTime = FormatDateTime(Today.Date, DateFormat.ShortDate)  ' for the return/eex lines

            If Not IsNothing(Session("soHdrInfo")) Then

                theSoHdrInfo = CType(Session("soHdrInfo"), SoHdrExchangeInfo)

                If (Not String.IsNullOrWhiteSpace(theSoHdrInfo.writtenDt.ToString)) AndAlso IsDate(theSoHdrInfo.writtenDt.ToString) Then
                    credTaxDt = FormatDateTime(theSoHdrInfo.writtenDt, DateFormat.ShortDate)
                End If
            End If

            ' line taxing calculates the tax for each line and delivery and setup charges separately
            ' header taxing sums all the lines by item type and then calculates the tax by tax authority based on the total percent by itm_tp
            ' therefore, line processing below calculates the tax whereas header processing just accumulates until the end and then calls the calculation 

            Select Case CalculateTotal
                Case CalculateTotal.NewSale
                    SaleTotalCalculation(taxCode, credTaxDt, taxDt, theSoHdrInfo)
                Case CalculateTotal.CRM
                    CRMTotalCalculation(txt_tax_cd.Text, credTaxDt, taxDt, theSoHdrInfo)
                Case CalculateTotal.OriginalSale
                    OrigSaleTotalCalculation(txt_tax_cd.Text, credTaxDt, taxDt, theSoHdrInfo)
                    CRMTotalCalculation(txt_tax_cd.Text, credTaxDt, taxDt, theSoHdrInfo)
                    SaleTotalCalculation(taxCode, credTaxDt, taxDt, theSoHdrInfo)
                Case Else
                    CRMTotalCalculation(txt_tax_cd.Text, credTaxDt, taxDt, theSoHdrInfo)
                    SaleTotalCalculation(taxCode, credTaxDt, taxDt, theSoHdrInfo)
            End Select


        'End If

        If Not IsNumeric(txt_sal_grand.Text) Then txt_sal_grand.Text = "0.00"
            If Not IsNumeric(txt_crm_grand.Text) Then txt_crm_grand.Text = "0.00"
        If Not IsNumeric(txt_origsale_grand.Text) Then txt_origsale_grand.Text = "0.00" 'Alice added on Mar 4, 2019 

        If Not IsNumeric(txt_sal_subtotal.Text) Then 'Alice added on Mar 4, 2019 
            txt_sal_subtotal.Text = "0.00"
        End If

        If CDbl(txt_sal_subtotal.Text) = 0 Then
                txt_sal_taxes.Text = "0.00"
                txt_sal_grand.Text = "0.00"
            End If
            If txt_sal_taxes.Text & "" = "" Then txt_sal_taxes.Text = FormatNumber(0, 2)

            Session("soHdrInfo") = theSoHdrInfo
            txt_difference.Text = FormatNumber(CDbl(txt_origsale_grand.Text) - CDbl(txt_sal_grand.Text), 2)


    End Sub

    Private Sub OrigSaleTotalCalculation(ByVal taxCode As String,
                                ByVal credTaxDt As DateTime, ByVal taxDt As DateTime, ByRef theSoHdrInfo As SoHdrExchangeInfo)
        Dim ds As New DataSet
        Dim merch_total As Double = 0
        Dim tax As Double = 0
        Dim tmp_Amt As Double = 0.0
        Dim hdrTaxReq As New taxHdrCalcRequest
        Dim tax_pct As Double = 0
        Dim tmp_tax As Double

        ds = Session("ORIG_SAL_LN")
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow
        If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

            'Calculate Tax on Merchandise and Line Items
            tax_pct = TaxUtils.getTaxPct(taxCode, credTaxDt, "") ' FUTURE - diff itm_tp_cd's have potential for diff tax pcts, this is not OK
        End If  ' line tax

        For Each dr In ds.Tables(0).Rows

            If dr("VOID_FLAG") = "N" Then
                merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))
            End If
            If dr("VOID_FLAG").ToString = "N" AndAlso IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) Then
                ' TODO - someday this needs to be based on tax code tax method
                ' TODO - line taxing has to be based on written date andAlso based on whether tax auth taxes itm_tp for each line
                If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

                    'If LineInput tax method then
                    If TaxUtils.Determine_Taxability(dr("ITM_TP_CD"), credTaxDt.ToString, txt_tax_cd.Text) = "Y" Then

                        tax = CDbl(tax) + ((CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC"))) * tax_pct)

                    End If
                Else ' header tax method

                    tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic

                    TaxUtils.AccumTaxingAmts(dr("ITM_TP_CD").ToString + "", tmp_Amt, hdrTaxReq.itmTpAmts)
                End If
            End If
        Next

        If IsNumeric(txtOriginalDelChgs.Text) Then

            'Determine if delivery should be taxed
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax
                tmp_tax = getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txtOriginalDelChgs.Text), "DEL")
                tax = tax + tmp_tax
                theSoHdrInfo.delChgTaxOnOrig = tmp_tax.ToString
            Else ' if header
                TaxUtils.AccumTaxingAmts("DEL", CDbl(txtOriginalDelChgs.Text), hdrTaxReq.itmTpAmts)
            End If

        End If

        If IsNumeric(txtOriginalSetupChgs.Text) Then

            'Determine the tax-ability of the setup charges
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax
                'tax = tax + getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txt_setup.Text), "SETUP")
                tmp_tax = getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txtOriginalSetupChgs.Text), "SETUP")
                tax = tax + tmp_tax
                theSoHdrInfo.setupChgTaxOnOrig = tmp_tax.ToString

            Else ' header method
                TaxUtils.AccumTaxingAmts("SETUP", CDbl(txtOriginalSetupChgs.Text), hdrTaxReq.itmTpAmts)
                ' hdrTaxReq.itmTpAmts.setup = CDbl(lblSetup.Text)
            End If
        End If

        If "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
            ' if header tax
            hdrTaxReq.taxCd = txt_tax_cd.Text
            hdrTaxReq.taxDt = credTaxDt
            tax = TaxUtils.ComputeHdrTax(hdrTaxReq)
        End If

        txt_origsale_taxes.Text = FormatNumber(Math.Round(CDbl(tax), 2, MidpointRounding.AwayFromZero))
        txt_origsale_grand.Text = FormatNumber(Math.Round(CDbl(merch_total) + CDbl(txtOriginalSetupChgs.Text) + CDbl(txtOriginalDelChgs.Text) + CDbl(txt_origsale_taxes.Text), 2, MidpointRounding.AwayFromZero))
        txt_origsale_subtotal.Text = FormatNumber(Math.Round(CDbl(merch_total), 2, MidpointRounding.AwayFromZero))
    End Sub
    Private Sub CRMTotalCalculation(ByVal taxCode As String,
                                ByVal credTaxDt As DateTime, ByVal taxDt As DateTime, ByRef theSoHdrInfo As SoHdrExchangeInfo)

        Dim ds As New DataSet
        Dim merch_total As Double = 0
        Dim tax As Double = 0
        Dim tmp_Amt As Double = 0.0
        Dim hdrTaxReq As New taxHdrCalcRequest
        Dim tax_pct As Double = 0
        Dim tmp_tax As Double

        ds = Session("SAL_LN")
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow
        If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

            'Calculate Tax on Merchandise and Line Items
            tax_pct = TaxUtils.getTaxPct(taxCode, credTaxDt, "") ' FUTURE - diff itm_tp_cd's have potential for diff tax pcts, this is not OK
        End If  ' line tax

        For Each dr In ds.Tables(0).Rows

            If dr("VOID_FLAG") = "N" Then
                merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))
            End If
            If dr("VOID_FLAG").ToString = "N" AndAlso IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) Then
                ' TODO - someday this needs to be based on tax code tax method
                ' TODO - line taxing has to be based on written date andAlso based on whether tax auth taxes itm_tp for each line
                If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

                    'If LineInput tax method then
                    If TaxUtils.Determine_Taxability(dr("ITM_TP_CD"), credTaxDt.ToString, txt_tax_cd.Text) = "Y" Then

                        tax = CDbl(tax) + ((CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC"))) * tax_pct)

                    End If
                Else ' header tax method

                    tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic

                    TaxUtils.AccumTaxingAmts(dr("ITM_TP_CD").ToString + "", tmp_Amt, hdrTaxReq.itmTpAmts)
                End If
            End If
        Next

        If IsNumeric(txt_del_chg.Text) Then

            'Determine if delivery should be taxed
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax

                tmp_tax = getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txt_del_chg.Text), "DEL")
                tax = tax + tmp_tax
                theSoHdrInfo.delChgTaxOnOrig = tmp_tax.ToString

            Else ' if header
                TaxUtils.AccumTaxingAmts("DEL", CDbl(txt_del_chg.Text), hdrTaxReq.itmTpAmts)
                'hdrTaxReq.itmTpAmts.del = CDbl(lblDelivery.Text)
            End If

        End If

        If IsNumeric(txt_setup.Text) Then

            'Determine the tax-ability of the setup charges
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax
                'tax = tax + getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txt_setup.Text), "SETUP")
                tmp_tax = getTaxAmt(txt_tax_cd.Text, taxDt, CDbl(txt_setup.Text), "SETUP")
                tax = tax + tmp_tax
                theSoHdrInfo.setupChgTaxOnOrig = tmp_tax.ToString

            Else ' header method
                TaxUtils.AccumTaxingAmts("SETUP", CDbl(txt_setup.Text), hdrTaxReq.itmTpAmts)
                ' hdrTaxReq.itmTpAmts.setup = CDbl(lblSetup.Text)
            End If
        End If

        If "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
            ' if header tax
            hdrTaxReq.taxCd = txt_tax_cd.Text
            hdrTaxReq.taxDt = credTaxDt
            tax = TaxUtils.ComputeHdrTax(hdrTaxReq)
        End If

        txt_crm_taxes.Text = FormatNumber(Math.Round(CDbl(tax), 2, MidpointRounding.AwayFromZero))
        txt_crm_grand.Text = FormatNumber(CDbl(merch_total) + CDbl(txt_setup.Text) + CDbl(txt_del_chg.Text) + CDbl(txt_crm_taxes.Text), 2)
        txt_crm_subtotal.Text = FormatNumber(CDbl(merch_total), 2)

        If CDbl(txt_crm_grand.Text) > CDbl(txt_origsale_grand.Text) Then
            hidDTDAmount.Value = FormatNumber(CDbl(txt_origsale_grand.Text), 2)
        Else
            hidDTDAmount.Value = FormatNumber(CDbl(txt_crm_grand.Text), 2)
        End If

    End Sub
    Private Sub SaleTotalCalculation(ByVal taxCode As String,
                                ByVal credTaxDt As DateTime, ByVal taxDt As DateTime, ByRef theSoHdrInfo As SoHdrExchangeInfo)
        Dim merch_total As Double = 0
        Dim tax_pct As Double = 0
        Dim tmp_Amt As Double = 0.0
        Dim tax As Double = 0
        Dim hdrTaxReq As New taxHdrCalcRequest
        Dim tmp_tax As Double

        Dim ds2 As New DataSet
        ds2 = Session("SAL_LN")
        Dim dt As DataTable = ds2.Tables(0)

        If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
            'Calculate Tax on Merchandise and Line Items
            tax_pct = TaxUtils.getTaxPct(taxCode, credTaxDt, "")  ' this tax date might need to be set by line - exchange/return to original - orig tax dt; uneven new sale lines, new tax date but then have to have separate docs
            ' currently only even exchanges so same tax date
        End If  ' line tax

        For Each dr In ds2.Tables(0).Rows
            Dim lineLevelTax As Double = 0.0
            If dr("VOID_FLAG") = "N" Then
                merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))
            End If
            If dr("VOID_FLAG").ToString = "N" AndAlso IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) Then
                ' TODO - someday this needs to be based on tax code tax method
                ' TODO - line taxing has to be based on written date andAlso based on whether tax auth taxes itm_tp for each line
                If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

                    'If LineInput tax method then
                    If TaxUtils.Determine_Taxability(dr("ITM_TP_CD"), credTaxDt.ToString, taxCode) = "Y" Then

                        tax = CDbl(tax) + ((CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC"))) * tax_pct)
                        lineLevelTax = ((CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC"))) * tax_pct)
                        dr("CUST_TAX_CHG") = FormatNumber(Math.Round(lineLevelTax, 2, MidpointRounding.AwayFromZero))
                    End If
                Else ' header tax method

                    tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic
                    dr("CUST_TAX_CHG") = CDbl(0)
                    TaxUtils.AccumTaxingAmts(dr("ITM_TP_CD").ToString + "", tmp_Amt, hdrTaxReq.itmTpAmts)
                End If
            End If
        Next

        If IsNumeric(txtNewDeliveryChgs.Text) Then

            'Determine if delivery should be taxed
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax

                tmp_tax = getTaxAmt(taxCode, taxDt, CDbl(txtNewDeliveryChgs.Text), "DEL")
                tax = tax + tmp_tax
                theSoHdrInfo.delChgTaxOnNew = tmp_tax.ToString

            Else ' if header
                TaxUtils.AccumTaxingAmts("DEL", CDbl(txtNewDeliveryChgs.Text), hdrTaxReq.itmTpAmts)
                'hdrTaxReq.itmTpAmts.del = CDbl(lblDelivery.Text)
            End If

        End If

        If IsNumeric(txtNewSetupChg.Text) Then

            'Determine the tax-ability of the setup charges
            ' TODO - someday this needs to be based on tax code tax method
            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                ' if line tax
                tmp_tax = getTaxAmt(taxCode, taxDt, CDbl(txtNewSetupChg.Text), "SETUP")
                tax = tax + tmp_tax
                theSoHdrInfo.setupChgTaxOnNew = tmp_tax.ToString

            Else ' header method
                TaxUtils.AccumTaxingAmts("SETUP", CDbl(txtNewSetupChg.Text), hdrTaxReq.itmTpAmts)
                ' hdrTaxReq.itmTpAmts.setup = CDbl(lblSetup.Text)
            End If
        End If

        If "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

            ' if header tax
            If hidTaxChange.Value = True Then
                hdrTaxReq.taxCd = lbl_tax_cd.Text
            Else
                hdrTaxReq.taxCd = txt_tax_cd.Text
            End If
            hdrTaxReq.taxDt = taxDt
            tax = TaxUtils.ComputeHdrTax(hdrTaxReq)
        End If

        txt_sal_taxes.Text = FormatNumber(Math.Round(CDbl(tax), 2, MidpointRounding.AwayFromZero))
        txt_sal_grand.Text = FormatNumber(CDbl(merch_total) + CDbl(txtNewSetupChg.Text) + CDbl(txtNewDeliveryChgs.Text) + CDbl(txt_sal_taxes.Text), 2)
        txt_sal_subtotal.Text = FormatNumber(CDbl(merch_total), 2)
    End Sub

    Public Function Determine_Taxability(ByVal itm_tp_cd As String)

        Determine_Taxability = "N"

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        If itm_tp_cd = "PKG" Or itm_tp_cd = "NLN" Then
            Determine_Taxability = "N"
            Exit Function
        End If

        If itm_tp_cd = "INV" Then
            Determine_Taxability = "Y"
            Exit Function
        End If

        sql = "select inventory from itm_tp where itm_tp_cd='" & itm_tp_cd & "' and inventory='Y'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try

        Dim TAT_QUERY As String = ""
        Select Case itm_tp_cd
            Case "LAB"
                TAT_QUERY = "LAB_TAX"
            Case "FAB"
                TAT_QUERY = "FAB_TAX"
            Case "NLT"
                TAT_QUERY = "NLT_TAX"
            Case "WAR"
                TAT_QUERY = "WAR_TAX"
            Case "PAR"
                TAT_QUERY = "PARTS_TAX"
            Case "MIL"
                TAT_QUERY = "MILEAGE_TAX"
            Case Else
                Determine_Taxability = "N"
                conn.Close()
                Exit Function
        End Select

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & lbl_tax_cd.Text & "' "
        sql = sql & "AND TAT." & TAT_QUERY & " = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function


    ''' <summary>
    ''' Extended so that additional attributes particular to the Exchange process
    ''' can be added.
    ''' </summary>
    ''' <remarks>Most attributes from base class are not populated, use with caution</remarks>
    Private Class SoHdrExchangeInfo
        Inherits SoHeaderDtc

        Property delChgTaxOnOrig As String
        Property delChgTaxOnNew As String
        Property setupChgTaxOnOrig As String
        Property setupChgTaxOnNew As String

    End Class
    Private Function AddPackages(addPkgCmpt As CreatePkgCmpntLnReq, ByRef pkgCmpts As ArrayList) As Boolean
        Dim showWarrPopup As Boolean = False

        If String.IsNullOrEmpty(addPkgCmpt.slsp1) Then
            addPkgCmpt.slsp1 = lbl_cust_cd.Text
        End If

        If Session("TAX_CD") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Session("TAX_CD")) Then
            Session("TAX_CD") = txt_tax_cd.Text
            Session("MANUAL_TAX_CD") = Nothing
        End If
        '***** set if store is ARS enabeld
        Dim store As StoreData = theSalesBiz.GetStoreInfo(Session("store_cd").ToString.Trim)
        Session("STORE_ENABLE_ARS") = store.enableARS
        ' create the object for header and transaction info     
        Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                                 addPkgCmpt.slsp1, addPkgCmpt.slsp2,
                                                                 addPkgCmpt.pctOfSale1, addPkgCmpt.pctOfSale2,
                                                                 Session("tet_cd"), Session("TAX_CD"),
                                                                 Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                                 Session("PD"), Session("pd_store_cd"),
                                                                 Session("cash_carry"))

        'Check if headderInfo storeCd exists
        If String.IsNullOrEmpty(headerInfo.puDelStore & "") Then
            headerInfo.puDelStore = Session("store_cd").ToString.Trim
        End If
        'Check if the pick/del is specified
        If String.IsNullOrEmpty(headerInfo.puDel & "") Then
            headerInfo.puDel = cbo_PD.SelectedValue
        End If

        ' TODO - TAX FIX - need to use the correct rates but we need tax date to calc
        '  Call SetTxPcts(tempItmReq.soHdrInf.soWrDt, tempItmReq.soHdrInf.taxCd).itmTpPcts
        If Session("TAX_RATE") IsNot Nothing Then
            Dim taxRate As Double = Session("TAX_RATE")
            headerInfo.taxRates.lin = taxRate
            headerInfo.taxRates.fab = taxRate
            headerInfo.taxRates.lab = taxRate
            headerInfo.taxRates.mil = taxRate
            headerInfo.taxRates.nlt = taxRate
            headerInfo.taxRates.nln = 0.0
            headerInfo.taxRates.par = taxRate
            headerInfo.taxRates.war = taxRate
            headerInfo.taxRates.svc = taxRate
            headerInfo.taxRates.pkg = taxRate
            headerInfo.taxRates.dnr = 0.0
            headerInfo.taxRates.del = taxRate
            headerInfo.taxRates.setup = taxRate
        End If

        ' create pkg components in temp_itm
        Dim tmpCmpnts As DataTable = theSalesBiz.AddPkgCmpntsToTempItm(headerInfo,
                                         addPkgCmpt.pkgSku, Session.SessionID.ToString.Trim, Session("store_cd").ToString.Trim, False)

        Dim drFilter() As DataRow = tmpCmpnts.Select("WARRANTABLE = 'Y' AND INVENTORY = 'Y'")
        For Each drFil As DataRow In drFilter
            If Not showWarrPopup Then showWarrPopup = SalesUtils.hasWarranty(drFil("ITM_CD"))
        Next

        'now get all the new lines which were inserted in this session- which here are the pkg component lines
        'these lines have to be inserted as rows in the master, Dataset that is cached in the session.
        Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpnts(Session.SessionID.ToString())
        Dim new_line_no As Double = addPkgCmpt.nextLnNum
        Dim lineDatSet As DataSet = Session("SAL_LN")
        Dim lineRow As DataRow
        Dim skuBiz As SKUBiz = New SKUBiz()
        Dim itemsList As ArrayList = New ArrayList()
        For Each line In pkgLines.Tables(0).Rows
            pkgCmpts.Add(line("ITM_CD"))
            itemsList.Add(line("ITM_CD").ToString)
            lineRow = lineDatSet.Tables(0).NewRow
            lineRow("DEL_DOC_NUM") = txt_del_doc_num.Text
            lineRow("DEL_DOC_LN#") = new_line_no
            lineRow("ITM_CD") = line("ITM_CD").ToString
            lineRow("COMM_CD") = line("COMM_CD").ToString
            lineRow("QTY") = line("QTY").ToString
            lineRow("UNIT_PRC") = line("RET_PRC").ToString
            lineRow("VE_CD") = line("VE_CD").ToString
            lineRow("ITM_TP_CD") = line("ITM_TP_CD").ToString
            lineRow("VOID_FLAG") = "N"
            lineRow("VSN") = Replace(line("VSN").ToString, "'", "''")
            lineRow("DES") = Replace(line("DES").ToString, "'", "''")
            lineRow("INVENTORY") = line("INVENTORY").ToString
            lineRow("WARRANTABLE") = line("WARRANTABLE")
            lineRow("delivery_points") = Double.TryParse(line("DEL_PTS").ToString, 0)
            lineRow("NEW_LINE") = "Y"
            lineRow("pkg_source") = addPkgCmpt.pkgLnNum  ' THis cannot work for multiple packages if this is the only line number
            If line("RET_PRC").ToString.ToUpper().Equals("INV") Then
                lineRow("PICKED") = "Z"
            Else
                lineRow("PICKED") = "X"
            End If
            lineRow("VOID_FLAG") = "N"
            lineRow("LN_SLSP1") = addPkgCmpt.slsp1
            lineRow("LN_SLSP2") = addPkgCmpt.slsp2
            lineRow("LN_SLSP1_PCT") = addPkgCmpt.pctOfSale1
            lineRow("LN_SLSP2_PCT") = addPkgCmpt.pctOfSale2
            lineRow("ITM_SRT_DESC") = skuBiz.GetItemSortCodesInString(itemsList)
            lineDatSet.Tables(0).Rows.Add(lineRow)
            new_line_no = new_line_no + 1
            itemsList.Clear()
        Next

        Session("SAL_LN") = lineDatSet
        Return showWarrPopup
    End Function

    Private Class CreatePkgCmpntLnReq

        Property pkgSku As String
        Property nextLnNum As Double  ' for adding components to line list
        Property pkgLnNum As Double
        Property slsp1 As String
        Property slsp2 As String
        Property pctOfSale1 As Double
        Property pctOfSale2 As Double
    End Class


    Private Function DoPreSaveValidation() As String

        Dim homePhone As String = txt_h_phone.Text
        Dim busPhone As String = txt_b_phone.Text
        Dim valResult As String = String.Empty

        'If String.IsNullOrWhiteSpace(txtTaxCd.Text) Then
        '    If (String.IsNullOrWhiteSpace(cbo_tax_exempt.Value)) Then
        '        valResult = Resources.POSMessages.MSG0022 & valResult
        '        Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
        '        hidIsInvokeManually.Value = "true"

        '        Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
        '        Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
        '        hidTaxDt.Value = FormatDateTime(Today.Date, vbShortDate) 'txt_wr_dt.Text.Trim()
        '        'If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
        '        hidZipCD.Value = txt_zip.Text.Trim 'String.Empty
        '        'End If
        '        ucTaxUpdate.LoadTaxData()
        '        PopupControlTax.ShowOnPageLoad = True
        '        Return valResult
        '    Else
        '        If String.IsNullOrWhiteSpace(txt_exempt_id.Text) Then
        '            valResult = Resources.POSMessages.MSG0023 & valResult
        '            txt_exempt_id.Focus()
        '            Return valResult
        '        End If
        '    End If
        'End If
        'Alice update on Mar 08, 2019 for tax exempt sales
        If String.IsNullOrWhiteSpace(txtTaxCd.Text) And String.IsNullOrWhiteSpace(Session("TET_CD")) Then
            valResult = Resources.POSMessages.MSG0065
        End If

        Return valResult
    End Function


    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        Dim errorMsg As String = DoPreSaveValidation()
        If Not String.IsNullOrEmpty(errorMsg) Then
            lbl_error.Text = errorMsg
            Exit Sub
        End If
        If IsApprovalRequired() Then
            ViewState("InitiatedSaveSequence") = "Y"
            GetApprovalPopup("Order Will Be saved")
        Else
            SaveChanges()
        End If
    End Sub
    ''' <summary>
    ''' This method saves the order details
    ''' </summary>
    Private Sub SaveChanges()

        Dim Proceed As Boolean = False
        Dim ds As New DataSet
        Dim Proceed2 As Boolean = False
        Dim test As String = ""
        Dim transPointTots As New TransportationPoints ' sum of all lines

        ViewState("InitiatedSaveSequence") = "Y"
        ds = Session("ORIG_SAL_LN")
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        For Each dr In ds.Tables(0).Rows
            If (Not String.IsNullOrEmpty(dr("ITM_CD").ToString)) AndAlso SystemUtils.isNumber(dr("QTY")) AndAlso dr("QTY") > 0.0 Then
                Proceed = True
            End If
            If (cbo_PD.SelectedValue.Equals("D") And SysPms.isDelvSchedEnabled) Then
                If dr("Delivery_points").ToString.isNotEmpty AndAlso IsNumeric(dr("Delivery_points").ToString) AndAlso CDbl(dr("Delivery_points").ToString) > 0 Then
                    transPointTots.schedPointTot = transPointTots.schedPointTot + (CDbl(dr("QTY", DataRowVersion.Current).ToString) * dr("delivery_points"))
                End If
            End If
        Next

        Dim ds2 As New DataSet

        ds2 = Session("SAL_LN")
        Dim dt2 As DataTable = ds2.Tables(0)
        Dim dr2 As DataRow

        For Each dr2 In ds2.Tables(0).Rows
            test = dr2("ITM_CD").ToString
            If Not String.IsNullOrEmpty(dr2("ITM_CD").ToString) Then
                Proceed2 = True
            End If
            If (cbo_PD.SelectedValue.Equals("D") And SysPms.isDelvSchedEnabled) Then
                If dr2("Delivery_points").ToString.isNotEmpty AndAlso IsNumeric(dr2("Delivery_points").ToString) AndAlso CDbl(dr2("Delivery_points").ToString) > 0 Then
                    transPointTots.schedPointTot = transPointTots.schedPointTot + (CDbl(dr2("QTY", DataRowVersion.Current).ToString) * dr2("delivery_points"))
                End If
            End If
        Next

        If Proceed2 = False Then
            lbl_error.Text = Resources.SalesExchange.Label03
            Exit Sub
        End If

        If Not IsDate(cbo_pd_date.Value) Then
            lbl_error.Text = Resources.SalesExchange.Label04
            Exit Sub
        ElseIf FormatDateTime(cbo_pd_date.Value) < Today.Date Then
            lbl_error.Text = Resources.SalesExchange.Label05
            Exit Sub
        End If

        If (ddlReturnReason.SelectedIndex = 0 OrElse ddlReturnReason.Enabled = False) Then
            lblErrorMessage.Visible = True
            lblErrorMessage.Text = Resources.POSErrors.ERR0038
            ddlReturnReason.Focus()
            Exit Sub
        End If

        If (String.IsNullOrWhiteSpace(Session("store_cd").ToString.Trim)) Then
            lbl_error.Text = Resources.POSErrors.ERR0010
            Exit Sub
        End If

        If (cbo_PD.SelectedValue.Equals("D") And SysPms.isDelvSchedEnabled) Then
            Dim delAvDT As Boolean = False
            delAvDT = CheckForDeliveryAvaDT()
            If Not delAvDT Then
                Dim strPointorStop As String = String.Empty
                If SysPms.isDelvByPoints Then
                    strPointorStop = "Point"
                Else
                    strPointorStop = "Stop"
                End If
                lbl_error.Text = String.Format(Resources.POSErrors.ERR0040, strPointorStop)
                Exit Sub
            End If
        End If
        'Alice added on Jan 07, 2019 for request #2260
        If (txtComments.Text.Trim.Length < 10) Then
            lbl_error.Text = Resources.SalesExchange.Label06
            Exit Sub
        End If

        Dim strFstStaConfCode As String = theSalesBiz.GetFirstConfirmationCode()

        '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Create the CRM document for any returned or exchanged items >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        Try


            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("store_cd").ToString.Trim, FormatDateTime(Today.Date, vbShortDate))

            If Not (soKeyInfo Is Nothing OrElse soKeyInfo.soDocNum Is Nothing OrElse soKeyInfo.soDocNum.delDocNum Is Nothing) Then
                If Len(soKeyInfo.soDocNum.delDocNum) <> 11 Then
                    lbl_error.Text = Resources.SalesExchange.Label07
                    Exit Sub
                End If
            End If

            Dim objsql2 As OracleCommand
            Dim Mydatareader2 As OracleDataReader
            Dim ar_trn_pk As String = ""

            Dim isArchived = False
            Dim whereClause = String.Empty
            Dim arcCount = 0

            sql = "SELECT AR_TRN_PK FROM AR_TRN WHERE IVC_CD=:IVC_CD AND TRN_TP_CD='SAL'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.Parameters.AddWithValue(":IVC_CD", txt_del_doc_num.Text)

            Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            Do While Mydatareader2.Read
                ar_trn_pk = Mydatareader2.Item("AR_TRN_PK").ToString
            Loop

            If String.IsNullOrEmpty(ar_trn_pk) Then
                sql = "SELECT COUNT(*) ARC FROM AR_TRN_ARC WHERE CO_CD=:CO_CD AND CUST_CD=:CUST_CD and IVC_CD=:IVC_CD AND TRN_TP_CD='SAL'"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objsql2.Parameters.Clear()

                objsql2.Parameters.AddWithValue(":CUST_CD", lbl_cust_cd.Text)
                objsql2.Parameters.AddWithValue(":CO_CD", Session("CO_CD"))
                objsql2.Parameters.AddWithValue(":IVC_CD", txt_del_doc_num.Text)

                Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                Do While Mydatareader2.Read
                    arcCount = Convert.ToInt32(Mydatareader2.Item("ARC").ToString)
                Loop

                If arcCount > 0 Then
                    whereClause = " FROM AR_TRN_ARC WHERE CO_CD=:CO_CD AND CUST_CD=:CUST_CD and IVC_CD=:IVC_CD AND TRN_TP_CD='SAL' "
                    isArchived = True
                Else
                    lbl_error.Text = Resources.POSErrors.ERR0064
                    Exit Sub
                End If
            Else
                isArchived = False
                whereClause = " FROM AR_TRN WHERE AR_TRN_PK=:AR_TRN_PK"
            End If

            'Create the AR_TRN Record
            sql = "INSERT INTO AR_TRN ("
            sql = sql & "CO_CD, CUST_CD, CNTR_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "
            sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, "
            sql = sql & "BNK_CRD_NUM, ARBR_DT, EXP_DT, MOP_CD, REF_NUM, HOST_REF_NUM, "
            sql = sql & "IVC_CD, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, WR_DT, POST_ID_NUM, "
            sql = sql & "ORIGIN_CD, BALANCED, LN_NUM, ACCT_NUM)"
            'sql = sql & "SELECT CO_CD, CUST_CD, CNTR_CD, :EMP_CD_CSHR, EMP_CD_APP, :EMP_CD_OP, :ORIGIN_STORE, CSH_DWR_CD, 'CRM', "
            sql = sql & "SELECT :CO_CD_NEW, CUST_CD, CNTR_CD, :EMP_CD_CSHR, EMP_CD_APP, :EMP_CD_OP, :ORIGIN_STORE, CSH_DWR_CD, 'CRM', "  'Alice update on Mar 07, 2018 for request 4527
            sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, :POST_DT, 'A', AR_TP, BNK_NUM, CHK_NUM, "
            If "N".Equals(ConfigurationManager.AppSettings("inherit_payment_info").ToString) Then
                sql = sql & "'','','','','','', "
            Else
                sql = sql & "BNK_CRD_NUM, ARBR_DT, EXP_DT, MOP_CD, "
                If Not isArchived Then
                    sql = sql & "REF_NUM, HOST_REF_NUM, "
                Else
                    sql = sql & "'','', "
                End If
            End If

            If Not isArchived Then
                sql = sql & ":NEW_IVC_CD, APP_CD, DES, :ADJ_IVC_CD, CHNG_OUT, :PMT_STORE, :WR_DT, POST_ID_NUM, "
                sql = sql & "'EXC',  BALANCED, LN_NUM, ACCT_NUM "
            Else
                sql = sql & ":NEW_IVC_CD, APP_CD, DES, :ADJ_IVC_CD, '', :PMT_STORE, :WR_DT, '', "
                sql = sql & "'EXC', '', '', '' "
            End If
            sql = sql & whereClause

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":NEW_AMT", OracleType.Number)
            If isArchived Then
                objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters.Add(":IVC_CD", OracleType.VarChar)

            Else
                objSql.Parameters.Add(":AR_TRN_PK", OracleType.VarChar)
            End If
            objSql.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)
            objSql.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
            objSql.Parameters.Add(":POST_DT", OracleType.DateTime)
            objSql.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
            objSql.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
            objSql.Parameters.Add(":WR_DT", OracleType.DateTime)
            objSql.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
            objSql.Parameters.Add(":CO_CD_NEW", OracleType.VarChar)   'Alice added on Mar 07, 2018 for request 4527
            objSql.Parameters.Add(":PMT_STORE", OracleType.VarChar)

            objSql.Parameters(":NEW_AMT").Value = CDbl(hidDTDAmount.Value)
            If isArchived Then
                objSql.Parameters(":CUST_CD").Value = lbl_cust_cd.Text
                objSql.Parameters(":CO_CD").Value = Session("CO_CD")
                objSql.Parameters(":IVC_CD").Value = txt_del_doc_num.Text

            Else
                If Not String.IsNullOrEmpty(ar_trn_pk) Then
                    objSql.Parameters(":AR_TRN_PK").Value = CDbl(ar_trn_pk)
                Else
                    objSql.Parameters(":AR_TRN_PK").Value = ar_trn_pk & ""
                End If
            End If

            objSql.Parameters(":NEW_IVC_CD").Value = txt_del_doc_num.Text
            objSql.Parameters(":ADJ_IVC_CD").Value = (soKeyInfo.soDocNum.delDocNum + "A")
            objSql.Parameters(":POST_DT").Value = FormatDateTime(Today.Date, vbShortDate)
            objSql.Parameters(":EMP_CD_CSHR").Value = UCase(Session("emp_cd"))
            objSql.Parameters(":EMP_CD_OP").Value = UCase(Session("emp_cd"))
            objSql.Parameters(":WR_DT").Value = FormatDateTime(Today.Date, 2)

            'Alice update on Mar 06, 2018 for request 4527, start 
            'the rule is: IF COMPANY CODE of Original SALE doesn't match company code of Cashier Activated Store then AR_TRN.ORIGIN_STORE = Cashier Activated store or Written store of Exchange
            'If COMPANY CODE OF Original SALE equals company code of Cashier Activated Store then AR_TRN.ORIGIN_STORE = Original Sales's written store
            'This is only for the CRM AR_TRN Transaction.  The SAL AR_TRN transaction is good

            Dim CompanyCodeOfOriginalSale As String = GetOriginalSaleCompanyCode()

            If CompanyCodeOfOriginalSale.ToUpper.Trim() = Session("CO_CD").ToString.ToUpper.Trim() Then
                objSql.Parameters(":ORIGIN_STORE").Value = txt_store_cd.Text 'equal to orignial sale store
                objSql.Parameters(":CO_CD_NEW").Value = CompanyCodeOfOriginalSale
            Else
                objSql.Parameters(":ORIGIN_STORE").Value = Session("store_cd").ToString.Trim
                objSql.Parameters(":CO_CD_NEW").Value = Session("CO_CD").ToString.Trim
            End If
            'Alice update on Mar 06, 2018, end

            objSql.Parameters(":PMT_STORE").Value = Session("store_cd").ToString.Trim

            objSql.ExecuteNonQuery()

            sql = "SELECT AR_TRN_PK FROM AR_TRN WHERE IVC_CD=:IVC_CD AND TRN_TP_CD='CRM'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.Parameters.Clear()

            objsql2.Parameters.AddWithValue(":IVC_CD", txt_del_doc_num.Text)

            Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)
            Dim crmtransNumber As String = ""
            Do While Mydatareader2.Read
                crmtransNumber = Mydatareader2.Item("AR_TRN_PK").ToString
            Loop



            'Create Debit entry for CRM
            Dim objAccount As New AccountingDtc
            If Not String.IsNullOrEmpty(crmtransNumber) Then
                objAccount.AccountKey = CDbl(crmtransNumber)
            Else
                objAccount.AccountKey = crmtransNumber & ""
            End If
            objAccount.IvcCode = txt_del_doc_num.Text
            objAccount.AdjIvcCode = (soKeyInfo.soDocNum.delDocNum + "A")
            objAccount.Amount = CDbl(hidDTDAmount.Value)
            objAccount.Description = "To cust:" & If(String.IsNullOrEmpty(lbl_cust_cd.Text), "", lbl_cust_cd.Text) & " ivc:" & soKeyInfo.soDocNum.delDocNum
            objAccount.MopCode = "TRC"
            objAccount.OriginCode = "EXC"
            objAccount.StatusCode = "T"
            objAccount.TransactionTypeCode = "DTD"
            objAccount.PostDate = FormatDateTime(Today.Date, vbShortDate)
            objAccount.DocumentSeqNumber = String.Empty

            If objAccount.Description.Length > 40 Then
                objAccount.Description = objAccount.Description.Substring(0, 40)
            End If

            ' theSalesBiz.InsertAccountingRecord(objAccount, conn)  'Alice disabled it on Jan 08,2019 for request 2260


            'Create the SO record [CRM]
            '
            sql = "INSERT INTO SO ("
            sql = sql & "DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, "
            sql = sql & "SHIP_TO_ST_CD, SHIP_TO_ZONE_CD, TAX_CD, TET_CD, ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, "
            sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
            sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
            sql = sql & "SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
            sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
            sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, "
            sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
            sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, PU_DEL, PU_DEL_DT, DEL_CHG, "
            sql = sql & "SETUP_CHG, TAX_CHG, STAT_CD, FINAL_DT, FINAL_STORE_CD, MASF_FLAG, ORIG_DEL_DOC_NUM, SER_CNTR_CD, "
            sql = sql & "SER_CNTR_VOID_FLAG,   RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
            sql = sql & "CASH_AND_CARRY, PRT_IVC_CNT, GRID_NUM, PRT_INS_CNT, "
            sql = sql & "ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, FRAN_SALE, "
            sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT "
            sql = sql & ", CONF_STAT_CD) "

            sql = sql & "SELECT :NEW_DEL_DOC_NUM, CUST_CD, :CAUSE_CD, VOID_CAUSE_CD, :EMP_CD_OP, :EMP_CD_KEYER,  "
            sql = sql & "SHIP_TO_ST_CD, :SHIP_TO_ZONE_CD, TAX_CD, TET_CD, 'CRM', :ORD_SRT_CD, SE_TP_CD, :NEW_SO_STORE_CD, "
            sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, "

            If Session("str_co_grp_cd").ToString = "BRK" Then 'Alice added on Feb 21, 2019 for request 4477
                sql = sql & "'HOU" & Session("store_cd").ToString.Trim & "', '', '', "
            Else
                sql = sql & "SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
            End If

            sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
            sql = sql & ":NEW_SO_DOC_NUM, :SO_WR_DT, :SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
            sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
            sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, "

            'Alice added on Feb 21, 2019 for request 4477
            'Original sale has 2 salespeople at 50 % each EXCHANGE changed to 1 salesperson HOU but needs to change to 100 for pct_of_sale1 And set pct_of_sale2 to 0 for brick store
            If Session("str_co_grp_cd").ToString = "BRK" Then
                sql = sql & "100,0, "
            Else
                sql = sql & "PCT_OF_SALE1, PCT_OF_SALE2, "
            End If

            sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, :SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
            sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, :PU_DEL, :PU_DEL_DT, :DEL_CHG, "
            sql = sql & ":SETUP_CHG, :TAX_CHG, 'O', NULL, NULL, MASF_FLAG, :ORIG_DEL_DOC_NUM, SER_CNTR_CD, "
            sql = sql & "SER_CNTR_VOID_FLAG, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
            sql = sql & "CASH_AND_CARRY, '1', GRID_NUM, PRT_INS_CNT, "
            sql = sql & "ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, FRAN_SALE, "
            sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, 'EXC', DEL_STAT "
            sql = sql & ",'" & strFstStaConfCode & "' "
            sql = sql & "FROM SO WHERE DEL_DOC_NUM=:ORIG_DEL_DOC_NUM"

            Dim cmdCrmSo As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            cmdCrmSo.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":NEW_SO_DOC_NUM", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":ORIG_DEL_DOC_NUM", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":SO_SEQ_NUM", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":SO_WR_SEC", OracleType.Number)
            cmdCrmSo.Parameters.Add(":TAX_CHG", OracleType.Number)
            cmdCrmSo.Parameters.Add(":SO_WR_DT", OracleType.DateTime)
            cmdCrmSo.Parameters.Add(":PU_DEL_DT", OracleType.DateTime)
            cmdCrmSo.Parameters.Add(":PU_DEL", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":DEL_CHG", OracleType.Number)
            cmdCrmSo.Parameters.Add(":SETUP_CHG", OracleType.Number)
            cmdCrmSo.Parameters.Add(":CAUSE_CD", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":ORD_SRT_CD", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":EMP_CD_KEYER", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
            cmdCrmSo.Parameters.Add(":NEW_SO_STORE_CD", OracleType.VarChar)

            cmdCrmSo.Parameters(":NEW_DEL_DOC_NUM").Value = soKeyInfo.soDocNum.delDocNum + "A"
            cmdCrmSo.Parameters(":NEW_SO_DOC_NUM").Value = soKeyInfo.soDocNum.delDocNum
            cmdCrmSo.Parameters(":ORIG_DEL_DOC_NUM").Value = txt_del_doc_existing.Text.Trim
            cmdCrmSo.Parameters(":SO_SEQ_NUM").Value = soKeyInfo.soKey.seqNum
            cmdCrmSo.Parameters(":SO_WR_SEC").Value = DateDiff("s", FormatDateTime(Today.Date, 2) & " 00:00:00", Now)
            cmdCrmSo.Parameters(":EMP_CD_KEYER").Value = UCase(Session("emp_cd"))
            cmdCrmSo.Parameters(":EMP_CD_OP").Value = UCase(Session("emp_cd"))
            cmdCrmSo.Parameters(":NEW_SO_STORE_CD").Value = Session("store_cd").ToString.Trim

            If TaxUtils.isHdrTax("") Then
                cmdCrmSo.Parameters(":TAX_CHG").Value = CDbl(txt_crm_taxes.Text)
            Else
                Dim theSoHdrInfo As SoHdrExchangeInfo = Session("soHdrInfo")
                Dim hdrtaxChg As Double = 0.0
                If theSoHdrInfo.delChgTaxOnOrig.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.delChgTaxOnOrig) Then
                    hdrtaxChg = CDbl(theSoHdrInfo.delChgTaxOnOrig)
                End If
                If theSoHdrInfo.setupChgTaxOnOrig.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.setupChgTaxOnOrig) Then
                    hdrtaxChg = hdrtaxChg + CDbl(theSoHdrInfo.setupChgTaxOnOrig)
                End If
                cmdCrmSo.Parameters(":TAX_CHG").Value = hdrtaxChg
            End If
            cmdCrmSo.Parameters(":SO_WR_DT").Value = FormatDateTime(Today.Date, vbShortDate)
            cmdCrmSo.Parameters(":PU_DEL_DT").Value = FormatDateTime(cbo_pd_date.Value, vbShortDate)
            cmdCrmSo.Parameters(":DEL_CHG").Value = IIf(AppConstants.PICKUP.Equals(cbo_PD.SelectedValue), CDbl(0), CDbl(txt_del_chg.Text))
            cmdCrmSo.Parameters(":SETUP_CHG").Value = txt_setup.Text
            cmdCrmSo.Parameters(":PU_DEL").Value = cbo_PD.SelectedValue
            cmdCrmSo.Parameters(":CAUSE_CD").Value = ddlReturnReason.Value
            cmdCrmSo.Parameters(":SHIP_TO_ZONE_CD").Value = txt_zone_cd.Text

            If cboOrdSrt.Value & "" <> "" Then
                cmdCrmSo.Parameters(":ORD_SRT_CD").Value = cboOrdSrt.Value
            Else
                cmdCrmSo.Parameters(":ORD_SRT_CD").Value = System.DBNull.Value
            End If


            cmdCrmSo.ExecuteNonQuery()

            'Update Sales Order Lines
            Dim crmLns As New DataSet
            crmLns = Session("SAL_LN")
            Dim credTbl As DataTable = crmLns.Tables(0)
            Dim credLn As DataRow
            Dim curr_ln As Double = 1
            Dim cust_tax_Str As String = ""
            Dim new_cust_tax_chg As Double = 0.0
            Dim isWrrantable As Boolean = False

            For Each credLn In crmLns.Tables(0).Rows
                If CDbl(credLn("QTY")) > 0 Then

                    cust_tax_Str = credLn("cust_tax_chg").ToString + ""
                    new_cust_tax_chg = 0.0
                    If (Not IsNothing(cust_tax_Str)) AndAlso IsNumeric(cust_tax_Str) AndAlso CDbl(cust_tax_Str) > 0.0 Then
                        new_cust_tax_chg = Math.Round(CDbl(cust_tax_Str) / (CDbl(credLn("QTY", DataRowVersion.Original).ToString)) * CDbl(credLn("QTY").ToString), TaxUtils.Tax_Constants.taxPrecision, MidpointRounding.AwayFromZero)
                    End If

                    If (credLn("WARRANTABLE").ToString.ToUpper.Equals("Y") AndAlso credLn("ITM_TP_CD").ToString.ToUpper.Equals("INV")) Or credLn("ITM_TP_CD").ToString.ToUpper.Equals("WAR") Then
                        isWrrantable = True
                    Else
                        isWrrantable = False
                    End If

                    Insert_SO_LN(soKeyInfo.soDocNum.delDocNum + "A", credLn("DEL_DOC_LN#").ToString, credLn("DEL_DOC_LN#"), CDbl(credLn("QTY").ToString), new_cust_tax_chg, True, CDbl(credLn("unit_prc").ToString), isWrrantable)

                    If credLn("WARRANTABLE").ToString.ToUpper.Equals("Y") AndAlso credLn("ITM_TP_CD").ToString.ToUpper.Equals("INV") Then
                        If credLn("WARR_BY_LN").ToString <> String.Empty Then
                            InsertWarrantySoLn(soKeyInfo.soDocNum.delDocNum + "A", CDbl(credLn("WARR_BY_LN").ToString), soKeyInfo.soDocNum.delDocNum + "A", CDbl(credLn("DEL_DOC_LN#")))
                        End If
                    End If

                    If credLn("ITM_TP_CD").ToString.ToUpper.Equals("INV") Then 'Alice added on May16, 2019 for ticket 574450
                        ' Adding the INV_XREF_ARC records to INV_XREF table
                        AddPendingInventory(soKeyInfo.soDocNum.delDocNum + "A", credLn("DEL_DOC_LN#"), credLn("ITM_CD"), credLn("QTY"), txt_del_doc_existing.Text.Trim)
                    End If


                End If
                curr_ln = curr_ln + 1
            Next

            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Create the SALE document for any exchange or new sale items >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            'Create the AR_TRN Record
            sql = "INSERT INTO AR_TRN ("
            sql = sql & "CO_CD, CUST_CD, CNTR_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "
            sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, "

            sql = sql & "BNK_CRD_NUM, ARBR_DT, EXP_DT, MOP_CD, REF_NUM, HOST_REF_NUM, "
            sql = sql & "IVC_CD, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, WR_DT, POST_ID_NUM, "
            sql = sql & "ORIGIN_CD, BALANCED, LN_NUM, ACCT_NUM)"

            sql = sql & "SELECT '" & Session("CO_CD").ToString.Trim & "', CUST_CD, CNTR_CD, : EMP_CD_CSHR, EMP_CD_APP, :EMP_CD_OP, :ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "  ' SAL  Alice upate on Mar 08 2019
            sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, :POST_DT, 'A', AR_TP, BNK_NUM, CHK_NUM, "

            If "N".Equals(ConfigurationManager.AppSettings("inherit_payment_info").ToString) Then
                sql = sql & "'','','','','','', "
            Else
                sql = sql & "BNK_CRD_NUM, ARBR_DT, EXP_DT, MOP_CD, "
                If Not isArchived Then
                    sql = sql & "REF_NUM, HOST_REF_NUM, "
                Else
                    sql = sql & "'','', "
                End If
            End If
            If Not isArchived Then
                sql = sql & ":NEW_IVC_CD, APP_CD, DES, :ADJ_IVC_CD, CHNG_OUT, :PMT_STORE, :WR_DT, POST_ID_NUM, "
                sql = sql & "'EXC', BALANCED, LN_NUM, ACCT_NUM "
            Else
                sql = sql & ":NEW_IVC_CD, APP_CD, DES, :ADJ_IVC_CD, '', :PMT_STORE, :WR_DT, '', "
                sql = sql & "'EXC', '', '', '' "
            End If

            sql = sql & whereClause

            Dim cmdSalAr As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            cmdSalAr.Parameters.Add(":NEW_AMT", OracleType.Number)
            If isArchived Then
                cmdSalAr.Parameters.Add(":CUST_CD", OracleType.VarChar)
                cmdSalAr.Parameters.Add(":CO_CD", OracleType.VarChar)
                cmdSalAr.Parameters.Add(":IVC_CD", OracleType.VarChar)
            Else
                cmdSalAr.Parameters.Add(":AR_TRN_PK", OracleType.VarChar)
            End If
            cmdSalAr.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)
            cmdSalAr.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
            cmdSalAr.Parameters.Add(":POST_DT", OracleType.DateTime)
            cmdSalAr.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)

            cmdSalAr.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
            cmdSalAr.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
            cmdSalAr.Parameters.Add(":WR_DT", OracleType.DateTime)
            cmdSalAr.Parameters.Add(":PMT_STORE", OracleType.VarChar)

            cmdSalAr.Parameters(":NEW_AMT").Value = CDbl(hidDTDAmount.Value)



            If isArchived Then
                cmdSalAr.Parameters(":CUST_CD").Value = lbl_cust_cd.Text
                cmdSalAr.Parameters(":CO_CD").Value = Session("CO_CD")
                cmdSalAr.Parameters(":IVC_CD").Value = txt_del_doc_num.Text
            Else
                If Not String.IsNullOrEmpty(ar_trn_pk) Then
                    cmdSalAr.Parameters(":AR_TRN_PK").Value = CDbl(ar_trn_pk)
                Else
                    cmdSalAr.Parameters(":AR_TRN_PK").Value = ar_trn_pk & ""
                End If
            End If
            cmdSalAr.Parameters(":NEW_IVC_CD").Value = soKeyInfo.soDocNum.delDocNum
            cmdSalAr.Parameters(":ADJ_IVC_CD").Value = ""
            cmdSalAr.Parameters(":POST_DT").Value = FormatDateTime(Today.Date, vbShortDate)
            cmdSalAr.Parameters(":ORIGIN_STORE").Value = Session("store_cd").ToString.Trim


            cmdSalAr.Parameters(":EMP_CD_CSHR").Value = UCase(Session("emp_cd"))
            cmdSalAr.Parameters(":EMP_CD_OP").Value = UCase(Session("emp_cd"))
            cmdSalAr.Parameters(":WR_DT").Value = FormatDateTime(Today.Date, 2)
            cmdSalAr.Parameters(":PMT_STORE").Value = Session("store_cd").ToString.Trim

            Dim NEW_IVC_CD As String = cmdSalAr.Parameters(":NEW_IVC_CD").Value

            cmdSalAr.ExecuteNonQuery()


            ''Alice added start on Jan14,2019 for request 2260 to set original co_cd for sale record in ar_trn table

            'Dim sql10 As String = "update ar_trn a set a.co_cd= (SELECT s.CO_CD FROM store s WHERE s.STORE_CD=a.origin_store) where a.cust_cd=:cust_cd and a.trn_tp_cd='SAL' and a.ivc_cd=:ivc_cd"
            'Dim objsql10 As OracleCommand = DisposablesManager.BuildOracleCommand(sql10, conn)

            'objsql10.Parameters.Add(":cust_cd", OracleType.VarChar)
            'objsql10.Parameters.Add(":ivc_cd", OracleType.VarChar)
            'objsql10.Parameters(":cust_cd").Value = lbl_cust_cd.Text
            'objsql10.Parameters(":ivc_cd").Value = NEW_IVC_CD
            'objsql10.ExecuteNonQuery()

            ''Alice added end on Jan14,2019 for request 2260


            sql = "SELECT AR_TRN_PK FROM AR_TRN WHERE IVC_CD=:IVC_CD AND TRN_TP_CD='SAL'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.Parameters.Clear()
            objsql2.Parameters.AddWithValue(":IVC_CD", soKeyInfo.soDocNum.delDocNum)

            Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)
            Dim saltransNumber As String = ""
            Do While Mydatareader2.Read
                saltransNumber = Mydatareader2.Item("AR_TRN_PK").ToString
            Loop

            ''Create Credit entry for SO
            If Not String.IsNullOrEmpty(saltransNumber) Then
                objAccount.AccountKey = CDbl(saltransNumber)
            Else
                objAccount.AccountKey = saltransNumber & ""
            End If
            objAccount.IvcCode = soKeyInfo.soDocNum.delDocNum
            objAccount.AdjIvcCode = String.Empty
            objAccount.Amount = CDbl(hidDTDAmount.Value)
            objAccount.Description = "From cust:" & If(String.IsNullOrEmpty(lbl_cust_cd.Text), "", lbl_cust_cd.Text) & " cntr:" & txt_del_doc_num.Text.Trim
            objAccount.MopCode = "TRC"
            objAccount.OriginCode = "EXC"
            objAccount.PostDate = FormatDateTime(Today.Date, vbShortDate)
            objAccount.StatusCode = "T"
            objAccount.TransactionTypeCode = "DTC"
            objAccount.PostDate = FormatDateTime(Today.Date, vbShortDate)
            objAccount.DocumentSeqNumber = soKeyInfo.soKey.seqNum
            If objAccount.Description.Length > 40 Then
                objAccount.Description = objAccount.Description.Substring(0, 40)
            End If

            ' theSalesBiz.InsertAccountingRecord(objAccount, conn)  'Alice disabled it on Jan 09, 2019 for request 2260

            'Exchange SO Creation

            sql = "INSERT INTO SO ("
            sql = sql & "DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, "
            sql = sql & "SHIP_TO_ST_CD, SHIP_TO_ZONE_CD, TAX_CD, TET_CD, ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, "
            sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
            sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
            sql = sql & "SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
            sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
            sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, "
            sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
            sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, PU_DEL, PU_DEL_DT, DEL_CHG, "
            sql = sql & "SETUP_CHG, TAX_CHG, STAT_CD, FINAL_DT, FINAL_STORE_CD, MASF_FLAG, ORIG_DEL_DOC_NUM, SER_CNTR_CD, "
            sql = sql & "SER_CNTR_VOID_FLAG, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
            sql = sql & "CASH_AND_CARRY, PRT_IVC_CNT, GRID_NUM, PRT_INS_CNT, "
            sql = sql & "ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, FRAN_SALE, "
            sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT "
            sql = sql & ", CONF_STAT_CD) "

            sql = sql & "SELECT :NEW_DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, :EMP_CD_OP, :EMP_CD_KEYER,  "
            sql = sql & "SHIP_TO_ST_CD, :SHIP_TO_ZONE_CD, :TAX_CD, TET_CD, 'SAL', :ORD_SRT_CD, SE_TP_CD, :NEW_SO_STORE_CD, "
            sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, "

            If Session("str_co_grp_cd").ToString = "BRK" Then 'Alice added on Feb 21, 2019 for request 4477
                sql = sql & "'HOU" & Session("store_cd").ToString.Trim & "', '', '', "
            Else
                sql = sql & "SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
            End If
            sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
            sql = sql & ":NEW_SO_DOC_NUM, :SO_WR_DT, :SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
            sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
            sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, "

            'Alice added on Feb 21, 2019 for request 4477
            'Original sale has 2 salespeople at 50 % each EXCHANGE changed to 1 salesperson HOU but needs to change to 100 for pct_of_sale1 And set pct_of_sale2 to 0 for brick store
            If Session("str_co_grp_cd").ToString = "BRK" Then
                sql = sql & "100,0, "
            Else
                sql = sql & "PCT_OF_SALE1, PCT_OF_SALE2, "
            End If

            sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, :SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
            sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, :PU_DEL, :PU_DEL_DT, :DEL_CHG, "
            sql = sql & ":SETUP_CHG, :TAX_CHG, 'O', NULL, NULL, MASF_FLAG, '', SER_CNTR_CD, "
            sql = sql & "SER_CNTR_VOID_FLAG, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
            sql = sql & "CASH_AND_CARRY, '1', GRID_NUM, PRT_INS_CNT, "
            sql = sql & "ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, FRAN_SALE, "
            sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, 'EXC', DEL_STAT "
            sql = sql & ",'" & strFstStaConfCode & "' "
            sql = sql & "FROM SO WHERE DEL_DOC_NUM=:ORIG_DEL_DOC_NUM"

            Dim cmdSalSo As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            cmdSalSo.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":ORIG_DEL_DOC_NUM", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":NEW_SO_DOC_NUM", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":SO_SEQ_NUM", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":SO_WR_SEC", OracleType.Number)
            cmdSalSo.Parameters.Add(":NEW_SO_STORE_CD", OracleType.VarChar)
            'cmdSalSo.Parameters.Add(":NEW_PU_DEL_STORE_CD", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":TAX_CHG", OracleType.Number)
            cmdSalSo.Parameters.Add(":SO_WR_DT", OracleType.DateTime)
            cmdSalSo.Parameters.Add(":PU_DEL_DT", OracleType.DateTime)
            cmdSalSo.Parameters.Add(":PU_DEL", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":DEL_CHG", OracleType.Number)
            cmdSalSo.Parameters.Add(":SETUP_CHG", OracleType.Number)
            cmdSalSo.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":ORD_SRT_CD", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":TAX_CD", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":EMP_CD_KEYER", OracleType.VarChar)
            cmdSalSo.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)

            cmdSalSo.Parameters(":NEW_DEL_DOC_NUM").Value = soKeyInfo.soDocNum.delDocNum
            cmdSalSo.Parameters(":TAX_CD").Value = lnkUpdatedTax.Text
            cmdSalSo.Parameters(":NEW_SO_DOC_NUM").Value = soKeyInfo.soDocNum.delDocNum
            cmdSalSo.Parameters(":ORIG_DEL_DOC_NUM").Value = txt_del_doc_existing.Text.Trim
            cmdSalSo.Parameters(":SO_SEQ_NUM").Value = soKeyInfo.soKey.seqNum
            cmdSalSo.Parameters(":NEW_SO_STORE_CD").Value = Session("store_cd").ToString.Trim
            'cmdSalSo.Parameters(":NEW_PU_DEL_STORE_CD").Value = txt_new_pd_store_cd.Text
            cmdSalSo.Parameters(":SO_WR_SEC").Value = DateDiff("s", FormatDateTime(Today.Date, 2) & " 00:00:00", Now)
            cmdSalSo.Parameters(":EMP_CD_KEYER").Value = UCase(Session("emp_cd"))
            cmdSalSo.Parameters(":EMP_CD_OP").Value = UCase(Session("emp_cd"))

            If TaxUtils.isHdrTax("") Then
                cmdSalSo.Parameters(":TAX_CHG").Value = CDbl(txt_sal_taxes.Text)
            Else
                Dim theSoHdrInfo As SoHdrExchangeInfo = Session("soHdrInfo")
                Dim hdrtaxChg As Double = 0.0
                If theSoHdrInfo.delChgTaxOnNew.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.delChgTaxOnNew) Then
                    hdrtaxChg = CDbl(theSoHdrInfo.delChgTaxOnNew)
                End If
                If theSoHdrInfo.setupChgTaxOnNew.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.setupChgTaxOnNew) Then
                    hdrtaxChg = hdrtaxChg + CDbl(theSoHdrInfo.setupChgTaxOnNew)
                End If
                cmdSalSo.Parameters(":TAX_CHG").Value = hdrtaxChg
            End If
            Dim soWrittenDate As Date = FormatDateTime(Today.Date, vbShortDate)
            cmdSalSo.Parameters(":SO_WR_DT").Value = soWrittenDate
            cmdSalSo.Parameters(":PU_DEL_DT").Value = FormatDateTime(cbo_pd_date.Value, vbShortDate)
            txt_del_chg.Text = IIf(String.IsNullOrWhiteSpace(txt_del_chg.Text), 0.0, txt_del_chg.Text)
            cmdSalSo.Parameters(":DEL_CHG").Value = IIf(AppConstants.PICKUP.Equals(cbo_PD.SelectedValue), CDbl(0), CDbl(txtNewDeliveryChgs.Text))
            cmdSalSo.Parameters(":SETUP_CHG").Value = CDbl(txtNewSetupChg.Text)
            cmdSalSo.Parameters(":PU_DEL").Value = cbo_PD.SelectedValue
            cmdSalSo.Parameters(":SHIP_TO_ZONE_CD").Value = txtNewZone.Text

            If cboOrdSrt.Value & "" <> "" Then
                cmdSalSo.Parameters(":ORD_SRT_CD").Value = cboOrdSrt.Value
            Else
                cmdSalSo.Parameters(":ORD_SRT_CD").Value = System.DBNull.Value
            End If

            cmdSalSo.ExecuteNonQuery()

            objSql.Parameters.Clear()
            'Add comments after adding Sales Order
            SaveComments(objSql, txtComments.Text, soKeyInfo.soKey.seqNum, soWrittenDate, Session("store_cd").ToString.Trim, soKeyInfo.soDocNum.delDocNum)
            SaveComments(objSql, txtComments.Text, soKeyInfo.soKey.seqNum, soWrittenDate, Session("store_cd").ToString.Trim, soKeyInfo.soDocNum.delDocNum + "A") 'alice added ON Jan182019 for request 2260: the comments entered in Exchange should show up for both the SAL and CRM SO documents

            'Update Sales Order Lines
            Dim salLines As New DataSet
            salLines = Session("SAL_LN")
            Dim salLn As DataRow
            curr_ln = 1

            '   :pkgSource,   ' TODO - dsa - TEST PKGS
            Dim zoneCode As String = If(txtNewZone.Text.Trim.Equals(String.Empty), txt_zone_cd.Text.Trim, txtNewZone.Text.Trim)
            Dim transPoint As New TransportationPoints
            Dim origTranSched As New TransportationSchedule
            origTranSched.zoneCd = txt_zone_cd.Text.ToString
            origTranSched.puDelCd = cbo_PD.SelectedValue
            origTranSched.puDelDt = FormatDateTime(hidOriginalDate.Value, DateFormat.ShortDate)
            Dim currTranSched As New TransportationSchedule
            currTranSched.zoneCd = zoneCode.ToString
            currTranSched.puDelCd = cbo_PD.SelectedValue
            currTranSched.puDelDt = FormatDateTime(cbo_pd_date.Text, DateFormat.ShortDate)

            For Each salLn In salLines.Tables(0).Rows
                If CDbl(salLn("QTY")) > 0 Then

                    If salLn("NEW_LINE") + "" = "Y" Then
                        Insert_New_SO_LN(soKeyInfo.soDocNum.delDocNum, salLn("DEL_DOC_LN#"), salLn)

                        If salLn("WARRANTABLE").ToString.ToUpper.Equals("Y") AndAlso salLn("ITM_TP_CD").ToString.ToUpper.Equals("INV") Then
                            If salLn("WARR_BY_LN").ToString <> String.Empty Then
                                InsertWarrantySoLn(soKeyInfo.soDocNum.delDocNum, CDbl(salLn("WARR_BY_LN").ToString), soKeyInfo.soDocNum.delDocNum, CDbl(salLn("DEL_DOC_LN#")))
                            End If
                        End If
                    Else
                        new_cust_tax_chg = salLn("CUST_TAX_CHG").ToString
                        If (salLn("WARRANTABLE").ToString.ToUpper.Equals("Y") AndAlso salLn("ITM_TP_CD").ToString.ToUpper.Equals("INV")) Or salLn("ITM_TP_CD").ToString.ToUpper.Equals("WAR") Then
                            isWrrantable = True
                        Else
                            isWrrantable = False
                        End If

                        Insert_SO_LN(soKeyInfo.soDocNum.delDocNum, salLn("DEL_DOC_LN#").ToString, salLn("DEL_DOC_LN#"), CDbl(salLn("QTY").ToString), new_cust_tax_chg, False, CDbl(salLn("unit_prc").ToString), isWrrantable)

                        If salLn("WARRANTABLE").ToString.ToUpper.Equals("Y") AndAlso salLn("ITM_TP_CD").ToString.ToUpper.Equals("INV") Then
                            UpdateWarrantySoLn(txt_del_doc_existing.Text.Trim, CDbl(salLn("DEL_DOC_LN#").ToString), soKeyInfo.soDocNum.delDocNum, CDbl(salLn("DEL_DOC_LN#")))
                        End If

                    End If
                End If
                CalcSoLnPoints(salLn, origTranSched, currTranSched, transPoint)
                curr_ln = curr_ln + 1
            Next

            objSql.Dispose()
            objsql2.Dispose()

            cmdCrmSo.Dispose()
            cmdSalSo.Dispose()
            cmdSalAr.Dispose()
            conn.Close()
            txtComments.Text = String.Empty
            btn_Save.Enabled = False
            btn_submit.Enabled = False
            Session("STORE_ENABLE_ARS") = False

            orig_Sale.Enabled = False
            grd_lines_new.Enabled = False

            If (cbo_PD.SelectedValue.Equals("D") And SysPms.isDelvSchedEnabled) Then
                Using connection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                    connection.Open()
                    Using command As OracleCommand = DisposablesManager.BuildOracleCommand(connection)
                        Dim transaction As OracleTransaction = connection.BeginTransaction()
                        command.Transaction = transaction
                        ProcessTransportationUpdt(transPoint, origTranSched, currTranSched, command)
                        transaction.Commit()
                    End Using
                End Using
            End If

            lbl_error.Text = soKeyInfo.soDocNum.delDocNum + "A" & " CRM Created - " & soKeyInfo.soDocNum.delDocNum & " SAL Created"

            Response.Redirect("SalesOrderMaintenance.aspx?del_doc_num=" & soKeyInfo.soDocNum.delDocNum & "&query_returned=Y")

        Catch ex As Exception
            lbl_error.Text = ex.Message
        End Try

    End Sub
    'Alice added on Mar 06, 2018
    'Alice udpated on Jun 25, 2019 to get co_cd from store table instead of ar_trn table, becasue some sales exist in ar_trn_arc table, not ar_trn table, ar_trn_arc is huge, is slow to look up 
    Private Function GetOriginalSaleCompanyCode() As String

        Dim orig_co_cd As String = String.Empty

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
            Dim sql As String
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            Dim objsql As OracleCommand
            Dim Mydatareader As OracleDataReader

        Try
            conn.Open()
            sql = "Select co_cd from store where store_cd = :store_cd "
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.Parameters.AddWithValue(":store_cd", txt_store_cd.Text)
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Do While Mydatareader.Read
                orig_co_cd = Mydatareader.Item("co_cd").ToString
            Loop

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Mydatareader.Close()
        conn.Close()

        Return orig_co_cd

    End Function

    Private Sub SaveComments(dbCommand As OracleCommand, strComments As String, seqNum As String,
                             writtenDate As String, storeCode As String, soOrderNumber As String)
        If Len(strComments) > 0 Then
            HBCG_Utils.InsertSOComment(dbCommand, strComments, writtenDate, storeCode, seqNum, "S", Session("EMP_CD"), soOrderNumber)
        End If
    End Sub


    Private Sub CalcSoLnPoints(ByRef soLn As DataRow,
                                 ByVal origTranSched As TransportationSchedule, ByVal currTranSched As TransportationSchedule, ByRef tranPoints As TransportationPoints)

        If SysPms.isDelvByPoints Then
            If Not IsDBNull(soLn("POINT_SIZE").ToString) AndAlso soLn("POINT_SIZE").ToString.isNotEmpty AndAlso IsNumeric(soLn("POINT_SIZE").ToString) AndAlso CDbl(soLn("POINT_SIZE").ToString) > 0 Then
                If origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY Then
                    tranPoints.currPointTot = tranPoints.currPointTot + (soLn("QTY") * soLn("POINT_SIZE"))
                    tranPoints.origPointTot = tranPoints.origPointTot + (soLn("QTY") * soLn("POINT_SIZE"))
                    tranPoints.schedPointTot = tranPoints.schedPointTot + (soLn("QTY") * soLn("POINT_SIZE"))
                End If
            End If
        End If

    End Sub

    Private Sub ProcessTransportationUpdt(ByRef tranLnPoints As TransportationPoints, ByVal origTranSched As TransportationSchedule,
                                          ByVal currTranSched As TransportationSchedule, ByRef cmd As OracleCommand)

        Dim trnTp As String = AppConstants.Order.TYPE_SAL

        If origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY Then

            Dim schedNumber As Double = IIf(SysPms.isDelvByPoints, tranLnPoints.schedPointTot, 1) * CRMPointsCalculation()
            'Schedule to deliver and Pick up new item
            theTMBiz.SchedPuDel(cmd, currTranSched.zoneCd, currTranSched.puDelCd, currTranSched.puDelDt,
                                       trnTp, schedNumber)

        End If
    End Sub

    Public Sub Insert_New_SO_LN(ByVal del_doc_num As String, ByVal lineNo As String, ByVal salLn As DataRow)

        Dim sqlStmtSb As New StringBuilder

        sqlStmtSb.Append("INSERT INTO SO_LN (").Append(
            "DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, COMM_CD, SO_DOC_NUM, ID_NUM, UNIT_PRC, QTY, SPIFF, PICKED, VOID_FLAG, ").Append(
            "TAKEN_WITH, PKG_SOURCE, SET_UP, LV_IN_CARTON, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, ").Append(
            "COMM_ON_DEL_CHG, TAX_CD, TAX_BASIS, CUST_TAX_CHG, STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT) ")
        sqlStmtSb.Append(" VALUES( :NEW_DEL_DOC_NUM, :NEW_DEL_DOC_LN, :itmCd, :commCd, :soDocNum, '', :unitPrc, :qty, :spiff, :pick, 'N', ").Append(
            "'N', :pkgSource, :setup, :lvInCarton, :slsp1, :slsp2, :pct1, :pct2, :commSetup, ").Append(
            ":commDel, :taxCd, :taxBasis, :custTax, :storeTax, :taxResp, :delPts, :delDt ) ")

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

        cmd.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters(":NEW_DEL_DOC_NUM").Value = del_doc_num
        cmd.Parameters.Add(":NEW_DEL_DOC_LN", OracleType.Number)
        cmd.Parameters(":NEW_DEL_DOC_LN").Value = CDbl(lineNo)   'salLn("DEL_DOC_LN#").ToString)
        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = salLn("ITM_CD").ToString
        cmd.Parameters.Add(":commCd", OracleType.VarChar)
        cmd.Parameters(":commCd").Value = salLn("COMM_CD").ToString
        cmd.Parameters.Add(":soDocNum", OracleType.VarChar)
        cmd.Parameters(":soDocNum").Value = Left(del_doc_num, 11)

        cmd.Parameters.Add(":unitPrc", OracleType.Number)
        cmd.Parameters(":unitPrc").Value = CDbl(salLn("UNIT_PRC").ToString)
        cmd.Parameters.Add(":qty", OracleType.Number)
        cmd.Parameters(":qty").Value = CDbl(salLn("QTY").ToString)
        cmd.Parameters.Add(":spiff", OracleType.Number)
        If (salLn("SPIFF").ToString + "").isNotEmpty AndAlso IsNumeric(salLn("SPIFF").ToString + "") Then
            cmd.Parameters(":spiff").Value = CDbl(salLn("SPIFF").ToString)
        Else
            cmd.Parameters(":spiff").Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(":pick", OracleType.VarChar)
        cmd.Parameters(":pick").Value = salLn("PICKED").ToString

        'Alice updated on April 30, 2019 for request 4648 
        If cmd.Parameters(":pick").Value = "F" Then
            cmd.Parameters(":pick").Value = "Z"
        End If

        cmd.Parameters.Add(":delPts", OracleType.Number)
        If (salLn("delivery_points").ToString + "").isNotEmpty AndAlso IsNumeric(salLn("delivery_points").ToString + "") Then
            cmd.Parameters(":delPts").Value = CDbl(salLn("delivery_points").ToString)
        Else
            cmd.Parameters(":delPts").Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(":delDt", OracleType.DateTime)
        cmd.Parameters(":delDt").Value = FormatDateTime(cbo_pd_date.Value, vbShortDate)

        cmd.Parameters.Add(":pkgSource", OracleType.VarChar)
        cmd.Parameters(":pkgSource").Value = System.DBNull.Value

        Dim itm As ItemLibrary.Item = ItemLibrary.ItemUtils.getItmInfo(salLn("ITM_CD").ToString)
        cmd.Parameters.Add(":setup", OracleType.VarChar)
        cmd.Parameters(":setup").Value = itm.setupReq
        cmd.Parameters.Add(":lvInCarton", OracleType.VarChar)
        cmd.Parameters(":lvInCarton").Value = itm.inCarton

        Dim theSoHdrInfo As SoHdrExchangeInfo = Session("soHdrInfo")

        If Session("str_co_grp_cd").ToString = "BRK" Then 'Alice added on Feb 27, 2019 for request 4477

            cmd.Parameters.Add(":slsp1", OracleType.VarChar)
            cmd.Parameters(":slsp1").Value = "HOU" & Session("store_cd").ToString.Trim
            cmd.Parameters.Add(":slsp2", OracleType.VarChar)
            cmd.Parameters(":slsp2").Value = String.Empty
            cmd.Parameters.Add(":pct1", OracleType.Number)
            cmd.Parameters(":pct1").Value = 100
            cmd.Parameters.Add(":pct2", OracleType.Number)
            cmd.Parameters(":pct2").Value = 0
        Else
            cmd.Parameters.Add(":slsp1", OracleType.VarChar)
            cmd.Parameters(":slsp1").Value = theSoHdrInfo.slsp1
            cmd.Parameters.Add(":slsp2", OracleType.VarChar)
            cmd.Parameters(":slsp2").Value = theSoHdrInfo.slsp2

            cmd.Parameters.Add(":pct1", OracleType.Number)
            cmd.Parameters(":pct1").Value = theSoHdrInfo.slspPct1
            cmd.Parameters.Add(":pct2", OracleType.Number)
            If theSoHdrInfo.slspPct1 > 99.999 Then
                cmd.Parameters(":pct2").Value = CDbl(0.0)
            Else
                cmd.Parameters(":pct2").Value = theSoHdrInfo.slspPct2
            End If

        End If

        cmd.Parameters.Add(":commSetup", OracleType.VarChar)
        cmd.Parameters(":commSetup").Value = IIf(SysPms.commissionOnSetupCharge, "Y", "N")
        cmd.Parameters.Add(":commDel", OracleType.VarChar)
        cmd.Parameters(":commDel").Value = IIf(SysPms.commissionOnDelvCharge, "Y", "N")

        cmd.Parameters.Add(":taxCd", OracleType.VarChar)
        cmd.Parameters.Add(":taxBasis", OracleType.Number)
        cmd.Parameters.Add(":custTax", OracleType.Number)
        cmd.Parameters.Add(":storeTax", OracleType.Number)
        cmd.Parameters.Add(":taxResp", OracleType.VarChar)

        If TaxUtils.isHdrTax(theSoHdrInfo.taxCd) Then

            cmd.Parameters(":taxCd").Value = System.DBNull.Value
            cmd.Parameters(":taxBasis").Value = System.DBNull.Value
            cmd.Parameters(":custTax").Value = System.DBNull.Value
            cmd.Parameters(":storeTax").Value = System.DBNull.Value
            cmd.Parameters(":taxResp").Value = System.DBNull.Value

        Else
            ' TODO - future - should have kep this info, at least the custTax when calc'd during calculate_totals but logic
            '    didn't so we'll calculate now - sb able to use CDbl(salLn("cust_tax_chg").ToString)
            cmd.Parameters(":taxCd").Value = lnkUpdatedTax.Text.Trim 'theSoHdrInfo.taxCd
            cmd.Parameters(":taxBasis").Value = CDbl(salLn("UNIT_PRC").ToString)
            cmd.Parameters(":custTax").Value = salLn("CUST_TAX_CHG").ToString
            cmd.Parameters(":storeTax").Value = System.DBNull.Value    'split tax not used yet
            cmd.Parameters(":taxResp").Value = "C" ' split tax not used yet
        End If

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()

    End Sub

    ''' <summary>
    ''' Create an SO_LN from the original sale line
    ''' </summary>
    ''' <param name="del_doc_num"></param>
    ''' <param name="old_line_no"></param>
    ''' <param name="new_line_no"></param>
    ''' <param name="qty"></param>
    ''' <param name="cust_tax_chg"></param>
    ''' <param name="cred">true if credit line being created on CRM - indicates to copy alt_doc_ln# from orig sale; false otherwise</param>
    ''' <param name="unitPrc"></param>
    ''' <remarks></remarks>
    Public Sub Insert_SO_LN(ByVal del_doc_num As String, ByVal old_line_no As String, ByVal new_line_no As String, ByVal qty As String,
                            ByVal cust_tax_chg As String, ByVal cred As Boolean, ByVal unitPrc As String, ByVal isWrrantable As Boolean)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "INSERT INTO SO_LN ("
        sql = sql & "DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, STORE_CD, LOC_CD, REF_DEL_DOC_LN#, REF_ITM_CD,"
        sql = sql & "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, SO_DOC_NUM, ID_NUM, SER_NUM, "
        sql = sql & "UNIT_PRC, QTY, SPIFF, PICKED, OUT_ENTRY_DT, FIFL_DT, FIFO_CST, FIFO_DT, FILL_DT, VOID_FLAG, "
        sql = sql & "COD_AMT, OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, WARRANTED_BY_LN#, "
        sql = sql & "WARRANTED_BY_ITM_CD, TREATS_LN#, TREATS_ITM_CD, TREATED_BY_LN#, TREATED_BY_ITM_CD, DISC_AMT, "
        sql = sql & "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, "
        sql = sql & "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, "
        sql = sql & "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT, "
        sql = sql & "COMPLAINT_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, "
        sql = sql & "COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, "
        sql = sql & "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, TAX_CD, TAX_BASIS, CUST_TAX_CHG, "
        sql = sql & "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT, ALT_DOC_LN#) "

        sql = sql & "SELECT :NEW_DEL_DOC_NUM, :NEW_DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, NULL, NULL, "

        If isWrrantable Then
            sql = sql & "NULL, NULL, "
        Else
            sql = sql & "REF_DEL_DOC_LN#, REF_ITM_CD, "
        End If

        sql = sql & "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, :SO_DOC_NUM, ID_NUM, SER_NUM, "
        sql = sql & ":newPrc, :NEW_QTY, SPIFF, 'Z', OUT_ENTRY_DT, NULL, NULL, NULL, NULL, VOID_FLAG, "
        sql = sql & "COD_AMT, OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, "

        If isWrrantable Then
            sql = sql & "NULL, NULL, "
        Else
            sql = sql & "WARRANTED_BY_LN#,WARRANTED_BY_ITM_CD, "
        End If

        sql = sql & " TREATS_LN#, TREATS_ITM_CD, TREATED_BY_LN#, TREATED_BY_ITM_CD, DISC_AMT, "
        ' sql = sql & "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, "
        sql = sql & "SER_CD, SO_LN_CMNT, NULL, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, " 'Alice udpated on Aug 12, 2019 for INC-578139
        sql = sql & "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, "
        sql = sql & "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT,COMPLAINT_CD, "

        If Session("str_co_grp_cd").ToString = "BRK" Then 'Alice added on Feb 21, 2019 for request 4477
            sql = sql & "'HOU" & Session("store_cd").ToString.Trim & "','', 100, 0, "
        Else
            sql = sql & "SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, "
        End If

        sql = sql & "COMM_ON_SETUP_CHG, COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, "
        sql = sql & "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, :TAX_CD, TAX_BASIS, :CUST_TAX_CHG, "
        sql = sql & "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, :PU_DEL_DT, :origSalLn "
        sql = sql & "FROM SO_LN WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN#"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":SO_DOC_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":NEW_QTY", OracleType.Number)
        objSql.Parameters.Add(":NEW_DEL_DOC_LN#", OracleType.Number)
        objSql.Parameters.Add(":DEL_DOC_LN#", OracleType.Number)
        objSql.Parameters.Add(":PU_DEL_DT", OracleType.DateTime)
        objSql.Parameters.Add(":CUST_TAX_CHG", OracleType.Number)
        objSql.Parameters.Add(":origSalLn", OracleType.Number)
        objSql.Parameters.Add(":newPrc", OracleType.Number)
        objSql.Parameters.Add(":TAX_CD", OracleType.VarChar)

        objSql.Parameters(":NEW_DEL_DOC_NUM").Value = del_doc_num
        objSql.Parameters(":SO_DOC_NUM").Value = Left(del_doc_num, 11)
        objSql.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_existing.Text.Trim
        objSql.Parameters(":NEW_QTY").Value = CDbl(qty)
        objSql.Parameters(":newPrc").Value = CDbl(unitPrc)
        objSql.Parameters(":NEW_DEL_DOC_LN#").Value = CDbl(new_line_no)
        objSql.Parameters(":DEL_DOC_LN#").Value = CDbl(old_line_no)
        objSql.Parameters(":PU_DEL_DT").Value = FormatDateTime(cbo_pd_date.Value, vbShortDate)
        objSql.Parameters(":TAX_CD").Value = lnkUpdatedTax.Text.Trim
        If TaxUtils.isHdrTax("") OrElse isEmpty(cust_tax_chg) OrElse Not IsNumeric(cust_tax_chg) Then
            objSql.Parameters(":CUST_TAX_CHG").Value = System.DBNull.Value
        Else
            objSql.Parameters(":CUST_TAX_CHG").Value = CDbl(cust_tax_chg)
        End If
        ' if a credit to original del doc, then we want to capture the original sale line in the alt_doc_ln#, otherwise no
        If cred Then
            objSql.Parameters(":origSalLn").Value = CDbl(old_line_no)
        Else
            objSql.Parameters(":origSalLn").Value = System.DBNull.Value
        End If

        objSql.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Function getCustTaxChgFromOrigLn(ByVal line_no As String, ByVal qty As String, ByRef credTbl As DataTable) As Double

        Dim custTaxChg As Double = 0.0
        Dim custTaxStr As String = ""

        For Each credRow In credTbl.Rows

            If credRow("DEL_DOC_LN#") = line_no Then

                custTaxStr = credRow("cust_tax_chg").ToString + ""
                If (Not IsNothing(custTaxStr)) AndAlso IsNumeric(custTaxStr) AndAlso CDbl(custTaxStr) > 0.0 Then

                    custTaxChg = Math.Round(CDbl(custTaxStr) / (CDbl(credRow("QTY", DataRowVersion.Original).ToString)) * CDbl(qty), TaxUtils.Tax_Constants.taxPrecision, MidpointRounding.AwayFromZero)
                End If
            End If
        Next

        Return custTaxChg

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Determines if the passed-in warranty has been defined  in the system(ITM2ITM_WARR table)
    ''' with the specified warrantable sku. 
    ''' </summary>
    ''' <param name="itmCd">item code of the warrantable line</param>
    ''' <param name="warrantyItmCd">>item code of the warranty line</param>
    ''' <returns>a flag indicating if a match was found for the specified warranty and warrantable skus</returns>
    Private Function IsValidWarrantyForItem(ByVal itmCd As String, ByVal warrantyItmCd As String) As Boolean

        Dim rtnVal As Boolean = False

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As New StringBuilder()
        sql.Append(" SELECT war_itm_cd ")
        sql.Append("   FROM ITM2ITM_WARR ")
        sql.Append("  WHERE itm_cd = :itmCd AND war_itm_cd = :warrItmCd ")

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = itmCd
        cmd.Parameters.Add(":warrItmCd", OracleType.VarChar)
        cmd.Parameters(":warrItmCd").Value = warrantyItmCd
        Dim rdr As OracleDataReader

        Try
            conn.Open()
            rdr = DisposablesManager.BuildOracleDataReader(cmd)

            If rdr.Read() Then
                rtnVal = (Not IsDBNull(rdr.Item("war_itm_cd").ToString)) AndAlso rdr.Item("war_itm_cd").ToString.isNotEmpty
            End If
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function

    ''' <summary>
    ''' Returns the current line number
    ''' </summary>
    Private Function GetCurrentLineNum() As Integer

        Dim currLineNum As Integer = 0

        If Not IsNothing(Session("SAL_LN")) Then

            Dim datSet As DataSet = Session("SAL_LN")
            Dim datTbl As DataTable = datSet.Tables(0)
            Dim datRow As DataRow

            'find max del_doc_ln# in table
            For Each datRow In datSet.Tables(0).Rows

                If IsNumeric(datRow("DEL_DOC_LN#")) AndAlso
                    datRow("DEL_DOC_LN#") > currLineNum Then

                    currLineNum = datRow("DEL_DOC_LN#")
                End If
            Next
        End If

        Return currLineNum

    End Function

    ''' <summary>
    ''' Updates the warranty link of the warrantable sku based on the passed-in values. This could be because a new or 
    ''' existing warranty was selected.
    ''' </summary>
    ''' <param name="warrantySku">the warranty sku to be linked</param>
    ''' <param name="warrantySkuLnNum">the line number of the warranty sku to be linked</param>
    ''' <param name="warrantableSkuLnNum">the line number of the warrantable sku to which this warranty needs to be linked</param>
    Private Sub UpdateWarrantyLinks(ByVal warrantySku As String, ByVal warrantySkuLnNum As String, ByVal warrantableSkuLnNum As String)
        '**** Now update the dataset itself
        Dim soLnDataSet As DataSet = Session("SAL_LN")
        Dim soLnDatTbl As DataTable = soLnDataSet.Tables(0)
        Dim soLnRow As DataRow
        For Each soLnRow In soLnDatTbl.Rows

            If soLnRow("DEL_DOC_LN#") = warrantableSkuLnNum Then
                'finds the warrantied row and update the warranty link on it

                soLnRow("WARR_BY_LN") = CInt(warrantySkuLnNum)
                soLnRow("WARR_BY_SKU") = warrantySku
                soLnRow("WARR_BY_DOC_NUM") = txt_del_doc_num.Text
                soLnRow("WARRANTABLE") = "Y"
                Exit For
            End If
        Next
    End Sub

    Private Sub UnlinkWarranty(ByVal warrantableSkuLnNum As String)
        '**** Now update the dataset itself
        Dim soLnDataSet As DataSet = Session("SAL_LN")
        Dim soLnDatTbl As DataTable = soLnDataSet.Tables(0)
        Dim soLnRow As DataRow
        For Each soLnRow In soLnDatTbl.Rows

            If soLnRow("DEL_DOC_LN#") = warrantableSkuLnNum Then
                'finds the warrantied row and update the warranty link on it

                soLnRow("WARR_BY_LN") = System.DBNull.Value
                soLnRow("WARR_BY_SKU") = System.DBNull.Value
                soLnRow("WARR_BY_DOC_NUM") = System.DBNull.Value
                soLnRow("WARRANTABLE") = "N"
                Exit For
            End If
        Next
    End Sub
    Private Sub InsertWarrantySoLn(strDelDocNum As String, dblLineNum As Double, strNewDelDocNum As String, dblNewLineNum As Double)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Try
            If conn.State <> ConnectionState.Open Then
                conn.Open()
            End If

            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            cmd.Connection = conn
            cmd.CommandText = "bt_sale.insert_soln_warr_rec"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New OracleParameter("warr_del_doc_num_i", OracleType.VarChar)).Value = strDelDocNum
            cmd.Parameters.Add(New OracleParameter("warr_del_doc_ln#_i", OracleType.Number)).Value = dblLineNum
            cmd.Parameters.Add(New OracleParameter("del_doc_num_i", OracleType.VarChar)).Value = strNewDelDocNum
            cmd.Parameters.Add(New OracleParameter("del_doc_ln#_i", OracleType.Number)).Value = dblNewLineNum

            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            conn.Close()
        End Try
    End Sub

    Private Sub UpdateWarrantySoLn(strDelDocNum As String, dblLineNum As Double, strNewDelDocNum As String, dblNewLineNum As Double)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Try
            If conn.State <> ConnectionState.Open Then
                conn.Open()
            End If

            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            cmd.Connection = conn
            cmd.CommandText = "bt_sale.update_soln_warr_rec"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New OracleParameter("orig_del_doc_num_i", OracleType.VarChar)).Value = strDelDocNum
            cmd.Parameters.Add(New OracleParameter("orig_del_doc_ln#_i", OracleType.Number)).Value = dblLineNum
            cmd.Parameters.Add(New OracleParameter("new_del_doc_num_i", OracleType.VarChar)).Value = strNewDelDocNum
            cmd.Parameters.Add(New OracleParameter("new_del_doc_ln#_i", OracleType.Number)).Value = dblNewLineNum

            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            conn.Close()
        End Try
    End Sub

    Protected Sub btnDateLookup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDateLookup.Click
        If lbl_cust_cd.Text & "" = "" Then
            lbl_error.Text = Resources.POSMessages.MSG0041
            Exit Sub
        ElseIf cbo_PD.SelectedValue = AppConstants.PICKUP Then
            lbl_error.Text = Resources.POSMessages.MSG0042 '"Sorry, push the down arrow on left to find available pickup date calendar.  Delivery dates unavailable for pickup."
            Exit Sub
        End If

        Dim hidZone As HiddenField = CType(UcDeliveryDatePopup.FindControl("hidZone"), HiddenField)
        hidZone.Value = txtNewZone.Text.Trim.ToUpper
        PopupDeliveryDate.ShowOnPageLoad = True
        UcDeliveryDatePopup.GetDeliveryDates(String.Empty)

    End Sub


    Protected Sub PopupZone_Load(sender As Object, e As EventArgs)

        Dim hidZipcode As HiddenField = CType(UcZonePopup.FindControl("hidZip"), HiddenField)

        Dim hidSaleType As HiddenField = CType(UcZonePopup.FindControl("hidSaleType"), HiddenField)
        Dim hidZoneCode As HiddenField = CType(UcZonePopup.FindControl("hidZoneCode"), HiddenField)
        Dim txtzip As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(UcZonePopup.FindControl("txtzip"), DevExpress.Web.ASPxEditors.ASPxTextBox)
        txtzip.Text = String.Empty
        'Get store Zip code so if sale type changes then according to sale type Zip code should populate
        If String.IsNullOrEmpty(hidStoreZipCode.Value) Then
            hidStoreZipCode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
        End If

        'Set written the store code zip code
        If Not String.IsNullOrEmpty(hidWrittenStoreCode.Value) Then
            hidWrittenStoreZipCode.Value = SalesUtils.getStoreZipCode(hidWrittenStoreCode.Value, lbl_cust_cd.Text)
        Else
            hidWrittenStoreZipCode.Value = String.Empty
        End If

        If cbo_PD.SelectedValue.Equals("P") Then
            If String.IsNullOrEmpty(hidWrittenStoreZipCode.Value) Then
                hidZipcode.Value = hidStoreZipCode.Value
            Else
                hidZipcode.Value = hidWrittenStoreZipCode.Value
            End If
        Else
            hidZipcode.Value = txtShipToZip.Text
        End If

        'if Zipcode is not there that means page is loading for first time so set value according to sale type
        If String.IsNullOrEmpty(txtzip.Text) Then
            txtzip.Text = hidZipcode.Value
        ElseIf txtShipToZip.Text = txtzip.Text Then
            'If sale is type of Delivery then set the value of customer zip code
            txtzip.Text = txtShipToZip.Text
        ElseIf hidStoreZipCode.Value = txtzip.Text Then
            'If sale is type of Pick Up then set the value of store zip code
            txtzip.Text = hidStoreZipCode.Value
        ElseIf hidWrittenStoreZipCode.Value = txtzip.Text Then
            txtzip.Text = hidWrittenStoreZipCode.Value
        End If

    End Sub


    Protected Sub txt_setup_TextChanged(sender As Object, e As EventArgs) Handles txt_setup.TextChanged
        If (txt_setup.Text.isNotEmpty) Then
            txt_setup.Text = FormatNumber(txt_setup.Text, 2)
        Else
            txt_setup.Text = FormatNumber(0, 2)
        End If
        calculate_total(CalculateTotal.CRM)
    End Sub

    Protected Sub txt_del_chg_TextChanged(sender As Object, e As EventArgs) Handles txt_del_chg.TextChanged
        If (txt_del_chg.Text.isNotEmpty) Then
            txt_del_chg.Text = FormatNumber(txt_del_chg.Text, 2)
        Else
            txt_del_chg.Text = FormatNumber(0, 2)
        End If
        calculate_total(CalculateTotal.CRM)
    End Sub


    Protected Sub txtNewSetupChg_TextChanged(sender As Object, e As EventArgs) Handles txtNewSetupChg.TextChanged
        If (txtNewSetupChg.Text.isNotEmpty) Then
            txtNewSetupChg.Text = FormatNumber(txtNewSetupChg.Text, 2)
        Else
            txtNewSetupChg.Text = FormatNumber(0, 2)
        End If
        calculate_total(CalculateTotal.NewSale)
    End Sub

    Protected Sub txtNewDeliveryChgs_TextChanged(sender As Object, e As EventArgs) Handles txtNewDeliveryChgs.TextChanged
        If (txtNewDeliveryChgs.Text.isNotEmpty) Then
            txtNewDeliveryChgs.Text = FormatNumber(txtNewDeliveryChgs.Text, 2)
        Else
            txtNewDeliveryChgs.Text = FormatNumber(0, 2)
        End If
        calculate_total(CalculateTotal.NewSale)
    End Sub


    Private Sub AddLine(SKUEntry As String, qtyEntry As String, unitPriceEntry As String)
        ResetShowApprovalPopup()
        If String.IsNullOrEmpty(SKUEntry) Then
            lbl_error.Text = Resources.SalesExchange.Label08
            Exit Sub
        End If
        Dim priceChangeMessage As String = String.Empty
        Dim quantity As Integer = 0
        Dim unitPrice As Double = 0
        Integer.TryParse(qtyEntry, quantity)
        Double.TryParse(unitPriceEntry, unitPrice)
        If quantity < 1 Then quantity = 1

        Dim showWarrPopup As Boolean = False
        Dim currLine As Integer = 0
        Dim dsSaleLn As New DataSet
        Dim dtSaleLn As New DataTable

        If Not IsNothing(Session("SAL_LN")) Then
            dsSaleLn = Session("SAL_LN")
            If dsSaleLn.Tables.Count > 0 Then dtSaleLn = dsSaleLn.Tables(0)

            For Each dr As DataRow In dtSaleLn.Rows
                If IsNumeric(dr("DEL_DOC_LN#")) Then
                    If dr("DEL_DOC_LN#") > currLine Then
                        currLine = dr("DEL_DOC_LN#")
                    End If
                End If
            Next
        End If

        If currLine < 1 Then
            currLine = theSalesBiz.GetCurrDelDocLine(Left(txt_del_doc_num.Text, 11))
        End If

        Dim SKUs() As String = Array.ConvertAll(SKUEntry.TrimStart(",").TrimEnd(",").Split(","c), Function(p) p.Trim())
        Dim validSKUs() As String = SKUs

        For Each SKU As String In SKUs
            If Not theSkuBiz.IsSellable(SKU) Then
                lbl_error.Text = "Please type a valid SKU"
                validSKUs = validSKUs.Where(Function(w) w <> SKU).ToArray()
            End If
        Next

        Dim dtSKUs As DataTable = theSkuBiz.GetItemsDetail(validSKUs)
        If dtSKUs.Rows.Count < validSKUs.Length Then
            Dim duplicates = validSKUs.GroupBy(Function(p) p).Where(Function(g) g.Count() > 1).[Select](Function(g) g.Key)
            For Each dupStr In duplicates
                Dim dupCnt = validSKUs.Where(Function(c) c = dupStr).ToArray().Length
                If dupCnt > 1 Then
                    For a = 0 To dupCnt - 2
                        Dim result() As DataRow = dtSKUs.Select("ITM_CD = '" + dupStr + "'")
                        dtSKUs.ImportRow(result(0))
                    Next
                End If
            Next
        End If

        Dim dsSortCd As DataSet = theSkuBiz.GetItemSortCodes(New ArrayList(validSKUs))
        Dim dtSortCd As DataTable = IIf(dsSortCd.Tables.Count > 0, dsSortCd.Tables(0), Nothing)

        Dim retPrice As Double = 0
        Dim drSaleLn As DataRow

        Dim pkgSKUs As String = ""
        Dim pkgLN As Integer = 0

        currLine = currLine + 1

        Dim margin As Double = 0
        If IsNumeric(ConfigurationManager.AppSettings("margin").ToString) Then
            margin = CDbl(ConfigurationManager.AppSettings("margin").ToString)
        End If
        'Dim isMarginNotMet As Boolean = (margin > 0 AndAlso determine_gm(txt_new_itm_cd.Text.Trim, txt_new_unit_prc.Text) < margin)
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        'We need to prompt for approval only for the SAL type of sale. Since the queried order is only of type SAL, that condition check is ignored for Exchange!
        Dim promptForApproval As Boolean = AppConstants.ManagerOverride.BY_LINE = prcOverride OrElse
                                           AppConstants.ManagerOverride.BY_ORDER = prcOverride


        Dim origPrice As Double = 0
        Dim maxDiscPerc As Double = 0
        Dim marginAmount As Double = 0
        Dim maxDiscAmt As Decimal = 0
        Dim isMarginNotMet As Boolean = True
        For Each drSKU As DataRow In dtSKUs.Rows
            If Not IsNothing(dtSaleLn) Then
                drSaleLn = dtSaleLn.NewRow
                retPrice = CDbl(drSKU("RET_PRC").ToString)

                retPrice = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                     Session("store_cd").ToString.Trim,
                                                     String.Empty,
                                                     FormatDateTime(Today.Date, DateFormat.ShortDate),
                                                     drSKU("ITM_CD").ToString,
                                                     retPrice)
                'if the unit price is zero, then set the retail price from the package call as unit price
                If CDbl(unitPrice) = CDbl(0) Then
                    unitPrice = retPrice
                ElseIf CDbl(unitPrice) <> CDbl(retPrice) Then
                    Decimal.TryParse(drSKU("MAX_DISC_PCNT") & "", maxDiscPerc)
                    maxDiscAmt = retPrice - (retPrice * (maxDiscPerc / 100))
                    maxDiscAmt = Math.Round(maxDiscAmt, 2)  'Alice added on June 6, 2019, the issue was found during test 4168
                    If (maxDiscAmt > unitPrice) Then
                        lbl_error.Text = String.Format(Resources.POSErrors.ERR0046, drSKU("ITM_CD").ToString, FormatCurrency(maxDiscAmt, 2))
                        unitPrice = retPrice
                    Else
                        'Get the margin amount for the item
                        marginAmount = determine_gm(drSKU("ITM_CD").ToString, retPrice)
                        isMarginNotMet = (margin > 0 AndAlso marginAmount < margin)
                        If promptForApproval OrElse isMarginNotMet Then
                            'if need approval, then set the flag to N
                            drSaleLn("IsApproved") = "N"
                            'hidShowApprovalPopup.Value = "Y"
                            'Flag the view state so that the the popup is shown
                            priceChangeMessage = String.Format(Resources.POSMessages.MSG0058, drSKU("ITM_CD"), retPrice)
                            SetShowApprovalPopup()
                        Else
                            drSaleLn("IsApproved") = "Y"
                        End If
                    End If
                Else
                    drSaleLn("IsApproved") = "Y"
                End If

                drSaleLn("DEL_DOC_NUM") = Trim(txt_del_doc_num.Text)
                drSaleLn("DEL_DOC_LN#") = currLine
                drSaleLn("ITM_CD") = drSKU("ITM_CD").ToString
                drSaleLn("COMM_CD") = drSKU("COMM_CD").ToString
                drSaleLn("QTY") = quantity
                drSaleLn("VE_CD") = drSKU("VE_CD").ToString
                drSaleLn("ITM_TP_CD") = drSKU("ITM_TP_CD").ToString
                drSaleLn("SERIAL_TP") = drSKU("SERIAL_TP").ToString
                drSaleLn("INVENTORY") = drSKU("INVENTORY").ToString
                drSaleLn("WARRANTABLE") = drSKU("WARRANTABLE").ToString
                drSaleLn("WARR_DAYS") = CDbl(drSKU("WARR_DAYS").ToString)
                drSaleLn("OriginalRetailPrice") = CDbl(retPrice)


                Dim drFilered() As DataRow = dtSortCd.Select("ITM_CD = '" & drSKU("ITM_CD").ToString() & "'")
                If drFilered.Length > 0 Then drSaleLn("ITM_SRT_DESC") = drFilered(0)("DESCRIPTION")

                If drSKU("POINT_SIZE").ToString.isNotEmpty AndAlso
                        IsNumeric(drSKU("POINT_SIZE")) AndAlso
                        CDbl(drSKU("POINT_SIZE")) > 0 Then
                    drSaleLn("DELIVERY_POINTS") = CDbl(drSKU("POINT_SIZE").ToString)
                End If

                If drSKU("ITM_TP_CD").ToString = "PKG" And "Y".Equals(ConfigurationManager.AppSettings("PACKAGE_BREAKOUT").ToString + "") Then
                    drSaleLn("UNIT_PRC") = 0
                    drSaleLn("IsApproved") = "Y"
                    ResetShowApprovalPopup()
                Else
                    drSaleLn("UNIT_PRC") = unitPrice
                End If

                If drSKU("ITM_TP_CD").ToString = "PKG" Then
                    pkgSKUs = pkgSKUs & "'" & drSKU("ITM_CD").ToString & "',"
                    pkgLN = currLine
                End If

                drSaleLn("VOID_FLAG") = "N"
                drSaleLn("VSN") = drSKU("VSN").ToString
                drSaleLn("DES") = drSKU("DES").ToString
                drSaleLn("NEW_LINE") = "Y"

                If drSKU("INVENTORY").ToString = "Y" Then
                    drSaleLn("PICKED") = "Z"
                Else
                    drSaleLn("PICKED") = "X"
                End If

                If IsNumeric(drSKU("SPIFF")) Then
                    If CDbl(drSKU("SPIFF")) > 0 Then
                        drSaleLn("SPIFF") = drSKU("SPIFF")
                    End If
                End If

                dtSaleLn.Rows.Add(drSaleLn)

                currLine = currLine + 1
            End If

            unitPrice = 0
            Dim isWarrantieFlag As Boolean = IIf("Y".Equals(drSKU("INVENTORY") + ""), True, False) AndAlso
                IIf("Y".Equals(drSKU("WARRANTABLE") + ""), True, False)

            If Not showWarrPopup Then
                showWarrPopup = (validSKUs.Length > 0) AndAlso
                                isWarrantieFlag AndAlso SalesUtils.hasWarranty(drSKU("ITM_CD"))
            End If
        Next

        Dim pkgCmpts As New ArrayList
        If pkgSKUs & "" <> "" Then
            Dim theSoHdrInfo As SoHdrExchangeInfo = Session("soHdrInfo")
            Dim addPkgCmpt As New CreatePkgCmpntLnReq
            addPkgCmpt.pkgSku = pkgSKUs

            If Not IsNothing(dtSaleLn) AndAlso dtSaleLn.Rows.Count > 0 Then
                addPkgCmpt.nextLnNum = currLine 'dtSaleLn.Rows.Count + 1
            End If

            addPkgCmpt.pkgLnNum = pkgLN
            addPkgCmpt.slsp1 = theSoHdrInfo.slsp1
            addPkgCmpt.pctOfSale1 = theSoHdrInfo.slspPct1
            addPkgCmpt.slsp2 = theSoHdrInfo.slsp2
            addPkgCmpt.pctOfSale2 = theSoHdrInfo.slspPct2

            Dim isWarrantableComps As Boolean = AddPackages(addPkgCmpt, pkgCmpts)
            If Not showWarrPopup AndAlso isWarrantableComps Then showWarrPopup = isWarrantableComps

            pkgSKUs = String.Empty
        End If
        ViewState("ShowWarrentyPopup") = showWarrPopup
        hidIsWarrChecked.Value = False
        Session("SAL_LN") = dsSaleLn
        If validSKUs.Length > 0 Then
            hidItemCd.Value = hidItemCd.Value & "," & String.Join(",", validSKUs.AsEnumerable())
        End If

        If Not IsNothing(pkgCmpts) AndAlso pkgCmpts.Count > 0 Then
            hidItemCd.Value = hidItemCd.Value & "," & String.Join(",", pkgCmpts.ToArray())
        End If
        If prcOverride.Equals(AppConstants.ManagerOverride.BY_LINE) AndAlso ViewState("ShowApprovalPopup") & "" = "Y" Then
            GetApprovalPopup(priceChangeMessage)
        Else
            ShowOtherPopups()
        End If
    End Sub
    ''' <summary>
    ''' this method will bringup the other popup's
    ''' </summary>
    Private Sub ShowOtherPopups()
        If ViewState("ShowWarrentyPopup") IsNot Nothing AndAlso ViewState("ShowWarrentyPopup") = True Then ShowWarrantyPopup(String.Empty)

        If hidIsWarrChecked.Value = False Then
            Dim relatedSku As String = hidItemCd.Value
            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub
    ''' <summary>
    ''' this method will bringup the warranty popup
    ''' </summary>
    ''' <param name="focusedRowId"></param>
    Private Sub ShowWarrantyPopup(focusedRowId As String)
        Dim dsSaleLn As New DataSet
        Dim dtSaleLn As New DataTable

        If Not IsNothing(Session("SAL_LN")) Then
            dsSaleLn = Session("SAL_LN")
            If dsSaleLn.Tables.Count > 0 Then dtSaleLn = dsSaleLn.Tables(0)
            Dim strFilter As String = "ITM_CD IS NOT NULL AND VOID_FLAG ='" + "N" + "' AND ITM_TP_CD = '" & SkuUtils.ITM_TP_WAR & "'"
            Dim warrantyRows() As DataRow = dtSaleLn.Select(strFilter)

            Dim tempWarrObj As TempWarrDtc
            Dim itemCd As String = String.Empty
            Dim isInventory As Boolean = False
            Dim isWarrantable As Boolean = False

            Dim dtWarrantable As New DataTable
            dtWarrantable.Columns.Add("ROW_ID", GetType(String))
            dtWarrantable.Columns.Add("ITM_CD", GetType(String))
            dtWarrantable.Columns.Add("ITM_TP_CD", GetType(String))
            dtWarrantable.Columns.Add("WARR_ROW_ID", GetType(String))
            dtWarrantable.Columns.Add("WARR_ITM_CD", GetType(String))
            dtWarrantable.Columns.Add("WARR_ITM_TEXT", GetType(String))
            dtWarrantable.Columns.Add("IS_PREV_SEL", GetType(Boolean))
            Dim drWarrantable As DataRow

            For Each drSaleLn As DataRow In dtSaleLn.Rows
                If Not (IsNothing(drSaleLn("ITM_CD")) OrElse IsDBNull(drSaleLn("ITM_CD"))) Then
                    itemCd = drSaleLn("ITM_CD")
                    isInventory = IIf("Y".Equals(drSaleLn("INVENTORY") + ""), True, False)
                    isWarrantable = IIf("Y".Equals(drSaleLn("WARRANTABLE") + ""), True, False)

                    If (SalesUtils.hasWarranty(itemCd)) Then
                        If SysPms.isOne2ManyWarr Then
                            For i As Integer = 0 To warrantyRows.GetUpperBound(0)
                                Dim drWarr As DataRow = (warrantyRows(i))

                                If (IsValidWarrantyForItem(itemCd, drWarr("ITM_CD").ToString)) Then
                                    tempWarrObj = New TempWarrDtc()
                                    tempWarrObj.sessId = drWarr("DEL_DOC_NUM")  'TODO:AXK - DSA to confirm if this should be changed to Session.SessionID.ToString
                                    tempWarrObj.itmCd = drWarr("ITM_CD").ToString
                                    tempWarrObj.lnNum = drWarr("DEL_DOC_LN#").ToString
                                    tempWarrObj.retPrc = CDbl(drWarr("unit_prc"))
                                    tempWarrObj.vsn = drWarr("VSN").ToString
                                    tempWarrObj.days = drWarr("WARR_DAYS")
                                    HBCG_Utils.CreateTempWarr(tempWarrObj)
                                End If
                            Next i
                        End If

                        If isInventory And isWarrantable Then
                            drWarrantable = dtWarrantable.NewRow()
                            drWarrantable("ROW_ID") = drSaleLn("DEL_DOC_LN#")
                            drWarrantable("ITM_CD") = itemCd
                            drWarrantable("ITM_TP_CD") = drSaleLn("ITM_TP_CD")
                            drWarrantable("WARR_ROW_ID") = drSaleLn("WARR_BY_LN")
                            drWarrantable("WARR_ITM_CD") = drSaleLn("WARR_BY_SKU")

                            If dtWarrantable.Rows.Count > 0 Then
                                Dim filterExp As String = "IS_PREV_SEL = FALSE AND WARR_ROW_ID = '" & drSaleLn("WARR_BY_LN") & "' AND WARR_ITM_CD = '" & drSaleLn("WARR_BY_SKU") & "'"
                                Dim drFiltered As DataRow() = dtWarrantable.Select(filterExp)
                                drWarrantable("IS_PREV_SEL") = drFiltered.Length > 0
                            Else
                                drWarrantable("IS_PREV_SEL") = False
                            End If

                            dtWarrantable.Rows.Add(drWarrantable)
                        End If
                    End If
                End If
            Next

            If dtWarrantable.Rows.Count > 0 Then
                hidIsWarrChecked.Value = True

                Dim _popupWarranties As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMultiWarranties.FindControl("popupWarranties")
                _popupWarranties.Modal = True

                Dim _hidDelDocNum As HiddenField = _popupWarranties.FindControl("hidDelDocNum")
                _hidDelDocNum.Value = String.Empty

                Dim _hidFocusItem As HiddenField = _popupWarranties.FindControl("hidFocusItem")
                _hidFocusItem.Value = IIf(String.IsNullOrEmpty(focusedRowId), GetCurrentLineNum(), focusedRowId)

                _popupWarranties.ShowOnPageLoad = True
                ucMultiWarranties.LoadWarranties(dtWarrantable)
            End If
        End If
    End Sub

    Protected Sub ucMultiWarranties_WarrantyAdded(sender As Object, e As EventArgs) Handles ucMultiWarranties.WarrantyAdded
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarrInfo As DataTable = CType(Session("WARRINFO"), DataTable)
            Dim sortExp As String = "WARR_ROW_ID, IS_PREV_SEL"
            Dim dt As DataTable = dtWarrInfo.Select(Nothing, sortExp).CopyToDataTable()

            Dim warrRowId As String = String.Empty
            Dim warrItmCd As String
            Dim drWarrRowId As DataRow()

            For Each dr As DataRow In dt.Rows
                warrItmCd = String.Empty

                If Not IsDBNull(dr("WARR_ITM_CD")) AndAlso Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                    warrItmCd = dr("WARR_ITM_CD")
                End If

                'Need to fill the WARR_ROW_ID from warrRowIdSeqNum
                If Not IsDBNull(dr("IS_PREV_SEL")) AndAlso dr("IS_PREV_SEL") = True Then
                    drWarrRowId = dt.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" + warrItmCd + "'")

                    If drWarrRowId.Length > 0 Then
                        dr("WARR_ROW_ID") = drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID")
                    Else
                        dr("WARR_ROW_ID") = String.Empty
                    End If
                End If

                '***Check for Warranties being added or not after returning from warranties popup *******
                If Not String.IsNullOrEmpty(warrItmCd) Then
                    If String.IsNullOrEmpty(dr("WARR_ROW_ID")) Then
                        '*** Implies that a new warranty sku is being added after the popup selection. In this case a new 
                        '*** line for the warranty sku needs to be added. Then, the link info on the warrantable sku 
                        '*** also needs to be updated.
                        If Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                            '--means a warrantable sku was selected
                            'Dim createLnReq As HBCG_Utils.CreateTempItmReq = New CreateTempItmReq()
                            'createLnReq.itmCd = dr("WARR_ITM_CD")
                            'createLnReq.warrItmLink = dr("ITM_CD")
                            'createLnReq.warrRowLink = CInt(dr("ROW_ID"))

                            'warrRowIdSeqNum = HBCG_Utils.CreateWarrantyLn(createLnReq)
                            'dr("WARR_ROW_ID") = warrRowIdSeqNum

                            AddLine(warrItmCd, String.Empty, String.Empty)
                            warrRowId = GetCurrentLineNum()
                            dr("WARR_ROW_ID") = warrRowId
                            UpdateWarrantyLinks(warrItmCd, warrRowId, dr("ROW_ID"))
                        End If
                    Else
                        '*** Means that an existing warranty was selected, so just update the link info on the warrantable
                        '*** sku. No need to add a separate warranty line.
                        'UpdtWarrLnk(warrItmCd, dr("WARR_ROW_ID"), dr("ROW_ID"))
                        UpdateWarrantyLinks(warrItmCd, dr("WARR_ROW_ID"), dr("ROW_ID"))
                    End If
                Else
                    UnlinkWarranty(dr("ROW_ID"))

                End If
            Next
            bindgrid()
            calculate_total()
        End If

        hidIsWarrChecked.Value = False

        If Not String.IsNullOrEmpty(hidItemCd.Value) Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty
            ShowRelatedSkuPopUp(relatedSku)
        End If

        Session.Remove("WARRINFO")
    End Sub

    Protected Sub ucMultiWarranties_NoWarrantyAdded(sender As Object, e As EventArgs) Handles ucMultiWarranties.NoWarrantyAdded
        If Not String.IsNullOrEmpty(hidItemCd.Value) Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
        Else
            bindgrid()
            calculate_total()
        End If
        hidIsWarrChecked.Value = False

    End Sub
    ''' <summary>
    ''' this method sets the view state  parameter to Y indicating that the popup is to be shown
    ''' </summary>
    Private Sub SetShowApprovalPopup()
        ViewState("ShowApprovalPopup") = "Y"
    End Sub
    ''' <summary>
    ''' this method sets the view state  parameter to N indicating that the popup is Not to be shown
    ''' </summary>
    Private Sub ResetShowApprovalPopup()
        ViewState("ShowApprovalPopup") = "N"
    End Sub
    Private Sub ShowRelatedSkuPopUp(relatedSku As String)
        If Not String.IsNullOrEmpty(relatedSku) Then
            ucRelatedSKU.LoadRelatedSKUData(relatedSku.Replace("'", String.Empty))
            hidIsWarrChecked.Value = False
        End If
    End Sub

    Protected Sub ucRelatedSKU_AddSelection(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSKU.AddSelection
        hidItemCd.Value = String.Empty
        Dim skus As String = String.Empty

        For Each obj As Object In selectedRelSKUs
            If Not IsNothing(obj) AndAlso Not String.IsNullOrEmpty(obj) Then
                skus = skus & "," & UCase(obj.ToString())
            End If
        Next

        If Not String.IsNullOrEmpty(skus) Then
            AddLine(skus, String.Empty, String.Empty)

            bindgrid()
            calculate_total()
        End If
    End Sub

    Protected Sub ucRelatedSKU_NoneAdded(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSKU.NoneAdded
        hidItemCd.Value = String.Empty
        bindgrid()
        calculate_total()
    End Sub

    ''' <summary>
    ''' Executes when the manager override is successfully carried out via the popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved
        'When approved!!
        ApprovePriceChange()
        ShowOtherPopups()
    End Sub
    ''' <summary>
    ''' This method approves the records
    ''' </summary>
    Private Sub ApprovePriceChange()
        Dim ds As DataSet = Session("SAL_LN")
        For Each row As DataRow In ds.Tables(0).Rows
            If row("IsApproved") & "" = "N" Then
                row("IsApproved") = "Y"
            End If
        Next
        Session("SAL_LN") = ds
        bindgrid()
        popupMgrOverride.ShowOnPageLoad = False
        'ProcessSaveAfterApproval()
    End Sub

    ''' <summary>
    ''' Gets fired when the manager override for 'line level' fails. Any resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled
        'When Cancelled!!
        CancelPriceChange()
        ShowOtherPopups()
    End Sub
    ''' <summary>
    ''' This Method will revert the new price with the actual price of the item
    ''' </summary>
    Private Sub CancelPriceChange()
        Dim ds As DataSet = Session("SAL_LN")
        For Each row As DataRow In ds.Tables(0).Rows
            If row("IsApproved") & "" = "N" Then
                row("IsApproved") = "Y"
                'replace the unit price with the original retail price!
                row("UNIT_PRC") = row("OriginalRetailPrice")
            End If
        Next
        Session("SAL_LN") = ds
        bindgrid()
        popupMgrOverride.ShowOnPageLoad = False
        'ProcessSaveAfterApproval()
    End Sub

    ''' <summary>
    ''' Line level manager approval popup display.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub MgrByLineApprovalPopup(ByVal popupMessage As String)
        ''means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
        Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = popupMgrOverride.FindControl("txtMgrPwd")
        If txtMgrPwd IsNot Nothing Then
            txtMgrPwd.Focus()
        End If
        ucMgrOverride.SetDisplayMessage(popupMessage)
        popupMgrOverride.ShowOnPageLoad = True
    End Sub


    ''' <summary>
    ''' Determines if the price on any lines on the order has been modified and if needed, prompts for a 
    ''' manager approval via the summary price approval popup. 
    ''' </summary>
    Private Sub PromptForSummaryPriceMgrOverride()

        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        If AppConstants.ManagerOverride.BY_ORDER = prcOverride Then

            If IsApprovalRequired() Then
                Dim dsSaleLn As DataSet = Session("SAL_LN")

                Dim table As DataTable = SalesUtils.GetOrderLinesTable()

                For Each row As DataRow In dsSaleLn.Tables(0).Rows
                    'gather the needed info to build the data table to pass to the price summary grid
                    If row IsNot Nothing AndAlso row("IsApproved") & "" = "N" Then

                        SalesUtils.AddOrderLineRow(table,
                                                    row("DEL_DOC_LN#") & "",
                                                    row("ITM_CD") & "",
                                                    row("DES") & "",
                                                    CDbl(row("QTY") & ""),
                                                    CDbl(row("OriginalRetailPrice") & ""),
                                                    CDbl(row("UNIT_PRC") & "")
                                                    )
                    End If
                Next
                'pass this datatable to the new summ price approval popup & display it.
                If table IsNot Nothing AndAlso table.Rows.Count > 0 Then
                    ucSummaryPrcChange.BindData(table)
                    ucSummaryPrcChange.SetDisplayMessage(Resources.POSMessages.MSG0059)
                    Dim popupPrcChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
                    popupPrcChangeAppr.ShowOnPageLoad = True
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Brings up the manager approval popup
    ''' </summary>
    Private Sub GetApprovalPopup(ByVal popupMessage As String)

        If IsApprovalRequired() Then
            Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
            If prcOverride.Equals(AppConstants.ManagerOverride.BY_LINE) Then
                MgrByLineApprovalPopup(popupMessage)
            ElseIf prcOverride.Equals(AppConstants.ManagerOverride.BY_ORDER) Then
                PromptForSummaryPriceMgrOverride()
            Else
                'no popup
            End If
        End If
    End Sub

    '
    ''' <summary>
    '''this method determines that any records exists for manager approval 
    ''' </summary>
    ''' <returns></returns>
    Private Function IsApprovalRequired() As Boolean
        Dim dsSaleLn As DataSet = Session("SAL_LN")
        Dim isApprovalNeeded As Boolean = False
        For Each row As DataRow In dsSaleLn.Tables(0).Rows
            'if rows found to be approved, bring up the popup
            If row("IsApproved") & "" = "N" Then
                isApprovalNeeded = True
                Exit For
            End If
        Next

        Return isApprovalNeeded
    End Function
    ''' <summary>
    ''' This method calls the sae metod to save the order. When some of the changes needs approval and user clicks on save changes, 
    ''' manager approval password popup is brought up with the flag Y set in hidInitiatedSaveSequence(Hidden field). Once manager approves this, order will procede to save.
    ''' </summary>
    Private Sub ProcessSaveAfterApproval()
        'If hidInitiatedSaveSequence.Value = "Y" Then
        If ViewState("InitiatedSaveSequence") & "" = "Y" Then
            SaveChanges()
        End If
    End Sub
    ''' <summary>
    ''' responds to the summary price change Aprroval
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCApproved(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCApproved
        ApprovePriceChange()
        ProcessSaveAfterApproval()
    End Sub
    ''' <summary>
    ''' responds to the summary price change Cancel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCCanceled(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCCanceled
        CancelPriceChange()
        ProcessSaveAfterApproval()
    End Sub

    Protected Sub ddlReturnReason_Load(sender As Object, e As EventArgs)
        PopulateReturnCauseCodes()
    End Sub

    Protected Sub txt_zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If String.IsNullOrEmpty(txt_zip.Text) Then
            lbl_error.Text = Resources.SalesExchange.Label09
            Exit Sub
        End If
        Dim showZone As Boolean = False
        If Not (AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso txt_del_doc_existing.Text + "" <> "") Then
            'Added the validation to avoid the tax popup in case of search.
            If Not String.IsNullOrEmpty(txt_del_doc_num.Text) Then
                showZone = determine_tax(txt_zip.Text)
            End If
        End If

        Update_City_State()
        'Added the validation to avoid the calculation in case of search.
        If Not String.IsNullOrEmpty(txt_del_doc_num.Text) Then
            calculate_total()
        End If
        If Not showZone Then
            PopulateZones(sender)
        End If
    End Sub

    Private Sub PopulateZones(ByVal sender As Object)
        Dim hidShowPopup As HiddenField = CType(UcZonePopup.FindControl("hidShowPopup"), HiddenField)
        hidShowPopup.Value = True
        Dim zipValue As String = ShowZonePopup()

        Dim rowsCount As Integer = UcZonePopup.UpdateZonesDisplay(zipValue)
        'Update zone code for new zip code
        UcZonePopup.UpdateZoneInfo(rowsCount, sender)
        If rowsCount = 0 AndAlso zipValue & "" = "" Then
            lbl_error.Text = String.Empty
            PopupDeliveryDate.ShowOnPageLoad = False
            PopupZone.ShowOnPageLoad = True
            Exit Sub
        End If
        If rowsCount > 1 Then
            'Empty zip/zone notification when ever store type changes
            lbl_error.Text = String.Empty
            'If multiple zones found then zone pop up should display first
            PopupDeliveryDate.ShowOnPageLoad = False
            PopupZone.ShowOnPageLoad = True
        Else
            If cbo_PD.SelectedValue = "D" Then
                PopupDeliveryDate.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Private Function ShowZonePopup() As String
        Dim hidZipcode As HiddenField = CType(UcZonePopup.FindControl("hidZip"), HiddenField)

        Dim txtzip As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(UcZonePopup.FindControl("txtzip"), DevExpress.Web.ASPxEditors.ASPxTextBox)
        If cbo_PD.SelectedValue = "P" Then
            'set zip code of selected store.
            hidZipcode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
        Else
            'If Sale is delivery type then customer code zip should get selected
            hidZipcode.Value = txt_zip.Text
        End If

        'When sale type is changed then Customer/Store zip code should get selected by default
        txtzip.Text = hidZipcode.Value
        Return txtzip.Text
    End Function

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(txt_zip.Text) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String

            conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & txt_zip.Text & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    txt_city.Text = MyDataReader.Item("CITY").ToString
                    cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                    conn.Close()
                End If
            Catch
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Public Function determine_tax(ByVal zip_cd As String, Optional ByVal conversion As String = "N") As Boolean
        Dim isTaxPopUpDisplayed As Boolean = False
        If cbo_tax_exempt.Value & "" = "" Then
            Dim sql As String = ""
            Dim dbConnection As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            Dim dbReader As OracleDataReader

            If Session("PD") = "P" Then
                If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                    Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                    zip_cd = store.addr.postalCd
                End If
            Else
                zip_cd = txt_zip.Text
            End If
            zip_cd = UCase(zip_cd)

            Dim Use_Written As String = "N"
            If zip_cd.isNotEmpty Then
                sql = "SELECT USE_WR_ST_TAX_ON_PU,USE_WR_ST_TAX_ON_DEL FROM ZIP WHERE ZIP_CD='" & zip_cd & "'"
                dbCommand.CommandText = sql

                Try
                    dbConnection.Open()
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If dbReader.Read() Then
                        If Session("PD") = "P" Then
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                        Else
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                        End If
                    End If
                    dbReader.Close()

                    If Use_Written = "Y" Then
                        sql = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
                        dbCommand.CommandText = sql

                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If dbReader.Read() Then
                            zip_cd = dbReader.Item("ZIP_CD")
                        End If
                        dbReader.Close()
                    End If
                Catch ex As Exception
                    Throw
                Finally
                    dbConnection.Close()
                End Try

                zip_cd = UCase(zip_cd)
                Dim MyTable As New DataTable
                Dim ds As DataSet = theSalesBiz.GetTaxCodesByZip(zip_cd)
                MyTable = ds.Tables(0)

                If (MyTable.Rows.Count = 1) Then
                    'if the ZIp code is mapped to only one tax code irr-respective of security the application will assign that tax code and no promt for tax.
                    Session("TAX_RATE") = MyTable.Rows(0).Item("SumofPCT").ToString
                    Session("TAX_CD") = MyTable.Rows(0).Item("TAX_CD").ToString
                    '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                    '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                    Session("MANUAL_TAX_CD") = Nothing

                    lbl_tax_rate.Text = Session("TAX_RATE")
                    lbl_tax_cd.Text = Session("TAX_CD")
                    lnkUpdatedTax.Text = lbl_tax_cd.Text
                    lnkTaxCode.Text = lbl_tax_cd.Text.Trim
                    ' Added the below code as a part of MM-4240
                    ' The below code was adding an "-" symbol in the txtTaxCd textbox so making this change
                    If Not (String.IsNullOrWhiteSpace(Session("TAX_CD"))) Then
                        txtTaxCd.Text = Session("TAX_CD") & " - " & MyTable.Rows(0).Item("DES").ToString
                    Else
                        txtTaxCd.Text = String.Empty
                    End If
                    'ElseIf SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                    '    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                    '    Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                    '    hidZipCD.Value = String.Empty
                    '    hidTaxDt.Value = FormatDateTime(Today.Date, vbShortDate) ' txt_wr_dt.Text.Trim()
                    '    ucTaxUpdate.LoadTaxData()
                    '    PopupControlTax.ShowOnPageLoad = True
                Else
                    'MM-7026
                    'if logged in user do not have security then we need to displsy security based on the zip.
                    Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                    hidIsInvokeManually.Value = "true"

                    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                    Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                    hidTaxDt.Value = FormatDateTime(Today.Date, vbShortDate) ' txt_wr_dt.Text.Trim()
                    If Session("PD") = "P" Then
                        If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                            Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                            hidZipCD.Value = store.addr.postalCd
                        End If
                    Else
                        hidZipCD.Value = zip_cd
                    End If
                    ucTaxUpdate.LoadTaxData()
                    PopupControlTax.ShowOnPageLoad = True
                    isTaxPopUpDisplayed = True
                End If
            End If
        Else
            lbl_tax_rate.Text = ""
            lbl_tax_cd.Text = ""
            txt_sal_taxes.Text = "0.00"
        End If
        Return isTaxPopUpDisplayed
    End Function
    'This method isn't in use, because panel taxExcepmtion is invisible -- Alice comments on Mar 08, 2019
    Protected Sub cbo_tax_exempt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_tax_exempt.SelectedIndexChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.UpdateRequest(txt_del_doc_existing.Text, Session("browserID").ToString(), " Changed the Tax exemption code ")
            End If
            txt_exempt_id.Text = String.Empty
            If cbo_tax_exempt.SelectedIndex <> 0 Then
                txtTaxCd.Text = ""
                Session("TAX_CD") = Nothing
                ViewState("TaxMethod") = Nothing
                ViewState("TaxExempted") = True
                txt_exempt_id.Text = String.Empty
                lbl_tax_cd.Text = ""
                determine_tax(txt_zip.Text)   ' TODO - consider - if they change this, then all bets are off
                calculate_total()
                txt_exempt_id.Focus()
                'Else
                '    Session("TAX_CD") = txtTaxCd.Text
                '    ViewState("TaxExempted") = False
                '    'Added the below code as a part of MM-4240
                '    If (Not (Convert.ToBoolean(ViewState("CanModifyTax")))) Then
                '        Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
                '        ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
                '        msgPopup.ShowOnPageLoad = True
                '    Else
                '        Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                '        hidIsInvokeManually.Value = "true"
                '        Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)

                '        '  If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                '        Dim hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                '        hidZip.Value = txt_zip.Text.Trim 'String.Empty
                '        ' End If
                '        hidTaxDt.Value = FormatDateTime(Today.Date, vbShortDate) 'txt_wr_dt.Text.Trim()
                '        ucTaxUpdate.LoadTaxData()
                '        PopupControlTax.ShowOnPageLoad = True
                '    End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' Responds as the subscriber to the tax update user control event. It will hide the popup
    ''' and update the UI and recalculate amounts based on the new tax info.
    ''' </summary>
    ''' <param name="sender"></param>
    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated

        PopupControlTax.ShowOnPageLoad = False
        Dim hidTaxCd As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxCd"), HiddenField)
        Dim hidTaxDesc As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDesc"), HiddenField)
        ViewState("TaxMethod") = (CType(ucTaxUpdate.FindControl("hidTaxMethod1"), HiddenField)).Value
        lbl_tax_cd.Text = hidTaxCd.Value
        ' Newly selected tax_cd should be added to session
        Session("TAX_CD") = lbl_tax_cd.Text
        lnkUpdatedTax.Text = lbl_tax_cd.Text
        lnkTaxCode.Text = lbl_tax_cd.Text.Trim
        ' Added the below code as a part of MM-4240
        ' The below code was adding an "-" symbol in the txtTaxCd textbox so making this change
        If Not (String.IsNullOrWhiteSpace(hidTaxCd.Value)) Then
            txtTaxCd.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(hidTaxCd.Value.ToString() & " - " & hidTaxDesc.Value.ToString()))
        Else
            txtTaxCd.Text = String.Empty
        End If

        ViewState("TaxMethod") = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(ViewState("TaxMethod")))
        'When tax has been selected
        ViewState("TaxExempted") = False
        cbo_tax_exempt.SelectedIndex = 0
        txt_exempt_id.Text = String.Empty

        PopulateZones(sender)
        hidTaxChange.Value = True
        calculate_total()

    End Sub


    ''' <summary>
    ''' On select of zone code the fallowing method is called
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucZonUpdate_ZoneUpdated(sender As Object, e As ZoneInfo) Handles UcZonePopup.ZoneUpdated
        'If AppConstants.DELIVERY = cbo_PD_new.SelectedValue And Not IsNothing(e) Then
        If Not String.IsNullOrEmpty(e.ZoneCode) Then
            txtNewZone.Text = e.ZoneCode
            hideZipCode.Value = e.ZipCode
            Dim hidZipcode As HiddenField = CType(UcDeliveryDatePopup.FindControl("hidZone"), HiddenField)
            If Not String.IsNullOrEmpty(txtNewZone.Text) Then
                hidZipcode.Value = txtNewZone.Text
            End If
            'bringup the del date popup for delivery sale type only
            If AppConstants.DELIVERY = cbo_PD.SelectedValue And Not IsNothing(e) Then
                Dim delvStore As String = e.DeliveryStoreCode
                Dim defDelChg As Decimal = e.Deliverycharges
                Dim defSetChg As Decimal = e.SetupCharges
                txtNewSetupChg.Text = FormatNumber(defSetChg.ToString, 2)
                txtNewDeliveryChgs.Text = FormatNumber(defDelChg.ToString, 2)
                txt_new_pd_store_cd.Text = delvStore
                UcDeliveryDatePopup.GetDeliveryDates(txt_del_doc_existing.Text)
                'GetDelStore()
                PopupDeliveryDate.ShowOnPageLoad = True
            Else
                txt_new_pd_store_cd.Text = e.DeliveryStoreCode
            End If
            PopupZone.ShowOnPageLoad = False
            calculate_total()
            lbl_old_or_new.Text = "NEW"

        End If
        'End If
    End Sub

    ''' <summary>
    ''' On select of delivery date the fallowing method is called
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="selecteddate"></param>
    Protected Sub ucDeliveryDate_DeliveryDateUpdated(sender As Object, selecteddate As Date) Handles UcDeliveryDatePopup.DeliveryDateSelected
        If selecteddate <> Nothing Then
            If lbl_old_or_new.Text = "OLD" Then
                cbo_pd_date.Value = selecteddate.Date
            Else
                cbo_pd_date.Value = selecteddate.Date
            End If
            IsZoneSelected.Value = True
        Else
            txtNewZone.Text = Nothing
            IsZoneSelected.Value = False
        End If
        PopupDeliveryDate.ShowOnPageLoad = False
    End Sub

    Private Function CheckForDeliveryAvaDT() As Boolean
        Dim zoneCode As String = If(txtNewZone.Text.Trim.Equals(String.Empty), txt_zone_cd.Text.Trim, txtNewZone.Text.Trim)
        If cbo_PD.SelectedValue = AppConstants.DELIVERY Then
            zoneCode = zoneCode.Trim
        Else
            zoneCode = txt_zone_cd.Text.Trim
        End If

        Dim currTranSched As New TransportationSchedule
        currTranSched.zoneCd = zoneCode.ToString
        currTranSched.puDelCd = cbo_PD.SelectedValue
        currTranSched.puDelDt = FormatDateTime(cbo_pd_date.Text, DateFormat.ShortDate)
        Dim delAvDT As Boolean = False
        If SysPms.isDelvByPoints Then
            Dim ds As DataSet
            If Not Session("SAL_LN") Is Nothing Then
                ds = Session("SAL_LN")
                If currTranSched.puDelCd = AppConstants.DELIVERY Then
                    Dim dt As DataTable = ds.Tables(0)
                    Dim gPointCnt As Double = 0.0
                    If dt.Rows.Count > 0 Then
                        For Each dr In dt.Rows
                            If Not IsDBNull(dr("POINT_SIZE")) AndAlso IsNumeric(dr("POINT_SIZE")) AndAlso IsNumeric(dr("QTY")) AndAlso dr("ITM_TP_CD") = AppConstants.Sku.TP_INV Then
                                gPointCnt = gPointCnt + (dr("POINT_SIZE") * dr("qty"))
                            End If
                        Next
                    End If
                    gPointCnt = gPointCnt * CRMPointsCalculation() 'Truck will be booked for two ways for picking up item as well as delivery 
                    delAvDT = theTMBiz.CheckForDeliveryAvaDT(gPointCnt, currTranSched.puDelDt, zoneCode.ToString)
                Else
                    delAvDT = True
                End If
            End If
        Else
            If currTranSched.puDelCd = AppConstants.DELIVERY Then
                gPointCnt = 1.0 * CRMPointsCalculation() 'Truck will be booked for two ways for picking up item as well as delivery 
                delAvDT = theTMBiz.CheckForDeliveryAvaDT(gPointCnt, currTranSched.puDelDt, zoneCode.ToString)
            Else
                delAvDT = True
            End If
        End If
        Return delAvDT
    End Function

    Private Sub PopulateTaxExempt()

        Dim ds As DataSet = theSalesBiz.GetTaxExemptCodes()
        With cbo_tax_exempt
            .DataSource = ds
            .ValueField = "tet_cd"
            .TextField = "full_desc"
            .DataBind()
            If Session("tet_cd") & "" <> "" Then
                .Value = Session("tet_cd")
            End If
        End With

        cbo_tax_exempt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Tax Exempt Code", ""))
        cbo_tax_exempt.Items.FindByText("Please Select A Tax Exempt Code").Value = ""
        If Session("tet_cd") & "" = "" And cbo_tax_exempt.Value & "" = "" Then
            cbo_tax_exempt.SelectedIndex = 0
        End If

        If Session("tet_cd") & "" = "" And cbo_tax_exempt.SelectedIndex <> 0 Then
            Session("tet_cd") = cbo_tax_exempt.Value
        End If

    End Sub

    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkUpdatedTax.Click
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user makes the line VOID
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Voiding a line ")
            End If
            If (Not (Convert.ToBoolean(ViewState("CanModifyTax")))) Then
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
                ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
                msgPopup.ShowOnPageLoad = True
            Else

                Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                hidIsInvokeManually.Value = "true"

                Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)

                hidTaxDt.Value = FormatDateTime(Today.Date, vbShortDate)
                'If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                '    hidZipCD.Value = String.Empty
                'Else
                If cbo_PD.SelectedValue = "P" Then
                    If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                        Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                        hidZipCD.Value = store.addr.postalCd
                    End If
                Else
                    hidZipCD.Value = txt_zip.Text
                End If
                'End If
                ucTaxUpdate.LoadTaxData()
                PopupControlTax.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Public Sub GetDelStore()
        ' get delivery store depending if pickup or delivery
        Dim store As New StoreData
        store = theSalesBiz.GetStoreInfo(txt_new_pd_store_cd.Text)

        If (Not IsNothing(store)) AndAlso SystemUtils.isNotEmpty(store.cd) Then
            txt_new_pd_store_cd.Text = store.shipToStoreCd
        End If
    End Sub
    Protected Sub orig_Sale_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs)
        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        Dim Line_no As String = grid.GetRowValues(e.VisibleIndex, "DEL_DOC_LN#")
        Dim Line_Switched = False

        Dim ds2 As DataSet = Session("SAL_LN")
        Dim dt2 As DataTable = ds2.Tables(0)
        Dim salLn As DataRow
        Dim loopCounter As Integer = 0
        Dim txt_qty As TextBox = Nothing
        Dim origLns As DataSet = Session("ORIG_SAL_LN")
        Dim dr As DataRow

        Dim split As Boolean = True ' either split or moved
        Dim moveQtyColumn As DevExpress.Web.ASPxGridView.GridViewDataColumn = CType(grid.Columns(7), DevExpress.Web.ASPxGridView.GridViewDataColumn)

        For Each dr In origLns.Tables(0).Rows
            If dr("DEL_DOC_LN#") & "" = Line_no Then
                txt_qty = CType(orig_Sale.FindRowCellTemplateControl(e.VisibleIndex, moveQtyColumn, "txtQty"), TextBox)
                If Not txt_qty Is Nothing Then
                    If Not IsNumeric(txt_qty.Text) Then
                        lbl_error.Text = Resources.SalesExchange.Label10
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf (txt_qty.Text Mod 1 > 0) AndAlso CType(orig_Sale.FindRowCellTemplateControl(e.VisibleIndex, moveQtyColumn, "LblAllowFraction"), TextBox).Text.Equals("N") Then
                        lbl_error.Text = Resources.SalesExchange.Label11
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf (txt_qty.Text Mod 1 > 0) AndAlso CType(orig_Sale.FindRowCellTemplateControl(e.VisibleIndex, moveQtyColumn, "LblAllowFraction"), TextBox).Text.Equals("Y") AndAlso CDbl(txt_qty.Text) < 1 Then
                        lbl_error.Text = Resources.SalesExchange.Label12
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf CDbl(txt_qty.Text) <= 0 Then
                        lbl_error.Text = Resources.SalesExchange.Label13
                        txt_qty.Text = 1
                        Exit Sub
                    End If
                End If
            End If
            If dr("DEL_DOC_LN#") = Line_no And Line_Switched = False Then
                If (CDbl(txt_qty.Text) > CDbl(dr("QTY"))) Then
                    lbl_error.Text = Resources.SalesExchange.Label14
                    Exit Sub
                End If

                dr("QTY") = Math.Round(CDbl(dr("QTY")) - CDbl(txt_qty.Text), 2)

                If CDbl(dr("QTY")) = 0 Then
                    dr("ITM_CD") = System.DBNull.Value
                    dr("STORE_CD") = System.DBNull.Value
                    dr("LOC_CD") = System.DBNull.Value
                    dr("UNIT_PRC") = 0.0
                    split = False
                End If
            End If
            loopCounter = loopCounter + 1
        Next

        For Each salLn In ds2.Tables(0).Rows
            If salLn("DEL_DOC_LN#") = Line_no And Line_Switched = False Then
                salLn("ITM_CD") = grid.GetRowValues(e.VisibleIndex, "ITM_CD").ToString()
                salLn("ITM_SRT_Desc") = ""
                salLn("QTY") = CDbl(salLn("QTY")) + CDbl(txt_qty.Text)
                salLn("STORE_CD") = grid.GetRowValues(e.VisibleIndex, "STORE_CD").ToString()
                salLn("LOC_CD") = grid.GetRowValues(e.VisibleIndex, "LOC_CD").ToString()
                salLn("UNIT_PRC") = grid.GetRowValues(e.VisibleIndex, "UNIT_PRC").ToString()
                Line_Switched = True
                salLn("NEW_LINE") = "Y"

                If grid.GetRowValues(e.VisibleIndex, "ITM_TP_CD").ToString().ToUpper().Equals("WAR") Then
                    Dim dsCRM As DataSet = Session("ORIG_SAL_LN")
                    Dim rows = dsCRM.Tables(0).Select("WARR_BY_LN='" + grid.GetRowValues(e.VisibleIndex, "DEL_DOC_LN#").ToString() + "'")
                    If rows.Count > 0 Then
                        For Each row As DataRow In rows
                            UpdateWarrantyLinks(grid.GetRowValues(e.VisibleIndex, "ITM_CD").ToString(), grid.GetRowValues(e.VisibleIndex, "DEL_DOC_LN#").ToString(), row(4).ToString)
                        Next
                    End If
                End If

                lbl_error.Text = ""
            End If
        Next

        Session("SAL_LN") = ds2
        grd_lines_new.DataSource = ds2
        grd_lines_new.DataBind()

        lnkUpdatedTax.Text = lbl_tax_cd.Text.Trim
        lnkTaxCode.Text = lbl_tax_cd.Text.Trim

        calculate_total()
    End Sub
    Protected Sub orig_Sale_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs)
        If Not (e.RowType = DevExpress.Web.ASPxGridView.GridViewRowType.Data) Then Return

        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        If grid.GetRowValues(e.VisibleIndex, "QTY").ToString() = "0" Then
            Dim dataCell1 As TableCell = CType(e.Row.Cells(1), TableCell)
            dataCell1.Text = ""

            Dim dataCell5 As TableCell = CType(e.Row.Cells(5), TableCell)
            dataCell5.Text = ""

            Dim txt_qty As TextBox = Nothing
            Dim moveQtyColumn As DevExpress.Web.ASPxGridView.GridViewDataColumn = CType(grid.Columns(7), DevExpress.Web.ASPxGridView.GridViewDataColumn)

            txt_qty = CType(orig_Sale.FindRowCellTemplateControl(e.VisibleIndex, moveQtyColumn, "txtQty"), TextBox)
            If Not txt_qty Is Nothing Then txt_qty.Enabled = False
        End If

        If Not Session("ORIG_SAL_LN") Is Nothing Then
            Dim dataCell4 As TableCell = CType(e.Row.Cells(4), TableCell)
            Try
                dataCell4.ToolTip = CType(Session("ORIG_SAL_LN"), DataSet).Tables(0).Rows(e.VisibleIndex)("ITM_SRT_Desc").ToString().Replace(", ", Environment.NewLine)
            Catch ex As Exception
                'If any exeptions is thrown, assign empty string to ToolTip proprety
                dataCell4.ToolTip = ""
            End Try
        End If

    End Sub
    Protected Sub orig_Sale_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs)
        If e.VisibleIndex = -1 Then Return

        If e.ButtonID = "img_Move" Then
            Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

            If grid.GetRowValues(e.VisibleIndex, "QTY").ToString() = "0" Then
                e.Visible = DevExpress.Utils.DefaultBoolean.False
                hidIsZoneSelected.Value = "Prevent"
            End If

            If Not ViewState("ItemTable") Is Nothing Then
                Dim Dtemp As DataTable = CType(ViewState("ItemTable"), DataTable)
                Dim PreventCheck As String = Dtemp.Rows(e.VisibleIndex)("PREVENT")
                If PreventCheck = "Y" Then
                    e.Visible = DevExpress.Utils.DefaultBoolean.False
                    hidIsZoneSelected.Value = "Prevent"
                End If
            End If
        End If
    End Sub
    Protected Sub orig_Sale_DataBound(sender As Object, e As EventArgs)
        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)
        If grid.VisibleRowCount < 7 Then
            '  grid.Settings.VerticalScrollBarMode = ScrollBarMode.Hidden
        Else
            ' grid.Settings.VerticalScrollBarMode = ScrollBarMode.Visible
            grid.Settings.VerticalScrollableHeight = 150
        End If
    End Sub
    Protected Sub grd_lines_new_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs)
        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)
        Dim selectedLn As String = grid.GetRowValues(e.VisibleIndex, "DEL_DOC_LN#")
        Dim isSwithchedLn As Boolean = False

        Dim origLns As New DataSet
        origLns = Session("ORIG_SAL_LN")
        Dim dt As DataTable = origLns.Tables(0)
        Dim dr2 As DataRow
        Dim FilterText As String
        Dim txt_qty As TextBox = Nothing
        Dim moveQty As Double = 0
        Dim loopCounter As Integer = 0
        moveQty = GetMovedQuantity(selectedLn)

        For Each dr2 In origLns.Tables(0).Rows
            If dr2("DEL_DOC_LN#") & "" = selectedLn Then
                Dim moveQtyColumn As DevExpress.Web.ASPxGridView.GridViewDataColumn = CType(orig_Sale.Columns(7), DevExpress.Web.ASPxGridView.GridViewDataColumn)
                txt_qty = CType(orig_Sale.FindRowCellTemplateControl(loopCounter, moveQtyColumn, "txtQty"), TextBox)
                If dr2("DEL_DOC_LN#") = selectedLn And isSwithchedLn = False Then

                    dr2("ITM_CD") = grid.GetRowValues(e.VisibleIndex, "ITM_CD").ToString()
                    dr2("QTY") = moveQty + dr2("QTY")
                    txt_qty.Text = 1
                    dr2("STORE_CD") = grid.GetRowValues(e.VisibleIndex, "STORE_CD").ToString()
                    dr2("LOC_CD") = grid.GetRowValues(e.VisibleIndex, "LOC_CD").ToString()
                    dr2("UNIT_PRC") = grid.GetRowValues(e.VisibleIndex, "UNIT_PRC").ToString()
                    isSwithchedLn = True

                    lbl_error.Text = ""
                End If
            End If
            loopCounter = loopCounter + 1
        Next

        isSwithchedLn = False

        If Not IsNothing(Session("SAL_LN")) Then
            Dim dsSale As DataSet = Session("SAL_LN")
            If Not IsNothing(dsSale) AndAlso dsSale.Tables.Count > 0 Then
                Dim drFilter() As DataRow = dsSale.Tables(0).Select("[DEL_DOC_LN#] = '" & selectedLn & "'")
                Dim drFilterWarr() As DataRow
                Dim qty As Integer

                '-----------------------------------------
                If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT AndAlso (Not String.IsNullOrEmpty(drFilter(0)("PKG_SOURCE").ToString)) Then
                    Dim drFilterParent() As DataRow = dsSale.Tables(0).Select("[DEL_DOC_LN#] = '" & drFilter(0)("PKG_SOURCE").ToString & "'")
                    Dim packagePrice = drFilterParent(0)("UNIT_PRC")
                    FilterText = "PKG_SOURCE = " + drFilter(0)("PKG_SOURCE").ToString
                    Dim packageRows() As DataRow = dsSale.Tables(0).Select(FilterText)
                    Dim packageItem As PackageItem = New PackageItem()
                    packageItem.PackagePrice = packagePrice
                    packageItem.PackageSKU = drFilterParent(0)("ITM_CD")
                    packageItem.Components = (From m In packageRows
                                              Select New PackageComponent() With {
                              .SKU = m.Field(Of String)("ITM_CD"),
                              .OrderOfOccurance = Convert.ToInt32(m.Field(Of Decimal)("DEL_DOC_LN#"))
                              }).ToList()

                    Dim itemToRemove As String = drFilter(0)("ITM_CD") & ""
                    Dim packageBiz As PackageBiz = New PackageBiz()
                    Dim packageSplit As PackageItem = packageBiz.GetPackageAndComponentPrice(packageItem, Session("store_cd").ToString.Trim, itemToRemove)
                    drFilterParent(0)("UNIT_PRC") = packageSplit.PackagePrice
                End If
                '-----------------------------------------


                For Each dr As DataRow In drFilter
                    drFilterWarr = Nothing
                    qty = 0
                    If Not isSwithchedLn Then
                        If Integer.TryParse(dr("QTY"), qty) AndAlso qty > 0 Then
                            qty = 0
                        End If
                        dr("QTY") = qty

                        If CDbl(dr("QTY")) = 0 Then
                            dr("STORE_CD") = System.DBNull.Value
                            dr("LOC_CD") = System.DBNull.Value
                            dr("ITM_CD") = System.DBNull.Value
                            dr("UNIT_PRC") = 0.0
                            dr("ITM_SRT_DESC") = System.DBNull.Value

                            If dr("ITM_TP_CD").ToString().Equals(AppConstants.Sku.TP_WAR.ToUpper()) Then
                                drFilterWarr = dsSale.Tables(0).Select("WARR_BY_LN = '" & selectedLn & "'")
                                For Each drWarr As DataRow In drFilterWarr
                                    drWarr("WARR_BY_LN") = System.DBNull.Value
                                    drWarr("WARR_BY_SKU") = System.DBNull.Value
                                    drWarr("WARR_BY_DOC_NUM") = System.DBNull.Value
                                Next
                            End If
                        End If

                        isSwithchedLn = True
                    End If
                Next

                Session("SAL_LN") = dsSale

                grd_lines_new.DataSource = dsSale
                grd_lines_new.DataBind()
                calculate_total()
            End If
        End If

        Session("ORIG_SAL_LN") = origLns
        orig_Sale.DataSource = origLns
        'Not binding data because it is calling orig_Sale_CustomButtonInitialize event and disabling few buttons
        'orig_Sale.DataBind()

    End Sub
    Protected Sub grd_lines_new_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs)
        If Not (e.RowType = DevExpress.Web.ASPxGridView.GridViewRowType.Data) Then Return
        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        If grid.GetRowValues(e.VisibleIndex, "QTY").ToString() = "0" Then
            Dim dataCell1 As TableCell = CType(e.Row.Cells(1), TableCell)
            dataCell1.Text = ""

            Dim dataCell5 As TableCell = CType(e.Row.Cells(5), TableCell)
            dataCell5.Text = ""
        End If

        If Not Session("SAL_LN") Is Nothing Then
            Dim dataCell4 As TableCell = CType(e.Row.Cells(4), TableCell)
            Try
                dataCell4.ToolTip = StringUtils.EliminateCommas(CType(Session("SAL_LN"), DataSet).Tables(0).Rows(e.VisibleIndex)("ITM_SRT_Desc").ToString()).Replace(",", Environment.NewLine)
            Catch ex As Exception
                'If any exeptions is thrown, assign empty string to ToolTip proprety
                dataCell4.ToolTip = ""
            End Try
        End If

        'update for request 4527 by Alice on Mar 06,2018, null out the display of values in ST and LOC fields.  This is misleading and confusing to stores and is not necessary.
        CType(e.Row.Cells(2), TableCell).Text = ""
        CType(e.Row.Cells(3), TableCell).Text = ""

    End Sub
    Protected Sub grd_lines_new_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs)
        If e.VisibleIndex = -1 Then Return

        If e.ButtonID = "img_Remove" Then
            Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

            If grid.GetRowValues(e.VisibleIndex, "QTY").ToString() = "0" Then
                e.Visible = DevExpress.Utils.DefaultBoolean.False
                hidIsZoneSelected.Value = "Prevent"
            End If
        End If
    End Sub
    Protected Sub grd_lines_new_DataBound(sender As Object, e As EventArgs)
        Dim grid As DevExpress.Web.ASPxGridView.ASPxGridView = CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView)
        If grid.VisibleRowCount < 7 Then
            '  grid.Settings.VerticalScrollBarMode = ScrollBarMode.Hidden
        Else
            ' grid.Settings.VerticalScrollBarMode = ScrollBarMode.Visible
            grid.Settings.VerticalScrollableHeight = 150
        End If
    End Sub
End Class
