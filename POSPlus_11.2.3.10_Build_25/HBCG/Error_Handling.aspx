<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Error_Handling.aspx.vb" Inherits="Error_Handling" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server" id="main_body" style="text-align: center">
        &nbsp;&nbsp;
        <img src="images/warning.jpg" style="font-size: 16pt" /><strong>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="We're Sorry.....">
            </dx:ASPxLabel>
            <br />
            <br />
        </strong>
        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="The application has experienced an error.&nbsp; If you would like to try again,
            please" Width="100%">
        </dx:ASPxLabel>
        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" NavigateUrl="javascript:history.back();"
            Text="click here">
        </dx:ASPxHyperLink>
        <br />
        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="If you continue to encounter this
                error, logging out will reset your session.  You will lose all previously saved
                data." Width="803px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="SessionID" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="EmpCd" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="ErrorCd" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="PrevPath" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>    
        <dx:ASPxLabel ID="ClientIP" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>    
        <dx:ASPxLabel ID="ServerIP" runat="server" Text="" Width="803px">
        </dx:ASPxLabel>       
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
