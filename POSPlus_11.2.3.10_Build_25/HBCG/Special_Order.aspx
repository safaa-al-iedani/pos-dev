<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Special_Order.aspx.vb" Inherits="Special_Order"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript">
        function SelectAndClosePopup4(){
            ASPxPopupControl4.Hide();
        }
    </script>

    <div>
        <table style="position: relative; top: 0px">
            <tr>
                <td>
                    SKU Number</td>
                <td colspan="3">
                    <asp:TextBox ID="txt_SKU" runat="server" MaxLength="9" Style="position: relative"
                        Width="64px"></asp:TextBox>
                    &nbsp;
                    <asp:Label ID="lbl_SKU_Info" runat="server" Style="position: relative" Text="Enter up to 9 characters"
                        Width="293px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Item Type Code</td>
                <td colspan="3">
                    <asp:DropDownList ID="cbo_ITM_TP_CD" runat="server" Width="320px" CssClass="style5">
                    </asp:DropDownList>
                    *</td>
            </tr>
            <tr>
                <td>
                    Vendor Code</td>
                <td colspan="3" valign="top">
                    <asp:TextBox ID="txt_Vendor" runat="server" MaxLength="4" Width="45px" CssClass="style5"
                        AutoPostBack="true"></asp:TextBox>&nbsp;*
                    <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                        Width="19px" OnClick="ImageButton2_Click" />
                    &nbsp; &nbsp; &nbsp;&nbsp;
                    <asp:Label ID="lbl_ve_label" runat="server" Width="295px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Minor Code
                </td>
                <td colspan="3" valign="top">
                    <asp:DropDownList ID="cbo_minor" runat="server" Width="374px" AutoPostBack="True"
                        CssClass="style5">
                    </asp:DropDownList>&nbsp;*
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                        Width="19px" />
                </td>
            </tr>
            <tr>
                <td nowrap>
                    Vendor Stock Number</td>
                <td colspan="3">
                    <asp:TextBox ID="txtVSN" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox>
                    *</td>
            </tr>
            <tr>
                <td>
                    Description</td>
                <td colspan="3">
                    <asp:TextBox ID="txtDES" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox>
                    *</td>
            </tr>
            <tr>
                <td style="height: 23px">
                    Cover</td>
                <td colspan="3" style="height: 23px">
                    <asp:TextBox ID="txtCover" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 23px">
                    Retail Price</td>
                <td style="height: 23px">
                    <asp:TextBox ID="txtRetail" runat="server" CssClass="style5"></asp:TextBox>
                    *</td>
                <td style="width: 77px; height: 23px; text-align: right">
                    Mfg List</td>
                <td style="width: 132px; height: 23px">
                    <asp:TextBox ID="txtMFG" runat="server" Width="120px" CssClass="style5"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Grade</td>
                <td colspan="3" style="height: 21px">
                    <asp:TextBox ID="txtGrade" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Size</td>
                <td colspan="3">
                    <asp:TextBox ID="txtSize" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Finish</td>
                <td colspan="3">
                    <asp:TextBox ID="txtFinish" runat="server" Width="368px" CssClass="style5" MaxLength="30"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Comment</td>
                <td>
                </td>
                <td style="width: 77px">
                </td>
                <td style="width: 132px">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="txtComment" runat="server" Width="472px" TextMode="MultiLine" CssClass="style5"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Style Code</td>
                <td colspan="3">
                    <asp:DropDownList ID="cboStyle" runat="server" Width="344px" CssClass="style5">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    Category</td>
                <td colspan="3">
                    <asp:DropDownList ID="cbo_category" runat="server" Width="344px" CssClass="style5"
                        AutoPostBack="True" OnSelectedIndexChanged="cbo_category_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    Volume</td>
                <td>
                    <asp:TextBox ID="txtVolume" runat="server" CssClass="style5" MaxLength="13"></asp:TextBox></td>
                <td style="width: 77px">
                    Measure Code</td>
                <td style="width: 132px">
                    <asp:TextBox ID="txtMeasure" runat="server" CssClass="style5"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Freight Factor</td>
                <td>
                    <asp:TextBox ID="txtFreight" runat="server" CssClass="style5"></asp:TextBox></td>
                <td style="width: 77px">
                    Stop Time</td>
                <td style="width: 132px">
                    <asp:TextBox ID="txtStop" runat="server" CssClass="style5"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Carpet Width</td>
                <td>
                    <asp:TextBox ID="txtCarpet" runat="server" CssClass="style5"></asp:TextBox></td>
                <td style="width: 77px">
                    Warrantable</td>
                <td style="width: 132px">
                    <asp:DropDownList ID="cbo_Warr" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N">No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    Pad Days</td>
                <td>
                    <asp:TextBox ID="txtPad" runat="server" CssClass="style5"></asp:TextBox></td>
                <td style="width: 77px">
                    Delivery Points</td>
                <td style="width: 132px">
                    <asp:TextBox ID="txtPoints" runat="server" CssClass="style5"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Treatable</td>
                <td>
                    <asp:DropDownList ID="cboTreatable" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N">No</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                    Family Code</td>
                <td>
                    <asp:TextBox ID="txtFamily" runat="server" CssClass="style5"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <asp:DropDownList ID="cbo_Special_Forms" runat="server" AppendDataBoundItems="True"
        AutoPostBack="True" Width="235px" CssClass="style5">
    </asp:DropDownList>
    &nbsp;<asp:HyperLink ID="hpl_excel" runat="server" Visible="false" Target="_blank">
        <asp:Image ImageUrl="images/icons/ms_excel.jpg" BorderWidth="0" ID="img_excel" runat="server"
            Visible="false" Style="width: 16px; height: 16px" /></asp:HyperLink>
    <asp:DropDownList ID="cbo_Frames" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
        Width="235px" CssClass="style5">
    </asp:DropDownList>&nbsp;<br />
    <asp:Button ID="btn_save_exit" runat="server" Text="Save" Width="119px" CssClass="style5" />&nbsp;
    <asp:Button ID="btn_Clear" runat="server" Text="Clear" Width="119px" CssClass="style5" />
    <asp:Button ID="btn_exit" runat="server" Text="Exit" Width="119px" CssClass="style5" />
    <asp:Button ID="btn_sku_copy" runat="server" Text="Copy SKU" Width="119px" CssClass="style5" OnClick="btn_sku_copy_Click" />
    <asp:Button ID="btn_save_add" runat="server" Text="Save and Add Another" Width="142px"
        CssClass="style5" Enabled="False" Visible="False" />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" ClientInstanceName="ASPxPopupControl4" CloseAction="CloseButton"
        HeaderText="Vendor Lookup" Height="207px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="400px" RenderIFrameForPopupElements="True"
        AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <table width="100%">
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                        </td>
                        <td>
                            &nbsp;&nbsp;
                        </td>
                        <td valign="middle" align="center" style="width: 413px">
                            Vendor Search
                            <br />
                            <asp:Panel ID="Panel4" runat="server" Width="333px" DefaultButton="btn_ve_submit"
                                BorderWidth="0">
                                <table class="style5">
                                    <tr>
                                        <td>
                                            Vendor Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_ve_cd" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Vendor Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_ve_name" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btn_ve_submit" runat="server" Text="Search" Width="100px" CssClass="style5" />
                            </asp:Panel>
                            <br />
                            <br />
                            <asp:Panel ID="Panel1" runat="server" Height="185px" Width="440px" ScrollBars="Auto"
                                BorderWidth="0">
                                <asp:DataGrid ID="ve_grid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                    AutoGenerateColumns="False" DataKeyField="ve_cd" Height="16px" PageSize="7" AlternatingItemStyle-BackColor="Beige"
                                    CssClass="style5" Visible="true">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Vendor Code" DataField="VE_CD" />
                                        <asp:BoundColumn HeaderText="Vendor Name" DataField="VE_NAME" />
                                        <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select" ItemStyle-CssClass="style5">
                                        </asp:ButtonColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                    <EditItemStyle CssClass="style5" />
                                    <ItemStyle CssClass="style5" />
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="Minor Lookup" Height="207px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="400px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <table width="100%">
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                        </td>
                        <td>
                            &nbsp;&nbsp;
                        </td>
                        <td valign="middle" align="center" style="width: 413px">
                            Minor Search
                            <br />
                            <asp:Panel ID="Panel3" runat="server" Width="333px" DefaultButton="btn_mnr_submit"
                                BorderWidth="0">
                                <table class="style5">
                                    <tr>
                                        <td>
                                            Minor Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_mnr_search" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Description
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_mnr_desc_search" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btn_mnr_submit" runat="server" Text="Search" Width="100px" CssClass="style5" />
                            </asp:Panel>
                            <br />
                            <br />
                            <asp:Panel ID="Panel2" runat="server" Height="185px" Width="440px" ScrollBars="Auto"
                                BorderWidth="0">
                                <asp:DataGrid ID="mnr_grid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                    AutoGenerateColumns="False" DataKeyField="mnr_cd" Height="16px" PageSize="7"
                                    AlternatingItemStyle-BackColor="Beige" CssClass="style5" Visible="true">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Minor Code" DataField="MNR_CD" />
                                        <asp:BoundColumn HeaderText="Description" DataField="FULL_DES" />
                                        <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select" ItemStyle-CssClass="style5">
                                        </asp:ButtonColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                    <EditItemStyle CssClass="style5" />
                                    <ItemStyle CssClass="style5" />
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
        HeaderText="SKU Lookup" Height="207px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="400px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <table width="100%">
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                        </td>
                        <td>
                            &nbsp;&nbsp;
                        </td>
                        <td valign="middle" align="center" style="width: 413px">
                            SKU Search
                            <br />
                            <asp:Panel ID="Panel5" runat="server" Width="333px" DefaultButton="btn_sku_search_submit"
                                BorderWidth="0">
                                <table class="style5">
                                    <tr>
                                        <td>
                                            SKU
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_sku_search" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            VSN
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_vsn_search" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_des_search" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btn_sku_search_submit" runat="server" Text="Search" Width="100px" />
                            </asp:Panel>
                            <br />
                            <br />
                            <asp:Panel ID="Panel6" runat="server" Height="185px" Width="440px" ScrollBars="Auto"
                                BorderWidth="0">
                                <asp:DataGrid ID="itm_grd" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                    AutoGenerateColumns="False" DataKeyField="itm_cd" Height="16px" PageSize="7"
                                    AlternatingItemStyle-BackColor="Beige" Visible="true">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="SKU" DataField="ITM_CD" />
                                        <asp:BoundColumn HeaderText="VSN" DataField="VSN" />
                                        <asp:BoundColumn HeaderText="DES" DataField="DES" />
                                        <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select">
                                        </asp:ButtonColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
</asp:Content>
