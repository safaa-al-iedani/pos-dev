Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Order
    Inherits POSBasePage

    Private theSkuBiz As SKUBiz = New SKUBiz()
	Private LeonsBiz As LeonsBiz = New LeonsBiz() ' Daniela 
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        ' Daniela added
        If txtsku.Text & "" <> "" Then
            If InStr(txtsku.Text, "%") = 0 Then
                txtsku.Text = UCase(txtsku.Text.Trim)
                Dim usr_flag As String = LeonsBiz.sec_check_itm(txtsku.Text, Session("emp_cd"))

                If usr_flag = "N" Then  'multi co
                    lbl_Errors.Text = "You are not authorized to select that SKU"
                    Exit Sub
                End If
            End If
        End If
        ' Daniela end       
        If txtvendor.Text & "" = "" And txtvsn.Text & "" = "" And txtdesc.Text & "" = "" And txtsku.Text & "" = "" And txtprice.Text & "" = "" And txtminor.Text & "" = "" And txtcategory.Text & "" = "" And txtstyle.Text & "" = "" And txtfinish.Text & "" = "" And txtcover.Text & "" = "" And txtgrade.Text & "" = "" Then
            lbl_Errors.Text = "You must enter at least one search criteria!"
        ElseIf txtvendor.Text & "" = "" And txtvsn.Text & "" = "" And txtdesc.Text & "" = "" And UCase(txtsku.Text) = "S" And txtprice.Text & "" = "" And txtminor.Text & "" = "" And txtcategory.Text & "" = "" And txtstyle.Text & "" = "" And txtfinish.Text & "" = "" And txtcover.Text & "" = "" And txtgrade.Text & "" = "" And Request("referrer") & "" = "" Then
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("special_order.aspx?LEAD=TRUE")
            Else
                Response.Redirect("special_order.aspx")
            End If
        ElseIf Not IsNumeric(txtprice.Text) & txtprice.Text & "" <> "" Then
            txtprice.Text = ""
            txtprice.Focus()
            lbl_Errors.Text = "Retail price must be numeric!"
        Else
            Session("vendor") = txtvendor.Text
            Session("vsn") = txtvsn.Text
            Session("desc") = txtdesc.Text
            Session("sku") = txtsku.Text
            Session("price") = txtprice.Text
            Session("minor") = txtminor.Text
            Session("category") = txtcategory.Text
            Session("style") = txtstyle.Text
            Session("finish") = txtfinish.Text
            Session("size") = txtsize.Text
            Session("cover") = txtcover.Text
            Session("grade") = txtgrade.Text
            Session("dropped") = chk_dropped.Value

            Dim Query_string As String = ""
            If Request("referrer") & "" <> "" Then
                Query_string = Request.QueryString.ToString()
            End If
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("InventoryResults.aspx?LEAD=TRUE" & "&" & Query_string)
            Else
                Response.Redirect("InventoryResults.aspx?" & Query_string)
            End If
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request("LEAD") = "TRUE" Then
            btn_Special.Visible = False
        End If
        Dim RoundPanel As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel
        RoundPanel = Master.FindControl("ASPxRoundPanel4")
        If Not IsNothing(RoundPanel) Then
            RoundPanel.DefaultButton = "btnSubmit"
        End If
        If Not IsPostBack Then
            'next condition seems backwards but it's correct.  chk_dropped label is "Do NOT search for dropped items"
            chk_dropped.Checked = (Not SysPms.searchDropSkus)
        End If
        Session("dropped") = chk_dropped.Value
        If Request("referrer") & "" = "" Then
            If Session("Add_More_Items") <> "TRUE" Then
                If Session("itemid") & "" <> "" Then
                    If Request("LEAD") = "TRUE" Then
                        Response.Redirect("order_detail.aspx?LEAD=TRUE")
                    Else
                        Response.Redirect("order_detail.aspx")
                    End If
                End If
            Else
                If Request("itm_cd") & "" <> "" Then
                    If (theSkuBiz.IsSellable(Request("itm_cd"))) Then
                        lbl_Errors.Text = Request("vsn") & " (" & Request("itm_cd") & ") added to order."
                    Else
                        lbl_Errors.Text = String.Format(Resources.POSMessages.MSG0018, UCase(Request("itm_cd")))
                    End If
                End If
            End If
        End If

        If ConfigurationManager.AppSettings("special_orders") = "Y" And Request("referrer") & "" = "" Then
            btn_Special.Enabled = True
        Else
            btn_Special.Enabled = False
        End If

        txtvendor.Focus()

    End Sub

    Protected Sub btn_Special_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Special.Click

        If Request("LEAD") = "TRUE" Then
            Response.Redirect("Special_Order.aspx?LEAD=TRUE")
        Else
            Response.Redirect("Special_Order.aspx")
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        ElseIf Request("referrer") & "" <> "" Then
            If Request("minimal") = "true" Then
                Page.MasterPageFile = "~/NoContent.Master"
            Else
                Page.MasterPageFile = "~/MasterPages/NoWizard2.Master"
            End If
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

End Class
