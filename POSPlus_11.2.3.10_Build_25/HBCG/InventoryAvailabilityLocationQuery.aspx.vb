Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'JAM added 20Oct2014
Imports DevExpress.Web.ASPxEditors
Imports System.Data

'JAM import SalesDac
Imports SalesDac


Partial Class InventoryAvailabilityLocationQuery
    Inherits POSBasePage

    Public Const gs_version As String = "Version 2.1"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. Basic item data query and inventory quantity levels. Also included:
    '               Lead days, PO reserved and available quantities, soonest PO available date and quantity
    '
    '   1.0.1:  Minor bug fixes during testing:
    '           (1) Changing item security to use Leon's DB package didn't convert the entered item code to uppercase.
    '           (2) As per Jacquie S., modified 'RetrievePOArrivalSoonest()' to properly retrieve the soonest date - removed the
    '               'AND rownum = 1' condition and then forced the query results to just grab the FIRST data row info.
    '
    '   2.0:    POS+ French conversion: 27-Aug-2015 - Kumaran
    '            (1) Labels, button captions, command messages are converted into French strings.
    '
    '   2.1:     Minor Bug Fixing: 08-Dec-2015 - Kumaran
    '            (1) Modified to use itm_fifl table to report available inventory for non RF stores.
    '            (2) Added screen title.
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetAllFields()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Resets/clears all fields.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        lbl_resultsInfo.Visible = False
        lbl_resultsInfo.Text = ""

        txt_des.Text = ""
        txt_vendor_code.Text = ""
        txt_vsn.Text = ""
        txt_itm_tp_cd.Text = ""
        txt_drop_cd.Text = ""
        txt_comm_cd.Text = ""
        txt_related_itm_cd.Text = ""
        lst_identification_code.Items.Clear()

        txt_reg_total_showroom.Text = ""
        txt_reg_reserved_showroom.Text = ""
        txt_reg_avail_showroom.Text = ""
        txt_reg_total_warehouse.Text = ""
        txt_reg_reserved_warehouse.Text = ""
        txt_reg_avail_warehouse.Text = ""
        txt_reg_total_store.Text = ""
        txt_reg_total_reserved.Text = ""
        txt_reg_total_available.Text = ""

        'Alice added start for request 4350 on Nov 16, 2018
        txt_reg_avail_vendor.Text = ""
        txt_outlet_avail_vendor.Text = ""
        'txt_fm_avail_vendor.Text = ""
        'txt_nas_avail_vendor.Text = ""
        'Alice added end

        txt_outlet_total_showroom.Text = ""
        txt_outlet_reserved_showroom.Text = ""
        txt_outlet_avail_showroom.Text = ""
        txt_outlet_total_warehouse.Text = ""
        txt_outlet_reserved_warehouse.Text = ""
        txt_outlet_avail_warehouse.Text = ""
        txt_outlet_total_store.Text = ""
        txt_outlet_total_reserved.Text = ""
        txt_outlet_total_available.Text = ""

        txt_fm_total_showroom.Text = ""
        txt_fm_reserved_showroom.Text = ""
        txt_fm_avail_showroom.Text = ""
        txt_fm_total_warehouse.Text = ""
        txt_fm_reserved_warehouse.Text = ""
        txt_fm_avail_warehouse.Text = ""
        txt_fm_total_store.Text = ""
        txt_fm_total_reserved.Text = ""
        txt_fm_total_available.Text = ""

        txt_nas_total_showroom.Text = ""
        txt_nas_reserved_showroom.Text = ""
        txt_nas_avail_showroom.Text = ""
        txt_nas_total_warehouse.Text = ""
        txt_nas_reserved_warehouse.Text = ""
        txt_nas_avail_warehouse.Text = ""
        txt_nas_total_store.Text = ""
        txt_nas_total_reserved.Text = ""
        txt_nas_total_available.Text = ""

        txt_lead_time_to_store.Text = ""
        txt_po_total_reserved.Text = ""
        txt_po_total_available.Text = ""
        txt_po_arrival_date_soonest.Text = ""
        txt_po_available_soonest.Text = ""

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   SECURITY
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           IsValidItemSecurityCheck_LEONS()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which validates the item is accessible for the given user.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function IsValidItemSecurityCheck_LEONS(ByVal p_itemCode As String) As String

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim SQL As String = ""

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_employeeCode = Session("EMP_CD")
        'MsgBox("employee: " + s_employeeCode)

        'TO TEST empty employee value
        's_employeeCode = ""

        If (String.IsNullOrEmpty(Trim(s_employeeCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security"
            s_errorMessage = Resources.CustomMessages.MSG0032
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            IsValidItemSecurityCheck_LEONS = "NO_EMP"

        Else
            'Dim s_errorMessage As String = ""
            's_errorMessage = "INFO: Current user employee code is [" + s_employeeCode + "]"
            'DisplayMessage(s_errorMessage, "LABEL", "INFO")

            If (Not (s_itemCode Is Nothing) AndAlso s_itemCode.isNotEmpty) Then

                conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                'Open Connection 
                conn.Open()

                'we use isValidItm2() so we can pass in the EMP_CD which determines the EMP_INIT which is required for isValidItm()
                SQL = "STD_MULTI_CO.isValidItm2"
                objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
                objsql.CommandType = CommandType.StoredProcedure

                'objsql.Parameters.Add("p_empinit", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("p_empCd", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("v_returnValue", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

                'objsql.Parameters("p_empinit").Value = s_employeeCode
                objsql.Parameters("p_empCd").Value = s_employeeCode
                objsql.Parameters("p_itm").Value = s_itemCode

                objsql.ExecuteNonQuery()
                IsValidItemSecurityCheck_LEONS = objsql.Parameters("v_returnValue").Value

            End If

        End If

    End Function

    ''------------------------------------------------------------------------------------------------------------------------------------
    ''   Name:           IsValidItemSecurityCheck()
    ''   Created by:     John Melenko
    ''   Date:           Jan2015
    ''   Description:    Procedure which validates the item is accessible for the given user.
    ''   Called by:      btn_Lookup_Click()
    ''------------------------------------------------------------------------------------------------------------------------------------
    'Private Function IsValidItemSecurityCheck(ByVal p_itemCode As String) As String

    '    Dim conn As OracleConnection
    '    Dim objsql As OracleCommand
    '    Dim Mydatareader As OracleDataReader
    '    Dim SQL As String = ""

    '    Dim s_result As String = ""

    '    Dim s_itemCode As String = p_itemCode.ToUpper
    '    Dim s_employeeCode = Session("EMP_CD")
    '    'MsgBox("employee: " + s_employeeCode)

    '    'TO TEST empty employee value
    '    's_employeeCode = ""

    '    If (String.IsNullOrEmpty(Trim(s_employeeCode.ToString()))) Then

    '        Dim s_errorMessage As String = ""

    '        s_errorMessage = "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security"
    '        'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
    '        'lbl_resultsInfo.Text = s_errorMessage
    '        'lbl_resultsInfo.ForeColor = Color.Red
    '        DisplayMessage(s_errorMessage, "LABEL", "ERROR")

    '        IsValidItemSecurityCheck = "NO_EMP"

    '    Else
    '        'Dim s_errorMessage As String = ""
    '        's_errorMessage = "INFO: Current user employee code is [" + s_employeeCode + "]"
    '        'DisplayMessage(s_errorMessage, "LABEL", "INFO")

    '        If Not (String.IsNullOrEmpty(Trim(s_itemCode.ToString()))) Then
    '            'If (Not (p_itemCode Is Nothing) AndAlso p_itemCode.isNotEmpty) Then

    '            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
    '            'Open Connection 
    '            conn.Open()

    '            SQL = "DECLARE "
    '            SQL = SQL & "s_emp_cd            EMP.emp_cd%TYPE := :x_employeeCode; " ''SQLTEST'; "
    '            SQL = SQL & "s_emp_init          EMP.emp_init%TYPE; "
    '            SQL = SQL & "s_company_group_cd  CO_GRP.co_grp_cd%TYPE; "
    '            SQL = SQL & "s_itm_cd            ITM.itm_cd%TYPE := :x_itemCode; " ''5998273GN'; "
    '            SQL = SQL & "s_result            VARCHAR2(1) := 'N'; "

    '            SQL = SQL & "CURSOR c_emp_init IS "
    '            SQL = SQL & "SELECT emp_init "
    '            SQL = SQL & "FROM emp "
    '            SQL = SQL & "WHERE emp_cd = s_emp_cd; "
    '            SQL = SQL & "CURSOR c_company_group_cd IS "
    '            SQL = SQL & "SELECT co_grp_cd "
    '            SQL = SQL & "FROM co_grp "
    '            SQL = SQL & "WHERE co_cd = ( SELECT co_cd FROM store WHERE store_cd = "
    '            SQL = SQL & " ( SELECT home_store_cd FROM emp WHERE emp_cd = s_emp_cd) "
    '            SQL = SQL & "); "
    '            SQL = SQL & "BEGIN "
    '            SQL = SQL & "  OPEN c_emp_init; "
    '            SQL = SQL & "  FETCH c_emp_init INTO s_emp_init; "
    '            SQL = SQL & "  CLOSE c_emp_init; "
    '            SQL = SQL & "  OPEN c_company_group_cd; "
    '            SQL = SQL & "  FETCH c_company_group_cd INTO s_company_group_cd; "
    '            SQL = SQL & "  CLOSE c_company_group_cd; "
    '            '    -- ---------------------------------
    '            '    -- Brick user logic
    '            '    -- ---------------------------------
    '            SQL = SQL & "  IF STD_MULTI_CO.isBrickCoGrp(s_company_group_cd) = 'Y' THEN "
    '            SQL = SQL & "    IF STD_MULTI_CO.isBrickItm(s_itm_cd) = 'Y' THEN "
    '            'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Brick item - return value = Y');
    '            '--RETURN 'Y';
    '            SQL = SQL & "      s_result := 'Y'; "
    '            SQL = SQL & "    END IF; "
    '            SQL = SQL & "  END IF; "


    '            '    -- ---------------------------------
    '            '   -- Leon user logic
    '            '  -- ---------------------------------
    '            SQL = SQL & "  IF STD_MULTI_CO.isLeonCoGrp(s_company_group_cd) = 'Y' THEN "
    '            SQL = SQL & "    IF STD_MULTI_CO.isLeonsItm(s_itm_cd) = 'Y' THEN "
    '            '-- corporate itm"
    '            'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons item - return value = Y');
    '            SQL = SQL & "      IF STD_ROLE.isLeonsCorpStoreUsr(s_emp_init) = 'Y' "
    '            SQL = SQL & "         OR STD_ROLE.isLeonsHomeOfficeUsr(s_emp_init) = 'Y' "
    '            SQL = SQL & "         OR STD_ROLE.isLeonsFranchiseStrUsr(s_emp_init) = 'Y' THEN "
    '            'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons(2) item - return value = Y');
    '            '  --RETURN 'Y';
    '            SQL = SQL & "          s_result := 'Y'; "
    '            SQL = SQL & "      END IF; "
    '            SQL = SQL & "  END IF;"
    '            SQL = SQL & "  IF STD_MULTI_CO.isLeonsFrItm(s_itm_cd) = 'Y' THEN "
    '            '  -- franchise itm
    '            '-- Note: Item maintenance screen is not release to
    '            '--       franchise user in general. only restricted user.
    '            SQL = SQL & "      IF STD_ROLE.isLeonsFrItmMaintUsr(s_emp_init) = 'Y' "
    '            SQL = SQL & "         OR STD_ROLE.isLeonsFranchiseStrUsr(s_emp_init) = 'Y' THEN "
    '            '          DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons Franchise item - return value = Y');
    '            '             --RETURN 'Y';
    '            SQL = SQL & "          s_result := 'Y'; "
    '            SQL = SQL & "      END IF; "
    '            SQL = SQL & "    END IF; "
    '            SQL = SQL & "  END IF; "

    '            '  --DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is not a valid company item - return value = N');
    '            ' --RETURN 'N';
    '            '--end isValidItm;
    '            SQL = SQL & "  :x_result := s_result; "
    '            SQL = SQL & "END;"

    '            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

    '            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
    '            objsql.Parameters(":x_employeeCode").Value = s_employeeCode
    '            'Dim s_paramEmployeeCode As OracleParameter = New OracleParameter(":x_employeeCode", OracleType.VarChar, 255)
    '            's_paramEmployeeCode.Direction = ParameterDirection.Input
    '            'objsql.Parameters.Add(s_paramEmployeeCode)
    '            'objsql.Parameters(":x_employeeCode").Value = s_employeeCode

    '            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
    '            objsql.Parameters(":x_itemCode").Value = s_itemCode
    '            'Dim s_paramItemCode As OracleParameter = New OracleParameter(":x_itemCode", OracleType.VarChar, 255)
    '            's_paramItemCode.Direction = ParameterDirection.Input
    '            'objsql.Parameters.Add(s_paramItemCode)
    '            'objsql.Parameters(":x_itemCode").Value = s_itemCode

    '            'objsql.Parameters.Add(":x_result", OracleType.VarChar)
    '            'objsql.Parameters(":x_result").Value = s_result
    '            Dim s_resultParameter As OracleParameter = New OracleParameter(":x_result", OracleType.VarChar, 255)
    '            s_resultParameter.Direction = ParameterDirection.Output
    '            objsql.Parameters.Add(s_resultParameter)

    '            objsql.ExecuteNonQuery()

    '            s_result = s_resultParameter.Value

    '        End If

    '    End If

    '    IsValidItemSecurityCheck = s_result

    'End Function

    ''------------------------------------------------------------------------------------------------------------------------------------
    ''   Name:           belongsToBrickCompanyGroup()
    ''   Created by:     John Melenko
    ''   Date:           Apr2015
    ''   Description:    Checks if user belongs to the Brick company group
    ''   Called by:      NOT USED but kept for future use
    ''------------------------------------------------------------------------------------------------------------------------------------
    'Private Function belongsToBrickCompanyGroup() As Boolean

    '    Dim conn As New OracleConnection
    '    Dim SQL As String
    '    Dim Mydatareader As OracleDataReader
    '    Dim objsql As OracleCommand

    '    Dim b_Continue As Boolean = False

    '    Dim s_companyCode As String = Session("CO_CD")
    '    Dim s_employeeCode As String = Session("EMP_CD")
    '    'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

    '    If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

    '        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '        conn.Open()

    '        SQL = "SELECT std_multi_co.isBrickCoGrp(cg.co_grp_cd) as brick_company_group "
    '        SQL = SQL & "FROM co_grp cg "
    '        SQL = SQL & "WHERE cg.co_cd = UPPER(:x_companyCode) "

    '        'Set SQL OBJECT 
    '        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

    '        objsql.Parameters.Add(":x_companyCode", OracleType.VarChar)
    '        objsql.Parameters(":x_companyCode").Value = s_companyCode

    '        Try
    '            'Execute DataReader 
    '            Mydatareader = objsql.ExecuteReader
    '            'Store Values in String Variables 
    '            If Mydatareader.Read() Then

    '                If (Mydatareader.Item("brick_company_group").ToString.Equals("Y")) Then
    '                    b_Continue = True
    '                End If
    '                Mydatareader.Close()

    '            End If

    '        Catch
    '            Throw
    '            conn.Close()
    '        End Try
    '        conn.Close()

    '    End If

    '    Return b_Continue

    'End Function

    ''------------------------------------------------------------------------------------------------------------------------------------
    ''   Name:           hasAccess()
    ''   Created by:     John Melenko
    ''   Date:           Apr2015
    ''   Description:    Checks if user has access using the passed in security code
    ''   Called by:      NOT USED but kept for future use
    ''------------------------------------------------------------------------------------------------------------------------------------
    'Private Function hasAccess(p_securityCode As String) As Boolean

    '    Dim conn As New OracleConnection
    '    Dim SQL As String
    '    Dim Mydatareader As OracleDataReader
    '    Dim objsql As OracleCommand

    '    Dim b_Continue As Boolean = False
    '    Dim s_result As String = "N"
    '    Dim b_hasAccess As Boolean = False

    '    If (String.IsNullOrEmpty(Trim(p_securityCode.ToString()))) Then

    '        Dim s_errorMessage As String = ""

    '        s_errorMessage = "ERROR: Security code must have a value to determine access"
    '        'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
    '        'lbl_resultsInfo.Text = s_errorMessage
    '        'lbl_resultsInfo.ForeColor = Color.Red
    '        DisplayMessage(s_errorMessage, "LABEL", "ERROR")

    '        Return False
    '    End If

    '    Dim s_employeeCode As String = Session("EMP_CD")
    '    'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

    '    If (Not (s_employeeCode Is Nothing)) AndAlso s_employeeCode.isNotEmpty Then

    '        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '        conn.Open()

    '        SQL = "DECLARE "
    '        SQL = SQL & "b_result BOOLEAN := FALSE; "
    '        SQL = SQL & "s_result VARCHAR(1) := 'N'; "
    '        SQL = SQL & "BEGIN "
    '        SQL = SQL & "  b_result := bt_security.has_security( :x_employeeCode, :x_securityCode ); "
    '        SQL = SQL & "  IF b_result THEN "
    '        SQL = SQL & "      s_result := 'Y'; "
    '        SQL = SQL & "  END IF; "
    '        SQL = SQL & "  :x_result := s_result; "
    '        SQL = SQL & "END;"

    '        'Set SQL OBJECT 
    '        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

    '        objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
    '        objsql.Parameters(":x_employeeCode").Value = s_employeeCode

    '        objsql.Parameters.Add(":x_securityCode", OracleType.VarChar)
    '        objsql.Parameters(":x_securityCode").Value = p_securityCode

    '        Dim s_resultParameter As OracleParameter = New OracleParameter(":x_result", OracleType.VarChar, 255)
    '        s_resultParameter.Direction = ParameterDirection.Output
    '        objsql.Parameters.Add(s_resultParameter)

    '        objsql.ExecuteNonQuery()

    '        s_result = s_resultParameter.Value

    '        conn.Close()

    '    End If

    '    If (s_result.Equals("Y")) Then
    '        b_hasAccess = True
    '    End If
    '    hasAccess = b_hasAccess

    'End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   ITEM VALIDATION
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           isValidItem()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Confirms if item is a valid item - it exists!
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function isValidItem(p_itemCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT i."
        SQL = SQL & Resources.CustomMessages.MSG0080
        SQL = SQL & " des "
        SQL = SQL & "FROM itm i "
        SQL = SQL & "WHERE i.itm_cd = UPPER(:x_itemCode) "
        SQL = SQL & "AND i.itm_tp_cd = 'INV' "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = s_itemCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                'txt_des.Text = Mydatareader.Item("DES").ToString

            Else

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = "ERROR: Item '" + s_itemCode + "' is not a valid inventory item or does not exist"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0057, s_itemCode)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_Continue

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   POPULATE DROP DOWN LISTS
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String = ""

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        'MCCL Dec 8, 2017
        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s order by store_cd "
        End If
        If (isNotEmpty(userType) And userType > 0) Then
            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
              "from MCCL_STORE_GROUP a, store s, store_grp$store b "
            If isNotEmpty(userType) And userType = 5 Then
                'MCCL 8 validation Franchise leons 
                SQL = SQL & "where a.user_id in (3,:userType) and "
            Else
                SQL = SQL & "where a.user_id = :userType and "
            End If
            SQL = SQL & "s.store_cd = b.store_cd and " +
              "a.store_grp_cd = b.store_grp_cd "
            SQL = SQL & " order by store_cd"
        End If

        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'txt_store_cd.Items.Insert(0, "Select a Store Code")
        'txt_store_cd.Items.FindByText("Select a Store Code").Value = ""
        txt_store_cd.Items.Insert(0, GetLocalResourceObject("combo_select_store_code.Text").ToString())
        txt_store_cd.Items.FindByText(GetLocalResourceObject("combo_select_store_code.Text").ToString()).Value = ""
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
        txt_store_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With


            If (isNotEmpty(userType) And userType > 0) Then
                cmdGetCodes.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetCodes.Parameters(":userType").Value = userType
            End If

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
                .SelectedIndex = 0
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateSKUSortCodes()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Procedure which populates the SKU sort codes drop-down list.
    '   Called by:      RetrieveItemInfo()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateSKUSortCodes(p_itemCode As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String

        If (Not (p_itemCode Is Nothing)) AndAlso p_itemCode.isNotEmpty Then

            SQL = "SELECT DISTINCT itm_srt_cd "
            SQL = SQL & "FROM itm$itm_srt "
            SQL = SQL & "WHERE itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "ORDER BY itm_srt_cd "

            ' bind variables
            cmdGetCodes.Parameters.Add(":x_itemCode", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_itemCode").Value = p_itemCode

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_itm_srt_cd
                    .DataSource = ds
                    .DataValueField = "itm_srt_cd"
                    .DataTextField = "itm_srt_cd"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try
        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   ITEM DATA RETRIEVAL
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveItemInfo()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Main procedure which populates the item information based on query parameters entered by the user.
    '   Called by:      "Search" button - btn_Search_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveItemInfo(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_storeCode As String = p_storeCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        If (String.IsNullOrEmpty(Trim(p_itemCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Item code required"
            s_errorMessage = Resources.CustomMessages.MSG0058
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        ElseIf (String.IsNullOrEmpty(Trim(p_storeCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Store code required."
            s_errorMessage = Resources.CustomMessages.MSG0056
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand
            SQL = "SELECT i.itm_cd, i."
            SQL = SQL & Resources.CustomMessages.MSG0080
            SQL = SQL & " des, i.vsn, i.itm_tp_cd, i.drop_cd, i.comm_cd, i.related_itm_cd, "
            SQL = SQL & "DECODE(v.ve_cust_cd,NULL,'N','Y') as brick_vendor, NVL(v.ve_cust_cd,v.ve_cd) as brick_vendor_code, v.ve_cd as leons_vendor_code, v.ve_name as vendor_name, "
            SQL = SQL & "NVL(i.avail_pad_days, bt_adv_res.get_ve_pad_days(i.ve_cd)) as po_buffer_days "
            SQL = SQL & "FROM itm i, ve v "
            SQL = SQL & "WHERE i.ve_cd = v.ve_cd "
            SQL = SQL & "AND i.itm_tp_cd = 'INV' "
            SQL = SQL & "AND i.itm_cd = UPPER(:x_itemCode) "


            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
            objsql.Parameters(":x_itemCode").Value = s_itemCode

            'objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            'objsql.Parameters(":x_storeCode").Value = s_storeCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    b_Continue = True

                    txt_sku.Text = txt_sku.Text.ToUpper 'force uppercase on what user entered

                    txt_des.Text = Mydatareader.Item("DES").ToString
                    txt_vsn.Text = Mydatareader.Item("VSN").ToString
                    txt_itm_tp_cd.Text = Mydatareader.Item("ITM_TP_CD").ToString
                    txt_drop_cd.Text = Mydatareader.Item("DROP_CD").ToString
                    txt_comm_cd.Text = Mydatareader.Item("COMM_CD").ToString
                    txt_related_itm_cd.Text = Mydatareader.Item("RELATED_ITM_CD").ToString
                    txt_vendor_code.Text = Mydatareader.Item("brick_vendor_code").ToString
                    txt_po_buffer_days.Text = Mydatareader.Item("po_buffer_days").ToString

                    If (String.IsNullOrEmpty(Trim(txt_po_buffer_days.Text.ToString()))) Then
                        txt_po_buffer_days.Text = 0
                    End If

                Else

                    'MsgBox("Invalid Item [" + s_itemCode + "]", Title:="Error", MsgBoxStyle.Exclamation)
                    'Response.Write("<script type=""text/javascript"">alert(""Invalid Item [" + s_itemCode + "]"");</script")

                    Dim s_errorMessage As String = ""
                    Dim s_employeeCode = Session("EMP_CD")

                    's_errorMessage = "ERROR: Invalid Item [" + s_itemCode + "] - Employee [" + s_employeeCode + "]"
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    's_errorMessage = "ERROR: Item '" + s_itemCode + "' is not a valid inventory item or does not exist"
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0057, s_itemCode)
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                    'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                    'lbl_resultsInfo.Text = s_errorMessage
                    'lbl_resultsInfo.ForeColor = Color.Red
                    DisplayMessage(s_errorMessage, "LABEL", "ERROR")

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

            If b_Continue Then

                ' populate the SKU sort codes
                PopulateSKUSortCodes(s_itemCode)
                RetrieveItemQuantities(s_itemCode, s_storeCode)

                'Alice Added on Nov 19,2018 for request 4350
                RetrieveVendorQuantities(s_itemCode, s_storeCode)

                RetrievePOQuantities(s_itemCode, s_storeCode)
                RetrievePOArrivalSoonest(s_itemCode, s_storeCode)

                txt_lead_time_to_store.Text = RetrieveLeadTime(s_itemCode, p_storeCode).ToString

            End If
        End If

    End Sub
    
     Private Sub RetrieveIdentificationInfo(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_storeCode As String = p_storeCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        If (String.IsNullOrEmpty(Trim(p_itemCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Item code required"
            s_errorMessage = Resources.CustomMessages.MSG0058
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        ElseIf (String.IsNullOrEmpty(Trim(p_storeCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Store code required."
            s_errorMessage = Resources.CustomMessages.MSG0056
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand
            SQL = "select ii_cd from iicst where iicst.itm_cd = '" & p_itemCode & "' And iicst.st_cd = '" + txt_store_cd.Text + "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                While Mydatareader.Read()
                    lst_identification_code.Items.Add(Mydatareader.Item("ii_cd").ToString)
                End While
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If
    End Sub


    'Alice added on Nov 19. 2018 for request 4350
    Private Sub RetrieveVendorQuantities(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""
        Dim oAdp As OracleDataAdapter
        Dim b_Continue As Boolean = False
        Dim ds As DataSet = New DataSet
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "Select e.whs_qty_oh,e.whs_qty_dt, e.whs_qty_onorder, e.whs_shp_dt From store_grp sg, store_grp$store sgg, VE_INV2STORE_GRP vsg, edi_846ib e"
        SQL = SQL & " Where sg.store_grp_cd = sgg.store_grp_cd And sg.store_grp_cd = vsg.store_grp_cd And vsg.whs_id = e.whs_id And sgg.store_cd=UPPER(:x_storeCode) And e.itm_cd=UPPER(:x_itemCode)"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            'Store Values in String Variables 

            If ds.Tables(0).Rows.Count <> 0 Then
                b_Continue = True

                txt_reg_avail_vendor.Text = ds.Tables(0).Rows(0)("whs_qty_oh").ToString
                txt_outlet_avail_vendor.Text = ds.Tables(0).Rows(0)("whs_qty_onorder").ToString

                'alice update on Mar29, 2019 for catch null datetime value

                If ds.Tables(0).Rows(0)("whs_qty_dt").ToString = "" Then
                    lit_reg_date_vendor.Text = ""
                Else
                    lit_reg_date_vendor.Text = Convert.ToDateTime(ds.Tables(0).Rows(0)("whs_qty_dt")).ToShortDateString
                End If
                '  lit_reg_date_vendor.Text = IIf(ds.Tables(0).Rows(0)("whs_qty_dt") Is System.DBNull.Value, "", Convert.ToDateTime(ds.Tables(0).Rows(0)("whs_qty_dt")).ToShortDateString) 



                If ds.Tables(0).Rows(0)("whs_shp_dt").ToString = "" Then
                    lit_outlet_date_vendor.Text = ""
                Else
                    lit_outlet_date_vendor.Text = Convert.ToDateTime(ds.Tables(0).Rows(0)("whs_shp_dt")).ToShortDateString
                End If

                'lit_outlet_date_vendor.Text = IIf(ds.Tables(0).Rows(0)("whs_shp_dt") Is System.DBNull.Value, "", Convert.ToDateTime(ds.Tables(0).Rows(0)("whs_shp_dt")).ToShortDateString) 
            Else
                txt_reg_avail_vendor.Text = ""
                lit_reg_date_vendor.Text = ""
                txt_outlet_avail_vendor.Text = ""
                lit_outlet_date_vendor.Text = ""
            End If
            Mydatareader.Close()

        Catch ex As Exception
            Throw
            conn.Close()
        End Try
        conn.Close()

    End Sub
    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveItemQuantities()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves and populates the item quantity information within each bucket based on query parameters entered by the user.
    '   Called by:      RetrieveItemInfo() - if the item is found/valid
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveItemQuantities(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        'Dim s_itemCode As String = p_itemCode.ToUpper
        'Dim s_storeCode As String = p_storeCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        '---(2)-----------------------------------------------------
        SQL = ""
        SQL = SQL & "SELECT itm_cd, store_cd, "

        SQL = SQL & "SUM(reg_reserved_show) AS reg_reserved_show_total, "
        SQL = SQL & "SUM(reg_reserved_ware) AS reg_reserved_ware_total, "
        SQL = SQL & "SUM(reg_avail_show) AS reg_avail_show_total ,"
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts
        'SQL = SQL & "SUM(reg_avail_ware) AS reg_avail_ware_total, "
        If (IsWHSEStore(p_storeCode)) Then
            SQL = SQL & "reg_avail_ware AS reg_avail_ware_total, "
        Else
            SQL = SQL & "SUM(reg_avail_ware) AS reg_avail_ware_total, "
        End If
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Ends

        SQL = SQL & "SUM(out_reserved_show) AS out_reserved_show_total, "
        SQL = SQL & "SUM(out_reserved_ware) AS out_reserved_ware_total, "
        SQL = SQL & "SUM(out_avail_show) + SUM(out_avail2_show) AS out_avail_show_total, "
        SQL = SQL & "SUM(out_avail_ware) + SUM(out_avail2_ware) AS out_avail_ware_total, "

        SQL = SQL & "SUM(fm_reserved_show) AS fm_reserved_show_total, "
        SQL = SQL & "SUM(fm_reserved_ware) AS fm_reserved_ware_total, "
        SQL = SQL & "SUM(fm_avail_show) AS fm_avail_show_total, "
        SQL = SQL & "SUM(fm_avail_ware) AS fm_avail_ware_total, "

        SQL = SQL & "SUM(nas_reserved_show) AS nas_reserved_show_total, "
        SQL = SQL & "SUM(nas_reserved_ware) AS nas_reserved_ware_total, "
        SQL = SQL & "SUM(nas_avail_show) AS nas_avail_show_total, "
        SQL = SQL & "SUM(nas_avail_ware) AS nas_avail_ware_total "

        SQL = SQL & "FROM ("

        '---(1)-----------------------------------------------------
        SQL = SQL & "SELECT i.itm_cd, "
        SQL = SQL & "ix.qty as quantity, ix.store_cd, ix.loc_cd, l.loc_tp_cd, ix.disposition, ix.nas_out_cd, "

        '--Regular Reserved Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'S',1,0) AS reg_reserved_show, "
        '--Regular Reserved Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'W',1,0) AS reg_reserved_ware, "

        '--Regular Available Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'FIF', DECODE(l.loc_tp_cd, 'O', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'S',1,0) AS reg_avail_show, "
        '--Regular Available Warehouse
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts
        'SQL = SQL & "DECODE( DECODE(ix.disposition, 'FIF', DECODE(l.loc_tp_cd, 'O', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'W',1,0) AS reg_avail_ware, "
        If (IsWHSEStore(p_storeCode)) Then
            SQL = SQL & "NVL(if.qty, 0) As reg_avail_ware,"
        Else
            SQL = SQL & "DECODE( DECODE(ix.disposition, 'FIF', DECODE(l.loc_tp_cd, 'O', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'W',1,0) AS reg_avail_ware, "
        End If
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Ends

        '--Outlet Reserved Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, NULL, 'FM', NULL, DECODE((select 1 from out_cd where out_cd = ix.nas_out_cd),1,DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'),NULL)), NULL),'S',1,0) AS out_reserved_show, "
        '--Outlet Reserved Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, NULL, 'FM', NULL, DECODE((select 1 from out_cd where out_cd = ix.nas_out_cd),1,DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'),NULL)), NULL),'W',1,0) AS out_reserved_ware, "

        '--Outlet Available Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'FIF', DECODE(l.loc_tp_cd, 'O', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'S',1,0) AS out_avail_show, "
        '--Outlet Available Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'FIF', DECODE(l.loc_tp_cd, 'O', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'W',1,0) AS out_avail_ware, "

        '--Outlet Available(2) Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'OUT', DECODE(ix.nas_out_cd, 'FM', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'S',1,0) AS out_avail2_show, "
        '--Outlet Available(2) Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'OUT', DECODE(ix.nas_out_cd, 'FM', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'W',1,0) AS out_avail2_ware, "

        '--Floor Model Reserved Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, 'FM', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'S',1,0) AS fm_reserved_show, "
        '--Floor Model Reserved Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, 'FM', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'W',1,0) AS fm_reserved_ware, "

        '--Floor Model Available Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'NAS', DECODE(ix.nas_out_cd, 'FM', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'S',1,0) AS fm_avail_show, "
        '--Floor Model Available Warehouse
        SQL = SQL & "DECODE(DECODE(ix.disposition, 'NAS', DECODE(ix.nas_out_cd, 'FM', DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'), NULL), NULL),'W',1,0) AS fm_avail_ware, "

        '--NAS Reserved Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, NULL, 'FM', NULL, DECODE((select 1 from nas_cd where nas_cd = ix.nas_out_cd),1,DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'),NULL)), NULL),'S',1,0) AS nas_reserved_show, "
        '--NAS Reserved Warehouse
        SQL = SQL & "DECODE(DECODE(ix.disposition, 'RES', DECODE(ix.nas_out_cd, NULL, NULL, 'FM', NULL, DECODE((select 1 from nas_cd where nas_cd = ix.nas_out_cd),1,DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W'),NULL)), NULL),'W',1,0) AS nas_reserved_ware, "

        '--NAS Available Showroom
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'NAS', DECODE(ix.nas_out_cd, 'FM', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'S',1,0) AS nas_avail_show, "
        '--NAS Available Warehouse
        SQL = SQL & "DECODE( DECODE(ix.disposition, 'NAS', DECODE(ix.nas_out_cd, 'FM', NULL, DECODE(SUBSTR(l.loc_cd,1,1),'S','S','W')), NULL),'W',1,0) AS nas_avail_ware "

        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts
        'SQL = SQL & "FROM inv_xref ix, itm i, ve v, loc l "
        If (IsWHSEStore(p_storeCode)) Then
            SQL = SQL & "FROM inv_xref ix, itm i, ve v, loc l ,itm_fifl if "
        Else
            SQL = SQL & "FROM inv_xref ix, itm i, ve v, loc l "
        End If
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Ends
        SQL = SQL & "WHERE i.ve_cd = v.ve_cd "
        SQL = SQL & "AND i.itm_cd = ix.itm_cd "
        SQL = SQL & "AND ix.store_cd = l.store_cd "
        SQL = SQL & "AND ix.loc_cd = l.loc_cd "
        SQL = SQL & "AND i.itm_tp_cd = 'INV' "
        'Code Added by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts
        If (IsWHSEStore(p_storeCode)) Then
            SQL = SQL & "AND if.itm_cd(+) = ix.itm_cd "
            SQL = SQL & "AND if.store_cd(+) = ix.store_cd "
            SQL = SQL & "AND if.loc_cd(+) = 'WHSE' "
        End If
        'Code Added by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-20155 - Ends
        SQL = SQL & "AND i.itm_cd = UPPER(:x_itemCode) "
        SQL = SQL & "AND ix.store_cd = UPPER(:x_storeCode) "

        SQL = SQL & "ORDER BY store_cd, loc_tp_cd, disposition "
        '---(1)-----------------------------------------------------

        SQL = SQL & ") "
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts
        'SQL = SQL & "GROUP BY itm_cd, store_cd"
        If (IsWHSEStore(p_storeCode)) Then
            SQL = SQL & "GROUP BY itm_cd, store_cd, reg_avail_ware"
        Else
            SQL = SQL & "GROUP BY itm_cd, store_cd"
        End If
        'Code Modified by Kumaran for POS+ IALQ � Warehouse Available Inventory on 01-Dec-2015 - Starts

        '---(2)-----------------------------------------------------

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                txt_reg_reserved_showroom.Text = Mydatareader.Item("reg_reserved_show_total").ToString
                txt_reg_reserved_warehouse.Text = Mydatareader.Item("reg_reserved_ware_total").ToString
                txt_reg_avail_showroom.Text = Mydatareader.Item("reg_avail_show_total").ToString
                txt_reg_avail_warehouse.Text = Mydatareader.Item("reg_avail_ware_total").ToString

                txt_outlet_reserved_showroom.Text = Mydatareader.Item("out_reserved_show_total").ToString
                txt_outlet_reserved_warehouse.Text = Mydatareader.Item("out_reserved_ware_total").ToString
                txt_outlet_avail_showroom.Text = Mydatareader.Item("out_avail_show_total").ToString
                txt_outlet_avail_warehouse.Text = Mydatareader.Item("out_avail_ware_total").ToString

                txt_fm_reserved_showroom.Text = Mydatareader.Item("fm_reserved_show_total").ToString
                txt_fm_reserved_warehouse.Text = Mydatareader.Item("fm_reserved_ware_total").ToString
                txt_fm_avail_showroom.Text = Mydatareader.Item("fm_avail_show_total").ToString
                txt_fm_avail_warehouse.Text = Mydatareader.Item("fm_avail_ware_total").ToString

                txt_nas_reserved_showroom.Text = Mydatareader.Item("nas_reserved_show_total").ToString
                txt_nas_reserved_warehouse.Text = Mydatareader.Item("nas_reserved_ware_total").ToString
                txt_nas_avail_showroom.Text = Mydatareader.Item("nas_avail_show_total").ToString
                txt_nas_avail_warehouse.Text = Mydatareader.Item("nas_avail_ware_total").ToString

                txt_reg_total_showroom.Text = Convert.ToDecimal(txt_reg_reserved_showroom.Text) + Convert.ToDecimal(txt_reg_avail_showroom.Text)
                txt_reg_total_warehouse.Text = Convert.ToDecimal(txt_reg_reserved_warehouse.Text) + Convert.ToDecimal(txt_reg_avail_warehouse.Text)
                txt_reg_total_store.Text = Convert.ToDecimal(txt_reg_total_showroom.Text) + Convert.ToDecimal(txt_reg_total_warehouse.Text)
                txt_reg_total_reserved.Text = Convert.ToDecimal(txt_reg_reserved_showroom.Text) + Convert.ToDecimal(txt_reg_reserved_warehouse.Text)
                txt_reg_total_available.Text = Convert.ToDecimal(txt_reg_avail_showroom.Text) + Convert.ToDecimal(txt_reg_avail_warehouse.Text)

                txt_outlet_total_showroom.Text = Convert.ToDecimal(txt_outlet_reserved_showroom.Text) + Convert.ToDecimal(txt_outlet_avail_showroom.Text)
                txt_outlet_total_warehouse.Text = Convert.ToDecimal(txt_outlet_reserved_warehouse.Text) + Convert.ToDecimal(txt_outlet_avail_warehouse.Text)
                txt_outlet_total_store.Text = Convert.ToDecimal(txt_outlet_total_showroom.Text) + Convert.ToDecimal(txt_outlet_total_warehouse.Text)
                txt_outlet_total_reserved.Text = Convert.ToDecimal(txt_outlet_reserved_showroom.Text) + Convert.ToDecimal(txt_outlet_reserved_warehouse.Text)
                txt_outlet_total_available.Text = Convert.ToDecimal(txt_outlet_avail_showroom.Text) + Convert.ToDecimal(txt_outlet_avail_warehouse.Text)

                txt_fm_total_showroom.Text = Convert.ToDecimal(txt_fm_reserved_showroom.Text) + Convert.ToDecimal(txt_fm_avail_showroom.Text)
                txt_fm_total_warehouse.Text = Convert.ToDecimal(txt_fm_reserved_warehouse.Text) + Convert.ToDecimal(txt_fm_avail_warehouse.Text)
                txt_fm_total_store.Text = Convert.ToDecimal(txt_fm_total_showroom.Text) + Convert.ToDecimal(txt_fm_total_warehouse.Text)
                txt_fm_total_reserved.Text = Convert.ToDecimal(txt_fm_reserved_showroom.Text) + Convert.ToDecimal(txt_fm_reserved_warehouse.Text)
                txt_fm_total_available.Text = Convert.ToDecimal(txt_fm_avail_showroom.Text) + Convert.ToDecimal(txt_fm_avail_warehouse.Text)

                txt_nas_total_showroom.Text = Convert.ToDecimal(txt_nas_reserved_showroom.Text) + Convert.ToDecimal(txt_nas_avail_showroom.Text)
                txt_nas_total_warehouse.Text = Convert.ToDecimal(txt_nas_reserved_warehouse.Text) + Convert.ToDecimal(txt_nas_avail_warehouse.Text)
                txt_nas_total_store.Text = Convert.ToDecimal(txt_nas_total_showroom.Text) + Convert.ToDecimal(txt_nas_total_warehouse.Text)
                txt_nas_total_reserved.Text = Convert.ToDecimal(txt_nas_reserved_showroom.Text) + Convert.ToDecimal(txt_nas_reserved_warehouse.Text)
                txt_nas_total_available.Text = Convert.ToDecimal(txt_nas_avail_showroom.Text) + Convert.ToDecimal(txt_nas_avail_warehouse.Text)

            Else

                Dim s_errorMessage As String = ""
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0034, p_itemCode, p_storeCode)
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()



    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   LEAD TIME
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveLeadTime()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves lead days for item/vendor
    '   Called by:      RetrieveItemInfo()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function RetrieveLeadTime(p_itemCode As String, p_storeCode As String) As Integer

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False
        Dim s_result As String = "N"
        Dim n_result As Integer = -1
        Dim b_hasAccess As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = "DECLARE "
        SQL = SQL & "n_result NUMBER(5) := 0; "
        SQL = SQL & "s_result VARCHAR(1) := 'N'; "
        SQL = SQL & "BEGIN "
        SQL = SQL & "  :x_result := BT_ITEM_AVAIL.get_lead_days( :x_itemCode, :x_storeCode, NULL, NULL ); "
        SQL = SQL & "END;"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Dim s_resultParameter As OracleParameter = New OracleParameter(":x_result", OracleType.VarChar, 255)
        s_resultParameter.Direction = ParameterDirection.Output
        objsql.Parameters.Add(s_resultParameter)

        objsql.ExecuteNonQuery()

        s_result = s_resultParameter.Value
        n_result = s_resultParameter.Value

        conn.Close()

        Return n_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   PO DATA RETRIEVAL
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrievePOQuantities()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves PO reserved and available quantities
    '   Called by:      RetrieveItemInfo()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrievePOQuantities(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False
        Dim s_result As String = "N"
        Dim n_result As Integer = -1
        Dim b_hasAccess As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = ""
        SQL = SQL & "SELECT itm_cd, sum(resqty) total_reserved_qty, sum(ordqty - rcvqty - resqty) total_available_qty "
        SQL = SQL & "FROM ( "
        SQL = SQL & "   SELECT p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord ordqty, bt_adv_res.get_po_received_qty(p.po_cd,pl.ln#) rcvqty, "
        SQL = SQL & "   bt_adv_res.get_po_reserved_qty(p.po_cd,pl.ln#,pl.itm_cd) resqty, pl.arrival_dt "
        SQL = SQL & "   FROM po p, po_ln pl, po_ln$actn_hst ph "
        SQL = SQL & "   WHERE p.po_cd = pl.po_cd "
        SQL = SQL & "   AND p.po_cd = ph.po_cd "
        SQL = SQL & "   AND pl.ln# = ph.ln# "
        SQL = SQL & "   AND p.stat_cd ='O' "
        SQL = SQL & "   AND ph.po_actn_tp_cd IN ('AKF','AKR','AKI','ADC','AKQ','855','ASN') "
        SQL = SQL & "   AND pl.dest_store_cd IN ( "
        SQL = SQL & "   SELECT store_cd "
        SQL = SQL & "   FROM store_grp$store sgs1 "
        SQL = SQL & "   WHERE store_grp_cd = ( "
        SQL = SQL & "       SELECT MAX(sgs2.store_grp_cd) "
        SQL = SQL & "       FROM store_grp$store sgs2, store_grp sgx "
        SQL = SQL & "       WHERE sgs2.store_cd = UPPER(:x_storeCode) "
        SQL = SQL & "       AND sgs2.store_grp_cd = sgx.store_grp_cd "
        SQL = SQL & "       AND sgx.store_grp_tp = 'DC') "
        SQL = SQL & "   ) "
        SQL = SQL & "   AND pl.arrival_dt >= TRUNC(SYSDATE) "
        SQL = SQL & "   AND pl.itm_cd = UPPER(:x_itemCode) "
        SQL = SQL & "   GROUP BY p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord, pl.arrival_dt "
        SQL = SQL & "   ) vw "
        SQL = SQL & "WHERE (ordqty - rcvqty ) > 0 " 'not fully received
        SQL = SQL & "GROUP BY itm_cd"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                txt_po_total_reserved.Text = Mydatareader.Item("total_reserved_qty").ToString
                txt_po_total_available.Text = Mydatareader.Item("total_available_qty").ToString

            Else
                'Alice update on Sep 24, 2018 to retrieve data for Leon's store
                Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim SQL2 As String
                Dim Mydatareader2 As OracleDataReader
                Dim objsql2 As OracleCommand

                Dim s_result2 As String = "N"
                Dim n_result2 As Integer = -1


                conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn2.Open()
                SQL2 = ""
                SQL2 = SQL2 & " Select itm_cd, sum(resqty) total_reserved_qty, sum(ordqty - rcvqty - resqty) total_available_qty "

                SQL2 = SQL2 & "FROM( SELECT p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord ordqty, bt_adv_res.get_po_received_qty(p.po_cd, pl.ln#) rcvqty,  "

                SQL2 = SQL2 & " bt_adv_res.get_po_reserved_qty(p.po_cd, pl.ln#, pl.itm_cd) resqty, pl.arrival_dt "

                SQL2 = SQL2 & "From po p, po_ln pl, po_ln$actn_hst ph "

                SQL2 = SQL2 & "WHERE p.po_cd = pl.po_cd  And p.po_cd = ph.po_cd  And pl.ln# = ph.ln#  And p.stat_cd ='O'"

                SQL2 = SQL2 & " And ph.po_actn_tp_cd IN ('AKF','AKR','AKI','ADC','AKQ','855','ASN')"

                SQL2 = SQL2 & " And pl.dest_store_cd =:x_storeCode "

                SQL2 = SQL2 & "And pl.arrival_dt >= TRUNC(SYSDATE)"

                SQL2 = SQL2 & " And pl.itm_cd =  UPPER(:x_itemCode) "

                SQL2 = SQL2 & " Group BY p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord, pl.arrival_dt ) vw"
                SQL2 = SQL2 & " WHERE(ordqty - rcvqty) > 0    Group BY itm_cd"

                'Set SQL OBJECT 
                objsql2 = DisposablesManager.BuildOracleCommand(SQL2, conn2)

                objsql2.Parameters.Add(":x_itemCode", OracleType.VarChar)
                objsql2.Parameters(":x_itemCode").Value = p_itemCode

                objsql2.Parameters.Add(":x_storeCode", OracleType.VarChar)
                objsql2.Parameters(":x_storeCode").Value = p_storeCode

                Try
                    Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    'Store Values in String Variables 
                    If Mydatareader2.Read() Then

                        b_Continue = True

                        txt_po_total_reserved.Text = Mydatareader2.Item("total_reserved_qty").ToString
                        txt_po_total_available.Text = Mydatareader2.Item("total_available_qty").ToString

                    Else
                        txt_po_total_reserved.Text = "NA"
                        txt_po_total_available.Text = "NA"
                    End If
                    Mydatareader2.Close()
                Catch ex As Exception
                    Throw
                    conn2.Close()
                End Try
                conn2.Close()
            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        If b_Continue Then
            'do nothing
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrievePOArrivalSoonest()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves soonest PO available date and quantity
    '   Called by:      RetrieveItemInfo()
    '
    '   Label save:
    '       ---------------------------------------------------------- Purchase Orders ----------------------------------------------------------
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrievePOArrivalSoonest(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False
        Dim s_result As String = "N"
        Dim n_result As Integer = -1
        Dim b_hasAccess As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = ""
        SQL = SQL & "SELECT itm_cd, dest_store_cd, arrival_dt, TO_CHAR(arrival_dt,'DD-MON-YYYY') as str_arrival_dt, sum(ordqty - rcvqty - resqty) available_qty "
        SQL = SQL & "FROM ( "
        SQL = SQL & "   SELECT p.po_cd, pl.ln#, pl.itm_cd, pl.dest_store_cd, pl.qty_ord ordqty, bt_adv_res.get_po_received_qty(p.po_cd,pl.ln#) rcvqty, "
        SQL = SQL & "   bt_adv_res.get_po_reserved_qty(p.po_cd, pl.ln#, pl.itm_cd) resqty, pl.arrival_dt "
        SQL = SQL & "   FROM po p, po_ln pl, po_ln$actn_hst ph "
        SQL = SQL & "   WHERE p.po_cd = pl.po_cd "
        SQL = SQL & "   AND p.po_cd = ph.po_cd "
        SQL = SQL & "   AND pl.ln# = ph.ln# "
        SQL = SQL & "   AND p.stat_cd ='O' "
        SQL = SQL & "   AND ph.po_actn_tp_cd IN ('AKF','AKR','AKI','ADC','AKQ','855','ASN') "
        SQL = SQL & "   AND pl.dest_store_cd IN ( "
        SQL = SQL & "   SELECT store_cd "
        SQL = SQL & "   FROM store_grp$store sgs1 "
        SQL = SQL & "   WHERE store_grp_cd = ( "
        SQL = SQL & "       SELECT MAX(sgs2.store_grp_cd) "
        SQL = SQL & "       FROM store_grp$store sgs2, store_grp sgx "
        SQL = SQL & "       WHERE sgs2.store_cd = UPPER(:x_storeCode) "
        SQL = SQL & "       AND sgs2.store_grp_cd = sgx.store_grp_cd "
        SQL = SQL & "       AND sgx.store_grp_tp = 'DC') "
        SQL = SQL & "   ) "
        SQL = SQL & "   AND pl.arrival_dt >= TRUNC(SYSDATE) "
        SQL = SQL & "   AND pl.itm_cd = UPPER(:x_itemCode) "
        SQL = SQL & "   GROUP BY p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord, pl.arrival_dt, pl.dest_store_cd "
        SQL = SQL & ") vw "
        SQL = SQL & "WHERE (ordqty - rcvqty - resqty ) > 0 "
        'SQL = SQL & "AND rownum = 1 "
        SQL = SQL & "GROUP BY itm_cd, dest_store_cd, arrival_dt "
        SQL = SQL & "ORDER BY arrival_dt"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)


            Dim b_haveFirstRecord = False

            If Not b_haveFirstRecord Then

                If Mydatareader.Read() Then

                    b_Continue = True

                    txt_po_arrival_date_soonest.Text = Mydatareader.Item("str_arrival_dt").ToString
                    txt_po_available_soonest.Text = Mydatareader.Item("available_qty").ToString
                    txt_po_dest_store_cd.Text = Mydatareader.Item("dest_store_cd").ToString

                    Dim s_arrival_date As String = txt_po_arrival_date_soonest.Text
                    Dim d_arrival_date As Date
                    Dim n_buffer_days As Integer = Convert.ToDecimal(txt_po_buffer_days.Text)

                    'd_arrival_date = Date.ParseExact(s_arrival_date, "dd-MMM-yyyy", Nothing)  'System.Globalization.CultureInfo.InvariantCulture
                    d_arrival_date = Date.Parse(s_arrival_date)
                    d_arrival_date = d_arrival_date.AddDays(n_buffer_days)
                    txt_po_arrival_date_soonest.Text = d_arrival_date.ToString("dd-MMM-yyyy")

                    b_haveFirstRecord = True

                Else

                    Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

                    Dim SQL2 As String
                    Dim Mydatareader2 As OracleDataReader
                    Dim objsql2 As OracleCommand


                    Dim s_result2 As String = "N"
                    Dim n_result2 As Integer = -1
                    Dim b_hasAccess2 As Boolean = False

                    conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                    conn2.Open()

                    SQL2 = ""
                    SQL2 = SQL2 & "Select itm_cd, dest_store_cd, arrival_dt, TO_CHAR(arrival_dt,'DD-MON-YYYY') as str_arrival_dt, sum(ordqty - rcvqty - resqty) available_qty "
                    SQL2 = SQL2 & "FROM ( "
                    SQL2 = SQL2 & "   SELECT p.po_cd, pl.ln#, pl.itm_cd, pl.dest_store_cd, pl.qty_ord ordqty, bt_adv_res.get_po_received_qty(p.po_cd,pl.ln#) rcvqty, "
                    SQL2 = SQL2 & "   bt_adv_res.get_po_reserved_qty(p.po_cd, pl.ln#, pl.itm_cd) resqty, pl.arrival_dt "
                    SQL2 = SQL2 & "   FROM po p, po_ln pl, po_ln$actn_hst ph "
                    SQL2 = SQL2 & "   WHERE p.po_cd = pl.po_cd "
                    SQL2 = SQL2 & "   AND p.po_cd = ph.po_cd "
                    SQL2 = SQL2 & "   AND pl.ln# = ph.ln# "
                    SQL2 = SQL2 & "   AND p.stat_cd ='O' "
                    SQL2 = SQL2 & "   AND ph.po_actn_tp_cd IN ('AKF','AKR','AKI','ADC','AKQ','855','ASN') "
                    SQL2 = SQL2 & "   AND pl.dest_store_cd IN ( "
                    SQL2 = SQL2 & "   SELECT store_cd "
                    SQL2 = SQL2 & "   FROM store_grp$store sgs1 "
                    SQL2 = SQL2 & "   WHERE store_grp_cd = ( "
                    SQL2 = SQL2 & "       SELECT MAX(sgs2.store_grp_cd) "
                    SQL2 = SQL2 & "       FROM store_grp$store sgs2, store_grp sgx "
                    SQL2 = SQL2 & "       WHERE sgs2.store_cd = UPPER(:x_storeCode) "
                    SQL2 = SQL2 & "       AND sgs2.store_grp_cd = sgx.store_grp_cd) "
                    'SQL2 = SQL2 & "       AND sgx.store_grp_tp = 'DC') "
                    SQL2 = SQL2 & "   ) "
                    SQL2 = SQL2 & "   AND pl.arrival_dt >= TRUNC(SYSDATE) "
                    SQL2 = SQL2 & "   AND pl.itm_cd = UPPER(:x_itemCode) "
                    SQL2 = SQL2 & "   GROUP BY p.po_cd, pl.ln#, pl.itm_cd, pl.qty_ord, pl.arrival_dt, pl.dest_store_cd "
                    SQL2 = SQL2 & ") vw "
                    SQL2 = SQL2 & "WHERE (ordqty - rcvqty - resqty ) > 0 "
                    'SQL2 = SQL2 & "AND rownum = 1 "
                    SQL2 = SQL2 & "GROUP BY itm_cd, dest_store_cd, arrival_dt "
                    SQL2 = SQL2 & "ORDER BY arrival_dt"

                    'Set SQL2 OBJECT 
                    objsql2 = DisposablesManager.BuildOracleCommand(SQL2, conn)

                    objsql2.Parameters.Add(":x_itemCode", OracleType.VarChar)
                    objsql2.Parameters(":x_itemCode").Value = p_itemCode

                    objsql2.Parameters.Add(":x_storeCode", OracleType.VarChar)
                    objsql2.Parameters(":x_storeCode").Value = p_storeCode
                    Try


                        'Execute DataReader 
                        Mydatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                        If Mydatareader2.Read() Then

                            b_Continue = True
                            txt_po_available_soonest.Text = Mydatareader2.Item("available_qty").ToString
                            txt_po_dest_store_cd.Text = Mydatareader2.Item("dest_store_cd").ToString
                            txt_po_arrival_date_soonest.Text = Mydatareader2.Item("str_arrival_dt").ToString


                            Dim s_arrival_date As String = txt_po_arrival_date_soonest.Text
                            Dim d_arrival_date As Date
                            Dim n_buffer_days As Integer = Convert.ToDecimal(txt_po_buffer_days.Text)

                            'd_arrival_date = Date.ParseExact(s_arrival_date, "dd-MMM-yyyy", Nothing)  'System.Globalization.CultureInfo.InvariantCulture
                            d_arrival_date = Date.Parse(s_arrival_date)
                            d_arrival_date = d_arrival_date.AddDays(n_buffer_days)
                            txt_po_arrival_date_soonest.Text = d_arrival_date.ToString("dd-MMM-yyyy")
                            b_haveFirstRecord = True

                        Else
                            txt_po_arrival_date_soonest.Text = "NA"
                            txt_po_available_soonest.Text = "NA"
                            txt_po_dest_store_cd.Text = "NA"
                        End If

                    Catch ex As Exception
                        Throw
                        conn2.Close()
                    End Try
                    Mydatareader2.Close()
                    conn2.Close()

                End If
            Else
                'Done, just exit the loop
                Mydatareader.Close()
                Exit Try

            End If

            Mydatareader.Close()

        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        If b_Continue Then
            'do nothing
        End If

    End Sub



    '------------------------------------------------------------------------------------------------------------------------------------
    '   OTHER
    '------------------------------------------------------------------------------------------------------------------------------------


    'Private Sub PrintRows(ByVal dataSet As DataSet)
    '    Dim table As DataTable
    '    Dim row As DataRow
    '    Dim column As DataColumn
    '    ' For each table in the DataSet, print the row values. 
    '    For Each table In dataSet.Tables
    '        For Each row In table.Rows
    '            For Each column In table.Columns
    '                Console.WriteLine(row(column))
    '            Next column
    '        Next row
    '    Next table
    'End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PrintDataView()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Debugging procedure for dataviews
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    'Private Shared Sub PrintDataView(dv As DataView)
    '    ' Printing first DataRowView to demo that the row in the first index of the DataView changes depending on sort and filters
    '    'Console.WriteLine("First DataRowView value is '{0}'", dv(0)("priority"))
    '    MsgBox("First DataRowView value is '{0}' " + dv(0)("priority") + "; prc_cd: " + dv(0)("prc_cd") + "; des: " + dv(0)("des"))

    '    ' Printing all DataRowViews 
    '    For Each drv As DataRowView In dv

    '        'Console.WriteLine(vbTab & " {0}", drv("priority"))
    '        'MsgBox(" {0}" + drv("priority"))
    '        MsgBox(" {0} " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des"))

    '    Next
    'End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation, Title:="Error")
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = MsgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = MsgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = MsgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_resultsInfo.Visible = False

        If Not IsPostBack Then
            '    If Find_Security("ISTM", Session("emp_cd")) = "N" Then
            'btn_Lookup.Enabled = False
            'btn_Clear.Enabled = False
            'lbl_Cust_Info.Text = "You don't have the appropriate securities to view Franchise-Store percentages."
            'End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then

                btn_Search.Enabled = False

            Else

                'JAM: populate the store codes drop-down list
                PopulateStoreCodes()

            End If
        End If

        'txt_store_cd.SelectedValue = ConfigurationManager.AppSettings("default_store_code").ToString

        lbl_versionInfo.Text = gs_version

        txt_des.ReadOnly = True
        txt_vendor_code.ReadOnly = True
        txt_vsn.ReadOnly = True
        txt_itm_tp_cd.ReadOnly = True
        txt_drop_cd.ReadOnly = True
        txt_comm_cd.ReadOnly = True
        txt_related_itm_cd.ReadOnly = True

        txt_reg_total_showroom.ReadOnly = True
        txt_reg_reserved_showroom.ReadOnly = True
        txt_reg_avail_showroom.ReadOnly = True
        txt_reg_total_warehouse.ReadOnly = True
        txt_reg_reserved_warehouse.ReadOnly = True
        txt_reg_avail_warehouse.ReadOnly = True
        txt_reg_total_store.ReadOnly = True
        txt_reg_total_reserved.ReadOnly = True
        txt_reg_total_available.ReadOnly = True

        txt_outlet_total_showroom.ReadOnly = True
        txt_outlet_reserved_showroom.ReadOnly = True
        txt_outlet_avail_showroom.ReadOnly = True
        txt_outlet_total_warehouse.ReadOnly = True
        txt_outlet_reserved_warehouse.ReadOnly = True
        txt_outlet_avail_warehouse.ReadOnly = True
        txt_outlet_total_store.ReadOnly = True
        txt_outlet_total_reserved.ReadOnly = True
        txt_outlet_total_available.ReadOnly = True

        txt_fm_total_showroom.ReadOnly = True
        txt_fm_reserved_showroom.ReadOnly = True
        txt_fm_avail_showroom.ReadOnly = True
        txt_fm_total_warehouse.ReadOnly = True
        txt_fm_reserved_warehouse.ReadOnly = True
        txt_fm_avail_warehouse.ReadOnly = True
        txt_fm_total_store.ReadOnly = True
        txt_fm_total_reserved.ReadOnly = True
        txt_fm_total_available.ReadOnly = True

        txt_nas_total_showroom.ReadOnly = True
        txt_nas_reserved_showroom.ReadOnly = True
        txt_nas_avail_showroom.ReadOnly = True
        txt_nas_total_warehouse.ReadOnly = True
        txt_nas_reserved_warehouse.ReadOnly = True
        txt_nas_avail_warehouse.ReadOnly = True
        txt_nas_total_store.ReadOnly = True
        txt_nas_total_reserved.ReadOnly = True
        txt_nas_total_available.ReadOnly = True

        txt_lead_time_to_store.ReadOnly = True
        txt_po_total_reserved.ReadOnly = True
        txt_po_total_available.ReadOnly = True
        txt_po_arrival_date_soonest.ReadOnly = True
        txt_po_available_soonest.ReadOnly = True

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Copied from IPQ and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_store_cd.Enabled = True

        Response.Redirect("InventoryAvailabilityLocationQuery.aspx")

    End Sub
    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To set title for this page 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = GetLocalResourceObject("hdr_title.Text").ToString()
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Search_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           May2015
    '   Description:    Copied from IPQ and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click

        ResetAllFields()
        'Session("IPQ_SEARCH") = "ON"

        If (isValidItem(txt_sku.Text)) Then

            'Dim s_isValidItemSecurityCheck = IsValidItemSecurityCheck(txt_sku.Text)
            Dim s_isValidItemSecurityCheck = IsValidItemSecurityCheck_LEONS(txt_sku.Text)


            If (s_isValidItemSecurityCheck = "Y") Then

                RetrieveItemInfo(txt_sku.Text, txt_store_cd.Text)
                RetrieveIdentificationInfo(txt_sku.Text, txt_store_cd.Text)
            ElseIf (s_isValidItemSecurityCheck = "N") Then

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
                's_errorMessage = "Security Violation for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = "Employee '" + s_employeeCode + "' does not have the appropriate security privileges to view item '" + txt_sku.Text.ToUpper + "' inventory information."
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0035, s_employeeCode, txt_sku.Text.ToUpper)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Else

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = "Cannot determine security information for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0036, txt_sku.Text.ToUpper, s_employeeCode)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AddError()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub AddError(ByVal errors As Dictionary(Of GridViewColumn, String), ByVal column As GridViewColumn, ByVal errorText As String)
        If errors.ContainsKey(column) Then
            Return
        End If
        errors(column) = errorText
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Checks whether the given store is a Warehouse or not
    ''' </summary>
    ''' <param name="p_storeCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsWHSEStore(ByVal p_storeCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = "SELECT NVL(rf_whse,'Y') AS rf_whse "
        SQL = SQL & "FROM store s "
        SQL = SQL & "WHERE NVL(s.rf_whse,'Y') = 'N' "
        SQL = SQL & "AND s.store_cd = UPPER(:p_store_cd)"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":p_store_cd", OracleType.VarChar)
        objsql.Parameters(":p_store_cd").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                Dim s_rf_whse As String = Mydatareader.Item("rf_whse").ToString
                If (s_rf_whse = "N") Then
                    b_Continue = True
                End If
                Mydatareader.Close()

            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

        Return b_Continue

    End Function

End Class
