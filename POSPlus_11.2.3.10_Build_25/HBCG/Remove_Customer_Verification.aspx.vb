Imports System.Data.OracleClient

Partial Class Remove_Customer_Verification
    Inherits POSBasePage

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        conn.Open()
        sql = "DELETE FROM CUST_INFO WHERE SESSION_ID='" & Request("Session_ID") & "' AND CUST_CD='" & Request("CUST_CD") & "'"

        With cmdDeleteItems
            .Connection = conn
            .CommandText = sql
        End With
        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()

        Session("lname") = System.DBNull.Value
        Session("fname") = System.DBNull.Value
        Session("fname") = System.DBNull.Value
        Session("addr") = System.DBNull.Value
        Session("addr") = System.DBNull.Value
        Session("custcd") = System.DBNull.Value
        Session("bus_phone") = System.DBNull.Value
        Session("home_phone") = System.DBNull.Value
        Session("cust_cd") = System.DBNull.Value
        Session("TAX_RATE") = System.DBNull.Value
        Session("CUST_LNAME") = System.DBNull.Value
        Session("rel_check") = System.DBNull.Value
        Session("ZIP_CD") = Nothing
        Session("ZONE_CD") = Nothing
        Session("ZONE_ZIP_CD") = Nothing

        Dim LEAD
        LEAD = ""
        If Request("LEAD") = "TRUE" Then
            LEAD = "?LEAD=TRUE"
        End If
        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        Response.Write("parentWindow.location.href='customer.aspx" & LEAD & "';" & vbCrLf)
        Response.Write("</script>")

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        Response.Write("</script>")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim LEAD As String
        LEAD = Request("LEAD")
        If Request("SESSION_ID") & "" = "" Or Request("CUST_CD") & "" = "" Then
            lbl_warning.Text = "No customer information was found.  Please close this window and try again."
        Else
            lbl_warning.Text = "*WARNING*  You are about to replace Customer Code " & Request("CUST_CD") & " on this order.  Do you want to proceed?"
        End If

    End Sub
End Class
