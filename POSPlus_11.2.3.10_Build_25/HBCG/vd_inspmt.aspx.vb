﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic
Imports System.Web.SessionState
Imports HBCG_Utils
Imports SalesUtils
Imports System.Math
Partial Class vd_inspmt

    Inherits POSBasePage
    Private LeonsBiz As LeonsBiz = New LeonsBiz() 'sabrina R1999
    Private theSalesBiz As SalesBiz = New SalesBiz() 'sabrina R1999    
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("vd_inspmt") = "Y"

        ' Daniela June 24
        CreateCustomer()

        If Not IsDBNull(Session("cust_cd")) And Not isEmpty(Session("cust_cd")) Then
            Text_cust_cd.Text = Session("cust_cd")
            Text_acct_num.Focus()
        Else
            Text_cust_cd.Focus()
        End If

    End Sub

    Public Function check_dig(ByRef p_acct_num As String, ByRef p_fname As String, ByRef p_lname As String, ByRef p_cust_cd As String) As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Dim str_y_n As String

        Dim str_err As String
        'Session("str_fname") = ""
        'Session("str_lname") = ""
        'Session("str_cust_cd") = ""
        lbl_err.Text = "" 'sabrina Dec18,2015
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_inspmt.check_acct_num"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_acct_num", OracleType.Char)).Value = p_acct_num
        myCMD.Parameters.Add("p_fname", OracleType.VarChar, 40).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_lname", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_cust_cd", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        ' myCMD.Parameters.Add("p_err", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.ExecuteNonQuery()
        ' str_err = myCMD.Parameters("p_err").Value
        str_y_n = myCMD.Parameters("p_y_n").Value
        If str_y_n <> "Y" Then
            'lbl_err.Text = "The account number is invalid" sabrina-french dec14
            lbl_err.Text = Resources.LibResources.Label832
            Return "N"
        ElseIf str_y_n = "Y" Then
            lbl_err.Text = ""
        Else   'this part is not in use, we do not get name from table
            If Not IsDBNull(myCMD.Parameters("p_fname").Value) Then
                Session("str_fname") = myCMD.Parameters("p_fname").Value
                Text_fname.Text = Session("str_fname")
            End If
            If Not IsDBNull(myCMD.Parameters("l_fname").Value) Then
                Session("str_lname") = myCMD.Parameters("p_lname").Value
                Text_lname.Text = Session("str_lname")
            End If
            If Not IsDBNull(myCMD.Parameters("p_cust_cd").Value) Then
                Session("str_cust_cd") = myCMD.Parameters("p_cust_cd").Value
                Text_cust_cd.Text = Session("str_cust_cd")
            End If


        End If

        Try


            Return str_y_n
        Catch x
            Throw
            Return "N"
            objConnection.Close()
        End Try

    End Function

    Public Function check_cust_cd(ByRef p_cust_cd As String) As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Dim str_y_n As String



        lbl_err.Text = "" 'sabrina Dec18,2015
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_inspmt.check_cust_cd"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = p_cust_cd

        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.ExecuteNonQuery()
        ' str_err = myCMD.Parameters("p_err").Value
        str_y_n = myCMD.Parameters("p_y_n").Value
        If str_y_n <> "Y" Then
            'lbl_err.Text = "The customer code is not on file"     sabrina-french dec14
            lbl_err.Text = Resources.LibResources.Label830
        ElseIf str_y_n = "Y" Then
            lbl_err.Text = ""


        End If


        Try
            Return str_y_n
        Catch x
            Throw
            Return "N"
            objConnection.Close()
        End Try

    End Function

    Public Function check_vd_inspmt_store(ByRef p_emp_cd As String) As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Dim str_y_n As String

        lbl_err.Text = "" 'sabrina Dec18,2015
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_inspmt.check_vd_inspmt_store"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.Char)).Value = p_emp_cd

        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.ExecuteNonQuery()

        str_y_n = myCMD.Parameters("p_y_n").Value
        If str_y_n <> "Y" Then
            lbl_err.Text = Resources.LibResources.Label863   'sabrina-french dec14"The store is not on file"
            Exit Function
        ElseIf str_y_n = "Y" Then
            lbl_err.Text = ""

        End If


        Try

            Return str_y_n
        Catch x
            Throw

            objConnection.Close()
        End Try

    End Function
    Public Function check_plan(ByRef p_plan_id As String) As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Dim str_y_n As String

        lbl_err.Text = "" 'sabrina Dec18,2015
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_inspmt.check_plan"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_plan_id", OracleType.Char)).Value = p_plan_id

        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.ExecuteNonQuery()

        str_y_n = myCMD.Parameters("p_y_n").Value
        If str_y_n <> "Y" Then
            lbl_err.Text = Resources.LibResources.Label854 'sabrina-french dec14 "Plan id is invalid"
            Exit Function
        ElseIf str_y_n = "Y" Then
            lbl_err.Text = ""

        End If


        Try

            Return str_y_n
        Catch x
            Throw

            objConnection.Close()
        End Try

    End Function
    Protected Sub text_cust_cd_TextChanged(sender As Object, e As EventArgs) Handles Text_cust_cd.TextChanged
        Dim str_fname As String
        Dim str_lname As String
        Dim str_cust_cd As String
        Dim str_acct As String = Text_acct_num.Text
        Dim str_emp_cd As String
        str_emp_cd = Session("emp_cd")
        Dim str_y_n As String
        If Len(Text_cust_cd.Text) > 2 Then
            Text_cust_cd.Text = UCase(Text_cust_cd.Text)
            str_y_n = check_vd_inspmt_store(str_emp_cd)
            If str_y_n <> "Y" Then
                lbl_err.Text = Resources.LibResources.Label872 'sabrina-french dec14 "Your store is not on file"
                Exit Sub
            End If

            str_y_n = check_cust_cd(Text_cust_cd.Text)
            If str_y_n <> "Y" Then
                lbl_err.Text = Resources.LibResources.Label842 'sabrina-french dec14 "Customer code is invalid"
                Text_cust_cd.Focus()
            End If
            Text_acct_num.Focus()
        Else
            Text_acct_num.Focus()
        End If
    End Sub

    Protected Sub text_acct_num_TextChanged(sender As Object, e As EventArgs) Handles Text_acct_num.TextChanged
        Dim str_fname As String
        Dim str_lname As String
        Dim str_cust_cd As String
        Dim str_acct As String = Text_acct_num.Text
        Dim str_emp_cd As String
        str_emp_cd = Session("emp_cd")
        Dim str_y_n As String
        If Len(Text_acct_num.Text) > 10 Then


            str_y_n = check_dig(str_acct, str_fname, str_lname, str_cust_cd)
            If str_y_n <> "Y" Then

                lbl_err.Text = Resources.LibResources.Label832  'sabrina-french dec14 "Account number is invalid"
                Text_acct_num.Focus()
            Else
                Text_fname.Focus()
            End If
        Else
            Text_acct_num.Focus()
        End If
    End Sub

    Protected Sub Button1_Click()
        'lucy need to do more work
        Dim str_fname As String
        Dim str_lname As String
        Dim str_cust_cd As String
        Dim str_acct As String = Text_acct_num.Text
        Dim str_y_n As String

        lbl_err.Text = "" 'sabrina Dec18,2015
        ' If Len(Text_acct_num.Text) > 15 Then
        If isEmpty(Text_cust_cd.Text) Then
            lbl_err.Text = Resources.LibResources.Label842 'sabrina-french dec14 "Customer code is required"
            Text_cust_cd.Focus()
            Exit Sub
        Else
            Text_cust_cd.Text = UCase(Text_cust_cd.Text)
            str_y_n = check_cust_cd(Text_cust_cd.Text)
            If str_y_n <> "Y" Then
                lbl_err.Text = Resources.LibResources.Label842 'sabrina-french dec14 "Customer code is invalid"
                Text_cust_cd.Focus()
                Exit Sub
            End If

        End If

        If isEmpty(Text_acct_num.Text) Then
            lbl_err.Text = Resources.LibResources.Label833 'sabrina-french dec14 "Account number is required"
            Text_acct_num.Focus()
            Exit Sub
        Else
            'R1999 sabrina Dec15  - for Brick card payment validate card#
            If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
                If Left(Text_acct_num.Text, 2) <> "45" Then
                    lbl_err.Text = Resources.LibResources.Label832 'account number is invalid
                    Text_acct_num.Focus()
                    Exit Sub
                End If
            End If  'sabrina Dec15
            str_y_n = check_dig(str_acct, str_fname, str_lname, str_cust_cd) 'str_fname, str_lname, str_cust_cd not used yet, they are output value
            If str_y_n <> "Y" Then
                lbl_err.Text = Resources.LibResources.Label832 'sabrina-french dec14 "Account number is invalid"
                Exit Sub
                Text_acct_num.Focus()
            End If
        End If

        If isEmpty(Text_fname.Text) Or Len(Text_fname.Text) < 1 Then
            lbl_err.Text = Resources.LibResources.Label843 'sabrina-french dec14 "First name is required "          'sabrina Dec03,2015 
            Text_fname.Focus()
            Exit Sub
        End If

        If isEmpty(Text_lname.Text) Or Len(Text_lname.Text) < 1 Then
            lbl_err.Text = Resources.LibResources.Label848 'sabrina-french dec14"Last name is required "           'sabrina Dec03,2015
            Text_lname.Focus()
            Exit Sub
        End If

        If Text_plan_id.Text & "" = "" Then
            lbl_err.Text = Resources.LibResources.Label855 'sabrina-french dec14 "Plan id is required"
            Text_plan_id.Focus()
            Exit Sub
        Else
            'R1999 sabrina
            If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
                str_y_n = check_plan(Text_plan_id.Text)
                If str_y_n <> "Y" Then
                    lbl_err.Text = Resources.LibResources.Label854 'sabrina-french dec14 "Plan id is invalid"

                    Text_plan_id.Focus()
                    Exit Sub
                End If

            Else  'sabrina Dec18,2015
                If isvalidBRKplan() <> "Y" Then
                    lbl_err.Text = Resources.LibResources.Label854
                    Text_plan_id.Focus()
                    Exit Sub
                End If
            End If


        End If




        Dim str_space As String = " "
        Session("vd_inspmt_plan_id") = UCase(Text_plan_id.Text)
        Session("vd_inspmt_plan_name") = Text_fname.Text & str_space & Text_lname.Text
        Session("vd_inspmt_plan_name") = Left(Session("vd_inspmt_plan_name"), 40)
        Session("vd_inspmt_acct_num") = Text_acct_num.Text
        Session("vd_inspmt") = "Y"
        Dim str_amt As String = 0
        Dim str_ivc_cd As String = Session("ivc_cd")
        Dim URL_String As String
        Dim str_csh_pss As String = Session("csh_pss")
        Dim str_csh_drw As String = Session("csh_drw")
        Dim str_store_cd As String = Session("store_cd")

        URL_String = "?ip_ord_total=" & str_amt & "&refresh=true" & "&ip_csh_pass=" & str_csh_pss & "&ip_store_cd=" & str_store_cd & "&ip_cust_cd=" & Text_cust_cd.Text & "&ip_ivc_cd=" & str_ivc_cd & "&ip_csh_drw=" & str_csh_drw
        Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)


    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Session("vd_inspmt") = "N"
        Session("vd_inspmt_plan_name") = ""
        Session("vd_inspmt_plan_name") = ""
        Session("vd_inspmt_acct_num") = ""
        lbl_err.Text = ""
        Dim str_amt As String = 0
        Dim str_ivc_cd As String = Session("ivc_cd")
        Dim URL_String As String
        Dim str_csh_pss As String = Session("csh_pss")
        Dim str_csh_drw As String = Session("csh_drw")
        Dim str_store_cd As String = Session("store_cd")

        URL_String = "?ip_ord_total=" & str_amt & "&refresh=true" & "&ip_csh_pass=" & str_csh_pss & "&ip_store_cd=" & str_store_cd & "&ip_cust_cd=" & Text_cust_cd.Text & "&ip_ivc_cd=" & str_ivc_cd & "&ip_csh_drw=" & str_csh_drw
        Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)
    End Sub
    Public Function isvalidBRKplan() As String



        If Len(Text_plan_id.Text) <> 3 Then
            Return "N"
        ElseIf UCase(Text_plan_id.Text) = "REG" Then
            Text_plan_id.Text = UCase(Text_plan_id.Text)
            Return "Y"
        ElseIf Text_plan_id.Text = "000" Then
            Return "N"
        Else
            If Regex.IsMatch(Text_plan_id.Text, "^\d{3}") Then 'sabrina - Match exactly three numeric characters
                Return "Y"
            Else
                Return "N"
            End If
        End If


    End Function
    Protected Sub Text_plan_id_TextChanged(sender As Object, e As EventArgs) Handles Text_plan_id.TextChanged
        Dim str_y_n As String
        lbl_err.Text = "" 'sabrina Dec18,2015

        If Text_plan_id.Text & "" = "" Then
            lbl_err.Text = Resources.LibResources.Label853 'sabrina-french dec14 "Plan Id is a required field"
            Text_plan_id.Focus()
            Exit Sub
        ElseIf Len(Text_plan_id.Text) < 2 Then
            lbl_err.Text = Resources.LibResources.Label854 'Plan Id is invalid"
            Text_plan_id.Focus()
            Exit Sub
        Else
            'R1999 SABRINA
            If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
                str_y_n = check_plan(Text_plan_id.Text)
                If str_y_n <> "Y" Then
                    lbl_err.Text = Resources.LibResources.Label854 'sabrina-french dec14"Plan Id is invalid"
                    Exit Sub
                    Text_plan_id.Focus()
                End If
            Else  'sabrina Dec18.2015
                If isvalidBRKplan() <> "Y" Then
                    lbl_err.Text = Resources.LibResources.Label854
                    Exit Sub
                    Text_plan_id.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub CreateCustomer()

        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click


        'sabrina R1999 - add swipe button (new sub)
        lbl_err.Text = ""

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_rt1 As String
        Dim l_rt2 As String
        Dim l_tr1 As String
        Dim l_tr2 As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_cst As String
        Dim l_dsp As String
        Dim v_pos1 As OracleNumber
        Dim v_pos2 As OracleNumber

        Dim v_term_id As String = ""
        'Dim v_term_id As String = LeonsBiz.get_term_id(Session("clientip"))

        ' sabrina feb26
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Daniela Oct 21
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            'sabrina-french dec14 lbl_err.Text = "Invalid Terminal ID " & v_term_id
            lbl_err.Text = Resources.LibResources.Label847 & " " & v_term_id
            Exit Sub
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Button3.Enabled = False
        Button1.Enabled = False

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp7_swipe"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_trantp", OracleType.VarChar)).Value = "SW"
            myCMD.Parameters.Add(New OracleParameter("p_subtp", OracleType.VarChar)).Value = "7"
            myCMD.Parameters.Add(New OracleParameter("p_displn1", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_displn2", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_rsvd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_term_id", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cst", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 500).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            Button3.Enabled = True
            Button1.Enabled = True

            If InStr(UCase(myCMD.Parameters("p_dsp").Value), "SUCCESSFUL") > 0 Then

                l_crn = myCMD.Parameters("p_crn").Value
                Text_acct_num.Text = l_crn
                Session("vd_inspmt_acct_num") = l_crn
                If myCMD.Parameters("p_res").Value & "" <> "" Then
                    l_res = myCMD.Parameters("p_res").Value
                End If
                If myCMD.Parameters("p_rt1").Value & "" <> "" Then
                    l_rt1 = myCMD.Parameters("p_rt1").Value
                End If
                If myCMD.Parameters("p_rt2").Value & "" <> "" Then
                    l_rt2 = myCMD.Parameters("p_rt2").Value
                End If
                If myCMD.Parameters("p_tr1").Value & "" <> "" Then
                    l_tr1 = myCMD.Parameters("p_tr1").Value
                End If
                If myCMD.Parameters("p_tr2").Value & "" <> "" Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                End If
                If myCMD.Parameters("p_cst").Value & "" <> "" Then
                    l_cst = myCMD.Parameters("p_cst").Value
                    v_pos1 = InStr(l_cst, "/")
                    If v_pos1 = 0 Then
                        v_pos1 = InStr(l_cst, " ")
                    End If
                    If v_pos1 > 0 Then
                        Text_lname.Text = Left(l_cst, v_pos1 - 1)
                        v_pos2 = Len(l_cst) - v_pos1
                        Text_fname.Text = Right(l_cst, v_pos2)
                    End If
                End If


            Else   'transation not done 
                l_crn = ""
                Session("vd_inspmt_acct_num") = l_crn
                lbl_err.Text = Resources.LibResources.Label840  'sabrina-french dec14 "Click to Swipe Again"
            End If

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw
        End Try



    End Sub

End Class
