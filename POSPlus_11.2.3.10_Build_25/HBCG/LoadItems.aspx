<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LoadItems.aspx.vb" Inherits="LoadItems" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label ID="txt_title" runat="server" Style="position: relative" CssClass="style5">Load Item File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload1" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Upload Item File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="itm_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="itm_cd" headertext="Item Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
              <asp:BoundColumn DataField="comm_cd" headertext="Comm Cd" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ve_cd" headertext="Vendor Cd" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="mnr_cd" headertext="Minor Cd" ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="cat_cd" headertext="Cat Cd" ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="itm_tp_cd" headertext="Item tp cd" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="Inventory" headertext="INV" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
            </asp:BoundColumn>
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>
    <br />
     <asp:Button ID="btn_process" runat="server" Text="Process Items" Width="200px" Visible="false">
     </asp:Button>
    <br />
     <asp:Button ID="btn_clear" runat="server" Text="Clear" Width="100px" Visible="true">
     </asp:Button>
</asp:Content>
