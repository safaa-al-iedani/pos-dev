<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="NewsMaintenance.aspx.vb" Inherits="NewsMaintenance" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="admin_form" runat="server">
        <dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server" Width="579px">
        </dx:ASPxHtmlEditor>
        <br />
        <asp:Button ID="btn_save" runat="server" CssClass="style5" Text="Save Changes" />
        <asp:Button ID="btn_cancel" runat="server" CssClass="style5" Text="Cancel Changes" />
        <asp:Label ID="lbl_help_id" runat="server" Visible="True"></asp:Label>
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
