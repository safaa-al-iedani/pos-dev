<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Customer.aspx.vb" Inherits="customer"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table cellspacing="0" cellpadding="0" id="CustTable" runat="server">
            <tr>
                <td colspan="2">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxTextBox ID="txtCustCode" runat="server" Width="100px">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label140 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label111 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxComboBox ID="cbo_cust_tp" runat="server" ValueType="System.String" Width="106px"
                        IncrementalFilteringMode ="StartsWith">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="" Value="" />
                            <dx:ListEditItem Text="<%$ Resources:LibResources, Label236 %>" Value="I" />
                            <dx:ListEditItem Text="<%$ Resources:LibResources, Label112 %>" Value="C" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txt_Corp" runat="server" Width="136px">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label211 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label263 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxTextBox ID="txtFName" runat="server" Width="100px" MaxLength="15">
                    </dx:ASPxTextBox>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtLName" runat="server" Width="100px" MaxLength="20">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label229 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label27 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxTextBox ID="txtHomePhone" runat="server" Width="100px" MaxLength="12">
                    </dx:ASPxTextBox>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtBusPhone" runat="server" Width="100px" MaxLength="12">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label22 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxTextBox ID="txtAddr" runat="server" Width="250px" MaxLength="30">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label80 %>">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="txt_city" runat="server" Width="169px" MaxLength="20">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_state" runat="server" Width="25px" MaxLength="2">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_zip" runat="server" Width="49px" MaxLength="10">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label275 %>" Width="139px">
                    </dx:ASPxButton>
                    <br />
                    <dx:ASPxLabel ID="lbl_errors" runat="server" Text="" Width="100%">
                    </dx:ASPxLabel>
                    <br />
                </td>
            </tr>
        </table>
    &nbsp;&nbsp;&nbsp;
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
