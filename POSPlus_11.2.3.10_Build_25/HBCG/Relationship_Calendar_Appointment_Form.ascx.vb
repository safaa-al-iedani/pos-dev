Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.XtraScheduler
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxScheduler
Imports DevExpress.Web.ASPxScheduler.Internal

Partial Public Class AppointmentForm
    Inherits SchedulerFormControl
    Public ReadOnly Property CanShowReminders() As Boolean
        Get
            Return (CType(Parent, AppointmentFormTemplateContainer)).Control.Storage.EnableReminders
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

        'PrepareChildControls();
        tbSubject.Focus()

    End Sub

    Public Overrides Sub DataBind()
        MyBase.DataBind()

        Dim container As AppointmentFormTemplateContainer = CType(Parent, AppointmentFormTemplateContainer)
        Dim apt As Appointment = container.Appointment

        btnOk.ClientSideEvents.Click = container.CancelHandler

    End Sub

    Protected Overrides Sub PrepareChildControls()

        Dim container As AppointmentFormTemplateContainer = CType(Parent, AppointmentFormTemplateContainer)
        Dim control As ASPxScheduler = container.Control

        MyBase.PrepareChildControls()

    End Sub
    Protected Overrides Function GetChildEditors() As ASPxEditBase()

        Dim edits() As ASPxEditBase = {lblSubject, tbSubject, tbLocation, lblStartDate, edtStartDate, lblEndDate, edtEndDate, tbDescription}
        Return edits

    End Function
    Protected Overrides Function GetChildButtons() As ASPxButton()

        Dim buttons() As ASPxButton = {btnOk}
        Return buttons

    End Function

    Protected Sub btn_relationship_open_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_relationship_open.Click

        Dim REL_NO As String = ""
        If tbSubject.Text & "" <> "" Then
            REL_NO = Left(tbSubject.Text, 11).Trim
            Response.Redirect("Relationship_Maintenance.aspx?del_doc_num=" & REL_NO & "&query_returned=Y")
        End If

    End Sub
End Class