Imports System.Data.OracleClient
Imports System.Net.NetworkInformation
Imports HBCG_Utils

Partial Class Terminal_Status
    Inherits POSBasePage
    Dim ds As New DataSet

    Public Sub bindgrid(Optional ByVal store_cd As String = "ALL")
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds2 = New DataSet

        Gridview1.DataSource = ""

        Gridview1.Visible = True
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT a.TERMINAL_IP, a.STATUS, a.STATUS_DT, a.LAST_UPDATED, a.UPDATED_EMP, b.TERMINAL_NAME, b.STORE_CD, b.CASH_DWR_CD FROM TERMINAL_STATUS a, "
        sql = sql & "TERMINAL_SETUP b WHERE a.TERMINAL_IP=b.TERMINAL_IP "
        If store_cd <> "ALL" Then
            sql = sql & "AND b.store_cd='" & store_cd & "' "
        End If
        sql = sql & "ORDER BY STORE_CD, CASH_DWR_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds2
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
                Gridview1.DataSource = ""
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_filter_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            cbo_filter_store.Items.Insert(0, "ALL STORES")
            cbo_filter_store.Items.FindByText("ALL STORES").Value = "ALL"
            cbo_filter_store.SelectedIndex = 0

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            cbo_filter_store.Enabled = False
            btn_open_all.Enabled = False
            Exit Sub
        End If

        If Not IsPostBack Then
            bindgrid()
            store_cd_populate()
        End If

    End Sub

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim TempList As DropDownList
        
        TempList = e.Item.FindControl("cbo_status")
        If Not TempList Is Nothing Then
            If TempList.SelectedValue = "O" Then
                TempList.BackColor = Color.LightGreen
            Else
                TempList.BackColor = Color.LightSalmon
            End If
        End If

    End Sub

    Protected Sub Store_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim textdata As DropDownList = CType(sender, DropDownList)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        sql = "UPDATE TERMINAL_STATUS SET STATUS=:STATUS, STATUS_DT=SYSDATE, LAST_UPDATED=SYSDATE, UPDATED_EMP=:EMP_CD WHERE TERMINAL_IP='" & dgItem.Cells(0).Text & "'"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":STATUS", OracleType.VarChar)
        objSql.Parameters(":STATUS").Value = textdata.SelectedValue.ToString
        objSql.Parameters.Add(":EMP_CD", OracleType.VarChar)
        objSql.Parameters(":EMP_CD").Value = Session("EMP_CD").ToString

        objSql.ExecuteNonQuery()

        objSql.Dispose()
        conn.Close()

        bindgrid()

    End Sub

    Protected Sub cbo_filter_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_filter_store.SelectedIndexChanged

        bindgrid(cbo_filter_store.SelectedValue.ToString)

    End Sub

    Protected Sub btn_open_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_open_all.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        sql = "UPDATE TERMINAL_STATUS SET STATUS='O', STATUS_DT=SYSDATE, LAST_UPDATED=SYSDATE, UPDATED_EMP=:EMP_CD"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":EMP_CD", OracleType.VarChar)
        objSql.Parameters(":EMP_CD").Value = Session("EMP_CD").ToString

        objSql.ExecuteNonQuery()

        objSql.Dispose()
        conn.Close()

        bindgrid()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
