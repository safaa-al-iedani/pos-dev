Imports System.Xml

Partial Class ADS_Test
    Inherits POSBasePage
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()
    Private Const cResponseCriteria As String = "/HandleCode/ReasonCode/CodeDesc"
    Private Const cReturnCriteria As String = "/HandleCode/ReturnCode/CodeDesc"
    Private Const cAVSCriteria As String = "/HandleCode/AVSResponseCode/CodeDesc"

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If cbo_sale_or_ret.SelectedValue = "SAL" Then
            Dim requestDS As New RequestAllianceDataSet()
            Dim responseDS As ResponseAllianceDataSet
            requestDS.Request.Rows.Add(New Object() {"LEVIN", DateTime.Now, 19, TextBox1.Text, txt_amt.Text, txt_swiped.Text})
            requestDS.AcceptChanges()
            Dim fiService As New FITransactionService()
            responseDS = fiService.InvokeFITransaction(requestDS)
            Dim returnDesc As String
            If (Not responseDS Is Nothing) And (responseDS.Response.Rows.Count) > 0 Then
                'reasonDesc = GetDesc(responseDS.Response(0).reasonCode.ToString, cResponseCriteria)
                returnDesc = GetDesc(responseDS.Response(0).returnCode.ToString, cReturnCriteria)
                'avsDesc = GetDesc(responseDS.Response(0).avsResponse.ToString, cAVSCriteria)
                'lbl_reasoncode.Text = reasonDesc
                lbl_returncode.Text = returnDesc
                'lbl_avsresponse.Text = avsDesc
                lbl_authcode.Text = responseDS.Response(0).authCode.ToString
                'lbl_storenumber.Text = responseDS.Response(0).storeNumber.ToString
            End If
        Else
            Dim requestDS As New RequestAllianceDataSet()
            Dim responseDS As ResponseAllianceDataSet
            requestDS.Request.Rows.Add(New Object() {"LEVIN", DateTime.Now, 19, TextBox1.Text, txt_amt.Text, txt_swiped.Text})
            requestDS.AcceptChanges()
            responseDS = ParseResponseAndFillDS(SendRequestAndGetResponse(requestDS))
            Dim reasonDesc, returnDesc As String
            If (Not responseDS Is Nothing) And (responseDS.Response.Rows.Count) > 0 Then
                reasonDesc = GetDesc(responseDS.Response(0).reasonCode.ToString, cResponseCriteria)
                returnDesc = GetDesc(responseDS.Response(0).returnCode.ToString, cReturnCriteria)
                'avsDesc = GetDesc(responseDS.Response(0).avsResponse.ToString, cAVSCriteria)
                lbl_reasoncode.Text = reasonDesc
                lbl_returncode.Text = returnDesc
                'lbl_avsresponse.Text = avsDesc
                lbl_authcode.Text = responseDS.Response(0).authCode.ToString
                'lbl_storenumber.Text = responseDS.Response(0).storeNumber.ToString
            End If
        End If


        'Response.Write("Reason Description:- " & reasonDesc & "     Return Description:- " & returnDesc & "     AVS Description:- " & avsDesc)
    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            ElseIf IsNumeric(m_node.ChildNodes.Item(0).InnerText) And IsNumeric(reasonCd) Then
                If CDbl(m_node.ChildNodes.Item(0).InnerText) = CDbl(reasonCd) Then
                    desc = m_node.ChildNodes.Item(1).InnerText
                    Exit For
                End If
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc
    End Function

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        If InStr(TextBox1.Text, "^") > 0 Then
            Dim sCC As Array
            sCC = Split(TextBox1.Text.ToString, "^")
            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
            TextBox3.Text = Left(sCC(2).Trim, 2)
            TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
            lbl_Full_Name.Text = sCC(1).Trim
            TextBox1.Enabled = False
            TextBox2.Enabled = False
            TextBox3.Enabled = False
            txt_swiped.Text = "Y"
        Else
            TextBox2.Focus()
            txt_swiped.Text = "N"
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        txt_swiped.Text = "N"
        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox1.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "ADS 3rd Party Finance Test"
        TextBox1.Focus()

    End Sub

    Public Function SendRequestAndGetResponse(ByRef requestDS As RequestAllianceDataSet) 'As XmlDocument

        Dim oWebReq As System.Net.HttpWebRequest
        oWebReq = CType(System.Net.HttpWebRequest.Create("https://retailuat.alldata.net/axes103/web-services/returns"), System.Net.HttpWebRequest)

        Dim xml_string As String = ""

        xml_string = "<?xml version=""1.0"" encoding=""utf-8""?>"
        xml_string = xml_string & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
        xml_string = xml_string & "<soap:Body>"
        xml_string = xml_string & "<ns1:returns20 xmlns:ns1=""http://axes.alldata.net/web-services"" version=""2.0"">"
        xml_string = xml_string & "<request>"
        xml_string = xml_string & "<clientHeader>"
        xml_string = xml_string & "<clientName>LEVIN</clientName>"
        xml_string = xml_string & "<timestamp>" & FormatDateTime(Now.Date.ToString, DateFormat.ShortDate) & " " & FormatDateTime(Now.TimeOfDay.ToString, DateFormat.LongTime) & "</timestamp>"
        xml_string = xml_string & "</clientHeader>"
        xml_string = xml_string & "<storeNumber>" & requestDS.Request(0).storeNumber & "</storeNumber>"
        If requestDS.Request(0).accountNumber & "" <> "" Then
            xml_string = xml_string & "<accountNumber>" & requestDS.Request(0).accountNumber & "</accountNumber>"
        End If
        xml_string = xml_string & "<swipeInd>" & requestDS.Request(0).swipeInd & "</swipeInd>"
        xml_string = xml_string & "<transactionAmount>" & requestDS.Request(0).transactionAmount & "</transactionAmount>"
        xml_string = xml_string & "</request>"
        xml_string = xml_string & "</ns1:returns20>"
        xml_string = xml_string & "</soap:Body>"
        xml_string = xml_string & "</soap:Envelope>"

        _requestXML = New XmlDataDocument
        _requestXML.RemoveAll()
        _requestXML.LoadXml(xml_string)

        Dim reqBytes As Byte()
        reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
        oWebReq.ContentLength = reqBytes.Length
        oWebReq.Method = "POST"
        oWebReq.ContentType = "text/xml"
        oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
        Try
            Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
            oReqStream.Write(reqBytes, 0, reqBytes.Length)
            Dim responseBytes As Byte()
            Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
            If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
                responseBytes = processResponse(oWebResp)
                BuildResponseDocument(responseBytes)
            End If
            oWebResp.Close()
        Catch ex As Exception
            If Err.Number = 5 Then
                Return _responseXML
                Exit Function
            End If
            Throw
        End Try
        Return _responseXML

    End Function

    Public Sub BuildResponseDocument(ByRef responseBytes As Byte())

        Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
        _responseXML.RemoveAll()
        _responseXML.LoadXml(strResp)

    End Sub

    Private Function processResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()

        Dim memStream As New System.IO.MemoryStream()
        Const BUFFER_SIZE As Integer = 4096
        Dim iRead As Integer = 0
        Dim idx As Integer = 0
        Dim iSize As Int64 = 0
        memStream.SetLength(BUFFER_SIZE)
        While (True)
            Dim respBuffer(BUFFER_SIZE) As Byte
            Try
                iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
            Catch e As System.Exception
                Throw e
            End Try
            If (iRead = 0) Then
                Exit While
            End If
            iSize += iRead
            memStream.SetLength(iSize)
            memStream.Write(respBuffer, 0, iRead)
            idx += iRead
        End While

        Dim content As Byte() = memStream.ToArray()
        memStream.Close()
        Return content

    End Function

    Public Function ParseResponseAndFillDS(ByRef responseXML As XmlDocument)

        Dim rsaDS As New DataSet()
        Dim ResponseDS As New ResponseAllianceDataSet
        rsaDS.ReadXml(New XmlNodeReader(responseXML))
        Dim responseAllianceDSDataRow As ResponseAllianceDataSet.ResponseRow
        If (Not rsaDS Is Nothing) And (rsaDS.Tables.Count > 0) Then
            If rsaDS.Tables(0).Rows.Count > 0 Then
                responseAllianceDSDataRow = ResponseDS.Response.NewResponseRow
                Dim dr As DataRow = rsaDS.Tables(2).Rows(0)
                If dr.Table.Columns.Contains("returnCode") Then
                    responseAllianceDSDataRow.returnCode = dr("returnCode")
                End If
                If dr.Table.Columns.Contains("authCode") Then
                    responseAllianceDSDataRow.authCode = dr("authCode")
                End If
                If dr.Table.Columns.Contains("reasonCode") Then
                    responseAllianceDSDataRow.reasonCode = dr("reasonCode")
                End If
                ResponseDS.Response.Rows.Add(responseAllianceDSDataRow)
            End If
        End If

        Return ResponseDS

    End Function

End Class
