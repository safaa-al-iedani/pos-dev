﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Partial Class CredMain
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_stat As String = ""

        lbl_msg.Text = ""

        If IsPostBack Then
            If new_event_ind.Checked = True Then
                If new_event_cd.Text & "" = "" Then
                    save_btn.Visible = True
                Else
                    If EventExists(new_event_cd.Text) <> "Y" Then
                        save_btn.Visible = True
                    Else
                        save_btn.Visible = False
                        populate_items()
                    End If
                End If
                If store_grp_list.SelectedIndex > 1 Then
                    str_drp_list.SelectedIndex = 0
                    str_drp_list.Enabled = False
                Else
                    str_drp_list.Enabled = True
                End If
                search_btn.Visible = False
                stat_drp_list.Enabled = False
                event_drp_list.Visible = False
                new_event_cd.Visible = True
                If new_event_cd.Text & "" = "" Then
                    set_next_event_code()
                End If
                vcm_btn.Visible = False

 		'sabrina R3153
                reset_exclu_fran()
            Else
                If event_drp_list.SelectedIndex > 0 Then
                    vcm_btn.Visible = True
                    search_btn.Visible = False
                    v_stat = getCurrValue("stat_cd")
                    If v_stat & "" = "" Then
                        stat_drp_list.Enabled = True
                        save_btn.Visible = True
                    Else
                        If v_stat = "O" Then
                            If credit_tp_drp_list.SelectedIndex > 0 Then
                                If credit_tp_drp_list.SelectedItem.Value.ToString <> "IC" And
                                    credit_tp_drp_list.SelectedItem.Value.ToString <> "FC" And
                                    credit_tp_drp_list.SelectedItem.Value.ToString <> "TC" Then
                                    save_btn.Visible = True
                                Else
                                    save_btn.Visible = False
                                End If
                            Else
                                stat_drp_list.Enabled = False
                                save_btn.Visible = False
                            End If
                        Else
                            stat_drp_list.Enabled = False
                            save_btn.Visible = False
                        End If
                    End If

                    If credit_tp_drp_list.SelectedIndex > 0 Then
                        
                        If credit_tp_drp_list.SelectedItem.Value.ToString = "DC" Then
                            credsku_btn.Visible = False
                        Else
                            credsku_btn.Visible = True
                            If event_drp_list.SelectedIndex > 0 Then
                                populate_items()
                            End If
                        End If

                    End If
                Else
                    vcm_btn.Visible = False
                    credsku_btn.Visible = False
                    'search_btn.Visible = True
                    stat_drp_list.Enabled = True
                End If

                event_drp_list.Visible = True
                new_event_cd.Visible = False


            End If
        Else
            If Session("EMP_CD") & "" = "" Then
                Response.Redirect("login.aspx")
            End If
            store_grp_list.Items.Clear()
            credsku_btn.Visible = False
            search_btn.Visible = True
            stat_drp_list.Enabled = True
            save_btn.Visible = False
            event_drp_list.Items.Clear()
            credit_tp_drp_list.Items.Clear()

            vc_tp_drp_list.Items.Clear()

            str_drp_list.Items.Clear()

            stat_drp_list.Items.Clear()


            Populate_buyer()
            Populate_ve()
            Populate_credit_tp()
            Populate_vc_tp()
            Populate_stat_cd()
            Populate_store_grp()

            'sabrina - R3153
            exclu_fran_list.Items.Clear()
            If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
                exclu_fran_list.Items.Add("N")
            Else
                exclu_fran_list.Items.Add("N")
                exclu_fran_list.Items.Add("Y")
            End If
            'sabrina - R3153-end

            Populate_str()
            event_drp_list.Visible = True
            vcm_btn.Visible = False
            credsku_btn.Visible = False

            start_dt.SelectedDate = DateTime.MinValue
            start_dt.VisibleDate = DateTime.MinValue

            end_dt.SelectedDate = DateTime.MinValue
            end_dt.VisibleDate = DateTime.MinValue


        End If

    End Sub
    Public Function getsystemdate() As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT to_char(sysdate,'MMDDY') dt from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value
    End Function
    Public Function getNextSeq(ByVal p_event_cd As String) As Integer

        Dim v_value As String = ""
        Dim v_seq As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT max(event_cd) evt from credevt "
        sql = sql & " where event_cd like '" & p_event_cd & "%' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("evt")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return 1
        Else
            If IsNumeric(Right(v_value, 2)) Then
                v_seq = Right(v_value, 2) + 1
                If v_seq > 99 Then
                    Return 1
                Else
                    Return v_seq
                End If
            Else
                Return 1
            End If
        End If

    End Function

    Public Sub set_next_event_code()

        If buyer_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Buyer to proceed"
            buyer_drp_list.Focus()
            new_event_ind.Checked = False
            Exit Sub
        End If

        If ve_cd_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Vendor to proceed"
            new_event_ind.Checked = False
            ve_cd_drp_list.Focus()
            Exit Sub
        End If

        Dim v_co_cd As String = ""
        Dim v_seq As Integer = 0
        Dim v_dt As String = getsystemdate()

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_co_cd = "L"
        Else
            v_co_cd = "B"
        End If

        Dim v_event As String = v_co_cd & ve_cd_drp_list.SelectedItem.Value.ToString() & v_dt
        v_seq = getNextSeq(v_event)
        If v_seq < 10 Then
            v_event = v_event & "0" & v_seq
        Else
            v_event = v_event & v_seq
        End If

        new_event_ind.Enabled = False
        buyer_drp_list.Enabled = False
        ve_cd_drp_list.Enabled = False

        new_event_cd.Text = v_event

    End Sub

    Public Sub Populate_buyer()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String
        Dim v_tab As String = ""

        buyer_drp_list.Items.Clear()

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_tab = " buyer$name "
        Else
            v_tab = " brick_buyer$name "
        End If

        sql = "select distinct buyer buyer, buyer||' - '||name name from " & v_tab & " where buyer is not null "
        sql = sql & " order by 1 "

        buyer_drp_list.Items.Insert(0, "Buyer")
        buyer_drp_list.Items.FindByText("Buyer").Value = ""
        buyer_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With buyer_drp_list
                .DataSource = ds
                .DataValueField = "buyer"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub
    Public Sub Populate_ve()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        If Session("emp_init") & "" = "" Then
            Session("emp_init") = LeonsBiz.GetEmpInit(Session("emp_cd"))
        End If

        ve_cd_drp_list.Items.Clear()

        sql = "select v.ve_cd ve_cd "
        sql = sql & " from ve v "
        sql = sql & " where std_multi_co2.isvalidvdr('" & Session("emp_init") & "', v.ve_cd) = 'Y' "
        sql = sql & "   and exists (select 'x' from ve$ve_tp t "
        sql = sql & "                where t.ve_cd = v.ve_cd   "
        sql = sql & "                  and t.ve_tp_cd = 'MER') "
        sql = sql & " order by 1 "

        ve_cd_drp_list.Items.Insert(0, "Vendor")
        ve_cd_drp_list.Items.FindByText("Vendor").Value = ""
        ve_cd_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With ve_cd_drp_list
                .DataSource = ds
                .DataValueField = "ve_cd"
                .DataTextField = "ve_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub
    Public Sub Populate_credit_tp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        credit_tp_drp_list.Items.Clear()


        sql = " select credit_tp, credit_tp||' - '||credit_tp_des des from credevt_credit_tp "
        sql = sql & " where credit_tp_use = 'O' "
        sql = sql & "  order by 1 "

        credit_tp_drp_list.Items.Insert(0, "Cred Type")
        credit_tp_drp_list.Items.FindByText("Cred Type").Value = ""
        credit_tp_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With credit_tp_drp_list
                .DataSource = ds
                .DataValueField = "credit_tp"
                .DataTextField = "des"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub
    Public Sub Populate_vc_tp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        vc_tp_drp_list.Items.Clear()

        sql = " Select vc_srt_cd, vc_srt_cd||' - '||des des1 from vc_srt "
        sql = sql & " order by "
        sql = sql & " case "
        sql = sql & " when upper(des) like 'MARG%' then 1 "
        sql = sql & " when upper(des) like 'SUPP%' then 2 "
        sql = sql & " else 3 "
        sql = sql & " end "

        vc_tp_drp_list.Items.Insert(0, "VCM Sort")
        vc_tp_drp_list.Items.FindByText("VCM Sort").Value = ""
        vc_tp_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With vc_tp_drp_list
                .DataSource = ds
                .DataValueField = "vc_srt_cd"
                .DataTextField = "des1"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_str()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String = ""
        Dim sql2 As String = ""

        'R2913 - sabrina 
        Dim co_grp_cd As String = ""
        If Session("CO_CD") & "" <> "" Then
            co_grp_cd = LeonsBiz.GetGroupCode(Session("CO_CD"))
        End If

        str_drp_list.Items.Clear()
        'R2913 - sabrina 
        If co_grp_cd <> "BRK" And exclu_fran_list.SelectedItem.Value.ToString() & "" = "N" Then
            sql = " select store_cd str_cd from store "
            sql = sql & " where std_multi_co2.isvalidstr3('" & Session("emp_init") & "',store_cd) = 'Y' "
            sql = sql & " union select store_cd str_cd from store s "
            sql = sql & " where s.store_cd in (select sg.store_cd from store_Grp$store sg, store_grp g "
            sql = sql & "                       where sg.store_grp_cd = g.store_grp_cd "
            sql = sql & "                         and g.store_grp_tp = 'CRD')"
            sql = sql & " order by 1 "
        Else
            sql = " select store_cd str_cd from store "
            sql = sql & " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd) = 'Y' "
            sql = sql & " order by 1 "
        End If

        str_drp_list.Items.Insert(0, "ALL")
        str_drp_list.Items.FindByText("ALL").Value = ""
        str_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With str_drp_list
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "str_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_store_grp()

        'R3053 sabrina
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String = ""

        store_grp_list.Items.Clear()


        sql = " select store_grp_cd from store_grp "
        sql = sql & " where std_multi_co2.isvalidStrGrp3('" & Session("emp_init") & "',store_grp_cd) = 'Y' "
        sql = sql & " union select '-' store_grp_cd from dual"
        sql = sql & " order by 1 "

        store_grp_list.Items.Insert(0, "-")
        store_grp_list.Items.FindByText("-").Value = ""
        store_grp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With store_grp_list
                .DataSource = ds
                .DataValueField = "store_grp_cd"
                .DataTextField = "store_grp_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_stat_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        stat_drp_list.Items.Clear()

        sql = " select ' ' stat_cd from dual union"
        sql = sql & " select 'C' stat_cd from dual union "
        sql = sql & " select 'E' stat_cd from dual union"
        sql = sql & " select 'O' stat_cd from dual union"
        sql = sql & " select 'V' stat_cd from dual "
        sql = sql & " order by 1 "

        stat_drp_list.Items.Insert(0, "Status")
        stat_drp_list.Items.FindByText("Status").Value = ""
        stat_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With stat_drp_list
                .DataSource = ds
                .DataValueField = "stat_cd"
                .DataTextField = "stat_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub show_disc_fields()

        lbl_disc_cd.Visible = True
        lbl_disc_pct.Visible = True
        lbl_disc_amt.Visible = True


        disc_cd.Visible = True
        disc_pct.Visible = True
        disc_amt.Visible = True



    End Sub
    Public Sub dont_show_disc_fields()

        lbl_disc_cd.Visible = False
        lbl_disc_pct.Visible = False
        lbl_disc_amt.Visible = False


        disc_cd.Visible = False
        disc_pct.Visible = False
        disc_amt.Visible = False

    End Sub
    Public Sub Populate_events()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String
        Dim v_start_dt_sql As String = ""
        Dim v_end_dt_sql As String = ""
        Dim v_credit_tp_sql As String = ""
        Dim v_stat_sql As String = ""
        Dim v_vc_tp_sql As String = ""
        Dim v_str_sql As String = ""
        Dim v_credit_tp As String = ""
        Dim v_vc_tp As String = ""
        Dim v_str As String = ""
        Dim v_stat_cd As String = ""
        Dim v_buyer As String = ""
        Dim v_event_cd_sql As String = ""
        Dim v_ve_cd As String = ""
        Dim v_ve_cd_sql As String = ""
        Dim v_store_grp_sql As String = ""

        event_drp_list.Items.Clear()

        If buyer_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Buyer to continue"
            Exit Sub
        Else
            v_buyer = buyer_drp_list.SelectedItem.Value.ToString()
        End If

        'sabrina R3053
        If store_grp_list.SelectedIndex < 1 Then
            v_store_grp_sql = ""
        Else
            v_store_grp_sql = " and nvl(store_grp_cd,'x') = '" & store_grp_list.SelectedItem.Value.ToString() & "' "
        End If

        If ve_cd_drp_list.SelectedIndex < 1 Then
            v_ve_cd_sql = ""
        Else
            v_ve_cd_sql = " and (nvl(ve_cd,'x') = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "' " &
                          "  or exists (select 'x' " &
                          "               from credevt_sku_dtl s, " &
                          "                    itm i " &
                          "             where s.event_cd = a.event_cd " &
                          "               and i.itm_cd = s.itm_cd " &
                          "               and i.ve_cd = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "')) "
        End If

        If credit_tp_drp_list.SelectedIndex < 1 Then
            v_credit_tp_sql = ""
        Else
            v_credit_tp_sql = "   and credit_tp = decode('" & credit_tp_drp_list.SelectedItem.Value.ToString() & "','ALL',credit_tp, '" & credit_tp_drp_list.SelectedItem.Value.ToString() & "')"
        End If

        If vc_tp_drp_list.SelectedIndex < 1 Then
            v_vc_tp_sql = ""
        Else
            v_vc_tp_sql = "  and vc_tp_cd = decode('" & vc_tp_drp_list.SelectedItem.Value.ToString() & "','ALL', vc_tp_cd, '" & vc_tp_drp_list.SelectedItem.Value.ToString() & "')"
        End If

        If stat_drp_list.SelectedIndex < 1 Then
            v_stat_sql = ""
        ElseIf stat_drp_list.SelectedItem.Value.ToString() = " " Then
            v_stat_sql = "   and stat_cd is null "
        Else
            v_stat_sql = "   and stat_cd = decode('" & stat_drp_list.SelectedItem.Value.ToString() & "','Status',stat_cd, '" & stat_drp_list.SelectedItem.Value.ToString() & "')"
        End If

        If str_drp_list.SelectedIndex < 1 Then
            v_str_sql = "   and (pri_str is null or pri_str = 'ALL' or std_multi_co2.isvalidstr2 ('" & Session("emp_init") & "',pri_str) = 'Y') "
        Else
            v_str = str_drp_list.SelectedItem.Value.ToString()
            v_str_sql = "   and pri_str = '" & v_str & "' "
        End If
        If start_dt.SelectedDate.Date <> DateTime.MinValue Then
            v_start_dt_sql = "   and trunc(strt_dt) = to_date('" & FormatDateTime(start_dt.SelectedDate.ToString(), DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        Else
            v_start_dt_sql = ""
        End If

        If end_dt.SelectedDate.Date <> DateTime.MinValue Then
            v_end_dt_sql = "   and trunc(end_dt) = to_date('" & FormatDateTime(end_dt.SelectedDate.ToString(), DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        Else
            v_end_dt_sql = ""
        End If

        If event_drp_list.SelectedValue & "" <> "" Then
            v_event_cd_sql = " and event_cd = '" & event_drp_list.SelectedValue.ToString() & "'"
        End If

        sql = "select event_cd from credevt a "
        sql = sql & " where buyer = decode('" & v_buyer & "','ALL',buyer, '" & v_buyer & "')"
        sql = sql & v_str_sql
        sql = sql & v_credit_tp_sql
        sql = sql & v_stat_sql
        sql = sql & v_vc_tp_sql
        sql = sql & v_start_dt_sql
        sql = sql & v_end_dt_sql
        sql = sql & v_event_cd_sql
        sql = sql & v_ve_cd_sql
        sql = sql & v_store_grp_sql
        sql = sql & " and ( cre_by is not null "
        sql = sql & " and std_multi_co2.isValidCo(nvl(cre_by,'x'), '" & Session("CO_CD") & "') = 'Y') "
        sql = sql & " order by 1 "

        event_drp_list.Items.Insert(0, "EVENT:")
        event_drp_list.Items.FindByText("EVENT:").Value = ""
        event_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With event_drp_list
                .DataSource = ds
                .DataValueField = "event_cd"
                .DataTextField = "event_cd"
                .DataBind()
            End With

            connErp.Close()
            buyer_drp_list.Enabled = False
            ve_cd_drp_list.Enabled = False
            search_btn.Visible = False

            If v_credit_tp = "DC" Then
                credsku_btn.Visible = False
            Else
                credsku_btn.Visible = True
            End If

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    Protected Function getRowCnt() As Integer

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from credevt_sku_dtl "
        sql = sql & " where event_cd = '" & event_drp_list.SelectedItem.Text.ToString() & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value

    End Function
    Protected Sub populate_items()


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_event_cd As String = ""

        If new_event_cd.Text & "" = "" Then
            If event_drp_list.SelectedIndex < 1 Then
                lbl_msg.Text = "Please select an Event to continue"
                Exit Sub
            Else
                v_event_cd = event_drp_list.SelectedItem.Value.ToString()
            End If
        Else
            v_event_cd = new_event_cd.Text
        End If

        GridView1.DataSource = ""
        GridView9.DataSource = ""


        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = "SELECT s.itm_cd itm_cd, i.vsn vsn, i.des itm_des,s.qty qty, s.credit_amt amt, s.stat_cd itm_stat " &
                            "from itm i, credevt_sku_dtl s" &
                            " where i.itm_cd = s.itm_cd  " &
                            " and s.event_cd = '" & v_event_cd & "'" &
                            " order by 1 "

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub
    Protected Function getCurrValue(ByVal p_tp As String) As String

        Dim v_value As String = String.Empty
        Dim v_event_cd As String = ""

        If new_event_ind.Checked = True And new_event_cd.Text & "" <> "" Then
            v_event_cd = new_event_cd.Text
        Else
            If event_drp_list.SelectedIndex > 0 Then
                v_event_cd = event_drp_list.SelectedItem.Value.ToString()
            Else
                Return ""
            End If
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT " & p_tp & " v_data "
        sql = sql & " from credevt "
        sql = sql & " where event_cd = '" & v_event_cd & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value

    End Function

    Protected Function getItemInfo(ByVal p_tp As String, ByVal p_itm As String) As String

        Dim v_value As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT " & p_tp & " v_data "
        sql = sql & " from itm "
        sql = sql & " where itm_cd = '" & p_itm & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value

    End Function
    Protected Function getcredskuvendor() As String

        Dim v_value As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT min(ve_cd) v_data "
        sql = sql & " from itm i, credevt_sku_dtl s "
        sql = sql & " where s.itm_cd = i.itm_cd "
        sql = sql & "   and s.event_cd = '" & event_drp_list.SelectedItem.Value.ToString() & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = ""
            End If

        Catch
            v_value = ""
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value

    End Function
    Protected Function validDisc(ByVal p_disc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " SELECT std_multi_co.isValidDisc('" & Session("emp_init") & "','" & p_disc & "') v_data "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = "N"
            End If

        Catch
            v_value = "N"
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Protected Function isvalidItmVendor(ByVal p_itm_cd As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = " SELECT 'Y' v_data from itm i "
        sql = sql & " where i.itm_cd = '" & p_itm_cd & "' "
        sql = sql & "   and std_multi_co2.isValidVDR('" & Session("emp_init") & "',i.ve_cd) = 'Y' "
        sql = sql & "   and i.ve_cd = '" & ve_cd_drp_list.SelectedItem.Value.ToString() & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = "N"
            End If

        Catch
            v_value = "N"
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub populate_event_detail()

        Dim v_credit_tp As String = getCurrValue("credit_tp")
        Dim v_vcm As String = getCurrValue("vc_tp_cd")
        Dim v_stat As String = getCurrValue("stat_cd")
        Dim v_str As String = getCurrValue("pri_str")
        Dim v_start_dt As String = getCurrValue("strt_dt")
        Dim v_end_dt As String = getCurrValue("end_dt")
        Dim v_ve_cd As String = getCurrValue("ve_cd")
        Dim v_store_grp_cd As String = getCurrValue("store_grp_cd")
        Dim v_exclu_fran As String

        credit_tp_drp_list.ClearSelection()
        vc_tp_drp_list.ClearSelection()
        stat_drp_list.ClearSelection()
        str_drp_list.ClearSelection()
        store_grp_list.ClearSelection()

        If v_ve_cd & "" = "" Then
            v_ve_cd = getcredskuvendor()
        End If

        If v_ve_cd & "" <> "" Then
            ve_cd_drp_list.Items.FindByText(v_ve_cd).Selected = True
            ve_cd_drp_list.SelectedValue = v_ve_cd
        End If

        'sabrina R3053
        If v_store_grp_cd & "" <> "" Then
            store_grp_list.Items.FindByText(v_store_grp_cd).Selected = True
            store_grp_list.SelectedValue = v_store_grp_cd
        End If

        v_exclu_fran = getCurrValue("exclu_franchise")
        If v_exclu_fran & "" = "" Then
            exclu_fran_list.Items.FindByValue("Y").Selected = True
            exclu_fran_list.SelectedValue = "Y"
        Else
            exclu_fran_list.Items.FindByValue(v_exclu_fran).Selected = True
            exclu_fran_list.SelectedValue = v_exclu_fran
        End If

        cmnt.Text = getCurrValue("cmnt")
        'event_des.Text = getCurrValue("des")

        If v_credit_tp & "" <> "" Then
            credit_tp_drp_list.Items.FindByValue(v_credit_tp).Selected = True
            credit_tp_drp_list.SelectedValue = v_credit_tp
            If v_credit_tp = "DC" Then
                credsku_btn.Visible = False

                show_disc_fields()
                disc_cd.Text = getCurrValue("disc_cd")
                disc_pct.Text = getCurrValue("pct")
                disc_amt.Text = getCurrValue("amt")
            Else
                credsku_btn.Visible = True
                dont_show_disc_fields()
            End If
            If v_credit_tp = "FC" Or v_credit_tp = "IC" Or v_credit_tp = "BC" Then
                lbl_end_dt.Visible = False
                end_dt.Visible = False
            Else
                end_dt.Visible = True
                lbl_end_dt.Visible = True
                If v_end_dt & "" = "" Then
                    end_dt.SelectedDate = DateTime.MinValue
                Else
                    end_dt.SelectedDate = v_end_dt
                    end_dt.VisibleDate = v_end_dt
                End If
            End If
        End If
        If v_vcm & "" <> "" Then
            vc_tp_drp_list.Items.FindByValue(v_vcm).Selected = True
            vc_tp_drp_list.SelectedValue = v_vcm
        End If

        If v_stat & "" = "" Or isEmpty(v_stat) = True Then
            save_btn.Visible = True
            stat_drp_list.Items.FindByText(" ").Selected = True
            stat_drp_list.SelectedValue = " "
        Else
            save_btn.Visible = False
            stat_drp_list.Items.FindByText(v_stat).Selected = True
            stat_drp_list.SelectedValue = v_stat
            If v_stat = "O" Then
                If credit_tp_drp_list.SelectedItem.Value.ToString() <> "BC" And credit_tp_drp_list.SelectedItem.Value.ToString() <> "FC" And credit_tp_drp_list.SelectedItem.Value.ToString() <> "IC" Then
                    lbl_msg.Text = "Please note - only END DATE may be updated - no other change will be saved"
                    save_btn.Visible = True
                End If
            End If
        End If

        If v_start_dt & "" = "" Then
            start_dt.SelectedDate = DateTime.MinValue
        Else
            start_dt.SelectedDate = v_start_dt
            start_dt.VisibleDate = v_start_dt
        End If

        If v_str & "" <> "" Then
            If v_str <> "ALL" Then
                str_drp_list.Items.FindByValue(v_str).Selected = True
                str_drp_list.SelectedValue = v_str
            End If
        Else
            str_drp_list.Items.FindByValue(v_str).Selected = True
            str_drp_list.SelectedValue = v_str
        End If

        If (Not GridView1.DataSource Is Nothing) Then
            GridView1.DataSource = ""
            GridView1.DataBind()
        End If

        'sabrina R3053
        If store_grp_list.SelectedIndex > 1 Then
            str_drp_list.SelectedIndex = 0
            str_drp_list.Enabled = False
        Else
            str_drp_list.Enabled = True
        End If

        populate_items()

    End Sub

    Protected Sub EventSelected(ByVal sender As Object, ByVal e As System.EventArgs)

        populate_event_detail()
        vcm_btn.Visible = True


    End Sub
    Protected Sub CreditTPSelected(ByVal sender As Object, ByVal e As System.EventArgs)

        If credit_tp_drp_list.SelectedItem.Value.ToString() = "DC" Then
            lbl_str.Visible = False
            str_drp_list.Visible = False
            disc_cd.Visible = True
            disc_amt.Visible = True
            disc_pct.Visible = True
            lbl_disc_cd.Visible = True
            lbl_disc_amt.Visible = True
            lbl_disc_pct.Visible = True

        Else
            lbl_str.Visible = True
            str_drp_list.Visible = True
            disc_cd.Visible = False
            disc_amt.Visible = False
            disc_pct.Visible = False

            lbl_disc_cd.Visible = False
            lbl_disc_amt.Visible = False
            lbl_disc_pct.Visible = False

        End If

        If credit_tp_drp_list.SelectedItem.Value.ToString() = "BC" Or credit_tp_drp_list.SelectedItem.Value.ToString() = "FC" Or credit_tp_drp_list.SelectedItem.Value.ToString() = "IC" Then
            lbl_end_dt.visible = False
            end_dt.Visible = False
            end_dt.Enabled = False
        Else
            lbl_end_dt.visible = True
            end_dt.Visible = True
            end_dt.Enabled = True
        End If

        'sabrina R3151
        reset_exclu_fran()

    End Sub
    Protected Sub BuyerSelected(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            If credit_tp_drp_list.SelectedIndex > 0 Then
                credit_tp_drp_list.SelectedIndex = 0
            End If

            If vc_tp_drp_list.SelectedIndex > 0 Then
                vc_tp_drp_list.SelectedIndex = 0
            End If

            If str_drp_list.SelectedIndex > 0 Then
                str_drp_list.SelectedIndex = 0
            End If

            If stat_drp_list.SelectedIndex > 0 Then
                stat_drp_list.SelectedIndex = 0
            End If

            If ve_cd_drp_list.SelectedIndex > 0 Then
                ve_cd_drp_list.SelectedIndex = 0
            End If

            If event_drp_list.SelectedIndex > 0 Then
                event_drp_list.Items.Clear()
            End If

            'event_des.Text = ""
            cmnt.Text = ""

            start_dt.SelectedDate = datetime.minvalue
            start_dt.VisibleDate = datetime.minvalue

            end_dt.SelectedDate = datetime.minvalue
            end_dt.VisibleDate = datetime.minvalue

            If (Not GridView1.DataSource Is Nothing) Then
                GridView1.DataSource = ""
                GridView1.DataBind()
            End If



        Catch x As Exception
            Throw
        End Try

    End Sub

    Public Sub reset_exclu_fran()

        'sabrina R3153 (new sub)
        If credit_tp_drp_list.SelectedIndex > 0 Then
            If credit_tp_drp_list.SelectedItem.Value.ToString() = "DC" Or
               credit_tp_drp_list.SelectedItem.Value.ToString() = "SC" Then
                exclu_fran_list.Items.Clear()
                exclu_fran_list.Items.Add("N")
            Else
                If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
                    exclu_fran_list.Items.Clear()
                    exclu_fran_list.Items.Add("N")
                Else
                    If exclu_fran_list.SelectedItem.Value.ToString() = "Y" Then
                        exclu_fran_list.Items.Clear()
                        exclu_fran_list.Items.Add("Y")
                        exclu_fran_list.Items.Add("N")
                    Else
                        exclu_fran_list.Items.Clear()
                        exclu_fran_list.Items.Add("N")
                        exclu_fran_list.Items.Add("Y")
                    End If
                End If
            End If
        End If
        'sabrina - R3153-end

    End Sub

    Protected Sub VCMTPSelected(ByVal sender As Object, ByVal e As System.EventArgs)

        If new_event_ind.Checked = True Then
            If vc_tp_drp_list.SelectedIndex > 0 Then
                If vc_tp_drp_list.SelectedItem.Value.ToString = "PRO" Or
                    vc_tp_drp_list.SelectedItem.Value.ToString = "PDT" Or
                    vc_tp_drp_list.SelectedItem.Value.ToString = "ADV" Then
                    lbl_msg.Text = "Invalid VCM Sort Code - please try again"
                    vc_tp_drp_list.Focus()
                End If

            End If
        End If

    End Sub
    Protected Sub STRSelected(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub STATSelected(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub vecdSelected(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub search_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Populate_events()
        new_event_ind.Enabled = False


    End Sub
    Protected Sub vcm_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If event_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Select Event Code to proceed"
            Exit Sub
        End If

        ASPxPopupvcmdetail.ShowOnPageLoad = True
        populate_vcm()

    End Sub
    Protected Sub populate_vcm()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_event_cd As String = event_drp_list.SelectedItem.Value.ToString()
        Dim v_ve_cd As String = getCurrValue("ve_cd")

        If v_ve_cd & "" = "" Then
            v_ve_cd = getcredskuvendor()
        End If

        If (Not GridView2.DataSource Is Nothing) Then
            GridView2.DataSource = ""
            GridView2.DataBind()
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = "SELECT a.co_cd CO_CD, a.event_cd EVENT_CD, a.vc_cd VC_CD, " &
                             "       a.processed_ind PROCESSED_IND, a.proc_dt PROC_DT, i.ivc_amt AMOUNT " &
                             "  FROM credevt_vcm a, ivc i " &
                             " where a.event_cd = '" & v_event_cd & "'" &
                             "   and std_multi_co2.isvalidco('" & Session("emp_init") & "', a.co_cd) = 'Y' " &
                             "   and i.ivc_cd = a.vc_cd " &
                             "   and i.co_cd = '" & Session("CO_CD") & "' " &
                             "   and i.ve_cd = '" & v_ve_cd & "' " &
                             " union " &
                             "SELECT '" & Session("CO_CD") & "' CO_CD, b.event_cd EVENT_CD, b.vc_cd VC_CD, " &
                             "       b.processed_ind PROCESSED_IND, nvl(b.upd_dt,'01-JAN-1900') PROC_DT, i.ivc_amt AMOUNT" &
                             "  FROM credevt b, ivc i " &
                             " where b.event_cd = '" & v_event_cd & "'" &
                             "   and b.vc_cd is not null " &
                             "   and b.vc_cd = i.ivc_cd " &
                             "   and i.co_cd = '" & Session("CO_CD") & "' " &
                             "   and i.ve_cd = '" & v_ve_cd & "' "


        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView2.DataSource = ds
                GridView2.DataBind()

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView2.DataSource = ds
                GridView2.DataBind()

            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub
    Protected Sub storeGrpChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'R3053 sabrina
        If store_grp_list.SelectedIndex > 1 Then
            str_drp_list.SelectedIndex = 0
            str_drp_list.Enabled = False
        Else
            str_drp_list.Enabled = True
        End If

 
    End Sub
    Protected Sub strtdatechanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Sub excludeIndChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'R2913 - sabrina 
        Dim co_grp_cd As String = ""
        If Session("CO_CD") & "" <> "" Then
            co_grp_cd = LeonsBiz.GetGroupCode(Session("CO_CD"))
            If co_grp_cd <> "BRK" Then
                Populate_str()
            End If
        End If

        'sabrina R3153
        reset_exclu_fran()

    End Sub


    Public Function enddateisvalid() As String
        Dim v_dt_diff As String
        v_dt_diff = DateDiff(DateInterval.Day, end_dt.SelectedDate, Now)
        If v_dt_diff > 0 Then
            lbl_msg.Text = "Error - Invalid End Date - must be equal to or greater than today's date "
            end_dt.Focus()
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Public Function SelectedDataisValid() As String


        Dim v_credit_tp As String = getCurrValue("credit_tp")
        Dim v_vc_tp_cd As String = getCurrValue("vc_tp_cd")
        Dim v_stat As String = getCurrValue("stat_cd")
        Dim v_str As String = getCurrValue("pri_str")
        Dim v_start_dt As String = getCurrValue("strt_dt")
        Dim v_end_dt As String = getCurrValue("end_dt")
        Dim v_exclu_fran As String = getCurrValue("exclu_franchise")
        Dim v_des As String = "" 'getCurrValue("des")
        Dim v_cmnt As String = ""
        Dim v_ve_cd As String = getCurrValue("ve_cd")

        If buyer_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Must select a Buyer to continue"
            buyer_drp_list.Focus()
            Return "N"
        End If

        'If event_des.Text & "" = "" Then
        '    lbl_msg.Text = "Please enter a Description"
        '    event_des.Focus()
        '    Return "N"
        'End If

        If credit_tp_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Credit Type must be selected to continue"
            credit_tp_drp_list.Focus()
            Return "N"
        End If

        If vc_tp_drp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "VCM Sort code must be selected to continue"
            vc_tp_drp_list.Focus()
            Return "N"
        End If

        If cmnt.Text & "" = "" Then
            lbl_msg.Text = "Please enter a value in the COMMENT field"
            cmnt.Focus()
            Return "N"
        End If

        'check discount fields for DC type
        If credit_tp_drp_list.SelectedItem.Value.ToString() = "DC" Then
            If disc_cd.Text & "" = "" Then
                lbl_msg.Text = "Must enter a valid Discount Code"
                disc_cd.Focus()
                Return "N"
            ElseIf validDisc(UCase(disc_cd.Text)) <> "Y" Then
                lbl_msg.Text = "Discount Code is invalid - please try again"
                disc_cd.Focus()
                Return "N"
            End If

            If disc_pct.Text.isEmpty = True And disc_amt.Text.isEmpty = True Then
                lbl_msg.Text = "Must enter Discount Amount or PCT "
                disc_pct.Focus()
                Return "N"
            ElseIf Not disc_pct.Text.isEmpty And Not disc_amt.Text.isEmpty Then
                lbl_msg.Text = "Must enter either Discount Amount or PCT - not both"
                disc_pct.Focus()
                Return "N"
            ElseIf disc_pct.Text & "" <> "" And Not disc_pct.Text.isEmpty Then
                If Not IsNumeric(disc_pct.Text) Then
                    lbl_msg.Text = "Invalid Value - please try again"
                    disc_pct.Focus()
                    Return "N"
                End If
            ElseIf disc_amt.Text & "" <> "" And Not disc_amt.Text.isEmpty Then
                If Not IsNumeric(disc_amt.Text) Then
                    lbl_msg.Text = "Invalid Value - please try again"
                    disc_amt.Focus()
                    Return "N"
                End If
            End If
        End If

        'sabrina R3053
        If store_grp_list.SelectedIndex > 0 And
           str_drp_list.SelectedIndex > 0 Then
            lbl_msg.Text = "Invalid value - must select either Store or Store Group not both"
            str_drp_list.Focus()
            Return "N"
        End If
        '...............check discount fields for DC type


        If start_dt.SelectedDate.Date = DateTime.MinValue Then
            lbl_msg.Text = "Please select a START date to continue"
            start_dt.Focus()
            Return "N"
        End If

        Dim v_dt_diff As String
        v_dt_diff = DateDiff(DateInterval.Day, start_dt.SelectedDate, Now)
        If v_dt_diff > 0 Then
            lbl_msg.Text = "Error - Invalid Start Date - cannot back date"
            start_dt.Focus()
            Return "N"
        End If

        If credit_tp_drp_list.SelectedItem.Value.ToString() = "FC" Or credit_tp_drp_list.SelectedItem.Value.ToString() = "IC" Or credit_tp_drp_list.SelectedItem.Value.ToString() = "BC" Then
            'do nothing
        Else
            If end_dt.SelectedDate.Date = DateTime.MinValue Then
                lbl_msg.Text = "Please select END date to continue"
                end_dt.Focus()
                Return "N"
            Else
                If end_dt.SelectedDate.Date < start_dt.SelectedDate.Date Then
                    lbl_msg.Text = "Error - Invalid End Date - must be equal to or greater than Start Date"
                    end_dt.Focus()
                    Return "N"
                End If
            End If
        End If

        Return "Y"

    End Function
    Protected Sub save_new_credevt()

        Dim v_valid As String = ""

        Dim v_buyer As String = ""
        Dim v_ve_cd As String = ""
        Dim v_exclu_fran As String = ""


        Dim v_credit_tp As String = ""
        Dim v_vc_tp As String = ""
        Dim v_str_cd As String = ""
        Dim v_str_sql As String = ""

        Dim v_disc_cd As String = ""
        Dim v_disc_amt As String = ""
        Dim v_disc_pct As String = ""
        Dim v_disc_sql As String = ""

        Dim v_des As String = ""
        Dim v_cmnt As String = ""

        Dim v_start_dt As String = ""
        Dim v_end_dt As String = ""
        Dim v_start_dt_sql As String = ""
        Dim v_end_dt_sql As String = ""
        Dim v_dt_diff As Integer = 0
        Dim v_store_grp_cd As String = ""

        Dim sql As String = ""

        v_valid = SelectedDataisValid()
        If v_valid <> "Y" Then
            Exit Sub
        End If

        v_buyer = buyer_drp_list.SelectedItem.Value.ToString()

        v_exclu_fran = exclu_fran_list.SelectedItem.Value.ToString()
        v_credit_tp = credit_tp_drp_list.SelectedItem.Value.ToString()
        v_vc_tp = vc_tp_drp_list.SelectedItem.Value.ToString()
        If v_vc_tp = "ADV" Or v_vc_tp = "PRO" Or v_vc_tp = "PTD" Then
            lbl_msg.Text = "Invalid VCM Sort Code - please try again"
            Exit Sub
        End If

        v_des = Replace(cmnt.Text, "'", "'||''''||'")
        v_cmnt = Replace(cmnt.Text, "'", "'||''''||'")

        If ve_cd_drp_list.SelectedIndex < 1 Then
            v_ve_cd = ""
        Else
            v_ve_cd = ve_cd_drp_list.SelectedItem.Value.ToString()
        End If

        If str_drp_list.SelectedIndex < 1 Then
            v_str_cd = "ALL"
        Else
            v_str_cd = str_drp_list.SelectedItem.Value.ToString()
        End If

        If v_credit_tp = "DC" Then

            v_disc_cd = UCase(disc_cd.Text)
            If disc_pct.Text & "" = "" Or disc_pct.Text.isEmpty = True Then
                v_disc_pct = "''"
            Else
                v_disc_pct = disc_pct.Text
            End If

            If disc_amt.Text & "" = "" Or disc_amt.Text.isEmpty = True Then
                v_disc_amt = "''"
            Else
                v_disc_amt = disc_amt.Text
            End If
        Else
            v_disc_cd = ""
            v_disc_pct = "''"
            v_disc_amt = "''"
        End If

        v_start_dt_sql = " ,to_date('" & FormatDateTime(start_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"

        If v_credit_tp = "FC" Or v_credit_tp = "IC" Or v_credit_tp = "BC" Then
            v_end_dt_sql = " null "
        Else
            v_end_dt_sql = " to_date('" & FormatDateTime(end_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        End If

        'sabrina R3053 (add store_grp_cd)
        If store_grp_list.SelectedIndex < 1 Then
            v_store_grp_cd = ""
        Else
            v_store_grp_cd = store_grp_list.SelectedItem.Value.ToString()
        End If

        '---------------------------------------------------------------------------------------------
        '----------------------------------- insert into credevt -------------------------------------
        '---------------------------------------------------------------------------------------------

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'sabrina R3053 (add store_grp_cd)
        sql = "Insert into credevt (event_cd,buyer,des,strt_dt,end_dt,credit_tp,vc_tp_cd,stat_cd,processed_ind,appl_lvl,cre_dt" &
              ",cre_by,upd_dt,upd_by,pri_str,vc_cd,auth_num,approval_nm,cmnt,disc_cd,pct,amt,ve_cd,exclu_franchise,store_grp_cd) " &
              " values ( " &
                    "'" & UCase(new_event_cd.Text) & "'" &
                    ",'" & UCase(v_buyer) & "'" &
                    ",'" & v_des & "'" &
                    v_start_dt_sql &
                    " ," & v_end_dt_sql &
                    ",'" & v_credit_tp & "'" &
                    ",'" & v_vc_tp & "'" &
                    ", null " &
                    " ,null,null " &
                    " ,to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')" &
                    ",'" & Session("emp_init") & "'" &
                    " ,null,null " &
                    " ,'" & v_str_cd & "'" &
                    " , null,null,null " &
                    ",'" & v_cmnt & "'" &
                    ",'" & v_disc_cd & "'" &
                    "," & v_disc_pct &
                    "," & v_disc_amt &
                    ",'" & v_ve_cd & "'" &
                    ",'" & v_exclu_fran & "'" &
                    ",'" & v_store_grp_cd & "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        save_btn.Visible = False

        If v_credit_tp = "DC" Then
            lbl_msg.Text = " Cred. event " & UCase(new_event_cd.Text) & " is created"
            credsku_btn.Visible = False
        Else
            lbl_msg.Text = " Cred. event " & UCase(new_event_cd.Text) & " is created - ITEMs can be added to the Event "
            credsku_btn.Visible = True
        End If

    End Sub
    Protected Sub update_credevet()

        Dim v_buyer As String = ""
        Dim v_ve_cd As String = ""
        Dim v_exclu_fran As String = ""
        Dim v_stat As String = ""

        Dim v_credit_tp As String = ""
        Dim v_vc_tp As String = ""
        Dim v_str_cd As String = ""

        Dim v_disc_cd As String = ""
        Dim v_disc_amt As String = ""
        Dim v_disc_pct As String = ""
        Dim v_disc_sql As String = ""
        Dim v_store_grp_cd As String = ""


        Dim v_des As String = ""
        Dim v_cmnt As String = ""

        Dim v_start_dt As String = ""
        Dim v_end_dt As String = ""
        Dim v_start_dt_sql As String = ""
        Dim v_end_dt_sql As String = ""
        Dim v_dt_diff As Integer = 0

        Dim v_curr_stat As String = getCurrValue("stat_cd")
        Dim v_curr_vc_tp_cd As String = getCurrValue("vc_tp_cd")

        If stat_drp_list.SelectedIndex > 0 Then

            If stat_drp_list.SelectedItem.Value.ToString() = "C" Or stat_drp_list.SelectedItem.Value.ToString() = "E" Then
                lbl_msg.Text = "Invalid Status code - please try again"
                stat_drp_list.Focus()
                Exit Sub
            ElseIf stat_drp_list.SelectedItem.Value.ToString() = "V" Then
                VoidTheEvent()       'VOID THE EVENT - NO OTHER VALIDATION IS REQUIRED
                Exit Sub
            End If
        End If

        If v_curr_stat = "O" Then
            If enddateisvalid() <> "Y" Then
                Exit Sub
            End If
        Else
            If SelectedDataisValid() <> "Y" Then
                Exit Sub
            End If
        End If


        v_buyer = buyer_drp_list.SelectedItem.Value.ToString()
        v_ve_cd = ve_cd_drp_list.SelectedItem.Value.ToString()
        v_exclu_fran = exclu_fran_list.SelectedItem.Value.ToString()
        v_stat = stat_drp_list.SelectedItem.Value.ToString()
        v_credit_tp = credit_tp_drp_list.SelectedItem.Value.ToString()
        v_vc_tp = vc_tp_drp_list.SelectedItem.Value.ToString()


        If v_vc_tp <> v_curr_vc_tp_cd Then
            If v_vc_tp = "ADV" Or v_vc_tp = "PRO" Or v_vc_tp = "PDT" Then
                lbl_msg.Text = "Invalid VCM Sort Code - please try again"
                Exit Sub
            End If
        End If

        v_des = Replace(cmnt.Text, "'", "'||''''||'")
        v_cmnt = Replace(cmnt.Text, "'", "'||''''||'")

        If v_stat & "" <> "" Then
            If v_stat = " " Then
                v_stat = ""
            End If
        End If

        If str_drp_list.SelectedIndex < 1 Then
            v_str_cd = "ALL"
        Else
            v_str_cd = str_drp_list.SelectedItem.Value.ToString()
        End If

        'sabrina R3053
        If store_grp_list.SelectedIndex < 1 Then
            v_store_grp_cd = ""
        Else
            v_store_grp_cd = store_grp_list.SelectedItem.Value.ToString()
        End If

        If v_credit_tp = "DC" Then
            v_disc_cd = UCase(disc_cd.Text)

            If disc_pct.Text & "" = "" Or disc_pct.Text.isEmpty = True Then
                v_disc_pct = "''"
            Else
                v_disc_pct = disc_pct.Text
            End If

            If disc_amt.Text & "" = "" Or disc_amt.Text.isEmpty = True Then
                v_disc_amt = "''"
            Else
                v_disc_amt = disc_amt.Text
            End If

        Else
            v_disc_amt = "''"
            v_disc_pct = "''"
            v_disc_cd = ""
        End If

        v_start_dt_sql = " to_date('" & FormatDateTime(start_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"

        If v_credit_tp = "FC" Or v_credit_tp = "IC" Or v_credit_tp = "BC" Then
            v_end_dt_sql = " null "
        Else
            v_end_dt_sql = " to_date('" & FormatDateTime(end_dt.SelectedDate.ToString, DateFormat.ShortDate) & "', 'mm/dd/RRRR')"
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()


        If v_curr_stat & "" = "" Then
            sql = " update credevt "
            sql = sql & "set stat_cd = '" & v_stat & "'"
            sql = sql & "  , buyer = '" & v_buyer & "'"
            sql = sql & "  , exclu_franchise = '" & v_exclu_fran & "'"
            sql = sql & "  , credit_tp = '" & v_credit_tp & "'"
            sql = sql & "  , vc_tp_cd = '" & v_vc_tp & "'"
            sql = sql & "  , pri_str = '" & v_str_cd & "'"
            sql = sql & "  , amt = " & v_disc_amt
            sql = sql & "  , pct = " & v_disc_pct
            sql = sql & "  , disc_cd = '" & v_disc_cd & "'"
            sql = sql & "  , ve_cd = '" & v_ve_cd & "'"
            sql = sql & "  , cmnt = '" & v_cmnt & "'"
            sql = sql & "  , des = '" & v_des & "'"
            sql = sql & "  , store_grp_cd = '" & v_store_grp_cd & "'"
            sql = sql & "  , strt_dt = " & v_start_dt_sql
            sql = sql & "  , end_dt = " & v_end_dt_sql
            sql = sql & "  , upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')"
            sql = sql & "  , upd_by = '" & Session("emp_init") & "'"
            sql = sql & " where event_cd = '" & event_drp_list.SelectedItem.Value.ToString() & "'"
            sql = sql & "   and nvl(stat_cd,'x') = 'x' "
        ElseIf v_curr_stat = "O" Then
            sql = " update credevt "
            sql = sql & " set  end_dt = " & v_end_dt_sql
            sql = sql & "  , upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')"
            sql = sql & "  , upd_by = '" & Session("emp_init") & "'"
            sql = sql & " where event_cd = '" & event_drp_list.SelectedItem.Value.ToString() & "'"
            sql = sql & "   and nvl(stat_cd,'x') = 'O' "
        Else
            lbl_msg.Text = "No updates allowed - Event status is (" & v_curr_stat & ")"
            stat_drp_list.Focus()
            Exit Sub
        End If

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        save_btn.Visible = False
        lbl_msg.Text = "Cred. event " & event_drp_list.SelectedItem.Value.ToString() & " is updated"

    End Sub
    Protected Sub save_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If new_event_ind.Checked = True And new_event_cd.Text & "" <> "" Then
            If EventExists(UCase(new_event_cd.Text)) = "Y" Then
                lbl_msg.Text = "Event " & UCase(new_event_cd.Text) & " already exists - please press CLEAR and try again"
                Exit Sub
            Else
                save_new_credevt()
            End If
        Else
            If event_drp_list.SelectedIndex < 1 Then
                lbl_msg.Text = "Error - No Event is selected "
                Exit Sub
            Else
                Dim v_stat As String = getCurrValue("stat_cd")
                If v_stat.isEmpty Or v_stat = "O" Then
                    update_credevet()
                Else
                    lbl_msg.Text = "Error - No updates allowed for this event - Event status is " & v_stat
                End If
            End If
        End If

    End Sub
    Protected Sub clear_all_flds()


        If credit_tp_drp_list.SelectedIndex > 0 Then
            credit_tp_drp_list.SelectedIndex = 0
        End If
        If vc_tp_drp_list.SelectedIndex > 0 Then
            vc_tp_drp_list.SelectedIndex = 0
        End If
        If str_drp_list.SelectedIndex > 0 Then
            str_drp_list.SelectedIndex = 0
        End If
        If stat_drp_list.SelectedIndex > 0 Then
            stat_drp_list.SelectedIndex = 0
        End If
        'sabrina R3053
        If store_grp_list.SelectedIndex > 0 Then
            store_grp_list.SelectedIndex = 0
        End If

        event_drp_list.Items.Clear()
        credit_tp_drp_list.Items.Clear()
        vc_tp_drp_list.Items.Clear()
        str_drp_list.Items.Clear()
        stat_drp_list.Items.Clear()
        ve_cd_drp_list.Items.Clear()
        store_grp_list.Items.Clear()

        str_drp_list.Visible = True
        lbl_str.Visible = True

        end_dt.Visible = True
        lbl_end_dt.Visible = True

        show_disc_fields()

        cmnt.Text = ""
        disc_cd.Text = ""
        disc_pct.Text = ""
        disc_amt.Text = ""
        new_event_cd.Text = ""
        new_event_ind.Enabled = True
        new_event_ind.Checked = False
        'event_des.Text = ""

        start_dt.SelectedDate = DateTime.MinValue
        start_dt.VisibleDate = DateTime.MinValue

        end_dt.SelectedDate = DateTime.MinValue
        end_dt.VisibleDate = DateTime.MinValue

        Populate_credit_tp()
        Populate_vc_tp()
        Populate_stat_cd()
        Populate_str()
        Populate_store_grp()
        Populate_ve()

        'GridView1.DataSource = ""
        'GridView1.DataBind()

        search_btn.Visible = True
        stat_drp_list.Enabled = True
        save_btn.Visible = False

        new_event_ind.Enabled = True
        new_event_ind.Checked = False

        new_event_cd.Visible = False
        event_drp_list.Visible = True
        buyer_drp_list.Enabled = True
        ve_cd_drp_list.Enabled = True

        vcm_btn.Visible = False

        'unprotect_all_fields()

        ASPxPopupCREDSKU2.ShowOnPageLoad = False
        ASPxPopupCREDSKU1.ShowOnPageLoad = False
        ASPxPopupvcmdetail.ShowOnPageLoad = False

        'R3153 sabrina
        exclu_fran_list.Items.Clear()
        If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
            exclu_fran_list.Items.Add("N")
        Else
            exclu_fran_list.Items.Add("N")
            exclu_fran_list.Items.Add("Y")
        End If

        'sabrina R3053
        str_drp_list.Enabled = True

    End Sub
    Protected Sub VoidTheEvent()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " update credevt "
        sql = sql & "set stat_cd = 'V' "
        sql = sql & " where event_cd = '" & event_drp_list.SelectedItem.Value.ToString() & "'"
        sql = sql & "   and nvl(stat_cd,'x') = 'x' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        save_btn.Visible = False
        lbl_msg.Text = "Cred. event " & event_drp_list.SelectedItem.Value.ToString() & " is VOIDED "

    End Sub
    Protected Sub Clear_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        buyer_drp_list.SelectedIndex = 0
        Populate_buyer()
        clear_all_flds()
        'event_drp_list.Enabled = True
        event_drp_list.Visible = True

    End Sub
    Protected Sub credsku_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim v_status As String = ""

        If event_drp_list.SelectedIndex < 1 Then
            If new_event_cd.Text & "" = "" Then
                lbl_msg.Text = "Please select an Event to continue"
                Exit Sub
            Else
                credsku_event_cd.Text = new_event_cd.Text
            End If
        Else
            credsku_event_cd.Text = event_drp_list.SelectedItem.Value.ToString()
        End If

        v_status = getCurrValue("stat_cd")

        populate_items()

        If v_status & "" = "" Then
            ASPxPopupCREDSKU1.ShowOnPageLoad = True
        Else
            ASPxPopupCREDSKU2.ShowOnPageLoad = True
            credsku_event_cd9.Text = credsku_event_cd.Text
        End If



    End Sub
    Protected Function ItemAlreadyExists(ByVal p_itm As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim v_event_cd As String

        If new_event_cd.Text & "" = "" Then
            If event_drp_list.SelectedIndex < 0 Then
                Return "N"
            Else
                v_event_cd = event_drp_list.SelectedItem.Value.ToString()
            End If
        Else
            v_event_cd = UCase(new_event_cd.Text)
        End If


        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from credevt_sku_dtl "
        sql = sql & " where event_cd = '" & v_event_cd & "'"
        sql = sql & " and itm_cd = '" & p_itm & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
    Protected Function ItemIsVoided(ByVal p_itm As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT stat_cd "
        sql = sql & " from credevt_sku_dtl "
        sql = sql & " where event_cd = '" & event_drp_list.SelectedValue.ToString() & "'"
        sql = sql & " and itm_cd = '" & p_itm & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("stat_cd")
            Else
                v_value = ""
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Protected Function EventExists(ByVal p_event As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt "
        sql = sql & " from credevt "
        sql = sql & " where event_cd = '" & p_event & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If

        Catch
            v_value = 0
            'do nothing

        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
    Protected Function isvaliditm(ByVal p_itm As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " SELECT std_multi_co.isValiditm('" & Session("emp_init") & "','" & p_itm & "') v_data "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = "N"
            End If

        Catch
            v_value = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        If e.Column.FieldName = "ITM_DES" Or e.Column.FieldName = "VSN" Then
            e.Editor.ReadOnly = True
        Else
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        End If


    End Sub
    Protected Function isValidItmTp(ByVal p_itm As String) As String

        Dim v_cnt As Integer = 0


        If credit_tp_drp_list.SelectedIndex < 1 Then
            Return "Y"
        Else
            If credit_tp_drp_list.SelectedItem.Value.ToString() <> "PC" Then
                Return "Y"
            End If
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " SELECT count(*) cnt from itm where itm_cd = '" & p_itm & "' and itm_tp_cd = 'PKG' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_cnt = dbReader.Item("cnt")
            Else
                v_cnt = 0
            End If

        Catch
            v_cnt = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_cnt & "" = "" Then
            Return "N"
        Else
            If v_cnt = 0 Then
                Return "N"
            Else
                Return "Y"
            End If
        End If

    End Function

    Protected Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("ITM_CD") = ""
        e.NewValues("VSN") = ""
        e.NewValues("ITM_DES") = ""
        e.NewValues("QTY") = ""
        e.NewValues("AMT") = ""
        e.NewValues("ITM_STAT") = ""

    End Sub


    Protected Sub rowvalidation(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        e.RowError = ""

        If e.IsNewRow Then

            If e.NewValues("ITM_CD") & "" = "" Then
                e.RowError = "Item code is required"
            ElseIf isvaliditm(UCase(e.NewValues("ITM_CD"))) <> "Y" Then
                e.RowError = "Invalid Item - please try again"
            ElseIf isvalidItmVendor(UCase(e.NewValues("ITM_CD"))) <> "Y" Then
                e.RowError = "Item must belogn to vendor " & ve_cd_drp_list.SelectedItem.Value.ToString() & " - Please try again."
            ElseIf e.IsNewRow = True Then
                If ItemAlreadyExists(UCase(e.NewValues("ITM_CD"))) = "Y" Then
                    e.RowError = "Item already exists - please try again"
                ElseIf isValidItmTp(UCase(e.NewValues("ITM_CD"))) <> "Y" Then
                    e.RowError = "Invalid Item Type - must be PKG for this event"
                Else
                    e.RowError = ""
                    e.Keys.Clear()
                End If
            Else
                e.RowError = ""
                e.Keys.Clear()
            End If
        Else
            If e.OldValues("ITM_STAT") & "" <> "" Then
                If UCase(e.OldValues("ITM_STAT")) = "V" Then
                    e.RowError = "This Item is voided - no changes allowed - press CANCEL to exit"
                    Exit Sub
                End If
            End If
            If e.NewValues("ITM_CD") & "" <> "" And e.OldValues("ITM_CD") & "" <> "" Then
                If e.OldValues("ITM_CD") <> e.NewValues("ITM_CD") Then
                    e.RowError = "ITEM code cannot be changed "
                    Exit Sub
                End If
            End If
        End If

        If e.RowError <> "" Then
            Exit Sub
        End If

        If e.NewValues("QTY") & "" = "" Then
        Else
            If credit_tp_drp_list.SelectedIndex > 0 Then
                If e.NewValues("QTY") & "" <> 0 Then
                    If credit_tp_drp_list.SelectedItem.Value.ToString = "BC" Or
                        credit_tp_drp_list.SelectedItem.Value.ToString = "IC" Or
                        credit_tp_drp_list.SelectedItem.Value.ToString = "SC" Or
                        credit_tp_drp_list.SelectedItem.Value.ToString = "TC" Then
                        e.RowError = "QTY field must be null for Event type " & credit_tp_drp_list.SelectedItem.Value.ToString
                        Exit Sub
                    Else
                        If Not IsNumeric(e.NewValues("QTY")) Then
                            e.RowError = "Invalid QTY - please try again"
                            Exit Sub
                        End If
                    End If
                End If
            End If
        End If

        If e.NewValues("AMT") & "" = "" Then
            e.RowError = "Credit Amount is required"
        ElseIf Not IsNumeric(e.NewValues("AMT")) Then
            e.RowError = "Invalid Credit Amount - please try again"
        Else
            e.RowError = ""
        End If

        If e.RowError <> "" Then
            Exit Sub
        End If

        If e.NewValues("ITM_STAT") & "" <> "" Then
            If UCase(e.NewValues("ITM_STAT")) <> "V" Then
                e.RowError = "Invalid Status code- please try again "
            Else
                e.RowError = ""
            End If
        Else
            e.RowError = ""
        End If

    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        Dim hasError As Boolean = False
        Dim errorMsg As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_stat_sql As String = ""
        Dim v_qty_sql As String = ""
        Dim v_void_dt_sql As String = ""
        Dim v_addon_dt_sql As String = ""

        If e.NewValues("QTY") & "" = "" Or isEmpty(e.NewValues("QTY")) Then
            v_qty_sql = " ,QTY = NULL "
        Else
            v_qty_sql = " ,QTY = " & e.NewValues("QTY").ToString() & " "
        End If

        If e.NewValues("ITM_STAT") & "" = "" Then
            v_stat_sql = " , STAT_CD = NULL "
        Else
            If e.NewValues("ITM_STAT") = " " Then
                v_stat_sql = " , STAT_CD = NULL "
            Else
                v_stat_sql = " ,STAT_CD = '" + UCase(e.NewValues("ITM_STAT").ToString()) + "'"
                If UCase(e.NewValues("ITM_STAT").ToString()) = "V" Then
                    v_void_dt_sql = " , void_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR')"
                Else
                    v_void_dt_sql = " , void_dt = null "
                End If
            End If
        End If
        sql = "UPDATE CREDEVT_SKU_DTL SET "
        sql = sql & " CREDIT_AMT = " + e.NewValues("AMT").ToString()
        sql = sql & v_stat_sql
        sql = sql & v_qty_sql
        sql = sql & v_void_dt_sql
        sql = sql & " WHERE EVENT_CD = '" + UCase(credsku_event_cd.Text) + "'"
        sql = sql & " and ITM_CD = '" + UCase(e.OldValues("ITM_CD").ToString()) + "'"

        Dim v_val As String = ""

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        populate_items()
        GridView1.DataBind()
        e.Cancel = True
        GridView1.CancelEdit()

        Throw New Exception("Update is done")

    End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim v_stat As String = ""
        Dim v_event_cd As String = ""
        Dim v_amt As String = ""
        Dim v_qty As String = ""
        Dim v_addon_dt_sql As String = ""

        If new_event_cd.Text & "" <> "" Then
            v_event_cd = new_event_cd.Text
        Else
            If event_drp_list.SelectedIndex < 1 Then
                Throw New Exception("Error - Event code must be selected first")
            Else
                v_event_cd = event_drp_list.SelectedItem.Value.ToString()
            End If
        End If

        If e.NewValues("ITM_STAT") & "" = "" Then
            v_stat = " null,"
        Else
            v_stat = " '" & UCase(e.NewValues("ITM_CD").ToString()) & "',"
        End If

        If e.NewValues("QTY") & "" = "" Or isEmpty(e.NewValues("QTY")) Then
            v_qty = " null "
        ElseIf e.NewValues("QTY") = 0 Then
            v_qty = " null "
        Else
            v_qty = e.NewValues("QTY").ToString().ToUpper
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into credevt_sku_dtl (event_cd,itm_cd,credit_amt,stat_cd,qty,addon_dt) "
        sql = sql & " values ( "
        sql = sql & "'" + UCase(v_event_cd) + "',"
        sql = sql & "'" + UCase(e.NewValues("ITM_CD").ToString()) + "',"
        sql = sql & e.NewValues("AMT").ToString().ToUpper + ","
        sql = sql & v_stat
        sql = sql & v_qty
        sql = sql & "  , to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') )"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Error - Duplicate Item")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        ASPxPopupCREDSKU1.ShowOnPageLoad = False
        populate_items()
        GridView1.DataBind()
        e.Cancel = True
        GridView1.CancelEdit()
        ASPxPopupCREDSKU1.ShowOnPageLoad = True

        lbl_msg2.Text = "Insert is done"
        Throw New Exception("Insert is done")

    End Sub

End Class
