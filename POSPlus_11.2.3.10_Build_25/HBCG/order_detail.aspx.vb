Imports System.Data.OracleClient
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports System.Linq
Imports HBCG_Utils
Imports SalesUtils
Imports InventoryUtils
Imports SessionIntializer

Partial Class order_detail
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theTranBiz As TransportationBiz = New TransportationBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Dim recsDeleted As New ArrayList
    Dim SecurityCache As New System.Collections.Generic.Dictionary(Of String, Boolean)
    Dim Session_String As String
    Dim treatmentFound As Boolean
    Dim GM_Sec As String = ""
    Dim FAB_FOUND As String
    Dim pkg_items As String
    Dim slsp_ds As New DataSet
    Public preventMessageFlag As Boolean '------ this flag is used for error message for prevent SKU's
    Public warrantiePopupURL As String '------- it will hold the url and append flag to it.
    Dim rowid As String
    Dim resClicked As Boolean = False
    Dim sbError As StringBuilder = New StringBuilder()
    Public Class SoLnGrid
        ' NOTE that the SOM SoLnGrid and this SoLnGrid are NOT the same

        Public Const ITM_CD As Integer = 0
        Public Const VE_CD As Integer = 1
        Public Const VSN_COL As Integer = 2 ' includes image, vendor, serial number, po, ist, warr, leave-in_carton
        Public Const SKU_DET As Integer = 3
        ' 5, 6, 7, 8 are only enabled if INV type 
        Public Const INV_LOOKUP As Integer = 4
        Public Const STORE_CD As Integer = 5
        Public Const LOC_CD As Integer = 6
        Public Const FAB_CHKBOX As Integer = 7  ' ref'd as  ROW_ID ???
        Public Const WARR_CHKBOX As Integer = 8
        Public Const TW_CHKBOX As Integer = 9
        Public Const ROW_ID As Integer = 10
        Public Const UNIT_PRC As Integer = 11
        Public Const RELN_PRC As Integer = 12
        Public Const QTY_TXTBOX As Integer = 13
        Public Const DEL_CHKBOX As Integer = 14 ' and add button
        Public Const ITM_TP_CD As Integer = 15
        Public Const TREATABLE As Integer = 16
        Public Const TREATED As Integer = 17
        Public Const TW As Integer = 18
        Public Const NEW_SPEC_ORD As Integer = 19 ' only Y if created new this session
        Public Const PKG_LN As Integer = 20       ' pkg SKU lineNum/Row_id filled in only on components
        Public Const TAX_BASIS As Integer = 21 ' called taxable amt, this is supposed to be tax_basis or is this retail price ???, unit or extended
        Public Const LV_IN_CARTON As Integer = 22  ' the flag from VSN_COL, not the check-box
        Public Const SLSP1 As Integer = 23
        Public Const SLSP2 As Integer = 24
        Public Const SLSP_PCT1 As Integer = 25
        Public Const SLSP_PCT2 As Integer = 26
        Public Const BULK_TP_ITM As Integer = 27
        Public Const SER_TP As Integer = 28
        Public Const TAX_AMT As Integer = 29
        Public Const WARR_ITM_LINK As Integer = 30
        Public Const WARR_ROW_LINK As Integer = 31
        Public Const WARRANTABLE As Integer = 32
        Public Const INVENTORY As Integer = 33
        Public Const QTY As Integer = 34
        Public Const AVAIL_DT As Integer = 35
        Public Const RES_ID As Integer = 36

    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ConfigurationManager.AppSettings("sku_generation").ToString & "" <> "" Then
            btn_sku_gen.Visible = True
        End If
        If Not String.IsNullOrEmpty(Session("hidUpdLnNos")) Then
            hidUpdLnNos.Value = Session("hidUpdLnNos")
        End If

        If Request.Form("__EVENTTARGET") = "SummaryPriceChangeApproval" Then
            PromptForSummaryPriceMgrOverride()
        End If

        If Not IsNothing(Session("isCommited")) Then
            IntializeSession()
        End If

        If Not IsPostBack AndAlso (Request("referrer") Is Nothing Or Request("referrer") <> "CC") AndAlso
            Not (SessVar.ordTpCd = AppConstants.Order.TYPE_MCR OrElse SessVar.ordTpCd = AppConstants.Order.TYPE_MDB) Then
            ' TODO Currently getting zone code based on above conditions (called from Cash and Carry mode) but then calls to other routines have diff and may be diff as
            '      (IsNothing(Session("cash_carry")) OrElse Session("cash_carry") = "FALSE") - probably sb consistent
            GetZoneCode()
        End If

        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTax")) OrElse ViewState("CanModifyTax").ToString = String.Empty Then
            ViewState("CanModifyTax") = CheckSecurity(SecurityUtils.OVERRIDE_TAXCD_ENTRY, Session("EMP_CD"))
        End If

        Gridview1.Visible = True

        If Request("warn") = "Y" And Session("PD") <> "P" Then
            lbl_warnings.Visible = True
            lbl_warnings.Text = lbl_warnings.Text & "All items on the sale are Take-With.  Your order was changed to a pickup, and the delivery charges were removed." & vbCrLf
        End If
        ' if qty was reset for outlet 
        If (Not Request("warning") Is Nothing) AndAlso HBCG_Utils.warning.qtyChangeWarning = Request("warning") Then
            lbl_warnings.Visible = True
            lbl_warnings.Text = lbl_warnings.Text & "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1." & vbCrLf
        End If
        If new_special.Text <> "FALSE" Then
            If Request("special_added") = "TRUE" Then
                new_special.Text = "TRUE"
            Else
                new_special.Text = "FALSE"
            End If
        End If

        ' if regular sale; request("lead") = true is a quote/relationship/lead
        If Request("LEAD") <> "TRUE" Then

            If (Request("ser_num") & "" <> "" OrElse Request("out_id") & "" <> "") Then
                Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Try
                    conn.Open()
                    cmdDeleteItems.Connection = conn

                    'when outlet ID is captured, the serial number may also be present
                    If (Request("out_id") & "" <> "") Then
                        cmdDeleteItems.CommandText = UCase("update temp_itm set out_id='" & Request("out_id") & "', serial_num='" & Request("ser_num") & "', comm_cd='" & Request("out_comm_cd") & "', spiff=" & Request("spiff") & " where row_id=" & Request("row_id"))
                    Else
                        cmdDeleteItems.CommandText = UCase("update temp_itm set serial_num='" & Request("ser_num") & "' where row_id=" & Request("row_id"))
                    End If
                    cmdDeleteItems.ExecuteNonQuery()
                    conn.Close()
                    Response.Redirect("order_detail.aspx")
                Catch
                    conn.Close()
                End Try
            End If
        End If

        If Not (IsPostBack) Then

            If (IsNothing(Session("TAX_CD")) OrElse Session("TAX_CD").ToString.isEmpty) AndAlso String.IsNullOrEmpty(Session("tet_cd")) Then

                ' get and use tax code based on store_cd
                Dim zipCd As String = ""
                If Session("PD") = "P" Then

                    If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                        Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                        zipCd = store.addr.postalCd.ToUpper
                    End If
                ElseIf Session("CUST_CD") + "" <> "" Then
                    ' per DSA - this zipCd used for taxing which is based on customer zip for a delivery, not any other zip code setting even if ARS enabled
                    zipCd = theCustomerBiz.GetCustomerZip(Session.SessionID.ToString, Session("CUST_CD"))
                End If
                Dim taxCd As String = TaxUtils.Get_Tax_CD(Session("pd_store_cd"), zipCd, "N", Session("store_cd"), Session("tran_dt"), Session("PD"))  ' Assuming not take-wih 
                Dim multCds As Integer = InStr(taxCd, ";") ' just take the first one - need something now, can recalc later
                If multCds > 0 Then
                    taxCd = taxCd.Substring(0, multCds - 1)
                End If
                Session("TAX_CD") = taxCd
                '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                Session("MANUAL_TAX_CD") = Nothing

            End If

            If SessVar.skuListToAdd.isNotEmpty AndAlso SessVar.skuListToAdd <> "NONEENTERED" Then

                SetupForDiscReCalc()       ' page load
                ' insert new records into table, if they exist
                Dim recsAdded As Integer = insert_records()

                'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
                'if will reapply it, if ANY of the lines added qualify for the discount.
                If SessVar.LastLnSeqNum.isNotEmpty() AndAlso recsAdded > 0 Then

                    DoDiscReCalcNowOrLater()
                End If
            End If

            'lucy start
            ' If Session("cash_carry") = "TRUE" And Session("lucy_been_to_rf_scan") = "Y" Then   'dave does not want to go rf_scan first time

            ' Daniela  add postback check moved block
            If Session("cash_carry") = "TRUE" Then
                lucy_lbl.Visible = True
                ' ElseIf Session("cash_carry") = "TRUE" And Session("lucy_been_to_rf_scan") = "N" Then  'lucy ,first time from take with menu, dave does not want to go rf_scan first time
                ' Response.Redirect("rf_scan.aspx")
            Else
                lucy_lbl.Visible = False

            End If

            ' If Len(Trim(txt_SKU.Text)) < 8 And Session("cash_carry") = "TRUE" And Len(Session("str_itm_cd")) > 7 Then 'not right for the Brick

            If Session("cash_carry") = "TRUE" And Len(Session("str_itm_cd")) > 0 Then

                If Session("lucy_rf_scan") = "Y" Then

                    txt_SKU.Text = Session("str_itm_cd")

                    lucy_rf_scanned()
                    lucy_Take_With()
                    lucy_ConvertToCashCarrySale()
                End If


            End If  ' lucy end
            ' End take with 

            UpdateLineItemsAvailDate()

            'Populate Datagrid with new records
            bindgrid()

            KeepSessions()

            txt_SKU.Focus()
            If (Not IsNothing(Session("modifieditems")) AndAlso Not String.IsNullOrEmpty(Session("modifieditems"))) Then
                hidUpdLnNos.Value = Session("modifieditems")
                Session("modifieditems") = Nothing
            End If
        End If

        ' if credit to original sale then cannot add SKU's not on orig (may have to allow this eventually, in which case can check on SKU entry if on orig, but then have to do credit check on qty and retail also
        If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

            cmd_add.Enabled = False
            txt_SKU.Enabled = False
            ImageButton1.Enabled = False
            btn_sku_gen.Enabled = False
            cmd_add.Visible = False
            txt_SKU.Visible = False
            ImageButton1.Visible = False
            btn_sku_gen.Visible = False
        End If
        ''this code change is done to retain error message for prevented sale in case of multiple 
        ''SKU are entered in search box.
        ''and for any SKU entered is having warranty  or related SKU then this flag is checked.
        If Not Request("preventMessageFlag") Is Nothing Or preventMessageFlag = True Then
            If Not Session("preventMessageID") Is Nothing Then
                lbl_warnings.Text = Session("preventMessageID").ToString
            End If
        Else
            'do nothing
        End If
        If (Not IsNothing(Session("oracleException"))) Then
            Dim oracleExceptionMessage As String = Convert.ToString(Session("oracleException"))
            lbl_warnings.Text = oracleExceptionMessage
            Session.Remove("oracleException")
        End If

        If Not IsNothing(Session("InvError")) Then
            Dim sbError As StringBuilder = Session("InvError")
            lbl_warnings.Text = sbError.ToString()

            lbl_warnings.Visible = True
            sbError.Length = 0 ' dispose the string builder
            Session("InvError") = Nothing 'Kill the session once the job is complete
        End If

        'mariam-Sept 27,2016 - Price change approve
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            lblErrorMsg.Visible = True
            lblErrorMsg.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            ' DB May 18, 2016 move message in CustomMessages
            'lblErrorMsg.Text = Resources.POSMessages.MSG0058
            lblErrorMsg.Text = Resources.CustomMessages.MSG0118
        Else
            lblErrorMsg.Text = String.Empty
        End If
    End Sub

    Public Function lucy_check_itm(ByRef p_itm_cd As String, ByRef p_user As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isValidItm2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_itm", OracleType.VarChar)).Value = p_itm_cd
        myCMD.Parameters.Add(New OracleParameter("p_empcd", OracleType.VarChar)).Value = p_user
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_y_n").Value) = False Then
                str_y_n = myCMD.Parameters("p_y_n").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally ' Daniela added
            objConnection.Close()

        End Try
        Return str_y_n
    End Function
    Public Function lucy_invent_itm(ByRef p_itm_cd As String) As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()


        sql = "select inventory from itm where itm_cd='" & UCase(p_itm_cd) & "' and inventory='Y' "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                'Daniela french
                'lucy_lbl.Text = "Please clear the sku and click on Add Item to scan a RFID"
                lucy_lbl.Text = Resources.LibResources.Label703
                Return "Y"

            Else

                Return "N"
                lucy_lbl.Text = "Please Click on Add Item to scan a RFID"
            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try


    End Function
    Public Sub lucy_get_row_id(ByRef p_itm_cd As String, ByRef p_row_id As String)


        Dim sql As String
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        'Open Connection 
        conn2.Open()


        sql = "select max(row_id) as idOfNewRow from temp_itm where session_id='" & Session.SessionID.ToString & "' and itm_cd='" & p_itm_cd & "'"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then

                p_row_id = CInt(MyDataReader2("idOfNewRow").ToString)
            End If



            'Close Connection 
            MyDataReader2.Close()
            conn2.Close() ' Daniela added
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try
    End Sub
    Public Sub lucy_get_h_m(ByRef p_h As String, ByRef p_m As String)


        Dim sql As String
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        'Open Connection 
        conn2.Open()


        sql = "select to_char(sysdate,'HH24') as hh,to_char(sysdate,'MI') as mi from dual"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then

                p_h = CInt(MyDataReader2("hh").ToString)
                p_m = CInt(MyDataReader2("mi").ToString)
            End If



            'Close Connection 
            MyDataReader2.Close()
            conn2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try
    End Sub


    Public Sub insert_lucy_pos_rf(ByRef Session_String As String, ByRef loc_cd As String, ByRef sku As String, ByRef store_cd As String, ByRef str_rfid As String, ByRef p_row_id As String) 'lucy
        Dim sql As String
        Dim connString As String
        Dim thistransaction As OracleTransaction
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim str_today As String = FormatDateTime(Today.Date, 2)
        Dim str_year As String = Right(str_today, 4)


        Dim dt_today As Date
        If Not isEmpty(str_rfid) Then
            ' Daniela March 24 spiff issues
            'Session("store_cd") = store_cd
            Dim str_disposition As String = Session("disposition")
            Dim str_nas_out_id As String
            If str_disposition = "OUT" Then
                str_nas_out_id = Session("nas_out_id")
            Else
                str_nas_out_id = ""
            End If
            dt_today = FormatDateTime(Today.Date, 2)
            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim str_h As String
            Dim str_m As String

            lucy_get_h_m(str_h, str_m)

            ' sql = "INSERT INTO lucy_pos_rf_id (SESSION_ID,LOC_CD,ITM_CD,STORE_CD,RF_ID_CD)  values ('" & Session.SessionID.ToString.Trim & "','" & loc_cd & "','" & sku & "','" & store_cd & "','" & str_rfid & "')"
            sql = "INSERT INTO lucy_pos_rf_id (SESSION_ID,row_id,LOC_CD,ITM_CD,STORE_CD,enter_dt,RF_ID_CD,nas_out_id,enter_hh,enter_mi,disposition )"
            ' sql = sql & " values ('" & Session.SessionID.ToString.Trim & "','" & loc_cd & "','" & sku & "','" & store_cd & "'," & _
            sql = sql & " values ('" & Session.SessionID.ToString.Trim & "','" & p_row_id & "','" & loc_cd & "','" & sku & "','" & store_cd & "'," &
                 "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & str_rfid & "','" & str_nas_out_id & "','" & str_h & "','" & str_m & "','" & str_disposition & "')"

            'sql = sql & "  values ('" & Session.SessionID.ToString.Trim & "','" & loc_cd & "','" & sku & "','" & store_cd & "','" & str_rfid & "')"

            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            objConnection.Close()  '01-dec-14 add
            '  Dim str_done As String = lucy_check_create_dt(str_rfid)

            '  "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & ")"
        End If
    End Sub

    Protected Sub lucy_rf_scanned()
        'lucy
        If txt_SKU.Text & "" <> "" Then

            Session("itemid") = Session("itemid") & "," & UCase(txt_SKU.Text)

            'Add the SKU
            insert_records()

            'Populate Datagrid with new records
            ' Daniela not needed here
            'bindgrid()

            txt_SKU.Text = ""
        End If
    End Sub


    Private Sub lucy_DoStoreLocationUpdate(ByRef dgItem As DataGridItem)
        'lucy copied from DoStoreLocationUpdate
        'Outlet validations are failing due to case sensitivity.  Forcing upper case as soon as value is entered.
        ' theTextBox.Text = UCase(theTextBox.Text).Trim 'lucy

        Dim isStoreUpdate As Boolean = False 'lucy
        ' Dim dgItem As DataGridItem = CType(theTextBox.NamingContainer, DataGridItem)  'lucy



        'Outlet validations are failing due to case sensitivity.  Forcing upper case as soon as value is entered.
        ' theTextBox.Text = UCase(theTextBox.Text).Trim
        ' Dim dgItem As DataGridItem = CType(theTextBox.NamingContainer, DataGridItem)
        Dim tBoxStoreCd As TextBox = CType(dgItem.FindControl("store_cd"), TextBox)
        Dim tboxLocCd As TextBox = CType(dgItem.FindControl("loc_cd"), TextBox)
        Dim tboxQty As TextBox = CType(dgItem.FindControl("qty_txtbox"), TextBox)
        Dim isOutletLoc As Boolean = theInvBiz.IsOutletLocation(tBoxStoreCd.Text, tboxLocCd.Text)

        Try
            '-- Regardless of the change to store or location, soft reservations must be removed
            '-- Has to retrieve the original Store/Location to properly remove the reservations --
            Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim,
                                                                  dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                                                  temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)
            Dim resId As String = If(Not IsDBNull(tmpItmRow("res_id")), tmpItmRow("res_id"), "")
            If (resId.isNotEmpty()) Then
                Dim request As UnResRequestDtc = GetRequestForUnreserve(resId,
                                                                        tmpItmRow("itm_cd"),
                                                                        tmpItmRow("store_cd"),
                                                                        tmpItmRow("loc_cd"),
                                                                        tmpItmRow("qty"))
                theInvBiz.RemoveSoftRes(request)
            End If

            '-- When changing or clearing the STORE, the location gets cleared by default (MM-6326)
            If (isStoreUpdate) Then tboxLocCd.Text = String.Empty

            '-- When STORE/LOCATION are present, a prompt for serial number may occur.
            ' Done at this point, because if no serial #s exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            ' IMPORTANT: If the SKU is serialized, but an Outlet location was entered, the 
            '            requirement is to prompt for Outlet ID and not the Serial#
            If (isOutletLoc = False AndAlso
                ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" AndAlso
                IsSerialType(dgItem.Cells(SoLnGrid.SER_TP).Text) AndAlso
                tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If (Not PromptForSerialNumber(dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                              dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                              tBoxStoreCd.Text,
                                              tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '-- When STORE/LOCATION are present, a prompt for Outlet ID may occur.
            ' Done at this point, because if no outlet id exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            If (isOutletLoc AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If IsNumeric(tboxQty.Text) AndAlso tboxQty.Text > 1 Then
                    lbl_warnings.Text = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    lbl_out_err.Text = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    tboxQty.Text = "1"
                ElseIf Not IsNumeric(tboxQty.Text) Then
                    lbl_warnings.Text = "Quantity must be a valid number. Quantity set to 1."
                    lbl_out_err.Text = "Quantity must be a valid number. Quantity set to 1."
                    tboxQty.Text = "1"
                End If

                If (Not PromptForOutlet(dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                        dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                        tBoxStoreCd.Text,
                                        tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '==================================  PROCESS RESERVATIONS =========================================
            '-- When STORE/LOCATION are present (if not cleared during Serial #s OR Outlet ID processing above)
            'attempts to do a DIRECTED soft reservation (only allowed for NON outlet location)
            ' ------------------------------  OR  -------------------------------------------------------------
            '-- When the STORE field has an "R" attempts to do an AUTO soft reservation
            Dim newLinesCreated As Boolean = False
            Dim reservationsMade As Boolean = False
            Dim resRequest As ResRequestDtc = Nothing

            If (isOutletLoc = False AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                resRequest = GetRequestForDirectedRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                      tboxQty.Text,
                                                      tBoxStoreCd.Text,
                                                      tboxLocCd.Text,
                                                      Session("PD"))

            ElseIf (isStoreUpdate AndAlso tBoxStoreCd.Text.isNotEmpty AndAlso "R".Equals(tBoxStoreCd.Text.Trim)) Then

                ' if priority fill is by zone, then the api will error w/o a zone code 
                If SessVar.zoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone Then
                    lbl_warnings.Text = Resources.POSErrors.ERR0004
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                Else
                    resRequest = GetRequestForAutoRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                      tboxQty.Text,
                                                      Session("PD"),
                                                      Session("PD_STORE_CD"),
                                                      SessVar.zoneCd)
                End If
            End If

            If (resRequest IsNot Nothing) Then  'reservations need to be processed
                resRequest.lineNum = CInt(dgItem.Cells(SoLnGrid.ROW_ID).Text)

                ' Dim resResponse As ResResponseDtc = ReserveInventory(resRequest)   '01-dec-14

                Dim resResponse As ResResponseDtc = theSalesBiz.ReserveLine(Session.SessionID,
                                                                           SessVar.discCd,
                                                                           resRequest)



                If (resResponse.resId.Trim.isNotEmpty()) Then
                    reservationsMade = True
                    newLinesCreated = (resResponse.reservations.Rows.Count > 1)
                    dgItem.Cells(SoLnGrid.RES_ID).Text = resResponse.resId  'updates this value, otherwise since binding is skipped, doesn't get set
                    Dim invRow As DataRow = resResponse.reservations.Rows(0)
                    tBoxStoreCd.Text = UCase(invRow("STORE_CD").ToString)
                    tboxLocCd.Text = UCase(invRow("LOC_CD").ToString)
                    tboxQty.Text = invRow("QTY").ToString
                Else
                    'if here means NO reservations were made, therefore clears the STORE/LOCATION values
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            'when reservations are processed, the current line gets updated as part of that process, no need to repeat
            If (reservationsMade) Then
                If (newLinesCreated) Then bindgrid() 'to display the newly added lines
            Else
                ' when STORE is cleared, the location/serial-number/outletid will be cleared by default
                theSalesBiz.UpdateTempItmTable(dgItem.Cells(SoLnGrid.ROW_ID).Text, tboxQty.Text,
                                               tBoxStoreCd.Text, tboxLocCd.Text, String.Empty,
                                               String.Empty, String.Empty,
                                               String.Empty, String.Empty)

                'HIDES the Serial# & Outlet components, since these values are empty if landing here
                Dim lbl_sernum As Label = DirectCast(dgItem.FindControl("lbl_sernum"), Label)
                Dim lbl_sernumval As Label = DirectCast(dgItem.FindControl("lbl_sernumval"), Label)
                Dim lbl_outletid As Label = DirectCast(dgItem.FindControl("lbl_outletid"), Label)
                Dim lbl_outletval As Label = DirectCast(dgItem.FindControl("lbl_outletval"), Label)
                lbl_sernum.Visible = False
                lbl_sernumval.Visible = False
                lbl_outletid.Visible = False
                lbl_outletval.Visible = False

                If (isStoreUpdate AndAlso tBoxStoreCd.Text.isEmpty()) Then bindgrid() 'to update inv count if unreserve
            End If

            If (tBoxStoreCd.Text.isEmpty) Then
                tBoxStoreCd.Focus()
            ElseIf (tboxLocCd.Text.isEmpty) Then
                tboxLocCd.Focus()
            Else
                Dim tboxRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)
                tboxRetPrc.Focus()
            End If
        Catch oracleException As OracleException
            Select Case oracleException.Code
                Case 20101
                    lbl_warnings.Text = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Protected Sub lucy_ConvertToCashCarrySale()
        'copied from ConvertToCashCarrySale
        Session("PD") = "P"
        Session("DEL_DT") = FormatDateTime(Today.Date, DateFormat.ShortDate)
        Session("DEL_CHG") = "0"
        Session("CASH_CARRY") = "TRUE"
        calculate_total(Page.Master)
        lbl_warnings.Visible = True
        'lbl_warnings.Text = lbl_warnings.Text & " Your order was changed to cash and carry." & vbCrLf
        lbl_warnings.Text = lbl_warnings.Text & " " & Resources.LibResources.Label682 & " " & vbCrLf

        ASPxPopupControl11.ShowOnPageLoad = False

        Dim sql As String = " UPDATE TEMP_ITM SET TAKE_WITH='N' WHERE session_id= :theSessionId "
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":theSessionId", OracleType.VarChar)
            dbCommand.Parameters(":theSessionId").Value = Session.SessionID.ToString.Trim
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub

    Protected Sub lucy_Take_With()

        'lucy copied from Take_With_CheckedChanged

        ' Dim chkBoxTW As CheckBox = CType(sender, CheckBox)   'lucy comment
        ' Dim dgItem As DataGridItem = CType(chkBoxTW.NamingContainer, DataGridItem)  'lucy
        Dim dgItem As DataGridItem  'lucy
        Dim SoLnGItm As DataGridItem  'lucy

        '  Dim lblAvail As Label = DirectCast(dgItem.FindControl("lblAvailDt"), Label) 'lucy
        '  Dim lnRowId As String = dgItem.Cells(SoLnGrid.ROW_ID).Text  'lucy

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String = ""

        Session("str_itm_cd") = "" 'lucy add in order not to have infinite loop

        Dim p_no_rec As Integer = Gridview1.Items.Count - 1  'lucy add

        For intRow = 0 To p_no_rec 'lucy add
            If intRow = p_no_rec Then ' last record
                SoLnGItm = Gridview1.Items(intRow)   'lucy add
                dgItem = Gridview1.Items(intRow)      'lucy add
                Dim lnRowId As String = dgItem.Cells(SoLnGrid.ROW_ID).Text

                lucy_DoStoreLocationUpdate(dgItem)



                Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objsql2 As OracleCommand 'lucy

                conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn2.Open()





                Session("lucy_has_take_with") = "Y"
                If Session("lucy_rf_scan") = "Y" Then
                    If Session("disposition") = "OUT" Then
                        sql = "UPDATE TEMP_ITM SET STORE_CD='" & Session("str_store_cd") & "', out_id='" & Session("nas_out_id") & "', LOC_CD='" & Session("str_loc_cd") & "'"
                    Else
                        sql = "UPDATE TEMP_ITM SET STORE_CD='" & Session("str_store_cd") & "',  LOC_CD='" & Session("str_loc_cd") & "'"
                    End If
                    sql = sql & "WHERE ROW_ID = " & dgItem.Cells(SoLnGrid.ROW_ID).Text
                    ' & " AND STORE_CD IS NULL"  'lucy comment
                    objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objsql2.ExecuteNonQuery()
                End If

                ' ==== DOES THE COMMON PROCESSING FOR THE CHECKBOX BEING CLICKED ====
                ' hides the available date label (it may show it below)
                '  lblAvail.Visible = False  'lucy

                ' updates the TAKE-WITH value on the treatment lines accordingly
                Try
                    dbConnection.Open()
                    sql = "SELECT treated_by FROM temp_itm WHERE ROW_ID = :lnRowId "
                    dbCommand.CommandText = sql
                    dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
                    dbCommand.Parameters(":lnRowId").Value = lnRowId
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    dbCommand.Parameters.Clear()
                    If (dbReader.Read() AndAlso IsNumeric(dbReader.Item("TREATED_BY").ToString)) Then
                        sql = "UPDATE temp_itm SET TAKE_WITH = :value WHERE row_id = :treatByRowId "
                        dbCommand.Parameters.Add(":treatByRowId", OracleType.VarChar)
                        dbCommand.Parameters.Add(":value", OracleType.VarChar)
                        dbCommand.Parameters(":treatByRowId").Value = dbReader.Item("TREATED_BY").ToString
                        ' dbCommand.Parameters(":value").Value = IIf(chkBoxTW.Checked, "Y", "")  'lucy
                        dbCommand.Parameters(":value").Value = "Y"  'lucy
                        dbCommand.ExecuteNonQuery()
                        dbCommand.Parameters.Clear()
                    End If
                Catch ex As Exception
                    dbConnection.Close()
                    Throw
                End Try

                ' ==== DOES THE PROCESSING FOR THE CHECKBOX BEING CHECKED / UNCHECKED ====
                '  If chkBoxTW.Checked = True Then  'lucy

                Try
                    sql = "UPDATE temp_itm SET TAKE_WITH = 'Y', AVAIL_DT = '' WHERE ROW_ID = :lnRowId "
                    dbCommand.CommandText = sql
                    dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
                    dbCommand.Parameters(":lnRowId").Value = lnRowId
                    dbCommand.ExecuteNonQuery()
                    dbCommand.Parameters.Clear()

                    'If there are package SKUs, we need to break out the pricing
                    If IsNumeric(dgItem.Cells(SoLnGrid.PKG_LN).Text) Then
                        sql = "UPDATE TEMP_ITM SET RET_PRC=TAXABLE_AMT WHERE SESSION_ID='" & Session.SessionID.ToString() & "' AND PACKAGE_PARENT=" & dgItem.Cells(SoLnGrid.PKG_LN).Text
                        dbCommand.CommandText = sql
                        dbCommand.ExecuteNonQuery()

                        sql = "UPDATE TEMP_ITM SET RET_PRC=0.00 WHERE ITM_TP_CD='PKG' AND SESSION_ID='" & Session.SessionID.ToString() & "' AND ROW_ID=" & dgItem.Cells(SoLnGrid.PKG_LN).Text
                        dbCommand.CommandText = sql
                        dbCommand.ExecuteNonQuery()
                    End If

                    Set_Take_With_Tax()

                    ' --- Checks if ALL lines are now TAKE-WITH to prompt the user
                    Dim totalLinesCount As Double = 0
                    Dim twLinesCount As Double = 0
                    sql = "SELECT count(ROW_ID) As TOTAL_COUNT, SUM(QTY*RET_PRC) As T_SUM FROM TEMP_ITM where TAKE_WITH='Y' AND ITM_TP_CD='INV' AND session_id = :sessId"
                    dbCommand.CommandText = sql
                    dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
                    dbCommand.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If (dbReader.Read()) Then
                        If IsNumeric(dbReader.Item("TOTAL_COUNT").ToString) Then twLinesCount = CDbl(dbReader.Item("TOTAL_COUNT").ToString)
                        If IsNumeric(dbReader.Item("T_SUM").ToString) Then Session("Take_Deposit") = CDbl(dbReader.Item("T_SUM").ToString)
                    End If

                    If twLinesCount > 0 Then
                        sql = "SELECT count(ROW_ID) As TOTAL_COUNT FROM temp_itm WHERE itm_tp_cd = 'INV' AND session_id= :sessId"
                        dbCommand.CommandText = sql  'no need to set the parameters are set above
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If (dbReader.Read() AndAlso IsNumeric(dbReader.Item("TOTAL_COUNT").ToString)) Then
                            totalLinesCount = CDbl(dbReader.Item("TOTAL_COUNT").ToString)
                        End If

                        If totalLinesCount - twLinesCount = 0 Then
                            '  ASPxPopupControl11.ShowOnPageLoad = True   'lucy
                            lbl_warnings.Text = "All items on the sale are Take-With."
                        End If
                    End If

                    If InStr(lbl_warnings.Text, "All items on the sale are Take-With.") > 0 Then
                        'Do Nothing, a pop-up box will appear
                    Else
                        Response.Redirect(IIf(Request("LEAD") = "TRUE", "order_detail.aspx?LEAD=TRUE", "order_detail.aspx"))
                    End If
                    dbReader.Close()

                Catch ex As Exception
                    Throw
                Finally
                    dbConnection.Close()
                End Try

                'lucy deleted the else part because it is not take with

                ' End If
            End If  'lucy
        Next 'lucy
        Session("lucy_rf_scan") = "N"
    End Sub

    Public Function lucy_check_create_dt(ByRef p_rf_id_cd As String)
        'lucy
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_done As String
        Dim p_done As String
        Dim str_rf = p_rf_id_cd
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()



        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_rf.check_rf_create_dt"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_done", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.Parameters.Add("p_rf_id_cd", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add(New OracleParameter("p_rf_id_cd", OracleType.VarChar)).Value = str_rf



        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_done = myCMD.Parameters("p_done").Value
            '  Return str_done

        Catch x
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString


        End Try
        objConnection.Close()

    End Function

    Protected Sub setWarningLbl(ByVal warning As String)

        lbl_warnings.Text = warning

    End Sub

    ''' <summary>
    ''' Called from the Store and Location text box listeners.  Combined since these events
    ''' require most of the same processing
    ''' </summary>
    ''' <param name="theTextBox">the field that triggered the event</param>
    ''' <param name="isStoreUpdate">true=indicates store changed, false=indicates location changed</param>
    Private Sub DoStoreLocationUpdate(ByVal theTextBox As TextBox, ByVal isStoreUpdate As Boolean)

        lbl_warnings.Text = String.Empty
        'Outlet validations are failing due to case sensitivity.  Forcing upper case as soon as value is entered.
        theTextBox.Text = UCase(theTextBox.Text).Trim
        Dim dgItem As DataGridItem = CType(theTextBox.NamingContainer, DataGridItem)
        Dim tBoxStoreCd As TextBox = CType(dgItem.FindControl("store_cd"), TextBox)
        Dim tboxLocCd As TextBox = CType(dgItem.FindControl("loc_cd"), TextBox)
        Dim tboxQty As TextBox = CType(dgItem.FindControl("qty_txtbox"), TextBox)
        Dim isOutletLoc As Boolean = False
        If (((tBoxStoreCd.Text.isNotEmpty) AndAlso (tboxLocCd.Text.isNotEmpty))) Then
            isOutletLoc = theInvBiz.IsOutletLocation(tBoxStoreCd.Text, tboxLocCd.Text)
        End If

        'DB Feb 1 2018 showroom changes
        Dim isShowroomLoc As Boolean = False
        If (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
            If (Not isOutletLoc And ((tBoxStoreCd.Text.isNotEmpty) AndAlso (tboxLocCd.Text.isNotEmpty))) Then
                isShowroomLoc = LeonsBiz.IsShowroomLocation(tBoxStoreCd.Text, tboxLocCd.Text)
            End If
        End If

        Try
            '-- Regardless of the change to store or location, soft reservations must be removed
            '-- Has to retrieve the original Store/Location to properly remove the reservations --
            Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim,
                                                                  dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                                                  temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)
            Dim resId As String = If(Not IsDBNull(tmpItmRow("res_id")), tmpItmRow("res_id"), "")
            If (resId.isNotEmpty()) Then
                Dim request As UnResRequestDtc = GetRequestForUnreserve(resId,
                                                                        tmpItmRow("itm_cd"),
                                                                        tmpItmRow("store_cd"),
                                                                        tmpItmRow("loc_cd"),
                                                                        tmpItmRow("qty"))
                theInvBiz.RemoveSoftRes(request)
            End If

            '-- When changing or clearing the STORE, the location gets cleared by default (MM-6326)
            If (isStoreUpdate) Then tboxLocCd.Text = String.Empty

            '-- When STORE/LOCATION are present, a prompt for serial number may occur.
            ' Done at this point, because if no serial #s exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            ' IMPORTANT: If the SKU is serialized, but an Outlet location was entered, the 
            '            requirement is to prompt for Outlet ID and not the Serial#
            If (isOutletLoc = False AndAlso isShowroomLoc = False AndAlso
                ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" AndAlso
                IsSerialType(dgItem.Cells(SoLnGrid.SER_TP).Text) AndAlso
                tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If (Not PromptForSerialNumber(dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                              dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                              tBoxStoreCd.Text,
                                              tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '-- When STORE/LOCATION are present, a prompt for Outlet ID may occur.
            ' Done at this point, because if no outlet id exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            If (isOutletLoc AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If IsNumeric(tboxQty.Text) AndAlso tboxQty.Text > 1 Then
                    lbl_warnings.Text = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    lbl_out_err.Text = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    tboxQty.Text = "1"
                ElseIf Not IsNumeric(tboxQty.Text) Then
                    lbl_warnings.Text = "Quantity must be a valid number. Quantity set to 1."
                    lbl_out_err.Text = "Quantity must be a valid number. Quantity set to 1."
                    tboxQty.Text = "1"
                End If

                If (Not PromptForOutlet(dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                        dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                        tBoxStoreCd.Text,
                                        tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '==================================  PROCESS RESERVATIONS =========================================
            '-- When STORE/LOCATION are present (if not cleared during Serial #s OR Outlet ID processing above)
            'attempts to do a DIRECTED soft reservation (only allowed for NON outlet location)
            ' ------------------------------  OR  -------------------------------------------------------------
            '-- When the STORE field has an "R" attempts to do an AUTO soft reservation
            Dim newLinesCreated As Boolean = False
            Dim reservationsMade As Boolean = False
            Dim resRequest As ResRequestDtc = Nothing

            If (isOutletLoc = False AndAlso isShowroomLoc = False AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                resRequest = GetRequestForDirectedRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                      tboxQty.Text,
                                                      tBoxStoreCd.Text,
                                                      tboxLocCd.Text,
                                                      Session("PD"))

            ElseIf (isStoreUpdate AndAlso tBoxStoreCd.Text.isNotEmpty AndAlso "R".Equals(tBoxStoreCd.Text.Trim)) Then

                ' if priority fill is by zone, then the api will error w/o a zone code 
                If SessVar.zoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone Then
                    lbl_warnings.Text = Resources.POSErrors.ERR0004
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                Else
                    resRequest = GetRequestForAutoRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                      tboxQty.Text,
                                                      Session("PD"),
                                                      Session("PD_STORE_CD"),
                                                      SessVar.zoneCd)
                End If
            End If

            If (resRequest IsNot Nothing) Then  'reservations need to be processed
                resRequest.lineNum = CInt(dgItem.Cells(SoLnGrid.ROW_ID).Text)

                Dim resResponse As ResResponseDtc = theSalesBiz.ReserveLine(Session.SessionID,
                                                                            SessVar.discCd,
                                                                            resRequest)
                If (resResponse.resId.Trim.isNotEmpty()) Then
                    reservationsMade = True
                    newLinesCreated = (resResponse.reservations.Rows.Count > 1)
                    dgItem.Cells(SoLnGrid.RES_ID).Text = resResponse.resId  'updates this value, otherwise since binding is skipped, doesn't get set
                    Dim invRow As DataRow = resResponse.reservations.Rows(0)
                    tBoxStoreCd.Text = UCase(invRow("STORE_CD").ToString)
                    tboxLocCd.Text = UCase(invRow("LOC_CD").ToString)
                    tboxQty.Text = invRow("QTY").ToString
                Else
                    'if here means NO reservations were made, therefore clears the STORE/LOCATION values
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            'when reservations take place, the current line gets updated as part of it, no need to repeat
            If (reservationsMade) Then
                If (newLinesCreated) Then bindgrid() 'to display the newly added lines
            Else
                ' when STORE is cleared, the location/serial-number/outletid will be cleared by default
                theSalesBiz.UpdateTempItmTable(dgItem.Cells(SoLnGrid.ROW_ID).Text, tboxQty.Text,
                                               tBoxStoreCd.Text, tboxLocCd.Text, String.Empty,
                                               String.Empty, String.Empty,
                                               String.Empty, String.Empty)

                'HIDES the Serial# & Outlet components, since these values are empty if landing here
                Dim lbl_sernum As Label = DirectCast(dgItem.FindControl("lbl_sernum"), Label)
                Dim lbl_sernumval As Label = DirectCast(dgItem.FindControl("lbl_sernumval"), Label)
                Dim lbl_outletid As Label = DirectCast(dgItem.FindControl("lbl_outletid"), Label)
                Dim lbl_outletval As Label = DirectCast(dgItem.FindControl("lbl_outletval"), Label)
                lbl_sernum.Visible = False
                lbl_sernumval.Visible = False
                lbl_outletid.Visible = False
                lbl_outletval.Visible = False

                If (isStoreUpdate AndAlso tBoxStoreCd.Text.isEmpty()) Then bindgrid() 'to update inv count if unreserve
            End If

            If (tBoxStoreCd.Text.isEmpty) Then
                tBoxStoreCd.Focus()
            ElseIf (tboxLocCd.Text.isEmpty) Then
                tboxLocCd.Focus()
            Else
                Dim tboxRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)
                tboxRetPrc.Focus()
            End If

            'Showroom feb 2 2018
            'Update Available date for showroom items
            If (isShowroomLoc) Then
                'UpdateShowroomItemsAvailDate()
                'Populate Datagrid with new records
                bindgrid()
            End If

        Catch oracleException As OracleException
            Select Case oracleException.Code
                Case 20101
                    lbl_warnings.Text = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Retrieves the Serial numbers and binds the results to the grid component displayed in the Serial Numbers popup
    ''' </summary>
    ''' <param name="selRowId">the row_id value coming from the TEMP_ITEM table, indicates the row being processed</param>
    ''' <param name="itemCd">the item code being processed</param>
    ''' <param name="storeCd">store code provided by the user to be used for this item</param>
    ''' <param name="locCd">location code provided by the user to be used for this item</param>
    ''' <returns>TRUE if serial numbers are found for the store and location entered; FALSE otherwise</returns>
    Private Function PromptForSerialNumber(ByVal selRowId As String,
                                           ByVal itemCd As String,
                                           ByVal storeCd As String,
                                           ByVal locCd As String) As Boolean
        Dim sNumbersFound As Boolean = True

        'makes sure to display only those Serial #s that have not been assigned in the current order
        Dim sNumbersInUse As String = theSalesBiz.GetSerialNumbersInUse(Session.SessionID.ToString)
        Dim sNumbersDataSet As DataSet = theInvBiz.GetSerialNumbersAvailable(itemCd, storeCd, locCd, sNumbersInUse)

        lbl_ser_row_id.Text = selRowId
        lbl_ser_itm_cd.Text = itemCd
        lbl_ser_loc_cd.Text = locCd
        lbl_ser_store_cd.Text = storeCd
        ASPxPopupControl7.ShowOnPageLoad = True
        ASPxGridView1.DataSource = sNumbersDataSet
        ASPxGridView1.DataBind()

        If sNumbersDataSet.Tables(0).Rows.Count < 1 Then
            lbl_ser_err.Text = "No items were found for the selected store and location."
            sNumbersFound = False    'to notify caller that no serial numbers were found
        End If
        Return sNumbersFound
    End Function

    ''' <summary>
    ''' Retrieves the Outlet IDs and binds the results to the grid component displayed in the Outlet Ids popup
    ''' </summary>
    ''' <param name="selRowId">the row_id value coming from the TEMP_ITEM table, indicates the row being processed</param>
    ''' <param name="itemCd">the item code being processed</param>
    ''' <param name="storeCd">store code provided by the user to be used for this item</param>
    ''' <param name="locCd">location code provided by the user to be used for this item</param>
    ''' <returns>TRUE if outlet IDs are found for the store and location entered; FALSE otherwise</returns>
    Private Function PromptForOutlet(ByVal selRowId As String,
                                    ByVal itemCd As String,
                                    ByVal storeCd As String,
                                    ByVal locCd As String) As Boolean

        Dim outIdsFound As Boolean = True

        'makes sure to display only those Outlet Ids that have not been assigned in the current order
        Dim outIdsInUse As String = theSalesBiz.GetOutletIdsInUse(Session.SessionID.ToString)
        Dim outIdsDataSet As DataSet = theInvBiz.GetOutletIdsAvailable(itemCd, storeCd, locCd, outIdsInUse)

        lbl_out_row_id.Text = selRowId
        lbl_out_itm_cd.Text = itemCd
        lbl_out_loc_cd.Text = locCd
        lbl_out_store_cd.Text = storeCd
        If Session("lucy_rf_scan") <> "Y" Then   ' lucy out_id
            ASPxPopupControl12.ShowOnPageLoad = True
        Else
            ASPxPopupControl12.ShowOnPageLoad = False   'lucy 02-Mar-15
        End If
        ASPxGridView2.DataSource = outIdsDataSet
        ASPxGridView2.DataBind()

        If outIdsDataSet.Tables(0).Rows.Count < 1 Then
            lbl_out_err.Text = "No items were found for the selected store and location."
            ASPxPopupControl12.ShowCloseButton = True
            ASPxPopupControl12.CloseAction = DevExpress.Web.ASPxClasses.CloseAction.CloseButton
            outIdsFound = False       'to notify caller no outlet IDs were found
        End If
        Return outIdsFound
    End Function

    ''' <summary>
    ''' Used in the ItemDataBound to avoid hitting the database multiple times for
    ''' every security and every line placed on the order.
    ''' </summary>
    ''' <param name="key">the Security KEY that needs to be read</param>
    ''' <param name="emp">the Employee code to check security for</param>
    ''' <returns>TRUE, if user has the security indicated; FALSE otherwise</returns>
    Private Function CheckSecurity(key As String, emp As String) As Boolean
        Dim catKey As String = key + "~" + emp
        If Not SecurityCache.ContainsKey(catKey) Then
            SecurityCache.Add(catKey, SecurityUtils.hasSecurity(key, emp))
        End If
        Return SecurityCache(catKey)
    End Function

    ''' <summary>
    ''' Attempts to reserve ALL inventoriable lines that are not linked to a PO 
    ''' and not already reserved 
    ''' </summary>
    ''' <remarks>Called when the 'Reserve Lines' check box gets checked</remarks>
    Private Sub ReserveLines()

        Dim valMsg As String = InventoryUtils.AllowReservations(SessVar.zoneCd, SessVar.puDelStoreCd)
        If (valMsg.isNotEmpty()) Then
            lbl_warnings.Text = valMsg
        Else
            Dim resRequest As ResRequestDtc
            Dim resResponse As ResResponseDtc
            Dim resMade As Boolean = False
            Dim callAutoRes As Boolean
            Dim sessId As String = Session.SessionID.ToString.Trim
            Dim soLnsTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(sessId, temp_itm_key_tp.tempItmBySessionId)

            For Each soLnRow In soLnsTbl.Rows

                ' Attempt reservation for Inventory lines, when line NOT linked to PO and NOT already reserved
                ' TODO - cannot change to inventory flag at this time - only CCExpress 
                '        is setting inventory flag yet, wait till use new INSERT logic
                callAutoRes = Not IsDBNull(soLnRow.Item("ITM_TP_CD")) AndAlso
                              AppConstants.Sku.TP_INV = soLnRow.Item("ITM_TP_CD").ToString AndAlso
                              (IsDBNull(soLnRow.Item("PO_CD")) OrElse soLnRow.Item("PO_CD").ToString.isEmpty) AndAlso
                              (IsDBNull(soLnRow.Item("RES_ID")) OrElse soLnRow.Item("RES_ID").ToString.isEmpty) AndAlso
                              ((IsDBNull(soLnRow("STORE_CD")) OrElse soLnRow("STORE_CD").ToString.isEmpty) OrElse
                               (IsDBNull(soLnRow("LOC_CD")) OrElse soLnRow("LOC_CD").ToString.isEmpty))

                If callAutoRes Then
                    resRequest = GetRequestForAutoRes(soLnRow.Item("ITM_CD"),
                                                      soLnRow.Item("QTY"),
                                                      Session("PD"),
                                                      Session("PD_STORE_CD"), Session("ZONE_CD"))
                    resRequest.lineNum = soLnRow.Item("ROW_ID")

                    '--attempts to reserve the line, (includes SPLIT of lines when needed)
                    resResponse = theSalesBiz.ReserveLine(sessId, SessVar.discCd, resRequest, sbError)
                    ' Check for null reference
                    If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then resMade = True
                    If resResponse.isForARSStore AndAlso resResponse.isLineSplitted Then
                        resMade = False
                    End If
                End If
            Next
            If resMade Then bindgrid() 'since new lines got added

        End If
    End Sub

    ''' <summary>
    ''' Recalculates the discounts (when applicable)
    ''' </summary>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">the ID for the line that triggered this event</param>
    Private Sub ReapplyDiscounts(ByVal action As ReapplyDiscountRequestDtc.LnChng, ByVal lnSeq As String)

        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd")) Then

            Dim discRequest As ReapplyDiscountRequestDtc
            discRequest = New ReapplyDiscountRequestDtc(action, Session("pd"), Session.SessionID.ToString.Trim, lnSeq)
            theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest)
        End If

        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub

    Protected Sub Price_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim txtRetPrc As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txtRetPrc.NamingContainer, DataGridItem)
        Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim, dgItem.Cells(SoLnGrid.ROW_ID).Text, temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)


        lbl_warnings.Text = ""
        If Not IsNumeric(txtRetPrc.Text) Then
            lbl_warnings.Text = Resources.POSMessages.MSG0021
            lbl_warnings.Visible = True
            txtRetPrc.Text = tmpItmRow("ret_prc").ToString
            txtRetPrc.Focus()

            'hidUpdLnNos.Value = String.Empty
            Exit Sub
        Else
            lbl_warnings.Text = String.Empty
            lbl_warnings.Visible = False
            txtRetPrc.Text = Math.Abs(CDbl(txtRetPrc.Text))
        End If

        'If order type is MCR and user entered retail is greater than the original retail(that was maximum avaialable credit for an item)
        If AppConstants.Order.TYPE_MCR = Session("ord_tp_cd") Then
            If Not IsNothing(Session("MCRData")) Then
                Dim listCreditInfo As New List(Of CreditInfoDtc)
                listCreditInfo = Session("MCRData")

                Dim maxCredit As Double = (From credit In listCreditInfo Where credit.ItemCode.Equals(dgItem.Cells(SoLnGrid.ITM_CD).Text) Select credit.AvalCred).Single
                ' totalItemRetail - Holds the sum of the item retail price from temp_itm (its nothing but data displayed on the grid)
                Dim totalItemRetail As Double = theSalesBiz.GetTotalItemRetail(dgItem.Cells(SoLnGrid.ITM_CD).Text, Session.SessionID.ToString())
                ' totalItemRetail = totalItemRetail - previous retail price + current retail price
                totalItemRetail = (totalItemRetail - tmpItmRow(1)) + Convert.ToDouble(txtRetPrc.Text)
                If Convert.ToDouble((totalItemRetail) * tmpItmRow("qty")) > Convert.ToDouble(maxCredit) Then
                    lbl_warnings.Text = String.Format(Resources.POSMessages.MSG0046, dgItem.Cells(SoLnGrid.ITM_CD).Text, FormatCurrency(maxCredit, 2), tmpItmRow("qty"))
                    'Resetting the retail of an item            
                    txtRetPrc.Text = FormatNumber(tmpItmRow(2), 2)
                    lbl_warnings.Visible = True

                    RemoveUpdLn(dgItem.Cells(SoLnGrid.ROW_ID).Text.ToString)
                    Exit Sub

                End If
            End If
        End If

        txtRetPrc.Text = FormatNumber(txtRetPrc.Text, 2)
        Dim hidMaxDisc As HiddenField = CType(dgItem.FindControl("hidMaxDisc"), HiddenField)
        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso Not String.IsNullOrEmpty(hidMaxDisc.Value)) Then

            Dim origPrice As Decimal = 0, maxDiscPerc As Decimal = 0, enteredRetPrice As Decimal = 0

            If Decimal.TryParse(tmpItmRow("orig_prc").ToString, origPrice) _
                AndAlso Decimal.TryParse(hidMaxDisc.Value, maxDiscPerc) _
                AndAlso Decimal.TryParse(txtRetPrc.Text, enteredRetPrice) Then

                Dim maxDiscAmt As Decimal = origPrice - (origPrice * (maxDiscPerc / 100))

                maxDiscAmt = Math.Round(maxDiscAmt, 2) 'Alice added on June 6, 2019, the issue was found during test 4168

                'Alice updated on May 10, 2019 for request 4168
                '1.	Allow max discount item to have its price manually changed And move forward for manager approval if outlet id reserved to item.
                '2.	Allow max discount item to have its price changed if ORDER's sort code is acceptable/approved order sort codes.

                'add start
                Dim ItemIsAssociatePurchaseOrHasOutletId As Boolean = False

                If CType(dgItem.FindControl("lbl_outletid"), Label).Visible = True Then
                    ItemIsAssociatePurchaseOrHasOutletId = True
                End If


                If SalesUtils.isAllowMaxDiscountSortCode(Session("ord_srt").ToString) = "Y" Then
                    ItemIsAssociatePurchaseOrHasOutletId = True
                End If


                If ItemIsAssociatePurchaseOrHasOutletId And maxDiscPerc <> 100 Then

                    'update temp_itm
                    'update the Style_cd, disc_amt in temp_itm right away, use style_cd field to hold "MAX" value
                    Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand

                    Dim cmdconn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    cmdconn.Open()
                    With cmdUpdate
                        .Connection = cmdconn
                        .CommandText = "UPDATE temp_itm SET style_cd = 'MAX' WHERE row_id = " & dgItem.Cells(SoLnGrid.ROW_ID).Text
                    End With
                    cmdUpdate.ExecuteNonQuery()
                    cmdconn.Dispose()
                    cmdUpdate.Dispose()
                End If

                'add end

                'MM-6914 - Maximum discount restriction should not be applied in case of CRMs, MDBs and MCRs
                If (maxDiscAmt > enteredRetPrice) And ItemIsAssociatePurchaseOrHasOutletId = False Then  'Alice updated for request 4168
                    lbl_warnings.Text = String.Format(Resources.POSErrors.ERR0019, FormatCurrency(maxDiscAmt, 2))
                    lbl_warnings.Visible = True

                    txtRetPrc.Text = tmpItmRow("ret_prc")
                    txtRetPrc.Focus()

                    RemoveUpdLn(dgItem.Cells(SoLnGrid.ROW_ID).Text.ToString)
                    Exit Sub
                End If
            End If

        End If

        Dim margin As Double = 0
        If IsNumeric(ConfigurationManager.AppSettings("margin").ToString) Then
            margin = CDbl(ConfigurationManager.AppSettings("margin").ToString)
        End If

        'will prompt for mgr approver if set to 'Line' or "order' OR if "N" but the GM is < the specified margin
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        Dim promptForApproval As Boolean = AppConstants.ManagerOverride.BY_LINE = prcOverride OrElse
                                           AppConstants.ManagerOverride.BY_ORDER = prcOverride OrElse
                                           (margin > 0 AndAlso determine_gm(dgItem.Cells(SoLnGrid.ITM_CD).Text, txtRetPrc.Text) < margin)

        If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso promptForApproval Then
            If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then

                'store the rowId of the line being modified to use later
                If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                    hidUpdLnNos.Value = hidUpdLnNos.Value.ToString() & ";" & dgItem.Cells(SoLnGrid.ROW_ID).Text
                    Session("hidUpdLnNos") = hidUpdLnNos.Value
                Else
                    hidUpdLnNos.Value = dgItem.Cells(SoLnGrid.ROW_ID).Text
                    Session("hidUpdLnNos") = hidUpdLnNos.Value
                End If

                'update the price in temp_itm right away, so that any subsequent retrievals from temp_itm, show the updated price
                Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()
                With cmdUpdateItems
                    .Connection = conn
                    .CommandText = "UPDATE temp_itm SET ret_prc = " & Double.Parse(txtRetPrc.Text) & " WHERE row_id = " & dgItem.Cells(SoLnGrid.ROW_ID).Text
                End With
                cmdUpdateItems.ExecuteNonQuery()
                calculate_total(Page.Master)

                'mariam-sept 14, 2016 - price changed then Merchandise lookup click - show approval popup
                Session("SHOWPOPUP") = True
            Else
                'means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
                ViewState("RETPRC") = FormatNumber(txtRetPrc.Text, 2)
                MgrByLineApprovalPopup(dgItem)
            End If
        Else
            '** not a SAL type of transaction OR that approval is not needed
            If CDbl(txtRetPrc.Text) < 0 Then txtRetPrc.Text = Math.Abs(CDbl(txtRetPrc.Text))
            If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

                If Session("ord_tp_cd") = AppConstants.Order.TYPE_CRM AndAlso CDbl(txtRetPrc.Text) > tmpItmRow("max_ret_prc").ToString Then

                    lbl_warnings.Text = "Credit " + FormatCurrency(txtRetPrc.Text, 2) + " entered for SKU " + dgItem.Cells(SoLnGrid.ITM_CD).Text +
                        " exceeds the available credit for this line from the original document; credit changed to maximum."
                    txtRetPrc.Text = FormatNumber(tmpItmRow("max_ret_prc").ToString, 2)
                    lbl_warnings.Visible = True

                    RemoveUpdLn(dgItem.Cells(SoLnGrid.ROW_ID).Text.ToString)
                    Exit Sub
                Else  ' calc and enforce max extended  

                    ' MCR processing block is handled in the above MCR block
                    'If AppConstants.Order.TYPE_MCR = Session("ord_tp_cd") Then

                    'End If
                End If
            End If

            Dim retPrc = Double.Parse(txtRetPrc.Text)
            Dim cmdUpdateItems As OracleCommand = Nothing
            Dim conn As OracleConnection = Nothing
            If prcOverride <> AppConstants.ManagerOverride.BY_LINE Then
                cmdUpdateItems = DisposablesManager.BuildOracleCommand

                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()
                With cmdUpdateItems
                    .Connection = conn
                    .CommandText = "UPDATE temp_itm set ret_prc = " & retPrc & " , MANUAL_PRC =" & retPrc & "  WHERE row_id = " & dgItem.Cells(SoLnGrid.ROW_ID).Text
                End With
                cmdUpdateItems.ExecuteNonQuery()

                theSalesBiz.UpdateLnTaxAmt(cmdUpdateItems, Double.Parse(txtRetPrc.Text), dgItem.Cells(SoLnGrid.ROW_ID).Text)
            End If

            If dgItem.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then Modify_Package_Price(dgItem.Cells(SoLnGrid.ROW_ID).Text, dgItem.Cells(SoLnGrid.ITM_CD).Text, txtRetPrc.Text)
            Gridview1.EditItemIndex = -1

            '***** Need to check here if discounts need to be reapplied for ONLY a SAL & when the prc override syspm is set as "NO" 
            '***** This condition should ideally be checked up above beofre getting to this 'if' instead of here but that is more restructuring
            If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso AppConstants.ManagerOverride.NONE = prcOverride Then
                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
            End If

            If prcOverride <> AppConstants.ManagerOverride.BY_LINE Then
                bindgrid()
                If Not IsNothing(conn) AndAlso conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                calculate_total(Page.Master)
            End If

            If AppConstants.ManagerOverride.BY_LINE = prcOverride Then
                MgrByLineApprovalPopup(dgItem)
            End If

        End If
        'If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
        '    theSalesBiz.updateSplitItmShareWhenPriceChange(dgItem.Cells(SoLnGrid.ROW_ID).Text, Double.Parse(txtRetPrc.Text))
        'End If
        'mariam-Sept 27,2016 - Price change approve
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            lblErrorMsg.Visible = True
            lblErrorMsg.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            ' DB May 18, 2016 move message in CustomMessages
            'lblErrorMsg.Text = Resources.POSMessages.MSG0058
            lblErrorMsg.Text = Resources.CustomMessages.MSG0118
        End If
        'mariam -end

    End Sub

    Protected Sub Loc_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim textdata As TextBox = CType(sender, TextBox)
        DoStoreLocationUpdate(textdata, False)
    End Sub

    Protected Sub Store_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim textdata As TextBox = CType(sender, TextBox)
        DoStoreLocationUpdate(textdata, True)
    End Sub

    Protected Sub Qty_Update(ByVal sender As Object, ByVal e As EventArgs)
        If Session("cash_carry") = "TRUE" Then  'lucy 02-Mar-15
            Exit Sub
        End If
        lbl_warnings.Text = ""

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdt As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim availDate As Date
        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)
        Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim, dgItem.Cells(SoLnGrid.ROW_ID).Text, temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)

        lbl_warnings.Text = OrderUtils.ValAndFormatQty(textdata.Text, tmpItmRow("QTY"), tmpItmRow("ITM_TP_CD"), tmpItmRow("WARR_ROW_LINK") & "", tmpItmRow("OUT_ID") & "")
        If lbl_warnings.Text.isNotEmpty Then
            lbl_warnings.Visible = True
        End If

        'only if there truly was a change of QTY the next needs to be completed
        If textdata.Text <> tmpItmRow("QTY") Then

            conn.Open()

            If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

                If Session("ord_tp_cd") = AppConstants.Order.TYPE_CRM AndAlso CDbl(textdata.Text) > CDbl(tmpItmRow("max_crm_qty").ToString) Then

                    lbl_warnings.Text = "Quantity " + textdata.Text + " entered for SKU " + dgItem.Cells(SoLnGrid.ITM_CD).Text +
                        " is greater than available quantity to return from original document; quantity changed to maximum."
                    textdata.Text = FormatNumber(tmpItmRow("max_crm_qty").ToString, 0)
                    lbl_warnings.Visible = True

                    Exit Sub
                Else  ' calc and enforce max extended  
                    Dim maxExtRet As Double = 0
                    ' just happens that max_crm_qty got loaded with orig sal qty so we can use it here to calc MCR only extended too
                    maxExtRet = Math.Round(CDbl(tmpItmRow("max_crm_qty").ToString) * CDbl(tmpItmRow("max_ret_prc").ToString), SystemUtils.CurrencyPrecision)
                    If Math.Round(CDbl(textdata.Text) * CDbl(tmpItmRow("ret_prc").ToString), SystemUtils.CurrencyPrecision) > maxExtRet Then

                        lbl_warnings.Text = "Maximum credit available for SKU " + dgItem.Cells(SoLnGrid.ITM_CD).Text + " is " + FormatCurrency(maxExtRet, 2) + "; quantity changed to lesser of previous or maximum."
                        If tmpItmRow("max_crm_qty").ToString > tmpItmRow("qty") Then
                            textdata.Text = FormatNumber(tmpItmRow("qty").ToString, 0)
                        Else
                            textdata.Text = FormatNumber(tmpItmRow("max_crm_qty").ToString, 0)
                        End If
                        lbl_warnings.Visible = True

                        Exit Sub
                    End If
                End If
            End If

            'if ARS store, and a sale doc and line is not take with, then get avail date
            Dim isTWith As String = If(IsDBNull(tmpItmRow("take_with")), "N", tmpItmRow("take_with"))
            If ((AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso isTWith = "N") Then
                Dim isInventory As Boolean = ResolveYnToBooleanEquiv(dgItem.Cells(SoLnGrid.INVENTORY).Text.Trim())
                Dim lblAvail As Label = DirectCast(dgItem.FindControl("lblAvailDt"), Label)
                If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                    availDate = GetAvailDate(isInventory, dgItem.Cells(SoLnGrid.ITM_CD).Text, textdata.Text)
                    If Not (availDate = DateTime.MinValue) And isInventory Then
                        lblAvail.Text = Resources.POSMessages.MSG0002 & FormatDateTime(availDate, DateFormat.ShortDate)
                        lblAvail.Visible = True
                    End If
                Else
                    lblAvail.Visible = False
                End If
            End If
            If IsNumeric(dgItem.Cells(SoLnGrid.ROW_ID).Text) Then

                ' ---- REMOVE PENDING (SOFT) RESERVATIONS WHEN PRESENT
                ' The Store/Location will get cleared if the line gets unreserved
                Dim theStore As String = If(IsDBNull(tmpItmRow("STORE_CD")), String.Empty, tmpItmRow("STORE_CD"))
                Dim theLocation As String = If(IsDBNull(tmpItmRow("LOC_CD")), String.Empty, tmpItmRow("LOC_CD"))
                Dim theResId As String = If(IsDBNull(tmpItmRow("RES_ID")), String.Empty, tmpItmRow("RES_ID"))
                Dim isReserved As Boolean = If(String.IsNullOrEmpty(theResId), False, True)
                If (isReserved) Then
                    Dim oldQty As Double = CDbl(tmpItmRow("QTY"))
                    Dim newQty As Double = CDbl(textdata.Text)
                    Dim qtyToUnres As Double = oldQty
                    If (newQty > 0 AndAlso newQty < oldQty) Then qtyToUnres = (oldQty - newQty)

                    Dim request As UnResRequestDtc = GetRequestForUnreserve(tmpItmRow("RES_ID"),
                                                                            tmpItmRow("ITM_CD"),
                                                                            theStore,
                                                                            theLocation,
                                                                            qtyToUnres.ToString)
                    theInvBiz.RemoveSoftRes(request)
                    'A qty change triggers a partial unreserve (except when line is zero)
                    'therefore the store/location has to be kept on those cases
                    theStore = If(oldQty = qtyToUnres, String.Empty, theStore)
                    theLocation = If(oldQty = qtyToUnres, String.Empty, theLocation)
                    theResId = If(oldQty = qtyToUnres, String.Empty, theResId)
                    CType(dgItem.FindControl("store_cd"), TextBox).Text = theStore
                    CType(dgItem.FindControl("loc_cd"), TextBox).Text = theLocation
                End If

                Dim sql As String = "UPDATE temp_itm SET qty = :qty, avail_dt = :availDt, store_cd = :theStore, loc_cd = :theLocation, res_id = :theResId WHERE row_id= :row_id"
                cmdUpdt.Parameters.Clear()
                With cmdUpdt
                    .Connection = conn
                    .CommandText = sql
                    .Parameters.Add(":qty", OracleType.Number)
                    .Parameters.Add(":availDt", OracleType.DateTime)
                    .Parameters.Add(":theStore", OracleType.VarChar)
                    .Parameters.Add(":theLocation", OracleType.VarChar)
                    .Parameters.Add(":theResId", OracleType.VarChar)
                    .Parameters.Add(":row_id", OracleType.VarChar)
                    .Parameters(":qty").Value = CDbl(textdata.Text)
                    .Parameters(":availDt").Value = IIf(availDate = DateTime.MinValue, DBNull.Value, availDate)
                    .Parameters(":theStore").Value = theStore
                    .Parameters(":theLocation").Value = theLocation
                    .Parameters(":theResId").Value = theResId
                    .Parameters(":row_id").Value = dgItem.Cells(SoLnGrid.ROW_ID).Text
                End With
                cmdUpdt.ExecuteNonQuery()

                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Qty, dgItem.Cells(SoLnGrid.ROW_ID).Text)

                calculate_total(Page.Master)
            End If
            conn.Close()
        End If

    End Sub

    'called when the 'FAB' check box is clicked
    Protected Sub Treated__CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim chkboxTreated As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chkboxTreated.NamingContainer, DataGridItem)
        Dim lnRowId As String = dgItem.Cells(SoLnGrid.ROW_ID).Text

        Dim sql As String = " UPDATE temp_itm SET TREATED = :treatedValue WHERE ROW_ID = :lnRowId "
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
            dbCommand.Parameters.Add(":treatedValue", OracleType.VarChar)
            dbCommand.Parameters(":lnRowId").Value = lnRowId
            dbCommand.Parameters(":treatedValue").Value = IIf(chkboxTreated.Checked, "Y", "N")
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try
    End Sub

    Protected Sub Carton_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim chk_carton As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chk_carton.NamingContainer, DataGridItem)
        If chk_carton.Checked = True Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set leave_carton = 'Y' where row_id=" & dgItem.Cells(SoLnGrid.ROW_ID).Text
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update temp_itm set leave_carton = 'N' where row_id=" & dgItem.Cells(SoLnGrid.ROW_ID).Text
            End With
        End If
        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()

    End Sub

    'called when user clicks the TAKE-WITH checkbox in the Lines grid
    Protected Sub Take_With_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim chkBoxTW As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chkBoxTW.NamingContainer, DataGridItem)
        Dim lblAvail As Label = DirectCast(dgItem.FindControl("lblAvailDt"), Label)
        Dim lnRowId As String = dgItem.Cells(SoLnGrid.ROW_ID).Text

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String = ""

        ' ==== DOES THE COMMON PROCESSING FOR THE CHECKBOX BEING CLICKED ====
        ' hides the available date label (it may show it below)
        lblAvail.Visible = False

        ' updates the TAKE-WITH value on the treatment lines accordingly
        Try
            dbConnection.Open()
            sql = "SELECT treated_by FROM temp_itm WHERE ROW_ID = :lnRowId "
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
            dbCommand.Parameters(":lnRowId").Value = lnRowId
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            dbCommand.Parameters.Clear()
            If (dbReader.Read() AndAlso IsNumeric(dbReader.Item("TREATED_BY").ToString)) Then
                sql = "UPDATE temp_itm SET TAKE_WITH = :value WHERE row_id = :treatByRowId "
                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":treatByRowId", OracleType.VarChar)
                dbCommand.Parameters.Add(":value", OracleType.VarChar)
                dbCommand.Parameters(":treatByRowId").Value = dbReader.Item("TREATED_BY").ToString
                dbCommand.Parameters(":value").Value = IIf(chkBoxTW.Checked, "Y", "")
                dbCommand.ExecuteNonQuery()
                dbCommand.Parameters.Clear()
            End If
        Catch ex As Exception
            dbConnection.Close()
            Throw
        End Try

        ' ==== DOES THE PROCESSING FOR THE CHECKBOX BEING CHECKED / UNCHECKED ====
        If chkBoxTW.Checked = True Then

            Try
                sql = "UPDATE temp_itm SET TAKE_WITH = 'Y', AVAIL_DT = '' WHERE ROW_ID = :lnRowId "
                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
                dbCommand.Parameters(":lnRowId").Value = lnRowId
                dbCommand.ExecuteNonQuery()
                dbCommand.Parameters.Clear()

                'If there are package SKUs, we need to break out the pricing
                If IsNumeric(dgItem.Cells(SoLnGrid.PKG_LN).Text) Then
                    sql = "UPDATE TEMP_ITM SET RET_PRC=TAXABLE_AMT WHERE SESSION_ID='" & Session.SessionID.ToString() & "' AND PACKAGE_PARENT=" & dgItem.Cells(SoLnGrid.PKG_LN).Text
                    dbCommand.CommandText = sql
                    dbCommand.ExecuteNonQuery()

                    sql = "UPDATE TEMP_ITM SET RET_PRC=0.00 WHERE ITM_TP_CD='PKG' AND SESSION_ID='" & Session.SessionID.ToString() & "' AND ROW_ID=" & dgItem.Cells(SoLnGrid.PKG_LN).Text
                    dbCommand.CommandText = sql
                    dbCommand.ExecuteNonQuery()
                End If

                Set_Take_With_Tax()

                ' --- Checks if ALL lines are now TAKE-WITH to prompt the user
                Dim totalLinesCount As Double = 0
                Dim twLinesCount As Double = 0
                sql = "SELECT COUNT(ROW_ID) AS TOTAL_COUNT,SUM(T_SUM) As T_SUM  FROM (SELECT (ROW_ID)  , (QTY*RET_PRC) As T_SUM FROM TEMP_ITM WHERE ITM_TP_CD <> 'WAR' AND TAKE_WITH = 'Y' AND SESSION_ID = :sessId UNION SELECT (ROW_ID), (QTY*RET_PRC) As T_SUM FROM TEMP_ITM WHERE ITM_TP_CD = 'WAR' AND WARR_ROW_LINK IS NOT NULL AND SESSION_ID =:sessId)"
                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
                dbCommand.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
                If (dbReader.Read()) Then
                    If IsNumeric(dbReader.Item("TOTAL_COUNT").ToString) Then twLinesCount = CDbl(dbReader.Item("TOTAL_COUNT").ToString)
                    If IsNumeric(dbReader.Item("T_SUM").ToString) Then Session("Take_Deposit") = CDbl(dbReader.Item("T_SUM").ToString)
                End If

                If twLinesCount > 0 Then
                    sql = "SELECT COUNT(ROW_ID) AS TOTAL_COUNT FROM temp_itm WHERE session_id= :sessId"
                    dbCommand.CommandText = sql  'no need to set the parameters are set above
                    dbCommand.Parameters.Clear()
                    dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
                    dbCommand.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
                    If (dbReader.Read() AndAlso IsNumeric(dbReader.Item("TOTAL_COUNT").ToString)) Then
                        totalLinesCount = CDbl(dbReader.Item("TOTAL_COUNT").ToString)
                    End If


                    If totalLinesCount - twLinesCount = 0 Then
                        ASPxPopupControl11.ShowPageScrollbarWhenModal = True
                        ASPxPopupControl11.ShowOnPageLoad = True
                        lbl_warnings.Text = "All items on the sale are Take-With."
                    End If
                End If

                If InStr(lbl_warnings.Text, "All items on the sale are Take-With.") > 0 Then
                    'Do Nothing, a pop-up box will appear
                Else
                    Response.Redirect(IIf(Request("LEAD") = "TRUE", "order_detail.aspx?LEAD=TRUE", "order_detail.aspx"))
                End If
                dbReader.Close()

            Catch ex As Exception
                Throw
            Finally
                dbConnection.Close()
            End Try

        Else  ' the TW checkbox was unchecked

            Try
                If Session("tet_cd") & "" = "" Then
                    Dim taxCd As String = If(IsDBNull(Session("tax_cd")) = True OrElse IsNothing(Session("tax_cd")) = True, String.Empty, Session("tax_cd").ToString)
                    ' TODO DSA NOW - tax rate needs to be based on itm_tp_cd
                    sql = "UPDATE temp_itm SET TAKE_WITH = NULL, TAX_CD = :taxCd"
                    If IsNumeric(Session("tax_rate")) Then sql = sql & ", TAX_PCT = " & Session("tax_rate")
                    sql = sql & " WHERE ROW_ID = :lnRowId"

                    dbCommand.CommandText = sql
                    dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
                    dbCommand.Parameters(":lnRowId").Value = lnRowId
                    dbCommand.Parameters.Add(":taxCd", OracleType.VarChar)
                    dbCommand.Parameters(":taxCd").Value = taxCd
                    dbCommand.ExecuteNonQuery()
                    dbCommand.Parameters.Clear()
                End If

                'sets the available date               
                Dim availDate As Date = Date.Today
                Dim isInventory As Boolean = ResolveYnToBooleanEquiv(dgItem.Cells(SoLnGrid.INVENTORY).Text.Trim())
                Dim tBoxQty As TextBox = DirectCast(dgItem.FindControl("qty_txtbox"), TextBox)
                If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                    availDate = GetAvailDate(isInventory, dgItem.Cells(SoLnGrid.ITM_CD).Text, CDbl(tBoxQty.Text))
                End If

                If Not (availDate = DateTime.MinValue) And isInventory Then
                    lblAvail.Text = Resources.POSMessages.MSG0002 & FormatDateTime(availDate, DateFormat.ShortDate)
                    If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                        lblAvail.Visible = True
                    Else
                        lblAvail.Visible = False
                    End If
                End If
                sql = "UPDATE temp_itm SET AVAIL_DT = :availDt WHERE row_id = :lnRowId"
                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
                dbCommand.Parameters.Add(":availDt", OracleType.DateTime)
                dbCommand.Parameters(":lnRowId").Value = lnRowId
                dbCommand.Parameters(":availDt").Value = IIf(availDate = DateTime.MinValue, DBNull.Value, availDate)
                dbCommand.ExecuteNonQuery()
                dbConnection.Close()

            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try
        End If
    End Sub

    Public Sub Set_Take_With_Tax()

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim Zip_cd As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'Open Connection 
        conn.Open()

        SQL = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Zip_cd = MyDataReader.Item("ZIP_CD")
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Zip_cd & "" <> "" Then
            SQL = "SELECT ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
            SQL = SQL & "FROM TAT, TAX_CD$TAT, ZIP2TAX "
            SQL = SQL & "WHERE TAT.EFF_DT <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            SQL = SQL & "AND TAX_CD$TAT.TAX_CD = ZIP2TAX.TAX_CD "
            SQL = SQL & "AND  ZIP2TAX.ZIP_CD='" & Zip_cd & "' "
            SQL = SQL & "GROUP BY ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD"

            Dim dv As DataView
            Dim MyTable As DataTable
            Dim numrows As Integer
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            ds = New DataSet

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                ' TODO DSA NOW - need to loop thru lines and set these values based on the itm type
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) And numrows = 1 Then
                    'Add tax rates to the inventory, if items exist
                    Dim connlocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


                    connlocal.Open()
                    SQL = "UPDATE TEMP_ITM SET TAX_CD='" & MyDataReader.Item("TAX_CD").ToString & "'"
                    If IsNumeric(MyDataReader.Item("SumofPCT").ToString) Then
                        SQL = SQL & ", TAX_PCT=" & MyDataReader.Item("SumofPCT").ToString
                    End If
                    SQL = SQL & " WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH='Y'"
                    With cmdDeleteItems
                        .Connection = connlocal
                        .CommandText = SQL
                    End With
                    cmdDeleteItems.ExecuteNonQuery()
                    connlocal.Close()

                End If
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()

    End Sub

    ''' <summary>
    ''' Builds the QUERY that will be used to retrieve ITEM info after a new SKU is added to the order
    ''' </summary>
    ''' <param name="itemCd">the items or list of items being added</param>
    ''' <param name="useAltId">TRUE if the alternate ID should be used; FALSE otherwise</param>
    ''' <returns>the resulting QUERY</returns>
    Private Function GetItemQuery(ByVal itemCd As String, Optional useAltId As Boolean = False) As String

        Dim sqlSB As StringBuilder = New StringBuilder()
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")

        sqlSB.Append(" SELECT I.ITM_CD, BULK_TP_ITM, SERIAL_TP, VE_CD, MNR_CD, CAT_CD, RET_PRC, ADV_PRC, VSN, DES, SIZ, FINISH, COVER, INVENTORY, ")
        sqlSB.Append(" GRADE, STYLE_CD, ITM_TP_CD,TREATABLE, FRAME_TP_ITM, DROP_CD, DROP_DT, USER_QUESTIONS, POINT_SIZE, REPL_CST, ")
        sqlSB.Append(" COMM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF, NVL(warrantable,'N') AS warrantable ")
        sqlSB.Append(" FROM itm I LEFT JOIN  " + spiffTable + " sp ON I.ITM_CD = SP.ITM_CD AND TO_DATE(sysdate) BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT) ")
        sqlSB.Append(If(SysPms.isSpiffBySKU, " ", (" AND STORE_CD = '" & Session("store_cd") & "' ")))

        If (useAltId) Then
            sqlSB.Append(" WHERE I.itm_cd in (SELECT ITM_CD FROM ALT_ITM_ID WHERE ALT_ITM_ID_CD IN ( ").Append(itemCd).Append(" )) ")
        Else
            sqlSB.Append(" WHERE I.itm_cd in (").Append(itemCd).Append(")")
        End If
        Return sqlSB.ToString()
    End Function

    ''' <summary>
    ''' Routine that accept one or more entered/selected SKUs, extracts related SKU info and inserts appropriate data into TEMP_ITM
    ''' which is the dataset for the lines grid
    ''' </summary>
    ''' <returns>0 if no rows added, 1 or more if SKU line(s) inserted to temp_itm</returns>
    Private Function insert_records() As Integer
        ' Suspected auto_add functionality - only appears to be used when user goes to Order.page which is the 'sku find' icon (flashlight) 
        '   (or click on image in this page)
        '   on the right of the SKU entry; if the process returns a single SKU and that SKU is not a custom SKU and has no related SKUs, then
        '   after record insertion, this routine will go back to the Order page to allow another entry
        Dim auto_add As Boolean = False
        Dim recsAdded As Integer = 0

        If SessVar.skuListToAdd.isNotEmpty AndAlso SessVar.skuListToAdd <> "NONEENTERED" Then

            lbl_warnings.Text = ""

            Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim itemcd As String = ""
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim MyTable As DataTable
            Dim sql As String
            Dim sAdp As OracleDataAdapter
            Dim ds As DataSet
            Dim numrows As Integer
            Dim loop1 As Integer
            Dim sku As String = ""
            Dim VeCd As String = ""
            Dim mnrcd As String = ""
            Dim catcd As String = ""
            Dim point_size As String = ""
            Dim retprc, pass_ret_prc, adv_prc As Double
            Dim prom_prc As String = ""
            Dim cost As Double
            Dim vsn As String = ""
            Dim comm_cd As String = ""
            Dim spiff As String = ""
            Dim des As String = ""
            Dim siz As String = ""
            Dim finish As String = ""
            Dim cover As String = ""
            Dim grade As String = ""
            Dim stylecd As String = ""
            Dim itm_tp_cd As String = ""
            Dim treatable As String = ""
            Dim isWarrantable As Boolean
            Dim isInventory As Boolean
            Dim thistransaction As OracleTransaction
            Dim dropped_SKUS As String = ""
            Dim dropped_dialog As String = ""
            Dim User_Questions As String = ""
            Dim tax_cd As String = ""
            Dim tax_pct As String = ""
            Dim taxable_amt As String
            Dim row_id_seq_num As String = ""
            Dim showWarrPopup As Boolean = False
            Dim focusedRowId As String = String.Empty
            Dim store_cd As String = ""
            Dim loc_cd As String = ""
            Dim defaultStoreLocation As Boolean = (Session("cash_carry") = "TRUE" And FAB_FOUND <> "Y")

            objConnection.Open()

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            itemcd = Session("itemid")
            Dim itms() As String = itemcd.Trim().TrimStart(",").TrimEnd(",").Split(",")
            itemcd = "'" + itemcd.Replace(",", "','") + "'"

            ' TODO - E1 probably does this in the reverse sequence, alt first, then regular
            'looks for ITEM using the item-cd
            sAdp = DisposablesManager.BuildOracleDataAdapter(GetItemQuery(itemcd), conn)
            ds = New DataSet
            sAdp.Fill(ds)
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            'if the ITEM_CD was not found, then uses the ALT_ITM_CD
            If numrows = 0 Then
                sAdp = DisposablesManager.BuildOracleDataAdapter(GetItemQuery(itemcd, True), conn)
                ds = New DataSet
                sAdp.Fill(ds)
                MyTable = ds.Tables(0)
                numrows = MyTable.Rows.Count
            End If

            If numrows < itms.Length Then
                Dim duplicates = itms.GroupBy(Function(p) p).Where(Function(g) g.Count() > 1).[Select](Function(g) g.Key)
                For Each dupStr In duplicates
                    Dim dupCnt = itms.Where(Function(c) c = dupStr).ToArray().Length
                    If dupCnt > 1 Then
                        For a = 0 To dupCnt - 2
                            Dim result() As DataRow = MyTable.Select("ITM_CD = '" + dupStr + "'")
                            MyTable.ImportRow(result(0))
                        Next
                        numrows = MyTable.Rows.Count
                    End If
                Next
            End If

            If numrows = 0 Then
                ' Daniela french added
                'lbl_warnings.Text = "An invalid SKU has been entered."
                lbl_warnings.Text = Resources.POSMessages.MSG0024
                Return recsAdded
                Exit Function
            ElseIf numrows = 1 And Request("auto_add") = "TRUE" Then
                auto_add = True
            End If

            Session_String = Session.SessionID.ToString()
            Dim taxability As String = ""
            Dim PKG_SKUS As String = ""
            Dim CUSTOM_SKUS As String = ""
            Dim selectedSKU As String = ""

            Dim preventBuilder As New StringBuilder
            Dim preventCount As Integer = 0
            Dim numrowsCount As Integer
            numrowsCount = numrows
            For loop1 = 0 To numrows - 1

                sku = MyTable.Rows(loop1).Item("ITM_CD").ToString
                selectedSKU = sku
                'numrowsCount = numrowsCount - 1
                'Do not allow SKUs that have a prevent sale flag on them
                If (theSkuBiz.IsSellable(sku) AndAlso Session("ord_tp_cd") = "SAL") Or Session("ord_tp_cd") <> "SAL" Then

                    ' TODO - this needs to be compared to HBCG_Utils.CreateTempItm with fine detail before implementing BUT
                    '   that routine was cloned from this one so it should be almost spot on
                    isInventory = IIf("Y".Equals(MyTable.Rows(loop1).Item("INVENTORY").ToString), True, False)
                    'for inventoriable ITEMs if order is Take-with defaults the store/location
                    '=================================================================================================
                    'NOTE: 04/03/2014 the defaulting of the store and location has been commented.  Reason being
                    '      that the user can reserve inventory with a single click
                    'store_cd = Session("store_cd")
                    'If defaultStoreLocation AndAlso isInventory Then   'MyTable.Rows(loop1).Item("INVENTORY").ToString = "Y" Then
                    '    loc_cd = GetDefaultLocation(store_cd)
                    'End If
                    'If isEmpty(loc_cd) Then store_cd = ""
                    '=================================================================================================
                    'lucy add the condition
                    If Session("lucy_rf_scan") = "Y" Then
                        store_cd = Session("str_store_cd")
                        loc_cd = Session("str_loc_cd")
                        If loc_cd & "" <> "" Then
                            'Dim str_rfid As String = Session("rf_id_cd")
                            'insert_lucy_pos_rf(Session_String, loc_cd, sku, store_cd, str_rfid, row_id_seq_num) 'lucy
                        End If
                        If isEmpty(loc_cd) Then
                            store_cd = ""
                            Session("rf_id_cd") = ""
                        End If
                    End If    'lucy


                    If MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & "" <> "" Then
                        User_Questions = User_Questions & MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & vbCrLf
                    End If
                    If IsDate(MyTable.Rows(loop1).Item("DROP_DT").ToString) Then
                        dropped_SKUS = FormatDateTime(MyTable.Rows(loop1).Item("DROP_DT").ToString, DateFormat.ShortDate)
                    Else
                        dropped_SKUS = ""
                    End If
                    If dropped_SKUS & "" <> "" Then
                        If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                            dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyTable.Rows(loop1).Item("VSN").ToString & ") was dropped on " & dropped_SKUS & vbCrLf
                        End If
                    End If
                    If MyTable.Rows(loop1).Item("ITM_TP_CD").ToString = "PKG" Then
                        PKG_SKUS = PKG_SKUS & "'" & sku & "',"
                    End If
                    If MyTable.Rows(loop1).Item("FRAME_TP_ITM").ToString = "Y" Then
                        CUSTOM_SKUS = CUSTOM_SKUS & sku & ","
                    End If
                    VeCd = MyTable.Rows(loop1).Item("VE_CD").ToString
                    mnrcd = MyTable.Rows(loop1).Item("MNR_CD").ToString
                    If IsDBNull(MyTable.Rows(loop1).Item("CAT_CD")) = False Then
                        catcd = MyTable.Rows(loop1).Item("CAT_CD").ToString
                    Else
                        catcd = ""
                    End If
                    If IsDBNull(MyTable.Rows(loop1).Item("COMM_CD")) = False Then
                        comm_cd = MyTable.Rows(loop1).Item("COMM_CD").ToString
                    Else
                        comm_cd = ""
                    End If
                    'If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" And MyTable.Rows(loop1).Item("ITM_TP_CD").ToString = "PKG" Then
                    '    pass_ret_prc = CDbl(MyTable.Rows(loop1).Item("RET_PRC").ToString)
                    '    retprc = 0
                    'Else
                    retprc = CDbl(MyTable.Rows(loop1).Item("RET_PRC").ToString)
                    pass_ret_prc = CDbl(MyTable.Rows(loop1).Item("RET_PRC").ToString)
                    'End If
                    If IsNumeric(MyTable.Rows(loop1).Item("ADV_PRC").ToString) Then
                        adv_prc = CDbl(MyTable.Rows(loop1).Item("ADV_PRC").ToString)
                    Else
                        adv_prc = 0
                    End If
                    'If adv_prc < retprc And adv_prc > 0 Then retprc = adv_prc
                    If MyTable.Rows(loop1).Item("REPL_CST").ToString & "" <> "" Then
                        cost = CDbl(MyTable.Rows(loop1).Item("REPL_CST").ToString)
                    Else
                        cost = 0
                    End If
                    vsn = MyTable.Rows(loop1).Item("VSN").ToString
                    If MyTable.Rows(loop1).Item("DES").ToString & "" <> "" Then
                        des = MyTable.Rows(loop1).Item("DES").ToString
                    End If
                    If MyTable.Rows(loop1).Item("POINT_SIZE").ToString & "" <> "" Then
                        point_size = MyTable.Rows(loop1).Item("POINT_SIZE").ToString
                    End If
                    If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                        If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                            spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                        Else
                            spiff = 0
                        End If
                    Else
                        spiff = 0
                    End If
                    If point_size & "" = "" Then point_size = "0"
                    itm_tp_cd = MyTable.Rows(loop1).Item("ITM_TP_CD").ToString
                    treatable = MyTable.Rows(loop1).Item("TREATABLE").ToString
                    isWarrantable = IIf("Y".Equals(MyTable.Rows(loop1).Item("warrantable").ToString + ""), True, False)
                    thistransaction = objConnection.BeginTransaction

                    'Daniela validate trans_dt and custTpPrcCd
                    Dim custTpPrcCd As String = ""

                    If (isNotEmpty(SessVar.custTpPrcCd)) Then
                        custTpPrcCd = SessVar.custTpPrcCd
                    End If
                    If Not IsDate(Session("tran_dt")) Then
                        Session("tran_dt") = Today.Date
                    End If

                    'retprc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                    '                                     SessVar.wrStoreCd,
                    '                                     SessVar.custTpPrcCd,
                    '                                     Session("tran_dt"),
                    '                                     sku, retprc)

                    retprc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                        SessVar.wrStoreCd,
                                                        custTpPrcCd,
                                                        Session("tran_dt"),
                                                        sku, retprc)
                    'End Daniela

                    'Determine if the select item should be taxed.
                    If (Session("TAX_CD") & "" <> "") Then
                        taxability = TaxUtils.Determine_Taxability(itm_tp_cd, Date.Today, Session("TAX_CD"))
                    End If
                    If taxability = "Y" And String.IsNullOrEmpty(Session("tet_cd")) Then
                        tax_cd = Session("TAX_CD")
                        ' TODO DSA NOW - there is not a session tax rate - where does this come from - maybe array (can it be) of rates by applicable itm_tp_cd?
                        ' TODO - no reason to do this - replace all with call to HBCG_UTils.createTempItm
                        tax_pct = Session("TAX_RATE")
                        taxable_amt = retprc
                    Else
                        tax_cd = 0
                        tax_pct = 0
                        taxable_amt = 0
                    End If
                    If Not IsNumeric(tax_pct) Then tax_pct = 0
                    If Not IsNumeric(retprc) Then retprc = 0
                    If Not IsNumeric(point_size) Then point_size = 0
                    If Not IsNumeric(cost) Then cost = 0
                    If Not IsNumeric(taxable_amt) Then taxable_amt = 0
                    If Not IsNumeric(spiff) Then spiff = 0
                    If Not IsNumeric(Session("pct_1")) Then Session("pct_1") = "100"
                    If Not IsNumeric(Session("pct_2")) Then Session("pct_2") = "0"

                    Dim special As String = "N"

                    If new_special.Text = "TRUE" Then
                        special = "Y"
                    End If

                    'Alice updated on June 10, 2019 for request 81, not allow to default salesperson1 to login user for brick CSR sales
                    If Session("str_co_grp_cd").ToString = "BRK" Then
                        If Session("slsp1") & "" = "" Then
                            Session("pct_1") = "100"
                            If SalesUtils.Validate_Slsp1(System.Web.HttpContext.Current.Session("emp_cd")) = True Then
                                Session("slsp1") = Session("emp_cd")
                            End If
                        End If

                    Else
                        If Session("slsp1") & "" = "" Then
                            Session("slsp1") = Session("emp_cd")
                            Session("pct_1") = "100"
                        End If
                    End If



                    Dim itm_prc As String = ""
                    Dim Calc_Ret As Double
                    Calc_Ret = taxable_amt * (tax_pct / 100)
                    If itm_tp_cd = "PKG" And ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then retprc = 0

                    'calculate availability for ARS store, if any                
                    Dim availDate As Date = Today.Date
                    If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                        availDate = GetAvailDate(isInventory, sku, 1)
                    End If

                    ' save the record for the entered/selected SKU
                    sql = "INSERT INTO temp_itm (session_id, ITM_CD, VE_CD, MNR_CD, CAT_CD, RET_PRC, ORIG_PRC,  MANUAL_PRC, VSN, DES, SIZ, FINISH, COVER, GRADE, "
                    sql = sql & "STYLE_CD, ITM_TP_CD, TREATABLE, DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF, NEW_SPECIAL_ORDER, "
                    sql = sql & "SLSP1, SLSP1_PCT, TAX_AMT, "
                    If Session("slsp2") & "" <> "" Then
                        sql = sql & "SLSP2, SLSP2_PCT, "
                    End If
                    sql = sql & "BULK_TP_ITM, SERIAL_TP, STORE_CD, LOC_CD, WARRANTABLE, AVAIL_DT) values ('"
                    sql = sql & Session.SessionID.ToString.Trim & "','" & sku & "','" & VeCd & "','" & mnrcd & "','" & catcd & "'," & retprc & ", :orig_prc, :manual_prc, '"
                    sql = sql & Replace(vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''")
                    sql = sql & "','" & Replace(cover, "'", "''") & "','" & Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & treatable
                    sql = sql & "'," & point_size & "," & cost & ",'" & comm_cd & "','" & tax_cd & "'," & tax_pct & "," & taxable_amt & "," & spiff & ",'"
                    sql = sql & special & "'" & ",'" & Session("slsp1") & "'," & Session("pct_1") & ",ROUND(" & Calc_Ret & "," & TaxUtils.Tax_Constants.taxPrecision & ")"
                    If Session("slsp2") & "" <> "" Then
                        sql = sql & ",'" & Session("slsp2") & "'," & Session("pct_2")
                    End If
                    sql = sql & ", '" & MyTable.Rows(loop1).Item("BULK_TP_ITM").ToString & "', '" & MyTable.Rows(loop1).Item("SERIAL_TP").ToString & "'"
                    sql = sql & ", '" & store_cd & "','" & loc_cd & "', :warra, :avail_dt) RETURNING rowid INTO :row_id "

                    Dim selectedSkuRowId As String = ""
                    cmdInsertItems.Parameters.Clear()

                    With cmdInsertItems
                        .Transaction = thistransaction
                        .Connection = objConnection
                        .CommandText = sql
                        .Parameters.Add(":warra", OracleType.VarChar).Direction = ParameterDirection.Input
                        .Parameters(":warra").Value = IIf(isWarrantable, "Y", "N")
                        .Parameters.Add(":avail_dt", OracleType.DateTime).Direction = ParameterDirection.Input
                        .Parameters(":avail_dt").Value = IIf(availDate = DateTime.MinValue, DBNull.Value, availDate)
                        .Parameters.Add(":orig_prc", OracleType.Number).Direction = ParameterDirection.Input
                        .Parameters(":orig_prc").Value = retprc
                        .Parameters.Add(":manual_prc", OracleType.Number)
                        .Parameters(":manual_prc").Value = retprc
                        .Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                        .Parameters(":row_id").Value = System.DBNull.Value
                    End With

                    cmdInsertItems.ExecuteNonQuery()
                    selectedSkuRowId = cmdInsertItems.Parameters(":row_id").Value.ToString
                    thistransaction.Commit()
                    recsAdded = recsAdded + 1 ' this does not need to be an exact count but need to know if any added for purposes of reapplying header discounts
                    point_size = "0"

                    ' Daniela save rowid
                    Dim str_row_id As String = SalesUtils.GetTempItmRowSeqNum(selectedSkuRowId)
                    Session("itm_row_id") = str_row_id

                    ' Daniela save URL for each item
                    ' Feb 28 check if url is already in temp table - not needed 
                    'Dim image_URL As String = LeonsBiz.GetURL(sku)

                    'lucy add the if condition
                    If Session("lucy_rf_scan") = "Y" Then
                        'Dim str_row_id As String
                        store_cd = Session("str_store_cd")
                        loc_cd = Session("str_loc_cd")
                        If loc_cd & "" <> "" Then
                            ' Daniela comment, aleady have the str_row_id
                            'lucy_get_row_id(sku, str_row_id)
                            Dim str_rfid As String = Session("rf_id_cd")
                            insert_lucy_pos_rf(Session_String, loc_cd, sku, store_cd, str_rfid, str_row_id)
                            Session("rf_id_cd") = ""
                        End If
                        If isEmpty(loc_cd) Then
                            store_cd = ""
                            Session("rf_id_cd") = ""
                        End If

                    End If
                    ' end lucy add

                    ' extract the TEMP_ITM.ROW_ID (not the Oracle rowid) in case linking a treatment or a warranty
                    If (isInventory AndAlso isWarrantable) OrElse
                        (ConfigurationManager.AppSettings("fab_populate").ToString = "Y" AndAlso treatable = "Y") Then
                        ' Daniela use str_row_id
                        'row_id_seq_num = SalesUtils.GetTempItmRowSeqNum(selectedSkuRowId)
                        row_id_seq_num = str_row_id
                        'MM-6305
                    ElseIf itm_tp_cd = "PKG" Then
                        row_id_seq_num = str_row_id
                    End If

                    'Check the INV_MNR_CAT table for fab protection to auto-insert, if system parameters allow it
                    If treatable = "Y" AndAlso ConfigurationManager.AppSettings("fab_populate").ToString = "Y" AndAlso
                        catcd.isNotEmpty Then

                        sku = SkuUtils.GetTreatmentItmCd(mnrcd, catcd)

                        If sku.isNotEmpty Then

                            ' use the same session variable as previous
                            Dim tempItmReq As HBCG_Utils.CreateTempItmReq = initTempItmReq()
                            tempItmReq.itmCd = sku

                            ' CREATE the treatment TEMP_ITM line
                            Dim createRespFab As HBCG_Utils.CreateTempItmResp = HBCG_Utils.CreateTempItm(tempItmReq)

                            If createRespFab.errMsg.isNotEmpty Then
                                lbl_warnings.Text = createRespFab.errMsg
                                'Exit Function
                                'GoTo TheExit
                                ' no need to test for invalid SKU, we selected this one out of the database
                            End If

                            User_Questions = User_Questions & createRespFab.User_Questions & vbCrLf
                            dropped_dialog = dropped_dialog & createRespFab.dropped_dialog & vbCrLf
                            'FAB SKU cannot be frame or pkg 
                            ' no check on warranty, this is a FAB type SKU; E1 allows warranties on FAB but POS+ has not yet

                            '  Update the treatment link information (temp_itm.row_id) on the treatable SKU
                            Dim updtReq As New TempItmUpdateRequest
                            updtReq.tmpItm = New TempItm
                            row_id_seq_num = SalesUtils.GetTempItmRowSeqNum(createRespFab.insertedSkuRowId)
                            updtReq.tmpItm.treatedBy = row_id_seq_num
                            If updtReq.tmpItm.treatedBy.isNotEmpty Then
                                updtReq.tmpItm.treated = "Y"
                            Else
                                updtReq.tmpItm.treated = ""
                            End If
                            updtReq.treatedByUpdt = True
                            updtReq.treatedUpdt = True
                            updtReq.tmpItm.rowId = selectedSkuRowId  'createResp.insertedSkuRowId
                            updtReq.UpdtBy = temp_itm_key_tp.tempItmByOraRowId
                            ' UPDATE the TEMP_ITM line with the treatment link
                            HBCG_Utils.UpdtTempItm(updtReq)
                        End If
                    End If

                    If PKG_SKUS & "" <> "" Then
                        Dim errorMessage As String = String.Empty
                        Dim isWarrantableComps As Boolean = AddPackages(PKG_SKUS, errorMessage)
                        If Not showWarrPopup AndAlso isWarrantableComps Then showWarrPopup = isWarrantableComps
                        ' MM-6305 
                        ' When there are no components for the Package SKU then an exception will occur  
                        ' Package Parent item needs to be removed from the TEMP_ITM
                        If Not (String.IsNullOrWhiteSpace(errorMessage)) Then
                            HBCG_Utils.deleteTempItmByRowId(row_id_seq_num)
                            Session("itemid") = "NONEENTERED"
                            PKG_SKUS = ""
                            'Return recsAdded
                            'Exit Function
                            recsAdded = IIf(recsAdded > 0, recsAdded - 1, recsAdded)
                        End If
                        PKG_SKUS = ""
                    End If
                Else
                    If preventCount > 0 Then
                        preventBuilder = preventBuilder.Append(",")
                    End If
                    preventBuilder = preventBuilder.Append(sku)
                    preventCount = preventCount + 1
                End If

                If Not showWarrPopup Then
                    showWarrPopup = isInventory AndAlso isWarrantable AndAlso SalesUtils.hasWarranty(sku)
                End If
                If String.IsNullOrEmpty(focusedRowId) Then focusedRowId = row_id_seq_num
            Next

            If showWarrPopup Then ShowWarrantyPopup(focusedRowId)

            If Not preventBuilder.ToString = String.Empty Then
                preventMessageFlag = True
                If preventCount > 1 Then
                    lbl_warnings.Text = String.Format(Resources.POSMessages.MSG0016, preventBuilder.ToString)
                    Session("preventMessageID") = lbl_warnings.Text
                Else
                    lbl_warnings.Text = String.Format(Resources.POSMessages.MSG0018, preventBuilder.ToString)
                    Session("preventMessageID") = lbl_warnings.Text
                End If
            End If

            point_size = "0"
            Dim strScript As String = ""
            'Open up a window for the custom order SKUS so the user can select the appropriate options
            If CUSTOM_SKUS & "" <> "" Then
                CUSTOM_SKUS = Left(CUSTOM_SKUS, Len(CUSTOM_SKUS) - 1)
                If Request("LEAD") = "TRUE" Then
                    ASPxPopupControl10.ContentUrl = "customorderentry.aspx?LEAD=TRUE;itm_cd=" & CUSTOM_SKUS
                Else
                    ASPxPopupControl10.ContentUrl = "customorderentry.aspx?itm_cd=" & CUSTOM_SKUS
                End If
                ASPxPopupControl10.ShowOnPageLoad = True

                'strScript = "<script type='text/javascript'>window.open('customorderentry.aspx?itm_cd=" & CUSTOM_SKUS & "','frmTest','height=390px,width=480px,top=50,left=50,status=no,toolbar=no,menubar=no,scrollbars=yes');</script>"
                'ClientScript.RegisterStartupScript(Me.GetType(), "AvisoScript", strScript)
            End If

            hidItemCd.Value = itemcd

            If Not String.IsNullOrEmpty(pkg_items) Then
                hidItemCd.Value = hidItemCd.Value & pkg_items
            End If

            If SessVar.ordTpCd = AppConstants.Order.TYPE_SAL AndAlso
                (Not String.IsNullOrEmpty(hidItemCd.Value)) Then
                SessVar.discReCalc.maybeRelated = True
            End If

            Dim hasRelatedSKU As Boolean = False

            If hidWarrantyCheck.Value = 0 Then
                hasRelatedSKU = True
                Dim relatedSku As String = hidItemCd.Value
                hidItemCd.Value = String.Empty

                ShowRelatedSkuPopUp(relatedSku)
            End If

            Session("itemid") = "NONEENTERED"

            If User_Questions & "" <> "" Then
                lbl_questions.Text = User_Questions
            End If

            objConnection.Close()
            conn.Close()

            If auto_add = True Then
                If Not hasRelatedSKU And String.IsNullOrEmpty(CUSTOM_SKUS) Then
                    'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
                    'if will reapply it, if ANY of the lines added qualify for the discount.
                    ' if we are here and going to order, then we aren't coming back to order_detail to re-calc discount so do it now
                    If SessVar.LastLnSeqNum.isNotEmpty() Then

                        ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                    End If
                    Session("Add_More_Items") = "TRUE"  ' (insert_records) tell the order page to track may cycle back with ability to add more if other conditions pass
                    calculate_total(Page.Master)
                    If Request("LEAD") = "TRUE" Then
                        Response.Redirect("Order.aspx?LEAD=TRUE&auto_add=true&itm_cd=" & sku & "&vsn=" & vsn & "&ret_prc=" & pass_ret_prc)
                    Else
                        Response.Redirect("Order.aspx?auto_add=true&itm_cd=" & sku & "&vsn=" & vsn & "&ret_prc=" & pass_ret_prc)
                    End If
                End If
            End If
        End If

        txt_SKU.Focus()
        new_special.Text = "FALSE"

        Return recsAdded
    End Function

    Private Function initTempItmReq() As HBCG_Utils.CreateTempItmReq

        Dim tempItmReq As HBCG_Utils.CreateTempItmReq = New HBCG_Utils.CreateTempItmReq()

        tempItmReq.sessId = Session.SessionID.ToString.Trim
        tempItmReq.empCd = Session("emp_cd")
        If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
        tempItmReq.tranDt = Session("tran_dt")
        tempItmReq.defLocCd = ""    'Session("loc_cd") - only in CC Express
        tempItmReq.newSpecOrd = False
        tempItmReq.takeWith = IIf(SessVar.cashNCarry = "TRUE", True, False)
        tempItmReq.warrRowLink = 0
        tempItmReq.warrItmLink = ""
        tempItmReq.soHdrInf.isCashNCarry = SessVar.cashNCarry
        tempItmReq.soHdrInf.writtenDt = Session("tran_dt")
        tempItmReq.soHdrInf.writtenStoreCd = Session("store_cd")
        tempItmReq.soHdrInf.custTpPrcCd = SessVar.custTpPrcCd
        tempItmReq.soHdrInf.orderTpCd = SessVar.ordTpCd
        tempItmReq.soHdrInf.slsp1 = Session("slsp1")
        tempItmReq.soHdrInf.slsp2 = IIf(IsNothing(Session("slsp2")), "", Session("slsp2"))
        tempItmReq.soHdrInf.slspPct1 = Session("pct_1")
        tempItmReq.soHdrInf.slspPct2 = Session("pct_2")
        tempItmReq.soHdrInf.taxExemptCd = IIf(IsNothing(Session("tet_cd")), "", Session("tet_cd"))
        tempItmReq.soHdrInf.taxCd = If(IsDBNull(Session("tax_cd")) OrElse IsNothing(Session("tax_cd")), "", Session("tax_cd"))

        Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(tempItmReq.soHdrInf.writtenDt, tempItmReq.soHdrInf.taxCd)
        If taxPcts IsNot Nothing Then
            tempItmReq.soHdrInf.taxRates = taxPcts.itmTpPcts
        End If
        Return tempItmReq

    End Function

    ''' Calls an API to calculate availability for a store that has been setup to use
    ''' advanced reservation logic (ARS).
    ''' </summary>
    ''' <param name="isInventory">if the sku is 'inventoriable' or not</param>
    ''' <param name="sku">the item for which to check availability</param>
    ''' <param name="qty">the qty needed</param>
    ''' <returns>the available date</returns>
    Private Function GetAvailDate(ByVal isInventory As Boolean, ByVal sku As String, ByVal qty As Double) As Date

        Dim availDt As Date = Today.Date
        Dim isSale As Boolean = If(Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString, True, False)
        Dim isNotCashCarry As Boolean = If(IsNothing(Session("cash_carry")) OrElse Session("cash_carry") = "FALSE", True, False)

        'calculate availability for ARS store, if any 
        If isInventory AndAlso isSale AndAlso Session("STORE_ENABLE_ARS") AndAlso isNotCashCarry Then
            availDt = theInvBiz.CalcAvailARS(sku, Session("PD"), Session("ZONE_CD"), qty, Session("pd_store_cd"))
        End If

        Return availDt
    End Function

    Private Function GetDefaultLocation(ByVal store_cd As String) As String

        Dim defaultLoc As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim Mydatareader As OracleDataReader
        Dim sql As String = "SELECT DEFAULT_LOC FROM STORE WHERE STORE_CD='" & store_cd & "'"

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            If Mydatareader.Read() Then
                If Mydatareader.Item("DEFAULT_LOC").ToString & "" <> "" Then
                    defaultLoc = Mydatareader.Item("DEFAULT_LOC").ToString
                End If
            End If
            conn.Close()
        Catch
            conn.Close()
            Throw
        End Try
        Return defaultLoc

    End Function

    ' TODO - the next two methods need to be changed to use the HBCG_Utils versions.
    ''' <summary>
    ''' Creates temp_itm table entry for the warranty record and update the link to the warranty on the applicable warrantable SKU 
    ''' </summary>
    ''' <param name="warrItmCd">warranty SKU</param>
    ''' <param name="itmRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid)  for the warrantable item to link</param>
    Private Function CreateWarrantyLn(ByVal warrItmCd As String, ByVal itmRowIdSeqNum As String) As String

        Dim warrRowIdSeqNum As String = String.Empty

        Dim req As HBCG_Utils.CreateTempItmReq = New CreateTempItmReq()
        req.itmCd = warrItmCd
        req.sessId = Session.SessionID.ToString.Trim
        req.empCd = Session("emp_cd")
        req.soHdrInf.orderTpCd = Session("ord_tp_cd")
        req.soHdrInf.writtenDt = Session("tran_dt") 'FormatDateTime(Today.Date, DateFormat.ShortDate)
        'If (Not String.IsNullOrWhiteSpace(soHdrInf.soWrDt.ToString)) AndAlso IsDate(soHdrInf.soWrDt.ToString) Then
        '    req.soWrDt = FormatDateTime(soHdrInf.soWrDt, DateFormat.ShortDate)
        'End If
        If (Not IsNothing(Session("tran_dt"))) AndAlso SystemUtils.isNotEmpty(Session("tran_dt")) AndAlso IsDate(Session("tran_dt")) Then
            req.tranDt = Session("tran_dt")
        Else
            req.tranDt = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
        End If
        req.soHdrInf.writtenStoreCd = Session("store_cd")
        'If (Not String.IsNullOrWhiteSpace(soHdrInf.soStoreCd)) Then
        '    req.soStoreCd = soHdrInf.soStoreCd  'Session("store_cd")
        'Else
        '    req.soStoreCd = ""
        'End If
        req.soHdrInf.custTpPrcCd = Session("cust_tp_prc_cd")
        'If (Not String.IsNullOrWhiteSpace(x)) Then
        '    req.custTpPrcCd = Session("cust_tp_prc_cd")
        'Else
        '    req.custTpPrcCd = ""
        'End If
        req.defLocCd = ""
        req.newSpecOrd = False
        req.soHdrInf.slsp1 = Session("slsp1")
        If (Not IsNothing(Session("slsp2"))) AndAlso SystemUtils.isNotEmpty(Session("slsp2")) Then
            req.soHdrInf.slsp2 = Session("slsp2")
        End If
        req.soHdrInf.slspPct1 = Session("pct_1")
        If (Not IsNothing(Session("pct_2"))) AndAlso SystemUtils.isNotEmpty(Session("pct_2")) Then
            req.soHdrInf.slspPct2 = Session("pct_2")
        End If
        req.warrRowLink = CInt(itmRowIdSeqNum)
        req.warrItmLink = warrItmCd  ' this is perfect for one to one and for one to many it doesn't matter so will be first one
        req.soHdrInf.taxExemptCd = Session("tet_cd")
        req.soHdrInf.taxCd = If(IsDBNull(Session("TAX_CD")) = True OrElse "" & Session("TAX_CD") = "", "", Session("TAX_CD"))

        If IsDBNull(Session("TAX_CD")) = False AndAlso Session("TAX_RATE") <> Nothing Then
            req.soHdrInf.taxRates.war = Session("TAX_RATE")   ' TODO - this is just wrong - there is no session rate except for inv so this - finish implementing tax rates
        Else
            req.soHdrInf.taxRates.war = 0
        End If
        ' value might be wrong - TBD when rest are fixed
        Dim resp As HBCG_Utils.CreateTempItmResp = HBCG_Utils.CreateTempItm(req)

        If (Not IsNothing(resp)) AndAlso resp.insertedSkuRowId.isNotEmpty Then

            '  Update the warranty link information (itm_cd, temp_itm.row_id) on the warrantable SKU
            warrRowIdSeqNum = SalesUtils.GetTempItmRowSeqNum(resp.insertedSkuRowId)
            UpdtWarrLnk(warrItmCd, warrRowIdSeqNum, itmRowIdSeqNum)
        End If
        Return warrRowIdSeqNum
    End Function

    ''' <summary>
    ''' update the warranty link information on the applicable warrantable SKU in the temp_itm table
    ''' </summary>
    ''' <param name="warrItmCd">warranty SKU</param>
    ''' <param name="warrRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid) for the warranty item to link</param>
    ''' <param name="itmRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid) for the warrantable item to link</param>
    Private Sub UpdtWarrLnk(ByVal warrItmCd As String, ByVal warrRowIdSeqNum As String, ByVal itmRowIdSeqNum As String)

        'Session.SessionID.ToString.Trim
        'If Not IsNothing(resp)) AndAlso resp.insertedSkuRowId.isNotEmpty Then   ' TODO DSA - clean up further validation maybe
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim thistransaction As OracleTransaction = conn.BeginTransaction

        With cmd
            .Transaction = thistransaction
            .Connection = conn
            .CommandText = "UPDATE TEMP_ITM SET warr_itm_link = :itmCd, warr_row_link = :warrRowNum WHERE row_id = :selSkuRowNum "
            .Parameters.Add(":itmCd", OracleType.VarChar).Direction = ParameterDirection.Input
            .Parameters(":itmCd").Value = warrItmCd     'Request("warrItmCd")
            .Parameters.Add(":warrRowNum", OracleType.Double).Direction = ParameterDirection.Input
            .Parameters(":warrRowNum").Value = CInt(warrRowIdSeqNum)    'Request("warrItmRowNum")
            .Parameters.Add(":selSkuRowNum", OracleType.Double).Direction = ParameterDirection.Input
            .Parameters(":selSkuRowNum").Value = CInt(itmRowIdSeqNum)   'Request("itmRowNum")
        End With
        cmd.ExecuteNonQuery()
        thistransaction.Commit()
        conn.Close()
        'End If

    End Sub

    ' TODO - this will not handle multiple package SKUs - does it need to
    Private Function AddPackages(ByVal pkgSkus As String, Optional ByRef errorMessage As String = "") As Boolean
        Dim showWarrPopup As Boolean = False
        ' create the object for header and transaction info
        Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                               Session("slsp1"),
                                                               If(String.IsNullOrEmpty(Session("slsp2")), "", Session("slsp2")),
                                                               Session("pct_1"),
                                                               IIf(IsNothing(Session("pct_2")), 0, Session("pct_2")),
                                                               Session("tet_cd"), If(IsDBNull(Session("TAX_CD")), "", Session("TAX_CD")),
                                                               Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                               Session("PD"), Session("pd_store_cd"),
                                                               Session("cash_carry"))

        If (Not IsNothing(Session("TAX_CD")) AndAlso Not IsDBNull(Session("TAX_CD")) AndAlso Not String.IsNullOrEmpty(Session("TAX_CD"))) Then

            headerInfo.writtenDt = Session("tran_dt")
            Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(headerInfo.writtenDt, headerInfo.taxCd)

            headerInfo.taxRates = taxPcts.itmTpPcts  'SetTxPcts(headerInfo.soWrDt, headerInfo.taxCd).itmTpPcts
        Else
            Dim taxPcts As TaxUtils.taxPcts = New TaxUtils.taxPcts
            HttpContext.Current.Session("taxPcts") = taxPcts
            headerInfo.taxRates = New ItemTypeTaxAmtsDtc()
        End If

        ' create pkg components in temp_itm
        Dim tmpCmpnts As DataTable = theSalesBiz.AddPkgCmpntsToTempItm(headerInfo,
                                                                       pkgSkus,
                                                                       Session.SessionID.ToString.Trim, Session("store_cd"), errorMessage)
        Dim drFilter() As DataRow = tmpCmpnts.Select("WARRANTABLE = 'Y' AND INVENTORY = 'Y'")
        For Each drFil As DataRow In drFilter
            If Not showWarrPopup Then showWarrPopup = SalesUtils.hasWarranty(drFil("ITM_CD"))
        Next

        'If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
        '    theSalesBiz.addSplitDataToSession(headderRorID, tmpCmpnts)
        'End If

        ' related SKUs uses the component list and requires a comma delimited string of single quoted item codes
        ' do not allow duplicates in the component list  - happens when qty > 1
        lbl_warnings.Value = True
        lbl_warnings.Text = errorMessage.ToString()

        Dim cmpnts As DataTable = tmpCmpnts.DefaultView.ToTable(True, "itm_cd")
        Dim cmpntList As New StringBuilder

        For Each datRow In cmpnts.Rows
            cmpntList.Append(",'" & datRow("itm_cd") & "'")
        Next

        pkg_items = cmpntList.ToString
        Return showWarrPopup
    End Function

    Public Sub bindgrid()
        Dim orderLnsDataSet As DataSet = Nothing
        Dim orderLnsDataTbl As DataTable = Nothing
        Dim isRelationship As Boolean = (Not IsNothing(Request.QueryString("LEAD")) AndAlso Request.QueryString("LEAD") = "TRUE")
        Dim priceChangeSKU As String = String.Empty
        chk_slsp_view_all.Visible = False
        chk_reserve_all.Visible = False
        Gridview1.DataSource = ""
        Gridview1.Visible = True

        orderLnsDataSet = theSalesBiz.GetLineItemsForSOE(Session.SessionID.ToString(), False, Session("ord_tp_cd"))
        orderLnsDataSet.Tables(0).Columns.Add("RelationshipPrice", System.Type.GetType("System.String"))

        'Remove the voided Warranties

        If Not IsNothing(orderLnsDataSet) AndAlso orderLnsDataSet.Tables.Count > 0 Then
            orderLnsDataTbl = orderLnsDataSet.Tables(0)
            ' Daniela remove warranty only if linked
            'Dim drWarSKUs As DataRow() = orderLnsDataTbl.Select("TYPECODE = '" & AppConstants.Sku.TP_WAR & "'")

            'For Each dr As DataRow In drWarSKUs
            '    Dim drWarrRowId As DataRow() = orderLnsDataTbl.Select("WARR_ROW_LINK = '" & dr("ROW_ID") & "'")
            '    If drWarrRowId.Length < 1 Then
            '        HBCG_Utils.deleteTempItmByRowId(dr("ROW_ID"))
            '        Dim drWarRow As DataRow() = orderLnsDataTbl.Select("ROW_ID = '" & dr("ROW_ID") & "'")
            '        If drWarRow.Length > 0 Then
            '            orderLnsDataTbl.Rows(orderLnsDataTbl.Rows.IndexOf(drWarRow(0))).Delete()
            '            orderLnsDataTbl.AcceptChanges()
            '        End If
            '    End If
            'Next
            ' Daniela end
        End If

        If Not IsNothing(Session("Converted_REL_NO")) AndAlso Not IsNothing(Session("priceChangeDictionary")) Then
            Dim RelanPriceChange As New List(Of RelationshipLineDetails)
            RelanPriceChange = CType(Session("priceChangeDictionary"), List(Of RelationshipLineDetails))
            priceChangeSKU = theSalesBiz.AddRelationshipPrice(orderLnsDataSet, RelanPriceChange)
            Gridview1.Columns(SoLnGrid.RELN_PRC).Visible = True
            If Not String.IsNullOrEmpty(priceChangeSKU) Then
                lblErrorMsg.Visible = True
                lblErrorMsg.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
                lblErrorMsg.Text = String.Format(Resources.POSMessages.MSG0045, String.Join(",", priceChangeSKU.ToString.Split(",").AsEnumerable().Distinct()))
            End If
        End If

        If (orderLnsDataTbl.Rows.Count > 0) Then
            '*****ViewState holds row items with prevent flag yes or no *****'
            'AVOIDS caching ALL columns when only these few are needed
            Dim orderLnsDataView As DataView = New DataView(orderLnsDataTbl)
            ViewState("ItemTable") = orderLnsDataView.ToTable(False, "ROW_ID", "PREVENT", "TYPECODE", "RET_PRC")

            chk_slsp_view_all.Visible = (Not isRelationship)
            'only for SALEs, Reserve ALL should be allowed
            chk_reserve_all.Visible = (Not isRelationship AndAlso Session("ord_tp_cd") = "SAL")

            Gridview1.DataSource = orderLnsDataSet
            Gridview1.DataBind()

            ' Daniela calculate only if rows found
            calculate_total(Page.Master)
            DoAfterBindgrid()
        Else
            Session("itemid") = System.DBNull.Value
            Session("sub_total") = 0
        End If

        ' Daniela comment
        ' calculate_total(Page.Master)
        ' DoAfterBindgrid()
    End Sub

    ''' <summary>
    ''' Fetches the max row_id (line sequence number, not db rowid) from temp_itm for the session and sets in a session variable; 
    ''' initially used by discounting recalculation processing
    ''' </summary>
    Protected Sub SetupForDiscReCalc()

        'Prepares data for later use in discount recalculation (if applicable)
        SessVar.LastLnSeqNum = String.Empty
        If AppConstants.Order.TYPE_SAL = SessVar.ordTpCd Then

            If (theSalesBiz.GetHeaderDiscountCode(Session.SessionID.ToString().Trim, True).isNotEmpty()) Then

                'fetches last lineSeq, to recalculate (if applicable) due to any of the new lines
                SessVar.LastLnSeqNum = theSalesBiz.GetLastLnSeqNum(Session.SessionID.ToString().Trim)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Determines if a warranty or related SKU may be added later and if so, postpones re-applying the header discount(s); otherwise
    ''' re-applies the discounts here 
    ''' </summary>
    Protected Sub DoDiscReCalcNowOrLater()

        ' if we might add more records from warr or related, then re-calc discount later
        If SessVar.discReCalc.maybeWarr = True OrElse SessVar.discReCalc.maybeRelated = True Then

            SessVar.discReCalc.needToReCalc = True

        Else
            'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
            'if will reapply it, if ANY of the lines added qualify for the discount.
            If SessVar.LastLnSeqNum.isNotEmpty() Then

                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
            End If
        End If  ' TODO - maybe we shouldn't bindgrid yet either

    End Sub

    Protected Sub cmd_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmd_add.Click

        'lbl_warnings.Text = " " ' Daniela reset message
        If txt_SKU.Text & "" <> "" Then

            txt_SKU.Text = txt_SKU.Text.Trim

            'start lucy add
            If Session("cash_carry") = "TRUE" Then
                If lucy_invent_itm(UCase(txt_SKU.Text)) = "Y" Then
                    'Daniela french
                    'lbl_warnings.Text = "Inventory Sku is only allowed to be scanned"
                    lbl_warnings.Text = Resources.LibResources.Label704
                    Exit Sub
                End If
            End If
            ' Daniela use common function
            Dim usr_flag = LeonsBiz.sec_check_itm(UCase(txt_SKU.Text), Session("emp_cd"))
            If usr_flag = "N" Then  'multi co
                'Daniela french
                'lbl_warnings.Text = "You are not authorized to select that SKU"
                lbl_warnings.Text = Resources.LibResources.Label673
                Exit Sub
            End If

            ' end lucy add

            'Prepares data for later use in discount recalculation (if applicable)
            SetupForDiscReCalc()    ' cmd_add_click, adding a line from this page

            If UCase(txt_SKU.Text.Trim()) = "S" Then
                If Request("LEAD") = "TRUE" Then
                    Response.Redirect("special_order.aspx?LEAD=TRUE")
                Else
                    Response.Redirect("special_order.aspx")
                End If
            End If

            'Removing the white spaces
            Dim items() As String = UCase(txt_SKU.Text).Trim(" ", ",").Split(",").[Select](Function(g) g.Trim()).ToArray()
            'Distint the entry by removing the duplicates
            Dim enteredSKUs As String = String.Join(",", items.AsEnumerable().Distinct())

            If Not IsNothing(Session("itemid")) And Not IsDBNull(Session("itemid")) Then
                If Session("itemid") = "NONEENTERED" Then
                    Session("itemid") = enteredSKUs
                Else
                    Session("itemid") = Session("itemid") & "," & enteredSKUs
                End If
            Else
                Session("itemid") = enteredSKUs
            End If

            'Add the SKU
            insert_records()

            ' if we might add more records from warr or related, then re-calc discount later, otherwise re-calc now if appropriate
            DoDiscReCalcNowOrLater()

            'Populate Datagrid with new records
            bindgrid()

        ElseIf Len(Trim(txt_SKU.Text)) < 2 And Session("cash_carry") = "TRUE" Then  'lucy add this 2 lines
            Response.Redirect("rf_scan.aspx")

        Else
            ' Daniela french added
            'lbl_warnings.Text = "Please enter a SKU"
            lbl_warnings.Text = "Please enter a SKU"
        End If

        txt_SKU.Text = String.Empty
    End Sub

    'Gets fired when any button is clicked in the Data Grid
    Protected Sub Gridview1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.ItemCommand
        Try
            Dim Lead As String = ""
            If Request("LEAD") = "TRUE" Then
                Lead = "&LEAD=TRUE"
            End If
            Select Case e.CommandName
                Case "find_inventory"
                    lbl_warnings.Text = ""
                    If ConfigurationManager.AppSettings("inv_avail").ToString = "CUSTOM" Then
                        ASPxPopupControl3.ContentUrl = "Custom_Availability.aspx?SKU=" & e.Item.Cells(SoLnGrid.ITM_CD).Text & Lead & "&ROW_ID=" & Gridview1.DataKeys(e.Item.ItemIndex) & "&INV_SELECTION=Y&referrer=order_detail.aspx"
                    Else
                        ASPxPopupControl3.ContentUrl = "InventoryLookup.aspx?SKU=" & e.Item.Cells(SoLnGrid.ITM_CD).Text & Lead & "&ROW_ID=" & Gridview1.DataKeys(e.Item.ItemIndex) & "&INV_SELECTION=Y&referrer=order_detail.aspx"
                    End If
                    ASPxPopupControl3.ShowOnPageLoad = True
                Case "inventory_lookup"
                    ' FYI - THIS IS ACTUALLY THE SKU DETAIL FROM SOE+ 
                    ASPxPopupControl4.ContentUrl = "Special_Order_Popup.aspx?SKU=" & e.Item.Cells(SoLnGrid.ITM_CD).Text & Lead & "&ROW_ID=" & Gridview1.DataKeys(e.Item.ItemIndex) & "&SPEC_ORD=" & e.Item.Cells(SoLnGrid.NEW_SPEC_ORD).Text
                    ASPxPopupControl4.ShowOnPageLoad = True
                Case "remove_po"
                    Dim sql As String
                    Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    Dim objSql As OracleCommand

                    'Open Connection 
                    conn.Open()

                    sql = "UPDATE temp_itm set PO_CD=NULL where row_id=" & e.Item.Cells(SoLnGrid.ROW_ID).Text & " AND session_id='" & Session.SessionID.ToString & "'"

                    'Set SQL OBJECT 
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()
                    conn.Close()
                    Response.Redirect("order_detail.aspx?" & Lead)
                Case "find_gm"
                    Dim sql As String
                    Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                    Dim objSql As OracleCommand
                    Dim MyDataReader As OracleDataReader
                    Dim frt_factor As Double = 0
                    Dim repl_cst As Double = 0
                    Dim ret_prc As Double = 0
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

                    Dim txt_ret_prc As TextBox
                    txt_ret_prc = CType(e.Item.FindControl("ret_prc"), TextBox)

                    If IsNumeric(txt_ret_prc.Text) Then ret_prc = CDbl(txt_ret_prc.Text)

                    conn.Open()

                    sql = "select repl_cst, frt_fac from itm where itm_cd='" & e.Item.Cells(SoLnGrid.ITM_CD).Text & "'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    Try
                        'Execute DataReader 
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        'Store Values in String Variables 
                        If MyDataReader.Read() Then
                            If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                            If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                        End If
                        MyDataReader.Close()
                    Catch
                        conn.Close()
                        Throw
                    End Try
                    conn.Close()
                    If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                    lbl_gm.Text = "Retail: " & FormatCurrency(ret_prc, 2) & "<br />"
                    lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(repl_cst, 2) & "<br />"
                    lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(ret_prc - repl_cst, 2) & "<br />"
                    lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((ret_prc - repl_cst) / ret_prc, 2) & "<br />"
                    ASPxPopupControl6.ShowOnPageLoad = True
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub Gridview1_DeleteCommand(source As Object, e As DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        'Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim cmdSelect As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim datRdr As OracleDataReader
        Dim deleteRowId As String
        Dim intRow As Integer = 0
        Dim intRows As Integer = Gridview1.Items.Count - 1
        Dim bln_match As Boolean = False
        Dim sql As String
        Dim treated_by As String = ""
        Dim resId As String = ""
        Dim packageParent As String = String.Empty
        Dim delITM As String = String.Empty
        Dim storeCD As String = String.Empty
        Dim pakgHeaderRowID As String = String.Empty
        deleteRowId = Gridview1.DataKeys(e.Item.ItemIndex)
        Dim popupPrcChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
        If recsDeleted.Contains(deleteRowId) Then
            Exit Sub
        Else
            recsDeleted.Add(deleteRowId)
        End If

        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True AndAlso (popupMgrOverride.ShowOnPageLoad = True)) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If
        lbl_warnings.Text = ""

        'We need to find any fabric protection if linked to ensure that we delete those records as well.
        'also, needs to find if reservations exist, to remove them
        sql = "SELECT ITM_CD, QTY, TREATED_BY, STORE_CD, LOC_CD, RES_ID, PACKAGE_PARENT,ROW_ID FROM TEMP_ITM WHERE ROW_ID=" & deleteRowId
        Try
            conn.Open()
            cmdSelect = DisposablesManager.BuildOracleCommand(sql, conn)
            datRdr = DisposablesManager.BuildOracleDataReader(cmdSelect)


            If (datRdr.Read()) Then
                treated_by = datRdr.Item("TREATED_BY").ToString
                resId = datRdr.Item("RES_ID").ToString
                delITM = datRdr.Item("ITM_CD").ToString
                packageParent = datRdr.Item("PACKAGE_PARENT").ToString
                storeCD = datRdr.Item("STORE_CD").ToString
                pakgHeaderRowID = datRdr.Item("ROW_ID").ToString
                ' ---- REMOVE PENDING (SOFT) RESERVATIONS WHEN PRESENT
                If (resId.isNotEmpty) Then
                    Dim request As UnResRequestDtc = GetRequestForUnreserve(datRdr.Item("RES_ID").ToString,
                                                                            datRdr.Item("ITM_CD").ToString,
                                                                            datRdr.Item("STORE_CD").ToString,
                                                                            datRdr.Item("LOC_CD").ToString,
                                                                            datRdr.Item("QTY").ToString)
                    theInvBiz.RemoveSoftRes(request)
                End If
            End If
            datRdr.Close()
        Catch ex As Exception
            datRdr.Close()
            conn.Close()
            Throw
        End Try

        'Delete corresponding treatment record
        If treated_by & "" <> "" Then
            With cmd
                .Connection = conn
                .CommandText = "delete from temp_itm where row_id = " & treated_by
            End With
            cmd.ExecuteNonQuery()
            hidUpdLnNos.Value = hidUpdLnNos.Value.Replace(";" & treated_by, "")
        End If

        ' delete relationship records for the deleting line
        With cmd
            .Connection = conn
            .CommandText = "delete from relationship_lines_desc where line='" & deleteRowId & "' and rel_no='" & Session.SessionID.ToString & "'"
        End With
        cmd.ExecuteNonQuery()

        ' if SKU to be deleted, was a treatment, then remove the links to it
        With cmd
            .Connection = conn
            .CommandText = "update temp_itm set treated ='N', treated_by='' where treated_by = '" & deleteRowId & "'"
        End With
        cmd.ExecuteNonQuery()

        With cmd
            .Parameters.Clear()
            .Connection = conn
            .CommandText = "delete from temp_itm where row_id = " & deleteRowId
        End With
        cmd.ExecuteNonQuery()

        ' start lucy add 
        With cmd
            .Connection = conn
            .CommandText = "delete from lucy_pos_rf_id where row_id = " & deleteRowId
        End With
        cmd.ExecuteNonQuery()
        ' end lucy add

        'If it is a package SKU, then we need to delete the treatment on any package component 
        If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then
            'theSalesBiz.removeAllSplitPkgItms(pakgHeaderRowID)
            sql = "SELECT TREATED_BY FROM TEMP_ITM WHERE PACKAGE_PARENT=" & deleteRowId & " AND TREATED_BY IS NOT NULL"
            cmdSelect = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                datRdr = DisposablesManager.BuildOracleDataReader(cmdSelect)

                Do While datRdr.Read()
                    With cmd
                        .Connection = conn
                        .CommandText = "delete from temp_itm where row_id = " & datRdr.Item("TREATED_BY").ToString
                    End With
                    cmd.ExecuteNonQuery()
                Loop
                datRdr.Close()
            Catch
                datRdr.Close()
                Throw
            End Try

            'After confirmation, need to delete the warranty linked with package componets
            'Need to implement same as: RemoveWarrLink(e.Item)

            ' delete the package components
            With cmd
                .Connection = conn
                .CommandText = "delete from temp_itm where package_parent = " & deleteRowId
            End With
            cmd.ExecuteNonQuery()

        End If
        Dim ret_prc As Double = 0
        ' if the deleting SKU is a component of a package, then 
        If IsNumeric(e.Item.Cells(SoLnGrid.PKG_LN).Text) Then

            ' this is scary, so the original item unit price is on the 
            If IsNumeric(e.Item.Cells(SoLnGrid.TAX_BASIS).Text) Then

                With cmd
                    .Connection = conn
                    .CommandText = "update temp_itm set ret_prc = ret_prc-" & e.Item.Cells(SoLnGrid.TAX_BASIS).Text & " where ret_prc > 0 and row_id=" & e.Item.Cells(SoLnGrid.PKG_LN).Text
                End With
                cmd.ExecuteNonQuery()

                sql = "SELECT RET_PRC FROM TEMP_ITM WHERE row_id=" & e.Item.Cells(SoLnGrid.PKG_LN).Text
                cmdSelect = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    datRdr = DisposablesManager.BuildOracleDataReader(cmdSelect)

                    If datRdr.Read Then
                        Modify_Package_Price(e.Item.Cells(SoLnGrid.PKG_LN).Text, e.Item.Cells(SoLnGrid.ITM_CD).Text, datRdr.Item("RET_PRC").ToString)
                    End If
                    datRdr.Close()
                Catch
                    datRdr.Close()
                    Throw
                End Try
            End If
        End If

        ' if warranty, remove links from warrantables; if warrantable, if linked to warranty and is last on warranty, then delete warranty.
        If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = SkuUtils.ITM_TP_WAR Then

            With cmd
                .Connection = conn
                .CommandText = "update temp_itm set warr_itm_link = NULL, warr_row_link = NULL WHERE warr_row_link = :delId "
                .Parameters.Add(":delId", OracleType.VarChar)
                .Parameters(":delId").Value = deleteRowId
            End With
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        ElseIf "Y".Equals(e.Item.Cells(SoLnGrid.WARRANTABLE).Text) AndAlso e.Item.Cells(SoLnGrid.WARR_ROW_LINK).Text <> "" Then
            RemoveWarrLink(e.Item)
        End If

        cmdSelect.Dispose()
        cmd.Dispose()
        conn.Close()

        'reapplies the discounts (if any) when applicable
        ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Void, String.Empty)
        Session("modifieditems") = hidUpdLnNos.Value.Replace(";" & deleteRowId, "")
        If Request("LEAD") = "TRUE" Then
            Response.Redirect("order_detail.aspx?LEAD=TRUE")
        Else
            Response.Redirect("order_detail.aspx")
        End If
    End Sub

    ''' <summary>
    ''' This event is fired after an item is data bound to the DataGrid. So it will be called once for each row
    ''' in the datagrid. This event provides the last opportunity to access the data item before being displayed 
    ''' on the client.
    ''' </summary>
    Public Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim cash_carry As Boolean = False

        conn.Open()


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim objsql2 As OracleCommand
        Dim store_cd As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
        Dim loc_cd As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)
        Dim lbl_PO As Label = CType(e.Item.FindControl("lbl_PO"), Label)
        Dim img_PO As Image = CType(e.Item.FindControl("img_PO"), Image)

        If Not IsNothing(lbl_PO) And Request("LEAD") <> "TRUE" And IsNumeric(e.Item.Cells(SoLnGrid.ROW_ID).Text) Then
            sql = "select po_cd from temp_itm where row_id=" & e.Item.Cells(SoLnGrid.ROW_ID).Text & " AND session_id='" & Session.SessionID.ToString & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    If Mydatareader.Item("PO_CD").ToString & "" <> "" Then
                        lbl_PO.Text = "PO: " & Mydatareader.Item("PO_CD").ToString
                        If Not IsNothing(img_PO) Then
                            img_PO.Visible = True
                        End If
                        If Not IsNothing(store_cd) Then
                            store_cd.Enabled = False
                        End If
                        If Not IsNothing(loc_cd) Then
                            loc_cd.Enabled = False
                        End If
                    Else
                        If Not IsNothing(img_PO) Then
                            img_PO.Visible = False
                            lbl_PO.Text = ""
                        End If
                    End If
                Else
                    If Not IsNothing(img_PO) Then
                        img_PO.Visible = False
                        lbl_PO.Text = ""
                    End If
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
        End If

        If Request("LEAD") = "TRUE" Or Session("ord_tp_cd") = "CRM" Then
            e.Item.Cells(SoLnGrid.STORE_CD).Visible = False
            e.Item.Cells(SoLnGrid.LOC_CD).Visible = False
            e.Item.Cells(SoLnGrid.TW_CHKBOX).Visible = False
        End If
        If FAB_FOUND <> "Y" Then

            sql = "SELECT itm_tp_cd, take_with from temp_itm where session_id='" & Session.SessionID.ToString() & "'"

            sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
            ds = New DataSet
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            For loop1 = 0 To numrows - 1
                If MyTable.Rows(loop1).Item("ITM_TP_CD").ToString = "FAB" Then
                    FAB_FOUND = "Y"
                    treatmentFound = True
                End If
                If FAB_FOUND <> "Y" Then
                    e.Item.Cells(SoLnGrid.FAB_CHKBOX).Visible = False
                Else
                    e.Item.Cells(SoLnGrid.FAB_CHKBOX).Visible = True
                End If
            Next
        End If

        'for whatever reason, we could be processing an 'empty' grid row(such as a template header). In that case the column values will be
        'initialized to the col header text and not the actual db value. Therefore, check for that before making any db calls or processing further
        If e.Item.Cells(SoLnGrid.ITM_CD).Text.isNotEmpty AndAlso Not (e.Item.Cells(SoLnGrid.ITM_CD).Text.ToUpper.Equals("SKU".ToUpper)) Then
            sql = "SELECT DROP_CD, DROP_DT FROM ITM WHERE ITM_CD='" & Replace(e.Item.Cells(SoLnGrid.ITM_CD).Text, "'", "''") & "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If (Mydatareader.Read()) Then
                    If IsDate(Mydatareader.Item("DROP_DT").ToString) Then
                        Dim lbl_itm_warning As Label = CType(e.Item.FindControl("lbl_itm_warning"), Label)
                        lbl_itm_warning.Text = "SKU was dropped on " & FormatDateTime(Mydatareader.Item("DROP_DT").ToString, DateFormat.ShortDate) & " (" & Mydatareader.Item("DROP_CD").ToString & ")"
                    End If
                End If
            Catch
                conn.Close()
                Throw
            End Try

            If (chk_slsp_view_all.Checked) Then
                If slsp_ds.Tables.Count < 1 Then slsp_ds = LeonsBiz.lucy_RetrieveSalespersons(UCase(Session("emp_cd")))

                Dim cbo_slsp1 As DropDownList = CType(e.Item.FindControl("cbo_slsp1"), DropDownList)
                Dim cbo_slsp2 As DropDownList = CType(e.Item.FindControl("cbo_slsp2"), DropDownList)
                Dim txt_slsp1_pct As TextBox = CType(e.Item.FindControl("slsp1_pct"), TextBox)
                Dim txt_slsp2_pct As TextBox = CType(e.Item.FindControl("slsp2_pct"), TextBox)
                If Not IsNothing(cbo_slsp1) Then
                    cbo_slsp1.DataSource = slsp_ds
                    cbo_slsp1.DataValueField = "EMP_CD"
                    cbo_slsp1.DataTextField = "FullNAME"
                    cbo_slsp1.DataBind()
                    cbo_slsp2.DataSource = slsp_ds
                    cbo_slsp2.DataValueField = "EMP_CD"
                    cbo_slsp2.DataTextField = "FullNAME"
                    cbo_slsp2.DataBind()
                    If e.Item.Cells(SoLnGrid.SLSP1).Text & "" <> "" Then
                        cbo_slsp1.SelectedValue = e.Item.Cells(SoLnGrid.SLSP1).Text
                        txt_slsp1_pct.Text = e.Item.Cells(SoLnGrid.SLSP_PCT1).Text
                    End If
                    cbo_slsp2.Items.Insert(0, "Please Select A Salesperson")
                    cbo_slsp2.Items.FindByText("Please Select A Salesperson").Value = ""
                    cbo_slsp2.SelectedIndex = 0
                    If e.Item.Cells(SoLnGrid.SLSP2).Text & "" <> "" Then
                        cbo_slsp2.SelectedValue = e.Item.Cells(SoLnGrid.SLSP2).Text
                        txt_slsp2_pct.Text = e.Item.Cells(SoLnGrid.SLSP_PCT2).Text
                    End If
                End If
            End If
            ' Daniela comented not needed
            ' Dim TheFile As System.IO.FileInfo
            '  Dim File_Path As String = ""

            ' sql = "select major.mjr_cd || '_' || major.des || '/' || minor.mnr_cd || '_' || minor.des || " & _
            '   "   '/' || item.ve_cd || '_' || item.itm_cd || '_WEB.jpg' " & _
            '   " from itm item, inv_mnr minor, inv_mjr major, ve vendor where item.mnr_cd = minor.mnr_cd and minor.mjr_cd = major.mjr_cd and item.ve_cd = vendor.ve_cd and item.itm_cd='" & e.Item.Cells(SoLnGrid.ITM_CD).Text & "'"

            '  objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            '  Try
            'Mydatareader = objsql.ExecuteReader
            ' If Mydatareader.Read() Then
            'File_Path = Mydatareader.Item(0).ToString
            '  File_Path = Replace(File_Path, " ", "_")
            ' TheFile = New System.IO.FileInfo(Server.MapPath("\external_images\") & File_Path)
            ' End If
            '   Catch

            ' End Try
            ' End Daniela comment
            Try

                '    'sabrinay comment out
                '    'If TheFile.Exists Then
                '    '    Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                '    '    img_picture.ImageUrl = "~/external_images/" & File_Path
                '    'Else

                '    ' Daniela get Zip code and Lang Code only one time
                '    If isEmpty(Session("ImgZipCd")) Then
                '        Session("ImgZipCd") = HBCG_Utils.GetPostalCode()
                '    End If
                '    If isEmpty(Session("ImgLangCode")) Then
                '        Session("ImgLangCode") = HBCG_Utils.GetLangCode()
                '    End If

                '    ' Daniela add parameters
                '    'Dim image_URL As String = HBCG_Utils.Find_Merchandise_Images(e.Item.Cells(SoLnGrid.ITM_CD).Text)

                '    Dim image_URL As String = ""

                ' Daniela default picture if img_picture found
                If Not ViewState("ItemTable") Is Nothing Then
                    Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                    If isEmpty(img_picture.ImageUrl) Then
                        img_picture.ImageUrl = "~/images/image_not_found.jpg"
                    End If
                End If
                '    image_URL = HBCG_Utils.Find_Merchandise_Images(e.Item.Cells(SoLnGrid.ITM_CD).Text, Session("ImgZipCd"), Session("ImgLangCode"))

                '    If image_URL.ToString & "" <> "" Then
                '        img_picture.ImageUrl = image_URL
                '    Else
                '        img_picture.ImageUrl = "~/images/image_not_found.jpg"
                '    End If
                '    'End If
            Catch
            End Try

            If Request("LEAD") = "TRUE" Then

                Dim txt_line_desc As TextBox = CType(e.Item.FindControl("txt_line_desc"), TextBox)
                If Not IsNothing(txt_line_desc) Then
                    txt_line_desc.Visible = True

                    sql = "SELECT DESCRIPTION FROM RELATIONSHIP_LINES_DESC WHERE LINE='" & e.Item.Cells(SoLnGrid.ROW_ID).Text & "' AND REL_NO='" & Session.SessionID.ToString & "'"

                    'Set SQL OBJECT 
                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                    Try
                        'Execute DataReader 
                        Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                        'Store Values in String Variables 
                        If (Mydatareader.Read()) Then
                            txt_line_desc.Text = Mydatareader.Item("DESCRIPTION").ToString
                        End If
                    Catch
                        conn.Close()
                        Throw
                    End Try
                    Mydatareader.Close()

                End If
            End If

            If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))

            Dim img_gm As ImageButton = CType(e.Item.FindControl("img_gm"), ImageButton)
            If Not IsNothing(img_gm) Then
                If GM_Sec = "Y" Then
                    img_gm.Visible = True
                    btn_total_margins.Visible = True
                Else
                    img_gm.Visible = False
                End If
            End If
        End If

        If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then
            Dim txt_qty As TextBox = CType(e.Item.FindControl("qty_txtbox"), TextBox)
            txt_qty.Enabled = False
        End If

        If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then
            If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then
                Dim txt_qty3 As TextBox = CType(e.Item.FindControl("qty_txtbox"), TextBox)
                Dim ret_prc As TextBox = CType(e.Item.FindControl("ret_prc"), TextBox)
                If Not IsNothing(txt_qty3) And Not IsNothing(ret_prc) Then
                    ret_prc.Enabled = False
                    txt_qty3.Enabled = False
                End If
            End If
        ElseIf ConfigurationManager.AppSettings("package_breakout").ToString = "C" Then
            If IsNumeric(e.Item.Cells(SoLnGrid.PKG_LN).Text) Then
                Dim txt_qty3 As TextBox = CType(e.Item.FindControl("qty_txtbox"), TextBox)
                Dim ret_prc As TextBox = CType(e.Item.FindControl("ret_prc"), TextBox)
                If Not IsNothing(txt_qty3) And Not IsNothing(ret_prc) Then
                    txt_qty3.Enabled = False
                    ret_prc.Enabled = False
                End If
            End If
        Else

            If IsNumeric(e.Item.Cells(SoLnGrid.PKG_LN).Text) Then
                Dim txt_qty3 As TextBox = CType(e.Item.FindControl("qty_txtbox"), TextBox)
                Dim ret_prc As TextBox = CType(e.Item.FindControl("ret_prc"), TextBox)
                If Not IsNothing(txt_qty3) And Not IsNothing(ret_prc) Then
                    txt_qty3.Enabled = False
                    ret_prc.Enabled = False
                End If
            End If
        End If

        Dim Image2 As ImageButton = CType(e.Item.FindControl("Image2"), ImageButton)
        If Session("ord_tp_cd") <> "SAL" Then
            If Not IsNothing(store_cd) Then
                store_cd.Enabled = False
            End If
            If Not IsNothing(loc_cd) Then
                loc_cd.Enabled = False
            End If
            If Not IsNothing(Image2) Then
                Image2.Visible = False
            End If
        End If

        Dim slsp_div As HtmlGenericControl = CType(e.Item.FindControl("slsp_section"), HtmlGenericControl)
        If Not IsNothing(slsp_div) Then slsp_div.Visible = (chk_slsp_view_all.Checked)

        'Daniela Dec 04 disable Salesperson update
        If Not IsNothing(slsp_div) Then
            slsp_div.Disabled = True
        End If

        Dim chk_carton As CheckBox = CType(e.Item.FindControl("chk_carton"), CheckBox)
        If Not IsNothing(chk_carton) Then
            Dim lbl_carton As Label = CType(e.Item.FindControl("lbl_carton"), Label)
            If Request("LEAD") = "TRUE" Then
                lbl_carton.Visible = False
                chk_carton.Visible = False
            Else
                ' Daniela Nov 11 hide Leave in Carton
                'lbl_carton.Visible = True
                'chk_carton.Visible = True
                'chk_carton.Checked = (e.Item.Cells(SoLnGrid.LV_IN_CARTON).Text = "Y")
            End If
        End If

        If IsSerialType(e.Item.Cells(SoLnGrid.SER_TP).Text) AndAlso Request("LEAD") <> "TRUE" Then
            Dim txt_qty2 As TextBox = CType(e.Item.FindControl("qty_txtbox"), TextBox)
            If Not IsNothing(txt_qty2) Then
                txt_qty2.Enabled = False
            End If
        End If

        If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Or ConfigurationManager.AppSettings("allow_cash_carry") = "N" Then
            Dim txt_take_With As CheckBox = CType(e.Item.FindControl("TakeWith"), CheckBox)
            If Not IsNothing(txt_take_With) Then
                txt_take_With.Enabled = False
            End If
        End If

        'if the avail date in the temp_itm table was null, re-retrieve it.
        Dim isInventory As Boolean = ResolveYnToBooleanEquiv(e.Item.Cells(SoLnGrid.INVENTORY).Text.Trim())
        If isInventory Then
            Dim locCd1 As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)
            Dim storeCd1 As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
            ' Feb 02, 2018 showroom items
            Dim isOutletLoc As Boolean = False
            Dim isShowroomLoc As Boolean = False
            If ((storeCd1.Text.isNotEmpty) AndAlso (locCd1.Text.isNotEmpty)) Then
                isOutletLoc = theInvBiz.IsOutletLocation(storeCd1.Text, locCd1.Text)
            End If
            If (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                If (Not isOutletLoc And ((storeCd1.Text.isNotEmpty) AndAlso (locCd1.Text.isNotEmpty))) Then
                    isShowroomLoc = LeonsBiz.IsShowroomLocation(storeCd1.Text, locCd1.Text)
                End If
            End If
            Dim availDate As DateTime = Today.Date

            If (isOutletLoc Or isShowroomLoc) Then
                If Not (Session("pd_store_cd").Equals(storeCd1.Text.Trim())) Then
                    If (IsNothing(ViewState("zoneLeadDays")) AndAlso String.IsNullOrWhiteSpace(ViewState("zoneLeadDays"))) Then
                        ViewState("zoneLeadDays") = theTranBiz.GetZoneLeadDays(SessVar.zoneCd)
                    End If
                    availDate = availDate.AddDays(ViewState("zoneLeadDays"))
                End If
            Else
                Dim tBoxQty As TextBox = DirectCast(e.Item.FindControl("qty_txtbox"), TextBox)
                If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                    availDate = GetAvailDate(isInventory, e.Item.Cells(SoLnGrid.ITM_CD).Text, CDbl(tBoxQty.Text))
                End If
            End If

            If Not (availDate = DateTime.MinValue) And isInventory Then
                Dim lblAvail As Label = DirectCast(e.Item.FindControl("lblAvailDt"), Label)
                lblAvail.Text = Resources.POSMessages.MSG0002 & FormatDateTime(availDate, DateFormat.ShortDate)
                If (Not IsNothing(Session("ord_tp_cd")) AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString) AndAlso (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                    lblAvail.Visible = True
                Else
                    lblAvail.Visible = False
                End If
                e.Item.Cells(SoLnGrid.AVAIL_DT).Text = FormatDateTime(availDate, DateFormat.ShortDate)
                UpdtTempItmAvailDtByLnNum(availDate.ToString, CInt(e.Item.Cells(SoLnGrid.ROW_ID).Text))
            End If

        End If
        conn.Close()
        'the Grid rows are disabled based on the flag "PREVENT" stored in table viewstate("ItemTable")
        If Session("ord_tp_cd") = "SAL" Then
            If ViewState("ItemTable") Is Nothing Then
                'Do nothing
            Else
                If e.Item.ItemIndex >= 0 Then
                    Dim Dtemp As DataTable = CType(ViewState("ItemTable"), DataTable)
                    Dim PreventCheck As String = Dtemp.Rows(e.Item.ItemIndex)("PREVENT")
                    If PreventCheck = "Y" Then
                        Dim Image1 As ImageButton = CType(e.Item.FindControl("Image1"), ImageButton)
                        Dim Img2 As ImageButton = CType(e.Item.FindControl("Image2"), ImageButton)
                        Dim storeCd1 As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
                        Dim locCd1 As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)
                        Dim SelectThis As CheckBox = CType(e.Item.FindControl("SelectThis"), CheckBox)
                        Dim SelectWarr As CheckBox = CType(e.Item.FindControl("SelectWarr"), CheckBox)
                        Dim chkWarr As CheckBox = CType(e.Item.FindControl("chk_warr"), CheckBox)
                        Dim TakeWith As CheckBox = CType(e.Item.FindControl("TakeWith"), CheckBox)
                        Dim retPrc As TextBox = CType(e.Item.FindControl("ret_prc"), TextBox)
                        Dim QtyTxtBox As TextBox = CType(e.Item.FindControl("QTY_TXTBOX"), TextBox)
                        Image1.Enabled = False
                        Img2.Enabled = False
                        storeCd1.Enabled = False
                        storeCd1.Text = ""
                        locCd1.Enabled = False
                        locCd1.Text = ""
                        If Not SelectThis Is Nothing Then
                            SelectThis.Enabled = False
                        End If
                        If Not SelectWarr Is Nothing Then
                            SelectWarr.Enabled = False
                        End If
                        If Not chkWarr Is Nothing Then
                            chkWarr.Enabled = False
                        End If
                        If Not TakeWith Is Nothing Then
                            TakeWith.Enabled = False
                        End If
                        retPrc.Enabled = False
                        'retPrc.Text = "0.00"
                        QtyTxtBox.Text = "0"
                        QtyTxtBox.Enabled = False
                    End If
                End If
            End If
        End If

        'Applying security to disscount package.
        If Session("ord_tp_cd") = "SAL" Then
            If Not ViewState("ItemTable") Is Nothing Then
                If e.Item.ItemIndex >= 0 Then
                    Dim packageDtemp As DataTable = CType(ViewState("ItemTable"), DataTable)
                    Dim FilterText As String
                    Dim distinctPackageParent() As DataRow
                    Dim pkgCmp As String
                    Dim packgeComponetCheck As String = packageDtemp.Rows(e.Item.ItemIndex)("TYPECODE")
                    Dim txtRetPrc As TextBox = CType(e.Item.FindControl("ret_prc"), TextBox)
                    Dim txtQty As TextBox = CType(e.Item.FindControl("QTY_TXTBOX"), TextBox)
                    If packgeComponetCheck = "CMP" Then
                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                        txtRetPrc.Enabled = False
                        txtQty.Enabled = False
                        If e.Item.Cells(SoLnGrid.PKG_LN).Text.ToString().Trim() = "&nbsp;" Then
                            pkgCmp = ""
                        Else
                            pkgCmp = e.Item.Cells(SoLnGrid.PKG_LN).Text.ToString().Trim()
                        End If
                        FilterText = "ROW_ID='" + pkgCmp + "'"
                        If Not pkgCmp.Equals("") Then
                            distinctPackageParent = packageDtemp.Select(FilterText)
                        End If
                        Dim prc As Double = distinctPackageParent(0)("RET_PRC").ToString
                        If prc = 0 Then
                            Dim checkFlag As Boolean
                            'If the user is super user having all access
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = True
                                txtQty.Enabled = True
                                checkFlag = True
                            ElseIf CheckSecurity("", Session("EMP_CD")) = False Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line. 
                                'The line must be deleted or voided and then a new line must be added.
                                'And this security will not allow changes to the qty.
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                End If
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    txtQty.Enabled = True
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                                'This security "SOE_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.
                                'The user must have ' both to be able to change the component price.
                                If (CheckSecurity(SecurityUtils.SOE_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = True
                                End If
                            End If
                        ElseIf prc > 0 Then
                            Dim checkFlag As Boolean
                            'If the user is super user having all access
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = True
                                checkFlag = True
                            ElseIf CheckSecurity("", Session("EMP_CD")) = False Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line.The line must be deleted or voided and then a new line must be added.
                                'And this security will not allow changes to the qty.
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                End If
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                                'This security "SOE_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.
                                'The user must have ' both to be able to change the component price.
                                If (CheckSecurity(SecurityUtils.SOE_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = False
                                End If
                            End If
                        End If
                    ElseIf packgeComponetCheck = "PKG" Then
                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                        txtRetPrc.Enabled = False
                        txtQty.Enabled = False
                        Dim prc As Double = txtRetPrc.Text.Trim
                        If prc = 0 Then
                            Dim checkFlag As Boolean
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                checkFlag = True
                            ElseIf CheckSecurity("", Session("EMP_CD")) = False Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                'This security "SOE_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                If (CheckSecurity(SecurityUtils.SOE_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                            End If
                        ElseIf prc > 0 Then
                            Dim checkFlag As Boolean
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = True
                                txtQty.Enabled = False
                                checkFlag = True
                            ElseIf CheckSecurity("", Session("EMP_CD")) = False Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                'This security "SOE_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                If (CheckSecurity(SecurityUtils.SOE_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = True
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                            End If
                        End If
                    Else
                        txtRetPrc.Enabled = False
                        If CheckSecurity("", Session("EMP_CD")) Then
                            txtRetPrc.Enabled = True
                        Else
                            txtRetPrc.Enabled = False
                        End If
                        If (CheckSecurity(SecurityUtils.SOE_CHG_PRC, Session("EMP_CD"))) Then
                            txtRetPrc.Enabled = True
                        End If
                    End If

                End If
            Else
                'Do nothing
            End If
        End If
    End Sub

    Public Sub DoAfterBindgrid()

        Dim intRow As Integer = 0
        Dim str_treatable As String = ""
        Dim str_tp_cd As String = "".Trim
        Dim str_treated As String = ""
        Dim take_with As String
        Dim isInventory As Boolean
        Dim str_warrable As String = ""
        Dim str_warr_itm As String = ""
        Dim lblAvail As Label
        Dim crmALnCnt As Integer = 0  '  accumulate the number of CRM lines with qty > 0
        ' Number of rows in the DataGrid
        Dim intRows As Integer = Gridview1.Items.Count - 1
        ' DataGrid rows are really DataGridItems.
        Dim SoLnGItm As DataGridItem
        Using dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP), dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
            dbConnection.Open()
            For intRow = 0 To intRows

                ' Get the grid row.
                SoLnGItm = Gridview1.Items(intRow)

                lblAvail = DirectCast(SoLnGItm.FindControl("lblAvailDt"), Label)
                lblAvail.Visible = False

                ' if the sale line has no return qty left to return (qty set to 0), then disable all entries on the line except delete; also set qty to 0 if MCR and 0 credit because
                '     cannot disable based on price - can sell or return at 0 retail
                If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) AndAlso SystemUtils.isNotEmpty(SoLnGItm.Cells(SoLnGrid.QTY).Text) AndAlso
                    SystemUtils.isNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text) AndAlso SoLnGItm.Cells(SoLnGrid.QTY).Text = 0.0 Then

                    SoLnGItm.Cells(SoLnGrid.TW_CHKBOX).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.STORE_CD).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.LOC_CD).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.INV_LOOKUP).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.WARR_CHKBOX).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.FAB_CHKBOX).Enabled = False
                    If SoLnGItm.Cells(SoLnGrid.ITM_TP_CD).Text = "CPT" Then
                        SoLnGItm.Cells(SoLnGrid.QTY).Text = FormatNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text, 2)
                        SoLnGItm.Cells(SoLnGrid.QTY_TXTBOX).Text = FormatNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text, 2)
                    Else
                        SoLnGItm.Cells(SoLnGrid.QTY).Text = FormatNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text, 0)
                        SoLnGItm.Cells(SoLnGrid.QTY_TXTBOX).Text = FormatNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text, 0)
                        SoLnGItm.Cells(SoLnGrid.QTY).Text = FormatNumber(SoLnGItm.Cells(SoLnGrid.QTY).Text, 0)
                    End If
                    SoLnGItm.Cells(SoLnGrid.QTY_TXTBOX).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.QTY_TXTBOX).Visible = True
                    SoLnGItm.Cells(SoLnGrid.UNIT_PRC).Enabled = False
                    SoLnGItm.Cells(SoLnGrid.VSN_COL).Enabled = False

                Else
                    If OrderUtils.isCrmWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                        crmALnCnt = crmALnCnt + 1
                    End If

                    If (SoLnGItm.Cells(SoLnGrid.AVAIL_DT).Text.isNotEmpty) And SoLnGItm.Cells(SoLnGrid.INVENTORY).Text.Trim().Equals("Y") Then
                        If (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS")) Then
                            lblAvail.Visible = True
                        Else
                            lblAvail.Visible = False
                        End If
                        lblAvail.Text = Resources.POSMessages.MSG0002 & FormatDateTime(SoLnGItm.Cells(SoLnGrid.AVAIL_DT).Text, DateFormat.ShortDate)
                    Else
                        lblAvail.Text = String.Empty
                    End If

                    str_treatable = SoLnGItm.Cells(SoLnGrid.TREATABLE).Text().Trim()
                    str_treated = SoLnGItm.Cells(SoLnGrid.TREATED).Text().Trim()
                    take_with = SoLnGItm.Cells(SoLnGrid.TW).Text().Trim()
                    isInventory = ResolveYnToBooleanEquiv(SoLnGItm.Cells(SoLnGrid.INVENTORY).Text.Trim())
                    str_warr_itm = SoLnGItm.Cells(SoLnGrid.WARR_ITM_LINK).Text().Trim()
                    str_warrable = SoLnGItm.Cells(SoLnGrid.WARRANTABLE).Text().Trim()
                    'MM-7099
                    If isInventory Then
                        SoLnGItm.Cells(SoLnGrid.TW_CHKBOX).Enabled = True
                        SoLnGItm.Cells(SoLnGrid.STORE_CD).Enabled = True
                        SoLnGItm.Cells(SoLnGrid.INV_LOOKUP).Enabled = True
                        SoLnGItm.Cells(SoLnGrid.LOC_CD).Enabled = True
                    Else
                        If theSalesBiz.SkuCheckForTakeWith(SoLnGItm.Cells(SoLnGrid.ITM_CD).Text().Trim(), dbCommand) Then
                            SoLnGItm.Cells(SoLnGrid.TW_CHKBOX).Enabled = False
                        End If
                        SoLnGItm.Cells(SoLnGrid.STORE_CD).Enabled = False
                        SoLnGItm.Cells(SoLnGrid.LOC_CD).Enabled = False
                        SoLnGItm.Cells(SoLnGrid.INV_LOOKUP).Enabled = False
                    End If

                    If take_with = "Y" Then
                        Dim cbTakeWith As CheckBox = DirectCast(SoLnGItm.FindControl("TakeWith"), CheckBox)
                        cbTakeWith.Checked = True
                    End If

                    If Session("lucy_has_take_with") = "Y" Then  'lucy add if condition
                        SoLnGItm.Cells(SoLnGrid.TW_CHKBOX).Enabled = False
                    End If
                    If Session("cash_carry") = "TRUE" Then  'lucy 26 -nov-15

                        SoLnGItm.Cells(SoLnGrid.STORE_CD).Enabled = False
                        SoLnGItm.Cells(SoLnGrid.LOC_CD).Enabled = False
                    End If

                    'hides/shows the serial number components accordingly
                    Dim lbl_sernum As Label = DirectCast(SoLnGItm.FindControl("lbl_sernum"), Label)
                    Dim lbl_sernumval As Label = DirectCast(SoLnGItm.FindControl("lbl_sernumval"), Label)
                    If IsSerialType(SoLnGItm.Cells(SoLnGrid.SER_TP).Text) Then
                        lbl_sernum.Visible = True
                        lbl_sernumval.Visible = True
                    Else
                        lbl_sernum.Visible = False
                        lbl_sernumval.Visible = False
                    End If

                    'hides/shows the Outlet ID
                    Dim lbl_outletid As Label = DirectCast(SoLnGItm.FindControl("lbl_outletid"), Label)
                    Dim lbl_outletval As Label = DirectCast(SoLnGItm.FindControl("lbl_outletval"), Label)
                    lbl_outletid.Visible = IIf(lbl_outletval.Text & "" <> "", True, False)

                    'hides/shows the Inventory Count
                    UpdateInvCount(SoLnGItm, isInventory)

                    'hides/shows the CRPT ID
                    Dim lbl_crpt_num As Label = DirectCast(SoLnGItm.FindControl("lbl_crpt_num"), Label)
                    Dim lbl_crpt_num_val As Label = DirectCast(SoLnGItm.FindControl("lbl_crpt_num_val"), Label)
                    If lbl_crpt_num_val.Text & "" <> "" Then
                        lbl_crpt_num.Visible = True
                    Else
                        lbl_crpt_num.Visible = False
                    End If

                    'hides/shows the warr - column in temp_itm is filled either for warranty or warrantied warrantable so have to check itm_tp_cd
                    Dim lbl_warr As Label = DirectCast(SoLnGItm.FindControl("lbl_warr"), Label)
                    Dim lbl_warr_sku As Label = DirectCast(SoLnGItm.FindControl("lbl_warr_sku"), Label)
                    If isInventory AndAlso "Y".Equals(SoLnGItm.Cells(SoLnGrid.WARRANTABLE).Text) AndAlso str_tp_cd <> "WAR" Then  ' TODO DSA - now make sure doesn't for void lines

                        'GridItem.Cells(SoLnGrid.WARR_CHKBOX).Enabled = True  ' TODO - DSA - why doesn't this work
                        If (str_warr_itm).isNotEmpty AndAlso
                            str_warr_itm <> "&nbsp;" Then

                            lbl_warr.Visible = True
                            lbl_warr_sku.Visible = True
                            Dim cbWarr As CheckBox = DirectCast(SoLnGItm.FindControl("SelectWarr"), CheckBox)
                            cbWarr.Checked = True
                        Else
                            lbl_warr.Visible = False
                            lbl_warr_sku.Visible = False
                        End If
                    Else
                        lbl_warr.Visible = False
                        lbl_warr_sku.Visible = False
                        SoLnGItm.Cells(SoLnGrid.WARR_CHKBOX).Enabled = False
                    End If

                    If treatmentFound = True Then
                        If isInventory And str_treatable = "Y" Then
                            SoLnGItm.Cells(SoLnGrid.FAB_CHKBOX).Enabled = True
                            If str_treated = "Y" Then
                                Dim cbDelete As CheckBox = DirectCast(SoLnGItm.FindControl("SelectThis"), CheckBox)
                                cbDelete.Checked = True
                            End If
                        Else
                            SoLnGItm.Cells(SoLnGrid.FAB_CHKBOX).Enabled = False
                        End If
                    End If
                End If
            Next

            If OrderUtils.isCrmWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) AndAlso crmALnCnt = 0 Then

                lbl_warnings.Text = "All quantities on all lines of original document have been returned."
            End If
        End Using
    End Sub

    ''' <summary>
    ''' Checks if the Inventory count should be displayed and if so, 
    ''' retrieves the count to display on it.
    ''' </summary>
    ''' <param name="soLnRow">the datagrid row being modified</param>
    ''' <param name="isInventory">TRUE if the ITEM on this row is inventory; FALSE otherwise</param>
    Private Sub UpdateInvCount(ByRef soLnRow As DataGridItem, ByVal isInventory As Boolean)

        Dim lbl_invcount As Label = DirectCast(soLnRow.FindControl("lbl_invcount"), Label)
        lbl_invcount.Visible = (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso isInventory)

        If (lbl_invcount.Visible) Then
            Dim totalWhse As Integer = 0
            Dim lbl_sku As Label = DirectCast(soLnRow.FindControl("lbl_SKU"), Label)
            Dim skuId As String = lbl_sku.Text.Trim

            If SysPms.priorityFillForInvAvail Then
                Dim pfReq As PriFillReq = New PriFillReq
                pfReq.itmCd = skuId
                pfReq.homeStoreCd = SessVar.homeStoreCd
                pfReq.puDel = SessVar.puDel
                pfReq.puDelStoreCd = SessVar.puDelStoreCd
                pfReq.takeWith = "N"
                pfReq.zoneCd = SessVar.zoneCd
                pfReq.zoneZipCd = SessVar.zipCode4Zone
                pfReq.locTpCd = AppConstants.Loc.TYPE_CD_W
                totalWhse = GetQtyCount(theInvBiz.GetInvPriFill(pfReq))
            Else
                'NOTE: When SYSPM priority fill is on, the API takes into account the soft-res
                '      but in this case counts have to be adjusted manually
                Dim softRes As DataSet = theInvBiz.GetReservations(skuId)
                Dim whseInv As DataSet = theInvBiz.GetStoreData(skuId, AppConstants.Loc.TYPE_CD_W)
                totalWhse = GetQtyCount(InventoryUtils.RemoveResCounts(whseInv, softRes, AppConstants.Loc.TYPE_CD_W))
            End If
            lbl_invcount.Text = totalWhse
        End If
    End Sub

    Protected Sub CheckWarrClicked(ByVal sender As Object, ByVal e As EventArgs)
        Dim cbWarr As CheckBox = CType(sender, CheckBox)
        Dim soLnGItm As DataGridItem = CType(cbWarr.NamingContainer, DataGridItem)
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If
        If cbWarr.Checked = True Then ' CheckWarrClicked for adding a warranty to a line and is new warranty line
            ' can only have warranty attached if one to many allowed or if not allowed, then qty must = 1
            If SysPms.isOne2ManyWarr OrElse 1 = CInt(soLnGItm.Cells(SoLnGrid.QTY).Text) Then

                hidWarrantyClicked.Value = 1
                SessVar.discReCalc.needToReCalc = True
                'Prepares data for later use in discount recalculation (if applicable)
                SetupForDiscReCalc()

                If SalesUtils.hasWarranty(soLnGItm.Cells(SoLnGrid.ITM_CD).Text) Then
                    ShowWarrantyPopup(soLnGItm.Cells(SoLnGrid.ROW_ID).Text)
                End If
            Else
                lbl_warnings.Text = "Quantity must equal 1 when a warranty is attached to a SKU."
                cbWarr.Checked = False
            End If

        Else  ' remove warranty link 
            If RemoveWarrLink(soLnGItm) Then
                ' if a warranty line was removed, then reapply discount; sometimes only the link is removed, sometimes the line
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Void, String.Empty)
            End If
        End If

        bindgrid()
        'calculate_total()
    End Sub

    ''' <summary>
    ''' removes the warranty link when warranty/warrantable removed or warranty unlinked and if no warrantables attached, removes the warranty line itself
    ''' </summary>
    ''' <param name="soLnGItm">order line data grid</param>
    ''' <returns>true if a line was removed; false otherwise</returns>
    Protected Function RemoveWarrLink(ByVal soLnGItm As DataGridItem) As Boolean

        Dim currWarrLnNum As Integer = 0
        Dim rowRemoved As Boolean = False
        ' can have a warranty SKU without a Warranty line# (if doc was split or the warranty added later, just means warrantable not on this doc)
        If soLnGItm.Cells(SoLnGrid.WARR_ROW_LINK).Text.isNotEmpty AndAlso soLnGItm.Cells(SoLnGrid.WARR_ROW_LINK).Text <> "&nbsp;" Then
            currWarrLnNum = soLnGItm.Cells(SoLnGrid.WARR_ROW_LINK).Text
        End If

        Dim hasOtherLinks As Boolean = False
        Dim warrLnNumList As New ArrayList
        Dim soLnDatTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Session.SessionID.ToString.Trim, temp_itm_key_tp.tempItmBySessionId)

        Dim soLnRow As DataRow
        For Each soLnRow In soLnDatTbl.Rows

            If soLnRow("ROW_ID") = soLnGItm.Cells(SoLnGrid.ROW_ID).Text Then
                'finds the warrantied row and remove the warranty link from it; do not add to list

                Dim updtReq As New TempItmUpdateRequest
                updtReq.tmpItm = New TempItm
                updtReq.tmpItm.warrItmLink = ""
                updtReq.tmpItm.warrRowLink = convStrToNum("")
                updtReq.warrRowLinkUpdt = True
                updtReq.warrItmLinkUpdt = True
                updtReq.tmpItm.rowIdSeqNum = soLnGItm.Cells(SoLnGrid.ROW_ID).Text
                updtReq.UpdtBy = temp_itm_key_tp.tempItmByRowIdSeqNum
                HBCG_Utils.UpdtTempItm(updtReq)
                'soLnGItm.Cells(SoLnGrid.WARR_ROW_LINK).Text = ""
                'soLnGItm.Cells(SoLnGrid.WARR_ITM_LINK).Text = ""

                If Not SysPms.isOne2ManyWarr Then
                    Exit For 'no need to keep on iterating for single warr, updated the correct row already but for one to many, will need to go thru all
                End If

            ElseIf SysPms.isOne2ManyWarr AndAlso soLnRow("WARR_ROW_LINK").ToString.isNotEmpty AndAlso "Y".Equals(soLnRow("WARRANTABLE").ToString) Then
                ' track the warrantied lines that are not the one being removed
                warrLnNumList.Add(soLnRow("WARR_ROW_LINK").ToString)
            End If
        Next

        If SysPms.isOne2ManyWarr AndAlso currWarrLnNum > 0 AndAlso (Not warrLnNumList Is Nothing) AndAlso warrLnNumList.Count > 0 Then
            For Each warrLnNum In warrLnNumList
                'finds if any other warrantables are linked to the warranty
                If warrLnNum = currWarrLnNum Then
                    hasOtherLinks = True
                End If
            Next
        End If

        If (Not hasOtherLinks) AndAlso currWarrLnNum > 0 Then
            ' now iterate to find the warranty row to void/remove it
            For Each soLnRow In soLnDatTbl.Rows

                If soLnRow("ROW_ID") = currWarrLnNum Then

                    ' delete row if not existing and void if existing  
                    ' TODO DSA - see how this works on existing vs new
                    HBCG_Utils.deleteTempItmByRowId(currWarrLnNum)
                    rowRemoved = True
                    lbl_warnings.Text = "Removing the referenced warranty SKU " + soLnRow("ITM_CD")   ' TODO - could add line # but not NOW 'sku x on line y'
                    ' TODO not sure if this will show, it might get overwritten - we need better POS+ messaging
                    'soLnRow("ref_del_doc_ln#") = ""   these will not work because void line doesn't update anything else; 
                    ' TODO is not clearing line tax on voided lines a problem somewhere???
                    'soLnRow("ref_itm_cd") = ""
                    hidUpdLnNos.Value = hidUpdLnNos.Value.Replace(";" & currWarrLnNum, "")
                    Exit For 'no need to keep on iterating, updated the correct row already
                End If
            Next
        End If

        Return rowRemoved
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "Regular.Master"
        ElseIf Request("minimal") = "true" Then
            Page.MasterPageFile = "~/NoContent.Master"
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Session("cash_carry") <> "TRUE" Then   'lucy
            Session("Add_More_Items") = "TRUE"  ' (Image button - tell order page can cyle around again if conditions permit)
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("order.aspx?LEAD=TRUE")
            Else
                Response.Redirect("order.aspx")
            End If
        End If

    End Sub

    Public Sub Modify_Package_Price(ByVal row_id As String, ByVal itm_cd As String, ByVal package_retail As Double, Optional ByVal change_parent As String = "NO")

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim retprc As Double
        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim objSql8 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim MyDataReader8 As OracleDataReader
        Dim qty As Double = 1
        Dim PKG_GO As Boolean
        PKG_GO = False
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection.Open()

        conn.Open()

        'If we are to populate the retail amounts, we need to calculate based on cost percentages
        sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd='" & itm_cd & "' "
        sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

        Do While MyDataReader.Read()
            pkg_items = pkg_items & ",'" & MyDataReader.Item("ITM_CD").ToString & "'"
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST,ROW_ID) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & "," & row_id & ")"
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
            PKG_GO = True
        Loop
        MyDataReader.Close()

        'Insert new retail price
        thistransaction = objConnection.BeginTransaction
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & package_retail & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd='" & itm_cd & "' and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

        'Calculate the total cost
        Do While MyDataReader.Read()
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST").ToString & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD").ToString & "' AND ROW_ID=" & row_id
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
            PKG_GO = True
        Loop
        MyDataReader.Close()
        Dim PERCENT_OF_TOTAL As Double
        Dim NEW_RETAIL As Double
        Dim TOTAL_NEW_RETAIL As Double
        Dim TOTAL_RETAIL As Double
        Dim LAST_SKU As String
        Dim LAST_PKG_SKU As String

        If PKG_GO = True Then
            'Calculate the percentage based on cost/total cost
            sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id & " ORDER BY PKG_SKU"
            objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read()
                If IsNumeric(MyDataReader2.Item("COST").ToString) And IsNumeric(MyDataReader2.Item("TOTAL_COST").ToString) Then
                    PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                Else
                    PERCENT_OF_TOTAL = CDbl(0)
                End If
                If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                NEW_RETAIL = PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND ROW_ID=" & row_id
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
            Loop

            MyDataReader2.Close()
            objSql2.Dispose()
        End If

        sql = "SELECT NEW_RETAIL, COMPONENT_SKU, ROW_ID FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & itm_cd & "'"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

        Do While MyDataReader2.Read()
            sql = "SELECT SUM(QTY) AS QTY FROM TEMP_ITM WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND ITM_CD='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND PACKAGE_PARENT=" & row_id
            objSql8 = DisposablesManager.BuildOracleCommand(sql, objConnection)
            MyDataReader8 = DisposablesManager.BuildOracleDataReader(objSql8)

            If MyDataReader8.Read() Then
                If IsNumeric(MyDataReader8.Item("QTY").ToString) Then
                    qty = MyDataReader8.Item("QTY").ToString
                Else
                    qty = 1
                End If
            Else
                qty = 1
            End If
            retprc = MyDataReader2.Item("NEW_RETAIL")
            If Not IsNumeric(retprc) Then retprc = 0
            sql = "update temp_itm set taxable_amt=" & retprc & "/" & qty & ", tax_amt=ROUND((" & retprc & "/" & qty & ") * (tax_pct / 100), " & TaxUtils.Tax_Constants.taxPrecision & ") WHERE PACKAGE_PARENT=" & row_id & " AND ITM_CD='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
        Loop
        MyDataReader2.Close()
        objSql2.Dispose()

        If change_parent <> "NO" Then
            'Update the taxable amounts for the newly updated Component SKUS
            sql = "update temp_itm set taxable_amt=ret_prc, tax_amt=ROUND(taxable_amt * (tax_pct / 100), " & TaxUtils.Tax_Constants.taxPrecision & ") WHERE PACKAGE_PARENT=" & row_id & " AND SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAX_PCT IS NOT NULL"
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

            'Update the Parent Package SKU retail to 0.00
            sql = "update temp_itm set ret_prc=0 WHERE ROW_ID=" & row_id
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()
        End If

        'We're done, delete out all of the records to avoid duplication
        thistransaction = objConnection.BeginTransaction
        sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = sql
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        conn.Close()
        objConnection.Close()

    End Sub

    Protected Sub btn_detail_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        bind_line_desc()
        ASPxPopupControl5.ShowOnPageLoad = True

    End Sub

    Protected Sub Line_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        conn.Open()

        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        If textdata.Text & "" <> "" Then
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            sql = "SELECT * FROM RELATIONSHIP_LINES_DESC WHERE REL_NO='" & Session.SessionID.ToString & "' AND LINE='" & dgItem.Cells(SoLnGrid.ROW_ID).Text & "'"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    sql = "UPDATE RELATIONSHIP_LINES_DESC SET DESCRIPTION=:DESCRIPTION WHERE REL_NO='" & Session.SessionID.ToString & "' AND LINE='" & dgItem.Cells(SoLnGrid.ROW_ID).Text & "'"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":DESCRIPTION", OracleType.Clob)

                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.ExecuteNonQuery()
                Else
                    sql = "INSERT INTO RELATIONSHIP_LINES_DESC (REL_NO, LINE, DESCRIPTION) VALUES(:REL_NO, :LINE, :DESCRIPTION)"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":REL_NO", OracleType.Clob)
                    objSql.Parameters.Add(":LINE", OracleType.VarChar)
                    objSql.Parameters.Add(":DESCRIPTION", OracleType.VarChar)

                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.Parameters(":LINE").Value = dgItem.Cells(SoLnGrid.ROW_ID).Text
                    objSql.Parameters(":REL_NO").Value = Session.SessionID.ToString
                    objSql.ExecuteNonQuery()
                End If
            Catch
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()

    End Sub

    Public Sub bind_line_desc()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim REL_NO As String = Request("REL_NO")

        If REL_NO & "" = "" Then
            REL_NO = Session.SessionID.ToString
            sql = "SELECT b.SESSION_ID as REL_NO, b.VSN, b.ROW_ID as LINE, a.DESCRIPTION "
            sql = sql & "FROM TEMP_ITM b, RELATIONSHIP_LINES_DESC a "
            sql = sql & "WHERE b.SESSION_ID = a.REL_NO (+) "
            sql = sql & "AND b.ROW_ID = a.LINE (+) "
            sql = sql & "AND b.SESSION_ID = '" & REL_NO & "' "
            sql = sql & "ORDER BY ROW_ID"
        Else
            sql = "SELECT b.REL_NO, b.VSN, b.LINE, a.DESCRIPTION "
            sql = sql & "FROM RELATIONSHIP_LINES b, RELATIONSHIP_LINES_DESC a "
            sql = sql & "WHERE b.REL_NO = a.REL_NO (+) "
            sql = sql & "AND b.LINE = a.LINE (+) "
            sql = sql & "AND b.REL_NO = '" & REL_NO & "' "
            sql = sql & "ORDER BY LINE"
        End If

        ds = New DataSet

        MyDataGrid.DataSource = ""
        MyDataGrid.Visible = True

        conn.Open()


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                MyDataGrid.DataSource = ds
                MyDataGrid.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_close.Click

        ASPxPopupControl5.ShowOnPageLoad = False
        Response.Redirect("order_detail.aspx?" & Request.QueryString.ToString)

    End Sub

    Protected Sub btn_total_margins_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Display gross margin

        Dim sql As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim frt_factor As Double = 0
        Dim repl_cst As Double = 0
        Dim ret_prc As Double = 0
        Dim qty As Double = 0
        Dim total_ret_prc As Double = 0
        Dim total_repl_cst As Double = 0

        'Open Connection 
        conn.Open()
        conn2.Open()

        sql = "select ret_prc, itm_cd, qty from temp_itm where session_id='" & Session.SessionID.ToString & "'"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            Do While MyDataReader2.Read()
                If IsNumeric(MyDataReader2.Item("RET_PRC").ToString) Then ret_prc = CDbl(MyDataReader2.Item("RET_PRC").ToString)
                If IsNumeric(MyDataReader2.Item("QTY").ToString) Then qty = CDbl(MyDataReader2.Item("QTY").ToString)

                sql = "select repl_cst, frt_fac from itm where itm_cd='" & MyDataReader2.Item("ITM_CD").ToString & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                    If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                    If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                End If
                MyDataReader.Close()
                total_ret_prc = total_ret_prc + (ret_prc * qty)
                total_repl_cst = total_repl_cst + (repl_cst * qty)
            Loop
            MyDataReader2.Close()
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        conn.Close()
        conn2.Close()
        lbl_gm.Text = "Retail: " & FormatCurrency(total_ret_prc, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(total_ret_prc - total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((total_ret_prc - total_repl_cst) / total_ret_prc, 2) & "<br />"
        ASPxPopupControl6.ShowOnPageLoad = True

    End Sub

    Protected Sub chk_slsp_view_all_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindgrid()
    End Sub

    Protected Sub chk_reserve_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_reserve_all.CheckedChanged
        Try
            Dim chkBoxdata As CheckBox = CType(sender, CheckBox)
            If (chkBoxdata.Checked) Then
                ReserveLines()
                chkBoxdata.Checked = False   'resets the value for next time
            Else
                If resClicked = False Then
                    lbl_warnings.Text = String.Empty
                End If
            End If
            If resClicked = False Then
                lbl_warnings.Text = sbError.ToString()
                sbError.Length = 0 ' Dispose the string builder
            End If


            resClicked = True
        Catch oracleException As OracleException
            Select Case oracleException.Code
                Case 20101
                    lbl_warnings.Text = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
            End Select
        Catch ex As Exception
            lbl_warnings.Text = ex.Message.ToString()
        End Try
    End Sub

    Protected Sub txt_pct_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim textdata As TextBox = CType(sender, TextBox)
        DoSalesPersonPctUpdate(textdata, True)
    End Sub

    Protected Sub txt_pct_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim textdata As TextBox = CType(sender, TextBox)
        DoSalesPersonPctUpdate(textdata, False)
    End Sub

    ''' <summary>
    ''' Called when the Salesperson Percents are keyed in
    ''' </summary>
    ''' <param name="theSlspPctTextBox">the textbox that triggered this event</param>
    ''' <param name="isSlspPct1Update">TRUE, if SalesPerson1 PCT is being updated; FALSE if SalesPerson2 PCT is</param>
    Private Sub DoSalesPersonPctUpdate(ByVal theSlspPctTextBox As TextBox, ByVal isSlspPct1Update As Boolean)

        Dim dgItem As DataGridItem = CType(theSlspPctTextBox.NamingContainer, DataGridItem)
        Dim ddlbSlsp1 As DropDownList = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        Dim ddlbSlsp2 As DropDownList = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)
        Dim tboxSlsp1Pct As TextBox = CType(dgItem.FindControl("slsp1_pct"), TextBox)
        Dim tboxSlsp2Pct As TextBox = CType(dgItem.FindControl("slsp2_pct"), TextBox)
        Dim lnRowId As String = dgItem.Cells(SoLnGrid.ROW_ID).Text

        If (IsNumeric(theSlspPctTextBox.Text)) Then
            If CDbl(theSlspPctTextBox.Text) > 0 AndAlso CDbl(theSlspPctTextBox.Text) < 100 Then
                'the percent entered is a valid number, therefore makes sure the percents sum 100
                If (isSlspPct1Update) Then
                    tboxSlsp2Pct.Text = 100 - CDbl(theSlspPctTextBox.Text) 'since slsp1 was entered, adjusts slsp2
                Else
                    tboxSlsp1Pct.Text = 100 - CDbl(theSlspPctTextBox.Text) 'since slsp2 was entered, adjusts slsp1
                End If
            Else
                lbl_warnings.Text = "Commission percentages must be between 1 and 100"
                If (isSlspPct1Update) Then
                    tboxSlsp1Pct.Text = IIf(IsNumeric(tboxSlsp2Pct.Text), (100 - CDbl(tboxSlsp2Pct.Text)), 100)
                Else
                    If IsNumeric(tboxSlsp1Pct.Text) And CDbl(tboxSlsp2Pct.Text) > 0 Then
                        tboxSlsp2Pct.Text = 100 - CDbl(tboxSlsp1Pct.Text)
                    Else
                        tboxSlsp1Pct.Text = 100
                        tboxSlsp2Pct.Text = 0
                        ddlbSlsp2.SelectedValue = ""
                    End If
                End If
            End If
        Else
            lbl_warnings.Text = "Commission percentages must be numeric"
            If (isSlspPct1Update) Then
                tboxSlsp1Pct.Text = IIf(IsNumeric(tboxSlsp2Pct.Text), (100 - CDbl(tboxSlsp2Pct.Text)), 100)
            Else
                tboxSlsp1Pct.Text = 100
                tboxSlsp2Pct.Text = 0
                ddlbSlsp2.SelectedValue = ""
            End If
        End If

        Dim sql As String = "UPDATE temp_itm SET SLSP1 = :slsp1, SLSP2 = :slsp2, SLSP1_PCT = :slsp1Pct, SLSP2_PCT = :slsp2Pct WHERE ROW_ID = :lnRowId"
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":lnRowId", OracleType.VarChar)
            dbCommand.Parameters.Add(":slsp1", OracleType.VarChar)
            dbCommand.Parameters.Add(":slsp1Pct", OracleType.Number)
            dbCommand.Parameters.Add(":slsp2", OracleType.VarChar)
            dbCommand.Parameters.Add(":slsp2Pct", OracleType.Number)
            dbCommand.Parameters(":lnRowId").Value = lnRowId
            dbCommand.Parameters(":slsp1").Value = ddlbSlsp1.SelectedValue
            dbCommand.Parameters(":slsp1Pct").Value = CDbl(tboxSlsp1Pct.Text)
            dbCommand.Parameters(":slsp2").Value = ddlbSlsp2.SelectedValue
            dbCommand.Parameters(":slsp2Pct").Value = CDbl(tboxSlsp2Pct.Text)
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try
    End Sub

    Protected Sub DoSalesPersonUpdate(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim txt_pct_2 As TextBox
        txt_pct_2 = CType(dgItem.FindControl("slsp2_pct"), TextBox)
        Dim txt_pct_1 As TextBox
        txt_pct_1 = CType(dgItem.FindControl("slsp1_pct"), TextBox)
        Dim slsp1 As DropDownList
        slsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        Dim slsp2 As DropDownList
        slsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If slsp2.SelectedValue <> "" Then
            If slsp2.SelectedValue = slsp1.SelectedValue Then
                slsp2.SelectedIndex = 0
            Else
                If Not IsNumeric(ConfigurationManager.AppSettings("slsp2")) Then
                    txt_pct_1.Text = "50"
                    txt_pct_2.Text = "50"
                Else
                    txt_pct_2.Text = ConfigurationManager.AppSettings("slsp2")
                    txt_pct_1.Text = 100 - CDbl(txt_pct_2.Text)
                End If
            End If
        End If

        With cmdUpdateItems
            .Connection = conn
            .CommandText = "update temp_itm set slsp1='" & slsp1.SelectedValue & "', slsp2='" & slsp2.SelectedValue & "', slsp1_pct=" & txt_pct_1.Text & ", slsp2_pct=" & txt_pct_2.Text & " where row_id=" & dgItem.Cells(SoLnGrid.ROW_ID).Text
            .ExecuteNonQuery()
        End With

    End Sub

    Protected Sub ASPxGridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView1.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If e.GetValue("SER_NUM").ToString() & "" <> "" Then
            Dim hpl_serial As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_serial"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            If Not IsNothing(hpl_serial) Then
                hpl_serial.Text = e.GetValue("SER_NUM").ToString()
                hpl_serial.NavigateUrl = "order_detail.aspx?ser_num=" & HttpUtility.UrlEncode(e.GetValue("SER_NUM").ToString()) & "&ROW_ID=" & lbl_ser_row_id.Text
            End If
        End If

    End Sub

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ' This is the 'Generate Sku' button to Lazyboy ??? only displays if POS+ inventory syspm is not blank
        ASPxPopupControl7.ShowOnPageLoad = False
        If Request("LEAD") = "TRUE" Then
            Response.Redirect("order_detail.aspx?LEAD=TRUE")
        Else
            Response.Redirect("order_detail.aspx")
        End If

    End Sub

    Protected Sub btn_sku_gen_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('" & ConfigurationManager.AppSettings("sku_generation").ToString & "','frmSKUGen','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub

    'Called when the 'CONTINUE' button is pressed after the last (or only line) on the order is made a Take With
    Protected Sub ConvertToCashCarrySale(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("PD") = "P"
        Session("DEL_DT") = FormatDateTime(Today.Date, DateFormat.ShortDate)
        Session("DEL_CHG") = "0"
        SessVar.cashNCarry = "TRUE"
        calculate_total(Page.Master)
        lbl_warnings.Visible = True
        lbl_warnings.Text = lbl_warnings.Text & " " & Resources.LibResources.Label682 & " " & vbCrLf
        ASPxPopupControl11.ShowOnPageLoad = False

        Dim sql As String = " UPDATE TEMP_ITM SET TAKE_WITH='N' WHERE session_id= :theSessionId "
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":theSessionId", OracleType.VarChar)
            dbCommand.Parameters(":theSessionId").Value = Session.SessionID.ToString.Trim
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub

    'Called when a ROW gets added to the grid that displays the Outlet IDs
    Protected Sub ASPxGridView2_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return
        If e.GetValue("ID_CD").ToString() & "" <> "" Then
            Dim hpl_out As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_out"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            If Not IsNothing(hpl_out) Then
                hpl_out.Text = e.GetValue("ID_CD").ToString()
                hpl_out.NavigateUrl = "order_detail.aspx?out_id=" & HttpUtility.UrlEncode(e.GetValue("ID_CD").ToString()) & "&ser_num=" & HttpUtility.UrlEncode(e.GetValue("SER_NUM").ToString()) & "&out_comm_cd=" & HttpUtility.UrlEncode(e.GetValue("OUT_COMM_CD").ToString()) & "&spiff=" & HttpUtility.UrlEncode(e.GetValue("SPIFF").ToString()) & "&ROW_ID=" & lbl_out_row_id.Text
            End If
        End If
    End Sub

    ''' <summary>
    ''' Determines if the price on any lines on the order has been modified and if needed, prompts for a 
    ''' manager approval via the summary price approval popup. 
    ''' </summary>
    Private Sub PromptForSummaryPriceMgrOverride()

        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        If AppConstants.ManagerOverride.BY_ORDER = prcOverride Then

            'get the unique, not null value of all rows whose price was modified
            Dim modifiedRowIds = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()

            If (modifiedRowIds.Any) Then
                Dim table As DataTable = SalesUtils.GetOrderLinesTableNew()
                For Each dgItem As DataGridItem In Gridview1.Items

                    Dim rowId As Integer = dgItem.Cells(SoLnGrid.ROW_ID).Text
                    For Each id As String In modifiedRowIds
                        If rowId = CInt(id) Then
                            Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim, dgItem.Cells(SoLnGrid.ROW_ID).Text, temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)

                            'gather the needed info to build the data table to pass to the price summary grid
                            Dim lblDesc As Label = CType(dgItem.FindControl("Label2"), Label)
                            Dim tboxQty As TextBox = CType(dgItem.FindControl("qty_txtbox"), TextBox)

                            SalesUtils.AddOrderLineRow(table,
                                                       dgItem.Cells(SoLnGrid.ROW_ID).Text,
                                                       dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                       lblDesc.Text,
                                                       CDbl(tboxQty.Text),
                                                       tmpItmRow("orig_prc"),
                                                       tmpItmRow("ret_prc"),
                                                       IIf(IsDBNull(tmpItmRow("style_cd")), "", tmpItmRow("style_cd")))

                            Exit For
                        End If
                    Next

                Next

                'pass this datatable to the new summ price approval popup & display it.
                If table IsNot Nothing AndAlso table.Rows.Count > 0 Then
                    ucSummaryPrcChange.BindData(table)
                    Dim popupPrcChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
                    popupPrcChangeAppr.ShowOnPageLoad = True
                End If
            End If
        End If


    End Sub

    ''' <summary>
    ''' Executes when there is a successful manager approval via the summary price approval popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCApproved(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCApproved

        'means that the approval for all the line price changes was successful, so update the lines in the grid and temp_itm          
        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")

        'get the approver's emp_cd and set it on the line                            
        Dim hidAppEmpCd As HiddenField = CType(popupPriceChangeAppr.FindControl("hidAppEmpCd"), HiddenField)
        Dim approverEmpCd As String = hidAppEmpCd.Value.ToString

        'Alice added on Oct 16, 2019 for request 294
        'Get the approver's comments
        Dim txtCommentL1 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL1"), TextBox)
        Dim txtCommentL2 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL2"), TextBox)
        Dim txtCommentL3 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL3"), TextBox)
        Dim commentL1 As String = txtCommentL1.Text.Trim()
        Dim commentL2 As String = txtCommentL2.Text.Trim()
        Dim commentL3 As String = txtCommentL3.Text.Trim()
        Session("ApprovalComment1") = commentL1
        Session("ApprovalComment2") = commentL2
        Session("ApprovalComment3") = commentL3
        Session("Approver_emp_cd") = approverEmpCd


        Dim modifiedRowIds = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()
        For Each dgItem As DataGridItem In Gridview1.Items

            'see if this line was one of the lines modified 
            Dim rowId As Integer = dgItem.Cells(SoLnGrid.ROW_ID).Text
            For Each id As String In modifiedRowIds
                If rowId = CInt(id) Then

                    Dim txtRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)
                    Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand
                    Dim item_code As String = CType(dgItem.FindControl("lbl_SKU"), Label).Text

                    Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    conn.Open()
                    With cmdUpdateItems
                        .Connection = conn
                        .CommandText = "UPDATE temp_itm SET manual_prc = " & Double.Parse(txtRetPrc.Text) & ", prc_chg_app_cd='" & approverEmpCd &
                                       "' WHERE row_id = " & id
                    End With
                    cmdUpdateItems.ExecuteNonQuery()

                    Exit For
                End If
            Next

        Next


        'reapplies the discounts (if any) when applicable
        ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)

        bindgrid()
        popupPriceChangeAppr.ShowOnPageLoad = False
        Session("SHOWPOPUP") = False    'mariam-sep 27,2016-price approval



        'the hidden url is the page that the user wanted to go to before bein prompted for summ prc approval. After
        ' capturing mgr approval, let the navigation resume to where it was going originally
        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.Trim()) Then
            'clear out the lines that were approved this time.
            hidUpdLnNos.Value = String.Empty

            If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
                Response.Redirect(hidDestUrl.Value)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Gets fired when the manager manager approval in the summary price approval popup fails. Any 
    ''' resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCCanceled(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCCanceled

        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")

        'since the approval was aborted, get the lines that were modified and update their info
        Dim modifiedRowIds = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()
        Dim tmpItmRow As DataRow
        For Each dgItem As DataGridItem In Gridview1.Items

            Dim txtRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)

            Dim rowId As Integer = dgItem.Cells(SoLnGrid.ROW_ID).Text
            For Each id As String In modifiedRowIds
                If rowId = CInt(id) Then

                    tmpItmRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim, dgItem.Cells(SoLnGrid.ROW_ID).Text, temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)
                    ' reset the display field with the manual price, clear the approver emp cd & update the db. 
                    txtRetPrc.Text = FormatNumber(tmpItmRow("manual_prc"), 2)
                    Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

                    Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    conn.Open()
                    With cmdUpdateItems
                        .Connection = conn
                        ' .CommandText = "UPDATE temp_itm SET ret_prc = " & Double.Parse(txtRetPrc.Text) & ", prc_chg_app_cd='' WHERE row_id = " & rowId
                        .CommandText = "UPDATE temp_itm SET ret_prc = orig_prc, manual_prc=orig_prc,prc_chg_app_cd='' WHERE row_id = " & rowId   'lucy, 23-apr-15
                    End With
                    cmdUpdateItems.ExecuteNonQuery()

                    If AppConstants.Order.TYPE_SAL <> Session("ord_tp_cd") Then
                        theSalesBiz.UpdateLnTaxAmt(cmdUpdateItems, Double.Parse(txtRetPrc.Text), dgItem.Cells(SoLnGrid.ROW_ID).Text)
                    End If
                    Exit For
                End If
            Next
        Next

        'reapplies the discounts (if any) when applicable
        ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
        bindgrid()
        popupPriceChangeAppr.ShowOnPageLoad = False
        Session("SHOWPOPUP") = False    'mariam-sep 27,2016-price approval

        'clear out the lines that were approved this time.
        hidUpdLnNos.Value = String.Empty

        'the hidden url is the page that the user wanted to go to before bein prompted for summ prc approval. After
        ' capturing mgr approval, let the navigation resume to where it was going originally
        If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
            Response.Redirect(hidDestUrl.Value)
        End If

    End Sub

    ''' <summary>
    ''' Executes when the manager override is successfully carried out via the popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved

        If Session("SelectedRow") IsNot Nothing Then

            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(Gridview1.Items(CType(modifiedRow(0), Integer)), DataGridItem)
            If dgItem IsNot Nothing Then

                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)
                If Not IsNothing(ViewState("RETPRC")) AndAlso Not (ViewState("RETPRC").ToString = txtRetPrc.Text) Then
                    txtRetPrc.Text = ViewState("RETPRC")
                    ViewState("RETPRC") = Nothing
                Else
                    ViewState("RETPRC") = Nothing
                End If
                If txtRetPrc IsNot Nothing Then

                    'since the approval was successful, set the retail and manual price in the temp table  
                    Dim hidAppEmpCd As HiddenField = CType(ucMgrOverride.FindControl("hidAppEmpCd"), HiddenField)
                    Dim approverEmpCd As String = hidAppEmpCd.Value.ToString

                    Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

                    Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    conn.Open()
                    With cmdUpdateItems
                        .Connection = conn
                        .CommandText = "UPDATE temp_itm SET ret_prc = " & Double.Parse(txtRetPrc.Text) & ", manual_prc = " & Double.Parse(txtRetPrc.Text) &
                                        ", prc_chg_app_cd='" & approverEmpCd & "' WHERE row_id = " & modifiedRow(1)
                    End With
                    cmdUpdateItems.ExecuteNonQuery()

                    If AppConstants.Order.TYPE_SAL <> Session("ord_tp_cd") Then
                        theSalesBiz.UpdateLnTaxAmt(cmdUpdateItems, Double.Parse(txtRetPrc.Text), dgItem.Cells(SoLnGrid.ROW_ID).Text)
                    End If
                    'reapplies the discounts (if any) when applicable
                    ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
                    bindgrid()
                End If
            End If
        End If

        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If

        popupMgrOverride.ShowOnPageLoad = False

        'the hidden url is the page that the user wanted to go to before bein prompted for summ prc approval. After
        ' capturing mgr approval, let the navigation resume to where it was going originally
        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.Trim()) Then
            'clear out the lines that were approved this time.
            hidUpdLnNos.Value = String.Empty

            If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
                Response.Redirect(hidDestUrl.Value)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Gets fired when the manager override for 'line level' fails. Any resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled

        If Session("SelectedRow") IsNot Nothing Then

            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(Gridview1.Items(CType(modifiedRow(0), Integer)), DataGridItem)
            If dgItem IsNot Nothing Then

                'get the price field for the line being changed
                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("ret_prc"), TextBox)
                If txtRetPrc IsNot Nothing Then

                    Dim tmpItmRow As DataRow = GetTempItmIinfoByDoubleKey(Session.SessionID.ToString.Trim, dgItem.Cells(SoLnGrid.ROW_ID).Text, temp_itm_key_tp.tempItmBySessAndRowIdSeqNum).Rows(0)
                    '*** since the approval was aborted, reset the retail to the manual price, which was the last edited value
                    txtRetPrc.Text = FormatNumber(tmpItmRow("manual_prc"), 2)
                End If
            End If
        End If

        'reapplies the discounts (if any) when applicable
        ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
        bindgrid()
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If
        popupMgrOverride.ShowOnPageLoad = False

        'clear out the lines that were changed this time.
        hidUpdLnNos.Value = String.Empty

        'the hidden url is the page that the user wanted to go to before bein prompted for summ prc approval. After
        ' capturing mgr approval, let the navigation resume to where it was going originally
        If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
            Response.Redirect(hidDestUrl.Value)
        End If
    End Sub

    ''' <summary>
    ''' gets the zone code for the processing if required
    ''' 1) if cash and carry and need a zone code, then just get the first avail, on the written store
    ''' 2) otherwise, if zip code available and only one zone related to it, take that zone
    ''' 3) otherwise, if no zip or multiple zones, then have to redirect to delivery page to select new zip and/or zone
    ''' </summary>
    Private Sub GetZoneCode()

        If SessVar.zoneCd.isEmpty Then

            Dim strRedirectUrl As String = String.Empty
            If Request("LEAD") <> "TRUE" Then
                strRedirectUrl = "Delivery.aspx?src=order_detail"
            Else
                strRedirectUrl = "Delivery.aspx?LEAD=TRUE&src=order_detail"
            End If

            If theInvBiz.RequireZoneCd(SessVar.puDel, SessVar.isArsEnabled, SessVar.ordTpCd) Then

                If Session("cash_carry") = "TRUE" Then
                    ' got to use the written store and no matter what the sessvar.pudel is, this is a pickup
                    Dim zoneCd As String = theInvBiz.GetPriFillZoneCode(AppConstants.PICKUP, "", "", "", SessVar.wrStoreCd)
                    SessVar.zoneCd = zoneCd

                ElseIf SessVar.zipCode4Zone.isNotEmpty Then
                    Dim zoneResp As ZoneFromZipResp = theTranBiz.GetZoneCd4Zip(SessVar.zipCode4Zone)
                    If zoneResp.numRows = 1 And zoneResp.zoneCd.isNotEmpty Then
                        SessVar.zoneCd = zoneResp.zoneCd
                    Else
                        Response.Redirect(strRedirectUrl)
                    End If
                Else
                    Response.Redirect(strRedirectUrl)
                End If
            End If
        End If
    End Sub

    Private Sub KeepSessions()

        Dim dict As New StringDictionary()

        dict.Add("PD", Session("PD"))
        dict.Add("ZONE_CD", Session("ZONE_CD"))
        dict.Add("PD_STORE_CD", Session("PD_STORE_CD"))

        Session("RetainSesVals") = dict

    End Sub

    Private Sub UpdateLineItemsAvailDate()

        'this should only be executed for ARS enabled stores
        If (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS") AndAlso Not IsNothing(Session("RetainSesVals"))) Then

            Dim dict As StringDictionary = CType(Session("RetainSesVals"), StringDictionary)

            If dict.ContainsKey("PD") AndAlso dict.ContainsKey("ZONE_CD") AndAlso dict.ContainsKey("PD_STORE_CD") AndAlso
                (dict("PD") <> Session("PD") Or dict("ZONE_CD") <> Session("ZONE_CD") Or dict("PD_STORE_CD") <> Session("PD_STORE_CD")) Then

                Dim oraCon As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim strQry As String = "SELECT T.ROW_ID, T.ITM_CD, NVL(T.QTY, 0) AS QTY, T.AVAIL_DT, I.INVENTORY FROM ITM I, TEMP_ITM T WHERE SESSION_ID = :SESSID AND I.ITM_CD = T.ITM_CD ORDER BY T.ROW_ID"
                Dim oraCmd As OracleCommand = DisposablesManager.BuildOracleCommand(strQry, oraCon)
                oraCmd.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
                oraCmd.Parameters("sessId").Value = Session.SessionID.ToString()
                oraCon.Open()

                Dim oraReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(oraCmd)

                oraCmd.Parameters.Clear()

                Do While oraReader.Read()
                    Dim availDt As Date = Date.Today
                    If (oraReader("INVENTORY").Equals("Y")) Then
                        availDt = theInvBiz.CalcAvailARS(oraReader("ITM_CD"), Session("PD"), Session("ZONE_CD"), oraReader("QTY"), Session("pd_store_cd"))
                    End If
                    strQry = "UPDATE TEMP_ITM SET AVAIL_DT = :AVAIL_DT WHERE ROW_ID = :ROW_ID"
                    oraCmd = DisposablesManager.BuildOracleCommand(strQry, oraCon)

                    oraCmd.Parameters.Add("AVAIL_DT", OracleType.DateTime).Direction = ParameterDirection.Input
                    oraCmd.Parameters("AVAIL_DT").Value = availDt
                    oraCmd.Parameters.Add("ROW_ID", OracleType.Number).Direction = ParameterDirection.Input
                    oraCmd.Parameters("ROW_ID").Value = oraReader("ROW_ID")

                    oraCmd.ExecuteNonQuery()
                    oraCmd.Parameters.Clear()
                Loop

                oraReader.Close()
                oraCon.Close()
            End If
        End If

    End Sub

    Private Sub UpdateShowroomItemsAvailDate()

        'this should only be executed for ARS enabled stores
        If (Not IsNothing(Session("STORE_ENABLE_ARS")) AndAlso Session("STORE_ENABLE_ARS") AndAlso Not IsNothing(Session("RetainSesVals"))) Then

            Dim oraCon As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim strQry As String = "SELECT T.ROW_ID, T.ITM_CD, NVL(T.QTY, 0) AS QTY, T.AVAIL_DT, I.INVENTORY FROM ITM I, TEMP_ITM T WHERE SESSION_ID = :SESSID AND I.ITM_CD = T.ITM_CD ORDER BY T.ROW_ID"
            Dim oraCmd As OracleCommand = DisposablesManager.BuildOracleCommand(strQry, oraCon)
            oraCmd.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
            oraCmd.Parameters("sessId").Value = Session.SessionID.ToString()
            oraCon.Open()

            Dim oraReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(oraCmd)

            oraCmd.Parameters.Clear()

            Do While oraReader.Read()
                Dim availDt As Date = Date.Today
                strQry = "UPDATE TEMP_ITM SET AVAIL_DT = :AVAIL_DT WHERE ROW_ID = :ROW_ID"
                oraCmd = DisposablesManager.BuildOracleCommand(strQry, oraCon)

                oraCmd.Parameters.Add("AVAIL_DT", OracleType.DateTime).Direction = ParameterDirection.Input
                oraCmd.Parameters("AVAIL_DT").Value = availDt
                oraCmd.Parameters.Add("ROW_ID", OracleType.Number).Direction = ParameterDirection.Input
                oraCmd.Parameters("ROW_ID").Value = oraReader("ROW_ID")

                oraCmd.ExecuteNonQuery()
                oraCmd.Parameters.Clear()
            Loop

            oraReader.Close()
            oraCon.Close()
        End If

    End Sub

    ''' <summary>
    ''' Executes when the user clicks on the button to modify the tax code.
    ''' </summary>
    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkModifyTax.Click

        If (Not IsNothing(ViewState("CanModifyTax")) AndAlso Convert.ToBoolean(ViewState("CanModifyTax"))) Then
            Dim hidisinvokemanually As HiddenField = CType(ucTaxUpdate.FindControl("hidisinvokemanually"), HiddenField)
            hidisinvokemanually.Value = "true"

            Dim hidtaxdt As HiddenField = CType(ucTaxUpdate.FindControl("hidtaxdt"), HiddenField)
            hidtaxdt.Value = Session("tran_dt")

            'Refer MM-7026 JIRA - All Tax codes need to be displayed
            Dim _hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
            _hidZip.Value = String.Empty

            ucTaxUpdate.LoadTaxData()
            PopupTax.ShowOnPageLoad = True
        Else
            Dim msgpopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
            msgpopup.ShowOnPageLoad = True
        End If

    End Sub

    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated
        calculate_total(Page.Master)
        PopupTax.ShowOnPageLoad = False
    End Sub

    Private Sub ShowWarrantyPopup(focusedRowId As String)
        Dim itmCd As String
        Dim isInventory As Boolean
        Dim isWarrantable As Boolean

        Dim dtWarrantable As New DataTable
        dtWarrantable.Columns.Add("ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("ITM_TP_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_TEXT", GetType(String))
        dtWarrantable.Columns.Add("IS_PREV_SEL", GetType(Boolean))
        Dim drWarrantable As DataRow

        Dim dtSaleItems As DataTable = theSalesBiz.GetSalesItemForSOE(Session.SessionID)

        For Each dr As DataRow In dtSaleItems.Rows
            itmCd = dr("ITM_CD")
            isInventory = IIf("Y".Equals(dr.Item("INVENTORY").ToString), True, False)
            isWarrantable = IIf("Y".Equals(dr.Item("warrantable").ToString + ""), True, False)

            ' check if need to apply warranty to the selectedSKU
            If isInventory AndAlso isWarrantable AndAlso Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
                Dim warranty_skus As Boolean = SalesUtils.hasWarranty(itmCd)

                If warranty_skus = True Then
                    Dim discReCal As New SessVar.DiscountReCalcObj
                    discReCal = SessVar.discReCalc
                    discReCal.maybeWarr = True
                    SessVar.discReCalc = discReCal

                    If SysPms.isOne2ManyWarr Then
                        HBCG_Utils.CreateTempWarrFromTempItm(Session.SessionID.ToString.Trim, itmCd)
                    End If

                    drWarrantable = dtWarrantable.NewRow()
                    drWarrantable("ROW_ID") = dr("ROW_ID")
                    drWarrantable("ITM_CD") = itmCd
                    drWarrantable("ITM_TP_CD") = dr("ITM_TP_CD")
                    drWarrantable("WARR_ROW_ID") = dr("WARR_ROW_ID")
                    drWarrantable("WARR_ITM_CD") = dr("WARR_ITM_CD")

                    If dtWarrantable.Rows.Count > 0 Then
                        Dim filterExp As String = "IS_PREV_SEL = FALSE AND WARR_ROW_ID = '" & dr("WARR_ROW_ID") & "' AND WARR_ITM_CD = '" & dr("WARR_ITM_CD") & "'"
                        Dim drFiltered As DataRow() = dtWarrantable.Select(filterExp)
                        drWarrantable("IS_PREV_SEL") = drFiltered.Length > 0
                    Else
                        drWarrantable("IS_PREV_SEL") = False
                    End If

                    dtWarrantable.Rows.Add(drWarrantable)
                End If
            End If
        Next

        If dtWarrantable.Rows.Count > 0 Then
            hidWarrantyCheck.Value = 1

            Dim _popupWarranties As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucWarranties.FindControl("popupWarranties")
            _popupWarranties.Modal = True

            Dim _hidDelDocNum As HiddenField = _popupWarranties.FindControl("hidDelDocNum")
            _hidDelDocNum.Value = String.Empty

            Dim _hidFocusItem As HiddenField = _popupWarranties.FindControl("hidFocusItem")
            _hidFocusItem.Value = focusedRowId

            _popupWarranties.ShowOnPageLoad = True
            ucWarranties.LoadWarranties(dtWarrantable)
        End If
    End Sub

    Protected Sub ucWarranties_WarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.WarrantyAdded
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarrInfo As DataTable = CType(Session("WARRINFO"), DataTable)
            Dim sortExp As String = "WARR_ROW_ID, IS_PREV_SEL"
            Dim dt As DataTable = dtWarrInfo.Select(Nothing, sortExp).CopyToDataTable()

            Dim warrRowIdSeqNum As String = String.Empty
            Dim warrItmCd As String
            Dim drWarrRowId As DataRow()

            For Each dr As DataRow In dt.Rows

                warrItmCd = String.Empty

                'Need to fill the WARR_ROW_ID from warrRowIdSeqNum
                If Not IsDBNull(dr("IS_PREV_SEL")) AndAlso dr("IS_PREV_SEL") = True Then
                    warrItmCd = dr("WARR_ITM_CD")
                    drWarrRowId = dt.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" + warrItmCd + "'")

                    If drWarrRowId.Length > 0 Then
                        dr("WARR_ROW_ID") = drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID")
                    Else
                        dr("WARR_ROW_ID") = String.Empty
                    End If
                End If

                '***Check for Warranties being added or not after returning from warranties popup *******
                If Not IsDBNull(dr("WARR_ITM_CD")) AndAlso Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                    If String.IsNullOrEmpty(dr("WARR_ROW_ID")) Then

                        '*** Implies that a new warranty sku is being added after the popup selection. In this case a new 
                        '*** line for the warranty sku needs to be added. Then, the link info on the warrantable sku 
                        '*** also needs to be updated.
                        If Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                            '--means a warrantable sku was selected
                            warrRowIdSeqNum = CreateWarrantyLn(dr("WARR_ITM_CD"), dr("ROW_ID"))
                            dr("WARR_ROW_ID") = warrRowIdSeqNum
                        End If
                    Else
                        '*** Means that an existing warranty was selected, so just update the link info on the warrantable
                        '*** sku. No need to add a separate warranty line.
                        UpdtWarrLnk(dr("WARR_ITM_CD"), dr("WARR_ROW_ID"), dr("ROW_ID"))
                    End If
                    ' if we might add more records from warr or related, then re-calc discount later
                    If SessVar.discReCalc.maybeRelated = True Then
                        SessVar.discReCalc.needToReCalc = True
                    Else
                        If SessVar.LastLnSeqNum.isNotEmpty() Then
                            ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                        End If
                        'UpdtWarrLnk(strWarItmCd, GetCurrentLineNum(), strRowNo)
                    End If
                End If
            Next

            bindgrid()
        End If

        If Not String.IsNullOrEmpty(hidItemCd.Value) And hidWarrantyClicked.Value = 0 Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
        End If

        Session.Remove("WARRINFO")
        hidWarrantyClicked.Value = 0
    End Sub

    Protected Sub ucWarranties_NoWarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.NoWarrantyAdded
        ' if related SKUs then go get any
        If Not String.IsNullOrEmpty(hidItemCd.Value) And hidWarrantyClicked.Value = 0 Then
            Dim relatedSku As String = hidItemCd.Value
            hidItemCd.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
            ' if we might add more records from warr or related, then re-calc discount later
        ElseIf SessVar.discReCalc.needToReCalc = True Then
            If SessVar.LastLnSeqNum.isNotEmpty() Then
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                bindgrid()  ' needed to show discounted prices
            End If
        End If

        hidWarrantyClicked.Value = 0
    End Sub

    Private Sub ShowRelatedSkuPopUp(itemcd As String)
        If SessVar.LastLnSeqNum.isNotEmpty() Then
            ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
        End If

        If Not String.IsNullOrEmpty(itemcd) And Session("ord_tp_cd") = "SAL" Then
            SessVar.discReCalc.maybeRelated = True

            ucRelatedSku.LoadRelatedSKUData(itemcd.Replace("'", String.Empty))
            hidWarrantyCheck.Value = 0
        End If
    End Sub

    Protected Sub ucRelatedSku_AddSelection(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSku.AddSelection
        hidItemCd.Value = String.Empty
        Dim skus As String = String.Empty

        For Each obj As Object In selectedRelSKUs
            If Not IsNothing(obj) AndAlso Not String.IsNullOrEmpty(obj) Then
                skus = skus & "," & UCase(obj.ToString())
            End If
        Next

        If Not String.IsNullOrEmpty(skus) Then
            Session("ITEMID") = skus

            'Add the SKU
            insert_records()

            ' if we might add more records from warr or related, then re-calc discount later, otherwise re-calc now if appropriate
            DoDiscReCalcNowOrLater()

            'Populate Datagrid with new records
            bindgrid()
        End If
    End Sub

    Protected Sub ucRelatedSku_NoneAdded(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSku.NoneAdded
        hidItemCd.Value = String.Empty

        If SessVar.discReCalc.needToReCalc = True Then
            If SessVar.LastLnSeqNum.isNotEmpty() Then
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)

                bindgrid() ' needed to show discounted prices
            End If
        End If
    End Sub
    Public Sub RemoveUpdLn(ByVal rowId As String)
        Dim seperator As Char = ";"c
        Dim sArr() As String = hidUpdLnNos.Value.ToString().Split(seperator)
        If sArr.Contains(rowId) Then
            Dim index As Integer = hidUpdLnNos.Value.LastIndexOf(seperator)
            hidUpdLnNos.Value = hidUpdLnNos.Value.Substring(0, index)
        End If
    End Sub

    ''' <summary>
    ''' Line level manager approval popup display.
    ''' </summary>
    ''' <param name="dgItem"></param>
    ''' <remarks></remarks>
    Protected Sub MgrByLineApprovalPopup(ByRef dgItem As DataGridItem)
        'means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
        Session("SelectedRow") = dgItem.ItemIndex & ":" & dgItem.Cells(SoLnGrid.ROW_ID).Text

        Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = popupMgrOverride.FindControl("txtMgrPwd")
        If txtMgrPwd IsNot Nothing Then
            txtMgrPwd.Focus()
        End If
        popupMgrOverride.ShowOnPageLoad = True
        Session("SHOWPOPUP") = True
    End Sub

End Class
