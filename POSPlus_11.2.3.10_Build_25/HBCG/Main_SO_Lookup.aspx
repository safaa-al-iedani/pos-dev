<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main_SO_Lookup.aspx.vb"
    Inherits="Main_SO_Lookup" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
        <dx:ASPxGridView ID="GridView2" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="DEL_DOC_NUM" EnableCallBacks="False" OnDataBinding="grid_DataBinding">
            <SettingsBehavior AllowSort="true" />
           <%-- <Settings ShowFilterRow="true" />--%>
            <SettingsPager Visible="False">
            </SettingsPager>
            <SettingsBehavior AllowSort="False" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label511 %>" FieldName="DEL_DOC_NUM" VisibleIndex="0" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label668 %>" FieldName="SO_WR_DT" VisibleIndex="1" Settings-AllowSort="True" >
                    <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label633 %>" FieldName="ORD_TP_CD" VisibleIndex="2" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Stat" FieldName="STAT_CD" VisibleIndex="3" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label211 %>" FieldName="SHIP_TO_F_NAME" Visible="False" Settings-AllowSort="True" 
                    VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label131 %>" FieldName="FULL_NAME" VisibleIndex="3" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label229 %>" FieldName="SHIP_TO_H_PHONE" VisibleIndex="4" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label572 %>" FieldName="SO_STORE_CD" VisibleIndex="5" Settings-AllowSort="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="6" Settings-AllowSort="True" >
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="<%$ Resources:LibResources, Label528 %>" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <%--<asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Font-Size="XX-Small" Width="100%" AllowPaging="True" BorderStyle="None" BorderColor="White"
            BorderWidth="0px" PagerSettings-Visible="false">

            <Columns>
                <asp:BoundField DataField="DEL_DOC_NUM" HeaderText="SO#" SortExpression="Delivery Document">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SO_WR_DT" HeaderText="Written Date" HtmlEncode="False"
                    SortExpression="Written Date" DataFormatString="{0:MM/dd/yyyy}">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ORD_TP_CD" HeaderText="Type" HtmlEncode="False" SortExpression="Status">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="STAT_CD" HeaderText="Stat" HtmlEncode="False" SortExpression="Status">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SHIP_TO_F_NAME" SortExpression="First Name" Visible="False">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FULL_NAME" HeaderText="Customer" SortExpression="Last Name">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SHIP_TO_H_PHONE" HeaderText="Home Phone" SortExpression="Home Phone">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SO_STORE_CD" HeaderText="Store" SortExpression="Store Code">
                    <ControlStyle Font-Size="X-Small" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" ButtonType="Button">
                    <ControlStyle CssClass="style5" />
                </asp:CommandField>
            </Columns>
            <PagerSettings Visible="False" />
            <AlternatingRowStyle BackColor="Beige" />
        </asp:GridView>--%>
        <table width="98%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label210 %>"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label425 %>"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label337 %>"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label261 %>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="40%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="<%$ Resources:LibResources, Label527 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;">
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
            &nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label451 %>" />:&nbsp;<asp:DropDownList ID="cbo_records" runat="server" AutoPostBack="True"
                CssClass="style5" Style="position: relative">
                <asp:ListItem Selected="True" Value="10">10</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="100">100</asp:ListItem>
                <asp:ListItem Value="9999" Text="<%$ Resources:LibResources, Label23 %>"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
</asp:Content>
