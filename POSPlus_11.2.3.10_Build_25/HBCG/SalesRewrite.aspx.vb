Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports DevExpress.Web.ASPxPopupControl
Imports HBCG_Utils
Imports SalesUtils

Partial Class SalesRewrite
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private gPointCnt As String = "0"

    ' TODO - still need to document methods, cache syspms, and change to bind variables all over but mostly older and add all the missing stuff
    Private Class SoLnGrid

        Public Const DEL_DOC_LN_NUM As Integer = 0
        Public Const QTY As Integer = 1
        Public Const STORE_CD As Integer = 2
        Public Const LOC_CD As Integer = 3
        Public Const ITM_CD As Integer = 4
        Public Const UNIT_PRC As Integer = 5
    End Class

    Private Class ArTrnGrid

        Public Const AR_TRN_PK As Integer = 0
        Public Const ORIGIN_STORE As Integer = 1
        Public Const CSH_DWR_CD As Integer = 2
        Public Const AMT As Integer = 3
        Public Const MOP_CD As Integer = 4
    End Class

    Private Class DelDtGrid

        Public Const DEL_DT As Integer = 0
        Public Const DAY As Integer = 1
        Public Const DEL_STOPS As Integer = 2
        Public Const ACTUAL_STOPS As Integer = 3
        Public Const REMAINING_STOPS As Integer = 4
        Public Const SELECT_BUTTON As Integer = 5
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTaxOrig")) OrElse ViewState("CanModifyTaxOrig").ToString = String.Empty Then
            ViewState("CanModifyTaxOrig") = SecurityUtils.hasSecurity(SecurityUtils.OVERRIDE_TAXCD_MGMT, Session("EMP_CD"))
        End If


        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTaxNew")) OrElse ViewState("CanModifyTaxNew").ToString = String.Empty Then
            ViewState("CanModifyTaxNew") = SecurityUtils.hasSecurity(SecurityUtils.OVERRIDE_TAXCD_ENTRY, Session("EMP_CD"))
        End If

        If Not IsPostBack Then

            store_cd_populate()
            lbl_error.Text = ""
            cbo_pd_date.MinDate = Today.Date
            cbo_pd_date.MaxDate = Convert.ToDateTime("12/31/2049").Date
            cbo_pd_date_new.MinDate = Today.Date
            cbo_pd_date_new.MaxDate = Convert.ToDateTime("12/31/2049").Date

            If Request("RELATED_SKU") & "" <> "" Then

                txt_del_doc_num.Text = Request("RELATED_SKU")

                Populate_Results()

                If lbl_cust_cd.Text & "" <> "" Then
                    btn_Clear.Visible = True
                    btn_Save.Visible = True
                    btnSOM.Visible = True
                    btnSOM.Enabled = True
                End If
            End If
        Else
            lbl_error.Text = ""
        End If
        lblZoneChangeNotification.Text = String.Empty
    End Sub
    Public Function lucy_ctl_change(ByRef p_del_doc_num As String, ByRef p_ln As OracleNumber)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_err As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_ctl_change.pos_main"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = p_del_doc_num
        myCMD.Parameters.Add(New OracleParameter("p_ln", OracleType.Number)).Value = p_ln
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 200).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()


            If IsDBNull(myCMD.Parameters("p_err").Value) = False Then   ' allow final if str_err=Y
                str_err = myCMD.Parameters("p_err").Value
            End If

            If str_err <> "Y" Then
                str_err = "Rewrite not allowed. Lines have been picked or checked. The user will need to unpick the affected lines for the above changes to be allowed."
                btn_Save.Enabled = False
            Else
                btn_Save.Enabled = True
            End If
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_err
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If btn_submit.Enabled = True Then
            lbl_error.Text = ""
            Populate_Results()

            If lbl_cust_cd.Text & "" <> "" Then
                btn_Clear.Visible = True
                btn_Save.Visible = True
                btnSOM.Visible = True
                btnSOM.Enabled = True
            End If
        End If

    End Sub

    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click

        If Not Is_Validate_Successful() Then
            Exit Sub
        End If

        'the connection is created so that either all changes are saved OR
        'if an error all changes are rolled back
        Dim cmd As OracleCommand
        Dim transaction As OracleTransaction
        Dim sql As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim delAvDT As Boolean = False
        delAvDT = CheckForDeliveryAvaDT()
        If delAvDT Then
            Try
                conn.Open()
                cmd = DisposablesManager.BuildOracleCommand(conn)
                transaction = conn.BeginTransaction()
                cmd.Transaction = transaction

                Dim newDelDocNum As String = txt_del_doc_new.Text.Trim
                Dim origDelDocNum As String = txt_del_doc_existing.Text.Trim

                Process_So_Headers(origDelDocNum, newDelDocNum, cmd)

                Process_Auto_Comments(origDelDocNum, newDelDocNum, cmd)

                Process_Finance(origDelDocNum, newDelDocNum, cmd)

                Process_So_Lines(origDelDocNum, newDelDocNum, cmd)

                Process_Payments(origDelDocNum, newDelDocNum, cmd)

                Process_AR_Headers(origDelDocNum, newDelDocNum, lbl_cust_cd.Text, cmd)

                'saves all the pending changes.
                transaction.Commit()
                lbl_error.Text = "    Changes saved."

            Catch ex As Exception
                ' Attempt to roll back the transaction.
                lbl_error.Text = " Error while saving.  Try saving again. " + ex.Message
                Try
                    transaction.Rollback()
                Catch ex2 As Exception
                    ' This catch block will handle any errors that may have occurred
                    ' on the server that would cause the rollback to fail, such as
                    ' a closed connection.
                    Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                    Console.WriteLine("Message: {0}", ex2.Message)
                End Try
                'Throw
            Finally
                If Not IsNothing(transaction) Then
                    transaction.Dispose()
                End If
                cmd.Dispose()
                conn.Close()
                conn.Dispose()

            End Try

            btn_Save.Enabled = False
            btnSOM.Enabled = True
            btn_submit.Enabled = False
            grd_ar.Enabled = False
            grd_ar_new.Enabled = False
            grd_lines.Enabled = False
            grd_lines_new.Enabled = False
            btn_original.Enabled = True
            btn_new.Enabled = True
            ResetFormControlValues(Me)
        Else
            Dim strPointorStop As String = String.Empty
            If SysPms.isDelvByPoints Then
                strPointorStop = "Point"
            Else
                strPointorStop = "Stop"
            End If
            lbl_error.Text = String.Format(Resources.POSErrors.ERR0040, strPointorStop)
        End If
    End Sub

    Private Function Is_Validate_Successful() As Boolean
        'it will return TRUE if all validations pass, and the save process can continue, FALSE otherwise

        Dim is_Success As Boolean = True
        lbl_error.Text = ""

        If cbo_pd_date.Text & "" <> "" Then
            If Not IsDate(cbo_pd_date.Value.ToString) Then
                lbl_error.Text = "Old Pickup/Delivery date is invalid"
                is_Success = False
                Return is_Success
            End If
        Else
            lbl_error.Text = "Old Pickup/Delivery date is invalid"
            is_Success = False
            Return is_Success
        End If
        If IsZoneSelected.Value = False And cbo_PD_new.SelectedValue = "D" Then
            is_Success = False
            lbl_error.Text = "Zone code and delivery date has not been selected"
            Return is_Success
        End If
        If Not IsNumeric(txt_del_chg.Text.ToString) Then
            lbl_error.Text = "Old Pickup/Delivery charge must be numeric"
            txt_del_chg.Focus()
            is_Success = False
            Return is_Success
        End If

        If Not IsNumeric(txt_setup.Text.ToString) Then
            lbl_error.Text = "Old Setup charge must be numeric"
            txt_setup.Focus()
            is_Success = False
            Return is_Success
        End If

        If Not IsNumeric(txt_finance.Text.ToString) Then
            lbl_error.Text = "Old Finance charge must be numeric"
            txt_finance.Focus()
            is_Success = False
            Return is_Success
        End If

        If cbo_pd_date_new.Text & "" <> "" Then
            If Not IsDate(cbo_pd_date_new.Value.ToString) Then
                lbl_error.Text = "New Pickup/Delivery date is invalid"
                is_Success = False
                Return is_Success
            End If
        Else
            lbl_error.Text = "New Pickup/Delivery date is invalid"
            is_Success = False
            Return is_Success
        End If

        If Not IsNumeric(txt_del_chg_new.Text.ToString) Then
            lbl_error.Text = "Pickup/Delivery charge must be numeric"
            txt_del_chg_new.Focus()
            is_Success = False
            Return is_Success
        End If

        If Not IsNumeric(txt_setup_new.Text.ToString) Then
            lbl_error.Text = "Setup charge must be numeric"
            txt_setup_new.Focus()
            is_Success = False
            Return is_Success
        End If

        If Not IsNumeric(txt_finance_new.Text.ToString) Then
            lbl_error.Text = "Finance charge must be numeric"
            is_Success = False
            txt_finance_new.Focus()
            Return is_Success
        End If

        Dim atLeastOneOrigLine As Boolean = False
        Dim atLeastOneNewLine As Boolean = False
        Dim splitOnPo As Boolean = False
        Dim datRowSoLn As DataRow
        Dim dsOrig As DataSet = Session("DEL_DOC_LN")
        Dim dsNew As DataSet = Session("LN_NEW")

        For Each datRowSoLn In dsOrig.Tables(0).Rows

            If Not String.IsNullOrEmpty(datRowSoLn("ITM_CD").ToString) Then

                atLeastOneOrigLine = True
                'Exit For   'no need to keep on searching

            End If
            If (Not datRowSoLn("po_cd") Is Nothing) AndAlso datRowSoLn("po_cd").ToString.isNotEmpty AndAlso
                CDbl(datRowSoLn("QTY").ToString) > 0 AndAlso CDbl(datRowSoLn("orig_soln_qty").ToString) > CDbl(datRowSoLn("QTY").ToString) Then

                lbl_error.Text = "Line " + datRowSoLn("DEL_DOC_LN_NUM").ToString + " is attached to a purchase order -- Must move entire quantity or none"
                is_Success = False
                Exit For   'no need to keep on searching
            End If
        Next

        If Not is_Success Then
            Return is_Success
        End If

        For Each datRowSoLn In dsNew.Tables(0).Rows
            If Not String.IsNullOrEmpty(datRowSoLn("ITM_CD").ToString) Then
                atLeastOneNewLine = True
                Exit For   'no need to keep on searching
            End If
        Next

        If atLeastOneOrigLine = False Or atLeastOneNewLine = False Then
            lbl_error.Text = "You must have at least one line on each order"
            is_Success = False
        End If

        Return is_Success

    End Function

    Private Sub Process_So_Headers(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)

        'TODO potential issues with zone code on pickups especially if PUM enabled; so_wr_sec (SORW makes same as orig)
        'TODO also copied from orig in SORW:  prt_emp_cd, prt_con_cd, prt_dt, prt_ivc_cnt, prt_prg_nm

        Dim soRowid As String = String.Empty
        Try
            soRowid = SalesUtils.LockSoRec(origDelDocNum, cmd)

        Catch oraex As OracleException
            If oraex.Code = AppConstants.OraErr.RECORD_LOCKED Then
                Throw New Exception(String.Format(Resources.POSErrors.ERR0042, origDelDocNum))
                'lbl_error.Text = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text)
            Else
                Throw oraex
            End If
        Catch genExp As Exception
            Throw genExp
        End Try

        'ZONE should be kept for a delivery OR if the written store is ARS enabled then for pickups too
        Dim isWrStARSEnabled As Boolean = "Y".Equals(hidWrStARS.Value.ToString)
        Dim sql As New StringBuilder

        'Create the new sale
        sql.Append("INSERT INTO SO (").Append(
                        "DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, FIN_CUST_CD, ").Append(
                        "SHIP_TO_ST_CD, SHIP_TO_ZONE_CD, TAX_CD, TET_CD, ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, ").Append(
                        "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, ").Append(
                        "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, ").Append(
                        "SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, ").Append(
                        "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, ").Append(
                        "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, ").Append(
                        "PCT_OF_SALE3, ORDER_PO_NUM, SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, ").Append(
                        "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, PU_DEL, PU_DEL_DT, DEL_CHG, ").Append(
                        "SETUP_CHG, TAX_CHG, STAT_CD, FINAL_DT, FINAL_STORE_CD, MASF_FLAG, ORIG_DEL_DOC_NUM, SER_CNTR_CD, ").Append(
                        "SER_CNTR_VOID_FLAG, ORIG_FI_AMT, APPROVAL_CD, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, ").Append(
                        "CASH_AND_CARRY, PRT_DT, PRT_PU_SLIP_CNT, PRT_IVC_CNT, PRT_PRG_NM, PU_SLIP_STAT_CD, GRID_NUM, PRT_INS_CNT, ").Append(
                        "REQUESTED_FI_AMT, ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, DRIVER_EMP_CD, HELPER_EMP_CD, FRAN_SALE, ").Append(
                        "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT, CONF_STAT_CD) ")

        sql.Append("SELECT :NEW_DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, FIN_CUST_CD, ").Append(
                       "SHIP_TO_ST_CD, :SHIP_TO_ZONE_CD, :TAX_CD, TET_CD, ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, ").Append(
                       ":PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, ").Append(
                       "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, ").Append(
                       "SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, ").Append(
                       "SHIP_TO_ADDR2, :SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, ").Append(
                       "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, ").Append(
                       "PCT_OF_SALE3, ORDER_PO_NUM, :SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, ").Append(
                       "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, :PU_DEL, :PU_DEL_DT, :DEL_CHG, ").Append(
                       ":SETUP_CHG, :TAX_CHG, STAT_CD, FINAL_DT, FINAL_STORE_CD, MASF_FLAG, ORIG_DEL_DOC_NUM, SER_CNTR_CD, ").Append(
                       "SER_CNTR_VOID_FLAG, :ORIG_FI_AMT, APPROVAL_CD, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, ").Append(
                       "CASH_AND_CARRY, PRT_DT, PRT_PU_SLIP_CNT, PRT_IVC_CNT, PRT_PRG_NM, PU_SLIP_STAT_CD, GRID_NUM, PRT_INS_CNT, ").Append(
                       ":REQ_FI_AMT, ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, DRIVER_EMP_CD, HELPER_EMP_CD, FRAN_SALE, ").Append(
                       "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT ").Append(
                       ",'" & theSalesBiz.GetFirstConfirmationCode() & "'  ").Append(
                       "FROM SO WHERE DEL_DOC_NUM=:DEL_DOC_NUM ")
        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
        cmd.Parameters.Add(":SHIP_TO_ZIP_CD", OracleType.VarChar)
        cmd.Parameters.Add(":TAX_CD", OracleType.VarChar)
        cmd.Parameters.Add(":SO_WR_SEC", OracleType.Number)
        cmd.Parameters.Add(":PU_DEL", OracleType.VarChar)
        cmd.Parameters.Add(":PU_DEL_DT", OracleType.DateTime)
        cmd.Parameters.Add(":PU_DEL_STORE_CD", OracleType.VarChar)
        cmd.Parameters.Add(":DEL_CHG", OracleType.Number)
        cmd.Parameters.Add(":SETUP_CHG", OracleType.Number)
        cmd.Parameters.Add(":TAX_CHG", OracleType.Number)
        cmd.Parameters.Add(":ORIG_FI_AMT", OracleType.Number)
        cmd.Parameters.Add(":REQ_FI_AMT", OracleType.Number)

        cmd.Parameters(":NEW_DEL_DOC_NUM").Value = newDelDocNum
        cmd.Parameters(":DEL_DOC_NUM").Value = origDelDocNum
        'Zone should be 
        cmd.Parameters(":SHIP_TO_ZONE_CD").Value = If(isWrStARSEnabled OrElse AppConstants.DELIVERY.Equals(cbo_PD_new.SelectedValue.ToString),
                                                      IIf(String.IsNullOrEmpty(hidZoneCode.Value), txt_zone_cd.Text.ToString, hidZoneCode.Value), txt_zone_cd.Text)
        'SHIP_TO_ZIP_CD should always customers zip code
        cmd.Parameters(":SHIP_TO_ZIP_CD").Value = hidCustomerZipCode.Value
        cmd.Parameters(":TAX_CD").Value = txt_tax_cd_new.Text.ToString.ToUpper()
        cmd.Parameters(":SO_WR_SEC").Value = DateDiff("s", FormatDateTime(Today.Date, 2) & " 00:00:00", Now)
        cmd.Parameters(":PU_DEL").Value = cbo_PD_new.SelectedValue.ToString
        cmd.Parameters(":PU_DEL_DT").Value = cbo_pd_date_new.Value.ToString
        cmd.Parameters(":DEL_CHG").Value = CDbl(txt_del_chg_new.Text.ToString)
        cmd.Parameters(":PU_DEL_STORE_CD").Value = cbo_new_pd_store_cd.SelectedValue.ToString
        cmd.Parameters(":SETUP_CHG").Value = CDbl(txt_setup_new.Text.ToString)
        If TaxUtils.isHdrTax("") Then
            cmd.Parameters(":TAX_CHG").Value = CDbl(txt_tax_new.Text.ToString)
        Else
            Dim hdrtaxChg As Double = 0.0
            Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")
            If theSoHdrInfo.delChgTaxOnNew.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.delChgTaxOnNew) Then
                hdrtaxChg = CDbl(theSoHdrInfo.delChgTaxOnNew)
            End If
            If theSoHdrInfo.setupChgTaxOnNew.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.setupChgTaxOnNew) Then
                hdrtaxChg = hdrtaxChg + CDbl(theSoHdrInfo.setupChgTaxOnNew)
            End If
            cmd.Parameters(":TAX_CHG").Value = hdrtaxChg
        End If
        cmd.Parameters(":ORIG_FI_AMT").Value = CDbl(txt_finance_new.Text.ToString)  ' orig and requested are maintained the same in E1 ???
        cmd.Parameters(":REQ_FI_AMT").Value = CDbl(txt_finance_new.Text.ToString)
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        sql.Clear()

        'Update the Original Sale
        sql.Append("UPDATE SO SET ").Append(
                        "PU_DEL=:PU_DEL, ").Append(
                        "PU_DEL_DT=:PU_DEL_DT, ").Append(
                        "DEL_CHG=:DEL_CHG, ").Append(
                        "SHIP_TO_ZONE_CD=:SHIP_TO_ZONE_CD, ").Append(
                        "SETUP_CHG=:SETUP_CHG, ").Append(
                        "TAX_CHG=:TAX_CHG, ").Append(
                        "ORIG_FI_AMT=:ORIG_FI_AMT, requested_fi_amt = :REQ_FI_AMT, TAX_CD = :TAX_CD ")
        If CDbl(txt_finance.Text.ToString) < 0.001 Then
            sql.Append(", approval_cd = NULL, fin_cust_cd = NULL ")
        End If
        sql.Append("WHERE DEL_DOC_NUM=:DEL_DOC_NUM ")
        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters.Add(":PU_DEL", OracleType.VarChar)
        cmd.Parameters.Add(":PU_DEL_DT", OracleType.DateTime)
        cmd.Parameters.Add(":DEL_CHG", OracleType.Number)
        cmd.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
        cmd.Parameters.Add(":SETUP_CHG", OracleType.Number)
        cmd.Parameters.Add(":TAX_CHG", OracleType.Number)
        cmd.Parameters.Add(":ORIG_FI_AMT", OracleType.Number)
        cmd.Parameters.Add(":REQ_FI_AMT", OracleType.Number)
        cmd.Parameters.Add(":TAX_CD", OracleType.VarChar)

        cmd.Parameters(":DEL_DOC_NUM").Value = origDelDocNum
        cmd.Parameters(":PU_DEL").Value = cbo_PD.SelectedValue.ToString
        cmd.Parameters(":SHIP_TO_ZONE_CD").Value = If(isWrStARSEnabled OrElse AppConstants.DELIVERY.Equals(cbo_PD.SelectedValue.ToString),
                                                      txt_zone_cd.Text.ToString, "")
        cmd.Parameters(":PU_DEL_DT").Value = cbo_pd_date.Value.ToString
        cmd.Parameters(":DEL_CHG").Value = CDbl(txt_del_chg.Text.ToString)
        cmd.Parameters(":SETUP_CHG").Value = CDbl(txt_setup.Text.ToString)
        If TaxUtils.isHdrTax("") Then
            cmd.Parameters(":TAX_CHG").Value = CDbl(txt_tax.Text.ToString)
        Else
            Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")
            Dim hdrtaxChg As Double = 0.0
            If theSoHdrInfo.delChgTaxOnOrig.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.delChgTaxOnOrig) Then
                hdrtaxChg = CDbl(theSoHdrInfo.delChgTaxOnOrig)
            End If
            If theSoHdrInfo.setupChgTaxOnOrig.isNotEmpty AndAlso IsNumeric(theSoHdrInfo.setupChgTaxOnOrig) Then
                hdrtaxChg = hdrtaxChg + CDbl(theSoHdrInfo.setupChgTaxOnOrig)
            End If
            cmd.Parameters(":TAX_CHG").Value = hdrtaxChg
        End If
        'cmd.Parameters(":TAX_CHG").Value = CDbl(txt_tax.Text.ToString)
        cmd.Parameters(":ORIG_FI_AMT").Value = CDbl(txt_finance.Text.ToString)
        cmd.Parameters(":REQ_FI_AMT").Value = CDbl(txt_finance.Text.ToString)
        cmd.Parameters(":TAX_CD").Value = txt_tax_cd.Text.ToString.ToUpper()

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub

    ''' <summary>
    ''' Processes any auto comments that need to be inserted.
    ''' </summary>
    ''' <param name="origDelDocNum">the original document/order #</param>
    ''' <param name="newDelDocNum">the new document #</param>
    ''' <param name="cmd">the oracle command to use</param>
    Private Sub Process_Auto_Comments(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)

        ' Add auto comment for tax code change for both original and new sale tax, if  they have changed.
        If hidOrigTaxCd.Value.ToString.isNotEmpty Then

            If (hidOrigTaxCd.Value.ToString <> txt_tax_cd.Text) Then
                'means that the tax code on the original sale has changed
                Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")
                SaveAutoComment(cmd, txt_del_doc_num.Text, theSoHdrInfo.writtenDt, "TCD", hidOrigTaxCd.Value.ToString, txt_tax_cd.Text, Session("EMP_CD"))
            End If

            If (hidOrigTaxCd.Value.ToString <> txt_tax_cd_new.Text) Then
                'means that the tax code on the new sale has changed
                SaveAutoComment(cmd, txt_del_doc_num.Text, FormatDateTime(Now(), DateFormat.ShortDate), "TCD", hidOrigTaxCd.Value.ToString, txt_tax_cd_new.Text, Session("EMP_CD"))
            End If
        End If
    End Sub

    Private Sub Process_Finance(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        ' If finance split, copy finance information to new; if all finance moved, then move finance info to new;
        ' SO_ASP and SO_ASP_RESP records

        If (CDbl(txt_finance_new.Text.ToString) > 0) Then

            Dim sqlStmt As String = ""
            Dim sql As New StringBuilder
            ' finance split
            If (CDbl(txt_finance.Text.ToString) > 0) Then

                sql.Append("INSERT INTO SO_ASP (").Append(
                                        "DEL_DOC_NUM, PROMO_CD, AS_PROMO_CD, DISC_PCT, SPIFF, DEFER_PAY_MTH, DEFER_INT_MTH, ").Append(
                                        "DEFER_UNTIL, INS_CD, EMP_SLSP_CD, SUB_ACCT, PROMO_DES, PROMO_DES1, PROMO_DES2, PROMO_DES3, ").Append(
                                        "PROMO_APR, PROMO_APR_FLAG, ACCT_APR, ACCT_APR_FLAG, MANUAL)").Append(
                                    "SELECT :newDelDocNum, PROMO_CD, AS_PROMO_CD, DISC_PCT, SPIFF, DEFER_PAY_MTH, DEFER_INT_MTH, ").Append(
                                        "DEFER_UNTIL, INS_CD, EMP_SLSP_CD, SUB_ACCT, PROMO_DES, PROMO_DES1, PROMO_DES2, PROMO_DES3, ").Append(
                                        "PROMO_APR, PROMO_APR_FLAG, ACCT_APR, ACCT_APR_FLAG, MANUAL ").Append(
                                        "FROM SO_ASP   WHERE  DEL_DOC_NUM = :origDelDocNum")

                cmd.CommandText = sql.ToString
                cmd.CommandType = CommandType.Text

                cmd.Parameters.Clear()
                cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
                cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)

                cmd.Parameters(":origDelDocNum").Value = origDelDocNum
                cmd.Parameters(":newDelDocNum").Value = newDelDocNum

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
                sql.Clear()

                sql.Append("INSERT INTO SO_ASP_RESP (DEL_DOC_NUM, AS_CD, RESP_CD, RESP_DT_TIME, RESP_TP_CD) ").Append(
                                "SELECT :newDelDocNum, as_cd, resp_cd, resp_dt_time, resp_tp_cd ").Append(
                                "FROM SO_ASP_RESP WHERE  DEL_DOC_NUM = :origDelDocNum")
                cmd.CommandText = sql.ToString
                cmd.CommandType = CommandType.Text

                cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
                cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)

                cmd.Parameters(":origDelDocNum").Value = origDelDocNum
                cmd.Parameters(":newDelDocNum").Value = newDelDocNum

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()

            Else ' all finance moved to new

                sqlStmt = "UPDATE so_asp SET del_Doc_num = :newDelDocNum WHERE del_doc_num = :origDelDocNum "
                cmd.CommandText = sqlStmt
                cmd.CommandType = CommandType.Text

                cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
                cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)

                cmd.Parameters(":origDelDocNum").Value = origDelDocNum
                cmd.Parameters(":newDelDocNum").Value = newDelDocNum

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()

                sqlStmt = "UPDATE so_asp_resp SET del_Doc_num = :newDelDocNum WHERE del_doc_num = :origDelDocNum "
                cmd.CommandText = sqlStmt
                cmd.CommandType = CommandType.Text

                cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
                cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)

                cmd.Parameters(":origDelDocNum").Value = origDelDocNum
                cmd.Parameters(":newDelDocNum").Value = newDelDocNum

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
            End If
        End If
    End Sub

    Private Sub Process_Payments(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        'Update AR_TRN Payments

        Dim sql As String = ""
        Dim ds4 As DataSet = Session("AR")
        Dim dr4 As DataRow
        Dim orig_pmt_amt As Double = 0
        Dim new_pmt_amt_on_orig As Double = 0

        For Each dr4 In ds4.Tables(0).Rows
            orig_pmt_amt = CDbl(Check_AR_TRN(dr4("AR_TRN_PK")))   ' TODO - why can we not just retain orig_pmt_amt from orig trans (like orig_soln_qty) instead of re-acquiring from db? - not hitting AR_TRN - huge
            new_pmt_amt_on_orig = CDbl(dr4("AMT"))

            If (new_pmt_amt_on_orig > 0) And (orig_pmt_amt > new_pmt_amt_on_orig) Then


                Dim ct As CommandType = cmd.CommandType
                cmd.CommandType = CommandType.Text
                Insert_New_AR_TRN(dr4("AR_TRN_PK"), (orig_pmt_amt - new_pmt_amt_on_orig), cmd)
                Update_AR_TRN(dr4("AR_TRN_PK"), new_pmt_amt_on_orig, cmd)
                cmd.CommandType = ct

            ElseIf new_pmt_amt_on_orig = 0 Then

                sql = "UPDATE AR_TRN SET IVC_CD = :newDelDocNum WHERE AR_TRN_PK = :ar_trn_pk "

                cmd.CommandText = sql
                cmd.CommandType = CommandType.Text

                cmd.Parameters.Clear()
                cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
                cmd.Parameters(":newDelDocNum").Value = newDelDocNum
                cmd.Parameters.Add(":ar_trn_pk", OracleType.Number)
                cmd.Parameters(":ar_trn_pk").Value = CInt(dr4("AR_TRN_PK").ToString)

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
            End If
        Next
    End Sub

    Private Sub Process_AR_Headers(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByVal custCd As String, ByRef cmd As OracleCommand)
        'SAL ar_trn records created/updated
        'TODO pmt_store may be a problem (try to find C1 pcr from Lane Wozniak for this; 
        ' also FI info remains on orig even if all finance moved, SORW too so this may be wrong in both or not

        Dim sql As New StringBuilder

        'Create the new AR_TRN record
        sql.Append("INSERT INTO AR_TRN (").Append(
                     "CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ").Append(
                     "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ").Append(
                     "IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ").Append(
                     "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM)")
        'Daniela as per Donna Sep 12 added  co_cd in ( select co_cd from store where store_cd = origin_store )
        'sql.Append("SELECT CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ").Append(
        '                "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ").Append(
        '                ":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ").Append(
        '                "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM ").Append(
        '                "FROM AR_TRN WHERE IVC_CD=:IVC_CD AND CUST_CD = :cust_cd AND co_cd = :co_cd AND TRN_TP_CD='SAL'")
        sql.Append("SELECT CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ").Append(
                       "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ").Append(
                       ":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ").Append(
                       "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM ").Append(
                       "FROM AR_TRN WHERE IVC_CD=:IVC_CD AND CUST_CD = :cust_cd AND co_cd in ( select co_cd from store where store_cd = origin_store ) AND TRN_TP_CD='SAL'")

        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":NEW_AMT", OracleType.Number)
        cmd.Parameters.Add(":IVC_CD", OracleType.VarChar)
        cmd.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)
        cmd.Parameters.Add(":cust_cd", OracleType.VarChar)
        'cmd.Parameters.Add(":co_cd", OracleType.VarChar)

        cmd.Parameters(":NEW_AMT").Value = CDbl(txt_new_total.Text)
        cmd.Parameters(":IVC_CD").Value = origDelDocNum
        cmd.Parameters(":NEW_IVC_CD").Value = newDelDocNum
        cmd.Parameters(":cust_cd").Value = custCd
        'cmd.Parameters(":co_cd").Value = Session("CO_CD")

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        sql.Clear()

        'Update the existing AR_TRN amount
        ' Daniela add co_cd in ( select co_cd from store where store_cd = origin_store )
        'sql.Append("UPDATE AR_TRN SET AMT = :original_total WHERE IVC_CD=:IVC_CD AND CUST_CD = :cust_cd AND co_cd = :co_cd AND TRN_TP_CD='SAL'")
        sql.Append("UPDATE AR_TRN SET AMT = :original_total WHERE IVC_CD=:IVC_CD AND CUST_CD = :cust_cd AND co_cd in ( select co_cd from store where store_cd = origin_store ) AND TRN_TP_CD='SAL'")
        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add(":original_total", OracleType.Number)
        cmd.Parameters(":original_total").Value = CDbl(txt_original_total.Text)
        cmd.Parameters.Add(":IVC_CD", OracleType.VarChar)
        cmd.Parameters(":IVC_CD").Value = origDelDocNum
        cmd.Parameters.Add(":cust_cd", OracleType.VarChar)
        cmd.Parameters(":cust_cd").Value = custCd
        'cmd.Parameters.Add(":co_cd", OracleType.VarChar)
        'cmd.Parameters(":co_cd").Value = Session("CO_CD")

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub

    Protected Sub btn_Clear_Click(sender As Object, e As EventArgs) Handles btn_Clear.Click

        Session("DEL_DOC_LN") = Nothing
        Session("LN_NEW") = Nothing
        Session("AR") = Nothing
        Session("AR_NEW") = Nothing
        Session("soHdrInfo") = Nothing
        lnkModifyTax.Visible = False
        lnkModifyTaxNew.Visible = False
        Response.Redirect("SalesRewrite.aspx")

    End Sub

    Protected Sub btn_orig_lookup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If lbl_cust_cd.Text & "" = "" Then

            lbl_error.Text = "Sales Order information not found"
            Exit Sub

        ElseIf cbo_PD.Text = AppConstants.PICKUP Then

            lbl_error.Text = "Sorry, push the down arrow on left to find available pickup date calendar.  Delivery dates unavailable for pickup."
            Exit Sub
        End If
        Dim hidZipcode As HiddenField = CType(UcDeliveryDatePopup.FindControl("hidZone"), HiddenField)
        If Not String.IsNullOrEmpty(txt_zone_cd.Text) Then
            hidZipcode.Value = txt_zone_cd.Text
        End If
        UcDeliveryDatePopup.GetDeliveryDates(txt_del_doc_existing.Text)
        PopupDeliveryDate.ShowOnPageLoad = True
        lbl_old_or_new.Text = "OLD"

    End Sub

    Protected Sub btn_new_lookup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_new_lookup.Click

        If lbl_cust_cd.Text & "" = "" Then

            lbl_error.Text = "Sales Order information not found"
            Exit Sub

        ElseIf cbo_PD_new.Text = AppConstants.PICKUP Then

            lbl_error.Text = "Sorry, push the down arrow on left to find available pickup date calendar.  Delivery dates unavailable for pickup."
            Exit Sub
        End If
        Dim hidZipcode As HiddenField = CType(UcDeliveryDatePopup.FindControl("hidZone"), HiddenField)
        If Not String.IsNullOrEmpty(hidZoneCode.Value) Then
            hidZipcode.Value = hidZoneCode.Value
        ElseIf Not String.IsNullOrEmpty(txt_zone_cd.Text) Then
            hidZipcode.Value = txt_zone_cd.Text
        End If
        UcDeliveryDatePopup.GetDeliveryDates(txt_del_doc_existing.Text)
        PopupDeliveryDate.ShowOnPageLoad = True
        lbl_old_or_new.Text = "NEW"


    End Sub

    Protected Sub btn_original_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_original.Click

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        Dim default_invoice As String = ""
        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim MyDataReader2 As OracleDataReader

        conn.Open()
        Dim Sql As String = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & txt_store_cd.Text & "'"
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(Sql, conn)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmd)

            Do While MyDataReader2.Read

                If MyDataReader2.Item("invoice_file").ToString & "" = "" Then

                    default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                Else
                    default_invoice = MyDataReader2.Item("invoice_file").ToString
                End If
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If String.IsNullOrEmpty(default_invoice) Then
            Sql = "select store_cd, invoice_file, default_file from invoice_admin"

            cmd = DisposablesManager.BuildOracleCommand(Sql, conn)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmd)

                If MyDataReader2.Read Then

                    default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        'resolve invoice to print
        If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/" & default_invoice & "?DEL_DOC_NUM=" & txt_del_doc_existing.Text & "','frmTest827','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
            'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('merch_images/invoice_files/" & txt_del_doc_existing.Text & ".pdf" & "','frmTest827','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & txt_del_doc_existing.Text & "&INVOICE_NAME=" & default_invoice & "','frmTest827','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        End If


    End Sub

    Protected Sub btn_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_new.Click

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        Dim default_invoice As String = ""

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader

        conn.Open()
        Dim Sql As String = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & txt_store_cd_new.Text & "'"
        cmd = DisposablesManager.BuildOracleCommand(Sql, conn)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmd)

            Do While MyDataReader2.Read

                If MyDataReader2.Item("invoice_file").ToString & "" = "" Then

                    default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                Else
                    default_invoice = MyDataReader2.Item("invoice_file").ToString
                End If
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If String.IsNullOrEmpty(default_invoice) Then

            Sql = "select store_cd, invoice_file, default_file from invoice_admin"

            cmd = DisposablesManager.BuildOracleCommand(Sql, conn)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmd)

                If MyDataReader2.Read Then

                    default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        'resolve invoice to print
        If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/" & default_invoice & "?DEL_DOC_NUM=" & txt_del_doc_new.Text & "','frmTest913','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
            'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('merch_images/invoice_files/" & txt_del_doc_new.Text & ".pdf" & "','frmTest913','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & txt_del_doc_new.Text & "&INVOICE_NAME=" & default_invoice & "','frmTest913','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        End If

    End Sub

    Public Sub Find_Related_Sales(ByVal del_doc As String)

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim ds As DataSet = New DataSet

        Dim sql As String = "SELECT DEL_DOC_NUM FROM SO WHERE SO_DOC_NUM='" & del_doc & "' AND STAT_CD='O'"
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)

        conn.Open()
        oAdp.Fill(ds)
        Dim mytable As DataTable = New DataTable
        mytable = ds.Tables(0)
        Dim numrows As Integer = mytable.Rows.Count

        If numrows > 1 Then

            With cbo_related_docs
                .DataSource = ds
                .DataValueField = "DEL_DOC_NUM"
                .DataTextField = "DEL_DOC_NUM"
                .DataBind()
            End With

            cbo_related_docs.SelectedValue = txt_del_doc_num.Text
            cbo_related_docs.Visible = True
        End If
        oAdp.Dispose()
        conn.Close()

    End Sub

    ''' <summary>
    ''' Extended so that additional attributes particular to the Sales Rewrite process
    ''' can be added.
    ''' </summary>
    ''' <remarks>Most attributes from base class are not populated, use with caution</remarks>
    Private Class SoHdrRewriteInfo
        Inherits SoHeaderDtc

        Property delChgTaxOnOrig As String
        Property delChgTaxOnNew As String
        Property setupChgTaxOnOrig As String
        Property setupChgTaxOnNew As String

    End Class

    Private Sub Populate_Results()

        Const CO_GRP_BRK As String = "BRK'"
        Const CO_GRP_LEO As String = "LEO"
        txt_del_doc_num.Enabled = True
        btn_submit.Visible = True

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim MyDataReader As OracleDataReader
        Dim SO_DOC_NUM As String = txt_del_doc_num.Text.ToString.TrimStart
        SO_DOC_NUM = SO_DOC_NUM.TrimEnd
        If Not SO_DOC_NUM.Equals(txt_del_doc_num.Text.ToString) Then
            txt_del_doc_num.Text = SO_DOC_NUM
        End If
        SO_DOC_NUM = ""

        ' MCCL DB Nov 9, 2017

        Dim userType As Double = -1
     
        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        Dim sql As String = "SELECT SO.* FROM SO, STORE STR WHERE SO.SO_STORE_CD=STR.STORE_CD AND " +
            " DEL_DOC_NUM='" & UCase(txt_del_doc_num.Text) & "' AND STAT_CD='O' and ORD_TP_CD <> 'CRM' " +
            "AND SO.SO_STORE_CD IN (SELECT S.STORE_CD " +
            "FROM STORE S, MCCL_STORE_GROUP A, STORE_GRP$STORE B WHERE SO.SO_STORE_CD = S.STORE_CD AND A.STORE_GRP_CD = B.STORE_GRP_CD " +
            "AND S.STORE_CD = B.STORE_CD AND A.USER_ID = :userType "
        ' Leons Franchise exception
        If isNotEmpty(userType) And userType = 5 Then
            sql = sql & " and '" & coCd & "' = s.co_cd "
        End If
        sql = sql & ") "

        cmd.Parameters.Add(":userType", OracleType.VarChar)
        cmd.Parameters(":userType").Value = userType
        
        'Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.CommandText = UCase(sql)
        Try
            cmd.Connection = conn
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.Read) Then
                'Add This PU_DEL_STORE_CD
                If MyDataReader.Item("ORD_TP_CD") & "" = AppConstants.Order.TYPE_CRM Then
                    lbl_error.Text = (MyDataReader.Item("ORD_TP_CD") & "") & " Sale Cannot be split."
                    Exit Sub
                Else
                    cbo_pd_store_cd.SelectedValue = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                    cbo_new_pd_store_cd.SelectedValue = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                    SO_DOC_NUM = MyDataReader.Item("SO_DOC_NUM").ToString
                    txt_del_doc_num.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                    txt_del_doc_num.Enabled = False
                    txt_del_doc_existing.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                    txt_del_doc_existing.Enabled = False
                    lnkModifyTax.Visible = True
                    If IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                        txt_tax.Text = FormatNumber(MyDataReader.Item("TAX_CHG").ToString, 2)
                    Else
                        txt_tax.Text = FormatNumber(0, 2)
                    End If

                    lnkModifyTax.Text = MyDataReader.Item("TAX_CD").ToString
                    txt_tax_cd.Text = MyDataReader.Item("TAX_CD").ToString
                    txt_tax_cd_new.Text = MyDataReader.Item("TAX_CD").ToString
                    hidOrigTaxCd.Value = MyDataReader.Item("TAX_CD").ToString
                    lbl_fname.Text = MyDataReader.Item("SHIP_TO_F_NAME").ToString
                    lbl_lname.Text = MyDataReader.Item("SHIP_TO_L_NAME").ToString
                    txt_zone_cd.Text = MyDataReader.Item("SHIP_TO_ZONE_CD").ToString
                    txt_zip_cd.Text = MyDataReader.Item("SHIP_TO_ZIP_CD").ToString
                    hidCustomerZipCode.Value = txt_zip_cd.Text
                    lbl_cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                    txt_store_cd.Text = MyDataReader.Item("SO_STORE_CD").ToString
                    cbo_PD.SelectedValue = MyDataReader.Item("PU_DEL").ToString
                    cbo_pd_date.Value = Convert.ToDateTime(MyDataReader.Item("PU_DEL_DT")).Date 'FormatDateTime(MyDataReader.Item("PU_DEL_DT").ToString, DateFormat.ShortDate)
                    txt_store_cd_new.Text = MyDataReader.Item("SO_STORE_CD").ToString
                    cbo_PD_new.SelectedValue = MyDataReader.Item("PU_DEL").ToString
                    cbo_pd_date_new.Value = Convert.ToDateTime(MyDataReader.Item("PU_DEL_DT")).Date 'FormatDateTime(MyDataReader.Item("PU_DEL_DT").ToString, DateFormat.ShortDate)
                    txt_store_cd.Enabled = False
                    txt_store_cd_new.Enabled = False

                    'needs to know if written store is ARS enabled, because if so, then the ZONE has
                    'to be carried over when saving PKP documents
                    Dim store As StoreData = theSalesBiz.GetStoreInfo(txt_store_cd.Text)
                    If (Not IsNothing(store)) AndAlso SystemUtils.isNotEmpty(store.cd) Then
                        hidWrStARS.Value = If(store.enableARS, "Y", "N")
                    End If

                    If IsNumeric(MyDataReader.Item("DEL_CHG").ToString) Then
                        txt_del_chg.Text = FormatNumber(MyDataReader.Item("DEL_CHG").ToString, 2)
                    Else
                        txt_del_chg.Text = FormatNumber(0, 2)
                    End If
                    If IsNumeric(MyDataReader.Item("SETUP_CHG").ToString) Then
                        txt_setup.Text = FormatNumber(MyDataReader.Item("SETUP_CHG").ToString, 2)
                    Else
                        txt_setup.Text = FormatNumber(0, 2)
                    End If

                    Dim theSoHdrInfo As SoHdrRewriteInfo = New SoHdrRewriteInfo()
                    theSoHdrInfo.writtenDt = MyDataReader("SO_WR_DT").ToString
                    theSoHdrInfo.delChgTaxOnOrig = 0.0    ' txt_del_chg.Text
                    theSoHdrInfo.setupChgTaxOnOrig = 0.0  ' txt_setup.Text
                    theSoHdrInfo.delChgTaxOnNew = 0.0    ' txt_del_chg.Text
                    theSoHdrInfo.setupChgTaxOnNew = 0.0  ' txt_setup.Text
                    theSoHdrInfo.taxCd = txt_tax_cd.Text
                    Session("soHdrInfo") = theSoHdrInfo

                    If IsNumeric(MyDataReader.Item("ORIG_FI_AMT").ToString) Then
                        txt_finance.Text = FormatNumber(MyDataReader.Item("ORIG_FI_AMT").ToString, 2)
                        txt_total_finance.Text = MyDataReader.Item("ORIG_FI_AMT").ToString
                        txt_finance.Enabled = True
                        txt_finance_new.Enabled = True
                        txt_finance_new.Text = "0.00"
                        If CDbl(MyDataReader.Item("ORIG_FI_AMT").ToString) = 0 Then
                            txt_finance.Enabled = False
                            txt_finance_new.Enabled = False
                        End If
                    Else
                        txt_finance.Enabled = False
                        txt_finance_new.Enabled = False
                        txt_finance.Text = "0.00"
                        txt_finance_new.Text = "0.00"
                    End If
                    Find_Related_Sales(MyDataReader.Item("SO_DOC_NUM").ToString)
                End If
            Else
                'lbl_error.Text = "Sales Order Not Found"
                lbl_error.Text = Resources.LibResources.Label819
                conn.Close()
                Exit Sub
            End If

            MyDataReader.Close()
            cmd.Dispose()

        Catch ex As Exception
            conn.Close()
            Throw
        Finally
            MyDataReader.Close()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            cmd.Dispose()
        End Try

        'Set the store code zip code
        If String.IsNullOrEmpty(hidStoreZipCode.Value) Then
            'Daniela Jan06
            'hidStoreZipCode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
            If cbo_PD.SelectedValue = "P" Then
                hidStoreZipCode.Value = SalesUtils.getStoreZipCode(cbo_new_pd_store_cd.SelectedValue.ToString(), lbl_cust_cd.Text)
            Else
                hidStoreZipCode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
            End If
        End If

        sql = "SELECT DEL_DOC_NUM FROM SO WHERE LENGTH(DEL_DOC_NUM)=12 AND SO_DOC_NUM='" & SO_DOC_NUM & "' ORDER BY DEL_DOC_NUM DESC"
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            'March 02, IT request: 2674 
            Dim lastChar As String

            If (MyDataReader.Read) Then
                'March 02, IT request: 2674 
                lastChar = Chr(Asc(Right(MyDataReader.Item("DEL_DOC_NUM").ToString, 1)))
                If lastChar = "Z" Then
                    lbl_error.Text = "Cannot split document " & SO_DOC_NUM & " more than 26 times."
                    lblZoneChangeNotification.Text = ""
                    lbl_cust_cd.Text = ""
                    btn_Clear.Visible = True
                    btn_Save.Visible = False
                    btnSOM.Visible = True
                    btnSOM.Enabled = True
                    conn.Close()
                    Exit Sub
                End If
                txt_del_doc_new.Text = SO_DOC_NUM & Chr(Asc(Right(MyDataReader.Item("DEL_DOC_NUM").ToString, 1)) + 1)

            Else
                txt_del_doc_new.Text = SO_DOC_NUM & "A"
            End If
        Catch
            conn.Close()
            Throw
        End Try

        MyDataReader.Close()
        conn.Close()
        bindgrid()

        calculate_total()
        btn_submit.Enabled = False

        If cbo_PD.SelectedValue = "D" Then cbo_pd_date.Enabled = False And btn_orig_lookup.Enabled = False
        If cbo_PD_new.SelectedValue = "D" Then cbo_pd_date_new.Enabled = False And btn_new_lookup.Enabled = False

    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim MyDataReader As OracleDataReader
        Dim origLns As DataSet = New DataSet
        Dim newLns As DataSet = New DataSet
        Dim ar_ds As DataSet = New DataSet
        Dim ar_ds_new As New DataSet
        Dim DEL_DOC_NUM As String = UCase(txt_del_doc_num.Text)
        Dim SO_Found As Boolean = True

        grd_lines.DataSource = ""
        'CustTable.Visible = False
        grd_lines.Visible = True
        grd_lines_new.Visible = True
        grd_ar.Visible = True
        grd_ar_new.Visible = True
        conn.Open()

        ' itm_cd is extracted twice, one is displayed and will be cleared if line moved; soln_itm_cd remains for processing
        Dim strBldr As New StringBuilder
        strBldr.Append("SELECT SO.DEL_DOC_NUM, SO_LN.COMM_CD, ITM.POINT_SIZE, SO_LN.PICKED, SO_LN.DEL_DOC_LN# As DEL_DOC_LN_NUM, SO_LN.ITM_CD, SO_LN.TREATED_BY_ITM_CD, ITM.VE_CD, ITM.VSN, ITM.DES, ITM.TREATABLE, ").Append(
                       "ITM.ITM_TP_CD, SO_LN.UNIT_PRC, SO_LN.QTY, SO_LN.VOID_FLAG, SO_LN.STORE_CD, SO_LN.LOC_CD, NULL AS TAX_CD, 0 AS TAXABLE_AMT, 0 AS TAX_PCT, SO_LN.UNIT_PRC AS UNIT_PRC2, ").Append(
                       "SO_LN.QTY AS orig_soln_qty, SO_LN.VOID_FLAG AS VOID_FLAG2, SO_LN.STORE_CD AS STORE_CD2, SO_LN.LOC_CD AS LOC_CD2, NULL AS PACKAGE_PARENT, SO_LN.OOC_QTY, ").Append(
                       "so_ln.ref_del_doc_ln#, so_ln.warranted_by_ln#, treats_ln#, treated_by_ln#, itm.warrantable, NULL AS new_del_doc_num, so.disc_cd as SO_DISC_CD, so_ln.itm_cd As soln_itm_cd, ").Append(
                       "so_ln.store_cd AS noDisplay_store_cd, so_ln.loc_cd AS noDisplay_loc_cd, sp.po_cd, sp.ln# AS po_ln_num, so_ln.cust_tax_chg ,SW.WARR_DEL_DOC_LN# AS WARR_BY_LN ,SW.WARR_DEL_DOC_NUM AS WARR_BY_DOC_NUM,case when ITM.ITM_TP_CD in('CPT') then 'Y' else 'N' end AllowFraction ").Append(
                       "FROM so_ln$po_ln sp, SO_LN, ITM, SO ,SO_LN2WARR SW ").Append(
                       "WHERE SO.DEL_DOC_NUM = :delDocNum AND SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM AND SO_LN.VOID_FLAG='N' AND SO.STAT_CD='O' AND SO_LN.ITM_CD = ITM.ITM_CD AND sp.del_doc_num (+) = so_ln.del_Doc_num AND sp.del_doc_ln# (+) = so_ln.del_Doc_ln# ").Append(
                       " AND SO_LN.DEL_DOC_NUM = SW.DEL_DOC_NUM(+) AND SO_LN.DEL_DOC_LN# = SW.DEL_DOC_LN#(+) order by DEL_DOC_LN_NUM ")

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(strBldr.ToString, conn)
        Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)

        cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
        cmd.Parameters(":delDocNum").Value = DEL_DOC_NUM
        oAdp.Fill(origLns)
        oAdp.Fill(newLns)
        Session("DEL_DOC_LN") = origLns

        'If ConfigurationManager.AppSettings("package_breakout") = "N" Then

        '    Package_Explosion()
        '    origLns = Session("DEL_DOC_LN")
        'End If

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.Read()) Then
                grd_lines.DataSource = origLns
                grd_lines.DataBind()

                Dim dt As DataTable = newLns.Tables(0)
                Dim dr As DataRow

                For Each dr In newLns.Tables(0).Rows
                    dr("ITM_CD") = System.DBNull.Value
                    dr("QTY") = 0
                    dr("STORE_CD") = System.DBNull.Value
                    dr("LOC_CD") = System.DBNull.Value
                    dr("UNIT_PRC") = 0.0
                Next
                Session("LN_NEW") = newLns
                grd_lines_new.DataSource = newLns
                grd_lines_new.DataBind()
            Else
                grd_lines.Visible = False
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            MyDataReader.Close()
            conn.Close()
            Throw
        End Try

        Dim sql As String = "SELECT TRN_TP_CD, ORIGIN_STORE, AMT, MOP_CD, CSH_DWR_CD, AR_TRN_PK FROM AR_TRN where IVC_CD='" & DEL_DOC_NUM & "' AND TRN_TP_CD IN('DEP','PMT') order by AR_TRN_PK"
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ar_ds)
        oAdp.Fill(ar_ds_new)
        Session("AR") = ar_ds
        grd_ar.DataSource = ar_ds
        grd_ar.DataBind()

        Dim dt3 As DataTable = ar_ds_new.Tables(0)
        Dim dr3 As DataRow

        For Each dr3 In ar_ds_new.Tables(0).Rows
            dr3("TRN_TP_CD") = System.DBNull.Value
            dr3("ORIGIN_STORE") = System.DBNull.Value
            dr3("AMT") = 0
            dr3("MOP_CD") = System.DBNull.Value
            dr3("CSH_DWR_CD") = System.DBNull.Value
        Next
        Session("AR_NEW") = ar_ds_new
        grd_ar_new.DataSource = ar_ds_new
        grd_ar_new.DataBind()

    End Sub

    Protected Sub grd_lines_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_lines_new.ItemDataBound

        If e.Item.Cells(SoLnGrid.QTY).Text = "0" Then

            e.Item.Cells(SoLnGrid.QTY).Text = ""
            e.Item.Cells(SoLnGrid.UNIT_PRC).Text = ""
            Dim Move_Image As System.Web.UI.WebControls.Image
            Move_Image = e.Item.FindControl("Image2")
            If Not Move_Image Is Nothing Then
                Move_Image.Visible = False
            End If
        End If

    End Sub

    Public Sub store_cd_populate()

        ' TODO DSA - can I/should I use StoreUtils.getEmpStores here? - does pu_del_store_cd depend on emp?
        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        '' Feb 23 Franchise MCCL commented to prevent invalid del store error
        ''Dim SQL As String = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "

        'Dim SQL As String = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store WHERE "
        'If isNotEmpty(Session("str_co_grp_cd")) Then
        '    'Feb 23 add MCCL logic Franchise
        '    Dim str_co_grp_cd As String = Session("str_co_grp_cd")
        '    If str_co_grp_cd = "BRK" Then   '  the Brick 
        '        SQL = SQL & " store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd= '" & str_co_grp_cd & "' )) "
        '    Else
        '        SQL = SQL & " store_cd in (select store_cd from store where co_cd = nvl('" & Session("CO_CD") & "', co_cd)) "
        '    End If
        'End If
        'SQL = SQL & " order by store_cd"

        Dim userType As Double = -1
        Dim SQL As String = ""

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
               "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
               "where a.user_id = :userType and " +
               "s.store_cd = b.store_cd and " +
               "a.store_grp_cd = b.store_grp_cd "
            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                SQL = SQL & " and '" & coCd & "' = s.co_cd "

            End If
        End If
        SQL = SQL & " order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            ' Parameters only if not Super User
            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType

            End If

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            With cbo_pd_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            With cbo_new_pd_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            oAdp.Dispose()

        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Sub

    Public Sub GetPuDelStore()
        ' get pickup/delivery store depending if pickup or delivery

        Dim store As StoreData = theSalesBiz.GetStoreInfo(txt_store_cd.Text)

        If (Not IsNothing(store)) AndAlso SystemUtils.isNotEmpty(store.cd) Then

            If cbo_PD_new.SelectedValue = "P" Then

                If store.puStoreCd.isEmpty Then

                    cbo_new_pd_store_cd.SelectedValue = txt_store_cd.Text

                Else
                    cbo_new_pd_store_cd.SelectedValue = store.puStoreCd 'MyDataReader.Item("PICKUP_STORE_CD").ToString
                End If

            Else
                cbo_new_pd_store_cd.SelectedValue = store.shipToStoreCd 'MyDataReader.Item("SHIP_TO_STORE_CD").ToString
            End If
        End If
    End Sub

    Public Sub determine_tax(ByVal sale As String)

        If sale = "NEW" Then
            If cbo_PD.SelectedValue = cbo_PD_new.SelectedValue Then
                lnkModifyTaxNew.Text = txt_tax_cd.Text
                txt_tax_cd_new.Text = txt_tax_cd.Text
                calculate_total()
                Exit Sub
            End If

        Else
            If cbo_PD.SelectedValue = cbo_PD_new.SelectedValue Then
                lnkModifyTax.Text = txt_tax_cd_new.Text
                txt_tax_cd.Text = txt_tax_cd_new.Text
                calculate_total()
                Exit Sub
            End If
        End If

        ' TODO - could I replace this with call to TaxUtils.get_tax_cd
        Dim SQL As String = ""
        Dim cmd As OracleCommand
        Dim MyDataReader As OracleDataReader

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim Use_Written As String = "N"
        If txt_zip_cd.Text & "" <> "" Then
            SQL = "SELECT * FROM ZIP WHERE ZIP_CD='" & txt_zip_cd.Text & "'"
            cmd = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                If MyDataReader.Read() Then
                    If Session("PD") = "P" Then
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                    Else
                        Use_Written = MyDataReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                    End If
                End If
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If Use_Written = "Y" Then
                SQL = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & txt_store_cd.Text & "'"
                cmd = DisposablesManager.BuildOracleCommand(SQL, conn)

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                    If MyDataReader.Read() Then
                        txt_zip_cd.Text = MyDataReader.Item("ZIP_CD")
                    End If
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            End If

            SQL = "SELECT ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
            SQL = SQL & "FROM TAT, TAX_CD$TAT, ZIP2TAX "
            SQL = SQL & "WHERE TAT.EFF_DT <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            SQL = SQL & "AND TAX_CD$TAT.TAX_CD = ZIP2TAX.TAX_CD "
            SQL = SQL & "AND  ZIP2TAX.ZIP_CD='" & txt_zip_cd.Text & "' "
            SQL = SQL & "GROUP BY ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD"

            Dim dv As DataView
            Dim MyTable As DataTable
            Dim numrows As Integer
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            ds = New DataSet

            cmd = DisposablesManager.BuildOracleCommand(SQL, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            ' IT sets tax code only if there is only one record setup for zip, what does it do otherwise, pop-up tax code selection?
            cmd = DisposablesManager.BuildOracleCommand(SQL, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                If (MyDataReader.Read()) And numrows = 1 Then
                    txt_tax_cd.Text = MyDataReader.Item("TAX_CD").ToString
                End If
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            calculate_total()
        End If
        conn.Close()

    End Sub

    Protected Sub grd_lines_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_lines.ItemCommand
        ' action when clicked on button on orig doc


        lnkModifyTaxNew.Text = txt_tax_cd_new.Text

        Dim Line_no As String = e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
        Dim Line_Switched = False
        Dim split As Boolean = True ' either split or moved
        Dim onPo As Boolean = False
        Dim IsWarr As Boolean = True
        Dim IsWarrLink As Boolean = False
        Dim FilterText As String
        Dim distinctWarrantyLine() As DataRow
        Dim origLns As New DataSet
        Dim loopCounter As Integer = 0
        Dim txt_qty As TextBox = Nothing

        Dim numLn_no As Integer = Line_no
        Dim strDelDocNum As String = txt_del_doc_num.Text
        Dim zstrErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim, Line_no)
        Dim strErr As String
        If zstrErr <> "Y" Then          'lucy 26-may-16
            lbl_error.Text = zstrErr
            Exit Sub
        End If
        origLns = Session("DEL_DOC_LN")
        Dim dr As DataRow
        DisableSOMButton()
        For Each dr In origLns.Tables(0).Rows
            If dr("DEL_DOC_LN_NUM") & "" = Line_no Then
                txt_qty = CType(grd_lines.Items(loopCounter).FindControl("txtQty"), TextBox)
                If Not txt_qty Is Nothing Then
                    If Not IsNumeric(txt_qty.Text) Then
                        lbl_error.Text = "WARNING: Quantity must be a valid number.Quantity reset."
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf (txt_qty.Text Mod 1 > 0) AndAlso CType(grd_lines.Items(loopCounter).FindControl("LblAllowFraction"), TextBox).Text.Equals("N") Then
                        lbl_error.Text = "WARNING: Fractional quantities are only allowed for carpet SKUs. Quantity reset."
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf (txt_qty.Text Mod 1 > 0) AndAlso CType(grd_lines.Items(loopCounter).FindControl("LblAllowFraction"), TextBox).Text.Equals("Y") AndAlso CDbl(txt_qty.Text) < 1 Then
                        lbl_error.Text = "WARNING: Fractional quantities for carpet SKUs should be greater than 1. Quantity reset."
                        txt_qty.Text = 1
                        txt_qty.Focus()
                        Exit Sub
                    ElseIf CDbl(txt_qty.Text) <= 0 Then
                        lbl_error.Text = "WARNING: The entered quantity is less than or equal to zero.Quantity reset."
                        txt_qty.Text = 1
                        Exit Sub
                    End If
                End If
            End If
            If dr("DEL_DOC_LN_NUM") = Line_no And Line_Switched = False Then
                'This code change is for ,when user is try to move only warranty SLU to the new order, without moving associated SKU then Warning message is displayed
                'After moving warranty associated SKU , warranty SKU can be moved to the new order. Related to MM-7172 (this validation already exist in E1,now implemented in POS+)
                If dr("ITM_TP_CD") = "WAR" Then
                    FilterText = "[QTY]=0 AND [Warr_BY_DOC_NUM] = '" + dr("DEL_DOC_NUM").ToString + "' AND [WARR_BY_LN]= " + dr("DEL_DOC_LN_NUM").ToString + ""
                    distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                    If (distinctWarrantyLine.Length > 0) Then
                        IsWarrLink = False
                    Else
                        IsWarrLink = theSalesBiz.CheckWarrLink(dr("DEL_DOC_NUM").ToString, IsWarr)
                    End If
                End If

                If (Not dr("ITM_TP_CD") = "WAR") OrElse (dr("ITM_TP_CD") = "WAR" AndAlso (Not IsWarrLink)) Then
                    If (CDbl(txt_qty.Text) > CDbl(dr("QTY"))) Then
                        lbl_error.Text = "WARNING: The entered quantity is greater than the available quantity"
                        Exit Sub
                    End If

                    dr("QTY") = Math.Round(CDbl(dr("QTY")) - CDbl(txt_qty.Text), 2)
                    If CDbl(dr("QTY")) = 0 Then
                        dr("ITM_CD") = System.DBNull.Value
                        dr("STORE_CD") = System.DBNull.Value
                        dr("LOC_CD") = System.DBNull.Value
                        dr("UNIT_PRC") = 0.0
                        split = False
                    End If
                End If
                If (Not dr("po_cd") Is Nothing) AndAlso dr("po_cd").ToString.isNotEmpty Then
                    onPo = True
                End If
                Line_Switched = True
            End If
            loopCounter = loopCounter + 1
        Next

        Line_Switched = False
        Dim newLns As New DataSet
        newLns = Session("LN_NEW")
        Dim dr2 As DataRow

        For Each dr2 In newLns.Tables(0).Rows

            If dr2("DEL_DOC_LN_NUM") = Line_no And Line_Switched = False Then
                'This code change is for ,when user is try to move only warranty SLU to the new order, without moving associated SKU then Warning message is displayed
                'After moving warranty associated SKU , warranty SKU can be moved to the new order. Related to MM-7172 (this validation already exist in E1,now implemented in POS+)
                If dr2("ITM_TP_CD") = "WAR" Then
                    FilterText = "[QTY]=0 AND [Warr_BY_DOC_NUM] = '" + dr2("DEL_DOC_NUM").ToString + "' AND [WARR_BY_LN]= " + dr2("DEL_DOC_LN_NUM").ToString + ""
                    distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                    If (distinctWarrantyLine.Length > 0) Then
                        IsWarrLink = False
                    Else
                        IsWarrLink = theSalesBiz.CheckWarrLink(dr2("DEL_DOC_NUM").ToString, IsWarr)
                    End If
                End If
                If (Not dr2("ITM_TP_CD") = "WAR") OrElse (dr2("ITM_TP_CD") = "WAR" AndAlso (Not IsWarrLink)) Then
                    dr2("ITM_CD") = e.Item.Cells(SoLnGrid.ITM_CD).Text
                    dr2("QTY") = CDbl(dr2("QTY")) + CDbl(txt_qty.Text)
                    dr2("STORE_CD") = e.Item.Cells(SoLnGrid.STORE_CD).Text
                    dr2("LOC_CD") = e.Item.Cells(SoLnGrid.LOC_CD).Text
                    dr2("UNIT_PRC") = CDbl(e.Item.Cells(SoLnGrid.UNIT_PRC).Text)
                    Line_Switched = True
                End If
            End If
        Next

        Session("DEL_DOC_LN") = origLns
        grd_lines.DataSource = origLns
        grd_lines.DataBind()

        Session("LN_NEW") = newLns
        grd_lines_new.DataSource = newLns
        grd_lines_new.DataBind()
        lnkModifyTaxNew.Visible = True

        calculate_total()
        If split AndAlso onPo Then
            lbl_error.Text = "WARNING: Line " + Line_no + " is attached to a purchase order -- Must move entire quantity or none."
        ElseIf IsWarrLink Then
            lbl_error.Text = "WARNING: The associated warranted SKU must be moved first."
        End If

    End Sub
    ''' <summary>
    ''' calculate the quantity which moved to new sale.
    ''' </summary>
    ''' <param name="lineNo"></param>
    ''' <returns></returns>
    Protected Function GetMovedQuantity(ByVal lineNo As String) As Double
        Dim drNew As DataRow
        Dim newLines As New DataSet
        newLines = Session("LN_NEW")
        Dim movedQuantity As Double = 0
        For Each drNew In newLines.Tables(0).Rows
            If drNew("DEL_DOC_LN_NUM") = lineNo Then
                movedQuantity = drNew("QTY")
            End If
        Next
        Return movedQuantity
    End Function
    Protected Sub grd_lines_new_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_lines_new.ItemCommand
        ' action when clicked on button on new doc

        Dim Line_no As String = e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
        Dim Line_Switched = False
        Dim newLinesCount, newArLinesCount As Integer
        Dim origLns As New DataSet
        origLns = Session("DEL_DOC_LN")
        Dim dt As DataTable = origLns.Tables(0)
        Dim dr As DataRow
        Dim IsWarr As Boolean = True
        Dim IsWarrLink As Boolean = True
        Dim FilterText As String
        Dim distinctWarrantyLine() As DataRow
        Dim txt_qty As TextBox = Nothing
        Dim moveQty As Double = 0
        Dim loopCounter As Integer = 0
        moveQty = GetMovedQuantity(Line_no)

        For Each dr In origLns.Tables(0).Rows
            If dr("DEL_DOC_LN_NUM") & "" = Line_no Then
                txt_qty = CType(grd_lines.Items(loopCounter).FindControl("txtQty"), TextBox)
                If dr("DEL_DOC_LN_NUM") = Line_no And Line_Switched = False AndAlso IsWarrLink Then

                    If Not (dr("ITM_TP_CD") = "WAR") Then
                        FilterText = "[QTY]=0 AND [Warr_BY_DOC_NUM] = '" + dr("DEL_DOC_NUM").ToString + "' AND [DEL_DOC_LN_NUM]<> " + dr("DEL_DOC_LN_NUM").ToString + " AND [ITM_TP_CD]<>'WAR'"
                        distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                        If (distinctWarrantyLine.Length > 0) Then
                            IsWarrLink = True
                        Else
                            FilterText = "[QTY]=0 AND [DEL_DOC_LN_NUM]<> " + dr("DEL_DOC_LN_NUM").ToString + " AND [ITM_TP_CD]='WAR'"
                            distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                            If (distinctWarrantyLine.Length > 0) Then
                                IsWarrLink = False
                            Else
                                IsWarrLink = True
                            End If
                        End If
                    End If
                    If (dr("ITM_TP_CD") = "WAR") OrElse (Not (dr("ITM_TP_CD") = "WAR") AndAlso IsWarrLink) Then
                        dr("ITM_CD") = e.Item.Cells(SoLnGrid.ITM_CD).Text
                        dr("QTY") = moveQty + dr("QTY")
                        txt_qty.Text = 1
                        dr("STORE_CD") = e.Item.Cells(SoLnGrid.STORE_CD).Text
                        dr("LOC_CD") = e.Item.Cells(SoLnGrid.LOC_CD).Text
                        dr("UNIT_PRC") = CDbl(e.Item.Cells(SoLnGrid.UNIT_PRC).Text)
                        Line_Switched = True
                    End If
                End If
            End If
            loopCounter = loopCounter + 1
        Next

        Line_Switched = False
        Dim newLns As New DataSet
        newLns = Session("LN_NEW")
        Dim dt2 As DataTable = newLns.Tables(0)
        Dim dr2 As DataRow
        Dim split As Boolean = True ' either split or moved
        Dim onPo As Boolean = False

        For Each dr2 In newLns.Tables(0).Rows
            If dr2("DEL_DOC_LN_NUM") = Line_no And Line_Switched = False AndAlso IsWarrLink Then
                If Not (dr2("ITM_TP_CD") = "WAR") Then
                    FilterText = "[QTY]=0 AND [Warr_BY_DOC_NUM] = '" + dr2("DEL_DOC_NUM").ToString + "' AND [DEL_DOC_LN_NUM]<> " + dr2("DEL_DOC_LN_NUM").ToString + " AND [ITM_TP_CD]<>'WAR'"
                    distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                    If (distinctWarrantyLine.Length > 0) Then
                        IsWarrLink = True
                    Else
                        FilterText = "[QTY]=0 AND [DEL_DOC_LN_NUM]<> " + dr2("DEL_DOC_LN_NUM").ToString + " AND [ITM_TP_CD]='WAR'"
                        distinctWarrantyLine = origLns.Tables(0).Select(FilterText.Trim)
                        If (distinctWarrantyLine.Length > 0) Then
                            IsWarrLink = False
                        Else
                            IsWarrLink = True
                        End If
                    End If
                End If
                If (dr2("ITM_TP_CD") = "WAR") OrElse (Not (dr2("ITM_TP_CD") = "WAR") AndAlso IsWarrLink) Then
                    dr2("QTY") = 0
                    If CDbl(dr2("QTY")) = 0 Then
                        dr2("STORE_CD") = System.DBNull.Value
                        dr2("LOC_CD") = System.DBNull.Value
                        dr2("ITM_CD") = System.DBNull.Value
                        dr2("UNIT_PRC") = 0.0
                        split = False
                    End If
                End If

                If (Not dr2("po_cd") Is Nothing) AndAlso dr2("po_cd").ToString.isNotEmpty Then
                    onPo = True
                End If
                Line_Switched = True
            End If
        Next

        Session("DEL_DOC_LN") = origLns
        grd_lines.DataSource = origLns
        grd_lines.DataBind()

        Session("LN_NEW") = newLns
        grd_lines_new.DataSource = newLns
        grd_lines_new.DataBind()

        calculate_total()
        If split AndAlso onPo Then
            lbl_error.Text = "WARNING: Line " + Line_no + " is attached to a purchase order -- Must move entire quantity or none."
        ElseIf Not IsWarrLink Then
            lbl_error.Text = "WARNING: The associated warranted SKU must be removed first."
        End If

        Dim dsNewLns As New DataSet
        Dim dsArLns As New DataSet
        dsNewLns = Session("LN_NEW")
        dsArLns = Session("AR_NEW")
        Dim dr3 As DataRow
        Dim dr4 As DataRow

        'since the new lines dataset always has lines, need to sum up the qty to truly see if there is at least one non-zero line
        For Each dr3 In dsNewLns.Tables(0).Rows
            newLinesCount += CDbl(dr3("QTY"))
        Next

        For Each dr4 In dsArLns.Tables(0).Rows
            newArLinesCount += dr4("AMT")
        Next

        If newLinesCount = 0 And newArLinesCount = 0 Then
            EnableSOMButton()
        Else
            DisableSOMButton()
        End If

        lnkModifyTaxNew.Visible = (newLinesCount > 0)

        If newLinesCount < 1 Then
            lnkModifyTaxNew.Text = String.Empty
        End If

    End Sub

    Protected Sub grd_ar_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_ar.ItemCommand

        Dim Line_no As String = e.Item.Cells(DelDtGrid.DEL_DT).Text
        Dim Move_Amt As Double = 0

        Dim Move_Text As TextBox
        Move_Text = e.Item.FindControl("txt_move_amt")

        If IsNumeric(Move_Text.Text) Then
            Move_Amt = Move_Text.Text
        End If

        If Move_Amt < 0.01 Then
            lbl_error.Text = "Amount to move must be greater than 0"
            Exit Sub
        End If

        Dim ds As New DataSet

        ds = Session("AR")
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        For Each dr In ds.Tables(0).Rows

            If dr("AR_TRN_PK") = Line_no Then

                If Move_Amt < e.Item.Cells(ArTrnGrid.AMT).Text Then

                    dr("ORIGIN_STORE") = e.Item.Cells(ArTrnGrid.ORIGIN_STORE).Text
                    dr("CSH_DWR_CD") = e.Item.Cells(ArTrnGrid.CSH_DWR_CD).Text
                    dr("AMT") = e.Item.Cells(ArTrnGrid.AMT).Text - Move_Amt
                    dr("MOP_CD") = e.Item.Cells(ArTrnGrid.MOP_CD).Text

                ElseIf Move_Amt = e.Item.Cells(ArTrnGrid.AMT).Text Then
                    dr("ORIGIN_STORE") = System.DBNull.Value
                    dr("CSH_DWR_CD") = System.DBNull.Value
                    dr("AMT") = 0
                    dr("MOP_CD") = System.DBNull.Value

                Else
                    lbl_error.Text = "Amount to move must be less than or equal to the current deposit"
                    Exit Sub
                End If
            End If
        Next

        Dim ds2 As New DataSet

        ds2 = Session("AR_NEW")
        Dim dt2 As DataTable = ds2.Tables(0)
        Dim dr2 As DataRow

        For Each dr2 In ds2.Tables(0).Rows

            If (Move_Amt > (CDbl(txt_new_total.Text)) - (CDbl(dr2("AMT")))) Then

                lbl_error.Text = "Total deposit exceeds total save on new delivery document -- Please Try Again"
                Exit Sub

            Else
                If dr2("AR_TRN_PK") = Line_no Then
                    dr2("ORIGIN_STORE") = e.Item.Cells(ArTrnGrid.ORIGIN_STORE).Text
                    dr2("CSH_DWR_CD") = e.Item.Cells(ArTrnGrid.CSH_DWR_CD).Text
                    dr2("AMT") = Move_Amt + dr2("AMT")
                    dr2("MOP_CD") = e.Item.Cells(ArTrnGrid.MOP_CD).Text
                End If
            End If
        Next

        Session("AR") = ds
        grd_ar.DataSource = ds
        grd_ar.DataBind()

        Session("AR_NEW") = ds2
        grd_ar_new.DataSource = ds2
        grd_ar_new.DataBind()

    End Sub

    Protected Sub grd_ar_new_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_ar_new.ItemCommand

        Dim Line_no As String = e.Item.Cells(DelDtGrid.DEL_DT).Text
        Dim TotalNumberOfRowIngrd_lines_new, TotalNumberOfRowIngrd_ar_new As Integer
        Dim ds As New DataSet

        ds = Session("AR")
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow
        Dim cur_amt As Double

        For Each dr In ds.Tables(0).Rows

            If dr("AR_TRN_PK") = Line_no Then

                dr("ORIGIN_STORE") = e.Item.Cells(ArTrnGrid.ORIGIN_STORE).Text
                dr("CSH_DWR_CD") = e.Item.Cells(ArTrnGrid.CSH_DWR_CD).Text
                If IsNumeric(dr("AMT")) Then
                    cur_amt = dr("AMT")
                Else
                    cur_amt = 0
                End If
                dr("AMT") = cur_amt + If(String.IsNullOrEmpty(e.Item.Cells(ArTrnGrid.AMT).Text), 0, CDbl(e.Item.Cells(ArTrnGrid.AMT).Text))
                dr("MOP_CD") = e.Item.Cells(ArTrnGrid.MOP_CD).Text
            End If
        Next

        Dim ds2 As New DataSet

        ds2 = Session("AR_NEW")
        Dim dt2 As DataTable = ds2.Tables(0)
        Dim dr2 As DataRow

        For Each dr2 In ds2.Tables(0).Rows

            If dr2("AR_TRN_PK") = Line_no Then

                dr2("ORIGIN_STORE") = System.DBNull.Value
                dr2("CSH_DWR_CD") = System.DBNull.Value
                dr2("AMT") = 0
                dr2("MOP_CD") = System.DBNull.Value
            End If
        Next

        Session("AR") = ds
        grd_ar.DataSource = ds
        grd_ar.DataBind()

        Session("AR_NEW") = ds2
        grd_ar_new.DataSource = ds2
        grd_ar_new.DataBind()

        Dim newLnscount As New DataSet
        Dim arlnscount As New DataSet
        newLnscount = Session("LN_NEW")
        arlnscount = Session("AR_NEW")
        Dim dt3 As DataTable = newLnscount.Tables(0)
        Dim dt4 As DataTable = arlnscount.Tables(0)
        Dim dr3 As DataRow
        Dim dr4 As DataRow

        For Each dr3 In newLnscount.Tables(0).Rows
            If CDbl(dr3("QTY")) = 0 Then
                TotalNumberOfRowIngrd_lines_new = CDbl(dr3("QTY"))
            Else
                TotalNumberOfRowIngrd_lines_new = CDbl(dr3("QTY"))
            End If
        Next


        For Each dr4 In arlnscount.Tables(0).Rows
            If dr4("AMT") = 0 Then
                TotalNumberOfRowIngrd_ar_new = dr4("AMT")
            Else
                TotalNumberOfRowIngrd_ar_new = dr4("AMT")
            End If
        Next

        If TotalNumberOfRowIngrd_lines_new = 0 And TotalNumberOfRowIngrd_ar_new = 0 Then
            EnableSOMButton()
        Else
            DisableSOMButton()
        End If

    End Sub

    Protected Sub grd_lines_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_lines.ItemDataBound

        If e.Item.Cells(SoLnGrid.QTY).Text = "0" Then

            Dim Move_Image As System.Web.UI.WebControls.Image
            Move_Image = e.Item.FindControl("Image1")
            Dim txt_qty As TextBox = e.Item.FindControl("txtQty")
            If Not Move_Image Is Nothing Then
                Move_Image.Visible = False
            End If
            If Not txt_qty Is Nothing Then
                txt_qty.Visible = False
            End If
            e.Item.Cells(SoLnGrid.QTY).Text = ""
            e.Item.Cells(SoLnGrid.UNIT_PRC).Text = ""
        End If

    End Sub

    Protected Sub grd_ar_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_ar.ItemDataBound

        If IsNumeric(e.Item.Cells(ArTrnGrid.AMT).Text) Then

            If CDbl(e.Item.Cells(ArTrnGrid.AMT).Text) = 0 Then

                e.Item.Cells(ArTrnGrid.AMT).Text = ""
                Dim Move_Image As System.Web.UI.WebControls.Image
                Move_Image = e.Item.FindControl("Image3")
                If Not Move_Image Is Nothing Then
                    Move_Image.ImageUrl = "images/shim.gif"
                End If
            End If
        End If

    End Sub

    Protected Sub grd_ar_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_ar_new.ItemDataBound

        If IsNumeric(e.Item.Cells(ArTrnGrid.AMT).Text) Then

            If CDbl(e.Item.Cells(ArTrnGrid.AMT).Text) = 0 Then

                e.Item.Cells(ArTrnGrid.AMT).Text = ""
                Dim Move_Image As System.Web.UI.WebControls.Image
                Move_Image = e.Item.FindControl("Image4")
                If Not Move_Image Is Nothing Then
                    Move_Image.ImageUrl = "images/shim.gif"
                End If
            End If
        End If

    End Sub

    Private Sub ResetFormControlValues(ByVal parent As Control)

        For Each c As Control In parent.Controls
            If c.Controls.Count > 0 Then
                ResetFormControlValues(c)
            Else
                Select Case (c.GetType().ToString())
                    Case "System.Web.UI.WebControls.TextBox"
                        CType(c, TextBox).Enabled = False
                End Select
            End If
        Next c

    End Sub

    Protected Sub txt_finance_new_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ' TODO if this value is set to non-numeric and button save clicked, then the values are set back, presumably the error is displayed but the save process doesn't fail and the numbers are no longer invalid
        '   changes could be lost and unknown because the process didn't stop and allow correction
        If Not IsNumeric(txt_finance_new.Text) Then
            lbl_error.Text = "Finance amount must be numeric"
            txt_finance_new.Text = "0.00"
            txt_finance.Text = FormatNumber(txt_total_finance.Text, 2)
            txt_finance_new.Focus()

        Else
            If CDbl(txt_finance_new.Text) > CDbl(txt_total_finance.Text) Then

                lbl_error.Text = "Finance amount cannot be more than original finance amount"
                txt_finance_new.Text = "0.00"
                txt_finance.Text = FormatNumber(txt_total_finance.Text, 2)
                txt_finance_new.Focus()

            Else
                txt_finance.Text = FormatNumber(txt_total_finance.Text - txt_finance_new.Text, 2)
                txt_finance_new.Text = FormatNumber(txt_finance_new.Text, 2)
            End If
        End If

    End Sub

    Protected Sub txt_finance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_finance.TextChanged

        If Not IsNumeric(txt_finance.Text) Then
            lbl_error.Text = "Old Finance amount must be numeric"
            txt_finance_new.Text = "0.00"
            txt_finance.Text = FormatNumber(txt_total_finance.Text, 2)
            txt_finance.Focus()

        Else
            If CDbl(txt_finance.Text) > CDbl(txt_total_finance.Text) Then

                lbl_error.Text = "Old Finance amount cannot be more than original finance amount"
                txt_finance_new.Text = "0.00"
                txt_finance.Text = FormatNumber(txt_total_finance.Text, 2)
                txt_finance.Focus()

            Else
                txt_finance_new.Text = FormatNumber(txt_total_finance.Text - txt_finance.Text, 2)
                txt_finance.Text = FormatNumber(txt_finance.Text, 2)
            End If
        End If

    End Sub

    Private Class MerchAndTaxTotals
        ' data from summation of lines

        Property tax_total As Double
        Property merch_total As Double
    End Class

    Private Function calculate_ln_tax_and_tot(ByVal tax_cd As String, ByRef lnTab As DataTable) As MerchAndTaxTotals
        ' dataTable in is assumed to be of SO_LN type and must include the following fields with these exact column names:
        '   ITM_TP_CD, QTY, UNIT_PRC, VOID_FLAG
        ' line taxing reworked to avoid invalid taxing by tax auths on specific itm_tp_cds

        Dim tots As New MerchAndTaxTotals
        tots.tax_total = 0
        tots.merch_total = 0

        If (Not IsNothing(lnTab)) AndAlso lnTab.Rows.Count > 0 Then

            Dim lnRow As DataRow
            Dim lab_tot As Double = 0
            Dim fab_tot As Double = 0
            Dim nlt_tot As Double = 0
            Dim war_tot As Double = 0
            Dim par_tot As Double = 0
            Dim mil_tot As Double = 0
            Dim inv_tot As Double = 0
            Dim tmp_amt As Double = 0

            For Each lnRow In lnTab.Rows

                If lnRow("VOID_FLAG").ToString = "N" AndAlso IsNumeric(lnRow("QTY")) AndAlso IsNumeric(lnRow("UNIT_PRC")) Then

                    tmp_amt = (CDbl(lnRow("QTY")) * CDbl(lnRow("UNIT_PRC")))

                    tots.merch_total = tots.merch_total + tmp_amt

                    Select Case lnRow("ITM_TP_CD").ToString
                        Case "PKG"
                            ' nothing
                        Case "NLN"
                            ' nothing
                        Case "LAB"
                            lab_tot = lab_tot + tmp_amt
                        Case "FAB"
                            fab_tot = fab_tot + tmp_amt
                        Case "NLT"
                            nlt_tot = nlt_tot + tmp_amt
                        Case "WAR"
                            war_tot = war_tot + tmp_amt
                        Case "PAR"
                            par_tot = par_tot + tmp_amt
                        Case "MIL"
                            mil_tot = mil_tot + tmp_amt
                        Case "DNR"
                            ' nothing
                        Case Else ' inventory types -  CPT, INV, APP, ACC
                            inv_tot = inv_tot + tmp_amt
                    End Select
                End If
            Next

            Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")
            tots.tax_total = calc_hdr_tax(tax_cd, inv_tot, "INV", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, lab_tot, "LAB", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, fab_tot, "FAB", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, nlt_tot, "NLT", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, war_tot, "WAR", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, par_tot, "PAR", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + calc_hdr_tax(tax_cd, mil_tot, "MIL", theSoHdrInfo.writtenDt)

        End If

        Return tots
    End Function

    Private Sub calculate_total()

        '
        'Calculate orig order totals
        '
        'March 02 prevent error
        If IsNothing(Session("DEL_DOC_LN")) Then
            Exit Sub
        End If

        Dim ds As New DataSet
        ds = Session("DEL_DOC_LN")

        'Calculate Tax on Merchandise and Line Items
        Dim tots As MerchAndTaxTotals = calculate_ln_tax_and_tot(txt_tax_cd.Text, ds.Tables(0))

        Dim setup_chg As Double = 0
        Dim del_chg As Double = 0
        Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")

        If IsNumeric(txt_del_chg.Text.Trim) Then

            del_chg = CDbl(txt_del_chg.Text.Trim)
            ' calculate tax on delivery charge, if valid value and applicable
            theSoHdrInfo.delChgTaxOnOrig = calc_hdr_tax(txt_tax_cd.Text, del_chg, "DEL", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + theSoHdrInfo.delChgTaxOnOrig
        End If

        If IsNumeric(txt_setup.Text.Trim) Then

            setup_chg = CDbl(txt_setup.Text.Trim)
            ' calculate tax on setup charge, if valid value and applicable
            theSoHdrInfo.setupChgTaxOnOrig = calc_hdr_tax(txt_tax_cd.Text, setup_chg, "SET", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + theSoHdrInfo.setupChgTaxOnOrig
        End If

        txt_tax.Text = FormatNumber(Math.Round(tots.tax_total, 2, MidpointRounding.AwayFromZero))
        txt_original_total.Text = FormatNumber(tots.merch_total + setup_chg + del_chg + tots.tax_total, 2)  ' TODO should the merch_total be rounded before sum here?  

        '
        'Calculate new order totals
        '
        setup_chg = 0
        del_chg = 0

        Dim ds2 As DataSet = Session("LN_NEW")

        'Calculate Tax on Merchandise and Line Items
        tots = calculate_ln_tax_and_tot(txt_tax_cd_new.Text, ds2.Tables(0))

        If IsNumeric(txt_del_chg_new.Text.Trim) Then

            del_chg = CDbl(txt_del_chg_new.Text.Trim)
            ' calculate tax on delivery charge, if valid value and applicable
            ' TODO - how do the taxes get handled if change pu/del and it changes the tax code - how does SORW split this?
            '   I don't believe there is any change of money on SORW but I'm not sure.
            theSoHdrInfo.delChgTaxOnNew = calc_hdr_tax(txt_tax_cd.Text, del_chg, "DEL", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + theSoHdrInfo.delChgTaxOnNew
        End If

        If IsNumeric(txt_setup_new.Text.Trim) Then

            setup_chg = CDbl(txt_setup_new.Text.Trim)
            ' calculate tax on setup charge, if valid value and applicable
            theSoHdrInfo.setupChgTaxOnNew = calc_hdr_tax(txt_tax_cd.Text, setup_chg, "SET", theSoHdrInfo.writtenDt)
            tots.tax_total = tots.tax_total + theSoHdrInfo.setupChgTaxOnNew
        End If

        txt_tax_new.Text = FormatNumber(Math.Round(CDbl(tots.tax_total), 2, MidpointRounding.AwayFromZero))
        txt_new_total.Text = FormatNumber(tots.merch_total + setup_chg + del_chg + tots.tax_total, 2)

    End Sub

    Protected Sub cbo_PD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_PD.SelectedIndexChanged

        If cbo_PD.SelectedValue = "P" Then

            cbo_pd_date.Enabled = True
            btn_orig_lookup.Enabled = False

        Else
            cbo_pd_date.Enabled = False
            btn_orig_lookup.Enabled = True
        End If
        determine_tax("OLD")

    End Sub

    Protected Sub cbo_PD_new_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_PD_new.SelectedIndexChanged

        Dim hidZipcode As HiddenField = CType(UcZonePopup.FindControl("hidZip"), HiddenField)
        Dim txtzip As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(UcZonePopup.FindControl("txtzip"), DevExpress.Web.ASPxEditors.ASPxTextBox)

        DisableSOMButton()
        If cbo_PD_new.SelectedValue = "P" Then
            cbo_pd_date_new.Enabled = True
            btn_new_lookup.Enabled = False
            'If Sale is pick up type then store code zip should get selected
            hidZipcode.Value = hidStoreZipCode.Value
        Else
            cbo_pd_date_new.Enabled = False
            btn_new_lookup.Enabled = True
            'If Sale is delivery type then customer code zip should get selected
            hidZipcode.Value = hidCustomerZipCode.Value
        End If

        'When sale type is changed then Customer/Store zip code should get selected by default
        txtzip.Text = hidZipcode.Value

        GetPuDelStore()
        determine_tax("NEW")

        Dim rowsCount As Integer = UcZonePopup.UpdateZonesDisplay(txtzip.Text)
        'Update zone code for new zip code
        UcZonePopup.UpdateZoneInfo(rowsCount, sender)
        If rowsCount >= 1 Then
            'Empty zip/zone notification when ever store type changes
            lblZoneChangeNotification.Text = String.Empty
            'If multiple zones found then zone pop up should display first
            PopupDeliveryDate.ShowOnPageLoad = False
            PopupZone.ShowOnPageLoad = True
        Else
            If cbo_PD_new.SelectedValue = "D" Then
                PopupDeliveryDate.ShowOnPageLoad = True
            End If
        End If

    End Sub

    Private Sub Update_AR_TRN(ByVal ivc_cd As String, ByVal qty As String, ByRef cmd As OracleCommand)

        Dim sql As String = "UPDATE AR_TRN SET AMT=:NEW_AMT WHERE AR_TRN_PK=:AR_TRN_PK"
        cmd.CommandText = sql

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":NEW_AMT", OracleType.Number)
        cmd.Parameters.Add(":AR_TRN_PK", OracleType.Number)

        cmd.Parameters(":AR_TRN_PK").Value = CDbl(ivc_cd)
        cmd.Parameters(":NEW_AMT").Value = CDbl(qty)

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub

    Private Sub Insert_New_AR_TRN(ByVal ivc_cd As String, ByVal qty As String, ByRef cmd As OracleCommand)

        Dim sql As New StringBuilder
        sql.Append("INSERT INTO AR_TRN (").Append(
                        "CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ").Append(
                        "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ").Append(
                        "IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ").Append(
                        "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM)")

        sql.Append("SELECT CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ").Append(
                        "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ").Append(
                        ":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ").Append(
                        "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM ").Append(
                        "FROM AR_TRN WHERE AR_TRN_PK=:AR_TRN_PK")
        cmd.CommandText = sql.ToString
        cmd.Parameters.Clear()
        cmd.Parameters.Add(":NEW_AMT", OracleType.Number)
        cmd.Parameters.Add(":AR_TRN_PK", OracleType.VarChar)
        cmd.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)

        cmd.Parameters(":NEW_AMT").Value = CDbl(qty)
        cmd.Parameters(":AR_TRN_PK").Value = CDbl(ivc_cd)
        cmd.Parameters(":NEW_IVC_CD").Value = txt_del_doc_new.Text.Trim
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub

    Public Function Check_AR_TRN(ByVal ar_trn_pk As String)

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)

        conn.Open()
        Dim sql As String = "SELECT AMT FROM AR_TRN WHERE AR_TRN_PK=" & ar_trn_pk
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)

        If MyDataReader.Read Then

            If IsNumeric(MyDataReader.Item("AMT").ToString) Then

                Return MyDataReader.Item("AMT").ToString
            Else
                Return 0
            End If
        Else
            Return 0
        End If

        MyDataReader.Close()
        conn.Close()
    End Function

    Protected Sub txt_del_chg_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If IsNumeric(txt_del_chg.Text.ToString) Then

            txt_del_chg.Text = FormatNumber(txt_del_chg.Text.ToString, 2)
            calculate_total()

        ElseIf txt_del_chg.Text.ToString.isEmpty Then

            txt_del_chg.Text = FormatNumber(0, 2)
            calculate_total()

        Else
            lbl_error.Text = "Old Pickup/Delivery charge must be numeric"
            txt_del_chg.Focus()
        End If
    End Sub

    Protected Sub txt_setup_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If IsNumeric(txt_setup.Text.ToString) Then

            txt_setup.Text = FormatNumber(txt_setup.Text.ToString, 2)
            calculate_total()

        ElseIf txt_setup.Text.ToString.isEmpty Then

            txt_setup.Text = FormatNumber(0, 2)
            calculate_total()
        Else
            lbl_error.Text = "Old Setup charge must be numeric"
            txt_setup.Focus()
        End If

    End Sub

    Protected Sub txt_del_chg_new_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        DisableSOMButton()

        If IsNumeric(txt_del_chg_new.Text.ToString) Then

            txt_del_chg_new.Text = FormatNumber(txt_del_chg_new.Text.ToString, 2)
            calculate_total()

        ElseIf txt_del_chg_new.Text.ToString.isEmpty Then

            txt_del_chg_new.Text = FormatNumber(0, 2)
            calculate_total()
        Else
            lbl_error.Text = "Pickup/Delivery charge must be numeric"
            txt_del_chg_new.Focus()
        End If

    End Sub

    Protected Sub txt_setup_new_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        DisableSOMButton()

        If IsNumeric(txt_setup_new.Text.ToString) Then

            txt_setup_new.Text = FormatNumber(txt_setup_new.Text.ToString, 2)
            calculate_total()

        ElseIf txt_setup_new.Text.ToString.isEmpty Then

            txt_setup_new.Text = FormatNumber(0, 2)
            calculate_total()
        Else
            lbl_error.Text = "Setup charge must be numeric"
            txt_setup_new.Focus()
        End If

    End Sub

    Protected Sub cbo_related_docs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_related_docs.SelectedIndexChanged

        If cbo_related_docs.SelectedValue <> txt_del_doc_num.Text.ToString Then

            Response.Redirect("SalesRewrite.aspx?RELATED_SKU=" & cbo_related_docs.SelectedValue.ToString)
        End If

    End Sub


    ' TODO - SORW and SOM both have this routine - need to make generic and move to util; not doing now - this might be in branch and need to be ported - that new pkg stuff they did in branch
    Public Sub Package_Explosion()

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim retprc As Double
        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim objSql8 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim MyDataReader8 As OracleDataReader
        Dim MyDataReader10 As OracleDataReader
        Dim qty As Double = 1
        Dim PKG_GO As Boolean
        Dim itm_cd As String = ""
        Dim package_retail As Double = 0
        Dim row_id As Double = 0
        PKG_GO = False
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection.Open()

        conn.Open()

        sql = "SELECT SO_LN.ITM_CD, SO_LN.DEL_DOC_LN#, SO_LN.UNIT_PRC from itm, so_ln where DEL_DOC_NUM='" & txt_del_doc_num.Text & "' AND ITM.ITM_CD=SO_LN.ITM_CD AND ITM.ITM_TP_CD='PKG' AND SO_LN.UNIT_PRC > 0 ORDER BY DEL_DOC_LN#"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader10 = DisposablesManager.BuildOracleDataReader(objSql)

        Do While MyDataReader10.Read()
            itm_cd = MyDataReader10.Item("ITM_CD").ToString
            package_retail = MyDataReader10.Item("UNIT_PRC").ToString
            row_id = MyDataReader10.Item("DEL_DOC_LN#").ToString

            'If we are to populate the retail amounts, we need to calculate based on cost percentages
            sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd='" & itm_cd & "' "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST,ROW_ID) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & "," & row_id & ")"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()

            'Insert new retail price
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & package_retail & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

            sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd='" & itm_cd & "' and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Calculate the total cost
            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST").ToString & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD").ToString & "' AND ROW_ID=" & row_id
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            Dim PERCENT_OF_TOTAL As Double = 0
            Dim NEW_RETAIL As Double = 0
            Dim TOTAL_NEW_RETAIL As Double = 0
            Dim TOTAL_RETAIL As Double = 0
            Dim LAST_SKU As String = ""
            Dim LAST_PKG_SKU As String = ""

            If PKG_GO = True Then
                'Calculate the percentage based on cost/total cost
                sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id & " ORDER BY PKG_SKU"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Dim pkg_qty As Double
                Do While MyDataReader2.Read()
                    sql = "SELECT NVL(SUM(QTY),0) AS SUM_QTY FROM PACKAGE WHERE PKG_ITM_CD='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND CMPNT_ITM_CD='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read Then
                        If IsNumeric(MyDataReader.Item("SUM_QTY").ToString) Then
                            pkg_qty = CDbl(MyDataReader.Item("SUM_QTY").ToString)
                        Else
                            pkg_qty = 0
                        End If
                    Else
                        pkg_qty = 0
                    End If
                    If IsNumeric(MyDataReader2.Item("COST").ToString) And IsNumeric(MyDataReader2.Item("TOTAL_COST").ToString) Then
                        PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                    Else
                        PERCENT_OF_TOTAL = CDbl(0)
                    End If
                    If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                    NEW_RETAIL = Math.Round(PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString), 2, MidpointRounding.AwayFromZero)
                    TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                    TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                    LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                    thistransaction = objConnection.BeginTransaction
                    With cmdInsertItems
                        .Transaction = thistransaction
                        .Connection = objConnection
                        .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL / pkg_qty, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND ROW_ID=" & row_id
                    End With
                    cmdInsertItems.ExecuteNonQuery()
                    thistransaction.Commit()
                Loop

                MyDataReader2.Close()
                objSql2.Dispose()
            End If

            Dim ds2 As DataSet
            ds2 = Session("DEL_DOC_LN")
            Dim FINAL_RETAIL_TOTAL As Double = 0

            If Not IsNothing(Session("DEL_DOC_LN")) Then

                sql = "SELECT NEW_RETAIL, COMPONENT_SKU, ROW_ID FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & itm_cd & "'"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read()
                    retprc = MyDataReader2.Item("NEW_RETAIL")
                    If Not IsNumeric(retprc) Then retprc = 0

                    Dim dt As DataTable = ds2.Tables(0)
                    Dim dr As DataRow

                    For Each dr In ds2.Tables(0).Rows
                        If dr.RowState = DataRowState.Unchanged Then
                            If IsNumeric(dr("DEL_DOC_LN_NUM")) Then
                                If dr("DEL_DOC_LN_NUM") > row_id Then
                                    If dr("UNIT_PRC") = 0 Then
                                        If MyDataReader2.Item("COMPONENT_SKU").ToString = dr("ITM_CD").ToString Then
                                            If TOTAL_RETAIL <> TOTAL_NEW_RETAIL And CDbl(dr("QTY")) = 1 And retprc > (TOTAL_NEW_RETAIL - TOTAL_RETAIL) Then
                                                retprc = retprc - (TOTAL_NEW_RETAIL - TOTAL_RETAIL)
                                                TOTAL_NEW_RETAIL = TOTAL_RETAIL
                                            End If

                                            dr("TAXABLE_AMT") = retprc / CDbl(dr("QTY").ToString)
                                            dr("UNIT_PRC") = retprc / CDbl(dr("QTY").ToString)
                                            If retprc / CDbl(dr("QTY").ToString) > 0 Then
                                                FINAL_RETAIL_TOTAL = FINAL_RETAIL_TOTAL + retprc / CDbl(dr("QTY").ToString)
                                                LAST_SKU = dr("ITM_CD").ToString
                                            End If
                                            dr("PACKAGE_PARENT") = row_id
                                        End If
                                    ElseIf dr("ITM_TP_CD").ToString = "FAB" Or dr("ITM_TP_CD").ToString = "WAR" Then
                                        'Do nothing, let the process continue running
                                    Else
                                        Exit For
                                    End If
                                ElseIf dr("DEL_DOC_LN_NUM") = row_id Then
                                    dr("TAXABLE_AMT") = 0
                                    dr("UNIT_PRC") = 0
                                End If
                            End If
                        End If
                    Next
                Loop
                If TOTAL_RETAIL <> FINAL_RETAIL_TOTAL Then
                    For Each dr In ds2.Tables(0).Rows
                        If LAST_SKU = dr("ITM_CD").ToString And CDbl(dr("QTY").ToString) = 1 Then
                            dr("TAXABLE_AMT") = CDbl(dr("TAXABLE_AMT").ToString) - (FINAL_RETAIL_TOTAL - TOTAL_RETAIL)
                            dr("UNIT_PRC") = CDbl(dr("UNIT_PRC").ToString) - (FINAL_RETAIL_TOTAL - TOTAL_RETAIL)
                            Exit For
                        End If
                    Next
                End If
            End If

            MyDataReader2.Close()
            objSql2.Dispose()

            Session("DEL_DOC_LN") = ds2

            'We're done, delete out all of the records to avoid duplication
            thistransaction = objConnection.BeginTransaction
            sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

        Loop

        conn.Close()
        objConnection.Close()

    End Sub


    'perhaps call TaxUtils.ComputeHdrTax
    Private Shared Function calc_hdr_tax(ByVal tax_cd As String, ByVal amt As Double, ByVal itm_tp_cd As String, Optional ByVal tax_dt_str As String = "") As Double
        'Determine tax on input amount; if itm_tp_cd is found as special handling, then use to find taxability 

        Dim tax As Double = 0

        If amt > 0 AndAlso (Not String.IsNullOrWhiteSpace(tax_cd)) Then  ' this may have to change if we ever do Normal Selling Price logic

            Dim conn As OracleConnection = GetConn(Connection_Constants.CONN_ERP)
            Dim strBldr As New StringBuilder
            Dim cmd As OracleCommand
            Dim datRdr As OracleDataReader
            Dim tax_pct As Double = 0
            Dim tax_dt As Date = FormatDateTime(Today.Date, DateFormat.ShortDate)
            If IsDate(tax_dt_str) Then
                tax_dt = FormatDateTime(tax_dt_str, DateFormat.ShortDate)
            End If

            strBldr.Append("SELECT SUM(tat.pct) AS SumOfPCT ")
            strBldr.Append("FROM TAT, TAX_CD$TAT ")
            strBldr.Append("WHERE TAT.EFF_DT <= :tax_dt And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= :tax_dt ")
            strBldr.Append("AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
            strBldr.Append("AND TAX_CD$TAT.TAX_CD= :tax_cd ")

            Dim itm_tp_col As String = ""
            Select Case itm_tp_cd
                Case "LAB"
                    itm_tp_col = "LAB_TAX"
                Case "FAB"
                    itm_tp_col = "FAB_TAX"
                Case "NLT"
                    itm_tp_col = "NLT_TAX"
                Case "WAR"
                    itm_tp_col = "WAR_TAX"
                Case "PAR"
                    itm_tp_col = "PARTS_TAX"
                Case "MIL"
                    itm_tp_col = "MILEAGE_TAX"
                Case "DEL"
                    itm_tp_col = "DEL_TAX"
                Case "SET"
                    itm_tp_col = "SU_TAX"
            End Select

            If Not String.IsNullOrWhiteSpace(itm_tp_col) Then

                strBldr.Append("AND TAT.").Append(itm_tp_col).Append(" = 'Y' ")
            End If

            cmd = DisposablesManager.BuildOracleCommand(strBldr.ToString, conn)
            cmd.Parameters.Add(":tax_cd", OracleType.VarChar)
            cmd.Parameters(":tax_cd").Value = tax_cd
            cmd.Parameters.Add(":tax_dt", OracleType.DateTime)
            cmd.Parameters(":tax_dt").Value = tax_dt

            Try
                conn.Open()
                datRdr = DisposablesManager.BuildOracleDataReader(cmd)

                If datRdr.Read() Then
                    If IsNumeric(datRdr.Item("SumOfPCT").ToString) Then
                        tax_pct = CDbl(datRdr.Item("SumOfPCT").ToString) / 100
                    Else
                        tax_pct = 0
                    End If
                    tax = (amt * tax_pct)
                End If
                datRdr.Close()

            Finally
                conn.Close()
            End Try
        End If

        Return tax
    End Function
#Region " ----> Logic related to SPLIT/MOVE of SO_LN, needed by the btn_Save_Click"

    Private Sub Process_So_Lines(ByVal origDelDocNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        'Update Sales Order Lines

        Dim origLns As DataSet = Session("DEL_DOC_LN")
        Dim origLn As DataRow
        Dim orig_qty As Double = 0
        Dim new_qty_on_origLn As Double = 0
        Dim qty_on_newLn As Double = 0
        Dim sql As String
        Dim soLnInp As soLnUpdt
        Dim transPointTots As New TransportationPoints ' sum of all lines
        Dim zoneCode As String = If(hidZoneCode.Value.Trim.Equals(String.Empty), txt_zone_cd.Text.Trim, hidZoneCode.Value.Trim)
        Dim origTranSched As New TransportationSchedule
        origTranSched.zoneCd = txt_zone_cd.Text.ToString
        origTranSched.puDelCd = cbo_PD.SelectedValue
        origTranSched.puDelDt = FormatDateTime(cbo_pd_date.Text, DateFormat.ShortDate)
        Dim currTranSched As New TransportationSchedule
        currTranSched.zoneCd = zoneCode.ToString
        currTranSched.puDelCd = cbo_PD_new.SelectedValue
        currTranSched.puDelDt = FormatDateTime(cbo_pd_date_new.Text, DateFormat.ShortDate)

        For Each origLn In origLns.Tables(0).Rows

            sql = ""
            origLn.Item("new_del_doc_num") = txt_del_doc_new.Text.Trim
            orig_qty = CDbl(origLn("orig_soln_qty").ToString)
            new_qty_on_origLn = CDbl(origLn("QTY"))

            ' if line moving or splitting
            If orig_qty <> new_qty_on_origLn Then

                qty_on_newLn = orig_qty - new_qty_on_origLn

                If (new_qty_on_origLn > 0) AndAlso (orig_qty > new_qty_on_origLn) Then
                    '------> if here means it is a LINE SPLIT

                    SplitSoLineAndRelated(origLn, new_qty_on_origLn, qty_on_newLn, origLns.Tables(0), cmd)
                    If origLn.Item("ITM_TP_CD") = AppConstants.Sku.TP_INV Then
                        CalcSoLnPoints(origLn, orig_qty, new_qty_on_origLn, qty_on_newLn, origTranSched, currTranSched, transPointTots)
                    End If

                ElseIf new_qty_on_origLn = 0 Then
                    '------> if here means it is a LINE MOVE 

                    MoveSoLineAndRelated(origLn, origLns.Tables(0), cmd)
                    If origLn.Item("ITM_TP_CD") = AppConstants.Sku.TP_INV Then
                        CalcSoLnPoints(origLn, orig_qty, new_qty_on_origLn, qty_on_newLn, origTranSched, currTranSched, transPointTots)
                    End If

                End If

            Else
                'this line didn't move or split but need to check if attached line moved and may need to process attached line pointers from this non-changing line    
                Dim lnLinks As LineLinks = FindLinkedLines(origLn, origLns.Tables(0), linkTypes.treatAndWarr)
                updtLinkLnNum(origLn, lnLinks, sql, False)

                'if nothing setup in sql statement to update, then NO update
                If sql.Length > 0 Then

                    soLnInp = Create_SoLnUpdt(origLn("DEL_DOC_NUM").ToString, origLn("DEL_DOC_LN_NUM").ToString, sql)
                    Update_SO_LN(soLnInp, cmd)
                End If
            End If
        Next
        ProcessTransportationUpdt(transPointTots, origTranSched, currTranSched, cmd)
    End Sub

    Private Sub Insert_New_SO_LN(ByVal del_doc_num As String, ByVal line_no As String,
                             ByVal qty As Double, ByVal ooc_qty As Double, ByVal trts_ln_num As String, ByVal trted_by_ln_num As String,
                             ByVal cust_tax_chg As String, ByRef cmd As OracleCommand)

        Dim sql As New StringBuilder
        sql.Append("INSERT INTO SO_LN (").Append(
                    "DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, STORE_CD, LOC_CD, REF_DEL_DOC_LN#, REF_ITM_CD,").Append(
                    "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, SO_DOC_NUM, ID_NUM, SER_NUM, ").Append(
                    "UNIT_PRC, QTY, SPIFF, PICKED, OUT_ENTRY_DT, FIFL_DT, FIFO_CST, FIFO_DT, FILL_DT, VOID_FLAG, ").Append(
                    "COD_AMT, OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, WARRANTED_BY_LN#, ").Append(
                    "WARRANTED_BY_ITM_CD, TREATS_LN#, TREATS_ITM_CD, TREATED_BY_LN#, TREATED_BY_ITM_CD, DISC_AMT, ").Append(
                    "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, ").Append(
                    "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, ").Append(
                    "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT, ").Append(
                    "COMPLAINT_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, ").Append(
                    "COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, ").Append(
                    "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, TAX_CD, TAX_BASIS, CUST_TAX_CHG, ").Append(
                    "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT, ALT_DOC_LN#) ")

        sql.Append("SELECT :NEW_DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, STORE_CD, LOC_CD, REF_DEL_DOC_LN#, REF_ITM_CD,").Append(
                    "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, SO_DOC_NUM, ID_NUM, SER_NUM, ").Append(
                    "UNIT_PRC, :NEW_QTY, SPIFF, PICKED, OUT_ENTRY_DT, FIFL_DT, FIFO_CST, FIFO_DT, FILL_DT, VOID_FLAG, ").Append(
                    "COD_AMT, :NEW_OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, WARRANTED_BY_LN#, ").Append(
                    "WARRANTED_BY_ITM_CD, :trts_ln_num, TREATS_ITM_CD, :trted_by_ln_num, TREATED_BY_ITM_CD, DISC_AMT, ").Append(
                    "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, ").Append(
                    "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, ").Append(
                    "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT, ").Append(
                    "COMPLAINT_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, ").Append(
                    "COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, ").Append(
                    "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, :TAX_CD, TAX_BASIS, :CUST_TAX_CHG, ").Append(
                    "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT, ALT_DOC_LN# ").Append(
                    "FROM SO_LN WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND DEL_DOC_LN#=:DEL_DOC_LN#")
        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters(":NEW_DEL_DOC_NUM").Value = txt_del_doc_new.Text.Trim
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters(":DEL_DOC_NUM").Value = del_doc_num
        cmd.Parameters.Add(":NEW_QTY", OracleType.Number)
        cmd.Parameters(":NEW_QTY").Value = qty
        cmd.Parameters.Add(":NEW_OOC_QTY", OracleType.Number)
        cmd.Parameters(":NEW_OOC_QTY").Value = ooc_qty
        cmd.Parameters.Add(":DEL_DOC_LN#", OracleType.Number)
        cmd.Parameters(":DEL_DOC_LN#").Value = CInt(line_no)
        cmd.Parameters.Add(":trts_ln_num", OracleType.Number)
        If isEmpty(trts_ln_num) Then
            cmd.Parameters(":trts_ln_num").Value = System.DBNull.Value
        Else
            cmd.Parameters(":trts_ln_num").Value = CInt(trts_ln_num)
        End If
        cmd.Parameters.Add(":trted_by_ln_num", OracleType.Number)
        If isEmpty(trted_by_ln_num) Then
            cmd.Parameters(":trted_by_ln_num").Value = System.DBNull.Value
        Else
            cmd.Parameters(":trted_by_ln_num").Value = CInt(trted_by_ln_num)
        End If
        cmd.Parameters.Add(":CUST_TAX_CHG", OracleType.Number)
        If TaxUtils.isHdrTax("") OrElse isEmpty(cust_tax_chg) OrElse Not IsNumeric(cust_tax_chg) Then
            cmd.Parameters(":CUST_TAX_CHG").Value = System.DBNull.Value
        Else
            cmd.Parameters(":CUST_TAX_CHG").Value = CDbl(cust_tax_chg)
        End If
        'tax_line
        cmd.Parameters.Add(":TAX_CD", OracleType.VarChar)
        cmd.Parameters(":TAX_CD").Value = If(System.Configuration.ConfigurationManager.AppSettings("tax_line").ToString() = "N", System.DBNull.Value, txt_tax_cd_new.Text.Trim().ToUpper())

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub

    Private Enum processType
        ' indicates what process the line is undergoing

        moving = 1
        splitting = 2
        notMoveOrSplit = 4 ' updating line links but otherwise line not touched
    End Enum

    Private Class soLnUpdt
        ' data for SO_LN update process; value set depending on what process the line is undergoing for this update

        Property procTp As processType
        Property orig_del_doc_num As String
        Property line_num As Double
        Property new_del_doc_num As String
        Property new_qty_on_orig As Double
        Property new_ooc_on_orig As Double
        Property new_cust_tax_chg_on_orig As String
        Property setString As String
    End Class

    Private Function Create_SoLnUpdt(ByVal orig_del_doc_num As String, ByVal new_del_Doc_num As String, ByVal line_num As String,
                         ByVal setstring As String) As soLnUpdt
        ' overload to create input to moving line logic

        Dim soLnI As New soLnUpdt

        soLnI.procTp = processType.moving

        soLnI.orig_del_doc_num = orig_del_doc_num
        soLnI.new_del_doc_num = new_del_Doc_num
        soLnI.line_num = CDbl(line_num)
        soLnI.setString = setstring

        Return soLnI
    End Function

    Private Function Create_SoLnUpdt(ByVal orig_del_doc_num As String, ByVal line_num As String,
                     ByVal setstring As String) As soLnUpdt
        ' overload to create input to update but not moving line logic

        Dim soLnI As New soLnUpdt

        soLnI.procTp = processType.notMoveOrSplit

        soLnI.orig_del_doc_num = orig_del_doc_num
        soLnI.new_del_doc_num = ""
        soLnI.line_num = CDbl(line_num)
        soLnI.setString = setstring

        Return soLnI
    End Function

    Private Function Create_SoLnUpdt(ByVal orig_del_doc_num As String, ByVal new_del_Doc_num As String, ByVal line_num As String,
                     ByVal setstring As String, ByVal qty As Double, ByVal ooc_qty As Double) As soLnUpdt
        ' overload to create input to splitting line logic

        Dim soLnI As New soLnUpdt

        soLnI.procTp = processType.splitting

        soLnI.orig_del_doc_num = orig_del_doc_num
        soLnI.new_del_doc_num = new_del_Doc_num
        soLnI.line_num = CDbl(line_num)
        soLnI.new_qty_on_orig = qty
        soLnI.new_ooc_on_orig = ooc_qty
        soLnI.setString = setstring

        Return soLnI
    End Function

    Private Sub Update_SO_LN(ByVal soLnInfo As soLnUpdt, ByRef cmd As OracleCommand)
        ' param set_String string of updates for the SO_LN built dynamically depending on the changes needed for the line

        If Not String.IsNullOrWhiteSpace(soLnInfo.setString) Then

            Dim sql As New StringBuilder
            sql.Append("UPDATE SO_LN SET ").Append(soLnInfo.setString).Append(" WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND DEL_DOC_LN#=:DEL_DOC_LN#")
            cmd.CommandText = sql.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = soLnInfo.orig_del_doc_num
            cmd.Parameters.Add(":DEL_DOC_LN#", OracleType.Number)
            cmd.Parameters(":DEL_DOC_LN#").Value = soLnInfo.line_num

            If soLnInfo.procTp = processType.splitting Then

                cmd.Parameters.Add(":NEW_QTY", OracleType.Number)
                cmd.Parameters(":NEW_QTY").Value = soLnInfo.new_qty_on_orig
                cmd.Parameters.Add(":NEW_OOC_QTY", OracleType.Number)
                cmd.Parameters(":NEW_OOC_QTY").Value = soLnInfo.new_ooc_on_orig
                cmd.Parameters.Add(":CUST_TAX_CHG", OracleType.Number)
                If TaxUtils.isHdrTax("") OrElse isEmpty(soLnInfo.new_cust_tax_chg_on_orig) OrElse Not IsNumeric(soLnInfo.new_cust_tax_chg_on_orig) Then
                    cmd.Parameters(":CUST_TAX_CHG").Value = System.DBNull.Value
                Else
                    cmd.Parameters(":CUST_TAX_CHG").Value = CDbl(soLnInfo.new_cust_tax_chg_on_orig)
                End If

            ElseIf soLnInfo.procTp = processType.moving Then

                cmd.Parameters.Add(":new_del_doc_num", OracleType.VarChar)
                cmd.Parameters(":new_del_doc_num").Value = soLnInfo.new_del_doc_num
            End If
            ' else neither splitting or moving but may be updated anyway

            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

    End Sub

    Private Class LineLinks

        'Property warrdByLn As Double
        'Property warrsLn As Double
        Property treatdByLn As Double
        Property treatsLn As Double
        Property attachedRows() As Array
    End Class

    Private Sub updtLinkLnNum(ByRef datRowSoLn As DataRow, ByRef lnLinks As LineLinks, ByRef setStmt As String, ByVal currLnMoving As Boolean)
        ' if related linking (warranty and/or treatment) and one of the linked lines is moving without the other, then need to remove line number pointers, if applicable
        ' for removal, add the corresponding line clearing, depending if warranty/treatment or warrantable/treatable line
        ' param setStmt is the 'SET' statement portion of the SO_LN update SQL statement for updating the moved/split line, it will appended to appropriately
        '    here for warranty/treatment link update as necessary
        ' parameter movingLn TRUE indicates the current line is moving; FALSE indicates the current line is not moving but if it is the linked line (warrantable/treatable or warranty/treatment), 
        '    may need to have the link line number updated on this line anyway

        ' TODO currently treatment is only 1 to 1 (single entry = N) in POS+, if change, might have to split

        ' determine if any linked rows and if so, determine whether they are moving also or not
        ' if there are NO attached rows, nothing to do - return false no removal of links necessary
        ' 24-JAN-14 - DSA - warranty logic removed - no longer retained in so_ln, now in SO_LN2WARR
        If (lnLinks.attachedRows IsNot Nothing) AndAlso
            UBound(lnLinks.attachedRows) > -1 Then

            For Each attachedlnRow In lnLinks.attachedRows

                ' if there are linked warranty/treatment and they will be on a different doc when SORW+ complete, then remove line number links, not itm_cd links
                'setAttachedLine(lnLinks.warrdByLn, attachedlnRow, setStmt, " warranted_by_ln# = NULL ", currLnMoving)    '    if warrantable line, remove pointer to warranty line
                setAttachedLine(lnLinks.treatdByLn, attachedlnRow, setStmt, " treated_by_ln# = NULL ", currLnMoving)     '    if treatable line, remove pointer to treatment line
                'setAttachedLine(lnLinks.warrsLn, attachedlnRow, setStmt, " ref_del_doc_ln# = NULL ", currLnMoving)       '    if warranty line, remove pointer to warrantable line
                setAttachedLine(lnLinks.treatsLn, attachedlnRow, setStmt, " treats_ln# = NULL ", currLnMoving)           '    if treatment line, remove pointer to treatable line

            Next
        End If
    End Sub

    Private Enum linkTypes
        treatment = 1
        'warranty = 2
        treatAndWarr = 4  ' both
    End Enum

    Private Function FindLinkedLines(ByRef datRowSoLn As DataRow, ByRef datTabSoLn As DataTable, ByVal linkTp As linkTypes) As LineLinks
        ' find related linked lines on this document, warranty and/or treatment, warrantable and/or treatable

        Dim lnLinks As New LineLinks
        Dim strBld As New StringBuilder

        'find the related linked lines
        ' being a warranty or treatment (fab) type are mutually exclusive from one another and mutually exclusive from warrantable and treatable (hope? - is a treatment warrantied)
        '   on the other hand, a piece of inventory can be both treatable and warrantable
        'If (linkTp = linkTypes.warranty OrElse linkTp = linkTypes.treatAndWarr) AndAlso datRowSoLn("ITM_TP_CD").ToString = "WAR" AndAlso (datRowSoLn("REF_DEL_DOC_LN#").ToString).isNotEmpty Then

        '    lnLinks.warrsLn = CDbl(datRowSoLn("REF_DEL_DOC_LN#").ToString)
        '    strBld.Append("'").Append(lnLinks.warrsLn).Append("'")

        'Else
        If (linkTp = linkTypes.treatment OrElse linkTp = linkTypes.treatAndWarr) AndAlso is_linked_treatment_lnNum(datRowSoLn) Then

            lnLinks.treatsLn = CDbl(datRowSoLn("TREATS_LN#").ToString)
            strBld.Append("'").Append(lnLinks.treatsLn).Append("'")

        Else
            'If (linkTp = linkTypes.warranty OrElse linkTp = linkTypes.treatAndWarr) AndAlso datRowSoLn("WARRANTABLE").ToString = "Y" AndAlso (datRowSoLn("WARRANTED_BY_LN#").ToString).isNotEmpty Then

            '    lnLinks.warrdByLn = CDbl(datRowSoLn("WARRANTED_BY_LN#").ToString)
            '    strBld.Append("'").Append(lnLinks.warrdByLn).Append("'")
            'End If
            '  this process handles a one-to-one treatment (syspm single entry = No)
            If (linkTp = linkTypes.treatment OrElse linkTp = linkTypes.treatAndWarr) AndAlso is_linked_treatable_lnNum(datRowSoLn) Then

                lnLinks.treatdByLn = CDbl(datRowSoLn("TREATED_BY_LN#").ToString)
                strBld.Append(IIf(strBld.Length > 0, ", '", "'")).Append(lnLinks.treatdByLn).Append("'")
            End If
        End If

        If strBld.Length > 0 Then
            lnLinks.attachedRows = datTabSoLn.Select("DEL_DOC_LN_NUM IN (" + strBld.ToString + ")")
        End If

        Return lnLinks
    End Function

    Private Function is_linked_treatment_lnNum(ByRef datRowSoLn As DataRow) As Boolean
        ' returns TRUE if the line passed in is a treatment SKU (FAB) and it has a treatable line number, FALSE otherwise

        If datRowSoLn("ITM_TP_CD").ToString = "FAB" AndAlso (datRowSoLn("TREATS_LN#").ToString).isNotEmpty Then

            Return True

        Else
            Return False
        End If
    End Function

    Private Function is_linked_treatable_lnNum(ByRef datRowSoLn As DataRow) As Boolean
        ' returns TRUE if the line passed in is a treatable SKU and it has a treatment line number, FALSE otherwise

        If datRowSoLn("TREATABLE").ToString = "Y" AndAlso (datRowSoLn("TREATED_BY_LN#").ToString).isNotEmpty Then

            Return True

        Else
            Return False
        End If
    End Function

    Private Sub setAttachedLine(ByVal attachedlineNum As Double, ByVal attachedLnRow As DataRow, ByRef setStmt As String, ByVal setStr As String, ByVal currLnMoving As Boolean)
        ' if the line number passed in (lineNum) is the line number of the row passed in and the row is not moving (qty > 0) but the line is moving (moving) then
        '     append the string to clear the appropriate field
        ' OR if the line number passed in (lineNum) is the line number of the row passed in and the row is moving (qty < .001) but the line is not moving (moving) then
        '     append the string to clear the appropriate field
        ' QTY is the new quantity on the original row so if 0, then the line is moved, if not 0, then the line is not moved (maybe split) (warr info lines are not ever split)

        If attachedlineNum > 0 AndAlso CDbl(attachedLnRow("DEL_DOC_LN_NUM")) = attachedlineNum Then

            ' if current row moving but attached line is not moving or split (orig qty - new qty on orig = qty on split (or moved) = 0)
            ' if current row NOT moving but attached line is moving (new qty on orig = 0)
            If (currLnMoving AndAlso (CDbl(attachedLnRow("orig_soln_qty")) - CDbl(attachedLnRow("QTY")) < 0.001)) OrElse
                 ((Not currLnMoving) AndAlso CDbl(attachedLnRow("QTY")) < 0.001) Then

                setStmt = IIf(setStmt.Length() > 0, setStmt + ",", setStmt) + setStr
            End If
        End If
    End Sub

    Private Function GetNextIstLnNum(ByVal storeCd As String, ByVal seqNum As String, ByVal wrDt As Date) As Double
        'Returns the next IST_LN LN# available for a specific IST

        Dim returnVal As Double = 0

        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)

        Dim sqlstmt As String = "SELECT MAX(ln#)+1 nextIstLnNum FROM ist_ln WHERE ist_wr_dt = :wrDt AND ist_store_cd = :storeCd AND ist_seq_num = :seqNum"
        Dim cmd = DisposablesManager.BuildOracleCommand(sqlstmt, conn)
        cmd.CommandType = CommandType.Text

        Try
            conn.Open()

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":storeCd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add(":seqNum", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add(":wrDt", OracleType.DateTime).Direction = ParameterDirection.Input

            cmd.Parameters(":storeCd").Value = storeCd
            cmd.Parameters(":seqNum").Value = seqNum
            cmd.Parameters(":wrDt").Value = wrDt
            cmd.ExecuteNonQuery()

            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.HasRows) Then
                MyDataReader.Read()
                returnVal = MyDataReader.Item("nextIstLnNum")
            End If

        Catch ex As Exception
            Throw New Exception("Error in getNextIstLnNum ", ex)

        Finally
            MyDataReader.Close()
            conn.Close()
        End Try

        Return returnVal

    End Function

    Private Function setTrtLineNum(ByRef datRowSoLn As DataRow, ByRef attachedRows As Array, ByVal attachedLnNum As String) As String
        ' determine the setting of the linked treatment/treatable line number on the new (to be inserted) line
        '   if the original line was linked and there will be a new linked line on the new document, then the linked line number remains (returns input)
        '   if the original line was not linked, or will not have a new linked line on the new document, then the linked line number output is empty

        If (attachedRows IsNot Nothing) AndAlso
            UBound(attachedRows) > -1 AndAlso
            attachedRows(0)("DEL_DOC_LN_NUM").ToString = attachedLnNum AndAlso
            CDbl(attachedRows(0)("orig_soln_qty").ToString) - CDbl(attachedRows(0)("QTY")) > 0 Then ' qty on new linked line > 0 - NOT current line new qty

            Return attachedLnNum

        Else
            Return ""
        End If
    End Function

    Private Sub SplitSoLineAndRelated(ByRef datRowSoLn As DataRow, ByVal newQtyOnOrigLn As Double, ByVal qtyOnNewLn As Double, ByRef datTabSoLn As DataTable, ByRef cmd As OracleCommand)
        ' creates new line related objects and updates original related objects for split SO_LN's

        Dim orig_ooc_qty As Double = 0
        Dim new_ooc_qty As Double = 0
        Dim origDelDocNum As String = datRowSoLn("DEL_DOC_NUM").ToString
        Dim newDelDocNum As String = datRowSoLn("new_del_doc_num").ToString
        Dim delDoclnNum As String = datRowSoLn("DEL_DOC_LN_NUM").ToString
        Dim treatsLnNum As String = ""
        Dim treatedByLnNum As String = ""

        If (IsDBNull(datRowSoLn("OOC_QTY")) Or IsNothing(datRowSoLn("OOC_QTY"))) Then
            orig_ooc_qty = 0
        Else
            orig_ooc_qty = CDbl(datRowSoLn("OOC_QTY"))
        End If
        new_ooc_qty = IIf(orig_ooc_qty > newQtyOnOrigLn, (orig_ooc_qty - newQtyOnOrigLn), 0)

        Dim lnLinks As LineLinks = FindLinkedLines(datRowSoLn, datTabSoLn, linkTypes.treatment)   ' warranty links are 1 to 1 so never split therefore at most one row

        ' The SO_LN INSERT must be FIRST - must CREATE the SO_LN itself so Primary Key exists for other processing
        ' determine if any attached treatment or treatable line is splitting or moving to remove line number pointers on this new line
        If is_linked_treatment_lnNum(datRowSoLn) Then

            treatsLnNum = setTrtLineNum(datRowSoLn, lnLinks.attachedRows, datRowSoLn("TREATS_LN#").ToString)

        ElseIf is_linked_treatable_lnNum(datRowSoLn) Then

            treatedByLnNum = setTrtLineNum(datRowSoLn, lnLinks.attachedRows, datRowSoLn("TREATED_BY_LN#").ToString)
        End If
        Dim cust_tax_Str As String = datRowSoLn("cust_tax_chg").ToString + ""
        Dim new_cust_tax_chg As Double = 0.0
        Dim new_cust_tax_chg_on_orig As Double = 0.0
        If (Not IsNothing(cust_tax_Str)) AndAlso IsNumeric(cust_tax_Str) AndAlso CDbl(cust_tax_Str) > 0.0 Then
            new_cust_tax_chg = Math.Round((CDbl(cust_tax_Str) / (newQtyOnOrigLn + qtyOnNewLn)) * qtyOnNewLn, TaxUtils.Tax_Constants.taxPrecision, MidpointRounding.AwayFromZero)
            new_cust_tax_chg_on_orig = Math.Round((CDbl(cust_tax_Str) - new_cust_tax_chg), TaxUtils.Tax_Constants.taxPrecision, MidpointRounding.AwayFromZero)
        End If

        Insert_New_SO_LN(origDelDocNum, delDoclnNum, qtyOnNewLn, new_ooc_qty, treatsLnNum, treatedByLnNum, new_cust_tax_chg, cmd)

        '  NOW, update the quantity, and anything else necessary, on the original line
        '   using sqlSet variable as set string
        Dim sqlSet = " QTY=:NEW_QTY, OOC_QTY=:NEW_OOC_QTY, cust_tax_chg = :cust_tax_chg "
        updtLinkLnNum(datRowSoLn, lnLinks, sqlSet, False)
        Dim soLnInp As soLnUpdt = Create_SoLnUpdt(origDelDocNum, newDelDocNum, delDoclnNum, sqlSet, newQtyOnOrigLn, (orig_ooc_qty - new_ooc_qty))
        soLnInp.new_cust_tax_chg_on_orig = new_cust_tax_chg_on_orig
        Update_SO_LN(soLnInp, cmd)

        ' TODO future - this should replace the comm_paid and so_ln2disc but not done now
        'Dim splitLnReq As New SplitLineRequest
        'splitLnReq.newDelDocNum = newDelDocNum
        'splitLnReq.origDelDocNum = origDelDocNum
        'splitLnReq.origLnNum = delDoclnNum
        'splitLnReq.newLnNum = delDoclnNum
        'splitLnReq.soDiscCd = datRowSoLn("so_disc_cd").ToString.Trim
        'SalesUtils.SplitLnRelatedTbls(splitLnReq, cmd)  ' split the comm_paid and so_ln2disc

        ' for comm_paid, the last entry is the one that is valid
        Dim sqlstmt As New StringBuilder
        sqlstmt.Append("INSERT INTO comm_paid ( DEL_DOC_NUM, DEL_DOC_LN, so_emp_slsp_cd1, refund_flag1, pct_of_sale1, so_emp_slsp_cd2, ").Append(
                  " refund_flag2, pct_of_sale2, dt_time)").Append(
                  " SELECT :newDelDocNum, :newlnNum, so_emp_slsp_cd1, refund_flag1, pct_of_sale1, so_emp_slsp_cd2, ").Append(
                  " refund_flag2, pct_of_sale2, SYSDATE ").Append(
                  " FROM comm_paid cp1 WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN = :origlnNum AND dt_time = ").Append(
                  "(SELECT MAX(cp2.dt_time) FROM comm_paid cp2 WHERE cp1.del_doc_num = cp2.del_doc_num ").Append(
                  "AND cp1.del_doc_ln = cp2.del_doc_ln)")
        cmd.CommandText = sqlstmt.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origlnNum", OracleType.Number)
        cmd.Parameters.Add(":newlnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters(":origlnNum").Value = CInt(delDoclnNum)
        cmd.Parameters(":newlnNum").Value = CInt(delDoclnNum)

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        sqlstmt.Clear()

        ' warranties are still qty = 1 so can be moved, not split therefore this is always only applicable to warrantables
        sqlstmt.Append("INSERT INTO so_ln2warr ( del_doc_num, del_doc_ln#, warr_del_doc_num, warr_del_doc_ln# ) ").Append(
                  " SELECT :newDelDocNum, :newlnNum, warr_del_doc_num, warr_del_doc_ln# ").Append(
                  " FROM so_ln2warr WHERE del_doc_num = :origDelDocNum AND del_doc_ln# = :origlnNum ")
        cmd.CommandText = sqlstmt.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origlnNum", OracleType.Number)
        cmd.Parameters.Add(":newlnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters(":origlnNum").Value = CInt(delDoclnNum)
        cmd.Parameters(":newlnNum").Value = CInt(delDoclnNum)
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        sqlstmt.Clear()
        If (Not String.IsNullOrEmpty(datRowSoLn("so_disc_cd").ToString)) AndAlso SysPms.multiDiscCd = datRowSoLn("so_disc_cd").ToString Then

            sqlstmt.Clear()
            sqlstmt.Append("INSERT INTO SO_LN2DISC ( DEL_DOC_NUM, DEL_DOC_LN#, DISC_CD, DISC_AMT, DISC_REASON_CD, DISC_CMNT, DISC_PCT, SEQ_NUM ) ").Append(
                      " SELECT :newDelDocNum, :newlnNum, DISC_CD, (DISC_AMT / (:qtyOnNew + :newQtyOnOrig)) * :qtyOnNew, ").Append(
                      " DISC_REASON_CD, DISC_CMNT, DISC_PCT, SEQ_NUM ").Append(
                      " FROM SO_LN2DISC WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN# = :origlnNum ")
            cmd.CommandText = sqlstmt.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)
            cmd.Parameters.Add(":newlnNum", OracleType.Number)
            cmd.Parameters.Add(":qtyOnNew", OracleType.Number)
            cmd.Parameters.Add(":newQtyOnOrig", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = CInt(delDoclnNum)
            cmd.Parameters(":newlnNum").Value = CInt(delDoclnNum)
            cmd.Parameters(":qtyOnNew").Value = qtyOnNewLn
            cmd.Parameters(":newQtyOnOrig").Value = newQtyOnOrigLn
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

            sqlstmt.Clear()
            sqlstmt.Append("UPDATE SO_LN2DISC SET DISC_AMT = ((DISC_AMT / (:qtyOnNew + :newQtyOnOrig)) * :newQtyOnOrig) ").Append(
                      "WHERE DEL_DOC_NUM = :origDelDocNum AND   DEL_DOC_LN# = :origlnNum ")
            cmd.CommandText = sqlstmt.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)
            cmd.Parameters.Add(":qtyOnNew", OracleType.Number)
            cmd.Parameters.Add(":newQtyOnOrig", OracleType.Number)

            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = CInt(delDoclnNum)
            cmd.Parameters(":qtyOnNew").Value = qtyOnNewLn
            cmd.Parameters(":newQtyOnOrig").Value = newQtyOnOrigLn
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

        ' line has inventory filled then update inventory
        If (Not String.IsNullOrWhiteSpace(datRowSoLn("noDisplay_store_cd").ToString)) AndAlso
            (Not String.IsNullOrWhiteSpace(datRowSoLn("noDisplay_loc_cd").ToString)) Then

            Dim ist As CustOwnedIst = SplitRelatedIstLine(origDelDocNum, delDoclnNum, newQtyOnOrigLn, newDelDocNum, delDoclnNum, qtyOnNewLn, cmd)
            SplitRelatedInv(origDelDocNum, delDoclnNum, newQtyOnOrigLn, newDelDocNum, delDoclnNum, qtyOnNewLn, datRowSoLn("soln_itm_cd"), ist, cmd)
        End If
    End Sub

    Private Sub VoidRfPick(ByVal docNum As String, ByVal lnNum As String, ByRef cmd As OracleCommand)
        ' voids the RF pick 

        'TODO - should be checking security RFVPICK - SORW CHECK_SEC_RFVPICK (maybe a syspm too) before removing RF_PICK (if not, process fails)

        If SysPms.isRFEnabled Then

            Dim sqlstmt As New StringBuilder
            sqlstmt.Append("UPDATE RF_PICK P SET P.VOID_FLAG = 'Y' ").Append(
                                    "WHERE P.VOID_FLAG IS NULL  AND  P.DOC_NUM = :docNum  AND  P.LN# = :lnNum ")
            cmd.CommandText = sqlstmt.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":docNum", OracleType.VarChar)
            cmd.Parameters.Add(":lnNum", OracleType.Number)

            cmd.Parameters(":docNum").Value = docNum
            cmd.Parameters(":lnNum").Value = CInt(lnNum)
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

    End Sub

    Private Function GetIstLnForSoLn(ByVal origDelDocNum As String, ByVal origlnNum As String) As DataTable
        ' Extract and return the information from an open, non-void IST_LN, if link exists, for an SO_LN

        Dim datSet As New DataSet
        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)

        Dim sqlstmt As New StringBuilder
        sqlstmt.Append(" SELECT ISL.IST_WR_DT, ISL.IST_STORE_CD, ISL.IST_SEQ_NUM, ISL.IST_LN#, ITM_CD, OLD_STORE_CD, OLD_LOC_CD, QTY, ").Append(
                      "    CRPT_SER_NUM, CRPT_ID_NUM, FIFO_CST, FIFO_DT, FIFL_DT, UNIT_CST, ID_CD, IL.STAT_CD, NEW_STORE_CD, ").Append(
                      "    NEW_LOC_CD, OUT_COMM_CD, OUT_SPIFF,  OOC_QTY, DEST_FLAG, FRAN_MRKUP_AMT, SER_NUM, ist.DOC_NUM  ").Append(
                      " FROM IST_LN IL, IST, IST_LN$SO_LN ISL ").Append(
                      " WHERE ISL.IST_WR_DT = IST.IST_WR_DT AND ISL.IST_SEQ_NUM = IST.IST_SEQ_NUM AND ISL.IST_WR_DT = IST.IST_WR_DT ").Append(
                      " AND IL.IST_WR_DT = IST.IST_WR_DT AND IL.IST_SEQ_NUM = IST.IST_SEQ_NUM AND IL.IST_WR_DT = IST.IST_WR_DT ").Append(
                      " AND IL.IST_WR_DT = ISL.IST_WR_DT AND IL.IST_SEQ_NUM = ISL.IST_SEQ_NUM AND IL.IST_WR_DT = ISL.IST_WR_DT ").Append(
                      " AND IL.LN# = ISL.IST_LN# AND IST.FINAL_DT IS NULL AND il.qty > 0 AND NVL(il.void_flag, 'N') != 'Y' ").Append(
                      " AND ISL.DEL_DOC_NUM = :origDelDocNum AND ISL.DEL_DOC_LN# = :origlnNum ")
        Try
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlstmt.ToString, conn)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)

            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = CInt(origlnNum)

            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return datSet.Tables(0)

    End Function

    Private Class CustOwnedIst

        Property storeCd As String = ""
        Property seqNum As String = ""
        Property wrDt As Date
        Property origLnNum As Double = 0
        Property newLnNum As Double = 0
    End Class

    Private Function Create_CustOwnedIst(ByVal ist_store_cd As String, ByVal ist_seq_num As String, ByVal ist_wr_dt As Date,
                 ByVal orig_ln_num As Double, ByVal new_ln_num As Double) As CustOwnedIst
        ' Ist information for splitting line

        Dim ist As New CustOwnedIst

        ist.storeCd = ist_store_cd
        ist.seqNum = ist_seq_num
        ist.wrDt = ist_wr_dt
        ist.origLnNum = orig_ln_num
        ist.newLnNum = new_ln_num

        Return ist
    End Function

    Private Function SplitRelatedIstLine(ByVal origDelDocNum As String, ByVal origSolnNum As String, ByVal newQtyOnOrig As Double,
                                   ByVal newDelDocNum As String, ByVal newSolnNum As String,
                                   ByVal qtyOnNew As Double, ByRef cmd As OracleCommand) As CustOwnedIst
        ' creates new line related IST objects and updates original related IST objects for split SO_LN's
        Dim ist As CustOwnedIst

        Dim istLns As DataTable = GetIstLnForSoLn(origDelDocNum, origSolnNum)
        If (Not IsNothing(istLns)) AndAlso istLns.Rows.Count > 0 Then

            Dim sqlstmt As New StringBuilder
            Dim istLn As DataRow = istLns.Rows(0)
            Dim istStoreCd As String = istLn("IST_STORE_CD")
            Dim istSeqNum As String = istLn("IST_SEQ_NUM")
            Dim istWrDt As Date = istLn("IST_WR_DT")
            Dim istLnNum As Double = istLn("IST_LN#")
            Dim istDocNum As String = istLn("DOC_NUM")

            VoidRfPick(istDocNum, istLnNum, cmd)

            sqlstmt.Append("UPDATE IST_LN  SET QTY = :newQtyOnOrig ").Append(
                      "WHERE IST_WR_DT = :istWrDt AND IST_STORE_CD = :istStoreCd AND IST_SEQ_NUM = :istSeqNum  AND LN# = :istLnNum ")
            cmd.CommandText = sqlstmt.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":newQtyOnOrig", OracleType.Number)
            cmd.Parameters.Add(":istWrDt", OracleType.DateTime)
            cmd.Parameters.Add(":istStoreCd", OracleType.VarChar)
            cmd.Parameters.Add(":istSeqNum", OracleType.VarChar)
            cmd.Parameters.Add(":istLnNum", OracleType.Number)

            cmd.Parameters(":newQtyOnOrig").Value = newQtyOnOrig
            cmd.Parameters(":istWrDt").Value = istWrDt
            cmd.Parameters(":istStoreCd").Value = istStoreCd
            cmd.Parameters(":istSeqNum").Value = istSeqNum
            cmd.Parameters(":istLnNum").Value = istLnNum
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

            Dim newIstLnNum As Double = GetNextIstLnNum(istStoreCd, istSeqNum, istWrDt)

            sqlstmt.Clear()
            ' TODO copied from SORW.inp but qty is different (???) - and how can use same OUTLET and SER_NUM info??? - 
            ' can() 't split there - this would be for move ???
            sqlstmt.Append("INSERT INTO IST_LN (IST_WR_DT,  IST_STORE_CD, IST_SEQ_NUM, LN#, ITM_CD, QTY, CRPT_SER_NUM, CRPT_ID_NUM, FIFO_CST, ").Append(
                "FIFO_DT, FIFL_DT,  UNIT_CST, VOID_FLAG, ID_CD, STAT_CD, OUT_COMM_CD, OUT_SPIFF,  OOC_QTY, DEST_FLAG, FRAN_MRKUP_AMT, SER_NUM) ").Append(
                "SELECT IST_WR_DT, IST_STORE_CD, IST_SEQ_NUM, :newIstLnNum, ITM_CD, :qtyOnNew, CRPT_SER_NUM, CRPT_ID_NUM, FIFO_CST, ").Append(
                "FIFO_DT, FIFL_DT,  UNIT_CST, VOID_FLAG, ID_CD, STAT_CD, OUT_COMM_CD, OUT_SPIFF,  OOC_QTY, DEST_FLAG, FRAN_MRKUP_AMT, SER_NUM ").Append(
                "FROM IST_LN WHERE IST_WR_DT = :istWrDt AND IST_STORE_CD = :istStoreCd AND IST_SEQ_NUM = :istSeqNum  AND LN# = :istLnNum ")

            cmd.CommandText = sqlstmt.ToString
            cmd.Parameters.Add(":newIstLnNum", OracleType.Number)
            cmd.Parameters.Add(":qtyOnNew", OracleType.Number)
            cmd.Parameters.Add(":istWrDt", OracleType.DateTime)
            cmd.Parameters.Add(":istStoreCd", OracleType.VarChar)
            cmd.Parameters.Add(":istSeqNum", OracleType.VarChar)
            cmd.Parameters.Add(":istLnNum", OracleType.Number)

            cmd.Parameters(":newIstLnNum").Value = newIstLnNum
            cmd.Parameters(":qtyOnNew").Value = qtyOnNew
            cmd.Parameters(":istWrDt").Value = istWrDt
            cmd.Parameters(":istStoreCd").Value = istStoreCd
            cmd.Parameters(":istSeqNum").Value = istSeqNum
            cmd.Parameters(":istLnNum").Value = istLnNum
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

            sqlstmt.Clear()
            sqlstmt.Append("INSERT INTO IST_LN$SO_LN (DEL_DOC_NUM, DEL_DOC_LN#, IST_WR_DT, IST_STORE_CD, IST_SEQ_NUM, IST_LN#, DIST, SO_LN_SEQ, ").Append(
                      "  SE_PART_SEQ) SELECT :newDelDocNum, :newSoLnNum, IST_WR_DT, IST_STORE_CD, IST_SEQ_NUM, :newIstLnNum, DIST, SO_LN_SEQ, ").Append(
                      "  SE_PART_SEQ FROM ist_ln$so_ln WHERE IST_WR_DT = :istWrDt AND IST_STORE_CD = :istStoreCd AND IST_SEQ_NUM = :istSeqNum ").Append(
                      " AND IST_LN# = :istLnNum AND del_Doc_num = :origDelDocNum AND del_doc_ln# = :origSoLnNum ")

            cmd.CommandText = sqlstmt.ToString
            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":newSoLnNum", OracleType.Number)
            cmd.Parameters.Add(":newIstLnNum", OracleType.Number)
            cmd.Parameters.Add(":istWrDt", OracleType.DateTime)
            cmd.Parameters.Add(":istStoreCd", OracleType.VarChar)
            cmd.Parameters.Add(":istSeqNum", OracleType.VarChar)
            cmd.Parameters.Add(":istLnNum", OracleType.Number)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origSoLnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":newSoLnNum").Value = CInt(newSolnNum)
            cmd.Parameters(":newIstLnNum").Value = newIstLnNum
            cmd.Parameters(":istWrDt").Value = istWrDt
            cmd.Parameters(":istStoreCd").Value = istStoreCd
            cmd.Parameters(":istSeqNum").Value = istSeqNum
            cmd.Parameters(":istLnNum").Value = istLnNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origSoLnNum").Value = CInt(origSolnNum)
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

            ist = Create_CustOwnedIst(istStoreCd, istSeqNum, istWrDt, istLnNum, newIstLnNum)
        End If

        Return ist
    End Function

    Private Function GetRfInvForSoLn(ByVal delDocNum As String, ByVal lnNum As String,
                                     ByVal itmCd As String, ByVal noPicked As Boolean) As DataTable
        ' Extract and return the information from the RF inventory (INV_XREF)n open, non-void IST_LN, if link exists, for an SO_LN

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim datSet As New DataSet
        Dim oAdp As OracleDataAdapter

        Dim sqlstmt As New StringBuilder
        sqlstmt.Append("SELECT X.RF_ID_CD FROM INV_XREF X  ").Append(
                  "WHERE X.DEL_DOC_NUM = :delDocNum AND X.DEL_DOC_LN# = :lnNum AND X.ITM_CD = :itmCd AND X.REPLACED_BY IS NULL ")
        If noPicked Then
            sqlstmt.Append(" AND NOT EXISTS ").Append(
                                "(SELECT 'X' FROM RF_PICK P WHERE(P.RF_ID_CD = X.RF_ID_CD) And P.VOID_FLAG Is NULL  AND P.STAT_CD IN ('PCK','CHK')) ")
        End If

        Try
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlstmt.ToString, conn)

            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":lnNum", OracleType.Number)
            cmd.Parameters.Add(":itmCd", OracleType.VarChar)

            cmd.Parameters(":delDocNum").Value = delDocNum
            cmd.Parameters(":lnNum").Value = CInt(lnNum)
            cmd.Parameters(":itmCd").Value = itmCd

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)
            oAdp.Dispose()

        Catch ex As Exception
            oAdp.Dispose()
            Throw
        Finally
            conn.Close()
        End Try

        Return datSet.Tables(0)

    End Function

    Private Sub MoveSoLineAndRelated(ByRef datRowSoLn As DataRow, ByRef datTabSoLn As DataTable, ByRef cmd As OracleCommand)
        ' updates line related objects for moved (SORW) SO_LN's

        Dim newDelDocNum As String = datRowSoLn("new_del_doc_num").ToString
        Dim origDelDocNum As String = datRowSoLn("DEL_DOC_NUM").ToString
        Dim delDocLnNum As Integer = CInt(datRowSoLn("DEL_DOC_LN_NUM").ToString)

        ' THIS SO_LN updt must be FIRST - must MOVE the SO_LN itself so Primary Key exists for other processing
        '   using sqlSet variable as set string
        Dim sqlSet = " DEL_DOC_NUM = :new_del_doc_num "

        Dim lnLinks As LineLinks = FindLinkedLines(datRowSoLn, datTabSoLn, linkTypes.treatAndWarr)
        updtLinkLnNum(datRowSoLn, lnLinks, sqlSet, True)
        Dim soLnInp As soLnUpdt = Create_SoLnUpdt(origDelDocNum, newDelDocNum, delDocLnNum, sqlSet)
        Update_SO_LN(soLnInp, cmd)
        'Dim updt As New soLn_updates
        'updt.move = True
        'Update_SoLn(soLnInp, datRowSoLn, lnLinks, updt, cmd)
        ' end of SO_LN updt

        Dim sqlstmtSb As New StringBuilder
        ' for comm_paid, the last entry is the one that is valid
        sqlstmtSb.Append("UPDATE comm_paid cp1 SET DEL_DOC_NUM = :newDelDocNum, dt_time = SYSDATE ").Append(
            "WHERE DEL_DOC_NUM = :origDelDocNum  AND DEL_DOC_LN = :origlnNum AND dt_time = ").Append(
            "(SELECT MAX(cp2.dt_time) FROM comm_paid cp2 WHERE cp1.del_doc_num = cp2.del_doc_num ").Append(
            "AND cp1.del_doc_ln = cp2.del_doc_ln)")
        cmd.CommandText = sqlstmtSb.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origlnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters(":origlnNum").Value = delDocLnNum
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        sqlstmtSb.Clear()
        If "Y".Equals(datRowSoLn("warrantable").ToString) Then

            sqlstmtSb.Append("UPDATE so_ln2warr SET del_doc_num = :newDelDocNum ").Append(
                           "WHERE del_doc_num = :origDelDocNum  AND del_doc_ln# = :origlnNum")
            cmd.CommandText = sqlstmtSb.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = delDocLnNum
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        ElseIf SkuUtils.ITM_TP_WAR.Equals(datRowSoLn("itm_tp_cd").ToString) Then

            sqlstmtSb.Append("UPDATE so_ln2warr SET warr_del_doc_num = :newDelDocNum ").Append(
                           "WHERE warr_del_doc_num = :origDelDocNum AND warr_del_doc_ln# = :origlnNum")
            cmd.CommandText = sqlstmtSb.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = delDocLnNum
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

        If (Not String.IsNullOrEmpty(datRowSoLn("so_disc_cd").ToString.Trim)) AndAlso SysPms.multiDiscCd = datRowSoLn("so_disc_cd").ToString.Trim Then

            sqlstmtSb.Clear()
            sqlstmtSb.Append("UPDATE SO_LN2DISC SET DEL_DOC_NUM = :newDelDocNum ").Append(
                           "WHERE DEL_DOC_NUM = :origDelDocNum  AND DEL_DOC_LN# = :origlnNum")
            cmd.CommandText = sqlstmtSb.ToString
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origlnNum").Value = delDocLnNum
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

        ' line has inventory filled then update inventory
        If (Not String.IsNullOrWhiteSpace(datRowSoLn("noDisplay_store_cd").ToString)) AndAlso
            (Not String.IsNullOrWhiteSpace(datRowSoLn("noDisplay_loc_cd").ToString)) Then

            MoveRelatedInv(origDelDocNum, delDocLnNum, newDelDocNum, cmd)
            MoveRelatedIstLn(origDelDocNum, delDocLnNum, newDelDocNum, cmd)

        ElseIf (Not String.IsNullOrWhiteSpace(datRowSoLn("po_cd").ToString)) AndAlso
            (Not String.IsNullOrWhiteSpace(datRowSoLn("po_ln_num").ToString)) Then

            MoveRelatedPoLn(datRowSoLn("po_cd").ToString, datRowSoLn("po_ln_num").ToString, newDelDocNum, cmd, datRowSoLn("DEL_DOC_LN_NUM").ToString)
        End If
    End Sub

    Public Sub MoveRelatedInv(ByVal origDelDocNum As String, ByVal origSolnNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        ' creates new line related INVENTORY objects for moved (SORW) SO_LN's

        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.ITM_INV) Then

            cmd.CommandText = " UPDATE ITM_INV   SET DEL_DOC_NUM = :newDelDocNum  " &
                              " WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN# = :origSolnNum "
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origSoLnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origSoLnNum").Value = CInt(origSolnNum)

            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

        If SysPms.isRFEnabled Then
            MoveRelatedRfInv(origDelDocNum, origSolnNum, newDelDocNum, cmd)
        End If

    End Sub

    Private Sub MoveRelatedRfInv(ByVal origDelDocNum As String, ByVal origSolnNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        ' creates new line related INVENTORY objects for moved (SORW) SO_LN's

        Dim empCd As String = Session("emp_cd")
        Dim tranDt As Date = Today.Date

        Dim sqlstmt As New StringBuilder
        sqlstmt.Append("UPDATE INV_XREF SET DEL_DOC_NUM = :newDelDocNum, LAST_ACTN_EMP_CD = :empCd, ").Append(
                                "   LAST_ACTN_DT = :tranDt, ORIGIN_CD = 'SORW+',  STAGE = NULL ").Append(
                                "WHERE DEL_DOC_NUM = :origDelDocNum  AND  DEL_DOC_LN# = :origSolnNum ")
        cmd.CommandText = sqlstmt.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":empCd", OracleType.VarChar)
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origSoLnNum", OracleType.Number)
        cmd.Parameters.Add(":tranDt", OracleType.DateTime)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":empCd").Value = empCd
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters(":origSoLnNum").Value = CInt(origSolnNum)
        cmd.Parameters(":tranDt").Value = tranDt
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        VoidRfPick(origDelDocNum, origSolnNum, cmd)
        ' TODO - NOT completed - see SPLIT_DOCUMENT_PENDING_LINES
    End Sub

    Private Sub MoveRelatedIstLn(ByVal origDelDocNum As String, ByVal origSolnNum As String,
                                 ByVal newDelDocNum As String, ByRef cmd As OracleCommand)
        ' creates new line related INVENTORY objects for moved (SORW) SO_LN's

        Dim sqlstmt As String = ""

        Dim istLns As DataTable = GetIstLnForSoLn(origDelDocNum, origSolnNum)
        If istLns.Rows.Count > 0 Then

            Dim istLn As DataRow = istLns.Rows(0)
            Dim istLnNum As Double = istLn("IST_LN#")
            Dim istDocNum As String = istLn("DOC_NUM")

            VoidRfPick(istDocNum, istLnNum, cmd)

            sqlstmt = "UPDATE IST_LN$SO_LN  SET DEL_DOC_NUM = :newDelDocNum  " &
                      "WHERE  DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN# = :origSolnNum "
            cmd.CommandText = sqlstmt
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Clear()
            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origSolnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = origDelDocNum
            cmd.Parameters(":origSolnNum").Value = CInt(origSolnNum)
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        End If

        ' TODO - NOT completed - see SPLIT_DOCUMENT_PENDING_LINES
    End Sub

    Private Sub UpdtInvXref(ByVal delDocNum As String, ByVal rfIdCd As String, ByVal ist As CustOwnedIst, ByRef cmd As OracleCommand)
        ' creates new line related INV_XREF objects and updates original related objects for split SO_LN's

        Dim empCd As String = Session("emp_cd")
        Dim tranDt As Date = Today.Date

        Dim sqlstmtSb As New StringBuilder
        sqlstmtSb.Append("UPDATE INV_XREF SET DEL_DOC_NUM = :delDocNum, LAST_ACTN_EMP_CD = :empCd, LAST_ACTN_DT = :tranDt, ORIGIN_CD = 'SORW+' ")
        If Not IsNothing(ist) Then
            sqlstmtSb.Append(", ist_ln# = :newIstlnNum ")
        End If
        sqlstmtSb.Append("WHERE REPLACED_BY IS NULL AND RF_ID_CD = :rfIdCd ")
        cmd.CommandText = sqlstmtSb.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
        cmd.Parameters(":delDocNum").Value = delDocNum
        cmd.Parameters.Add(":empCd", OracleType.VarChar)
        cmd.Parameters(":empCd").Value = empCd
        cmd.Parameters.Add(":rfIdCd", OracleType.VarChar)
        cmd.Parameters(":rfIdCd").Value = rfIdCd
        cmd.Parameters.Add(":tranDt", OracleType.DateTime)
        cmd.Parameters(":tranDt").Value = tranDt

        If Not IsNothing(ist) Then
            cmd.Parameters.Add(":newIstlnNum", OracleType.Number)
            cmd.Parameters(":newIstlnNum").Value = CInt(ist.newLnNum)
        End If

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        ' TODO - NOT completed - see SPLIT_DOCUMENT_PENDING_LINES

    End Sub

    Public Function GetInvForSoLn(ByVal delDocNum As String, ByVal lnNum As Double) As DataTable
        ' Extract and return the information from the RF inventory (INV_XREF)n open, non-void IST_LN, if link exists, for an SO_LN

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim datSet As New DataSet
        Dim oAdp As OracleDataAdapter

        Dim sqlstmt As String = "SELECT INV_ID, QTY, ooc_qty  FROM ITM_INV  WHERE DEL_DOC_NUM = :delDocNum AND DEL_DOC_LN# = :lnNum " &
                  " ORDER BY INV_ID "
        Try
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlstmt, conn)

            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":lnNum", OracleType.Number)

            cmd.Parameters(":delDocNum").Value = delDocNum
            cmd.Parameters(":lnNum").Value = CInt(lnNum)

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)
            oAdp.Dispose()

        Catch ex As Exception
            oAdp.Dispose()
            Throw
        Finally
            conn.Close()
        End Try

        Return datSet.Tables(0)

    End Function

    Public Function GetNextInvId(ByVal tranDt As Date) As String
        'Returns the next ITM_INV INV_ID available 

        Dim returnVal As String = ""

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim sqlstmt As String = "bt_util.inv_id_seq_gen"

        Dim cmd = DisposablesManager.BuildOracleCommand(sqlstmt, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            conn.Open()
            cmd.Parameters.Add("returnValue", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add("seq_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input

            cmd.Parameters("seq_dt_i").Value = tranDt
            cmd.ExecuteNonQuery()

            returnVal = cmd.Parameters("returnValue").Value

        Catch ex As Exception
            Throw New Exception("Error in getNextInvId ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnVal
    End Function

    'TODO DSA CHG   - Added - this needs to be called when the qty is set on the new line - if attached to PO, then must move whole line or nothing
    'this validation must be where set the line value

    '            IF :SORW1_B4.PO_CD IS NOT NULL THEN
    '           IF NVL(:SORW1_B4.MOVE,0) != 0 AND
    '              NVL(:SORW1_B4.MOVE,0) != NVL(:SORW1_B4.ORIG_QTY,0) THEN
    '              FAIL('Line is attached to a purchase order -- Must move entire quantity or none.'); 
    '  ALSO - retain PO_CD/PO_LN for the SO_LN (if possible) for MOVE
    ' UNTESTED
    Private Function GetRelatedPoLn(ByVal delDocNum As String, ByVal lnNum As Double) As DataTable
        'Returns the related PO and PO line for a specific SO line

        Dim returnVal As String = ""

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim datSet As New DataSet
        Dim oAdp As OracleDataAdapter

        Dim sqlstmt As String = "SELECT PO_CD, LN#    FROM SO_LN$PO_LN    WHERE  DEL_DOC_NUM = :delDocNum " &
                                "AND  DEL_DOC_LN# = :lnNum "

        Try
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlstmt, conn)

            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":lnNum", OracleType.Number)

            cmd.Parameters(":delDocNum").Value = delDocNum
            cmd.Parameters(":lnNum").Value = CInt(lnNum)

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)
            cmd.ExecuteNonQuery()

        Finally
            oAdp.Dispose()
            conn.Close()
        End Try

        Return datSet.Tables(0)

    End Function

    Private Sub UpdtItmInvDelDoc(ByVal origDelDocNum As String, ByVal origSolnNum As Double, ByVal newDelDocNum As String,
                          ByVal invId As String, ByVal ist As CustOwnedIst, ByRef cmd As OracleCommand)
        ' moves ITM_INV record to new Document for SPLIT of SO_LN where this ITM_INV is portion or all of split
        '  passing the IST information means that a new IST line for customer owned was created for the split-off and the IST_LN# must also be updated

        Dim sqlStmt As New StringBuilder
        sqlStmt.Append("UPDATE ITM_INV  SET DEL_DOC_NUM = :newDelDocNum ")
        ' if split an attached IST line, then update the IST_LN number here also
        If Not IsNothing(ist) Then
            sqlStmt.Append(", ist_ln# = :newIstlnNum ")
        End If
        sqlStmt.Append(" WHERE DEL_DOC_NUM = :origDelDocNum  AND DEL_DOC_LN# = :origSolnNum  AND INV_ID = :invId ")

        cmd.CommandText = sqlStmt.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters.Add(":invId", OracleType.VarChar)
        cmd.Parameters(":invId").Value = invId
        cmd.Parameters.Add(":origSolnNum", OracleType.Number)
        cmd.Parameters(":origSolnNum").Value = CInt(origSolnNum)

        If Not IsNothing(ist) Then
            cmd.Parameters.Add(":newIstlnNum", OracleType.Number)
            cmd.Parameters(":newIstlnNum").Value = CInt(ist.newLnNum)
        End If

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

    End Sub

    Public Sub UpdtItmInvQty(ByVal origDelDocNum As String, ByVal origSolnNum As Double, ByVal newQtyOnOrig As Double, ByVal newOocQtyOnOrig As Double, ByRef cmd As OracleCommand)
        ' creates new line related INV_XREF objects and updates original related objects for split SO_LN's

        cmd.CommandText = "UPDATE ITM_INV  SET QTY = :newQtyOnOrig, OOC_QTY = :newOocQtyOnOrig " &
                          "WHERE DEL_DOC_NUM = :origDelDocNum  AND DEL_DOC_LN# = :origSolnNum  "
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origSolnNum", OracleType.Number)
        cmd.Parameters.Add(":newQtyOnOrig", OracleType.Number)
        cmd.Parameters.Add(":newOocQtyOnOrig", OracleType.Number)

        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters(":origSolnNum").Value = CInt(origSolnNum)
        cmd.Parameters(":newQtyOnOrig").Value = newQtyOnOrig
        cmd.Parameters(":newOocQtyOnOrig").Value = newOocQtyOnOrig

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

    End Sub

    Public Sub MoveRelatedPoLn(ByVal poCd As String, ByVal polnNum As String, ByVal newDelDocNum As String, ByRef cmd As OracleCommand, ByVal delDocLnNum As String)
        ' creates new line related INV_XREF objects and updates original related objects for split SO_LN's

        cmd.CommandText = "UPDATE SO_LN$PO_LN  SET DEL_DOC_NUM = :newDelDocNum " &
                          " WHERE PO_CD = :poCd  AND LN# = :polnNum  And DEL_DOC_NUM=:oldDelDocNum and DEL_DOC_LN#=:oldDelDocLnNum "
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":poCd", OracleType.VarChar)
        cmd.Parameters.Add(":polnNum", OracleType.Number)
        cmd.Parameters.Add(":oldDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":oldDelDocLnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":poCd").Value = poCd
        cmd.Parameters(":polnNum").Value = CInt(polnNum)
        cmd.Parameters(":oldDelDocNum").Value = txt_del_doc_existing.Text.Trim
        cmd.Parameters(":oldDelDocLnNum").Value = CInt(delDocLnNum)
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        cmd.CommandText = "UPDATE po_ln2rf_id_cd  SET DEL_DOC_NUM = :newDelDocNum " &
                  "WHERE PO_CD = :poCd  AND LN# = :polnNum  "

        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":poCd", OracleType.VarChar)
        cmd.Parameters.Add(":polnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters(":poCd").Value = poCd
        cmd.Parameters(":polnNum").Value = CInt(polnNum)

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        ' TODO future - should this even be here
        ' cannot execute because constraint POLN2RFXDK_SOLN_FK will cause to fail (most likely on SO_LN update)- not sure when/how this created and wouldn't want to allow split anyway?
        'cmd.CommandText = "UPDATE po_ln2rf_xdock  SET DEL_DOC_NUM = :newDelDocNum " &
        '  "WHERE PO_CD = :poCd  AND LN# = :polnNum  "

        'cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        'cmd.Parameters.Add(":poCd", OracleType.VarChar)
        'cmd.Parameters.Add(":polnNum", OracleType.Number)

        'cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        'cmd.Parameters(":poCd").Value = poCd
        'cmd.Parameters(":polnNum").Value = CInt(polnNum)

        'cmd.ExecuteNonQuery()
        'cmd.Parameters.Clear()
    End Sub

    Private Sub InsertItmInvFromOrig(ByVal origDelDocNum As String, ByVal origSolnNum As Double, ByVal origInvId As String, ByVal newDelDocNum As String, ByVal newSolnNum As Double,
                            ByVal qtyOnNew As Double, ByVal oocQtyOnNew As Double, ByVal ist As CustOwnedIst, ByRef cmd As OracleCommand)
        ' creates new line related INV_XREF objects and updates original related objects for split SO_LN's

        Dim tranDt As Date = Today.Date
        Dim newInvId As String = GetNextInvId(tranDt)
        Dim strBldr As New StringBuilder

        strBldr.Append("INSERT INTO ITM_INV (INV_ID, INV_STATUS, ITM_CD, STORE_CD, LOC_CD, RCV_DT, QTY, CRPT_ID_NUM, CRPT_SER_NUM, ").Append(
                          " CST, UP_CHRG, DIV_CD, LST_ACT_DT, PO_CD, PO_LN#, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ").Append(
                          " IST_SEQ_NUM, NAS_OUT_ID, OOC_QTY, IST_LN# ) ")
        strBldr.Append(" SELECT :newInvId, INV_STATUS, ITM_CD, STORE_CD, LOC_CD, RCV_DT, :qtyOnNew, CRPT_ID_NUM, CRPT_SER_NUM, ").Append(
                         " CST, UP_CHRG, DIV_CD, :tranDt, PO_CD, PO_LN#, :newDelDocNum, :newSolnNum, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ").Append(
                         " IST_SEQ_NUM, NAS_OUT_ID, :oocQtyOnNew, ")
        If IsNothing(ist) Then
            strBldr.Append(" IST_LN#")
        Else
            strBldr.Append(" :newIstlnNum ")
        End If
        strBldr.Append(" FROM ITM_INV WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN# = :origSolnNum ")
        cmd.CommandText = strBldr.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":oocQtyOnNew", OracleType.Number)
        cmd.Parameters(":oocQtyOnNew").Value = oocQtyOnNew
        cmd.Parameters.Add(":qtyOnNew", OracleType.Number)
        cmd.Parameters(":qtyOnNew").Value = qtyOnNew
        cmd.Parameters.Add(":tranDt", OracleType.DateTime)
        cmd.Parameters(":tranDt").Value = tranDt
        cmd.Parameters.Add(":newInvId", OracleType.VarChar)
        cmd.Parameters(":newInvId").Value = newInvId
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters(":origDelDocNum").Value = origDelDocNum
        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters(":newDelDocNum").Value = newDelDocNum
        cmd.Parameters.Add(":origSolnNum", OracleType.Number)
        cmd.Parameters(":origSolnNum").Value = CInt(origSolnNum)
        cmd.Parameters.Add(":newSolnNum", OracleType.Number)
        cmd.Parameters(":newSolnNum").Value = CInt(newSolnNum)
        If Not IsNothing(ist) Then
            cmd.Parameters.Add(":newIstlnNum", OracleType.Number)
            cmd.Parameters(":newIstlnNum").Value = CInt(ist.newLnNum)
        End If

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        ' split the costs info
        cmd.CommandText = "bt_other_cst.split_inv_csts"
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("inv_id1_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("inv_id2_i", OracleType.VarChar).Direction = ParameterDirection.Input

        cmd.Parameters("inv_id1_i").Value = origInvId
        cmd.Parameters("inv_id2_i").Value = newInvId
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

    End Sub

    Private Sub SplitRelatedInv(ByVal origDelDocNum As String, ByVal origSolnNum As Double, ByVal newQtyOnOrig As Double,
                         ByVal newDelDocNum As String, ByVal newSolnNum As Double,
                         ByVal qtyOnNew As Double, ByVal itmCd As String, ByVal ist As CustOwnedIst, ByRef cmd As OracleCommand)
        ' creates new line related INVENTORY objects and updates original related objects for split SO_LN's
        '  passing the IST information means that a new IST line for customer owned was created for the split-off and the IST_LN# must also be updated

        Dim newOocQtyOnOrigTot As Double = 0
        Dim newOocQtyTot As Double = 0

        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.ITM_INV) Then

            Dim invRows As DataTable
            Dim updtCnt As Double = 0
            Dim invRow As DataRow
            Dim rowNum As Integer = 0
            Dim qtyRemain As Double
            Dim newOocQtyOnOrig As Double
            Dim newOocQty As Double
            Dim qtyNeeded As Double = qtyOnNew

            invRows = GetInvForSoLn(origDelDocNum, origSolnNum)

            If invRows.Rows.Count > 0 Then

                Do While (qtyNeeded > 0 And
                          (rowNum <= invRows.Rows.Count - 1))

                    invRow = invRows.Rows(rowNum)   ' TODO - does this work ???
                    If invRow("QTY") <= qtyNeeded Then

                        UpdtItmInvDelDoc(origDelDocNum, origSolnNum, newDelDocNum, invRow("INV_ID"), ist, cmd)

                        qtyNeeded = qtyNeeded - invRow("QTY")

                        ' TODO - ooc qty sb calc'd from SO_LN split - sent in
                        newOocQtyTot = newOocQtyTot + invRow("OOC_QTY")

                    Else
                        qtyRemain = invRow("QTY") - qtyNeeded
                        ' TODO - ooc qty sb calc'd from SO_LN split - sent in
                        ' delivery - use out of carton first; pickups use in-carton first - not that detailed here
                        '   delivery/pickup split logic in bt_inv_manger.subtract_from_itm_inv IF inv_updt_rec_i.pu_del_flg = k_delivery THEN  
                        If qtyRemain < invRow("OOC_QTY") Then

                            newOocQty = qtyRemain
                            newOocQtyOnOrig = invRow("OOC_QTY") - newOocQty

                        Else
                            newOocQty = invRow("OOC_QTY")
                            newOocQtyOnOrig = 0
                        End If

                        UpdtItmInvQty(origDelDocNum, origSolnNum, qtyRemain, newOocQtyOnOrig, cmd)
                        InsertItmInvFromOrig(origDelDocNum, origSolnNum, invRow("INV_ID"), newDelDocNum, newSolnNum, qtyNeeded, newOocQty, ist, cmd)

                        qtyNeeded = 0
                        newOocQtyTot = newOocQtyTot + newOocQty
                        newOocQtyOnOrigTot = newOocQtyOnOrigTot + newOocQtyOnOrig

                    End If
                    rowNum = rowNum + 1
                Loop

            End If

        Else
            newOocQtyOnOrigTot = 0
            newOocQtyTot = 0
        End If

        If SysPms.isRFEnabled Then
            SplitRelatedRfInv(origDelDocNum, origSolnNum, newQtyOnOrig, newDelDocNum, newSolnNum, qtyOnNew, itmCd, newOocQtyOnOrigTot,
                              newOocQtyTot, ist, cmd)
        End If
    End Sub

    Private Sub SplitRelatedRfInv(ByVal origDelDocNum As String, ByVal origSolnNum As Double, ByVal newQtyOnOrig As Double,
                                 ByVal newDelDocNum As String, ByVal newSolnNum As Double, ByVal qtyOnNew As Double,
                                 ByVal itmCd As String, ByVal newOocQtyOnOrigTot As Double, ByVal newOocQtyTot As Double, ByVal ist As CustOwnedIst, ByRef cmd As OracleCommand)
        ' creates new line related INV_XREF objects and updates original related objects for split SO_LN's
        '  passing the IST information means that a new IST line for customer owned was created for the split-off and the IST_LN# must also be updated 

        'TODO - apply OOC qty logic - use the ooc qty input to select more specifically which rows (in or out of carton to move)

        Dim rfUpdtCnt As Double = 0
        Dim invXref As DataRow
        Dim invXrefs As DataTable = GetRfInvForSoLn(origDelDocNum, origSolnNum, itmCd, True)

        'process the PICKED RF inventory (INV_XREF)n open, non-void IST_LN, if link exists, for the SO_LN
        If invXrefs.Rows.Count > 0 Then

            Do While rfUpdtCnt < qtyOnNew

                For Each invXref In invXrefs.Rows

                    UpdtInvXref(newDelDocNum, invXref.Item("RF_ID_CD"), ist, cmd)
                    rfUpdtCnt = rfUpdtCnt + 1
                    If rfUpdtCnt >= qtyOnNew Then
                        Exit For
                    End If

                Next
            Loop
        End If

        'if qty not fulfilled yet, moves the remaining PICKED RF inventory (INV_XREF)n pieces for the SO_LN
        If rfUpdtCnt < qtyOnNew Then

            invXrefs = GetRfInvForSoLn(origDelDocNum, origSolnNum, itmCd, False) ' move remaining, even if picked
            If invXrefs.Rows.Count > 0 Then

                Do While rfUpdtCnt < qtyOnNew

                    For Each invXref In invXrefs.Rows

                        UpdtInvXref(newDelDocNum, invXref.Item("RF_ID_CD"), ist, cmd)
                        rfUpdtCnt = rfUpdtCnt + 1
                        If rfUpdtCnt >= qtyOnNew Then
                            Exit For
                        End If

                    Next
                Loop
            End If
        End If

        VoidRfPick(origDelDocNum, origSolnNum, cmd)

        ' TODO - NOT completed - see SPLIT_DOCUMENT_PENDING_LINES
    End Sub

#End Region

    Protected Sub btn_Save_ValidationContainerResolve(sender As Object, e As DevExpress.Web.ASPxClasses.ControlResolveEventArgs) Handles btn_Save.ValidationContainerResolve

    End Sub


    Public Sub DisableSOMButton()
        btnSOM.Enabled = False
    End Sub

    Public Sub EnableSOMButton()
        btnSOM.Enabled = True
    End Sub

    Protected Sub cbo_new_pd_store_cd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_new_pd_store_cd.SelectedIndexChanged
        DisableSOMButton()
    End Sub

    Protected Sub cbo_pd_date_new_ValueChanged(sender As Object, e As EventArgs) Handles cbo_pd_date_new.ValueChanged
        DisableSOMButton()
    End Sub

    Protected Sub btnSOM_Click(sender As Object, e As EventArgs) Handles btnSOM.Click
        If Find_Security("SOM", Session("emp_cd")) = "Y" Then
            Response.Redirect("SalesOrderMaintenance.aspx?del_doc_num=" & txt_del_doc_num.Text & "&query_returned=Y&BindGrid=Y", False)
        Else
            lbl_error.Text = "You do not have access to SOM"
        End If
    End Sub

    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkModifyTax.Click

        If (Not (Convert.ToBoolean(ViewState("CanModifyTaxOrig")))) Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
            ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
            msgPopup.ShowOnPageLoad = True
        Else

            Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
            hidIsInvokeManually.Value = "true"

            Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
            Dim theSoHdrInfo As SoHdrRewriteInfo = Session("soHdrInfo")
            hidTaxDt.Value = theSoHdrInfo.writtenDt
            hidIsNewSO.Value = "False"

            ucTaxUpdate.LoadTaxData()
            PopupTax.ShowOnPageLoad = True
        End If
    End Sub


    Protected Sub lnkModifyTaxNew_Click(sender As Object, e As EventArgs) Handles lnkModifyTaxNew.Click

        If (Not (Convert.ToBoolean(ViewState("CanModifyTaxNew")))) Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
            ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
            msgPopup.ShowOnPageLoad = True
        Else
            Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
            hidIsInvokeManually.Value = "true"

            Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
            hidTaxDt.Value = FormatDateTime(Now(), DateFormat.ShortDate)

            hidIsNewSO.Value = "True"

            ucTaxUpdate.LoadTaxData()
            PopupTax.ShowOnPageLoad = True
        End If
    End Sub

    ''' <summary>
    ''' Responds as the subscriber to the tax update user control event. It will hide the popup
    ''' and update the UI and recalculate amounts based on the new tax info.
    ''' </summary>
    ''' <param name="sender"></param>
    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated

        Dim hidTaxCd As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxCd"), HiddenField)
        Dim hidTaxDesc As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDesc"), HiddenField)

        Dim isNewSO As Boolean

        Boolean.TryParse(hidIsNewSO.Value, isNewSO)

        If isNewSO Then
            lnkModifyTaxNew.Text = hidTaxCd.Value
            txt_tax_cd_new.Text = hidTaxCd.Value
        Else
            lnkModifyTax.Text = hidTaxCd.Value
            txt_tax_cd.Text = hidTaxCd.Value
        End If

        calculate_total()
        PopupTax.ShowOnPageLoad = False

    End Sub
    ''' <summary>
    ''' On select of zone code the fallowing method is called
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucZonUpdate_ZoneUpdated(sender As Object, e As ZoneInfo) Handles UcZonePopup.ZoneUpdated
        'If AppConstants.DELIVERY = cbo_PD_new.SelectedValue And Not IsNothing(e) Then
        If Not String.IsNullOrEmpty(e.ZoneCode) Then
            hidZoneCode.Value = e.ZoneCode
            hideZipCode.Value = e.ZipCode
            Dim hidZipcode As HiddenField = CType(UcDeliveryDatePopup.FindControl("hidZone"), HiddenField)
            If Not String.IsNullOrEmpty(hidZoneCode.Value) Then
                hidZipcode.Value = hidZoneCode.Value
            End If
            'bringup the del date popup for delivery sale type only
            If AppConstants.DELIVERY = cbo_PD_new.SelectedValue And Not IsNothing(e) Then
                Dim delvStore As String = e.DeliveryStoreCode
                Dim defDelChg As Decimal = e.Deliverycharges
                Dim defSetChg As Decimal = e.SetupCharges
                txt_setup_new.Text = FormatNumber(defSetChg.ToString, 2)
                txt_del_chg_new.Text = FormatNumber(defDelChg.ToString, 2)
                If ViewState("STORECHANGE") & "" = "Y" Then
                    'Dont change the store code.
                    ViewState("STORECHANGE") = Nothing
                Else
                    If Not String.IsNullOrEmpty(delvStore) Then
                        cbo_new_pd_store_cd.SelectedValue = delvStore
                    End If
                End If
                UcDeliveryDatePopup.GetDeliveryDates(txt_del_doc_existing.Text)
                ' Do not show popup every time
                'PopupDeliveryDate.ShowOnPageLoad = True
            End If
            PopupZone.ShowOnPageLoad = False
            calculate_total()
            lbl_old_or_new.Text = "NEW"
            If Not String.IsNullOrEmpty(hidZoneCode.Value) AndAlso Not String.IsNullOrEmpty(hideZipCode.Value) Then
                'message should be populated here because zone/zip changes are made on this event.
                lblZoneChangeNotification.Text = "Zone is changed to " & hidZoneCode.Value & ".(Zip Code = " & hideZipCode.Value & ")"
            End If
        End If
        'End If
    End Sub
    ''' <summary>
    ''' On select of delivery date the fallowing method is called
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="selecteddate"></param>
    Protected Sub ucDeliveryDate_DeliveryDateUpdated(sender As Object, selecteddate As Date) Handles UcDeliveryDatePopup.DeliveryDateSelected
        If selecteddate <> Nothing Then
            If lbl_old_or_new.Text = "OLD" Then

                cbo_pd_date.Value = selecteddate.Date

            Else
                cbo_pd_date_new.Value = selecteddate.Date
            End If
            IsZoneSelected.Value = True
        Else
            hidZoneCode.Value = Nothing
            IsZoneSelected.Value = False
        End If
        PopupDeliveryDate.ShowOnPageLoad = False
        If Not String.IsNullOrEmpty(hidZoneCode.Value) AndAlso Not String.IsNullOrEmpty(hideZipCode.Value) Then
            'If Sale is changed to delivery then after selecting delivery date message should be retained on page.
            lblZoneChangeNotification.Text = "Zone is changed to " & hidZoneCode.Value & ".(Zip Code = " & hideZipCode.Value & ")"
        End If
    End Sub
    '''
    Private Sub CalcSoLnPoints(ByRef soLn As DataRow, ByVal origqty As Double, ByVal newqty_on_origLn As Double, ByVal qty_on_newLn As Double,
                                 ByVal origTranSched As TransportationSchedule, ByVal currTranSched As TransportationSchedule, ByRef tranPoints As TransportationPoints)

        If SysPms.isDelvByPoints Then
            If Not IsDBNull(soLn("POINT_SIZE").ToString) AndAlso soLn("POINT_SIZE").ToString.isNotEmpty AndAlso IsNumeric(soLn("POINT_SIZE").ToString) AndAlso CDbl(soLn("POINT_SIZE").ToString) > 0 Then

                If origTranSched.puDelCd = AppConstants.PICKUP AndAlso currTranSched.puDelCd = AppConstants.PICKUP Then
                    'nochange
                ElseIf origTranSched.puDelCd = AppConstants.PICKUP AndAlso currTranSched.puDelCd = AppConstants.DELIVERY Then
                    'schedule points
                    tranPoints.schedPointTot = tranPoints.schedPointTot + (qty_on_newLn * soLn("POINT_SIZE"))
                    tranPoints.currPointTot = tranPoints.currPointTot + (newqty_on_origLn * soLn("POINT_SIZE"))
                ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.PICKUP Then
                    'unschedule points
                    tranPoints.origPointTot = tranPoints.origPointTot + (origqty * soLn("POINT_SIZE"))
                    tranPoints.unschedPointTot = tranPoints.unschedPointTot + ((origqty - newqty_on_origLn) * soLn("POINT_SIZE"))
                ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY AndAlso origTranSched.puDelDt = currTranSched.puDelDt Then
                    'nochange
                ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY AndAlso origTranSched.puDelDt <> currTranSched.puDelDt Then
                    'schedule points and unschedule points
                    tranPoints.currPointTot = tranPoints.currPointTot + (newqty_on_origLn * soLn("POINT_SIZE"))
                    tranPoints.origPointTot = tranPoints.origPointTot + (origqty * soLn("POINT_SIZE"))
                    tranPoints.schedPointTot = tranPoints.schedPointTot + (qty_on_newLn * soLn("POINT_SIZE"))
                    tranPoints.unschedPointTot = tranPoints.unschedPointTot + ((origqty - newqty_on_origLn) * soLn("POINT_SIZE"))
                End If
            End If
        End If

    End Sub
    Private Sub ProcessTransportationUpdt(ByRef tranLnPoints As TransportationPoints, ByVal origTranSched As TransportationSchedule,
                                          ByVal currTranSched As TransportationSchedule, ByRef cmd As OracleCommand)

        Dim trnTp As String = AppConstants.Order.TYPE_SAL
        If origTranSched.puDelCd = AppConstants.PICKUP AndAlso currTranSched.puDelCd = AppConstants.DELIVERY Then
            theTMBiz.SchedPuDel(cmd, currTranSched.zoneCd, currTranSched.puDelCd, currTranSched.puDelDt,
                                       trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.schedPointTot, 1))
        ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.PICKUP Then
            If SysPms.isDelvByPoints Then
                theTMBiz.UnschedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                          trnTp, tranLnPoints.unschedPointTot)
            End If
        ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY AndAlso origTranSched.puDelDt <> currTranSched.puDelDt Then

            theTMBiz.SchedPuDel(cmd, currTranSched.zoneCd, currTranSched.puDelCd, currTranSched.puDelDt,
                                       trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.schedPointTot, 1))

            theTMBiz.UnschedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                  trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.unschedPointTot, 0))
        ElseIf origTranSched.puDelCd = AppConstants.DELIVERY AndAlso currTranSched.puDelCd = AppConstants.DELIVERY AndAlso origTranSched.puDelDt = currTranSched.puDelDt Then
            If Not SysPms.isDelvByPoints Then
                theTMBiz.SchedPuDel(cmd, currTranSched.zoneCd, currTranSched.puDelCd, currTranSched.puDelDt,
                                       trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.schedPointTot, 1))
            End If
        End If
    End Sub

    Private Function CheckForDeliveryAvaDT() As Boolean
        Dim zoneCode As String = If(hidZoneCode.Value.Trim.Equals(String.Empty), txt_zone_cd.Text.Trim, hidZoneCode.Value.Trim)
        If cbo_PD_new.SelectedValue = AppConstants.DELIVERY Then
            zoneCode = zoneCode.Trim
        Else
            zoneCode = txt_zone_cd.Text.Trim
        End If
        Dim currTranSched As New TransportationSchedule
        currTranSched.zoneCd = zoneCode.ToString
        currTranSched.puDelCd = cbo_PD_new.SelectedValue
        currTranSched.puDelDt = FormatDateTime(cbo_pd_date_new.Text, DateFormat.ShortDate)
        Dim delAvDT As Boolean = False
        If SysPms.isDelvByPoints Then
            Dim ds As DataSet
            If Not Session("LN_NEW") Is Nothing Then
                ds = Session("LN_NEW")
                If currTranSched.puDelCd = AppConstants.DELIVERY Then
                    Dim dt As DataTable = ds.Tables(0)
                    Dim gPointCnt As Double = 0.0
                    If dt.Rows.Count > 0 Then
                        For Each dr In dt.Rows
                            If Not IsDBNull(dr("POINT_SIZE")) AndAlso IsNumeric(dr("POINT_SIZE")) AndAlso IsNumeric(dr("QTY")) AndAlso dr("ITM_TP_CD") = AppConstants.Sku.TP_INV Then
                                gPointCnt = gPointCnt + (dr("POINT_SIZE") * dr("qty"))
                            End If
                        Next
                    End If
                    delAvDT = theTMBiz.CheckForDeliveryAvaDT(gPointCnt, currTranSched.puDelDt, zoneCode.ToString)
                Else
                    delAvDT = True
                End If
            End If
        Else
            If currTranSched.puDelCd = AppConstants.DELIVERY Then
                delAvDT = theTMBiz.CheckForDeliveryAvaDT(1.0, currTranSched.puDelDt, zoneCode.ToString)
            Else
                delAvDT = True
            End If
        End If
        Return delAvDT
    End Function
    Protected Sub PopupZone_Load(sender As Object, e As EventArgs)

        Dim hidZipcode As HiddenField = CType(UcZonePopup.FindControl("hidZip"), HiddenField)

        Dim hidSaleType As HiddenField = CType(UcZonePopup.FindControl("hidSaleType"), HiddenField)
        Dim hidZoneCode As HiddenField = CType(UcZonePopup.FindControl("hidZoneCode"), HiddenField)
        Dim txtzip As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(UcZonePopup.FindControl("txtzip"), DevExpress.Web.ASPxEditors.ASPxTextBox)

        'Get store Zip code so if sale type changes then according to sale type Zip code should populate
        If String.IsNullOrEmpty(hidStoreZipCode.Value) Then
            'Daniela Jan06
            'hidStoreZipCode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
            If cbo_PD_new.SelectedValue.Equals("P") Then
                hidStoreZipCode.Value = SalesUtils.getStoreZipCode(cbo_new_pd_store_cd.SelectedValue.ToString(), lbl_cust_cd.Text)
            Else
                hidStoreZipCode.Value = SalesUtils.getStoreZipCode(txt_store_cd.Text, lbl_cust_cd.Text)
            End If
        End If

        If cbo_PD_new.SelectedValue.Equals("P") Then
            hidZipcode.Value = hidStoreZipCode.Value
        Else
            hidZipcode.Value = hidCustomerZipCode.Value
        End If

        'if Zipcode is not there that means page is loading for first time so set value according to sale type
        If String.IsNullOrEmpty(txtzip.Text) Then
            txtzip.Text = hidZipcode.Value
        ElseIf hidCustomerZipCode.Value = txtzip.Text Then
            'If sale is type of Delivery then set the value of customer zip code
            txtzip.Text = hidCustomerZipCode.Value
        ElseIf hidStoreZipCode.Value = txtzip.Text Then
            'If sale is type of Pick Up then set the value of store zip code
            txtzip.Text = hidStoreZipCode.Value
        End If

        'If txt zip is not empty that means user has manipulated the value of zip code
        hidZipcode.Value = txtzip.Text
        hidSaleType.Value = cbo_PD_new.SelectedValue.ToString


    End Sub
    Protected Sub NewStoreChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_new_pd_store_cd.SelectedIndexChanged
        Dim store As StoreData = theSalesBiz.GetStoreInfo(cbo_new_pd_store_cd.SelectedValue)
        'hidArsEnabled.Value = store.enableARS
        'If cbo_PD_new.SelectedValue = "D" OrElse store.enableARS Then
        'bring the zone popup here.
        ViewState("STORECHANGE") = "Y"
        'if store is changed then zip should also be updated
        hidStoreZipCode.Value = String.Empty
        'Update zip code of new store
        If String.IsNullOrEmpty(hidStoreZipCode.Value) Then
            'If user change store then changed zip and zone should also appear in message
            hidStoreZipCode.Value = SalesUtils.getStoreZipCode(cbo_new_pd_store_cd.SelectedValue.ToString, lbl_cust_cd.Text)
        End If
        Dim zipValue As String = ShowZonePopup()
        'Else

        'End If

        Dim rowsCount As Integer = UcZonePopup.UpdateZonesDisplay(zipValue)
        'Update zone code for new zip code
        UcZonePopup.UpdateZoneInfo(rowsCount, sender)
        If rowsCount >= 1 Then
            'Empty zip/zone notification when ever store type changes
            lblZoneChangeNotification.Text = String.Empty
            'If multiple zones found then zone pop up should display first
            PopupDeliveryDate.ShowOnPageLoad = False
            PopupZone.ShowOnPageLoad = True
        Else
            If cbo_PD_new.SelectedValue = "D" Then
                PopupDeliveryDate.ShowOnPageLoad = True
            End If
        End If

    End Sub
    Private Function ShowZonePopup() As String
        Dim hidZipcode As HiddenField = CType(UcZonePopup.FindControl("hidZip"), HiddenField)

        Dim txtzip As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(UcZonePopup.FindControl("txtzip"), DevExpress.Web.ASPxEditors.ASPxTextBox)
        If cbo_PD_new.SelectedValue = "P" Then
            'If Sale is pick up type then store code zip should get selected
            hidZipcode.Value = hidStoreZipCode.Value
        Else
            'If Sale is delivery type then customer code zip should get selected
            hidZipcode.Value = hidCustomerZipCode.Value
        End If

        'When sale type is changed then Customer/Store zip code should get selected by default
        txtzip.Text = hidZipcode.Value

        determine_tax("NEW")

        Return txtzip.Text
    End Function
End Class
