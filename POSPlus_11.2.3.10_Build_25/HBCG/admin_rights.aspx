<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="admin_rights.aspx.vb" Inherits="admin_rights" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div id="frmsubmit" runat=server></div>
    Add User: &nbsp;<asp:DropDownList ID="cbo_new_users" runat="server" CssClass="style5"
        Width="296px">
    </asp:DropDownList>
    <asp:Button ID="btn_add_user" runat="server" CssClass="style5" Text="Add User" /><br />
    <asp:Label ID="lbl_errors" runat="server" ForeColor="Red" Width="602px"></asp:Label><br />
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
        AutoGenerateColumns="False" CellPadding="2" DataKeyField="emp_cd" Font-Bold="False"
        Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="144px" Width="643px" HeaderStyle-Height="20px"
        HeaderStyle-HorizontalAlign="Left" OnDeleteCommand="DoItemSave" CssClass="style5">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False" Height="20px"
            HorizontalAlign="Center" CssClass="style5"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="emp_cd" HeaderText="User ID" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Admin Page">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpAdmin" runat="server" DataTextField="admin_page" DataValueField="admin_page"
                        SelectedValue='<%# Bind("admin_page") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Credit Card Maintenance">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpCCMnt" runat="server" DataTextField="credit_card_info" DataValueField="credit_card_info"
                        SelectedValue='<%# Bind("credit_card_info") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Modify Help Pages">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpModHlp" runat="server" DataTextField="help_files" DataValueField="help_files"
                        SelectedValue='<%# Bind("help_files") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Change Rights">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpChgRts" runat="server" DataTextField="change_rights" DataValueField="change_rights"
                        SelectedValue='<%# Bind("change_rights") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Upload Pictures">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpUpPics" runat="server" DataTextField="upload_pics" DataValueField="upload_pics"
                        SelectedValue='<%# Bind("upload_pics") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Transfer Leads">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpTrfLds" runat="server" DataTextField="transfer_leads" DataValueField="transfer_leads"
                        SelectedValue='<%# Bind("transfer_leads") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Modify Invoices">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpModInv" runat="server" DataTextField="invoice_admin" DataValueField="invoice_admin"
                        SelectedValue='<%# Bind("invoice_admin") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Payment Setup">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drppaymentsetup" runat="server" DataTextField="invoice_admin" DataValueField="invoice_admin"
                        SelectedValue='<%# Bind("PAYMENT_XREF") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Terminal Setup">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drp_terminalsetup" runat="server" DataTextField="invoice_admin" DataValueField="invoice_admin"
                        SelectedValue='<%# Bind("TERMINAL_SETUP") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Terminal Status">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <ItemTemplate>
                    <asp:DropDownList ID="drpterminalstatus" runat="server" DataTextField="invoice_admin" DataValueField="invoice_admin"
                        SelectedValue='<%# Bind("TERMINAL_STATUS") %>' CssClass="style5">
                        <asp:ListItem Text="Y" Value="Y" />
                        <asp:ListItem Text="N" Value="N" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Save24.gif' border='0'&gt;">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" CssClass="style5" />
            </asp:ButtonColumn>
        </Columns>
        <ItemStyle VerticalAlign="Top" CssClass="style5" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
    </asp:DataGrid>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
