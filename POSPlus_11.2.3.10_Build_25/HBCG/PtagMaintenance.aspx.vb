Imports HBCG_Utils
Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient


Partial Class PtagMaintenance
    Inherits POSBasePage
    '    Inherits System.Web.UI.Page

    Public Const gs_version As String = "Version 1.0"
    Dim footer_message As String
    Dim warning_msg As String
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of Product tag maintenance screen. 
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If IsPostBack = False Then
            disable_prtag_fields()
            disable_prtag_fr_fields()
            disable_dimension_fields()
            Dim startDateColumn As GridViewDataDateColumn = TryCast(GV_item_options.Columns("START_DATE"), GridViewDataDateColumn)
            Dim endDateColumn As GridViewDataDateColumn = TryCast(GV_item_options.Columns("END_DATE"), GridViewDataDateColumn)
            startDateColumn.PropertiesDateEdit.MinDate = Date.Now.Date
            endDateColumn.PropertiesDateEdit.MinDate = Date.Now.Date
            ASPxPageControl1.ActiveTabIndex = 0

            'To build feature code combo box
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As String
            Dim ds As New DataSet

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            sqlString = New String("SELECT feat_cd, feat_cd || ' - ' || des as des from feature")

            objCmd.CommandText = sqlString
            objCmd.Connection = conn

            Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

            Try
                dataAdapter.Fill(ds)
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
            For Each row As DataRow In ds.Tables(0).Rows
                CType(GV_feature_benefits.Columns("FEAT_CD"), GridViewDataComboBoxColumn).PropertiesComboBox.Items.Add(row.ItemArray(1).ToString(), row.ItemArray(0).ToString())
            Next
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : PRTAG Tab Search button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click
        Dim retValidItmSecurityCheck As String

        lbl_warning.Text = ""
        footer_message = ""
        txt_itm_cd.Text = txt_itm_cd.Text.ToUpper()
        retValidItmSecurityCheck = IsValidItemSecurityCheck(txt_itm_cd.Text.ToUpper())

        If retValidItmSecurityCheck <> "VALID" Then
            lbl_warning.Text = retValidItmSecurityCheck
            Return
        End If

        populate_prtag()
        populate_prtag_fr()
        populate_itm_options()
        populate_itm_features()
        populate_Item_Dimensions()

        txt_itm_cd.Enabled = False
        btn_search.Enabled = False
        btn_clear.Enabled = True


    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : PRTAG Tab Save button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder
        Dim ds As New DataSet

        txt_itm_cd.Text = txt_itm_cd.Text.ToUpper()
        txt_origin.Text = txt_origin.Text
        txt_stackable.Text = txt_stackable.Text.ToUpper()
        txt_packaging.Text = txt_packaging.Text.ToUpper()

        Validate_Prtag_Fields()

        If String.IsNullOrEmpty(lbl_warning_prtag.Text) = False Then

            lbl_warning_prtag.Text = "Please correct the following errors <br/>" + lbl_warning_prtag.Text
            Return
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If IsNothing(Session("prtag_itm_cd")) Then
            sqlString = New StringBuilder("INSERT INTO prtag ")
            sqlString.Append("(itm_cd, origin, mtrl1, mtrl2, wrnty1, wrnty2, care1, care2, prot1, prot2, expln_a1, expln_a2, expln_b1,")
            sqlString.Append("expln_b2, expln_c1, udate, siz, packaged, weight, stackable, packaging, comment_1, comment_2, comment_3, comment_4) VALUES")
            sqlString.Append("(:p_itm_cd, :p_origin, :p_mtrl1, :p_mtrl2, :p_wrnty1, :p_wrnty2, :p_care1, :p_care2, :p_prot1, :p_prot2, :p_expln_a1, :p_expln_a2, :p_expln_b1,")
            sqlString.Append(":p_expln_b2, :p_expln_c1, :p_udate, :p_siz, :p_packaged, :p_weight, :p_stackable, :p_packaging, :p_comments1, :p_comments2, :p_comments3, :p_comments4)")

            objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)

            objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

            objCmd.Parameters.Add(":p_origin", OracleType.VarChar)
            objCmd.Parameters(":p_origin").Value = txt_origin.Text()

            objCmd.Parameters.Add(":p_mtrl1", OracleType.VarChar)
            objCmd.Parameters(":p_mtrl1").Value = txt_material1.Text()

            objCmd.Parameters.Add(":p_mtrl2", OracleType.VarChar)
            objCmd.Parameters(":p_mtrl2").Value = txt_material2.Text()

            objCmd.Parameters.Add(":p_wrnty1", OracleType.VarChar)
            objCmd.Parameters(":p_wrnty1").Value = txt_warranty1.Text()

            objCmd.Parameters.Add(":p_wrnty2", OracleType.VarChar)
            objCmd.Parameters(":p_wrnty2").Value = txt_warranty2.Text()

            objCmd.Parameters.Add(":p_care1", OracleType.VarChar)
            objCmd.Parameters(":p_care1").Value = txt_care1.Text()

            objCmd.Parameters.Add(":p_care2", OracleType.VarChar)
            objCmd.Parameters(":p_care2").Value = txt_care2.Text()

            objCmd.Parameters.Add(":p_prot1", OracleType.VarChar)
            objCmd.Parameters(":p_prot1").Value = txt_protection1.Text.ToUpper()

            objCmd.Parameters.Add(":p_prot2", OracleType.VarChar)
            objCmd.Parameters(":p_prot2").Value = txt_protection2.Text.ToUpper()

            objCmd.Parameters.Add(":p_expln_a1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_a1").Value = txt_features1.Text()

            objCmd.Parameters.Add(":p_expln_a2", OracleType.VarChar)
            objCmd.Parameters(":p_expln_a2").Value = txt_features2.Text()

            objCmd.Parameters.Add(":p_expln_b1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_b1").Value = txt_features3.Text()

            objCmd.Parameters.Add(":p_expln_b2", OracleType.VarChar)
            objCmd.Parameters(":p_expln_b2").Value = txt_features4.Text()

            objCmd.Parameters.Add(":p_expln_c1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_c1").Value = txt_features5.Text()

            objCmd.Parameters.Add(":p_udate", OracleType.DateTime)
            If String.IsNullOrEmpty(dt_date.Text) Then
                objCmd.Parameters(":p_udate").Value = DBNull.Value
            Else
                objCmd.Parameters(":p_udate").Value = dt_date.Text
            End If

            objCmd.Parameters.Add(":p_siz", OracleType.VarChar)
            objCmd.Parameters(":p_siz").Value = txt_size.Text

            objCmd.Parameters.Add(":p_packaged", OracleType.VarChar)
            objCmd.Parameters(":p_packaged").Value = txt_vol.Text

            objCmd.Parameters.Add(":p_weight", OracleType.VarChar)
            objCmd.Parameters(":p_weight").Value = txt_weight.Text

            objCmd.Parameters.Add(":p_stackable", OracleType.VarChar)
            objCmd.Parameters(":p_stackable").Value = txt_stackable.Text.ToUpper()

            objCmd.Parameters.Add(":p_packaging", OracleType.VarChar)
            objCmd.Parameters(":p_packaging").Value = txt_packaging.Text.ToUpper()

            objCmd.Parameters.Add(":p_comments1", OracleType.VarChar)
            objCmd.Parameters(":p_comments1").Value = txt_comments1.Text

            objCmd.Parameters.Add(":p_comments2", OracleType.VarChar)
            objCmd.Parameters(":p_comments2").Value = txt_comments2.Text

            objCmd.Parameters.Add(":p_comments3", OracleType.VarChar)
            objCmd.Parameters(":p_comments3").Value = txt_comments3.Text

            objCmd.Parameters.Add(":p_comments4", OracleType.VarChar)
            objCmd.Parameters(":p_comments4").Value = txt_comments4.Text
            Try
                conn.Open()
                objCmd.ExecuteNonQuery()
                conn.Close()
                Session("prtag_itm_cd") = txt_itm_cd.Text
                lbl_warning_prtag.Text = "Insert completed successfully"
            Catch ex As Exception
                conn.Close()
                lbl_warning_prtag.Text = "ERRORS WERE ENCOUNTERED!"
            End Try

        Else
            sqlString = New StringBuilder("UPDATE prtag SET ")

            sqlString.Append("origin = :p_origin ,")
            sqlString.Append("mtrl1 = :p_material1 ,")
            sqlString.Append("mtrl2 = :p_material2 ,")
            sqlString.Append("wrnty1 = :p_warranty1 ,")
            sqlString.Append("wrnty2 = :p_warranty2 ,")
            sqlString.Append("care1 = :p_care1 ,")
            sqlString.Append("care2 = :p_care2 ,")
            sqlString.Append("prot1 = :p_protection1 ,")
            sqlString.Append("prot2 = :p_protection2 ,")
            sqlString.Append("expln_a1 = :p_features1 ,")
            sqlString.Append("expln_a2 = :p_features2 ,")
            sqlString.Append("expln_b1 = :p_features3 ,")
            sqlString.Append("expln_b2 = :p_features4 ,")
            sqlString.Append("expln_c1 = :p_features5 ,")
            sqlString.Append("udate = :p_udate ,")
            sqlString.Append("siz = :p_siz ,")
            sqlString.Append("packaged = :p_vol ,")
            sqlString.Append("weight = :p_weight ,")
            sqlString.Append("stackable = :p_stackable ,")
            sqlString.Append("packaging = :p_packaging ,")
            sqlString.Append("comment_1 = :p_comments1 ,")
            sqlString.Append("comment_2 = :p_comments2 ,")
            sqlString.Append("comment_3 = :p_comments3 ,")
            sqlString.Append("comment_4 = :p_comments4  ")
            sqlString.Append("WHERE itm_cd = :p_itm_cd ")

            objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)

            objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

            objCmd.Parameters.Add(":p_origin", OracleType.VarChar)
            objCmd.Parameters(":p_origin").Value = txt_origin.Text

            objCmd.Parameters.Add(":p_material1", OracleType.VarChar)
            objCmd.Parameters(":p_material1").Value = txt_material1.Text

            objCmd.Parameters.Add(":p_material2", OracleType.VarChar)
            objCmd.Parameters(":p_material2").Value = txt_material2.Text

            objCmd.Parameters.Add(":p_warranty1", OracleType.VarChar)
            objCmd.Parameters(":p_warranty1").Value = txt_warranty1.Text

            objCmd.Parameters.Add(":p_warranty2", OracleType.VarChar)
            objCmd.Parameters(":p_warranty2").Value = txt_warranty2.Text

            objCmd.Parameters.Add(":p_care1", OracleType.VarChar)
            objCmd.Parameters(":p_care1").Value = txt_care1.Text

            objCmd.Parameters.Add(":p_care2", OracleType.VarChar)
            objCmd.Parameters(":p_care2").Value = txt_care2.Text

            objCmd.Parameters.Add(":p_protection1", OracleType.VarChar)
            objCmd.Parameters(":p_protection1").Value = txt_protection1.Text.ToUpper()

            objCmd.Parameters.Add(":p_protection2", OracleType.VarChar)
            objCmd.Parameters(":p_protection2").Value = txt_protection2.Text.ToUpper()

            objCmd.Parameters.Add(":p_features1", OracleType.VarChar)
            objCmd.Parameters(":p_features1").Value = txt_features1.Text

            objCmd.Parameters.Add(":p_features2", OracleType.VarChar)
            objCmd.Parameters(":p_features2").Value = txt_features2.Text

            objCmd.Parameters.Add(":p_features3", OracleType.VarChar)
            objCmd.Parameters(":p_features3").Value = txt_features3.Text

            objCmd.Parameters.Add(":p_features4", OracleType.VarChar)
            objCmd.Parameters(":p_features4").Value = txt_features4.Text

            objCmd.Parameters.Add(":p_features5", OracleType.VarChar)
            objCmd.Parameters(":p_features5").Value = txt_features5.Text

            objCmd.Parameters.Add(":p_udate", OracleType.DateTime)
            If String.IsNullOrEmpty(dt_date.Text) Then
                objCmd.Parameters(":p_udate").Value = DBNull.Value
            Else
                objCmd.Parameters(":p_udate").Value = dt_date.Text
            End If

            objCmd.Parameters.Add(":p_siz", OracleType.VarChar)
            objCmd.Parameters(":p_siz").Value = txt_size.Text

            objCmd.Parameters.Add(":p_vol", OracleType.VarChar)
            objCmd.Parameters(":p_vol").Value = txt_vol.Text

            objCmd.Parameters.Add(":p_weight", OracleType.VarChar)
            objCmd.Parameters(":p_weight").Value = txt_weight.Text

            objCmd.Parameters.Add(":p_stackable", OracleType.VarChar)
            objCmd.Parameters(":p_stackable").Value = txt_stackable.Text.ToUpper()

            objCmd.Parameters.Add(":p_packaging", OracleType.VarChar)
            objCmd.Parameters(":p_packaging").Value = txt_packaging.Text.ToUpper()

            objCmd.Parameters.Add(":p_comments1", OracleType.VarChar)
            objCmd.Parameters(":p_comments1").Value = txt_comments1.Text

            objCmd.Parameters.Add(":p_comments2", OracleType.VarChar)
            objCmd.Parameters(":p_comments2").Value = txt_comments2.Text

            objCmd.Parameters.Add(":p_comments3", OracleType.VarChar)
            objCmd.Parameters(":p_comments3").Value = txt_comments3.Text

            objCmd.Parameters.Add(":p_comments4", OracleType.VarChar)
            objCmd.Parameters(":p_comments4").Value = txt_comments4.Text
            Try
                conn.Open()
                objCmd.ExecuteNonQuery()
                conn.Close()
                lbl_warning_prtag.Text = "Update completed successfully"
            Catch ex As Exception
                conn.Close()
                lbl_warning_prtag.Text = "ERRORS WERE ENCOUNTERED!"
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : PRTAG French Tab Save button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_save_fr_Click(sender As Object, e As EventArgs) Handles btn_save_fr.Click
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder
        Dim ds As New DataSet

        txt_itm_cd.Text = txt_itm_cd.Text.ToUpper()
        txt_origin_fr.Text = txt_origin_fr.Text.ToUpper()
        txt_stackable_fr.Text = txt_stackable_fr.Text.ToUpper()
        txt_packaging_fr.Text = txt_packaging_fr.Text.ToUpper()

        Validate_Prtag_fr_Fields()

        If String.IsNullOrEmpty(lbl_warning_prtag_fr.Text) = False Then
            lbl_warning_prtag_fr.Text = "Please correct the following errors <br/>" + lbl_warning_prtag_fr.Text
            Return
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If IsNothing(Session("prtag_fr_itm_cd")) Then
            sqlString = New StringBuilder("INSERT INTO prtag_fr ")
            sqlString.Append("(itm_cd, origin, mtrl1, mtrl2, wrnty1, wrnty2, care1, care2, prot1, prot2, expln_a1, expln_a2, expln_b1,")
            sqlString.Append("expln_b2, expln_c1, udate, siz, packaged, weight, stackable, packaging, comment_1, comment_2, comment_3, comment_4) VALUES")
            sqlString.Append("(UPPER(:p_itm_cd), UPPER(:p_origin), UPPER(:p_mtrl1), UPPER(:p_mtrl2), UPPER(:p_wrnty1), UPPER(:p_wrnty2), UPPER(:p_care1), UPPER(:p_care2), UPPER(:p_prot1), UPPER(:p_prot2), UPPER(:p_expln_a1), UPPER(:p_expln_a2), UPPER(:p_expln_b1), ")
            sqlString.Append(" UPPER(:p_expln_b2), UPPER(:p_expln_c1), :p_udate, UPPER(:p_siz), UPPER(:p_packaged), UPPER(:p_weight), UPPER(:p_stackable), UPPER(:p_packaging), UPPER(:p_comments1), UPPER(:p_comments2), UPPER(:p_comments3), UPPER(:p_comments4))")

            objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)

            objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text

            objCmd.Parameters.Add(":p_origin", OracleType.VarChar)
            objCmd.Parameters(":p_origin").Value = txt_origin_fr.Text

            objCmd.Parameters.Add(":p_mtrl1", OracleType.VarChar)
            objCmd.Parameters(":p_mtrl1").Value = txt_material1_fr.Text

            objCmd.Parameters.Add(":p_mtrl2", OracleType.VarChar)
            objCmd.Parameters(":p_mtrl2").Value = txt_material2_fr.Text

            objCmd.Parameters.Add(":p_wrnty1", OracleType.VarChar)
            objCmd.Parameters(":p_wrnty1").Value = txt_warranty1_fr.Text

            objCmd.Parameters.Add(":p_wrnty2", OracleType.VarChar)
            objCmd.Parameters(":p_wrnty2").Value = txt_warranty2_fr.Text

            objCmd.Parameters.Add(":p_care1", OracleType.VarChar)
            objCmd.Parameters(":p_care1").Value = txt_care1_fr.Text

            objCmd.Parameters.Add(":p_care2", OracleType.VarChar)
            objCmd.Parameters(":p_care2").Value = txt_care2_fr.Text

            objCmd.Parameters.Add(":p_prot1", OracleType.VarChar)
            objCmd.Parameters(":p_prot1").Value = txt_protection1_fr.Text

            objCmd.Parameters.Add(":p_prot2", OracleType.VarChar)
            objCmd.Parameters(":p_prot2").Value = txt_protection2_fr.Text

            objCmd.Parameters.Add(":p_expln_a1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_a1").Value = txt_features1_fr.Text

            objCmd.Parameters.Add(":p_expln_a2", OracleType.VarChar)
            objCmd.Parameters(":p_expln_a2").Value = txt_features2_fr.Text

            objCmd.Parameters.Add(":p_expln_b1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_b1").Value = txt_features3_fr.Text

            objCmd.Parameters.Add(":p_expln_b2", OracleType.VarChar)
            objCmd.Parameters(":p_expln_b2").Value = txt_features4_fr.Text

            objCmd.Parameters.Add(":p_expln_c1", OracleType.VarChar)
            objCmd.Parameters(":p_expln_c1").Value = txt_features5_fr.Text

            objCmd.Parameters.Add(":p_udate", OracleType.DateTime)
            If String.IsNullOrEmpty(dt_date_fr.Text) Then
                objCmd.Parameters(":p_udate").Value = DBNull.Value
            Else
                objCmd.Parameters(":p_udate").Value = dt_date_fr.Text
            End If

            objCmd.Parameters.Add(":p_siz", OracleType.VarChar)
            objCmd.Parameters(":p_siz").Value = txt_size_fr.Text

            objCmd.Parameters.Add(":p_packaged", OracleType.VarChar)
            objCmd.Parameters(":p_packaged").Value = txt_vol_fr.Text

            objCmd.Parameters.Add(":p_weight", OracleType.VarChar)
            objCmd.Parameters(":p_weight").Value = txt_weight_fr.Text

            objCmd.Parameters.Add(":p_stackable", OracleType.VarChar)
            objCmd.Parameters(":p_stackable").Value = txt_stackable_fr.Text

            objCmd.Parameters.Add(":p_packaging", OracleType.VarChar)
            objCmd.Parameters(":p_packaging").Value = txt_packaging_fr.Text

            objCmd.Parameters.Add(":p_comments1", OracleType.VarChar)
            objCmd.Parameters(":p_comments1").Value = txt_comments1_fr.Text

            objCmd.Parameters.Add(":p_comments2", OracleType.VarChar)
            objCmd.Parameters(":p_comments2").Value = txt_comments2_fr.Text

            objCmd.Parameters.Add(":p_comments3", OracleType.VarChar)
            objCmd.Parameters(":p_comments3").Value = txt_comments3_fr.Text

            objCmd.Parameters.Add(":p_comments4", OracleType.VarChar)
            objCmd.Parameters(":p_comments4").Value = txt_comments4_fr.Text
            Try
                conn.Open()
                objCmd.ExecuteNonQuery()
                conn.Close()
                Session("prtag_fr_itm_cd") = txt_itm_cd.Text
                lbl_warning_prtag_fr.Text = "Insert completed successfully"
            Catch ex As Exception
                conn.Close()
                lbl_warning_prtag_fr.Text = "ERRORS WERE ENCOUNTERED!"
            End Try

        Else
            sqlString = New StringBuilder("UPDATE prtag_fr SET ")
            sqlString.Append("origin = UPPER(:p_origin),")
            sqlString.Append("mtrl1 = UPPER(:p_material1),")
            sqlString.Append("mtrl2 = UPPER(:p_material2),")
            sqlString.Append("wrnty1 = UPPER(:p_warranty1),")
            sqlString.Append("wrnty2 = UPPER(:p_warranty2),")
            sqlString.Append("care1 = UPPER(:p_care1),")
            sqlString.Append("care2 = UPPER(:p_care2),")
            sqlString.Append("prot1 = UPPER(:p_protection1),")
            sqlString.Append("prot2 = UPPER(:p_protection2),")
            sqlString.Append("expln_a1 = UPPER(:p_features1),")
            sqlString.Append("expln_a2 = UPPER(:p_features2),")
            sqlString.Append("expln_b1 = UPPER(:p_features3),")
            sqlString.Append("expln_b2 = UPPER(:p_features4),")
            sqlString.Append("expln_c1 = UPPER(:p_features5),")
            sqlString.Append("udate =:p_udate,")
            sqlString.Append("siz =:p_siz,")
            sqlString.Append("packaged =:p_vol,")
            sqlString.Append("weight =:p_weight,")
            sqlString.Append("stackable = UPPER(:p_stackable),")
            sqlString.Append("packaging = UPPER(:p_packaging),")
            sqlString.Append("comment_1 = UPPER(:p_comments1),")
            sqlString.Append("comment_2 = UPPER(:p_comments2),")
            sqlString.Append("comment_3 = UPPER(:p_comments3),")
            sqlString.Append("comment_4 = UPPER(:p_comments4) ")
            sqlString.Append("WHERE itm_cd =:p_itm_cd ")

            objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)

            objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

            objCmd.Parameters.Add(":p_origin", OracleType.VarChar)
            objCmd.Parameters(":p_origin").Value = txt_origin_fr.Text

            objCmd.Parameters.Add(":p_material1", OracleType.VarChar)
            objCmd.Parameters(":p_material1").Value = txt_material1_fr.Text

            objCmd.Parameters.Add(":p_material2", OracleType.VarChar)
            objCmd.Parameters(":p_material2").Value = txt_material2_fr.Text

            objCmd.Parameters.Add(":p_warranty1", OracleType.VarChar)
            objCmd.Parameters(":p_warranty1").Value = txt_warranty1_fr.Text

            objCmd.Parameters.Add(":p_warranty2", OracleType.VarChar)
            objCmd.Parameters(":p_warranty2").Value = txt_warranty2_fr.Text

            objCmd.Parameters.Add(":p_care1", OracleType.VarChar)
            objCmd.Parameters(":p_care1").Value = txt_care1_fr.Text.ToUpper()

            objCmd.Parameters.Add(":p_care2", OracleType.VarChar)
            objCmd.Parameters(":p_care2").Value = txt_care2_fr.Text

            objCmd.Parameters.Add(":p_protection1", OracleType.VarChar)
            objCmd.Parameters(":p_protection1").Value = txt_protection1_fr.Text

            objCmd.Parameters.Add(":p_protection2", OracleType.VarChar)
            objCmd.Parameters(":p_protection2").Value = txt_protection2_fr.Text

            objCmd.Parameters.Add(":p_features1", OracleType.VarChar)
            objCmd.Parameters(":p_features1").Value = txt_features1_fr.Text

            objCmd.Parameters.Add(":p_features2", OracleType.VarChar)
            objCmd.Parameters(":p_features2").Value = txt_features2_fr.Text

            objCmd.Parameters.Add(":p_features3", OracleType.VarChar)
            objCmd.Parameters(":p_features3").Value = txt_features3_fr.Text

            objCmd.Parameters.Add(":p_features4", OracleType.VarChar)
            objCmd.Parameters(":p_features4").Value = txt_features4_fr.Text

            objCmd.Parameters.Add(":p_features5", OracleType.VarChar)
            objCmd.Parameters(":p_features5").Value = txt_features5_fr.Text

            objCmd.Parameters.Add(":p_udate", OracleType.DateTime)
            If String.IsNullOrEmpty(dt_date_fr.Text) Then
                objCmd.Parameters(":p_udate").Value = DBNull.Value
            Else
                objCmd.Parameters(":p_udate").Value = dt_date_fr.Text
            End If

            objCmd.Parameters.Add(":p_siz", OracleType.VarChar)
            objCmd.Parameters(":p_siz").Value = txt_size_fr.Text

            objCmd.Parameters.Add(":p_vol", OracleType.VarChar)
            objCmd.Parameters(":p_vol").Value = txt_vol_fr.Text

            objCmd.Parameters.Add(":p_weight", OracleType.VarChar)
            objCmd.Parameters(":p_weight").Value = txt_weight_fr.Text

            objCmd.Parameters.Add(":p_stackable", OracleType.VarChar)
            objCmd.Parameters(":p_stackable").Value = txt_stackable_fr.Text

            objCmd.Parameters.Add(":p_packaging", OracleType.VarChar)
            objCmd.Parameters(":p_packaging").Value = txt_packaging_fr.Text

            objCmd.Parameters.Add(":p_comments1", OracleType.VarChar)
            objCmd.Parameters(":p_comments1").Value = txt_comments1_fr.Text

            objCmd.Parameters.Add(":p_comments2", OracleType.VarChar)
            objCmd.Parameters(":p_comments2").Value = txt_comments2_fr.Text

            objCmd.Parameters.Add(":p_comments3", OracleType.VarChar)
            objCmd.Parameters(":p_comments3").Value = txt_comments3_fr.Text

            objCmd.Parameters.Add(":p_comments4", OracleType.VarChar)
            objCmd.Parameters(":p_comments4").Value = txt_comments4_fr.Text
            Try
                conn.Open()
                objCmd.ExecuteNonQuery()
                conn.Close()
                lbl_warning_prtag_fr.Text = "Update completed successfully"
            Catch ex As Exception
                conn.Close()
                lbl_warning_prtag_fr.Text = "ERRORS WERE ENCOUNTERED!"
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : PRTAG Tab Clear button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        disable_prtag_fields()
        disable_prtag_fr_fields()
        disable_dimension_fields()
        clear_Item_options()
        clear_Item_features()
        clear_Item_Dimensions()
        lbl_warning_prtag.Text = ""
        lbl_warning_prtag_fr.Text = ""
        lbl_warning_dim.Text = ""


        txt_itm_cd.Enabled = True
        btn_search.Enabled = True
        btn_clear.Enabled = False

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : To Validate PRTAG Tab fields.
    ''' Called By      : btn_save_Click
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub Validate_Prtag_Fields()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String
        Dim objValidate As Object
        Dim retValidItmSecurityCheck As String

        lbl_warning_prtag.Text = ""

        'Item Code Validation  
        retValidItmSecurityCheck = IsValidItemSecurityCheck(txt_itm_cd.Text.ToUpper())
        If retValidItmSecurityCheck <> "VALID" Then
            lbl_warning_prtag.Text = retValidItmSecurityCheck
            Return
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()
        If Left(Session("CO_CD"), 1) = "B" Then 'it#: 4504, lucy, 06-Mar-19, a brick store
            'Warranty Code Validation        
            If String.IsNullOrEmpty(txt_protection1.Text) = False Then
                sqlString = "SELECT itm_cd FROM itm WHERE itm_tp_cd = 'WAR' AND itm_cd = :p_protection"

                objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
                objCmd.CommandType = CommandType.Text
                objCmd.CommandText = sqlString
                objCmd.Parameters.Clear()
                objCmd.Parameters.Add(":p_protection", OracleType.VarChar)
                objCmd.Parameters(":p_protection").Value = txt_protection1.Text.ToUpper()

                objValidate = objCmd.ExecuteScalar()
                If objValidate = Nothing Then
                    lbl_warning_prtag.Text = lbl_warning_prtag.Text + "<br/>" + "Warranty Code " + txt_protection1.Text.ToUpper() + " does not exists"
                End If
            End If
            If String.IsNullOrEmpty(txt_protection2.Text) = False Then
                sqlString = "SELECT itm_cd FROM itm WHERE itm_tp_cd = 'WAR' AND itm_cd = :p_protection"

                objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
                objCmd.CommandText = sqlString
                objCmd.Parameters.Clear()
                objCmd.Parameters.Add(":p_protection", OracleType.VarChar)
                objCmd.Parameters(":p_protection").Value = txt_protection2.Text.ToUpper()

                objValidate = objCmd.ExecuteScalar()
                If objValidate = Nothing Then
                    lbl_warning_prtag.Text = lbl_warning_prtag.Text + "<br/>" + "Warranty Code " + txt_protection2.Text.ToUpper() + " does not exists"
                End If
            End If
        End If 'it#: 4504, lucy, 06-Mar-19
        conn.Close()
        'Vol/Cu FT Validation        
        If String.IsNullOrEmpty(txt_vol.Text) = False Then
            If IsNumeric(txt_vol.Text) = False Then
                lbl_warning_prtag.Text = lbl_warning_prtag.Text + "<br/>" + "Vol/Cu FT Must be Numeric "
            End If
        End If

        'Weight Validation        
        If String.IsNullOrEmpty(txt_weight.Text) = False Then
            If IsNumeric(txt_weight.Text) = False Then
                lbl_warning_prtag.Text = lbl_warning_prtag.Text + "<br/>" + "Weight Must be Numeric "
            End If
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Validate PRTAG French Tab fields.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub Validate_Prtag_fr_Fields()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String
        Dim objValidate As Object
        Dim retValidItmSecurityCheck As String

        lbl_warning_prtag_fr.Text = ""

        'Item Code Validation  
        retValidItmSecurityCheck = IsValidItemSecurityCheck(txt_itm_cd.Text.ToUpper())
        If retValidItmSecurityCheck <> "VALID" Then
            lbl_warning_prtag.Text = retValidItmSecurityCheck
            Return
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'Warranty Code Validation        
        If String.IsNullOrEmpty(txt_protection1_fr.Text) = False Then
            sqlString = "SELECT itm_cd FROM itm WHERE itm_tp_cd = 'WAR' AND itm_cd = :p_protection"

            objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = sqlString
            objCmd.Parameters.Clear()
            objCmd.Parameters.Add(":p_protection", OracleType.VarChar)
            objCmd.Parameters(":p_protection").Value = txt_protection1_fr.Text.ToUpper()

            objValidate = objCmd.ExecuteScalar()
            If objValidate = Nothing Then
                lbl_warning_prtag_fr.Text = lbl_warning_prtag_fr.Text + "<br/>" + "Warranty Code " + txt_protection1_fr.Text.ToUpper() + " does not exists"
            End If
        End If
        If String.IsNullOrEmpty(txt_protection2_fr.Text) = False Then
            sqlString = "SELECT itm_cd FROM itm WHERE itm_tp_cd = 'WAR' AND itm_cd = :p_protection"

            objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
            objCmd.CommandText = sqlString
            objCmd.Parameters.Clear()
            objCmd.Parameters.Add(":p_protection", OracleType.VarChar)
            objCmd.Parameters(":p_protection").Value = txt_protection2_fr.Text.ToUpper()

            objValidate = objCmd.ExecuteScalar()
            If objValidate = Nothing Then
                lbl_warning_prtag_fr.Text = lbl_warning_prtag_fr.Text + "<br/>" + "Warranty Code " + txt_protection2_fr.Text.ToUpper() + " does not exists"
            End If
        End If
        conn.Close()
        'Vol/Cu FT Validation        
        If String.IsNullOrEmpty(txt_vol_fr.Text) = False Then
            If IsNumeric(txt_vol_fr.Text) = False Then
                lbl_warning_prtag_fr.Text = lbl_warning_prtag_fr.Text + "<br/>" + "Vol/Cu FT Must be Numeric "
            End If
        End If

        'Weight Validation        
        If String.IsNullOrEmpty(txt_weight_fr.Text) = False Then
            If IsNumeric(txt_weight_fr.Text) = False Then
                lbl_warning_prtag_fr.Text = lbl_warning_prtag_fr.Text + "<br/>" + "Weight Must be Numeric "
            End If
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : To Enable required PRTAG tab field for update
    ''' Called By      : btn_search_Click
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub enable_tab1_fields()
        txt_origin.Enabled = True
        dt_date.Enabled = True
        txt_size.Enabled = True
        txt_vol.Enabled = True
        txt_weight.Enabled = True
        txt_packaging.Enabled = True
        txt_stackable.Enabled = True

        txt_material1.Enabled = True
        txt_material2.Enabled = True

        txt_warranty1.Enabled = True
        txt_warranty2.Enabled = True

        txt_care1.Enabled = True
        txt_care2.Enabled = True

        txt_protection1.Enabled = True
        txt_protection2.Enabled = True

        txt_features1.Enabled = True
        txt_features2.Enabled = True
        txt_features3.Enabled = True
        txt_features4.Enabled = True
        txt_features5.Enabled = True

        txt_comments1.Enabled = True
        txt_comments2.Enabled = True
        txt_comments3.Enabled = True
        txt_comments4.Enabled = True

        btn_save.Enabled = True
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Enable required PRTAG French tab field for update
    ''' Called By      : btn_search_Click
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub enable_tab2_fields_fr()
        txt_origin_fr.Enabled = True
        dt_date_fr.Enabled = True
        txt_size_fr.Enabled = True
        txt_vol_fr.Enabled = True
        txt_weight_fr.Enabled = True
        txt_packaging_fr.Enabled = True
        txt_stackable_fr.Enabled = True

        txt_material1_fr.Enabled = True
        txt_material2_fr.Enabled = True

        txt_warranty1_fr.Enabled = True
        txt_warranty2_fr.Enabled = True

        txt_care1_fr.Enabled = True
        txt_care2_fr.Enabled = True

        txt_protection1_fr.Enabled = True
        txt_protection2_fr.Enabled = True

        txt_features1_fr.Enabled = True
        txt_features2_fr.Enabled = True
        txt_features3_fr.Enabled = True
        txt_features4_fr.Enabled = True
        txt_features5_fr.Enabled = True

        txt_comments1_fr.Enabled = True
        txt_comments2_fr.Enabled = True
        txt_comments3_fr.Enabled = True
        txt_comments4_fr.Enabled = True

        btn_save_fr.Enabled = True
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : To Disable PRTAG tab field and to Enable item code field and search button
    ''' Called By      : Page_Init, btn_clear_Click
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub disable_prtag_fields()
        txt_origin.Enabled = False
        dt_date.Enabled = False
        txt_size.Enabled = False
        txt_vol.Enabled = False
        txt_weight.Enabled = False
        txt_packaging.Enabled = False
        txt_stackable.Enabled = False

        txt_material1.Enabled = False
        txt_material2.Enabled = False

        txt_warranty1.Enabled = False
        txt_warranty2.Enabled = False

        txt_care1.Enabled = False
        txt_care2.Enabled = False

        txt_protection1.Enabled = False
        txt_protection2.Enabled = False

        txt_features1.Enabled = False
        txt_features2.Enabled = False
        txt_features3.Enabled = False
        txt_features4.Enabled = False
        txt_features5.Enabled = False

        txt_comments1.Enabled = False
        txt_comments2.Enabled = False
        txt_comments3.Enabled = False
        txt_comments4.Enabled = False

        btn_save.Enabled = False
        btn_clear.Enabled = False

        txt_itm_cd.Enabled = True
        btn_search.Enabled = True

        txt_itm_cd.Text = ""

        txt_origin.Text = ""
        dt_date.Text = ""
        txt_size.Text = ""
        txt_vol.Text = ""
        txt_weight.Text = ""
        txt_packaging.Text = ""
        txt_stackable.Text = ""

        txt_material1.Text = ""
        txt_material2.Text = ""

        txt_warranty1.Text = ""
        txt_warranty2.Text = ""

        txt_care1.Text = ""
        txt_care2.Text = ""

        txt_protection1.Text = ""
        txt_protection2.Text = ""

        txt_features1.Text = ""
        txt_features2.Text = ""
        txt_features3.Text = ""
        txt_features4.Text = ""
        txt_features5.Text = ""

        txt_comments1.Text = ""
        txt_comments2.Text = ""
        txt_comments3.Text = ""
        txt_comments4.Text = ""

        Session("prtag_itm_cd") = Nothing
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Apr 2015
    ''' Description    : To Disable PRTAG french tab field and to Enable item code field and search button
    ''' Called By      : Page_Init, btn_clear_Click
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub disable_prtag_fr_fields()
        txt_origin_fr.Enabled = False
        dt_date_fr.Enabled = False
        txt_size_fr.Enabled = False
        txt_vol_fr.Enabled = False
        txt_weight_fr.Enabled = False
        txt_packaging_fr.Enabled = False
        txt_stackable_fr.Enabled = False

        txt_material1_fr.Enabled = False
        txt_material2_fr.Enabled = False

        txt_warranty1_fr.Enabled = False
        txt_warranty2_fr.Enabled = False

        txt_care1_fr.Enabled = False
        txt_care2_fr.Enabled = False

        txt_protection1_fr.Enabled = False
        txt_protection2_fr.Enabled = False

        txt_features1_fr.Enabled = False
        txt_features2_fr.Enabled = False
        txt_features3_fr.Enabled = False
        txt_features4_fr.Enabled = False
        txt_features5_fr.Enabled = False

        txt_comments1_fr.Enabled = False
        txt_comments2_fr.Enabled = False
        txt_comments3_fr.Enabled = False
        txt_comments4_fr.Enabled = False

        btn_save_fr.Enabled = False

        txt_origin_fr.Text = ""
        dt_date_fr.Text = ""
        txt_size_fr.Text = ""
        txt_vol_fr.Text = ""
        txt_weight_fr.Text = ""
        txt_packaging_fr.Text = ""
        txt_stackable_fr.Text = ""

        txt_material1_fr.Text = ""
        txt_material2_fr.Text = ""

        txt_warranty1_fr.Text = ""
        txt_warranty2_fr.Text = ""

        txt_care1_fr.Text = ""
        txt_care2_fr.Text = ""

        txt_protection1_fr.Text = ""
        txt_protection2_fr.Text = ""

        txt_features1_fr.Text = ""
        txt_features2_fr.Text = ""
        txt_features3_fr.Text = ""
        txt_features4_fr.Text = ""
        txt_features5_fr.Text = ""

        txt_comments1_fr.Text = ""
        txt_comments2_fr.Text = ""
        txt_comments3_fr.Text = ""
        txt_comments4_fr.Text = ""

        Session("prtag_fr_itm_cd") = Nothing

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : ITEM OPTIONS Grid Row Update Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_row_update(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder
        Dim objResult As Object

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        Dim enumerator As IDictionaryEnumerator = e.NewValues.GetEnumerator()
        Dim startDate As Date = Convert.ToDateTime(e.NewValues("START_DATE"))
        Dim endDate As Date

        If String.IsNullOrEmpty(e.NewValues("QUANTITY")) Then
            Throw New Exception("Quantity must be entered")
        ElseIf IsNumeric(e.NewValues("QUANTITY").ToString()) = False Then
            Throw New Exception("Quantity must be numeric")
        ElseIf Convert.ToInt16(e.NewValues("QUANTITY")) <= 0 Then
            Throw New Exception("Quantity must be greater than Zero")
        End If

        If String.IsNullOrEmpty(e.NewValues("END_DATE")) Then
            endDate = New Date(2049, 12, 31)
        Else
            endDate = Convert.ToDateTime(e.NewValues("END_DATE"))
        End If

        If endDate < Date.Now.Date Then
            Throw New Exception("End Date Must be in future")
        End If

        If startDate > endDate Then
            Throw New Exception("End Date Must be later than Start Date")
        End If

        sqlString = New StringBuilder("SELECT itm_cd FROM itm2itm_options WHERE itm_cd = :p_itm_cd")
        sqlString.Append(" AND related_itm_cd = :p_related_itm_cd")
        sqlString.Append(" AND related_type = :p_related_type")
        sqlString.Append(" AND ((TO_DATE(:p_start_date, 'YYYYMMDD') BETWEEN start_date AND end_date) ")
        sqlString.Append(" OR  (TO_DATE(:p_end_date, 'YYYYMMDD') between start_date and end_date)")
        sqlString.Append(" OR  (start_date between TO_DATE(:p_start_date, 'YYYYMMDD') and TO_DATE(:p_end_date, 'YYYYMMDD'))")
        sqlString.Append(" OR  (end_date between TO_DATE(:p_start_date, 'YYYYMMDD') and TO_DATE(:p_end_date, 'YYYYMMDD')))")
        sqlString.Append(" and rowid <> :p_rowid ")

        objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)
        objCmd.CommandText = sqlString.ToString()

        objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper

        objCmd.Parameters.Add(":p_related_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_related_itm_cd").Value = e.NewValues("RELATED_ITM_CD").ToString()

        objCmd.Parameters.Add(":p_related_type", OracleType.VarChar)
        objCmd.Parameters(":p_related_type").Value = e.NewValues("RELATED_TYPE").ToString()

        objCmd.Parameters.Add(":p_start_date", OracleType.VarChar)
        objCmd.Parameters(":p_start_date").Value = startDate.ToString("yyyyMMdd")

        objCmd.Parameters.Add(":p_end_date", OracleType.VarChar)
        objCmd.Parameters(":p_end_date").Value = endDate.ToString("yyyyMMdd")

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        conn.Open()
        objResult = objCmd.ExecuteScalar()
        If objResult <> Nothing Then
            conn.Close()
            Throw New Exception("Given date range overlaps with other date range")
        End If

        sqlString = New StringBuilder("UPDATE itm2itm_options SET ")
        sqlString.Append("quantity = :p_quantity, ")
        sqlString.Append("end_date = TO_DATE(:p_end_date, 'YYYYMMDD') ")
        sqlString.Append("WHERE rowid = :p_rowid ")

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_quantity", OracleType.VarChar)
        objCmd.Parameters(":p_quantity").Value = e.NewValues("QUANTITY").ToString()

        objCmd.Parameters.Add(":p_end_date", OracleType.VarChar)
        objCmd.Parameters(":p_end_date").Value = endDate.ToString("yyyyMMdd")

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Data updated successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_item_options.CancelEdit()
        e.Cancel = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : ITEM OPTIONS Grid Row Insert Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_row_insert(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder
        Dim objResult As Object
        Dim seqNum As Integer

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim enumerator As IDictionaryEnumerator = e.NewValues.GetEnumerator()
        Dim startDate As Date
        Dim endDate As Date

        If String.IsNullOrEmpty(e.NewValues("RELATED_TYPE")) Then
            Throw New Exception("Relation type must be selected")
        End If

        If String.IsNullOrEmpty(e.NewValues("QUANTITY")) Then
            Throw New Exception("Quantity must be entered")
        ElseIf IsNumeric(e.NewValues("QUANTITY").ToString()) = False Then
            Throw New Exception("Quantity must be numeric")
        ElseIf Convert.ToInt16(e.NewValues("QUANTITY")) <= 0 Then
            Throw New Exception("Quantity must be greater than Zero")
        End If


        If String.IsNullOrEmpty(e.NewValues("RELATED_ITM_CD")) Then
            Throw New Exception("Relation item must be entered")
        End If

        If String.IsNullOrEmpty(e.NewValues("START_DATE")) Then
            startDate = Date.Now.AddDays(1)
        Else
            startDate = Convert.ToDateTime(e.NewValues("START_DATE"))
        End If

        If String.IsNullOrEmpty(e.NewValues("END_DATE")) Then
            endDate = New Date(2049, 12, 31)
        Else
            endDate = Convert.ToDateTime(e.NewValues("END_DATE"))
        End If

        If startDate > endDate Then
            Throw New Exception("End Date Must be later than Start Date")
        End If

        conn.Open()

        If e.NewValues("RELATED_TYPE").ToString().Equals("WTY") Or e.NewValues("RELATED_TYPE").ToString().Equals("VTY") Then
            sqlString = New StringBuilder("select itm_cd from itm where itm_tp_cd = 'WAR' and itm_cd = :p_related_itm_cd")
        ElseIf e.NewValues("RELATED_TYPE").ToString().Equals("VAP") Then
            sqlString = New StringBuilder("select itm_cd from itm where itm_tp_cd = 'LAB' and itm_cd = :p_related_itm_cd")
        Else
            sqlString = New StringBuilder("select itm_cd from itm where itm_tp_cd in ('INV', 'PKG') and itm_cd = :p_related_itm_cd")
        End If

        objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)
        objCmd.CommandType = CommandType.Text
        objCmd.CommandText = sqlString.ToString()

        objCmd.Parameters.Add(":p_related_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_related_itm_cd").Value = e.NewValues("RELATED_ITM_CD").ToString().ToUpper()

        objResult = objCmd.ExecuteScalar()
        If objResult = Nothing Then
            conn.Close()
            Throw New Exception("Invalid Related Item code")
        End If

        sqlString = New StringBuilder("SELECT itm_cd FROM itm2itm_options WHERE itm_cd = :p_itm_cd")
        sqlString.Append(" AND related_itm_cd = :p_related_itm_cd")
        sqlString.Append(" AND related_type = :p_related_type")
        sqlString.Append(" AND ((TO_DATE(:p_start_date, 'YYYYMMDD') BETWEEN start_date AND end_date) ")
        sqlString.Append(" OR  (TO_DATE(:p_end_date, 'YYYYMMDD') between start_date and end_date)")
        sqlString.Append(" OR  (start_date between TO_DATE(:p_start_date, 'YYYYMMDD') and TO_DATE(:p_end_date, 'YYYYMMDD'))")
        sqlString.Append(" OR  (end_date between TO_DATE(:p_start_date, 'YYYYMMDD') and TO_DATE(:p_end_date, 'YYYYMMDD')))")


        objCmd.CommandText = sqlString.ToString()
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper

        objCmd.Parameters.Add(":p_related_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_related_itm_cd").Value = e.NewValues("RELATED_ITM_CD").ToString().ToUpper

        objCmd.Parameters.Add(":p_related_type", OracleType.VarChar)
        objCmd.Parameters(":p_related_type").Value = e.NewValues("RELATED_TYPE").ToString()

        objCmd.Parameters.Add(":p_start_date", OracleType.VarChar)
        objCmd.Parameters(":p_start_date").Value = startDate.ToString("yyyyMMdd")

        objCmd.Parameters.Add(":p_end_date", OracleType.VarChar)
        objCmd.Parameters(":p_end_date").Value = endDate.ToString("yyyyMMdd")

        objResult = objCmd.ExecuteScalar()
        If objResult <> Nothing Then
            conn.Close()
            Throw New Exception("Given date range overlaps with an existing date range")
        End If

        sqlString = New StringBuilder("SELECT MAX(seq#) FROM itm2itm_options WHERE itm_cd = :p_itm_cd")

        objCmd.CommandText = sqlString.ToString()
        objCmd.Parameters.Clear()
        objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()
        objResult = objCmd.ExecuteScalar()

        If IsDBNull(objResult) Then
            seqNum = 1
        Else
            seqNum = CType(objResult, Integer) + 1
        End If

        sqlString = New StringBuilder("INSERT INTO itm2itm_options (itm_cd, related_itm_cd, related_type, seq#, quantity, start_date, end_date) VALUES (")
        sqlString.Append(":p_itm_cd, :p_related_itm_cd, :p_related_type, :p_seq_num, :p_quantity, TO_DATE(:p_start_date, 'YYYYMMDD'), TO_DATE(:p_end_date, 'YYYYMMDD'))")

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        objCmd.Parameters.Add(":p_related_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_related_itm_cd").Value = e.NewValues("RELATED_ITM_CD").ToString().ToUpper()

        objCmd.Parameters.Add(":p_related_type", OracleType.VarChar)
        objCmd.Parameters(":p_related_type").Value = e.NewValues("RELATED_TYPE").ToString().ToUpper()

        objCmd.Parameters.Add(":p_seq_num", OracleType.VarChar)
        objCmd.Parameters(":p_seq_num").Value = seqNum.ToString()

        objCmd.Parameters.Add(":p_quantity", OracleType.VarChar)
        objCmd.Parameters(":p_quantity").Value = e.NewValues("QUANTITY").ToString()

        objCmd.Parameters.Add(":p_start_date", OracleType.VarChar)
        objCmd.Parameters(":p_start_date").Value = startDate.ToString("yyyyMMdd")

        objCmd.Parameters.Add(":p_end_date", OracleType.VarChar)
        objCmd.Parameters(":p_end_date").Value = endDate.ToString("yyyyMMdd")

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Data Added successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_item_options.CancelEdit()
        e.Cancel = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : ITEM OPTIONS Grid Row New/Edit Event Handler. Enable only required columns for edit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GVCellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        If GV_item_options.IsNewRowEditing Then
            Return
        End If
        If e.Column.FieldName = "RELATED_TYPE" Then
            e.Editor.ReadOnly = True
        End If
        If e.Column.FieldName = "RELATED_ITM_CD" Then
            e.Editor.ReadOnly = True
        End If
        If e.Column.FieldName = "START_DATE" Then
            e.Editor.ReadOnly = True
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : To Bind Item Options Grid whenever required
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GV_item_option_DataBind()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sqlString As String
        Dim objcmd As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = "select rowid, related_itm_cd, quantity, start_date, end_date, related_type  from itm2itm_options where itm_cd=:p_itm_cd order by start_date"

        Dim ds As New DataSet
        objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)

        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper

        Dim objAdaptor As OracleDataAdapter
        objAdaptor = DisposablesManager.BuildOracleDataAdapter(objcmd)

        conn.Open()
        objAdaptor.Fill(ds)
        GV_item_options.DataSource = ds
        conn.Close()
        GV_item_options.Enabled = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : GV_item_options Tab Clear 
    ''' </summary>
    Protected Sub clear_Item_options()

        GV_item_options.CancelEdit()
        GV_item_options.DataSource = Nothing
        GV_item_options.DataBind()
        GV_item_options.Enabled = False

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Bind ITEM FEATURE BENEFITS Grid whenever required
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GV_feature_benefits_DataBind()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sqlString As String
        Dim objcmd As OracleCommand
        Dim ds As New DataSet

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = "SELECT itm_cd, feat_cd, rowid FROM itm2feature WHERE itm_cd=:p_itm_cd ORDER BY itm_cd "

        objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)

        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        Dim objAdaptor As OracleDataAdapter
        objAdaptor = DisposablesManager.BuildOracleDataAdapter(objcmd)

        conn.Open()
        objAdaptor.Fill(ds)
        GV_feature_benefits.DataSource = ds
        conn.Close()
        GV_feature_benefits.Enabled = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : ITEM FEATURE BENEFITS Grid Row Update Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_fb_row_update(sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim objResult As Object

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If IsNothing(e.NewValues("FEAT_CD")) Then
            Throw New Exception("Feature Code must be entered")
        End If

        conn.Open()

        sqlString = New StringBuilder("UPDATE itm2feature SET ")
        sqlString.Append("itm_cd = :p_itm_cd, ")
        sqlString.Append("feat_cd = :p_feat_cd ")
        sqlString.Append("WHERE rowid = :p_rowid ")

        objcmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)
        objcmd.CommandText = sqlString.ToString()
        objcmd.CommandType = CommandType.Text
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = e.NewValues("ITM_CD").ToString().ToUpper()

        objcmd.Parameters.Add(":p_feat_cd", OracleType.VarChar)
        objcmd.Parameters(":p_feat_cd").Value = e.NewValues("FEAT_CD").ToString().ToUpper()

        objcmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objcmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objcmd.ExecuteNonQuery()
            footer_message = "Feature Code updated successfully"
            conn.Close()

        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_feature_benefits.CancelEdit()
        e.Cancel = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : ITEM FEATURE BENEFITS Grid Row Insert Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_fb_row_insert(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        lbl_warning.Text = ""

        If String.IsNullOrEmpty(e.NewValues("FEAT_CD")) Then
            Throw New Exception("Features code must be entered")
        End If

        conn.Open()

        sqlString = New StringBuilder("INSERT INTO itm2feature (ITM_CD, FEAT_CD) VALUES ( ")
        sqlString.Append(":p_itm_cd, :p_feat_cd) ")

        objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objCmd.Parameters(":p_itm_cd").Value = e.NewValues("ITM_CD").ToString().ToUpper()

        objCmd.Parameters.Add(":p_feat_cd", OracleType.VarChar)
        objCmd.Parameters(":p_feat_cd").Value = e.NewValues("FEAT_CD").ToString().ToUpper()

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Feature Code Added successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_feature_benefits.CancelEdit()
        e.Cancel = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : ITEM FEATURE BENEFITS Grid Row Insert Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_fb_row_delete(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        conn.Open()

        sqlString = New StringBuilder("DELETE FROM itm2feature WHERE ROWID = :p_rowid")

        objCmd = DisposablesManager.BuildOracleCommand(sqlString.ToString, conn)
        objCmd.CommandType = CommandType.Text
        objCmd.CommandText = sqlString.ToString()
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Feature Code deleted successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_feature_benefits.CancelEdit()
        e.Cancel = True

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : ITEM FEATURE BENEFITS Grid Row New/Edit Event Handler. Enable only required columns for edit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GVFBCellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        If e.Column.FieldName = "FEAT_CD" Then
            e.Editor.ReadOnly = False
        End If
        If e.Column.FieldName = "ITM_CD" Then
            e.Editor.ReadOnly = True
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : Procedure which validates the item is accessible for the given user.
    ''' </summary>
    ''' <param name="p_itemCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsValidItemSecurityCheck_old(ByVal p_itemCode As String) As String
        ' lucy rename it to old
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String

        If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
            Return "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security."
        End If

        Dim retValidItm As String

        lbl_warning.Text = ""

        'Item Code Validation  
        retValidItm = IsValidItem(p_itemCode)
        If retValidItm <> "VALID" Then
            Return retValidItm
        End If

        Dim s_employeeCode = Session("EMP_CD")
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn.Open()

        'we use isValidItm2() so we can pass in the EMP_CD which determines the EMP_INIT which is required for isValidItm()
        sqlString = "STD_MULTI_CO.isValidItm2"
        objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objCmd.CommandType = CommandType.StoredProcedure

        objCmd.Parameters.Add("p_empCd", OracleType.VarChar).Direction = ParameterDirection.Input
        objCmd.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
        objCmd.Parameters.Add("v_returnValue", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

        objCmd.Parameters("p_empCd").Value = s_employeeCode.ToString()
        objCmd.Parameters("p_itm").Value = p_itemCode

        objCmd.ExecuteNonQuery()
        Dim RetVal = objCmd.Parameters("v_returnValue").Value
        conn.Close()
        If RetVal = "Y" Then
            Return "VALID"
        Else
            Return "ERROR: Security Violation for item '" + p_itemCode + "' and employee '" + s_employeeCode.ToString() + "'"
        End If

    End Function

    Private Function IsValidItemSecurityCheck(ByVal p_itemCode As String) As String
        ' lucy copy it from IsValidItemSecurityCheck_old
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String

        If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
            Return "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security."
        End If

        Dim retValidItm As String

        lbl_warning.Text = ""

        'Item Code Validation  
        retValidItm = IsValidItem(p_itemCode)
        If retValidItm <> "VALID" Then
            Return retValidItm
        End If

        Dim s_employeeCode = Session("EMP_CD")
        Dim s_emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn.Open()

        'we use isValidItm2() so we can pass in the EMP_CD which determines the EMP_INIT which is required for isValidItm()
        sqlString = "STD_MULTI_CO.isValidItmForUpdate"
        objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objCmd.CommandType = CommandType.StoredProcedure

        objCmd.Parameters.Add("p_empinit", OracleType.VarChar).Direction = ParameterDirection.Input
        objCmd.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
        objCmd.Parameters.Add("v_returnValue", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

        objCmd.Parameters("p_empinit").Value = s_emp_init.ToString()
        objCmd.Parameters("p_itm").Value = p_itemCode

        objCmd.ExecuteNonQuery()
        Dim RetVal = objCmd.Parameters("v_returnValue").Value
        conn.Close()
        If RetVal = "Y" Then
            Return "VALID"
        Else
            Return "ERROR: Security Violation for item '" + p_itemCode + "' and employee '" + s_emp_init.ToString() + "'"
        End If

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : ITEM FEATURE BENEFITS Tab Clear button click event
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub clear_Item_features()

        GV_feature_benefits.DataSource = Nothing
        GV_feature_benefits.CancelEdit()
        GV_feature_benefits.DataBind()
        GV_feature_benefits.Enabled = False

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : May 2015
    ''' Description    : Procedure which validates the item is valid or not
    ''' </summary>
    ''' <param name="p_itemCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsValidItem(ByVal p_itemCode As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String
        Dim objValidate As Object

        If String.IsNullOrEmpty(p_itemCode) Then
            Return "ERROR: Item Code must be entered"
        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sqlString = "SELECT itm_cd FROM itm WHERE itm_cd = UPPER(:p_item_cd)"
        objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objCmd.CommandText = sqlString

        objCmd.Parameters.Add(":p_item_cd", OracleType.VarChar)
        objCmd.Parameters(":p_item_cd").Value = p_itemCode


        objValidate = objCmd.ExecuteScalar()
        If objValidate = Nothing Then
            conn.Close()
            Return "ERROR: Item '" + p_itemCode + "' does not exist"
        End If
        conn.Close()
        Return "VALID"

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To popluate data in PRTAB
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populate_prtag()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String
        Dim ds As New DataSet

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        sqlString = "SELECT * FROM prtag WHERE itm_cd = UPPER(:p_item_cd)"

        objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objCmd.Parameters.Add(":p_item_cd", OracleType.VarChar)
        objCmd.Parameters(":p_item_cd").Value = txt_itm_cd.Text

        Dim dataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)
        dataAdapter.Fill(ds)
        conn.Close()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            txt_origin.Text = dr("ORIGIN").ToString
            dt_date.Value = dr("UDATE")

            txt_material1.Text = dr("MTRL1").ToString
            txt_material2.Text = dr("MTRL2").ToString

            txt_vol.Text = dr("PACKAGED").ToString
            txt_size.Text = dr("SIZ").ToString
            txt_weight.Text = dr("WEIGHT").ToString
            txt_packaging.Text = dr("PACKAGING").ToString
            txt_stackable.Text = dr("STACKABLE").ToString

            txt_warranty1.Text = dr("WRNTY1").ToString
            txt_warranty2.Text = dr("WRNTY2").ToString
            txt_care1.Text = dr("CARE1").ToString
            txt_care2.Text = dr("CARE2").ToString
            txt_protection1.Text = dr("PROT1").ToString
            txt_protection2.Text = dr("PROT2").ToString

            txt_features1.Text = dr("EXPLN_A1").ToString
            txt_features2.Text = dr("EXPLN_A2").ToString
            txt_features3.Text = dr("EXPLN_B1").ToString
            txt_features4.Text = dr("EXPLN_B2").ToString
            txt_features5.Text = dr("EXPLN_C1").ToString

            txt_comments1.Text = dr("COMMENT_1").ToString
            txt_comments2.Text = dr("COMMENT_2").ToString
            txt_comments3.Text = dr("COMMENT_3").ToString
            txt_comments4.Text = dr("COMMENT_4").ToString
            Session("prtag_itm_cd") = txt_itm_cd.Text
        End If
        enable_tab1_fields()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To populate the data in PRTAG French tab
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populate_prtag_fr()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As String
        Dim ds As New DataSet

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        sqlString = "SELECT * FROM prtag_fr WHERE itm_cd = UPPER(:p_item_cd)"
        objCmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objCmd.CommandText = sqlString
        objCmd.Connection = conn
        objCmd.Parameters.Add(":p_item_cd", OracleType.VarChar)
        objCmd.Parameters(":p_item_cd").Value = txt_itm_cd.Text

        Dim dataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)
        dataAdapter.Fill(ds)
        conn.Close()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            txt_origin_fr.Text = dr("ORIGIN").ToString
            dt_date_fr.Value = dr("UDATE")

            txt_material1_fr.Text = dr("MTRL1").ToString
            txt_material2_fr.Text = dr("MTRL2").ToString

            txt_vol_fr.Text = dr("PACKAGED").ToString
            txt_size_fr.Text = dr("SIZ").ToString
            txt_weight_fr.Text = dr("WEIGHT").ToString
            txt_packaging_fr.Text = dr("PACKAGING").ToString
            txt_stackable_fr.Text = dr("STACKABLE").ToString

            txt_warranty1_fr.Text = dr("WRNTY1").ToString
            txt_warranty2_fr.Text = dr("WRNTY2").ToString
            txt_care1_fr.Text = dr("CARE1").ToString
            txt_care2_fr.Text = dr("CARE2").ToString
            txt_protection1_fr.Text = dr("PROT1").ToString
            txt_protection2_fr.Text = dr("PROT2").ToString

            txt_features1_fr.Text = dr("EXPLN_A1").ToString
            txt_features2_fr.Text = dr("EXPLN_A2").ToString
            txt_features3_fr.Text = dr("EXPLN_B1").ToString
            txt_features4_fr.Text = dr("EXPLN_B2").ToString
            txt_features5_fr.Text = dr("EXPLN_C1").ToString

            txt_comments1_fr.Text = dr("COMMENT_1").ToString
            txt_comments2_fr.Text = dr("COMMENT_2").ToString
            txt_comments3_fr.Text = dr("COMMENT_3").ToString
            txt_comments4_fr.Text = dr("COMMENT_4").ToString
            Session("prtag_fr_itm_cd") = txt_itm_cd.Text
        End If
        enable_tab2_fields_fr()

    End Sub

    Protected Sub populate_itm_options()

        GV_item_options.PageIndex = 1
        GV_item_options.DataBind()

    End Sub

    Protected Sub populate_itm_features()

        GV_feature_benefits.PageIndex = 1
        GV_feature_benefits.DataBind()

    End Sub
    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Display ITEM DIMENSIONS whenever required
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populate_Item_Dimensions()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sqlString As String
        Dim objcmd As OracleCommand
        Dim Mydatareader As OracleDataReader
        Dim objResult As Object

        txt_dim_cover.Enabled = True
        txt_dim_grade.Enabled = True
        txt_dim_size.Enabled = True
        chk_NPT.Enabled = True

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sqlString = "SELECT siz, siz_id, cover, cover_id, grade, grade_id FROM itm WHERE itm_cd= :p_itm_cd "

        objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)

        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objcmd)

            If Mydatareader.Read() Then
                txt_dim_size.Text = Mydatareader.Item("siz").ToString
                txt_dim_cover.Text = Mydatareader.Item("cover").ToString
                txt_dim_grade.Text = Mydatareader.Item("grade").ToString
                txt_dim_size_id.Text = Mydatareader.Item("siz_id").ToString
                txt_dim_cover_id.Text = Mydatareader.Item("cover_id").ToString
                txt_dim_grade_id.Text = Mydatareader.Item("grade_id").ToString
            End If
        Catch
            Throw
            conn.Close()
        End Try

        sqlString = "SELECT ITM_CD, ITM_SRT_CD FROM itm$itm_srt WHERE ITM_CD= :p_itm_cd AND itm_srt_cd='NPT' "

        objcmd.CommandText = sqlString.ToString()
        objcmd.CommandType = CommandType.Text
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        objResult = objcmd.ExecuteScalar()
        If objResult <> Nothing Then
            chk_NPT.CheckState = 0
        End If
        enable_dimension_fields()
        conn.Close()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Update Item Dimensions whenever required
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub change_update_NPT_tag()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sqlString As String
        Dim sqlstrbuilder As StringBuilder
        Dim objcmd As OracleCommand
        Dim v_itm_srt_cd As Integer
        Dim objResult As Object

        v_itm_srt_cd = 0

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sqlString = "SELECT ITM_CD, ITM_SRT_CD FROM itm$itm_srt WHERE ITM_CD= :p_itm_cd AND itm_srt_cd='NPT' "

        objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
        objcmd.CommandText = sqlString.ToString()
        objcmd.CommandType = CommandType.Text
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        objResult = objcmd.ExecuteScalar()
        If objResult <> Nothing Then
            v_itm_srt_cd = 1
        End If

        If (chk_NPT.CheckState = 0 And v_itm_srt_cd = 0) Then

            sqlString = "INSERT INTO itm$itm_srt (ITM_CD, ITM_SRT_CD) VALUES (:p_itm_cd, :p_npt) "

            objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
            objcmd.Parameters.Clear()

            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

            objcmd.Parameters.Add(":p_npt", OracleType.VarChar)
            objcmd.Parameters(":p_npt").Value = "NPT"

            objcmd.ExecuteNonQuery()

        ElseIf (chk_NPT.CheckState = 1 And v_itm_srt_cd = 1) Then

            sqlString = "DELETE FROM itm$itm_srt WHERE ITM_CD=:p_itm_cd AND ITM_SRT_CD=:p_npt "

            objcmd = DisposablesManager.BuildOracleCommand(sqlString, conn)
            objcmd.Parameters.Clear()

            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
            objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

            objcmd.Parameters.Add(":p_npt", OracleType.VarChar)
            objcmd.Parameters(":p_npt").Value = "NPT"

            objcmd.ExecuteNonQuery()

        End If

        sqlstrbuilder = New StringBuilder("UPDATE itm SET ")
        sqlstrbuilder.Append("siz =:p_siz , ")
        sqlstrbuilder.Append("siz_id =:p_siz_id , ")
        sqlstrbuilder.Append("cover =:p_cover, ")
        sqlstrbuilder.Append("cover_id =:p_cover_id, ")
        sqlstrbuilder.Append("grade =:p_grade, ")
        sqlstrbuilder.Append("grade_id =:p_grade_id ")
        sqlstrbuilder.Append("WHERE itm_cd =:p_itm_cd ")

        objcmd.CommandText = sqlstrbuilder.ToString()
        objcmd.CommandType = CommandType.Text
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":p_itm_cd", OracleType.VarChar)
        objcmd.Parameters(":p_itm_cd").Value = txt_itm_cd.Text.ToUpper()

        objcmd.Parameters.Add(":p_siz", OracleType.VarChar)
        objcmd.Parameters(":p_siz").Value = txt_dim_size.Text.ToUpper()

        objcmd.Parameters.Add(":p_cover", OracleType.VarChar)
        objcmd.Parameters(":p_cover").Value = txt_dim_cover.Text.ToUpper()

        objcmd.Parameters.Add(":p_grade", OracleType.VarChar)
        objcmd.Parameters(":p_grade").Value = txt_dim_grade.Text.ToUpper()

        objcmd.Parameters.Add(":p_siz_id", OracleType.VarChar)
        objcmd.Parameters(":p_siz_id").Value = txt_dim_size_id.Text.ToUpper()

        objcmd.Parameters.Add(":p_cover_id", OracleType.VarChar)
        objcmd.Parameters(":p_cover_id").Value = txt_dim_cover_id.Text.ToUpper()

        objcmd.Parameters.Add(":p_grade_id", OracleType.VarChar)
        objcmd.Parameters(":p_grade_id").Value = txt_dim_grade_id.Text.ToUpper()

        lbl_warning_dim.Text = "Data has been Updated successfully"

        objcmd.ExecuteNonQuery()

        conn.Close()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : DIMENSIONS Tab SAVE button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_dim_save(sender As Object, e As EventArgs) Handles btn_di_save.Click

        change_update_NPT_tag()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : ITEM DIMENSIONS Tab Clear button click event
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub clear_Item_Dimensions()

        txt_dim_cover.Text = ""
        txt_dim_grade.Text = ""
        txt_dim_size.Text = ""
        txt_dim_size_id.Text = ""
        txt_dim_cover_id.Text = ""
        txt_dim_grade_id.Text = ""
        chk_NPT.CheckState = 1
        txt_dim_cover.Enabled = False
        txt_dim_grade.Enabled = False
        txt_dim_size.Enabled = False
        chk_NPT.Enabled = False

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : DIMENSION Tab New Row click event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_feature_benefits_InitNewRow(sender As Object, e As DevExpress.Web.Data.ASPxDataInitNewRowEventArgs) Handles GV_feature_benefits.InitNewRow

        e.NewValues("ITM_CD") = txt_itm_cd.Text

    End Sub


    Protected Sub disable_dimension_fields()
        txt_dim_cover.Enabled = False
        txt_dim_cover_id.Enabled = False
        txt_dim_grade.Enabled = False
        txt_dim_grade_id.Enabled = False
        txt_dim_size.Enabled = False
        txt_dim_size_id.Enabled = False
        chk_NPT.Enabled = False
        btn_di_save.Enabled = False

    End Sub

    Protected Sub enable_dimension_fields()
        txt_dim_cover.Enabled = True
        txt_dim_cover_id.Enabled = True
        txt_dim_grade.Enabled = True
        txt_dim_grade_id.Enabled = True
        txt_dim_size.Enabled = True
        txt_dim_size_id.Enabled = True
        chk_NPT.Enabled = True
        btn_di_save.Enabled = True

    End Sub

    Protected Sub GV_Footer_label_init(sender As Object, e As EventArgs)
        Dim gv_footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        gv_footer_label.Text = footer_message
    End Sub
    Protected Sub GV_Footer_label_init_io(sender As Object, e As EventArgs)
        Dim gv_footer_label_io As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        gv_footer_label_io.Text = footer_message
    End Sub

End Class