<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Relationship_Calendar.aspx.vb" Inherits="Relationship_Calendar" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dxwschs" %>
<%@ Register Assembly="DevExpress.XtraScheduler.v13.2.Core, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraScheduler" TagPrefix="cc2" %>
<%@ Register Namespace="DataControls" Assembly="DataCalendar" TagPrefix="dc" %>
<%@ Register Assembly="DataCalendar" Namespace="DataControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" border="0">
        <tr>
            <td align="right">
                <dx:ASPxComboBox ID="cbo_store" runat="server" AutoPostBack="true" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
                <dx:ASPxComboBox ID="cbo_slsp" runat="server" AutoPostBack="true" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <dxwschs:ASPxScheduler ID="ASPxScheduler1" runat="server" AppointmentDataSourceID="ORACLEDB"
        OnAppointmentFormShowing="ASPxScheduler1_AppointmentFormShowing" GroupType="Resource"
        ClientInstanceName="scheduler" OnBeforeExecuteCallbackCommand="ASPxScheduler1_BeforeExecuteCallbackCommand"
        Start="2011-01-29" ActiveViewType="Month">
        <OptionsForms AppointmentFormTemplateUrl="Relationship_Calendar_Appointment_Form.ascx" />
        <Views>
            <DayView>
                <TimeRulers>
                    <cc2:TimeRuler>
                    </cc2:TimeRuler>
                </TimeRulers>
                <AppointmentDisplayOptions EndTimeVisibility="Never" />
            </DayView>
            <WorkWeekView>
                <TimeRulers>
                    <cc2:TimeRuler>
                    </cc2:TimeRuler>
                </TimeRulers>
                <AppointmentDisplayOptions EndTimeVisibility="Never" />
            </WorkWeekView>
            <WeekView>
                <AppointmentDisplayOptions EndTimeVisibility="Never" />
            </WeekView>
            <MonthView>
                <AppointmentDisplayOptions EndTimeVisibility="Never" />
            </MonthView>
            <TimelineView>
                <AppointmentDisplayOptions EndTimeVisibility="Never" />
            </TimelineView>
        </Views>
        <Storage EnableReminders="False">
            <Appointments>
                <Mappings AppointmentId="REL_NO" Description="COMMENTS" Location="HPHONE" Start="FOLLOW_UP_DT"
                    Subject="REL_NO" Type="CUST_CD" />
                <CustomFieldMappings>
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="PROSPECT_TOTAL" Name="Prospect Total"
                        ValueType="Decimal" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="CORP_NAME" Name="Corporation"
                        ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="FNAME" Name="First Name" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="ADDR1" Name="Address" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="CITY" Name="City" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="ST" Name="State" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="ZIP" Name="Zip" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="LNAME" Name="Last Name" ValueType="String" />
                    <dxwschs:ASPxAppointmentCustomFieldMapping Member="HPHONE" Name="Home Phone" ValueType="String" />
                </CustomFieldMappings>
            </Appointments>
        </Storage>
        <OptionsCustomization AllowAppointmentCopy="None" AllowAppointmentCreate="None" AllowAppointmentDelete="None"
            AllowAppointmentDrag="None" AllowAppointmentDragBetweenResources="None" AllowAppointmentMultiSelect="False"
            AllowAppointmentResize="None" AllowInplaceEditor="None" AllowAppointmentEdit="NonRecurring" />
        <OptionsBehavior ShowDetailedErrorInfo="False" ShowRemindersForm="False" />
    </dxwschs:ASPxScheduler>
    <asp:SqlDataSource ID="ORACLEDB" runat="server" ConnectionString="<%$ ConnectionStrings:ERP %>"
        ProviderName="<%$ ConnectionStrings:ERP.ProviderName %>" SelectCommand="SELECT Replace(REL_NO || ' ' || NVL(CORP_NAME,LNAME),'&quot;') AS REL_NO, STORE_CD, SLSP1, FOLLOW_UP_DT || ' ' || APPT_TIME AS FOLLOW_UP_DT, APPT_TIME, PROSPECT_TOTAL, REPLACE(CORP_NAME,'&quot;'), FNAME, LNAME, HPHONE, COMMENTS, ADDR1, ADDR2, CITY, ST, ZIP FROM RELATIONSHIP WHERE UPPER(SLSP1) LIKE UPPER(:SLSP1) AND REL_STATUS='O'">
        <SelectParameters>
            <asp:ControlParameter ControlID="cbo_slsp" PropertyName="Value" Name="SLSP1" Type="String">
            </asp:ControlParameter>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
