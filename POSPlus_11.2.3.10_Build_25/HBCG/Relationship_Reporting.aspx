<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Relationship_Reporting.aspx.vb"
    Inherits="Reports_Relationship_Reporting" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="main_body" runat="server">
        <table>
            <tr>
                <td style="width: 117px; text-align: right">
                    Appointment Type:
                </td>
                <td style="width: 481px">
                    <asp:DropDownList ID="cbo_ord_tp_cd" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="PHONE">By Phone</asp:ListItem>
                        <asp:ListItem Value="STORE">In Store</asp:ListItem>
                        <asp:ListItem Value="HOME">In Home</asp:ListItem>
                        <asp:ListItem Selected="True" Value="ALL">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; text-align: right">
                    Order Status:
                </td>
                <td style="width: 481px">
                    <asp:DropDownList ID="cbo_ord_stat" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="O">Open</asp:ListItem>
                        <asp:ListItem Value="C">Converted</asp:ListItem>
                        <asp:ListItem Selected="True" Value="B">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; text-align: right;">
                    Written Date:
                </td>
                <td valign="middle">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="txt_start_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                            <td class="style5">
                                &nbsp;through&nbsp;
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="txt_end_dt" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; text-align: right">
                    Pickup/Delivery:
                </td>
                <td style="width: 481px">
                    <asp:DropDownList ID="cbo_pd" runat="server" Width="285px" CssClass="style5">
                        <asp:ListItem Value="D">Delivery</asp:ListItem>
                        <asp:ListItem Value="P">Pickup</asp:ListItem>
                        <asp:ListItem Selected="True" Value="B">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; text-align: right">
                    Follow Up Date:
                </td>
                <td style="width: 481px">
                    <table>
                        <tr>
                            <td style="height: 41px">
                                <dx:ASPxDateEdit ID="txt_del_start" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                            <td class="style5" style="height: 41px">
                                &nbsp;through&nbsp;
                            </td>
                            <td style="height: 41px">
                                <dx:ASPxDateEdit ID="txt_del_end" runat="server">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 117px; text-align: right">
                    Sort By:
                </td>
                <td style="text-align: left; width: 481px;">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="WR_DT">Written Date</asp:ListItem>
                        <asp:ListItem Value="CONVERSION_DT">Converted Date</asp:ListItem>
                        <asp:ListItem Value="LNAME">Customer Last Name</asp:ListItem>
                        <asp:ListItem Value="APPT_TYPE">Appointment Type</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btn_submit" runat="server" Text="Run Report" Width="120px" CssClass="style5" />
                    &nbsp;
                    <input id="Reset1" style="width: 120px" type="reset" value="Reset Options" class="style5" />
                    &nbsp;
                    <asp:Button ID="btn_exit" runat="server" Text="Exit Report" Width="120px" CssClass="style5" />
                    <br />
                    <asp:Label ID="lbl_errors" runat="server" Text="" Width="285px" Font-Bold="true"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
