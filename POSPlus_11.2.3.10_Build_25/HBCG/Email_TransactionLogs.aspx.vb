Imports HBCG_Utils

Partial Class Email_TransactionLogs
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
                ASPxGridView1.Visible = False
                ASPxDateEdit1.Enabled = False
                ASPxDateEdit2.Enabled = False
                ASPxLabel2.Text = "You are not authorized to view this page"
            End If
            ASPxDateEdit1.Value = Now.Date.AddDays(-7)
            ASPxDateEdit2.Value = Now.Date
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
