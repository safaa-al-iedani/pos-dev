
Imports System.Data.OracleClient

Partial Class SalesOrderComments
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_REL_NO.Text = Request("DEL_DOC_NUM")
        If Not IsPostBack Then
            Get_Comments()
        End If
        
    End Sub

    Public Sub Get_Comments()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim DEL_DOC_NUM, cust_cd As String

        DEL_DOC_NUM = Request("del_doc_num")
        cust_cd = Request("cust_cd")
        txt_del_past_comments.Text = ""
        txt_del_comments.Text = ""
        txt_past_Comments.Text = ""
        txt_ar_past_comments.Text = ""
        If Not Session("scomments") Is Nothing Then
            txt_comments.Text = Session("scomments").ToString
        End If
        If Not Session("dcomments") Is Nothing Then
            txt_del_comments.Text = Session("dcomments").ToString
        End If
        If Not Session("arcomments") Is Nothing Then
            txt_ar_comments.Text = Session("arcomments").ToString
        End If

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & DEL_DOC_NUM & "'"
        sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
        sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='S' ORDER BY SEQ#"
        sql = UCase(sql)

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim curr_dt As String = ""
        Dim curr_tp As String = ""

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read Then
                curr_dt = MyDataReader.Item("DT").ToString
                curr_tp = MyDataReader.Item("CMNT_TYPE").ToString
                txt_past_Comments.Text = FormatDateTime(MyDataReader.Item("DT").ToString, DateFormat.ShortDate) & " (" & MyDataReader.Item("CMNT_TYPE").ToString & ") - "
                txt_past_Comments.Text = txt_past_Comments.Text & MyDataReader.Item("TEXT").ToString
            End If
            Do While (MyDataReader.Read())
                If curr_dt <> MyDataReader.Item("DT").ToString Or curr_tp <> MyDataReader.Item("cmnt_type").ToString Then
                    curr_tp = MyDataReader.Item("CMNT_TYPE").ToString
                    txt_past_Comments.Text = txt_past_Comments.Text & vbCrLf & FormatDateTime(MyDataReader.Item("DT").ToString, DateFormat.ShortDate) & " (" & MyDataReader.Item("CMNT_TYPE").ToString & ") - "
                    curr_dt = MyDataReader.Item("DT").ToString
                End If
                txt_past_Comments.Text = txt_past_Comments.Text & MyDataReader.Item("TEXT").ToString
            Loop
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        'txt_past_Comments.Enabled = False

        sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & DEL_DOC_NUM & "'"
        sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
        sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='D' ORDER BY SEQ#"
        sql = UCase(sql)

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        curr_dt = ""
        curr_tp = ""
        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read Then
                curr_dt = MyDataReader.Item("DT").ToString
                curr_tp = MyDataReader.Item("CMNT_TYPE").ToString
                txt_del_past_comments.Text = FormatDateTime(MyDataReader.Item("DT").ToString, DateFormat.ShortDate) & " (" & MyDataReader.Item("CMNT_TYPE").ToString & ") - "
                txt_del_past_comments.Text = txt_del_past_comments.Text & MyDataReader.Item("TEXT").ToString
            End If
            Do While (MyDataReader.Read())
                If curr_dt <> MyDataReader.Item("DT").ToString Or curr_tp <> MyDataReader.Item("cmnt_type").ToString Then
                    curr_tp = MyDataReader.Item("CMNT_TYPE").ToString
                    txt_del_past_comments.Text = txt_past_Comments.Text & vbCrLf & FormatDateTime(MyDataReader.Item("DT").ToString, DateFormat.ShortDate) & " (" & MyDataReader.Item("CMNT_TYPE").ToString & ") - "
                    curr_dt = MyDataReader.Item("DT").ToString
                End If
                txt_del_past_comments.Text = txt_del_past_comments.Text & MyDataReader.Item("TEXT").ToString
            Loop
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        'txt_del_past_comments.Enabled = False

        sql = "SELECT SEQ#, CMNT_DT, TEXT FROM CUST_CMNT WHERE CUST_CD='" & cust_cd & "' ORDER BY SEQ#"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        curr_dt = ""

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read Then
                curr_dt = MyDataReader.Item("CMNT_DT").ToString
                txt_ar_past_comments.Text = FormatDateTime(curr_dt, DateFormat.ShortDate) & " - " & txt_ar_past_comments.Text
                txt_ar_past_comments.Text = txt_ar_past_comments.Text & MyDataReader.Item("TEXT").ToString
            End If
            Do While (MyDataReader.Read())
                If curr_dt <> MyDataReader.Item("CMNT_DT").ToString Then
                    curr_dt = MyDataReader.Item("CMNT_DT").ToString
                    txt_ar_past_comments.Text = txt_ar_past_comments.Text & vbCrLf & FormatDateTime(MyDataReader.Item("CMNT_DT").ToString, DateFormat.ShortDate) & " - " & MyDataReader.Item("TEXT").ToString
                End If
                txt_ar_past_comments.Text = txt_ar_past_comments.Text & MyDataReader.Item("TEXT").ToString
            Loop
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        'txt_ar_past_comments.Enabled = False

        'Close Connection 
        conn.Close()

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        Response.Write("parentWindow.location.href='salesordermaintenance.aspx?query_returned=Y&del_doc_num=" & Request("del_doc_num") & "';" & vbCrLf)
        Response.Write("</script>")

    End Sub

    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click

        Session("scomments") = txt_comments.Text
        Session("dcomments") = txt_del_comments.Text
        Session("arcomments") = txt_ar_comments.Text

    End Sub

    Protected Sub btn_save2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save2.Click

        Session("scomments") = txt_comments.Text
        Session("dcomments") = txt_del_comments.Text
        Session("arcomments") = txt_ar_comments.Text

    End Sub

    Protected Sub btn_exit2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit2.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        Response.Write("parentWindow.location.href='salesordermaintenance.aspx?query_returned=Y&del_doc_num=" & Request("del_doc_num") & "';" & vbCrLf)
        Response.Write("</script>")

    End Sub
End Class
