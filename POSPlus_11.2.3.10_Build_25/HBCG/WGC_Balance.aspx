<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WGC_Balance.aspx.vb" Inherits="WGC_Balance"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div width="100%" align="center">
        <br />
        &nbsp;<br />
        <b>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label77 %>">
            </dx:ASPxLabel>
        </b>
        <table class="style5">
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label56 %>">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <%-- sabrina -add enabled=false--%>
                <td align="left">
                    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Swiped?" Visible="False">
                    </dx:ASPxLabel>
                </td>
                <td align="left">
                    <asp:TextBox ID="txt_swiped" runat="server" AutoPostBack="false" Enabled="false"
                        Width="10px" Visible="False">N</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label76 %>">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>">
                                </dx:ASPxButton>
                                 </td>
                            </td>                           
                             <%-- sabrina added --%>
                            <td>              
                                <dx:ASPxComboBox ID="printer_drp" runat="server" Width="150px" DropDownRows="5">               
                                 </dx:ASPxComboBox>
                            </td>   
                            <td>
                                <dx:ASPxButton ID="btnprint_rcpt" runat="server" Text="Print Balance" />
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_response" runat="server" Width="100%">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
