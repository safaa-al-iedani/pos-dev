Imports System.Data
Imports System.Data.OracleClient
Imports System.IO

Partial Class IPRTX
    Inherits POSBasePage
    Dim v_del_doc_num As String
    Dim v_ord_tp_cd As String
    Dim v_fin_cust_cd As String
    Dim v_promo_cd As String
    Dim v_promo_cd_FIN As String
    Dim v_promo_cd_WDR As String
    Dim v_co_cd As String
    Dim v_cust_cd As String
    Dim v_fname As String
    Dim v_lname As String
    Dim v_addr1 As String
    Dim v_addr2 As String
    Dim v_city As String
    Dim v_st_cd As String
    Dim v_zip_cd As String
    Dim v_assoc_name1 As String
    Dim v_assoc_name2 As String
    Dim v_orig_fi_amt As String
    Dim v_so_wr_dt As Date
    Dim v_fin_account_num As String
    Dim v_defer_fixed_date As String
    Dim v_defer_fixed_date_FIN As String
    Dim v_defer_fixed_date_WDR As String
    Dim v_defer_months_FIN As String
    Dim v_defer_months_WDR As String
    Dim v_defer_days_FIN As String
    Dim v_defer_days_WDR As String
    Dim v_ar_balance As String
    Dim v_deposit_amt As String
    Dim v_mop_cd As String

    Public Sub get_order_info()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "a.del_doc_num "
        sql = sql & ",a.ord_tp_cd "
        sql = sql & ",a.fin_cust_cd "
        sql = sql & ",a.orig_fi_amt "
        sql = sql & ",a.so_wr_dt "
        sql = sql & ",e.promo_cd "
        sql = sql & ",s.co_cd "
        sql = sql & ",c.cust_cd "
        sql = sql & ",c.fname "
        sql = sql & ",c.lname "
        sql = sql & ",c.addr1 "
        sql = sql & ",c.addr2 "
        sql = sql & ",c.city "
        sql = sql & ",c.st_cd "
        sql = sql & ",c.zip_cd "
        sql = sql & ",g.fname || ' ' || g.lname	assoc_name#1 "
        sql = sql & ",h.fname || ' ' || h.lname	assoc_name#2 "
        sql = sql & "from "
        sql = sql & "emp h "
        sql = sql & ",emp    g "
        sql = sql & ",store  s "
        sql = sql & ",so_asp e "
        sql = sql & ",cust   c "
        sql = sql & ",so     a "
        sql = sql & "where  a.del_doc_num     =  '" & v_del_doc_num & "' "
        sql = sql & "and  e.del_doc_num       =  '" & v_del_doc_num & "' "
        sql = sql & "and  a.cust_cd     || '' =  c.cust_cd     (+) "
        sql = sql & "and  a.so_store_cd || '' =  s.store_cd    (+) "
        sql = sql & "and  a.so_emp_slsp_cd1   =  g.emp_cd      (+) "
        sql = sql & "and  a.so_emp_slsp_cd2   =  h.emp_cd      (+) "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_del_doc_num = MyDataReader2.Item("del_doc_num").ToString
                v_ord_tp_cd = MyDataReader2.Item("ord_tp_cd").ToString
                v_fin_cust_cd = MyDataReader2.Item("fin_cust_cd").ToString
                v_co_cd = MyDataReader2.Item("co_cd").ToString
                v_promo_cd_FIN = MyDataReader2.Item("promo_cd").ToString
                v_promo_cd = MyDataReader2.Item("promo_cd").ToString
                v_cust_cd = MyDataReader2.Item("cust_cd").ToString
                v_fname = MyDataReader2.Item("fname").ToString
                v_lname = MyDataReader2.Item("lname").ToString
                v_addr1 = MyDataReader2.Item("addr1").ToString
                v_addr2 = MyDataReader2.Item("addr2").ToString
                v_city = MyDataReader2.Item("city").ToString
                v_st_cd = MyDataReader2.Item("st_cd").ToString
                v_zip_cd = MyDataReader2.Item("zip_cd").ToString
                v_assoc_name1 = MyDataReader2.Item("assoc_name#1").ToString
                v_assoc_name2 = MyDataReader2.Item("assoc_name#2").ToString
                v_orig_fi_amt = MyDataReader2.Item("orig_fi_amt").ToString
                v_so_wr_dt = MyDataReader2.Item("so_wr_dt").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_WDR_Promo_Code()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select  rtrim(substr(b.text,42,5))	WDR_promo_code "
        sql = sql & "from "
        sql = sql & "so_cmnt b "
        sql = sql & ",so      a "
        sql = sql & "where  a.del_doc_num       =  '" & v_del_doc_num & "' "
        sql = sql & "and  a.so_wr_dt     +0   =  b.so_wr_dt    (+) "
        sql = sql & "and  a.so_store_cd || '' =  b.so_store_cd (+) "
        sql = sql & "and  a.so_seq_num  || '' =  b.so_seq_num  (+) "
        sql = sql & "and  b.text           like  'Promotion-Code Assigned to WDR Deposit = %'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_promo_cd_WDR = MyDataReader2.Item("WDR_promo_code").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_fin_acct_num()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select a.bnk_crd_num	"
        sql = sql & "from ar_trn a "
        sql = sql & "where a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd    = '" & v_cust_cd & "' "
        sql = sql & "and a.ivc_cd     = '" & v_del_doc_num & "' "
        sql = sql & "and a.trn_tp_cd  = '" & v_ord_tp_cd & "' "
        sql = sql & "and     'SAL'    = '" & v_ord_tp_cd & "' "
        sql = sql & "union "
        sql = sql & "select a.bnk_crd_num "
        sql = sql & "from ar_trn a "
        sql = sql & "where a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd    = '" & v_cust_cd & "' "
        sql = sql & "and a.adj_ivc_cd =  '" & v_del_doc_num & "' "
        sql = sql & "and a.trn_tp_cd  =  '" & v_ord_tp_cd & "' "
        sql = sql & "and     'SAL'   <>  '" & v_ord_tp_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_fin_account_num = MyDataReader2.Item("bnk_crd_num").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_promo_info()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "to_number(substr(d.des,1,2)) defer_months "
        sql = sql & ",to_char(d.end_dt,'mm/dd/yyyy') defer_fixed_date "
        sql = sql & "from "
        sql = sql & "asp_promo d "
        sql = sql & "where d.as_cd = '" & v_fin_cust_cd & "' "
        sql = sql & "and d.promo_cd =  '" & v_promo_cd & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_defer_months_FIN = MyDataReader2.Item("defer_months").ToString
                v_defer_fixed_date = MyDataReader2.Item("defer_fixed_date").ToString
                v_defer_fixed_date_FIN = v_defer_fixed_date
                v_defer_days_FIN = v_defer_months_FIN * 30
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_promo_info_WDR()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "to_number(substr(d.des,1,2)) defer_months "
        sql = sql & ",to_char(d.end_dt,'mm/dd/yyyy') defer_fixed_date "
        sql = sql & "from "
        sql = sql & "asp_promo d "
        sql = sql & "where d.as_cd = '" & v_fin_cust_cd & "' "
        sql = sql & "and d.promo_cd =  '" & v_promo_cd_WDR & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_defer_months_WDR = MyDataReader2.Item("defer_months").ToString
                v_defer_fixed_date = MyDataReader2.Item("defer_fixed_date").ToString
                v_defer_fixed_date_WDR = v_defer_fixed_date
                v_defer_days_WDR = v_defer_months_WDR * 30
                If v_defer_days_WDR = 30 Then v_defer_days_WDR = 25
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_ar_balance()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "nvl(SUM(DECODE(T.DC_CD,'D',A.AMT,'C',-A.AMT,0)),0) ar_balance "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and  a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and  a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and  a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and 'SAL' = '" & v_ord_tp_cd & "' "
        sql = sql & "union "
        sql = sql & "select "
        sql = sql & "nvl(SUM(DECODE(T.DC_CD,'D',A.AMT,'C',-A.AMT,0)),0)	ar_balance "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and 'SAL' <> '" & v_ord_tp_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_ar_balance = MyDataReader2.Item("ar_balance").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_deposit()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select sum(a.amt) deposit_amt "
        sql = sql & "from ar_trn a "
        sql = sql & "where a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and  a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and  a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and  a.trn_tp_cd in  ('DEP','PMT') "
        sql = sql & "and  a.mop_cd = '" & v_mop_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_deposit_amt = MyDataReader2.Item("deposit_amt").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_ufm As String
        Dim v_repl As String
        Dim v_max As Integer
        Dim v_pos As Integer
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String
        Dim v1 As String

        v_del_doc_num = "1203805AOHW"
        get_order_info()
        get_WDR_Promo_Code()
        get_fin_acct_num()
        get_promo_info()
        get_promo_info_WDR()
        get_ar_balance()
        get_deposit()

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select a.text from user_format_ln a where a.user_format_cd='SP8' order by ln#"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            Do While MyDataReader2.Read()

                v1 = MyDataReader2.Item("TEXT").ToString

                If InStr(v1, "@") > 0 Then
                    '
                    '   Sales Associate #1
                    '
                    v_ufm = "@127"
                    If String.IsNullOrEmpty(v_assoc_name1) Then v_assoc_name1 = " "
                    v_repl = v_assoc_name1
                    v_max = 36
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Ending of SO_LN
                    '
                    v_ufm = "@281"
                    v_repl = " "
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Ending of SO_LN BLOCK
                    '
                    v_ufm = "@282"
                    v_repl = " "
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Promotional Due Date (FIN)
                    '
                    v_ufm = "@322"
                    If Not IsNumeric(v_defer_months_FIN) Then v_defer_months_FIN = 0
                    If Len(v_defer_months_FIN) < 3 Then v_defer_months_FIN.PadLeft(3 - Len(v_defer_months_FIN), "0")
                    v_repl = Mid(v_defer_months_FIN, 2, 3)
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Promotional Due Date (WDR)
                    '
                    v_ufm = "@323"
                    v_pos = InStr(v1, v_ufm)
                    If v_pos > 0 Then
                        If Not IsNumeric(v_defer_days_WDR) Then v_defer_days_WDR = "0"
                        If Len(v_defer_days_WDR) < 3 Then v_defer_days_WDR = v_defer_days_WDR.PadLeft(2, "0")
                        v_repl = Mid(v_defer_days_WDR, 2, 3)
                        v_max = 4
                        v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)
                    End If
                    '
                    '   Promotional Due Date (FIXED)
                    '
                    v_ufm = "@324"
                    v_repl = v_defer_fixed_date_FIN
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to Customer Code
                    '
                    v_ufm = "@00"
                    If String.IsNullOrEmpty(v_cust_cd) Then v_cust_cd = " "
                    v_repl = v_cust_cd
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to First Name
                    '
                    v_ufm = "@01"
                    If String.IsNullOrEmpty(v_fname) Then v_fname = " "
                    v_repl = v_fname
                    v_max = 15
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to Last Name
                    '
                    v_ufm = "@02"
                    If String.IsNullOrEmpty(v_lname) Then v_lname = " "
                    v_repl = v_lname
                    v_max = 20
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Order Number
                    '
                    v_ufm = "@25"
                    If String.IsNullOrEmpty(v_del_doc_num) Then v_del_doc_num = " "
                    v_repl = v_del_doc_num
                    v_max = 14
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Deposit Amount (specific MOP)
                    '
                    v_ufm = "@77"
                    v_max = 13
                    v_pos = InStr(v1, v_ufm)

                    If v_pos > 0 Then
                        v_mop_cd = Mid(v1, v_pos + 3, 3).TrimStart
                        If Not IsNumeric(v_deposit_amt) Then v_deposit_amt = FormatCurrency(0, 2)
                        v_repl = FormatCurrency(v_deposit_amt, 2)
                        v1 = Mid(v1, 1, v_pos - 1) & v_repl & Mid(v1, v_pos + v_max, 132)
                    End If

                    '
                    '   Beginning of SO_LN BLOCK
                    '
                    v_ufm = "@85"
                    v_repl = " "
                    v_max = 3
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Original Finance Amount
                    '
                    v_ufm = "@92"
                    v_max = 13
                    v_pos = InStr(v1, v_ufm)
                    If Not IsNumeric(v_orig_fi_amt) Then v_orig_fi_amt = FormatCurrency(0, 2)
                    v_repl = FormatCurrency(v_orig_fi_amt, 2)

                    If v_pos > 0 Then
                        v1 = Mid(v1, 1, v_pos - 1) & v_repl & Mid(v1, v_pos + v_max, 132)
                    End If

                    '
                    '   Unknown Codes
                    '
                    v_ufm = "@"
                    v_repl = " "
                    v_max = 2
                    v_pos = InStr(v1, v_ufm)

                    If v_pos > 0 Then
                        v1 = Mid(v1, 1, v_pos - 1) & v_repl.PadRight(v_max) & Mid(v1, v_pos + v_max, 132)
                    End If
                End If
                v1 = Replace(v1, " ", "&nbsp;")
                main_body.InnerHtml = main_body.InnerHtml & v1 & "<br />"
            Loop
        Catch
            conn2.Close()
            Throw
        End Try

        conn2.Close()

    End Sub

    Public Function Process_Line(ByVal v_ufm As String, ByVal v_repl As String, ByVal v_max As Integer, ByVal v_pos As Integer, ByVal v1 As String)

        If v_pos > 0 Then
            v1 = Mid(v1, 1, v_pos - 1) & v_repl.PadRight(v_max) & Mid(v1, v_pos + v_max, 132)
        End If
        Return v1

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
