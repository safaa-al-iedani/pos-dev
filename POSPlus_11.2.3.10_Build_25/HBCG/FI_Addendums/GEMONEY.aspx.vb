Imports System.Data
Imports System.Data.OracleClient
Imports onBase

Partial Class GEMONEY
    Inherits POSBasePage
    Dim v_fin_name As String
    Dim v_approval_cd As String
    Dim ds As New DataSet
    Dim v_del_doc_num As String
    Dim v_ord_tp_cd As String
    Dim v_fin_cust_cd As String
    Dim v_promo_cd As String
    Dim v_promo_cd_FIN As String
    Dim v_promo_cd_WDR As String
    Dim v_co_cd As String
    Dim v_cust_cd As String
    Dim v_fname As String
    Dim v_lname As String
    Dim v_addr1 As String
    Dim v_addr2 As String
    Dim v_city As String
    Dim v_st_cd As String
    Dim v_zip_cd As String
    Dim v_assoc_name1 As String
    Dim v_assoc_name2 As String
    Dim v_orig_fi_amt As String
    Dim v_so_wr_dt As Date
    Dim v_fin_account_num As String
    Dim v_defer_fixed_date As String
    Dim v_defer_fixed_date_FIN As String
    Dim v_defer_fixed_date_WDR As String
    Dim v_defer_months_FIN As String
    Dim v_defer_months_WDR As String
    Dim v_defer_days_FIN As String
    Dim v_defer_days_WDR As String
    Dim v_ar_balance As String
    Dim v_deposit_amt As String
    Dim v_promo_desc As String
    Dim v_promo_duration As String
    Dim v_during_promo_apr As String
    Dim v_during_variable As String
    Dim v_after_promo_apr As String
    Dim v_after_variable As String
    Dim v_mop_cd As String = "GDR"

    Dim v_promoDes As String
    Dim v_promoDes1 As String
    Dim v_promoDes2 As String
    Dim v_promoDes3 As String
    Dim v_promoApr As String
    Dim v_promoAprFlag As String
    Dim v_acctApr As String
    Dim v_acctAprFlag As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DEL_DOC_NUM As String = Request("DEL_DOC_NUM")
        Dim UFM_CD As String = Request("UFM")
        Dim EMAIL As String = Request("EMAIL")
        Dim onBase As String = Request("onbase")

        If String.IsNullOrEmpty(DEL_DOC_NUM) Or String.IsNullOrEmpty(UFM_CD) Then
            Response.Write("Missing delivery document or UFM Code.")
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
            Response.End()
        Else
            ' Create a report.
            Dim Report As New FI_GE_UFM
            Dim THERMAL As Boolean = False
            Dim Report2 As New THERMAL_FI_GE_UFM
            If IsNumeric(determine_Thermal_Setup) Then
                If CDbl(determine_Thermal_Setup()) = "40" Then
                    THERMAL = True
                End If
            End If

            If THERMAL = False Then
                Process_FI_Contract(DEL_DOC_NUM, UFM_CD)
                Report.DataSource = ds
                Report.DataMember = "UFM"
                Report.BindUFMVariables()

                If Not String.IsNullOrEmpty(EMAIL) Or ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                    Try
                        Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                        If TheFile.Exists Then
                            System.IO.File.Delete("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                        End If
                    Catch ex As Exception

                    End Try
                    Dim ms As New System.IO.MemoryStream()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.Private)

                    Report.ExportToPdf(ms)
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", String.Format("inline;filename=FI" & DEL_DOC_NUM & ".pdf"))
                    ms.Seek(0, System.IO.SeekOrigin.Begin)
                    Response.BinaryWrite(ms.ToArray())
                    Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                    fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                    fs.Close()
                    ms.Close()
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    If ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                        Import_To_Onbase("FI" & DEL_DOC_NUM & ".pdf", "finance", DEL_DOC_NUM)
                    End If
                End If

                ReportViewer1.Report = Report
                ReportViewer1.WritePdfTo(Page.Response)
            Else
                Process_FI_Contract(DEL_DOC_NUM, UFM_CD)
                Report2.DataSource = ds
                Report2.DataMember = "UFM"
                Report2.BindUFMVariables()

                If Not String.IsNullOrEmpty(EMAIL) Or ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                    Try
                        Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                        If TheFile.Exists Then
                            System.IO.File.Delete("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                        End If
                    Catch ex As Exception

                    End Try
                    Dim ms As New System.IO.MemoryStream()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.Private)

                    Report2.ExportToPdf(ms)
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", String.Format("inline;filename=FI" & DEL_DOC_NUM & ".pdf"))
                    ms.Seek(0, System.IO.SeekOrigin.Begin)
                    Response.BinaryWrite(ms.ToArray())
                    Dim fs As System.IO.FileStream = System.IO.File.OpenWrite("c:\Invoices\FI" & DEL_DOC_NUM & ".pdf")
                    fs.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
                    fs.Close()
                    ms.Close()
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    If ConfigurationManager.AppSettings("onbase").ToString = "Y" Then
                        Import_To_Onbase("FI" & DEL_DOC_NUM & ".pdf", "finance", DEL_DOC_NUM)
                    End If
                End If

                ReportViewer1.Report = Report2
                ReportViewer1.WritePdfTo(Page.Response)
            End If
        End If

    End Sub


    Public Sub get_order_info()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "SELECT "
        sql = sql & "a.del_doc_num"
        sql = sql & ", a.ord_tp_cd"
        sql = sql & ", a.fin_cust_cd"
        sql = sql & ", a.orig_fi_amt"
        sql = sql & ", a.so_wr_dt"
        sql = sql & ", a.approval_cd"
        sql = sql & ", e.promo_cd"
        sql = sql & ", s.co_cd"
        sql = sql & ", c.cust_cd"
        sql = sql & ", c.fname"
        sql = sql & ", c.lname"
        sql = sql & ", c.addr1"
        sql = sql & ", c.addr2"
        sql = sql & ", c.city"
        sql = sql & ", c.st_cd"
        sql = sql & ", c.zip_cd"
        sql = sql & ", g.fname || ' ' || g.lname	assoc_name#1"
        sql = sql & ", h.fname || ' ' || h.lname	assoc_name#2"
        sql = sql & ",  asp.name"
        sql = sql & " FROM "
        sql = sql & "emp h"
        sql = sql & ", emp  g"
        sql = sql & ", store  s"
        sql = sql & ", so_asp e"
        sql = sql & ", cust  c"
        sql = sql & ", so a"
        sql = sql & ", asp asp"
        sql = sql & " WHERE  a.del_doc_num =  '" & v_del_doc_num & "' "
        sql = sql & " AND  e.del_doc_num =  '" & v_del_doc_num & "' "
        sql = sql & "AND  asp.as_cd  =  a.fin_cust_cd "
        sql = sql & "AND  a.cust_cd  || '' =  c.cust_cd  (+) "
        sql = sql & "AND  a.so_store_cd || '' =  s.store_cd  (+) "
        sql = sql & "AND  a.so_emp_slsp_cd1   =  g.emp_cd  (+) "
        sql = sql & "AND  a.so_emp_slsp_cd2   =  h.emp_cd  (+) "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_del_doc_num = MyDataReader2.Item("del_doc_num").ToString
                v_ord_tp_cd = MyDataReader2.Item("ord_tp_cd").ToString
                v_fin_cust_cd = MyDataReader2.Item("fin_cust_cd").ToString
                v_co_cd = MyDataReader2.Item("co_cd").ToString
                v_promo_cd_FIN = MyDataReader2.Item("promo_cd").ToString
                v_promo_cd = MyDataReader2.Item("promo_cd").ToString
                v_cust_cd = MyDataReader2.Item("cust_cd").ToString
                v_fname = MyDataReader2.Item("fname").ToString
                v_lname = MyDataReader2.Item("lname").ToString
                v_addr1 = MyDataReader2.Item("addr1").ToString
                v_addr2 = MyDataReader2.Item("addr2").ToString
                v_city = MyDataReader2.Item("city").ToString
                v_st_cd = MyDataReader2.Item("st_cd").ToString
                v_zip_cd = MyDataReader2.Item("zip_cd").ToString
                v_assoc_name1 = MyDataReader2.Item("assoc_name#1").ToString
                v_assoc_name2 = MyDataReader2.Item("assoc_name#2").ToString
                v_orig_fi_amt = MyDataReader2.Item("orig_fi_amt").ToString
                v_so_wr_dt = MyDataReader2.Item("so_wr_dt").ToString
                v_fin_name = MyDataReader2.Item("name").ToString
                v_approval_cd = MyDataReader2.Item("approval_cd").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub


    Public Sub GetAdditionalFinanceInfo()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "SELECT PROMO_DES, PROMO_DES1, " &
                                    "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                    "ACCT_APR, ACCT_APR_FLAG " &
                    "FROM so_asp " &
                    "WHERE del_doc_num = '" & v_del_doc_num & "' " &
                    "AND  promo_cd =  '" & v_promo_cd & "'"


        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                v_promoDes = MyDataReader.Item("PROMO_DES").ToString
                v_promoDes1 = MyDataReader.Item("PROMO_DES1").ToString
                v_promoDes2 = MyDataReader.Item("PROMO_DES2").ToString
                v_promoDes3 = MyDataReader.Item("PROMO_DES3").ToString
                v_promoApr = MyDataReader.Item("PROMO_APR").ToString
                v_promoAprFlag = MyDataReader.Item("PROMO_APR_FLAG").ToString
                v_acctApr = MyDataReader.Item("ACCT_APR").ToString
                v_acctAprFlag = MyDataReader.Item("ACCT_APR_FLAG").ToString
            End If

        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_CCA2009_Terms()

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        'Open Connection 
        conn2.Open()

        sql = "select * "
        sql = sql & "from "
        sql = sql & "settlement_terms b "
        sql = sql & "where  b.del_doc_num = '" & v_del_doc_num & "' "
        'sql = sql & "and b.ord_tp_cd = '" & v_mop_cd & "'"
        sql = sql & "and b.ord_tp_cd = 'FI'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_promo_desc = MyDataReader2.Item("PLAN_DESC").ToString
                v_promo_duration = MyDataReader2.Item("INTRO_DURATION").ToString
                v_during_promo_apr = MyDataReader2.Item("INTRO_APR").ToString
                v_during_variable = MyDataReader2.Item("INTRO_RATE").ToString
                v_after_promo_apr = MyDataReader2.Item("RATE_DEL_APR").ToString
                v_after_variable = MyDataReader2.Item("RATE_DEL_RATE_TYPE").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        If IsNumeric(v_during_promo_apr) Then v_during_promo_apr = v_during_promo_apr / 10000 & "% "
        If IsNumeric(v_after_promo_apr) Then v_after_promo_apr = v_after_promo_apr / 10000 & "% "
        If v_after_variable = "VARIABLE" Then
            v_after_variable = " (v)"
        Else
            v_after_variable = ""
        End If
        If v_during_variable = "VARIABLE" Then
            v_during_variable = " (v)"
        Else
            v_during_variable = ""
        End If

        conn2.Close()

    End Sub

    Public Sub get_WDR_Promo_Code()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select  rtrim(substr(b.text,42,5))	WDR_promo_code "
        sql = sql & "from "
        sql = sql & "so_cmnt b "
        sql = sql & ",so      a "
        sql = sql & "where  a.del_doc_num       =  '" & v_del_doc_num & "' "
        sql = sql & "and  a.so_wr_dt     +0   =  b.so_wr_dt    (+) "
        sql = sql & "and  a.so_store_cd || '' =  b.so_store_cd (+) "
        sql = sql & "and  a.so_seq_num  || '' =  b.so_seq_num  (+) "
        sql = sql & "and  b.text           like  'Promotion-Code Assigned to WDR Deposit = %'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_promo_cd_WDR = MyDataReader2.Item("WDR_promo_code").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Function determine_Thermal_Setup() As String

        determine_Thermal_Setup = ""

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select  NVL(LBL_WIDTH,0) AS LBL_WIDTH "
        sql = sql & "from "
        sql = sql & "user_format "
        sql = sql & "where  ORIGIN_CD       =  'IPRT' "
        sql = sql & "and  USER_FORMAT_CD   =  '" & Request("UFM") & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                determine_Thermal_Setup = MyDataReader2.Item("LBL_WIDTH").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Function

    ''' <summary>
    ''' Returns the finance card number from the AR tran table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub get_fin_acct_num()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select a.bnk_crd_num	"
        sql = sql & "from ar_trn a "
        sql = sql & "where a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd    = '" & v_cust_cd & "' "
        sql = sql & "and a.ivc_cd     = '" & v_del_doc_num & "' "
        sql = sql & "and a.trn_tp_cd  = '" & v_ord_tp_cd & "' "
        sql = sql & "and     'SAL'    = '" & v_ord_tp_cd & "' "
        sql = sql & "union "
        sql = sql & "select a.bnk_crd_num "
        sql = sql & "from ar_trn a "
        sql = sql & "where a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd    = '" & v_cust_cd & "' "
        sql = sql & "and a.adj_ivc_cd =  '" & v_del_doc_num & "' "
        sql = sql & "and a.trn_tp_cd  =  '" & v_ord_tp_cd & "' "
        sql = sql & "and     'SAL'   <>  '" & v_ord_tp_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_fin_account_num = MyDataReader2.Item("bnk_crd_num").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    ''' <summary>
    ''' Returns the promotion information such as defer_monhts, defer fixed date, etc from the 
    ''' provider promotion info table.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub get_promo_info()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "NVL(defer_int_mth,0) defer_months "
        sql = sql & ",to_char(d.end_dt,'mm/dd/yyyy') defer_fixed_date "
        sql = sql & "from "
        sql = sql & "asp_promo d "
        sql = sql & "where d.as_cd = '" & v_fin_cust_cd & "' "
        sql = sql & "and d.promo_cd =  '" & v_promo_cd & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_defer_months_FIN = MyDataReader2.Item("defer_months").ToString
                v_defer_fixed_date = MyDataReader2.Item("defer_fixed_date").ToString
                v_defer_fixed_date_FIN = v_defer_fixed_date
                v_defer_days_FIN = v_defer_months_FIN * 30
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_promo_info_WDR()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "to_number(substr(d.des,1,2)) defer_months "
        sql = sql & ",to_char(d.end_dt,'mm/dd/yyyy') defer_fixed_date "
        sql = sql & "from "
        sql = sql & "asp_promo d "
        sql = sql & "where d.as_cd = '" & v_fin_cust_cd & "' "
        sql = sql & "and d.promo_cd =  '" & v_promo_cd_WDR & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_defer_months_WDR = MyDataReader2.Item("defer_months").ToString
                v_defer_fixed_date = MyDataReader2.Item("defer_fixed_date").ToString
                v_defer_fixed_date_WDR = v_defer_fixed_date
                v_defer_days_WDR = v_defer_months_WDR * 30
                If v_defer_days_WDR = 30 Then v_defer_days_WDR = 25
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_ar_balance()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select "
        sql = sql & "nvl(SUM(DECODE(T.DC_CD,'D',A.AMT,'C',-A.AMT,0)),0) ar_balance "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and  a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and  a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and  a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and 'SAL' = '" & v_ord_tp_cd & "' "
        sql = sql & "union "
        sql = sql & "select "
        sql = sql & "nvl(SUM(DECODE(T.DC_CD,'D',A.AMT,'C',-A.AMT,0)),0)	ar_balance "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and 'SAL' <> '" & v_ord_tp_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_ar_balance = MyDataReader2.Item("ar_balance").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub get_deposit()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select nvl(SUM(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0)),0) deposit_amt "
        sql = sql & "from ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and a.co_cd = '" & v_co_cd & "' "
        sql = sql & "and  a.cust_cd = '" & v_cust_cd & "' "
        sql = sql & "and  a.ivc_cd = '" & v_del_doc_num & "' "
        sql = sql & "and  a.trn_tp_cd in  ('DEP','PMT','R') "
        sql = sql & "and  a.mop_cd = '" & v_mop_cd & "' "

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                v_deposit_amt = MyDataReader2.Item("deposit_amt").ToString
            End If
        Catch
            conn2.Close()
            Throw
        End Try
        conn2.Close()

    End Sub

    Public Sub Process_FI_Contract(ByVal del_doc_num As String, ByVal UFM_CD As String)

        Dim v_ufm As String
        Dim v_repl As String
        Dim v_max As Integer
        Dim v_pos As Integer
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String
        Dim v1 As String
        Dim dt As New System.Data.DataTable

        dt = New DataTable("UFM")

        dt.Columns.Add("TEXT", String.Empty.GetType())
        dt.Columns.Add("DEL_DOC_NUM", String.Empty.GetType())

        v_del_doc_num = del_doc_num
        get_order_info()
        get_WDR_Promo_Code()
        get_fin_acct_num()
        get_promo_info()
        get_promo_info_WDR()
        get_ar_balance()
        get_deposit()
        get_CCA2009_Terms()
        GetAdditionalFinanceInfo()

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "select a.text from user_format_ln a where a.user_format_cd='" & UFM_CD & "' order by ln#"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            Do While MyDataReader2.Read()

                v1 = MyDataReader2.Item("TEXT").ToString

                If InStr(v1, "@") > 0 OrElse (InStr(v1, "~")) Then
                    v_ufm = ""
                    v_repl = ""
                    v_max = 0
                    v_pos = 0

                    '
                    '   Finance Account Number
                    '
                    v_ufm = "~19"
                    If String.IsNullOrEmpty(v_fin_account_num) Then
                        v_fin_account_num = " "
                        v_repl = v_fin_account_num
                    Else
                        v_repl = v_fin_account_num.Substring(v_fin_account_num.Length - 4).PadLeft(v_fin_account_num.Length, "*")
                    End If
                    v_max = 16
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Finance Approval Code
                    '
                    v_ufm = "@45"
                    If String.IsNullOrEmpty(v_approval_cd) Then v_approval_cd = " "
                    v_repl = v_approval_cd
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Finance Company Name
                    '
                    v_ufm = "@113"
                    If String.IsNullOrEmpty(v_fin_name) Then v_fin_name = " "
                    v_repl = v_fin_name
                    v_max = 36
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Sales Associate #1
                    '
                    v_ufm = "@127"
                    If String.IsNullOrEmpty(v_assoc_name1) Then v_assoc_name1 = " "
                    v_repl = v_assoc_name1
                    v_max = 36
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Ending of SO_LN
                    '
                    v_ufm = "@281"
                    v_repl = " "
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Ending of SO_LN BLOCK
                    '
                    v_ufm = "@282"
                    v_repl = " "
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Promotional Due Date (FIN)
                    '
                    v_ufm = "@322"
                    If Not IsNumeric(v_defer_months_FIN) Then v_defer_months_FIN = 0
                    If Len(v_defer_months_FIN) < 3 Then v_defer_months_FIN.PadLeft(3 - Len(v_defer_months_FIN), "0")
                    v_repl = v_defer_months_FIN
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Promotional Due Date (WDR)
                    '
                    v_ufm = "@323"
                    v_pos = InStr(v1, v_ufm)
                    If v_pos > 0 Then
                        If Not IsNumeric(v_defer_days_WDR) Then v_defer_days_WDR = "0"
                        If Len(v_defer_days_WDR) < 3 Then v_defer_days_WDR = v_defer_days_WDR.PadLeft(2, "0")
                        'v_repl = Mid(v_defer_days_WDR, 2, 3)
                        v_repl = v_defer_days_WDR / 30
                        v_max = 4
                        v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)
                    End If
                    '
                    '   Promotional Due Date (FIXED)
                    '
                    v_ufm = "@324"
                    v_repl = v_defer_fixed_date_FIN
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Promotional Months (FIN)
                    '
                    v_ufm = "@325"
                    If Not IsNumeric(v_defer_months_FIN) Then v_defer_months_FIN = 0
                    If Len(v_defer_months_FIN) < 3 Then v_defer_months_FIN.PadLeft(3 - Len(v_defer_months_FIN), "0")
                    If IsNumeric(v_defer_months_FIN) Then
                        v_repl = FormatCurrency((v_orig_fi_amt / v_defer_months_FIN), 2)
                    Else
                        v_repl = 0
                    End If
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to Customer Code
                    '
                    v_ufm = "@00"
                    If String.IsNullOrEmpty(v_cust_cd) Then v_cust_cd = " "
                    v_repl = v_cust_cd
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to First Name
                    '
                    v_ufm = "@01"
                    If String.IsNullOrEmpty(v_fname) Then v_fname = " "
                    v_repl = v_fname
                    v_max = 15
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Bill to Last Name
                    '
                    v_ufm = "@02"
                    If String.IsNullOrEmpty(v_lname) Then v_lname = " "
                    v_repl = v_lname
                    v_max = 20
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Order Number
                    '
                    v_ufm = "@25"
                    If String.IsNullOrEmpty(v_del_doc_num) Then v_del_doc_num = " "
                    v_repl = v_del_doc_num
                    v_max = 14
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Deposit Amount (specific MOP)
                    '
                    v_ufm = "@77"
                    v_max = 13
                    v_pos = InStr(v1, v_ufm)

                    If v_pos > 0 Then
                        v_mop_cd = Mid(v1, v_pos + 3, 3).TrimStart
                        If Not IsNumeric(v_deposit_amt) Then v_deposit_amt = FormatCurrency(0, 2)
                        v_repl = FormatCurrency(v_deposit_amt, 2)
                        v1 = Mid(v1, 1, v_pos - 1) & v_repl & Mid(v1, v_pos + v_max, 132)
                    End If

                    '
                    '   Beginning of SO_LN BLOCK
                    '
                    v_ufm = "@85"
                    v_repl = " "
                    v_max = 3
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Original Finance Amount
                    '
                    v_ufm = "@92"
                    v_max = 13
                    v_pos = InStr(v1, v_ufm)
                    If Not IsNumeric(v_orig_fi_amt) Then v_orig_fi_amt = FormatCurrency(0, 2)
                    v_repl = FormatCurrency(v_orig_fi_amt, 2)

                    If v_pos > 0 Then
                        v1 = Mid(v1, 1, v_pos - 1) & v_repl & Mid(v1, v_pos + v_max, 132)
                    End If

                    '
                    '   CCA2009 Promo Description
                    '
                    v_ufm = "@GE1"
                    If String.IsNullOrEmpty(v_promo_desc) Then v_promo_desc = " "
                    v_repl = v_promo_desc
                    v_max = 50
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   CCA2009 Promo Duration
                    '
                    v_ufm = "@GE2"
                    If String.IsNullOrEmpty(v_promo_duration) Then v_promo_duration = " "
                    v_repl = v_promo_duration
                    v_max = 50
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   CCA2009 During Promo APR
                    '
                    v_ufm = "@GE3"
                    If String.IsNullOrEmpty(v_during_promo_apr) Then v_during_promo_apr = " "
                    v_repl = v_during_promo_apr
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   CCA2009 During Promo Variable
                    '
                    v_ufm = "@GE5"
                    If String.IsNullOrEmpty(v_during_variable) Then v_during_variable = " "
                    v_repl = v_during_variable
                    v_max = 50
                    v_pos = InStr(v1, v_ufm)

                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   CCA2009 After Promo APR
                    '
                    v_ufm = "@GE4"
                    If String.IsNullOrEmpty(v_after_promo_apr) Then v_after_promo_apr = " "
                    v_repl = v_after_promo_apr
                    v_max = 4
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   CCA2009 After Promo Variable
                    '
                    v_ufm = "@GE6"
                    If String.IsNullOrEmpty(v_after_variable) Then v_after_variable = " "
                    v_repl = v_after_variable
                    v_max = 50
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '====== ADDITIONAL FINANCE INFO processing =========
                    '*** General flag to activate that addtional finance info should be printed
                    v_ufm = "@505"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '*** Flag to indicate the  start of the Promo Description1
                    v_ufm = "@506"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '   Promo Description1
                    v_ufm = "@507"
                    If String.IsNullOrEmpty(v_promoDes1) Then v_promoDes1 = " "
                    v_repl = v_promoDes1
                    v_max = 2000
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '*** Flag to indicate the  END of the Promo Description1
                    v_ufm = "@508"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '*** Flag to indicate the  start of the Promo Description2
                    v_ufm = "@509"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '   Promo Description2                    
                    v_ufm = "@510"
                    If String.IsNullOrEmpty(v_promoDes2) Then v_promoDes2 = " "
                    v_repl = v_promoDes2
                    v_max = 250
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)                    '

                    '*** Flag to indicate the  END of the Promo Description2
                    v_ufm = "@511"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '*** Flag to indicate the  start of the Promo Description3
                    v_ufm = "@512"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    '   Promo Description3
                    v_ufm = "@513"
                    If String.IsNullOrEmpty(v_promoDes3) Then v_promoDes3 = " "
                    v_repl = v_promoDes3
                    v_max = 2000
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '*** Flag to indicate the  END of the Promo Description3
                    v_ufm = "@514"
                    v_pos = InStr(v1, v_ufm)
                    If (v_pos > 0) Then v1 = "    "

                    ' Promo Duration APR
                    v_ufm = "@518"
                    If String.IsNullOrEmpty(v_promoApr) Then v_promoApr = " "
                    v_repl = v_promoApr
                    v_max = 8
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)


                    ' Promo Duration APR Flag
                    v_ufm = "@520"
                    If String.IsNullOrEmpty(v_promoAprFlag) Then v_promoAprFlag = " "
                    v_repl = v_promoAprFlag
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    ' Acct APR
                    v_ufm = "@519"
                    If String.IsNullOrEmpty(v_acctApr) Then v_acctApr = " "
                    v_repl = v_acctApr
                    v_max = 8
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    ' Acct APR Flag
                    v_ufm = "@521"
                    If String.IsNullOrEmpty(v_acctAprFlag) Then v_acctAprFlag = " "
                    v_repl = v_acctAprFlag
                    v_max = 10
                    v_pos = InStr(v1, v_ufm)
                    v1 = Process_Line(v_ufm, v_repl, v_max, v_pos, v1)

                    '
                    '   Unknown Codes
                    '
                    'v_ufm = "@"
                    'v_repl = " "
                    'v_max = 4
                    'v_pos = InStr(v1, v_ufm)
                    'If v_pos > 0 Then
                    '    v1 = Mid(v1, 1, v_pos - 1) & v_repl.PadRight(v_max) & Mid(v1, v_pos + v_max, 132)
                    'End If
                End If
                dt.Rows.Add(New String() {v1, del_doc_num})
            Loop
        Catch
            conn2.Close()
            Throw
        End Try

        ds.Tables.Add(dt)
        conn2.Close()

    End Sub

    Public Function Process_Line(ByVal v_ufm As String, ByVal v_repl As String, ByVal v_max As Integer, ByVal v_pos As Integer, ByVal v1 As String)

        If v_pos > 0 Then
            v1 = Mid(v1, 1, v_pos - 1) & v_repl.PadRight(v_max) & Mid(v1, v_pos + v_max, 132)
        End If
        Return v1

    End Function
End Class
