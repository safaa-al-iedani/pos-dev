<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts --%>
<%-- <%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PaymentServerQuery.aspx.vb" Inherits="PaymentServerQuery" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %> --%>

<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PaymentServerQuery.aspx.vb" Inherits="PaymentServerQuery" meta:resourcekey="PageResource1" UICulture="auto" %>

<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table border="0" style="position: relative; width: 800px; left: 0px; top: 0px;">
        <tr>
            <td class="class_section_label" colspan="6" align="left">
                <dx:ASPxLabel ID="lbl_section_terminal" runat="server" meta:resourcekey="lbl_section_terminal" Font-Size="Medium" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_terminal_id" Width="130px" runat="server" meta:resourcekey="lbl_terminal_id" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_terminal_id" Width="120px" MaxLength="20" runat="server" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxLabel ID="lbl_terminal_ipaddr" Width="120px" runat="server" meta:resourcekey="lbl_terminal_ipaddr">--%>
                <dx:ASPxLabel ID="lbl_terminal_ipaddr" Width="150px" runat="server" meta:resourcekey="lbl_terminal_ipaddr">
                    <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_terminal_ipaddr" Width="120px" MaxLength="20" runat="server" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_pc_name" Width="100px" runat="server" meta:resourcekey="lbl_pc_name">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_pc_name" Width="220px" MaxLength="40" runat="server" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_transaction_date" runat="server" meta:resourcekey="lbl_transaction_date" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxDateEdit ID="dateEdit_transaction_date" runat="server" Width="200" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" --%>
                <dx:ASPxDateEdit ID="dateEdit_transaction_date" runat="server" Width="100" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                    AutoPostBack="True">
                    <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxDateEdit>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_company_code" runat="server" meta:resourcekey="lbl_company_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_company_code" runat="server" Width="30px" MaxLength="4" Visible="true"></asp:TextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_Search" runat="server" Text="Search" meta:resourcekey="lbl_btn_search">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear Screen" meta:resourcekey="lbl_btn_clearscreen">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td colspan="6">
                <hr />
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
    </table>
    <table>
        <%--Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015--%>
        <tr>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_section_label" colspan="3" align="left">--%>
            <td class="class_section_label" colspan="3" align="left" style="width: 600px">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                <dx:ASPxLabel ID="lbl_section_transaction" runat="server" meta:resourcekey="lbl_section_transaction" Font-Size="Medium" Font-Bold="true">
                </dx:ASPxLabel>
                <%--find_find_16x16 programming_bugreport_16x16   --%>
                <dx:ASPxButton ID="btn_SpecialAttention" runat="server" Text="" Image-IconID="support_suggestion_16x16">
                </dx:ASPxButton>

            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_section_label" colspan="2" align="left">--%>
            <td class="class_section_label" colspan="3" align="left">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                <dx:ASPxLabel ID="lbl_section_customer_account" runat="server" meta:resourcekey="lbl_section_customer_account" Font-Size="Medium" Font-Bold="true">
                </dx:ASPxLabel>
                <%--Code Commented by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--</td>
            <td class="class_section_label" colspan="1" align="right">--%>
                <%--Code Commented by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>

                <dx:ASPxButton ID="btn_ToggleToGridResults" runat="server" Text="" Image-IconID="grid_grid_16x16" Visible="false">
                </dx:ASPxButton>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxLabel ID="lbl_PET_trans_type" runat="server" meta:resourcekey="lbl_PET_trans_type" Font-Bold="true">--%>
                <dx:ASPxLabel ID="lbl_PET_trans_type" runat="server" meta:resourcekey="lbl_PET_trans_type" Font-Bold="true" Width="130px">
                    <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_trans_type" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxLabel ID="ASPxLabel1" runat="server" meta:resourcekey="lbl_ARTRN_trans_type">--%>
                <dx:ASPxLabel ID="lbl_ARTRN_trans_type" runat="server" meta:resourcekey="lbl_ARTRN_trans_type" Width="120px">
                    <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_trans_type" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_ps_invoice" runat="server" meta:resourcekey="lbl_PET_ps_invoice" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_ps_invoice" runat="server" Width="300px" MaxLength="14" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_invoice" runat="server" meta:resourcekey="lbl_ARTRN_invoice">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_invoice" runat="server" Width="300px" MaxLength="14" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_amount" runat="server" meta:resourcekey="lbl_PET_amount" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_amount" runat="server" Width="300px" MaxLength="20" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_amount" runat="server" meta:resourcekey="lbl_ARTRN_amount">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_amount" runat="server" Width="300px" MaxLength="20" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_entry_type" runat="server" meta:resourcekey="lbl_PET_entry_type">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_entry_type" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_adj_invoice" runat="server" meta:resourcekey="lbl_ARTRN_adj_invoice">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_adj_invoice" runat="server" Width="300px" MaxLength="14" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_card_number" runat="server" meta:resourcekey="lbl_PET_card_number" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_card_number" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_contract_code" runat="server" meta:resourcekey="lbl_ARTRN_contract_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_contract_code" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_expiry_date" runat="server" meta:resourcekey="lbl_PET_expiry_date">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_expiry_date" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_cash_drawer" runat="server" meta:resourcekey="lbl_ARTRN_cash_drawer">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_cash_drawer" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_card_type" runat="server" meta:resourcekey="lbl_PET_card_type" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_card_type" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_status_code" runat="server" meta:resourcekey="lbl_ARTRN_status_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_status_code" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_plan_num" runat="server" meta:resourcekey="lbl_PET_plan_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_plan_num" runat="server" Width="300px" MaxLength="500" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_mop_cd" runat="server" meta:resourcekey="lbl_ARTRN_mop_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_mop_cd" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_auth_num" runat="server" meta:resourcekey="lbl_PET_auth_num" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_auth_num" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_source" runat="server" meta:resourcekey="lbl_ARTRN_source">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_source" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_dsp_trans_msg" runat="server" meta:resourcekey="lbl_PET_dsp_trans_msg">
                </dx:ASPxLabel>

            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_dsp_trans_msg" runat="server" Width="280px" MaxLength="500" Visible="true"></asp:TextBox>
                <asp:TextBox ID="txt_PET_rcp_trans_msg" runat="server" Width="280px" MaxLength="450"
                    Visible="false">
                </asp:TextBox>
                <dx:ASPxButton ID="btn_additional_msg" runat="server" Visible="true" Width="18px" Text="..">
                    <ClientSideEvents Click="function(s, e) {ShowAddlnMessage()}" />
                </dx:ASPxButton>
                <dx:ASPxPopupControl ClientInstanceName="ASPxPopupControl_Addl_Msg" Width="330px" Height="50px"
                    MaxWidth="800px" MaxHeight="800px" MinHeight="50px" MinWidth="150px" ID="ASPxPopupControl_Addl_Msg"
                    PopupElementID="btn_additional_msg" HeaderText=" " meta:resourcekey="lbl_additional_msg"
                    runat="server" EnableViewState="True" PopupHorizontalAlign="LeftSides" PopupVerticalAlign="Below"
                    EnableHierarchyRecreation="True">
                    <ContentCollection>
                        <dx:PopupControlContentControl runat="server">
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="lbl_PET_rcp_trans_msg" runat="server" Width="300px" MaxLength="500" Visible="true"></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_reference_des" runat="server" meta:resourcekey="lbl_ARTRN_reference_des">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_reference_des" runat="server" Width="300px" MaxLength="40" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_cust_cd" runat="server" meta:resourcekey="lbl_PET_cust_cd" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_cust_cd" runat="server" Width="300px" MaxLength="10" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_post_date" runat="server" meta:resourcekey="lbl_ARTRN_post_date">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_post_date" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_date_time" runat="server" meta:resourcekey="lbl_PET_date_time">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_date_time" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_receipt_no" runat="server" meta:resourcekey="lbl_ARTRN_receipt_no">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_receipt_no" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_batch_num" runat="server" meta:resourcekey="lbl_PET_batch_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_batch_num" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="3" align="right">&nbsp;
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_seq_num" runat="server" meta:resourcekey="lbl_PET_seq_num">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_seq_num" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ARTRN_employee" runat="server" meta:resourcekey="lbl_ARTRN_employee">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_ARTRN_employee_code" runat="server" Width="100px" MaxLength="10" Visible="true"></asp:TextBox>
                <asp:TextBox ID="txt_ARTRN_employee_name" runat="server" Width="190px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <%-- ************************************************************************************************************************** --%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_PET_lpsf_indicator" runat="server" meta:resourcekey="lbl_PET_lpsf_indicator">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_PET_lpsf_indicator" runat="server" Width="300px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_SO_employee" runat="server" meta:resourcekey="lbl_SO_employee">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_SO_employee_code" runat="server" Width="100px" MaxLength="10" Visible="true"></asp:TextBox>
                <asp:TextBox ID="txt_SO_employee_name" runat="server" Width="190px" MaxLength="50" Visible="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxComboBox ID="printer_drp" runat="server" Width="150px" DropDownRows="5" Visible="false">
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_print_receipt" runat="server" Visible="false" Width="150px" meta:resourcekey="btn_print_receipt" EncodeHtml="false">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <table border="0" width="100%">
                    <tr>
                        <td width="25%" align="center">
                            <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                CommandArgument="First" Visible="false" meta:resourcekey="lbl_btn_first">
                            </dx:ASPxButton>
                        </td>
                        <td width="25%" align="center">
                            <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                CommandArgument="Prev" Visible="false" meta:resourcekey="lbl_btn_prev">
                            </dx:ASPxButton>
                        </td>
                        <td width="25%" align="center">
                            <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                CommandArgument="Next" Visible="false" meta:resourcekey="lbl_btn_next">
                            </dx:ASPxButton>
                        </td>
                        <td width="25%" align="center">
                            <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                CommandArgument="Last" Visible="false" meta:resourcekey="lbl_btn_last">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>

    <div runat="server" id="div_version">
        <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
        </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
