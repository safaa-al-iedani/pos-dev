Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils

Partial Class Delivery
    Inherits POSBasePage

    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theCustBiz As CustomerBiz = New CustomerBiz()
    Private theSalBiz As SalesBiz = New SalesBiz()
    Private theTranBiz As TransportationBiz = New TransportationBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' NOT SURE THIS PAGE SHOULDN"T BE RENAMED TO ZoneSelection or something as it is now called for zone code selection for pickups as needed as well as delivery zone selection

        'mariam -Oct 4,2016-Price Change Approval
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            If Request("LEAD") <> "TRUE" Then
                Response.Redirect("order_detail.aspx")
            Else
                Response.Redirect("order_detail.aspx?LEAD=TRUE")
            End If
        End If
        'mariam -end

        If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Then
            'Calendar1.Visible = False
            'Pickup.Visible = False
            'nav_table.Visible = False
            'lblDesc.Text = "You cannot schedule deliveries/pickups for Miscellaneous Credits or Debits."
            'txtZipCode.Visible = False
            'btnSubmit.Visible = False
            'GridView1.Visible = False
            Response.Redirect("DeliveryDate.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then

            If Session("STORE_ENABLE_ARS") = "True" AndAlso
                Not Session("ord_tp_cd") = Nothing AndAlso
                AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString().ToUpper() Then

                ' TODO - have to deal with maxdeldate as string vs date - not at this time, maybe have getmaxavaildt return string ????
                'Dim MaxDelDate As String = ""
                'Dim maxTempItmDt As Date = theSalBiz.GetMaxAvailDate(SessVar.sessId)
                'If (Not IsNothing(maxTempItmDt)) AndAlso IsDate(maxTempItmDt) Then

                '    If maxTempItmDt < Date.Today Then

                '        MaxDelDate = Date.Today

                '    Else
                '        MaxDelDate = maxTempItmDt
                '    End If
                'End If
                'ViewState("MaxDelDate") = MaxDelDate
                Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim MaxDelDate As String
                Dim MyDataReader2 As OracleDataReader
                Dim Sql As String = "SELECT   MAX( AVAIL_DT )  AvailDate FROM TEMP_ITM WHERE SESSION_ID='" & Session.SessionID.ToString & "'"
                Try

                    objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    objConnection.Open()
                    objsql2 = DisposablesManager.BuildOracleCommand(Sql, objConnection)
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    'Store Values in String Variables 
                    If (MyDataReader2.Read()) Then
                        If MyDataReader2.Item("AvailDate").ToString <> String.Empty Then
                            If Date.Parse(MyDataReader2.Item("AvailDate").ToString) < Date.Today Then
                                MaxDelDate = Date.Today
                            Else
                                MaxDelDate = Date.Parse(MyDataReader2.Item("AvailDate").ToString)
                            End If

                        Else
                            MaxDelDate = ""
                        End If
                    Else
                        'If no rows found, assign Empty
                        MaxDelDate = String.Empty
                    End If
                    ViewState("MaxDelDate") = MaxDelDate
                    'Close Connection 
                    MyDataReader2.Close()
                    objConnection.Close()
                Catch ex As Exception
                    'nothing to do
                Finally
                    If objConnection.State = ConnectionState.Open Then
                        objConnection.Close()
                    End If
                End Try


                'if ARS is enabled, then only consider disabling the button

                If MaxDelDate = String.Empty Then
                    Calendar1.Enabled = False
                    If Not Request.QueryString("src") = Nothing AndAlso Not Request.QueryString("src").Trim().Equals("order_detail") Then
                        ASPlblError.Text = "No Pickup date found for Item(s)"
                    Else
                        ASPlblError.Text = String.Empty
                    End If

                    divError.Visible = True
                Else
                    Calendar1.Enabled = True
                    Calendar1.MinDate = Convert.ToDateTime(MaxDelDate)
                    ASPlblError.Text = ""
                    divError.Visible = False
                End If

            Else ' not ARS
                Calendar1.MinDate = Today.Date
            End If

            ' if we got here from order_detail, then came to get the zone and now will return
            If Request.QueryString("src") IsNot Nothing AndAlso Request.QueryString("src").Equals("order_detail") AndAlso theInvBiz.RequireZoneCd(SessVar.puDel, SessVar.isArsEnabled, SessVar.ordTpCd) Then

                btn_search_again.Visible = False
                Calendar1.Visible = False

                If Request.QueryString("LEAD") IsNot Nothing AndAlso Request.QueryString("LEAD").ToUpper().Equals("TRUE") Then
                    lblDesc.Visible = False
                    txtZipCode.Visible = False
                    btnSubmit.Visible = False
                Else
                    lblDesc.Visible = True
                    txtZipCode.Visible = True
                    btnSubmit.Visible = True
                End If

                If Not String.IsNullOrEmpty(SessVar.zipCode4Zone) Then
                    delivery.Visible = True
                    nav_table.Visible = True
                    GridView1.Visible = True
                    txtZipCode.Text = SessVar.zipCode4Zone
                    GridView1_Binddata()
                End If

            ElseIf Session("cash_carry") = "TRUE" Then
                nav_table.Visible = False
                Pickup.Visible = False
                txtZipCode.Visible = False
                btnSubmit.Visible = False
                GridView1.Visible = False
                Calendar1.Visible = False
                'lblDesc.Text = "CASH AND CARRY TRANSACTION"
                lblDesc.Text = Resources.LibResources.Label60
                ' lbl_curr_data.Text = "The customer is taking this order with them"
                lbl_curr_data.Text = Resources.LibResources.Label599
            Else
                If Session("PD") = "P" Then
                    Calendar1.Visible = True
                    Pickup.Visible = True
                    nav_table.Visible = False
                    'Daniela french
                    'lblDesc.Text = "Please select a pickup date below."
                    lblDesc.Text = Resources.LibResources.Label413
                    txtZipCode.Visible = False
                    btnSubmit.Visible = False
                    GridView1.Visible = False
                    'MM-7300
                    lblZoneCd.Text = Resources.POSMessages.MSG0039 + Session("zone_des")

                    If IsDate(Session("del_dt")) Then
                        Calendar1.SelectedDate = Session("del_dt")
                        lbl_curr_data.Text = Resources.LibResources.Label603 & " " & Session("del_dt")
                        lbl_curr_data.Visible = True
                    Else
                        'Do Nothing
                    End If
                ElseIf Session("PD") = "D" Then
                    Calendar1.Visible = False
                    delivery.Visible = True
                    nav_table.Visible = True
                    lblDesc.Text = Resources.LibResources.Label405
                    txtZipCode.Visible = True
                    btnSubmit.Visible = True
                    GridView1.Visible = True
                    GridView1_Binddata()
                Else
                    If Session("MP") & "" <> "" Then
                        Calendar1.Visible = False
                        delivery.Visible = True
                        nav_table.Visible = True
                        lblDesc.Text = Resources.LibResources.Label405
                        txtZipCode.Visible = True
                        btnSubmit.Visible = True
                        GridView1.Visible = True
                        GridView1_Binddata()
                        If Not IsPostBack Then
                            btn_search_again.Visible = False
                        Else
                            btn_search_again.Visible = True
                        End If
                    Else
                        delivery.Visible = True
                        Pickup.Visible = False
                        nav_table.Visible = False
                        lblDesc.Visible = True
                        If Request("LEAD") = "TRUE" Then
                            lblDesc.Text = "You must select a delivery option in the first screen.  Please click <a href=""Startup.aspx?LEAD=TRUE"">here</a> to complete this process."
                        Else
                            lblDesc.Text = "You must select a delivery option in the first screen.  Please click <a href=""Startup.aspx"">here</a> to complete this process."
                        End If
                        txtZipCode.Visible = False
                        btnSubmit.Visible = False
                        GridView1.Visible = False
                    End If
                End If
                If IsDate(Session("del_dt")) And Not String.IsNullOrEmpty(Session("zone_cd")) Then
                    If Session("PD") = "D" Then
                        lbl_curr_data.Text = "This order is currently linked to zone " & Session("zone_cd") & " scheduled for delivery on " & Session("del_dt")
                    End If
                End If
            End If
        End If
        If Session("MP") & "" <> "" Then
            Calendar1.Visible = False
            delivery.Visible = True
            nav_table.Visible = True
            ' lblDesc.Text = "Please enter the zip code you would like to view availability for below."
            lblDesc.Text = Resources.LibResources.Label405
            txtZipCode.Visible = True
            btnSubmit.Visible = True
            GridView1.Visible = True
            GridView1_Binddata()
        End If
        If Session("STORE_ENABLE_ARS") = "True" AndAlso
            Not Session("ord_tp_cd") = Nothing AndAlso
            AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString().ToUpper() Then

            If ViewState("MaxDelDate").ToString() = String.Empty Then
                Calendar1.Enabled = False
                If Not Request.QueryString("src") = Nothing AndAlso Not Request.QueryString("src").Trim().Equals("order_detail") Then
                    ASPlblError.Text = "No Pickup date found for Item(s)"
                Else
                    ASPlblError.Text = String.Empty
                End If
                divError.Visible = True
                lbl_curr_data.Visible = False
            Else
                Calendar1.Enabled = True
                Calendar1.MinDate = Convert.ToDateTime(ViewState("MaxDelDate"))
                ASPlblError.Text = ""
                divError.Visible = False
                lbl_curr_data.Visible = False
            End If
        Else
            'Do Nothing
        End If

    End Sub

    Private Sub SetZipEntryVisible()

        txtZipCode.Visible = True
        lblDesc.Visible = True
        btnSubmit.Visible = True
    End Sub

    Private Sub GridView1_Binddata()
        ASPlblError.Visible = False

        GridView1.DataSource = ""

        Dim haveZip As Boolean = False
        'lblDesc.Visible = False
        'txtZipCode.Visible = False
        'btnSubmit.Visible = False
        ' Daniela french french
        btnSubmit.Text = Resources.LibResources.Label577
        If IsPostBack = True Then

            If txtZipCode.Text & "" <> "" Then
                Session("ZIP_CD") = txtZipCode.Text
                haveZip = True
            ElseIf Session("ZIP_CD") & "" <> "" Then
                haveZip = True
            Else
                SetZipEntryVisible()

                'txtZipCode.Visible = True
                'lblDesc.Visible = True
                'btnSubmit.Visible = True
            End If

        Else
            ' if the zip isn't input, then try to acquire it in this order, from generic zip_cd session variable, from zip for zone session variable,
            '    from customer zip code session variable, and then from cust record if a cust_cd exists
            ' TODO DSA - remove this and fix comment above
            'If SessVar.aZipCd.isNotEmpty Then

            '    SetZipEntryVisible()
            '    btnSubmit.Text = "Change Zone"

            'Else
            If SessVar.zipCode4Zone.isNotEmpty Then

                SessVar.aZipCd = SessVar.zipCode4Zone
                SetZipEntryVisible()
                'Daniela french
                'btnSubmit.Text = "Change Zone"
                btnSubmit.Text = Resources.LibResources.Label73

                If String.IsNullOrEmpty(txtZipCode.Text.Trim) Then
                    ' why would you not fill in the visible with the code being used ????  this is ARS logic ???
                    txtZipCode.Text = SessVar.aZipCd
                End If
                ' if from cust code, then no option to override
            ElseIf SessVar.custZipCd.isNotEmpty Then

                SessVar.aZipCd = SessVar.custZipCd

            ElseIf SessVar.custCd.isNotEmpty Then

                SessVar.aZipCd = theCustBiz.GetCustomerZip(SessVar.sessId, SessVar.custCd)
            End If
            ' if we still don't have a zip code, then provide for entry
            If SessVar.aZipCd.isEmpty Then

                SetZipEntryVisible()

            Else
                haveZip = True
            End If
        End If

        GridView1.Visible = True
        ' Session("MP") not being null MAY mean that this page was started from the menu master page but not absolutely certain 
        '   this session variable holds a page title like 'DELIVERY AVAILABILITY'  ???
        If Session("ZIP_CD") & "" = "" And Session("MP") & "" = "" Then

            haveZip = False
            'Daniela french
            'lblDesc.Text = vbCrLf & vbCrLf & "You must select a zip code for your customer in order to schedule delivery." & vbCrLf & vbCrLf
            lblDesc.Text = vbCrLf & vbCrLf & Resources.LibResources.Label735 & vbCrLf & vbCrLf
            lblDesc.Visible = True
            txtZipCode.Visible = True 'False
            btnSubmit.Visible = True 'False
            Exit Sub
        End If

        Dim zoneDs As DataSet = New DataSet
        Dim dv As DataView
        Dim numrows As Integer = 0
        Dim zoneTbl As DataTable = New DataTable

        'Dim str_store_cd As String = Session("HOME_STORE_CD")
        'Dim str_co_cd As String = theTranBiz.lucy_get_co_cd(str_store_cd)  'lucy
        Dim str_co_cd As String = Session("CO_CD")  'Daniela improve performance
        Dim str_co_grp_cd As String = Session("str_co_grp_cd")  'Daniela franchise
        If haveZip = True Then ' get the zone

            ' zoneDs = theTranBiz.GetZones4ZipOrAll(SessVar.aZipCd)
            'If str_co_grp_cd = "LEO" Then
            'zoneDs = theTranBiz.lucy_GetZones4ZipOrAll(SessVar.aZipCd, str_co_cd, str_co_grp_cd) 'Franchise
            'Else
            'Nov 28 MCCL changes, no need to pass group
            zoneDs = theTranBiz.lucy_GetZones4ZipOrAll(SessVar.aZipCd, str_co_cd) 'Lucy
            'End If
            dv = zoneDs.Tables(0).DefaultView
            zoneTbl = zoneDs.Tables(0)
            numrows = zoneTbl.Rows.Count
        End If

        ' if only one row, we have what we need, we are done, go back, go forward or whatever needs to be done next, otherwise, show the grid
        If numrows = 1 Then

            Dim zoneRow As DataRow = zoneTbl.Rows(0)
            SessVar.zoneCd = zoneRow("ZONE_CD")
            'MM-7300
            SessVar.ZoneDes = zoneRow("ZONE_CD") + " - " + zoneRow("DES")
            GridView1.Visible = False
            'IF only zone associted with the ZIP assign the default delivery store code
            If (AppConstants.DELIVERY = SessVar.puDel) Then
                SetDeliveryStoreByZone(zoneRow("DELIVERY_STORE_CD").ToString)
            End If
            ' if called from order_detail to extract the zone code for inventory processing and there is only one row, then return to order_detail with the zone found
            If Request.QueryString("src") IsNot Nothing AndAlso Request.QueryString("src").Equals("order_detail") Then

                If Request("LEAD") <> "TRUE" Then
                    Response.Redirect("order_detail.aspx")
                Else
                    Response.Redirect("order_detail.aspx?LEAD=TRUE")
                End If

            Else
                If ConfigurationManager.AppSettings("delivery_options").ToString <> "TIERED" AndAlso SessVar.puDel = AppConstants.DELIVERY Then
                    If Not (Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") Then
                        If Not IsNumeric(Session("DEL_CHG")) Then
                            Session("DEL_CHG") = zoneRow("DEFAULT_DEL_CHG")
                        End If
                        If Not IsNumeric(Session("SETUP_CHG")) Then
                            Session("SETUP_CHG") = zoneRow("DEFAULT_SETUP_CHG")
                        End If
                    End If
                End If
                If Request("LEAD") = "TRUE" Then
                    Response.Redirect("Lead_Stage.aspx?LEAD=TRUE")
                Else
                    Response.Redirect("DeliveryDate.aspx")
                End If
            End If

        ElseIf numrows > 1 Then
            'Daniela french
            'btnSubmit.Text = "Change Zone"
            btnSubmit.Text = Resources.LibResources.Label73
            GridView1.Visible = True
            GridView1.DataSource = dv
            GridView1.DataBind()
        End If

        lbl_pageinfo.Text = "Page " + CStr(GridView1.PageIndex + 1) + " " + Resources.LibResources.Label686 + " " + CStr(GridView1.PageCount)

        ' make all the buttons visible if page count is more than 0
        If GridView1.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If GridView1.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView1.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView1.PageIndex = (GridView1.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView1.PageCount = 0 And haveZip = True Then
            ' Daniela Nov 11, change message
            'lblDesc.Text = "Sorry, no zones were found for the specified zip code.  Please re-enter"
            lblDesc.Text = "Sorry, no zones were found for the specified zip code.  Please re-enter or use the postal code of the pickup or delivery store."
            txtZipCode.Text = SessVar.aZipCd ' fill in the zip used so user will not re-use that one
            lblDesc.Visible = True
            txtZipCode.Visible = True
            btnSubmit.Visible = True
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView1.PageIndex < (GridView1.PageCount - 1) Then
                    GridView1.PageIndex += 1
                End If
            Case "Prev"
                If GridView1.PageIndex > 0 Then
                    GridView1.PageIndex -= 1
                End If
            Case "Last"
                GridView1.PageIndex = GridView1.PageCount - 1
            Case Else
                GridView1.PageIndex = 0
        End Select

        GridView1_Binddata()

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

        ' Get the currently selected row using the SelectedRow property.
        Dim row As GridViewRow = GridView1.SelectedRow

        'Assign the Zone Code from the selected row.
        If Not IsNothing(Session("PD")) AndAlso Session("PD") = AppConstants.DELIVERY AndAlso ConfigurationManager.AppSettings("delivery_options").ToString <> "TIERED" Then
            Session("DEL_CHG") = row.Cells(3).Text
        End If
        Session("SETUP_CHG") = row.Cells(4).Text

        'Daniela IT Request 2475
        If (Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") Then
            Session("DEL_CHG") = 0
            Session("SETUP_CHG") = 0
        End If

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()
        SQL = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        SQL = SQL & "FROM TAT, TAX_CD$TAT "
        SQL = SQL & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        SQL = SQL & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
        SQL = SQL & "AND TAT.DEL_TAX = 'Y' "
        SQL = SQL & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("DEL_TAX") = MyDataReader.Item("SumOfPCT")
            Else
                Session("DEL_TAX") = "N"
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        SQL = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        SQL = SQL & "FROM TAT, TAX_CD$TAT "
        SQL = SQL & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        SQL = SQL & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
        SQL = SQL & "AND TAT.SU_TAX = 'Y' "
        SQL = SQL & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("SU_TAX") = MyDataReader.Item("SumOfPCT")
            Else
                Session("SU_TAX") = "N"
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

        Session("ZONE_CD") = row.Cells(0).Text
        'MM-7300
        If row.Cells(1).Text <> "&nbsp;" Then
            SessVar.ZoneDes = row.Cells(0).Text + " - " + row.Cells(1).Text
        Else
            SessVar.ZoneDes = row.Cells(0).Text
        End If

        'Added to fix MM-6362  (on purpose didn't check for Session("STORE_ENABLE_ARS") = "True")
        If (AppConstants.DELIVERY = SessVar.puDel) Then
            SetDeliveryStoreByZone(Server.HtmlDecode(row.Cells(2).Text))
        End If

        If Request("LEAD") = "TRUE" Then
            If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                calculate_total(Page.Master)
            End If

            If Request.QueryString("src") IsNot Nothing AndAlso Request.QueryString("src").Equals("order_detail") Then
                Response.Redirect("order_detail.aspx?LEAD=TRUE")
            Else
                Response.Redirect("Lead_Stage.aspx?LEAD=TRUE")
            End If
        Else
            If Request.QueryString("src") IsNot Nothing AndAlso Request.QueryString("src").Equals("order_detail") Then
                Response.Redirect("order_detail.aspx")
            Else
                Response.Redirect("DeliveryDate.aspx")
            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        End If
        If Session("MP") & "" <> "" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged

        Session("del_dt") = Calendar1.SelectedDate
        'Daniela french
        'lbl_curr_data.Text = "This order is currently scheduled for pickup on " & Session("del_dt")
        lbl_curr_data.Text = Resources.LibResources.Label603 & " " & Session("del_dt")
        lbl_curr_data.Visible = True

    End Sub

    Protected Sub btn_search_again_Click(sender As Object, e As System.EventArgs) Handles btn_search_again.Click

        If Request.QueryString("LEAD") IsNot Nothing AndAlso Request.QueryString("LEAD").ToUpper().Equals("TRUE") Then
            Response.Redirect("Delivery.aspx?LEAD=TRUE")
        Else
            Response.Redirect("Delivery.aspx")
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        If Not String.IsNullOrEmpty(txtZipCode.Text.Trim) Then
            GridView1_Binddata()
            txtZipCode.Border.BorderColor = Color.Empty
        Else
            txtZipCode.Border.BorderColor = Color.Red
        End If
    End Sub

    ''' <summary>
    ''' Set the delivery store by Zone code
    ''' </summary>
    ''' <param name="zcmStore"></param>
    ''' <remarks></remarks>
    Private Sub SetDeliveryStoreByZone(ByVal zcmStore As String)
        If (zcmStore.isNotEmpty()) Then
            SessVar.puDelStoreCd = zcmStore
        Else
            Dim storeObj As StoreData = theSalBiz.GetStoreInfo(SessVar.wrStoreCd)
            If Not IsNothing(storeObj) AndAlso storeObj.shipToStoreCd.isNotEmpty Then
                SessVar.puDelStoreCd = storeObj.shipToStoreCd
            Else
                SessVar.puDelStoreCd = SessVar.wrStoreCd
            End If
        End If
    End Sub
End Class
