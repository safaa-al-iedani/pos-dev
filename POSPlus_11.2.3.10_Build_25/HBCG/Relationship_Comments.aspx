<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Relationship_Comments.aspx.vb"
    Inherits="Relationship_Comments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" height="100%">
                <tr>
                    <td valign="top" align="center">
                        <asp:Label ID="lbl_REL_NO" runat="server" Font-Bold="True" Font-Size="Small"
                            Height="24px" Width="169px"></asp:Label><br />
                        <br />
                        Follow Up Date:&nbsp;&nbsp;<asp:TextBox ID="txt_follow_up_dt" runat="server" Width=70></asp:TextBox>&nbsp;&nbsp;
                        <br />
                        <br />
                        <strong><span style="text-decoration: underline">Salesperson Comments:</span></strong><br />
                        <asp:TextBox ID="txt_comments" runat="server" Rows="3" TextMode="MultiLine" Height="69px" Width="327px"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="txt_past_Comments" runat="server" Rows="3" TextMode="MultiLine" Height="75px" Width="327px"></asp:TextBox>&nbsp;<br />
                        <br />
                        <strong><span style="text-decoration: underline">Customer Comments:<br />
                        </span></strong>
                        <asp:TextBox ID="txt_cust_comments" runat="server" Height="69px" Rows="3" TextMode="MultiLine"
                            Width="327px"></asp:TextBox><br />
                        <br />
                        <asp:TextBox ID="txt_cust_past" runat="server" Height="75px" Rows="3" TextMode="MultiLine"
                            Width="327px"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="btn_Save" runat="server" Text="Save Changes" Width="119px" />
                        &nbsp; &nbsp;&nbsp; &nbsp;<asp:Button ID="btn_Clear" runat="server" Text="Exit"
                            Width="119px" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
