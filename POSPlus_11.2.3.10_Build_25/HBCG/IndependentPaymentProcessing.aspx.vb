Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports System.Xml
Imports AppUtils
Imports PaymentUtils
Imports SD_Utils
Imports NCT_Utils
Imports HBCG_Utils
Imports World_Gift_Utils

Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp


Partial Class IndependentPaymentProcessing
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private thePaymentBiz As PaymentBiz = New PaymentBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Dim Insert_Payment As Boolean = True
    Dim partial_amt As Double = 0
    Private Const cResponseCriteria As String = "/HandleCode/ReasonCode/CodeDesc"
    Private Const cReturnCriteria As String = "/HandleCode/ReturnCode/CodeDesc"
    Private Const cAVSCriteria As String = "/HandleCode/AVSResponseCode/CodeDesc"

    '*************** following data members created for refund to orig MOP functionality ****
    Dim _tblOrderDetails As DataTable = New DataTable
    Dim _dsOrigPmtsForRefunds As DataSet
    Dim _hTableOrigPmtAmtsForRefunds As Hashtable
    Dim _listOrigPmtsForRefundsDd As List(Of OriginalPaymentDtc)
    Dim _dtRefundDetails As DataTable
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("EMP_CD") & "" = "" Then
                ClearCache()    'mm-sep 20,2016 - backbutton concern
                Response.Redirect("login.aspx")
            Else
                lucy_show_aprv() 'lucy
            End If
        End If
        If Not IsPostBack Then
            If Session("lucy_from_logout") = "YES" Then   'lucy
                'lbl_Error.Text = "Authorized payment on file, commit to save the transaction"
                lbl_Error.Text = Resources.LibResources.Label837
                'Dec 17 do not let go back to SOM+
                btn_return.Enabled = False
            Else
                lbl_Error.Text = String.Empty
            End If


            lbl_Error.Text = String.Empty
            txt_pmt_amt.Text = String.Format("{0:C}", 0)
            ResetBalances()
            btn_commit.Enabled = False
            lbl_post_dt.Text = FormatDateTime(System.DateTime.Today.ToString, 2)
            lbl_co_cd.Visible = False
            txt_cust_cd.Text = Request("ip_cust_cd")

            'Added in MM-758
            lbl_emp_cd.Text = Session("emp_cd")

            'set, enable/disable cbo_payment d.d. depending on securities, etc.
            SetPaymentType(Session("emp_cd"))

            'set the SHU date
            txtSHU.Text = FormatDateTime(SessVar.shuDt, DateFormat.ShortDate)

            If Request("ip_csh_pass") & "" <> "" Then
                txt_cshr_pwd.Text = Decrypt(Request("ip_csh_pass"), "CrOcOdIlE")
            End If

            If IsNumeric(Request("ip_ord_total")) Then lbl_current_pmts_amt.Text = FormatCurrency(CDbl(Request("ip_ord_total")), 2)

            'now that the cashier password has been set, validate the cashier info
            ValidateCashier()

            'this has to happen after the cashier validation because this overrules any setting of
            'the store code that might have been done in the call to validateCashier. Don't know if that
            'is correct or not??
            Dim storeCode As String = ""
            If Request("ip_store_cd") & "" <> "" Then
                storeCode = Request("ip_store_cd")
            ElseIf Session("store_cd") & "" <> "" Then
                storeCode = Session("store_cd")
            ElseIf Session("home_store_cd") & "" <> "" Then
                storeCode = Session("home_store_cd")
            End If

            'NOTE: This has to be done like this here. Otherwise there will be issues with the setting
            'of the company code later in the code
            cbo_store_cd.Value = storeCode
            GetCompanyCode()

            If Request("ip_csh_drw") & "" <> "" Then
                cbo_csh_drw.Value = Request("ip_csh_drw")
            End If

            If Request("referrer") = "salesordermaintenance.aspx" Then
                txt_ivc_cd.Text = Request("ip_ivc_cd")
                bindgrid()
                btn_return.Visible = True   'reads 'Return to SOM'
            End If
            'MM-5693
            If Request("referrer") = "Order_Stage.aspx" Then
                txt_ivc_cd.Text = Request("ip_ivc_cd")
            End If
            ValidateLoadCasherDetails()

            '***NOTE - This code is necessary and CANNOT be removed. It is for the case when the redirect or 
            ' request happens after an event that requires a full reinitialize such as deleting a payment, or showing
            ' a different popup and returning to the main page (e.g. CC)
            If Request("refresh") = "true" Then
                txt_ivc_cd.Text = Request("ip_ivc_cd")
                ' Daniela set del doc upper case
                If txt_ivc_cd.Text.isNotEmpty() Then
                    txt_ivc_cd.Text = UCase(txt_ivc_cd.Text)
                End If

                If Session("OtherInvoice") & "" = "Y" Then 'Alice add for request 167--if user delete payment detail item, re-bind customer info during screen refresh for other invoice
                    AddCustInfo(Request("ip_cust_cd"))
                Else
                    AddCustOrderInfo()
                End If
                bindgrid()
                DisplayMops()
                UpdateTotals()
            ElseIf Request("search") = "true" Then
                'means the redirect got called after an order search was done. So, just 
                'default the order# here because the expectation is that the user needs
                ' to click 'Go" button for the order validation and retrieval to happen
                txt_ivc_cd.Text = Request("ip_ivc_cd")
                ' Daniela set del doc upper case
                If txt_ivc_cd.Text.isNotEmpty() Then
                    txt_ivc_cd.Text = UCase(txt_ivc_cd.Text)
                End If
            End If
        End If  'end if Not IsPostBack 

        If (txt_cust_cd.Text.isNotEmpty) And (lbl_qs_avail.Text.isEmpty) And ConfigurationManager.AppSettings("quick_screen").ToString = "Y" Then
            CheckCustomerQuickCredit()
        End If

        'label7 is the cashier pwd- if not null, enable buttons
        Dim enableFlag As Boolean = Label7.Text.isNotEmpty
        btn_lookup_ivc.Enabled = enableFlag
        btn_show_invoices.Enabled = enableFlag
        btnOrderSearch.Enabled = enableFlag
        btn_reset.Enabled = enableFlag

        'enable/disable the add payments button- the validate order should enable it properly
        btn_add.Enabled = txt_ivc_cd.Text.isNotEmpty

        'Check if terminal has been closed and the sytem param for terminal lock-down is enabled
        ' Daniela comment not needed 
        'If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "C" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
        'SetPropertiesForLockedTerminal()
        'End If
        If InStr(UCase(Session("lucy_display")), "APPR") > 0 Then  'lucy

            'lbl_Error.Text = "Authorized transaction on file, commit to save the transaction"
            lbl_Error.Text = Resources.LibResources.Label837
            ' Daniela May 1 disable add payment to force commit
            btn_add.Enabled = False
            'Dec 17 do not let go back to SOM+
            btn_return.Enabled = False
        End If

        ' Daniela add print pick ticket
        If Not IsPostBack Then
            sy_printer_drp_Populate()
        End If

        'Alice add for request 167
        If Not IsPostBack Then
            If Session("pa_cust_cd") & "" <> "" And Session("OtherCustCd") & "" <> "" Then
                CreateCustomer()
                txtCustCode.Text = Session("pa_cust_cd")
                ASPxPopupOtherInvoice.ShowOnPageLoad = True
            End If
        End If

    End Sub

    Public Function lucy_check_del_doc_num(ByRef p_del_doc_num As String, p_user As String) As String
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isValidDelDoc2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.VarChar)).Value = p_del_doc_num
        myCMD.Parameters.Add(New OracleParameter("p_empcd", OracleType.VarChar)).Value = p_user
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_y_n").Value) = False Then
                str_y_n = myCMD.Parameters("p_y_n").Value
            End If

            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try

        Return str_y_n
    End Function
    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged

        ' Daniela remove wanings
        'lbl_fi_warning.Text = "" ' Daniela clear warning message

        'lucy add all
        If Not String.IsNullOrEmpty(cbo_finance_company.SelectedValue.ToString) Then

            ' GetPromoCodes()    

            Dim strfinCo = cbo_finance_company.SelectedValue
            ' Daniela added
            Dim mopcd As String = Session("lucy_mop_cd")

            Session("lucy_as_cd") = strfinCo
            'Daniela Jan 15 hide button if TD
            If (mopcd = "TDF") Then
                lucy_btn_new_cust.Visible = False
            Else
                lucy_btn_new_cust.Visible = True
            End If
            btn_save_fi_co.Visible = True
            btn_save_fi_co.Focus()

        End If

    End Sub
    Public Sub AddFinanceCompany()
        'lucy copied from version 44
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        Dim sql As String = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' ORDER BY NAME"
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            If (reader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "AS_CD"
                cbo_finance_company.DataTextField = "NAME"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select finance company")
            cbo_finance_company.Items.Insert(0, Resources.LibResources.Label415)
            cbo_finance_company.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub

    Public Sub AddTdCompany()
        'Daniela TD added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        Dim sql As String = "select plan_cd mop_cd, des from brick_finance_plan order by des"

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            If (reader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "MOP_CD"
                cbo_finance_company.DataTextField = "DES"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select TD finance plan")
            cbo_finance_company.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub

    Public Sub AddTdCompany(ByVal tdPlan As String)
        'Daniela TD added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        Dim sql As String = "select plan_cd mop_cd, des from brick_finance_plan where mop_cd = '" & tdPlan & "'"

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            If (reader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "MOP_CD"
                cbo_finance_company.DataTextField = "DES"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select TD finance plan")
            cbo_finance_company.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub


    Public Function MonthLastDay(ByVal dCurrDate As Date)
        'lucy
        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Public Sub lucy_check_payment(ByRef p_yn As String)
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "select mop_cd from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
        sql = sql & " and (mop_cd = 'VI' or mop_cd='FI' or mop_cd='MC' or mop_cd='FV' or mop_cd='AMX' or mop_cd='DC' or mop_cd='GC' or MOP_CD='DCS' or mop_cd = 'TDF')" 'sabrina added GC, Dec 18 added DCS, TDF
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                'lbl_Error.Text = "Authorized payment on file, commit to save the transaction"
                lbl_Error.Text = Resources.LibResources.Label837
                'Dec 17 do not let go back to SOM+
                btn_return.Enabled = False
                p_yn = "NO"
                '  Dim previousPage As String = Page.Request.UrlReferrer.ToString
                '  If IsDBNull(Request.UrlReferrer) = False Then
                ' Response.Write(Request.UrlReferrer.ToString())
                'Response.Redirect(PreviousPage)
                ' End If
            Else
                p_yn = "YES"
            End If
            conn.Close()
        Catch x
            Throw
            conn.Close()
        End Try
    End Sub

    Public Sub lucy_insert_cust_asp(ByRef p_cust_cd As String, ByRef p_bnk_num As String, ByRef p_acct_num As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Dim str_y_n As String

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_pos_inspmt.lucy_insert_cust_asp"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = p_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_bnk_num", OracleType.Char)).Value = p_bnk_num
            myCMD.Parameters.Add(New OracleParameter("p_acct_num", OracleType.Char)).Value = p_acct_num

            myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
            myCMD.ExecuteNonQuery()
            ' str_err = myCMD.Parameters("p_err").Value
            str_y_n = myCMD.Parameters("p_y_n").Value

            objConnection.Close()

        Catch x
            Throw
            objConnection.Close()
        End Try

    End Sub
    Public Sub lucy_log(ByVal p_msg As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_pos.prn"
        myCMD.CommandType = CommandType.StoredProcedure
        'strMsg = p_msg & v_count
        strMsg = p_msg
        myCMD.Parameters.Add(New OracleParameter("p_in", OracleType.Char)).Value = strMsg

        myCMD.ExecuteNonQuery()

        v_count = v_count + 1

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try
            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()
            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub

    Public Sub lucy_get_plan(ByRef p_del_doc_num As String, ByRef p_str_plan As String)

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "select fin_cust_cd from so where del_doc_num='" & p_del_doc_num & "' and trim(fin_cust_cd) is not null "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                If isEmpty(MyDataReader.Item("fin_cust_cd")) = False Or IsDBNull(MyDataReader.Item("fin_cust_cd")) = False Then
                    p_str_plan = MyDataReader.Item("fin_cust_cd")
                Else
                    p_str_plan = ""
                End If
            Else
                p_str_plan = ""
            End If
            conn.Close()
        Catch x
            Throw
            conn.Close()
        End Try
    End Sub
    Public Sub lucy_get_amt(ByRef p_del_doc_num As String, ByRef p_amt As Double)

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "select orig_fi_amt from so where del_doc_num='" & p_del_doc_num & "'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                If IsDBNull(MyDataReader.Item("orig_fi_amt")) = False Then
                    p_amt = MyDataReader.Item("orig_fi_amt")
                Else
                    p_amt = 0
                End If
            Else
                p_amt = 0
            End If
            conn.Close()
        Catch x
            Throw
            conn.Close()
        End Try
    End Sub



    Public Function GetIPAddress() As String

        ' lucy created
        Dim strHostName As String

        Dim strIPAddress As String
        Dim str_term_id As String

        strHostName = System.Net.Dns.GetHostName()

        strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(0).ToString()

        Return strIPAddress

        ' MessageBox.Show("Host Name: " & strHostName & "; IP Address: " & strIPAddress)

    End Function

    Protected Sub print_receipt(ByVal p_prt_cmd As String)
        Dim str_ip As String
        Dim str_user As String
        Dim str_pswd As String

        str_ip = get_database_ip()
        str_user = get_user()
        str_pswd = get_pswd()


        Dim syclient As New Renci.SshNet.SshClient(str_ip, str_user, str_pswd)
        ' lblInfo.Text = "in testprint1 "

        Dim sycmd As Renci.SshNet.SshCommand
        Dim str_excep As String
        Dim str_print_cmd As String

        ' str_print_cmd = Session("str_print_cmd")
        '---------'connect to test
        Try
            syclient.Connect()

        Catch
            Throw

            str_excep = Err.Description
        End Try

        '  sycmd = syclient.RunCommand("touch /moved5/lucy/pr.lst")
        sycmd = syclient.RunCommand(p_prt_cmd)
        Try
            ' Catch syex3 As Exception
        Catch syex3 As SshException

            str_excep = Err.Description
        End Try

        sycmd.Dispose()
        syclient.Disconnect()


    End Sub
    Public Function get_database_ip()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_ip As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshIP"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_ip", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_ip = myCMD.Parameters("p_ip").Value

            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_ip
    End Function
    Public Function get_user()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_user As String
        Dim p_user As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_user", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_user = myCMD.Parameters("p_user").Value
            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_user
    End Function
    Public Function get_pswd()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_pswd As String
        Dim str_pswd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshPwd"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_pswd", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_pswd = myCMD.Parameters("p_pswd").Value
            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_pswd
    End Function


    Protected Sub lucy_btn_add_Click(ByRef p_mop_cd As String)
        ' copied from btn_add_click
        Session("del_doc_num") = txt_ivc_cd.Text
        ' Daniela reset exp dt
        Session("lucy_exp_dt") = ""
        lbl_fi_warning.Text = ""
        txt_fi_exp_dt.Text = ""

        Dim str_emp_cd As String = Session("emp_cd")
        mop_drp.Value = p_mop_cd 'lucy add
        lbl_Error.Text = String.Empty    'lucy add
        ' If mop_drp.SelectedIndex = 0 Then     'lucy comment
        'lbl_Error.Text = "You must select a payment type"
        ' Else
        If p_mop_cd = "FI" Then  'lucy  
            lucy_change_DropDownList1()
        End If

        lbl_pmt_amt.Text = txt_pmt_amt.Text.ToString()
        If ValidateAmount(lbl_Error, txt_pmt_amt.Text) Then
            lbl_Error.Text = String.Empty
            GridviewPmts.Enabled = True
            If (ProcessPayment()) Then
                txt_pmt_amt.Text = String.Format("{0:C}", 0)
                mop_drp.SelectedIndex = 0
                bindgrid()
                UpdateTotals()
                'disable the 'Go' button once a payment has been added to the payment grid
                btn_lookup_ivc.Enabled = False

            End If
        Else
            Dim s = "FFF"
        End If

        '  End If  lucy comment
    End Sub



    Public Function send_out_fi_request() As String
        'Lucy created new one

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = "PMT"
        Dim sessionid As String = Session.SessionID.ToString.Trim


        Dim strAsCd As String = Session("lucy_as_cd")

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return "No designated pinpad"
        End If

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()
            v_sub_type = ""
            Session("lucy_display") = ""
            Dim str_del_doc_num As String = Session("del_doc_num")
            If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
                str_del_doc_num = ""
            End If
            Session("del_doc_num") = ""
            Dim str_cust As String = Session("cust_cd")
            str_cust = txt_cust_cd.Text
            '  v_rpt_name ="IPRT"


            ' If TextBox1.Text & "" = "" And Len(TextBox1.Text) = 16 Then
            'v_card_num = TextBox1.Text
            ' End If

            v_auth_no = "          "    '10 spaces

            v_mop_cd = "FI"

            v_trans_type = "12"
            If Len(Trim(lbl_pmt_amt.Text)) = 0 Then
                lbl_pmt_amt.Text = Session("lucy_amt")
                '  End If
                ' v_amt = CDbl(txt_pmt_amt.Text)
                ' Else
            End If
            v_amt = CDbl(lbl_pmt_amt.Text)


            ' deferred payment 
            v_private_lbl_finance_tp = strAsCd
            '   v_private_lbl_finance_tp = "D"
            ' v_private_lbl_plan_num = "081"

            '  if l_data_rec.private_lbl.finance_tp ='D' then
            '  l_data_rec.private_lbl.grc_prd :=99;
            '  l_data_rec.private_lbl.noPayinMth :='00';
            '  equal payment 
            '  elsif	l_data_rec.private_lbl.finance_tp ='E' then 
            ' l_data_rec.private_lbl.noPayinMth :=99;
            ' l_data_rec.private_lbl.grc_prd :='00';
            ' end if;

            'Daniela Amex 15 digits change
            If Len(Trim(txt_fi_acct.Text)) >= 15 And IsNumeric(txt_fi_acct.Text) = True Then   'manualy enterred
                v_card_num = Trim(txt_fi_acct.Text)
                v_expire_date = txt_fi_exp_dt.Text
                str_s_or_m = "M"
            ElseIf Len(Trim(txt_fi_acct.Text)) >= 15 And IsNumeric(txt_fi_acct.Text) = False And
                (Len(Trim(Session("lucy_card_num"))) = 16 Or Len(Trim(Session("lucy_card_num"))) = 15) And
                IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
                v_card_num = Session("lucy_card_num")
                ' v_expire_date = txt_fi_exp_dt.Text
                v_card_num = ""   'force to swipe or need a exp_dt enterred
                v_expire_date = ""
                str_s_or_m = "S"
            Else    'swipe
                v_card_num = ""
                v_expire_date = ""
                str_s_or_m = "S"
            End If


            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
            'select lpad(glpmt_seq.nextval,12,'0') into v_trans_no from dual;
            ' peter will get v_trans_no in his procedure which is a sequence#
            ' myCMD.Parameters.Add(New OracleParameter("p_transTp", OracleType.VarChar)).Value = v_trans_type
            ' myCMD.Parameters.Add(New OracleParameter("p_authNo", OracleType.VarChar)).Value = v_auth_no




            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            'sabrina changed 5 to 10
            'myCMD.Parameters.Add("p_res", OracleType.VarChar, 5).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()



            ' If l_aut <> "empty" Then ' peter put empty means l_aut has null value



            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            v_trans_no = myCMD.Parameters("p_transTp").Value

            lbl_fi_warning.Text = myCMD.Parameters("p_dsp").Value
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value
                lucy_lbl_ind_aprv.ForeColor = Color.Green
                lucy_lbl_ind_aprv.Text = l_dsp
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then


                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_crn = myCMD.Parameters("p_crn").Value

                    l_filename = myCMD.Parameters("p_outfile").Value
                    l_printcmd = myCMD.Parameters("p_cmd").Value

                    Session("str_print_cmd") = l_printcmd
                    Session("auth_no") = l_aut
                    Session("lucy_approval_cd") = l_aut
                    Session("lucy_bnk_card") = Left(l_crn, 16)

                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                    print_receipt(l_printcmd)



                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                    txt_fi_appr.Text = l_aut
                    txt_fi_exp_dt.Text = Session("lucy_exp_dt")
                End If




            Else   'transation not done 


                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch
            Throw
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function td_send_out_fi_request() As String
        'Lucy created new one

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection


        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String

        Dim l_exp As String = Nothing
        Dim l_tr2 As String
        Dim l_aut As String

        Dim l_ack As String


        Dim v_term_id As String

        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String

        Dim v_auth_no As String
        Dim v_mop_cd As String
        Dim v_private_lbl_finance_tp As String
        Dim l_filename As String
        Dim l_printcmd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp As String = ""
        If cbo_payment_type.Value IsNot Nothing Then
            If cbo_payment_type.Value = "R" Then
                str_pmt_tp = "R"
            Else : str_pmt_tp = "DEP"
            End If
        Else : str_pmt_tp = "PMT"
        End If

        Dim sessionid As String = Session.SessionID.ToString.Trim

        Dim strAsCd As String = Session("lucy_as_cd")

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        ''Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            lbl_fi_warning.Text = "No designated pinpad"
            Return "empty"
        End If

        v_sub_type = ""
        Session("lucy_display") = ""
        Dim str_del_doc_num As String = Session("del_doc_num")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If
        Session("del_doc_num") = ""
        Dim str_cust As String = Session("cust_cd")
        str_cust = txt_cust_cd.Text

        v_auth_no = "          "    '10 spaces

        v_mop_cd = "FI"

        'v_trans_type = "12"
        If lbl_pmt_amt.Text Is Nothing Then
            lbl_pmt_amt.Text = Session("tdfRefundAmt")
        End If
        If Len(Trim(lbl_pmt_amt.Text)) = 0 Then
            lbl_pmt_amt.Text = Session("tdfRefundAmt")
            'lbl_pmt_amt.Text = Session("lucy_amt")
            '  End If
            ' v_amt = CDbl(txt_pmt_amt.Text)
            ' Else
        End If
        If lbl_pmt_amt.Text = 0 Then
            lbl_pmt_amt.Text = Session("tdfRefundAmt")
            'lbl_pmt_amt.Text = Session("lucy_amt")
            '  End If
            ' v_amt = CDbl(txt_pmt_amt.Text)
            ' Else
        End If
        v_amt = CDbl(lbl_pmt_amt.Text)

        ' deferred payment 
        v_private_lbl_finance_tp = strAsCd

        If Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = True Then   'manualy enterred
            v_card_num = Trim(txt_fi_acct.Text)
            If isEmpty(txt_fi_exp_dt.Text) Then
                lbl_fi_warning.Text = "Please enter expiry date"
                Return "empty"
            End If
            v_expire_date = txt_fi_exp_dt.Text
            'Session("lucy_card_num") = v_card_num
            'Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fi_acct.Text)) >= 15 And IsNumeric(txt_fi_acct.Text) = False And
            (Len(Trim(Session("lucy_card_num"))) = 16 Or Len(Trim(Session("lucy_card_num"))) = 15) And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated ' Daniela change from 16 to 15 for AMEX  to work
            v_card_num = Session("lucy_card_num")
            ' v_expire_date = txt_fi_exp_dt.Text
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        'Daniela prevent sending wrong exp_dt
        If isNotEmpty(v_expire_date) Then
            If Len((v_expire_date)) > 4 Then
                lbl_fi_warning.Text = "Please enter valid expiry date"
                Return "empty" 'Daniela 4 characters
            End If
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp8_td"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            'v_trans_no = myCMD.Parameters("p_transTp").Value

            lbl_fi_warning.Text = myCMD.Parameters("p_dsp").Value
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value
                lucy_lbl_ind_aprv.ForeColor = Color.Green
                lucy_lbl_ind_aprv.Text = l_dsp
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Or str_s_or_m = "M" Then ' manual trans included
                    'If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then

                    If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                        l_tr2 = myCMD.Parameters("p_tr2").Value
                    End If
                    If IsDBNull(myCMD.Parameters("p_exp").Value) = False Then
                        l_exp = myCMD.Parameters("p_exp").Value
                    End If
                    l_crn = myCMD.Parameters("p_crn").Value

                    l_filename = myCMD.Parameters("p_outfile").Value
                    l_printcmd = myCMD.Parameters("p_cmd").Value

                    Session("str_print_cmd") = l_printcmd
                    Session("auth_no") = l_aut
                    Session("lucy_approval_cd") = l_aut
                    Session("lucy_bnk_card") = Left(l_crn, 16)

                    'Dim str_month As String = Left(l_exp, 2)
                    'Dim str_year As String = Right(l_exp, 2)
                    'Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)
                    ' Daniela exp_dt fix
                    Dim str_year As String
                    Dim str_month As String
                    If str_s_or_m = "M" Then
                        str_month = Left(l_exp, 2)
                        str_year = Right(l_exp, 2)
                    Else
                        str_year = Left(l_exp, 2)
                        str_month = Right(l_exp, 2)
                    End If

                    'Daniela prevent Invalid Date yellow traingle
                    Try
                        Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/20" & str_year) ' Daniela fix 1946 issue
                    Catch ex As Exception
                        Session("lucy_exp_dt") = MonthLastDay("12/1/2049")
                    End Try
                    print_receipt(l_printcmd)

                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                    txt_fi_appr.Text = l_aut
                    txt_fi_exp_dt.Text = Session("lucy_exp_dt")
                End If
                ' Manual transaction 
                txt_fi_appr.Text = l_aut
                txt_fi_exp_dt.Text = ""


            Else   'transation not done 

                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                Session("lucy_exp_dt") = ""

                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            Return l_dsp

        Catch ex As Exception
            lbl_fi_warning.Text = Resources.LibResources.Label598
            Return "empty"
            'Throw New Exception("Error in td_send_out_fi_request() for terminal " & v_term_id & " and emp code " & Session("emp_cd"))
        Finally
            lbl_fi_warning.Text = Resources.LibResources.Label598
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function


    Protected Sub lucy_btn_save_fi_co_Click()
        ' lucy_lbl_ind_pay.Text = "SWIPE/INSERT IN POS TERMINAL"
        Dim strfinCo = cbo_finance_company.SelectedValue
        Dim paymType = cbo_payment_type.Value ' Daniela test
        Session("lucy_as_cd") = strfinCo

        Dim merchant_id As String = "" 'lucy add

        Dim lucy_fi_co As String
        ' lucy_fi_co = txt_fi_acct.Text  'lucy add
        ' ASPxPopupControl1.ShowOnPageLoad = True

        Dim str_display As String  'lucy add to call tenderretail
        ' Daniela
        If Session("lucy_mop_cd") = "TDF" Then
            str_display = td_send_out_fi_request()
        ElseIf Session("lucy_mop_cd") = "DCS" Then 'sabrina R1999
            str_display = sy_brk_process_visad() 'sabrina R1999
        Else
            str_display = send_out_fi_request()
        End If

        If InStr(str_display, "APPR") = 0 Then  'lucy add, not approved
            ' Response.Redirect("payment.aspx")      'lucy add
            ' ASPxPopupControl1.ShowOnPageLoad = False
            ' ASPxPopupControl2.ShowOnPageLoad = True     
            If isNotEmpty(str_display) And str_display <> "empty" Then
                lbl_fi_warning.Text = str_display 'sabrina R1999
            End If

            btn_save_fi_co.Enabled = True
            btn_save_fi_co.Visible = True
            lbl_fi_warning.Visible = True
            '    lucy_lbl_ind_pay.Text = "str_display" + str_display

            '  lucy_lbl_ind_pay.Text = "Please enter the card information and then click Save after finished for transmission"
            Exit Sub
        End If


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim cmd As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        'If lbl_pmt_tp.Text = "WDR" Then
        If Is_Finance_DP(lbl_pmt_tp.Text) = True Then
            bc = "FI"
        End If

        ' lucy copied the following from btn_save_fi_co_click the lower part
        lbl_GE_tran_id.Text = txt_fi_acct.Text  'lucy add
        lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            ' sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            ' sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"
            ' Daniela add bank card
            If Session("lucy_bnk_card") Is Nothing Then
                Session("lucy_bnk_card") = ""
            End If
            ' Daniela add exp for refunds
            Dim expDt As String
            If isEmpty(txt_fi_exp_dt.Text) And Session("lucy_exp_dt") IsNot Nothing Then
                expDt = Session("lucy_exp_dt")
            Else : expDt = txt_fi_exp_dt.Text
            End If

            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, BC, MOP_TP, FIN_CO, FI_ACCT_NUM, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :BC, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":sessionid", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
            cmd.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            cmd.Parameters.Add(":EXP_DT", OracleType.VarChar)
            cmd.Parameters.Add(":AMT", OracleType.VarChar)
            cmd.Parameters.Add(":DES", OracleType.VarChar)
            cmd.Parameters.Add(":BC", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_TP", OracleType.VarChar)
            cmd.Parameters.Add(":FIN_CO", OracleType.VarChar)
            cmd.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            ' cmd.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)  'lucy comment this otherwise there is an error
            cmd.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            cmd.Parameters.Add(":DL_STATE", OracleType.VarChar)
            cmd.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            cmd.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            cmd.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            cmd.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            cmd.Parameters(":EXP_DT").Value = expDt ' Daniela txt_fi_exp_dt.Text
            cmd.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            cmd.Parameters(":DES").Value = des
            cmd.Parameters(":BC").Value = Session("lucy_bnk_card") 'Daniela add bank card
            cmd.Parameters(":MOP_TP").Value = bc ' Daniela remove hard code "FI"
            cmd.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            cmd.Parameters(":FI_ACCT_NUM").Value = lbl_GE_tran_id.Text
            ' cmd.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue  'lucy comment this otherwise there is an error
            cmd.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            cmd.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            cmd.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            cmd.ExecuteNonQuery()
            'End If

            'Close Connection 
            conn.Close()


        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupFinancePmt.ShowOnPageLoad = False

        ' End If   'end if merchant id is not null

        'just update the grid and totals, instead of a total redirect to itself
        bindgrid()
        UpdateTotals()
        'Session("payment") = True

        ' make the button visible after swipe DB 20140722
        btn_save_fi_co.Enabled = True
        btn_save_fi_co.Visible = True


    End Sub
    Public Sub sy_print_instpmt(ByVal p_delDocNum As String,
                                ByVal p_emp_cd As String,
                                ByVal p_doc_seq_num As String,
                                ByVal p_pmt_store As String,
                                ByVal p_cust_cd As String)
        'sabrina instpmt-rcpt (new)
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.print_instpmt_rcpt"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_pmt_str", OracleType.Char)).Value = p_pmt_store
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = p_cust_cd

        'Daniela add french
        Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang
        'End Daniela

        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()

        Catch x
            objConnection.Close()
        End Try


    End Sub
    Public Sub sy_print_instpmt_at_printer(ByVal p_delDocNum As String,
                            ByVal p_emp_cd As String,
                            ByVal p_doc_seq_num As String,
                            ByVal p_pmt_store As String,
                            ByVal p_cust_cd As String,
                            ByVal p_printer As String)
        'sabrina R1910 (new sub)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.print_instpmt_rcpt_at_printer"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_pmt_str", OracleType.Char)).Value = p_pmt_store
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = p_cust_cd
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer

        Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang


        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()

        Catch x
            objConnection.Close()
        End Try


    End Sub
    Public Sub print_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByRef p_doc_seq_num As String, ByRef p_ufm As String, ByRef p_printer As String)
        ' Daniela created to add Ufm parameter
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.receipt_print"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_ufm", OracleType.Char)).Value = p_ufm

        'R2030 R1999 sabrina
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer
        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch x
            objConnection.Close()
        End Try

    End Sub


    'Public Sub lucy_print_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByRef p_doc_seq_num As String)
    '    ' Lucy created
    '    Dim connString As String
    '    Dim objConnection As New OracleConnection
    '    Dim x As Exception
    '    Dim Ds As New DataSet()
    '    Dim v_count As Integer = 1
    '    Dim strMsg As String
    '    Dim str_emp_cd As String

    '    connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
    '    objConnection = DisposablesManager.BuildOracleConnection(connString)
    '    objConnection.Open()

    '    ' Daniela comment, not needed in LIVE
    '    'lucy_log("from independent")
    '    'lucy_log(p_delDocNum)

    '    Dim myCMD As New OracleCommand()

    '    myCMD.Connection = objConnection
    '    ' myCMD.CommandText = "std_tenderretail_util.invoice_print"
    '    myCMD.CommandText = "std_tenderretail_util.receipt_print"
    '    myCMD.CommandType = CommandType.StoredProcedure

    '    myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
    '    myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
    '    myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
    '    ' Dim MyDA As New OracleDataAdapter(myCMD)

    '    Try

    '        'MyDA.Fill(Ds)
    '        myCMD.ExecuteNonQuery()
    '        objConnection.Close()

    '    Catch x
    '        ' lbl_Lucy_Test.Text = x.Message.ToString

    '        objConnection.Close()
    '    End Try
    'End Sub

    Protected Sub lucy_show_aprv()

        If InStr(UCase(Session("lucy_display")), "APPR") > 0 Then

            lucy_lbl_ind_aprv.ForeColor = Color.Green
            lucy_lbl_ind_aprv.Text = Session("lucy_display")    'lucy add

        ElseIf InStr(Session("lucy_display"), "DECLINE") > 0 Then

            lucy_lbl_ind_aprv.ForeColor = Color.Red
            lucy_lbl_ind_aprv.Text = Session("lucy_display")    'lucy add
        Else
            lucy_lbl_ind_aprv.Text = ""
            lucy_lbl_ind_aprv.ForeColor = Color.White
        End If

    End Sub

    Protected Sub lucy_change_DropDownList1()
        ' lucy add  , this dropdownlist1 is defined at designer time
        'If mop_drp.Value = "FI" Then ' Daniela coment, FI already checked
        Dim str_list As ListItem
        str_list = DropDownList1.Items.FindByValue("R")
        DropDownList1.Items.Remove(str_list)
        str_list = DropDownList1.Items.FindByValue("I")
        DropDownList1.Items.Remove(str_list)
        'End If
        ' Dim str_sel As String = DropDownList1.SelectedItem.Text
        ' If str_sel <> "OPEN" Then


    End Sub

    Protected Sub lucy_refund(sender As Object, e As EventArgs) Handles MyDataGrid.SelectedIndexChanged
        'lucy select 

        Dim textBoxRefundAmt As TextBox
        Dim selectChkBox As CheckBox
        Dim dgItem As DataGridItem

        Dim str_del_doc_num As String = txt_ivc_cd.Text
        Dim str_plan As String
        selectChkBox = CType(sender, CheckBox)

        Session("del_doc_num") = txt_ivc_cd.Text

        'first check if the total amount for all the refund pmts is within the allowed limit
        Dim availableRefundAmt As Double = Math.Abs(CalculateRefundableAmount())
        If (GetRefundPopupAmtTotal() > availableRefundAmt) Then
            lblRefundError.Text = GetRefundAmtErrorMsg(availableRefundAmt)
            'uncheck all the entries since the total exceeds the available amt.
            UncheckAllOrigRefundPayments()
        Else
            lblRefundError.Text = String.Empty
            'Iterate again to save the payment for the ones marked for selection
            '  For i As Integer = 0 To MyDataGrid.Items.Count - 1  'lucy comment
            ' dgItem = MyDataGrid.Items(i) 'lucy comment
            dgItem = CType(selectChkBox.NamingContainer, DataGridItem)
            ' selectChkBox = CType(dgItem.FindControl("ChkBoxSelectPmt"), CheckBox)  'lucy comment
            Dim str_mop As String
            Dim num_amt As Double
            'str_mop = dgItem.Cells(5).Text ' Daniela comment not needed
            str_mop = dgItem.Cells(4).Text

            If str_mop = PaymentUtils.Bank_MOP_CD.TDFinancial Then
                ' Daniela added to fix refund for TDF
                ' Daniela save TDF plan
                Session("lucy_mop_cd") = str_mop
                Dim tdPlanDes As String = dgItem.Cells(0).Text
                If isNotEmpty(tdPlanDes) Then
                    Dim tdPlan As String = Right(tdPlanDes, 5)
                    Session("tdf_plan") = tdPlan
                End If ' Daniela end
            ElseIf str_mop = PaymentUtils.Bank_MOP_CD.DESJARDINS Then 'R1999 sabrina (add)
                Session("lucy_mop_cd") = str_mop
                Dim v_dcsplandes As String = dgItem.Cells(0).Text
                If isNotEmpty(v_dcsplandes) Then
                    Dim v_dcsPlan As String = Right(v_dcsplandes, 3)
                    Session("dcs_plan") = v_dcsPlan
                End If 'R1999 sabrina end

                '  Alice added on Oct 10, 2018
            ElseIf str_mop = PaymentUtils.Bank_MOP_CD.FLEXITI Then 'R1999 sabrina (add)
                Session("lucy_mop_cd") = str_mop
                Dim v_flxplandes As String = dgItem.Cells(0).Text
                If isNotEmpty(v_flxplandes) Then
                    Dim v_flxPlan As String = Right(v_flxplandes, 3)
                    Session("flx_plan") = v_flxPlan
                End If
                '  Alice added on June 03, 2019 for reuqest 235
            ElseIf str_mop = PaymentUtils.Bank_MOP_CD.MCFinancial Then
                Session("lucy_mop_cd") = str_mop
                Dim v_fmplandes As String = dgItem.Cells(0).Text
                If isNotEmpty(v_fmplandes) Then
                    Dim v_fmPlan As String = Right(v_fmplandes, 3)
                    Session("fm_plan") = v_fmPlan
                End If

            End If

            textBoxRefundAmt = CType(dgItem.FindControl("txt_refund_amt"), TextBox)
            num_amt = textBoxRefundAmt.Text
            If str_mop = "FI" Then    ' FI gives negative value
                num_amt = textBoxRefundAmt.Text
                num_amt = Math.Abs(num_amt)
                dgItem.Cells(4).Text = "FV"   'finance void
                ' dgItem.Cells(5).Text = "FV"
                lucy_get_plan(str_del_doc_num, str_plan)
                Session("lucy_as_cd") = str_plan
            End If
            ' If (selectChkBox.Checked AndAlso CDbl(textBoxRefundAmt.Text) > 0) Then
            'sabrina R2159 - If (selectChkBox.Checked AndAlso num_amt > 0) Then
            If num_amt > 0 Then 'sabrina R2159
                ' Dim str_mop As String
                ' str_mop = dgItem.Cells(5).Text
                str_mop = dgItem.Cells(4).Text
                ' If str_mop = "FI" Or str_mop = "VI" Or str_mop = "AMX" Or str_mop = "MC" Or str_mop = "DC" Then  'lucy modified
                txt_pmt_amt.Text = num_amt
                'PopupOrigRefundPmts.ShowOnPageLoad = False
                'sabrina add if 
                If str_mop <> "GC" Then
                    lucy_btn_add_Click(str_mop) ' it does not work because the credit_card_processing page will not pop up , if use redirect("credit_card_processing.aspx"), no value passed
                    'lucy_btn_add_Click(str_mop) 'because use need to click twice (because add_payment btn), by calling twice, it is like click twice ' Daniela comment
                End If

            End If
            ' Next  lucy comment 
            PopupOrigRefundPmts.ShowOnPageLoad = False
            ' bindgrid() 'lucy comment
            ' UpdateTotals()  'lucy comment
            'disable the 'Go' button once a payment has been added or not to the payment grid
            ' btn_lookup_ivc.Enabled = False 'lucy comment
            btn_lookup_ivc.Enabled = True 'lucy
        End If
    End Sub




    Protected Sub lucy_btn_new_cust_Click(sender As Object, e As EventArgs) Handles lucy_btn_new_cust.Click
        ' lucy_lbl_ind_pay.Text = "SWIPE/INSERT IN POS TERMINAL"
        Dim strfinCo = cbo_finance_company.SelectedValue
        Session("lucy_as_cd") = strfinCo

        Dim merchant_id As String = "" 'lucy add

        Dim lucy_fi_co As String
        Dim str_display As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim cmd As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'sabrina R1999
        If lbl_pmt_tp.Text = "DCS" Then
            'sabrina NOV20
            If txt_fi_acct.Text & "" <> "" Then
                lbl_fi_warning.Text = Resources.LibResources.Label838 'sabrina-french dec14 "Cannot proceed-Account# not allowed for NEW CUSTOMER"
                txt_fi_acct.Focus()
                Exit Sub
            End If
            'sabrina Nov20
            txt_fi_acct.Enabled = False
            txt_fi_exp_dt.Enabled = False
            If txt_fi_appr.Text & "" = "" Then
                txt_fi_appr.Focus()
                lbl_fi_warning.Text = Resources.LibResources.Label836 'sabrina-french dec14 "Auth# Required"
                Exit Sub
            Else
                If Len(txt_fi_appr.Text) < 5 Or Len(txt_fi_appr.Text) > 10 Then
                    txt_fi_appr.Focus()
                    lbl_fi_warning.Text = Resources.LibResources.Label1013 'sabrina-french dec14 "Auth# must be between 5 - 10 characters"
                    lbl_fi_warning.Visible = True
                    Exit Sub
                End If
            End If

            If LeonsBiz.isvalidDCSplan(cbo_finance_company.SelectedValue()) <> "Y" Then
                cbo_finance_company.Focus()
                lbl_fi_warning.Text = Resources.LibResources.Label861 'sabrina-french dec14 "Select a Plan"
                Exit Sub
            End If
        End If
        'sabrina R1999 END         

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        'If lbl_pmt_tp.Text = "WDR" Then
        If Is_Finance_DP(lbl_pmt_tp.Text) = True Then
            bc = "FI"
        End If

        'sabrina R1999
        If lbl_pmt_tp.Text = "DCS" Then
            bc = "BC"
        End If
        'sabrina R1999 END        

        lbl_GE_tran_id.Text = txt_fi_acct.Text  'lucy add
        lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE") 'lucy
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            ' sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            ' sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"
            If lbl_pmt_tp.Text = "DCS" Then
                sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,  APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
                sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,   :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"
            Else
                sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,   DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
                sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,   :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"
            End If
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":sessionid", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
            cmd.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            cmd.Parameters.Add(":EXP_DT", OracleType.VarChar)
            cmd.Parameters.Add(":AMT", OracleType.VarChar)
            cmd.Parameters.Add(":DES", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_TP", OracleType.VarChar)
            cmd.Parameters.Add(":FIN_CO", OracleType.VarChar)
            cmd.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            ' cmd.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)  'lucy comment this otherwise there is an error
            cmd.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            cmd.Parameters.Add(":DL_STATE", OracleType.VarChar)
            cmd.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            cmd.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            cmd.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            cmd.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            cmd.Parameters(":EXP_DT").Value = txt_fi_exp_dt.Text
            cmd.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            cmd.Parameters(":DES").Value = des

            'SABRINA R1999
            If lbl_pmt_tp.Text = "DCS" Then
                cmd.Parameters(":MOP_TP").Value = "BC"
                cmd.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            Else
                cmd.Parameters(":MOP_TP").Value = "FI"
            End If
            cmd.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            '  cmd.Parameters(":FI_ACCT_NUM").Value = lbl_GE_tran_id.Text
            ' cmd.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue  'lucy comment this otherwise there is an error
            '  cmd.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            cmd.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            cmd.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            cmd.ExecuteNonQuery()
            'End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupFinancePmt.ShowOnPageLoad = False

        ' End If   'end if merchant id is not null

        'just update the grid and totals, instead of a total redirect to itself
        bindgrid()
        UpdateTotals()
        'Session("payment") = True
    End Sub

    Private Function ActivateIndependentPaymentCashier(ByVal empCd As String) As String
        ' input should be cashier
        Dim errMsg As String = ""
        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = "SELECT ca.CSHR_INIT, ca.CO_CD, ca.STORE_CD, ca.CSH_DWR_CD, ca.POST_DT " &
              "FROM CSHR_ACTIVATION ca, emp " +
              "WHERE ca.CSHR_INIT = emp.emp_init AND emp.emp_cd = :EMPCD"

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objSql.Parameters.Add(":EMPCD", OracleType.VarChar)
        objSql.Parameters(":EMPCD").Value = UCase(empCd)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                cbo_csh_drw.SelectedItem = cbo_csh_drw.Items.FindByValue(MyDataReader.Item("CSH_DWR_CD").ToString)
                cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(MyDataReader.Item("STORE_CD").ToString)
                ViewState("co_cd") = MyDataReader.Item("CO_CD").ToString
                If IsDate(MyDataReader.Item("POST_DT").ToString) Then
                    lbl_post_dt.Text = FormatDateTime(MyDataReader.Item("POST_DT").ToString, DateFormat.ShortDate)
                End If
                errMsg = MyDataReader.Item("CSHR_INIT").ToString
            Else
                errMsg = "INVALIDCASHIER"
            End If

            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
        Return errMsg
    End Function
    Private Sub ValidateLoadCasherDetails()
        Dim errMsg As String
        If SysPms.isCashierActiv And Request("LEAD") <> "TRUE" Then

            errMsg = ActivateIndependentPaymentCashier(lbl_emp_cd.Text.Trim)

            txt_ivc_cd.Enabled = True
            If errMsg = "INVALIDCASHIER" Then
                lbl_Error.Text = "No records found for this cashier"
                lbl_Error.Visible = True
                btn_add.Enabled = False
                btn_commit.Enabled = False
                txt_ivc_cd.Enabled = False
            Else
                'cbo_csh_drw.SelectedItem = cbo_csh_drw.Items.FindByValue(Session("csh_dwr_cd"))
                'cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(Session("store_cd"))
                'lbl_post_dt.Text = Session("tran_dt")
                cbo_csh_drw.Enabled = False
                cbo_store_cd.Enabled = False
                btn_add.Enabled = True
                btn_commit.Enabled = True
            End If
            If cbo_csh_drw.SelectedItem Is Nothing Then
                cbo_store_cd.Items.Clear()
                cbo_store_cd.Value = ""
            End If
            Dim ShuDt As String = SessVar.shuDt
            If IsDate(lbl_post_dt.Text) Then
                If FormatDateTime(lbl_post_dt.Text) < Today.Date.AddDays(-SysPms.numDaysPostDtTrans) Then
                    errMsg = "INVALIDCASHIER"
                    lbl_Error.Text = "Your post date is less than the minimum allowed transaction date"
                    lbl_Error.Visible = True
                    btn_add.Enabled = False
                    btn_commit.Enabled = False
                    txt_ivc_cd.Enabled = False
                ElseIf FormatDateTime(lbl_post_dt.Text) > Today.Date.AddDays(SysPms.numDaysPostDtTrans) Then
                    errMsg = "INVALIDCASHIER"
                    lbl_Error.Text = "Your post date is greater than the maximum allowed transaction date"
                    lbl_Error.Visible = True
                    btn_add.Enabled = False
                    btn_commit.Enabled = False
                    txt_ivc_cd.Enabled = False
                ElseIf IsDate(ShuDt) AndAlso DateDiff("d", FormatDateTime(lbl_post_dt.Text, DateFormat.ShortDate), ShuDt) >= 0 Then
                    errMsg = "INVALIDCASHIER"
                    lbl_Error.Text = "Your post date must be later than the SHU date --  Please activate a cash drawer for a new date."
                    lbl_Error.Visible = True
                    btn_add.Enabled = False
                    btn_commit.Enabled = False
                    txt_ivc_cd.Enabled = False
                End If
            End If
            ' MM-6376
            ValidateCashDrawer()
        ElseIf errMsg & "" <> "" Then
            'if the cashier initials are not empty
            cbo_csh_drw.Enabled = False
            cbo_store_cd.Enabled = False
            btn_add.Enabled = False
            btn_commit.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Sets the payment type dd value to the appropiate one based on seecurities and 
    ''' the context of the calling page.
    ''' </summary>
    Private Sub SetPaymentType(ByVal empCD As String)
        If Request("pmt_tp_cd") & "" <> "" Then
            cbo_payment_type.SelectedItem = cbo_payment_type.Items.FindByValue(Request("pmt_tp_cd"))
        Else
            'cbo_payment_type.SelectedItem = cbo_payment_type.Items.FindByValue("PMT") ' Daniela to keep the selected value
        End If

        'check securities
        If SecurityUtils.hasSecurity(SecurityUtils.REFUND_ENTRY, empCD) Then
            cbo_payment_type.Enabled = True
        Else
            cbo_payment_type.Enabled = False
            cbo_payment_type.Value = "PMT"
            lbl_Error.Text = "YOU ARE NOT AUTHORIZED TO ENTER REFUNDS"
        End If

        If cbo_payment_type.Value = "R" Then
            'lbl_Error.Text = "PROCESSING A REFUND"
            lbl_Error.Text = Resources.LibResources.Label437.ToUpper
        Else
            lbl_Error.Text = String.Empty
        End If
    End Sub

    ''' <summary>
    ''' Enables/disables various components when a terminal is locked.
    ''' </summary>
    Private Sub SetPropertiesForLockedTerminal()
        lbl_co_cd.Visible = False
        cbo_store_cd.Enabled = False
        cbo_csh_drw.Enabled = False
        txt_ivc_cd.Enabled = False
        GridviewPmts.Enabled = False
        btn_add.Enabled = False
        txt_pmt_amt.Enabled = False
        mop_drp.Enabled = False
        txt_cshr_pwd.Enabled = False
        Label5.Visible = True
        Label5.Text = "This terminal has been closed; payments cannot be processed"

    End Sub

    ''' <summary>
    ''' Intializes the balances section to zero amounts
    ''' </summary>
    Private Sub ResetBalances()

        lbl_current_balance.Text = String.Format("{0:C}", 0)
        lbl_current_pmts_amt.Text = String.Format("{0:C}", 0)
        lbl_new_pmts_amt.Text = String.Format("{0:C}", 0)

    End Sub

    ''' <summary>
    ''' Updates the totals amount, such as balance due, in-process payments, etc. for the current order.
    ''' </summary>
    Private Sub UpdateTotals()

        If (txt_ivc_cd.Text.isNotEmpty) Then

            Dim currentBalance As Double = PaymentUtils.GetBalance(txt_cust_cd.Text, txt_ivc_cd.Text)
            Dim currentPmtTotal As Double = PaymentUtils.GetInProcessPaymentAmt(Session.SessionID.ToString.Trim)

            If cbo_payment_type.Value = "R" Then
                ASPxLabel11.Text = "Refunds:"
                currentPmtTotal = currentPmtTotal * -1
            Else
                ASPxLabel11.Text = "Payments:"
            End If

            lbl_current_balance.Text = String.Format("{0:C}", currentBalance)
            lbl_current_pmts_amt.Text = String.Format("{0:C}", currentPmtTotal, 2)

            'display the net of the balance and any new payments made in this session
            lbl_new_pmts_amt.Text = String.Format("{0:C}", currentBalance - currentPmtTotal, 2)

            'sabrina tdpl
            If Session("td_instpmt") = "Y" Then
                ' Session("tdpl_amt") = 'String.Format(currentPmtTotal)
                Session("tdpl_amt") = FormatNumber(CDbl(currentPmtTotal), 2)
            End If

        Else
            ResetBalances()
        End If

    End Sub


    ''' <summary>
    ''' Updates the order status based on the order type and status
    ''' </summary>
    Private Sub UpdateStatus()

        If (txt_ivc_cd.Text.isNotEmpty) Then

            Dim orderStatus As String = String.Empty
            Dim pkpDelType As String = String.Empty

            btn_add.Enabled = True
            txt_pmt_amt.Enabled = True
            mop_drp.Enabled = True

            Try
                'retrieve the order status, type, etc. from the db
                Dim dTable As DataTable = OrderUtils.GetOrderDetails(txt_ivc_cd.Text)
                If dTable.Rows.Count > 0 Then
                    Dim dr As DataRow = dTable.Rows(0)
                    orderStatus = dr("STAT_CD")
                    pkpDelType = dr("PU_DEL")

                    ' Daniela
                    ' If pkpDelType = "P" And orderStatus <> AppConstants.Order.STATUS_VOID And InStr(LCase(Request.ServerVariables("HTTP_HOST")), "levin") > 0 Then
                    If pkpDelType = "P" And orderStatus <> AppConstants.Order.STATUS_VOID Then
                        btn_pickup.Enabled = True
                    Else
                        btn_pickup.Enabled = False
                    End If
                    Select Case orderStatus
                        Case AppConstants.Order.STATUS_OPEN
                            Label8.Text = "OPEN"
                        Case AppConstants.Order.STATUS_FINAL
                            Label8.Text = "FINALIZED"
                        Case AppConstants.Order.STATUS_VOID
                            Label8.Text = "VOIDED"
                    End Select
                    hdnOrderStatus.Value = orderStatus

                End If

            Catch ex As Exception
                Throw
            End Try
        End If

    End Sub


    ''' <summary>
    ''' Validates that the cashier/emp password entered is valid and if it is, it retrieves the other
    ''' emp details such as emp home store, emp code, etc. and defaults the values in the store dd, which
    ''' in turn populates the list of cash drawers for that store.
    ''' </summary>
    Private Sub ValidateCashier()

        Dim SQL As String
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connErp.Open()

        Label7.Text = txt_cshr_pwd.Text
        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.APP_PASS='" & UCase(txt_cshr_pwd.Text.ToString) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='CSH'"
        SQL = UCase(SQL)

        cmd = DisposablesManager.BuildOracleCommand(SQL, connErp)

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            If (reader.Read()) Then
                Label5.ForeColor = Color.Black
                Label5.Text = reader.Item("FNAME").ToString & " " & reader.Item("LNAME").ToString
                lbl_emp_cd.Text = reader.Item("EMP_CD").ToString
                PopulateStoreCode()
                If Request("ip_Store_cd") & "" <> "" Then
                    If Not IsNothing(cbo_store_cd.Items.FindByValue(Request("ip_store_cd"))) Then
                        cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(Request("ip_store_cd"))
                    End If
                Else
                    If Not IsNothing(cbo_store_cd.Items.FindByValue(reader.Item("HOME_STORE_CD").ToString)) Then
                        cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(reader.Item("HOME_STORE_CD").ToString)
                    End If
                End If
                PopulateCashDrawer()
                cbo_store_cd.Enabled = True
                cbo_csh_drw.Enabled = True
                txt_cust_cd.Enabled = True
                btn_lookup_ivc.Enabled = True
                btnOrderSearch.Enabled = True
            Else
                If IsPostBack Then
                    Label5.Text = "Invalid Password"
                    Label5.ForeColor = Color.Red
                End If
                cbo_store_cd.Enabled = False
                cbo_csh_drw.Enabled = False
                txt_cust_cd.Enabled = False
                btn_lookup_ivc.Enabled = False
                btnOrderSearch.Enabled = False
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            connErp.Close()
        End Try
    End Sub

    Protected Sub txt_cshr_pwd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cshr_pwd.TextChanged
        ' Daniela reset message
        lbl_pickticket.Text = ""
        If Label7.Text.isNotEmpty Then
            If txt_cshr_pwd.Text.isNotEmpty Then
                'if the new cashier pwd and the previous one are different, then validate cashier
                If Label7.Text.Trim.ToUpper <> txt_cshr_pwd.Text.Trim.ToUpper Then ' Daniela password not case sensitive
                    ValidateCashier()
                    SetPaymentType(lbl_emp_cd.Text)
                    ValidateLoadCasherDetails()
                    Exit Sub
                Else
                    ' Daniela set password validation flag
                    Session("csh_pss_flag") = "Y"
                    Exit Sub
                End If
            Else
             
                Exit Sub
            End If
        End If
        ValidateCashier()
        SetPaymentType(lbl_emp_cd.Text)
        ValidateLoadCasherDetails()
    End Sub

    ''' <summary>
    ''' Populates the store drop down
    ''' </summary>
    Private Sub PopulateStoreCode()

        Dim SQL As String
        Dim store_cd As String = ""
        If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then

            Dim cmd As OracleCommand
            Dim reader As OracleDataReader
            Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            connLocal.Open()

            SQL = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
            SQL = SQL & " OR TERMINAL_IP = '" & Left(Request.ServerVariables("REMOTE_ADDR"), Find_First_IP(Request.ServerVariables("REMOTE_ADDR"))) & "' GROUP BY STORE_CD, CASH_DWR_CD"
            SQL = UCase(SQL)

            cmd = DisposablesManager.BuildOracleCommand(SQL, connLocal)
            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                Do While reader.Read
                    store_cd = store_cd & "'" & reader.Item("STORE_CD").ToString & "',"
                Loop
            Catch
                Throw
            Finally
                connLocal.Close()
            End Try
        End If

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connErp.Open()

        SQL = "SELECT STORE.STORE_CD, STORE.STORE_NAME, STORE.STORE_CD || ' - ' || STORE.STORE_NAME as full_desc FROM STORE, CSH_DWR "
        If Not String.IsNullOrEmpty(Replace(store_cd, ",", "")) Then
            SQL = SQL & "WHERE STORE_CD IN(" & Left(store_cd, Len(store_cd) - 1) & ") "
        End If
        If InStr(SQL, "WHERE") > 0 Then
            SQL = SQL & "AND CSH_DWR.STORE_CD=STORE.STORE_CD "
        Else
            SQL = SQL & "WHERE CSH_DWR.STORE_CD=STORE.STORE_CD "
        End If
        SQL = SQL & "GROUP BY STORE.STORE_CD, STORE.STORE_NAME order by STORE.STORE_CD"

        Try
            With cmdGetStores
                .Connection = connErp
                .CommandText = SQL
            End With

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            Throw
        Finally
            connErp.Close()
        End Try
        GetCompanyCode()

    End Sub

    ''' <summary>
    ''' Selects the company code associated with the selected store.
    ''' </summary>
    Private Sub GetCompanyCode()

        If cbo_store_cd.Value <> "" Then

            Try
                lbl_co_cd.Text = theSystemBiz.GetCompanyCd(cbo_store_cd.Value)
            Catch ex As Exception
                Throw
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Populates the cash drawers for the selected store
    ''' </summary>
    Private Sub PopulateCashDrawer()

        Dim SQL As String
        Dim store_cd As String = ""

        If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "O" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
            Dim cmd As OracleCommand
            Dim reader As OracleDataReader
            Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            connLocal.Open()

            SQL = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP='" & Request.ServerVariables("REMOTE_ADDR") & "'"
            SQL = SQL & " AND STORE_CD='" & cbo_store_cd.Value & "' "
            SQL = SQL & " OR TERMINAL_IP = '" & Left(Request.ServerVariables("REMOTE_ADDR"), Find_First_IP(Request.ServerVariables("REMOTE_ADDR"))) & "' "
            SQL = SQL & " AND STORE_CD='" & cbo_store_cd.Value & "' "
            SQL = SQL & "GROUP BY STORE_CD, CASH_DWR_CD"
            SQL = UCase(SQL)

            cmd = DisposablesManager.BuildOracleCommand(SQL, connLocal)
            Try
                'Execute DataReader 
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                Do While reader.Read
                    store_cd = store_cd & "'" & reader.Item("CASH_DWR_CD").ToString & "',"
                Loop
            Catch
                connLocal.Close()
                Throw
            End Try
            connLocal.Close()
        End If


        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        SQL = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE store_cd='" & cbo_store_cd.Value & "' "
        If Not String.IsNullOrEmpty(Replace(store_cd, ",", "")) Then
            SQL = SQL & "AND csh_dwr_cd IN(" & Left(store_cd, Len(store_cd) - 1) & ") "
        End If
        SQL = SQL & "ORDER by csh_dwr_cd"

        Try
            With cmdGetStores
                .Connection = connErp
                .CommandText = SQL

            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_csh_drw
                .DataSource = ds
                .ValueField = "csh_dwr_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
            cbo_csh_drw.SelectedIndex = 0
        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        connErp.Close()

    End Sub

    Protected Sub cbo_store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store_cd.SelectedIndexChanged
        PopulateCashDrawer()
        bindgrid()
    End Sub

    ''' <summary>
    ''' Fires when the selection in the 'Payment Type' drop-down changes. It indicates whether a 
    ''' refund or payment is going to be processed and does corresponding validations.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub cbo_payment_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_payment_type.SelectedIndexChanged
        lucy_lbl_ind_aprv.Text = ""    'lucy
        If (txt_ivc_cd.Text.isNotEmpty) Then

            If (txt_ivc_cd.Text.Trim.ToUpper = Resources.LibResources.Label373.ToUpper) Then 'Alice add for request 167
                lbl_Error.Text = ""
            Else

            Dim proceed As Boolean = True
            If (cbo_payment_type.Value = "R") Then
                proceed = CanProcessRefunds(txt_ivc_cd.Text)
            Else
                proceed = CanProcessPayments(txt_ivc_cd.Text)
            End If

            If (Not proceed) AndAlso (txt_ivc_cd.Text.isNotEmpty) Then
                txt_ivc_cd.Focus()
                ResetBalances()
            End If

            '***** NOTE: ideally there should be a confirmation msg asking user if they really want to change to refund/pmts 'coz 
            ' all the related data will be deleted too. But this can only be done via Jscript or some custom code. So, until that
            ' is in place, need to just clear the grid and delete any temp pmt record from the db.
            thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)
            mop_drp.Items.Clear()
            GridviewPmts.DataSource = ""
            GridviewPmts.DataBind()
            GridviewPmts.Enabled = proceed
            GridviewPmts.Visible = proceed
            btn_add.Enabled = proceed
            txt_pmt_amt.Enabled = proceed
            End If

        End If

    End Sub


    ''' <summary>
    ''' Gets fired when user clicks on the 'GO' button, typically after entering 
    ''' the del-doc#/ivc#/order#
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btn_lookup_ivc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_lookup_ivc.Click

        If Session("OtherInvoice") = "Y" And txt_ivc_cd.Text = txtInvoiceNum.Text.Trim().ToUpper Then   'Alice add for request 167
            AddCustInfo(txtCustCode.Text.Trim())
            Exit Sub
        Else

            ' Daniela reset message
            lbl_pickticket.Text = ""
            Dim proceed As Boolean = False
            If (txt_ivc_cd.Text.isNotEmpty) Then

                'Alice add for request 167 --start
                If txt_ivc_cd.Text.Trim.ToUpper = Resources.LibResources.Label373.ToUpper Then

                    If cbo_payment_type.Value = "R" Then
                        lbl_Error.Text = Resources.LibResources.Label1034
                    Else
                        txtCustCode.Text = ""
                        txtInvoiceNum.Text = ""
                        ASPxPopupOtherInvoice.ShowOnPageLoad = True

                        If Label7.Text.isNotEmpty Then
                            Session("CashhierPwd") = Encrypt(Label7.Text, "CrOcOdIlE")
                        End If
                    End If
                    Exit Sub

                End If
                'request 167 -- end

                ' Daniela set del doc upper case
                txt_ivc_cd.Text = UCase(txt_ivc_cd.Text)

            'Daniela check for lock before payments are added
            Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
            Dim soRowid As String = String.Empty

            Try
                dbConn.Open()
                dbCmd.Transaction = dbConn.BeginTransaction()

                soRowid = SalesUtils.LockSoRec(txt_ivc_cd.Text, dbCmd)
                dbCmd.Transaction.Commit()
            Catch ex As Exception
                dbCmd.Transaction.Rollback()
                lbl_Error.Text = ex.Message
                btn_add.Enabled = proceed
                txt_pmt_amt.Enabled = proceed
                mop_drp.Enabled = proceed
                btn_add.Enabled = proceed
                btn_show_invoices.Enabled = proceed
                txt_pmt_amt.Enabled = proceed
                mop_drp.Enabled = proceed
                btn_pickup.Enabled = proceed
                Exit Sub

            Finally
                If Not IsNothing(dbCmd.Transaction) Then
                    dbCmd.Transaction.Dispose()
                End If
                dbCmd.Dispose()
                dbConn.Close()
                dbConn.Dispose()
            End Try
            'Daniela end check for lock on so table            

            If OrderValidator.IsValid Then

                AddCustOrderInfo()
                UpdateStatus()
                proceed = DisplayMops()
                If proceed Then proceed = DisplayRefundsPopup()
                If proceed Then UpdateTotals()

                If txt_cust_cd.Text.isNotEmpty And lbl_qs_avail.Text.isEmpty And ConfigurationManager.AppSettings("quick_screen").ToString = "Y" Then
                    CheckCustomerQuickCredit()
                End If
            Else
                'order was not valid, so clear & reset everything
                thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)
                bindgrid()
                ResetInfo()
            End If

            btn_add.Enabled = proceed
            txt_pmt_amt.Enabled = proceed
            mop_drp.Enabled = proceed
            btn_add.Enabled = proceed
            btn_show_invoices.Enabled = proceed
            txt_pmt_amt.Enabled = proceed
            mop_drp.Enabled = proceed
            End If
        End If
    End Sub

    ''' <summary>
    ''' Method called by the Order validator to verify if the order entered is valid based on the 
    ''' order status and type.
    ''' </summary>
    Protected Sub ValidateOrder(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)

        'since the order# is changing, clear out the session variables that contain any orig payment info
        _listOrigPmtsForRefundsDd = Nothing
        _dsOrigPmtsForRefunds = Nothing
        _hTableOrigPmtAmtsForRefunds = Nothing
        _dtRefundDetails = Nothing

        If (cbo_payment_type.Value = "R") Then
            args.IsValid = CanProcessRefunds(args.Value.Trim)
            'sabrina cncRefund 
            If Not args.IsValid Then
                If Session("cnc_refund_proceed") = "Y" Then
                    cnc_del_doc_num.Text = UCase(txt_ivc_cd.Text)
                    cnc_msg.Text = ""
                    Session("cnc_ar_trn_found_sw") = "Y"
                    sy_cnc_set_info(UCase(txt_ivc_cd.Text))

                    If Session("cnc_ar_trn_found_sw") = "Y" Then
                        ASPxPopupcncrefund.ShowOnPageLoad = True
                    End If

                End If
            End If
            'sabrina cncRefund end
        Else

            'Alice add for request 167 --start
            If Session("OtherInvoice") = "Y" And txt_ivc_cd.Text = txtInvoiceNum.Text.Trim().ToUpper Then
                AddCustInfo(txtCustCode.Text.Trim())
                DisplayMops()
                btn_add.Enabled = True
                txt_pmt_amt.Enabled = True
                mop_drp.Enabled = True
                args.IsValid = True

                Exit Sub
            End If

            If txt_ivc_cd.Text.Trim.ToUpper = Resources.LibResources.Label373.ToUpper Then
                Exit Sub
            End If
            ' request 167 -- end


            'lucy add foloowing if condition lines
            'sabrina tdpl moved if stmt
            'txt_ivc_cd.Text = UCase(txt_ivc_cd.Text)
            'If lucy_check_del_doc_num(txt_ivc_cd.Text, Session("emp_cd")) = "N" Then
            '    lbl_Error.Text = "You have entered an invalid Order"
            '    Exit Sub
            'End If
            txt_ivc_cd.Text = UCase(txt_ivc_cd.Text)
            Dim str_ivc As String = txt_ivc_cd.Text
            If (InStr(str_ivc, "VISA") Or InStr(str_ivc, "PAI") Or InStr(str_ivc, "DESJ")) And Not IsNumeric(Left(str_ivc, 1)) Then
                ' Daniela start - fix printing issue by default VISAD May 01
                txt_ivc_cd.Text = "VISAD PAYMENT"
                str_ivc = "VISAD PAYMENT"
                ' Daniela end
                Dim str_amt As String = 0
                Dim str_ivc_cd As String = "VISAD PAYMENT"
                Dim URL_String As String
                Session("csh_pss") = Encrypt(Label7.Text, "CrOcOdIlE")
                Session("ivc_cd") = txt_ivc_cd.Text
                Session("csh_drw") = cbo_csh_drw.Value
                Session("store_cd") = cbo_store_cd.Value
                '  URL_String = "?ip_ord_total=" & lbl_current_pmts_amt.Text & "&refresh=true" & "&ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&ip_store_cd=" & cbo_store_cd.Value & "&ip_cust_cd=" & txt_cust_cd.Text & "&ip_ivc_cd=" & txt_ivc_cd.Text & "&ip_csh_drw=" & cbo_csh_drw.Value & "&pmt_tp_cd=" & cbo_payment_type.Value
                Response.Redirect("vd_inspmt.aspx")
                'sabrina tdpl
            ElseIf InStr(str_ivc, "TDPL") > 0 Or InStr(str_ivc, "TD PL") Or InStr(str_ivc, "BRICK") > 0 Then
                Dim str_amt As String = 0
                Dim str_ivc_cd As String = "TD PL"
                Dim URL_String As String
                Session("csh_pss") = Encrypt(Label7.Text, "CrOcOdIlE")
                Session("ivc_cd") = txt_ivc_cd.Text
                Session("csh_drw") = cbo_csh_drw.Value
                Session("store_cd") = cbo_store_cd.Value
                URL_String = "?ip_ord_total=" & lbl_current_pmts_amt.Text & "&refresh=true" & "&ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&ip_store_cd=" & cbo_store_cd.Value & "&ip_cust_cd=" & txt_cust_cd.Text & "&ip_ivc_cd=" & txt_ivc_cd.Text & "&ip_csh_drw=" & cbo_csh_drw.Value & "&pmt_tp_cd=" & cbo_payment_type.Value

                'Alice disabled the ability to make a TD "BRICK PAYMENT" on Mar 20, 2019 for request 4597
                ' Response.Redirect("tdpl.aspx")

            ElseIf lucy_check_del_doc_num(txt_ivc_cd.Text, Session("emp_cd")) = "N" Then
                lbl_Error.Text = "You have entered an invalid Order"
                Exit Sub
                'ElseIf InStr(str_ivc, "TD") Then
                '    Dim str_amt As String = 0
                '    Dim str_ivc_cd As String = "TD PL"
                '    Dim URL_String As String
                '    Session("csh_pss") = Encrypt(Label7.Text, "CrOcOdIlE")
                '    Session("ivc_cd") = txt_ivc_cd.Text
                '    Session("csh_drw") = cbo_csh_drw.Value
                '    Session("store_cd") = cbo_store_cd.Value
                '    '  URL_String = "?ip_ord_total=" & lbl_current_pmts_amt.Text & "&refresh=true" & "&ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&ip_store_cd=" & cbo_store_cd.Value & "&ip_cust_cd=" & txt_cust_cd.Text & "&ip_ivc_cd=" & txt_ivc_cd.Text & "&ip_csh_drw=" & cbo_csh_drw.Value & "&pmt_tp_cd=" & cbo_payment_type.Value
                '    Response.Redirect("vd_inspmt.aspx")
            Else
                args.IsValid = CanProcessPayments(args.Value)
            End If
            args.IsValid = CanProcessPayments(args.Value.Trim)
        End If

        btn_add.Enabled = args.IsValid
    End Sub

    ''' <summary>
    ''' Determines if the refund can be processed based on the order status and type of order. For instance,
    ''' refunds can only be made to finalized CRM doc.
    ''' </summary>
    ''' <param name="delDocNum">the order to be processed.</param>
    Private Function CanProcessRefunds(ByVal delDocNum As String) As Boolean

        Dim rtnVal As Boolean = True
        Dim sy_credit_amt As Double = 0 'sabrina cncrefund

        'get order details- this will also validate if order is in the system or not
        Dim dTableOrderDetails As DataTable = OrderUtils.GetOrderDetails(UCase(delDocNum))
        If dTableOrderDetails.Rows.Count > 0 Then

            'means that the CRM/MCR order is on file. Now make sure it is a final doc...(phase1 of refunds)
            Dim dRow As DataRow = dTableOrderDetails.Rows(0)
            Dim isCrmOrMcr As Boolean = (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR OrElse
                                         dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM)

            If (isCrmOrMcr AndAlso dRow("STAT_CD") <> AppConstants.Order.STATUS_FINAL) Then
                lbl_Error.Text = "Only finalized CRM or MCR orders can be refunded. Please enter another order."
                rtnVal = False
            Else
                'lbl_Error.Text = "PROCESSING A REFUND"
                lbl_Error.Text = Resources.LibResources.Label437.ToUpper
            End If
        Else
            lbl_Error.Visible = True
            'sabrina cncRefund add 
            Session("cnc_orig_refund_amt") = String.Format("{0:C}", 0)
            sy_credit_amt = sy_cnc_get_max_refund(UCase(delDocNum))

            If sy_credit_amt > 0 Then
                If sy_cnc_chk_security() = "N" Then
                    Session("cnc_refund_proceed") = "N"
                    lbl_Error.Text = "You are not authorized to proceed with this Refund transaction "
                Else
                    Session("cnc_refund_proceed") = "Y"
                    cnc_refund_amt.Text = String.Format("{0:C}", sy_credit_amt)
                    Session("cnc_orig_refund_amt") = String.Format("{0:C}", sy_credit_amt)
                    lbl_Error.Text = "Proceed - Refund to a Gift Card"
                End If
            Else
                Session("cnc_refund_proceed") = "N"
                lbl_Error.Text = "Order Not Found."
            End If
            'sabrina lbl_Error.Text = "Order Not Found."
            'sabrina cncRefund end
            btn_show_invoices.Enabled = False
            rtnVal = False
        End If
        Return rtnVal
    End Function

    ''' <summary>
    ''' Determines if the payment can be processed based on the order status and type of order. For instance,
    ''' payments can only be made to open sale docs.
    ''' </summary>
    ''' <param name="delDocNum">the order to be processed.</param>
    Private Function CanProcessPayments(ByVal delDocNum As String) As Boolean

        Dim rtnVal As Boolean = True

        'get order details- this will also validate if order is in the system or not
        Dim dTableOrderDetails As DataTable = OrderUtils.GetOrderDetails(UCase(delDocNum))
        If dTableOrderDetails.Rows.Count > 0 Then

            'means that the SAL/MDB order is on file. Now make sure it is an open doc because payments can only be added to open orders
            Dim dRow As DataRow = dTableOrderDetails.Rows(0)
            Dim isSaleOrMdb As Boolean = (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MDB OrElse dRow("ORD_TP_CD") = AppConstants.Order.TYPE_SAL)
            If Not (isSaleOrMdb) Then
                '****payments can be against SAL or MDB�s that are open, final or void
                lbl_Error.Text = "Payments can only be added to Sale or MDB orders. Please enter another order."
                'ClientScript.RegisterStartupScript(Me.GetType(), "Setup Error", "alert('" + "Payments can only be added to Open Sale or Open MDB orders. \n Please enter another order." + "');", True)
                rtnVal = False
            Else
                lbl_Error.Text = String.Empty
            End If
        Else
            lbl_Error.Visible = True
            lbl_Error.Text = "Order Not Found."
            btn_show_invoices.Enabled = False
            rtnVal = False
        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Retrieves the customer info and finance amt based on the original del-doc/order(if existent)
    ''' If the original order has financing, then an error msg forbidding another finance
    ''' payment is displayed.
    ''' </summary>
    Private Sub AddCustOrderInfo()

        Dim delDocNum As String = txt_ivc_cd.Text.Trim.ToUpper
        Dim dTable As DataTable = OrderUtils.GetOrderDetails(delDocNum)
        If dTable.Rows.Count > 0 Then

            'means that the order entered is on file- it can be a CRM/MCR or SAL/MDB 
            Dim dRow As DataRow = dTable.Rows(0)
            Dim isCrmOrMcr As Boolean = (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR OrElse dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM)

            If (isCrmOrMcr) Then
                'For a CRM or MCR, we have to get the original del doc so that the orig customer info can be retrieved  
                'delDocNum = OrderUtils.GetOrginalDelDocNum(delDocNum)
                'Daniela fix conversion Orders with no Original
                Dim origdelDocNum As String = OrderUtils.GetOrginalDelDocNum(delDocNum)
                If (isNotEmpty(origdelDocNum)) Then
                    delDocNum = origdelDocNum
                End If
                'End Daniela
                'TODO -AXK -WIP - change the above line to get it from the dtable.drow instead of calling Util
                'delDocNum = dRow("ORIG_DEL_DOC_NUM")
            End If

            Dim reader As OracleDataReader
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            Dim sql As String = "SELECT ORIG_FI_AMT, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, " &
                                "SHIP_TO_H_PHONE, SO.CUST_CD, ORD_TP_CD, CUST.EMAIL_ADDR " & "FROM SO, CUST " &
                                "WHERE DEL_DOC_NUM='" & delDocNum & "' AND SO.CUST_CD=CUST.CUST_CD "

            Dim cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                If (reader.Read()) Then

                    'means that the referenced orginal order or the SAL/MDB order info was found, display orig cust info
                    lbl_email.Text = reader.Item("EMAIL_ADDR").ToString
                    lbl_fin_exists.Text = reader.Item("ORIG_FI_AMT").ToString
                    Label9.Text = reader.Item("SHIP_TO_F_NAME").ToString & " " & reader.Item("SHIP_TO_L_NAME").ToString
                    Label6.Text = reader.Item("SHIP_TO_ADDR1").ToString
                    Label10.Text = reader.Item("SHIP_TO_H_PHONE").ToString
                    If txt_cust_cd.Text <> reader.Item("CUST_CD").ToString Then
                        If String.IsNullOrEmpty(lbl_email.Text) And ConfigurationManager.AppSettings("email_prompt").ToString = "Y" Then
                            PopupCustomerEMail.ShowOnPageLoad = True
                        End If
                    End If
                    txt_cust_cd.Text = reader.Item("CUST_CD").ToString
                    lbl_email.Text = reader.Item("EMAIL_ADDR").ToString
                Else
                    'implies that the referenced orginal order for the CRM/MCR was not found                     
                End If
                reader.Close()

                'if a finance pmt exists on the orig order, then  do not allow another finance pmt
                If lbl_fin_exists.Text.isNotEmpty AndAlso cbo_payment_type.Value <> "R" Then
                    If IsNumeric(lbl_fin_exists.Text) AndAlso CDbl(lbl_fin_exists.Text) <> 0 Then

                        If isEmpty(lucy_lbl_ind_aprv.Text) = False Then 'lucy
                            '  lucy_lbl_ind_aprv.Text = ""   'lucy
                            'lbl_Error.Text = "Authorized transaction on file, commit to save the transaction"
                            lbl_Error.Text = Resources.LibResources.Label837
                            'Dec 17 do not let go back to SOM+
                            btn_return.Enabled = False
                        Else
                            lucy_lbl_ind_aprv.Text = ""   'lucy
                            lbl_Error.Text = "THIS ORDER HAS FINANCING ASSOCIATED WITH IT, ADDING FINANCE PAYMENTS WILL BE DISABLED."
                        End If
                    End If



                End If
            Catch ex As Exception
                Throw
            Finally
                conn.Close()
            End Try

            GridviewPmts.Enabled = True
            btn_add.Enabled = True
            txt_pmt_amt.Enabled = True
            mop_drp.Enabled = True

        End If

    End Sub

    ''' <summary>
    ''' Resets the main labels and fields to empty values
    ''' </summary>
    Private Sub ResetInfo()
        lbl_email.Text = String.Empty
        lbl_fin_exists.Text = String.Empty
        Label9.Text = String.Empty  'ship to fname/lname
        Label6.Text = String.Empty  'ship to addr
        Label10.Text = String.Empty 'ship to phone
        txt_cust_cd.Text = String.Empty
        lbl_qs_avail.Text = String.Empty
        lbl_qs_desc.Text = String.Empty
        Label8.Text = String.Empty 'order status
        txt_pmt_amt.Text = String.Format("{0:C}", 0)
        hdnOrderStatus.Value = String.Empty
        ResetBalances()
        SetMinimumDue(False)
        btn_add.Enabled = False
        btn_lookup_ivc.Enabled = True

    End Sub

    ''' <summary>
    ''' Occurs when the 'delete' icon button is clicked to indicate that the current payment
    ''' in the payments grid should be deleted.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridviewPmts.DeleteCommand

        Dim rowSeqNum As String = GridviewPmts.DataKeys(e.Item.ItemIndex)

        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 4)
        Dim str_y_n As String

        str_y_n = e.Item.Cells(5).Text

        If str_y_n <> "YES" Then   ' not credit card

            thePaymentBiz.DeletePayment(rowSeqNum)

            'NOTE: Could not avoid the redirect and LOAD of the same page- just calling the bindgrid, delete of 
            ' pmts and recalculating totals did not work. so have to call redirect
            Dim URL_String As String
            URL_String = "?ip_ord_total=" & lbl_current_pmts_amt.Text & "&refresh=true" & "&ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&ip_store_cd=" & cbo_store_cd.Value & "&ip_cust_cd=" & txt_cust_cd.Text & "&ip_ivc_cd=" & txt_ivc_cd.Text & "&ip_csh_drw=" & cbo_csh_drw.Value & "&pmt_tp_cd=" & cbo_payment_type.Value
            Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)


        Else  'lucy
            Exit Sub 'lucy
        End If

    End Sub

    ''' <summary>
    ''' Checks and retrieves any new payments that were added in the current session and displays 
    ''' them in the Payment grid.
    ''' </summary>
    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        GridviewPmts.DataSource = ""
        GridviewPmts.Visible = True


        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()          'sabrina R1999 
        sql = "SELECT ROW_ID,SESSIONID,MOP_CD,AMT,decode(mop_cd, 'TDF', DES || ' - ' || FIN_CO, 'DCS',DES || ' - ' || FIN_CO, DES) DES,BC,EXP_DT,MOP_TP,FIN_CO,FIN_PROMO_CD,FI_ACCT_NUM,APPROVAL_CD,CHK_NUM,CHK_TP,TRANS_ROUTE,CHK_ACCOUNT_NO," +
            " SECURITY_CD, HOST_RESP_MSG, HOST_RESPONSE_CD, PB_RESPONSE_MSG, DL_STATE, DL_LICENSE_NO " +
            " FROM payment where sessionid='" & Session.SessionID.ToString.Trim & "' order by row_id"
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                'lucy add  6 lines
                Dim i As Integer
                Dim numrows As Integer
                Dim mytable As DataTable
                Dim SoLnGItm As DataGridItem
                mytable = New DataTable
                mytable = ds.Tables(0)
                'end lucy add

                GridviewPmts.DataSource = ds
                GridviewPmts.DataBind()
                btn_commit.Enabled = True

                'start lucy add the following to make the electronic content
                numrows = mytable.Rows.Count
                Dim str_app_cd As String = Session("lucy_approval_cd")
                For i = 0 To numrows - 1

                    SoLnGItm = GridviewPmts.Items(i)

                    Dim str_mop As String
                    Dim str_mop_tp As String
                    str_mop = Left(SoLnGItm.Cells(1).Text, 6)
                    str_mop_tp = SoLnGItm.Cells(4).Text



                    ' If InStr(str_mop, "VISA") = 0 And InStr(str_mop, "VISA") = 0 And InStr(str_mop, "FINA") = 0 And InStr(str_mop, "DEBI") = 0 Or (InStr(str_mop, "FINA") > 0 And Len(str_app_cd) < 6) Then
                    If str_mop_tp = "BC" Or str_mop_tp = "FI" Or str_mop_tp = "GC" Then 'sabrina add GC
                        SoLnGItm.Cells(5).Text = "YES"
                        SoLnGItm.Cells(5).ForeColor = Color.Green
                    Else
                        SoLnGItm.Cells(5).Text = "NO"

                        SoLnGItm.Cells(5).ForeColor = Color.Gray
                    End If
                Next 'lucy add end
            Else
                GridviewPmts.DataSource = ""
                GridviewPmts.DataBind()
                GridviewPmts.Visible = False
                btn_commit.Enabled = False
            End If

            reader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub



    ''' <summary>
    ''' Shows a popup window with all the original payments that allows the user to alter or accept the
    ''' original refund amount and mark them for refunds. It adds all the existing payments for the order 
    ''' entered, provided the order is a valid order that exists in the system and a refund is being processed. 
    ''' IF no original payments are retrieved then an error message is displayed and the popup is not shown. 
    ''' </summary>
    ''' <returns></returns>
    Public Function DisplayRefundsPopup() As Boolean
        Dim rtnVal As Boolean = True

        'retrieve all the original payments for the original order
        If (txt_ivc_cd.Text.isNotEmpty AndAlso (cbo_payment_type.Value = "R")) Then

            'this will check if there is a balance due and populate the class vbl for orig refund pmts
            PopulateOriginalPayments()

            If Not _dsOrigPmtsForRefunds Is Nothing Then

                'need to insert a ID/DATAKEY for the ds that will be used to display all refundable payments in the popup
                Dim dr As DataRow
                Dim dtableNew As DataTable = New DataTable
                Dim drowNew As DataRow

                ' Create the structure for the new table for displaying all the columns of the orig pmts
                ' plus an ID/DATAKEY column that will be used to retrieve the row to do amount validation
                ' once the refund amt per MOP Is entered.
                dtableNew.Columns.Add("DES")
                dtableNew.Columns.Add("ACCT_NUM")
                dtableNew.Columns.Add("BNK_CRD_NUM")
                dtableNew.Columns.Add("EXP_DT")
                dtableNew.Columns.Add("MOP_CD")
                dtableNew.Columns.Add("MOP_TP")
                dtableNew.Columns.Add("AMT")
                dtableNew.Columns.Add("ID")   'new ID column needed for amt validation


                'iterate through each row of the original mops to extract the info and dump into new table
                'that has the new ID/DATAKEY column
                Dim id As Integer = 1
                Dim hTableOrigPmtAmts As Hashtable = New Hashtable
                For Each dr In _dsOrigPmtsForRefunds.Tables(0).Rows

                    drowNew = dtableNew.NewRow()
                    Dim acctNum As String = IIf(IsDBNull(dr("acct_num")), "", Replace(dr("acct_num").ToString, "ID:", ""))
                    drowNew("DES") = dr("des")
                    drowNew("ACCT_NUM") = acctNum
                    drowNew("BNK_CRD_NUM") = dr("bnk_crd_num")
                    drowNew("EXP_DT") = dr("exp_dt")
                    drowNew("MOP_CD") = dr("mop_cd")
                    drowNew("MOP_TP") = dr("mop_tp")
                    drowNew("AMT") = dr("amt")
                    drowNew("ID") = id   'this is the new ID/DataKey column
                    hTableOrigPmtAmts.Add(id, dr("AMT"))
                    id = id + 1
                    dtableNew.Rows.Add(drowNew)
                Next

                If _dsOrigPmtsForRefunds.Tables(0).Rows.Count > 0 Then
                    'add the hash table containing the new ID column and amt to the viewstate so that it preserved during postback
                    _hTableOrigPmtAmtsForRefunds = hTableOrigPmtAmts
                    ViewState("hTableOrigPmtAmtsForRefunds") = _hTableOrigPmtAmtsForRefunds

                    'set the new table with the ID column, as the datasource for the original pmt grid in the refund popup
                    MyDataGrid.DataSource = dtableNew
                    MyDataGrid.DataBind()

                    'if the order is open and there are original refund details, get and set the min due.
                    If (hdnOrderStatus.Value = AppConstants.Order.STATUS_OPEN AndAlso Not IsNothing(_dtRefundDetails)) Then
                        If (_dtRefundDetails.Rows.Count > 0) Then

                            Dim drow As DataRow = _dtRefundDetails.Rows(0)
                            Dim orderTotal As Double = CDbl(drow("ORDER_TOTAL").ToString)
                            SetMinimumDue(True, GetMinimumDue(orderTotal))
                        End If
                    Else
                        SetMinimumDue(False)
                    End If

                    '**** show the window with all the original payments ********
                    lblRefundError.Text = String.Empty
                    PopupOrigRefundPmts.ShowOnPageLoad = True
                End If
            Else
                'if there were no orig pmts for whatever reason like wrong type of order, etc., just display a message
                lbl_Error.Visible = True
                lbl_Error.Text = "Refunds cannot be issued to the original order."
                rtnVal = False
            End If

        End If   'if it's a refund and has an order id

        Return rtnVal
    End Function

    ''' <summary>
    ''' Displays the appropriate Method of Payment based on whether a refund or payment is 
    ''' being processed and also securites.
    ''' </summary>
    ''' <returns>Boolean indicating if either all or original payments were successfully retrieved</returns>
    Private Function DisplayMops() As Boolean

        Dim rtnVal As Boolean = True
        'Dim hasRefdOverride As Boolean = False
        Dim hasRefdOverride As Boolean = SecurityUtils.hasSecurity(SecurityUtils.REFUND_DEFAULT_MOP_OVERRIDE, lbl_emp_cd.Text)
        Dim isRefund = (cbo_payment_type.Value = "R")

        ' 1. when processing payments, all MOPs should be displayed
        ' 2. when processing refunds & user has security, all MOPs should be displayed
        ' 3. when processing refunds against a CRM/MCR/MDB that does not have an orig doc, all MOPs should be shown
        ' 4. when processing refunds & user has does NOT have security, original MOPs should be displayed

        'sabrina tdpl
        ' Daniela added vd_inspmt apr 30
        If Session("td_instpmt") = "Y" Or Session("vd_inspmt") = "Y" Then
            sy_displaymop()
        ElseIf (hasRefdOverride OrElse (Not isRefund) OrElse IsRefundAgainstNoOrigOrder()) Then
            DisplayAllMops()
        Else
            rtnVal = DisplayOriginalMops()
        End If

        Return rtnVal
    End Function
    Private Function sy_displaymop() As Boolean

        'sabrina tdpl new 
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim mop_found As Boolean = False
        Dim cmd As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As New DataSet
        Dim MyDataReader As OracleDataReader

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','FI','(FINANCE)','') AS DES "
        sql = sql & "FROM SETTLEMENT_MOP "
        sql = sql & "WHERE MOP_CD IN ('CS','DC') "
        sql = sql & "ORDER BY DES"

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        mop_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.Read()) Then
                mop_found = True
                mop_drp.DataSource = ds
                mop_drp.ValueField = "MOP_CD"
                mop_drp.TextField = "DES"
                mop_drp.DataBind()
            End If

            MyDataReader.Close()
            conn.Close()

            ' put the default first item
            mop_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label698, ""))
            mop_drp.SelectedIndex = 0
        Catch ex As Exception
            conn.Close()
            Throw
        End Try



    End Function

    ''' <summary>
    ''' Populates the MOP drop-down with the original MOP for the order entered, provided the order is a valid order that exists in
    ''' the system and a refund is being processed. It will display the entries in the drop-down as a concatenation of the MOP desc + amount.
    ''' It will also store behind the scenes a list consists of OriginalPaymentDtc objects so that when the user selects a particular MOP,
    ''' it's corresponding object can be used to validate the amount entered for refund. If no original payments are found for the
    ''' order, it returns false.
    ''' </summary>
    ''' <returns>Boolean indicating if original payments were successfully retrieved</returns>
    Private Function DisplayOriginalMops() As Boolean

        Dim rtnVal As Boolean = True
        Dim gc_mop As String = ""

        'this will check if there is a balance due and populate the class vbl for orig refund pmts
        PopulateOriginalPayments()

        gc_mop = ""
        If Not _dsOrigPmtsForRefunds Is Nothing Then

            'iterate through each row of the original mops to extract the info and dump into new table
            _listOrigPmtsForRefundsDd = New List(Of OriginalPaymentDtc)
            Dim origPmtDtc As OriginalPaymentDtc

            For Each dr In _dsOrigPmtsForRefunds.Tables(0).Rows
                origPmtDtc = New OriginalPaymentDtc()
                origPmtDtc.acctNum = IIf(IsDBNull(dr("acct_num")), "", dr("acct_num"))
                origPmtDtc.amt = dr("amt")
                origPmtDtc.bankCardNum = IIf(IsDBNull(dr("bnk_crd_num")), "", dr("bnk_crd_num"))
                origPmtDtc.expDate = IIf(IsDBNull(dr("exp_dt")), "", dr("exp_dt"))
                origPmtDtc.mopCd = dr("mop_cd")
                'sabrina add
                If dr("mop_cd") = "GC" Then
                    gc_mop = "Y"
                End If
                origPmtDtc.mopTp = IIf(IsDBNull(dr("mop_tp")), "", dr("mop_tp"))
                origPmtDtc.mopDesc = dr("des")
                origPmtDtc.mopDescAmt = dr("des") & "      " & FormatCurrency(dr("amt"))
                _listOrigPmtsForRefundsDd.Add(origPmtDtc)
            Next

            'sabrina add the following
            'If gc_mop <> "Y" Then

            '    origPmtDtc = New OriginalPaymentDtc()
            '    origPmtDtc.acctNum = ""
            '    origPmtDtc.amt = "0"
            '    origPmtDtc.bankCardNum = ""
            '    origPmtDtc.expDate = ""
            '    origPmtDtc.mopCd = "GC"
            '    origPmtDtc.mopTp = "GC"
            '    origPmtDtc.mopDesc = "Gift Card"
            '    origPmtDtc.mopDescAmt = ""
            '    _listOrigPmtsForRefundsDd.Add(origPmtDtc)
            'End If

            'Add the list to the viewstate so that it preserved during postback
            ViewState("listOrigPmtsForRefundsDd") = _listOrigPmtsForRefundsDd

            'set the new list of payments with the special description as the datasource for the dd
            ' This way we can get the original pmt with the details like amt to validate.
            mop_drp.Items.Clear()
            mop_drp.DataSource = _listOrigPmtsForRefundsDd
            mop_drp.ValueField = "mopCd"
            mop_drp.TextField = "mopDescAmt"
            mop_drp.DataBind()

            ' put the default first item
            mop_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label698, ""))
            mop_drp.SelectedIndex = 0
        Else
            'means there were no original payments found on the original order being refunded
            'if there were no orig pmts, just display a message
            lbl_Error.Visible = True
            'lbl_Error.Text = "Refunds cannot be issued due to lack of funds on the original order."
            lbl_Error.Text = Resources.LibResources.Label828
            rtnVal = False
        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Populates the MOP drop down with all the MOps found in the system. This is used when either a payment is being processed
    ''' or a refund against a non-existent order is being done. 
    ''' </summary>
    Public Sub DisplayAllMops()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim mop_found As Boolean = False
        Dim cmd As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As New DataSet
        Dim MyDataReader As OracleDataReader

        ' Daniela add multi company logic
        Dim emp_init As String
        If isEmpty(Session("emp_init")) Then
            emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
            Session("emp_init") = emp_init
        Else : emp_init = Session("emp_init")
        End If

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','FI','(FINANCE)','') AS DES "
        sql = sql & "FROM SETTLEMENT_MOP a "
        'sql = sql & "WHERE Std_multi_co.isValidMop(upper('" & emp_init & "'), mop_cd) = 'Y' and (MOP_ACTIVE='Y' "
        ' Daniela remove finance, improve performance
        sql = sql & "WHERE MOP_CD != 'FI' "
        'sql = sql & "and Std_multi_co.isValidMop(upper('" & emp_init & "'), mop_cd) = 'Y' "
        sql = sql & " AND exists ("
        sql = sql & "       Select 1 "
        sql = sql & "       from   co_grp$mop"
        sql = sql & "       where  ("
        sql = sql & "              co_grp_cd  =  (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & Session("CO_CD") & "'),co_cd))"
        sql = sql & "              or"
        sql = sql & "              co_grp_cd  = 'ALL'"
        sql = sql & "              )"
        sql = sql & "       and    mop_cd     =  a.mop_cd"
        sql = sql & "       ) "
        sql = sql & " and (MOP_ACTIVE='Y' "
        If lbl_fin_exists.Text & "" <> "" Then
            If IsNumeric(lbl_fin_exists.Text) Then
                If CDbl(lbl_fin_exists.Text) <> 0 Then
                    sql = sql & "AND MOP_TP != 'FI' "
                End If
            Else
                sql = sql & "AND MOP_TP != 'FI' "
            End If
        End If
        sql = sql & "OR MOP_TP IS NULL AND MOP_ACTIVE='Y') "
        sql = sql & "ORDER BY DES"

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        mop_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (MyDataReader.Read()) Then
                mop_found = True
                mop_drp.DataSource = ds
                mop_drp.ValueField = "MOP_CD"
                mop_drp.TextField = "DES"
                mop_drp.DataBind()
            End If

            MyDataReader.Close()
            conn.Close()

            ' put the default first item
            mop_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label698, ""))
            mop_drp.SelectedIndex = 0
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    ''' <summary>
    ''' Determines if there is a balance due on the order being refunded and accordingly populates 
    ''' the class-level list that represents all the original payments. 
    ''' </summary>
    Private Sub PopulateOriginalPayments()

        Dim delDocNum As String = txt_ivc_cd.Text.Trim
        Dim dTable As DataTable = OrderUtils.GetOrderDetails(delDocNum)
        Dim dRow As DataRow = Nothing
        If dTable.Rows.Count > 0 Then
            dRow = dTable.Rows(0)
        End If

        If _dsOrigPmtsForRefunds Is Nothing Then

            Dim proceed As Boolean = True
            If Not IsNothing(dRow) Then

                Dim isCrmOrMcr As Boolean = ((dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR) OrElse (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM))
                Dim isSalOrMdb As Boolean = ((dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MDB) OrElse (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_SAL))

                If isCrmOrMcr Then
                    'if it's a CRM/MCR then use the balance to determine to proceed
                    proceed = (GetBalance(txt_cust_cd.Text, delDocNum) < 0)

                ElseIf isSalOrMdb Then

                    If (dRow("STAT_CD") = AppConstants.Order.STATUS_VOID) Then
                        'For a VOID sale/mdb, if there's a balance due(like in an overpayment case), refund any original MOPs. 
                        proceed = (GetBalance(txt_cust_cd.Text, delDocNum) < 0)

                    ElseIf (dRow("STAT_CD") = AppConstants.Order.STATUS_FINAL) Then
                        ' for a final SAL, check if there are any open CRM/MCR/MDB docs against it because in that case no refund is allowed
                        'If there is no linked doc, then it means there could be an overpayment situation and so check for that later
                        Dim linkedDelDoc As String = PaymentUtils.GetLinkedDocForSale(txt_ivc_cd.Text, lbl_co_cd.Text, txt_cust_cd.Text)
                        ' Daniela fix refunds  Dec 12
                        proceed = (Not linkedDelDoc Is Nothing)
                        'proceed = (linkedDelDoc = "N") ' Daniela Comment, should not be 'N' or orig del_doc_num
                        If (proceed) Then proceed = (GetBalance(txt_cust_cd.Text, delDocNum) < 0)

                    Else
                        'For OPEN sale orders, get the order total, min due, etc.
                        If (_dtRefundDetails Is Nothing) Then
                            _dtRefundDetails = PaymentUtils.GetRefundDetails(delDocNum, lbl_co_cd.Text, txt_cust_cd.Text)
                            'Add the list to the viewstate so that it preserved during postback
                            ViewState("dtRefundDetails") = _dtRefundDetails
                        End If

                        'check if there is any amt to refund and also the doc num should be the referenced one or the entered one, if no orig.
                        If (_dtRefundDetails.Rows.Count > 0) Then
                            Dim dr As DataRow = _dtRefundDetails.Rows(0)
                            delDocNum = dr("DOC_NUM")
                            proceed = CDbl(dr("REFUNDABLE_AMT").ToString) > 0
                        End If
                    End If
                End If

                Dim isCrmOrMcrOrMdb As Boolean = ((dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR) OrElse
                                                 (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM)) OrElse
                                                 (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MDB)
                If isCrmOrMcrOrMdb Then
                    'need to get the orig del_doc_num, if refund is being done against a CRM/MCR doc that has an orig doc
                    If (dRow("ORIG_DEL_DOC_NUM").ToString.isNotEmpty) Then delDocNum = dRow("ORIG_DEL_DOC_NUM")
                End If

                If (proceed) Then
                    _dsOrigPmtsForRefunds = PaymentUtils.GetOriginalPayments(delDocNum,
                                                                             lbl_co_cd.Text,
                                                                             txt_cust_cd.Text)
                End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' Determines if the CRM/MCR/MDB doc number entered for the refund references
    ''' a valid, original order or not.
    ''' </summary>
    ''' <returns></returns>
    Private Function IsRefundAgainstNoOrigOrder() As Boolean
        Dim rtnVal As Boolean = False

        If (txt_ivc_cd.Text.isNotEmpty) AndAlso (cbo_payment_type.Value = "R") Then
            Dim dTable As DataTable = OrderUtils.GetOrderDetails(txt_ivc_cd.Text)
            Dim dRow As DataRow = Nothing
            If dTable.Rows.Count > 0 Then
                dRow = dTable.Rows(0)
            End If
            Dim isCrmOrMcrOrMdb As Boolean = ((dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR) OrElse
                                                 (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM)) OrElse
                                                 (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MDB)
            If isCrmOrMcrOrMdb Then
                'if refund is being done against a CRM/MCR/MDB doc that has does not have an orig doc, show all MOPs
                rtnVal = dRow("ORIG_DEL_DOC_NUM").ToString.isEmpty
            End If
        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Executes when the user clicks the 'Add Payment Detail' button to add 
    ''' details of the selected MOP.
    ''' </summary>
    Protected Sub btn_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add.Click

        lbl_fi_warning.Text = "" ' Daniela clear prevoius message

        lbl_Error.Text = String.Empty
        If mop_drp.SelectedIndex = 0 Then
            lbl_Error.Text = Resources.LibResources.Label803
        Else
            ' Daniela save mop cd for TD
            Session("lucy_mop_cd") = mop_drp.Value
            ' start lucy add
            If mop_drp.Value = "FI" Or mop_drp.Value = "TDF" Then
                lucy_change_DropDownList1()
            End If
            'end lucy add


            lbl_pmt_amt.Text = txt_pmt_amt.Text.ToString()
            If ValidateAmount(lbl_Error, txt_pmt_amt.Text) Then
                lbl_Error.Text = String.Empty
                GridviewPmts.Enabled = True
                If (ProcessPayment()) Then
                    txt_pmt_amt.Text = String.Format("{0:C}", 0)
                    mop_drp.SelectedIndex = 0
                    bindgrid()
                    UpdateTotals()
                    'disable the 'Go' button once a payment has been added to the payment grid
                    btn_lookup_ivc.Enabled = False

                    ' Reset cashbreakdown section values
                    reset_cash_breakdown()

                End If
            Else
                Dim s = "FFF"
            End If

        End If
    End Sub

    ''' <summary>
    ''' Displays the relevant payment window(CC, Finance) to capture any payment 
    ''' details based on the MOP selected.  If all is successful, saves a record 
    ''' in the temp PAYMENT table.
    ''' </summary>
    Public Function ProcessPayment() As Boolean



        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim oraTransaction As OracleTransaction
        Dim oraCmd As OracleCommand
        Dim oraAdp As OracleDataAdapter
        Dim dTable As DataTable
        Dim dSet As DataSet
        Dim desc As String = ""
        Dim mopTp As String = ""
        Dim isFinanceDeposit As Boolean = Is_Finance_DP(mop_drp.Value)
        Session("del_doc_num") = txt_ivc_cd.Text 'lucy
        connErp.Open()
        connLocal.Open()

        oraCmd = DisposablesManager.BuildOracleCommand

        oraCmd.Connection = connErp
        oraCmd.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :MOP"
        oraCmd.Parameters.Add(":MOP", OracleType.VarChar)
        oraCmd.Parameters(":MOP").Value = mop_drp.Value

        dSet = New DataSet
        oraAdp = DisposablesManager.BuildOracleDataAdapter(oraCmd)
        oraAdp.Fill(dSet)
        dTable = New DataTable
        dTable = dSet.Tables(0)
        desc = dTable.Rows(0).Item("DES").ToString
        mopTp = dTable.Rows(0).Item("MOP_TP").ToString
        If (isFinanceDeposit) Then mopTp = "FI"
        ' Daniela
        If mop_drp.Value = "TDF" Then
            mopTp = "TD" ' added to procees TD finance
        End If

        'sabrina R1930
        If Session("emp_init") & "" = "" Then
            Session("emp_init") = LeonsBiz.GetEmpInit(Session("emp_cd"))
        End If

        'sabrina R1999
        lucy_btn_new_cust.Visible = False
        txt_fi_appr.Visible = False
        Literal1.Visible = False
        txt_fi_acct.Enabled = True
        txt_fi_exp_dt.Enabled = True
        'sabrina R1999 end


        'multiple finance deposits ARE allowed per transaction, so skip this check
        'however if this was a finance, only ONE finance is allowed per transaction
        'therefore this check will stop the user if a second finance is added
        If (Not isFinanceDeposit AndAlso "FI" = mopTp) Then
            If (thePaymentBiz.HasFinancePayment(Session.SessionID.ToString)) Then
                lbl_Error.Visible = True
                lbl_Error.Text = "Only one finance payment allowed per order."
                Return False
            End If
        End If
        oraCmd.Parameters.Clear()
        If mopTp = "BC" Then
            If mop_drp.Value = PaymentUtils.Bank_MOP_CD.DESJARDINS Then
                txt_fi_acct.Text = ""
                txt_fi_appr.Text = ""
                lbl_fi_warning.Text = ""
                lucy_btn_new_cust.Visible = True
                txt_fi_appr.Visible = True
                txt_fi_appr.Enabled = True
                sy_AddDCSPlan()

                If cbo_payment_type.Value = "R" Then   'for refund
                    If Session("dcs_plan") & "" <> "" Then

                        Dim planExist As Boolean = False 'Alice update on Oct 25,2018 for request 4115. If finance plan is no logner available, it doesn't allow refund
                        For Each item As ListItem In cbo_finance_company.Items
                            If item.Value = Session("dcs_plan") Then
                                planExist = True
                                Exit For
                            End If
                        Next
                        If planExist Then
                            cbo_finance_company.SelectedValue = Session("dcs_plan") 'sabrina R1999 Nov12 Dec21-uncomment - jan22,2016
                            Literal1.Visible = True
                            lbl_pmt_tp.Text = mop_drp.Value
                            PopupFinancePmt.ShowOnPageLoad = True
                        Else
                            lbl_Error.Text = Resources.POSErrors.ERR0065 ' You have tried to refund a finance plan that is no longer available.

                        End If

                    End If
                Else 'for payment
                    Literal1.Visible = True
                    lbl_pmt_tp.Text = mop_drp.Value
                    PopupFinancePmt.ShowOnPageLoad = True
                End If



            ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.FLEXITI Then
                Literal2.Visible = True

                txt_fi_appr_flx.Text = ""

                btn_save_flx.Visible = True
                txt_fi_appr_flx.Visible = True
                sy_AddFLXPlan()

                If cbo_payment_type.Value = "R" Then
                    If Session("flx_plan") & "" <> "" Then

                        Dim planExist As Boolean = False  'Alice update on Oct 25,2018 for request 4115. If finance plan is no logner available, it doesn't allow refund
                        For Each item As ListItem In cbo_finance_company_flx.Items
                            If item.Value = Session("flx_plan") Then
                                planExist = True
                                Exit For
                            End If
                        Next
                        If planExist Then
                            cbo_finance_company_flx.SelectedValue = Session("flx_plan")
                            Literal2.Visible = True
                            lbl_pmt_tp_flx.Text = mop_drp.Value
                            PopupFinancePmt_FLX.ShowOnPageLoad = True
                        Else
                            lbl_Error.Text = Resources.POSErrors.ERR0065 ' You have tried to refund a finance plan that is no longer available.

                        End If

                    End If
                Else
                    Literal2.Visible = True
                    lbl_pmt_tp_flx.Text = mop_drp.Value
                    PopupFinancePmt_FLX.ShowOnPageLoad = True

                End If


            ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.MCFinancial Then 'Alice added on May 31, 2019 for request 235
                txt_fm_acct.Text = ""
                txt_fm_appr.Text = ""
                lbl_fm_warning.Text = ""
                txt_fm_appr.Visible = True
                txt_fm_appr.Enabled = True
                sy_addFMcompany()

                If cbo_payment_type.Value = "R" Then   'for refund
                    If Session("fm_plan") & "" <> "" Then

                        Dim planExist As Boolean = False
                        For Each item As ListItem In cbo_finance_company_fm.Items
                            If item.Value = Session("fm_plan") Then
                                planExist = True
                                Exit For
                            End If
                        Next
                        If planExist Then
                            cbo_finance_company_fm.SelectedValue = Session("fm_plan") 'sabrina R1999 Nov12 Dec21-uncomment - jan22,2016
                            lit_fm.Visible = True
                            lbl_pmt_tp_fm.Text = mop_drp.Value
                            PopupFinancePmt_FM.ShowOnPageLoad = True
                        Else
                            lbl_Error.Text = Resources.POSErrors.ERR0065 ' You have tried to refund a finance plan that is no longer available.

                        End If

                    End If
                Else 'for payment
                    lit_fm.Visible = True
                    lbl_pmt_tp_fm.Text = mop_drp.Value
                    PopupFinancePmt_FM.ShowOnPageLoad = True
                End If

                ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.ALI Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.WECHAT Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.UNION Then
                Literal3.Visible = True
                txt_fi_appr_china.Text = ""
                btn_save_china.Visible = True
                txt_fi_appr_china.Visible = True

                lbl_pmt_tp_china.Text = mop_drp.Value
                PopupFinancePmt_China.ShowOnPageLoad = True

                Select Case mop_drp.Value
                    Case PaymentUtils.Bank_MOP_CD.ALI
                        PopupFinancePmt_China.HeaderText = Resources.LibResources.Label1007
                    Case PaymentUtils.Bank_MOP_CD.WECHAT
                        PopupFinancePmt_China.HeaderText = Resources.LibResources.Label1008
                    Case PaymentUtils.Bank_MOP_CD.UNION
                        PopupFinancePmt_China.HeaderText = Resources.LibResources.Label1009
                End Select

            Else
                If IsDBNull(cbo_store_cd.Value) = False Then  'lucy 
                    Dim query_string As String = ""
                    If txt_ivc_cd.Text & "" <> "" Then
                        Session("cust_cd") = txt_cust_cd.Text   'lucy add
                        query_string = "ip_ivc_cd=" & txt_ivc_cd.Text.Trim & "&"
                    End If
                    If Label7.Text & "" <> "" Then
                        query_string = query_string & "ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&"
                    End If
                    If cbo_store_cd.Value & "" <> "" Then
                        query_string = query_string & "ip_store_cd=" & cbo_store_cd.Value & "&"
                    End If
                    If cbo_csh_drw.Value & "" <> "" Then
                        query_string = query_string & "ip_csh_drw=" & cbo_csh_drw.Value & "&"
                    End If
                    If cbo_payment_type.Value & "" <> "" Then
                        query_string = query_string & "pmt_tp_cd=" & cbo_payment_type.Value & "&"
                    End If
                    If txt_cust_cd.Text & "" <> "" Then
                        query_string = query_string & "ip_cust_cd=" & txt_cust_cd.Text & "&"
                    End If
                    If Request("referrer") & "" <> "" Then
                        query_string = query_string & "referrer=" & Request("referrer") & "&"
                    End If
                    PopupCCPmt.ContentUrl = "credit_card_processing.aspx?page=independentpaymentprocessing.aspx&AMT=" & CDbl(txt_pmt_amt.Text) & "&MOP_CD=" & mop_drp.Value & "&DES=" & desc & "&" & query_string
                    PopupCCPmt.ShowOnPageLoad = True
                Else
                    oraTransaction = connLocal.BeginTransaction
                    With oraCmd
                        .Transaction = oraTransaction
                        .Connection = connLocal
                        .CommandText = "insert into payment (sessionid,MOP_CD, AMT, DES, MOP_TP) values ('" & Session.SessionID.ToString.Trim & "','" & mop_drp.Value & "','" & CDbl(txt_pmt_amt.Text) & "','" & desc & "','" & mopTp & "')"
                    End With
                    oraCmd.ExecuteNonQuery()
                    oraTransaction.Commit()
                    Session("lucy_from_logout") = "NO" 'lucy
                End If

            End If
        ElseIf mopTp = "" Or (mopTp = "GC" And LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y") Then  'sabrina R1930
            oraTransaction = connLocal.BeginTransaction
            With oraCmd
                .Transaction = oraTransaction
                .Connection = connLocal
                .CommandText = "insert into payment (sessionid,MOP_CD, AMT, DES, MOP_TP) values ('" & Session.SessionID.ToString.Trim & "','" & mop_drp.Value & "','" & CDbl(txt_pmt_amt.Text) & "','" & desc & "','" & mopTp & "')"
            End With
            oraCmd.ExecuteNonQuery()
            oraTransaction.Commit()
            Session("lucy_from_logout") = "NO" 'lucy
        ElseIf mopTp = "FI" Then
            txt_fi_acct.Text = ""
            txt_fi_appr.Text = ""
            AddFinanceCompany()   'lucy add
            PaymentUtils.PopulateFinanceProviders(cbo_store_cd.Value, cbo_finance_company)
            GetPromoCodes()
            lbl_pmt_tp.Text = mop_drp.Value
            PopupFinancePmt.ShowOnPageLoad = True
        ElseIf mopTp = "TD" Then ' Daniela TD added
            Session("tdfRefundAmt") = txt_pmt_amt.Text
            txt_fi_acct.Text = ""
            txt_fi_appr.Text = ""
            lbl_fi_warning.Text = "" ' Daniela clear previous message
            lucy_btn_new_cust.Visible = False
            If cbo_payment_type.Value = "R" Then
                AddTdCompany()
                If isNotEmpty(Session("tdf_plan")) Then
                    ' cbo_finance_company.SelectedValue = Session("tdf_plan")

                    Dim planExist As Boolean = False 'Alice update on Oct 25,2018 for request 4115. If finance plan is no logner available, it doesn't allow refund
                    For Each item As ListItem In cbo_finance_company.Items
                        If item.Value = Session("tdf_plan") Then
                            planExist = True
                            Exit For
                        End If
                    Next
                    If planExist Then
                        cbo_finance_company.SelectedValue = Session("tdf_plan")
                        lbl_pmt_tp.Text = mop_drp.Value
                        PopupFinancePmt.ShowOnPageLoad = True
                    Else
                        lbl_Error.Text = Resources.POSErrors.ERR0065 ' You have tried to refund a finance plan that is no longer available.
                    End If
                End If
                'cbo_finance_company.Enabled = False ' Daniela does nor refresh
            Else
                AddTdCompany()
                lbl_pmt_tp.Text = mop_drp.Value
                PopupFinancePmt.ShowOnPageLoad = True
            End If

        ElseIf mopTp = "CK" Then
            lbl_pmt_tp.Text = mop_drp.Value
            PopupCheckPmt.ShowOnPageLoad = True
            'sabrina R1930 ElseIf mopTp = "GC" Then
        ElseIf (mopTp = "GC" And LeonsBiz.isLeonsUser(Session("CO_CD")) = "N") Then 'sabrina R1930
            lbl_pmt_tp.Text = mop_drp.Value
            PopupGiftCardPmt.ShowOnPageLoad = True
            'sabrina add
            sy_processGC()
        End If
        connLocal.Close()
        connErp.Close()

        Return True
    End Function


    ''' <summary>
    ''' Retrieves and populates the promotions for a particular finance provider.
    ''' </summary>
    Private Sub GetPromoCodes()
        PaymentUtils.PopulatePromotions(lbl_pmt_tp.Text, cbo_finance_company.SelectedValue, cbo_promo)
    End Sub


    Protected Sub btn_save_fi_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fi_co.Click

        Dim pmtType As String = cbo_payment_type.Value
        Dim merchant_id As String = Lookup_Merchant("FI", cbo_store_cd.Value, lbl_pmt_tp.Text)
        Dim regex1 As Regex = New Regex("^[0-9]")
        Dim match1 As Match = regex1.Match(txt_fi_exp_dt.Text)
        If ConfigurationManager.AppSettings("finance") = "N" Then merchant_id = ""

        If (Not String.IsNullOrEmpty(merchant_id)) Then
            '' verify the card/acct number 
            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)
            If String.IsNullOrEmpty(myTypeValid.CCType) Or myTypeValid.CCType = "Unknown" Then
                lbl_fi_warning.Text = "Invalid account #"
                txt_fi_acct.Focus()
                Exit Sub
            Else
                lbl_fi_warning.Text = String.Empty
            End If
            If (match1.Success AndAlso txt_fi_exp_dt.Text <> "" AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) >= 1 AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) <= 12) Then

                txt_fi_appr.Focus()
            Else
                If (txt_fi_exp_dt.Text <> "") Then
                    lbl_fi_warning.Text = "Invalid Month #"
                End If
                Exit Sub
            End If
            If String.IsNullOrEmpty(txt_fi_acct.Text) Then
                lbl_fi_warning.Text = "You must enter an account #"
                Exit Sub
            End If
            If Not IsNumeric(txt_fi_acct.Text) Then
                lbl_fi_warning.Text = "Account # must be numeric"
                Exit Sub
            End If
        End If

        'sabrina R1999
        If Session("lucy_mop_cd") = "DCS" Then
            If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
                If LeonsBiz.isvalidDCSplan(cbo_finance_company.SelectedValue()) <> "Y" Then
                    lbl_fi_warning.Text = "Please Select a Plan"
                    cbo_finance_company.Focus()
                    Exit Sub
                End If
            End If
        End If

        'sabrina R1999 end
        lucy_btn_save_fi_co_Click() 'lucy

        'lucy removed the following lines

    End Sub

    'Fires when user clicks on the 'Save Check Details' button in the check popup
    Protected Sub btn_save_chk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_chk.Click

        If txt_check.Text & "" = "" Then
            lbl_chk_warnings.Text = "Check # cannot be blank"
            Exit Sub
        End If
        If ConfigurationManager.AppSettings("chk_guar") = "Y" Then
            If TextBox2.Text & "" = "" Then
                lbl_chk_warnings.Text = "Routing # cannot be blank"
                Exit Sub
            End If
            If TextBox3.Text & "" = "" Then
                lbl_chk_warnings.Text = "Account # cannot be blank"
                Exit Sub
            End If
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If
        If ConfigurationManager.AppSettings("dl_verify") = "Y" Then
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim cmd As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, CHK_NUM, CHK_TP, TRANS_ROUTE, CHK_ACCOUNT_NO, APPROVAL_CD, DL_STATE, DL_LICENSE_NO)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :CHK_NUM, :CHK_TP, :TRANS_ROUTE, :CHK_ACCOUNT_NO, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO)"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)

            cmd.Parameters.Add(":sessionid", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
            cmd.Parameters.Add(":AMT", OracleType.VarChar)
            cmd.Parameters.Add(":DES", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_TP", OracleType.VarChar)
            cmd.Parameters.Add(":CHK_NUM", OracleType.VarChar)
            cmd.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            cmd.Parameters.Add(":CHK_TP", OracleType.VarChar)
            cmd.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
            cmd.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)
            cmd.Parameters.Add(":DL_STATE", OracleType.VarChar)
            cmd.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            cmd.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            cmd.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            cmd.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            cmd.Parameters(":DES").Value = des
            cmd.Parameters(":MOP_TP").Value = bc
            cmd.Parameters(":CHK_NUM").Value = txt_check.Text.ToString
            cmd.Parameters(":APPROVAL_CD").Value = txt_appr.Text.ToString
            cmd.Parameters(":CHK_TP").Value = DropDownList2.SelectedValue.ToString
            cmd.Parameters(":TRANS_ROUTE").Value = TextBox2.Text.ToString
            cmd.Parameters(":CHK_ACCOUNT_NO").Value = TextBox3.Text.ToString
            cmd.Parameters(":DL_STATE").Value = txt_dl_state.Text.ToString
            cmd.Parameters(":DL_LICENSE_NO").Value = txt_dl_number.Text.ToString

            cmd.ExecuteNonQuery()

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupCheckPmt.ShowOnPageLoad = False

        'just update the grid and totals, instead of a total redirect to itself
        bindgrid()
        UpdateTotals()
        'Session("payment") = True

    End Sub


    Protected Sub btn_commit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_commit.Click


            If GridviewPmts.Items.Count = 0 Then  'Alice add for request 167
                Exit Sub
            End If
            Dim objsql As OracleCommand
            Dim sql, sql2 As String
            Dim MyDataReader As OracleDataReader
            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim so_wr_dt As String = ""
        Dim store_cd As String = ""
        Dim so_seq_num As String = ""
        Dim promo_cd As String = ""
        Dim order_tp_cd As String = ""
        Dim ivc_cd As String = txt_ivc_cd.Text.Trim
        Dim adj_ivc_cd As String = ""
        Dim fin_co As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        'lucy  
        If Session("vd_inspmt") = "Y" Then
            store_cd = Session("store_cd")
            so_wr_dt = Today.Date
        End If

        'sabrina tdpl
        If Session("td_instpmt") = "Y" Then
            store_cd = Session("store_cd")
            so_wr_dt = Today.Date
        End If

            'Alice add for request 167
            If Session("OtherInvoice") = "Y" Then
                store_cd = cbo_store_cd.Value
                so_wr_dt = Today.Date
            End If

        sql = "SELECT SO_WR_DT, SO_STORE_CD, ORD_TP_CD, so_seq_num FROM SO WHERE DEL_DOC_NUM='" & ivc_cd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

            Do While MyDataReader.Read()
                so_wr_dt = FormatDateTime(MyDataReader.Item("SO_WR_DT").ToString, DateFormat.ShortDate)
                store_cd = MyDataReader.Item("SO_STORE_CD").ToString
                order_tp_cd = MyDataReader.Item("ORD_TP_CD").ToString
                so_seq_num = MyDataReader.Item("so_seq_num").ToString
            Loop
        Catch
            Throw
            conn.Close()
        End Try

        If order_tp_cd = AppConstants.Order.TYPE_CRM OrElse order_tp_cd = AppConstants.Order.TYPE_MCR OrElse order_tp_cd = AppConstants.Order.TYPE_MDB Then
            adj_ivc_cd = ivc_cd
            ivc_cd = ""
            sql = "SELECT IVC_CD FROM AR_TRN, SO WHERE AR_TRN.ADJ_IVC_CD=SO.DEL_DOC_NUM AND AR_TRN.CUST_CD=SO.CUST_CD AND SO.DEL_DOC_NUM='" & adj_ivc_cd & "' AND TRN_TP_CD='" & order_tp_cd & "'"
            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                Do While MyDataReader.Read()
                    ivc_cd = MyDataReader.Item("IVC_CD").ToString
                Loop
            Catch
                Throw
                conn.Close()
            End Try
        End If

        Dim Error_Found As Boolean = False
        If store_cd & "" = "" Then
            Error_Found = True
            lbl_Error.Text = "Store Code cannot be blank"
        End If
        If so_wr_dt & "" = "" Then
            Error_Found = True
            lbl_Error.Text = lbl_Error.Text & vbCrLf & "Could not find sale written date"
        End If
        If txt_del_doc_num.Text.Trim & "" = "" And txt_ivc_cd.Text.Trim & "" = "" Then 'lucy ,because txt_del_doc_num has no value
            'If txt_ivc_cd.Text.Trim & "" = "" Then
            Error_Found = True
            lbl_Error.Text = lbl_Error.Text & vbCrLf & "Please select an invoice to continue"
        End If
        If lbl_co_cd.Text & "" = "" Then
            If cbo_store_cd.Value & "" <> "" Then
                GetCompanyCode()
            End If
            If lbl_co_cd.Text & "" = "" And cbo_store_cd.Value & "" <> "" Then
                Error_Found = True
                lbl_Error.Text = lbl_Error.Text & vbCrLf & "Could not locate company code for the selected store"
            End If
        End If
        If txt_cust_cd.Text & "" = "" Then
            Error_Found = True
            lbl_Error.Text = lbl_Error.Text & vbCrLf & "Please enter a customer to continue"
        End If
        If cbo_csh_drw.Value & "" = "" Then
            Error_Found = True
            lbl_Error.Text = lbl_Error.Text & vbCrLf & "Invalid Cash Drawer"
        End If

        If Error_Found = True Then Exit Sub

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyDataReader2 As OracleDataReader
        conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()
        'Insert Payments

        Dim Auth_No As String = ""
        Dim Trans_Id As String = ""
        Dim Host_Response_Cd As String = ""
        Dim Protobase_Response_Msg As String = ""
        Dim PB_Response_Cd As String = ""
        Dim Host_Response_Msg As String = ""
        Dim Host_Merchant_ID As String = ""
        Dim bnk_crd_num As String = ""
        Dim SDC_Response As String = ""
        Dim order_tp As String = cbo_payment_type.Value.ToString
        Dim err_msg As String = ""
        Dim cc_resp As String = ""
        Dim Card_Type As String = ""
        Dim exp_dt As String = ""
        Dim ref_num As String = ""
        Dim merchant_id As String = ""
        Dim lngSEQNUM As Integer, strSO_SEQ_NUM As String
        Dim strMidCmnts As String
        Dim wdr As Boolean = False
        Dim x As Integer = 0
        Dim Transport As String = ""
        Dim delDocNum As String = txt_ivc_cd.Text.Trim
        Dim plan_desc As String = ""
        Dim rate_info As String = ""
        Dim intro_rate As String = ""
        Dim v_dcs_selected As String = ""

        '   WARNING - SEE NOTE BELOW ABOVE LockSoRec FOR USE OF THIS dbCmd
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(conn)

        Dim doc_seq_num As String = "" 'lucy

        Try

            If thePaymentBiz.HasFinancePayment(Session.SessionID.ToString) Then

                ' NOTE THAT SINCE THIS DBCMD IS NOT USING A TRANSACTION, THE LOCK IS ONLY GOOD FOR ONE UPDATE
                '    TO THE COMMAND;  AT THIS POINT, THE CODE IS SETUP FOR JUST SO UPDATE USING THIS COMMAND
                '    WHICH LOCKS THE SO;  AFTER UPDATE OF SO, IT IS NO LONGER LOCKED;  TO USE FOR MULTIPLE
                '    UPDATES, HAVE TO CHANGE TO USE TRANSACTION BUT NEED TO REWORK PER PAYMENT THEN SO DO NOT
                '    SAVE PART OF A PAYMENT (PART UPDATE IN TRANSACTION, PART NOT IN TRANSACTION) AND THAT DO NOT
                '    ALLOW RE_AUTH'ing/SALE AGAINST SAME PAYMENT TWICE WHICH WOULD OCCUR IF COMPLETED A BC PAYMENT
                '    AND THEN FAILED ON FINANCE BUT DID NOT DELETE THE PAYMENT ENTRY.
                Try
                    Dim soRowid As String = SalesUtils.LockSoRec(delDocNum, dbCmd)

                Catch ex As Exception
                    Throw ex
                End Try

            End If
            'Daniela move up
            'Dim doc_seq_num As String = "" 'lucy        

            ' Dim l_del_doc_num As String = soKeys.soDocNum.delDocNum
            'start lucy
            If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
            Dim lucy_soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(cbo_store_cd.Value, Session("tran_dt"))  'lucy
            Dim lucy_del_doc_num As String = lucy_soKeyInfo.soDocNum.delDocNum
            doc_seq_num = Right(lucy_del_doc_num, 4)

            sql = "SELECT * FROM payment where sessionid='" & Session.SessionID.ToString.Trim & "' order by ROW_ID"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)


            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            Do While MyDataReader2.Read()
                plan_desc = ""
                rate_info = ""
                intro_rate = ""
                bnk_crd_num = ""
                Auth_No = ""
                Trans_Id = ""
                Host_Response_Cd = ""
                Host_Merchant_ID = ""
                PB_Response_Cd = ""
                Host_Response_Msg = ""
                Protobase_Response_Msg = ""
                ref_num = ""
                merchant_id = ""
                Card_Type = ""
                partial_amt = 0

                Insert_Payment = True

                ' Daniela TD plan save
                Dim tdPlan As String
                Dim tdBankCard As String
                If MyDataReader2.Item("MOP_CD").ToString = "TDF" Then
                    tdPlan = MyDataReader2.Item("FIN_CO").ToString
                    tdBankCard = MyDataReader2.Item("BC").ToString
                Else : tdPlan = ""
                End If

                ' SABRINA R1999
                Dim brk_vdplan As String
                If MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.DESJARDINS Or MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.FLEXITI Or MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.MCFinancial Then
                    v_dcs_selected = "Y"
                    brk_vdplan = MyDataReader2.Item("FIN_CO").ToString
                Else : brk_vdplan = ""
                End If

                If Not String.IsNullOrEmpty(merchant_id) Then
                    If MyDataReader2.Item("BC").ToString & "" <> "" And MyDataReader2.Item("MOP_TP").ToString = "BC" And ConfigurationManager.AppSettings("cc") = "Y" Then
                        Insert_Payment = False
                        If order_tp = "PMT" Then
                            SDC_Response = SD_Submit_Query_CC(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_SALE, delDocNum)
                        Else
                            SDC_Response = SD_Submit_Query_CC(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_REFUND, delDocNum)
                        End If


                        Dim SDC_Fields As String() = Nothing
                        Dim SDC_BreakOut As String() = Nothing

                        If SDC_Response = AUTH_INQUIRY Then
                            SDC_Response = SD_Submit_Query_CC(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, delDocNum)
                        End If

                        If InStr(SDC_Response, " this stream") > 0 Then
                            If InStr(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                Dim sCC As Array
                                sCC = Split(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^")
                                bnk_crd_num = Right(sCC(0).Trim, 4)
                            Else
                                bnk_crd_num = Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE")
                            End If
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "PMT", txt_ivc_cd.Text)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        ElseIf InStr(SDC_Response, "Protobase is not responding") > 0 Then
                            If InStr(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                Dim sCC As Array
                                sCC = Split(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^")
                                bnk_crd_num = Right(sCC(0).Trim, 4)
                            Else
                                bnk_crd_num = Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE")
                            End If
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "PMT", delDocNum)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        ElseIf SDC_Response & "" = "" Then
                            If InStr(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                Dim sCC As Array
                                sCC = Split(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^")
                                bnk_crd_num = Right(sCC(0).Trim, 4)
                            Else
                                bnk_crd_num = Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE")
                            End If
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "PMT", delDocNum)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        Else
                            Dim sep(3) As Char
                            sep(0) = Chr(10)
                            sep(1) = Chr(12)
                            SDC_Fields = SDC_Response.Split(sep)
                            Dim s As String
                            Dim MsgArray As Array
                            For Each s In SDC_Fields
                                If InStr(s, ",") > 0 Then
                                    MsgArray = Split(s, ",")
                                    Select Case MsgArray(0)
                                        Case "0002"
                                            partial_amt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "0003"
                                            Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "0004"
                                            exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                        Case "0006"
                                            Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "0007"
                                            ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1000"
                                            Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1003"
                                            PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1004"
                                            Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1005"
                                            Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1008"
                                            bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                        Case "1009"
                                            Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        Case "1010"
                                            Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    End Select
                                End If
                            Next s
                            Select Case PB_Response_Cd
                                Case "0"
                                    cc_resp = "APPROVED"
                                Case "0000"
                                    cc_resp = "APPROVED"
                                Case "60"
                                    cc_resp = "DECLINED"
                                Case "0060"
                                    cc_resp = "DECLINED"
                                Case Else
                                    Host_Response_Msg = PB_Response_Cd
                                    Host_Response_Cd = Protobase_Response_Msg
                                    cc_resp = "ERROR"
                            End Select
                            If InStr(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 And bnk_crd_num & "" <> "" Then
                                Dim sCC As Array
                                sCC = Split(Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE"), "^")
                                bnk_crd_num = Right(sCC(0).Trim, 4)
                            End If
                            If cc_resp = "APPROVED" Then
                                Insert_Payment = True
                                theSystemBiz.SaveAuditLogComment("PAYMENT_SUCCESS", "Sale " & delDocNum & " bankcard transaction for " & MyDataReader2.Item("AMT").ToString & " succeeded for card " & Right(bnk_crd_num, 4))
                                If ConfigurationManager.AppSettings("partial_pay") = "Y" Then
                                    If CDbl(MyDataReader2.Item("AMT").ToString) <> CDbl(partial_amt) Then
                                        theSystemBiz.SaveAuditLogComment("PAYMENT_PARTIAL", "Sale " & delDocNum & " bankcard transaction for " & MyDataReader2.Item("AMT").ToString & " approved for " & partial_amt & " for card " & Right(bnk_crd_num, 4))
                                        Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "PARTIAL AUTHORIZATION", "PARTIAL AUTHORIZATION", "EXPECTED:" & MyDataReader2.Item("AMT").ToString & ", ACTUAL:" & partial_amt, "DEP", delDocNum)
                                        err_msg = "TRUE"
                                        sql = "UPDATE PAYMENT_HOLDING SET AMT=:AMT WHERE ROW_ID=:ROW_ID"

                                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

                                        objsql2.Parameters.Add(":AMT", OracleType.Number)
                                        objsql2.Parameters.Add(":ROW_ID", OracleType.Number)

                                        objsql2.Parameters(":AMT").Value = partial_amt
                                        objsql2.Parameters(":ROW_ID").Value = MyDataReader2.Item("ROW_ID").ToString

                                        objsql2.ExecuteNonQuery()
                                    End If
                                End If
                            Else
                                Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", delDocNum)
                                Insert_Payment = False
                                theSystemBiz.SaveAuditLogComment("PAYMENT_FAILURE", "Sale " & delDocNum & " bankcard transaction for " & MyDataReader2.Item("AMT").ToString & " failed for card " & Right(bnk_crd_num, 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                                err_msg = "TRUE"
                            End If
                        End If
                    End If
                    If MyDataReader2.Item("MOP_TP").ToString = "CK" And ConfigurationManager.AppSettings("chk_guar") = "Y" Then
                        cc_resp = NCT_Submit_Query(MyDataReader2.Item("ROW_ID").ToString, merchant_id)
                        If InStr(LCase(cc_resp), "approval") > 0 Then
                            Auth_No = Right(cc_resp, cc_resp.IndexOf("Message = ") - 10).Trim
                            Auth_No = Replace(Auth_No, "=", "")
                            Insert_Payment = True
                        ElseIf InStr(LCase(cc_resp), "denied") > 0 Then
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Check " & MyDataReader2.Item("CHK_NUM").ToString & " was declined", "PMT", delDocNum)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        Else
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Check " & MyDataReader2.Item("CHK_NUM").ToString & " was not approved. " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5), "PMT", delDocNum)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        End If
                    End If
                    '
                    'Process Gift Cards through World Gift Card
                    '
                    Dim xml_document As New XmlDocument
                    If MyDataReader2.Item("MOP_TP").ToString = "GC" And ConfigurationManager.AppSettings("gift_card") = "Y" Then
                        If order_tp = "PMT" Then
                            xml_document = WGC_SendRequestAndGetResponse(merchant_id, "sale", MyDataReader2.Item("BC").ToString, MyDataReader2.Item("AMT").ToString)
                            cc_resp = xml_document.InnerText.ToString
                        Else
                            xml_document = WGC_SendRequestAndGetResponse(merchant_id, "sale", MyDataReader2.Item("BC").ToString, CDbl(MyDataReader2.Item("AMT").ToString) * -1)
                            cc_resp = xml_document.InnerText.ToString
                        End If
                        If IsNumeric(cc_resp) Then
                            Insert_Payment = True
                            bnk_crd_num = MyDataReader2.Item("BC").ToString
                        Else
                            Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", cc_resp, "PMT", delDocNum)
                            err_msg = "TRUE"
                            Insert_Payment = False
                        End If
                    End If
                    If MyDataReader2.Item("MOP_TP").ToString = "FI" And ConfigurationManager.AppSettings("finance") = "Y" Then
                        Insert_Payment = False
                        '
                        'Refunds post immediately to the customer's account.  For now, post refund in GERS
                        'and let the settlement process create the Refnd entry when the order is finalized.
                        '
                        If Is_Finance_DP(MyDataReader2.Item("MOP_CD").ToString) = False And order_tp <> "PMT" Then
                            Transport = ""
                            Insert_Payment = True
                        Else
                            Transport = Lookup_Transport(MyDataReader2.Item("MOP_TP").ToString, cbo_store_cd.Value, MyDataReader2.Item("MOP_CD").ToString)
                        End If

                        If Transport = "GE_182" Or Transport = "WF_148" Then
                            '
                            'Process Finance Authorization through the Protobase Instance
                            '
                            If order_tp = "PMT" Then
                                If InStr(DP_String_Creation, MyDataReader2.Item("MOP_CD").ToString) > 0 Then
                                    'if this finance mop cd is found as FINANCE_DEPOSIT_TP='Y' in the settlment_mop table, treat it as a auth sale -02
                                    SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_SALE, delDocNum)
                                Else
                                    SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_ONLY, delDocNum)
                                End If
                            Else
                                SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_REFUND, delDocNum)
                            End If

                            Dim SDC_Fields As String() = Nothing
                            Dim SDC_BreakOut As String() = Nothing

                            If SDC_Response = AUTH_INQUIRY Then
                                SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, delDocNum)
                            End If

                            If InStr(SDC_Response, " this stream") > 0 Then
                                Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", delDocNum)
                                err_msg = "TRUE"
                                Insert_Payment = False
                            ElseIf InStr(SDC_Response, "Protobase is not responding") > 0 Then
                                Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", delDocNum)
                                err_msg = "TRUE"
                                Insert_Payment = False
                            ElseIf SDC_Response & "" = "" Then
                                Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", delDocNum)
                                err_msg = "TRUE"
                                Insert_Payment = False
                            Else
                                'means a successful FI auth response was received
                                Dim sep(3) As Char
                                sep(0) = Chr(10)
                                sep(1) = Chr(12)
                                SDC_Fields = SDC_Response.Split(sep)
                                Dim s As String
                                Dim MsgArray As Array
                                For Each s In SDC_Fields
                                    If InStr(s, ",") > 0 Then
                                        MsgArray = Split(s, ",")
                                        Select Case MsgArray(0)
                                            Case "0003"
                                                Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "0004"
                                                If (MsgArray(1).ToString.isNotEmpty) Then
                                                    exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                                End If

                                            Case "0006"
                                                Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "0007"
                                                ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                'Case "1000"
                                                '    Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "1003"
                                                PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "1004"
                                                Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                'Case "1005"
                                                '    Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "1008"
                                                bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                            Case "1009"
                                                Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "1010"
                                                Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "3319"
                                                plan_desc = ""
                                                For x = LBound(MsgArray) To UBound(MsgArray)
                                                    If x > 0 Then
                                                        plan_desc = plan_desc & MsgArray(x) & ","
                                                    End If
                                                Next
                                                plan_desc = Left(plan_desc, Len(plan_desc) - 1)
                                            Case "3322"
                                                rate_info = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            Case "3325"
                                                intro_rate = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        End Select
                                    End If
                                Next s
                                Select Case PB_Response_Cd
                                    Case "0"
                                        cc_resp = "APPROVED"
                                    Case "0000"
                                        cc_resp = "APPROVED"
                                    Case "60"
                                        cc_resp = "DECLINED"
                                    Case "0060"
                                        cc_resp = "DECLINED"
                                    Case Else
                                        Host_Response_Msg = PB_Response_Cd
                                        Host_Response_Cd = Protobase_Response_Msg
                                        cc_resp = "ERROR"
                                End Select
                                If cc_resp = "APPROVED" Then
                                    Insert_Payment = True
                                Else
                                    Update_Payment_Holding(MyDataReader2.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", delDocNum)
                                    Insert_Payment = False
                                    err_msg = "TRUE"
                                End If
                            End If
                            If Not String.IsNullOrEmpty(MyDataReader2.Item("FI_ACCT_NUM").ToString) Then
                                bnk_crd_num = "FI"
                            End If
                        End If
                    ElseIf Transport = "ADS" Then

                    End If      '***end if finance mop type
                End If            ' TODO- add an else here to display a message if terminal/merchant id is null in payment_xref table

                Dim isFinanceDP As Boolean = Is_Finance_DP(MyDataReader2.Item("MOP_CD").ToString)
                If MyDataReader2.Item("MOP_TP").ToString = "FI" And Insert_Payment = True Then

                    ' If Not String.IsNullOrEmpty(MyDataReader2.Item("FI_ACCT_NUM").ToString) Then
                    'lucy modified above line
                    If Not String.IsNullOrEmpty(MyDataReader2.Item("FI_ACCT_NUM").ToString) Or (MyDataReader2.Item("mop_cd").ToString = "FI" And Not String.IsNullOrEmpty(MyDataReader2.Item("FIn_co").ToString)) Then
                        bnk_crd_num = "FI"
                    End If

                    Insert_Payment = False
                    fin_co = MyDataReader2.Item("FIN_CO").ToString
                    Dim rate_info_array As Array = Nothing
                    Dim intro_rate_array As Array = Nothing
                    Dim during_promo_apr As String = String.Empty
                    Dim during_promo_apr_flag As String = String.Empty
                    Dim after_promo_apr As String = String.Empty
                    Dim after_promo_apr_flag As String = String.Empty
                    Dim promo_duration As String = String.Empty
                    Dim aspPromoCd As String = String.Empty

                    If plan_desc.isNotEmpty Then
                        'this is the promo/during promo info
                        If InStr(intro_rate, ";") > 0 Then
                            intro_rate_array = Split(intro_rate, ";")
                            If (Not intro_rate Is Nothing) Then
                                during_promo_apr_flag = intro_rate_array(0)
                                during_promo_apr = intro_rate_array(1)
                                promo_duration = intro_rate_array(4)
                            End If
                        End If

                        'this is the account/after promo info 
                        If InStr(rate_info, ";") > 0 Then
                            rate_info_array = Split(rate_info, ";")
                            If (Not rate_info_array Is Nothing) Then
                                after_promo_apr_flag = rate_info_array(4)
                                after_promo_apr = rate_info_array(6)
                            End If
                        End If

                        sql = "INSERT INTO SETTLEMENT_TERMS (DEL_DOC_NUM, ORD_TP_CD, RATE_DEL_RATE_TYPE, RATE_DEL_APR, INTRO_RATE, INTRO_APR, INTRO_DURATION, PLAN_DESC) "
                        sql = sql & "VALUES (:DEL_DOC_NUM, :ORD_TP_CD, :RATE_DEL_RATE_TYPE, :RATE_DEL_APR, :INTRO_RATE, :INTRO_APR, :INTRO_DURATION, :PLAN_DESC) "
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

                        objsql2.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                        objsql2.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
                        objsql2.Parameters.Add(":RATE_DEL_RATE_TYPE", OracleType.VarChar)
                        objsql2.Parameters.Add(":RATE_DEL_APR", OracleType.VarChar)
                        objsql2.Parameters.Add(":INTRO_RATE", OracleType.VarChar)
                        objsql2.Parameters.Add(":INTRO_APR", OracleType.VarChar)
                        objsql2.Parameters.Add(":INTRO_DURATION", OracleType.VarChar)
                        objsql2.Parameters.Add(":PLAN_DESC", OracleType.VarChar)

                        objsql2.Parameters(":DEL_DOC_NUM").Value = delDocNum
                        objsql2.Parameters(":ORD_TP_CD").Value = MyDataReader2.Item("MOP_CD").ToString
                        objsql2.Parameters(":RATE_DEL_RATE_TYPE").Value = after_promo_apr_flag  'rate_info_array(4).ToString
                        objsql2.Parameters(":RATE_DEL_APR").Value = after_promo_apr   'rate_info_array(6).ToString
                        objsql2.Parameters(":INTRO_RATE").Value = during_promo_apr      'intro_rate_array(0).ToString
                        objsql2.Parameters(":INTRO_APR").Value = during_promo_apr         'intro_rate_array(1).ToString
                        objsql2.Parameters(":INTRO_DURATION").Value = promo_duration 'intro_rate_array(4).ToString
                        objsql2.Parameters(":PLAN_DESC").Value = plan_desc.trimString(0, 50)             'the desc in the response needs to be trimmed before saving 

                        objsql2.ExecuteNonQuery()
                    Else
                        theSystemBiz.SaveAuditLogComment("FINANCE_PROMO_FAILURE", "Sales Order " & delDocNum & " did not retrieve promotional terms")
                    End If

                    If Not isFinanceDP Then
                        promo_cd = MyDataReader2.Item("FIN_PROMO_CD").ToString
                        sql = "SELECT DEL_DOC_NUM FROM SO_ASP WHERE DEL_DOC_NUM = '" & delDocNum & "'"
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)


                        '****************** Insert the credit entry details into SO_ASP table, if it does not exist *****************
                        If Not MyDataReader.Read() Then
                            MyDataReader.Close()

                            ''get  first the ASP_PROMO info like promo des, etc.
                            Dim promoDes As String = String.Empty
                            Dim promoDes3 As String = String.Empty
                            Dim ds As DataSet = GetAspPromoDetails(MyDataReader2.Item("FIN_CO"), promo_cd)
                            For Each dr In ds.Tables(0).Rows
                                If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                                If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                                If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
                            Next

                            If IsNumeric(during_promo_apr) Then
                                during_promo_apr = during_promo_apr / 10000 & "% "
                            End If
                            If IsNumeric(after_promo_apr) Then
                                after_promo_apr = after_promo_apr / 10000 & "% "
                            End If
                            sql = "INSERT INTO SO_ASP (" &
                                        "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                                        "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                        "ACCT_APR, ACCT_APR_FLAG)" &
                                    " VALUES('" & delDocNum & "', 'Y', '" & promo_cd & "','" & aspPromoCd & "','" & promoDes & "','" & plan_desc & "','" &
                                                           promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                                           after_promo_apr & "','" & after_promo_apr_flag & "')"
                            sql = UCase(sql)
                            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                            objsql.ExecuteNonQuery()
                        End If

                        If Auth_No.isEmpty Then Auth_No = MyDataReader2.Item("APPROVAL_CD").ToString
                        sql = "UPDATE SO SET APPROVAL_CD='" & Auth_No & "' "
                        If IsNumeric(MyDataReader2.Item("AMT").ToString) Then
                            sql = sql & ", ORIG_FI_AMT=" & MyDataReader2.Item("AMT").ToString
                        End If
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("FIN_CO").ToString) Then
                            sql = sql & ", FIN_CUST_CD='" & MyDataReader2.Item("FIN_CO").ToString & "'"
                        End If
                        sql = sql & " WHERE DEL_DOC_NUM='" & delDocNum & "'"
                        sql = UCase(sql)
                        dbCmd.Parameters.Clear()
                        dbCmd.CommandText = sql
                        dbCmd.ExecuteNonQuery()
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("FI_ACCT_NUM").ToString) Then  'lucy
                            Trans_Id = Decrypt(MyDataReader2.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                            If (IsSwiped(Trans_Id)) Then
                                Trans_Id = GetAccountNumber(Trans_Id)
                            End If
                        End If  'lucy
                        Dim expDt As String = MyDataReader2.Item("EXP_DT").ToString 'lucy

                        If AppConstants.Order.TYPE_SAL = order_tp_cd Then
                            sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & Trans_Id & "', MOP_CD='FI',exp_dt= TO_DATE('" & expDt & "','mm/dd/RRRR'), HOST_REF_NUM='" & MyDataReader2.Item("ROW_ID").ToString &
                                    "', REF_NUM = '" & ref_num & "'" & " WHERE IVC_CD='" & delDocNum & "' AND TRN_TP_CD='" & order_tp_cd & "'"      'lucy add exp_dt

                        Else
                            sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & Trans_Id & "', MOP_CD='FI', HOST_REF_NUM='" & MyDataReader2.Item("ROW_ID").ToString &
                                        "', REF_NUM = '" & ref_num & "'" & " WHERE ADJ_IVC_CD='" & delDocNum & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                        End If
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                    ElseIf isFinanceDP Then
                        wdr = True
                        Insert_Payment = True
                        lngSEQNUM = 1

                        x = 1
                        Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
                        cmntKey.wrDt = FormatDateTime(so_wr_dt, DateFormat.ShortDate)
                        cmntKey.storeCd = store_cd
                        cmntKey.seqNum = so_seq_num
                        lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey)
                        strSO_SEQ_NUM = so_seq_num

                        strMidCmnts = "Promotion-Code Assigned to WDR Deposit = " & MyDataReader2.Item("FIN_PROMO_CD").ToString
                        'If Len(del_doc_num) = 12 Then
                        '    strSO_SEQ_NUM = Mid(del_doc_num, 8, 4)
                        'Else
                        '    strSO_SEQ_NUM = Right(del_doc_num, 4)
                        'End If
                        sql = "insert into SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM) Values ( " & _
                                    "TO_DATE('" & so_wr_dt & "','mm/dd/RRRR')" & ",'" & store_cd & "','" & strSO_SEQ_NUM & "'," & _
                                    lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & _
                                    strMidCmnts & "','" & delDocNum & "')"  ' pmt to delDoc
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()
                    End If     'end if Is_Finance_DP type

                    '*** Save the asp response info when an approval is recieved for a finance auth
                    SaveAspResponseCodeInfo(fin_co, delDocNum)
                ElseIf MyDataReader2.Item("MOP_TP").ToString = "FI" AndAlso Insert_Payment = False AndAlso (Not isFinanceDP) Then
                    'if the finance auth failed and the the provider is not a 'DP' type 
                    sql = "UPDATE SO SET ORIG_FI_AMT=NULL, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & delDocNum & "'"
                    dbCmd.Parameters.Clear()
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                    If AppConstants.Order.TYPE_SAL = order_tp_cd Then
                        If order_tp_cd = "SAL" Then 'lucy add this if condition, 3 lines
                            sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE IVC_CD='" & delDocNum & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                        End If

                    Else
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE ADJ_IVC_CD='" & delDocNum & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                    End If
                    objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql2.ExecuteNonQuery()
                End If           '****end if it's a finance and insert_payment= true

                If Insert_Payment = True Then

                    'means this could be any type of pmt - CHK, CC, FI DP, GC etc. 
                    'sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, CSH_DWR_CD, ORIGIN_CD "
                    'lucy modified ,add ADJ_IVC_CD to the line
                    ' Daniela removed DES tdPlan
                    ' Daniela change PMT_STORE logic

                    Dim pmtStore As String
                    If Not Session("clientip") Is Nothing Then
                        pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
                    Else ' if not go global then default to remote ip
                        pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
                    End If
                    If pmtStore & "" = "" Then
                        pmtStore = cbo_store_cd.Value
                    End If

                    'sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD,ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, PMT_STORE, WR_DT, CSH_DWR_CD, ORIGIN_CD,ADJ_IVC_CD,doc_seq_num "
                    sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, adj_ivc_cd, PMT_STORE, WR_DT, CSH_DWR_CD, ORIGIN_CD, doc_seq_num "
                    sql2 = " VALUES('" & lbl_co_cd.Text & "','" & txt_cust_cd.Text & "','" & MyDataReader2.Item("MOP_CD").ToString & "'"
                    sql2 = sql2 & ",'"
                    If ivc_cd & "" = "" Then
                        sql2 = sql2 & Mid(delDocNum, 6, 2)
                    Else
                        sql2 = sql2 & store_cd
                    End If
                    sql2 = sql2 & "','" & order_tp & "',"
                    If ConfigurationManager.AppSettings("partial_pay") = "Y" Then
                        If CDbl(MyDataReader2.Item("AMT").ToString) <> CDbl(partial_amt) And partial_amt > 0 Then
                            sql2 = sql2 & partial_amt
                        Else
                            sql2 = sql2 & MyDataReader2.Item("AMT").ToString
                        End If
                    Else
                        sql2 = sql2 & MyDataReader2.Item("AMT").ToString
                    End If

                    sql2 = sql2 & ", TO_DATE('" & lbl_post_dt.Text & "','mm/dd/RRRR'),'T','O', '"
                    ' if not sale and has orig doc, then ivc_cd = orig doc and adj_ivc_cd is MDB/MCR/CRM doc
                    If order_tp_cd <> AppConstants.Order.TYPE_SAL AndAlso adj_ivc_cd.isNotEmpty Then
                        sql2 = sql2 & ivc_cd & "', '" & adj_ivc_cd
                    Else
                        sql2 = sql2 & delDocNum & "', '"
                    End If
                    'sql2 = sql2 & "', '" & cbo_store_cd.Value & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"
                    ' Daniela use pmtStore
                    sql2 = sql2 & "', '" & pmtStore & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"
                    sql2 = sql2 & ",'" & cbo_csh_drw.Value & "','POS'"

                    sql2 = sql2 & ",'" & doc_seq_num & "'"   'lucy add doc_seq_num

                    If lbl_emp_cd.Text & "" <> "" Then
                        sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                        sql2 = sql2 & ", '" & lbl_emp_cd.Text & "','" & lbl_emp_cd.Text & "'"
                    End If

                    If bnk_crd_num = "FI" Then
                        Dim acctData As String = Decrypt(MyDataReader2.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                        If (IsSwiped(acctData)) Then
                            'extract the account# from the decrypted track info, so it can be saved into AR tables
                            acctData = GetAccountNumber(acctData)
                        End If
                        sql = sql & ", BNK_CRD_NUM"
                        sql2 = sql2 & ", '" & acctData & "'"
                    ElseIf MyDataReader2.Item("MOP_TP").ToString = "GC" Then
                        bnk_crd_num = Left(MyDataReader2.Item("BC").ToString, 20) 'sabrina add 
                        sql = sql & ", BNK_CRD_NUM"
                        sql2 = sql2 & ", '" & bnk_crd_num & "'"
                    ElseIf MyDataReader2.Item("BC").ToString & "" <> "" Then
                        If MyDataReader2.Item("MOP_CD").ToString = "AMX" Then
                            bnk_crd_num = Left(MyDataReader2.Item("BC").ToString, 15) 'daniela add
                        Else
                            bnk_crd_num = Left(MyDataReader2.Item("BC").ToString, 16) 'lucy add
                        End If
                        If IsNumeric(bnk_crd_num) Then
                            sql = sql & ", BNK_CRD_NUM"
                            'Daniela comment sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                            sql2 = sql2 & ", '" & bnk_crd_num & "'"
                        Else
                            Dim acctData As String = Decrypt(MyDataReader2.Item("BC").ToString, "CrOcOdIlE")
                            If (IsSwiped(acctData)) Then
                                acctData = GetAccountNumber(acctData)
                            End If
                            bnk_crd_num = Right(acctData, 4)
                            sql = sql & ", BNK_CRD_NUM"
                            sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                        End If
                    End If
                    If MyDataReader2.Item("CHK_NUM").ToString() & "" <> "" Then
                        sql = sql & ", CHK_NUM"
                        sql2 = sql2 & ", '" & MyDataReader2.Item("CHK_NUM").ToString() & "' "
                    End If

                    Dim expDt As String = MyDataReader2.Item("EXP_DT").ToString
                    If IsDate(expDt) Then
                        sql = sql & ", EXP_DT"
                        sql2 = sql2 & ", TO_DATE('" & expDt & "','mm/dd/RRRR')"
                    End If
                    If Card_Type & "" <> "" Then
                        sql = sql & ", REF_NUM"
                        sql2 = sql2 & ", '" & Card_Type & "' "
                    End If
                    If Host_Merchant_ID & "" <> "" And tdPlan & "" = "" Then
                        sql = sql & ", DES"
                        sql2 = sql2 & ", 'MID:" & Host_Merchant_ID & "' "
                    End If
                    If tdPlan & "" <> "" Then ' Daniela added 
                        sql = sql & ", DES"
                        sql2 = sql2 & ", '" & tdPlan & "' "
                    End If
                    'sabrina tdpl
                    If Session("td_instpmt") = "Y" Then
                        sql = sql & ", DES"
                        sql2 = sql2 & ", '" & Session("tdpl_acct_num") & "' "
                    End If

                    'sabrina R1999
                    If MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.DESJARDINS Then  'sabrina Nov10 R1999
                        If Session("dcs_plan") & "" <> "" Then
                            sql = sql & ", DES"
                            sql2 = sql2 & ", '" & Session("dcs_plan") & "' "
                        Else
                            If brk_vdplan & "" <> "" Then
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan & "' "
                            End If
                        End If
                    End If

                    'Alice added on Oct 10, 2018 for request 4115
                    If MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.FLEXITI Then 'sabrina Nov10 R1999
                        If Session("flx_plan") & "" <> "" Then
                            sql = sql & ", DES"
                            sql2 = sql2 & ", '" & Session("flx_plan") & "' "
                        Else
                            If brk_vdplan & "" <> "" Then
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan & "' "
                            End If
                        End If
                    End If

                    'Alice added on May 31, 2019 for request 235
                    If MyDataReader2.Item("MOP_CD").ToString = PaymentUtils.Bank_MOP_CD.MCFinancial Then
                        If Session("fm_plan") & "" <> "" Then
                            sql = sql & ", DES"
                            sql2 = sql2 & ", '" & Session("fm_plan") & "' "
                        Else
                            If brk_vdplan & "" <> "" Then
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan & "' "
                            End If
                        End If
                    End If

                    'End If ' Daniela APR 28 fix FI issues

                    Auth_No = MyDataReader2.Item("APPROVAL_CD").ToString  'lucy, 23-Jul-14
                    If Auth_No & "" <> "" Then
                        sql = sql & ", APP_CD"
                        sql2 = sql2 & ",'" & Auth_No & "'"
                    End If
                    If ref_num & "" <> "" Then
                        sql = sql & ", HOST_REF_NUM"
                        sql2 = sql2 & ",'" & ref_num & "'"
                    End If

                    'start lucy add this if condition 
                    If Session("vd_inspmt") = "Y" Then
                        ' Daniela May 14 comment to fix multiple payment issues
                        'Session("vd_inspmt") = "N"
                        Dim str_des As String = Session("vd_inspmt_plan_name")
                        Dim str_acct_num As String = Session("vd_inspmt_acct_num")
                        Dim str_bnk_num As String = Session("vd_inspmt_plan_id")
                        lucy_insert_cust_asp(txt_cust_cd.Text, str_bnk_num, str_acct_num)
                        sql = sql & ", des,acct_num,bnk_num"
                        sql2 = sql2 & ",'" & str_des & "','" & str_acct_num & "','" & str_bnk_num & "'"
                    End If
                    'end lucy add

                    sql = UCase(sql)
                    sql2 = UCase(sql2)

                    If Trans_Id & "" <> "" Then
                        sql = sql & ", ACCT_NUM"
                        '** for a Finance 'DP' type PAYMENTS being added, save the unencrypted acct# with the 'ID:' prefix
                        '** to the ar_trn 'Acct_num' column to be able to use that for refunds later.
                        If MyDataReader2.Item("MOP_TP").ToString = "FI" AndAlso order_tp = "DEP" AndAlso isFinanceDP Then
                            Trans_Id = "ID:" & Decrypt(MyDataReader2.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                        End If
                        sql2 = sql2 & ",'" & Trans_Id & "'"
                    End If
                    sql = sql & ")"
                    sql2 = sql2 & ")"
                    sql = sql & sql2

                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql.ExecuteNonQuery()

                    '** after the insert into ar_trn, check if any FI DP type refund records need to be processed
                    '** Need to insert those into asp_store_forward table so it can picked up for settlement
                    ProcessFinanceDepositRefunds(ref_num)
                End If 'insert_payment =true ' Daniela uncomment back APR 28
            Loop

            ' Daniela May 14 to fix multiple payment issues
            Session("vd_inspmt") = "N"

            'Alice add for request 167 - Clear sessions
            Session("OtherInvoice") = ""
            Session("OtherCustCd") = ""
            Session("pa_cust_cd") = ""
            Session("CashhierPwd") = ""

            thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)
            GridviewPmts.DataSource = ""
            GridviewPmts.DataBind()
            MyDataReader2.Close()

            ' SEE NOTE ABOVE FOR USE OF TRANSACTION LOGIC - NEED TO COMMIT PER PAYMENT, INCLUDING HOLD, AR_TRN, PAYMENT READ AND DELETE TO USE
            'If no error occured, save the transaction
            'dbcmd.Transaction.Commit()
        Catch ex As Exception
            ' dbCmd.Transaction.Rollback()
            ' cannot close conn2 (local) in finally here because still needed if processing continues to invoice below; only in catch OK if leaving
            If conn2.State = ConnectionState.Open Then
                conn2.Close()
            End If
            If ex.Message = String.Format(Resources.POSErrors.ERR0042, delDocNum) Then
                lbl_Error.Text = ex.Message
                Exit Sub
            Else
                Throw
            End If
        Finally
            dbCmd.Dispose()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try


        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")
        Dim default_invoice As String = ""

        Try
            If Not String.IsNullOrEmpty(err_msg) Then
                PopupPmtWarnings.ContentUrl = "Payment_Hold.aspx?del_doc_num=" & delDocNum
                PopupPmtWarnings.ShowOnPageLoad = True
            Else
                sql = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & cbo_store_cd.Value & "'"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    Do While MyDataReader2.Read
                        If MyDataReader2.Item("invoice_file").ToString & "" = "" Then
                            default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                        Else
                            default_invoice = MyDataReader2.Item("invoice_file").ToString
                        End If
                    Loop
                    MyDataReader2.Close()
                Catch ex As Exception

                    Throw
                End Try

                If String.IsNullOrEmpty(default_invoice) Then
                    sql = "select store_cd, invoice_file, default_file from invoice_admin"

                    objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    Try
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                        If MyDataReader2.Read Then
                            default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                        End If
                        MyDataReader2.Close()
                    Catch ex As Exception

                        Throw
                    End Try
                End If
                'lucy comment if
                ' If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                'System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike6778", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & delDocNum & "','frmTest67','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ' ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                'System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike6778", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & delDocNum & "&INVOICE_NAME=" & default_invoice & "','frmTest67','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ' End If

                bindgrid()

                If Not String.IsNullOrEmpty(promo_cd) Then
                    Dim fin_invoice As String = ""
                    sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & fin_co & "'"
                    objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    Try
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                        If MyDataReader2.Read Then
                            If wdr = True Then
                                fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                            Else
                                fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                            End If
                        End If
                        MyDataReader2.Close()
                    Catch ex As Exception

                        Throw
                    End Try
                    'If InStr(fin_co, "GE") > 0 Then
                    'lucy comment this line '  System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike877", "window.open('FI_Addendums/GEMONEY.aspx?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & delDocNum.ToString & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    'Else
                    '    ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike877", "window.open('FI_Addendums/ADS.aspx?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & del_doc_num.ToString & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    'End If

                End If
                'Session("pay_pass") = ""
                Dim str_emp_cd As String = Session("emp_cd") 'lucy add 3 lines 
                ' sabrina comment out lucy_print_salesbill(txt_ivc_cd.Text, str_emp_cd, doc_seq_num)

                Session("lucy_display") = ""

                'sabrina tdpl
                If Session("td_instpmt") = "Y" Then
                    'Session("tdpl_amt") = sy_get_total_pymnt()
                    sy_td_instpmt_subtp8()
                    'sy_td_instpmt_ar_trn()
                End If

                'sabrina instpmt-rcpt
                If Session("td_instpmt") = "Y" Or
                    Session("vd_inspmt") = "Y" Or
                    InStr(txt_ivc_cd.Text, "VISAD") > 0 Or
                    InStr(txt_ivc_cd.Text, "BRICK") > 0 Then

                    Dim pmtStr As String
                    Dim v_cust_cd As String
                    If Session("td_instpmt") = "Y" Then
                        v_cust_cd = Session("tdpl_cust_cd")
                    Else
                        v_cust_cd = txt_cust_cd.Text
                    End If
                    If Not Session("clientip") Is Nothing Then
                        pmtStr = LeonsBiz.GetPaymentStore(Session("clientip"))
                    Else ' if not go global then default to remote ip
                        pmtStr = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
                    End If
                    If pmtStr & "" = "" Then
                        pmtStr = cbo_store_cd.Value
                    End If

                    'sabrina R1910 (add printer)
                    Dim v_printer As String
                    If (isNotEmpty(printer_drp.SelectedItem.Value)) Then 'Daniela check if empty
                        v_printer = printer_drp.SelectedItem.Value.ToString()
                        sy_print_instpmt_at_printer(txt_ivc_cd.Text, str_emp_cd, doc_seq_num, pmtStr, v_cust_cd, v_printer)
                    End If
                Else
                    ' Daniela refund print fix add ufm parameter
                    Dim co_cd As String = lbl_co_cd.Text
                    Dim ufm As String = "LPO"  ' Default ufm 
                    'Daniela french UFM
                    Dim cult As String = UICulture
                    If InStr(cult.ToLower, "fr") >= 1 Then
                        ufm = "PRF"
                    End If

                    Dim grpCd As String = ""
                    If isNotEmpty(co_cd) Then
                        grpCd = LeonsBiz.GetGroupCode(co_cd)
                        If grpCd = "BRK" Then
                            If InStr(cult.ToLower, "fr") >= 1 Then
                                ufm = "BFC"
                            Else
                                ufm = "BRC"
                            End If
                        End If
                    End If

                    'sabrina R1999, R2030
                    Dim v_printer As String
                    If (isNotEmpty(printer_drp.SelectedItem.Value)) Then 'Daniela check if empty
                        v_printer = printer_drp.SelectedItem.Value.ToString()
                        If order_tp_cd = AppConstants.Order.TYPE_CRM OrElse order_tp_cd = AppConstants.Order.TYPE_MCR OrElse order_tp_cd = AppConstants.Order.TYPE_MDB Then
                            'lucy_print_salesbill(ivc_cd, str_emp_cd, doc_seq_num)
                            print_salesbill(ivc_cd, str_emp_cd, doc_seq_num, ufm, v_printer)
                        Else
                            print_salesbill(txt_ivc_cd.Text, str_emp_cd, doc_seq_num, ufm, v_printer)
                        End If
                    End If
                End If
                ' DeletePayments(Session.SessionID.ToString.Trim)


                thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)  ' lucy, this new version has this line after conn2.close()
                cbo_payment_type.Value = "PMT"
                txt_ivc_cd.Text = String.Empty
                lbl_Error.Text = String.Empty
                ResetInfo()
                _dsOrigPmtsForRefunds = Nothing
                _hTableOrigPmtAmtsForRefunds = Nothing
                _listOrigPmtsForRefundsDd = Nothing
                _dtRefundDetails = Nothing
                img_qs.Visible = False
                btn_commit.Enabled = False
                lucy_lbl_ind_aprv.Text = ""  'lucy 

                'sabrina tdpl
                If Session("td_instpmt") = "Y" Then
                    lbl_Error.Text = Session("sy_tdpl_msg")
                End If
            End If

            ' Daniela reset session after commit
            Session("td_instpmt") = ""
            Session("dcs_plan") = ""  'sabrina Nov10 R1999
            Session("flx_plan") = ""
            'Daniela Dec 18
            If Request("referrer") = "salesordermaintenance.aspx" Then
                btn_return.Visible = True   'reads 'Return to SOM'
            End If

        Finally
            If conn2.State = ConnectionState.Open Then
                conn2.Close()
            End If
        End Try
    End Sub

    Public Sub sy_td_instpmt_subtp8()
        'sabrina tdpl new sub

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_ack As String
        Dim v_term_id As String

        ' sabrina feb26
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            lbl_Error.Text = "No designated pinpad"
            Exit Sub
        End If


        'Dim v_term_id As String = LeonsBiz.get_term_id(Session("clientip"))
        Dim v_sub_type As String
        Dim v_card_num As String = Session("tdpl_acct_num")
        Dim v_cust_cd As String = Session("tdpl_cust_cd")
        Dim v_expire_date As String = ""
        Dim v_amt As String = Session("tdpl_amt")
        Dim v_auth_no As String
        Dim v_financetp As String = " "
        Dim l_filename As String
        Dim l_printcmd As String
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = "PMT"
        Dim sessionid As String = Session.SessionID.ToString.Trim

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        v_sub_type = "8"

        Dim str_del_doc_num As String = "BRICK PAYMENT"
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp8_td"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = "1249"
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_financetp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = "PMT"
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = v_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(myCMD.Parameters("p_dsp").Value, "Appr") > 0 Then 'sabrina R2019
                If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                    l_aut = myCMD.Parameters("p_aut").Value
                Else
                    l_aut = "empty"
                End If
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value

                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                End If

                l_crn = myCMD.Parameters("p_crn").Value
                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value

                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("tdpl_auth_no") = l_aut
                lucy_lbl_ind_aprv.Text = l_aut & " - " & l_dsp
                sy_td_instpmt_ar_trn()
            Else   'transation not done 
                Session("tdpl_auth_no") = ""
                lucy_lbl_ind_aprv.Text = "xxxx - " & l_dsp
            End If

            'sabrina tdpl add next line
            Session("sy_tdpl_msg") = lucy_lbl_ind_aprv.Text

            myCMD.Cancel()
            myCMD.Dispose()
        Catch
            Throw
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
    Public Sub sy_td_instpmt_ar_trn()

        'sabrina tdpl new sub
        Dim merchant_id As String = ""
        Dim sql As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim v_amt As Double

        Dim pmtStore As String
        If Not Session("clientip") Is Nothing Then
            pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else ' if not go global then default to remote ip
            pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If pmtStore & "" = "" Then
            pmtStore = cbo_store_cd.Value
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        conn.Open()

        'Daniela prevent top sql
        'Jan 15 fix Company code 
        Dim v_store As StoreData
        Dim coCd As String = Session("CO_CD")
        If Not Session("HOME_STORE_CD") = pmtStore Then
            v_store = theSalesBiz.GetStoreInfo(pmtStore)
            coCd = v_store.coCd
        End If
        If isEmpty(Session("CO_CD")) Then
            v_store = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
            coCd = v_store.coCd
            Session("CO_CD") = v_store.coCd
        End If

        sql = "insert into ar_trn (CO_CD,CUST_CD,EMP_CD_CSHR,EMP_CD_OP,ORIGIN_STORE,CSH_DWR_CD,TRN_TP_CD,IVC_CD,"
        sql = sql & "BNK_CRD_NUM,AMT,POST_DT,STAT_CD,AR_TP,APP_CD,DES,PMT_STORE,ORIGIN_CD,CREATE_DT,AR_TRN_PK) "
        sql = sql & " values (:CO_CD,:CUST_CD,:EMP_CD_CSHR,:EMP_CD_OP,:ORIGIN_STORE,:CSH_DWR_CD,:TRN_TP_CD,:IVC_CD,"
        sql = sql & ":BNK_CRD_NUM,:AMT,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & ":STAT_CD,:AR_TP,:APP_CD,:DES,:PMT_STORE,:ORIGIN_CD,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & " seq_ar_trn.nextval)"

        cmdAddRecords.Parameters.Clear()

        Try
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = sql.ToString

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    'Daniela Jan 15
                    'cmdAddRecords.Parameters(":CO_CD").Value = v_store.coCd
                    cmdAddRecords.Parameters(":CO_CD").Value = coCd

                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CUST_CD").Value = Session("tdpl_cust_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_CSHR").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_OP").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    'Daniela use payment store as per Donna Oct 07
                    'cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = pmtStore
                    cmdAddRecords.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CSH_DWR_CD").Value = Session("csh_drw").ToString.Trim

                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = "BCP"

                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":IVC_CD").Value = "BRICK PAYMENT"

                    cmdAddRecords.Parameters.Add(":BNK_CRD_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":BNK_CRD_NUM").Value = Session("tdpl_acct_num")


                    If IsNumeric(Session("tdpl_amt")) Then
                        'v_amt = CInt(Session("tdpl_amt"))
                        v_amt = CDbl(Session("tdpl_amt"))
                    Else
                        v_amt = 0
                    End If

                    cmdAddRecords.Parameters.Add(":AMT", OracleType.Number)
                    cmdAddRecords.Parameters(":AMT").Value = v_amt

                    cmdAddRecords.Parameters.Add(":STAT_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":STAT_CD").Value = "T"

                    cmdAddRecords.Parameters.Add(":AR_TP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":AR_TP").Value = "O"

                    cmdAddRecords.Parameters.Add(":APP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":APP_CD").Value = Session("tdpl_auth_no")

                    cmdAddRecords.Parameters.Add(":DES", OracleType.VarChar)
                    cmdAddRecords.Parameters(":DES").Value = "System Posted"

                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":PMT_STORE").Value = pmtStore

                    cmdAddRecords.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_CD").Value = "PBR"

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try

    End Sub


    ''' <summary>
    ''' Processes a finance 'Deposit' type refund by inserting a record in the ASP_STORE_FORWARD table
    ''' after a 'REFUND' type record has been created in the AR_TRN table for the same FI refund.
    ''' </summary>
    ''' <param name="refNum">the ref num</param>
    ''' <remarks></remarks>
    Private Sub ProcessFinanceDepositRefunds(ByVal refNum As String)
        If (cbo_payment_type.Value = "R") Then
            Dim sqlSb As New StringBuilder
            Dim cmd As OracleCommand
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            Try
                conn.Open()
                sqlSb.Append("SELECT * FROM payment WHERE sessionid=:SessionID AND mop_tp ='FI' ")
                cmd = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)
                cmd.Parameters.Add(":SESSIONID", OracleType.VarChar)
                cmd.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim

                Dim reader As OracleDataReader
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                If (reader.Read()) Then
                    If (Is_Finance_DP(reader.Item("MOP_CD").ToString)) Then

                        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

                        Dim Sql As New StringBuilder
                        Sql.Append(" INSERT INTO AR.ASP_STORE_FORWARD ( ")
                        Sql.Append("    CO_CD, STORE_CD, CSH_DWR_CD,  ")
                        Sql.Append("    AS_CD, CUST_CD, SO_EMP_SLSP_CD1,  ")
                        Sql.Append("    EMP_CD_CSHR, COV_CDS, DEL_DOC_NUM,  ")
                        Sql.Append("    AS_TRN_TP, AMT, BNK_CRD_NUM,  ")
                        Sql.Append("    EXP_DT, APP_CD, TRACK1,  ")
                        Sql.Append("    TRACK2, MANUAL, REF_NUM,  ")
                        Sql.Append("    ORIG_REF_NUM, ORIG_HOST_REF_NUM, MEDIUM_TP_CD,  ")
                        Sql.Append("    BATCH_NUM, STAT_CD, ERROR_DES,  ")
                        Sql.Append("    ORIGIN_CD, XMIT_DT_TIME, EMP_CD_OP)  ")
                        Sql.Append(" VALUES ('").Append(lbl_co_cd.Text).Append("', ")
                        Sql.Append("'").Append(cbo_store_cd.Value).Append("', ")
                        Sql.Append("'").Append(cbo_csh_drw.Value).Append("', ")
                        Sql.Append("'").Append(reader.Item("FIN_CO").ToString).Append("', ")
                        Sql.Append("'").Append(txt_cust_cd.Text).Append("', ")
                        Sql.Append("'").Append(Session("slsp1")).Append("', ")
                        ' Sql.Append("'").Append(Session("emp_cd")).Append("', ")
                        Sql.Append("'").Append(lbl_emp_cd.Text).Append("', ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("'").Append(txt_ivc_cd.Text).Append("', ")
                        Sql.Append("'").Append("RET").Append("', ")
                        Sql.Append(reader.Item("AMT").ToString).Append(", ")
                        Sql.Append("'").Append(Decrypt(reader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")).Append("', ")
                        Dim expDt As Date = Nothing
                        If (Not IsDBNull(reader.Item("EXP_DT")) AndAlso reader.Item("EXP_DT").ToString.isNotEmpty) Then
                            expDt = SystemUtils.GetFullExpirationDate(reader.Item("EXP_DT").ToString)
                            Sql.Append(expDt).Append(", ")
                        Else
                            Sql.Append("''").Append(", ") 'exp_dt
                        End If

                        Sql.Append("'").Append(reader.Item("APPROVAL_CD").ToString).Append("', ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("'").Append("Y").Append("', ")
                        Dim origRefNum As String = GetOrigRefNum(reader.Item("MOP_CD").ToString, Decrypt(reader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"))
                        Sql.Append("'").Append(refNum).Append("', ")
                        Sql.Append("'").Append(origRefNum).Append("', ")
                        Sql.Append("'").Append(origRefNum).Append("', ")
                        Dim mediumTypeCd = GetMediumTypeCode(reader.Item("FIN_CO").ToString)
                        Sql.Append("'").Append(mediumTypeCd).Append("', ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("''").Append(", ")
                        Sql.Append("'").Append("POS").Append("', ")
                        Sql.Append("TO_DATE('").Append(FormatDateTime(Now(), DateFormat.ShortDate)).Append("','mm/dd/RRRR'), ")
                        '  Sql.Append("'").Append(Session("emp_cd")).Append("' ").Append(" ) ")
                        Sql.Append("'").Append(lbl_emp_cd.Text).Append("' ").Append(" ) ")
                        Dim cmdErp = DisposablesManager.BuildOracleCommand(Sql.ToString, conn)
                        cmdErp.ExecuteNonQuery()
                    End If
                End If
                reader.Close()
            Catch
                Throw
            Finally
                conn.Close()
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Gets the reference number of the authorized payment associated with the passed-in payment and mop.
    ''' </summary>
    ''' <param name="mopCd">the mop code</param>
    ''' <param name="acctNum">the account number on the auth request</param>
    ''' <returns>the ref number</returns>
    Private Function GetOrigRefNum(ByVal mopCd As String, ByVal acctNum As String) As String

        Dim rtnVal As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader
        conn.Open()

        sql.Append(" SELECT ref_num ")
        sql.Append("   FROM ar_trn ")
        sql.Append("  WHERE     ivc_cd = :DEL_DOC_NUM ")
        sql.Append("        AND mop_cd = :MOP_CD ")
        sql.Append("        AND acct_num = :ACCT_NUM")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":DEL_DOC_NUM").Value = txt_ivc_cd.Text.Trim
        cmd.Parameters.Add(":MOP_CD", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":MOP_CD").Value = mopCd
        cmd.Parameters.Add(":ACCT_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":ACCT_NUM").Value = "ID:" + acctNum

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If reader.Read Then
                rtnVal = reader.Item(0).ToString()
            End If
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function

    ''' <summary>
    ''' The medium type code associated with the auth provider, in this case used only for Finance companies.
    ''' But it could be any authorizable provider. This is defined when setting up the auth provider in the system.
    ''' </summary>
    ''' <param name="FinanceCoCd">the finance company for which to get the medium code</param>
    ''' <returns></returns>
    Private Function GetMediumTypeCode(ByVal FinanceCoCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader
        conn.Open()

        sql.Append(" SELECT medium_tp_cd ")
        sql.Append("   FROM asp ")
        sql.Append("  WHERE as_cd = :AS_CD")
        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":AS_CD", OracleType.VarChar)
        cmd.Parameters(":AS_CD").Value = FinanceCoCd

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If reader.Read Then
                rtnVal = reader.Item(0).ToString()
            End If
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function

    ''' <summary>
    ''' Retrieves the actual promotion code provided by the service auth provider that has been
    ''' setup in the system. This should correspond to a user specified promo code that has been 
    ''' defined for the same ASP in the system.
    ''' </summary>
    ''' <param name="promoCd">the user specified promo code for which to get the ASP promo code</param>
    ''' <returns>the ASP promo code</returns>
    Public Function GetASPPromoCd(ByVal promoCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim reader As OracleDataReader
        conn.Open()

        Dim sql As String = "SELECT AS_PROMO_CD FROM SO_ASP WHERE AS_CD='" & promoCd & "'"
        cmd = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            Do While reader.Read
                If reader.Item("AS_PROMO_CD").ToString & "" = "" Then
                    rtnVal = ""
                Else
                    rtnVal = reader.Item("AS_PROMO_CD").ToString
                End If
            Loop
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function


    ''' <summary>
    ''' Raised when a DataGrid item is created, both during round-trips and when data is bound to the grid.
    ''' It used to control the content and appearance of a row in the grid- in this case it will disable the 
    ''' CHECK Payment row.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub MyDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemCreated
        'Public Sub MyDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyDataGrid.ItemCreated
        Dim dgItem As DataGridItem
        'iterate through all the rows to see if there is a CHECK payment on the original pmt
        ' If there is, disable it so it is not available for refund.
        For i As Integer = 0 To MyDataGrid.Items.Count - 1
            dgItem = MyDataGrid.Items(i)
            If PaymentUtils.Payment.TP_CHECK = dgItem.Cells(4).Text Then
                dgItem.Enabled = False
            Else
                dgItem.Enabled = True
            End If
        Next

    End Sub





    'Gets fired when an item in the Payment Details grid gets bound. It is used here to change the icon before redisplaying the grid.
    Protected Sub GridviewPmts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridviewPmts.ItemDataBound

        Dim Ping_Image As System.Web.UI.WebControls.Image

        Ping_Image = e.Item.FindControl("Image1")
        If Not Ping_Image Is Nothing Then
            If e.Item.Cells(4).Text & "" <> "" And e.Item.Cells(4).Text <> "&nbsp;" Then
                If Lookup_Merchant(e.Item.Cells(4).Text, cbo_store_cd.Value, e.Item.Cells(0).Text) & "" <> "" Then
                    Ping_Image.ImageUrl = "images/icons/tick.png"
                Else
                    Ping_Image.ImageUrl = "images/icons/error.png"
                End If
            Else
                Ping_Image.ImageUrl = "images/icons/error.png"
            End If
        End If

    End Sub


    'Resets the screen to it's initial state and if there is a cashier, that also gets initialized
    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click
        Dim str_yn As String  'lucy add if condition
        lucy_check_payment(str_yn)
        If str_yn = "NO" Then
            'lbl_Error.Text = "Authorized payment on file, commit to save the transaction"
            lbl_Error.Text = Resources.LibResources.Label837
            'Dec 17 do not let go back to SOM+
            btn_return.Enabled = False

            ' Reset cash breakdown values
            reset_cash_breakdown()
            Exit Sub
        Else
            thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)
            bindgrid()
            ResetInfo()
            cbo_payment_type.SelectedItem = cbo_payment_type.Items.FindByValue("PMT")
            DisplayMops()
            lbl_Error.Text = String.Empty
            txt_ivc_cd.Text = String.Empty

            ' Reset cash breakdown values
            reset_cash_breakdown()
        End If
    End Sub


    Protected Sub btn_show_invoices_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If (txt_ivc_cd.Text.isNotEmpty) Then
            Dim MyDataReader2 As OracleDataReader
            Dim sql As String = ""
            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand


            Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            connLocal.Open()

            Dim UpPanel As UpdatePanel = Master.FindControl("UpdatePanel1")
            Dim default_invoice As String = ""

            sql = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & cbo_store_cd.Value & "'"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, connLocal)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                Do While MyDataReader2.Read
                    If MyDataReader2.Item("invoice_file").ToString & "" = "" Then
                        default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                    Else
                        default_invoice = MyDataReader2.Item("invoice_file").ToString
                    End If
                Loop
                MyDataReader2.Close()
            Catch ex As Exception
                connLocal.Close()
                Throw
            End Try

            If String.IsNullOrEmpty(default_invoice) Then
                sql = "select store_cd, invoice_file, default_file from invoice_admin"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, connLocal)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDataReader2.Read Then
                        default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
                    End If
                    MyDataReader2.Close()
                Catch ex As Exception
                    connLocal.Close()
                    Throw
                End Try
            End If

            If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike6778", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & txt_ivc_cd.Text & "','frmTest67','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike6778", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & txt_ivc_cd.Text & "&INVOICE_NAME=" & default_invoice & "','frmTest67','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            End If

            bindgrid()

            Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim fin_invoice As String = String.Empty
            Dim promo_cd As String = String.Empty
            Dim wdr As Boolean = False
            Dim fin_co As String = String.Empty

            UpPanel = Master.FindControl("UpdatePanel1")
            connErp.Open()
            sql = "SELECT a.PROMO_CD, b.FIN_CUST_CD FROM SO_ASP a, SO b " &
                  "WHERE a.DEL_DOC_NUM='" & txt_ivc_cd.Text & " ' and a.DEL_DOC_NUM=b.DEL_DOC_NUM"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, connErp)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                If MyDataReader2.Read Then
                    promo_cd = MyDataReader2.Item("PROMO_CD").ToString
                    fin_co = MyDataReader2.Item("FIN_CUST_CD").ToString
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                Throw
                connErp.Close()
            End Try

            Dim mop_string As String = DP_String_Creation()
            If mop_string.isNotEmpty Then
                sql = "SELECT MOP_CD FROM AR_TRN WHERE IVC_CD='" & txt_ivc_cd.Text & "' AND MOP_CD IN(" & mop_string & ")"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, connErp)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDataReader2.Read Then
                        wdr = True
                    End If
                    MyDataReader2.Close()
                Catch ex As Exception
                    connErp.Close()
                    Throw
                End Try
            End If
            connErp.Close()

            sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & fin_co & "'"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, connLocal)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                If MyDataReader2.Read Then
                    If wdr = False Then
                        fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                    Else
                        fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                    End If
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                connLocal.Close()
                Throw
            End Try
            connLocal.Close()

            If (fin_invoice.isNotEmpty) Then
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike877", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & txt_ivc_cd.Text.ToString & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            End If

        End If
    End Sub

    ''' <summary>
    ''' This fires upon the 'Lookup Order' button within the 'Search' order popup
    ''' that gets displayed when user clicks on the 'Search' button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btn_lookup_so_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim query_string As String = ""
        If Label7.Text & "" <> "" Then
            query_string = query_string & "ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&"
        End If
        If cbo_store_cd.Value & "" <> "" Then
            query_string = query_string & "ip_store_cd=" & cbo_store_cd.Value & "&"
        End If
        If cbo_csh_drw.Value & "" <> "" Then
            query_string = query_string & "ip_csh_drw=" & cbo_csh_drw.Value & "&"
        End If
        If cbo_payment_type.Value & "" <> "" Then
            query_string = query_string & "pmt_tp_cd=" & cbo_payment_type.Value & "&"
        End If
        If txt_cust_cd.Text & "" <> "" Then
            query_string = query_string & "ip_cust_cd=" & txt_cust_cd.Text & "&"
        End If

        Dim strquery As String
        strquery = "del_doc_num=" & HttpUtility.UrlEncode(txt_del_doc_num.Text) & "&so_wr_dt=" & HttpUtility.UrlEncode(txt_wr_dt.Text) _
          & "&ord_tp_cd=" & HttpUtility.UrlEncode(txt_ord_tp_cd.Text) & "&cust_cd=" & HttpUtility.UrlEncode(TextBox4.Text) & "&stat_cd=" & HttpUtility.UrlEncode(txt_Stat_cd.Text) & "&f_name=" _
          & HttpUtility.UrlEncode(txt_f_name.Text) & "&l_name=" & HttpUtility.UrlEncode(txt_l_name.Text) & "&h_phone=" & HttpUtility.UrlEncode(txt_h_phone.Text) & "&addr1=" _
          & HttpUtility.UrlEncode(txt_addr1.Text) & "&bphone=" & HttpUtility.UrlEncode(txt_b_phone.Text) & "&addr2=" & HttpUtility.UrlEncode(txt_addr2.Text) & "&slsp1=" & HttpUtility.UrlEncode(txt_slsp1.Text) & "&store_cd=" & HttpUtility.UrlEncode(txt_store_cd.Text) _
          & "&corp_name=" & HttpUtility.UrlEncode(txt_corp_name.Text) & "&city=" & HttpUtility.UrlEncode(txt_city.Text) & "&state=" & HttpUtility.UrlEncode(txt_state.Text) & "&zip=" & HttpUtility.UrlEncode(txt_zip.Text) & "&slsp2=" & HttpUtility.UrlEncode(txt_slsp2.Text)
        Response.Redirect("Main_SO_Lookup.aspx?referrer=IndependentPaymentProcessing.aspx&" & strquery & "&" & query_string)

    End Sub

    Protected Sub btnOrderSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        txt_cust_cd.Text = ""
        thePaymentBiz.DeletePayments(Session.SessionID.ToString.Trim)

        Dim query_string As String = ""
        If Label7.Text & "" <> "" Then
            query_string = query_string & "ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&"
        End If
        PopupSalesOrderSearch.ShowOnPageLoad = True
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + btn_lookup_so.ClientID + "').focus();$get('" + btn_lookup_so.ClientID + "').select();", True)

    End Sub

    ' Daniela comment not needed
    'Protected Sub btn_pickup_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Dim UpPanel As UpdatePanel
    '    UpPanel = Master.FindControl("UpdatePanel1")

    '    System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike6778", "window.open('Invoices/LevinPickup.aspx?DEL_DOC_NUM=" & txt_ivc_cd.Text & "','frmTest23432','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    'End Sub

    Protected Sub btn_accept_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Response.Redirect("GEQuickCredit.aspx?store_cd=" & cbo_store_cd.Value & "&cust_cd=" & txt_cust_cd.Text)

    End Sub

    Protected Sub btn_decline_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        QSInitial("3")

    End Sub

    Protected Sub img_qs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_qs.Click

        Response.Redirect("GEQuickCredit.aspx?cust_cd=" & txt_cust_cd.Text)

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        PopupCustomerEMail.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If TextBox5.Text & "" = "" Then
            PopupCustomerEMail.ShowOnPageLoad = False
            Exit Sub
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim sql As String = "UPDATE CUST SET EMAIL_ADDR=:EMAIL_ADDR WHERE CUST_CD=:CUST_CD"
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        cmd.Parameters.Add(":EMAIL_ADDR", OracleType.VarChar)
        cmd.Parameters(":EMAIL_ADDR").Value = TextBox5.Text
        cmd.Parameters.Add(":CUST_CD", OracleType.VarChar)
        cmd.Parameters(":CUST_CD").Value = txt_cust_cd.Text

        cmd.ExecuteNonQuery()
        conn.Close()

        lbl_email.Text = TextBox5.Text
        PopupCustomerEMail.ShowOnPageLoad = False

    End Sub

    'Fires when user clicks on 'Return to SOM' button
    Protected Sub btn_return_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If Request("ip_ivc_cd") & "" <> "" Then
            Response.Redirect("salesordermaintenance.aspx?del_doc_num=" & Request("ip_ivc_cd") & "&query_returned=Y")
        Else
            Response.Redirect("salesordermaintenance.aspx?del_doc_num=" & txt_ivc_cd.Text & "&query_returned=Y")
        End If

    End Sub

    'Fires from the GCard popup
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'sabrina add GC
        Dim merchant_id As String = ""
        PopupGiftCardPmt.ShowOnPageLoad = True
        Dim str_display As String '= gc_send_subTp8()

        If str_display = "no designated pinpad " Then
            PopupGiftCardPmt.ShowOnPageLoad = False
            ASPxLabel15.Text = "no designated pinpad "
            Exit Sub
        End If

        If txt_gc.Text & "" = "" Then
            ASPxLabel15.Text = "Account # cannot be blank"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, BC, APPROVAL_CD)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :BC, :APPROVAL_CD)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":BC").Value = Session("gc_card")
            objSql.Parameters(":APPROVAL_CD").Value = Session("gc_approval_cd")

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupGiftCardPmt.ShowOnPageLoad = False
        Dim query_string As String = ""
        If txt_ivc_cd.Text & "" <> "" Then
            query_string = "ip_ivc_cd=" & txt_ivc_cd.Text & "&"
        End If
        If Label7.Text & "" <> "" Then
            query_string = query_string & "ip_csh_pass=" & Encrypt(Label7.Text, "CrOcOdIlE") & "&"
        End If
        If cbo_store_cd.Value & "" <> "" Then
            query_string = query_string & "ip_store_cd=" & cbo_store_cd.Value & "&"
        End If
        If cbo_csh_drw.Value & "" <> "" Then
            query_string = query_string & "ip_csh_drw=" & cbo_csh_drw.Value & "&"
        End If
        If cbo_payment_type.Value & "" <> "" Then
            query_string = query_string & "pmt_tp_cd=" & cbo_payment_type.Value & "&"
        End If
        If txt_cust_cd.Text & "" <> "" Then
            query_string = query_string & "ip_cust_cd=" & txt_cust_cd.Text & "&"
        End If

        'just update the grid and totals, instead of a total redirect to itself
        bindgrid()
        UpdateTotals()
        'Session("payment") = True

    End Sub


    ''' <summary>
    ''' Gets fired when the 'SELECT FOR REFUND' button gets clicked from within the 'Original Payments' datagrid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnSelectForRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectForRefund.Click

        Dim selectChkBox As CheckBox
        Dim isChecked As Boolean = False

        'iterate through all the rows to see if there is at least one that is checked
        For i As Integer = 0 To MyDataGrid.Items.Count - 1
            selectChkBox = CType(MyDataGrid.Items(i).FindControl("ChkBoxSelectPmt"), CheckBox)
            If (selectChkBox.Checked) Then
                isChecked = True
                Exit For
            Else
                isChecked = False
            End If
        Next

        'if none of the check boxes were selected, then display a message
        If (Not isChecked) Then
            lblRefundError.Text = "Please select a Payment to refund or close the window."
        Else
            ProcessCheckedRefundEntries()
        End If

    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    Private Function CalculateRefundableAmount() As Double

        Dim adjustedRefundableAmt As Double = 0

        'Determine if the refund being done is against a CRM/MCR or SAL type order
        Dim isCrmOrMcr As Boolean = False
        Dim dTable As DataTable = OrderUtils.GetOrderDetails(txt_ivc_cd.Text)
        Dim dRow As DataRow = Nothing
        If dTable.Rows.Count > 0 AndAlso (Not IsNothing(dTable.Rows(0))) Then
            dRow = dTable.Rows(0)
            isCrmOrMcr = ((dRow("ORD_TP_CD") = AppConstants.Order.TYPE_MCR) OrElse (dRow("ORD_TP_CD") = AppConstants.Order.TYPE_CRM))
        End If

        If isCrmOrMcr Then
            'since the refund against a CRM/MCR, the refundable amt is the balance on the order
            adjustedRefundableAmt = Math.Abs(GetBalance(txt_cust_cd.Text, txt_ivc_cd.Text))
        Else

            'for a sale/mdb that is VOID, only if there is a balance due(like in an overpayment case) can the orig MOP be refunded
            If (dRow("STAT_CD") = AppConstants.Order.STATUS_VOID) Then
                'For a VOID sale/mdb, if there's a balance due(like in an overpayment case), refund any original MOPs. 
                adjustedRefundableAmt = Math.Abs(GetBalance(txt_cust_cd.Text, txt_ivc_cd.Text))

            ElseIf (dRow("STAT_CD") = AppConstants.Order.STATUS_FINAL) Then
                ' for a final SAL, check if there are any open CRM/MCR/MDB docs against it because in that case no refund is allowed
                'If there is no linked doc, then it means there could be an overpayment situation and so check for that later
                Dim linkedDelDoc As String = PaymentUtils.GetLinkedDocForSale(txt_ivc_cd.Text, lbl_co_cd.Text, txt_cust_cd.Text)
                If (Not linkedDelDoc Is Nothing) Then ' Daniela Dec 12 added
                    'If (linkedDelDoc = "N") Then  ' Daniela comment as per Donna allow refunds Dec 12
                    adjustedRefundableAmt = Math.Abs(GetBalance(txt_cust_cd.Text, txt_ivc_cd.Text))
                End If
            Else

                'For OPEN sale orders, get the order total, min due, etc.
                If _dtRefundDetails Is Nothing Then
                    'Retrieve the table from the viewstate and  assign it back to the class-level vbl
                    _dtRefundDetails = CType(ViewState("dtRefundDetails"), DataTable)
                End If

                If (_dtRefundDetails.Rows.Count > 0) Then

                    Dim dr As DataRow = _dtRefundDetails.Rows(0)
                    Dim orderTotal As Double = CDbl(dr("ORDER_TOTAL").ToString)
                    Dim refundableAmt = CDbl(dr("REFUNDABLE_AMT").ToString)
                    Dim minDepositAmt As Double = GetMinimumDue(orderTotal)
                    SetMinimumDue(True, minDepositAmt)

                    'if user has security to go below min due requirement, then the whole refundable amount can be given out
                    Dim canOverrideMinDue As Boolean = SecurityUtils.hasSecurity(SecurityUtils.REFUND_MIN_DUE_OVERRIDE, lbl_emp_cd.Text)
                    If refundableAmt > 0 AndAlso canOverrideMinDue Then
                        adjustedRefundableAmt = refundableAmt
                    Else
                        'with no security the refundable amount has to be reduced by the min deposit. 
                        'CheckForManagerApproval()                    
                        adjustedRefundableAmt = refundableAmt - minDepositAmt
                    End If
                End If

            End If  'if open/final/void sale

        End If     'if crm/sal

        Return adjustedRefundableAmt

    End Function


    ''' <summary>
    ''' Processes the refunds that have check-marked for refunds processing.It validates that the total amount
    ''' does not exceed the available refundable amount. If validation passes, it saves the refund to the database.
    ''' </summary>
    Private Sub ProcessCheckedRefundEntries()

        Dim textBoxRefundAmt As TextBox
        Dim selectChkBox As CheckBox
        Dim dgItem As DataGridItem

        'first check if the total amount for all the refund pmts is within the allowed limit
        Dim availableRefundAmt As Double = Math.Abs(CalculateRefundableAmount())
        If (GetRefundPopupAmtTotal() > availableRefundAmt) Then
            lblRefundError.Text = GetRefundAmtErrorMsg(availableRefundAmt)
            'uncheck all the entries since the total exceeds the available amt.
            UncheckAllOrigRefundPayments()
        Else
            lblRefundError.Text = String.Empty
            'Iterate again to save the payment for the ones marked for selection
            For i As Integer = 0 To MyDataGrid.Items.Count - 1
                dgItem = MyDataGrid.Items(i)
                selectChkBox = CType(dgItem.FindControl("ChkBoxSelectPmt"), CheckBox)
                textBoxRefundAmt = CType(dgItem.FindControl("txt_refund_amt"), TextBox)
                If (selectChkBox.Checked AndAlso CDbl(textBoxRefundAmt.Text) > 0) Then
                    If dgItem.Cells(5).Text = "FI" Then
                        SaveOrigFinancePmt(dgItem, textBoxRefundAmt.Text, txt_ivc_cd.Text)
                    Else
                        SaveOrigPmt(dgItem, textBoxRefundAmt.Text)
                    End If
                End If
            Next
            PopupOrigRefundPmts.ShowOnPageLoad = False
            bindgrid()
            UpdateTotals()
            'disable the 'Go' button once a payment has been added or not to the payment grid
            btn_lookup_ivc.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Unchecks all the original payments being displayed for refunds
    ''' </summary>
    Private Sub UncheckAllOrigRefundPayments()

        Dim selectChkBox As CheckBox
        Dim isChecked As Boolean = False

        'iterate through all the rows to see if there is at least one that is checked
        For i As Integer = 0 To MyDataGrid.Items.Count - 1
            selectChkBox = CType(MyDataGrid.Items(i).FindControl("ChkBoxSelectPmt"), CheckBox)
            If (selectChkBox.Checked) Then
                selectChkBox.Checked = False
            End If
        Next
    End Sub

    ''' <summary>
    ''' Fires when the value in the refund amount field in the REFUND popup's 'original payment' table
    ''' is changed. It validates the amount and if fails validation, it displays an error msg and restores
    ''' the original value.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub txt_refund_amt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Iterate through each row of the datagrid and see if it's marked for selection
        Dim selectChkBox As CheckBox
        Dim dgItem As DataGridItem
        Dim thisTextBox As TextBox = CType(sender, TextBox)

        'find the datagrid row to which this amt textbox belongs and get it's corresponding checkbox
        dgItem = CType(thisTextBox.Parent.Parent, DataGridItem)
        selectChkBox = CType(dgItem.FindControl("ChkBoxSelectPmt"), CheckBox)

        If (selectChkBox.Checked) Then
            'since the amount changed, uncheck the checkbox
            selectChkBox.Checked = False
        End If

        'first get the original amount for the payment record being modified and then validate the amount
        Dim id As Integer = CType(MyDataGrid.DataKeys(dgItem.ItemIndex), Integer)

        Dim originalAmt As String = "0"
        If _hTableOrigPmtAmtsForRefunds Is Nothing Then
            'Retrieve the list of orig pmt objects from the viewstate after a postback and put it back into the vbl
            _hTableOrigPmtAmtsForRefunds = CType(ViewState("hTableOrigPmtAmtsForRefunds"), Hashtable)
        End If

        If _hTableOrigPmtAmtsForRefunds.ContainsKey(id) Then originalAmt = _hTableOrigPmtAmtsForRefunds(id)

        'get the min deposit originally paid -  that should be retained for refunding
        'validate the amount
        Dim val As String = String.Empty

        If ValidateAmount(lblRefundError, thisTextBox.Text) Then
            lblRefundError.Text = String.Empty

            Dim availableRefundAmt As Double = Math.Abs(CalculateRefundableAmount())
            If Not (ValidateRefundAmount(lblRefundError, originalAmt, availableRefundAmt, thisTextBox.Text)) Then
                'thisTextBox.Focus()
                'thisTextBox.Text = String.Format("{0:C}", availableRefundAmt)
                val = availableRefundAmt
            End If
        Else
            'thisTextBox.Focus()
            'thisTextBox.Text = String.Format("{0:C}", thisTextBox.Text)
            val = thisTextBox.Text
        End If

        If String.IsNullOrEmpty(val) Then val = thisTextBox.Text
        thisTextBox.Focus()
        thisTextBox.Text = String.Format("{0:C}", val)

    End Sub


    ''' <summary>
    ''' Fires when the value in PAYMENT AMOUNT field(on the main screen) changes. It will validate the amount for format 
    ''' as well as the actual allowed value.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub txt_pmt_amt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_pmt_amt.TextChanged

        ' Update cash breakdown amount as per user input
        If Not String.IsNullOrEmpty(txt_pmt_amt.Text) Then
            updated_cash_breakdown(sender, e)
        End If

        If cbo_payment_type.Value <> "R" And txt_ivc_cd.Text.isNotEmpty Then
            'Daniela dec 18
            Session("lucy_amt") = txt_pmt_amt.Text
            Exit Sub
        End If
        If (txt_ivc_cd.Text.isNotEmpty) Then
            'validate the amount regardless of it being a payment or refund case
            Dim origAmt As Double = 0
            If ValidateAmount(lbl_Error, txt_pmt_amt.Text) Then

                lbl_Error.Text = String.Empty
                If (cbo_payment_type.Value = "R") Then
                    ''***NOTE: This should only happen in the case of a REFUND not payment. check if the original payments in the
                    ' mop dd exist, which would be the case when there is a valid order and refunds are being issued.

                    If _listOrigPmtsForRefundsDd Is Nothing Then
                        'Retrieve the list of orig pmt objects from the viewstate after a postback and put it back into the vbl
                        _listOrigPmtsForRefundsDd = CType(ViewState("listOrigPmtsForRefundsDd"), List(Of OriginalPaymentDtc))
                    End If

                    Dim origPmtObj As OriginalPaymentDtc = Nothing
                    If Not IsNothing(_listOrigPmtsForRefundsDd) AndAlso _listOrigPmtsForRefundsDd.Count > 0 Then
                        origPmtObj = _listOrigPmtsForRefundsDd(mop_drp.SelectedIndex - 1)
                        If Not origPmtObj Is Nothing Then
                            origAmt = CDbl(origPmtObj.amt)
                        End If
                    End If

                    Dim availableRefundAmt As Double = Math.Abs(CalculateRefundableAmount())
                    If Not (ValidateRefundAmount(lbl_Error, origAmt, availableRefundAmt, txt_pmt_amt.Text)) Then
                        txt_pmt_amt.Focus()
                        txt_pmt_amt.Text = String.Format("{0:C}", Math.Abs(availableRefundAmt))
                    End If
                End If
            Else
                txt_pmt_amt.Focus()
                txt_pmt_amt.Text = String.Format("{0:C}", origAmt)
            End If
        End If
        Dim str_amt As String = txt_pmt_amt.Text  'lucy add
        Session("lucy_amt") = str_amt
    End Sub


    ''' <summary>
    ''' Checks that the new amount entered is valid and displays an error message on the passed-in label, 
    ''' if validation fails.
    ''' </summary>
    ''' <param name="errorLabel">label to display the error on</param>
    ''' <param name="newAmt">amount being validated</param>
    ''' <returns>a flag indicating if the  amount was valid or not.</returns>
    Private Function ValidateAmount(ByRef errorLabel As DevExpress.Web.ASPxEditors.ASPxLabel,
                                     ByVal newAmt As String) As Boolean
        Dim rtnVal As Boolean = True

        If Not IsNumeric(newAmt) Then
            errorLabel.Text = "Amount must be numeric."
            rtnVal = False
        Else
            Dim enteredAmt As Double = CDbl(newAmt)
            If enteredAmt <= 0 Then
                'errorLabel.Text = String.Format("Amount must be greater than {0:C}", 0)
                errorLabel.Text = String.Format(Resources.LibResources.Label825 & " " & "{0:C}", 0)
                rtnVal = False
            End If
        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Checks that the refund amount entered is valid and if validation fails, it displays 
    ''' an error on the passed-in label.
    ''' </summary>
    ''' <param name="errorLabel">lable to set error on</param>
    ''' <param name="originalAmt">original amount before any changes </param>
    ''' <param name="newAmt">new amount being validated</param>
    ''' <param name="availableRefundAmt">the maximum refund amount available </param>
    ''' <returns>a flag indicating if the refund amount was valid or not.</returns>
    Private Function ValidateRefundAmount(ByRef errorLabel As DevExpress.Web.ASPxEditors.ASPxLabel,
                                        ByVal originalAmt As Double,
                                        ByRef availableRefundAmt As Double,
                                        ByVal newAmt As String) As Boolean

        Dim rtnVal As Boolean = True
        errorLabel.Text = String.Empty
        Dim enteredAmt As Double = CDbl(newAmt)

        If (originalAmt > 0) Then
            '**** NOTE: means when a REFUND against a valid orig order is being done

            'AMOUNT validation rules for a refund against valid original order are:
            ' a. if amount more than available refund amt, display error
            ' b. if amount is NOT more than available amt, check for other in-process payments that have been made and 
            '      display error if the amount exceeds the net of available and in-process pmt amts

            ' get the balance available against the order# entered- that should be the mex. refundable amt available
            ' for any MOP. For instance, the orig sale could have pmts worth $200 but the CRM could have been for $75 only.
            ' So, even though the orig MOPs show for $200, the user should only be allowed a max. of $75 for that CRM doc.                    

            availableRefundAmt = IIf(originalAmt > availableRefundAmt, availableRefundAmt, originalAmt)
            availableRefundAmt = Math.Abs(availableRefundAmt - GetInProcessPaymentAmt(Session.SessionID.ToString))
            If (enteredAmt > availableRefundAmt) Then
                errorLabel.Text = GetRefundAmtErrorMsg(availableRefundAmt)
                rtnVal = False
            Else
                'the getinprocesspayment should be the total of all pmts, NOT by MOP
                Dim netAvailableAmt As Double = availableRefundAmt
                If (enteredAmt > (netAvailableAmt)) Then
                    availableRefundAmt = netAvailableAmt
                    errorLabel.Text = GetRefundAmtErrorMsg(netAvailableAmt)
                    rtnVal = False
                End If
            End If
        Else
            'means that a REFUND with ALL MOPs displayed is being done
            If (enteredAmt > availableRefundAmt) Then
                errorLabel.Text = GetRefundAmtErrorMsg(availableRefundAmt)
                rtnVal = False
            End If

        End If

        Return rtnVal
    End Function


    ''' <summary>
    ''' Calculates and returns the minimum due amount based on the current sale total and 
    ''' the configured min due percent system parameter.
    ''' </summary>
    ''' <param name="orderTotal">the total of the order on which to base the min due amount</param>
    ''' <returns>the min due amount</returns>
    Private Function GetMinimumDue(ByVal orderTotal As Double) As Double

        Dim minDue As Double = 0
        Dim minDepositPct As Double = 0

        ' get the current min due parameter
        Dim configuredMinDue = ConfigurationManager.AppSettings("min_deposit").ToString
        If configuredMinDue.isNotEmpty AndAlso CDbl(configuredMinDue) > 0 Then
            minDepositPct = (configuredMinDue / 100)
        End If

        'calculate min deposit amt based on current order total
        If (minDepositPct > 0 AndAlso orderTotal > 0) Then
            minDue = orderTotal * minDepositPct
        End If

        Return minDue
    End Function


    ''' <summary>
    ''' Sets and shows/hides the min due labels and amount in the refund original payment window.
    ''' </summary>
    ''' <param name="isVisible">indicates if the min due components should be displayed or not</param>
    ''' <param name="minDue">the min due amount</param>
    Private Sub SetMinimumDue(ByVal isVisible As Boolean, Optional ByVal minDue As Double = 0)

        If minDue > 0 Then

            lblMinDueAmt.Text = String.Format("{0:C}", minDue)
        End If

        lblMinDue.Visible = isVisible
        lblMinDueAmt.Visible = isVisible

    End Sub

    ''' <summary>
    ''' Fires when the selection in the MOP drop-down changes.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub mop_drp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mop_drp.SelectedIndexChanged
        ' Display cash breakdown method
        updated_cash_breakdown(sender, e)

        'only in the case of a refund against a valid original order do we want to default the amount
        If (txt_ivc_cd.Text.isNotEmpty) AndAlso (cbo_payment_type.Value = "R") Then

            Dim index As Integer = mop_drp.SelectedIndex
            If mop_drp.SelectedIndex > 0 Then

                If _listOrigPmtsForRefundsDd Is Nothing Then
                    'Retrieve the list of orig pmt objects from the viewstate after a postback and put it back into the vbl
                    _listOrigPmtsForRefundsDd = CType(ViewState("listOrigPmtsForRefundsDd"), List(Of OriginalPaymentDtc))
                End If

                If Not IsNothing(_listOrigPmtsForRefundsDd) AndAlso _listOrigPmtsForRefundsDd.Count > 0 Then
                    Dim origPmtObj As OriginalPaymentDtc = _listOrigPmtsForRefundsDd(mop_drp.SelectedIndex - 1)
                    If Not origPmtObj Is Nothing Then
                        lbl_Error.Text = String.Empty

                        'get the mex. refundable amt available for any MOP
                        Dim availableRefundAmt As Double = Math.Abs(CalculateRefundableAmount())
                        availableRefundAmt = IIf(origPmtObj.amt > availableRefundAmt, availableRefundAmt, origPmtObj.amt)
                        txt_pmt_amt.Text = String.Format("{0:C}", availableRefundAmt - GetInProcessPaymentAmt(Session.SessionID.ToString))

                        If PaymentUtils.Payment.TP_CHECK = origPmtObj.mopCd Then
                            txt_pmt_amt.Enabled = False
                            btn_add.Enabled = False
                        Else
                            txt_pmt_amt.Enabled = True
                            btn_add.Enabled = True
                        End If
                    End If
                Else
                    txt_pmt_amt.Text = String.Format("{0:C}", 0)
                End If
            End If
        End If

    End Sub


    ''' <summary>
    ''' Adds up all the amount in the refund popup for all the records that been check-marked
    ''' for refunds processing.
    ''' </summary>
    ''' <returns>Total amount of all checked refunds.</returns>
    Private Function GetRefundPopupAmtTotal() As Double
        Dim total As Double = 0
        Dim textBoxRefundAmt As TextBox
        Dim selectChkBox As CheckBox
        Dim dgItem As DataGridItem
        Dim isChecked As Boolean = False

        'go through all the MOPs that have been selected for refunds, and add their amts.
        For i As Integer = 0 To MyDataGrid.Items.Count - 1
            dgItem = MyDataGrid.Items(i)
            selectChkBox = CType(dgItem.FindControl("ChkBoxSelectPmt"), CheckBox)
            textBoxRefundAmt = CType(dgItem.FindControl("txt_refund_amt"), TextBox)
            If (selectChkBox.Checked) Then
                total = total + CDbl(textBoxRefundAmt.Text)
            End If
        Next

        Return total

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="maxAmt"></param>
    ''' <returns></returns>
    Private Function GetRefundAmtErrorMsg(ByVal maxAmt As Double) As String
        'Dim rtnVal As String = String.Format("The amount entered cannot exceed allowed amount of {0:C}", Math.Abs(maxAmt))
        Dim rtnVal As String = String.Format(Resources.LibResources.Label829 & " {0:C}", Math.Abs(maxAmt))

        If maxAmt = 0 Then
            rtnVal = String.Format("No money is available to be refunded.")
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' Saves the original payment(non finance) that has been selected for refunds. It will gather all the pertinent
    ''' details needed for issuing an auth of the finance payment(upon commit) from the orginal record or other
    ''' tables db, and save it to the db.
    ''' </summary>
    ''' <param name="dgItem">the refunds original payments datagrid item</param>
    ''' <param name="amt">the amount of the payment</param>
    Private Sub SaveOrigPmt(ByVal dgItem As DataGridItem, ByVal amt As String)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction
        Dim localConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        'save the original pmt info in the payments table to be used to do a refund auth when the payment is committed.
        'now save into payment all the  datagrid info and the addtional finance details
        '0 = MOP Description
        '1 = Card Num - acct# unmasked for FI, and for CC & FI 'DP' last 4 masked e.g. XXXXXXXX1346
        '2 = Expiration dt - MM/dd/yyy
        '3 = Acct Num - hidden field -is the token returned by PB-used for CC e.g ID:BVJVgiCDCAlVA2vD1k, & FI 'DP', null for regular FI
        '4 = MOP Cd -  hidden field 
        '5 = MOP Type - 'FI', 'BC', etc.
        Try

            localConn.Open()
            thistransaction = localConn.BeginTransaction
            Dim accountNum As String = IIf(Server.HtmlDecode(dgItem.Cells(3).Text).isNotEmpty, Encrypt(dgItem.Cells(3).Text.Trim, "CrOcOdIlE"), "")
            amt = Replace(amt, "$", "")
            With cmd
                .Transaction = thistransaction
                .Connection = localConn
                .CommandText = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP) values " &
                    "('" & Session.SessionID.ToString.Trim & "', '" & Server.HtmlDecode(dgItem.Cells(4).Text) & "', " & amt &
                    ", '" & dgItem.Cells(0).Text & "', '" & accountNum & "','" &
                    Server.HtmlDecode(dgItem.Cells(2).Text) & "','" & Server.HtmlDecode(dgItem.Cells(5).Text) & "')"
            End With
            cmd.ExecuteNonQuery()
            thistransaction.Commit()
            Session("lucy_from_logout") = "NO" 'lucy
        Catch ex As Exception
            Throw
        Finally
            localConn.Close()
        End Try

    End Sub


    ''' <summary>
    ''' Saves the original finance payment that has been selected for refunds. It will gather all the pertinent
    ''' details needed for issuing an auth of the finance payment(upon commit) from the orginal record or other
    ''' tables db, and save it to the db.
    ''' </summary>
    ''' <param name="dgItem">the refunds original payments datagrid item</param>
    ''' <param name="amt">the amount of the payment</param>
    ''' <param name="delDocNum">the document against which this payment is being issued</param>
    Private Sub SaveOrigFinancePmt(ByRef dgItem As DataGridItem, ByVal amt As String, ByVal delDocNum As String)

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction
        Dim reader As OracleDataReader
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim promoCd As String = String.Empty
        Dim financeCo As String = String.Empty

        Dim sql As String = "SELECT promo_cd, fin_cust_cd " &
                            "FROM so, so_asp " &
                            "WHERE so.del_doc_num = so_asp.del_doc_num " &
                            "AND so.del_doc_num =:del_doc_num"
        cmd = DisposablesManager.BuildOracleCommand(sql, connErp)
        cmd.Parameters.Add(":del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":del_doc_num").Value = delDocNum

        'in order to save the original finance pmt info, we need to get some additional info to save in the payments table
        'This info will later be used to do a refund auth when the payment is committed.
        Try
            connErp.Open()
            amt = Replace(amt, "$", "")
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If reader.Read() Then
                If reader.Item("PROMO_CD") & "" <> "" Then
                    promoCd = reader.Item("PROMO_CD")
                End If
                If reader.Item("FIN_CUST_CD") & "" <> "" Then
                    financeCo = reader.Item("FIN_CUST_CD")
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            connErp.Close()
        End Try

        'now save into payment all the  datagrid info and the addtional finance details
        '0 = MOP Description
        '1 = Card Num - acct# unmasked for FI, and for CC & FI 'DP' last 4 masked e.g. XXXXXXXX1346
        '2 = Expiration dt - MM/dd/yyy
        '3 = Acct Num - hidden field -is the token returned by PB-used for CC e.g BVJVgiCDCAlVA2vD1k, & FI 'DP', NULL for regular FI
        '4 = MOP Cd -  hidden field 
        '5 = MOP Type - 'FI', 'BC', etc.

        '** The acct# for a FI 'deposit' type will be the hidden field value but for a regular FI,
        '** it is the acct/card# col displayed in the refund popup.
        Dim isFinanceDP = Is_Finance_DP(dgItem.Cells(4).Text.Trim)
        Dim acctNum As String = IIf(isFinanceDP, Server.HtmlDecode(dgItem.Cells(3).Text), Server.HtmlDecode(dgItem.Cells(1).Text))
        Try
            connLocal.Open()
            thistransaction = connLocal.BeginTransaction
            Dim cmdInsert As OracleCommand = DisposablesManager.BuildOracleCommand

            With cmdInsert
                .Transaction = thistransaction
                .Connection = connLocal
                .CommandText = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP, FI_ACCT_NUM, FIN_PROMO_CD, FIN_CO) values " &
                    "('" & Session.SessionID.ToString.Trim & "', '" & dgItem.Cells(4).Text & "', " & amt &
                    ", '" & dgItem.Cells(0).Text & "', '" & "" & "', '" & Server.HtmlDecode(dgItem.Cells(2).Text) &
                    "', '" & dgItem.Cells(5).Text & "', '" & Encrypt(acctNum, "CrOcOdIlE") &
                    "', '" & promoCd & "', '" & financeCo & "')"
            End With

            cmdInsert.ExecuteNonQuery()
            thistransaction.Commit()
        Catch ex As Exception
            Throw
        Finally
            connLocal.Close()
        End Try
    End Sub


    ''' <summary>
    ''' Saves the original finance payment that has been selected for refunds. It will gather all the pertinent
    ''' details needed for issuing an auth of the finance payment(upon commit) from the orginal record or other
    ''' tables db, and save it to the db.
    ''' </summary>
    ''' <param name="dgItem">the refunds original payments datagrid item</param>
    ''' <param name="amt">the amount of the payment</param>
    ''' <param name="delDocNum">the document against which this payment is being issued</param>
    Private Sub SaveStoreForward(ByRef dgItem As DataGridItem, ByVal amt As String, ByVal delDocNum As String)

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction
        Dim reader As OracleDataReader
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim promoCd As String = String.Empty
        Dim financeCo As String = String.Empty

        Dim sql As String = "SELECT promo_cd, fin_cust_cd " &
                            "FROM so, so_asp " &
                            "WHERE so.del_doc_num = so_asp.del_doc_num " &
                            "AND so.del_doc_num =:del_doc_num"
        cmd = DisposablesManager.BuildOracleCommand(sql, connErp)
        cmd.Parameters.Add(":del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":del_doc_num").Value = delDocNum

        'in order to save the original finance pmt info, we need to get some additional info to save in the payments table
        'This info will later be used to do a refund auth when the payment is committed.
        Try
            connErp.Open()
            amt = Replace(amt, "$", "")
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If reader.Read() Then
                If reader.Item("PROMO_CD") & "" <> "" Then
                    promoCd = reader.Item("PROMO_CD")
                End If
                If reader.Item("FIN_CUST_CD") & "" <> "" Then
                    financeCo = reader.Item("FIN_CUST_CD")
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            connErp.Close()
        End Try

        'now save into payment all the  datagrid info and the addtional finance details
        '0 = MOP Description
        '1 = Card Num - acct# unmasked for FI, and for CC last 4 masked e.g. XXXXXXXX1346
        '2 = Expiration dt - MM/dd/yyy
        '3 = Acct Num - hidden field -is the token returned by PB-for CC e.g ID:BVJVgiCDCAlVA2vD1k, null for FI
        '4 = MOP Cd -  hidden field 
        '5 = MOP Type - 
        Try
            connLocal.Open()
            thistransaction = connLocal.BeginTransaction
            Dim cmdInsert As OracleCommand = DisposablesManager.BuildOracleCommand

            With cmdInsert
                .Transaction = thistransaction
                .Connection = connLocal
                .CommandText = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP, FI_ACCT_NUM, FIN_PROMO_CD, FIN_CO) values " &
                    "('" & Session.SessionID.ToString.Trim & "', '" & dgItem.Cells(4).Text & "', " & amt &
                    ", '" & dgItem.Cells(0).Text & "', '" & Server.HtmlDecode(dgItem.Cells(3).Text) & "', '" & Server.HtmlDecode(dgItem.Cells(2).Text) &
                    "', '" & dgItem.Cells(5).Text & "', '" & Encrypt(dgItem.Cells(1).Text.Trim, "CrOcOdIlE") &
                    "', '" & promoCd & "', '" & financeCo & "')"
            End With

            cmdInsert.ExecuteNonQuery()
            thistransaction.Commit()
        Catch ex As Exception
            Throw
        Finally
            connLocal.Close()
        End Try
    End Sub

    Protected Sub txt_gc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim merchant_id As String = ""
        'sabrina comment out
        'merchant_id = Lookup_Merchant("GC", cbo_store_cd.Value, "GC")

        'If String.IsNullOrEmpty(merchant_id) Then
        '    ASPxLabel15.Text = "No Gift Card Merchant ID found for your home store code"
        '    Exit Sub
        'End If

        'Dim xml_response As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id, "balance", txt_pmt_amt.Text)
        'ASPxLabel15.Text = "Response Received: " & Now.Date & vbCrLf
        'If IsNumeric(xml_response.InnerText) Then
        '    ASPxLabel15.Text = "Card Balance: $" & FormatNumber(CDbl(xml_response.InnerText), 2) & vbCrLf
        '    If FormatNumber(CDbl(xml_response.InnerText), 2) < CDbl(lbl_pmt_amt.Text) Then
        '        lbl_pmt_amt.Text = FormatNumber(CDbl(xml_response.InnerText), 2)
        '        ASPxLabel15.Text = "Payment Amount was changed!  The balance on this gift card was less than the amount requested for payment."
        '    End If
        'Else
        '    ASPxLabel15.Text = xml_response.InnerText
        'End If

    End Sub

    Protected Sub txt_fi_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        lbl_GE_tran_id.Text = ""
        If IsSwiped(txt_fi_acct.Text) Then
            'first encrypt and set the swiped track info  without any parsing
            lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")

            'get the parsed acct# and exp date from the swiped track info 
            Dim swipedData = txt_fi_acct.Text
            txt_fi_acct.Text = GetAccountNumber(swipedData)
            txt_fi_exp_dt.Text = GetExpirationDate(swipedData)
        Else
            'encrypt entered acct number and set it 
            If IsNumeric(txt_fi_acct.Text) Then
                lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
            End If
        End If

        ' verify the card/acct number 
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)
        Dim txt_fi_dt As TextBox = Panel2.FindControl("txt_fi_exp_dt")
        Dim txt_fi_ac As TextBox = Panel2.FindControl("txt_fi_acct")
        If myTypeValid.CCValid = True Then
            If Not txt_fi_dt Is Nothing Then
                txt_fi_dt.Focus()
            Else
                lbl_fi_warning.Text = String.Empty
                txt_fi_ac.Focus()
            End If
        Else
            txt_fi_dt.Focus()
        End If

    End Sub
    Protected Sub txt_fi_exp_dt_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim regex1 As Regex = New Regex("^[0-9]")
            Dim match1 As Match = regex1.Match(txt_fi_exp_dt.Text)
            If match1.Success AndAlso txt_fi_exp_dt.Text <> "" AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) >= 1 AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) <= 12 Then
                lbl_fi_warning.Visible = False
                Dim txt_fi_appr As TextBox = Panel2.FindControl("txt_fi_appr")
                If Not txt_fi_appr Is Nothing Then
                    txt_fi_appr.Focus()
                End If

            Else

                lbl_fi_warning.Visible = True
                lbl_fi_warning.Text = ""
                lbl_fi_warning.Text = " ** Invalid Month and Year ** "
                Dim txt_fi_exp_dt As TextBox = Panel2.FindControl("txt_fi_exp_dt")
                If Not txt_fi_exp_dt Is Nothing Then
                    txt_fi_exp_dt.Focus()
                End If
                'txt_fi_exp_dt.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CheckCustomerQuickCredit()

        Dim terminalId As String = ""
        Dim processorId As String = ""
        Dim acct_no As String = ""
        'merchant_id = Lookup_Merchant("ECA", cbo_store_cd.Value, "ECA")

        'first get the provider/ASP code from GP parms that has been setup for GE Quick Credit
        Dim providerCd = GetQuickCreditProviderCode()

        'retrieve the auth provider specific details from the db
        Dim ds As DataSet = GetAuthDetails(providerCd, cbo_store_cd.Value, GetCashDrawerCd(cbo_store_cd.Value))
        For Each dr In ds.Tables(0).Rows
            terminalId = dr("terminal_id")
            processorId = dr("processor_id")
        Next

        If (Not String.IsNullOrEmpty(terminalId)) Then

            Dim objSql As OracleCommand
            ds = New DataSet
            Dim MyDataReader As OracleDataReader
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()
            lbl_qs_avail.Text = "N"
            'Use the provider code setup in GP Parms to get the acct info from cust_asp table instead of hardcoding it here
            Dim sql As String = "SELECT ACCT_CD FROM CUST_ASP" &
                                " WHERE CUST_CD='" & txt_cust_cd.Text &
                                "' AND AS_CD = '" & providerCd & "'"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read() Then
                    conn.Close()
                    Exit Sub
                Else
                    lbl_qs_avail.Text = "Y"
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            Dim QS_Status As String = Retrieve_QS_Status(txt_cust_cd.Text)

            If QS_Status = "A" Then     'means it as already approved
                lbl_qs_avail.Text = "N"
            ElseIf QS_Status = "D" Then  'means it was declined
                lbl_qs_avail.Text = "N"
            ElseIf QS_Status = "X" Then
                lbl_qs_avail.Text = "N"
            ElseIf QS_Status & "" = "" Then
                lbl_qs_avail.Text = "Y"
            Else
                lbl_auth_no.Text = Retrieve_Auth_No(txt_cust_cd.Text)
                lbl_qs_avail.Text = "Y"
                lbl_qs_desc.Text = "Quick Screen is available"
                img_qs.Visible = True
            End If

            If lbl_qs_avail.Text = "Y" And QS_Status & "" = "" Then
                QSInitial("1")
            End If
        End If

    End Sub

    Public Sub QSInitial(ByVal app_tp As String)

        Dim terminalId As String = ""
        Dim processorId As String = ""
        Dim acct_no As String = ""
        'merchant_id = Lookup_Merchant("ECA", cbo_store_cd.Value, "ECA")

        'first get the provider/ASP code from GP parms that has been setup for GE Quick Credit
        Dim providerCd = GetQuickCreditProviderCode()

        'retrieve the auth provider specific details from the db
        Dim ds As DataSet = GetAuthDetails(providerCd, cbo_store_cd.Value, GetCashDrawerCd(cbo_store_cd.Value))
        For Each dr In ds.Tables(0).Rows
            terminalId = dr("terminal_id")
            processorId = dr("processor_id")
        Next

        If (Not String.IsNullOrEmpty(terminalId)) Then
            Dim conn3 As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql3 As OracleCommand
            Dim MyDatareader3 As OracleDataReader
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim home_phone As String = ""
            Dim alt_phone As String = ""

            conn3 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn3.Open()

            sql = "SELECT * FROM CUST WHERE CUST_CD='" & txt_cust_cd.Text & "'"
            objSql3 = DisposablesManager.BuildOracleCommand(sql, conn3)
            Try
                MyDatareader3 = DisposablesManager.BuildOracleDataReader(objSql3)

                If MyDatareader3.Read Then
                    fname = MyDatareader3.Item("FNAME").ToString
                    lname = MyDatareader3.Item("LNAME").ToString
                    addr1 = MyDatareader3.Item("ADDR1").ToString
                    addr2 = MyDatareader3.Item("ADDR2").ToString
                    city = MyDatareader3.Item("CITY").ToString
                    state = MyDatareader3.Item("ST_CD").ToString
                    zip = MyDatareader3.Item("ZIP_CD").ToString
                    home_phone = Replace(MyDatareader3.Item("HOME_PHONE").ToString, "-", "")
                    alt_phone = Replace(MyDatareader3.Item("BUS_PHONE").ToString, "-", "")
                End If
            Catch ex As Exception
                Exit Sub
            End Try

            home_phone = Replace(home_phone, " ", "")
            alt_phone = Replace(alt_phone, " ", "")

            If Not IsNumeric(alt_phone) Then alt_phone = ""
            If Not IsNumeric(home_phone) Then home_phone = ""
            If Not IsNumeric(zip) Then
                conn3.Close()
                Exit Sub
            End If

            If Len(alt_phone) > 10 Then alt_phone = ""
            If Len(home_phone) > 10 Then home_phone = ""
            If Len(zip) > 5 Then zip = Left(zip, 5)

            Dim objSql As OracleCommand
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            Try
                sql = "INSERT INTO EC_APPLICATION (PRIME_FNAME, PRIME_MIDDLE_INIT, PRIME_LNAME, PRIME_DOB, PRIME_SSN, PRIME_CELL, PRIME_ADDR1, PRIME_ADDR2, PRIME_CITY, "
                sql = sql & "PRIME_STATE, PRIME_ZIP_CD, PRIME_HOME_PHONE, PRIME_TIME_AT_RES, PRIME_HOME_STATUS, PRIME_EMP_TIME, PRIME_EMP_PHONE, "
                sql = sql & "PRIME_MON_INCOME, PRIME_YR_INCOME, RELATIVE_PHONE, CREDIT_PROTECTION, PRIME_DL, PRIME_DL_ST, LANGUAGE_INDICATOR, REQUESTED_AMOUNT, AUTHORIZATION)"
                sql = sql & " VALUES (:PRIME_FNAME, :PRIME_MIDDLE_INIT, :PRIME_LNAME, :PRIME_DOB, :PRIME_SSN, :PRIME_CELL, :PRIME_ADDR1, :PRIME_ADDR2, :PRIME_CITY, "
                sql = sql & ":PRIME_STATE, :PRIME_ZIP_CD, :PRIME_HOME_PHONE, :PRIME_TIME_AT_RES, :PRIME_HOME_STATUS, :PRIME_EMP_TIME, :PRIME_EMP_PHONE, "
                sql = sql & ":PRIME_MON_INCOME, :PRIME_YR_INCOME, :RELATIVE_PHONE, :CREDIT_PROTECTION, :PRIME_DL, :PRIME_DL_ST, :LANGUAGE_INDICATOR, :REQUESTED_AMOUNT, :AUTHORIZATION)"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":PRIME_FNAME", OracleType.VarChar)
                objSql.Parameters(":PRIME_FNAME").Value = fname
                objSql.Parameters.Add(":PRIME_MIDDLE_INIT", OracleType.VarChar)
                objSql.Parameters(":PRIME_MIDDLE_INIT").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_LNAME", OracleType.VarChar)
                objSql.Parameters(":PRIME_LNAME").Value = lname
                objSql.Parameters.Add(":PRIME_DOB", OracleType.DateTime)
                objSql.Parameters(":PRIME_DOB").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_SSN", OracleType.VarChar)
                objSql.Parameters(":PRIME_SSN").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_CELL", OracleType.VarChar)
                If alt_phone & "" = "" Then
                    objSql.Parameters(":PRIME_CELL").Value = DBNull.Value
                Else
                    objSql.Parameters(":PRIME_CELL").Value = alt_phone
                End If
                objSql.Parameters.Add(":PRIME_ADDR1", OracleType.VarChar)
                objSql.Parameters(":PRIME_ADDR1").Value = addr1
                objSql.Parameters.Add(":PRIME_ADDR2", OracleType.VarChar)
                If addr2 & "" = "" Then
                    objSql.Parameters(":PRIME_ADDR2").Value = DBNull.Value
                Else
                    objSql.Parameters(":PRIME_ADDR2").Value = addr2
                End If
                objSql.Parameters.Add(":PRIME_CITY", OracleType.VarChar)
                objSql.Parameters(":PRIME_CITY").Value = city

                objSql.Parameters.Add(":PRIME_STATE", OracleType.VarChar)
                objSql.Parameters(":PRIME_STATE").Value = state
                objSql.Parameters.Add(":PRIME_ZIP_CD", OracleType.VarChar)
                objSql.Parameters(":PRIME_ZIP_CD").Value = zip
                objSql.Parameters.Add(":PRIME_HOME_PHONE", OracleType.VarChar)
                If home_phone & "" = "" Then
                    objSql.Parameters(":PRIME_HOME_PHONE").Value = DBNull.Value
                Else
                    objSql.Parameters(":PRIME_HOME_PHONE").Value = home_phone
                End If
                objSql.Parameters.Add(":PRIME_TIME_AT_RES", OracleType.VarChar)
                objSql.Parameters(":PRIME_TIME_AT_RES").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_HOME_STATUS", OracleType.VarChar)
                objSql.Parameters(":PRIME_HOME_STATUS").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_EMP_TIME", OracleType.VarChar)
                objSql.Parameters(":PRIME_EMP_TIME").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_EMP_PHONE", OracleType.VarChar)
                objSql.Parameters(":PRIME_EMP_PHONE").Value = DBNull.Value

                objSql.Parameters.Add(":PRIME_MON_INCOME", OracleType.VarChar)
                objSql.Parameters(":PRIME_MON_INCOME").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_YR_INCOME", OracleType.VarChar)
                objSql.Parameters(":PRIME_YR_INCOME").Value = ""
                objSql.Parameters.Add(":RELATIVE_PHONE", OracleType.VarChar)
                objSql.Parameters(":RELATIVE_PHONE").Value = DBNull.Value
                objSql.Parameters.Add(":CREDIT_PROTECTION", OracleType.VarChar)
                objSql.Parameters(":CREDIT_PROTECTION").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_DL", OracleType.VarChar)
                objSql.Parameters(":PRIME_DL").Value = DBNull.Value
                objSql.Parameters.Add(":PRIME_DL_ST", OracleType.VarChar)
                objSql.Parameters(":PRIME_DL_ST").Value = DBNull.Value
                objSql.Parameters.Add(":LANGUAGE_INDICATOR", OracleType.VarChar)
                objSql.Parameters(":LANGUAGE_INDICATOR").Value = "E"
                objSql.Parameters.Add(":REQUESTED_AMOUNT", OracleType.VarChar)
                objSql.Parameters(":REQUESTED_AMOUNT").Value = DBNull.Value
                objSql.Parameters.Add(":AUTHORIZATION", OracleType.VarChar)
                If lbl_auth_no.Text & "" = "" Then
                    objSql.Parameters(":AUTHORIZATION").Value = DBNull.Value
                Else
                    objSql.Parameters(":AUTHORIZATION").Value = lbl_auth_no.Text
                End If

                objSql.ExecuteNonQuery()
            Catch ex As Exception
                Exit Sub
            End Try

            Dim row_id As String = ""
            sql = "SELECT ROW_ID FROM EC_APPLICATION WHERE PRIME_FNAME=:PRIME_FNAME AND PRIME_LNAME=:PRIME_LNAME"
            sql = sql & " ORDER BY ROW_ID DESC"

            Dim MyDataReader1 As OracleDataReader
            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.Parameters.Add(":PRIME_FNAME", OracleType.VarChar)
            objsql2.Parameters(":PRIME_FNAME").Value = fname
            objsql2.Parameters.Add(":PRIME_LNAME", OracleType.VarChar)
            objsql2.Parameters(":PRIME_LNAME").Value = lname

            MyDataReader1 = DisposablesManager.BuildOracleDataReader(objsql2)

            Dim SDC_Response As String = ""
            If MyDataReader1.Read Then
                SDC_Response = SD_Submit_Query_PL(MyDataReader1.Item("ROW_ID").ToString, terminalId, AUTH_ECA, "BLANK", app_tp, processorId)
            End If

            conn.Close()

            If IsNothing(SDC_Response) Then Exit Sub

            Dim Result_String As String = ""
            Dim SDC_Fields As String() = Nothing
            Dim SDC_BreakOut As String() = Nothing
            Dim PB_Response_Cd As String = ""
            Dim Protobase_Response_Msg As String = ""
            Dim Host_Response_Msg As String = ""
            Dim Host_Response_Cd As String = ""
            Dim cc_resp As String = ""

            Dim sep(3) As Char
            sep(0) = Chr(10)
            sep(1) = Chr(12)
            SDC_Fields = SDC_Response.Split(sep)
            Dim s As String
            Dim MsgArray As Array
            For Each s In SDC_Fields
                If InStr(s, ",") > 0 Then
                    MsgArray = Split(s, ",")
                    Select Case MsgArray(0)
                        Case "1003"
                            PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1004"
                            Result_String = Result_String & "Application Status: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                        Case "3165"
                            Result_String = Result_String & "Application Key: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                        Case "1004"
                            Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1009"
                            Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "1010"
                            Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "3305"
                            Result_String = Result_String & "Account Number: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                            acct_no = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                        Case "3306"
                            Result_String = Result_String & "Credit Limit: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                        Case "3317"
                            Result_String = Result_String & "Authorization Number: " & Left(MsgArray(1), Len(MsgArray(1)) - 1) & "<br />"
                            lbl_auth_no.Text = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    End Select
                End If
            Next s
            Result_String = Result_String & "Expiration Date: " & FormatDateTime(Now.Date.AddDays(60).ToString, DateFormat.ShortDate) & "<br />"

            lbl_response.Text = Result_String

            Select Case PB_Response_Cd
                Case "0"
                    cc_resp = "APPROVED"
                Case "0000"
                    cc_resp = "APPROVED"
                Case "60"
                    cc_resp = "DECLINED"
                Case "0060"
                    cc_resp = "DECLINED"
                Case Else
                    Host_Response_Msg = PB_Response_Cd
                    Host_Response_Cd = Protobase_Response_Msg
                    cc_resp = "ERROR"
            End Select

            If app_tp = "3" And cc_resp = "APPROVED" Then
                btn_decline.Enabled = False
                btn_accept.Enabled = True
                Update_QS_Status(txt_cust_cd.Text, "R", lbl_auth_no.Text, cbo_store_cd.Value)
                PopupCreditApp.ShowOnPageLoad = False
            End If
            If app_tp = "1" And cc_resp = "APPROVED" Then
                btn_accept.Enabled = True
                btn_decline.Enabled = True
                Update_QS_Status(txt_cust_cd.Text, "P", lbl_auth_no.Text, cbo_store_cd.Value)
                PopupCreditApp.ShowOnPageLoad = True
            End If
            If cc_resp = "DECLINED" Then
                btn_decline.Enabled = False
                btn_accept.Enabled = False
                Update_QS_Status(txt_cust_cd.Text, "D", lbl_auth_no.Text, cbo_store_cd.Value)
            End If
            If cc_resp = "ERROR" Then
                btn_decline.Enabled = False
                btn_accept.Enabled = False
                Update_QS_Status(txt_cust_cd.Text, "E", lbl_auth_no.Text, cbo_store_cd.Value)
            End If

        End If

    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Private Function GetQuickCreditProviderCode() As String

        'Returns the provider that has been setup in  gp_parms to do a GE_QUICK_CREDIT. This code
        ' must match the entry in  the ASP table for that provider.

        Dim providerCd As String = ""
        Dim cmd As OracleCommand
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try
            conn.Open()
            Dim sql As String = "SELECT value FROM (gp_parms)" &
                                 " WHERE key = 'PTS_POS+' " &
                                 " AND parm = 'GE_QUICK_CREDIT'"

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If MyDataReader.Read Then
                providerCd = MyDataReader.Item("VALUE").ToString
            End If
        Finally
            conn.Close()
        End Try

        Return providerCd.Trim
    End Function

    ''' <summary>
    ''' Used to format the payment amount field in the original payment grid to 
    ''' display the currency symbol.
    ''' </summary>
    ''' <param name="s">the payment amount field value</param>
    ''' <returns></returns>
    Public Function FormatOrigPmtAmt(ByVal s As String) As String
        Return System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol & s
    End Function

    Private Sub ValidateCashDrawer()
        ' MM-6376
        ' Checking the current cash drawer is active or not   
        Dim postDate As Date = FormatDateTime(lbl_post_dt.Text, DateFormat.ShortDate)
        If Not (cbo_store_cd.Value) Is Nothing AndAlso Not (cbo_csh_drw.Value) Is Nothing Then
            If (theSalesBiz.IsCashDrawerActive(ViewState("co_cd"), cbo_store_cd.Value, cbo_csh_drw.Value, postDate)) Then
                mop_drp.Enabled = True
                txt_pmt_amt.Enabled = True
                btn_add.Enabled = True
                btn_commit.Enabled = True
            Else
                mop_drp.Enabled = False
                txt_pmt_amt.Enabled = False
                btn_add.Enabled = False
                btn_commit.Enabled = False
                lbl_Error.Text = Resources.POSMessages.MSG0032
            End If
        End If
    End Sub

    Private Function sy_gc_send_subTp8() As String
        'sabrina add(new sub)

        Dim v_card_num As String = ""
        Dim merchant_id As String = ""
        Dim myipadd As String = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim myret_string As String = ""
        Dim v_term_id As String = ""
        Dim mybal As Integer = 0
        Dim str_del_doc_num As String = ""
        Dim str_cust_cd As String = ""
        Dim v_amt As String = CDbl(lbl_pmt_amt.Text)
        Dim v_refund_cd As String = cbo_payment_type.Value  'sabrina add (R or PMT)

        If InStr(txt_gc.Text, "?") > 0 Then
            Dim sCC As Array
            sCC = Split(txt_gc.Text.ToString, "?")
            txt_gc.Text = Right(sCC(0).Trim, Len(sCC(0)) - 1)
            txt_gc.Enabled = False
        End If

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            If IsDBNull(v_term_id) = True Or isEmpty(v_term_id) = True Or v_term_id = "empty" Then
                ASPxLabel15.Text = "Invalid Terminal ID " & v_term_id
                Exit Function
            End If
        Catch
            ASPxLabel15.Text = Err.Description
        End Try

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If IsDBNull(txt_ivc_cd.Text) = True Or isEmpty(txt_ivc_cd.Text) = True Then
            ASPxLabel15.Text = "No Order# - Cannnot proceed"
            Exit Function
        Else
            str_del_doc_num = txt_ivc_cd.Text
        End If

        If IsDBNull(txt_cust_cd.Text) = True Or isEmpty(txt_cust_cd.Text) = True Then
            ASPxLabel15.Text = "No Customer Code. Cannnot proceed"
            Exit Function
        Else
            str_cust_cd = txt_cust_cd.Text
        End If

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        'subtp8 for GC
        conn.Open()
        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp8"
        sycmd.CommandType = CommandType.StoredProcedure

        sycmd.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = "" ' Sabrina
        sycmd.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
        sycmd.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
        sycmd.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = "8"
        sycmd.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = v_refund_cd
        sycmd.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = "" 'str_s_or_m
        sycmd.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = "GC"
        sycmd.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = ""

        sycmd.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
        sycmd.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
        sycmd.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
        sycmd.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Dim l_aut As String = ""
        Dim l_dsp As String = ""
        Dim l_ack As String = ""
        Dim l_res As String = ""
        Dim l_crn As String = ""
        Dim l_exp As String = ""
        Dim l_tr2 As String = ""
        Dim l_rcp As String = ""
        Dim l_filename As String = ""
        Dim l_printcmd As String = ""

        'sycmd.ExecuteNonQuery()

        Try
            syda.Fill(syds)

        Catch ex As Exception
            ASPxLabel15.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try


        If IsDBNull(sycmd.Parameters("p_aut").Value) = False Then
            l_aut = sycmd.Parameters("p_aut").Value
        Else
            l_aut = "empty"
        End If

        l_dsp = sycmd.Parameters("p_dsp").Value
        l_ack = sycmd.Parameters("p_ackInd").Value
        l_res = sycmd.Parameters("p_res").Value
        l_dsp = sycmd.Parameters("p_dsp").Value

        If IsDBNull(sycmd.Parameters("p_tr2").Value) = False Then
            l_tr2 = sycmd.Parameters("p_tr2").Value
        Else
            l_tr2 = "empty"
        End If

        If IsDBNull(sycmd.Parameters("p_exp").Value) = False Then
            l_exp = sycmd.Parameters("P_exp").Value
        Else
            l_exp = "empty"
        End If

        If IsDBNull(sycmd.Parameters("p_crn").Value) = False Then
            l_crn = sycmd.Parameters("p_crn").Value.ToString()
        Else
            l_crn = "empty"
        End If

        ASPxLabel15.Text = l_dsp

        'sabrina R2019 
        'If l_dsp = "01 Approval" Then   'transation done successfully
        If InStr(UCase(l_dsp), "APPR") > 0 Then
            btnSubmit.Visible = True
            ASPxLabel15.Text = l_dsp & " - Press Save to continue"
            l_filename = sycmd.Parameters("p_outfile").Value
            l_printcmd = sycmd.Parameters("p_cmd").Value
            Session("str_print_cmd") = l_printcmd
            Session("auth_no") = l_aut
            Session("gc_approval_cd") = l_aut
            Session("gc_card") = l_crn
            txt_gc.Text = l_crn
            'print_receipt(l_printcmd)
        Else   'transation not done 
            btnSubmit.Visible = False
            l_dsp = sycmd.Parameters("p_dsp").Value
            ASPxLabel15.Text = l_dsp & " - press Clear to try again"
            txt_gc.Text = l_crn
        End If
        conn.Close()

        Return l_dsp
        Dim x As Exception
        Try
        Catch x
        End Try

    End Function

    Private Sub sy_processGC()
        'sabrina add
        PopupGiftCardPmt.ShowOnPageLoad = True
        sy_gc_send_subTp8()
    End Sub
    Protected Sub btn_Clear_Click(sender As Object, e As EventArgs) Handles btn_Clear.Click
        'sabrina add
        txt_gc.Text = ""
        sy_gc_send_subTp8()
    End Sub

    Public Sub sy_printer_drp_Populate()

        'sabrina new sub added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd) 'sabrina emp_cd to get default printer
        'Dim default_printer As String = ""
        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = "Printers:"
        End If

        Session("default_prt") = v_default_printer

        'sql = "SELECT PRINT_Q FROM misc.PRINT_QUEUE a WHERE PRINT_Q IS NOT NULL "
        'sql = sql & " AND (STD_CHK_PRINTER.IS_VALID_STR_PRINTER(PRINT_Q,STD_CHK_PRINTER.GET_HOME_STR("
        'sql = sql & "'" & v_user_init & "')) = 'Y'"
        'sql = sql & " OR STD_CHK_PRINTER.IS_SUPR_USR('" & v_user_init & "') = 'Y') "
        'sql = sql & " and exists (select 'x' from misc.console c where c.print_q = a.print_q) "
        'sql = sql & " and print_q <> '" & v_default_printer & "'"
        'sql = sql & " ORDER BY PRINT_Q "

        'Daniela Sept 08
        sql = "select pi.printer_nm PRINT_Q "
        sql = sql & "from  printer_info pi "
        sql = sql & "where pi.store_cd = (select home_store_cd "
        sql = sql & "from emp "
        sql = sql & "where emp_init = :v_user_init) "
        sql = sql & "and   pi.printer_tp = 'INVOICE'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try


    End Sub
    Private Function sy_get_emp_init(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Private Function sy_get_default_printer_id(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Sub print_pick_ticket(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByVal p_printer As String)
        ' Daniela reprint
        Dim connString As String
        Dim objConnection As OracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try

            'Daniela check lock first
            Dim soRowid As String = String.Empty
            myCMD.Connection = objConnection

            myCMD.Transaction = objConnection.BeginTransaction()
            soRowid = SalesUtils.LockSoRec(p_delDocNum, myCMD)
            myCMD.CommandText = "std_tenderretail_util.invoice_print_pickticket"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
            myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
            myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer
            'Daniela add french
            Dim cult As String = UICulture
            Dim lang As String = "E"
            If InStr(cult.ToLower, "fr") >= 1 Then
                lang = "F"
            End If
            myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang


            myCMD.ExecuteNonQuery()
            myCMD.Transaction.Commit()

        Catch ex As Exception
            myCMD.Transaction.Rollback()
            'If ex.Message = String.Format(Resources.POSErrors.ERR0042, p_delDocNum) Then
            'btn_save_fi_co.Enabled = False
            'End If
            lbl_pickticket.Text = ex.Message
            Exit Sub

        Finally
            If Not IsNothing(myCMD.Transaction) Then
                myCMD.Transaction.Dispose()
            End If
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        'Daniela end
    End Sub

    Protected Sub btn_pickup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pickup.Click
        ' Daniela print ticket option
        lbl_pickticket.Text = ""
        Dim balance As Double
        Dim pwd As String = txt_cshr_pwd.Text
        Dim cshPass As String = Request("ip_csh_pass")
        ' Force to enter password to print ticket
        If Session("csh_pss_flag") Is Nothing And Request("referrer") = "salesordermaintenance.aspx" Then
            lbl_pickticket.Text = Resources.LibResources.Label804
            btn_pickup.Enabled = False
            Exit Sub
        End If
        If Session("csh_pss_flag") = "Y" And Request("referrer") = "salesordermaintenance.aspx" Then
            'Only users who have an EMP_TP_CD of PRT should be allowed to print pick
            Dim empTpPrt As String = "PRT"
            Dim empTpCd = LeonsBiz.GetEmpTpCd(Session("emp_cd"), empTpPrt)
            If empTpCd <> empTpPrt Then
                lbl_pickticket.Text = "Employee not allowed to Print Ticket."
                btn_pickup.Enabled = False
                Exit Sub
            End If
            ' Commented as per new IT request 1893
            '    Try
            '        balance = CDbl(lbl_new_pmts_amt.Text)
            '    Catch
            '        balance = 1 ' Force enter password
            '    End Try
            '    If balance > 0 Then
            '        If pwd.isNotEmpty Then
            '            ValidateCashier()
            '            ' Force to enter password if balance
            '            If Label5.Text = "Invalid Password" Then
            '                lbl_pickticket.Text = "Order is not paid in full. Please enter valid Cashier Password."
            '                Exit Sub
            '            End If
            '        Else
            '            If Session("csh_pss_flag") Is Nothing Then
            '                lbl_pickticket.Text = "Order is not paid in full. Please enter Cashier Password."
            '                btn_pickup.Enabled = False
            '                Exit Sub
            '            End If
            '        End If
            '    End If
        End If
        Dim printer As String = printer_drp.SelectedItem.Value.ToString()
        If txt_ivc_cd.Text.isNotEmpty AndAlso Not Session("emp_cd") Is Nothing AndAlso Not printer Is Nothing Then
            lbl_pickticket.Text = Resources.LibResources.Label784 & " " & txt_ivc_cd.Text & " " & Resources.LibResources.Label785
            btn_pickup.Enabled = False
            print_pick_ticket(txt_ivc_cd.Text, Session("emp_cd"), printer)
        End If

        'lbl_pickticket.Text = "Pick Ticket for  " & txt_ivc_cd.Text & " has been printed."


    End Sub


    Protected Sub cnc_gc_swipe_Click(sender As Object, e As EventArgs) Handles cnc_gc_swipe_btn.Click
        'sabrina CNCREFUND ADD
        cnc_msg.Text = ""

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_rt1 As String
        Dim l_rt2 As String
        Dim l_tr1 As String
        Dim l_tr2 As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_cst As String
        Dim l_dsp As String

        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            cnc_msg.Text = "No designated pinpad"
            Exit Sub
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp7_swipe"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_trantp", OracleType.VarChar)).Value = "SW"
            myCMD.Parameters.Add(New OracleParameter("p_subtp", OracleType.VarChar)).Value = "7"
            myCMD.Parameters.Add(New OracleParameter("p_displn1", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_displn2", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_rsvd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_term_id", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cst", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 500).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(UCase(myCMD.Parameters("p_dsp").Value), "SUCCESSFUL") > 0 Then

                l_crn = myCMD.Parameters("p_crn").Value
                cnc_gc_no.Text = l_crn
                Session("cnc_gc_no") = l_crn
                If InStr(sy_cnc_process_gc("M3", ""), "?") > 0 Then
                    cnc_msg.Text = "For existing Gift Card please check the RELOAD checkbox"
                End If
            Else   'transation not done 
                l_crn = ""
                Session("cnc_gc_no") = l_crn
                cnc_msg.Text = "Click to Swipe Again"
            End If

            myCMD.Cancel()
            myCMD.Dispose()
        Catch
            Throw
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
    Protected Sub sy_cnc_set_info(ByVal p_del_doc_num As String)
        'sabrina CNCREFUND add

        Dim sql As String = ""
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connLocal.Open()

        sql = "select c.cust_cd cust_cd, c.fname||' '||c.lname cust_name,c.home_phone,c.addr1,c.addr2||' '||c.zip_cd cust_addr2 "
        sql = sql & " from cust c "
        sql = sql & " where c.cust_cd =  (select distinct nvl(ar.cust_cd,'x') from ar_trn ar "
        sql = sql & " where ar.ivc_cd = '" & p_del_doc_num & "')"

        cmd = DisposablesManager.BuildOracleCommand(SQL, connLocal)
        Try
            'Execute DataReader 
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            reader.Read()
            cnc_cust_cd.Text = reader.Item("cust_cd").ToString
            cnc_cust_name.Text = reader.Item("cust_name").ToString
            cnc_cust_addr1.Text = reader.Item("addr1").ToString
            cnc_cust_addr2.Text = reader.Item("cust_addr2").ToString
            cnc_cust_phone.Text = reader.Item("home_phone").ToString
            'store_cd = store_cd & "'" & reader.Item("CASH_DWR_CD").ToString & "',"
            Session("cnc_ar_trn_found_sw") = "Y"
        Catch ex As Exception
            connLocal.Close()
            'Throw
            lbl_Error.Text = "Order Not Found."
            Session("cnc_ar_trn_found_sw") = "N"
        End Try
        connLocal.Close()

    End Sub
    Public Function sy_cnc_get_credit(ByVal p_del_doc_num As String) As Double

        'sabrina CNCREFUND add
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select sum(decode(att.dc_cd,'C',ar.amt,ar.amt*-1)) as CNC_AMT "
        sql = sql & " from ar_trn ar, ar_trn_tp att "
        sql = sql & " where ar.trn_tp_cd = att.trn_tp_cd "
        sql = sql & " and ar.ivc_cd = '" & p_del_doc_num & "'"
        sql = sql & " and ar.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ar.stat_cd <> 'A' "
        'sql = sql & " and (ar.trn_tp_cd in ('TFC','CNC') or "
        'sql = sql & " (ar.trn_tp_cd = 'PMT' and ar.des = 'gift card')) "
        sql = sql & " group by ar.ivc_cd "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("CNC_AMT").ToString
            Else
                Return 0
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Public Function sy_cnc_get_debit(ByVal p_del_doc_num As String) As Double

        'sabrina CNCREFUND add
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select sum(ar.amt) as CNC_AMT "
        sql = sql & " from ar_trn ar,  ar_trn_tp att "
        sql = sql & " where ar.trn_tp_cd = att.trn_tp_cd "
        sql = sql & " and ar.ivc_cd = '" & p_del_doc_num & "'"
        sql = sql & " and ar.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ar.trn_tp_cd = 'R' "
        'sql = sql & " and (ar.trn_tp_cd = 'R' and ar.mop_cd = 'PMT')  "
        'sql = sql & "   or   (ar.trn_tp_cd = 'GCS' and ar.mop_cd is null))  "
        sql = sql & " group by ar.ivc_cd "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("CNC_AMT").ToString
            Else
                Return 0
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Function sy_cnc_get_max_refund(ByVal p_del_doc_num As String) As Double
        'sabrina cncRefund add
        Dim v_amt As Double = 0

        'sabrina R1924 v_amt = Math.Round((sy_cnc_get_credit(UCase(p_del_doc_num)) - sy_cnc_get_debit(UCase(p_del_doc_num))), 2)
        v_amt = Math.Round((sy_cnc_get_credit(UCase(p_del_doc_num))), 2)   'sabrina R1924
        Return v_amt

    End Function

    Protected Sub cnc_commit_Click(sender As Object, e As EventArgs) Handles cnc_commit_btn.Click
        'sabrina cncRefund add
        Dim v_orig_refund_amt As Double = sy_cnc_get_max_refund(UCase(cnc_del_doc_num.Text))
        Dim sy_credit_amt As Double = 0

        Session("cnc_orig_refund_amt") = String.Format("{0:C}", v_orig_refund_amt)

        If IsDBNull(cnc_gc_no.Text) = True Or cnc_gc_no.Text Is Nothing Or cnc_gc_no.Text = "" Then
            cnc_msg.Text = "Please enter a valid GiftCard# to proceed"
            Exit Sub
        End If

        If IsDBNull(cnc_refund_amt.Text) = True Or cnc_refund_amt.Text = "" Then
            cnc_msg.Text = "Refund amount must be greater than zero to proceed"
            Exit Sub
        End If

        If IsNumeric(cnc_refund_amt.Text) Then
            If CDbl(cnc_refund_amt.Text) <= 0 Then     'sabrina July27
                cnc_msg.Text = "Refund amount must be greater than zero to proceed"
                Exit Sub
            End If
        Else
            cnc_msg.Text = "Refund amount is invalid"
            Exit Sub
        End If

        If CDbl(cnc_refund_amt.Text) > v_orig_refund_amt Then
            cnc_msg.Text = "Maximum refund allowed is " & Session("cnc_orig_refund_amt").ToString & " - please try again "
            Exit Sub
        End If


        If CheckBox_reload.Checked Then
            cnc_msg.Text = sy_cnc_process_gc("Y3", CDbl(cnc_refund_amt.Text))  'reload
        Else
            cnc_msg.Text = sy_cnc_process_gc("M3", CDbl(cnc_refund_amt.Text))  'issue
        End If

        If InStr(UCase(cnc_msg.Text), "APPROV") > 0 Then
            sy_cnc_add_ar_trn()
            'sabrina R1924 sy_cnc_print_rcpt()
            'sabrina R1924 replace with following
            Dim v_doc_seq_num As String = Right(cnc_del_doc_num.Text, 4)
            Dim v_pmtStore As String
            If Not Session("clientip") Is Nothing Then
                v_pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
            Else ' if not go global then default to remote ip
                v_pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
            End If
            If v_pmtStore & "" = "" Then
                v_pmtStore = cbo_store_cd.Value
            End If
            sy_print_instpmt(cnc_del_doc_num.Text, Session("emp_cd"), v_doc_seq_num, v_pmtStore, cnc_cust_cd.Text)
            'sabrina R1924 end


            cnc_gc_no.Text = ""
            'sabrina R1924 sy_credit_amt = sy_cnc_get_credit(UCase(cnc_del_doc_num.Text)) - sy_cnc_get_debit(UCase(cnc_del_doc_num.Text))
            sy_credit_amt = sy_cnc_get_credit(UCase(cnc_del_doc_num.Text)) 'sabrina R1924
            cnc_refund_amt.Text = String.Format("{0:C}", sy_credit_amt)
            CheckBox_reload.Checked = False
        End If


    End Sub

    Public Function sy_cnc_process_gc(ByVal p_tran_tp As String, ByVal p_amt As String) As String
        'sabrina cncRefund add
        If IsDBNull(cnc_gc_no.Text) = True Then
            cnc_msg.Text = "Swipe Gift Card to Continue"
            Exit Function
        End If

        Dim v_card_num As String = cnc_gc_no.Text
        Dim merchant_id As String = Lookup_Merchant("GC", Session("home_store_cd"), "GC")
        Dim myipadd As String = Session("clientip")
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim myret_string As String = ""
        Dim v_term_id As String = ""

        Session("cnc_app_cd") = ""
        Session("cnc_bal") = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            If IsDBNull(v_term_id) Or isEmpty(v_term_id) Or v_term_id = "empty" Then
                cnc_msg.Text = "Invalid Termianl ID " & v_term_id
                Exit Function
            End If
        Catch
            cnc_msg.Text = Err.Description
        End Try

        Dim mybal As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp4"
        sycmd.CommandType = CommandType.StoredProcedure

        'subtp4
        sycmd.Parameters.Add("p_req_tp", OracleType.VarChar).Value = p_tran_tp
        sycmd.Parameters.Add("p_crdnum", OracleType.VarChar).Value = v_card_num
        sycmd.Parameters.Add("p_crdtoken", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_pswd", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_amt", OracleType.VarChar).Value = p_amt

        sycmd.Parameters.Add("p_tran_no", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_term_id", OracleType.VarChar).Value = v_term_id
        sycmd.Parameters.Add("p_currency", OracleType.VarChar).Value = "124"
        sycmd.Parameters.Add("p_crdnum2", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_opid", OracleType.VarChar).Value = ""
        'end of subtp4

        sycmd.Parameters.Add("p_sessionid", OracleType.VarChar).Value = sessionid
        sycmd.Parameters.Add("p_cust_cd", OracleType.VarChar).Value = UCase(cnc_cust_cd.Text)
        sycmd.Parameters.Add("p_doc_num", OracleType.VarChar).Value = UCase(cnc_del_doc_num.Text)

        sycmd.Parameters.Add("p_ackind", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authno", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transtp", OracleType.VarChar, 100).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_rcp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        'gift card
        sycmd.Parameters.Add("p_nba", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bnb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bpb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crf", OracleType.VarChar, 150).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Try
            syda.Fill(syds)

        Catch ex As Exception
            lbl_response.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try

        Dim v_rcp As String = sycmd.Parameters("p_rcp").Value.ToString
        myret_string = sycmd.Parameters("p_res").Value.ToString

        'sabrina R2019
        'If v_rcp = "DECLINED" Or (InStr(v_rcp, "APPROVED") = 0 And v_rcp <> "?") Then
        '    conn.Close()
        '    Return "DECLINED" & ": " & sycmd.Parameters("p_dsp").Value
        'End If


        'sabrina R2019
        If (InStr(UCase(v_rcp), "APPR") = 0) Then
            conn.Close()
            Return "DECLINED" & ": " & sycmd.Parameters("p_dsp").Value
        End If

        Session("cnc_app_cd") = sycmd.Parameters("p_aut").Value.ToString
        Session("cnc_bal") = sycmd.Parameters("p_nba").Value.ToString


        Return sycmd.Parameters("p_dsp").Value

    End Function

    Protected Sub cnc_cancel_Click(sender As Object, e As EventArgs) Handles cnc_cancel_btn.Click
        'sabrina cncRefund add
        cnc_cust_cd.Text = ""
        cnc_cust_name.Text = ""
        cnc_cust_addr1.Text = ""
        cnc_cust_addr2.Text = ""
        cnc_cust_phone.Text = ""
        cnc_gc_no.Text = ""


        Session("cnc_ar_trn_found_sw") = ""
        Session("cnc_orig_refund_amt") = String.Format("{0:C}", 0)
        Session("cnc_gc_no") = ""
        Session("cnc_refund_proceed") = ""
        Session("cnc_app_cd") = ""
        Session("cnc_bal") = ""

        ASPxPopupcncrefund.ShowOnPageLoad = False
        lbl_Error.Text = ""



    End Sub



    Function sy_cnc_chk_security() As String

        'sabrina CNCREFUND add
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoInsert('" & Session("emp_cd") & "','IndependentPaymentProcessing.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Public Sub sy_cnc_add_ar_trn()
        'sabrina cncRefund

        Dim merchant_id As String = ""
        Dim sql As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim v_amt As Double

        Dim pmtStore As String
        If Not Session("clientip") Is Nothing Then
            pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else ' if not go global then default to remote ip
            pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If pmtStore & "" = "" Then
            pmtStore = cbo_store_cd.Value
        End If

        If Session("csh_drw") = "" Then
            Session("csh_drw") = cbo_csh_drw.Value
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        conn.Open()

        'Daniela prevent top sql
        Dim v_store As StoreData
        If isEmpty(Session("CO_CD")) Then
            v_store = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
            Session("CO_CD") = v_store.coCd
        End If

        sql = "insert into ar_trn (CO_CD,CUST_CD,MOP_CD,EMP_CD_CSHR,EMP_CD_OP,ORIGIN_STORE,CSH_DWR_CD,TRN_TP_CD,IVC_CD,"
        sql = sql & "BNK_CRD_NUM,DOC_SEQ_NUM,AMT,POST_DT,STAT_CD,AR_TP,APP_CD,DES,PMT_STORE,ORIGIN_CD,CREATE_DT,AR_TRN_PK) "  'sabrina R1924 add doc_seq_num
        sql = sql & " values (:CO_CD,:CUST_CD,:MOP_CD,:EMP_CD_CSHR,:EMP_CD_OP,:ORIGIN_STORE,:CSH_DWR_CD,:TRN_TP_CD,:IVC_CD,"
        sql = sql & ":BNK_CRD_NUM,:DOC_SEQ_NUM,:AMT,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & ":STAT_CD,:AR_TP,:APP_CD,:DES,:PMT_STORE,:ORIGIN_CD,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & " seq_ar_trn.nextval)"

        cmdAddRecords.Parameters.Clear()

        Try
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = sql.ToString

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    'Daniela
                    'cmdAddRecords.Parameters(":CO_CD").Value = v_store.coCd
                    cmdAddRecords.Parameters(":CO_CD").Value = Session("CO_CD")

                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CUST_CD").Value = cnc_cust_cd.Text

                    cmdAddRecords.Parameters.Add(":MOP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":MOP_CD").Value = "GC"

                    cmdAddRecords.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_CSHR").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_OP").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim

                    cmdAddRecords.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CSH_DWR_CD").Value = Session("csh_drw").ToString.Trim

                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = "R"

                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":IVC_CD").Value = cnc_del_doc_num.Text

                    cmdAddRecords.Parameters.Add(":BNK_CRD_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":BNK_CRD_NUM").Value = cnc_gc_no.Text


                    If IsNumeric(cnc_refund_amt.Text) Then
                        'v_amt = CInt(Session("tdpl_amt"))
                        v_amt = CDbl(cnc_refund_amt.Text)
                    Else
                        v_amt = 0
                    End If

                    'sabrina R1924 add
                    cmdAddRecords.Parameters.Add(":DOC_SEQ_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":DOC_SEQ_NUM").Value = Right(cnc_del_doc_num.Text, 4)

                    cmdAddRecords.Parameters.Add(":AMT", OracleType.Number)
                    cmdAddRecords.Parameters(":AMT").Value = v_amt

                    cmdAddRecords.Parameters.Add(":STAT_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":STAT_CD").Value = "T"

                    cmdAddRecords.Parameters.Add(":AR_TP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":AR_TP").Value = "O"

                    cmdAddRecords.Parameters.Add(":APP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":APP_CD").Value = Session("cnc_app_cd")

                    cmdAddRecords.Parameters.Add(":DES", OracleType.VarChar)
                    cmdAddRecords.Parameters(":DES").Value = "GIFT CARD"

                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":PMT_STORE").Value = pmtStore

                    cmdAddRecords.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_CD").Value = "PGC"

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try

        cnc_msg.Text = "Refund is complete - GiftCard Balance is " & Session("cnc_bal") & " -Auth# is " & Session("cnc_app_cd")



    End Sub

    Public Sub sy_cnc_print_rcpt()

        'sabrina cncRefund add
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim v_ufm As String = "LPO"
        'Daniela french UFM
        Dim cult As String = UICulture
        If InStr(cult.ToLower, "fr") >= 1 Then
            v_ufm = "PRF"
        End If
        'End Daniela
        Dim v_seq_num As String = ""

        Dim co_cd As String = Session("CO_CD")
        Dim grpCd As String = ""
        If isNotEmpty(co_cd) Then
            grpCd = LeonsBiz.GetGroupCode(co_cd)
            If grpCd = "BRK" Then
                If InStr(cult.ToLower, "fr") >= 1 Then
                    v_ufm = "BFC"
                Else
                    v_ufm = "BRC"
                End If
            End If
        End If

        v_seq_num = Right(cnc_del_doc_num.Text, 4)

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "STD_TENDERRETAIL_UTIL.RECEIPT_PRINT"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = cnc_del_doc_num.Text
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = Session("emp_cd")
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = v_seq_num
        'myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = cnc_cust_cd.Text
        'myCMD.Parameters.Add(New OracleParameter("p_co_cd", OracleType.Char)).Value = Session("CO_CD")
        myCMD.Parameters.Add(New OracleParameter("p_ufm", OracleType.Char)).Value = v_ufm

        Try
            myCMD.ExecuteNonQuery()

        Catch ex As Exception
            objConnection.Close()
            Throw ex
        End Try
    End Sub
    Public Sub Add_DCSCompany(ByVal p_dcsplan As String)
        'sabrina R1999
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        Dim sql As String = "select plan_cd mop_cd, des from brick_dcs_plan where plan_cd = '" & p_dcsplan & "'"   'sabrina Jan22,2016

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "MOP_CD"
                cbo_finance_company.DataTextField = "DES"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select TD finance plan")
            cbo_finance_company.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub
    Public Sub sy_AddDCSPlan()

        'sabrina R1999 (new sub) 
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet

        'sabrina R2335 - for Quebec only show EMP (equal monthly payment plans)
        Dim v_str As String = ""
        Dim sql As String = ""

        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If

        'Alice added on Mar25, 2019 for ticket 572512 --start
        Dim CashierActStore As String = GetCashierActivatedStore(Session("emp_init"))
        If String.IsNullOrEmpty(CashierActStore) Then
            CashierActStore = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If
        '--end.

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Or LeonsBiz.isLeonFranStr(CashierActStore) = "Y" Then  'Alice updated on Oct11,2018 for request 4115
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') order by des"  'sabrina Jan22,2016

        Else
            If LeonsBiz.isQuebecStore(v_str) = "Y" Then
                sql = "select plan_cd mop_cd, des from brick_dcs_plan "
                sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  "
                sql = sql & " order by des"
            Else
                sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  order by des"  'sabrina Jan22,2016
            End If
        End If


        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "MOP_CD"
                cbo_finance_company.DataTextField = "DES"
                cbo_finance_company.DataBind()
            End If

            cbo_finance_company.Items.Insert(0, "Please select VISAD finance plan")
            cbo_finance_company.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub
    'Alice added on Mar25, 2019 for ticket 572512
    Public Shared Function GetCashierActivatedStore(ByVal p_usr As String) As String

        Dim v_str As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select store_cd from CSHR_ACTIVATION " &
                            " where cshr_init = '" & p_usr & "'"


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_str = dbReader.Item("store_cd")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Return ""
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return v_str

    End Function


    'Alice added on Oct 10, 2018 for request 4115
    Public Sub sy_AddFLXPlan()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet


        Dim v_str As String = ""
        Dim sql As String = ""

        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If

        If LeonsBiz.isQuebecStore(v_str) = "Y" Then
            sql = "select plan_cd mop_cd, des from brick_dcs_plan "
            sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='FLX' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')"
            sql = sql & " order by des"
        Else
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FLX' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y') order by des"
        End If


        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)
        cbo_finance_company_flx.ClearSelection()

        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                cbo_finance_company_flx.DataSource = ds
                cbo_finance_company_flx.DataValueField = "MOP_CD"
                cbo_finance_company_flx.DataTextField = "DES"
                cbo_finance_company_flx.DataBind()
            End If

            cbo_finance_company_flx.Items.Insert(0, "Please Select Flexiti finance plan")
            cbo_finance_company_flx.SelectedIndex = 0

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub

    'Alice added on Oct 10, 2018 for request 4115
    Protected Sub btn_save_flx_Click(sender As Object, e As EventArgs) Handles btn_save_flx.Click

        Dim strfinCo = cbo_finance_company_flx.SelectedValue
        Session("lucy_as_cd_flx") = strfinCo
        ' ASPxPopupControl2.ShowOnPageLoad = False


        Dim merchant_id As String = "" 'lucy add



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objsql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        If txt_fi_appr_flx.Text & "" = "" Then
                txt_fi_appr_flx.Focus()
                lbl_fi_warning_flx.Text = Resources.LibResources.Label836 'sabrina-french dec14 "6 digit Auth# Required"
                Exit Sub
            Else
                If Len(txt_fi_appr_flx.Text) <> 6 Then
                    lbl_fi_warning_flx.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
                    txt_fi_appr_flx.Focus()
                    Exit Sub
                End If
            End If

            If LeonsBiz.isvalidDCSplan(cbo_finance_company_flx.SelectedValue()) <> "Y" Then
                cbo_finance_company_flx.Focus()
                lbl_fi_warning_flx.Text = Resources.LibResources.Label861 'sabrina-french dec14 "Select a Plan"
                Exit Sub
            End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp_flx.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString


        If Is_Finance_DP(lbl_pmt_tp_flx.Text) = True Then
            bc = "FI"
        End If

        'lbl_GE_tran_id.Text = txt_fi_acct.Text  'lucy add
        'lbl_GE_tran_id.Text = Encrypt(txt_fi_acct.Text, "CrOcOdIlE") 'lucy


        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()


        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":sessionid", OracleType.VarChar)
            objsql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objsql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objsql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objsql.Parameters.Add(":AMT", OracleType.VarChar)
            objsql.Parameters.Add(":DES", OracleType.VarChar)
            objsql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objsql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objsql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objsql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objsql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objsql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objsql.Parameters(":MOP_CD").Value = lbl_pmt_tp_flx.Text
            objsql.Parameters(":SECURITY_CD").Value = String.Empty
            objsql.Parameters(":EXP_DT").Value = String.Empty
            objsql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objsql.Parameters(":DES").Value = des
            objsql.Parameters(":MOP_TP").Value = bc
            objsql.Parameters(":FIN_CO").Value = cbo_finance_company_flx.SelectedValue
            objsql.Parameters(":APPROVAL_CD").Value = txt_fi_appr_flx.Text
            objsql.Parameters(":DL_STATE").Value = String.Empty
            objsql.Parameters(":DL_LICENSE_NO").Value = String.Empty

            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupFinancePmt_FLX.ShowOnPageLoad = False
        bindgrid()
        UpdateTotals()



    End Sub


    'Alice added on Dec 13, 2018 for request 4394
    Protected Sub btn_save_china_Click(sender As Object, e As EventArgs) Handles btn_save_china.Click


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objsql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        'validate auth code
        If txt_fi_appr_china.Text & "" = "" Then
            txt_fi_appr_china.Focus()
            lbl_fi_warning_china.Text = Resources.LibResources.Label836 'sabrina-french dec14 "6 digit Auth# Required"
            Exit Sub
        Else
            If Len(txt_fi_appr_china.Text) <> 6 Then
                lbl_fi_warning_china.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
                txt_fi_appr_china.Focus()
                Exit Sub
            End If
        End If


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp_china.Text & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count


        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString


        If Is_Finance_DP(lbl_pmt_tp_china.Text) = True Then
            bc = "FI"
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()


        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":sessionid", OracleType.VarChar)
            objsql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objsql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objsql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objsql.Parameters.Add(":AMT", OracleType.VarChar)
            objsql.Parameters.Add(":DES", OracleType.VarChar)
            objsql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objsql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objsql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objsql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objsql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objsql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objsql.Parameters(":MOP_CD").Value = lbl_pmt_tp_china.Text
            objsql.Parameters(":SECURITY_CD").Value = String.Empty
            objsql.Parameters(":EXP_DT").Value = ""
            objsql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objsql.Parameters(":DES").Value = des
            objsql.Parameters(":MOP_TP").Value = bc
            objsql.Parameters(":FIN_CO").Value = String.Empty
            objsql.Parameters(":APPROVAL_CD").Value = txt_fi_appr_china.Text
            objsql.Parameters(":DL_STATE").Value = String.Empty
            objsql.Parameters(":DL_LICENSE_NO").Value = String.Empty

            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupFinancePmt_China.ShowOnPageLoad = False
        bindgrid()
        UpdateTotals()



    End Sub



    Public Function sy_brk_process_visad() As String

        'R1999 sabrina new function

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String = cbo_finance_company.SelectedValue
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim strAsCd As String = cbo_finance_company.SelectedValue


        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return Resources.LibResources.Label849 'sabrina-french dec14 "No designated pinpad"
        End If

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Session("lucy_display") = ""
            Dim str_del_doc_num As String = Session("del_doc_num")
            If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
                str_del_doc_num = ""
            End If
            Session("del_doc_num") = ""
            Dim str_cust As String = Session("cust_cd")
            str_cust = txt_cust_cd.Text
            v_auth_no = "          "
            v_mop_cd = "DCS"
            v_trans_type = "FI"
            If Len(Trim(lbl_pmt_amt.Text)) = 0 Then
                lbl_pmt_amt.Text = Session("lucy_amt")
            End If
            v_amt = CDbl(lbl_pmt_amt.Text)
            v_private_lbl_finance_tp = strAsCd

            'R1999 sabrina add
            v_card_num = ""
            v_expire_date = ""
            'R1999 sabrina comment out to check prompt to swipe
            'R1999 sabrina Nov20 - changed len to 16
            If Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = True Then
                'sabrina R1999 Nov20
                If txt_fi_exp_dt.Text & "" = "" Then
                    txt_fi_exp_dt.Focus()
                    Return "Exp. Date Required"
                End If
                'sabrina R1999 Nov20
                v_card_num = Trim(txt_fi_acct.Text)
                v_expire_date = txt_fi_exp_dt.Text
                str_s_or_m = "M"
            ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
                v_card_num = Session("lucy_card_num")
                v_card_num = ""
                v_expire_date = ""
                str_s_or_m = "S"
            Else    'swipe
                v_card_num = ""
                v_expire_date = ""
                str_s_or_m = "S"
            End If
            'sabrina R1999
            If cbo_payment_type.Value IsNot Nothing Then
                If cbo_payment_type.Value = "R" Then
                    str_pmt_tp = "R"
                    If txt_fi_appr.Text & "" = "" Then
                        txt_fi_appr.Focus()
                        Return Resources.LibResources.Label836 'sabrina-french dec14 "Auth# required"
                    Else
                        str_auth = txt_fi_appr.Text
                    End If

                Else : str_pmt_tp = "DEP"
                End If
            Else : str_pmt_tp = "PMT"
            End If

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = "8"
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            v_trans_no = myCMD.Parameters("p_transTp").Value
            'sabrina R1999
            If myCMD.Parameters("p_crn").Value & "" <> "" Then
                l_crn = myCMD.Parameters("p_crn").Value
                txt_fi_acct.Text = Left(l_crn, 16)
            End If

            lbl_fi_warning.Text = myCMD.Parameters("p_dsp").Value
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value
                lucy_lbl_ind_aprv.ForeColor = Color.Green
                lucy_lbl_ind_aprv.Text = l_dsp
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then

                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    ' sabrina R1999 l_crn = myCMD.Parameters("p_crn").Value

                    l_filename = myCMD.Parameters("p_outfile").Value
                    l_printcmd = myCMD.Parameters("p_cmd").Value

                    Session("str_print_cmd") = l_printcmd
                    Session("auth_no") = l_aut
                    Session("lucy_approval_cd") = l_aut
                    Session("lucy_bnk_card") = Left(l_crn, 16)

                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                    print_receipt(l_printcmd)

                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                    txt_fi_appr.Text = l_aut
                    txt_fi_exp_dt.Text = Session("lucy_exp_dt")
                End If

            Else   'transation not done 

                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch
            Throw

        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton concern
        Session.Abandon()
        Response.ClearHeaders()
        Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
        Response.AddHeader("Pragma", "no-cache")

        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
    End Sub

#Region "Alice added on May 31, 2019 for request 235"



    Protected Sub txt_fm_exp_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim regex1 As Regex = New Regex("^[0-9]")
        Dim match1 As Match = regex1.Match(txt_fm_exp.Text)
        If match1.Success AndAlso txt_fm_exp.Text <> "" AndAlso CInt((txt_fm_exp.Text).Substring(0, 2)) >= 1 AndAlso CInt((txt_fm_exp.Text).Substring(0, 2)) <= 12 Then
            txt_fm_appr.Focus()
            lbl_fm_warning.Text = ""
        Else

            lbl_fm_warning.Visible = True
            lbl_fm_warning.Text = " ** Invalid Month and Year ** "
            txt_fm_exp.Focus()
        End If

    End Sub

    Protected Sub txt_fm_appr_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Len(txt_fm_appr.Text) <> 6 Then
            lbl_fm_warning.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
            txt_fm_appr.Focus()
        Else
            lbl_fm_warning.Text = ""
        End If

    End Sub
    Protected Sub txt_fm_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        txt_fm_exp.Focus()

        ' verify the card/acct number 
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txt_fm_acct.Text)

        If myTypeValid.CCValid = False Then
            txt_fm_acct.Focus()
            lbl_fm_warning.Text = "Mastercard is invalid"
        Else
            txt_fm_exp.Focus()
            lbl_fm_warning.Text = ""
        End If

    End Sub



    Protected Sub btn_save_fm_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fm_co.Click

        Dim pmtType As String = cbo_payment_type.Value
        Dim strfinCo = cbo_finance_company_fm.SelectedValue
        Session("fm_cd") = strfinCo
        Dim merchant_id As String = ""

        Dim str_display As String

        If Session("emp_cd") & "" = "" Then
            ' Better to prevent payment in emp_cd is null
            Response.Redirect("login.aspx")
        End If


        str_display = sy_brk_process_mc()

        If str_display = "no designated pinpad " Or str_display = "No designated pinpad" Then
            lbl_fm_warning.Text = "no designated pinpad "
            Exit Sub
        End If

        If InStr(str_display, "APPR") = 0 And pmtType = "R" Then
            If isNotEmpty(str_display) And str_display <> "empty" Then
                lbl_fm_warning.Text = str_display
            End If

            btn_save_fm_co.Enabled = True
            btn_save_fm_co.Visible = True
            lbl_fm_warning.Visible = True

            Exit Sub
        End If

        If Left(str_display, 5) = "empty" Then
            btn_save_fm_co.Enabled = True
            btn_save_fm_co.Visible = True
            lit_fm.Visible = False
            Exit Sub
        End If

        'If isNotEmpty(str_display) And str_display <> "empty" Then
        '    lbl_fm_warning.Text = str_display 'sabrina R1999
        'End If


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        ' conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp_fm.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString


        If Is_Finance_DP(lbl_pmt_tp_fm.Text) = True Then
            bc = "FI"
        End If

        txt_fm_exp.Text = Session("lucy_exp_dt")
        Dim str_fm_acct_num As String = txt_fm_acct.Text
        Dim str_enc_acct_num As String = Encrypt(txt_fm_acct.Text, "CrOcOdIlE")
        lbl_fm_tran_id.Text = str_enc_acct_num

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try

            If Session("lucy_bnk_card") Is Nothing Then
                Session("lucy_bnk_card") = ""
            End If
            ' Daniela add exp for refunds
            Dim expDt As String
            If isEmpty(txt_fm_exp.Text) And Session("lucy_exp_dt") IsNot Nothing Then
                expDt = Session("lucy_exp_dt")
            Else : expDt = txt_fm_exp.Text
            End If


            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, BC, MOP_TP, FIN_CO, FI_ACCT_NUM, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :BC, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp_fm.Text
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            objSql.Parameters(":EXP_DT").Value = txt_fm_exp.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":BC").Value = Session("lucy_bnk_card")
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company_fm.SelectedValue
            objSql.Parameters(":FI_ACCT_NUM").Value = Encrypt(txt_fm_acct.Text, "CrOcOdIlE")
            objSql.Parameters(":APPROVAL_CD").Value = txt_fm_appr.Text
            objSql.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            objSql.ExecuteNonQuery()
            objSql.Cancel()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        PopupFinancePmt_FM.ShowOnPageLoad = False

        bindgrid()
        UpdateTotals()

        btn_save_fm_co.Enabled = True
        btn_save_fm_co.Visible = True


    End Sub

    Public Function sy_brk_process_mc() As String

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String = String.Empty
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        strAsCd = cbo_finance_company_fm.SelectedValue

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return Resources.LibResources.Label849 'sabrina-french dec14 "No designated pinpad"
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("lucy_display") = ""
        Dim str_del_doc_num As String = Session("del_doc_num")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If
        Session("del_doc_num") = ""
        Dim str_cust As String = Session("cust_cd")
        str_cust = txt_cust_cd.Text

        v_auth_no = "          "    '10 spaces
        v_mop_cd = "FM"
        v_trans_type = "FI"

        If Len(Trim(lbl_pmt_amt.Text)) = 0 Then
            lbl_pmt_amt.Text = Session("lucy_amt")
        End If
        v_amt = CDbl(lbl_pmt_amt.Text)

        v_private_lbl_finance_tp = strAsCd


        v_card_num = ""
        v_expire_date = ""

        If Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = True Then   'manualy enterred
            'sabrina R1999 Nov20
            If txt_fm_exp.Text & "" = "" Then
                txt_fm_exp.Focus()
                Return "Exp. Date Required"
            End If
            'sabrina R1999 Nov20
            v_card_num = Trim(txt_fm_acct.Text)
            v_expire_date = txt_fm_exp.Text
            str_s_or_m = "M"

            'Session("lucy_bnk_card") = v_card_num
            'Session("lucy_exp_dt") = v_expire_date

        ElseIf Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        If cbo_payment_type.Value IsNot Nothing Then
            If cbo_payment_type.Value = "R" Then
                str_pmt_tp = "R"
                If txt_fm_appr.Text & "" = "" Then
                    txt_fm_appr.Focus()
                    Return Resources.LibResources.Label836 'sabrina-french dec14 "Auth# required"
                Else
                    str_auth = txt_fm_appr.Text
                End If

            Else : str_pmt_tp = "DEP"
            End If
        Else : str_pmt_tp = "PMT"
        End If

        Try

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection

            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"

            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = "8"
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If


            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            v_trans_no = myCMD.Parameters("p_transTp").Value
            'sabrina R1999
            If myCMD.Parameters("p_crn").Value & "" <> "" Then
                l_crn = myCMD.Parameters("p_crn").Value
                txt_fm_acct.Text = Left(l_crn, 16)
            End If

            lbl_fm_warning.Text = myCMD.Parameters("p_dsp").Value

            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value
                lucy_lbl_ind_aprv.ForeColor = Color.Green
                lucy_lbl_ind_aprv.Text = l_dsp

                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_filename = myCMD.Parameters("p_outfile").Value
                    l_printcmd = myCMD.Parameters("p_cmd").Value

                    Session("str_print_cmd") = l_printcmd
                    Session("auth_no") = l_aut
                    Session("lucy_approval_cd") = l_aut
                    Session("lucy_bnk_card") = Left(l_crn, 16)

                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                    print_receipt(l_printcmd)

                    If Len(Trim(txt_fm_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fm_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = False Then 'auto populated account#
                        txt_fm_acct.Text = Left(l_crn, 16)
                    End If
                    txt_fm_appr.Text = l_aut
                    txt_fm_exp.Text = Session("lucy_exp_dt")

                End If

            Else   'transation not done 


                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fm_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fm_acct.Text = ""
                End If
            End If

            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch x
            Dim aa As String = x.Message
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally

            objConnection.Close() ' DANIELA
            objConnection.Dispose()
        End Try

    End Function




    Public Sub sy_addFMcompany()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim v_str As String = ""


        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If

        'Alice added on Mar25, 2019 for ticket 572512 --start
        Dim CashierActStore As String = GetCashierActivatedStore(Session("emp_init"))
        If String.IsNullOrEmpty(CashierActStore) Then
            CashierActStore = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If
        '--end.

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Or LeonsBiz.isLeonFranStr(CashierActStore) = "Y" Then
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FM' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') order by des"

        Else
            If LeonsBiz.isQuebecStore(v_str) = "Y" Then
                sql = "select plan_cd mop_cd, des from brick_dcs_plan "
                sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='FM' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  "
                sql = sql & " order by des"
            Else
                sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FM' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  order by des"
            End If
        End If

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company_fm.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                cbo_finance_company_fm.DataSource = ds
                cbo_finance_company_fm.DataValueField = "mop_cd"
                cbo_finance_company_fm.DataTextField = "des"
                cbo_finance_company_fm.DataBind()
            End If

            cbo_finance_company_fm.Items.Insert(0, "Please Select MC finance plan")
            cbo_finance_company_fm.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub
#End Region

    '<summaryGet dollar types</summary>
    Public Function get_cash_breakdown_dollar_types() As Integer()
        ' Declare dolalr types
        Return (New Integer() {5, 10, 20, 50, 100, 1000})
    End Function

    '<summary>Reset cash breakdown values</summary>
    Public Sub reset_cash_breakdown()
        ' Declare dolalr types
        Dim dollarTypes = get_cash_breakdown_dollar_types()

        ' Hide cash breakdown section
        cashBreakdownSection.Visible = False

        ' Reset coins input values
        txt_cash_breakdown_change.Text = String.Empty
        txt_cash_breakdown_total.Text = String.Empty

        ' Reset coins total value
        txt_cash_breakdown_coins_total.Text = String.Empty

        ' Process all dollar types except 'coins'
        For Each dollarType In dollarTypes
            ' Dollar type id
            Dim dollarTypeTextboxId As String = "txt_cash_breakdown_" & dollarType.ToString

            ' Dollar type total id
            Dim dollarTypeTotalTextboxId As String = dollarTypeTextboxId & "_total"

            ' Dollar type error id
            Dim dollarTypeErrorLabelId As String = dollarTypeTextboxId & "_error"

            ' Find and store dollar type textbox and label controls into variables
            Dim txtboxControl As Control = CashBreakDownPanel.FindControl(dollarTypeTextboxId)
            Dim txtboxTotalControl As Control = CashBreakdownPanel.FindControl(dollarTypeTotalTextboxId)

            ' Process only textbox and label fields
            If TypeOf txtboxControl Is TextBox AndAlso TypeOf txtboxTotalControl Is TextBox Then
                ' Dollar type textbox
                Dim dollarTypeTextbox As TextBox = CType(txtboxControl, TextBox)

                ' Dollar type total textbox
                Dim dollarTypeTotalTextbox As TextBox = CType(txtboxTotalControl, TextBox)

                ' Reset value of each dollar type textbox
                dollarTypeTextbox.Text = String.Empty

                ' Reset value of each dollar type total textbox
                dollarTypeTotalTextbox.Text = String.Empty

                ' Set Total cash value and change to empty
                txt_cash_breakdown_change.Text = String.Empty
                txt_cash_breakdown_total.Text = String.Empty

                ' Hide and cash breakdown change label to default
                cashBreakdownError.Visible = False
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
            End If
        Next
    End Sub

    '<summary>Update cash breakdown</summary>
    Public Sub updated_cash_breakdown(sender As Object, e As EventArgs)

        ' Hide cash breakdown section by default
        cashBreakdownSection.Visible = False

        '' Set default sale order amount value
        Dim saleOrderAmount As Double = 0

        ' Set sale order amount if not empty
        If txt_pmt_amt.Text.isNotEmpty Then
            saleOrderAmount = CDbl(txt_pmt_amt.Text)
        End If

        ' Do not display cash breakdown if sale order amount less than 1
        ' OR method of payment is not "CASH/ARGENT"
        If saleOrderAmount <= 0 OrElse LCase(mop_drp.Value) <> LCase("CS") Then
            Exit Sub
        End If

        ' Display cash breakdown section
        cashBreakdownSection.Visible = True

        ' Declare dollar types
        Dim dollarTypes As Integer() = get_cash_breakdown_dollar_types()

        ' Declare variable to store dollar type value
        Dim parseDollarTypeValue As Integer

        ' Declare variable to store dollar type multiplied value
        Dim parseDollarTypeCalValue As Integer

        ' Declare maximum dollar type digits length
        Dim maxAllowedDigitsLen As Integer = 5

        ' Declare variable to store final total amount
        Dim totalCashAmount As Double = 0

        ' Hide coins error message by default
        cashBreakdownError.Visible = False

        ' Reset border color of coins total textbox
        txt_cash_breakdown_coins_total.Attributes.Add("style", "border-color: inherit")

        ' Process coins value
        If Not String.IsNullOrEmpty(txt_cash_breakdown_coins_total.Text) Then
            ' Declare coins total integer value maximum digits length
            Dim maxCoinsIntegerPartLen As Integer = 4

            ' Declare coins total fraction value maximum digits length
            Dim maxCoinsFractionalPartLen As Integer = 2

            ' Declare coins total parse variable
            Dim coinsTotalValue As Double

            ' Declare variable to store integer and fraction part of coins total
            Dim coinsTotalArr() As String

            ' Declare variable to store integer part of coins total
            Dim coinsTotalIntegerPartValue As Integer = 0

            ' Declare variable to store fraction part of coins total
            ' Declare variale as string type to store exact entered fraction value instead of parse decimal value
            Dim coinsTotalFractionPartValue As String = String.Empty

            ' Declare to store error message
            Dim errorMsg As String = String.Empty

            ' Check if only digits are entered
            If Not (Double.TryParse(txt_cash_breakdown_coins_total.Text, coinsTotalValue)) Then
                errorMsg = "Please enter only digits."

            Else
                ' If comma exists in coins total (french price) then replace all commas with "." to process price later
                If InStr(1, txt_cash_breakdown_coins_total.Text.ToString, ",") > 0 Then
                    coinsTotalValue = CDbl(New StringBuilder(txt_cash_breakdown_coins_total.Text.ToString).
                        Replace(",", ".", 0, txt_cash_breakdown_coins_total.Text.ToString.Length).ToString())
                Else
                    coinsTotalValue = CDbl(txt_cash_breakdown_coins_total.Text)
                End If

                ' Display error message if negative amount entered
                If coinsTotalValue < 0 Then
                    errorMsg = "Negative amount is not allowed."
                End If
            End If

            ' Check if error is not set
            If errorMsg.isEmpty Then
                ' Check if dot(.) exists in coins total amount
                If InStr(1, coinsTotalValue.ToString, ".") Then
                    ' Replace all dots except last one
                    Dim cash_amount_value_parse As String = New StringBuilder(coinsTotalValue.ToString).
                        Replace(".", String.Empty, 0, coinsTotalValue.ToString.LastIndexOf(".")).ToString()

                    ' Split total by comma to get integer and fraction value of coins total
                    coinsTotalArr = Split(cash_amount_value_parse, ".")
                    coinsTotalIntegerPartValue = CInt(coinsTotalArr(0))
                    coinsTotalFractionPartValue = coinsTotalArr(1).ToString

                Else
                    coinsTotalIntegerPartValue = CInt(coinsTotalValue)
                End If

                ' Display error message if entered digits before decimal exceeds maximum allowed digits
                If coinsTotalIntegerPartValue.ToString.Length > maxCoinsIntegerPartLen Then
                    errorMsg = "Exceeds maximum length """ & maxCoinsIntegerPartLen.ToString & """" &
                           " of integer part of total """ & coinsTotalValue.ToString & """."

                ElseIf Not String.IsNullOrEmpty(coinsTotalFractionPartValue) Then
                    ' Display error message if entered digits after decimal exceeds maximum allowed digits
                    If coinsTotalFractionPartValue.Length > maxCoinsFractionalPartLen Then
                        errorMsg = "Exceeds maximum length """ & maxCoinsFractionalPartLen.ToString & """" &
                               " of fraction part of total """ & coinsTotalValue.ToString & """."
                    End If
                End If
            End If

            ' Display error message if exists
            If errorMsg.isNotEmpty Then
                ' Reset cash breakdown change and total amount
                txt_cash_breakdown_change.Text = String.Empty
                txt_cash_breakdown_total.Text = String.Empty

                ' Set error message
                cashBreakdownError.Text = errorMsg
                cashBreakdownError.ForeColor = System.Drawing.Color.Red
                cashBreakdownError.Attributes.Add("style", "display:block")
                cashBreakdownError.Visible = True

                ' Change border color of textbox to red
                txt_cash_breakdown_coins_total.Attributes.Add("style", "border-color: red")
                Exit Sub
            End If

            ' Add coins total to cashbeakdown total amount
            totalCashAmount += coinsTotalValue

        End If

        ' Process all dollar types except 'coins'
        For Each dollarType In dollarTypes
            ' Dollar type id
            Dim dollarTypeTextboxId As String = "txt_cash_breakdown_" & dollarType.ToString

            ' Dollar type total id
            Dim dollarTypeTotalTextboxId As String = dollarTypeTextboxId & "_total"

            ' Find and store dollar type textbox and label controls into variables
            Dim txtboxControl As Control = CashBreakdownPanel.FindControl(dollarTypeTextboxId)
            Dim txtboxTotalControl As Control = CashBreakdownPanel.FindControl(dollarTypeTotalTextboxId)

            ' Process only textbox and label fields
            If TypeOf txtboxControl Is TextBox AndAlso TypeOf txtboxTotalControl Is TextBox Then
                ' Dollar type textbox
                Dim dollarTypeTextbox As TextBox = CType(txtboxControl, TextBox)

                ' Dollar type total textbox
                Dim dollarTypeTotalTextbox As TextBox = CType(txtboxTotalControl, TextBox)

                ' Make dollar type total value empty by default
                dollarTypeTotalTextbox.Text = String.Empty

                ' Reset dollar type textbox border color
                dollarTypeTextbox.Attributes.Add("style", "border-color: inherit")

                If Not String.IsNullOrEmpty(dollarTypeTextbox.Text) Then
                    ' Declare varibale to check if input value can be parsed
                    Dim tryParseVal As Integer = 0

                    ' Declare to store error message
                    Dim errorMsg As String = String.Empty

                    If Not Integer.TryParse(dollarTypeTextbox.Text, tryParseVal) OrElse Not IsNumeric(dollarTypeTextbox.Text) Then
                        ' Display error message if entered value is not numeric
                        errorMsg = "Please enter only digits."

                    ElseIf dollarTypeTextbox.Text.ToString.Length > maxAllowedDigitsLen Then
                        ' Display error message if entered digits exceeds maximum allowed digits
                        errorMsg = "Entered value exceeds the maximum length of """ & maxAllowedDigitsLen & """."

                    ElseIf tryParseVal < 0 Then
                        ' Display error message if entered value is negative
                        errorMsg = "Negative amount is not allowed."
                    End If

                    ' Display error messsage if exists
                    If errorMsg.isNotEmpty Then
                        ' Make cashbreakdown change due value empty
                        txt_cash_breakdown_change.Text = String.Empty

                        ' Make cashbreakdown total value empty
                        txt_cash_breakdown_total.Text = String.Empty

                        ' Make dollar type total value empty
                        dollarTypeTotalTextbox.Text = String.Empty

                        ' Set error message to display
                        cashBreakdownError.Text = errorMsg
                        cashBreakdownError.ForeColor = System.Drawing.Color.Red
                        cashBreakdownError.Visible = True

                        ' Change border color of textbox to red
                        dollarTypeTextbox.Attributes.Add("style", "border-color: red")
                        Exit Sub
                    End If

                    ' Parse dollar type value
                    parseDollarTypeValue = CInt(dollarTypeTextbox.Text)

                    ' Calculate dollar type total value
                    parseDollarTypeCalValue = parseDollarTypeValue * dollarType

                    ' Set total dollar type value
                    dollarTypeTotalTextbox.Text = parseDollarTypeCalValue.ToString()

                    ' Add total dollar value to cashbreakdown total amount
                    totalCashAmount += parseDollarTypeCalValue
                End If
            End If
        Next

        ' Set total of all entered dollar types
        If totalCashAmount > 0 Then
            txt_cash_breakdown_total.Text = FormatNumber(totalCashAmount.ToString, 2)

            ' Check if total cash amount given less than sub total amount
            If totalCashAmount < saleOrderAmount Then
                ' Change label from 'Change Due' to 'Amount Due'
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
                txt_cash_breakdown_change.Text = FormatNumber((saleOrderAmount - totalCashAmount).ToString, 2)
            Else
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1016.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Red
                txt_cash_breakdown_change.Text = FormatNumber((totalCashAmount - saleOrderAmount).ToString, 2)
            End If
        Else
            ' Set Total cash value and change to empty
            txt_cash_breakdown_change.Text = String.Empty
            txt_cash_breakdown_total.Text = String.Empty

            ' Change label to default
            lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
            lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
        End If
    End Sub

#Region "Other Invoice Number popup functions" 'Alice added for request 167
    Protected Sub btn_findCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_findCustomer.Click
        Session("OtherCustCd") = "Y"
        Session("CUST_CD") = ""
        Response.Redirect("~/Customer.aspx")
    End Sub

    Protected Sub txt_ivc_cd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If txt_ivc_cd.Text.Trim.ToUpper = Resources.LibResources.Label373.ToUpper Then
            If cbo_payment_type.Value = "R" Then
                lbl_Error.Text = Resources.LibResources.Label1034
            Else
                txtCustCode.Text = ""
                txtInvoiceNum.Text = ""
                ASPxPopupOtherInvoice.ShowOnPageLoad = True

            If Label7.Text.isNotEmpty Then
                Session("CashhierPwd") = Encrypt(Label7.Text, "CrOcOdIlE")
                End If
            End If
        End If


    End Sub
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        If CustCdValidator.IsValid And InvoiceValidator.IsValid Then
            ASPxPopupOtherInvoice.ShowOnPageLoad = False
            Session("OtherInvoice") = "Y"
            Session("OtherCustCd") = ""
            Session("pa_cust_cd") = ""

            txt_ivc_cd.Text = txtInvoiceNum.Text.Trim().ToUpper

            If Label7.Text.isNotEmpty Then
                If Label5.Text <> "Invalid Password" Then
                    AddCustInfo(txtCustCode.Text.Trim().ToUpper)
                    DisplayMops()
                    GridviewPmts.Enabled = True
                    btn_add.Enabled = True
                    txt_pmt_amt.Enabled = True
                    mop_drp.Enabled = True
                End If


            Else
                If Session("CashhierPwd") & "" <> "" Then
                    Label7.Text = Decrypt(Session("CashhierPwd"), "CrOcOdIlE")
                    GetCashierForOther(Label7.Text)
                Else
                    Label5.Text = "Invalid Password"
                    Label5.ForeColor = Color.Red
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Retrieves the customer info 
    ''' </summary>

    Public Sub AddCustInfo(ByVal p_cust_cd As String)

        Dim sql As String = ""
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connLocal.Open()

        sql = "select c.cust_cd cust_cd, c.fname||' '||c.lname cust_name,c.addr1||' '||c.addr2||' '||c.zip_cd cust_addr, HOME_PHONE, EMAIL_ADDR "
        sql = sql & " from cust c "
        sql = sql & " where c.cust_cd = '" & p_cust_cd & "'"

        cmd = DisposablesManager.BuildOracleCommand(sql, connLocal)
        Try
            'Execute DataReader 
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then

                Label9.Text = reader.Item("cust_name").ToString
                Label6.Text = reader.Item("cust_addr").ToString
                Label10.Text = reader.Item("HOME_PHONE").ToString
                txt_cust_cd.Text = reader.Item("CUST_CD").ToString
                lbl_email.Text = reader.Item("EMAIL_ADDR").ToString

            End If
            reader.Close()

        Catch ex As Exception
            connLocal.Close()

        End Try
        connLocal.Close()

    End Sub


    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        ASPxPopupOtherInvoice.ShowOnPageLoad = False
        txt_ivc_cd.Text = String.Empty
        txt_ivc_cd.Focus()
        Session("OtherCustCd") = ""
        Session("OtherInvoice") = ""
        Session("pa_cust_cd") = ""
        Session("CashhierPwd") = ""
    End Sub

    ''' <summary>
    ''' Method called by the customer code validator to verify if the customer code entered is valid.
    ''' </summary>
    Protected Sub CustCdValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)

        If args.Value.Trim.Length > 0 Then
            Dim custName As String = LeonsBiz.GetCustomerName(args.Value.Trim)
            If (String.IsNullOrEmpty(custName)) Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        Else
            args.IsValid = False
        End If



    End Sub
    Protected Sub InvoiceValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)

        If args.Value.Trim.Length > 0 Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If

    End Sub

    Private Sub GetCashierForOther(ByVal CashierPwd As String)

        Dim SQL As String
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connErp.Open()


        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.APP_PASS='" & UCase(CashierPwd) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='CSH'"
        SQL = UCase(SQL)

        cmd = DisposablesManager.BuildOracleCommand(SQL, connErp)

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            If (reader.Read()) Then
                Label5.ForeColor = Color.Black
                Label5.Text = reader.Item("FNAME").ToString & " " & reader.Item("LNAME").ToString
                lbl_emp_cd.Text = reader.Item("EMP_CD").ToString
                PopulateStoreCode()
                If Request("ip_Store_cd") & "" <> "" Then
                    If Not IsNothing(cbo_store_cd.Items.FindByValue(Request("ip_store_cd"))) Then
                        cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(Request("ip_store_cd"))
                    End If
                Else
                    If Not IsNothing(cbo_store_cd.Items.FindByValue(reader.Item("HOME_STORE_CD").ToString)) Then
                        cbo_store_cd.SelectedItem = cbo_store_cd.Items.FindByValue(reader.Item("HOME_STORE_CD").ToString)
                    End If
                End If
                PopulateCashDrawer()
                cbo_store_cd.Enabled = True
                cbo_csh_drw.Enabled = True
                txt_cust_cd.Enabled = True
                btn_lookup_ivc.Enabled = True
                btnOrderSearch.Enabled = True
                AddCustInfo(txtCustCode.Text.Trim())
                DisplayMops()
                GridviewPmts.Enabled = True
                btn_add.Enabled = True
                txt_pmt_amt.Enabled = True
                mop_drp.Enabled = True

            Else

                Label5.Text = "Invalid Password"
                Label5.ForeColor = Color.Red
                cbo_store_cd.Enabled = False
                cbo_csh_drw.Enabled = False
                txt_cust_cd.Enabled = False
                btn_lookup_ivc.Enabled = False
                btnOrderSearch.Enabled = False

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            connErp.Close()
        End Try
    End Sub

    Private Sub CreateCustomer()

        'sabrina R1922 (new sub)
        If Session("pa_cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("pa_cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("pa_cust_cd") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("pa_cust_cd").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("pa_cust_cd") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub
#End Region

End Class
