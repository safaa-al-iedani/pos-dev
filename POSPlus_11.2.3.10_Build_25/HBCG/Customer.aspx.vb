Imports Microsoft.VisualBasic
Imports System.Data

Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils

Partial Class customer
    Inherits POSBasePage

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim CatchMe As Boolean

        'CustTable.Visible = False
        CatchMe = False
        If txtHomePhone.Text & "" = "" And txt_city.Text & "" = "" And txt_state.Text & "" = "" And txt_zip.Text & "" = "" And txtAddr.Text & "" = "" And txtCustCode.Text & "" = "" And txtFName.Text & "" = "" And txtFName.Text & "" = "" And txtLName.Text & "" = "" And txt_Corp.Text & "" = "" And cbo_cust_tp.Value & "" = "" Then
            CatchMe = True
        End If
        Session("lname") = UCase(txtLName.Text)
        Session("fname") = UCase(txtFName.Text)
        Session("addr") = UCase(txtAddr.Text)
        Session("custcd") = UCase(txtCustCode.Text)
        If cbo_cust_tp.Value & "" <> "" Then
            Session("cust_tp") = UCase(cbo_cust_tp.Value)
        Else
            Session("cust_tp") = ""
        End If
        Session("corp_name") = UCase(txt_Corp.Text)
        Session("bus_phone") = UCase(txtBusPhone.Text)
        Session("home_phone") = UCase(txtHomePhone.Text)
        Session("cust_city") = UCase(txt_city.Text)
        Session("cust_state") = UCase(txt_state.Text)
        Session("cust_zip") = UCase(txt_zip.Text)
        Session("cust_zip_entered") = UCase(txt_zip.Text)

        If CatchMe = True Then
            lbl_errors.Text = "You must enter at least one search criteria!"

        Else
            ' Daniela  - Gift Card customer lookup
            If Request("GIFT") = "TRUE" Then
                Response.Redirect("prs.aspx?LEAD=TRUE&GIFT=TRUE")
                Exit Sub
            End If
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("prs.aspx?LEAD=TRUE")
            Else
                If Request("referrer") & "" <> "" Then
                    Dim Query_string As String
                    Query_string = Request.QueryString.ToString()
                    Response.Redirect("prs.aspx?" & Query_string)
                Else
                    Response.Redirect("prs.aspx")
                End If
            End If
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String

        Dim RoundPanel As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel
        RoundPanel = Master.FindControl("ASPxRoundPanel4")
        If Not IsNothing(RoundPanel) Then
            RoundPanel.DefaultButton = "btnSubmit"
        End If

        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btnSubmit.UniqueID

        If Session("CUST_CD") & "" <> "" Then
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("cust_edit.aspx?LEAD=TRUE&Custid=" + Session("CUST_CD"))
            Else
                Response.Redirect("cust_edit.aspx?Custid=" + Session("CUST_CD"))
            End If
        Else
            Session("TAX_CD") = ""
            Session("TAX_RATE") = ""
            Dim connlocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

            connlocal.Open()

            'jkl
            'sql = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_PCT=0, TAX_AMT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
            With cmdDeleteItems
                .Connection = connlocal
                .CommandText = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_PCT=0, TAX_AMT=0 WHERE SESSION_ID=:SESSIONID AND TAKE_WITH IS NULL"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
            connlocal.Close()
        End If

        If Not IsPostBack And Request("referrer") & "" = "" Then
            calculate_total(Page.Master)
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "Regular.Master"
        ElseIf Request("referrer") & "" <> "" Then
            Page.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

End Class
