Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils
Imports StringUtils

Partial Class prs
    Inherits POSBasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsPostBack = True Then
        GridView1_Binddata()
        'End If
    End Sub


    Private Sub GridView1_Binddata()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet
        Dim LikeQry As String

        GridView1.DataSource = ""
        'CustTable.Visible = False
        CatchMe = False
        If Request("hphone") & "" = "" And Request("bphone") & "" = "" And Request("addr1") & "" = "" _
            And Request("cust_cd") & "" = "" And Request("fname") & "" = "" And Request("lname") & "" = "" _
            And Request("addr2") & "" = "" And Request("city") & "" = "" And Request("state") & "" = "" _
            And Request("zip") & "" = "" And Request("email") & "" = "" And Request("corp_name") & "" = "" Then
            CatchMe = True
        End If
        GridView1.Visible = True
        If CatchMe = False Then
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT * FROM CUST WHERE "

            If Request("hphone") & "" <> "" Then
                If InStr(Request("hphone"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "REPLACE(HOME_PHONE,'-','') " & LikeQry & " '" & Query_String_Prepare(Request("hphone")) & "' "
                proceed = True
            End If
            If Request("bphone") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("bphone"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "REPLACE(BUS_PHONE,'-','') " & LikeQry & " '" & Query_String_Prepare(Request("bphone")) & "' "
                proceed = True
            End If
            If Request("addr1") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("addr1"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ADDR1 " & LikeQry & " '" & Replace(Request("addr1"), "'", "''") & "' "
                proceed = True
            End If
            If Request("addr2") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("addr2"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ADDR2 " & LikeQry & " '" & Replace(Request("addr2"), "'", "''") & "' "
                proceed = True
            End If
            If Request("cust_cd") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("cust_cd"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CUST_CD " & LikeQry & " '" & Request("cust_cd") & "' "
                proceed = True
            End If
            If Request("fname") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("fname"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "FNAME " & LikeQry & " '" & Replace(Request("fname"), "'", "''") & "' "
                proceed = True
            End If
            If Request("corp_name") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("corp_name"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CORP_NAME " & LikeQry & " '" & Replace(Request("corp_name"), "'", "''") & "' "
                proceed = True
            End If
            If Request("lname") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("lname"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "LNAME " & LikeQry & " '" & Replace(Request("lname"), "'", "''") & "' "
                proceed = True
            End If
            If Request("city") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("city"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CITY " & LikeQry & " '" & Replace(Request("city"), "'", "''") & "' "
                proceed = True
            End If
            If Request("state") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("state"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ST_CD " & LikeQry & " '" & Request("state") & "' "
                proceed = True
            End If
            If Request("zip") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("zip"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ZIP_CD " & LikeQry & " '" & Request("zip") & "' "
                proceed = True
            End If
            If Request("email") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("email"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "EMAIL_ADDR " & LikeQry & " '" & Request("email") & "' "
                proceed = True
            End If
            'Daniela restrict number of records
            'If Right(sql, 6) = "WHERE " Then
            'sql = Replace(sql, "WHERE", "")
            'End If
            If Right(sql, 6) = "WHERE " Then
                sql = Replace(sql, "WHERE", "WHERE rownum <= 1000")
            Else : sql = sql & " AND rownum <= 1000 "
            End If
            sql = UCase(sql)

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Dim MyTable As DataTable
            Dim numrows As Integer
            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read) Then
                    If numrows = 1 Then
                        Response.Redirect("CPHQ.aspx?cust_cd=" & MyDataReader.Item("CUST_CD") & "&query_returned=Y")
                    Else
                        GridView1.DataSource = dv
                        GridView1.DataBind()
                    End If
                Else
                    GridView1.Visible = False
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Else
            Label1.Text = "No search criteria were found.  Please, <a href=""CPHQ.aspx"">search again</a>"
            Label1.Visible = True
        End If
        lbl_pageinfo.Text = "Page " + CStr(GridView1.PageIndex + 1) + " of " + CStr(GridView1.PageCount)
        ' make all the buttons visible if page count is more than 0
        If GridView1.PageCount >= 100 Then
            Label1.Text = "More than 1000 customers found.  Please, <a href=""CPHQ.aspx"">search again</a> and use additional search criteria."
            Label1.Visible = True
        End If
        If GridView1.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If GridView1.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView1.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView1.PageIndex = (GridView1.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView1.PageCount = 0 Then
            Label1.Text = "Sorry, no customer records found.  Please search again.<br /><br />"
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView1.PageIndex < (GridView1.PageCount - 1) Then
                    GridView1.PageIndex += 1
                End If
            Case "Prev"
                If GridView1.PageIndex > 0 Then
                    GridView1.PageIndex -= 1
                End If
            Case "Last"
                GridView1.PageIndex = GridView1.PageCount - 1
            Case Else
                GridView1.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub


    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView1.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If Not IsNothing(e.GetValue("CUST_CD").ToString()) Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)
            If Not IsNothing(ASPxMenu2) Then

                ASPxMenu2.RootItem.Items(0).NavigateUrl = "CPHQ.aspx?cust_cd=" & e.GetValue("CUST_CD").ToString() & "&query_returned=Y"
            End If
        End If

    End Sub

    'Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

    '    Dim row As GridViewRow = GridView1.SelectedRow

    '    ' Display the customer name from the selected row.
    '    Response.Redirect("Main_Customer.aspx?cust_cd=" & row.Cells(6).Text & "&query_returned=Y")

    'End Sub

    Protected Sub btn_search_again_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search_again.Click

        Response.Redirect("CPHQ.aspx")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
