<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="ASP_Promo_Admin.aspx.vb" Inherits="ASP_Promo_Admin" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
        <br />
        <asp:DataGrid ID="Gridview1" runat="server" AutoGenerateColumns="False" CellPadding="2"
            Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" Height="144px" Width="673px" HeaderStyle-Height="20px"
            HeaderStyle-HorizontalAlign="Left" OnItemDataBound="PopulateFiles" OnDeleteCommand="DoItemSave">
            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
            <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                Font-Bold="False" Height="20px" HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="as_name" HeaderText="Provider" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="as_cd" HeaderText="" ReadOnly="True" Visible="False">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="promo_cd" HeaderText="Promo Cd" ReadOnly="True" Visible="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="promo_des" HeaderText="Promotion" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="No WDR Format">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                    <ItemTemplate>
                        <asp:DropDownList ID="drpAddendum" runat="server" DataTextField="DES" DataValueField="USER_FORMAT_CD"
                            AutoPostBack="true" CssClass="style5" Width="200px">
                            <asp:ListItem Text="Select An Addendum" Value="" Selected="True" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="WDR Format">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                    <ItemTemplate>
                        <asp:DropDownList ID="drp_wdr_Addendum" runat="server" DataTextField="DES" DataValueField="USER_FORMAT_CD_WDR"
                            AutoPostBack="true" CssClass="style5" Width="200px">
                            <asp:ListItem Text="Select An Addendum" Value="" Selected="True" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="invoice_name" HeaderText="" ReadOnly="True" Visible="False">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="invoice_name_wdr" HeaderText="" ReadOnly="True" Visible="False">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Save24.gif' border='0'&gt;">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" CssClass="style5" />
                </asp:ButtonColumn>
            </Columns>
            <ItemStyle VerticalAlign="Top" />
        </asp:DataGrid>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_msg" runat="server" Text="The following promotions were found and added:<br />"
        Visible="false" CssClass="style5"></asp:Label>
</asp:Content>
