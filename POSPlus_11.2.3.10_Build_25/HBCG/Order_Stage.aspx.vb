Imports System.Data
Imports System.Data.OracleClient
Imports System.Xml
Imports System.Collections.Generic
Imports System.Linq
Imports AppUtils
Imports HBCG_Utils
Imports SD_Utils
Imports OrderUtils
Imports InventoryUtils
Imports NCT_Utils
Imports World_Gift_Utils
Imports AppExtensions
Imports SessionIntializer
Partial Class Order_Stage
    Inherits POSBasePage

    Private thePmtBiz As PaymentBiz = New PaymentBiz()
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()


    Dim x As Boolean
    Dim FAB_FOUND As String
    Dim DELETED_ONCE As Boolean = False
    Dim Insert_Payment As Boolean = True
    Dim partial_amt As Double = 0
    Dim deliverySalePoints As Integer
    Dim actualStops As Integer
    Dim isActualStops As Boolean = False
    Private Const cResponseCriteria As String = "/HandleCode/ReasonCode/CodeDesc"
    Private Const cReturnCriteria As String = "/HandleCode/ReturnCode/CodeDesc"
    Private Const cAVSCriteria As String = "/HandleCode/AVSResponseCode/CodeDesc"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'MM-5693
        Dim ctrlname As String = lbl_ivc_cd.Text
        isAeroplanPromptAllow()        'Oct 20,2016 - mariam - show Aeroplan button
        If Not String.IsNullOrEmpty(ctrlname) Then
            Exit Sub
        Else
            If Not IsPostBack Then

                ' Alice added for request 2197 on Aug 02,2018
                If Session("PO#") = String.Empty And Session("POchecked") = "YES" And Session("custType") = "C" And Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
                    Response.Redirect("Payment.aspx")
                End If

                If ConfigurationManager.AppSettings("picr").ToString = "N" Then
                    chk_picr.Visible = False
                    chk_picr.Checked = False
                Else
                    If Session("ord_tp_cd") = "SAL" Then
                        chk_picr.Visible = True
                        chk_picr.Checked = True
                    Else
                        chk_picr.Visible = False
                        chk_picr.Checked = False
                    End If
                End If
                If Session("Converted_REL_NO") & "" <> "" Then
                    btn_convert_to_rel.Visible = False
                End If
                If Session("cash_carry") = "TRUE" Then
                    btn_convert_to_rel.Visible = False
                End If
                ' Daniela April 21 prevent convert rel if payments not commited 
                Try
                    If Not Session("payment") Is Nothing And Not IsDBNull(Session("payment")) Then
                        If Session("payment") = True Then
                            btn_convert_to_rel.Visible = False
                        End If
                    End If
                Catch
                    'Do nothing
                End Try
                calculate_total(Page.Master)

                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim sql As String
                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim Take_deposit As Double = 0

                If Session("tet_cd") & "" = "" Then
                    Take_deposit = Calculate_Take_With()
                Else
                    Take_deposit = CDbl(Session("Take_Deposit"))
                End If

                SetFinanceInvoiceInfo()
                'sabrina added next line
                sy_printer_drp_Populate() ' Daniela moved up
                btn_commit.Visible = True
                isAeroplanPromptAllow()        'Oct 20,2016 - mariam - show Aeroplan button
                If ConfigurationManager.AppSettings("sig_cap").ToString = "Y" And Session("signature") & "" = "" Then
                    ASPxPopupControl4.ShowOnPageLoad = True
                End If

                ASPxPopupControl5.ContentUrl = "Email_Templates.aspx?ord_tp_cd=SAL&slsp1=" & Session("slsp1") & "&slsp2=" & Session("slsp2") & "&cust_cd=" & Session("cust_cd")
            End If

            DoPreSaveValidation()

            ' May 22 IT Req 2919
            If (Session("email_checked") = "Y") Then
                chk_email.Checked = True
                chk_email.Enabled = True
            ElseIf Not IsPostBack Then
                If Not LeonsBiz.CheckStoreGroup("EER", Session("HOME_STORE_CD")) Then
                    chk_email.Visible = False
                    emailLbl.Visible = False
                Else
                    If (isNotEmpty(email.Text) And (Not LeonsBiz.ValidateEmail(email.Text))) Then
                        lblErrorEmail.Text = Resources.LibResources.Label998
                        lbl_warning.ForeColor = Color.Red
                        lblErrorEmail.Visible = True
                        chk_email.Checked = False
                        chk_email.Enabled = False
                    Else
                        chk_email.Checked = False
                        chk_email.Enabled = True
                    End If
                End If
            End If

            If Terminal_Locked(Request.ServerVariables("REMOTE_ADDR")) = "C" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" And Request("LEAD") <> "TRUE" Then
                btn_commit.Visible = False
                lbl_Status.Text = "<br /><font color=red>This terminal has been closed for the day.  Sales cannot be entered at this station.</font><br /><br />"
                btnPromoCard.Visible = False               'Oct 20,2016 - mariam - show Aeroplan button
            End If
        End If
    End Sub

    ''' <summary>
    ''' Checks the system for finance providers and based on the type('Deposit type/DP') 
    ''' sets the invoice and promo info that will be used later for printing the finance addendum.
    ''' </summary>
    Private Sub SetFinanceInvoiceInfo()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim dataReader As OracleDataReader
        conn.Open()

        Dim DP_String As String = DP_String_Creation()
        If DP_String.isEmpty Then
            '***** Means that there were no finance providers defined in the system that are of type 'DP'
            sql = "SELECT a.FIN_PROMO_CD, a.MOP_CD, b.INVOICE_NAME " &
                    "FROM PAYMENT a, PROMO_ADDENDUMS b " &
                    "WHERE a.MOP_TP='FI' AND a.FIN_CO=b.AS_CD(+) AND a.FIN_PROMO_CD=b.PROMO_CD(+)  " &
                    "AND a.sessionid= :session_id"

        Else
            ' **** means there is at least one finance provider defined in the sytmem as 'DP' 
            sql = "SELECT a.FIN_PROMO_CD, a.MOP_CD, b.INVOICE_NAME " &
                    "FROM PAYMENT a, PROMO_ADDENDUMS b " &
                     "WHERE a.MOP_TP='FI' AND a.MOP_CD NOT IN (" & DP_String & ") AND a.FIN_CO=b.AS_CD(+) AND a.FIN_PROMO_CD=b.PROMO_CD(+) " &
                     "AND a.sessionid= :session_id"
        End If

        cbo_finance_addendums.SelectedIndex = 0
        cbo_finance_addendums.Visible = False
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.Parameters.Add("session_id", OracleType.VarChar).Direction = ParameterDirection.Input
        objSql.Parameters("session_id").Value = Session.SessionID.ToString()

        Try
            dataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If dataReader.Read Then
                '***** Records will only be returned if the finance pmt is not of type 'DP' or there are no 'DP' at all

                If String.IsNullOrEmpty(dataReader.Item("INVOICE_NAME").ToString) Then
                    'means that the finance addendum/invoice ufm was not defined- so retrieve all UFMs from E1
                    If cbo_finance_addendums.Items.Count < 1 Then
                        Populate_Finance_Addendums()
                    End If
                Else
                    'means that an finance addendum/invoice ufm was defined for this provider, so set it's promo code for later use
                    lbl_fin_co.Text = dataReader.Item("FIN_PROMO_CD").ToString
                End If
            End If
            dataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub
    Private Function sy_get_emp_init(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function



    Private Function sy_get_default_printer_id(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Function check_redp_store(ByRef p_store_cd As String, ByRef p_y_n As String) As String
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_rp_interface.isRPPilotStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.Char)).Value = p_store_cd
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try
            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_y_n = myCMD.Parameters("p_y_n").Value
            p_y_n = str_y_n

            objConnection.Close()
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
            str_y_n = "N"
            objConnection.Close()
        End Try
    End Function

    Public Sub lucy_update_del_doc(ByRef p_session_id As String, ByRef p_del_doc_num As String)
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql, sql2 As String

        Try
            dbConnection.Open()

            'lucy add 06-jul-15

            sql = "UPDATE lucy_pos_rf_id set del_doc_num='" & p_del_doc_num & "'  where del_doc_num is null and Session_ID='" & Session.SessionID.ToString.Trim & "'"


            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            dbConnection.Close()
            Throw
        End Try

    End Sub

    Public Sub lucy_insert_redp_rf(ByRef p_session_id As String, ByRef p_row_id As String, ByRef p_del_doc_num As String, ByRef p_soLn_num As Integer)
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_done As String
        Dim p_done As String
        Dim str_session_id As String = p_session_id
        Dim str_del_doc_num As String = p_del_doc_num
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_redp_rf.lucy_insert_redpair_table"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_session_id", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_row_id", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_ln", OracleType.Number).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add(New OracleParameter("p_session_id", OracleType.VarChar)).Value = str_session_id
        myCMD.Parameters.Add(New OracleParameter("p_row_id", OracleType.VarChar)).Value = p_row_id
        myCMD.Parameters.Add(New OracleParameter("p_ln", OracleType.Number)).Value = p_soLn_num
        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = str_del_doc_num
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 50).Direction = ParameterDirection.Output

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            Dim str_err As String
            str_err = myCMD.Parameters("p_err").Value
            If str_err <> "N" Then
                Exit Sub
            End If
        Catch x
            objConnection.Close()
            Throw

        End Try
        objConnection.Close()
    End Sub
    Public Sub update_ln_no(ByRef p_session_id As String, ByRef p_row_id As String, ByRef p_del_doc_num As String, ByRef p_soLn_num As Integer)
        ' lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_done As String
        Dim p_done As String
        Dim str_session_id As String = p_session_id
        Dim str_del_doc_num As String = p_del_doc_num
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_rf.update_ln_no"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_session_id", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_row_id", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_ln", OracleType.Number).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add(New OracleParameter("p_session_id", OracleType.VarChar)).Value = str_session_id
        myCMD.Parameters.Add(New OracleParameter("p_row_id", OracleType.VarChar)).Value = p_row_id
        myCMD.Parameters.Add(New OracleParameter("p_ln", OracleType.Number)).Value = p_soLn_num
        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = str_del_doc_num
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 50).Direction = ParameterDirection.Output

        ' Dim MyDA As New OracleDataAdapter(myCMD)


        Try

            myCMD.ExecuteNonQuery()

            Dim str_err As String
            str_err = myCMD.Parameters("p_err").Value
            If str_err <> "N" Then
                Exit Sub
            End If
        Catch x
            objConnection.Close()
            Throw

        End Try
        objConnection.Close()
    End Sub



    Public Function lucy_check_arc_rf(ByRef p_session_id As String, ByRef p_del_doc_num As String, ByRef p_store_cd As String)
        'lucy
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_done As String
        Dim p_done As String
        Dim str_session_id As String = p_session_id
        Dim str_del_doc_num As String = p_del_doc_num
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_rf.loop_scan_rf"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_done", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
        myCMD.Parameters.Add("p_session_id", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add("p_store_cd", OracleType.VarChar).Direction = ParameterDirection.Input
        myCMD.Parameters.Add(New OracleParameter("p_session_id", OracleType.VarChar)).Value = str_session_id
        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = str_del_doc_num
        myCMD.Parameters.Add(New OracleParameter("p_store_cd", OracleType.VarChar)).Value = p_store_cd


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_done").Value) = False Then
                str_done = myCMD.Parameters("p_done").Value
                '  Return str_done
            End If
        Catch x
            objConnection.Close()
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString


        End Try
        objConnection.Close()

    End Function

    Public Function lucy_change_back_rf_create_dt(ByRef p_session_id As String) As String
        'lucy
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_done As String
        Dim p_done As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_rf.change_create_dt_back"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_session_id", OracleType.VarChar).Direction = ParameterDirection.Input

        myCMD.Parameters.Add("p_done", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_done = myCMD.Parameters("p_done").Value
            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_done

    End Function

    Public Sub print_salesbill_byemail(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByVal p_printer As String, ByVal p_email As String)
        ' IT Req 2919
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        'Dim strMsg As String
        'Dim str_emp_cd As String

        ' DB Oct 31, 2014 Add the customer before Invoice Print
        CreateCustomer()

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.invoice_email_new"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        'sabrina add nextg line 
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer
        myCMD.Parameters.Add(New OracleParameter("p_email", OracleType.Char)).Value = p_email

        'Daniela add french
        Dim cult As String = UICulture
        Dim lang As String = "E" ' default to english
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub


    Public Sub lucy_print_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByVal p_printer As String) 'sabrina added p_printer
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        ' DB Oct 31, 2014 Add the customer before Invoice Print
        CreateCustomer()

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        'lucy_log("from order_stage")
        'lucy_log(p_delDocNum)

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.invoice_print_new"   'sabrina new
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        'sabrina add nextg line 
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer

        'Daniela add french
        Dim cult As String = UICulture
        Dim lang As String = "E" ' default to english
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub
    Public Function lucy_check_no_rf(ByRef p_session_id As String) As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "select rf_id_cd from lucy_pos_rf_id where session_id='" & p_session_id & "' "
        ' sql = sql & "and store_cd = '" & Session("store_cd") & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                Return "HAS"
                Exit Function
            Else
                Return "NO"

            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw
            Return "NO"
        End Try

    End Function
    Public Sub lucy_log(ByVal p_msg As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_pos.prn"
        myCMD.CommandType = CommandType.StoredProcedure
        'strMsg = p_msg & v_count
        strMsg = p_msg
        myCMD.Parameters.Add(New OracleParameter("p_in", OracleType.Char)).Value = strMsg

        myCMD.ExecuteNonQuery()

        v_count = v_count + 1

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try
            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()
            objConnection.Close()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub
    ''' <summary>
    ''' Performs business logic validation before the actual save/commit is allowed for an order.
    ''' </summary>
    Private Sub DoPreSaveValidation()
        Session("SaveSalesOrder") = True

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim custcd As String = ""
        Dim summary_txt As String = ""
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim Status As String = ""
        'MM-6631
        'The variable was initilized to empty string, for certain conditions, this string was not updated to the flag "R" or "Y". based on this, the ARS logic will kick in. therefore, the initial value is set to "Y".
        Dim Status_Proceed As String = "Y"
        Dim sqlb As New StringBuilder
        Dim Take_deposit As Double = 0
        Dim Payment_total As Double = 0.0
        If Session("tet_cd") & "" = "" Then
            Take_deposit = Calculate_Take_With()
        Else
            Take_deposit = CDbl(Session("Take_Deposit"))
        End If

        txt_Follow_up.Text = ""

        If (Not IsDBNull(Session("CUST_CD")) AndAlso Not String.IsNullOrWhiteSpace(Session("CUST_CD"))) Then
            custcd = Session("CUST_CD").ToString
            'If custcd & "" <> "" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & custcd & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                conn.Open()
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    'Daniela
                    'summary_txt = "You have successfully completed the sales module.  Please review the data listed below before committing your order.<br /><br />"
                    summary_txt = Resources.LibResources.Label678 & " " & Resources.LibResources.Label408 & "<br /><br />"
                    summary_txt = summary_txt & MyDataReader.Item("FNAME").ToString & " " & MyDataReader.Item("LNAME").ToString & "<br />"
                    summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("ADDR1").ToString & "<br />"
                    summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("CITY").ToString & " " & MyDataReader.Item("ST").ToString & ", " & MyDataReader.Item("ZIP").ToString & "<br />"
                    summary_txt = summary_txt & Resources.LibResources.Label999 & StringUtils.FormatPhoneNumber(MyDataReader.Item("HPHONE").ToString) & "<br />"  'lucy 23-Jul-18
                    summary_txt = summary_txt & "(C) " & StringUtils.FormatPhoneNumber(MyDataReader.Item("BPHONE").ToString) & "<br />"
                    If Session("zip_cd") & "" = "" Then
                        Session("zip_cd") = MyDataReader.Item("ZIP").ToString
                    End If
                    If String.IsNullOrEmpty(MyDataReader.Item("EMAIL").ToString) And ConfigurationManager.AppSettings("email_prompt").ToString = "Y" And Not IsPostBack Then
                        ASPxPopupControl1.ShowOnPageLoad = True
                    Else
                        summary_txt = summary_txt & MyDataReader.Item("EMAIL").ToString & "<br />"
                        ' May 22 IT REQ 2919
                        email.Text = MyDataReader.Item("EMAIL").ToString
                        'chk_email.Enabled = True
                    End If
                    If String.IsNullOrEmpty(MyDataReader.Item("HPHONE").ToString) And ConfigurationManager.AppSettings("primary_phone").ToString = "Y" Then
                        'Status = Status & "You must enter a primary phone #" & vbCrLf
                        Status = Status & Resources.LibResources.Label820 & vbCrLf
                        Status_Proceed = "R"
                    End If
                    If String.IsNullOrEmpty(MyDataReader.Item("FNAME").ToString) Or String.IsNullOrEmpty(MyDataReader.Item("LNAME").ToString) Then
                        Status = Status & Resources.LibResources.Label821 & vbCrLf
                        Status_Proceed = "R"
                    End If
                    If String.IsNullOrEmpty(MyDataReader.Item("BPHONE").ToString) And ConfigurationManager.AppSettings("secondary_phone").ToString = "Y" Then
                        Status = Status & Resources.LibResources.Label822 & vbCrLf
                        Status_Proceed = "R"
                    End If
                    'Daniela add province validation
                    If String.IsNullOrEmpty(MyDataReader.Item("ST").ToString) Then
                        Status = Status & Resources.LibResources.Label823 & vbCrLf
                        Status_Proceed = "R"
                    End If
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            lbl_Summary.Text = summary_txt


            'Validation on line item selection
            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader
            Dim lineItemsTotalQty As Integer = 0
            Dim itemQty As Integer = 0
            Dim zeroQtyItmCdList As New ArrayList()

            sql = "SELECT ITM_CD, QTY FROM TEMP_ITM WHERE session_id='" & Session.SessionID.ToString() & "'" ' AND ITM_TP_CD='INV'"  

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                conn2.Open()

                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)


                Do While MyDatareader2.Read()
                    itemQty = CInt(MyDatareader2.Item("QTY").ToString())
                    lineItemsTotalQty += itemQty
                    If (itemQty = 0) Then
                        If (Not zeroQtyItmCdList.Contains(MyDatareader2.Item("ITM_CD").ToString())) Then
                            zeroQtyItmCdList.Add(MyDatareader2.Item("ITM_CD").ToString())
                        End If
                    End If
                Loop

                MyDatareader2.Close()

                If lineItemsTotalQty > 0 Then
                    lbl_itm_summary.Text = String.Format(Resources.POSErrors.ERR0025, lineItemsTotalQty)
                    bindgrid()
                Else
                    lbl_itm_summary.Text = Resources.POSErrors.ERR0026
                End If

                If zeroQtyItmCdList.Count > 0 Then
                    Status = Status & String.Format(Resources.POSErrors.ERR0024, String.Join(",", zeroQtyItmCdList.ToArray()))
                    Status_Proceed = "R"
                End If

            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            '*****Validation For Prevent Sale SKU's Start ******'
            If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso Not IsNothing(Session.SessionID.ToString()) Then
                Dim droppedSkuResponse As DroppedSkuResponseDtc = theSkuBiz.GetDroppedItemsForSession(Session.SessionID.ToString())
                If (droppedSkuResponse.prevSaleCount >= 1) Then
                    Dim msg As String = If(droppedSkuResponse.prevSaleCount = 1,
                                           Resources.POSMessages.MSG0018,
                                           Resources.POSMessages.MSG0016)
                    Status = Status & String.Format(msg, droppedSkuResponse.prevSaleSkus) & vbCrLf
                    Status_Proceed = "R"
                End If
            End If
            '*****Validation For Prevent Sale SKU's End   ******'

            ' check take-with count and also CRM with orig count and retail - need all types to do this
            sql = "SELECT ITM_CD, STORE_CD, LOC_CD, TAKE_WITH, qty, ret_prc, max_ret_prc, orig_so_ln_seq, itm_tp_cd, max_crm_qty FROM TEMP_ITM WHERE qty > 0 AND session_id='" & Session.SessionID.ToString() & "'"

            Dim Item_Take_With As Integer = 0
            Dim invalidStores As Integer = 0
            Dim crmWithOrigQty As Integer = 0
            Dim storeCd, locCd, itmCd As String
            Dim errMsg As New StringBuilder
            Dim errMsgLen As Integer = 0
            Dim soLnDatSet As New DataSet

            Try
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                Dim soLnOadp = DisposablesManager.BuildOracleDataAdapter(objsql2)
                soLnOadp.Fill(soLnDatSet)
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)


                Do While MyDatareader2.Read()

                    If MyDatareader2.Item("itm_tp_cd") = "INV" Then ' TODO convert to use inventory flag when implemented

                        storeCd = MyDatareader2.Item("STORE_CD").ToString
                        locCd = MyDatareader2.Item("LOC_CD").ToString
                        itmCd = MyDatareader2.Item("ITM_CD").ToString

                        If (String.IsNullOrEmpty(storeCd)) Then
                            'stores are mandatory for take-withs and cash-carry lines 
                            If MyDatareader2.Item("TAKE_WITH").ToString = "Y" Or Session("cash_carry") = "TRUE" Then
                                Item_Take_With = Item_Take_With + 1
                            End If
                        End If
                        ' only counting return lines that are inventory, shouldn't it be an MCR otherwise? Hum, could be a non-inventory line from a CRM with an inventory line that was split off or ? leave for now
                        If OrderUtils.isCrmWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) AndAlso CDbl(MyDatareader2.Item("Qty").ToString) > 0 Then

                            crmWithOrigQty = crmWithOrigQty + 1
                        End If
                    End If
                    ' confirm credit per line not exceeded and total order
                    If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

                        If Session("ord_tp_cd") = AppConstants.Order.TYPE_CRM Then

                            If CDbl(MyDatareader2.Item("ret_prc").ToString) > CDbl(MyDatareader2.Item("max_ret_prc").ToString) Then

                                If errMsg.ToString.Length > 0 Then
                                    errMsg.Append(vbCrLf)
                                End If
                                errMsg.Append("SKU " + MyDatareader2.Item("itm_cd").ToString + " retail " + MyDatareader2.Item("ret_prc").ToString + " exceeds maximum " + FormatNumber(MyDatareader2.Item("max_ret_prc").ToString, 2))
                            End If
                            If CDbl(MyDatareader2.Item("qty").ToString) > CDbl(MyDatareader2.Item("max_crm_qty").ToString) Then

                                If errMsg.ToString.Length > 0 Then
                                    errMsg.Append(vbCrLf)
                                End If
                                errMsg.Append("SKU " + MyDatareader2.Item("itm_cd").ToString + " quantity " + MyDatareader2.Item("qty").ToString + " exceeds maximum " + FormatNumber(MyDatareader2.Item("max_crm_qty").ToString, 0))
                            End If
                            'MM-7099
                            If OrderUtils.isCrmWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) AndAlso CDbl(MyDatareader2.Item("Qty").ToString) > 0 Then

                                crmWithOrigQty = crmWithOrigQty + 1
                            End If
                        End If
                        ' We are handling MCR retail by ITEM wise(in ORDER_DETAILS.ASPX.VB page) before it was handled by line wise, hence removed the code
                    End If
                Loop
                MyDatareader2.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            If Item_Take_With > 0 Then
                Status = Status & "Take with items must have a store code and location" & vbCrLf
            End If
            If Item_Take_With > 0 Or invalidStores > 0 Then
                Status_Proceed = "R"
            End If
            If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

                If Session("ord_tp_cd") = AppConstants.Order.TYPE_CRM AndAlso crmWithOrigQty < 0.000001 Then
                    Status = Status & "No items have been placed on the order" & vbCrLf
                    Status_Proceed = "R"
                End If

                If errMsg.ToString.Length > 0 Then
                    Status = Status & errMsg.ToString & vbCrLf
                    Status_Proceed = "R"
                End If

                ' do not bother to run extra credit test if already going to fail
                If Not "R".Equals(Status_Proceed) Then

                    errMsg.Clear()
                    ' reconfirm all credits due to credit lines w/o original line 
                    errMsg.Append(OrderUtils.confirmMaxCredit(Session("orig_del_doc_num"), Session("ord_tp_cd"), "ret_prc", soLnDatSet))
                    If errMsg.ToString.Length > 0 Then
                        Status = Status & errMsg.ToString & vbCrLf
                        Status_Proceed = "R"
                    End If
                End If
            End If

            If Session("csh_act_init") = "INVALIDCASHIER" Then
                Status = Status & "There is a problem with your cashier activation (not activated, invalid post date)" & vbCrLf
                Status_Proceed = "R"
            End If

            If Session("del_dt") & "" = "" AndAlso (AppConstants.Order.TYPE_MCR = Session("ord_tp_cd") OrElse
                                                    AppConstants.Order.TYPE_MDB = Session("ord_tp_cd")) Then
                Session("PD") = "P"
                Session("del_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
            End If

            'if the date is NOT set or is set but it is not a valid date, cannot continue
            If Session("del_dt") & "" = "" OrElse Not IsDate(Session("del_dt")) Then
                Status = Status & "Pickup/Delivery date not selected" & vbCrLf
                Status_Proceed = "R"
            End If

            If Session("tet_cd") & "" <> "" Then
                If Session("exempt_id") & "" = "" Then
                    Status = Status & "Sale has been identified as tax exempt, but no tax id was found." & vbCrLf
                    Status_Proceed = "Y"
                End If
            End If
            If Session("cust_cd") & "" = "" Then
                'Status = Status & "Customer has not been selected" & vbCrLf
                Status = Status & Resources.LibResources.Label136 & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("ord_srt") & "" = "" And ConfigurationManager.AppSettings("ord_srt_cd").ToString = "Y" Then
                Status = Status & "You must select a sort code to proceed" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("zip_cd") & "" = "" Then
                Status = Status & "Customer must have a zip code." & vbCrLf
                Status_Proceed = "R"
            End If

            If Not IsNothing(Web.HttpContext.Current.Session("Converted_REL_NO")) AndAlso Not IsNothing(Web.HttpContext.Current.Session("priceChangeDictionary")) Then
                Dim RelanPriceChange As List(Of RelationshipLineDetails) = New List(Of RelationshipLineDetails)
                RelanPriceChange = CType(Web.HttpContext.Current.Session("priceChangeDictionary"), List(Of RelationshipLineDetails))

                Dim priceModifideSku As New StringBuilder
                If Not IsNothing(RelanPriceChange) AndAlso RelanPriceChange.Count > 0 Then
                    For Each line In RelanPriceChange
                        priceModifideSku.Append(line.ItemCD)
                        priceModifideSku.Append(",")
                    Next
                End If
                If priceModifideSku.Length > 0 Then
                    Status = Status & String.Format(Resources.POSMessages.MSG0045, String.Join(",", priceModifideSku.ToString.TrimEnd(",").Split(",").AsEnumerable().Distinct())) & vbCrLf
                End If
            End If

            ds = New DataSet

            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT sum(amt) As Payment FROM payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session.SessionID.ToString.Trim & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If IsNumeric(MyDataReader.Item("Payment").ToString) Then
                        Payment_total = CDbl(MyDataReader.Item("Payment").ToString.Trim)
                    End If
                End If

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            If Session("ord_tp_cd") <> "CRM" Then
                If Session("ord_tp_cd") <> "MCR" Then
                    If Session("payment") & "" <> "" Then
                        If Session("cash_carry") = "TRUE" Then
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & Resources.LibResources.Label59 & " Your balance is " & FormatCurrency(CDbl(Session("Grand_Total")) - CDbl(Payment_total), 2) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("allow_cod").ToString = "N" Then
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & "COD Balances are not allowed.  Your balance is " & FormatCurrency(CDbl(Session("Grand_Total")) - CDbl(Payment_total), 2) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("min_deposit").ToString <> "0" Then
                            If FormatNumber(CDbl(Payment_total), 2) - FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) < 0 Then
                                If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                                    Status = Status & "The minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ".  You need an additional payment of " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) - FormatNumber(CDbl(Payment_total), 2) & "." & vbCrLf
                                    Status_Proceed = "R"
                                Else
                                    Status = Status & Resources.LibResources.Label699 & " " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ". " & Resources.LibResources.Label700 & " " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * CDbl(Session("Grand_Total")) - CDbl(Payment_total)), 2) & "." & vbCrLf
                                    If Status_Proceed <> "R" Then
                                        Status_Proceed = "Y"
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If Session("cash_carry") = "TRUE" Then
                            ds = New DataSet
                            If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                                Status = Status & Resources.LibResources.Label59 & ".  Your balance is " & CDbl(Session("Grand_Total")) - CDbl(Payment_total) & "" & vbCrLf
                                Status_Proceed = "R"
                            End If
                        ElseIf ConfigurationManager.AppSettings("allow_cod").ToString = "N" Then
                            Status = Status & "COD Balances are not allowed.  Your balance is " & CDbl(Session("Grand_Total")) & "" & vbCrLf
                            Status_Proceed = "R"
                        ElseIf ConfigurationManager.AppSettings("min_deposit").ToString <> "0" Then
                            If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                                Status = Status & "The minimum payment on this order is " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & "." & vbCrLf
                                Status_Proceed = "R"
                            Else
                                'Daniela french
                                Status = Status & Resources.LibResources.Label699 & " " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & ".  " & Resources.LibResources.Label700 & " " & FormatNumber(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2) & "." & vbCrLf
                                If Status_Proceed <> "R" Then
                                    Status_Proceed = "Y"
                                End If
                            End If
                        Else
                            Status = Status & "Payment has not been entered" & vbCrLf
                            If Status_Proceed <> "R" Then
                                Status_Proceed = "Y"
                            End If
                        End If
                    End If
                Else
                    If CDbl(Session("Grand_Total")) - CDbl(Payment_total) < 0 Then
                        Status = Status & "Refund payments cannot be greater than the return amount.  Your finance amount has been automatically updated." & vbCrLf
                        sql = "UPDATE PAYMENT SET AMT=" & CDbl(Session("Grand_Total")) & " where SESSIONID='" & Session.SessionID.ToString.Trim & "' AND MOP_TP='FI'"

                        'Set SQL OBJECT 
                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql.ExecuteNonQuery()
                    End If
                End If
            Else
                If CDbl(Session("Grand_Total")) - CDbl(Payment_total) < 0 Then
                    Status = Status & "Refund payments cannot be greater than the return amount.  Your finance amount has been automatically updated." & vbCrLf
                    sql = "UPDATE PAYMENT SET AMT=" & CDbl(Session("Grand_Total")) & " where SESSIONID='" & Session.SessionID.ToString.Trim & "' AND MOP_TP='FI'"

                    'Set SQL OBJECT 
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()
                End If
            End If

            If Session("ord_tp_cd") = AppConstants.Order.TYPE_MCR Then
                'grandTotal of the current MCR
                Dim grandTotal = IIf(IsNothing(Session("grand_total")), 0, Session("grand_total"))
                'otherSaleCharges - includes delivery, setup charges
                Dim otherSaleCharges As Decimal = OrderUtils.GetMaxOtherChargesforMCR(Session("cust_cd"), Session("orig_del_doc_num"))
                'availableCredit - total credit available for this invoice
                Dim availableCredit As Decimal = OrderUtils.GetMaxAvailableCreditforMCR(Session("cust_cd"), Session("orig_del_doc_num"))
                'MM-5693
                If otherSaleCharges > 0 AndAlso availableCredit > 0 Then
                    If (grandTotal > (otherSaleCharges + availableCredit)) Then
                        Status = Status & String.Format(Resources.POSMessages.MSG0047, FormatCurrency(otherSaleCharges + availableCredit))
                        Status_Proceed = "R"
                    End If
                End If
            End If

            If Session("itemid") & "" = "" Then
                If Session("ord_tp_cd") <> "MCR" And Session("ord_tp_cd") <> "MDB" Then
                    Status = Status & "No items have been placed on the order" & vbCrLf
                    Status_Proceed = "R"
                End If
            End If
            'MM-9227
            If Session("ord_tp_cd") <> AppConstants.Order.TYPE_SAL AndAlso Session("crm_cause_cd") & "" = "" Then
                Status = Status & Resources.POSMessages.MSG0055 & vbCrLf
                Status_Proceed = "R"
            End If

            If Session("ord_tp_cd") <> AppConstants.Order.TYPE_SAL AndAlso Session("adj_ivc_cd") & "" = "" Then
                Status = Status & "No Original Invoice have been entered" & vbCrLf
                Status_Proceed = "R"
            End If

            If Session("comments") & "" = "" Then
                Status = Status & "No comments have been entered" & vbCrLf
                If Status_Proceed <> "R" Then
                    Status_Proceed = "Y"
                End If
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "Y" Then
                If Session("mark_cd") & "" = "" Then
                    Status = Status & "No marketing code has been selected" & vbCrLf
                    If Status_Proceed <> "R" Then
                        Status_Proceed = "Y"
                    End If
                End If
            End If
            If Session("csh_dwr_cd") & "" = "" Then
                Status = Status & "Cash Drawer Code has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("tran_dt") & "" = "" Then
                Status = Status & "Transaction date has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("store_cd") & "" = "" Then
                Status = Status & "Your store code has not been set" & vbCrLf
                Status_Proceed = "R"
            End If

            'Alice updated on June 10, 2019 for request 81
            If Session("slsp1").ToString = "" Then
                Status = Status & "Salesperson 1 must be selected to proceed" & vbCrLf
                Status_Proceed = "R"
            Else
                If SalesUtils.Validate_Slsp(Session("slsp1")) = False Then
                    Status = Status & "Salesperson 1 is invalid" & vbCrLf
                    Status_Proceed = "R"
                End If
            End If
            If Session("PD") & "" = "" Then
                Status = Status & "Pickup/Delivery flag has not been set" & vbCrLf
                Status_Proceed = "R"
            End If
            If Session("tax_cd") & "" = "" And Session("tet_cd") & "" = "" Then
                Status = Status & "You must select a tax code or tax exempt status to continue" & vbCrLf
                Status_Proceed = "R"
            End If
            conn.Close()
        Else
            'Status = Status & "Customer has not been selected" & vbCrLf
            Status = Status & Resources.LibResources.Label136 & vbCrLf
            Status_Proceed = "R"
        End If

        ' if processing a Delivery sale, then Checks Delivery Availability
        If Session("PD") = AppConstants.DELIVERY Then
            ' finds out the delivery points for the order being processed
            deliverySalePoints = theTMBiz.GetDeliveryPoints(Session.SessionID.ToString.Trim)
            Dim deliveryPointDetails As DeliveryPointDetails
            deliveryPointDetails = theTMBiz.GetDeliveryPointsDetails(Session("ZONE_CD") & "", Session("del_dt") & "")
            If Not IsNothing(deliveryPointDetails) Then
                actualStops = deliveryPointDetails.ActualStops
                isActualStops = True
                If SysPms.isDelvByPoints And SysPms.pointOverScheduling = "W" Then
                    Dim remainingPoints As Integer = deliveryPointDetails.RemainingStops
                    Dim exmpCD As String = deliveryPointDetails.ExmptDelCd
                    Dim extraPoints As Integer = remainingPoints - deliverySalePoints
                    If CDbl(FormatNumber(remainingPoints)) <= 0 And exmpCD = "&nbsp;" Then
                        Status = Status & "Delivery may not be scheduled" & vbCrLf
                    ElseIf CDbl(FormatNumber(remainingPoints)) <= 0 And exmpCD <> "&nbsp;" Then
                        Status = Status & "Point availability has been exceeded by  " & extraPoints.ToString().Replace("-", "") & "  for zone " & HttpContext.Current.Session("ZONE_CD") & vbCrLf
                    ElseIf CDbl(FormatNumber(remainingPoints)) > 0 And CDbl(FormatNumber(remainingPoints)) < CDbl(FormatNumber(deliverySalePoints)) Then
                        Status = Status & "Point availability has been exceeded by " & extraPoints.ToString().Replace("-", "") & "  for zone " & HttpContext.Current.Session("ZONE_CD") & vbCrLf
                    End If
                End If
            End If
        End If

        'Check if there is a zone in case it was celared out. A zone will be defaulted if only one found otherwise a msg will be displayed
        Dim zoneCd As String = GetZoneCode()
        If zoneCd.isEmpty Then
            If theInvBiz.RequireZoneCd(Session("PD"), Session("STORE_ENABLE_ARS"), Session("ORD_TP_CD")) Then
                Status = Status & Resources.POSMessages.MSG0034 & vbCrLf
                Status_Proceed = "R"
            End If
        Else
            'if it is a pickup, then set it back into the session--TODO:AXK -not sure if it applies to deliveries also -confirm with DSA
            If Session("PD") = AppConstants.PICKUP Then
                SessVar.zoneCd = zoneCd
            End If
        End If

        '****** check for ARS validation
        If Status_Proceed = "Y" Then
            If (Session("STORE_ENABLE_ARS") AndAlso
                AppConstants.Order.TYPE_SAL = Session("ORD_TP_CD") AndAlso
                (IsNothing(Session("cash_carry")) OrElse Session("cash_carry") = "FALSE")) Then

                Dim sessId As String = Session.SessionID.ToString.Trim
                Dim maxAvailDt As Date = theSalesBiz.GetMaxAvailDate(sessId)
                Dim pkpDelDt As Date = Session("DEL_DT")
                Dim preventedItems As List(Of String) = theSalesBiz.GetPreventedItemsForARS(sessId)
                'Dim paidInFull As Boolean = False
                Dim paidInFull As Boolean = IsSalePaidInFull(sessId, Payment_total, CDbl(Session("Grand_Total")))
                'If CDbl(Session("Grand_Total")) - CDbl(Payment_total) > 0 Then
                '    paidInFull = False
                'Else
                '    paidInFull = True
                'End If

                'reserves or unreserves inventory according to the rules for ARS
                Try
                    ProcessARSReservations(paidInFull, preventedItems)
                Catch ex As Exception

                End Try


                Dim arsRequest As New ArsValidationRequestDtc
                arsRequest.maxAvailDate = maxAvailDt
                arsRequest.droppedItemsFlaggedPreventPO = preventedItems
                arsRequest.checkInvReq.isPaidInFull = paidInFull
                arsRequest.checkInvReq.zoneCd = SessVar.zoneCd
                arsRequest.checkInvReq.puDel = Session("PD").ToString
                arsRequest.checkInvReq.puDelDt = pkpDelDt
                arsRequest.checkInvReq.puDelStoreCd = Session("PD_STORE_CD").ToString

                Dim arsResponse As ArsValidationResponseDtc = theSalesBiz.ValidateARSInventory(sessId, arsRequest)
                If (arsResponse.lnsToReserve.Count > 0) Then ReserveInventory(arsResponse.lnsToReserve) 'IF reservations pending, completes them
                If (Not arsResponse.isValid) Then
                    Status = arsResponse.message
                    Status_Proceed = "R"
                End If
            End If
        End If

        txt_Follow_up.Text = txt_Follow_up.Text & Status
        If Status_Proceed = "Y" Then
            If txt_Follow_up.Text.Trim.isEmpty Then
                lbl_Status.Text = ""
                btn_commit.Visible = True
                isAeroplanPromptAllow()        'Oct 20,2016 - mariam - show Aeroplan button
            Else
                lbl_Status.Text = Resources.LibResources.Label602 & " " & Resources.LibResources.Label407
                btn_commit.Visible = True
                isAeroplanPromptAllow()        'Oct 20,2016 - mariam - show Aeroplan button
            End If
        ElseIf Status_Proceed = "R" Then
            lbl_Status.Text = Resources.LibResources.Label602 & " " & Resources.LibResources.Label406
            btn_commit.Visible = False
            btnPromoCard.Visible = False     'Oct 20,2016 - mariam - show Aeroplan button
        End If

    End Sub

    ''' <summary>
    ''' Finds out if the current order is paid in full.  If order is not 
    ''' then it checks if the user has the security to override it.
    ''' >>>>>>>>>>>>>>>  Intended only for SALE OPEN orders <<<<<<<<<<<<<<<<
    ''' </summary>
    ''' <param name="sessId">the sessionID in use</param>
    ''' <returns>TRUE if order can be considered paid in full; FALSE otherwise</returns>
    Private Function IsSalePaidInFull(ByVal sessId As String, ByVal paidAmount As Double, ByVal grandTotal As Double) As Boolean

        ' retrieves any finance amounts that exist for this order
        Dim finAmount As Double = 0.0
        Dim finCompany As String = String.Empty
        Dim finApprovalCd As String = String.Empty
        Try
            Dim financePmts As DataTable = thePmtBiz.GetNonDPFinanceDetails(sessId)
            If financePmts.Rows.Count > 0 Then
                'it should be one row, since only one finance is allowed at this point
                finAmount = financePmts.Rows(0).Item("amt")
                finCompany = financePmts.Rows(0).Item("fin_co")
                finApprovalCd = financePmts.Rows(0).Item("approval_cd")
            End If
        Catch ex As Exception
            'assumes NO finance amounts
            finAmount = 0.0
            finCompany = String.Empty
            finApprovalCd = String.Empty
        End Try

        Dim rtnVal As Boolean = False
        Dim pifReq As New PaidInFullReq
        pifReq.ordTpCd = Session("ORD_TP_CD")
        pifReq.custCd = Session("CUST_CD")
        pifReq.delDocNum = sessId
        pifReq.writtenStoreCd = Session("STORE_CD")
        pifReq.finCoCd = finCompany
        pifReq.finApp = finApprovalCd
        pifReq.origFinAmt = finAmount
        pifReq.grandTotal = grandTotal
        pifReq.paidAmount = paidAmount
        pifReq.requestFromSOE = True
        'MM-8687
        Dim paidInFull As Boolean = theSalesBiz.IsPaidInFull(pifReq)
        Return paidInFull

    End Function

    Public Sub Populate_Finance_Addendums()

        'cbo_finance_addendums.Visible = True
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT USER_FORMAT_CD, DES FROM USER_FORMAT WHERE ORIGIN_CD='IPRT' ORDER BY DES"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_finance_addendums
                .DataSource = ds
                .DataValueField = "USER_FORMAT_CD"
                .DataTextField = "DES"
                .DataBind()
            End With

            cbo_finance_addendums.Items.Insert(0, "Please select a finance contract")
            cbo_finance_addendums.Items.FindByText("Please select a finance contract").Value = ""
            If cbo_finance_addendums.SelectedValue & "" = "" Then
                cbo_finance_addendums.SelectedIndex = 0
            End If

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Checks that if there is no zone code, gets it based on various rules and conditions.
    ''' 1) if cash and carry and need a zone code, then just get the first avail, on the written store
    ''' 2) otherwise, if zip code available and only one zone related to it, take that zone
    ''' 3) otherwise, if no zip or multiple zones, then have to return  to select new zip and/or zone
    ''' </summary>
    Private Function GetZoneCode() As String

        Dim rtnVal As String = String.Empty
        If SessVar.zoneCd.isEmpty Then

            If theInvBiz.RequireZoneCd(SessVar.puDel, SessVar.isArsEnabled, SessVar.ordTpCd) Then

                If Session("cash_carry") = "TRUE" Then
                    ' got to use the written store and no matter what the sessvar.pudel is, this is a pickup
                    rtnVal = theInvBiz.GetPriFillZoneCode(AppConstants.PICKUP, "", "", "", SessVar.wrStoreCd)
                ElseIf SessVar.zipCode4Zone.isNotEmpty Then
                    Dim zoneResp As ZoneFromZipResp = theTMBiz.GetZoneCd4Zip(SessVar.zipCode4Zone)
                    If zoneResp.numRows = 1 And zoneResp.zoneCd.isNotEmpty Then
                        rtnVal = zoneResp.zoneCd
                    End If
                End If
            End If
        Else
            rtnVal = SessVar.zoneCd
        End If
        Return rtnVal

    End Function


    Protected Sub btn_commit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_commit.Click
        Dim str_store_cd = Session("store_cd") 'lucy
        Dim p_printer As String = "" 'sabrina

        btn_convert_to_rel.Enabled = False

        CreateCustomer()

        Dim tax_chg As Double
        If String.IsNullOrEmpty(Session("tet_cd")) Then
            tax_chg = 0
        End If

        If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
        Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("store_cd").ToString, Session("tran_dt"))

        If Not (soKeyInfo Is Nothing OrElse soKeyInfo.soDocNum Is Nothing OrElse soKeyInfo.soDocNum.delDocNum Is Nothing) Then
            If Len(soKeyInfo.soDocNum.delDocNum) <> 11 Then
                lbl_Status.Text = "<font color=red>Could not create order, please try again</font>"
                Exit Sub
            End If
        End If

        'sabrina add next if
        'If IsDBNull(Session("printer_drp_id")) = True Or printer_drp.SelectedIndex = 0 Then
        '    lbl_Status.Text = "<font color=red>Please select a printer to proceed </font>" 'sabrina testing & Session("printer_drp_id") & " - idx = " & printer_drp.SelectedIndex
        '    Exit Sub
        'Else
        '    ' p_printer = printer_drp.SelectedItem.ToString()
        '    p_printer = Session("printer_drp_id")
        'End If

        'sabrina add next if
        If printer_drp.SelectedIndex = 0 Then
            p_printer = Session("default_prt")
        Else
            p_printer = Session("printer_drp_id")
        End If

        If IsDBNull(p_printer) = True Or p_printer Is Nothing Or p_printer = "Printers:" Then
            lbl_Status.Text = "<font color=red>Please select a printer to proceed </font>"
            Exit Sub
        End If

        btn_commit.Enabled = False
        ImageButton1.Enabled = False
        chk_email.Enabled = False
        chk_picr.Enabled = False
        btnPromoCard.Enabled = False    'Nov 1,2016 - mariam - Aeroplan promo

        SaveOrder(tax_chg, soKeyInfo, p_printer) 'sabrina add p_printer

        'lucy add the following


        Dim str_y_n As String

        check_redp_store(str_store_cd, str_y_n)


        Dim str_no As String = lucy_check_no_rf(Session.SessionID.ToString.Trim)  'lucy
        ' If Session("cash_carry") = "TRUE" And str_no = "HAS" Then     'HAS means has take with
        If str_no = "HAS" And str_y_n = "N" Then 'non redp
            lucy_update_del_doc(Session.SessionID.ToString.Trim, soKeyInfo.soDocNum.delDocNum)

            lucy_check_arc_rf(Session.SessionID.ToString.Trim, soKeyInfo.soDocNum.delDocNum, str_store_cd)  'lucy
        End If

        ' Daniela April 21
        btn_convert_to_rel.Visible = True
    End Sub


    Public Function Get_Sequence()

        Get_Sequence = ""

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        conn2.Open()

        sql = "SELECT CURR_SEQ FROM DOC_SEQ ORDER BY LAST_CHANGE DESC"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDatareader2.Read() Then
                Get_Sequence = Get_Next_Val(MyDatareader2.Item("CURR_SEQ").ToString.Trim)
                sql = "UPDATE DOC_SEQ SET CURR_SEQ='" & Get_Sequence & "',LAST_CHANGE=TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR')"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                objsql2.ExecuteNonQuery()
            End If

            'Close Connection 
            MyDatareader2.Close()
            conn2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

    End Function

    Public Function REL_NO()

        Dim DateString As String
        Dim GOOD_REL As Boolean
        Dim CHAR_SEQ As String = ""

        GOOD_REL = False
        REL_NO = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim x As Integer
        Dim sql As String
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn2.Open()
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If
        'Open Connection 
        conn.Open()
        x = 0
        Do While x < 3
            CHAR_SEQ = Get_Sequence()
            If CHAR_SEQ & "" <> "" And Len(CHAR_SEQ) = 4 Then
                Exit Do
            End If
            x = x + 1
        Loop

        If Len(CHAR_SEQ) <> 4 Then
            Return ""
            Exit Function
        End If

        Do While GOOD_REL = False
            x = x + 1
            DateString = XDigits(Today.Month.ToString, 2) & XDigits(Today.Day.ToString, 2) & Right(Today.Year, 1)
            REL_NO = DateString & Session("store_cd").ToString & CHAR_SEQ

            sql = "SELECT DEL_DOC_NUM FROM SO WHERE DEL_DOC_NUM='" & REL_NO & "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If (MyDatareader.Read()) Then
                    GOOD_REL = False
                    CHAR_SEQ = Get_Sequence()
                Else
                    GOOD_REL = True
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop

        conn.Close()
        conn2.Close()

        Return REL_NO

    End Function

    Public Function Get_First_Fab()

        Get_First_Fab = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        sql = "SELECT ROW_ID FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND ITM_TP_CD='FAB' ORDER BY ROW_ID"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDatareader2.Read() Then
                Get_First_Fab = MyDatareader2.Item("ROW_ID").ToString
            End If
            MyDatareader2.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Function Get_Temp_ITM_CD(ByVal row_id As String)

        Get_Temp_ITM_CD = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        sql = "SELECT ITM_CD FROM TEMP_ITM WHERE ROW_ID=" & row_id

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If MyDatareader2.Read() Then
                Get_Temp_ITM_CD = MyDatareader2.Item("ITM_CD").ToString
            End If
            MyDatareader2.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Sub SaveOrder(ByVal tax_chg As Double, ByVal soKeys As OrderUtils.SalesOrderKeys, ByVal p_printer As String) 'sabrina added p_printer


        'Dim dtSplitData As DataTable = DirectCast(Session("PKGSPLITDATA"), DataTable)
        Session("err") = ""   ' need to clear this or remains from previous

        ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
        ' a single transaction to be able to rollback all changes if something fails
        Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql, sql2 As String
        Dim lngHowLong As Long = DateDiff("s", FormatDateTime(Today.Date, 2) & " 00:00:00", Now)

        Dim doc_processing As Integer = ProcessingType.SingleOrFirstDoc ' num of docs to create, used when processing to determine which; if 2, processing TW doc of a split; if 1, may be cash and carry or pu/del
        Dim soLn_num As Integer = 1       ' set to 1 for each document and incremented for each SO_LN for the del_doc_ln# assignment
        Dim del_doc_num As String = soKeys.soDocNum.delDocNum
        Dim err_msg As String = ""
        Dim isSplitTw As Boolean = False
        Dim split_order As Boolean = False
        Dim split_amt As Double = 0
        Dim ttax As String = "", ttax_cd As String = ""
        Dim newtotal As Double = 0
        Dim default_invoice As String = ""


        ' To make these double, either need to add 'as Double' for each, initialize with decimals or do not initialize in the same statement, 
        '    otherwise only the last entry ends up as double
        '  Dim del_chg As Double = 0.0, setup_chg As Double = 0.0, del_tax = 0.0, setup_tax As Double = 0
        '  Dim sub_total = 0.0, delivery = 0.0, discount = 0.0, tax = 0.0, grand_total As Double = 0

        'lucy
        Dim str_exp_dt As String
        Dim del_chg As Double = 0
        Dim setup_chg As Double = 0
        Dim del_tax As Double = 0
        Dim setup_tax As Double = 0
        '  Dim del_chg = 0, setup_chg = 0, del_tax = 0, setup_tax As Double = 0
        Dim sub_total As Double = 0
        Dim delivery As Double = 0
        Dim discount As Double = 0
        Dim grand_total As Double = 0
        Dim tax As Double = 0  'lucy

        Dim finAmt As Double = 0
        Dim finCo As String = ""
        Dim finPromoCd As String = ""
        Dim finAcctNum As String = ""
        Dim approvalCd As String = ""
        Dim wdr As Boolean = False
        Dim wdr_promo As String = ""
        Dim dpString As String = ""

        Dim slsp1 As String = ""
        Dim slsp2 As String = ""

        Dim pct_1 As Double = 0
        Dim pct_2 As Double = 0

        Dim UpPanel As UpdatePanel = Master.FindControl("UpdatePanel1")

        'Dim slsp1 As String = Session("slsp1")
        'Dim slsp2 As String = Session("slsp2")

        'Alice updated on May 13, 2019 for reqeust 81
        dbConnection.Open()

        If Session("str_co_grp_cd").ToString = "BRK" Then
            Dim del_doc_tab As DataTable = OrderUtils.GetOrderDetails(Session("adj_ivc_cd"))
            If Not AppConstants.Order.TYPE_SAL.Equals(Session("ord_tp_cd")) Then
                If del_doc_tab.Rows.Count = 0 Then
                    'Dim OriginalSales As String = Session("slsp1")
                    ' Session("slsp1") = "HOU" & Session("store_cd").ToString.Trim
                    Dim InvalidMessage As String = "Original Invoice " & Session("adj_ivc_cd") & " was not validated. Entered by " & Session("emp_cd")
                    dbCommand = DisposablesManager.BuildOracleCommand(dbConnection)
                    theSalesBiz.SaveInvalidInvoiceComments(dbCommand, soKeys, del_doc_num, InvalidMessage, Session("STORE_CD"), Session("tran_dt"))

                End If
            End If
        End If



            'Alice update on Feb 22, 2019 for request 4477, put back the original Slsp on the CRM or MCR or MDB order for finished exchanged order 
            If Session("Orig_slsp1_forExchangeSo") IsNot Nothing Then
                If Session("Orig_slsp1_forExchangeSo").ToString <> "" Then
                    slsp1 = Session("Orig_slsp1_forExchangeSo")
                Else
                    slsp1 = Session("slsp1")
                End If
            Else
                slsp1 = Session("slsp1")
            End If

        If Session("Orig_slsp2_forExchangeSo") IsNot Nothing Then
            If Session("Orig_slsp2_forExchangeSo").ToString <> "" Then
                slsp2 = Session("Orig_slsp2_forExchangeSo")
            Else
                slsp2 = Session("slsp2")
            End If
        Else
            slsp2 = Session("slsp2")
        End If


        If Session("Orig_pct1_forExchangeSo") IsNot Nothing Then
            If Not IsDBNull(Session("Orig_pct1_forExchangeSo")) Then
                pct_1 = Session("Orig_pct1_forExchangeSo")
            Else
                pct_1 = IIf(Session("pct_1").ToString = "", 0, Session("pct_1"))
            End If
        Else
            pct_1 = IIf(Session("pct_1").ToString = "", 0, Session("pct_1"))
        End If

        If Session("Orig_pct2_forExchangeSo") IsNot Nothing Then
            If Not IsDBNull(Session("Orig_pct2_forExchangeSo")) Then
                pct_2 = Session("Orig_pct2_forExchangeSo")
            Else
                pct_2 = IIf(Session("pct_2").ToString = "", 0, Session("pct_2"))
            End If
        Else
            pct_2 = IIf(Session("pct_2").ToString = "", 0, Session("pct_2"))
        End If


        '' Daniela fix Take with orders
        'Dim pct_2 As Double
        'If Session("pct_2") IsNot Nothing Then
        '    If Session("pct_2").ToString <> "" Then
        '        pct_2 = Session("pct_2")
        '    End If
        'End If ' End Daniela 

        Dim tet_cd As String = Session("tet_cd")
        Dim STORE_ENABLE_ARS As Boolean = Session("STORE_ENABLE_ARS")
        Dim ZONE_CD As String = Session("ZONE_CD")
        Dim PD As String = Session("PD")

        Dim pd_store_cd As String = Session("pd_store_cd")


        Dim cash_carry As Boolean = Session("cash_carry")
        Dim Order_Type As String = Session("ord_tp_cd")
        Try
            'dbConnection.Open()
            If Session("cash_carry") = "TRUE" Then  'lucy
                sql = "UPDATE TEMP_ITM SET TAKE_WITH='Y' where Session_ID='" & Session.SessionID.ToString.Trim & "'"
            Else
                sql = "UPDATE TEMP_ITM SET TAKE_WITH='N' WHERE TAKE_WITH IS NULL AND Session_ID='" & Session.SessionID.ToString.Trim & "'"
            End If

            ' ---- this update doesn't have to be part of the single transaction operation
            ' sql = "UPDATE TEMP_ITM SET TAKE_WITH='N' WHERE TAKE_WITH IS NULL AND Session_ID='" & Session.SessionID.ToString.Trim & "'"   'lucy commented
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.ExecuteNonQuery()

            'lucy add 06-jul-15
            If Session("cash_carry") = "TRUE" Then  'lucy
                sql = "UPDATE TEMP_ITM SET out_id=(select NAS_OUT_ID from lucy_pos_rf_id where row_id=temp_itm.row_id and NAS_OUT_ID is not null) where Session_ID='" & Session.SessionID.ToString.Trim & "'"

            End If

            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.ExecuteNonQuery()
            ' ---- determines if the order needs to be split
            sql = "SELECT sum(qty*ret_prc) as ord_sum, sum(qty*tax_amt) as tax_sum, tax_cd FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH='Y' group by tax_cd"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                If IsNumeric(dbReader.Item("ord_sum").ToString) Then
                    ' if the order is entirely take-with 
                    If CDbl(dbReader.Item("ord_sum").ToString) = CDbl(Session("sub_total")) Or Session("cash_carry") = "TRUE" Then  'lucy add  Session("cash_carry") = "TRUE" ,21-apr-15 to prevent split 
                        ' If CDbl(dbReader.Item("ord_sum").ToString) = CDbl(Session("sub_total")) Then
                        Session("PD") = "P"
                        Session("del_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
                        Session("zone_cd") = System.DBNull.Value
                        Session("del_chg") = System.DBNull.Value
                        Session("cash_carry") = "TRUE"
                    Else
                        doc_processing = doc_processing + 1 ' at this time, only two
                        split_order = True
                        ttax = dbReader.Item("tax_sum").ToString
                        ttax_cd = dbReader.Item("tax_cd").ToString
                        newtotal = Session("sub_total")
                        Session("sub_total") = dbReader.Item("ord_sum").ToString
                    End If
                End If
            End If

            ' ---- Gathers Finance details
            'lucy add exp_dt
            sql = "SELECT AMT AS FIN_AMT, FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD,exp_dt FROM PAYMENT WHERE MOP_TP='FI' AND SessionID='" & Session.SessionID.ToString.Trim & "'"
            dpString = DP_String_Creation()
            If dpString & "" <> "" Then sql = sql & " AND MOP_CD NOT IN (" & dpString & ")"

            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                ' There will only be values/records found if this is a finance payment and either:
                ' 1) there were no 'DP' type finance providers setup in the system OR 
                ' 2) that there are some defined in the sytmem as 'DP' but the one used on this finance payment is not 'DP'
                If Not String.IsNullOrEmpty(dbReader.Item("FIN_CO").ToString) Then finCo = dbReader.Item("FIN_CO").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("APPROVAL_CD").ToString) Then approvalCd = dbReader.Item("APPROVAL_CD").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("FIN_PROMO_CD").ToString) Then finPromoCd = dbReader.Item("FIN_PROMO_CD").ToString
                If Not String.IsNullOrEmpty(dbReader.Item("FI_ACCT_NUM").ToString) Then finAcctNum = Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                If IsNumeric(dbReader.Item("FIN_AMT").ToString.Trim) Then finAmt = CDbl(dbReader.Item("FIN_AMT").ToString.Trim)
                If Not String.IsNullOrEmpty(dbReader.Item("EXP_DT").ToString) Then str_exp_dt = dbReader.Item("EXP_DT").ToString 'lucy
            End If
            dbReader.Close()
        Catch ex As Exception
            dbConnection.Close()
            Throw
        End Try

        ' >>>>>>>>>>>>>>>>>>>>  Gets the CUSTOMER details to update the CUST table  <<<<<<<<<<<<<<<<<<<<
        ' ----- NOTE:  The CUSTOMER will always be updated even if the order fails to commit -----
        Dim customer As CustomerDtc = theCustomerBiz.GetCustomer(Session.SessionID.ToString, Session("cust_cd").ToString)
        Dim dLicense As DriverLicenseDtc = theCustomerBiz.GetDriverLicense(Session.SessionID.ToString)
        theCustomerBiz.UpdateCustomer(customer, dLicense, Session("tet_cd"), Session("exempt_id"))

        ' --- EVERY operation queued on the dbAtomicCommand is saved at once, to avoid orphan data related to the order
        Try
            dbAtomicConnection.Open()
            dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction

            Do While doc_processing > 0

                ' if processing a Delivery sale, then Checks Delivery Availability
                'MM-12388 Additionally DELM is updated if the CRM is of Delivery("D") type and also System parameter "Allow credit memos to affect delivery availability?" is set to "Y"
                If (AppConstants.Order.TYPE_SAL.Equals(Session("ord_tp_cd")) OrElse (AppConstants.Order.TYPE_CRM.Equals(Session("ord_tp_cd")) AndAlso SysPms.crmAffectDelAvail)) AndAlso
                    doc_processing = ProcessingType.SingleOrFirstDoc AndAlso AppConstants.DELIVERY.Equals(Session("PD")) Then
                    Dim isDelDtAvailable = False
                    If isActualStops Then
                        sql = "UPDATE DEL SET ACTUAL_STOPS=" & CDbl(FormatNumber(actualStops)) + deliverySalePoints
                        sql = sql & " WHERE (ZONE_CD = '" & Session("ZONE_CD") & "') AND (DEL_DT = TO_DATE('" & Session("del_dt") & "','mm/dd/RRRR')) "
                        dbAtomicCommand.CommandText = sql
                        dbAtomicCommand.ExecuteNonQuery()
                        isDelDtAvailable = True
                    End If
                    If (Not isDelDtAvailable) Then
                        lbl_Status.Text = "The delivery date you have chosen is not available"
                        Exit Sub
                    End If
                End If



                ' Daniela move the code after SO create 
                If ConfigurationManager.AppSettings("package_breakout").ToString <> AppConstants.PkgSplit.ON_COMMIT Then
                    If doc_processing = ProcessingType.SingleOrFirstDoc Then ' cash and carry OR non-take-with portion of split OR just a regular P/D
                        If split_order AndAlso isSplitTw Then isSplitTw = False
                        If Len(del_doc_num) <> 11 Then del_doc_num = Left(del_doc_num, 11)
                        lbl_del_doc_num.Text = del_doc_num


                        ' --- calculates total tax
                        If HttpContext.Current.Session("sub_total") & "" <> "" Then
                            If IsNumeric(HttpContext.Current.Session("DEL_CHG")) Then
                                del_chg = CDbl(HttpContext.Current.Session("DEL_CHG"))
                                If IsNumeric(HttpContext.Current.Session("DEL_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                    del_tax = CDbl(HttpContext.Current.Session("DEL_CHG")) * (HttpContext.Current.Session("DEL_TAX") / 100)
                                End If
                            End If
                            If IsNumeric(HttpContext.Current.Session("SETUP_CHG")) Then
                                setup_chg = CDbl(HttpContext.Current.Session("SETUP_CHG"))
                                If IsNumeric(HttpContext.Current.Session("SU_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                    setup_tax = CDbl(HttpContext.Current.Session("SETUP_CHG")) * (HttpContext.Current.Session("SU_TAX") / 100)
                                End If
                            End If
                        End If

                        sql = "SELECT sum(qty*tax_amt) as tax_sum FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH <> 'Y'"
                        dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If (dbReader.Read()) Then
                            If IsNumeric(dbReader.Item("tax_sum").ToString) And String.IsNullOrEmpty(Session("tet_cd")) Then
                                ttax = dbReader.Item("tax_sum").ToString
                                ttax_cd = Session("tax_cd")
                            Else
                                ttax = 0
                            End If
                        End If
                        dbReader.Close()

                        If Not IsNumeric(ttax) Then ttax = 0
                        If String.IsNullOrEmpty(Session("tet_cd")) Then
                            ttax = Math.Round(CDbl(ttax + del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
                        Else
                            ttax = 0
                        End If

                        ' ---- adjusts order subtotal
                        If newtotal <> 0 Then Session("sub_total") = newtotal - CDbl(Session("sub_total"))

                    ElseIf doc_processing = ProcessingType.SplitTw Then  ' the take-with doc on a split
                        del_doc_num = del_doc_num & "A"
                        isSplitTw = True
                    End If

                    If Session("sub_total") & "" <> "" Then
                        sub_total = FormatCurrency(CDbl(Session("sub_total")))
                        If doc_processing <> ProcessingType.SplitTw Then
                            If IsNumeric(Session("DEL_CHG")) Then
                                delivery = FormatNumber(CDbl(Session("DEL_CHG")), 2)
                            Else
                                delivery = FormatNumber(0, 2)
                            End If
                            If IsNumeric(Session("SETUP_CHG")) Then
                                setup_chg = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
                            Else
                                setup_chg = FormatNumber(0, 2)
                            End If
                        End If
                        If IsNumeric(ttax) And String.IsNullOrEmpty(Session("tet_cd")) Then
                            If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                                Dim taxDt As String = Session("tran_dt")
                                If "CRM".Equals(Session("ord_tp_cd")) AndAlso Session("adj_ivc_cd") & "" <> "" Then
                                    If (Not Session("origSoWrDt") Is Nothing) AndAlso (Session("origSoWrDt").ToString + "").isNotEmpty AndAlso IsDate(Session("origSoWrDt").ToString) Then
                                        taxDt = FormatDateTime(Session("origSoWrDt").ToString, DateFormat.ShortDate)
                                    Else
                                        ' if there is an original sale for the return, then the tax should be based on the original sale tax
                                        Dim origSo As DataRow = OrderUtils.Get_Orig_Del_Doc(Session("adj_ivc_cd"))
                                        If Not origSo Is Nothing Then taxDt = origSo("so_wr_dt")
                                    End If
                                End If
                                If isSplitTw Then
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, 0.0, 0.0, OrderUtils.tempItmSelection.TakeWith)
                                ElseIf split_order Then
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.NonTakeWith)
                                Else
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.All)
                                End If
                            Else ' line tax calc left as was above
                                tax = CDbl(ttax)
                            End If
                        End If
                        grand_total = FormatCurrency((CDbl(Session("sub_total")) - CDbl(discount)) + FormatNumber(CDbl(tax), 2) + FormatNumber(CDbl(delivery), 2) + FormatNumber(CDbl(setup_chg), 2), 2)
                    End If
                    'MM-5693
                    If doc_processing = ProcessingType.SingleOrFirstDoc And Session("ord_tp_cd") = "CRM" Then
                        lbl_store_cd.Text = Session("store_cd")
                        lbl_cust_cd.Text = Session("CUST_CD")
                        lbl_ivc_cd.Text = Session("adj_ivc_cd")
                        lbl_ord_total.Text = grand_total
                    ElseIf Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MCR) Then
                        lbl_ivc_cd.Text = del_doc_num
                        lbl_ord_total.Text = grand_total
                    End If
                End If ' Daniela not on commit logic
                ' End Daniela comment

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO INFO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                Dim storeCd, soSeqNum As String

                Dim str_store As String = Session("pd_store_cd")
                Dim str_co_cd As String = theTMBiz.lucy_get_co_cd(str_store)  'lucy

                'lucy, 16-jun-15

                If PD = "D" Then

                    Dim strpdst As String = LeonsBiz.get_pu_del_store(str_store, ZONE_CD, str_co_cd)
                    Session("pd_store_cd") = strpdst
                End If
                Dim strpudelst As String = Session("pd_store_cd")

                sql = "INSERT INTO SO (ORIGIN_CD, DEL_DOC_NUM, CUST_CD, PU_DEL_STORE_CD, EMP_CD_OP,EMP_CD_KEYER, ORD_TP_CD, SO_STORE_CD, SO_EMP_SLSP_CD1, PCT_OF_SALE1, SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, PU_DEL, PU_DEL_DT, MASF_FLAG, SO_WR_SEC "
                sql2 = "VALUES('POS','" & del_doc_num & "','" & Session("cust_cd") & "','"

                If doc_processing = ProcessingType.SplitTw Then
                    storeCd = Session("store_cd")
                Else
                    storeCd = Session("pd_store_cd")
                End If

                sql2 = sql2 & storeCd & "','"
                sql2 = sql2 & Session("emp_cd") & "','"
                sql2 = sql2 & Session("emp_cd") & "','"
                sql2 = sql2 & Session("ord_tp_cd") & "','"
                sql2 = sql2 & Session("store_cd") & "','"
                'sql2 = sql2 & Session("slsp1") & "','"
                sql2 = sql2 & slsp1 & "','"   'Alice update on Feb 22, 2019
                sql2 = sql2 & Session("pct_1") & "','"
                sql2 = sql2 & Left(del_doc_num, 11) & "',"
                If IsDate(Session("tran_dt")) Then
                    Session("tran_dt") = FormatDateTime(Session("tran_dt"), DateFormat.ShortDate)
                Else
                    Session("tran_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
                End If
                sql2 = sql2 & "TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),'"

                soSeqNum = soKeys.soKey.seqNum
                sql2 = sql2 & soSeqNum & "','"
                If doc_processing = ProcessingType.SplitTw Then
                    sql2 = sql2 & "P',"
                    sql2 = sql2 & "TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),'"
                Else
                    sql2 = sql2 & Session("PD") & "',"
                    sql2 = sql2 & "TO_DATE('" & Session("del_dt") & "','mm/dd/RRRR'),'"
                End If
                sql2 = sql2 & ConfigurationManager.AppSettings("mass").ToString & "','" & lngHowLong & "'"

                If doc_processing <> ProcessingType.SplitTw Then
                    If IsNumeric(Session("DEL_CHG")) Then
                        sql = sql & ", DEL_CHG "
                        sql2 = sql2 & ", " & Session("DEL_CHG")
                    End If
                    If IsNumeric(Session("SETUP_CHG")) Then
                        sql = sql & ", SETUP_CHG "
                        sql2 = sql2 & ", " & Session("SETUP_CHG")
                    End If
                    If finAmt > 0 Then
                        sql = sql & ", ORIG_FI_AMT "
                        sql2 = sql2 & ", " & finAmt
                    End If
                    If Not String.IsNullOrEmpty(approvalCd) Then
                        sql = sql & ", APPROVAL_CD "
                        sql2 = sql2 & ", '" & approvalCd & "' "
                    End If
                    If Not String.IsNullOrEmpty(finCo) Then
                        sql = sql & ", FIN_CUST_CD "
                        sql2 = sql2 & ", '" & finCo & "'"
                    End If
                End If

                If Session("PO") & "" <> "" And ConfigurationManager.AppSettings("show_po").ToString = "Y" Then
                    sql = sql & ", ORDER_PO_NUM "
                    sql2 = sql2 & ", '" & Session("PO") & "'"
                End If
                If Session("USR_FLD_1") & "" <> "" Then
                    sql = sql & ", USR_FLD_1 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_1") & "'"
                End If
                If Session("USR_FLD_2") & "" <> "" Then
                    sql = sql & ", USR_FLD_2 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_2") & "'"
                End If
                If Session("USR_FLD_3") & "" <> "" Then
                    sql = sql & ", USR_FLD_3 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_3") & "'"
                End If
                If Session("USR_FLD_4") & "" <> "" Then
                    sql = sql & ", USR_FLD_4 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_4") & "'"
                End If
                If Session("USR_FLD_5") & "" <> "" Then
                    sql = sql & ", USR_FLD_5 "
                    sql2 = sql2 & ", '" & Session("USR_FLD_5") & "'"
                End If
                If Session("tet_cd") & "" <> "" Then
                    sql = sql & ", TET_CD "
                    sql2 = sql2 & ", '" & Session("TET_CD") & "'"
                End If
                Dim l_zone As String  ' start lucy add, 03-Mar-15 
                If Session("cash_carry") = "TRUE" Then

                    If IsDBNull(Session("ZONE_CD")) Then
                        l_zone = Session("store_cd") & "0"
                    Else
                        l_zone = Session("ZONE_CD")
                    End If

                    sql = sql & ", SHIP_TO_ZONE_CD "
                    sql2 = sql2 & ", '" & l_zone & "'"
                    sql = sql & ", CASH_AND_CARRY "
                    sql2 = sql2 & ", 'Y'"
                    sql = sql & ", STAT_CD "
                    sql2 = sql2 & ", 'F'"
                    sql = sql & ", FINAL_STORE_CD "
                    sql2 = sql2 & ", '" & Session("store_cd") & "'"
                    sql = sql & ", FINAL_DT "
                    sql2 = sql2 & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"

                    'lucy added the above whole block, 03-Mar-15

                    'MM-5693.
                    'If doc_processing <> ProcessingType.SplitTw And Session("cash_carry") <> "TRUE" Then
                ElseIf doc_processing <> ProcessingType.SplitTw And Session("cash_carry") <> "TRUE" Then   'lucy modified
                    If SysPms.isAutoFinalizeMCRMDB AndAlso (Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MCR) OrElse Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MDB)) Then
                        sql = sql & ", STAT_CD "
                        sql2 = sql2 & ", 'F'"
                        sql = sql & ", FINAL_DT "
                        sql2 = sql2 & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"
                    Else
                        If Session("ZONE_CD") & "" <> "" Then
                            sql = sql & ", SHIP_TO_ZONE_CD "
                            sql2 = sql2 & ", '" & Session("ZONE_CD") & "'"
                        End If
                        sql = sql & ", STAT_CD "
                        sql2 = sql2 & ", 'O'"
                    End If
                Else
                    sql = sql & ", CASH_AND_CARRY "
                    sql2 = sql2 & ", 'Y'"
                    sql = sql & ", STAT_CD "
                    sql2 = sql2 & ", 'F'"
                    sql = sql & ", FINAL_STORE_CD "
                    sql2 = sql2 & ", '" & Session("store_cd") & "'"
                    sql = sql & ", FINAL_DT "
                    sql2 = sql2 & ", TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR')"
                End If
                If Session("ord_tp_cd") <> "SAL" And Session("crm_cause_cd") & "" <> "" Then
                    sql = sql & ", CAUSE_CD "
                    sql2 = sql2 & ", '" & Session("crm_cause_cd") & "'"
                End If
                If Session("ord_tp_cd") <> "SAL" And Session("adj_ivc_cd") & "" <> "" Then
                    sql = sql & ", ORIG_DEL_DOC_NUM "
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "'"
                End If
                If Not SessVar.discCd.isEmpty Then
                    sql = sql & ", DISC_CD "
                    sql2 = sql2 & ", '" & SessVar.discCd & "'"
                End If

                'If Session("slsp2") & "" <> "" Then
                '    sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                '    sql2 = sql2 & ", '" & Session("slsp2") & "','" & Session("pct_2") & "'"
                'End If

                If slsp2 & "" <> "" Then
                    sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                    sql2 = sql2 & ", '" & slsp2 & "','" & Session("pct_2") & "'"
                End If


                If Session("TAX_CD") & "" <> "" And Session("tet_cd") & "" = "" Then
                    sql = sql & ", TAX_CD "
                    sql2 = sql2 & ", '" & Session("TAX_CD") & "'"
                End If
                If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                    If IsNumeric(tax) Then
                        sql = sql & ", TAX_CHG"
                        sql2 = sql2 & ", " & tax & ""
                    Else
                        If Not IsNumeric(Session("SU_TAX")) Then Session("SU_TAX") = 0
                        If Not IsNumeric(Session("DEL_TAX")) Then Session("SU_TAX") = 0
                        sql = sql & ", TAX_CHG"
                        sql2 = sql2 & ", " & CDbl(Session("SU_TAX")) + CDbl(Session("DEL_TAX"))
                    End If
                Else
                    sql = sql & ", TAX_CHG"
                    sql2 = sql2 & ", " & Math.Round(CDbl(del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
                End If
                If Session("ord_srt") & "" <> "" Then
                    sql = sql & ", ORD_SRT_CD"
                    sql2 = sql2 & ", '" & Session("ord_srt") & "'"
                End If
                If Session("cust_cd") & "" <> "" Then
                    sql = sql & ", SHIP_TO_CUST_CD"
                    sql2 = sql2 & ", '" & Replace(Session("cust_cd"), "'", "''") & "'"
                End If
                If customer.corpName & "" <> "" Then
                    sql = sql & ", SHIP_TO_CORP"
                    sql2 = sql2 & ", '" & Replace(customer.corpName, "'", "''") & "'"
                End If
                If customer.fname & "" <> "" Then
                    sql = sql & ", SHIP_TO_F_NAME"
                    sql2 = sql2 & ", '" & Replace(customer.fname, "'", "''") & "'"
                End If
                If customer.lname & "" <> "" Then
                    sql = sql & ", SHIP_TO_L_NAME"
                    sql2 = sql2 & ", '" & Replace(customer.lname, "'", "''") & "'"
                End If
                If customer.addr1 & "" <> "" Then
                    sql = sql & ", SHIP_TO_ADDR1"
                    sql2 = sql2 & ", '" & Replace(customer.addr1, "'", "''") & "'"
                End If
                If customer.addr2 & "" <> "" Then
                    sql = sql & ", SHIP_TO_ADDR2"
                    sql2 = sql2 & ", '" & Replace(customer.addr2, "'", "''") & "'"
                End If
                If customer.city & "" <> "" Then
                    sql = sql & ", SHIP_TO_CITY"
                    sql2 = sql2 & ", '" & Replace(customer.city, "'", "''") & "'"
                End If
                If customer.state & "" <> "" Then
                    sql = sql & ", SHIP_TO_ST_CD"
                    sql2 = sql2 & ", '" & customer.state & "'"
                End If
                If customer.zip & "" <> "" Then
                    sql = sql & ", SHIP_TO_ZIP_CD"
                    sql2 = sql2 & ", '" & customer.zip & "'"
                End If
                If customer.hphone & "" <> "" Then
                    sql = sql & ", SHIP_TO_H_PHONE"
                    sql2 = sql2 & ", '" & customer.hphone & "'"
                End If
                If customer.bphone & "" <> "" Then
                    sql = sql & ", SHIP_TO_B_PHONE"
                    sql2 = sql2 & ", '" & customer.bphone & "'"
                End If

                'Code changes for MM-4069
                'sql = sql & ")"
                'sql2 = sql2 & ")"

                sql = sql & ", CONF_STAT_CD)"
                sql2 = sql2 & ", '" & theSalesBiz.GetFirstConfirmationCode() & "')"
                'till here

                sql = UCase(sql & sql2)
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves a record to the CRM.AUDIT_LOG table
                theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "SALE_CREATED", del_doc_num & " was created via Sales Order Entry")

                'now save the marketing info, if any, to  the SO marketing tables
                theSalesBiz.SaveSOMarketingInfo(dbAtomicCommand, Session("tran_dt"), storeCd, soSeqNum, Session("mkt_cat"), Session("mkt_sub_cd"))

                If doc_processing = ProcessingType.SingleOrFirstDoc And newtotal <> 0 Then
                    lbl_Status.Text = lbl_Status.Text & "<b> / " & del_doc_num & "</b>"
                Else
                    lbl_Status.Text = "<br /><b>" & Resources.LibResources.Label121 & " " & del_doc_num & "</b>"
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO_CMNT     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                ' Add auto comemnt for tax code change only when the user has actually gone in to change it
                If Not (IsNothing(Session("MANUAL_TAX_CD"))) AndAlso
                    Session("MANUAL_TAX_CD").ToString.isNotEmpty AndAlso
                    Session("MANUAL_TAX_CD") <> Session("TAX_CD") Then
                    SaveAutoComment(dbAtomicCommand, del_doc_num, Session("tran_dt"), "TCD", Session("MANUAL_TAX_CD"), Session("TAX_CD"), Session("EMP_CD"))
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO_LN INFO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                ProcessSoLn(del_doc_num, doc_processing, dbAtomicCommand)
                dbAtomicCommand.CommandType = CommandType.Text
                dbAtomicCommand.Parameters.Clear()
                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    END OF SO_LN CREATE     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                ' Daniela Start split package here before temp_itm get deleted

                ' Daniela Calculate package price here to get th correct Subtotal

                Dim hdrInfo As SoHeaderDtc
                ' Need to capture the info before it is gone
                If ConfigurationManager.AppSettings("package_breakout").ToString = AppConstants.PkgSplit.ON_COMMIT Then
                    ' create the object for header and transaction info        
                    hdrInfo = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim, slsp1, slsp2, pct_1, IIf(IsNothing(pct_2), 0, pct_2),
                                                    tet_cd, ttax_cd, STORE_ENABLE_ARS, ZONE_CD, PD, pd_store_cd, cash_carry)
                    hdrInfo.delDocNum = del_doc_num
                    If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
                    hdrInfo.writtenDt = Session("tran_dt")
                    hdrInfo.orderTpCd = SessVar.ordTpCd
                    ' TODO - setting tax method here because it should eventually be based on tax code
                    hdrInfo.taxMethod = IIf(ConfigurationManager.AppSettings("tax_line").ToString = "Y", "L", "H")

                End If
                If ConfigurationManager.AppSettings("package_breakout").ToString = AppConstants.PkgSplit.ON_COMMIT AndAlso
                    Not IsNothing(hdrInfo) Then

                    If hdrInfo.orderTpCd = AppConstants.Order.TYPE_SAL OrElse hdrInfo.orderTpCd = AppConstants.Order.TYPE_CRM Then

                        If ttax_cd.isNotEmpty Then

                            Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(hdrInfo.writtenDt, hdrInfo.taxCd)
                            hdrInfo.taxRates = taxPcts.itmTpPcts  'SetTxPcts(headerInfo.soWrDt, headerInfo.taxCd).itmTpPcts
                        End If

                        theSalesBiz.splitSoLnPkgCmpntPrcs(hdrInfo)

                        ' Update temp_item table to recalculate correct total for packate items

                        Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(dbConn)

                        Try
                            dbConn.Open()
                            dbCmd.Transaction = dbConn.BeginTransaction()

                            'Dim cmdLocal As OracleCommand = dbAtomicCommand
                            Dim sqlUpd As String = ""

                            sqlUpd = "UPDATE TEMP_ITM t SET RET_PRC = (select unit_prc from so_ln " +
                                "where itm_cd = t.itm_cd and del_doc_ln# = t.SO_LN_NO and del_doc_num =  '" & del_doc_num & "') " +
                                "WHERE t.Session_ID='" & Session.SessionID.ToString.Trim & "' and (t.itm_cd, t.SO_LN_NO) in (select itm_cd, del_doc_ln# " +
                                "from so_ln where itm_cd = t.itm_cd and del_doc_ln# = t.SO_LN_NO and del_doc_num =  '" & del_doc_num & "')"

                            dbCmd.CommandText = sqlUpd

                            dbCmd.ExecuteNonQuery()
                            dbCmd.Transaction.Commit()

                        Finally
                            dbCmd.Dispose()
                            dbConn.Close()
                            dbConn.Dispose()
                        End Try

                        '  recalculate subtotal for the package
                        sql = "select sum(qty*unit_prc) sub_total_new from so_ln where del_doc_num = '" & del_doc_num & "'"

                        Dim sub_total_new As String
                        dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If (dbReader.Read()) Then
                            If IsNumeric(dbReader.Item("sub_total_new").ToString) Then
                                sub_total_new = dbReader.Item("sub_total_new").ToString
                            End If
                            If Not sub_total_new Is Nothing Then
                                HttpContext.Current.Session("sub_total") = sub_total_new
                            End If
                        End If

                        dbReader.Close()

                    End If

                End If

                ' Daniela moved code
                If ConfigurationManager.AppSettings("package_breakout").ToString = AppConstants.PkgSplit.ON_COMMIT Then
                    If doc_processing = ProcessingType.SingleOrFirstDoc Then ' cash and carry OR non-take-with portion of split OR just a regular P/D
                        If split_order AndAlso isSplitTw Then isSplitTw = False
                        If Len(del_doc_num) <> 11 Then del_doc_num = Left(del_doc_num, 11)
                        lbl_del_doc_num.Text = del_doc_num

                        ' --- calculates total tax
                        If HttpContext.Current.Session("sub_total") & "" <> "" Then
                            If IsNumeric(HttpContext.Current.Session("DEL_CHG")) Then
                                del_chg = CDbl(HttpContext.Current.Session("DEL_CHG"))
                                If IsNumeric(HttpContext.Current.Session("DEL_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                    del_tax = CDbl(HttpContext.Current.Session("DEL_CHG")) * (HttpContext.Current.Session("DEL_TAX") / 100)
                                End If
                            End If
                            If IsNumeric(HttpContext.Current.Session("SETUP_CHG")) Then
                                setup_chg = CDbl(HttpContext.Current.Session("SETUP_CHG"))
                                If IsNumeric(HttpContext.Current.Session("SU_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                                    setup_tax = CDbl(HttpContext.Current.Session("SETUP_CHG")) * (HttpContext.Current.Session("SU_TAX") / 100)
                                End If
                            End If
                        End If

                        sql = "SELECT sum(qty*tax_amt) as tax_sum FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH <> 'Y'"
                        dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If (dbReader.Read()) Then
                            If IsNumeric(dbReader.Item("tax_sum").ToString) And String.IsNullOrEmpty(Session("tet_cd")) Then
                                ttax = dbReader.Item("tax_sum").ToString
                                ttax_cd = Session("tax_cd")
                            Else
                                ttax = 0
                            End If
                        End If
                        dbReader.Close()

                        If Not IsNumeric(ttax) Then ttax = 0
                        If String.IsNullOrEmpty(Session("tet_cd")) Then
                            ttax = Math.Round(CDbl(ttax + del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
                        Else
                            ttax = 0
                        End If

                        ' ---- adjusts order subtotal
                        If newtotal <> 0 Then Session("sub_total") = newtotal - CDbl(Session("sub_total"))

                    ElseIf doc_processing = ProcessingType.SplitTw Then  ' the take-with doc on a split
                        del_doc_num = del_doc_num & "A"
                        isSplitTw = True
                    End If

                    If Session("sub_total") & "" <> "" Then
                        sub_total = FormatCurrency(CDbl(Session("sub_total")))
                        If doc_processing <> ProcessingType.SplitTw Then
                            If IsNumeric(Session("DEL_CHG")) Then
                                delivery = FormatNumber(CDbl(Session("DEL_CHG")), 2)
                            Else
                                delivery = FormatNumber(0, 2)
                            End If
                            If IsNumeric(Session("SETUP_CHG")) Then
                                setup_chg = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
                            Else
                                setup_chg = FormatNumber(0, 2)
                            End If
                        End If
                        If IsNumeric(ttax) And String.IsNullOrEmpty(Session("tet_cd")) Then
                            If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                                Dim taxDt As String = Session("tran_dt")
                                If "CRM".Equals(Session("ord_tp_cd")) AndAlso Session("adj_ivc_cd") & "" <> "" Then
                                    If (Not Session("origSoWrDt") Is Nothing) AndAlso (Session("origSoWrDt").ToString + "").isNotEmpty AndAlso IsDate(Session("origSoWrDt").ToString) Then
                                        taxDt = FormatDateTime(Session("origSoWrDt").ToString, DateFormat.ShortDate)
                                    Else
                                        ' if there is an original sale for the return, then the tax should be based on the original sale tax
                                        Dim origSo As DataRow = OrderUtils.Get_Orig_Del_Doc(Session("adj_ivc_cd"))
                                        If Not origSo Is Nothing Then taxDt = origSo("so_wr_dt")
                                    End If
                                End If
                                If isSplitTw Then
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, 0.0, 0.0, OrderUtils.tempItmSelection.TakeWith)
                                ElseIf split_order Then
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.NonTakeWith)
                                Else
                                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), taxDt, setup_chg, delivery, OrderUtils.tempItmSelection.All)
                                End If
                            Else ' line tax calc left as was above
                                tax = CDbl(ttax)
                            End If
                        End If
                        grand_total = FormatCurrency((CDbl(Session("sub_total")) - CDbl(discount)) + FormatNumber(CDbl(tax), 2) + FormatNumber(CDbl(delivery), 2) + FormatNumber(CDbl(setup_chg), 2), 2)
                    End If

                    If doc_processing = ProcessingType.SingleOrFirstDoc And Session("ord_tp_cd") = "CRM" Then
                        lbl_store_cd.Text = Session("store_cd")
                        lbl_cust_cd.Text = Session("CUST_CD")
                        lbl_ivc_cd.Text = Session("adj_ivc_cd")
                        lbl_ord_total.Text = grand_total
                    End If
                End If ' End Daniela on commit change

                ' Daniela End split package here before temp_itm get deleted
                If (doc_processing = ProcessingType.SplitTw Or Session("cash_carry") = "TRUE") AndAlso InStr(Session("err"), "Insufficient") > 0 Then

                    sql = "UPDATE SO SET STAT_CD='O', FINAL_STORE_CD=NULL, FINAL_DT=NULL WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.ExecuteNonQuery()

                    sql = "DELETE FROM GL_DOC_TRN WHERE DOC_NUM=:DOC_NUM AND CO_CD=:CO_CD AND ORIGIN_CD=:ORIGIN_CD AND PROCESSED=:PROCESSED"
                    dbAtomicCommand.CommandText = sql

                    dbAtomicCommand.Parameters.Add(":DOC_NUM", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    dbAtomicCommand.Parameters.Add(":PROCESSED", OracleType.VarChar)

                    dbAtomicCommand.Parameters(":DOC_NUM").Value = del_doc_num
                    dbAtomicCommand.Parameters(":CO_CD").Value = Session("CO_CD")
                    dbAtomicCommand.Parameters(":ORIGIN_CD").Value = "PSOE"
                    dbAtomicCommand.Parameters(":PROCESSED").Value = "N"

                    dbAtomicCommand.ExecuteNonQuery()
                    dbAtomicCommand.Parameters.Clear()
                End If
                'MM-5693
                If (doc_processing = ProcessingType.SplitTw OrElse Session("cash_carry") = "TRUE") AndAlso InStr(Session("err"), "Insufficient") < 1 Then

                    theSalesBiz.UpdateGldoctrn(dbAtomicCommand, del_doc_num, Session("CO_CD"))
                End If
                If isAutoFinalizeMCRMDB AndAlso (Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MCR) OrElse Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MDB)) Then

                    theSalesBiz.UpdateGldoctrn(dbAtomicCommand, del_doc_num, Session("CO_CD"))
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE SO_LN2DISC INFO - multiple discounts    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If SysPms.allowMultDisc AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then
                    ProcessSoLnDiscounts(del_doc_num, doc_processing, dbAtomicCommand)
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE AR_TRN INFO FOR SO     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing = ProcessingType.SplitTw Then split_amt = CDbl(FormatNumber(grand_total, 2))

                'Create Sales Transaction
                sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, PMT_STORE, WR_DT, ORIGIN_CD "
                sql2 = " VALUES('" & Session("CO_CD") & "','" & Session("cust_cd")
                sql2 = sql2 & "','" & Session("store_cd") & "','" & Session("ord_tp_cd") & "'," & CDbl(FormatNumber(grand_total, 2)) & ", TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),"
                If (doc_processing = ProcessingType.SplitTw OrElse Session("cash_carry") = "TRUE") AndAlso InStr(Session("err"), "Insufficient") < 1 Then
                    sql2 = sql2 & "'T',"
                ElseIf SysPms.isAutoFinalizeMCRMDB AndAlso (Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MCR) OrElse Session("ord_tp_cd").Equals(AppConstants.Order.TYPE_MDB)) Then
                    sql2 = sql2 & "'T',"
                Else
                    sql2 = sql2 & "'A',"
                End If
                sql2 = sql2 & "'O',"
                sql2 = sql2 & "'" & Session("store_cd") & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"

                'If Session("slsp1") & "" <> "" Then
                '    sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                '    sql2 = sql2 & ", '" & Session("EMP_CD") & "','" & Session("slsp1") & "'"
                'End If

                'Alice update on Feb 22, 2019 for request 4477
                sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                sql2 = sql2 & ", '" & Session("EMP_CD") & "','" & Session("EMP_CD") & "'"


                If Not String.IsNullOrEmpty(finAcctNum) Then
                    sql = sql & ", BNK_CRD_NUM, MOP_CD"
                    sql2 = sql2 & ", '" & finAcctNum & "', 'FI'"
                    sql = sql & ", exp_dt"     'lucy
                    sql2 = sql2 & ", TO_DATE('" & str_exp_dt & "','mm/dd/RRRR')"
                ElseIf String.IsNullOrEmpty(finAcctNum) And Not String.IsNullOrEmpty(finCo) Then    'lucy add following lines  , new customer, no card, no transation, but has plan
                    sql = sql & ", MOP_CD"
                    sql2 = sql2 & ",   'FI'"
                End If

                If Session("adj_ivc_cd") & "" <> "" And Session("ord_tp_cd") <> "SAL" Then
                    sql = sql & ", IVC_CD, ADJ_IVC_CD"
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "','" & del_doc_num & "'"
                ElseIf Session("adj_ivc_cd") & "" <> "" And Session("ord_tp_cd") = "MCR" Then
                    sql = sql & ", IVC_CD, ADJ_IVC_CD"
                    sql2 = sql2 & ", '" & Session("adj_ivc_cd") & "','" & del_doc_num & "'"
                ElseIf Session("ord_tp_cd") = "CRM" Then
                    sql = sql & ", ADJ_IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                ElseIf Session("ord_tp_cd") = "MCR" OrElse Session("ord_tp_cd") = "MDB" Then
                    sql = sql & ", ADJ_IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                Else
                    sql = sql & ", IVC_CD"
                    sql2 = sql2 & ",'" & del_doc_num & "'"
                End If
                sql = sql & ")"
                sql2 = sql2 & ")"
                sql = UCase(sql & sql2)

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE AR_TRN DEPOSITS/REFUNDS     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                Dim PB_Response_Cd As String = ""
                Dim Protobase_Response_Msg As String = ""
                Dim SDC_Response As String = ""
                Dim Host_Response_Msg As String = ""
                Dim Host_Response_Cd As String = ""
                Dim Host_Merchant_ID As String = ""
                Dim merchant_id As String = ""
                Dim plan_desc As String = ""
                Dim rate_info As String = ""
                Dim intro_rate As String = ""
                Dim bnk_crd_num As String = ""
                Dim Auth_No As String = ""
                Dim cc_resp As String = ""
                Dim Card_Type As String = ""
                Dim exp_dt As String = ""
                Dim ref_num As String = ""
                Dim prepaid_balance As String = ""
                Dim Trans_Id As String = ""
                Dim order_tp As String = IIf((Session("ord_tp_cd") = "SAL" OrElse Session("ord_tp_cd") = "MDB"), "DEP", "R")

                ' if split doc, Inserts Payments first, then split to TW if necessary 
                If doc_processing <> ProcessingType.SplitTw Then
                    sql = "SELECT * FROM payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session.SessionID.ToString.Trim & "' order by ROW_ID"
                    dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    Do While (dbReader.Read())
                        plan_desc = ""        'this represents the  promo info verbiage in field 3319
                        rate_info = ""        'this represents the  "after promo" or "acct apr" info in field 3322
                        intro_rate = ""       'this represents the  "during promo" or "promo apr" info in field 3325
                        merchant_id = ""      'merchant key for the store that the sale was written under
                        bnk_crd_num = ""
                        Auth_No = ""
                        Trans_Id = ""
                        Host_Merchant_ID = ""
                        Host_Response_Cd = ""
                        Host_Response_Msg = ""
                        Protobase_Response_Msg = ""
                        PB_Response_Cd = ""
                        ref_num = ""
                        Card_Type = ""
                        cc_resp = ""
                        prepaid_balance = ""
                        partial_amt = 0
                        Insert_Payment = True

                        'Check to see if there is a merchant key for the store that the sale was written under
                        'lucy commented the following 3 lines
                        '  If Not String.IsNullOrEmpty(dbReader.Item("MOP_TP").ToString) Then
                        'merchant_id = Lookup_Merchant(dbReader.Item("MOP_TP").ToString, Session("store_cd"), dbReader.Item("MOP_CD").ToString)
                        ' End If

                        ' Daniela TD plan save
                        Dim tdPlan As String
                        If dbReader.Item("MOP_CD").ToString = "TDF" Then
                            tdPlan = dbReader.Item("FIN_CO").ToString
                        Else : tdPlan = ""
                        End If

                        'SABRINA R1999
                        Dim brk_vdplan As String
                        If isNotEmpty(dbReader.Item("MOP_CD")) And dbReader.Item("MOP_CD").ToString = "DCS" Then
                            If (isNotEmpty(dbReader.Item("FIN_CO"))) Then
                                brk_vdplan = dbReader.Item("FIN_CO").ToString
                            End If
                        Else : brk_vdplan = ""
                        End If

                        'Alice added on Oct 05, 2018 for request 4115
                        Dim brk_vdplan_flx As String
                        If isNotEmpty(dbReader.Item("MOP_CD")) And dbReader.Item("MOP_CD").ToString = "FLX" Then
                            If (isNotEmpty(dbReader.Item("FIN_CO"))) Then
                                brk_vdplan_flx = dbReader.Item("FIN_CO").ToString
                            End If
                        Else : brk_vdplan_flx = ""
                        End If


                        'Alice added on May 31, 2019 for request 235
                        Dim brk_vdplan_fm As String
                        If isNotEmpty(dbReader.Item("MOP_CD")) And dbReader.Item("MOP_CD").ToString = "FM" Then
                            If (isNotEmpty(dbReader.Item("FIN_CO"))) Then
                                brk_vdplan_fm = dbReader.Item("FIN_CO").ToString
                            End If
                        Else : brk_vdplan_fm = ""
                        End If

                        If Not String.IsNullOrEmpty(merchant_id) Then
                            '
                            'Process Check Guarantees through National Check Trust
                            '
                            If dbReader.Item("MOP_TP").ToString = "CK" AndAlso ConfigurationManager.AppSettings("chk_guar") = "Y" Then
                                cc_resp = NCT_Submit_Query(dbReader.Item("ROW_ID").ToString, merchant_id)
                                If InStr(LCase(cc_resp), "approval") > 0 Then
                                    Auth_No = Right(cc_resp, cc_resp.IndexOf("Message = ") - 10).Trim
                                    Auth_No = Replace(Auth_No, "=", "")
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " succeeded for check " & dbReader.Item("CHK_NUM").ToString & ". Authorization #:" & Auth_No)
                                    Insert_Payment = True
                                ElseIf InStr(LCase(cc_resp), "denied") > 0 Then
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Check " & dbReader.Item("CHK_NUM").ToString & " was declined", "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " was declined for check " & dbReader.Item("CHK_NUM").ToString)
                                    Insert_Payment = False
                                Else
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Check " & dbReader.Item("CHK_NUM").ToString & " was not approved. " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5), "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " check transaction for " & dbReader.Item("AMT").ToString & " was not approved for check " & dbReader.Item("CHK_NUM").ToString & ". " & Right(cc_resp, cc_resp.IndexOf("Message = ") + 5))
                                    Insert_Payment = False
                                End If
                            End If
                            '
                            'Process Gift Cards through World Gift Card
                            '
                            If dbReader.Item("MOP_TP").ToString = "GC" AndAlso ConfigurationManager.AppSettings("gift_card") = "Y" Then
                                Dim gcAmt As String = dbReader.Item("AMT").ToString
                                Dim xml_document As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id,
                                                                                                "sale",
                                                                                                dbReader.Item("BC").ToString,
                                                                                                IIf(order_tp = "DEP", gcAmt, CDbl(gcAmt) * -1))
                                cc_resp = xml_document.InnerText.ToString
                                If IsNumeric(cc_resp) Then
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " gift card transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & dbReader.Item("BC").ToString & ".")
                                    Insert_Payment = True
                                Else
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", cc_resp, "PMT", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " gift card transaction for " & dbReader.Item("AMT").ToString & " failed for card " & dbReader.Item("BC").ToString & ".  " & cc_resp)
                                    Insert_Payment = False
                                End If
                            End If
                            '
                            'Process Credit Cards through Southern Datacomm
                            '
                            If dbReader.Item("BC").ToString & "" <> "" AndAlso dbReader.Item("MOP_TP").ToString = "BC" AndAlso
                               ConfigurationManager.AppSettings("cc") = "Y" Then

                                Insert_Payment = False
                                SDC_Response = SD_Submit_Query_CC(dbReader.Item("ROW_ID").ToString,
                                                                  merchant_id,
                                                                  IIf(order_tp = "DEP", AUTH_SALE, AUTH_REFUND),
                                                                  del_doc_num)
                                If SDC_Response = AUTH_INQUIRY Then
                                    SDC_Response = SD_Submit_Query_CC(dbReader.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, del_doc_num)
                                End If

                                If (InStr(SDC_Response, " this stream") > 0 OrElse
                                    InStr(SDC_Response, "Protobase is not responding") > 0 OrElse
                                    SDC_Response & "" = "") Then

                                    Dim theBankCardNumber As String = Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE")
                                    If InStr(theBankCardNumber, "^") > 0 Then
                                        Dim sCC As Array = Split(theBankCardNumber, "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    Else
                                        bnk_crd_num = theBankCardNumber
                                    End If
                                    SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", del_doc_num)
                                    err_msg = "TRUE"
                                    theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " failed.  Protobase server did not respond for card " & Right(bnk_crd_num, 4))
                                Else
                                    Dim s As String
                                    Dim MsgArray As Array
                                    Dim sep(3) As Char
                                    sep(0) = Chr(10)
                                    sep(1) = Chr(12)
                                    Dim SDC_Fields As String() = SDC_Response.Split(sep)

                                    For Each s In SDC_Fields
                                        If InStr(s, ",") > 0 Then
                                            MsgArray = Split(s, ",")
                                            Select Case MsgArray(0)
                                                Case "0002"
                                                    partial_amt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0003"
                                                    Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0004"
                                                    exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                                Case "0006"
                                                    Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0007"
                                                    ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "0632"
                                                    prepaid_balance = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1000"
                                                    Card_Type = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1003"
                                                    PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1004"
                                                    Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1005"
                                                    Host_Merchant_ID = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1008"
                                                    bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                                Case "1009"
                                                    Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                Case "1010"
                                                    Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                            End Select
                                        End If
                                    Next s

                                    If bnk_crd_num & "" <> "" AndAlso InStr(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                        Dim sCC As Array = Split(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    End If

                                    Select Case PB_Response_Cd
                                        Case "0", "0000"
                                            cc_resp = "APPROVED"
                                        Case "60", "0060"
                                            cc_resp = "DECLINED"
                                        Case Else
                                            Host_Response_Msg = PB_Response_Cd
                                            Host_Response_Cd = Protobase_Response_Msg
                                            cc_resp = "ERROR"
                                    End Select

                                    If cc_resp = "APPROVED" Then
                                        Insert_Payment = True
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & Right(bnk_crd_num, 4))

                                        If ConfigurationManager.AppSettings("partial_pay") = "Y" AndAlso CDbl(dbReader.Item("AMT").ToString) <> CDbl(partial_amt) Then

                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_PARTIAL", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " approved for " & partial_amt & " for card " & Right(bnk_crd_num, 4))
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "PARTIAL AUTHORIZATION", "PARTIAL AUTHORIZATION", "EXPECTED:" & dbReader.Item("AMT").ToString & ", ACTUAL:" & partial_amt, "DEP", del_doc_num)
                                            err_msg = "TRUE"

                                            sql = "UPDATE PAYMENT_HOLDING SET AMT=:AMT WHERE ROW_ID=:ROW_ID"
                                            dbAtomicCommand.CommandText = sql
                                            dbAtomicCommand.Parameters.Add(":AMT", OracleType.Number)
                                            dbAtomicCommand.Parameters.Add(":ROW_ID", OracleType.Number)
                                            dbAtomicCommand.Parameters(":AMT").Value = partial_amt
                                            dbAtomicCommand.Parameters(":ROW_ID").Value = dbReader.Item("ROW_ID").ToString
                                            dbAtomicCommand.ExecuteNonQuery()
                                            dbAtomicCommand.Parameters.Clear()

                                        End If
                                    Else
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILURE", "Sale " & del_doc_num & " bankcard transaction for " & dbReader.Item("AMT").ToString & " failed for card " & Right(bnk_crd_num, 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", del_doc_num)
                                        err_msg = "TRUE"
                                    End If
                                End If
                            End If
                            '
                            'Process Finance
                            '
                            If dbReader.Item("MOP_TP").ToString = "FI" AndAlso ConfigurationManager.AppSettings("finance") = "Y" Then

                                Insert_Payment = False

                                Dim provider As String = Lookup_Transport(dbReader.Item("MOP_TP").ToString, Session("store_cd"), dbReader.Item("MOP_CD").ToString)
                                If provider = "GE_182" OrElse provider = "WF_148" Then
                                    ' 
                                    'Process Finance Authorization through  Protobase Instance
                                    '
                                    If order_tp = "DEP" Then
                                        'if this finance mop cd is found as FINANCE_DEPOSIT_TP='Y' in the settlment_mop table, treat it as a auth sale -02
                                        SDC_Response = SD_Submit_Query_PL(dbReader.Item("ROW_ID").ToString,
                                                                    merchant_id,
                                                                    IIf(InStr(DP_String_Creation, dbReader.Item("MOP_CD").ToString) > 0, AUTH_SALE, AUTH_ONLY),
                                                                    del_doc_num)
                                    Else
                                        'SDC_Response = SD_Submit_Query_PL(MyDataReader2.Item("ROW_ID").ToString, merchant_id, AUTH_REFUND, del_doc_num)
                                        'Commenting out; we do not want to refund the customer until the CRM is finalized
                                    End If

                                    If SDC_Response = AUTH_INQUIRY Then
                                        SDC_Response = SD_Submit_Query_PL(dbReader.Item("ROW_ID").ToString, merchant_id, AUTH_INQUIRY, del_doc_num)
                                    End If

                                    If InStr(SDC_Response, " this stream") > 0 OrElse
                                       InStr(SDC_Response, "Protobase is not responding") > 0 OrElse SDC_Response & "" = "" Then

                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "", "", "Protobase server not responding", "DEP", del_doc_num)
                                        err_msg = "TRUE"
                                        theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " failed.  Protobase server did not respond for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4))

                                    ElseIf order_tp = "R" Then
                                        'Do nothing; we want to bypass things for CRM documents
                                    Else
                                        Dim s As String
                                        Dim MsgArray As Array
                                        Dim sep(3) As Char
                                        sep(0) = Chr(10)
                                        sep(1) = Chr(12)
                                        Dim SDC_Fields As String() = SDC_Response.Split(sep)

                                        For Each s In SDC_Fields
                                            If InStr(s, ",") > 0 Then
                                                MsgArray = Split(s, ",")
                                                Select Case MsgArray(0)
                                                    Case "0003"
                                                        Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "0004"
                                                        exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                        exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                                    Case "0006"
                                                        Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "0007"
                                                        ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1003"
                                                        PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1004"
                                                        Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1008"
                                                        bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                        bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                                    Case "1009"
                                                        Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "1010"
                                                        Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "3319"
                                                        plan_desc = ""
                                                        For seq_num = LBound(MsgArray) To UBound(MsgArray)
                                                            If seq_num > 0 Then plan_desc = plan_desc & MsgArray(seq_num) & ","
                                                        Next
                                                        plan_desc = Left(plan_desc, Len(plan_desc) - 1)
                                                    Case "3322"
                                                        rate_info = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                    Case "3325"
                                                        intro_rate = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                                End Select
                                            End If
                                        Next s

                                        Select Case PB_Response_Cd
                                            Case "0", "0000"
                                                cc_resp = "APPROVED"
                                            Case "60", "0060"
                                                cc_resp = "DECLINED"
                                            Case Else
                                                Host_Response_Msg = PB_Response_Cd
                                                Host_Response_Cd = Protobase_Response_Msg
                                                cc_resp = "ERROR"
                                        End Select

                                        If cc_resp = "APPROVED" Then
                                            Insert_Payment = True
                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " succeeded for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4))
                                        Else
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", del_doc_num)
                                            err_msg = "TRUE"
                                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_FAILED", "Sale " & del_doc_num & " finance transaction for " & dbReader.Item("AMT").ToString & " was declined for card " & Right(Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                                        End If
                                    End If

                                ElseIf provider = "ADS" Then
                                    ' 
                                    'Process Finance Authorization through Alliance Data Systems
                                    '
                                    Dim requestDS As New RequestAllianceDataSet()
                                    requestDS.Request.Rows.Add(New Object() {"LEVINS", DateTime.Now, merchant_id, Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE"), dbReader.Item("AMT").ToString, "N"})
                                    requestDS.AcceptChanges()
                                    Dim fiService As New FITransactionService()
                                    Dim responseDS As ResponseAllianceDataSet = fiService.InvokeFITransaction(requestDS)
                                    Dim reasonDesc As String = ""
                                    Dim returnDesc As String = ""
                                    Dim avsDesc As String = ""

                                    If (Not responseDS Is Nothing) And (responseDS.Response.Rows.Count) > 0 Then
                                        If Not IsNothing(responseDS.Response(0).reasonCode) Then
                                            reasonDesc = GetDesc(responseDS.Response(0).reasonCode.ToString, cResponseCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).returnCode) Then
                                            returnDesc = GetDesc(responseDS.Response(0).returnCode.ToString, cReturnCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).avsResponse) Then
                                            avsDesc = GetDesc(responseDS.Response(0).avsResponse.ToString, cAVSCriteria)
                                        End If
                                        If Not IsNothing(responseDS.Response(0).authCode) Then
                                            Auth_No = responseDS.Response(0).authCode.ToString
                                        End If
                                        If LCase(returnDesc) <> "approved" Then
                                            SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, returnDesc, reasonDesc, avsDesc, "FI", del_doc_num)
                                            err_msg = "TRUE"
                                            lbl_fin_co.Text = ""
                                        Else
                                            Insert_Payment = True
                                        End If
                                    Else
                                        SavePaymentHolding(dbAtomicCommand, dbReader.Item("ROW_ID").ToString, "No response from ADS", "", "", "FI", del_doc_num)
                                        lbl_fin_co.Text = ""
                                        err_msg = "TRUE"
                                    End If
                                End If
                                If Not String.IsNullOrEmpty(dbReader.Item("FI_ACCT_NUM").ToString) Then bnk_crd_num = "FI"

                            End If   '***end a finance mop type 

                        End If   ' TODO- add an else here to display a message if terminal/merchant id is null in payment_xref table

                        Dim isFinanceDP As Boolean = Is_Finance_DP(dbReader.Item("MOP_CD").ToString)
                        If dbReader.Item("MOP_TP").ToString = "FI" AndAlso Insert_Payment = True AndAlso order_tp = "DEP" Then

                            Insert_Payment = False
                            finCo = dbReader.Item("FIN_CO").ToString
                            Dim rate_info_array As Array = Nothing
                            Dim intro_rate_array As Array = Nothing
                            Dim during_promo_apr As String = String.Empty
                            Dim during_promo_apr_flag As String = String.Empty
                            Dim after_promo_apr As String = String.Empty
                            Dim after_promo_apr_flag As String = String.Empty
                            Dim promo_duration As String = String.Empty
                            Dim aspPromoCd As String = String.Empty

                            If plan_desc.isEmpty Then
                                theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "FINANCE_PROMO_FAILURE", "Sales Order " & del_doc_num & " did not retrieve promotional terms")

                            Else
                                'this is the promo/during promo info
                                If InStr(intro_rate, ";") > 0 Then
                                    intro_rate_array = Split(intro_rate, ";")
                                    If (Not intro_rate Is Nothing) Then
                                        during_promo_apr_flag = intro_rate_array(0)
                                        during_promo_apr = intro_rate_array(1)
                                        promo_duration = intro_rate_array(4)
                                    End If
                                End If

                                'this is the account/after promo info 
                                If InStr(rate_info, ";") > 0 Then
                                    rate_info_array = Split(rate_info, ";")
                                    If (Not rate_info_array Is Nothing) Then
                                        after_promo_apr_flag = rate_info_array(4)
                                        after_promo_apr = rate_info_array(6)
                                    End If
                                End If

                                sql = "INSERT INTO SETTLEMENT_TERMS (DEL_DOC_NUM, ORD_TP_CD, RATE_DEL_RATE_TYPE, RATE_DEL_APR, INTRO_RATE, INTRO_APR, INTRO_DURATION, PLAN_DESC) "
                                sql = sql & "VALUES (:DEL_DOC_NUM, :ORD_TP_CD, :RATE_DEL_RATE_TYPE, :RATE_DEL_APR, :INTRO_RATE, :INTRO_APR, :INTRO_DURATION, :PLAN_DESC) "
                                dbAtomicCommand.CommandText = UCase(sql)

                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RATE_DEL_RATE_TYPE", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RATE_DEL_APR", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_RATE", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_APR", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":INTRO_DURATION", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":PLAN_DESC", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = del_doc_num
                                dbAtomicCommand.Parameters(":ORD_TP_CD").Value = dbReader.Item("MOP_CD").ToString
                                dbAtomicCommand.Parameters(":RATE_DEL_RATE_TYPE").Value = after_promo_apr_flag  'rate_info_array(4).ToString
                                dbAtomicCommand.Parameters(":RATE_DEL_APR").Value = after_promo_apr   'rate_info_array(6).ToString
                                dbAtomicCommand.Parameters(":INTRO_RATE").Value = during_promo_apr_flag      'intro_rate_array(0).ToString
                                dbAtomicCommand.Parameters(":INTRO_APR").Value = during_promo_apr         'intro_rate_array(1).ToString
                                dbAtomicCommand.Parameters(":INTRO_DURATION").Value = promo_duration 'intro_rate_array(4).ToString
                                dbAtomicCommand.Parameters(":PLAN_DESC").Value = plan_desc.trimString(0, 50)

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()
                            End If

                            If Not isFinanceDP Then

                                '****************** Insert the credit entry details into SO_ASP table, if it does not exist *****************
                                If Not theSalesBiz.ExistsInSO_ASP(del_doc_num) Then

                                    ''get  first the ASP_PROMO info like promo des, etc.
                                    Dim promoDes As String = String.Empty
                                    Dim promoDes3 As String = String.Empty
                                    Dim ds As DataSet = GetAspPromoDetails(dbReader.Item("FIN_CO"), finPromoCd)
                                    For Each dr In ds.Tables(0).Rows
                                        If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                                        If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                                        If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
                                    Next

                                    If IsNumeric(during_promo_apr) Then during_promo_apr = during_promo_apr / 10000 & "% "
                                    If IsNumeric(after_promo_apr) Then after_promo_apr = after_promo_apr / 10000 & "% "

                                    sql = "INSERT INTO SO_ASP (" &
                                                "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                                                "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                                "ACCT_APR, ACCT_APR_FLAG)" &
                                            " VALUES('" & del_doc_num & "', 'Y', '" & finPromoCd & "','" & aspPromoCd & "','" & promoDes & "','" & plan_desc & "','" &
                                                                   promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                                                   after_promo_apr & "','" & after_promo_apr_flag & "')"
                                    dbAtomicCommand.CommandText = UCase(sql)
                                    dbAtomicCommand.ExecuteNonQuery()
                                End If

                                If (String.IsNullOrEmpty(Auth_No) AndAlso Not String.IsNullOrEmpty(approvalCd)) Then Auth_No = approvalCd

                                sql = "UPDATE SO SET APPROVAL_CD='" & Auth_No & "' WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                                sql = UCase(sql)
                                dbAtomicCommand.CommandText = sql
                                dbAtomicCommand.ExecuteNonQuery()

                            Else   'Is_Finance_DP type = true
                                Insert_Payment = True

                                wdr = True
                                wdr_promo = dbReader.Item("FIN_PROMO_CD").ToString
                                theSalesBiz.SaveSOFinanceComments(dbAtomicCommand, soKeys, del_doc_num, wdr_promo, Session("STORE_CD"), Session("tran_dt"))

                            End If      'end if Is_Finance_DP type

                            ' ***Update the AR_TRN with the ref_num used for the FIN auth request 
                            sql = "UPDATE AR_TRN  SET REF_NUM = '" & ref_num & "'" & ", HOST_REF_NUM = '" & ref_num & "'" &
                                        " WHERE ivc_cd = '" & del_doc_num & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            '*** Save the asp response info when an approval is recieved for a finance auth
                            SaveAspResponseCodeInfo(dbAtomicCommand, finCo, del_doc_num)

                            'now delete the payment record
                            sql = "DELETE FROM PAYMENT WHERE ROW_ID=" & dbReader.Item("ROW_ID").ToString
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        ElseIf dbReader.Item("MOP_TP").ToString = "FI" And Insert_Payment = False And Is_Finance_DP(dbReader.Item("MOP_CD").ToString) = False And order_tp = "DEP" Then 'MyDataReader2.Item("MOP_CD").ToString <> "WDR" Then
                            sql = "UPDATE SO SET ORIG_FI_AMT=NULL, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & Session("ord_tp_cd") & "'"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        ElseIf order_tp = "R" Then
                            sql = "INSERT INTO SO_ASP (DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD)" &
                                              " VALUES('" & del_doc_num & "', 'Y', '" & lbl_fin_co.Text & "','" & lbl_fin_co.Text & "')"
                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                        End If       '****end if it's a finance and insert_payment= true

                        If Insert_Payment = True AndAlso Not Session("ord_tp_cd").ToString.ToUpper().Equals(AppConstants.Order.TYPE_MCR) AndAlso Not order_tp.ToUpper.Equals("R") Then
                            'means this could be any type of pmt - CHK, CC, FI DP, GC etc. 
                            'means this could be any type of pmt - CHK, CC, FI DP, GC etc. 
                            ' Daniela add des - removed tdPlan
                            ' Daniela change PMT_STORE logic

                            Dim pmtStore As String
                            If Not Session("clientip") Is Nothing Then
                                pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
                            Else ' if not go global then default to remote ip
                                pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
                            End If
                            If pmtStore & "" = "" Then
                                pmtStore = Session("store_cd")
                            End If
                            If IsNumeric(prepaid_balance) Then Session(del_doc_num & "_CMNT") = Session(del_doc_num & "_CMNT") & "Gift Card -" & Right(bnk_crd_num.Trim, 4) & " new balance: " & FormatCurrency(CDbl(prepaid_balance), 2) & vbCrLf
                            sql = "INSERT INTO AR_TRN (CO_CD, CUST_CD, MOP_CD, ORIGIN_STORE, TRN_TP_CD, AMT, POST_DT, STAT_CD, AR_TP, IVC_CD, adj_ivc_cd, PMT_STORE, WR_DT, ORIGIN_CD "
                            sql2 = " VALUES('" & Session("CO_CD") & "','" & Session("cust_cd") & "','" & dbReader.Item("MOP_CD").ToString & "'"
                            sql2 = sql2 & ",'" & Session("store_cd") & "','" & order_tp & "',"

                            If ConfigurationManager.AppSettings("partial_pay") = "Y" AndAlso partial_amt > 0 AndAlso CDbl(dbReader.Item("AMT").ToString) <> CDbl(partial_amt) Then
                                sql2 = sql2 & partial_amt
                            Else
                                sql2 = sql2 & dbReader.Item("AMT").ToString
                            End If
                            sql2 = sql2 & ", TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),'T','O', '"
                            ' if not sale and has orig doc, then ivc_cd = orig doc and adj_ivc_cd is MDB/MCR/CRM doc
                            If Session("ord_tp_cd") <> AppConstants.Order.TYPE_SAL AndAlso Session("adj_ivc_cd") & "" <> "" Then
                                sql2 = sql2 & Session("adj_ivc_cd") & "', '" & del_doc_num  ' session variable not validly named - is orig del_Doc
                            ElseIf Session("ord_tp_cd") = AppConstants.Order.TYPE_MDB AndAlso Session("adj_ivc_cd") & "" = "" Then
                                sql2 = sql2 & Session("adj_ivc_cd") & "', '" & del_doc_num  ' session variable not validly named - is orig del_Doc
                            Else
                                sql2 = sql2 & del_doc_num & "', '"
                            End If
                            ' Daniela use pmtStore
                            sql2 = sql2 & "','" & pmtStore & "',TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'), 'POS'"
                            If Session("csh_dwr_cd") & "" <> "" Then
                                sql = sql & ", CSH_DWR_CD"
                                sql2 = sql2 & ", '" & Session("csh_dwr_cd") & "'"
                            End If

                            'If Session("slsp1") & "" <> "" Then
                            '    sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                            '    sql2 = sql2 & ", '" & Session("EMP_CD") & "','" & Session("slsp1") & "'"
                            'End If

                            'Alice update on Feb 22, 2019 for request 4477
                            sql = sql & ", EMP_CD_CSHR, EMP_CD_OP"
                            sql2 = sql2 & ", '" & Session("EMP_CD") & "','" & Session("EMP_CD") & "'"


                            If bnk_crd_num = "FI" Then
                                sql = sql & ", BNK_CRD_NUM"
                                sql2 = sql2 & ", '" & Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE") & "'"
                            ElseIf dbReader.Item("MOP_TP").ToString = "GC" Then
                                sql = sql & ", ACCT_NUM"
                                sql2 = sql2 & ", '" & dbReader.Item("BC").ToString & "'"
                            ElseIf dbReader.Item("BC").ToString & "" <> "" Then
                                If dbReader.Item("MOP_CD").ToString = "AMX" Then 'daniela add
                                    bnk_crd_num = Left(dbReader.Item("BC").ToString, 15)
                                Else
                                    bnk_crd_num = Left(dbReader.Item("BC").ToString, 16) 'lucy add
                                End If
                                If IsNumeric(bnk_crd_num) Then
                                    sql = sql & ", BNK_CRD_NUM"
                                    'Daniela comment sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"

                                    sql2 = sql2 & ", '" & bnk_crd_num & "'"
                                Else
                                    If InStr(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^") > 0 Then
                                        Dim sCC As Array = Split(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), "^")
                                        bnk_crd_num = Right(sCC(0).Trim, 4)
                                    Else
                                        bnk_crd_num = Right(Decrypt(dbReader.Item("BC").ToString, "CrOcOdIlE"), 4)
                                    End If
                                    sql = sql & ", BNK_CRD_NUM"
                                    sql2 = sql2 & ", '" & StringUtils.MaskString(bnk_crd_num, "x", 16, 4) & "'"
                                End If
                            End If
                            If Card_Type & "" <> "" Then
                                sql = sql & ", REF_NUM"
                                sql2 = sql2 & ", '" & Card_Type & "' "
                            End If
                            If dbReader.Item("CHK_NUM").ToString() & "" <> "" Then
                                sql = sql & ", CHK_NUM"
                                sql2 = sql2 & ", '" & dbReader.Item("CHK_NUM").ToString() & "' "
                            End If
                            Dim expDt As String = dbReader.Item("EXP_DT").ToString
                            If IsDate(expDt) Then
                                sql = sql & ", EXP_DT"
                                sql2 = sql2 & ", TO_DATE('" & expDt & "','mm/dd/RRRR')"
                            End If
                            If Host_Merchant_ID & "" <> "" And tdPlan & "" = "" Then ' Daniela
                                sql = sql & ", DES"
                                sql2 = sql2 & ", 'MID:" & Host_Merchant_ID & "' "
                            End If
                            If tdPlan & "" <> "" Then ' Daniela added 
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & tdPlan & "' "
                            End If
                            If brk_vdplan & "" <> "" Then ' sabrina R1999 
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan & "' "
                            End If

                            If brk_vdplan_flx & "" <> "" Then ' Alice request 4115
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan_flx & "' "
                            End If

                            If brk_vdplan_fm & "" <> "" Then ' Alice added on May 31, 2019 for request 235
                                sql = sql & ", DES"
                                sql2 = sql2 & ", '" & brk_vdplan_fm & "' "
                            End If

                            Auth_No = dbReader.Item("APPROVAL_CD").ToString  'lucy, 21-Jul-14
                            If Auth_No & "" <> "" Then
                                sql = sql & ", APP_CD"
                                sql2 = sql2 & ",'" & Auth_No & "'"
                            End If
                            sql = UCase(sql)
                            sql2 = UCase(sql2)
                            If ref_num & "" <> "" Then
                                sql = sql & ", HOST_REF_NUM"
                                sql2 = sql2 & ",'" & ref_num & "'"
                            End If
                            If Trans_Id & "" <> "" Then
                                sql = sql & ", ACCT_NUM"
                                'Overwrite the transId(which is the unique ID returned by PB) with the acct# for a 
                                'FI 'DP' type of auth as a workaround to be used for the Leons Refund to FI DP spec.
                                If dbReader.Item("MOP_TP").ToString = "FI" AndAlso isFinanceDP Then
                                    Trans_Id = "ID:" & Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                                End If
                                sql2 = sql2 & ",'" & Trans_Id & "'"
                            End If

                            sql = sql & ")"
                            sql2 = sql2 & ")"
                            sql = sql & sql2

                            dbAtomicCommand.CommandText = UCase(sql)
                            dbAtomicCommand.ExecuteNonQuery()

                            theSystemBiz.SaveAuditLogComment(dbAtomicCommand, "PAYMENT_SUCCESS", "AR_TRN(" & del_doc_num & ") " & dbReader.Item("MOP_CD").ToString & " transaction for " & dbReader.Item("AMT").ToString & " committed.")
                        End If
                    Loop   'Processing payments
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE COMMENTS     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing <> ProcessingType.SplitTw Then
                    'Insert Sales "S" Comments
                    theSalesBiz.SaveSalesComments(dbAtomicCommand, soKeys, Session("scomments"), Session("STORE_CD"), Session("tran_dt"))

                    'Insert Delivery "D" Comments
                    theSalesBiz.SaveDeliveryComments(dbAtomicCommand, soKeys, Session("dcomments"), Session("STORE_CD"), Session("tran_dt"))

                    'Insert A/R Comments
                    theSalesBiz.SaveARComments(dbAtomicCommand, Session("EMP_CD"), Session("cust_cd"), Session("arcomments"))
                End If

                'IT REQ 3354 Insert A/R Comments for finance
                If (approvalCd > "" And finCo > "") Then
                    Dim arCmnt As String = ""
                    arCmnt = Resources.LibResources.Label994 & " " & approvalCd & " " & Resources.LibResources.Label995 & " " & Session("emp_init") & " " & Resources.LibResources.Label996 & " " & del_doc_num
                    theSalesBiz.SaveARComments(dbAtomicCommand, Session("EMP_CD"), Session("cust_cd"), arCmnt)
                End If

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    START CLEANUP    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If doc_processing = ProcessingType.SplitTw Then
                    sql = "DELETE FROM TEMP_ITM WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH='Y'"
                    dbAtomicCommand.CommandText = sql
                    dbAtomicCommand.ExecuteNonQuery()
                End If

                If default_invoice & "" = "" Then default_invoice = theSalesBiz.GetDefaultInvoiceName(Session("STORE_CD"))

                doc_processing = doc_processing - 1
            Loop    ' Do While doc_processing > 0

            sql = "UPDATE RELATIONSHIP SET CONVERSION_DT=TO_DATE('" & FormatDateTime(DateTime.Today, DateFormat.ShortDate) & "','mm/dd/RRRR'), REL_STATUS='C' WHERE REL_NO='" & Session("Converted_REL_NO") & "'"
            dbAtomicCommand.CommandText = sql
            dbAtomicCommand.ExecuteNonQuery()

            'Process Swatch Sales and create a credit memo automatically
            If Session("SWATCH") = "TRUE" Then ProcessSwatchOrders(dbAtomicCommand, soKeys.soDocNum.delDocNum)

            'caches some of the relevant SESSION values
            Dim IPAD As String = Session("IPAD")
            Dim empCd As String = Session("EMP_CD")
            Dim empFName As String = Session("EMP_FNAME")
            Dim empLName As String = Session("EMP_LNAME")
            Dim orderTpCd As String = Session("ord_tp_cd")
            Dim homeStoreCd As String = Session("HOME_STORE_CD")
            Dim companyCd As String = Session("CO_CD")
            Dim empInit As String = Session("EMP_INIT")
            Dim transDate As String = Session("tran_dt")
            If Not String.IsNullOrEmpty(err_msg) Then
                ASPxPopupControl2.ContentUrl = "Payment_Hold.aspx?del_doc_num=" & del_doc_num
                ASPxPopupControl2.ShowOnPageLoad = True
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CLEANUP     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                theSalesBiz.DeleteTempOrder(dbAtomicCommand, Session.SessionID.ToString.Trim)
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<  COMMITS ALL CHANGES QUEUED DURING THIS WHOLE SAVE PROCESS >>>>>>>>>>>>>>>>>>>>>>>>
            ' FINANCE and XFER LIABILITY is OUTSIDE THE TRANSACTION FOR NOW, HAVE TO CHECK WITH DSA 
            dbAtomicCommand.Transaction.Commit()
            ' <<<<<<<<<<<<  WHAT HAPPENS BELOW THIS POINT, IS NOT WITHIN THE SINGLE TRANSACTION OBJECT >>>>>>>>>>>>>>>>>

            lbl_Follow_Up.Text = "Error Messages: " & vbCrLf

            If Session("cash_carry") = "TRUE" Then
                If Check_Split_Filled(del_doc_num, orderTpCd) = False Then
                    lbl_Follow_Up.Text = lbl_Follow_Up.Text & del_doc_num & " was not finalized due to insufficient inventory." & vbCrLf
                Else
                    Xfer_fi_Liability(del_doc_num, dbConnection)
                End If
            End If

            ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    SPLIT PMTS ON SPLIT - XFER FI LIAIBILITY     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            If split_order = True Then
                If split_amt > 0 Then
                    Process_Payment_Transfers(split_amt, del_doc_num, wdr, wdr_promo)
                End If

                If Check_Split_Filled(del_doc_num & "A", orderTpCd) = False Then
                    lbl_Follow_Up.Text = lbl_Follow_Up.Text & del_doc_num & "A was not finalized due to insufficient inventory." & vbCrLf
                Else
                    Xfer_fi_Liability(del_doc_num + "A", dbConnection)  ' the take-with document
                End If

                slsp1 = Session("slsp1")
                slsp2 = Session("slsp2")
                pct_1 = Session("pct_1")
                pct_2 = Session("pct_2")
                tet_cd = Session("tet_cd")
                STORE_ENABLE_ARS = Session("STORE_ENABLE_ARS")
                ZONE_CD = Session("ZONE_CD")
                PD = Session("PD")
                pd_store_cd = Session("pd_store_cd")
                cash_carry = Session("cash_carry")

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE INVOICE     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If String.IsNullOrEmpty(err_msg) Then

                    'lucy comment following 7 lines to avoid invoice popup
                    '  If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                    ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & del_doc_num & "','frmTestfg34','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikegf43tg", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & del_doc_num & "A','frmTestgf43tg','height=700px,width=700px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    ' ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                    ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & del_doc_num & "&INVOICE_NAME=" & default_invoice & "','frmTestfg34','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & del_doc_num & "A" & "&INVOICE_NAME=" & default_invoice & "','frmTestgf43tg','height=700px,width=700px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    'End If
                    Dim finCustCd As String = ""
                    sql = "SELECT SO.ORIG_FI_AMT, SO.APPROVAL_CD, SO.FIN_CUST_CD, SO_ASP.PROMO_CD, SO_ASP.AS_PROMO_CD FROM SO, SO_ASP " &
                                " WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & del_doc_num & "' AND ORIG_FI_AMT IS NOT NULL"

                    dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If (dbReader.Read) Then
                        finCustCd = dbReader.Item("FIN_CUST_CD").ToString
                        Finance_Split_Print(del_doc_num, dbReader.Item("PROMO_CD").ToString, finCustCd, wdr)
                    End If

                    'Print down payment type finance transactions
                    If wdr = True And wdr_promo & "" <> "" Then Finance_Split_Print(del_doc_num, wdr_promo, finCustCd, wdr)

                    'lucy comment 4 lines
                    ' If chk_picr.Checked = True And ConfigurationManager.AppSettings("picr").ToString = "Y" Then
                    'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnySc543665ffdsegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & del_doc_num & "','frhfgdhgffasdfgf43tg','height=700px,width=700px,top=50,left=50,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNaasdffdsegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & del_doc_num & "A','frmTasdfasdfgf43tg','height=700px,width=700px,top=60,left=60,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    ' End If
                End If

            Else ' order not split
                If String.IsNullOrEmpty(err_msg) Then
                    If Not String.IsNullOrEmpty(lbl_fin_co.Text.ToString) Then
                        Dim fin_invoice As String = theSalesBiz.GetAddendumInvoiceName(lbl_fin_co.Text, finCo, wdr)
                        'lucy comment   '   ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike87", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                        cbo_finance_addendums.Enabled = False

                    ElseIf Not String.IsNullOrEmpty(cbo_finance_addendums.SelectedValue.ToString) Then
                        'Commented by - SKALAKAP on 1/30/2013
                        ' In POS+ 2013.1.5 build 22,We were getting duplicate report for FI_addendums.After discussing with Anju.I commented below code.
                        'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike95", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & cbo_finance_addendums.SelectedValue.ToString & "','frmFinance" & doc_processing & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

                        Dim finCustCd As String = ""
                        sql = "SELECT SO.ORIG_FI_AMT, SO.APPROVAL_CD, SO.FIN_CUST_CD, SO_ASP.PROMO_CD, SO_ASP.AS_PROMO_CD FROM SO, SO_ASP " &
                                    " WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & del_doc_num & "' AND ORIG_FI_AMT IS NOT NULL"

                        dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If (dbReader.Read) Then
                            finCustCd = dbReader.Item("FIN_CUST_CD").ToString
                        End If

                        'Print down payment type finance transactions
                        If wdr = True And wdr_promo & "" <> "" And isNotEmpty(finCustCd) Then Finance_Split_Print(del_doc_num, wdr_promo, finCustCd, wdr)
                        cbo_finance_addendums.Enabled = False
                    End If
                    'lucy comment follwoing if condition
                    'If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                    'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/" & default_invoice & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & del_doc_num & "&EMAIL=" & chk_email.Checked & "','frmTestfgd543g','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    ' ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
                    ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & del_doc_num & "&INVOICE_NAME=" & default_invoice & "&EMAIL=" & chk_email.Checked & "','frmInvoice','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    'End If
                End If
                'lucy comment follwoing if condition
                ' If chk_picr.Checked = True And ConfigurationManager.AppSettings("picr").ToString = "Y" Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnySc543665ffdsegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & del_doc_num & "','frhfgdhgffasdfgf43tg','height=700px,width=700px,top=50,left=50,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ' End If

                If Not String.IsNullOrEmpty(err_msg) Then
                    lbl_default_invoice.Text = default_invoice
                    lbl_del_doc_num.Text = del_doc_num
                    lbl_split.Text = split_order
                    lbl_fin.Text = finCo
                    lbl_wdr.Text = wdr
                    ASPxButton1.Visible = True
                End If
            End If

            lbl_Follow_Up.ForeColor = Color.Red
            lbl_Follow_Up.Text = lbl_Follow_Up.Text & Session("err") & vbCrLf
            If Len(lbl_Follow_Up.Text) = 20 Then lbl_Follow_Up.Visible = False

            If Find_Security("POGEN", Session("emp_cd")) = "Y" AndAlso Session("ord_tp_cd") = "SAL" AndAlso Session("cash_carry") <> "TRUE" Then
                btn_generate_po.Visible = True
            End If

            'Oct 26,2016 - mariam - Save Aeroplan card Promo
            If Not Session("AeroplanCardNum") Is Nothing Then
                OrderUtils.insAeroplanStaging(del_doc_num, Session("AeroplanCardNum").ToString())
            End If
            'End - mariam
            ' restores the Session values from the cached values above to ready for the next transaction
            Dim partial_data As String = Session(del_doc_num & "_CMNT")
            Dim hdrInf As SoHeaderDtc
            ' Need to capture the info before it is gone
            ' Daniela Comment - code was move up
            ' If ConfigurationManager.AppSettings("package_breakout").ToString = AppConstants.PkgSplit.ON_COMMIT Then
            ' create the object for header and transaction info        
            'hdrInf = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim, slsp1, slsp2, pct_1, IIf(IsNothing(pct_2), 0, pct_2),
            '                               tet_cd, ttax_cd, STORE_ENABLE_ARS, ZONE_CD, PD, pd_store_cd, cash_carry)
            'hdrInf.delDocNum = del_doc_num
            ' If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
            ' hdrInf.writtenDt = Session("tran_dt")
            ' hdrInf.orderTpCd = SessVar.ordTpCd
            ' TODO - setting tax method here because it should eventually be based on tax code
            ' hdrInf.taxMethod = IIf(ConfigurationManager.AppSettings("tax_line").ToString = "Y", "L", "H")

            'End If


            ' Daniela save clientip
            Dim client_IP As String = Session("clientip")
            Dim supUser As String = Session("str_sup_user_flag")
            Dim coGrpCd As String = Session("str_co_grp_cd")
            Dim userType As String = Session("USER_TYPE")
            Dim emailFlag As String = Session("email_checked")
            Session.Clear()
            Session("clientip") = client_IP
            Session("str_sup_user_flag") = supUser ' Daniela
            Session("str_co_grp_cd") = coGrpCd ' Daniela
            Session("USER_TYPE") = userType ' Daniela
            Session(del_doc_num & "_CMNT") = partial_data
            Session("IPAD") = IPAD
            Session("EMP_CD") = empCd
            Session("EMP_FNAME") = empFName
            Session("EMP_LNAME") = empLName
            Session("HOME_STORE_CD") = homeStoreCd
            Session("CO_CD") = companyCd
            Session("EMP_INIT") = empInit
            If Not IsNothing(hdrInf) Then
                Session("HdrInfo") = hdrInf
            End If
            Session("tran_dt") = transDate
            Session("isCommited") = True
            Session("lucy_display") = ""
            Dim str_emp_cd As String = Session("emp_cd")   'lucy
            'May 22 IT Req 2919
            Dim emailString = ""
            If LeonsBiz.CheckStoreGroup("EER", Session("HOME_STORE_CD")) Then
                If (isNotEmpty(emailFlag) And isNotEmpty(email.Text)) Then
                    If (LeonsBiz.ValidateEmail(email.Text)) Then
                        emailString = email.Text
                    End If
                End If
                print_salesbill_byemail(del_doc_num, str_emp_cd, p_printer, emailString)
            Else
                lucy_print_salesbill(del_doc_num, str_emp_cd, p_printer)  'lucy
            End If

        Catch ex As Exception
            dbAtomicCommand.Transaction.Rollback()
            Dim err As String = ex.ToString
            Throw
        Finally
            dbAtomicCommand.Dispose()
            dbAtomicConnection.Close()
            dbConnection.Close()
        End Try
        ''This section is to initialize session after invoice is redirected, all the session will get clear, when page is redirected to some other page.
        'TODO  - are we sure about that because we are in trou8ble if the above session variables that got reset after clear get cleared
        'TODO - the fix for this is to make an invoice parameter that prints component price split out or not, then move this into the process_soln process
        ''This change is required to updating so_ln table after invoice is generated for the new option "Upon Commit" for the system parameter breaking out price of package

        ' Daniela comment - code was move up
        ' If ConfigurationManager.AppSettings("package_breakout").ToString = AppConstants.PkgSplit.ON_COMMIT AndAlso
        'Not IsNothing(Session("hdrInfo")) Then

        'Dim hdrInf As SoHeaderDtc = Session("hdrInfo")

        'If hdrInf.orderTpCd = AppConstants.Order.TYPE_SAL OrElse hdrInf.orderTpCd = AppConstants.Order.TYPE_CRM Then

        'If ttax_cd.isNotEmpty Then

        'Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(hdrInf.writtenDt, hdrInf.taxCd)
        'hdrInf.taxRates = taxPcts.itmTpPcts  'SetTxPcts(headerInfo.soWrDt, headerInfo.taxCd).itmTpPcts
        ' End If

        ' theSalesBiz.splitSoLnPkgCmpntPrcs(hdrInf)
        'End If
        'End If
        Session.Remove("hdrInfo")
        'MM-5693
        If SysPms.isAutoFinalizeMCRMDB AndAlso Order_Type.Equals(AppConstants.Order.TYPE_MCR) Then
            ASPxPopupControl3.ShowOnPageLoad = True
        End If

    End Sub



    'MM-5693
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Dim URL_String As String
        URL_String = "?ip_ord_total=" & lbl_ord_total.Text & "&ip_ivc_cd=" & lbl_ivc_cd.Text & "&referrer=Order_Stage.aspx" & "&pmt_tp_cd=R"
        Response.Redirect("independentpaymentprocessing.aspx" & URL_String, False)
    End Sub
    'MM-5693
    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        ASPxPopupControl3.ShowOnPageLoad = False
    End Sub
    Private Enum ProcessingType
        ' indicates what type of document is currently being processed in 
        SingleOrFirstDoc = 1
        SplitTw = 2
    End Enum

    ''' <summary>
    ''' Inserts into SO_LN2DISC table, all discounts that were created for the document passed in
    ''' </summary>
    ''' <param name="delDocNum">the document number the lines belong to</param>
    ''' <param name="docProcessing">indicates if processing a regular or Take-with document when created at the same time</param>
    ''' <param name="dbAtomicCommand">a command to execute the DB processing, it's part of the same transaction  as the save process</param>
    Private Sub ProcessSoLnDiscounts(ByVal delDocNum As String, ByVal docProcessing As Integer, ByRef dbAtomicCommand As OracleCommand)

        Dim dbReader As OracleDataReader
        Dim sbSql As StringBuilder = New StringBuilder()
        Dim delDocLnNum As String = String.Empty
        Dim discPct As Object
        Dim discSeqNum As Integer = 1

        sbSql.Append("SELECT TEMP_ITM.SO_LN_NO, MULTI_DISC.DISC_AMT, MULTI_DISC.DISC_CD, MULTI_DISC.DISC_PCT, MULTI_DISC.DISC_LEVEL ")
        sbSql.Append("FROM TEMP_ITM, MULTI_DISC ")
        sbSql.Append("WHERE TEMP_ITM.SESSION_ID = :sessId AND TEMP_ITM.ROW_ID = MULTI_DISC.LINE_ID ")
        If (ProcessingType.SplitTw = docProcessing) Then
            sbSql.Append("AND TAKE_WITH='Y' ")
        End If
        sbSql.Append("ORDER BY row_id")

        dbAtomicCommand.CommandText = sbSql.ToString()
        dbAtomicCommand.Parameters.Add(":sessId", OracleType.VarChar)
        dbAtomicCommand.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
        dbReader = DisposablesManager.BuildOracleDataReader(dbAtomicCommand)   'has to use same command, otherwise SO_LN_NO values are empty


        If (dbReader.HasRows()) Then
            sbSql.Clear()
            sbSql.Append("INSERT INTO SO_LN2DISC (DEL_DOC_NUM, DEL_DOC_LN#, DISC_CD, DISC_AMT, DISC_PCT, DISC_LEVEL, SEQ_NUM) ")
            sbSql.Append("VALUES(:delDocNum, :delDocLnNum, :discCd, :discAmt, :discPct, :discLevel, :discSeqNum)")

            dbAtomicCommand.CommandText = sbSql.ToString()
            dbAtomicCommand.Parameters.Clear()
            dbAtomicCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":delDocLnNum", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discAmt", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discPct", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discLevel", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discSeqNum", OracleType.VarChar)

            Do While dbReader.Read()
                'first line being processed
                If delDocLnNum.isEmpty Then delDocLnNum = dbReader.Item("SO_LN_NO").ToString

                'second to last line being processed
                If delDocLnNum <> dbReader.Item("SO_LN_NO").ToString Then
                    delDocLnNum = dbReader.Item("SO_LN_NO").ToString
                    discSeqNum = 1     'to start a new sequence number for the next order line
                End If

                discPct = DBNull.Value
                If (Not IsDBNull(dbReader.Item("DISC_PCT"))) Then discPct = dbReader.Item("DISC_PCT").ToString

                dbAtomicCommand.Parameters(":delDocNum").Value = delDocNum
                dbAtomicCommand.Parameters(":delDocLnNum").Value = delDocLnNum
                dbAtomicCommand.Parameters(":discCd").Value = dbReader.Item("DISC_CD").ToString
                dbAtomicCommand.Parameters(":discAmt").Value = dbReader.Item("DISC_AMT").ToString
                dbAtomicCommand.Parameters(":discPct").Value = discPct
                dbAtomicCommand.Parameters(":discLevel").Value = dbReader.Item("DISC_LEVEL").ToString
                dbAtomicCommand.Parameters(":discSeqNum").Value = discSeqNum
                dbAtomicCommand.ExecuteNonQuery()

                discSeqNum = discSeqNum + 1
            Loop
        End If
        dbReader.Close()
        dbAtomicCommand.Parameters.Clear()

    End Sub

    Private Sub ProcessSoLn(ByVal del_doc_num As String, ByVal doc_processing As Integer, ByRef cmdErp As OracleCommand)
        Dim objSql As OracleCommand ' lucy
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection ' lucy
        Dim connString As String 'lucy

        Dim cmdLocal As OracleCommand = cmdErp
        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim sqlInsert As New StringBuilder   ' sql
        Dim sqlValues As New StringBuilder   ' sql2

        Dim soLn_num As Integer = 1   '  set to 1 for each document and incremented for each SO_LN for the del_doc_ln# assignment
        Dim itmInfo As DataRow

        Dim cst As Double = 0
        Dim pkg_itm_cd As String = ""
        Dim itmTrnInsReq As InventoryUtils.ItmTrnInsertReq
        Dim invResReq As InventoryUtils.Inv_Reserve_Req

        Dim tempItmRow As DataRow
        ' warranty linkage requires knowing the offset of the warranty line to set the warranty row link in so_ln2warr
        Dim tmpItmAllTbl As DataTable      ' database extract for all non-0 rows for the session
        Dim tmpItmCurrTbl As New DataTable  ' the current table, take-with or non-take-with being processed 
        Dim tmpItmTwTbl As DataTable        ' take-with temp_itm records
        Dim tmpItmNonTwTbl As DataTable     ' non take-with temp_itm records
        Dim soLnRdr As OracleDataReader

        Dim firstRowId As Integer = 0  ' used to create offset for warranty line pointers; will only work as long as order is by row_id asc
        Dim tmpItmDs As New DataSet
        Dim tmpItmTbl As New DataTable
        Dim where As String = ""
        Dim tmpItm() As DataRow    ' internal SQL result - array of datarows
        Dim offSet As Integer = 0

        ' If there are both take-with and non-take-with lines on this transaction, this process will first process the 'A' doc which
        ' is the take-with doc and process all the take-with lines, then remove them from temp_itm and process the rest of the lines;
        ' now all are extracted into a dataset and then set above to two separate datatables for use during warranty logic
        Try

            ' Do not save 0 quantity lines
            tmpItmAllTbl = HBCG_Utils.GetTempItmInfoBySingleKey(Session.SessionID.ToString.Trim,
                                                                    temp_itm_key_tp.tempItmBySessionId,
                                                                    temp_itm_where_clause.qtyGreaterZero)
            If tmpItmAllTbl.Rows.Count > 0 Then
                ' for warranty links, we need both sets of data
                If Session("cash_carry") <> "TRUE" Then  'lucy add if condition
                    tmpItmNonTwTbl = (From datRow In tmpItmAllTbl.AsEnumerable() Where datRow.Field(Of String)("take_with").ToUpper.Equals("N") Select datRow).CopyToDataTable
                Else
                    tmpItmNonTwTbl = (From datRow In tmpItmAllTbl.AsEnumerable() Where datRow.Field(Of String)("take_with").ToUpper.Equals("Y") Select datRow).CopyToDataTable
                End If


                'lucy ' tmpItmNonTwTbl = (From datRow In tmpItmAllTbl.AsEnumerable() Where datRow.Field(Of String)("take_with").ToUpper.Equals("N") Select datRow).CopyToDataTable
                If doc_processing = ProcessingType.SplitTw Then

                    tmpItmCurrTbl = (From datRow In tmpItmAllTbl.AsEnumerable() Where datRow.Field(Of String)("take_with").ToUpper.Equals("Y") Select datRow).CopyToDataTable
                    tmpItmTwTbl = tmpItmCurrTbl.Copy
                Else
                    tmpItmCurrTbl = tmpItmNonTwTbl
                End If

                ' need the first row_id sequence number to get the offset warranty from
                Dim firstRow As DataRow = tmpItmAllTbl.Rows(0)
                firstRowId = CInt(firstRow.Item("ROW_ID").ToString)
                'Dim filterText As String = String.Empty
                'Dim filteredRows As DataRow()
                'Dim pkgRowId As String = String.Empty
                For Each tempItmRow In tmpItmCurrTbl.Rows

                    sqlInsert.Clear()
                    sqlValues.Clear()

                    sqlInsert.Append("INSERT INTO SO_LN (DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, COMM_CD, SO_DOC_NUM, QTY, UNIT_PRC, PICKED, VOID_FLAG ")
                    sqlValues.Append(" VALUES('" & del_doc_num & "'," & soLn_num & ",'" & Replace(tempItmRow.Item("ITM_CD").ToString, "'", "''") & "','" & Replace(tempItmRow.Item("COMM_CD").ToString, "'", "''") & "'")
                    sqlValues.Append(",'" & Left(del_doc_num, 11) & "'," & tempItmRow.Item("QTY").ToString & "," & tempItmRow.Item("RET_PRC").ToString & ",")
                    If tempItmRow.Item("ITM_TP_CD").ToString = "INV" Then
                        sqlValues.Append("'Z'")
                    Else
                        sqlValues.Append("'X'")
                    End If
                    sqlValues.Append(",'N'")
                    If (Session("ord_tp_cd") = "CRM" OrElse Session("ord_tp_cd") = "MCR") AndAlso SystemUtils.isNumber(tempItmRow.Item("orig_so_ln_seq").ToString() + "") Then

                        sqlInsert.Append(", alt_doc_ln# ")
                        sqlValues.Append(", " + tempItmRow.Item("orig_so_ln_seq").ToString())
                    End If
                    sql = sqlInsert.ToString
                    sql2 = sqlValues.ToString  ' TODO - future continue to change this to stringbuilder 

                    'when saving PKG Components, the PKG Itm-cd is needed
                    If (tempItmRow.Item("ITM_TP_CD").ToString = "PKG") Then
                        pkg_itm_cd = tempItmRow.Item("ITM_CD").ToString
                        'pkgRowId = soLn_num
                    End If
                    If (tempItmRow.Item("PACKAGE_PARENT").ToString).isNotEmpty Then
                        sql = sql & ", PKG_SOURCE"
                        sql2 = sql2 & ", '" & pkg_itm_cd & "'"
                        'filterText = "ITM_CD ='" + tempItmRow.Item("ITM_CD").ToString + "' AND PACKAGEROWID = " + tempItmRow.Item("PACKAGE_PARENT").ToString
                        'filteredRows = dtSplitData.Select(filterText)
                        'filteredRows(0)("PACKAGEROWID") = pkgRowId
                    End If

                    Dim pct1 As Double = 0
                    'Alice update on Feb 22, 2019 for request 4477
                    Dim slsp1 As String = ""
                    Dim slsp2 As String = ""

                    'Alice added on May 16, 2019 for request 81
                    'slsp1 = tempItmRow.Item("SLSP1").ToString

                    'If Session("str_co_grp_cd").ToString = "BRK" Then
                    '    Dim del_doc_tab As DataTable = OrderUtils.GetOrderDetails(Session("adj_ivc_cd"))
                    '    If Not AppConstants.Order.TYPE_SAL.Equals(Session("ord_tp_cd")) Then
                    '        If del_doc_tab.Rows.Count = 0 Then
                    '            slsp1 = "HOU" & Session("store_cd").ToString.Trim
                    '        End If
                    '    End If
                    'End If

                    If Session("Orig_slsp1_forExchangeSo") IsNot Nothing Then
                        If Session("Orig_slsp1_forExchangeSo").ToString <> "" Then
                            slsp1 = Session("Orig_slsp1_forExchangeSo")
                        Else
                            slsp1 = Session("slsp1")
                        End If
                    Else
                        slsp1 = Session("slsp1")
                    End If

                    If Session("Orig_slsp2_forExchangeSo") IsNot Nothing Then
                        If Session("Orig_slsp2_forExchangeSo").ToString <> "" Then
                            slsp2 = Session("Orig_slsp2_forExchangeSo")
                        Else
                            slsp2 = Session("slsp2")
                        End If
                    Else
                        slsp2 = Session("slsp2")
                    End If

                    If slsp1 & "" <> "" Then
                        sql = sql & ", SO_EMP_SLSP_CD1, PCT_OF_SALE1"
                        sql2 = sql2 & ", '" & Replace(slsp1, "'", "''") & "','"
                        If IsNumeric(tempItmRow.Item("SLSP1_PCT").ToString) Then
                            pct1 = tempItmRow.Item("SLSP1_PCT").ToString
                        Else
                            'pct1 = Session("pct_1")
                            pct1 = IIf(Session("pct_1").ToString = "", 0, Session("pct_1"))

                        End If
                        sql2 = sql2 & pct1.ToString & "'"
                    End If

                    If slsp2 & "" <> "" Then
                        sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                        sql2 = sql2 & ", '" & Replace(slsp2, "'", "''") & "','"
                        sql2 = sql2 & (100 - pct1).ToString & "'"
                    End If

                    'If tempItmRow.Item("SLSP1").ToString & "" <> "" Then
                    '    sql = sql & ", SO_EMP_SLSP_CD1, PCT_OF_SALE1"
                    '    sql2 = sql2 & ", '" & Replace(tempItmRow.Item("SLSP1").ToString, "'", "''") & "','"
                    '    If IsNumeric(tempItmRow.Item("SLSP1_PCT").ToString) Then
                    '        pct1 = tempItmRow.Item("SLSP1_PCT").ToString
                    '    Else
                    '        pct1 = Session("pct_1")
                    '    End If
                    '    sql2 = sql2 & pct1.ToString & "'"
                    'End If


                    'If tempItmRow.Item("SLSP2").ToString & "" <> "" Then
                    '    sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                    '    sql2 = sql2 & ", '" & Replace(tempItmRow.Item("SLSP2").ToString, "'", "''") & "','"
                    '    sql2 = sql2 & (100 - pct1).ToString & "'"
                    'End If




                    If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                        If Session("TAX_CD") & "" <> "" And Session("tet_cd") & "" = "" Then
                            sql = sql & ", TAX_CD "
                            sql2 = sql2 & ", '" & Session("TAX_CD") & "'"
                        End If
                        sql = sql & ", TAX_RESP"
                        sql2 = sql2 & ", 'C' "
                        If IsNumeric(tempItmRow.Item("TAXABLE_AMT").ToString) Then
                            sql = sql & ", TAX_BASIS"
                            sql2 = sql2 & ", " & tempItmRow.Item("TAXABLE_AMT").ToString & ""
                            If IsNumeric(tempItmRow.Item("TAX_PCT").ToString) Then
                                sql = sql & ", CUST_TAX_CHG"
                                sql2 = sql2 & ", " & Replace(FormatNumber((CDbl(tempItmRow.Item("TAXABLE_AMT").ToString) * CDbl(tempItmRow.Item("QTY").ToString)) * (CDbl(tempItmRow.Item("TAX_PCT").ToString) / 100), 2), ",", "") & " "
                            End If
                        End If
                    End If
                    If tempItmRow.Item("DISC_AMT").ToString & "" <> "" Then
                        If IsNumeric(tempItmRow.Item("DISC_AMT").ToString) Then
                            sql = sql & ", DISC_AMT"
                            sql2 = sql2 & ", " & CDbl(tempItmRow.Item("DISC_AMT").ToString) & ""
                        End If
                    End If
                    If tempItmRow.Item("LEAVE_CARTON").ToString = "Y" Then
                        sql = sql & ", LV_IN_CARTON"
                        sql2 = sql2 & ", '" & tempItmRow.Item("LEAVE_CARTON").ToString & "'"
                    Else
                        sql = sql & ", LV_IN_CARTON"
                        sql2 = sql2 & ", 'N'"
                    End If
                    If SysPms.commissionOnDelvCharge Then
                        sql = sql & ", COMM_ON_DEL_CHG"
                        sql2 = sql2 & ", 'Y'"
                    Else
                        sql = sql & ", COMM_ON_DEL_CHG"
                        sql2 = sql2 & ", 'N'"
                    End If
                    If SysPms.commissionOnSetupCharge Then
                        sql = sql & ", COMM_ON_SETUP_CHG"
                        sql2 = sql2 & ", 'Y'"
                    Else
                        sql = sql & ", COMM_ON_SETUP_CHG"
                        sql2 = sql2 & ", 'N'"
                    End If
                    sql = sql & ", SPIFF"
                    If IsNumeric(tempItmRow.Item("SPIFF").ToString) AndAlso
                        CDbl(tempItmRow.Item("SPIFF").ToString) = Math.Abs(CDbl(tempItmRow.Item("SPIFF").ToString)) Then ' matching E1, put in if 0 anyway
                        sql2 = sql2 & ", " & CDbl(tempItmRow.Item("SPIFF").ToString)
                    Else
                        sql2 = sql2 & ", 0" ' matching E1, put in if 0 anyway
                    End If

                    Try
                        If tempItmRow.Item("TREATED").ToString = "Y" Then
                            If IsNumeric(tempItmRow.Item("TREATED_BY").ToString) Then
                                If CDbl(tempItmRow.Item("TREATED_BY").ToString) > 0 Then
                                    sql = sql & ", TREATED_BY_LN#"
                                    sql2 = sql2 & ", " & Math.Abs(CDbl(tempItmRow.Item("TREATED_BY").ToString) - CDbl(tempItmRow.Item("ROW_ID").ToString)) + soLn_num
                                    If Get_Temp_ITM_CD(tempItmRow.Item("TREATED_BY").ToString) & "" <> "" Then
                                        sql = sql & ", TREATED_BY_ITM_CD"
                                        sql2 = sql2 & ", '" & Get_Temp_ITM_CD(tempItmRow.Item("TREATED_BY").ToString) & "' "
                                    End If
                                Else
                                    If IsNumeric(Get_First_Fab) Then
                                        sql = sql & ", TREATED_BY_LN#"
                                        sql2 = sql2 & ", " & Math.Abs(CDbl(Get_First_Fab.ToString) - CDbl(tempItmRow.Item("ROW_ID").ToString)) + soLn_num
                                        If Get_Temp_ITM_CD(Get_First_Fab) & "" <> "" Then
                                            sql = sql & ", TREATED_BY_ITM_CD"
                                            sql2 = sql2 & ", '" & Get_Temp_ITM_CD(Get_First_Fab) & "' "
                                        End If
                                    End If
                                End If
                            Else
                                If IsNumeric(Get_First_Fab) Then
                                    sql = sql & ", TREATED_BY_LN#"
                                    sql2 = sql2 & ", " & Math.Abs(CDbl(Get_First_Fab.ToString) - CDbl(tempItmRow.Item("ROW_ID").ToString)) + soLn_num
                                    If Get_Temp_ITM_CD(Get_First_Fab) & "" <> "" Then
                                        sql = sql & ", TREATED_BY_ITM_CD"
                                        sql2 = sql2 & ", '" & Get_Temp_ITM_CD(Get_First_Fab) & "' "
                                    End If
                                End If
                            End If
                        End If
                    Finally
                    End Try

                    If tempItmRow.Item("warr_row_link").ToString.isNotEmpty AndAlso
                        IsNumeric(tempItmRow.Item("warr_row_link").ToString) AndAlso
                        tempItmRow.Item("warr_row_link").ToString > 0 AndAlso
                        tempItmRow.Item("warr_itm_link").ToString.isNotEmpty Then

                        If tempItmRow.Item("warrantable").ToString = "Y" Then

                            Dim soLnWarr As New SalesUtils.SoLnWarrDtc
                            soLnWarr.docNum = del_doc_num
                            soLnWarr.docLnNum = soLn_num
                            soLnWarr.warrDocNum = del_doc_num
                            where = "row_id >= " + firstRowId.ToString + " AND row_id <= " + tempItmRow.Item("warr_row_link").ToString
                            tmpItm = tmpItmAllTbl.Select(where)
                            offSet = tmpItm.GetUpperBound(0)
                            'Nov 23 add MCR to validation
                            ' Daniela fix yellow triangle CRMs with warranty
                            If Not ((Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") And offSet < 0) Then


                                ' if warranty not on take-with, then make sure it points to non-take-with doc
                                If tmpItm(offSet).Item("Take_With").ToString = "N" Then
                                    soLnWarr.warrDocNum = Left(del_doc_num, 11)
                                End If
                                ' If warrantable and warranty on the same doc, then calc the offset from the current table being processed
                                ' Need to get the offset for the warranty from the table that the warranty in
                                If tempItmRow.Item("take_with").ToString = tmpItm(offSet).Item("Take_With").ToString Then

                                    tmpItm = tmpItmCurrTbl.Select(where)

                                ElseIf tempItmRow.Item("take_with").ToString = "Y" Then  ' warr is not take-with

                                    tmpItm = tmpItmNonTwTbl.Select(where)

                                Else   ' warrantable is not take-with, warr is take-with
                                    tmpItm = tmpItmTwTbl.Select(where)
                                End If
                                ' this numbering only works because there are one or two sets of data and we just get the
                                '    off set from the one the warranty is in
                                offSet = tmpItm.GetUpperBound(0)
                                soLnWarr.warrDocLnNum = offSet + 1
                                SalesUtils.InsertSoLnWarr(soLnWarr, cmdErp)
                                'sql = sql & ", warranted_by_ln#, warranted_by_itm_cd"
                                'sql2 = sql2 & ", " & Math.Abs(CDbl(tempItmRdr.Item("warr_row_link").ToString) - CDbl(tempItmRdr.Item("ROW_ID").ToString)) + soLn_num &
                                '    ", '" & tempItmRdr.Item("warr_itm_link").ToString & "' "

                                ' no more so_ln pointers, now use the So_ln2warr table
                                'Else  ' it is the warranty line
                                '    sql = sql & ", ref_del_doc_ln#, ref_itm_cd"
                                '    sql2 = sql2 & ", " & soLn_num - Math.Abs(CDbl(tempItmRdr.Item("warr_row_link").ToString) - CDbl(tempItmRdr.Item("ROW_ID").ToString)) &
                                '        ", '" & tempItmRdr.Item("warr_itm_link").ToString & "' "
                            End If
                        End If
                    End If

                    If (tempItmRow.Item("PRC_CHG_APP_CD").ToString.isNotEmpty) Then
                        sql = sql & ", PRC_CHG_APP_CD"
                        sql2 = sql2 & ", '" & tempItmRow.Item("PRC_CHG_APP_CD").ToString & "'"
                    End If

                    If IsNumeric(tempItmRow.Item("DEL_PTS").ToString) Then
                        If CDbl(tempItmRow.Item("DEL_PTS").ToString) > 0 Then
                            sql = sql & ", DELIVERY_POINTS"
                            sql2 = sql2 & ", " & CDbl(tempItmRow.Item("DEL_PTS").ToString)
                        End If
                    End If
                    If tempItmRow.Item("ID_NUM").ToString & "" <> "" Then
                        sql = sql & ", ID_NUM"
                        sql2 = sql2 & ", '" & tempItmRow.Item("ID_NUM").ToString & "'"
                    End If
                    sql = sql & ", TAKEN_WITH"
                    If doc_processing = ProcessingType.SplitTw Or tempItmRow.Item("TAKE_WITH").ToString = "Y" Or Session("cash_carry") = "TRUE" Then
                        sql2 = sql2 & ", 'Y'"
                    Else
                        sql2 = sql2 & ", 'N'"  ' E1 always fills this in
                    End If
                    ' pu_del_dt is only maintained on SO_LN in E1 for RES prior to sale conversion where will split to multi docs; 
                    '  multi pu_del_dt on SO is not allowed and this value is not filled in in E1 - per DSA
                    sql = sql & ")"
                    sql2 = sql2 & ")"
                    sql = sql & sql2

                    sql = UCase(sql)
                    cmdErp.Parameters.Clear()
                    cmdErp.CommandText = sql
                    cmdErp.CommandType = CommandType.Text

                    cmdErp.ExecuteNonQuery() 'lucy comment




                    'Build the SO_LN correlation
                    sql = "UPDATE TEMP_ITM SET SO_LN_NO=" & soLn_num & " WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND ROW_ID=" & tempItmRow.Item("ROW_ID")

                    cmdLocal.Parameters.Clear()
                    cmdLocal.CommandText = sql
                    cmdLocal.CommandType = CommandType.Text
                    cmdLocal.ExecuteNonQuery()  'lucy comment


                    '***insert an auto comment for price change
                    If (tempItmRow.Item("PRC_CHG_APP_CD").ToString.isNotEmpty) Then
                        'HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Today.Date, "SRC", tempItmRow("ORIG_PRC").ToString, tempItmRow("RET_PRC").ToString, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)
                        'if no discount applied
                        Dim discAmount As Double = 0
                        Double.TryParse(tempItmRow.Item("DISC_AMT").ToString, discAmount)
                        If (discAmount = 0) Then
                            'HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Today.Date, "SRC", tempItmRow("ORIG_PRC").ToString, tempItmRow("RET_PRC").ToString, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)

                            'Alice updated for request 294 - SO_CMNT.SO_WR_DT should be same as SO.SO_WR_DT, otherwise the comment can't be shown on SOM comments tab
                            HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "SRC", tempItmRow("ORIG_PRC").ToString, tempItmRow("RET_PRC").ToString, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)
                        Else
                            'when discount applied
                            Dim retailPrice As Double = 0
                            Double.TryParse(tempItmRow("RET_PRC").ToString, retailPrice)

                            'HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Today.Date, "SRC", tempItmRow("ORIG_PRC").ToString, retailPrice + discAmount, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)
                            'HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Today.Date, "DISC", retailPrice + discAmount, retailPrice, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)

                            'Alice updated for request 294 - SO_CMNT.SO_WR_DT should be same as SO.SO_WR_DT, otherwise the comment can't be shown on SOM comments tab
                            HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "SRC", tempItmRow("ORIG_PRC").ToString, retailPrice + discAmount, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)
                            HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "DISC", retailPrice + discAmount, retailPrice, tempItmRow.Item("PRC_CHG_APP_CD").ToString, soLn_num)


                        End If
                    End If
                    'Update inventory if allocated on the sales order line
                    If tempItmRow.Item("STORE_CD").ToString & "" <> "" And tempItmRow.Item("LOC_CD").ToString & "" <> "" Then

                        invResReq = New InventoryUtils.Inv_Reserve_Req
                        invResReq.qty = tempItmRow.Item("QTY").ToString
                        invResReq.del_Doc_Num = del_doc_num
                        invResReq.ln_Num = soLn_num
                        invResReq.itm_Cd = tempItmRow.Item("ITM_CD").ToString
                        invResReq.store_Cd = tempItmRow.Item("STORE_CD").ToString
                        invResReq.loc_Cd = tempItmRow.Item("LOC_CD").ToString
                        invResReq.pu_Del = Session("PD")
                        invResReq.ser_Num = tempItmRow.Item("SERIAL_NUM").ToString
                        invResReq.out_Id = tempItmRow.Item("OUT_ID").ToString
                        invResReq.fifo_Cst = 0.0
                        invResReq.out_Cd = ""

                        If tempItmRow.Item("ITM_TP_CD").ToString = "INV" Or tempItmRow.Item("ITM_TP_CD").ToString = "ACC" Or Session("cash_carry") <> "TRUE" Then   ' lucy 22-apr-15, to prevent check NLN itm
                            InventoryUtils.Debit_Inventory(invResReq, cmdErp)
                        End If
                        'remove Pending reservation
                        UnreserveInventory(tempItmRow)
                    Else
                        ProcessPOLinks(tempItmRow, del_doc_num, soLn_num, cmdLocal)
                    End If

                    ' if processing a take-with (part of multi-doc) or one cash and carry doc, then finalize stuff
                    If doc_processing = ProcessingType.SplitTw And InStr(Session("err"), "Insufficient") < 1 Or Session("cash_carry") = "TRUE" And InStr(Session("err"), "Insufficient") < 1 Then

                        itmInfo = SkuUtils.GetItemInfo(tempItmRow("ITM_CD").ToString())

                        sql = "SELECT FIFO_CST, FIFO_DT, FIFL_DT FROM SO_LN where DEL_DOC_NUM='" & del_doc_num & "' and DEL_DOC_LN#=" & soLn_num

                        cmdErp.Parameters.Clear()
                        cmdErp.CommandText = sql
                        cmdErp.CommandType = CommandType.Text
                        Dim disc_line As String = ""
                        itmTrnInsReq = New InventoryUtils.ItmTrnInsertReq
                        itmTrnInsReq.qty = CDbl(tempItmRow.Item("QTY").ToString)
                        itmTrnInsReq.delDocNum = del_doc_num
                        itmTrnInsReq.itmCd = tempItmRow.Item("ITM_CD").ToString
                        itmTrnInsReq.oldStoreCd = tempItmRow.Item("STORE_CD").ToString
                        itmTrnInsReq.oldLocCd = tempItmRow.Item("LOC_CD").ToString
                        itmTrnInsReq.lnNum = soLn_num
                        itmTrnInsReq.serNum = tempItmRow.Item("SERIAL_NUM").ToString
                        itmTrnInsReq.cst = 0
                        itmTrnInsReq.fifoDt = ""
                        itmTrnInsReq.fiflDt = ""
                        If (tempItmRow.Item("OUT_ID").ToString + "").isNotEmpty Then

                            itmTrnInsReq.idCd = tempItmRow.Item("OUT_ID").ToString
                            itmTrnInsReq.oldOutCd = invResReq.out_Cd
                        Else
                            itmTrnInsReq.idCd = ""
                            itmTrnInsReq.oldOutCd = ""
                        End If

                        Try
                            soLnRdr = DisposablesManager.BuildOracleDataReader(cmdErp)


                            If soLnRdr.Read() Then
                                If IsNumeric(soLnRdr("FIFO_CST").ToString) Then
                                    cst = soLnRdr("FIFO_CST")
                                ElseIf IsNumeric(itmInfo("REPL_CST").ToString) Then
                                    cst = itmInfo("REPL_CST")
                                Else
                                    cst = 0
                                End If
                                itmTrnInsReq.cst = CDbl(cst.ToString)
                                itmTrnInsReq.fifoDt = soLnRdr.Item("FIFO_DT").ToString
                                itmTrnInsReq.fiflDt = soLnRdr.Item("FIFL_DT").ToString
                            End If

                            Insert_Final_Doc_Itm_Trn(itmTrnInsReq)

                        Finally
                            soLnRdr.Close()
                        End Try

                        If Session("ord_tp_cd") = "SAL" AndAlso
                            itmInfo("INVENTORY").ToString = "Y" AndAlso
                            (ConfigurationManager.AppSettings("xref") = "Y" OrElse ConfigurationManager.AppSettings("enable_serialization") = "Y") AndAlso
                            (itmInfo("BULK_TP_ITM").ToString = "N") Then

                            HBCG_Utils.Finalize_Inv_Xref(del_doc_num, soLn_num, tempItmRow.Item("SERIAL_NUM").ToString, cmdErp) ' TODO need to find a way to not do this if trans fails 
                            '   TODO - probably need to make inv trans sub of 1st atomic ?
                        End If

                    End If

                    If Session("ord_tp_cd") = "CRM" And tempItmRow.Item("ITM_TP_CD").ToString = "INV" Then
                        AddPendingInventory(del_doc_num, soLn_num, tempItmRow.Item("ITM_CD").ToString, tempItmRow.Item("QTY").ToString, Session("adj_ivc_cd"))
                    End If

                    'start lucy add
                    Dim str_store_cd = Session("store_cd")

                    Dim str_y_n As String

                    check_redp_store(str_store_cd, str_y_n)
                    Dim str_session_id As String = Session.SessionID.ToString.Trim
                    Dim str_row_id As String = tempItmRow.Item("ROW_ID")
                    Dim str_no As String = lucy_check_no_rf(Session.SessionID.ToString.Trim)  'lucy
                    ' If Session("cash_carry") = "TRUE" And str_no = "HAS" Then     'HAS means has take with

                    If str_no = "HAS" And str_y_n = "Y" Then    'redp


                        lucy_insert_redp_rf(str_session_id, str_row_id, del_doc_num, soLn_num)
                    Else
                        update_ln_no(str_session_id, str_row_id, del_doc_num, soLn_num)     ' 12-mar-15
                    End If

                    'end lucy add

                    soLn_num = soLn_num + 1   ' get a new del_doc_ln# for next line
                Next

                'Alice added for request 294 to add approval comments of price changes on Oct 16, 2019 

                If Session("ApprovalComment1") & "" <> "" And Session("Approver_emp_cd") & "" <> "" Then
                    HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "APPCMNT", Session("ApprovalComment1"), "", Session("Approver_emp_cd"), "")
                    If Session("ApprovalComment2") & "" <> "" Then
                        HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "APPCMNT", Session("ApprovalComment2"), "", Session("Approver_emp_cd"), "")
                    End If
                    If Session("ApprovalComment3") & "" <> "" Then
                        HBCG_Utils.SaveAutoComment(cmdErp, del_doc_num, Session("tran_dt"), "APPCMNT", Session("ApprovalComment3"), "", Session("Approver_emp_cd"), "")
                    End If
                    Session("ApprovalComment1") = ""
                    Session("ApprovalComment2") = ""
                    Session("ApprovalComment3") = ""
                    Session("Approver_emp_cd") = ""
                End If


            End If
        Finally
        End Try
    End Sub


    ''' <summary>
    ''' Creates PO links accordingly
    ''' </summary>
    ''' <param name="dr">the datarow being processed</param>
    ''' <param name="dbAtomicCommand">the command to be used for the processing of PO links</param>
    Private Sub ProcessPOLinks(ByVal dr As DataRow,
                               ByVal delDocNum As String, ByVal delDocLnNum As String,
                               ByRef dbAtomicCommand As OracleCommand)

        'when PO linking is enabled, needs to make sure that PO links are removed or added accordingly
        If (ConfigurationManager.AppSettings("link_po").ToString.Equals("Y")) Then

            Dim poCd As String = If(IsDBNull(dr("PO_CD")), "", dr("PO_CD").ToString())
            Dim poLn As String = If(IsDBNull(dr("PO_LN")), "", dr("PO_LN").ToString())
            Dim poBiz As POBiz = New POBiz()
            If Not poLn.Equals(String.Empty) AndAlso poBiz.IsPOAvailable(dbAtomicCommand, poCd, poLn, dr("ITM_CD") & "", dr("Qty") & "") = True Then
                If (poCd.isNotEmpty() AndAlso poLn.isNotEmpty()) Then
                    Dim docTpCd As String = If(Not String.IsNullOrEmpty(Session("ord_tp_cd")), Session("ord_tp_cd").ToString(), AppConstants.Order.TYPE_SAL)
                    Dim dtPdTransfer As Date = FormatDateTime(New Date(), DateFormat.ShortDate)

                    theSalesBiz.AddPOLink(dbAtomicCommand, docTpCd, delDocNum, delDocLnNum,
                                      dtPdTransfer, dr("QTY").ToString(), poCd, poLn)
                Else
                    'do not process the PO links if the PO is not available .....
                End If
            End If
        End If
    End Sub

    Public Sub Insert_Final_Doc_Itm_Trn(ByVal insReq As InventoryUtils.ItmTrnInsertReq)

        If Session("ord_tp_cd") = "SAL" Then

            insReq.ittCd = AppConstants.Inv.ITM_TRN_TP_SOF

        ElseIf Session("ord_tp_cd") = "CRM" Then

            insReq.ittCd = AppConstants.Inv.ITM_TRN_TP_CMF
            ' input put into old set - OK if SAL but need to fix if CRM
            insReq.newStoreCd = insReq.oldStoreCd
            insReq.newLocCd = insReq.oldLocCd
            insReq.oldStoreCd = ""
            insReq.oldLocCd = ""

        End If

        insReq.empCdOp = Session("EMP_CD")
        insReq.trnDt = Session("tran_dt")  ' TODO DSA - check if this needs to be short or set to date - ditto fifl/fifo dates
        insReq.originCd = "POS"
        insReq.postedToGl = "N"
        insReq.confLabels = "N"

        InventoryUtils.InsertItmTrn(insReq, Nothing)
        ' TODO - updt 00c, serial num and rf_id_cd
    End Sub

    Private Sub Xfer_fi_Liability(ByVal del_doc_num As String, ByRef conn As OracleConnection)
        ' TODO - when make order save atomic transaction- need to pass in command and use in call to xfer liability below

        Dim is_financed As Boolean = False

        ' SO is not committed yet so a new connection will not find this info
        Dim sql = "SELECT orig_fi_amt, fin_cust_cd, orig_del_doc_num, ord_tp_cd, so.stat_cd, final_dt, so_store_cd, so.cust_cd, s.co_cd, bnk_crd_num " +
           " FROM  ar_trn ar, store s, so WHERE ar.ivc_cd = so.del_doc_num And s.store_cd = so.so_store_cd And ar.co_cd = s.co_cd " +
           " And ar.trn_tp_cd = so.ord_tp_cd And so.del_doc_num = '" & del_doc_num & "' AND so.fin_cust_cd IS NOT NULL and orig_fi_amt > 0 "

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim datRdr As OracleDataReader

        Try
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.Read() Then
                If datRdr.IsDBNull(0) = False Then
                    is_financed = True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        If is_financed Then

            ' TODO - when this becomes mostly one big atomic transaction then this does not need to be one, just pass cmd to util
            Dim connAtomic As OracleConnection = HBCG_Utils.GetConn(HBCG_Utils.Connection_Constants.CONN_ERP)
            Dim cmdA As OracleCommand
            Dim trans As OracleTransaction

            cmdA = DisposablesManager.BuildOracleCommand(connAtomic)

            Try
                connAtomic.Open()
                trans = connAtomic.BeginTransaction()
                cmdA.Transaction = trans

                'HBCG_Utils.xferCustLiability(del_doc_num, datRdr("ORIG_DEL_DOC_NUM").ToString, datRdr("ORD_TP_CD"), datRdr("STAT_CD"), datRdr("FINAL_DT"), datRdr("SO_STORE_CD"),
                '                  CDbl(datRdr("ORIG_FI_AMT").ToString), datRdr("CUST_CD"), datRdr("FIN_CUST_CD"), Session("emp_cd"), datRdr("CO_CD"),
                '                  "POS", "O", datRdr("BNK_CRD_NUM"), cmdA)
                'Daniela fix take with error
                Dim origNum As String
                If IsDBNull(datRdr("ORIG_DEL_DOC_NUM")) Then
                    origNum = ""
                Else : origNum = datRdr("ORIG_DEL_DOC_NUM")
                End If
                Dim bnkNum As String
                If IsDBNull(datRdr("BNK_CRD_NUM")) Then
                    bnkNum = ""
                Else : bnkNum = datRdr("BNK_CRD_NUM")
                End If

                HBCG_Utils.xferCustLiability(del_doc_num, origNum, datRdr("ORD_TP_CD"), datRdr("STAT_CD"), datRdr("FINAL_DT"), datRdr("SO_STORE_CD"),
                                  CDbl(datRdr("ORIG_FI_AMT").ToString), datRdr("CUST_CD"), datRdr("FIN_CUST_CD"), Session("emp_cd"), datRdr("CO_CD"),
                                  "POS", "O", bnkNum, cmdA)

                trans.Commit()

            Catch ex As Exception
                Try
                    trans.Rollback()

                Catch ex2 As Exception
                    ' This catch block will handle any errors that may have occurred
                    ' on the server that would cause the rollback to fail, such as
                    ' a closed connection.
                    Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                    Console.WriteLine("  Message: {0}", ex2.Message)
                End Try
                Throw ex

            Finally
                connAtomic.Close()
            End Try
        End If
    End Sub

    Public Function Check_Split_Filled(ByVal del_doc_num As String, ByVal order_tp_cd As String)

        Dim sql As String
        Dim approval_found As Boolean = False
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        sql = "SELECT STORE_CD FROM SO_LN, ITM WHERE DEL_DOC_NUM='" & del_doc_num & "' AND SO_LN.ITM_CD=ITM.ITM_CD AND STORE_CD IS NULL AND ITM.ITM_TP_CD='INV'"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                If MyDataReader.Item("STORE_CD").ToString & "" <> "" Then
                    Return True
                Else
                    sql = "UPDATE SO SET STAT_CD='O', FINAL_DT = NULL WHERE DEL_DOC_NUM='" & del_doc_num & "'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()


                    sql = "DELETE FROM GL_DOC_TRN WHERE DOC_NUM=:DOC_NUM AND CO_CD=:CO_CD AND ORIGIN_CD=:ORIGIN_CD AND PROCESSED=:PROCESSED"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":DOC_NUM", OracleType.VarChar)
                    objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":PROCESSED", OracleType.VarChar)

                    objSql.Parameters(":DOC_NUM").Value = del_doc_num
                    objSql.Parameters(":CO_CD").Value = Session("CO_CD")
                    objSql.Parameters(":ORIGIN_CD").Value = "PSOE"
                    objSql.Parameters(":PROCESSED").Value = "N"

                    objSql.ExecuteNonQuery()
                    objSql.Parameters.Clear()

                    sql = "UPDATE SO_LN SET TAKEN_WITH='N' WHERE DEL_DOC_NUM='" & del_doc_num & "' AND TAKEN_WITH='Y'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()

                    If order_tp_cd = "SAL" Then
                        sql = "UPDATE AR_TRN SET STAT_CD='A' WHERE IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                    ElseIf order_tp_cd = "MDB" Then
                        sql = "UPDATE AR_TRN SET STAT_CD='A' WHERE IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                    ElseIf order_tp_cd = "CRM" Then
                        sql = "UPDATE AR_TRN SET STAT_CD='A' WHERE ADJ_IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                    ElseIf order_tp_cd = "MCR" Then
                        sql = "UPDATE AR_TRN SET STAT_CD='A' WHERE ADJ_IVC_CD='" & del_doc_num & "' AND TRN_TP_CD='" & order_tp_cd & "'"
                    Else
                        sql = ""
                    End If
                    If sql <> "" Then
                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql.ExecuteNonQuery()
                    End If
                    Return False
                End If
            Else
                Return True
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Sub ProcessSwatchOrders(ByRef dbCommand As OracleCommand, ByVal ivc_cd As String)

        Dim sql As String

        sql = "UPDATE SO SET STAT_CD='F', FINAL_DT=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') WHERE DEL_DOC_NUM='" & ivc_cd & "'"
        dbCommand.CommandText = UCase(sql)
        dbCommand.ExecuteNonQuery()

        sql = "UPDATE AR_TRN SET STAT_CD='T' WHERE IVC_CD='" & ivc_cd & "'"
        dbCommand.CommandText = UCase(sql)
        dbCommand.ExecuteNonQuery()

        'Dim del_doc As String = Get_Next_Document(Microsoft.VisualBasic.Mid(ivc_cd, 6, 2), FormatDateTime(Today.Date, 2))  ' for store_cd > 2 char this is never true and it may not be true anyway in 12.x and up
        Dim del_doc As String = ""
        Dim crmKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("store_cd").ToString, Session("tran_dt"))
        If Not (crmKeyInfo Is Nothing OrElse crmKeyInfo.soDocNum Is Nothing OrElse crmKeyInfo.soDocNum.delDocNum Is Nothing OrElse Len(crmKeyInfo.soDocNum.delDocNum) <> 11) Then
            del_doc = crmKeyInfo.soDocNum.delDocNum
        Else
            Throw New Exception("Could not create order ID, please try again")
        End If

        'Create the credit memo
        sql = "INSERT INTO SO ("
        sql = sql & "DEL_DOC_NUM, CUST_CD, CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, FIN_CUST_CD, "
        sql = sql & "SHIP_TO_ST_CD, SHIP_TO_ZONE_CD, TAX_CD, TET_CD, ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, "
        sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
        sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
        sql = sql & "SO_DOC_NUM, SO_WR_DT, SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
        sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
        sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, "
        sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
        sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, PU_DEL, PU_DEL_DT, DEL_CHG, "
        sql = sql & "SETUP_CHG, TAX_CHG, STAT_CD, FINAL_DT, FINAL_STORE_CD, MASF_FLAG, ORIG_DEL_DOC_NUM, SER_CNTR_CD, "
        sql = sql & "SER_CNTR_VOID_FLAG, ORIG_FI_AMT, APPROVAL_CD, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
        sql = sql & "CASH_AND_CARRY, PRT_DT, PRT_PU_SLIP_CNT, PRT_IVC_CNT, PRT_PRG_NM, PU_SLIP_STAT_CD, GRID_NUM, PRT_INS_CNT, "
        sql = sql & "REQUESTED_FI_AMT, ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, DRIVER_EMP_CD, HELPER_EMP_CD, FRAN_SALE, "
        sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT "
        sql = sql & ", CONF_STAT_CD) "

        sql = sql & "SELECT :NEW_DEL_DOC_NUM, CUST_CD, :CAUSE_CD, VOID_CAUSE_CD, EMP_CD_OP, EMP_CD_KEYER, FIN_CUST_CD, "
        sql = sql & "SHIP_TO_ST_CD, SHIP_TO_ZONE_CD, TAX_CD, TET_CD, :ORD_TP_CD, ORD_SRT_CD, SE_TP_CD, SO_STORE_CD, "
        sql = sql & "PU_DEL_STORE_CD, SE_CENTER_STORE_CD, SE_ZONE_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, SO_EMP_SLSP_CD3, "
        sql = sql & "DISC_CD, DISC_EMP_APP_CD, DISC_EMP_CD, INT_FI_APP_CD, PRT_EMP_CD, PRT_APP_EMP_CD, PRT_CON_CD, "
        sql = sql & ":NEW_DEL_DOC_NUM, SO_WR_DT, :SO_SEQ_NUM, SHIP_TO_TITLE, SHIP_TO_F_NAME, SHIP_TO_L_NAME, SHIP_TO_ADDR1, "
        sql = sql & "SHIP_TO_ADDR2, SHIP_TO_ZIP_CD, SHIP_TO_H_PHONE, SHIP_TO_B_PHONE, SHIP_TO_EXT, SHIP_TO_CITY, "
        sql = sql & "SHIP_TO_COUNTRY, SHIP_TO_CORP, SHIP_TO_COUNTY, HOLD_UNTIL_DT, PCT_OF_SALE1, PCT_OF_SALE2, "
        sql = sql & "PCT_OF_SALE3, ORDER_PO_NUM, SO_WR_SEC, SHIP_TO_OUT_OF_TERR, CRED_RPT, BILL_ORD_NUM, SHIP_TO_SSN, "
        sql = sql & "NUM_ORDERS, RENEW_WARR, WARR_CUST, SHIP_TO_CUST_CD, SHIP_TO_CUST_NUM, PU_DEL, PU_DEL_DT, DEL_CHG, "
        sql = sql & "SETUP_CHG, TAX_CHG, 'O', NULL, FINAL_STORE_CD, MASF_FLAG, :ORIG_DEL_DOC_NUM, SER_CNTR_CD, "
        sql = sql & "SER_CNTR_VOID_FLAG, ORIG_FI_AMT, APPROVAL_CD, RESO_DEPOSIT, LAYAWAY, LAYAWAY_PMT, LAYAWAY_1ST_PMT_DUE, "
        sql = sql & "CASH_AND_CARRY, PRT_DT, PRT_PU_SLIP_CNT, PRT_IVC_CNT, PRT_PRG_NM, PU_SLIP_STAT_CD, GRID_NUM, PRT_INS_CNT, "
        sql = sql & "REQUESTED_FI_AMT, ARC_FLAG, PU_DEL_TIME_BEG, PU_DEL_TIME_END, DRIVER_EMP_CD, HELPER_EMP_CD, FRAN_SALE, "
        sql = sql & "USR_FLD_1, USR_FLD_2, USR_FLD_3, USR_FLD_4, USR_FLD_5, POS_LAY_NUM, ALT_DOC_NUM, ORIGIN_CD, DEL_STAT "
        sql = sql & ",'" & theSalesBiz.GetFirstConfirmationCode() & "' "
        sql = sql & "FROM SO WHERE DEL_DOC_NUM=:DEL_DOC_NUM"

        dbCommand.CommandText = UCase(sql)
        dbCommand.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        dbCommand.Parameters.Add(":ORIG_DEL_DOC_NUM", OracleType.VarChar)
        dbCommand.Parameters.Add(":SO_SEQ_NUM", OracleType.VarChar)
        dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        dbCommand.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
        dbCommand.Parameters.Add(":CAUSE_CD", OracleType.VarChar)

        dbCommand.Parameters(":NEW_DEL_DOC_NUM").Value = del_doc
        dbCommand.Parameters(":ORIG_DEL_DOC_NUM").Value = ivc_cd
        dbCommand.Parameters(":SO_SEQ_NUM").Value = crmKeyInfo.soKey.seqNum   'Microsoft.VisualBasic.Right(del_doc, 4)
        dbCommand.Parameters(":DEL_DOC_NUM").Value = ivc_cd
        dbCommand.Parameters(":ORD_TP_CD").Value = "CRM"
        dbCommand.Parameters(":CAUSE_CD").Value = "FAB"

        dbCommand.ExecuteNonQuery()
        dbCommand.Parameters.Clear()

        'Create the new AR_TRN record
        sql = "INSERT INTO AR_TRN ("
        sql = sql & "CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "
        sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, "
        sql = sql & "IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, "
        sql = sql & "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM)"

        sql = sql & "SELECT CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, 'CRM', "
        sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, 'A', AR_TP, BNK_NUM, CHK_NUM, "
        sql = sql & ":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, DES, :ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, "
        sql = sql & "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM "
        sql = sql & "FROM AR_TRN WHERE IVC_CD=:IVC_CD AND TRN_TP_CD='SAL'"
        dbCommand.CommandText = UCase(sql)

        dbCommand.Parameters.Add(":IVC_CD", OracleType.VarChar)
        dbCommand.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
        dbCommand.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)

        dbCommand.Parameters(":IVC_CD").Value = ivc_cd
        dbCommand.Parameters(":ADJ_IVC_CD").Value = del_doc
        dbCommand.Parameters(":NEW_IVC_CD").Value = ivc_cd

        dbCommand.ExecuteNonQuery()
        dbCommand.Parameters.Clear()

        'Copy over the sales order lines
        sql = "INSERT INTO SO_LN ("
        sql = sql & "DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, STORE_CD, LOC_CD, REF_DEL_DOC_LN#, REF_ITM_CD,"
        sql = sql & "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, SO_DOC_NUM, ID_NUM, SER_NUM, "
        sql = sql & "UNIT_PRC, QTY, SPIFF, PICKED, OUT_ENTRY_DT, FIFL_DT, FIFO_CST, FIFO_DT, FILL_DT, VOID_FLAG, "
        sql = sql & "COD_AMT, OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, WARRANTED_BY_LN#, "
        sql = sql & "WARRANTED_BY_ITM_CD, TREATS_LN#, TREATS_ITM_CD, TREATED_BY_LN#, TREATED_BY_ITM_CD, DISC_AMT, "
        sql = sql & "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, "
        sql = sql & "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, "
        sql = sql & "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT, "
        sql = sql & "COMPLAINT_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, "
        sql = sql & "COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, "
        sql = sql & "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, TAX_CD, TAX_BASIS, CUST_TAX_CHG, "
        sql = sql & "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT, ALT_DOC_LN#) "

        sql = sql & "SELECT :NEW_DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, WAR_EXP_DT, COMM_CD, STORE_CD, LOC_CD, REF_DEL_DOC_LN#, REF_ITM_CD,"
        sql = sql & "REF_SER_NUM, FAB_DEL_DOC_NUM, FAB_DEL_DOC_LN#, OUT_ID_CD, OUT_CD, SO_DOC_NUM, ID_NUM, SER_NUM, "
        sql = sql & "UNIT_PRC, QTY, SPIFF, PICKED, OUT_ENTRY_DT, FIFL_DT, FIFO_CST, FIFO_DT, FILL_DT, VOID_FLAG, "
        sql = sql & "COD_AMT, OOC_QTY, UP_CHRG, VOID_DT, PRC_CHG_APP_CD, TAKEN_WITH, PKG_SOURCE, WARRANTED_BY_LN#, "
        sql = sql & "WARRANTED_BY_ITM_CD, TREATS_LN#, TREATS_ITM_CD, TREATED_BY_LN#, TREATED_BY_ITM_CD, DISC_AMT, "
        sql = sql & "SER_CD, SO_LN_CMNT, ADDON_WR_DT, SET_UP, LV_IN_CARTON, EE_COUNT, WARR_ID_1, WARR_EXP_DT_1, "
        sql = sql & "WARR_ID_2, WARR_EXP_DT_2, WARR_ID_3, WARR_EXP_DT_3, WARR_ID_4, WARR_EXP_DT_4, WARR_ID_5, "
        sql = sql & "WARR_EXP_DT_5, WARR_ID_6, WARR_EXP_DT_6, SE_CAUSE_CD, SE_STATUS, SO_LN_SEQ, PU_DISC_AMT, "
        sql = sql & "COMPLAINT_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2, PCT_OF_SALE1, PCT_OF_SALE2, COMM_ON_SETUP_CHG, "
        sql = sql & "COMM_ON_DEL_CHG, FRAN_MRKUP_AMT, SE_COMP_DT, ACTIVATION_DT, ACTIVATION_PHONE, SHIP_GROUP, "
        sql = sql & "COMM_PAID_SLSP1, COMM_PAID_SLSP2, SHP_VE_CD, SHP_TRACK_ID, TAX_CD, TAX_BASIS, CUST_TAX_CHG, "
        sql = sql & "STORE_TAX_CHG, TAX_RESP, DELIVERY_POINTS, PU_DEL_DT, ALT_DOC_LN# "
        sql = sql & "FROM SO_LN WHERE DEL_DOC_NUM=:DEL_DOC_NUM"
        dbCommand.CommandText = UCase(sql)

        dbCommand.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)

        dbCommand.Parameters(":NEW_DEL_DOC_NUM").Value = del_doc
        dbCommand.Parameters(":DEL_DOC_NUM").Value = ivc_cd

        dbCommand.ExecuteNonQuery()
        dbCommand.Parameters.Clear()

    End Sub

    Public Sub Process_Payment_Transfers(ByVal amt As Double, ByVal ivc_cd As String, Optional ByVal wdr As Boolean = False, Optional wdr_promo As String = "")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim pmt_amt As Double
        Dim down_payments As String = DP_String_Creation()

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "SELECT AMT, AR_TRN_PK FROM AR_TRN WHERE IVC_CD='" & ivc_cd & "' "

        '
        'Removing this to push the Finance Down Payment transactions to splits as well.
        If down_payments & "" <> "" Then
            sql = sql & "AND MOP_CD NOT IN(" & down_payments & ") "
        End If
        sql = sql & "AND TRN_TP_CD='DEP' ORDER BY AMT DESC"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            Do While MyDataReader2.Read
                If amt <= 0 Then Exit Sub
                pmt_amt = CDbl(MyDataReader2.Item("AMT").ToString)
                If pmt_amt = amt Then
                    sql = "UPDATE AR_TRN SET IVC_CD='" & ivc_cd & "A" & "' WHERE AR_TRN_PK=" & MyDataReader2.Item("AR_TRN_PK").ToString
                    objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql2.ExecuteNonQuery()
                    amt = 0
                ElseIf pmt_amt > amt Then
                    Insert_New_AR_TRN(MyDataReader2.Item("AR_TRN_PK").ToString, amt, ivc_cd & "A")
                    Update_AR_TRN(MyDataReader2.Item("AR_TRN_PK").ToString, pmt_amt - amt)
                    amt = 0
                Else
                    Insert_New_AR_TRN(MyDataReader2.Item("AR_TRN_PK").ToString, pmt_amt, ivc_cd & "A")
                    Update_AR_TRN(MyDataReader2.Item("AR_TRN_PK").ToString, pmt_amt - amt)
                    amt = amt - pmt_amt
                End If
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If amt > 0 Then
            Dim bnk_crd_num As String = ""
            sql = "SELECT BNK_CRD_NUM FROM AR_TRN WHERE IVC_CD='" & ivc_cd & "' AND TRN_TP_CD='SAL' AND BNK_CRD_NUM IS NOT NULL"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                If MyDataReader2.Read Then
                    bnk_crd_num = MyDataReader2.Item("BNK_CRD_NUM").ToString
                End If
            Catch
                Throw
                conn.Close()
            End Try

            sql = "SELECT SO.ORIG_FI_AMT, SO.APPROVAL_CD, SO.FIN_CUST_CD, SO_ASP.AS_PROMO_CD, SO_ASP.PROMO_CD FROM SO, SO_ASP " &
                        " WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & ivc_cd & "' AND ORIG_FI_AMT IS NOT NULL"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                If MyDataReader2.Read Then
                    If IsNumeric(MyDataReader2.Item("ORIG_FI_AMT").ToString) Then
                        pmt_amt = CDbl(MyDataReader2.Item("ORIG_FI_AMT").ToString)
                    End If
                    If pmt_amt = amt Then
                        sql = "UPDATE SO SET ORIG_FI_AMT=0, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & ivc_cd & "'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL WHERE IVC_CD='" & ivc_cd & "' AND TRN_TP_CD='SAL'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE SO SET ORIG_FI_AMT=" & amt & ", APPROVAL_CD='" & MyDataReader2.Item("APPROVAL_CD").ToString & "', FIN_CUST_CD='" & MyDataReader2.Item("FIN_CUST_CD").ToString & "' WHERE DEL_DOC_NUM='" & ivc_cd & "A'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & bnk_crd_num & "' WHERE IVC_CD='" & ivc_cd & "A' AND TRN_TP_CD='SAL'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        'Insert the credit entry into the SO_ASP table
                        sql = "INSERT INTO SO_ASP (DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD) VALUES('" & ivc_cd & "A','Y','" & MyDataReader2.Item("PROMO_CD").ToString & "','" & MyDataReader2.Item("AS_PROMO_CD").ToString & "')"
                        sql = UCase(sql)
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()

                        'Print down payment type finance transactions
                        If wdr = True And wdr_promo & "" <> "" Then
                            Finance_Split_Print(ivc_cd, wdr_promo, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        End If

                        Finance_Split_Print(ivc_cd, MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        Finance_Split_Print(ivc_cd & "A", MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString)
                        amt = 0
                    ElseIf pmt_amt > amt Then
                        sql = "UPDATE SO SET ORIG_FI_AMT=" & pmt_amt - amt & ", APPROVAL_CD='" & MyDataReader2.Item("APPROVAL_CD").ToString & "', FIN_CUST_CD='" & MyDataReader2.Item("FIN_CUST_CD").ToString & "' WHERE DEL_DOC_NUM='" & ivc_cd & "'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE SO SET ORIG_FI_AMT=" & amt & ", APPROVAL_CD='" & MyDataReader2.Item("APPROVAL_CD").ToString & "', FIN_CUST_CD='" & MyDataReader2.Item("FIN_CUST_CD").ToString & "' WHERE DEL_DOC_NUM='" & ivc_cd & "A'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & bnk_crd_num & "' WHERE IVC_CD='" & ivc_cd & "A' AND TRN_TP_CD='SAL'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        'Insert the credit entry into the SO_ASP table
                        sql = "INSERT INTO SO_ASP (DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD) VALUES('" & ivc_cd & "A','Y','" & MyDataReader2.Item("PROMO_CD").ToString & "','" & MyDataReader2.Item("AS_PROMO_CD").ToString & "')"
                        sql = UCase(sql)
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()

                        'Print down payment type finance transactions
                        If wdr = True And wdr_promo & "" <> "" Then
                            Finance_Split_Print(ivc_cd, wdr_promo, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        End If

                        Finance_Split_Print(ivc_cd, MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        Finance_Split_Print(ivc_cd & "A", MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString)
                        amt = 0
                    Else
                        sql = "UPDATE SO SET ORIG_FI_AMT=0, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & ivc_cd & "'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL WHERE IVC_CD='" & ivc_cd & "' AND TRN_TP_CD='SAL'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE SO SET ORIG_FI_AMT=" & pmt_amt & ", APPROVAL_CD='" & MyDataReader2.Item("APPROVAL_CD").ToString & "', FIN_CUST_CD='" & MyDataReader2.Item("FIN_CUST_CD").ToString & "' WHERE DEL_DOC_NUM='" & ivc_cd & "A'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & bnk_crd_num & "' WHERE IVC_CD='" & ivc_cd & "A' AND TRN_TP_CD='SAL'"
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()
                        'Insert the credit entry into the SO_ASP table
                        sql = "INSERT INTO SO_ASP (DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD) VALUES('" & ivc_cd & "A','Y','" & MyDataReader2.Item("PROMO_CD").ToString & "','" & MyDataReader2.Item("AS_PROMO_CD").ToString & "')"
                        sql = UCase(sql)
                        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql2.ExecuteNonQuery()

                        'Print down payment type finance transactions
                        If wdr = True And wdr_promo & "" <> "" Then
                            Finance_Split_Print(ivc_cd, wdr_promo, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        End If

                        Finance_Split_Print(ivc_cd, MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString, wdr)
                        Finance_Split_Print(ivc_cd & "A", MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString)
                        amt = amt - pmt_amt
                    End If
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()

    End Sub

    Public Sub Finance_Split_Print(ByVal del_doc_num As String, ByVal promo_cd As String, ByVal as_cd As String, Optional ByVal wdr As Boolean = False)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        'Open Connection 
        conn.Open()
        Dim fin_invoice As String = ""
        sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & as_cd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                If wdr = False Then
                    fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                Else
                    fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                End If
            End If
            MyDataReader2.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'If InStr(as_cd, "GE") > 0 Then
        ' lucy comment  this line  ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike354543" & del_doc_num, "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance9505954" & del_doc_num & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        'Else
        '    ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike354543" & del_doc_num, "window.open('FI_Addendums/ADS.aspx?DEL_DOC_NUM=" & del_doc_num & "&UFM=" & fin_invoice & "','frmFinance34500404" & del_doc_num & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        'End If

    End Sub

    Public Sub Update_AR_TRN(ByVal ar_trn_pk As String, ByVal amt As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        If amt < 0 Then
            sql = "DELETE FROM AR_TRN WHERE AR_TRN_PK=:AR_TRN_PK"

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":AR_TRN_PK", OracleType.Number)
            objSql.Parameters(":AR_TRN_PK").Value = CDbl(ar_trn_pk)
            objSql.ExecuteNonQuery()
        Else
            sql = "UPDATE AR_TRN SET AMT=:NEW_AMT WHERE AR_TRN_PK=:AR_TRN_PK"

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":NEW_AMT", OracleType.Number)
            objSql.Parameters.Add(":AR_TRN_PK", OracleType.Number)

            objSql.Parameters(":AR_TRN_PK").Value = CDbl(ar_trn_pk)
            objSql.Parameters(":NEW_AMT").Value = CDbl(amt)
            objSql.ExecuteNonQuery()
        End If

        conn.Close()

    End Sub

    Public Sub Insert_New_AR_TRN(ByVal ar_trn_pk As String, ByVal amt As Double, ByVal ivc_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "INSERT INTO AR_TRN ("
        sql = sql & "CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "
        sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, "
        sql = sql & "IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, "
        sql = sql & "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM)"

        sql = sql & "SELECT CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, "
        sql = sql & "BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, :NEW_AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, "
        sql = sql & ":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, "
        sql = sql & "ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM "
        sql = sql & "FROM AR_TRN WHERE AR_TRN_PK=:AR_TRN_PK"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":NEW_AMT", OracleType.Number)
        objSql.Parameters.Add(":AR_TRN_PK", OracleType.VarChar)
        objSql.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)

        objSql.Parameters(":NEW_AMT").Value = CDbl(amt)
        objSql.Parameters(":AR_TRN_PK").Value = CDbl(ar_trn_pk)
        objSql.Parameters(":NEW_IVC_CD").Value = ivc_cd

        objSql.ExecuteNonQuery()
        conn.Close()

    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Function XDigits(ByVal sNumberString, ByVal nReturnLength)

        Dim x As Integer
        If nReturnLength > Len(sNumberString) Then
            XDigits = "0" & sNumberString
            If nReturnLength - Len(sNumberString) > 1 Then
                For x = 2 To nReturnLength - Len(sNumberString)
                    XDigits = "0" & XDigits
                Next
            End If
        Else
            XDigits = Right(sNumberString, nReturnLength)
        End If

    End Function

    Private Sub CreateCustomer()

        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        Gridview1.DataSource = ""
        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT * FROM temp_itm where session_id='" & Session.SessionID.ToString() & "' order by row_id"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
                Session("itemid") = System.DBNull.Value
                Session("sub_total") = 0
                Response.Redirect("order.aspx")
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Function Get_Next_Val(ByVal current_string As String)

        Dim corr_no As Integer = 0
        Dim x As Integer = 1
        Dim y As Integer = 1
        Dim Move_Next As Boolean = True
        Dim Jump_list As Boolean = True
        Dim reset_to_a As Boolean = False

        Get_Next_Val = ""

        Do While Move_Next = True
            Get_Next_Val = ""
            corr_no = Determine_Sequence(Mid(current_string, (Len(current_string) + 1) - x, 1)) + 2
            If corr_no > 26 Then
                If x > 1 Then
                    corr_no = (corr_no - 26) + 2
                Else
                    corr_no = (corr_no - 26) + 1
                End If
                x = x + 1
                corr_no = Determine_Sequence(Mid(current_string, (Len(current_string) + 1) - x, 1)) + 1
                If corr_no > 26 Then
                    If x > 1 Then
                        corr_no = (corr_no - 26) + 2
                    Else
                        corr_no = (corr_no - 26) + 1
                    End If
                    x = x + 1
                    corr_no = Determine_Sequence(Mid(current_string, (Len(current_string) + 1) - x, 1)) + 1
                Else
                    Move_Next = False
                End If
                'x = x + 1
                'corr_no = Determine_Sequence(Mid(current_string, (Len(current_string) + 1) - x, 1)) + 1
                If corr_no > 26 Then
                    If x > 1 Then
                        corr_no = (corr_no - 26) + 2
                    Else
                        corr_no = (corr_no - 26) + 1
                    End If
                    x = x + 1
                    corr_no = Determine_Sequence(Mid(current_string, (Len(current_string) + 1) - x, 1)) + 1
                Else
                    Move_Next = False
                End If
                reset_to_a = True
            Else
                Move_Next = False
            End If
            Select Case x
                Case 1
                    y = 0
                Case 2
                    y = 1
                Case 3
                    y = 2
                Case 4
                    y = 3
            End Select
            If Len(current_string) - x > 0 Then
                Get_Next_Val = Left(current_string, Len(current_string) - x)
            End If
            Get_Next_Val = Get_Next_Val & Determine_Correlation(corr_no)
            If y > 0 And reset_to_a = True Then
                If reset_to_a = True Then
                    Get_Next_Val = Get_Next_Val & StrDup(y, "A")
                    reset_to_a = False
                Else
                    Get_Next_Val = Get_Next_Val & Right(current_string, y)
                End If
                Move_Next = False
            End If
        Loop

    End Function

    ''' <summary>
    ''' As per the ARS Reservation rules, the following applies: 
    ''' 1) ONLY if the order is considered paid in full, reservations will be allowed
    ''' 2) Those items that are dropped, but flagged as PREVENT_PO = 'Y' should be reserved
    '''    regardless of the order being paid in full or not.
    ''' This process will attempt to reserve those inventoriable lines that qualify based on the
    ''' previous rules, as long as they are not linked to a PO and not already reserved 
    ''' </summary>
    Private Sub ProcessARSReservations(ByVal isPaidInFull As Boolean, ByVal preventedItems As List(Of String))
        Dim sbError As StringBuilder = New StringBuilder()
        Dim valMsg As String = InventoryUtils.AllowReservations(SessVar.zoneCd, SessVar.puDelStoreCd)
        If (valMsg.isEmpty()) Then
            Dim resRequest As ResRequestDtc
            Dim resResponse As ResResponseDtc
            Dim resMade As Boolean = False
            Dim preventedItem As Boolean
            Dim callAutoRes As Boolean
            Dim sessId As String = Session.SessionID.ToString.Trim
            Dim soLnsTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(sessId, temp_itm_key_tp.tempItmBySessionId)

            For Each soLnRow In soLnsTbl.Rows

                preventedItem = preventedItems.Contains(soLnRow.Item("ITM_CD").ToString)

                If (isPaidInFull OrElse (Not isPaidInFull AndAlso preventedItem)) Then

                    ' Attempt reservation for Inventory lines, when line NOT linked to PO and NOT already reserved
                    ' TODO - cannot change to inventory flag at this time - only CCExpress 
                    '        is setting inventory flag yet, wait till use new INSERT logic
                    callAutoRes = Not IsDBNull(soLnRow.Item("ITM_TP_CD")) AndAlso
                                  AppConstants.Sku.TP_INV = soLnRow.Item("ITM_TP_CD").ToString AndAlso
                                  (IsDBNull(soLnRow.Item("PO_CD")) OrElse soLnRow.Item("PO_CD").ToString.isEmpty) AndAlso
                                  (IsDBNull(soLnRow.Item("RES_ID")) OrElse soLnRow.Item("RES_ID").ToString.isEmpty) AndAlso
                                  ((IsDBNull(soLnRow("STORE_CD")) OrElse soLnRow("STORE_CD").ToString.isEmpty) OrElse
                                   (IsDBNull(soLnRow("LOC_CD")) OrElse soLnRow("LOC_CD").ToString.isEmpty))

                    If callAutoRes Then
                        resRequest = GetRequestForAutoRes(soLnRow.Item("ITM_CD"),
                                                          soLnRow.Item("QTY"),
                                                          Session("PD"),
                                                          Session("PD_STORE_CD"), Session("ZONE_CD"))
                        resRequest.lineNum = soLnRow.Item("ROW_ID")

                        '--attempts to reserve the line, (includes SPLIT of lines when needed)
                        resResponse = theSalesBiz.ReserveLine(sessId, SessVar.discCd, resRequest, sbError)

                        If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then resMade = True
                        If resResponse.isForARSStore AndAlso resResponse.isLineSplitted Then
                            resMade = False
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Reserves inventory ONLY for the lines that are indicated in the list passed in.
    ''' This method is currently ONLY called after an ARS validation.  Based on ARS rules
    ''' some lines may need to be reserved, therefore completes this reservation process.
    ''' </summary>
    Private Sub ReserveInventory(ByVal lnsToReserve As List(Of String))

        If (InventoryUtils.AllowReservations(SessVar.zoneCd, SessVar.puDelStoreCd).isEmpty()) Then

            Dim lnNum As String
            Dim resRequest As ResRequestDtc
            Dim sessionId As String = Session.SessionID.ToString.Trim
            Dim soLnsTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(sessionId,
                                                                             temp_itm_key_tp.tempItmBySessionId)
            If (Not IsNothing(soLnsTbl)) Then
                For Each soLn In soLnsTbl.Rows
                    lnNum = soLn("ROW_ID")
                    If (lnsToReserve.Contains(lnNum)) Then
                        resRequest = InventoryUtils.GetRequestForAutoRes(
                                                        soLn("ITM_CD"),
                                                        soLn("QTY"),
                                                        SessVar.puDel,
                                                        SessVar.puDelStoreCd,
                                                        SessVar.zoneCd)
                        resRequest.lineNum = lnNum
                        theSalesBiz.ReserveLine(sessionId, SessVar.discCd, resRequest)
                    End If
                Next
            End If

        End If
    End Sub

    ''' <summary>
    ''' Unreserves all lines in the current order.   This process gets called after the order 
    ''' has been saved, or if the order is being cancelled, to remove the ITM_RES records,
    ''' and release the hold on the inventory.
    ''' </summary>
    Private Sub UnreserveInventory()
        Dim soLns As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Session.SessionID.ToString.Trim,
                                                temp_itm_key_tp.tempItmBySessionId,
                                                temp_itm_where_clause.qtyGreaterZero)
        If (Not IsNothing(soLns)) Then
            For Each tmpItemRow In soLns.Rows
                UnreserveInventory(tmpItemRow)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Checks if the line passed in has soft-reservations attached, and if so, removes them
    ''' Since the reservation is removed, the Store/Location are also cleared from the line
    ''' </summary>
    ''' <param name="tmpItemRow">a Datarow that contains the line to remove reservations when present</param>
    Private Sub UnreserveInventory(ByRef tmpItemRow As DataRow)

        Dim isReserved As Boolean = If(IsDBNull(tmpItemRow.Item("RES_ID")) OrElse
                                        (tmpItemRow.Item("RES_ID")).ToString.isEmpty, False, True)
        If (isReserved) Then
            Dim request As UnResRequestDtc = InventoryUtils.GetRequestForUnreserve(
                                                                    tmpItemRow.Item("RES_ID").ToString,
                                                                    tmpItemRow.Item("ITM_CD").ToString,
                                                                    tmpItemRow.Item("STORE_CD").ToString,
                                                                    tmpItemRow.Item("LOC_CD").ToString,
                                                                    tmpItemRow.Item("QTY").ToString)
            theInvBiz.RemoveSoftRes(request)
        End If
    End Sub


    Public Function Determine_Correlation(ByVal corr_no As String)

        Select Case corr_no
            Case 1
                Determine_Correlation = "A"
            Case 2
                Determine_Correlation = "B"
            Case 3
                Determine_Correlation = "C"
            Case 4
                Determine_Correlation = "D"
            Case 5
                Determine_Correlation = "E"
            Case 6
                Determine_Correlation = "F"
            Case 7
                Determine_Correlation = "G"
            Case 8
                Determine_Correlation = "H"
            Case 9
                Determine_Correlation = "I"
            Case 10
                Determine_Correlation = "J"
            Case 11
                Determine_Correlation = "K"
            Case 12
                Determine_Correlation = "L"
            Case 13
                Determine_Correlation = "M"
            Case 14
                Determine_Correlation = "N"
            Case 15
                Determine_Correlation = "O"
            Case 16
                Determine_Correlation = "P"
            Case 17
                Determine_Correlation = "Q"
            Case 18
                Determine_Correlation = "R"
            Case 19
                Determine_Correlation = "S"
            Case 20
                Determine_Correlation = "T"
            Case 21
                Determine_Correlation = "U"
            Case 22
                Determine_Correlation = "V"
            Case 23
                Determine_Correlation = "W"
            Case 24
                Determine_Correlation = "X"
            Case 25
                Determine_Correlation = "Y"
            Case 26
                Determine_Correlation = "Z"
            Case Else
                Determine_Correlation = ""
        End Select
        If Determine_Correlation & "" = "" Then
            Determine_Correlation = "ERROR"
        End If

    End Function

    Public Function Determine_Sequence(ByVal corr_no As String)

        Select Case corr_no
            Case "A"
                Determine_Sequence = 1
            Case "B"
                Determine_Sequence = 2
            Case "C"
                Determine_Sequence = 3
            Case "D"
                Determine_Sequence = 4
            Case "E"
                Determine_Sequence = 5
            Case "F"
                Determine_Sequence = 6
            Case "G"
                Determine_Sequence = 7
            Case "H"
                Determine_Sequence = 8
            Case "I"
                Determine_Sequence = 9
            Case "J"
                Determine_Sequence = 10
            Case "K"
                Determine_Sequence = 11
            Case "L"
                Determine_Sequence = 12
            Case "M"
                Determine_Sequence = 13
            Case "N"
                Determine_Sequence = 14
            Case "O"
                Determine_Sequence = 15
            Case "P"
                Determine_Sequence = 16
            Case "Q"
                Determine_Sequence = 17
            Case "R"
                Determine_Sequence = 18
            Case "S"
                Determine_Sequence = 19
            Case "T"
                Determine_Sequence = 20
            Case "U"
                Determine_Sequence = 21
            Case "V"
                Determine_Sequence = 22
            Case "W"
                Determine_Sequence = 23
            Case "X"
                Determine_Sequence = 24
            Case "Y"
                Determine_Sequence = 25
            Case "Z"
                Determine_Sequence = 26
            Case Else
                Determine_Sequence = ""
        End Select

    End Function

    Protected Sub btn_convert_to_rel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_convert_to_rel.Click

        'removes ALL Pending reservations
        UnreserveInventory()

        Response.Redirect("startup.aspx?LEAD=TRUE")

    End Sub

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        ' May 22 IT Req 2919
        If (isEmpty(txt_email.Text) Or (Not LeonsBiz.ValidateEmail(txt_email.Text))) Then
            'chk_email.Enabled = False
            'chk_email.Checked = False
            lbl_warning.Text = Resources.LibResources.Label998
            lbl_warning.ForeColor = Color.Red
            ASPxPopupControl1.ShowOnPageLoad = True
            Exit Sub
        Else
            'chk_email.Enabled = True
            'chk_email.Checked = False
            'Session("email_checked") = "Y"
        End If


        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        Dim approval_found As Boolean = False

        conn2.Open()
        sql = "UPDATE CUST_INFO SET EMAIL='" & txt_email.Text & "' WHERE CUST_CD='" & Session("CUST_CD") & "' AND SESSION_ID='" & Session.SessionID.ToString.Trim & "'"

        With cmdDeleteItems
            .Connection = conn2
            .CommandText = sql
        End With
        cmdDeleteItems.ExecuteNonQuery()
        conn2.Close()

        ASPxPopupControl1.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        Session("email_checked") = ""
        chk_email.Enabled = False
        ASPxPopupControl1.ShowOnPageLoad = False

    End Sub

    Protected Sub chk_email_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'If chk_email.Checked = True Then
        '    ImageButton1.Enabled = True
        '    ASPxPopupControl5.ShowOnPageLoad = True
        'Else
        '    ImageButton1.Enabled = False
        'End If
        'May 22 add email flag to session IT Req 2919
        If chk_email.Checked = True Then
            Session("email_checked") = "Y"
            'ImageButton1.Enabled = True
            'ASPxPopupControl5.ShowOnPageLoad = True
        Else
            'ImageButton1.Enabled = False
            Session("email_checked") = ""
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub ImageButton1_Click1(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl5.ShowOnPageLoad = True

    End Sub

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim UpPanel As UpdatePanel

        Try

            UpPanel = Master.FindControl("UpdatePanel1")

            If lbl_split.Text = "True" Then

                'resolve invoice to print
                ' lucy comment the following if condition 
                'If (lbl_default_invoice.Text.isNotEmpty And lbl_default_invoice.Text.Contains(".aspx")) Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('Invoices/" & lbl_default_invoice.Text & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "','frmTestfg34','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikegf43tg", "window.open('Invoices/" & lbl_default_invoice.Text & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "A','frmTestgf43tg','height=700px,width=700px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ElseIf (lbl_default_invoice.Text.isNotEmpty And lbl_default_invoice.Text.Contains(".repx")) Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('merch_images/invoice_files/" & lbl_default_invoice.Text & ".pdf" & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "','frmTestfg34','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)                
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&INVOICE_NAME=" & lbl_default_invoice.Text & "','frmTestfg34','height=700px,width=700px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

                'now call for the split del doc num 'A'                
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikegf43tg", "window.open('merch_images/invoice_files/" & lbl_default_invoice.Text & ".pdf" & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "A','frmTestgf43tg','height=700px,width=700px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikegf43tg", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "A" & "&INVOICE_NAME=" & lbl_default_invoice.Text & "','frmTestgf43tg','height=700px,width=700px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ' End If

                sql = "SELECT SO.ORIG_FI_AMT, SO.APPROVAL_CD, SO.FIN_CUST_CD, SO_ASP.PROMO_CD, SO_ASP.AS_PROMO_CD FROM SO, SO_ASP WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & lbl_del_doc_num.Text & "' AND ORIG_FI_AMT IS NOT NULL"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        Finance_Split_Print(lbl_del_doc_num.Text, MyDataReader2.Item("PROMO_CD").ToString, MyDataReader2.Item("FIN_CUST_CD").ToString, lbl_wdr.Text)
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                ' lucy comment the following if condition
                'If chk_picr.Checked = True And ConfigurationManager.AppSettings("picr").ToString = "Y" Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnySc543665ffdsegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "','frhfgdhgffasdfgf43tg','height=700px,width=700px,top=50,left=50,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNaasdffdsegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "A','frmTasdfasdfgf43tg','height=700px,width=700px,top=60,left=60,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                ' End If
            Else
                If Not String.IsNullOrEmpty(lbl_fin_co.Text.ToString) Then
                    Dim fin_invoice As String = ""
                    sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & lbl_fin_co.Text & "' AND AS_CD='" & lbl_fin.Text & "'"""
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    Try
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                        If MyDataReader2.Read Then
                            If lbl_wdr.Text = "True" Then
                                fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                            Else
                                fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                            End If
                        End If
                        MyDataReader2.Close()
                    Catch ex As Exception
                        conn2.Close()
                        conn.Close()
                        Throw
                    End Try

                    ' lucy comment   '  ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike87", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&UFM=" & fin_invoice & "','frmFinance9505954" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    cbo_finance_addendums.Enabled = False
                ElseIf Not String.IsNullOrEmpty(cbo_finance_addendums.SelectedValue.ToString) Then
                    ' lucy comment   '   ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike95", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&UFM=" & cbo_finance_addendums.SelectedValue.ToString & "','frmFinance7','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                    cbo_finance_addendums.Enabled = False
                End If

                'resolve invoice to print
                ' lucy comment the following if condition 
                ' If (lbl_default_invoice.Text.isNotEmpty And lbl_default_invoice.Text.Contains(".aspx")) Then
                ' ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/" & lbl_default_invoice.Text & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&EMAIL=" & chk_email.Checked & "','frmTestfgd543g','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ElseIf (lbl_default_invoice.Text.isNotEmpty And lbl_default_invoice.Text.Contains(".repx")) Then
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefg34", "window.open('merch_images/invoice_files/" & lbl_default_invoice.Text & ".pdf" & "?onbase=" & ConfigurationManager.AppSettings("onbase").ToString & "&DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&EMAIL=" & chk_email.Checked & "','frmTestfgd543g','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLikefgd543g", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & lbl_del_doc_num.Text & "&INVOICE_NAME=" & lbl_default_invoice.Text & "&EMAIL=" & chk_email.Checked & "','frmInvoice','height=700px,width=700px,top=30,left=30,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
                'End If
            End If
            ASPxButton1.Enabled = False

        Catch ex As Exception
            conn.Close()
            conn2.Close()
        End Try
    End Sub

    Protected Sub btn_generate_po_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Response.Redirect("Generate_PO.aspx?del_doc_num=" & lbl_del_doc_num.Text)

    End Sub
    Protected Sub printer_drp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles printer_drp.SelectedIndexChanged
        'sabrina added new sub
        Session("printer_drp_id") = printer_drp.SelectedItem.Value.ToString()
        'lbl_Status.Text = " selected printer is " & Session("printer_drp_id")
    End Sub
    Public Sub sy_printer_drp_Populate()

        'sabrina new sub added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd) 'sabrina emp_cd to get default printer
        'Dim default_printer As String = ""
        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = "Printers:"
        End If

        Session("default_prt") = v_default_printer
        'Daniela Spet 08
        'sql = "SELECT PRINT_Q FROM misc.PRINT_QUEUE a WHERE PRINT_Q IS NOT NULL "
        'sql = sql & " AND (STD_CHK_PRINTER.IS_VALID_STR_PRINTER(PRINT_Q,STD_CHK_PRINTER.GET_HOME_STR("
        'sql = sql & "'" & v_user_init & "')) = 'Y'"
        'sql = sql & " OR STD_CHK_PRINTER.IS_SUPR_USR('" & v_user_init & "') = 'Y') "
        'sql = sql & " and exists (select 'x' from misc.console c where c.print_q = a.print_q) "
        'sql = sql & " and print_q <> '" & v_default_printer & "'"
        'sql = sql & " ORDER BY PRINT_Q "

        'Daniela Sept 08
        sql = "select pi.printer_nm PRINT_Q "
        sql = sql & "from  printer_info pi "
        sql = sql & "where pi.store_cd = (select home_store_cd "
        sql = sql & "from emp "
        sql = sql & "where emp_init = :v_user_init) "
        sql = sql & "and   pi.printer_tp = 'INVOICE'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub
    Public Sub isAeroplanPromptAllow()
        'Oct 20,2016-mariam- check if Homestore of user valid for Aeroplan card promotion
        Dim isValid As Boolean
        isValid = False
        'call sp to get valid or not to Prompt
        If Not Session("emp_init") Is Nothing Then
            isValid = OrderUtils.isAeroplanPromptAllow(Session("emp_init").ToString())
            'isValid = True  'For test- remove when deploy to DEV
            If isValid Then
                btnPromoCard.Visible = True
            End If
        End If
    End Sub
    '�ct 21,2016-mariam - Aeroplan Promo
    Protected Sub btnSubmitAeroplan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitAeroplan.Click
        '�ct 21,2016-mariam - Aeroplan Promo
        'Get the Aerplan card no from user and saves in Session
        'In button commit -> after SaveOrder, insert Aeroplan details into table

        Dim isValidCardNum As Boolean
        lblErrorMsg.Text = String.Empty
        lblErrorMsg.Visible = False

        'Validate Aeroplan Card Num
        isValidCardNum = OrderUtils.isValidAeroplanCardNum(txtAerplanNum.Text.ToString())
        If isValidCardNum Then
            Session("AeroplanCardNum") = txtAerplanNum.Text.ToString()
            btnPromoCard.Visible = False
            ASPxPopupAeroplanPromo.ShowOnPageLoad = False
        Else
            lblErrorMsg.Text = Resources.LibResources.Label973.ToString()
            lblErrorMsg.Visible = True
            lblErrorMsg.ForeColor = Color.Red
        End If
        Exit Sub
    End Sub
    Protected Sub btnNobtnCanelAeroplan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCanelAeroplan.Click
        'Oct 20,2016-mariam - Aeroplan Promo
        lblErrorMsg.Visible = False
        ASPxPopupAeroplanPromo.ShowOnPageLoad = False
    End Sub
    Protected Sub btnPromoCard_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Oct 21,2016-mariam - Aeroplan Promo
        ASPxPopupAeroplanPromo.ShowOnPageLoad = True
    End Sub
End Class
