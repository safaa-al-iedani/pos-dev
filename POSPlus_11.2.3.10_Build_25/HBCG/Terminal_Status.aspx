<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Terminal_Status.aspx.vb" Inherits="Terminal_Status" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <div id="frmsubmit" runat="server">
            </div>
            Filter by Store: &nbsp;<asp:DropDownList ID="cbo_filter_store" runat="server" AutoPostBack="True"
                CssClass="style5">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_open_all" runat="server" Text="Open All Terminals" CssClass="style5" />
            <br />
            <asp:DataGrid Style="position: relative; top: 9px" ID="Gridview1" runat="server"
                CssClass="style5" Width="668px" Height="156px" Font-Underline="False" Font-Strikeout="False"
                Font-Overline="False" Font-Italic="False" Font-Bold="False" DataKeyField="TERMINAL_IP"
                CellPadding="2" AutoGenerateColumns="False">
                <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                    Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                    Font-Bold="True" ForeColor="Black"></HeaderStyle>
                <Columns>
                    <asp:BoundColumn DataField="terminal_ip" HeaderText="IP Address" ReadOnly="True">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="terminal_name" HeaderText="Terminal Name" ReadOnly="True">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Store-Drawer">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.STORE_CD") %>'></asp:Label>
                            -
                            <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CASH_DWR_CD") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" />
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status">
                        <ItemTemplate>
                            <asp:DropDownList ID="cbo_status" Width="70px" runat="server" DataTextField="STATUS"
                                DataValueField="STATUS" CssClass="style5" SelectedValue='<%# Bind("STATUS") %>'
                                AutoPostBack="true" OnTextChanged="Store_Update">
                                <asp:ListItem Value="O" Text="Open"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Closed"></asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="status_dt" HeaderText="Status Date" ReadOnly="True" DataFormatString="{0:MM/dd/yyyy}">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="updated_emp" HeaderText="Employee" ReadOnly="True">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="last_updated" HeaderText="Last Updated" ReadOnly="True">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
            <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
