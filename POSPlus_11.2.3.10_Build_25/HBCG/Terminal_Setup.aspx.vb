Imports System.Data.OracleClient
Imports System.Net.NetworkInformation
Imports HBCG_Utils

Partial Class Terminal_Setup
    Inherits POSBasePage
    Dim ds As New DataSet
    Dim No_Ping As Boolean = True

    Public Function Ping_Check(ByVal IP_Address As String)

        Dim Ping As New Ping
        Dim PingReply As PingReply
        Try
            PingReply = Ping.Send(IP_Address)
            If PingReply.Status = IPStatus.Success Then
                Return "Success"
            Else
                Return "Failed"
            End If
        Catch
            Return "Failed"
        End Try

    End Function

    Public Sub bindgrid(Optional ByVal store_cd As String = "ALL")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds2 = New DataSet


        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT TERMINAL_IP, TERMINAL_NAME, STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP "
        If store_cd <> "ALL" Then
            sql = sql & "WHERE STORE_CD='" & store_cd & "' "
        End If
        sql = sql & "ORDER BY STORE_CD, CASH_DWR_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds2
                Gridview1.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            cbo_filter_store.Enabled = False
            btn_ping.Enabled = False
            txt_new_IP.Enabled = False
            txt_new_terminal.Enabled = False
            cbo_new_store.Enabled = False
            cbo_new_cash.Enabled = False
            btn_add.Enabled = False
            Exit Sub
        Else
            cbo_filter_store.Enabled = True
            btn_ping.Enabled = True
            txt_new_IP.Enabled = True
            txt_new_terminal.Enabled = True
            cbo_new_store.Enabled = True
            cbo_new_cash.Enabled = True
            btn_add.Enabled = True
        End If

        store_cd_populate()
        If Not IsPostBack Then
            bindgrid()
            cbo_new_store.DataSource = ds
            cbo_new_store.DataValueField = "store_cd"
            cbo_new_store.DataTextField = "full_desc"
            cbo_new_store.DataBind()
            csh_drw_populate()
            With cbo_filter_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            cbo_filter_store.Items.Insert(0, "ALL STORES")
            cbo_filter_store.Items.FindByText("ALL STORES").Value = "ALL"
            cbo_filter_store.SelectedIndex = 0
        End If

    End Sub

    Public Sub csh_drw_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter

        ds2 = New DataSet

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE STORE_CD='" & cbo_new_store.SelectedValue & "' order by csh_dwr_cd"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)

        With cbo_new_cash
            .DataSource = ds2
            .DataValueField = "csh_dwr_cd"
            .DataTextField = "full_desc"
            .DataBind()
        End With
        conn.Close()

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim TempList As DropDownList
        Dim strStCd As String = ""
        Dim Ping_Image As System.Web.UI.WebControls.Image

        TempList = e.Item.FindControl("cbo_store_cd")
        If Not TempList Is Nothing Then
            With TempList
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
                strStCd = e.Item.Cells(0).Text
                .Text = strStCd
            End With
        End If

        TempList = Nothing

        If Not String.IsNullOrEmpty(strStCd) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim ds2 As DataSet
            Dim oAdp As OracleDataAdapter

            ds2 = New DataSet

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            sql = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE STORE_CD='" & strStCd & "' order by csh_dwr_cd"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds2)

            conn.Close()

            TempList = e.Item.FindControl("cbo_csh_dwr")
            If Not TempList Is Nothing Then
                With TempList
                    .DataSource = ds2
                    .DataValueField = "csh_dwr_cd"
                    .DataTextField = "full_desc"
                    .DataBind()
                    strStCd = e.Item.Cells(1).Text
                    .Text = strStCd
                End With
            End If
        End If

        Ping_Image = e.Item.FindControl("Image1")
        If No_Ping = False Then
            If Not String.IsNullOrEmpty(e.Item.Cells(2).Text) Then
                If Ping_Check(e.Item.Cells(2).Text) = "Success" Then
                    If Not Ping_Image Is Nothing Then
                        Ping_Image.ImageUrl = "images/icons/tick.png"
                    End If
                Else
                    Ping_Image = e.Item.FindControl("Image1")
                    If Not Ping_Image Is Nothing Then
                        Ping_Image.ImageUrl = "images/icons/error.png"
                    End If
                End If
            End If
        Else
            If Not Ping_Image Is Nothing Then
                Ping_Image.ImageUrl = "images/icons/unknown.png"
            End If
        End If

    End Sub

    Protected Sub cbo_new_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_new_store.SelectedIndexChanged

        csh_drw_populate()

    End Sub

    Protected Sub Store_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds2 As DataSet
        Dim Templist As DropDownList
        Dim oAdp As OracleDataAdapter

        conn.Open()

        ds2 = New DataSet

        Dim textdata As DropDownList = CType(sender, DropDownList)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        sql = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE STORE_CD='" & textdata.Text & "' order by csh_dwr_cd"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)

        Templist = dgItem.FindControl("cbo_csh_dwr")
        If Not Templist Is Nothing Then
            With Templist
                .DataSource = ds2
                .DataValueField = "csh_dwr_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        End If
        'conn.Close()

        'conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        'conn.Open()

        sql = "UPDATE TERMINAL_SETUP "
        sql = sql & "SET STORE_CD='" & textdata.Text & "', "
        sql = sql & "CASH_DWR_CD='" & Templist.SelectedValue & "' "
        sql = sql & "WHERE TERMINAL_IP='" & dgItem.Cells(2).Text & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        conn.Close()

    End Sub

    Protected Sub btn_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add.Click

        Dim Error_Found As Boolean = False
        lbl_warnings.Text = "The following issues were encountered: "

        If Not IsValidIP(txt_new_IP.Text) Then
            lbl_warnings.Text = lbl_warnings.Text & "IP Address is invalid; "
            Error_Found = True
        End If
        If String.IsNullOrEmpty(txt_new_terminal.Text) Then
            lbl_warnings.Text = lbl_warnings.Text & "No Terminal Description Given; "
            Error_Found = True
        End If
        If String.IsNullOrEmpty(cbo_new_store.SelectedValue.ToString) Then
            lbl_warnings.Text = lbl_warnings.Text & "Store Code must be selected; "
            Error_Found = True
        End If
        If String.IsNullOrEmpty(cbo_new_cash.SelectedValue.ToString) Then
            lbl_warnings.Text = lbl_warnings.Text & "Cash Drawer must be selected; "
            Error_Found = True
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim MyDatareader As OracleDataReader

        conn.Open()

        Dim Insert_Record As Boolean = True

        Dim IPArray As Array
        IPArray = Split(txt_new_IP.Text.Trim, ".")

        If UBound(IPArray) <> 3 Then
            Insert_Record = False
        Else
            Insert_Record = True
        End If

        sql = "SELECT TERMINAL_IP FROM TERMINAL_SETUP WHERE TERMINAL_IP=:TERMINAL_IP AND STORE_CD=:STORE_CD AND CASH_DWR_CD=:CASH_DWR_CD"
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":TERMINAL_IP", OracleType.VarChar)
        objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objSql.Parameters.Add(":CASH_DWR_CD", OracleType.VarChar)

        objSql.Parameters(":TERMINAL_IP").Value = txt_new_IP.Text.Trim
        objSql.Parameters(":STORE_CD").Value = cbo_new_store.SelectedValue.ToString
        objSql.Parameters(":CASH_DWR_CD").Value = cbo_new_cash.SelectedValue.ToString

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDatareader.Read() Then
                If Not String.IsNullOrEmpty(MyDatareader.Item("TERMINAL_IP").ToString) Then
                    Insert_Record = False
                End If
            End If

            'Close Connection 
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Insert_Record = True Then
            sql = "INSERT INTO TERMINAL_SETUP (TERMINAL_IP, TERMINAL_NAME, STORE_CD, CASH_DWR_CD) "
            sql = sql & "VALUES(:TERMINAL_IP, :TERMINAL_NAME, :STORE_CD, :CASH_DWR_CD)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":TERMINAL_IP", OracleType.VarChar)
            objSql.Parameters.Add(":TERMINAL_NAME", OracleType.VarChar)
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CASH_DWR_CD", OracleType.VarChar)

            objSql.Parameters(":TERMINAL_IP").Value = txt_new_IP.Text.Trim
            objSql.Parameters(":TERMINAL_NAME").Value = txt_new_terminal.Text
            objSql.Parameters(":STORE_CD").Value = cbo_new_store.SelectedValue.ToString
            objSql.Parameters(":CASH_DWR_CD").Value = cbo_new_cash.SelectedValue.ToString

            objSql.ExecuteNonQuery()
        End If

        If Insert_Record = True Then
            sql = "SELECT TERMINAL_IP FROM TERMINAL_STATUS WHERE TERMINAL_IP=:TERMINAL_IP"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":TERMINAL_IP", OracleType.VarChar)
            objSql.Parameters(":TERMINAL_IP").Value = txt_new_IP.Text

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDatareader.Read() Then
                    If Not String.IsNullOrEmpty(MyDatareader.Item("TERMINAL_IP").ToString) Then
                        Insert_Record = False
                    End If
                End If

                'Close Connection 
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            sql = "INSERT INTO TERMINAL_STATUS (TERMINAL_IP, STATUS, UPDATED_EMP) "
            sql = sql & "VALUES(:TERMINAL_IP, :STATUS, :UPDATED_EMP)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":TERMINAL_IP", OracleType.VarChar)
            objSql.Parameters.Add(":STATUS", OracleType.VarChar)
            objSql.Parameters.Add(":UPDATED_EMP", OracleType.VarChar)

            objSql.Parameters(":TERMINAL_IP").Value = txt_new_IP.Text
            objSql.Parameters(":STATUS").Value = "O"
            objSql.Parameters(":UPDATED_EMP").Value = Session("EMP_CD").ToString

            objSql.ExecuteNonQuery()
            objSql.Dispose()
        End If

        conn.Close()

        'Inform User of Changes
        lbl_warnings.Text = "Terminal Added and Opened"
        txt_new_IP.Text = ""
        txt_new_terminal.Text = ""

        store_cd_populate()

        bindgrid()

    End Sub

    Public Function IsValidIP(ByVal addr As String) As Boolean

        'create our match pattern
        Dim pattern As String = "^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\." & _
        "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
        'create our Regular Expression object
        Dim check As New System.Text.RegularExpressions.Regex(pattern)
        'boolean variable to hold the status
        Dim valid As Boolean = False
        'check to make sure an ip address was provided
        If addr = "" Then
            'no address provided so return false
            valid = False
        Else
            'address provided so use the IsMatch Method
            'of the Regular Expression object
            valid = check.IsMatch(addr, 0)
        End If
        'return the results
        Return valid

    End Function

    Protected Sub btn_ping_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        No_Ping = False
        bindgrid(cbo_filter_store.SelectedValue.ToString)
        No_Ping = True

    End Sub

    Protected Sub cbo_filter_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_filter_store.SelectedIndexChanged

        bindgrid(cbo_filter_store.SelectedValue.ToString)

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
