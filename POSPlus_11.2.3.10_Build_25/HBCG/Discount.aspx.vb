﻿Imports HBCG_Utils

Partial Class Discount
    Inherits POSBasePage
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
    End Sub

    Protected Sub Page_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        If Not IsPostBack Then
            ucDiscount.ReqSource = "SOE"
            ucDiscount.LoadDiscount(AppConstants.Order.STATUS_OPEN)
        End If
    End Sub

    Protected Sub ucDiscount_DiscountApproved(sender As Object, e As EventArgs) Handles ucDiscount.DiscountApproved
        mgrApproval.Value = Session("mgrApproval")
    End Sub
    Protected Sub ucDiscount_DiscountApplied(sender As Object, e As EventArgs) Handles ucDiscount.DiscountApplied
        calculate_total(Page.Master)
        mgrApproval.Value = Session("mgrApproval")
    End Sub
    'MM-5661
    Protected Sub ucDiscount_CancelledManagerApproval(sender As Object, e As EventArgs) Handles ucDiscount.CancelledManagerApproval
        Session("mgrApproval") = "N"
        mgrApproval.Value = Session("mgrApproval")
        Session("IsCanceled") = True
        'MM-9870
        'Updating 'N' if click Cancel on the ManagerApproval Screen
        Dim discountsApplied As New DataTable
        Dim discountGrid As DevExpress.Web.ASPxGridView.ASPxGridView = ucDiscount.FindControl("grdDiscItems")
        Dim discountRowCount As Integer = discountGrid.VisibleRowCount
        If (SysPms.discWithAppSec = "W") AndAlso ((Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
            If Not IsNothing(Session("discountsApplied")) Then
                discountsApplied = Session("discountsApplied")
                Dim drs = (From u In discountsApplied.AsEnumerable() _
                                               Where u.Field(Of String)("Approved") = "A"
                                                Select u)
                For Each Row As DataRow In drs
                    If Row("DISC_IS_EXISTING") AndAlso Row("Approved").ToString.Equals("A") AndAlso IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(False) Then
                        Row("Approved") = "Y"
                    Else
                        Row("Approved") = "N"
                    End If
                Next

            End If
            If Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
                ReapplyDiscounts(ReapplyDiscountRequestDtc.LnChng.Void, String.Empty, True)
                discountsApplied = Session("discountsApplied")
                If (discountsApplied.Rows.Count > 0) Then
                    For i As Integer = discountsApplied.Rows.Count - 1 To 0 Step -1
                        If discountsApplied.Rows(i)("Approved") = "N" Then
                            discountsApplied.Rows.Remove(discountsApplied.Rows(i))
                        End If
                    Next

                End If
                If discountsApplied.Rows.Count() > 0 Then
                    ucDiscount.BindDiscountsApplied(discountsApplied)
                Else
                    ' If discountsApplied table is not containing any rows Hiding the discounts gridview and  Successful label message is hiding 
                    ucDiscount.FindControl("grdDiscItems").Visible = False
                    ucDiscount.FindControl("lblMessage").Visible = False
                End If
            End If

        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'mariam-Sept 27,2016 - Price change approve
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            If Request("LEAD") <> "TRUE" Then
                Response.Redirect("order_detail.aspx")
            Else
                Response.Redirect("order_detail.aspx?LEAD=TRUE")
            End If
        End If
        'mariam -end

        mgrApproval.Value = Session("mgrApproval")
    End Sub
    'MM-9870
    ''' <summary>
    ''' Recalculates the discounts (when applicable)
    ''' </summary>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">the ID for the line that triggered this event</param>
    Private Sub ReapplyDiscounts(ByVal action As ReapplyDiscountRequestDtc.LnChng, ByVal lnSeq As String, Optional ByVal isCanceled As Boolean = False)

        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd")) Then

            Dim discRequest As ReapplyDiscountRequestDtc
            discRequest = New ReapplyDiscountRequestDtc(action, Session("pd"), Session.SessionID.ToString.Trim, lnSeq)
            If isCanceled Then
                If Not IsNothing(Session("isSeqNum")) Then
                    theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest, True, Session("isSeqNum"))
                Else
                    theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest, True)
                End If


            Else
                theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest)
            End If

        End If

        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub
End Class
