Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class NewsMaintenance
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn.Open()

        sql = "SELECT COMMENT_TEXT FROM COMMENT_SETUP WHERE COMMENT_TP='MAIN'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read() Then
                ASPxHtmlEditor1.Html = MyDataReader.Item("COMMENT_TEXT").ToString
            Else
                sql = "INSERT INTO COMMENT_SETUP (COMMENT_TEXT, COMMENT_TP) VALUES(NULL,'MAIN')"
                Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql2.ExecuteNonQuery()
                objSql2.Dispose()
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            admin_form.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        conn.Open()

        sql = "UPDATE COMMENT_SETUP SET COMMENT_TEXT=:COMMENT_TEXT WHERE COMMENT_TP='MAIN'"
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":COMMENT_TEXT", OracleType.Clob)
        If ASPxHtmlEditor1.Html & "" = "" Then
            objSql.Parameters(":COMMENT_TEXT").Value = DBNull.Value
        Else
            objSql.Parameters(":COMMENT_TEXT").Value = ASPxHtmlEditor1.Html
        End If

        objSql.ExecuteNonQuery()
        objSql.Dispose()
        conn.Close()

        lbl_help_id.Text = "Changes saved."

    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click

        bindgrid()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
