'Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq


Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class SRRTD
    Inherits POSBasePage

    Dim SecurityCache As New System.Collections.Generic.Dictionary(Of String, Boolean)

    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If IsPostBack = False Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As StringBuilder
            Dim ds As New DataSet
            Dim Mydatareader As OracleDataReader
            Dim homeStoreCode As String
            Dim companyCode As String
            Dim coGroupCode As String

            ' lbl_versionInfo.Text = gs_version
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            btn_search.Enabled = False
            btn_clr_screen.Enabled = False

            If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
                Return
            End If

            'Dim str_emp_init As String = LeonsBiz.GetEmpInit(Session("EMP_CD"))
            Dim str_emp_init As String = Session("EMP_INIT")

            'If is_sup_user(str_emp_init) <> "Y" Then  ' not a super user
            ' Use session for performance

            'sabrina R3442 - comment out as per Warrren
            'If Not Session("str_sup_user_flag").Equals("Y") Then
            'Response.Redirect("srrtd_other.aspx")
            'End If
            'sabrina R3442 - comment out as per Warrren


            sqlString = New StringBuilder("SELECT e.home_store_cd, s.co_cd, cg.co_grp_cd FROM emp e, store s, co_grp cg  ")
            sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd AND e.emp_cd = UPPER(:p_emp_cd)")
            objCmd.CommandText = sqlString.ToString()
            objCmd.Connection = conn
            objCmd.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            objCmd.Parameters(":p_emp_cd").Value = Session("EMP_CD").ToString().ToUpper()


            Try
                conn.Open()
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)

                If Mydatareader.Read() Then

                    '   txt_company_code.Text = Mydatareader.Item("CO_CD").ToString()
                    '   txt_company_code.Enabled = False
                    companyCode = Mydatareader.Item("CO_CD").ToString()

                    homeStoreCode = Mydatareader.Item("HOME_STORE_CD").ToString()
                    Session("HOME_STORE_CD") = homeStoreCode

                    coGroupCode = Mydatareader.Item("CO_GRP_CD").ToString()
                    Session("CO_GRP_CD") = coGroupCode

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            If String.IsNullOrEmpty(homeStoreCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find home store code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0037
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            ElseIf String.IsNullOrEmpty(coGroupCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find company group code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0038
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            Else
                sqlString = New StringBuilder("SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s   ")
                'sabrina R3442
                sqlString.Append("where std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'")
                If is_sup_user(str_emp_init) <> "Y" Then
                    If homeStoreCode = "00" Then
                        sqlString.Append(" and s.co_cd not like 'B%'")
                    ElseIf homeStoreCode = "10" Then
                        sqlString.Append(" and s.co_cd like 'B%' ")
                    Else
                        'do nothing
                    End If
                End If
                'sabrina R3442
                If (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    sqlString.Append(" UNION SELECT sg.store_grp_cd store_cd, sg.store_grp_cd || ' - ' ||  sg.des as full_desc ")
                    sqlString.Append(" FROM store_grp sg ")
                    sqlString.Append("WHERE std_multi_co2.isvalidstrgrp3(:str_emp_init,sg.store_grp_cd) ='Y'") 'sabrina R3442
                    sqlString.Append("AND length(sg.store_grp_cd) >2 ")
                    sqlString.Append("AND exists (select 'x' from store_grp$store sg1, store s1 ") 'sabrina R3442
                    sqlString.Append(" where sg1.store_cd = s1.store_cd ")
                    sqlString.Append("   and sg1.store_grp_cd = sg.store_grp_cd")
                    sqlString.Append(" and std_multi_co2.isvalidstr(:str_emp_init,s1.store_cd) = 'Y')")

                    If Not CheckSecurity("POSSRRTDLF", Session("EMP_CD")) Then
                        sqlString.Append(" and sg.store_grp_cd <> 'ALL' ")
                    End If
                    'sabrina R3442
                    If is_sup_user(str_emp_init) <> "Y" Then
                        If homeStoreCode = "00" Then
                            sqlString.Append(" and std_srrtd.isBrickStrGrp(sg.store_grp_cd) = 'N'")
                        ElseIf homeStoreCode = "10" Then
                            sqlString.Append(" and std_srrtd.isBrickStrGrp(sg.store_grp_cd) = 'Y'")
                        Else
                            'do nothing
                        End If

                    End If
                    'sabrina R3442
                End If

                objCmd.CommandText = sqlString.ToString()
                objCmd.Connection = conn
                objCmd.Parameters.Clear()
                objCmd.Parameters.Add(":str_emp_init", OracleType.VarChar) '- sabrina R3442 uncomment
                objCmd.Parameters(":str_emp_init").Value = str_emp_init    '- sabrina R3442 uncomment

                Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

                Try
                    dataAdapter.Fill(ds)
                Catch
                    Throw
                    conn.Close()
                End Try
                conn.Close()
                If ds.Tables(0).Rows.Count = 0 Then
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
                    lbl_warning.Text = Resources.CustomMessages.MSG0039
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                    Return

                End If

                Dim defaultStoreIndex As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows

                    drpdwn_store.Items.Add(row.ItemArray(1).ToString())
                    dw_st.Items.Add(row.ItemArray(0).ToString())         ' store_grp_cd/store_cd
                    If row.ItemArray(0).ToString = homeStoreCode.ToString() Then
                        defaultStoreIndex = ds.Tables(0).Rows.IndexOf(row)
                    End If
                Next
                'this is defaul to home_store, commented
                drpdwn_store.SelectedIndex = defaultStoreIndex
                dw_st.SelectedIndex = defaultStoreIndex
                Session("defaultStoreIndex") = defaultStoreIndex


            End If
        End If
        drpdwn_w_d.Items.Add("W")
        drpdwn_w_d.Items.Add("D")

        populate_itm_tp()
        populate_inv_tp()

        Dim l_from_detail As String = Session("srrtd_from_detail")
        If l_from_detail = "Y" Then

            get_inv_itm_tp()


            drpdwn_store.SelectedIndex = Session("srrtd_store_index")
            dw_st.SelectedIndex = Session("srrtd_store_index")
            drpdwn_w_d.SelectedIndex = Session("srrtd_w_d_index")
            drpdwn_inv_tp.SelectedIndex = Session("srrtd_inv_tp_cd_index")
            drpdwn_itm_tp.SelectedIndex = Session("srrtd_itm_tp_cd_index")
            show_stores_summary()

        End If
        btn_search.Enabled = True
        btn_clr_screen.Enabled = True


    End Sub
    Protected Sub show_stores_summary()

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_w_d As String
        Dim l_inv_tp_cd As String
        Dim l_itm_tp_cd As String

        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button
        If IsNumeric(l_id) Then
            Exit Sub

        End If



         

        l_id = Session("srrtd_id")


        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            sqlString = New StringBuilder("SELECT  ")
            'Landed Cost Logic Starts
            sqlString.Append(" id,s_f,store_cd,GROSS_SALE , VOID_SALE, NET_SALE, NET_MGN ")
            sqlString.Append(" ,furn_mix, furn_net_mgn ")
            sqlString.Append(" ,matt_mix, matt_net_mgn ")
            sqlString.Append(" ,appl_mix, appl_net_mgn ")
            sqlString.Append(" ,elec_mix, elec_net_mgn ")
            sqlString.Append(" ,out_net_sale, out_net_mgn, out_net_qty ")
            sqlString.Append("FROM srrtd_output ")
            sqlString.Append("WHERE id =:l_id and store_cd ='ALL'")
            ' sqlString.Append("  order by rec_no")

            ds = New DataSet
            GV_SRR.DataSource = ""

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()
            objcmd.Parameters.Add(":l_id", OracleType.VarChar)
            objcmd.Parameters(":l_id").Value = l_id

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                GV_SRR.Columns(0).Visible = True
               
            End If
            'sabrina R3442
            If ds.Tables(0).Rows.Count = 0 Then
                btn_slsp.Visible = False
            Else
                btn_slsp.Visible = True
            End If
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

        
    End Sub
    Protected Sub cp_Page_Init(sender As Object, e As EventArgs)

        If IsPostBack = False Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As StringBuilder
            Dim ds As New DataSet
            Dim Mydatareader As OracleDataReader
            Dim homeStoreCode As String
            Dim companyCode As String
            Dim coGroupCode As String
            ' lbl_versionInfo.Text = gs_version
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            btn_search.Enabled = False
            btn_clr_screen.Enabled = False

            If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
                Return
            End If

            Dim str_emp_init As String = LeonsBiz.GetEmpInit(Session("EMP_CD"))

            sqlString = New StringBuilder("SELECT e.home_store_cd, s.co_cd, cg.co_grp_cd FROM emp e, store s, co_grp cg  ")
            sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd AND e.emp_cd = UPPER(:p_emp_cd)")
            objCmd.CommandText = sqlString.ToString()
            objCmd.Connection = conn
            objCmd.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            objCmd.Parameters(":p_emp_cd").Value = Session("EMP_CD").ToString().ToUpper()
            Try
                conn.Open()
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)

                If Mydatareader.Read() Then

                    '   txt_company_code.Text = Mydatareader.Item("CO_CD").ToString()
                    '   txt_company_code.Enabled = False
                    companyCode = Mydatareader.Item("CO_CD").ToString()

                    homeStoreCode = Mydatareader.Item("HOME_STORE_CD").ToString()
                    Session("HOME_STORE_CD") = homeStoreCode

                    coGroupCode = Mydatareader.Item("CO_GRP_CD").ToString()
                    Session("CO_GRP_CD") = coGroupCode

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            If String.IsNullOrEmpty(homeStoreCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find home store code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0037
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            ElseIf String.IsNullOrEmpty(coGroupCode) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find company group code. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0038
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                conn.Close()
                Return
            Else
                sqlString = New StringBuilder("SELECT s.store_cd, s.store_cd || ' - ' || s.store_name as full_desc FROM store s   ")
                'sqlString.Append("where std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'") 'lucy DB not needed for super users

                If (homeStoreCode = "00" Or homeStoreCode = "10") Then
                    sqlString.Append(" UNION SELECT sg.store_grp_cd store_cd, sg.store_grp_cd || ' - ' ||  sg.des as full_desc FROM store_grp sg ")
                    sqlString.Append("WHERE EXISTS (SELECT 1 FROM store_grp$store sgs, store s  ")
                    sqlString.Append("WHERE sgs.store_grp_cd = sg.store_grp_cd AND sgs.store_cd = s.store_cd ")
                    'sqlString.Append("AND std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'") 'lucy DB not needed for super users
                    sqlString.Append(" and length(sg.store_grp_cd) > 2 ")
                    ' sqlString.Append(" and sg.store_grp_cd <> 'ALL' ")

                    ' Top User sec_cd='POSSRRTDLF'
                    If Not CheckSecurity("POSSRRTDLF", Session("EMP_CD")) Then
                        sqlString.Append(" and sg.store_grp_cd <> 'ALL' ")
                    End If
                    sqlString.Append(")")
                End If
          
                objCmd.CommandText = sqlString.ToString()
                objCmd.Connection = conn
                objCmd.Parameters.Clear()
                objCmd.Parameters.Add(":str_emp_init", OracleType.VarChar)
                objCmd.Parameters(":str_emp_init").Value = str_emp_init

                Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

                Try
                    dataAdapter.Fill(ds)
                Catch
                    Throw
                    conn.Close()
                End Try
                conn.Close()
                If ds.Tables(0).Rows.Count = 0 Then
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
                    lbl_warning.Text = Resources.CustomMessages.MSG0039
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                    Return
                End If
                Dim defaultStoreIndex As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows

                    drpdwn_store.Items.Add(row.ItemArray(1).ToString())
                    dw_st.Items.Add(row.ItemArray(0).ToString())         ' store_grp_cd/store_cd
                    If row.ItemArray(0).ToString = homeStoreCode.ToString() Then
                        defaultStoreIndex = ds.Tables(0).Rows.IndexOf(row)
                    End If
                Next
                'this is defaul to home_store, commented
                drpdwn_store.SelectedIndex = defaultStoreIndex
                dw_st.SelectedIndex = defaultStoreIndex
                Session("defaultStoreIndex") = defaultStoreIndex


            End If
        End If

        drpdwn_w_d.Items.Add("W")
        drpdwn_w_d.Items.Add("D")

        populate_itm_tp()
        populate_inv_tp()

        btn_search.Enabled = True
        btn_clr_screen.Enabled = True

    End Sub
    Public Sub populate_itm_tp()
        '  If IsPostBack = False Then
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        Dim ds As New DataSet
        Dim Mydatareader As OracleDataReader
        Dim homeStoreCode As String
        Dim companyCode As String
        Dim coGroupCode As String
        ' lbl_versionInfo.Text = gs_version
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString




        sqlString = New StringBuilder("SELECT distinct itm_tp_cd FROM itm_tp  ")
        objCmd.CommandText = sqlString.ToString()
        objCmd.Connection = conn


        conn.Open()
        'Execute DataReader 
        Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)

        Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

        Try
            dataAdapter.Fill(ds)
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()
        drpdwn_itm_tp.Items.Clear()
        drpdwn_itm_tp.Items.Add("ALL")
        Dim defaultIndex As Integer = 0
        For Each row As DataRow In ds.Tables(0).Rows
            drpdwn_itm_tp.Items.Add(row.ItemArray(0).ToString())

            defaultIndex = ds.Tables(0).Rows.IndexOf(row)
        Next


        'this is defaul to ALL

        drpdwn_itm_tp.SelectedIndex = 0



        '   End If


    End Sub
    Public Sub populate_inv_tp()
         
        drpdwn_inv_tp.Items.Add("B")
        drpdwn_inv_tp.Items.Add("Y")
        drpdwn_inv_tp.Items.Add("N")

        'this is defaul to Y and N

        drpdwn_inv_tp.SelectedIndex = 0






    End Sub
    Public Sub get_inv_itm_tp()
        'If IsPostBack = False Then
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        Dim ds As New DataSet



        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim l_inv_tp As String = drpdwn_inv_tp.SelectedValue
        Dim l_from_detail As String = Session("srrtd_from_detail")

        If l_from_detail = "Y" Then

            drpdwn_inv_tp.SelectedIndex = Session("srrtd_inv_tp_cd_index")

            l_inv_tp = drpdwn_inv_tp.SelectedValue
        End If
        sqlString = New StringBuilder("SELECT distinct itm_tp_cd FROM itm_tp  ")
        If l_inv_tp = "Y" Then
            sqlString.Append(" WHERE inventory='Y'")
        ElseIf l_inv_tp = "N" Then
            sqlString.Append(" WHERE inventory='N'")
        End If

        objCmd.CommandText = sqlString.ToString()
        objCmd.Connection = conn

        conn.Open()


        Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

        Try
            dataAdapter.Fill(ds)
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Dim defaultIndex As Integer = 0
        drpdwn_itm_tp.Items.Clear()
        drpdwn_itm_tp.Items.Add("ALL")
        For Each row As DataRow In ds.Tables(0).Rows
            drpdwn_itm_tp.Items.Add(row.ItemArray(0).ToString())

            defaultIndex = ds.Tables(0).Rows.IndexOf(row)
        Next


        'this is defaul to ALL

        drpdwn_itm_tp.SelectedIndex = 0


    End Sub
    Public Function is_sup_user(ByRef p_emp_init As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrtd_security.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = p_emp_init
        myCMD.Parameters.Add("p_flag", OracleType.VarChar, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_flag").Value) = False Then
                str_id = myCMD.Parameters("p_flag").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_id
    End Function

    Public Function is_top_user(ByRef p_emp_init As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrtd_security.isTopUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = p_emp_init
        myCMD.Parameters.Add("p_flag", OracleType.VarChar, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_flag").Value) = False Then
                str_id = myCMD.Parameters("p_flag").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_id
    End Function

    Public Function calc_sale(ByRef p_store_grp_cd As String, ByRef p_w_d As String, ByRef p_inv_tp_cd As String, ByRef p_itm_tp_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrtd.pos_input"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_store_grp_cd", OracleType.VarChar)).Value = p_store_grp_cd
        myCMD.Parameters.Add(New OracleParameter("p_w_d", OracleType.VarChar)).Value = p_w_d
        myCMD.Parameters.Add(New OracleParameter("p_inv_tp_cd", OracleType.VarChar)).Value = p_inv_tp_cd
        myCMD.Parameters.Add(New OracleParameter("p_itm_tp_cd", OracleType.VarChar)).Value = p_itm_tp_cd
        myCMD.Parameters.Add("p_id", OracleType.Number, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_id").Value) = False Then
                str_id = myCMD.Parameters("p_id").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_id
    End Function

    Public Sub clean_srrtd_output(ByRef p_id As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()




        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrtd.cleanup"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("l_id", OracleType.VarChar)).Value = p_id


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            Session("srrtd_id") = "N"

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try

    End Sub

    ''' <summary>
    ''' To set title for this page.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0079
        _lblHeader.Text = "Sales Register Report"
    End Sub


    ' lucy
    Protected Sub click_btn_search(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_w_d As String
        Dim l_inv_tp_cd As String
        Dim l_itm_tp_cd As String

        'sabrina R3442
        ASPxPopupslsp.ShowOnPageLoad = False
        slsp_strs.Items.Clear()
        GV_SLSP.DataSource = ""
        GV_SLSP.DataBind()
        'sabrina R3442

        lbl_msg.Text = Nothing
        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button
        If IsNumeric(l_id) Then
            Exit Sub

        End If

        If Not IsDBNull(Session("srrtd_id")) Or Not isEmpty(Session("srrtd_id")) Then
            l_id = Session("srrtd_id")
            clean_srrtd_output(l_id)
        End If
        Session("from_detail") = "N"


        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


        l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        l_w_d = drpdwn_w_d.SelectedItem.Value
        l_inv_tp_cd = drpdwn_inv_tp.SelectedItem.Value
        l_itm_tp_cd = drpdwn_itm_tp.SelectedItem.Value

        l_id = calc_sale(l_store_grp_cd, l_w_d, l_inv_tp_cd, l_itm_tp_cd)
        Session("srrtd_slsp_id") = l_id
        Session("srrtd_id") = l_id
        Session("srrtd_store_grp") = l_store_grp_cd
        Session("srrtd_w_d") = l_w_d
        Session("srrtd_itm_tp_cd") = l_itm_tp_cd
        Session("srrtd_inv_tp_cd") = l_inv_tp_cd

        Session("srrtd_store_index") = drpdwn_store.SelectedIndex
        Session("srrtd_w_d_index") = drpdwn_w_d.SelectedIndex
        Session("srrtd_inv_tp_cd_index") = drpdwn_inv_tp.SelectedIndex
        Session("srrtd_itm_tp_cd_index") = drpdwn_itm_tp.SelectedIndex
        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            sqlString = New StringBuilder("SELECT  ")
            'Landed Cost Logic Starts
            sqlString.Append(" id,s_f,store_cd,GROSS_SALE , VOID_SALE, NET_SALE, NET_MGN ")
            sqlString.Append(" ,furn_mix, furn_net_mgn ")
            sqlString.Append(" ,matt_mix, matt_net_mgn ")
            sqlString.Append(" ,appl_mix, appl_net_mgn ")
            sqlString.Append(" ,elec_mix, elec_net_mgn ")
            sqlString.Append(" ,out_net_sale, out_net_mgn, out_net_qty ")
            sqlString.Append("FROM srrtd_output ")
            sqlString.Append("WHERE id =:l_id and store_cd='ALL'")

            ds = New DataSet
            GV_SRR.DataSource = ""

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()
            objcmd.Parameters.Add(":l_id", OracleType.VarChar)
            objcmd.Parameters(":l_id").Value = l_id

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()




                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then

                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_slsp.Visible = False 'sabrina R3442
            Else
                btn_slsp.Visible = True 'sabrina R3442
                If Len(l_store_grp_cd) = 2 Then
                    GV_SRR.Columns(0).Visible = False
                Else
                    GV_SRR.Columns(0).Visible = True
                End If
            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub

    Protected Sub show_detail()


        Response.Redirect("srrtd_detail.aspx")
    End Sub






    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To Clear Grid, search criteria and remove sessions
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clr_screen_Click(sender As Object, e As EventArgs)
        Dim l_id As String

        If Not IsDBNull(Session("srrtd_id")) Or Not isEmpty(Session("srrtd_id")) Then
            l_id = Session("srrtd_id")
            clean_srrtd_output(l_id)

        End If
        Session("from_detail") = "N"
        GV_SRR.Enabled = False
        GV_SRR.Visible = False
        Session("GV_NASOT") = Nothing
        Session("srrtd_from_detail") = "N"
        Session("srrtd_id") = Nothing
        Session("srrtd_slsp_id") = Nothing 'sabrina R3442
        Session("srrtd_store_grp") = Nothing
        Session("srrtd_w_d") = Nothing
        Session("srrtd_itm_tp_cd") = Nothing
        Session("srrtd_inv_tp_cd") = Nothing

        Session("srrtd_store_index") = Nothing
        Session("srrtd_w_d_index") = Nothing
        Session("srrtd_inv_tp_cd_index") = Nothing
        Session("srrtd_itm_tp_cd_index") = Nothing


        populate_itm_tp()
        drpdwn_store.SelectedIndex = 0
        drpdwn_w_d.SelectedIndex = 0
        drpdwn_inv_tp.SelectedIndex = 0
        drpdwn_itm_tp.SelectedIndex = 0

      


        lbl_warning.Text = ""
        lbl_msg.Text = ""

        'sabrina R3442
        btn_slsp.Visible = False 
        ASPxPopupslsp.ShowOnPageLoad = False
        slsp_strs.Items.Clear()
        GV_SLSP.DataSource = ""
        GV_SLSP.DataBind()
        'sabrina R3442


    End Sub


    ''' <summary>
    ''' Used this transient cache to avoid hitting the database multiple times for
    ''' every security and every line placed on the order, mostly during the
    ''' itemdatabound process
    ''' </summary>
    ''' <param name="key">the Security KEY that needs to be read</param>
    ''' <param name="emp">the Employee code to check security for</param>
    ''' <returns>TRUE, if user has the security indicated; FALSE otherwise</returns>
    Private Function CheckSecurity(key As String, emp As String) As Boolean
        Dim catKey As String = key + "~" + emp
        If Not SecurityCache.ContainsKey(catKey) Then
            SecurityCache.Add(catKey, SecurityUtils.hasSecurity(key, emp))
        End If
        Return SecurityCache(catKey)
    End Function
    Sub populate_slsp_strs()
        'sabrina R3442 (new sub)
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String = ""
        Dim sql2 As String = ""

        Dim co_grp_cd As String = ""
        If Session("CO_CD") & "" <> "" Then
            co_grp_cd = LeonsBiz.GetGroupCode(Session("CO_CD"))
        End If

        slsp_strs.Items.Clear()


        sql = " select store_cd str_cd from store_grp$store "
        sql = sql & " where store_grp_cd = '" & dw_st.SelectedItem.Value.ToString() & "'"
        sql = sql & " order by 1 "


        slsp_strs.Items.Insert(0, "-")
        slsp_strs.Items.FindByText("-").Value = ""
        slsp_strs.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With slsp_strs
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "str_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try
    End Sub
    Sub show_slsp_detail()
        'sabrina R3442 (new sub)

        ASPxPopupslsp.ShowOnPageLoad = False
        slsp_strs.Items.Clear()
        GV_SLSP.DataSource = ""
        GV_SLSP.DataBind()

        populate_slsp_strs()
        ASPxPopupslsp.ShowOnPageLoad = True

    End Sub

    Sub populate_slsp_detail()
        'sabrina R3442 (new sub)
        Dim l_id As String = Session("srrtd_slsp_id")
        Dim l_store_cd As String
        Dim l_w_d As String
        Dim l_inv_tp_cd As String
        Dim l_itm_tp_cd As String

        Session("from_detail") = "N"
        l_store_cd = slsp_strs.SelectedItem.Value

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            sqlString = New StringBuilder("SELECT  ")
            sqlString.Append(" id,s_f,e.lname||' '||substr(e.fname,1,1)||'.' EMP_NM,GROSS_SALE , VOID_SALE, NET_SALE, NET_MGN ")
            sqlString.Append(" ,furn_mix, furn_net_mgn ")
            sqlString.Append(" ,matt_mix, matt_net_mgn ")
            sqlString.Append(" ,appl_mix, appl_net_mgn ")
            sqlString.Append(" ,elec_mix, elec_net_mgn ")
            sqlString.Append(" ,out_net_sale, out_net_mgn, out_net_qty ")
            sqlString.Append("FROM srrtd_slsp_output s, emp e ")
            sqlString.Append("WHERE id =:l_id and store_cd='" & l_store_cd & "'")
            sqlString.Append(" and s.emp_cd = e.emp_cd ")
            sqlString.Append(" order by 4 desc ")

            ds = New DataSet
            GV_SLSP.DataSource = ""

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()
            objcmd.Parameters.Add(":l_id", OracleType.VarChar)
            objcmd.Parameters(":l_id").Value = l_id

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            objAdaptor.Fill(ds)

            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count

            If (MyDataReader.Read()) Then
                GV_SLSP.Enabled = True
                GV_SLSP.Visible = True
                GV_SLSP.DataSource = ds
                GV_SLSP.DataBind()

                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SLSP.Enabled = False
                GV_SLSP.Visible = False
            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try
    End Sub
End Class