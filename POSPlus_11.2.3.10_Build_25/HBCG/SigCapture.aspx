<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SigCapture.aspx.vb" Inherits="SigCapture" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Signature Capture</title>
    <link href="style.css" type="text/css" rel="stylesheet">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="VB .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="Javascript">
<!--
function OnClear() {
document.FORM1.SigPlus1.ClearTablet(); //Clears the signature, in case of error or mistake
}


function OnSign() {
document.FORM1.SigPlus1.TabletState = 1; //Turns tablet on
}


function OnSave() {

if(document.FORM1.SigPlus1.NumberOfTabletPoints > 0)
{
   document.FORM1.SigPlus1.TabletState = 0; //Turns tablet off
   document.FORM1.SigPlus1.AutoKeyStart();
   
   document.FORM1.SigPlus1.AutoKeyData = document.FORM1.SigText.value;
   //pass here the data you want to use to encrypt the signature
   //this demo simply encrypts to the name typed in by the user
   //you'll probably want to make sure your encryption data you use is
   //more useful...that you encrypt the signature to the data important
   //to your app, and what the client has agreed to
   
   document.FORM1.SigPlus1.AutoKeyFinish();
   document.FORM1.SigPlus1.EncryptionMode = 2;
   document.FORM1.SigPlus1.SigCompressionMode = 1; 
   
   document.FORM1.hidden.value = document.FORM1.SigPlus1.SigString;
   //pass the signature ASCII hex string to the hidden field,
   //so it will be automatically passed when the page is submitted
   
   document.FORM1.submit();
}
else
{
   alert("Please Sign Before Continuing...");
   return false;
}


}

//-->
    </script>

</head>
<body onload="OnSign()">
    <form id="FORM1" method="post" name="FORM1" runat="server">
        <dx:ASPxLabel ID="lbl_status" runat="server">
        </dx:ASPxLabel>
        <br />
        <table border="1" cellpadding="0">
            <tr>
                <td style="width: 449px">
                    <object id="SigPlus1" style="left: 0px; width: 452px; top: 0px; height: 180px" height="75"
                        classid="clsid:69A40DA3-4D42-11D0-86B0-0000C025864A" name="SigPlus1" CODEBASE="http://www.topazsystems.com/Software/sigplus.cab">
                        <param name="_Version" value="131095">
                        <param name="_ExtentX" value="8467">
                        <param name="_ExtentY" value="4763">
                        <param name="_StockProps" value="9">
                    </object>
                </td>
            </tr>
        </table>
        <%--<input id="SigText" type="text" name="SigText" class="style5" style="width: 452px"><br>
        <input id="SignBtn" onclick="OnSign()" type="button" value="Enable Signature Pad"
            name="SignBtn" class="style5">&nbsp;
        <input id="button1" onclick="OnClear()" type="button" value="Clear" name="ClearBtn"
            class="style5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp; &nbsp; &nbsp;<asp:Button ID="Button2" OnClientClick="OnSave()" runat="server"
            CssClass="style5" Text="Signature Complete" />
        &nbsp;&nbsp;&nbsp;&nbsp;--%>
        <input id="SigText" type="text" name="SigText" class="style5" style="width: 452px">
        <br />
        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="SignBtn" runat="server" Text="Enable Signature Pad">
                        <ClientSideEvents Click="function(s, e) {
	OnSign()
}" />
                    </dx:ASPxButton>
                </td>
                <td>
                    <dx:ASPxButton ID="button1" runat="server" Text="Clear">
                        <ClientSideEvents Click="function(s, e) {
	OnClear()
}" />
                    </dx:ASPxButton>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                    &nbsp;
                </td>
                <td>
                    <dx:ASPxButton ID="Button2" runat="server" Text="Signature Complete">
                        <ClientSideEvents Click="function(s, e) {
	OnSave()
}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input id="hidden" type="hidden" name="hidden">
    </form>
</body>
</html>
