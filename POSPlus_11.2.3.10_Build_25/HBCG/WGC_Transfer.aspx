﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WGC_Transfer.aspx.vb" Inherits="WGC_Transfer"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors"TagPrefix="dx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1"  runat="Server">
  
    <div width="95%" align="center">
        
        <br />
        &nbsp;<br />
        <b>
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LibResources, Label691 %>">
            </asp:Label>
        <br />
        <br />
        </b> 

        <table class="style5">
            <tr>
                <td align="left" style="width: 92px">
                 <asp:Label ID="Label2" runat="server" Text="Old Gift Card#"></asp:Label>
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="old_gc" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="true" ></asp:TextBox>
                </td>
                <td align="left">
                 <asp:button ID="swipe_btn1" runat="server" Text="Swipe"> </asp:button>
                </td>
             </tr>
             <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label220 %>"></asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="gc_bal" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="true" ReadOnly="True"></asp:TextBox>
                </td>
                <td align="left">
                 <asp:button ID="bal_btn" runat="server" Text="<%$ Resources:LibResources, Label76 %>">
                     
 </asp:button>
                </td>
             </tr>
             <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel7" runat="server" Text="">
                    </asp:label>
                    &nbsp;
                </td>
              </tr>
              <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                    </asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="cust_cd" runat="server" AutoPostBack="true" Width="173px" CssClass="style5" Enabled="true"></asp:TextBox>
                     <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Customer.aspx"  Text="<%$ Resources:LibResources, Label88 %>"> </asp:HyperLink>
                </td>
                  </tr>
                <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel4" runat="server" Text="">
                    </asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="cust_name" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="true" ReadOnly="True"></asp:TextBox>
                </td>

            </tr>
            
            <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel6" runat="server" Text="">
                    </asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="cust_addr" runat="server" AutoPostBack="true" Width="332px" CssClass="style5" Enabled="true" ReadOnly="True"></asp:TextBox>
                </td>

            </tr>
                                      <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel11" runat="server" Text="">
                    </asp:label>
                    &nbsp;
                </td>
                                          </tr>
                   <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="ASPxLabel9" runat="server" Text="New Gift Card# ">
                    </asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 336px">
                    <asp:TextBox ID="new_gc" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="true" ReadOnly="True"></asp:TextBox>
                &nbsp;</td>
                <td align="left">
                 <asp:button ID="swipe_btn2" runat="server" Text="Swipe"></asp:button>
                </td>
                       </tr>
           
                           <tr>
                <td align="left" style="width: 118px">
                    <asp:label ID="ASPxLabel13" runat="server" Text="">
                    </asp:label>
                    &nbsp;
                </td>

            </tr>
             <tr>
                <td align="left" style="width: 92px">
                    <asp:label ID="Label3" runat="server" Text="<%$ Resources:LibResources, Label95 %>">
                    </asp:label>
                    &nbsp;
                </td>
                <td align="left" style="width: 400px">
                    <asp:TextBox ID="xfer_comment" runat="server" AutoPostBack="true" Width="330px" CssClass="style5" Enabled="true" ReadOnly="false"></asp:TextBox>
                &nbsp;</td>
                
                       </tr>
       </table>
              
       <table>
            <tr>
              <td align="left">
                 <asp:button ID="commit_btn" runat="server" Text="Commit Transfer" Width="280px" Font-Bold="True"> </asp:button>
                </td>
                <td align="left">
                    <br />
                    <br />
                    <br />
                    <br />
                    &nbsp;


                </td>
                </tr>
       </table>
       <table>
            <tr>
       <td align="left" style="width: 588px">
                    <asp:textbox ID="xfer_msg" runat="server" Text="" Font-Bold="True" Font-Size="Larger" ForeColor="Red" ReadOnly="True" Width="653px"></asp:textbox>
                   
                </td> </tr>
                     
       </table>
     </div>
                
        <br />
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

