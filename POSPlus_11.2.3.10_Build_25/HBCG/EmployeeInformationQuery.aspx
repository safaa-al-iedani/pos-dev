<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="EmployeeInformationQuery.aspx.vb" Inherits="EmployeeInformationQuery" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" style="position: relative; width: 1200px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_employee_code" runat="server" meta:resourcekey="lbl_EMP_employee_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_employee_code" runat="server" Style="position: relative" Width="100px" MaxLength="10"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_employee_init" runat="server" meta:resourcekey="lbl_EMP_employee_init">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_employee_init" runat="server" Width="100px" MaxLength="8"></asp:TextBox>
            </td> 
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_first_name" runat="server" meta:resourcekey="lbl_EMP_first_name">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_first_name" runat="server" Width="250px" MaxLength="15"></asp:TextBox>
            </td> 

            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_last_name" runat="server" meta:resourcekey="lbl_EMP_last_name">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_last_name" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
            </td> 
        </tr>

        <tr>
            <td class="class_tdlbl" style="width: 200px" align="right">
                <dx:ASPxLabel ID="lbl_QUERY_store_code" runat="server" meta:resourcekey="lbl_QUERY_store_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdddlb" style="width: 450px">
                     <asp:DropDownList ID="txt_QUERY_store_code" runat="server" Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>

            <td class="class_tdempty" style="width: 200px" align="right">
                &nbsp;
            </td>

            <td class="class_tdbtn" style="width: 350px" align="right">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%-- <dx:ASPxButton ID="btn_Lookup" runat="server" Text="Search">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear Screen">
                </dx:ASPxButton> --%>
                <dx:ASPxButton ID="btn_Lookup" runat="server" meta:resourcekey="lbl_btn_search">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clearscreen">
                </dx:ASPxButton>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
            </td>
        </tr>

        <tr><td colspan="4">
        <hr />
        </td></tr>

        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_home_store_code" runat="server" meta:resourcekey="lbl_EMP_home_store_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_home_store_code" runat="server" Width="430px" MaxLength="100"></asp:TextBox>
            </td>         
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_termdate" runat="server" meta:resourcekey="lbl_EMP_termdate">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_termdate" runat="server" Width="200px" MaxLength="20"></asp:TextBox>
            </td> 
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_default_printer_queue" runat="server" meta:resourcekey="lbl_EMP_default_printer_queue">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_default_printer_queue" runat="server" Width="200px" MaxLength="31"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_EMP_default_printer_group" runat="server" meta:resourcekey="lbl_EMP_default_printer_group">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_EMP_default_printer_group" runat="server" Width="200px" MaxLength="20"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="class_tdempty" colspan="4">&nbsp;
            </td>
        </tr>


<%-- ************************************************************************************************************************** --%>
        <tr><td colspan="4">&nbsp;
        <%--<hr />--%>
        </td></tr>
<%-- ************************************************************************************************************************** --%>
        <tr>
            <td colspan="4" align="center">
                <table border="0" width="100%">
                <tr>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%-- <td width="25%" align="center">
                    <dx:ASPxButton ID="ASPxButton1" OnClick="PageButtonClick" runat="server" Text="<< First"
                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="ASPxButton2" OnClick="PageButtonClick" runat="server" Text="< Prev"
                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="ASPxButton3" OnClick="PageButtonClick" runat="server" Text="Next >"
                    ToolTip="Go to the next page" CommandArgument="Next" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="ASPxButton4" OnClick="PageButtonClick" runat="server" Text="Last >>"
                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                    </dx:ASPxButton>
                </td>--%>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" meta:resourcekey="lbl_btn_first"
                     CommandArgument="First" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" meta:resourcekey="lbl_btn_prev"
                     CommandArgument="Prev" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" meta:resourcekey="lbl_btn_next"
                     CommandArgument="Next" Visible="false">
                    </dx:ASPxButton>
                </td>
                <td width="25%" align="center">
                    <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" meta:resourcekey="lbl_btn_last"
                     CommandArgument="Last" Visible="false">
                    </dx:ASPxButton>
                </td>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="4" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>

    </table>

    
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
