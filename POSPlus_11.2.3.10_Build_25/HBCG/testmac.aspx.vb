Imports system
Imports System.Management

Partial Class testmac
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Populate_NICS()

    End Sub

    Private Sub Populate_NICS()

        cbo_NICS.ClearSelection()

        Dim objMOS As ManagementObjectSearcher
        Dim objMOC As Management.ManagementObjectCollection
        Dim objMO As Management.ManagementObject

        objMOS = New ManagementObjectSearcher("Select * From Win32_NetworkAdapter where netconnectionstatus=2")

        objMOC = objMOS.Get

        For Each objMO In objMOC
            cbo_NICS.Items.Add(objMO("NetConnectionID"))
            'Response.Write(objMO("MACAddress") & " - " & objMO("NetConnectionStatus") & " - " & objMO("NetConnectionID") & "<br />")
        Next

        objMOS.Dispose()
        objMOS = Nothing
        objMO.Dispose()
        objMO = Nothing

    End Sub

    Protected Sub cbo_NICS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_NICS.SelectedIndexChanged

        Response.Write(cbo_NICS.SelectedValue)

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class
