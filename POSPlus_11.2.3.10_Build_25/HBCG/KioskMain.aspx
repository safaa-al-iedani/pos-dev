<%@ Page Language="VB" AutoEventWireup="false" CodeFile="KioskMain.aspx.vb" Inherits="KioskMain" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body class="style5">
    <form id="form1" runat="server">
        <div align="center">
            <br />
            <asp:DataGrid ID="Gridview1" Style="position: relative; top: 0px; left: 0px;" runat="server"
                AutoGenerateColumns="False" CellPadding="2" DataKeyField="STORE_CD" Font-Bold="False"
                Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                Height="156px" ShowHeader="False">
                <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                    Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                    Font-Bold="True" ForeColor="Black"></HeaderStyle>
                <Columns>
                    <asp:BoundColumn DataField="store_cd" HeaderText="SKU" ReadOnly="True" Visible="False">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ORIG_COUNT" HeaderText="SKU" ReadOnly="True" Visible="False">
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Kiosk Store Evaluations">
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                   <%-- <td align="left" valign="top" width="150">
                                        <asp:Image ID="img_picture" runat="server" ImageUrl="~/images/kiosk.jpg" />
                                        <br />
                                    </td>--%>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lbl_store_info" runat="server" CssClass="style5"></asp:Label>
                                        <br />
                                        <br />
                                        <asp:HyperLink ID="hpl_store_details" runat="server" CssClass="style5">View Store Details</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Top" />
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:Label ID="lbl_kiosk_info" runat="server" CssClass="style5"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </form>
</body>
</html>
