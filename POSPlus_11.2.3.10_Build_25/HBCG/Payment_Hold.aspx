<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Payment_Hold.aspx.vb" Inherits="Payment_Hold" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Credit Card Processing</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
body {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
}
td {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
	color: #000000;
}
input {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
}
select {
	font-family: Tahoma, Arial, Helvetica;
	font-size: 11px;
	line-height: 17px;
	color: #000000;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="Label4" runat="server" Text="A problem was encountered while processing your payment(s).  Please review your payment information and re-submit below."
            Width="524px" Font-Size="10pt" Font-Strikeout="False" Font-Bold="True"></asp:Label><br />
        <asp:DropDownList ID="cbo_payment_type" runat="server" CssClass="style5" Width="221px"
            AutoPostBack="true">
        </asp:DropDownList>
        &nbsp; &nbsp;&nbsp;
        <asp:Label ID="lbl_amt" runat="server" Visible="False" Width="67px"></asp:Label>&nbsp;
        &nbsp; &nbsp;<asp:Label ID="lbl_row_id" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="Label3" runat="server" CssClass="style5" Width="189px"></asp:Label><br />
        <br />
        <table>
            <tr>
                <td>
                    <dx:aspxlabel id="ASPxLabel4" runat="server" text="Store Code:"></dx:aspxlabel>
                </td>
                <td>
                    <dx:aspxcombobox id="cbo_store_cd" runat="server" autopostback="true" width="224px"
                        IncrementalFilteringMode ="StartsWith">
                    </dx:aspxcombobox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:aspxlabel id="ASPxLabel5" runat="server" text="Cash Drawer Code:"></dx:aspxlabel>
                </td>
                <td>
                    <dx:aspxcombobox id="cbo_csh_drw" runat="server" autopostback="True" selectedindex="0"
                        width="224px" IncrementalFilteringMode ="StartsWith">
                    </dx:aspxcombobox>
                </td>
            </tr>
        </table>
        <br />
        <div id="cc" runat="server" visible="False">
            <table>
                <tr>
                    <td valign="middle" align="center">
                        <b>
                            <asp:Label ID="lbl_amount" runat="server" Text=""></asp:Label>
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Text="Credit Card: "></asp:Label>
                            <asp:Label ID="lbl_card_type" runat="server" Width="124px"></asp:Label>
                            <br />
                            <asp:TextBox ID="TextBox1" runat="server" Width="194px" AutoPostBack="true"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Label ID="Label2" runat="server" Text="Exp Date: "></asp:Label>
                            <asp:TextBox ID="TextBox2" runat="server" Width="17px" MaxLength="2"></asp:TextBox>
                            /
                            <asp:TextBox ID="TextBox3" runat="server" Width="17px" MaxLength="2"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SID:
                            <asp:TextBox ID="txt_security_code" runat="server" Width="28px" MaxLength="4"></asp:TextBox>
                            <br />
                            <asp:Label ID="lbl_Full_Name" runat="server" Width="202px"></asp:Label>
                            <br />
                            <asp:Button ID="btn_save" runat="server" Text="Submit" Width="100px" />
                            <asp:Button ID="btn_Clear" runat="server" Text="Clear" Width="100px" />
                            <br />
                            <asp:Label ID="lbl_tran_id" runat="server" Visible="False"></asp:Label><br />
                            Manual Authorization:
                            <asp:TextBox ID="txt_auth" runat="server" MaxLength="6" Width="49px"></asp:TextBox><br />
                        </b><sup>Please swipe or enter the credit card information and click "Save" when finished</sup>
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td valign="middle" align="center">
                        <img src="images/icons/Symbol-Error.png" style="width: 174px; height: 174px" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="ck" runat="server" visible="False">
            <table>
                <tr>
                    <td valign="middle" align="center" style="width: 330px">
                        <br />
                        <table class="style5">
                            <tr>
                                <td style="text-align: left">
                                    <strong>Check #: </strong>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txt_check" runat="server" CssClass="style5" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>Bank Routing #:</strong>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="TextBox4" runat="server" Width="194px" CssClass="style5" MaxLength="9"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:</strong></td>
                                <td align="left">
                                    <asp:TextBox ID="TextBox5" runat="server" Width="194px" CssClass="style5" MaxLength="25"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>Type: </strong>
                                </td>
                                <td align="left">
                                    &nbsp;<asp:DropDownList ID="DropDownList2" runat="server" CssClass="style5">
                                        <asp:ListItem Selected="True" Value="01">Personal</asp:ListItem>
                                        <asp:ListItem Value="90">Business</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <strong>Approval #: </strong>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txt_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>DL State:</strong>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txt_dl_state" runat="server" CssClass="style5" MaxLength="2" Width="20px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>DL #:</strong>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txt_dl_number" runat="server" CssClass="style5" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        &nbsp;&nbsp;
                        <br />
                        <asp:Button ID="btn_save_chk" runat="server" CssClass="style5" Text="Submit" Width="104px" />
                        <br />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td valign="middle" align="center">
                        <img src="images/icons/Symbol-Error.png" style="width: 174px; height: 174px" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="fin" runat="server" visible="False">
            <table>
                <tr>
                    <td valign="middle" align="center" style="width: 349px">
                        <br />
                        <table class="style5">
                            <tr>
                                <td style="text-align: left">
                                    <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />: </strong>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5">
                                        <asp:ListItem Value="O">Open</asp:ListItem>
                                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                                        <asp:ListItem Value="I">Installment</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />: </strong>
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_finance_company" runat="server" AutoPostBack="True" CssClass="style5"
                                        Width="240px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <strong>Promotion Plan:</strong>
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_promo" runat="server" AutoPostBack="True" CssClass="style5"
                                        Width="239px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />: </strong>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txt_fi_acct" runat="server" CssClass="style5" MaxLength="20" OnTextChanged="txt_fi_acct_TextChanged"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <strong>Approval #: </strong>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txt_fi_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Submit" Width="104px" />&nbsp;<br />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td valign="middle" align="center">
                        <img src="images/icons/Symbol-Error.png" style="width: 174px; height: 174px" />
                    </td>
                </tr>
            </table>
        </div>
        <table style="width: 524px">
            <tr>
                <td style="width: 147px" valign="top">
                    <strong><span style="text-decoration: underline">Host Response Code: </span></strong>
                </td>
                <td>
                    <asp:Label ID="lbl_host_resp_cd" runat="server" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 147px" valign="top">
                    <strong><span style="text-decoration: underline">Host Response Message:</span></strong>
                </td>
                <td>
                    <asp:Label ID="lbl_host_resp_msg" runat="server" Text="Label" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 147px">
                    <span style="text-decoration: underline"><strong>Protobase Response: </strong></span>
                </td>
                <td>
                    <asp:Label ID="lbl_pb_resp" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Label ID="lbl_del_doc_num" runat="server" Visible="False" Width="67px"></asp:Label>
        <asp:Label ID="lbl_mop_cd" runat="server" Visible="False" Width="67px"></asp:Label>
    </form>
</body>
</html>
