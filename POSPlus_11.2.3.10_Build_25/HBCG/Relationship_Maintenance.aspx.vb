Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.SessionState
Imports HBCG_Utils
Imports InventoryUtils
Imports System.Web.UI.WebControls
Imports TaxUtils
Imports System.Linq

Partial Class Relationship_Maintenance
    Inherits POSBasePage

    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theMerchBiz As MerchBiz = New MerchBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theCustomerBiz As CustomerBiz = New CustomerBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Dim x As Boolean
    Dim FAB_FOUND As String
    Dim GM_Sec As String = ""
    Dim running_balance As Double = 0
    Dim quote_balance As Double = 0
    Dim preventMessageFlag As Boolean = False

    Private Class RelLnGrid
        Public Const LN_NUM As Integer = 0  ' UNSURE OF DIFF BETWEEN THIS LINE NUMBER AND LINE
        Public Const ITM_CD As Integer = 1
        'Public Const VE_CD As Integer = 2   ' NOT USED
        'Public Const VSN_COL As Integer = 3 ' includes image, vendor, serial number, po, ist, warr, leave-in_carton
        Public Const SKU_DES As Integer = 4
        Public Const UNIT_PRC As Integer = 5
        Public Const QTY As Integer = 6
        Public Const FAB_CHKBOX As Integer = 7
        Public Const WARR_CHKBOX As Integer = 8
        Public Const EXT_PRC As Integer = 9   ' and add button 
        Public Const LINE As Integer = 10       ' line column in relationship_lines_desc table
        'Public Const DEL_CHKBOX As Integer = 10 ????' and add button 
        Public Const TREATED As Integer = 13
        Public Const TREATABLE As Integer = 14
        Public Const ITM_TP_CD As Integer = 15
        Public Const WARR_ITM_LINK As Integer = 16
        Public Const WARR_ROW_LINK As Integer = 17
        Public Const WARRANTABLE As Integer = 18
        Public Const WARR_DAYS As Integer = 19
        Public Const INVENTORY As Integer = 20
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If txt_del_doc_num.Text & "" = "" Then
            txt_del_doc_num.Enabled = "True"
            btn_Lookup.Visible = True
            btn_Save.Enabled = False
            tblTaxCd.Visible = False
        Else
            btn_Save.Enabled = True
            tblTaxCd.Visible = True
        End If
        lbl_warning.Text = ""


        If Request.Form("__EVENTTARGET") = "SummaryPriceChangeApproval" Then
            PromptForSummaryPriceMgrOverride()
        End If

        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTax")) OrElse ViewState("CanModifyTax").ToString = String.Empty Then
            ViewState("CanModifyTax") = SecurityUtils.hasSecurity(SecurityUtils.OVERRIDE_TAXCD_MGMT, Session("EMP_CD"))
        End If

        If Not IsPostBack Then
            Dim dsStates As DataSet = Nothing
            dsStates = HBCG_Utils.GetStates()
            With cbo_State
                .DataSource = dsStates
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
            cbo_State.Items.Insert(0, " ")
            cbo_State.Items.FindByText(" ").Value = ""
            lbltotal.Text = FormatCurrency(0, 2)

            If Request("query_returned") = "Y" OrElse Not (IsNothing(Request("warrItmCd"))) Then

                Initialize()

                If Request("cust_returned") = "Y" Then
                    'means that the redirect happened because a relationship was copied and user searched and selected a different customer
                    'for the newly created rel'ship. So, display the copy rel'ship popup again with this new customer.
                    ASPxPopupControl2.ContentUrl = "Relationship_Copy.aspx?del_doc_num=" & txt_del_doc_num.Text & "&cust_cd=" & Request("cust_cd")
                    ASPxPopupControl2.ShowOnPageLoad = True
                End If
            End If

            Dim txPcts As TaxUtils.taxPcts = SetTxPcts(Session("tran_dt"), Session("tax_cd")) ' local pass thru

            '***Check for Warranties being added or not after returning from warranties popup *******
            If Not (IsNothing(Request("warrItmCd")) OrElse "NONE".Equals(Request("warrItmCd") & "") OrElse IsNothing(Request("itmRowNum"))) Then
                ' if warrItmRowNum is empty, then user has selected to create a new warranty line, otherwise they've
                '     selected to link this warrantable to an existing warrantable line (warrItmRowNum)
                If IsNothing(Request("warrItmRowNum")) OrElse Request("warrItmRowNum") & "" = "" Then
                    ' Giving non-numeric price will make it pull from the E1 price calendars logic as it should
                    Dim newRelLn As DataRow = AddLine(Request("warrItmCd"), 1, "WAR", Request("del_doc_num"),
                                                      Request("itmCd"), Request("itmRowNum"))
                Else
                    '*** Means that an existing warranty was selected, so just update the link info on the warrantable
                    '*** sku. No need to add a separate warranty line.
                    UpdtLinks2Warrable(Request("warrItmCd"), Request("warrItmRowNum"), Request("itmRowNum"))
                End If

                Dim relLns As DataSet = Session("REL_LN")
                ReapplyDiscounts(relLns.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void)
                'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
                'if will reapply it, if ANY of the lines added qualify for the discount.
                ' NOTE THAT BINDGRID ABOVE AVOIDED BECAUSE DOING HERE - IF THIS CONDITION CHANGES, CHANGE ABOVE TO MATCH
                'If SessVar.LastLnSeqNum.isNotEmpty() AndAlso SessVar.discReCalc.needToReCalc = True Then

                '   ReapplyDiscounts(relLns.Tables(0), ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                '   Session("REL_LN") = relLns
                'End If

                ASPxPageControl1.ActiveTabIndex = 1
                bindgrid()
                calculate_total()

                ' if went to warranty page but selected nothing, after adding lines, then need to remain on page
            ElseIf "NONE".Equals(Request("warrItmCd") & "") Then
                ASPxPageControl1.ActiveTabIndex = 1
            End If
            txt_del_doc_num.Focus()
        End If

        If Not (txt_del_doc_num.Text.Trim) = String.Empty Then
            Dim droppedSkusMsg As String = CheckForDroppedSkus(txt_del_doc_num.Text.Trim)
            If droppedSkusMsg.isNotEmpty() Then
                lbl_warning.Text = droppedSkusMsg
            End If
        End If
        'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
        'if will reapply it, if ANY of the lines added qualify for the discount.
        ' NOTE THAT BINDGRID ABOVE AVOIDED BECAUSE DOING HERE - IF THIS CONDITION CHANGES, CHANGE ABOVE TO MATCH
        Dim relLn As DataSet = Session("REL_LN")
        If SessVar.LastLnSeqNum.isNotEmpty() AndAlso SessVar.discReCalc.needToReCalc = True Then

            ReapplyDiscounts(relLn.Tables(0), ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
            Session("REL_LN") = relLn
            ASPxPageControl1.ActiveTabIndex = 1
            bindgrid()
            calculate_total()
        End If

    End Sub

    Private Sub Initialize()
        PopulateTaxExempt()
        Populate_Results()
    End Sub

    Private Sub PopulateTaxExempt()
        Dim ds As DataSet = theSalesBiz.GetTaxExemptCodes()
        With cbo_tax_exempt
            .DataSource = ds
            .ValueField = "tet_cd"
            .TextField = "full_desc"
            .DataBind()
            If Session("tet_cd") & "" <> "" Then
                .Value = Session("tet_cd")
            End If
        End With

        cbo_tax_exempt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label414, ""))
        cbo_tax_exempt.Items.FindByText(Resources.LibResources.Label414).Value = ""
        If Session("tet_cd") & "" = "" And cbo_tax_exempt.Value & "" = "" Then
            cbo_tax_exempt.SelectedIndex = 0
        End If

        If Session("tet_cd") & "" = "" And cbo_tax_exempt.SelectedIndex <> 0 Then
            Session("tet_cd") = cbo_tax_exempt.Value
        End If
    End Sub

    Private Sub bindgrid()
        Dim DEL_DOC_NUM As String
        Dim noRelInProcess As Boolean = True  ' no relation queried and in changes or display in progress
        Dim relLns As DataSet = New DataSet

        If Not Session("REL_LN") Is Nothing Then ' And IsPostBack = True Then

            noRelInProcess = False
            relLns = Session("REL_LN")
            Dim dt As DataTable = relLns.Tables(0)
            Dim dr As DataRow

            ' Find a non-deleted line to confirm that working with the same relationship to re-display from session or to re-query
            For Each dr In relLns.Tables(0).Rows
                If dr.RowState <> DataRowState.Deleted Then
                    If dr(0).ToString <> Request("del_doc_num") Then

                        noRelInProcess = True
                        relLns.Clear()
                        Session("REL_LN") = relLns
                    Else
                        Gridview1.DataSource = relLns
                        Gridview1.DataBind()
                    End If
                    Exit For
                End If
            Next
        End If

        Gridview1.Visible = True

        If noRelInProcess = True Then
            DEL_DOC_NUM = Request("del_doc_num")
            Gridview1.DataSource = ""
            Gridview1.Visible = True
            Try
                relLns = GetRelLns(DEL_DOC_NUM)
                Session("REL_LN") = relLns
                Gridview1.DataSource = relLns
                Gridview1.DataBind()
            Catch
                Throw
            End Try
        End If

        If Gridview1.DataSource Is Nothing Then
            Gridview1.DataSource = relLns
            Gridview1.DataBind()
        End If

        DoAfterBindgrid()
    End Sub

    ''' <summary>
    ''' Extracts line and SKU information from relationship_lines and item for display or processing
    ''' </summary>
    ''' <param name="docNum">relationship number</param>
    ''' <returns>DataSet containing the set of lines with related SKU information currently in the database for this lead</returns>
    ''' <remarks></remarks>
    Private Function GetRelLns(ByVal docNum As String) As DataSet
        Dim salesType As ArrayList = New ArrayList()
        salesType.Add("SAL")
        salesType.Add("CRM")
        Dim saleBiz As SalesBiz = New SalesBiz()
        Dim datSet As DataSet = saleBiz.GetRelationLineItems(docNum, True)

        Return datSet
    End Function

    Protected Sub Add_SKU_To_Order(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim footerIndex As Integer = Gridview1.Controls(0).Controls.Count - 1
        Dim txt_new_itm_cd As TextBox = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
        Dim txt_new_unit_prc As TextBox = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("txt_new_unit_prc"), TextBox)
        Dim txt_new_qty As TextBox = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("txt_new_qty"), TextBox)

        Dim enteredSKUs() As String = UCase(txt_new_itm_cd.Text).Trim(" ", ",").Split(",").[Select](Function(g) g.Trim()).ToArray()
        Dim sellableItemCds As List(Of String) = theSkuBiz.GetSellableItems(enteredSKUs.Distinct().ToList())
        Session("ITMCDLIST") = String.Join(",", sellableItemCds)

        lbl_desc.Text = ""
        ' Daniela add multi comp logic
        If txt_new_itm_cd.Text & "" <> "" Then
            txt_new_itm_cd.Text = txt_new_itm_cd.Text.Trim
            Dim usr_flag = LeonsBiz.sec_check_itm(UCase(txt_new_itm_cd.Text), Session("emp_cd"))
            If usr_flag = "N" Then  'multi co
                txt_new_itm_cd.Text = ""
                lbl_warning.Text = "You are not authorized to select that SKU"
                Exit Sub
            End If
        End If

        Dim newRelLn As DataRow = Nothing
        Dim PKG_SKUS As String = String.Empty
        Dim preventCount As Integer = 0
        Dim preventBuilder As New StringBuilder
        Dim totalSKU As Integer = 0
        Dim showWarrPopup As Boolean = False

        If enteredSKUs.Distinct().ToList().Count = sellableItemCds.Count Then
            SetupForDiscReCalc()
            For Each newItemCd As String In sellableItemCds
                newRelLn = AddLine(newItemCd, txt_new_qty.Text, txt_new_unit_prc.Text, txt_del_doc_num.Text, "", "")

                If SystemUtils.dataRowIsNotEmpty(newRelLn) Then
                    If newRelLn("ITM_TP_CD").ToString = "PKG" Then
                        PKG_SKUS = PKG_SKUS & "'" & newRelLn("ITM_CD").ToString & "',"
                    End If
                ElseIf preventMessageFlag = True Then
                    If preventCount > 0 Then preventBuilder = preventBuilder.Append(",")
                    preventBuilder = preventBuilder.Append(newItemCd)
                    preventCount = preventCount + 1
                    totalSKU = totalSKU + 1
                Else
                    lbl_warning.Text = String.Format(Resources.POSMessages.MSG0024)
                    Exit Sub
                End If

                If Not IsNothing(newRelLn) AndAlso Not showWarrPopup Then
                    showWarrPopup = "Y".Equals(newRelLn("INVENTORY").ToString) _
                                        AndAlso "Y".Equals(newRelLn("WARRANTABLE").ToString) AndAlso SalesUtils.hasWarranty(newRelLn("ITM_CD"))
                End If
            Next
        Else
            lbl_warning.Text = String.Format(Resources.POSMessages.MSG0024)
            Exit Sub
        End If

        If Not preventBuilder Is Nothing And Not preventBuilder.ToString = String.Empty Then
            If totalSKU > 1 Then
                lbl_warning.Text = String.Format(Resources.POSMessages.MSG0016, UCase(preventBuilder.ToString))
            Else
                lbl_warning.Text = String.Format(Resources.POSMessages.MSG0018, UCase(preventBuilder.ToString))
            End If
        End If

        Dim focusLine As String = String.Empty
        If Not IsNothing(newRelLn) Then focusLine = newRelLn("line")

        If PKG_SKUS & "" <> "" Then
            Dim errorMessage As String = String.Empty

            focusLine = newRelLn("line") + 1
            If Not IsNothing(newRelLn) Then AddPackages(PKG_SKUS, newRelLn("line") + 1, showWarrPopup, errorMessage)

            If Not (String.IsNullOrWhiteSpace(errorMessage)) Then
                ' MM-6305
                ' Need to remove the last entered Package SKU from the dataset
                lbl_warning.Text = errorMessage
                Dim relLns As DataSet = New DataSet
                relLns = Session("REL_LN")
                Dim count As Integer = relLns.Tables(0).Rows.Count  'ds.Tables(0).Rows.Count
                relLns.Tables(0).Rows(count - 1).Delete()
                relLns.AcceptChanges()
                Session("REL_LN") = relLns
            Else
                lbl_warning.Text = String.Empty
            End If
            PKG_SKUS = ""
        End If

        If SystemUtils.dataRowIsNotEmpty(newRelLn) Then
            If showWarrPopup Then
                ShowWarrantyPopup(focusLine)
            Else
                If Not IsNothing(Session("ITMCDLIST")) Then
                    Dim relatedSku As String = Session("ITMCDLIST")
                    Session("ITMCDLIST") = Nothing

                    ShowRelatedSkuPopUp(relatedSku)
                End If
            End If

            DoDiscReCalcNowOrLater()
            bindgrid()
            calculate_total()
        End If
        'reapplies the discounts (if any) when applicable
        ASPxPageControl1.ActiveTabIndex = 1
    End Sub

    ''' <summary>
    ''' Fetches the max line number from the session line dataset and sets in a session variable; 
    ''' initially used by discounting recalculation processing so only done if there are applied discounts and a header discount is included
    ''' </summary>
    Protected Sub SetupForDiscReCalc()
        'Prepares data for later use in discount recalculation (if applicable)
        SessVar.LastLnSeqNum = String.Empty
        If Not IsNothing(Session("DISC_APPLIED_RELN")) AndAlso
            (Not IsNothing(Session("REL_LN"))) Then

            Dim hdrDiscCd As String = theSalesBiz.GetHeaderDiscountCode(Session("DISC_APPLIED_RELN"), False)
            If hdrDiscCd.isNotEmpty() Then

                Dim reln As DataSet = Session("REL_LN")
                Dim maxLnNum As Integer = reln.Tables(0).Compute("MAX([LINE])", String.Empty)
                'fetches last lineSeq, to recalculate (if applicable) due to any of the new lines
                SessVar.LastLnSeqNum = maxLnNum
            End If
        End If
    End Sub

    ''' <summary>
    ''' Determines if a warranty may be added later and if so, postpones re-applying the header discount(s); otherwise
    ''' re-applies the discounts here 
    ''' </summary>
    Protected Sub DoDiscReCalcNowOrLater()
        ' if we might add more records from warr or related, then re-calc discount later
        If SessVar.discReCalc.maybeWarr = True Then
            SessVar.discReCalc.needToReCalc = True
        Else
            'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
            'if will reapply it, if ANY of the lines added qualify for the discount.
            If SessVar.LastLnSeqNum.isNotEmpty() Then

                Dim relnDatSet As DataSet = Session("REL_LN")
                Dim relnTable As DataTable = relnDatSet.Tables(0)

                ReapplyDiscounts(relnTable, ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                Session("REL_LN") = relnDatSet
            End If
        End If  ' TODO - maybe we shouldn't bindgrid yet either
    End Sub

    ''' <summary>
    ''' Adds a new line to the current grid and dataset. It retrieves and sets any related data
    ''' needed for creating and saving a line.
    ''' </summary>
    ''' <param name="newItmCd">t</param>
    ''' <param name="newQty"></param>
    ''' <param name="newUnitPrice"></param>
    ''' <param name="docNum"></param>
    ''' <param name="warr_itm_link"></param>
    ''' <param name="warr_row_link"></param>
    ''' <returns>the new datarow created</returns>
    Private Function AddLine(ByVal newItmCd As String, ByVal newQty As String, ByVal newUnitPrice As String,
                        ByVal docNum As String, ByVal warr_itm_link As String, ByVal warr_row_link As String) As DataRow
        Dim qtyIn As String = newQty
        Dim prcIn As String = newUnitPrice
        Dim itmCdIn As String = Trim(newItmCd)
        Dim docNumIn As String = Trim(docNum)
        Dim newRelLn As DataRow = Nothing

        If itmCdIn & "" = "" Then
            lbl_warning.Text = "Invalid SKU: " & itmCdIn
            Return newRelLn
            Exit Function
        End If

        'Do not allow SKUs that have a prevent sale flag on them
        If Not theSkuBiz.IsSellable(itmCdIn) Then
            'lbl_warning.Text = "The SKU " & itmCdIn & " you have selected is no longer available for sale"
            preventMessageFlag = True
            Return newRelLn
            Exit Function
        End If

        If Not IsNumeric(prcIn) Then prcIn = "0.00"

        If IsNumeric(qtyIn) Then
            If qtyIn < 1 Then
                qtyIn = "1"
            Else
                qtyIn = System.Math.Abs(CDbl(qtyIn))
            End If
        Else
            qtyIn = "1"
        End If

        ' Get the current max line number
        Dim curr_ln As Integer = 1
        Dim relLns As New DataSet

        If Not IsNothing(Session("REL_LN")) Then
            relLns = Session("REL_LN")

            Dim dt As DataTable = relLns.Tables(0)
            If Not IsNothing(dt) AndAlso dt.Rows.Count > 0 Then
                ' Get get max line number
                Dim maxLnNum As Object = dt.Compute("Max(LINE)", "")
                If Not IsDBNull(maxLnNum) Then curr_ln = If(maxLnNum IsNot Nothing, CInt(maxLnNum), 0) + 1
            End If

            Dim ret_prc As Double = 0
            Dim ds As DataSet = New DataSet()
            Dim itms As ArrayList = New ArrayList()
            itms.Add(itmCdIn)
            ds = theSkuBiz.GetItemDetailsWithSortCodes(itms)

            Try
                For Each dr As DataRow In ds.Tables(0).Rows
                    ret_prc = dr("RET_PRC").ToString()
                    ret_prc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                         txt_store.Text,
                                                         SessVar.custTpPrcCd,
                                                         FormatDateTime(Today.Date, DateFormat.ShortDate),
                                                         dr("ITM_CD"),
                                                         ret_prc)

                    If CDbl(prcIn) = CDbl(0) Then prcIn = ret_prc
                    prcIn = System.Math.Abs(CDbl(prcIn))

                    newRelLn = dt.NewRow
                    newRelLn("REL_NO") = docNumIn
                    newRelLn("LINE") = curr_ln
                    newRelLn("ITM_CD") = dr("ITM_CD").ToString
                    newRelLn("QTY") = qtyIn

                    If "PKG".Equals(dr("ITM_TP_CD") & "") And "Y".Equals(ConfigurationManager.AppSettings("package_breakout").ToString & "") Then
                        newRelLn("RET_PRC") = "0"
                        newRelLn("ORIG_PRC") = "0"
                    Else
                        newRelLn("RET_PRC") = prcIn
                        newRelLn("ORIG_PRC") = prcIn
                    End If

                    newRelLn("VE_CD") = dr("VE_CD").ToString
                    newRelLn("ITM_TP_CD") = dr("ITM_TP_CD").ToString

                    If warr_itm_link.isNotEmpty AndAlso SystemUtils.isNumber(warr_row_link) Then
                        newRelLn("warr_row_link") = warr_row_link
                        newRelLn("warr_itm_link") = warr_itm_link
                    End If

                    newRelLn("VSN") = dr("VSN").ToString
                    newRelLn("DES") = dr("DES").ToString
                    newRelLn("warrantable") = dr("warrantable").ToString
                    newRelLn("inventory") = dr("inventory").ToString
                    newRelLn("warr_days") = dr("warr_days").ToString
                    newRelLn("sortDescription") = dr("sortDescription").ToString
                    newRelLn("sortcodes") = dr("SORTCODE").ToString
                    newRelLn("TREATABLE") = dr("TREATABLE")
                    newRelLn("MNR_CD") = dr("MNR_CD")
                    newRelLn("REPL_CST") = dr("REPL_CST")
                    newRelLn("PU_DISC_PCNT") = dr("PU_DISC_PCNT")
                    newRelLn("MANUAL_PRC") = ret_prc
                    newRelLn("TYPECODE") = dr("TYPECODE")
                    newRelLn("MAX_DISC_PCNT") = dr("MAX_DISC_PCNT")
                    newRelLn("ITM_RET_PRC") = dr("ITM_RET_PRC")

                    dt.Rows.Add(newRelLn)

                    If warr_itm_link.isNotEmpty AndAlso SkuUtils.ITM_TP_WAR.Equals(dr("ITM_TP_CD").ToString) Then
                        UpdtLinks2Warrable(itmCdIn, curr_ln, Request("itmRowNum"))
                    End If

                    curr_ln = curr_ln + 1
                    prcIn = 0
                Next

                Session("REL_LN") = relLns
            Catch

            End Try
        End If

        Return newRelLn
    End Function

    Public Sub AddPackages(ByVal pkgSku As String, ByVal line_no As Double, ByRef showWarrPopup As Boolean, Optional ByRef errorMessage As String = "")
        If IsNothing(Session("slsp1")) Then
            Session("slsp1") = lbl_slsp1_cd.Text.ToString
        End If

        If IsNothing(Session("TAX_CD")) OrElse (Session("TAX_CD") & "").Trim() <> String.Empty Then
            Session("TAX_CD") = lbl_tax_cd.Text
            '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
            '** Since the system is auto assigning the tax, maunal code should be set to nothing.
            Session("MANUAL_TAX_CD") = Nothing
        End If

        ' create the object for header and transaction info     
        Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                               Session("slsp1"), Session("slsp2"),
                                                               Session("pct_1"),
                                                               IIf(IsNothing(Session("pct_2")), 0, Session("pct_2")),
                                                               Session("tet_cd"), Session("TAX_CD"),
                                                               Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                               Session("PD"), Session("pd_store_cd"),
                                                               Session("cash_carry"))

        ' TODO - TAX FIX - need to use the correct rates but we need tax date to calc
        '  Call SetTxPcts(tempItmReq.soHdrInf.soWrDt, tempItmReq.soHdrInf.taxCd).itmTpPcts
        If Session("TAX_RATE") IsNot Nothing Then
            Dim taxRate As Double = Session("TAX_RATE")
            headerInfo.taxRates.lin = taxRate
            headerInfo.taxRates.fab = taxRate
            headerInfo.taxRates.lab = taxRate
            headerInfo.taxRates.mil = taxRate
            headerInfo.taxRates.nlt = taxRate
            headerInfo.taxRates.nln = 0.0
            headerInfo.taxRates.par = taxRate
            headerInfo.taxRates.war = taxRate
            headerInfo.taxRates.svc = taxRate
            headerInfo.taxRates.pkg = taxRate
            headerInfo.taxRates.dnr = 0.0
            headerInfo.taxRates.del = taxRate
            headerInfo.taxRates.setup = taxRate
        End If

        ' create pkg components in temp_itm
        theSalesBiz.AddPkgCmpntsToTempItm(headerInfo,
                                            pkgSku,
                                            Session.SessionID.ToString.Trim, txt_store.Text.Trim, errorMessage)

        'now get all the new lines which were inserted in this session- which here are the pkg component lines
        'these lines have to be inserted as rows in the master, Dataset that is cached in the session.
        Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpnts(Session.SessionID.ToString())
        Dim lineDatSet As DataSet = Session("REL_LN")
        Dim lineRow As DataRow
        Dim itemsList As ArrayList = New ArrayList()
        Dim pkgParentLine As String = String.Empty

        If line_no > 0 Then
            pkgParentLine = (line_no - 1).ToString()
        End If

        Dim pkgComponents As New StringBuilder
        If Not IsNothing(Session("ITMCDLIST")) Then
            pkgComponents.Append(Session("ITMCDLIST").ToString().Replace(pkgSku.Trim(" ", ",").Trim("'"), String.Empty))
        End If

        For Each line In pkgLines.Tables(0).Rows
            itemsList.Add(line("ITM_CD").ToString)
            lineRow = lineDatSet.Tables(0).NewRow
            lineRow("REL_NO") = txt_del_doc_num.Text
            lineRow("LINE") = line_no
            lineRow("ITM_CD") = line("ITM_CD").ToString
            lineRow("QTY") = line("QTY").ToString
            lineRow("RET_PRC") = line("RET_PRC").ToString
            lineRow("ORIG_PRC") = line("RET_PRC").ToString
            lineRow("VE_CD") = line("VE_CD").ToString
            lineRow("ITM_TP_CD") = line("ITM_TP_CD").ToString
            lineRow("VSN") = Replace(line("VSN").ToString, "'", "''")
            lineRow("DES") = Replace(line("DES").ToString, "'", "''")
            lineRow("INVENTORY") = line("INVENTORY").ToString
            lineRow("SORTCODES") = theSkuBiz.GetItemSortCodesCSV(itemsList)
            lineRow("SORTDESCRIPTION") = ""
            lineRow("PACKAGE_PARENT") = IIf(String.IsNullOrEmpty(pkgParentLine), DBNull.Value, pkgParentLine)
            lineRow("TREATABLE") = line("TREATABLE")
            lineRow("WARRANTABLE") = line("WARRANTABLE")
            lineDatSet.Tables(0).Rows.Add(lineRow)
            line_no = line_no + 1
            itemsList.Clear()
            pkgComponents.Append("," & line("ITM_CD").ToString())
            If Not showWarrPopup Then showWarrPopup = "Y".Equals(line("INVENTORY")) AndAlso "Y".Equals(line("WARRANTABLE").ToString)
        Next

        Session("ITMCDLIST") = pkgComponents.ToString()
        Session("REL_NO") = lineDatSet
        ' MM-6305
        ' Once the package components are added to the dataset then these items need to be deleted from the temp_itm table
        ' this was causing an issue when you add a package which has some components these items will be inserted to Temp_Itm, next time add a package sku which does not
        ' have any component(s) but still it was getting previously added package components from temp_itm from the below code which is situated above
        ' Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpnts(Session.SessionID.ToString())
        If (pkgLines.Tables(0).Rows.Count > 0) Then
            theSalesBiz.DeleteTempItems(Session.SessionID.ToString())
        End If

    End Sub

    Public Sub calculate_total()
        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = ""
        Dim objSql As OracleCommand
        Dim DEL_DOC_NUM As String
        Dim MyDataReader As OracleDataReader

        DEL_DOC_NUM = Request("del_doc_num")
        conn.Open()

        If DEL_DOC_NUM & "" <> "" Then
            If Not Session("REL_LN") Is Nothing Then
                Dim ds As DataSet
                Dim tax_pct As Double = 0
                Dim tax As Double = 0

                If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

                    'Calculate Tax on Merchandise and Line Items
                    tax_pct = TaxUtils.getTaxPct(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), "")
                End If  ' line tax

                ds = Session("REL_LN")
                Dim merch_total As Double = 0

                Dim dt As DataTable = ds.Tables(0)
                Dim dr As DataRow
                ' line taxing calculates the tax for each line and delivery and setup charges separately
                ' header taxing sums all the lines by item type and then calculates the tax by tax authority based on the total percent by itm_tp
                ' therefore, line processing below calculates the tax whereas header processing just accumulates until the end and then calls the calculation 
                Dim hdrTaxReq As New taxHdrCalcRequest
                Dim tmp_Amt As Double = 0.0

                For Each dr In ds.Tables(0).Rows
                    If dr.RowState <> DataRowState.Deleted Then

                        merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("RET_PRC")))

                        If IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("RET_PRC")) Then
                            ' TODO - someday this needs to be based on tax code tax method
                            ' TODO - line taxing has to be based on written date andAlso based on whether tax auth taxes itm_tp for each line
                            If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                                If Determine_Taxability(dr("ITM_TP_CD")) = "Y" Then
                                    tax = tax + ((CDbl(dr("QTY")) * CDbl(dr("RET_PRC"))) * tax_pct)
                                End If
                            Else ' header tax method
                                tmp_Amt = CDbl(dr("QTY")) * CDbl(dr("RET_PRC"))  ' TODO no unit carpet logic
                                TaxUtils.AccumTaxingAmts(dr("ITM_TP_CD") & "", tmp_Amt, hdrTaxReq.itmTpAmts)
                            End If
                        End If
                    End If
                Next

                Session("REL_LN") = ds

                If IsNumeric(lblDelivery.Text) Then
                    ' TODO - someday this needs to be based on tax code tax method
                    If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                        ' if line tax
                        tax = tax + getTaxAmt(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), CDbl(lblDelivery.Text), "DEL")
                    Else ' if header
                        TaxUtils.AccumTaxingAmts("DEL", CDbl(lblDelivery.Text), hdrTaxReq.itmTpAmts)
                    End If
                End If

                If cbo_tax_exempt.SelectedIndex <> 0 Then
                    tax = 0
                    ' TODO - someday this needs to be based on tax code tax method
                ElseIf "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

                    ' if header tax
                    hdrTaxReq.taxCd = lbl_tax_cd.Text
                    If (txt_wr_dt.Text.isNotEmpty) Then hdrTaxReq.taxDt = FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate)

                    tax = TaxUtils.ComputeHdrTax(hdrTaxReq)
                End If

                lblTax.Text = FormatNumber(CDbl(tax), 2)
                lblSubtotal.Text = FormatNumber(CDbl(merch_total), 2)
            End If

            lbltotal.Text = FormatCurrency(CDbl(lblSubtotal.Text) + (CDbl(lblTax.Text) + CDbl(lblDelivery.Text)), 2)
            Try
                Dim footerIndex As Integer = Gridview1.Controls(0).Controls.Count - 1
                Dim hpl_find_merch As HyperLink = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("hpl_find_merch"), HyperLink)
                hpl_find_merch.NavigateUrl = "order.aspx?referrer=Relationship_Maintenance.aspx&del_doc_num=" & txt_del_doc_num.Text & "&query_returned=Y"
            Catch
            End Try
        End If

        'Close Connection 
        conn.Close()
    End Sub

    Protected Sub Price_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim RelLnDataset As DataSet = Session("REL_LN")
        Dim txtRetPrc As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txtRetPrc.NamingContainer, DataGridItem)
        Dim hdnRetPrc As HiddenField = CType(dgItem.FindControl("hidRetPrc"), HiddenField)
        Dim dr As DataRow = OrderUtils.GetOrderLine(RelLnDataset, dgItem.Cells(RelLnGrid.LINE).Text, False)

        lbl_warning.Text = ""
        If Not IsNumeric(txtRetPrc.Text) Then
            lbl_warning.Text = Resources.POSMessages.MSG0021
            lbl_warning.Visible = True
            txtRetPrc.Text = hdnRetPrc.Value
            txtRetPrc.Focus()
            Exit Sub
        Else
            lbl_warning.Text = String.Empty
            lbl_warning.Visible = False
            txtRetPrc.Text = Math.Abs(CDbl(txtRetPrc.Text))
        End If

        'Check for maxdisc limt it will cross or not.
        Dim hidMaxDisc As HiddenField = CType(dgItem.FindControl("hidMaxDisc"), HiddenField)
        If (Not String.IsNullOrEmpty(hidMaxDisc.Value)) Then

            Dim maxDiscPct As Decimal = 0, enteredRetPrice As Decimal = 0
            Dim effDt As String = Today.Date

            Dim drItmRetPrice As Double = 0
            Double.TryParse(dr("itm_ret_prc").ToString, drItmRetPrice)

            'this represents the calendar or item retail price
            Dim itmRetPrc As Double = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                                 String.Empty,
                                                                 String.Empty,
                                                                 effDt,
                                                                 dr("ITM_CD").ToString,
                                                                 drItmRetPrice)

            If Decimal.TryParse(hidMaxDisc.Value, maxDiscPct) AndAlso Decimal.TryParse(txtRetPrc.Text, enteredRetPrice) Then

                Dim maxDiscAmt As Decimal = itmRetPrc - (itmRetPrc * (maxDiscPct / 100))

                maxDiscAmt = Math.Round(maxDiscAmt, 2)  'Alice added on June 6, 2019, the issue was found during test 4168

                'MM-6914 - Maximum discount restriction should not be applied in case of CRMs, MDBs and MCRs

                If (maxDiscAmt > enteredRetPrice) Then
                    lbl_desc.Text = String.Format(Resources.POSErrors.ERR0019, FormatCurrency(maxDiscAmt, 2))
                    lbl_desc.Visible = True

                    txtRetPrc.Text = FormatNumber(dr("RET_PRC"))
                    txtRetPrc.Focus()

                    Dim seperator As Char = ";"
                    Dim sArr() As String = hidUpdLnNos.Value.ToString().Split(seperator)
                    If sArr.Contains(dgItem.Cells(RelLnGrid.LINE).Text.ToString) Then
                        Dim index As Integer = hidUpdLnNos.Value.LastIndexOf(seperator)
                        hidUpdLnNos.Value = hidUpdLnNos.Value.Substring(0, index)
                    End If
                    ' hidUpdLnNos.Value = String.Empty
                    Exit Sub
                End If
            End If
        End If

        Dim margin As Double = 0
        If IsNumeric(ConfigurationManager.AppSettings("margin").ToString) Then
            margin = CDbl(ConfigurationManager.AppSettings("margin").ToString)
        End If

        'will prompt for mgr approver if set to 'Line' or "order' OR if "N" but the GM is < the specified margin
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        Dim promptForApproval As Boolean = AppConstants.ManagerOverride.BY_LINE = prcOverride OrElse
                                           AppConstants.ManagerOverride.BY_ORDER = prcOverride OrElse
                                           (margin > 0 AndAlso determine_gm(dgItem.Cells(RelLnGrid.ITM_CD).Text, txtRetPrc.Text) < margin)
        If promptForApproval Then
            If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
                'If Mgr Override is by order, then in the hdnRetprc field put the orig value. this will be needed if approval fails later.
                Dim hdnOrigPrc As HiddenField = CType(dgItem.FindControl("hidOrigPrc"), HiddenField)
                Dim txtExtPrc As TextBox = CType(dgItem.FindControl("ext_prc"), TextBox)
                Dim txtQty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
                hdnRetPrc.Value = hdnOrigPrc.Value

                'store the line# of the line being modified to use later
                If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                    hidUpdLnNos.Value = hidUpdLnNos.Value.ToString() & ";" & dgItem.Cells(RelLnGrid.LN_NUM).Text
                Else
                    hidUpdLnNos.Value = dgItem.Cells(RelLnGrid.LN_NUM).Text
                End If

                'update the ext price  value
                txtExtPrc.Text = Replace(FormatNumber(txtRetPrc.Text * txtQty.Text, 2), ",", "")

                'update the price in the dataset right away, so that everything shows the updated price
                Dim ds As DataSet = Session("REL_LN")
                'MM-9504
                For Each lnRow As DataRow In ds.Tables(0).Rows
                    If Not lnRow.RowState = DataRowState.Deleted Then
                        If lnRow("LINE") = CInt(dgItem.Cells(RelLnGrid.LN_NUM).Text) Then
                            lnRow("RET_PRC") = CDbl(txtRetPrc.Text)
                            Exit For
                        End If
                    End If
                Next
            Else
                'means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
                Session("SelectedRow") = dgItem.ItemIndex & ":" & dgItem.Cells(RelLnGrid.LN_NUM).Text

                ' Swap the values of the hidden field and the display price field. So, the hidden field will have the edited value
                ' and the display field the last price, before htis edit. This has to be done before opening the mgr approval popup. 
                ' That way, if the price approval fails, the display ield will have the orig value already and the hidden will be updated.
                Dim editedVal As String = txtRetPrc.Text
                'MM-9504
                txtRetPrc.Text = Replace(FormatNumber(hdnRetPrc.Value, 2), ",", "") 'display field has the last edited value
                hdnRetPrc.Value = editedVal      'hidden prc field has the edited val
                ViewState("editedVal") = editedVal
                Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")
                If txtMgrPwd IsNot Nothing Then
                    txtMgrPwd.Focus()
                End If
                popupMgrOverride.ShowOnPageLoad = True
                Session("SHOWPOPUP") = True
            End If
            calculate_total()
        Else
            'approval not required
            Dim ds As DataSet = Session("REL_LN")
            If txtRetPrc.Text & "" <> "" Then
                Dim dt As DataTable = ds.Tables(0)
                'Dim dr As DataRow

                For Each dr In ds.Tables(0).Rows
                    If dr.RowState <> DataRowState.Deleted Then
                        If dr("LINE") = dgItem.Cells(RelLnGrid.LN_NUM).Text Then
                            dr("RET_PRC") = System.Math.Abs(CDbl(txtRetPrc.Text))
                            dr("MANUAL_PRC") = System.Math.Abs(CDbl(txtRetPrc.Text))
                        End If
                    End If
                Next
                If AppConstants.ManagerOverride.NONE = prcOverride Then
                    ReapplyDiscounts(ds.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
                    Session("REL_LN") = ds
                End If
                bindgrid()
                calculate_total()
            End If
        End If
    End Sub

    Protected Sub Qty_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)
        Dim ds As DataSet
        Dim calculateDiscounts As Boolean = False
        ds = Session("REL_LN")

        If textdata.Text & "" <> "" Then
            Dim dt As DataTable = ds.Tables(0)
            Dim dr As DataRow

            For Each dr In ds.Tables(0).Rows

                If dr.RowState <> DataRowState.Deleted Then

                    If dr("LINE") = dgItem.Cells(RelLnGrid.LN_NUM).Text Then

                        lbl_warning.Text = OrderUtils.ValAndFormatQty(textdata.Text, dr("QTY"), dgItem.Cells(RelLnGrid.ITM_TP_CD).Text,
                                                                      dgItem.Cells(RelLnGrid.WARR_ITM_LINK).Text & "", "")
                        If lbl_warning.Text.isNotEmpty Then
                            lbl_warning.Visible = True
                        End If
                        If textdata.Text <> dr("QTY") Then
                            dr("QTY") = textdata.Text
                            calculateDiscounts = True
                        End If

                    End If
                End If
            Next
            If calculateDiscounts Then
                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(ds.Tables(0), ReapplyDiscountRequestDtc.LnChng.Qty)
            End If
            Session("REL_LN") = ds
        End If

        bindgrid()
        calculate_total()
    End Sub

    ''' <summary>
    ''' Executes when the manager override is successfully carried out via the popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved
        If Session("SelectedRow") IsNot Nothing Then
            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(Gridview1.Items(CType(modifiedRow(0), Integer)), DataGridItem)

            If dgItem IsNot Nothing Then
                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
                Dim hdnRetPrc As HiddenField = CType(dgItem.FindControl("hidRetPrc"), HiddenField)
                Dim txtExtPrc As TextBox = CType(dgItem.FindControl("ext_prc"), TextBox)
                Dim txtQty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
                'MM-9504
                If Not IsNothing(ViewState("editedVal")) AndAlso CInt(ViewState("editedVal").ToString) <> hdnRetPrc.Value Then
                    hdnRetPrc.Value = CInt(ViewState("editedVal").ToString)
                End If
                If txtRetPrc IsNot Nothing Then
                    'since the approval was successful, allow the new value to be set on the display field. 
                    ' So, need to get it from the hidden field that has the last value before this edit.
                    Dim displayValue As String = txtRetPrc.Text
                    txtRetPrc.Text = Replace(FormatNumber(hdnRetPrc.Value, 2), ",", "")
                    'update the hidden field with the newly edited value
                    hdnRetPrc.Value = displayValue

                    'get the approver's emp_cd and set it on the line
                    Dim hidAppEmpCd As HiddenField = CType(ucMgrOverride.FindControl("hidAppEmpCd"), HiddenField)
                    Dim approverEmpCd As String = hidAppEmpCd.Value.ToString

                    'update the ext price  value
                    txtExtPrc.Text = Replace(FormatNumber(txtRetPrc.Text * txtQty.Text, 2), ",", "")

                    Dim linesDatSet As DataSet = New DataSet
                    If Not Session("REL_LN") Is Nothing Then

                        linesDatSet = Session("REL_LN")
                        'MM-9504
                        For Each dr As DataRow In linesDatSet.Tables(0).Rows
                            If Not dr.RowState = DataRowState.Deleted Then
                                If dr.Item("line") = modifiedRow(1) Then
                                    dr.Item("ret_prc") = txtRetPrc.Text
                                    dr.Item("prc_chg_app_cd") = approverEmpCd
                                End If
                            End If
                        Next

                        'reapplies the discounts (if any) when applicable, and updates the session data
                        ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
                        Session("REL_LN") = linesDatSet
                        bindgrid()
                        calculate_total()
                    End If
                End If
            End If
        End If
        lbl_desc.Text = String.Empty
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If
        popupMgrOverride.ShowOnPageLoad = False
    End Sub

    ''' <summary>
    ''' Gets fired when the manager override fails. Any resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled
        If Session("SelectedRow") IsNot Nothing Then
            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(Gridview1.Items(CType(modifiedRow(0), Integer)), DataGridItem)
            If dgItem IsNot Nothing Then

                'get the price field and the hidden field for the line being changed
                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
                Dim hdnRetPrc As HiddenField = CType(dgItem.FindControl("hidRetPrc"), HiddenField)

                If txtRetPrc IsNot Nothing Then
                    'since the approval was aborted, reset the value on the hidden field. Display field can be what it was originally.
                    hdnRetPrc.Value = txtRetPrc.Text
                End If
            End If
            Dim linesDatSet As DataSet = Session("REL_LN")
            'reapplies the discounts (if any) when applicable, and updates the session data
            ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
            Session("REL_LN") = linesDatSet
            Gridview1.DataSource = linesDatSet
            Gridview1.DataBind()
            DoAfterBindgrid()
        End If
        lbl_desc.Text = String.Empty
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If
        popupMgrOverride.ShowOnPageLoad = False
    End Sub

    ''' <summary>
    ''' Determines if the price on any lines on the order has been modified and if needed, prompts for a 
    ''' manager approval via the summary price approval popup. 
    ''' </summary>
    Private Sub PromptForSummaryPriceMgrOverride()
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        If AppConstants.ManagerOverride.BY_ORDER = prcOverride Then
            'get the unique, not null value of all rows whose price was modified
            Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()

            If (Not IsNothing(Session("REL_LN")) AndAlso modifiedLnNums.Any) Then
                Dim table As DataTable = SalesUtils.GetOrderLinesTable()

                For Each dgItem As DataGridItem In Gridview1.Items
                    Dim txtRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
                    Dim hdnOrigPrc As HiddenField = CType(dgItem.FindControl("hidOrigPrc"), HiddenField)
                    Dim lnNum As Integer = dgItem.Cells(RelLnGrid.LN_NUM).Text

                    For Each id As String In modifiedLnNums
                        If lnNum = CInt(id) Then
                            'gather the needed info to build the data table to pass to the price summary grid
                            Dim lblDesc As Label = CType(dgItem.FindControl("Label2"), Label)
                            Dim tboxQty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
                            SalesUtils.AddOrderLineRow(table,
                                                       dgItem.Cells(RelLnGrid.LN_NUM).Text,
                                                       dgItem.Cells(RelLnGrid.ITM_CD).Text,
                                                       lblDesc.Text,
                                                       CDbl(tboxQty.Text),
                                                       CDbl(hdnOrigPrc.Value),
                                                       CDbl(txtRetPrc.Text))
                            Exit For
                        End If
                    Next
                Next

                'pass this datatable to the new summ price approval popup & display it.
                If table IsNot Nothing AndAlso table.Rows.Count > 0 Then
                    ucSummaryPrcChange.BindData(table)
                    Dim popupPrcChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
                    popupPrcChangeAppr.ShowOnPageLoad = True
                End If
            End If
        End If
    End Sub

    ''' <summary>
    '''  Responds as the subscriber to the summary price change approval user control 'WasPCApproved' event. It will hide the popup
    ''' and update the UI, the dataset and recalculate amounts based on the new price info.  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCApproved(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCApproved
        'means that the approval for all the line price changes was successful, so update the lines in the grid and temp_itm          
        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
        Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()
        'get the approver's emp_cd to set on the line                            
        Dim hidAppEmpCd As HiddenField = CType(popupPriceChangeAppr.FindControl("ucManagerOverride").FindControl("hidAppEmpCd"), HiddenField)
        Dim approverEmpCd As String = hidAppEmpCd.Value.ToString
        Dim linesDatSet As New DataSet

        If (Not IsNothing(Session("REL_LN")) AndAlso modifiedLnNums.Any) Then
            'see if this line was one of the lines modified 
            For Each id As Integer In modifiedLnNums
                If Not Session("REL_LN") Is Nothing Then
                    linesDatSet = Session("REL_LN")
                    For Each dr As DataRow In linesDatSet.Tables(0).Rows
                        'update the approver emp cd for each line
                        'MM-9504
                        If Not dr.RowState = DataRowState.Deleted Then
                            If dr.Item("line") = id Then
                                dr.Item("prc_chg_app_cd") = approverEmpCd
                                dr.Item("MANUAL_PRC") = dr.Item("RET_PRC")
                                Exit For
                            End If
                        End If
                    Next
                    calculate_total()
                End If
            Next

            'reapplies the discounts (if any) when applicable, and updates the session data
            ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
            Session("REL_LN") = linesDatSet
            Gridview1.DataSource = linesDatSet
            Gridview1.DataBind()
            DoAfterBindgrid()
            calculate_total()
        End If

        popupPriceChangeAppr.ShowOnPageLoad = False
        DoAfterSummaryPriceChangeApproval()
    End Sub

    ''' <summary>
    '''  Responds as the subscriber to the summary price change approval user control 'WasPCCanceled' event. It will hide the popup
    ''' and update the UI, the dataset and reset amounts based on the original price info.  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCCanceled(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCCanceled
        WasPCCanceled()
    End Sub

    ''' <summary>
    ''' Updating the Not approved columns with 'Y' flag
    ''' and update the UI, the dataset and reset amounts based on the original price info.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub WasPCCanceled()
        'Here needs to undo the Grid changes
        Dim txtRetPrc, txtExtPrc, txtQty As New TextBox
        Dim hdnOrigPrc As HiddenField
        Dim lnNum As Integer
        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
        Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()
        'MM-9870
        'Updating 'N' in the approved column if click on the cancel in the Manager Approval popup
        Dim discountGrid As DevExpress.Web.ASPxGridView.ASPxGridView = ucDiscount.FindControl("grdDiscItems")
        Dim discountRowCount As Integer = discountGrid.VisibleRowCount
        If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
            If Not IsNothing(Session("discountsApplied")) Then
                Dim discountsApplied As DataTable = Session("discountsApplied")
                Dim drs = (From u In discountsApplied.AsEnumerable()
                           Where u.Field(Of String)("Approved") = "A"
                           Select u)
                For Each Row As DataRow In drs
                    If Row("DISC_IS_EXISTING") AndAlso Row("Approved").ToString.Equals("A") Then
                        Row("Approved") = "Y"
                    Else
                        Row("Approved") = "N"
                    End If
                Next
                Session("discountsApplied") = discountsApplied
            End If
        End If
        'MM-9870
        If (Not IsNothing(Session("REL_LN")) AndAlso modifiedLnNums.Any) OrElse Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
            'see if this line was one of the lines modified 
            For Each id As Integer In modifiedLnNums
                '***update the a the grid and refresh all the display values
                For Each dgItem As DataGridItem In Gridview1.Items
                    txtRetPrc = CType(dgItem.FindControl("unit_prc"), TextBox)
                    hdnOrigPrc = CType(dgItem.FindControl("hidOrigPrc"), HiddenField)
                    txtExtPrc = CType(dgItem.FindControl("ext_prc"), TextBox)
                    txtQty = CType(dgItem.FindControl("qty"), TextBox)
                    lnNum = dgItem.Cells(RelLnGrid.LN_NUM).Text

                    If lnNum = CInt(id) Then
                        ' reset the orig price on the display field
                        txtRetPrc.Text = FormatNumber(hdnOrigPrc.Value)
                        txtExtPrc.Text = FormatNumber(txtRetPrc.Text * txtQty.Text, 2)
                        Exit For
                    End If
                Next

                '***update the asctual dataset now 
                Dim soLnDatSet As DataSet = New DataSet
                If Not Session("REL_LN") Is Nothing Then
                    'get the row that matches the current line being modified
                    Dim ds As DataSet = Session("REL_LN")
                    'MM-9504
                    For Each lnRow As DataRow In ds.Tables(0).Rows
                        If Not lnRow.RowState = DataRowState.Deleted Then
                            If lnRow("LINE") = id Then
                                lnRow("RET_PRC") = CDbl(txtRetPrc.Text)
                            End If
                        End If
                    Next
                End If
            Next
            Dim linesDatSet As DataSet = Session("REL_LN")
            'reapplies the discounts (if any) when applicable, and updates the session data
            'MM-9870
            If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
                If Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
                    ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void, "", True)
                    If discountGrid.VisibleRowCount = 0 Then
                        ' If discountGrid grid is not containing any rows Hiding the discounts gridview and  Successful label message is hiding 
                        ucDiscount.FindControl("grdDiscItems").Visible = False
                        ucDiscount.FindControl("lblMessage").Visible = False
                    End If
                Else
                    ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
                End If
            Else
                ReapplyDiscounts(linesDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
            End If
            Session("REL_LN") = linesDatSet
            Gridview1.DataSource = linesDatSet
            Gridview1.DataBind()
            DoAfterBindgrid()
            calculate_total()
        End If

        popupPriceChangeAppr.ShowOnPageLoad = False
        DoAfterSummaryPriceChangeApproval()
    End Sub

    ''' <summary>
    ''' Executes after the summary price approval popup has been dismissed.
    ''' It will redirect to the intended page or location before the approval 
    ''' popup was displayed, clear out the modified lines list, etc.
    ''' </summary>
    Private Sub DoAfterSummaryPriceChangeApproval()
        hidUpdLnNos.Value = String.Empty

        If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
            Response.Redirect(hidDestUrl.Value)
        ElseIf Not String.IsNullOrEmpty(ViewState("EventName")) Then
            Select Case ViewState("EventName").ToString()
                Case "PageControlActiveTabChange"
                    AddHandler ASPxPageControl1.ActiveTabChanging, AddressOf ASPxPageControl1_ActiveTabChanging
            End Select
        End If
    End Sub

    ''' <summary>
    ''' This fires when the change is in the process of changing but hasn't changed yet. 
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    Private Sub ASPxPageControl1_ActiveTabChanging(source As Object, e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs) Handles ASPxPageControl1.ActiveTabChanging
        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
            ViewState("EventName") = "PageControlActiveTabChange"
            ViewState("NewPageIndex") = DirectCast(e.Tab, DevExpress.Web.ASPxTabControl.TabPage).Index
        Else
            PageControlActiveTabChange(e)
        End If
    End Sub

    Private Sub PageControlActiveTabChange(e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs)
        Dim idx As Integer = 0
        If Integer.TryParse(ViewState("NewPageIndex"), idx) Then
            ASPxPageControl1.ActiveTabIndex = idx
        End If
        If ASPxPageControl1.ActiveTabIndex = 2 Then  'Discount TAB
            If IsOpenRelationship() Then
                Dim discountGrid As DevExpress.Web.ASPxGridView.ASPxGridView = ucDiscount.FindControl("grdDiscItems")
                Dim discountRowCount As Integer = discountGrid.VisibleRowCount
                If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
                    'MM-9870
                    If Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
                        ucDiscount.HideManagerPopUp()
                    Else
                        ucDiscount.ShowManagerPopUp()
                        e.Cancel = True
                    End If
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Returns a flag indicating if the order is an 'open', sale order
    ''' </summary>
    ''' <returns></returns>
    Private Function IsOpenRelationship() As Boolean
        Return (txt_del_doc_num.Text.isNotEmpty AndAlso
                AppConstants.Order.STATUS_OPEN = cbo_rel_status.Value)
    End Function

    Sub DoItemEdit(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Dim objRetPrc As TextBox
        Dim objQty As TextBox
        Gridview1.EditItemIndex = e.Item.ItemIndex
        bindgrid()
        objRetPrc = CType(Gridview1.Items(Gridview1.EditItemIndex).Cells(RelLnGrid.UNIT_PRC).Controls(0), TextBox)
        objQty = CType(Gridview1.Items(Gridview1.EditItemIndex).Cells(RelLnGrid.QTY).Controls(0), TextBox)
        objRetPrc.Width = Unit.Parse("1 cm")
        objQty.Width = Unit.Parse(".6 cm")
    End Sub

    Protected Sub Gridview1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.ItemCommand
        Dim Lead As String = ""
        If Request("LEAD") = "TRUE" Then
            Lead = "&LEAD=TRUE"
        End If

        Select Case e.CommandName
            Case "find_inventory"
                If cbo_rel_status.Value = "O" Then
                    If ConfigurationManager.AppSettings("inv_avail").ToString = "CUSTOM" Then
                        ASPxPopupControl3.ContentUrl = "Custom_Availability.aspx?SKU=" & e.Item.Cells(RelLnGrid.ITM_CD).Text & "&INV_SELECTION=Y"
                    Else
                        ASPxPopupControl3.ContentUrl = "InventoryLookup.aspx?SKU=" & e.Item.Cells(RelLnGrid.ITM_CD).Text & "&INV_SELECTION=Y"
                    End If
                    ASPxPopupControl3.ShowOnPageLoad = True
                End If
            Case "find_gm"
                Dim sql As String
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim frt_factor As Double = 0
                Dim repl_cst As Double = 0
                Dim ret_prc As Double = 0
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

                Dim txt_ret_prc As TextBox
                txt_ret_prc = CType(e.Item.FindControl("unit_prc"), TextBox)

                If IsNumeric(txt_ret_prc.Text) Then ret_prc = CDbl(txt_ret_prc.Text)

                conn.Open()
                sql = "select repl_cst, frt_fac from itm where itm_cd=:ITM_CD"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql.Parameters(":ITM_CD").Value = e.Item.Cells(RelLnGrid.ITM_CD).Text

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read() Then
                        If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                        If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                    End If
                    MyDataReader.Close()
                Catch
                    conn.Close()
                    Throw
                End Try

                conn.Close()
                If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                lbl_gm.Text = "Retail: " & FormatCurrency(ret_prc, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(repl_cst, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(ret_prc - repl_cst, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((ret_prc - repl_cst) / ret_prc, 2) & "<br />"
                ASPxPopupControl6.ShowOnPageLoad = True
        End Select
    End Sub

    Public Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound
        Dim dtable As DataTable
        Dim ds As DataSet
        Dim sql As String
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        If FAB_FOUND <> "Y" Then
            If Not Session("REL_LN") Is Nothing Then
                ds = Session("REL_LN")

                dtable = New DataTable
                dtable = ds.Tables(0)
                numrows = dtable.Rows.Count

                For loop1 = 0 To numrows - 1
                    If dtable.Rows(loop1).RowState <> DataRowState.Deleted Then
                        If dtable.Rows(loop1).Item("ITM_TP_CD").ToString = "FAB" Then
                            FAB_FOUND = "Y"
                            x = True
                        End If
                        If FAB_FOUND <> "Y" Then
                            e.Item.Cells(RelLnGrid.FAB_CHKBOX).Visible = False
                        Else
                            e.Item.Cells(RelLnGrid.FAB_CHKBOX).Visible = True
                        End If
                    End If
                Next
            End If
        End If

        If e.Item.Cells(RelLnGrid.ITM_CD).Text & "" <> "" Then
            Dim TheFile As System.IO.FileInfo = Nothing
            Dim File_Path As String = ""
            'jkl check on this
            sql = "select major.mjr_cd || '_' || major.des || '/' || minor.mnr_cd || '_' || minor.des || " &
                          "   '/' || item.ve_cd || '_' || item.itm_cd || '_WEB.jpg' " &
                          " from itm item, inv_mnr minor, inv_mjr major, ve vendor where item.mnr_cd = minor.mnr_cd and minor.mjr_cd = major.mjr_cd and item.ve_cd = vendor.ve_cd and item.itm_cd='" & e.Item.Cells(RelLnGrid.ITM_CD).Text & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

            ' Daniela coomment not needed 
            'Try
            '    'Execute DataReader 
            '    Mydatareader = objsql.ExecuteReader
            '    'Store Values in String Variables 
            '    If Mydatareader.Read() Then
            '        File_Path = Mydatareader.Item(0).ToString
            '        File_Path = Replace(File_Path, " ", "_")
            '        TheFile = New System.IO.FileInfo(Server.MapPath("\external_images\") & File_Path)
            '    End If
            '    Mydatareader.Close()
            'Catch

            'End Try

            ' Daniela add Picture URL
            Dim image_URL As String = ""
            Try
                Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                Dim imgZipCd As String = LeonsBiz.GetPostalCode()
                Dim imgLangCode As String = LeonsBiz.GetLangCode()
                image_URL = LeonsBiz.Find_Merchandise_Images(e.Item.Cells(RelLnGrid.ITM_CD).Text, imgZipCd, imgLangCode)
                If isNotEmpty(image_URL) Then
                    img_picture.ImageUrl = image_URL
                Else
                    img_picture.ImageUrl = "~/images/image_not_found.jpg"
                End If
            Catch ' Do nothing if picture not found or error
            End Try
            ' End Daniela

            'Try
            '    If Not IsNothing(TheFile) AndAlso TheFile.Exists Then
            '        Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
            '        img_picture.ImageUrl = "~/external_images/" & File_Path
            '    Else
            '        Dim image_URL As String = HBCG_Utils.Find_Merchandise_Images(e.Item.Cells(RelLnGrid.ITM_CD).Text)
            '        Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
            '        If image_URL.ToString & "" <> "" Then
            '            img_picture.ImageUrl = image_URL
            '        Else
            '            img_picture.ImageUrl = "~/images/image_not_found.jpg"
            '        End If
            '    End If
            'Catch

            'End Try

            Dim txt_line_desc As TextBox = CType(e.Item.FindControl("txt_line_desc"), TextBox)
            If Not IsNothing(txt_line_desc) Then
                txt_line_desc.Visible = True
                sql = "SELECT DESCRIPTION FROM RELATIONSHIP_LINES_DESC WHERE LINE=:LINE_ITM AND REL_NO=:DEL_DOC"
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                objsql.Parameters.Add(":LINE_ITM", OracleType.VarChar)
                objsql.Parameters(":LINE_ITM").Value = e.Item.Cells(RelLnGrid.LINE).Text
                objsql.Parameters.Add(":DEL_DOC", OracleType.VarChar)
                objsql.Parameters(":DEL_DOC").Value = txt_del_doc_num.Text

                Try
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If (Mydatareader.Read()) Then
                        txt_line_desc.Text = Mydatareader.Item("DESCRIPTION").ToString
                    End If
                Catch
                    conn.Close()
                    conn2.Close()
                    Throw
                End Try

                Mydatareader.Close()
            End If
        End If

        If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))
        Dim img_gm As ImageButton = CType(e.Item.FindControl("img_gm"), ImageButton)

        If Not IsNothing(img_gm) Then
            If GM_Sec = "Y" Then
                img_gm.Visible = True
                btn_total_margins.Visible = True
            Else
                img_gm.Visible = False
            End If
        End If

        If e.Item.Cells(RelLnGrid.ITM_TP_CD).Text = SkuUtils.ITM_TP_PKG Then
            Dim txt_qty As TextBox = CType(e.Item.FindControl("qty"), TextBox)
            txt_qty.Enabled = False
        End If

        If cbo_rel_status.Value <> AppConstants.Order.STATUS_OPEN Then
            Dim txt_qty As TextBox = CType(e.Item.FindControl("qty"), TextBox)
            If Not IsNothing(txt_qty) Then
                txt_qty.Enabled = False
            End If
            Dim unit_prc As TextBox = CType(e.Item.FindControl("unit_prc"), TextBox)
            If Not IsNothing(unit_prc) Then
                unit_prc.Enabled = False
            End If
        End If

        If cbo_rel_status.Value <> AppConstants.Order.STATUS_OPEN Then
            Dim btn_add_item As Button = CType(e.Item.FindControl("btn_add_item"), Button)
            If Not IsNothing(btn_add_item) Then
                btn_add_item.Enabled = False
            End If
            Dim txt_line_desc As TextBox = CType(e.Item.FindControl("txt_line_desc"), TextBox)
            If Not IsNothing(txt_line_desc) Then
                txt_line_desc.Enabled = False
            End If
        End If

        'check for ARS and show avail dt, if needed
        Dim txtQty As TextBox = CType(e.Item.FindControl("qty"), TextBox)
        Dim isInventory As Boolean = ResolveYnToBooleanEquiv(e.Item.Cells(RelLnGrid.INVENTORY).Text.Trim())
        If Not IsNothing(txtQty) Then
            Dim availDt As Date = GetAvailDate(isInventory,
                                               e.Item.Cells(RelLnGrid.ITM_CD).Text,
                                               CDbl(txtQty.Text))

            If Not (availDt = Date.MinValue) And isInventory Then
                Dim lblAvailResult As Label = CType(e.Item.FindControl("lblAvailResults"), Label)
                lblAvailResult.Visible = True
                lblAvailResult.Text = Resources.POSMessages.MSG0002 & FormatDateTime(availDt, DateFormat.ShortDate)
            End If
        End If

        conn.Close()
        conn2.Close()
    End Sub

    Private Sub DoAfterBindgrid()
        Dim intRow As Integer = 0
        Dim str_treatable As String = ""
        Dim str_tp_cd As String = "".Trim
        Dim str_treated As String = ""
        Dim sql As String

        ' Number of rows in the DataGrid        
        Dim intRows As Integer = Gridview1.Items.Count - 1
        Dim GridItem As DataGridItem

        If x = False Then
            Dim cmd As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT itm_tp_cd from RELATIONSHIP_LINES where REL_NO =:REL_NO and itm_tp_cd='FAB'"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add("REL_NO", OracleType.VarChar)
            cmd.Parameters("REL_NO").Value = txt_del_doc_num.Text

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                If (MyDataReader.Read()) Then
                    x = True
                End If

                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        Dim isInventory As Boolean
        For intRow = 0 To intRows
            ' Get the grid row.
            GridItem = Gridview1.Items(intRow)
            str_tp_cd = GridItem.Cells(RelLnGrid.ITM_TP_CD).Text().Trim()
            str_treatable = GridItem.Cells(RelLnGrid.TREATABLE).Text().Trim()
            str_treated = GridItem.Cells(RelLnGrid.TREATED).Text().Trim()

            If str_tp_cd = "INV" Then
                GridItem.Cells(RelLnGrid.FAB_CHKBOX).Enabled = True
            Else
                GridItem.Cells(RelLnGrid.FAB_CHKBOX).Enabled = False
            End If

            If x = True Then
                If str_tp_cd = "INV" And str_treatable = "Y" Then
                    GridItem.Cells(RelLnGrid.FAB_CHKBOX).Enabled = True
                    If str_treated = "Y" Then
                        Dim cbDelete As CheckBox = DirectCast(GridItem.FindControl("SelectThis"), CheckBox)
                        cbDelete.Checked = True
                    End If
                Else
                    GridItem.Cells(RelLnGrid.FAB_CHKBOX).Enabled = False
                End If
            End If

            ' for testing debug purposes
            'Dim A0 As String = GridItem.Cells(RelLnGrid.LN_NUM).Text
            'Dim A1 As String = GridItem.Cells(RelLnGrid.ITM_CD).Text
            'Dim A4 As String = GridItem.Cells(RelLnGrid.SKU_DES).Text
            'Dim A5 As String = GridItem.Cells(RelLnGrid.UNIT_PRC).Text
            'Dim A6 As String = GridItem.Cells(RelLnGrid.QTY).Text
            'Dim A10 As String = GridItem.Cells(RelLnGrid.LINE).Text
            'Dim A12 As String = GridItem.Cells(RelLnGrid.TREATED).Text
            'Dim A13 As String = GridItem.Cells(RelLnGrid.TREATABLE).Text
            'Dim A14 As String = GridItem.Cells(RelLnGrid.ITM_TP_CD).Text
            'Dim A15 As String = GridItem.Cells(RelLnGrid.WARR_ITM_LINK).Text
            'Dim A16 As String = GridItem.Cells(RelLnGrid.WARR_ROW_LINK).Text
            'Dim A17 As String = GridItem.Cells(RelLnGrid.WARRANTABLE).Text
            'Dim A18 As String = GridItem.Cells(RelLnGrid.WARR_DAYS).Text
            'Dim A19 As String = GridItem.Cells(RelLnGrid.INVENTORY).Text

            'disables or enables the warr field display
            Dim lbl_warr As Label = DirectCast(GridItem.FindControl("lbl_warr"), Label)
            Dim lbl_warr_sku As Label = DirectCast(GridItem.FindControl("lbl_warr_sku"), Label)
            isInventory = ResolveYnToBooleanEquiv(GridItem.Cells(RelLnGrid.INVENTORY).Text.Trim())

            If isInventory AndAlso "Y".Equals(GridItem.Cells(RelLnGrid.WARRANTABLE).Text) Then
                If (GridItem.Cells(RelLnGrid.WARR_ITM_LINK).Text & "").isNotEmpty AndAlso
                    GridItem.Cells(RelLnGrid.WARR_ITM_LINK).Text <> "&nbsp;" Then

                    lbl_warr.Visible = True
                    lbl_warr_sku.Visible = True
                    Dim cbWarr As CheckBox = DirectCast(GridItem.FindControl("ChkBoxWarr"), CheckBox)
                    cbWarr.Checked = True
                    GridItem.Cells(RelLnGrid.WARR_CHKBOX).Enabled = True
                Else
                    lbl_warr.Visible = False
                    lbl_warr_sku.Visible = False
                End If
            Else
                lbl_warr.Visible = False
                lbl_warr_sku.Visible = False
                GridItem.Cells(RelLnGrid.WARR_CHKBOX).Enabled = False
            End If

            ' TODO (OR NOT SINCE THIS SHOULD GO AWAY), the add button sb on the same column as the delete, like SOM;
            '  unfortunately, being below extended price means that extended price cannot be disabled and if you touch
            '  it, then the whole page clears
        Next
    End Sub

    ''' <summary>
    ''' Calls an API to calculate availability for a store that has been setup to use
    ''' advanced reservation logic (ARS). 
    ''' </summary>
    ''' <param name="isInventory">if the sku is 'inventoriable' or not</param>
    ''' <param name="sku">the item for which to check availability</param>
    ''' <param name="qty">the qty needed</param>
    ''' <returns>the available date</returns>
    Private Function GetAvailDate(ByVal isInventory As Boolean, ByVal sku As String, ByVal qty As Double) As Date
        Dim availDt As Date

        'calculate availability for ARS store, if any 
        If isInventory AndAlso Session("STORE_ENABLE_ARS") Then

            availDt = theInvBiz.CalcAvailARS(sku, Session("PD"), Session("ZONE_CD"), qty, Session("pd_store_cd"))
        End If

        Return availDt
    End Function

    ''' <summary>
    ''' Returns a list of line sequence numbers of all the warrantable lines that have the passed-in
    ''' warranty line attached to it.
    ''' </summary>
    ''' <param name="warrSkuLnNum">line sequence number of the warranty</param>
    ''' <returns>an array list of the line sequences of all the warrantable lines with the warranty</returns>
    Private Function GetLinkedWarrLnNums(ByVal warrSkuLnNum As String) As ArrayList
        Dim warrLnNumList As New ArrayList
        Dim soLnDataSet As DataSet = Session("REL_LN")

        For Each soLnRow As DataRow In soLnDataSet.Tables(0).Rows
            'Dim itm As String = soLnRow("WARR_itm_link").ToString
            'Dim lnNum As String = soLnRow("WARR_row_link")
            If soLnRow.RowState <> DataRowState.Deleted AndAlso
                soLnRow("WARR_row_link").ToString.isNotEmpty AndAlso soLnRow("WARR_row_link") = CInt(warrSkuLnNum) Then

                'If SOM, then must check the void flag; relationships do not have void flags, lines are deleted 
                'finds the warrantied row with the same warranty attached and adds it to the list
                warrLnNumList.Add(soLnRow("LINE").ToString)
            End If
        Next

        Return warrLnNumList
    End Function

    ''' <summary>
    ''' Removes ALL the warranty links from any and all warrantable lines that point to a warranty line that is being removed
    ''' </summary>
    ''' <param name="warrLnNum">the line# of the warranty to remove from the warrantables</param>
    Private Sub removeLinks2Warr(ByVal warrLnNum As String)
        Dim soLnDataSet As DataSet = Session("REL_LN")
        Dim rtnVal As DataRow = Nothing

        '**** iterate through all the rows to find the warrantable lines to unlink this warranty line
        For Each row In soLnDataSet.Tables(0).Rows
            If row.RowState <> DataRowState.Deleted AndAlso row("WARR_ROW_LINK").ToString.isNotEmpty AndAlso row("WARR_ROW_LINK") = warrLnNum Then
                'finds the matching datarow from the cached dataset table and updates it
                row("WARR_row_link") = System.DBNull.Value
                row("WARR_itm_link") = ""
            End If
        Next

        Session("REL_LN") = soLnDataSet
    End Sub

    ''' <summary>
    ''' Updates the warranty link of the warrantable sku based on the passed-in values. This could be because a new or 
    ''' existing warranty was selected or deselected 
    ''' </summary>
    ''' <param name="warrSku">the warranty sku to be linked or unlinked</param>
    ''' <param name="warrLnNum">the line number of the warranty sku to be linked or unlinked</param>
    ''' <param name="warrableLnNum">the line number of the warrantable sku to which this warranty needs to be linked or unlinked</param>
    Private Sub UpdtLinks2Warrable(ByVal warrSku As String, ByVal warrLnNum As String, ByVal warrableLnNum As String)
        Dim soLnDataSet As DataSet = Session("REL_LN")
        Dim soLnRow As DataRow

        '**** iterate through all the rows to find the warrantable line to link or unlink this warranty sku 
        For Each soLnRow In soLnDataSet.Tables(0).Rows
            If soLnRow.RowState <> DataRowState.Deleted AndAlso soLnRow("LINE").ToString.isNotEmpty AndAlso soLnRow("LINE") = warrableLnNum Then
                'finds the warrantied row and update the warranty link on it

                If warrLnNum.isEmpty Then
                    soLnRow("WARR_ROW_LINK") = System.DBNull.Value
                Else
                    soLnRow("WARR_ROW_LINK") = CInt(warrLnNum)
                End If

                soLnRow("WARR_ITM_LINK") = warrSku
                Exit For
            End If
        Next

        Session("REL_LN") = soLnDataSet
    End Sub

    ''' <summary>
    ''' Call when removing links based on warrantable line being unchecked or deleted;
    ''' removes the warranty links on the current warrantable item if not deleting it and if this is the 
    ''' last warrantable SKU linked to the warranty, then this process will also delete the warranty line
    ''' </summary>
    ''' <param name="dgItem">the datagrid for the warrantable sku to have it's warranty link removed</param>
    ''' <param name="delInProg">true indicates that the warrantable is being deleted and no point in 
    '''    searching to remove the link on the corresponding datarow right before deleting it; false 
    '''    indicates the warrantable line is not being deleted, so remove the warranty link</param>
    Protected Sub RemoveWarrLink(ByVal dgItem As DataGridItem, ByVal delInProg As Boolean)
        Dim currWarrLnNum As Integer = 0

        ' remove link to warranty line from current warrantable line
        If dgItem.Cells(RelLnGrid.WARR_ROW_LINK).Text.isNotEmpty Then
            currWarrLnNum = CInt(dgItem.Cells(RelLnGrid.WARR_ROW_LINK).Text)

            If currWarrLnNum > 0 Then
                Dim hasOtherLinks As Boolean = False

                If SysPms.isOne2ManyWarr Then
                    'see how many warrantables are attached to this warranty line num
                    Dim linkedLines As ArrayList = GetLinkedWarrLnNums(currWarrLnNum)

                    'this list includes the warrantable line being unchecked, that is why check if count > 1
                    hasOtherLinks = (linkedLines.Count > 1)
                End If

                ' if the warrantable line is being deleted, then don't bother to clear the links on the row
                ' if clear the link, then not available for counting > 1 so clear after counting
                If Not delInProg Then
                    'find the warrantied row in the dataset and remove the warranty link from it;
                    UpdtLinks2Warrable("", "", dgItem.Cells(RelLnGrid.LN_NUM).Text)
                End If

                ' if the warranty is linked to no other lines on this order, then delete
                If (Not hasOtherLinks) Then
                    ' remove the warranty line
                    deleteRow(currWarrLnNum)
                    lbl_warning.Text = "Removing the referenced warranty SKU " & dgItem.Cells(RelLnGrid.WARR_ITM_LINK).Text   ' TODO - could add line # but not NOW 'sku x on line y
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Executes when the 'warranty' check box in the item details tab grid is checked or unchecked
    '''    This is always a warrantable SKU line
    ''' </summary>
    Protected Sub ChkBoxWarr_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim cb1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(cb1.NamingContainer, DataGridItem)
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If
        If cb1.Checked = True Then
            Dim txt_qty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
            ' need to check the one to one or one to many warr syspm, pop-up the warranty window & update links
            If SysPms.isOne2ManyWarr OrElse 1 = CInt(txt_qty.Text) Then
                SessVar.discReCalc.needToReCalc = True
                'Prepares data for later use in discount recalculation (if applicable)
                SetupForDiscReCalc()  ' CheckWarrClicked for adding a warranty to a line and is new warranty line
                ' send warranty page for user selection
                If SalesUtils.hasWarranty(dgItem.Cells(RelLnGrid.ITM_CD).Text) Then
                    ShowWarrantyPopup(dgItem.Cells(RelLnGrid.LN_NUM).Text)
                End If
            Else
                lbl_warning.Text = "Quantity must equal 1 when a warranty is attached to a SKU."
                cb1.Checked = False
            End If
        Else  ' remove warranty link 
            RemoveWarrLink(dgItem, False)
        End If

        bindgrid()
        calculate_total()
    End Sub

    ''' <summary>
    ''' Finds out if there are any dropped SKUs in the relationship/sales order represented by the ID
    ''' passed in.   If there are any, a comma delimited string is returned with the dropped SKUs
    ''' </summary>
    ''' <param name="id">the Relationship number or the SessionId to check for dropped SKUs</param>
    ''' <returns>a comma delimited string with all the dropped SKUs or an empty string if none</returns>
    Private Function CheckForDroppedSkus(ByVal id As String, Optional isRelationship As Boolean = True) As String

        Dim droppedSkuMsg As String = String.Empty
        Dim droppedSkuResponse As DroppedSkuResponseDtc = If(isRelationship,
                                                             theSkuBiz.GetDroppedItemsForRelationship(id),
                                                             theSkuBiz.GetDroppedItemsForSession(id))
        If (droppedSkuResponse.prevSaleCount = 1) Then
            droppedSkuMsg = String.Format(Resources.POSMessages.MSG0018, droppedSkuResponse.prevSaleSkus)

        ElseIf (droppedSkuResponse.prevSaleCount > 1) Then
            droppedSkuMsg = String.Format(Resources.POSMessages.MSG0016, droppedSkuResponse.prevSaleSkus)

        End If

        Return droppedSkuMsg
    End Function

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_del_doc_num.Text = ""
        txt_wr_dt.Text = ""
        txt_f_name.Text = ""
        txt_l_name.Text = ""
        txt_Cust_cd.Text = ""
        txt_follow_dt.Text = ""
        txt_appt_time.Text = ""
        txt_addr1.Text = ""
        txt_addr2.Text = ""
        txt_h_phone.Text = ""
        txt_b_phone.Text = ""
        txt_city.Text = ""
        cbo_State.SelectedValue = ""
        txt_zip.Text = ""
        txt_email.Text = ""
        txt_slsp1_init.Text = ""
        lbl_slsp1_cd.Text = ""
        lblTax.Text = "0.00"
        lblDelivery.Text = "0.00"
        txt_del_doc_num.Enabled = True
        txt_wr_dt.Enabled = True
        txt_follow_dt.Enabled = True
        txt_store.Enabled = True
        txt_Cust_cd.Enabled = True
        txt_slsp1_init.Enabled = True
        txt_f_name.Enabled = True
        txt_l_name.Enabled = True
        txt_addr1.Enabled = True
        txt_addr2.Enabled = True
        txt_h_phone.Enabled = True
        txt_b_phone.Enabled = True
        txt_city.Enabled = True
        cbo_State.Enabled = True
        txt_zip.Enabled = True
        img_delivery.AlternateText = ""
        Gridview1.Visible = False
        gridQuoteHistory.Visible = False
        DataGrid1.Visible = False
        txt_del_doc_num.Enabled = True
        btn_Lookup.Visible = True
        btn_Save.Enabled = False
        btn_convert.Enabled = False
        btn_quote.Enabled = False
        btn_copy.Visible = False
        btn_total_margins.Visible = False
        chk_email.Visible = False
        chk_email.Checked = False
        btnConfigureEmail.Visible = False
        Session("REL_LN") = System.DBNull.Value
        Session("REL_LN") = Nothing
        Session("DISC_APPLIED_RELN") = Nothing
        Response.Redirect("Relationship_Maintenance.aspx", False)
    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        Response.Redirect("Relationship_Lookup.aspx?del_doc_num=" & HttpUtility.UrlEncode(txt_del_doc_num.Text.Trim) & "&so_wr_dt=" & lbl_slsp1_cd.Text _
          & "&corp_name=" & HttpUtility.UrlEncode(txt_corp_name.Text) & "&cust_cd=" & HttpUtility.UrlEncode(txt_Cust_cd.Text) & "&f_name=" _
          & HttpUtility.UrlEncode(txt_f_name.Text) & "&l_name=" & HttpUtility.UrlEncode(txt_l_name.Text) & "&h_phone=" & HttpUtility.UrlEncode(txt_h_phone.Text) & "&emp_cd=" & HttpUtility.UrlEncode(Session("emp_cd")) & "&addr1=" _
          & HttpUtility.UrlEncode(txt_addr1.Text) & "&bphone=" & HttpUtility.UrlEncode(txt_b_phone.Text) & "&addr2=" & HttpUtility.UrlEncode(txt_addr2.Text) & "&slsp1=" & HttpUtility.UrlEncode(lbl_slsp1_cd.Text) _
          & "&city=" & HttpUtility.UrlEncode(txt_city.Text) & "&state=" & HttpUtility.UrlEncode(cbo_State.SelectedValue.ToString) & "&zip=" & HttpUtility.UrlEncode(txt_zip.Text) _
          & "&follow_up=" & HttpUtility.UrlEncode(txt_follow_dt.Text) & "&rel_status=" & HttpUtility.UrlEncode(cbo_rel_status.Value.ToString))

    End Sub

    Private Sub Populate_Results()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim ds As DataSet = New DataSet
        Dim DEL_DOC_NUM As String = Request("del_doc_num")
        Dim MyDataReader As OracleDataReader
        Dim oAdp As OracleDataAdapter

        txt_del_doc_num.Enabled = "False"
        btn_Lookup.Visible = False
        ASPxPopupControl1.ContentUrl = "Email_Templates.aspx?ord_tp_cd=CRM&del_doc_num=" & DEL_DOC_NUM

        Dim sql As String = "SELECT REL.*, TAX.DES AS TAX_DES,e1.emp_init AS slsp1_init " &
                            "FROM RELATIONSHIP REL, TAX_CD TAX, emp e1 " &
                            "WHERE REL.tax_cd = tax.tax_cd (+) " &
                            "AND e1.emp_cd = rel.slsp1 AND REL_NO ='" & DEL_DOC_NUM & "'"

        dbCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
        oAdp.Fill(ds)
        Session("ORIG_RELN") = ds

        '--sets the original header discount (if any) and all discount details; avoid resetting if processing same doc;
        SessVar.discCd = If(IsDBNull(ds.Tables(0).Rows(0)("disc_cd")), String.Empty, ds.Tables(0).Rows(0)("disc_cd"))
        If (SessVar.discCd.isNotEmpty()) Then

            Dim ln2Discs As DataTable = theSalesBiz.GetDiscountsApplied(DEL_DOC_NUM, SessVar.discCd, False, True) ' for SOM populate results from so_ln or so_ln2disc
            Session("DISC_APPLIED_RELN") = ln2Discs
            If SystemUtils.dataTblIsNotEmpty(ln2Discs) Then

                ' sort to find if existing disc or not
                Dim drhdrDisc As DataRow() = ln2Discs.Select("DISC_LEVEL='H'")
                If drhdrDisc.Length > 0 AndAlso SystemUtils.dataRowIsNotEmpty(drhdrDisc(0)) Then

                    SessVar.existHdrDiscCd = drhdrDisc(0)("DISC_CD")
                End If

            End If
        End If

        conn.Open()
        dbCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (MyDataReader.Read()) Then

                Dim isOpenRelationship As Boolean = (AppConstants.Order.STATUS_OPEN = MyDataReader.Item("REL_STATUS").ToString)
                txt_del_doc_num.Enabled = False
                txt_wr_dt.Enabled = False
                txt_store.Enabled = False
                txt_Cust_cd.Enabled = False
                txt_slsp1_init.Enabled = False
                cbo_rel_status.Enabled = isOpenRelationship
                btn_convert.Enabled = isOpenRelationship
                btn_Save.Enabled = isOpenRelationship
                txt_follow_dt.Enabled = isOpenRelationship
                txt_corp_name.Enabled = isOpenRelationship
                txt_f_name.Enabled = isOpenRelationship
                txt_l_name.Enabled = isOpenRelationship
                txt_email.Enabled = isOpenRelationship
                txt_addr1.Enabled = isOpenRelationship
                txt_addr2.Enabled = isOpenRelationship
                txt_h_phone.Enabled = isOpenRelationship
                txt_b_phone.Enabled = isOpenRelationship
                txt_city.Enabled = isOpenRelationship
                cbo_State.Enabled = isOpenRelationship
                txt_zip.Enabled = isOpenRelationship
                cbo_call_type.Enabled = isOpenRelationship
                txt_appt_time.Enabled = isOpenRelationship
                cbo_tax_exempt.Enabled = isOpenRelationship
                tblTaxCd.Visible = isOpenRelationship

                If Len(MyDataReader.Item("APPT_TIME").ToString) > 5 Then
                    txt_appt_time.Text = Left(MyDataReader.Item("APPT_TIME").ToString, Len(MyDataReader.Item("APPT_TIME").ToString) - 2)
                    cbo_am_or_pm.SelectedItem = cbo_am_or_pm.Items.FindByValue(Right(MyDataReader.Item("APPT_TIME").ToString, 2))
                End If
                If MyDataReader.Item("APPT_TYPE").ToString & "" <> "" Then
                    cbo_call_type.SelectedItem = cbo_call_type.Items.FindByValue(MyDataReader.Item("APPT_TYPE").ToString)
                End If
                If MyDataReader.Item("TET_CD").ToString & "" <> "" Then
                    cbo_tax_exempt.Value = MyDataReader.Item("TET_CD").ToString
                    lbl_exempt_id.Text = MyDataReader.Item("EXEMPT_ID").ToString
                    txt_exempt_id.Text = MyDataReader.Item("EXEMPT_ID").ToString
                    lbl_tax_rate.Text = ""
                    lbl_tax_cd.Text = ""
                Else
                    lbl_tax_rate.Text = MyDataReader.Item("TAX_RATE").ToString
                    lbl_tax_cd.Text = MyDataReader.Item("TAX_CD").ToString
                    lnkModifyTax.Text = If(String.IsNullOrEmpty(MyDataReader.Item("TAX_CD")), If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString), MyDataReader.Item("TAX_CD").ToString)
                    txtTaxCd.Value = MyDataReader.Item("TAX_CD").ToString & " - " & MyDataReader.Item("TAX_DES").ToString
                End If
                txt_del_doc_num.Text = MyDataReader.Item("REL_NO").ToString
                txt_wr_dt.Text = MyDataReader.Item("WR_DT").ToString
                If IsDate(txt_wr_dt.Text.ToString) Then
                    txt_wr_dt.Text = FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate)
                End If
                txt_store.Text = MyDataReader.Item("STORE_CD").ToString.Trim
                txt_Cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                txt_follow_dt.Text = MyDataReader.Item("FOLLOW_UP_DT").ToString
                If IsDate(txt_follow_dt.Text.ToString) Then
                    txt_follow_dt.Text = FormatDateTime(txt_follow_dt.Text, DateFormat.ShortDate)
                End If
                txt_slsp1_init.Text = MyDataReader.Item("slsp1_init").ToString  'this is the slspn initials
                lbl_slsp1_cd.Text = MyDataReader.Item("SLSP1").ToString
                img_delivery.AlternateText = MyDataReader.Item("P_D").ToString
                Session("PD") = MyDataReader.Item("P_D").ToString
                If IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                    lblTax.Text = FormatNumber(CDbl(MyDataReader.Item("TAX_CHG").ToString), 2)
                End If
                If IsNumeric(MyDataReader.Item("DEL_CHG").ToString) Then
                    lblDelivery.Text = FormatNumber(CDbl(MyDataReader.Item("DEL_CHG").ToString), 2)
                End If
                If IsNumeric(MyDataReader.Item("PROSPECT_TOTAL").ToString) Then
                    lbltotal.Text = FormatCurrency(CDbl(MyDataReader.Item("PROSPECT_TOTAL")), 2)
                End If
                txt_email.Text = MyDataReader.Item("EMAIL_ADDR").ToString
                If String.IsNullOrEmpty(txt_email.Text.ToString) Then
                    txt_email.BackColor = Color.LightYellow
                End If
                txt_corp_name.Text = MyDataReader.Item("CORP_NAME").ToString
                txt_f_name.Text = MyDataReader.Item("FNAME").ToString
                txt_l_name.Text = MyDataReader.Item("LNAME").ToString
                lbl_PD.Text = MyDataReader.Item("P_D").ToString
                txt_addr1.Text = MyDataReader.Item("ADDR1").ToString
                txt_addr2.Text = MyDataReader.Item("ADDR2").ToString
                txt_h_phone.Text = MyDataReader.Item("HPHONE").ToString
                txt_b_phone.Text = MyDataReader.Item("BPHONE").ToString
                txt_city.Text = MyDataReader.Item("CITY").ToString
                cbo_State.SelectedValue = MyDataReader.Item("ST").ToString
                txt_zip.Text = MyDataReader.Item("ZIP").ToString
                cbo_rel_status.Value = MyDataReader.Item("REL_STATUS").ToString
                lbl_status_dt.Text = MyDataReader.Item("REL_STATUS_DT").ToString
                Session("ZONE_CD") = MyDataReader.Item("ZONE_CD").ToString
                Session("ZONE_ZIP_CD") = MyDataReader("ZIP").ToString

                '***** set if store is ARS enabeld
                Dim store As StoreData = theSalesBiz.GetStoreInfo(txt_store.Text)
                Session("STORE_ENABLE_ARS") = store.enableARS
                ' Daniela comment Feb 10 as per Dave request
                'btn_copy.Visible = True
                btn_Save.Enabled = True
                'btn_convert.Visible = True
                'btn_quote.Visible = True
                'btn_copy.Visible = True
                chk_email.Visible = True
                btnConfigureEmail.Visible = True
                ASPxPopupControl2.ContentUrl = "Relationship_Copy.aspx?del_doc_num=" & DEL_DOC_NUM & "&cust_cd=" & txt_Cust_cd.Text

                Dim warning_list As String = ""
                sql = "SELECT DES FROM CUST_WARN, CUST2CUST_WARN WHERE CUST_WARN.CUST_WARN_CD=CUST2CUST_WARN.CUST_WARN_CD AND CUST2CUST_WARN.CUST_CD=:CUST_CD"
                dbCommand.CommandText = sql
                dbCommand.Parameters.Clear()
                dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbCommand.Parameters(":CUST_CD").Value = txt_Cust_cd.Text

                MyDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                Do While MyDataReader.Read()
                    warning_list = warning_list & "- " & MyDataReader.Item("des") & vbCrLf
                Loop
                If warning_list & "" <> "" Then
                    img_warn.Visible = True
                    lbl_cust_warn.Visible = True
                    lbl_cust_warn.Text = "PLEASE REVIEW"
                    img_warn.ToolTip = warning_list
                Else
                    lbl_cust_warn.Visible = False
                    img_warn.Visible = False
                End If

                If String.IsNullOrEmpty(lbl_tax_rate.Text) And Not String.IsNullOrEmpty(txt_zip.Text) Then
                    determine_tax(txt_zip.Text)
                End If

                bindgrid()

                Try
                    Dim footerIndex As Integer = Gridview1.Controls(0).Controls.Count - 1
                    Dim hpl_find_merch As HyperLink = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("hpl_find_merch"), HyperLink)
                    hpl_find_merch.NavigateUrl = "order.aspx?referrer=Relationship_Maintenance.aspx&del_doc_num=" & txt_del_doc_num.Text & "&query_returned=Y"

                    If Request("SKU") & "" <> "" Then
                        Dim txt_new_itm_cd As TextBox = DirectCast(Gridview1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
                        txt_new_itm_cd.Text = Request("SKU")
                        Add_SKU_To_Order(txt_new_itm_cd, EventArgs.Empty)
                        ASPxPageControl1.ActiveTabIndex = 1
                    End If
                Catch
                End Try

                DoAfterBindgrid()
                calculate_total()
            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            SaveRelationship()
        End If
    End Sub

    Public Sub SaveRelationship()
        Try
            Dim objsql As OracleCommand
            Dim sql, REL_NO As String

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            REL_NO = txt_del_doc_num.Text.ToString
            sql = "UPDATE RELATIONSHIP SET "
            If Not String.IsNullOrEmpty(txt_appt_time.Text.ToString) Then
                sql = sql & "APPT_TIME='" & txt_appt_time.Text.ToString & cbo_am_or_pm.SelectedItem.ToString & "',"
                If cbo_call_type.Value.ToString & "" <> "" Then
                    sql = sql & "APPT_TYPE='" & cbo_call_type.Value.ToString & "',"
                End If
            Else
                sql = sql & "APPT_TIME=NULL,"
                sql = sql & "APPT_TYPE=NULL,"
            End If
            If cbo_rel_status.Enabled = True And cbo_rel_status.Value.ToString = "F" Then
                sql = sql & "REL_STATUS='" & cbo_rel_status.Value.ToString & "',"
            End If
            If Not String.IsNullOrEmpty(txt_email.Text.ToString) Then
                sql = sql & "EMAIL_ADDR='" & txt_email.Text.ToString & "',"
            Else
                sql = sql & "EMAIL_ADDR=NULL,"
            End If
            If cbo_tax_exempt.SelectedIndex = 0 Then
                If lbl_tax_rate.Text.ToString & "" <> "" Then
                    sql = sql & "TAX_RATE=" & lbl_tax_rate.Text.ToString & ","
                End If
                If lbl_tax_cd.Text & "" <> "" Then
                    sql = sql & "TAX_CD='" & lbl_tax_cd.Text & "',"
                End If
                sql = sql & "TET_CD=NULL,"
                sql = sql & "EXEMPT_ID=NULL,"
            Else
                If cbo_tax_exempt.Value & "" <> "" Then
                    sql = sql & "TET_CD='" & cbo_tax_exempt.Value & "',"
                End If
                If txt_exempt_id.Text & "" <> "" Then
                    sql = sql & "EXEMPT_ID='" & txt_exempt_id.Text & "',"
                End If
                sql = sql & "TAX_RATE=NULL,"
                sql = sql & "TAX_CD=NULL,"
            End If

            If (SessVar.discCd.isEmpty) Then
                sql = sql & "DISC_CD = NULL ,"
            Else
                sql = sql & "DISC_CD = '" & SessVar.discCd & "',  "
            End If

            sql = sql & "STORE_CD='" & txt_store.Text.ToString & "',"
            If IsDate(txt_follow_dt.Text.ToString) Then
                sql = sql & "FOLLOW_UP_DT=TO_DATE('" & txt_follow_dt.Text.ToString & "','mm/dd/RRRR'),"
            End If
            sql = sql & "SLSP1='" & lbl_slsp1_cd.Text.ToString & "',"
            sql = sql & "CORP_NAME='" & Replace(txt_corp_name.Text.ToString, "'", "''") & "',"
            sql = sql & "FNAME='" & Replace(txt_f_name.Text.ToString, "'", "''") & "',"
            sql = sql & "LNAME='" & Replace(txt_l_name.Text.ToString, "'", "''") & "',"
            sql = sql & "ADDR1='" & Replace(txt_addr1.Text.ToString, "'", "''") & "',"
            sql = sql & "ADDR2='" & Replace(txt_addr2.Text.ToString, "'", "''") & "',"
            sql = sql & "HPHONE='" & StringUtils.FormatPhoneNumber(txt_h_phone.Text.ToString) & "',"
            sql = sql & "BPHONE='" & StringUtils.FormatPhoneNumber(txt_b_phone.Text.ToString) & "',"
            sql = sql & "CITY='" & Replace(txt_city.Text.ToString, "'", "''") & "',"
            sql = sql & "ST='" & Replace(cbo_State.SelectedValue.ToString, "'", "''") & "',"
            sql = sql & "ZIP='" & txt_zip.Text.ToString & "', "
            sql = sql & "PROSPECT_TOTAL=" & CDbl(lbltotal.Text.ToString) & ", "
            sql = sql & "TAX_CHG=" & CDbl(lblTax.Text.ToString) & " "
            sql = sql & "WHERE REL_NO='" & txt_del_doc_num.Text.ToString & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.ExecuteNonQuery()

            Dim ds As DataSet
            Dim curr_ln As Double = 0

            If Not Session("REL_LN") Is Nothing Then

                ds = Session("REL_LN")

                Dim sql2 As String
                Dim dt As DataTable = ds.Tables(0)
                Dim dr As DataRow

                For Each dr In ds.Tables(0).Rows

                    If dr.RowState = DataRowState.Added Then

                        sql = "INSERT INTO RELATIONSHIP_LINES (REL_NO, LINE, ITM_CD, QTY, RET_PRC, TREATED, VE_CD, VSN, "
                        sql = sql & "DES, ITM_TP_CD, PRC_CHG_APP_CD, TREATABLE, WARR_ITM_LINK, WARR_ROW_LINK, PACKAGE_PARENT,DISC_AMT  "
                        sql2 = " VALUES('" & txt_del_doc_num.Text.ToString & "'," & dr("LINE") & ",'" & dr("ITM_CD") & "',"
                        sql2 = sql2 & dr("QTY") & "," & dr("RET_PRC") & ",'"
                        sql2 = sql2 & dr("TREATED").ToString & "','" & dr("VE_CD").ToString & "','"
                        sql2 = sql2 & Replace(dr("VSN").ToString, "'", "''") & "','" & Replace(dr("DES").ToString, "'", "''") & "','"
                        sql2 = sql2 & dr("ITM_TP_CD").ToString & "','" & dr("PRC_CHG_APP_CD").ToString & "','"
                        sql2 = sql2 & dr("TREATABLE").ToString & "', '" & dr("WARR_ITM_LINK").ToString

                        If dr("WARR_ROW_LINK").ToString.isEmpty Then
                            sql2 = sql2 & "', '' "
                        Else
                            sql2 = sql2 & "', '" & dr("WARR_ROW_LINK").ToString & "'"
                        End If

                        If IsDBNull(dr("PACKAGE_PARENT")) Then
                            sql2 = sql2 & ", '' "
                        Else
                            sql2 = sql2 & ", '" & dr("PACKAGE_PARENT").ToString & "'"
                        End If

                        If IsDBNull(dr("disc_amt")) Then
                            sql2 = sql2 & ", 0.0 "
                        Else
                            sql2 = sql2 & ", '" & dr("disc_amt").ToString & "'"
                        End If

                        sql = sql & ")"
                        sql2 = sql2 & ")"
                        sql = sql & sql2

                        sql = UCase(sql)
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()

                    ElseIf dr.RowState = DataRowState.Modified Then
                        sql = "UPDATE RELATIONSHIP_LINES "
                        sql = sql & "SET ITM_CD='" & dr("ITM_CD").ToString & "', "
                        sql = sql & "QTY=" & dr("QTY") & ", "
                        sql = sql & "RET_PRC=" & dr("RET_PRC") & ", "
                        'sql = sql & "ORIG_PRC=" & dr("RET_PRC") & ", "
                        sql = sql & "TREATED='" & dr("TREATED").ToString & "', "
                        sql = sql & "VE_CD='" & dr("VE_CD").ToString & "', "
                        sql = sql & "VSN='" & Replace(dr("VSN").ToString, "'", "''") & "', "
                        sql = sql & "DES='" & Replace(dr("DES").ToString, "'", "''") & "', "
                        sql = sql & "ITM_TP_CD='" & dr("ITM_TP_CD").ToString & "', "
                        sql = sql & "TREATABLE='" & dr("TREATABLE").ToString & "', "
                        sql = sql & "WARR_ITM_LINK='" & dr("WARR_ITM_LINK").ToString & "', "
                        sql = sql & "WARR_ROW_LINK='" & dr("WARR_ROW_LINK").ToString & "', "
                        sql = sql & "PRC_CHG_APP_CD='" & dr("PRC_CHG_APP_CD").ToString & "', "
                        sql = sql & "PACKAGE_PARENT ='" & dr("PACKAGE_PARENT").ToString & "', "
                        sql = sql & "DISC_AMT = '" & dr("DISC_AMT").ToString & "' "
                        sql = sql & "WHERE REL_NO='" & txt_del_doc_num.Text & "' "
                        sql = sql & "AND LINE=" & dr("LINE", DataRowVersion.Original)

                        sql = UCase(sql)
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()

                    ElseIf dr.RowState = DataRowState.Deleted Then
                        curr_ln = dr("LINE", DataRowVersion.Original)
                        sql = "DELETE FROM RELATIONSHIP_LN2DISC "
                        sql = sql & "WHERE REL_NO=:REL_NO "
                        sql = sql & "AND LINE=:LINE"
                        sql = UCase(sql)

                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.Parameters.Add(":REL_NO", OracleType.VarChar)
                        objsql.Parameters.Add(":LINE", OracleType.VarChar)
                        objsql.Parameters(":REL_NO").Value = txt_del_doc_num.Text.Trim
                        objsql.Parameters(":LINE").Value = curr_ln
                        objsql.ExecuteNonQuery()

                        sql = "DELETE FROM RELATIONSHIP_LINES "
                        sql = sql & "WHERE REL_NO='" & txt_del_doc_num.Text & "' "
                        sql = sql & "AND LINE=" & curr_ln

                        sql = UCase(sql)
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()

                        sql = "DELETE FROM RELATIONSHIP_LINES_DESC "
                        sql = sql & "WHERE REL_NO='" & txt_del_doc_num.Text & "' "
                        sql = sql & "AND LINE='" & curr_ln & "'"

                        sql = UCase(sql)
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()
                    End If
                Next
                'Updating data to the RELATIONSHIP_LN2DISC
                If SysPms.allowMultDisc Then
                    If Not Session("DISC_APPLIED_RELN") Is Nothing Then
                        Dim relnDt As DataTable = Session("DISC_APPLIED_RELN")
                        theSalesBiz.ProcessRelnDiscounts(REL_NO, relnDt)
                    End If
                End If
            End If
            conn.Close()

            Session("REL_LN") = System.DBNull.Value
            Session("REL_LN") = Nothing
        Catch
            Throw
        End Try
        bindgrid()
        calculate_total()
        ' lbl_warning.Text = "Relationship saved"
        lbl_warning.Text = Resources.LibResources.Label772

    End Sub

    ''' <summary>
    ''' deletes the row with the line number from the dataset (marks as deleted)
    ''' </summary>
    ''' <param name="lnNum">line number of row to delete</param>
    Protected Sub deleteRow(ByVal lnNum As String)
        Dim relLnsDatSet As DataSet = Session("REL_LN")
        Dim relLns As DataTable = relLnsDatSet.Tables(0)

        If lnNum & "" <> "" Then
            Try
                Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                conn.Open()

                For Each dr In relLns.Rows
                    If dr.RowState <> DataRowState.Deleted AndAlso dr("LINE") = lnNum Then
                        dr.Delete()
                        'relLnsDatSet.AcceptChanges()
                        ' TODO - note that the above marks the row as deleted but the below actually updates the db so if
                        '    the user exits without save, then the above row remains but the comments in relationship_lines_desc is gone
                        '    And it does delete them during the save too so I think this shouldn't be here but I found no customer jiras
                        '    as of 06-MAR-14 so I'm not going to remove it because I'm just not sure
                        With cmdDeleteItems
                            .Connection = conn
                            .CommandText = "delete from relationship_lines_desc where rel_no = :DEL_DOC and line = :LINE_ITM"
                            .Parameters.Add(":DEL_DOC", OracleType.VarChar)
                            .Parameters(":DEL_DOC").Value = txt_del_doc_num.Text
                            .Parameters.Add(":LINE_ITM", OracleType.VarChar)
                            .Parameters(":LINE_ITM").Value = lnNum
                        End With

                        cmdDeleteItems.ExecuteNonQuery()
                        cmdDeleteItems.Dispose()
                        cmdDeleteItems = Nothing

                        Exit For
                    End If
                Next
                conn.Close()
            Catch

            End Try
            'relLnsDatSet.AcceptChanges()
            Session("REL_LN") = relLnsDatSet
        End If
    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand
        ' if the relationship status is open
        Dim isDiscountApplied As Boolean = False
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True AndAlso (popupMgrOverride.ShowOnPageLoad = True)) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If
        If cbo_rel_status.Value = "O" Then
            Dim delLnNum As String = Gridview1.DataKeys(e.Item.ItemIndex)

            ' if warranty, remove links from warrantables; if warrantable, if linked to warranty and is last on warranty, then delete warranty.
            If e.Item.Cells(RelLnGrid.ITM_TP_CD).Text = SkuUtils.ITM_TP_WAR Then
                removeLinks2Warr(delLnNum)
            ElseIf "Y".Equals(e.Item.Cells(RelLnGrid.WARRANTABLE).Text) AndAlso e.Item.Cells(RelLnGrid.WARR_ROW_LINK).Text <> "" Then
                RemoveWarrLink(e.Item, True)
                isDiscountApplied = True
            End If

            ' find and mark the row as deleted
            deleteRow(delLnNum)
            Dim relLnsDatSet As DataSet = Session("REL_LN")

            If Not IsNothing(relLnsDatSet) AndAlso relLnsDatSet.Tables(0).Rows.Count > 0 Then
                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(relLnsDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void)
            End If

            Session("REL_LN") = relLnsDatSet
            bindgrid()
            calculate_total()
        Else
            lbl_warning.Text = "You cannot change Converted or Finalized Relationships."
        End If
    End Sub

    ''' <summary>
    ''' create an object of tax percentages by item type as the taxes are applied by tax date and tax code; if the session variable object
    '''   already exists and was created for thee input tax date and tax code, there is no reason to re-acquire - save some db hits
    ''' </summary>
    Private Function SetTxPcts(ByVal tax_dt As String, ByVal tax_cd As String) As TaxUtils.taxPcts

        Dim txPcts As TaxUtils.taxPcts
        If tax_cd.isNotEmpty AndAlso tax_dt.isNotEmpty AndAlso IsDate(tax_dt) Then

            If IsNothing(Session("taxPcts")) Then

                txPcts = TaxUtils.SetTaxPcts(tax_dt, tax_cd)
                Session("taxPcts") = txPcts

            Else
                txPcts = Session("taxPcts")

                If txPcts.taxCd.isNotEmpty AndAlso txPcts.taxCd = tax_cd AndAlso
                     (Not IsNothing(txPcts.taxDt)) AndAlso txPcts.taxDt = CDate(tax_dt) Then

                    txPcts = Session("taxPcts")

                Else
                    txPcts = TaxUtils.SetTaxPcts(tax_dt, tax_cd)
                    Session("taxPcts") = txPcts
                End If
            End If
        End If

        Return txPcts
    End Function

    Private Function initTempItmReq() As HBCG_Utils.CreateTempItmReq

        Dim tempItmReq As HBCG_Utils.CreateTempItmReq = New HBCG_Utils.CreateTempItmReq()

        tempItmReq.sessId = Session.SessionID.ToString.Trim
        tempItmReq.empCd = Session("emp_cd")
        tempItmReq.tranDt = Session("tran_dt")
        tempItmReq.defLocCd = Session("loc_cd")
        tempItmReq.newSpecOrd = False
        tempItmReq.takeWith = False
        tempItmReq.warrRowLink = 0
        tempItmReq.warrItmLink = ""
        tempItmReq.soHdrInf.isCashNCarry = "FALSE"
        tempItmReq.soHdrInf.writtenDt = Session("tran_dt")
        tempItmReq.soHdrInf.writtenStoreCd = Session("store_cd")
        tempItmReq.soHdrInf.custTpPrcCd = IIf(IsNothing(Session("cust_tp_prc_cd")), "", Session("cust_tp_prc_cd"))
        tempItmReq.soHdrInf.orderTpCd = AppConstants.Order.TYPE_SAL
        tempItmReq.soHdrInf.slsp1 = Session("slsp1")
        tempItmReq.soHdrInf.slsp2 = ""
        tempItmReq.soHdrInf.slspPct1 = Session("pct_1")
        tempItmReq.soHdrInf.slspPct2 = 0
        tempItmReq.soHdrInf.taxExemptCd = IIf(IsNothing(Session("tet_cd")), "", Session("tet_cd"))
        tempItmReq.soHdrInf.taxCd = lbl_tax_cd.Text
        tempItmReq.soHdrInf.taxRates = SetTxPcts(tempItmReq.soHdrInf.writtenDt, tempItmReq.soHdrInf.taxCd).itmTpPcts

        Return tempItmReq

    End Function

    Protected Sub btn_convert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_convert.Click
        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            Session("tran_dt") = FormatDateTime(Today.Date.ToString, 2)
            Dim dictCurrentPrice As Dictionary(Of String, Double) = theSkuBiz.CheckPriceChange(Request("del_doc_num"), SessVar.homeStoreCd, SessVar.custTpPrcCd, Session("tran_dt"))
            If Not IsNothing(dictCurrentPrice) AndAlso dictCurrentPrice.Count > 0 Then
                Session("dictCurrentPrice") = dictCurrentPrice
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                Dim errorMsg As String = String.Format(Resources.POSMessages.MSG0048, String.Join(",", dictCurrentPrice.Select(Function(pair) String.Format("{0}", pair.Key)).ToArray()))
                ucMsgPopup.DisplayConfirmMsg(errorMsg, "PRICE")
                Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
                msgPopup.ShowOnPageLoad = True
            Else
                ConvertToOrder()
            End If
        End If
    End Sub
    Protected Sub ConvertToOrder()
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim emp_cd, fname, lname, store_cd, IPAD, co_cd As String
        Dim deldocnum As String = Request("del_doc_num")

        IPAD = Session("IPAD")
        emp_cd = Session("EMP_CD")
        fname = Session("EMP_FNAME")
        lname = Session("EMP_LNAME")
        store_cd = Session("HOME_STORE_CD")
        co_cd = Session("CO_CD")
        Dim dictCurrentPrice As Dictionary(Of String, Double) = Nothing
        If Not IsNothing(Session("dictCurrentPrice")) Then
            dictCurrentPrice = CType(Session("dictCurrentPrice"), Dictionary(Of String, Double))
        End If

        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
            cmdDeleteItems.Dispose()
            cmdDeleteItems = Nothing
            cmdDeleteItems = DisposablesManager.BuildOracleCommand

            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from temp_itm where session_id='" & Session.SessionID.ToString.Trim & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
            cmdDeleteItems.Dispose()
            cmdDeleteItems = Nothing
            cmdDeleteItems = DisposablesManager.BuildOracleCommand

            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from cust_info where session_id='" & Session.SessionID.ToString.Trim & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
        End If
        ' Daniela save clientip, emp_init and super user
        Dim client_IP As String = Session("clientip")
        Dim empInit As String = Session("EMP_INIT")
        Dim supUser As String = Session("str_sup_user_flag")
        Dim coGrpCd As String = Session("str_co_grp_cd")
        Dim userType As String = Session("USER_TYPE")
        Session.Clear()
        Session("clientip") = client_IP
        Session("str_sup_user_flag") = supUser ' Daniela
        Session("str_co_grp_cd") = coGrpCd ' Daniela
        Session("USER_TYPE") = userType ' Daniela
        Session("EMP_INIT") = empInit
        Session("IPAD") = IPAD
        Session("EMP_CD") = emp_cd
        Session("CO_CD") = co_cd
        Session("EMP_FNAME") = fname
        Session("EMP_LNAME") = lname
        Session("HOME_STORE_CD") = store_cd
        Session("store_cd") = store_cd
        Session("slsp1") = lbl_slsp1_cd.Text.ToString
        Session("pct_1") = "100"
        Session("PD") = img_delivery.AlternateText.ToString
        Session("LNAME") = txt_l_name.Text.ToString
        Session("ord_tp_cd") = "SAL"
        Session("tran_dt") = FormatDateTime(Today.Date.ToString, 2)
        Session("CUST_CD") = txt_Cust_cd.Text.ToString
        Session("cust_lname") = txt_l_name.Text.ToString
        Session("del_chg") = lblDelivery.Text.ToString
        Session("tet_cd") = cbo_tax_exempt.Value
        If Session("tet_cd") & "" <> "" Then Session("no_tax") = "True"
        Session("exempt_id") = txt_exempt_id.Text
        Session("tax_rate") = lbl_tax_rate.Text.ToString
        Session("tax_cd") = lbl_tax_cd.Text
        '** The 'manual' tax cd is the tax, before the user co
        Session("MANUAL_TAX_CD") = Nothing
        Session("One_Time_Conversion") = "TRUE"
        Session("grand_total") = lbltotal.Text.ToString
        Session("sub_total") = CDbl(lbltotal.Text.ToString) - (CDbl(lblTax.Text.ToString) + CDbl(lblDelivery.Text.ToString))
        Session("Converted_REL_NO") = txt_del_doc_num.Text
        Session("priceChangeDictionary") = Nothing
        Dim sql As String
        Dim objsql As OracleCommand
        Dim objsql2 As OracleCommand
        Dim itmDatRdr As OracleDataReader

        Dim z As Integer = 1
        Dim ln1RowSeqNum As Integer = 0   ' this is the row_id number column from temp_itm
        Dim ln1RowId As String = ""       ' this is the actual Oracle rowid
        Dim conv_qty As Double = 1
        Dim orig_sql As String = ""
        Dim RelanPriceChange As New List(Of RelationshipLineDetails)
        Dim ln2disc As DataTable
        Dim itmcd As String = String.Empty
        Dim strWhereClause As String = String.Empty
        Dim dtLnsFiltered As DataRow()
        Dim needTorecalDiscount As Boolean = False

        Try
            Dim relLns As DataSet = GetRelLns(txt_del_doc_num.Text.ToString)
            ln2disc = theSalesBiz.GetDiscountsApplied(deldocnum, SessVar.discCd, False, True) ' FOR SOM POPULATE RESULTS FROM SO_LN OR SO_LN2DISC
            ln2disc.Columns.Add("ROW_ID")

            Dim pkgParentLine As String = String.Empty
            Dim retprc As Double

            For Each relLn As DataRow In relLns.Tables(0).Rows
                retprc = 0.0
                If Not IsNothing(dictCurrentPrice) AndAlso dictCurrentPrice.Count > 0 AndAlso dictCurrentPrice.ContainsKey(relLn.Item("ITM_CD").ToString) Then
                    retprc = dictCurrentPrice(relLn.Item("ITM_CD").ToString)
                    needTorecalDiscount = True
                    ViewState("needTorecalDiscount") = needTorecalDiscount
                End If
                conv_qty = 1
                z = 1
                Session("itemid") = "NONEENTERED"
                Dim origPrc = relLn.Item("ORIG_PRC")
                Dim manualPrc = 0.0
                If IsDBNull(relLn.Item("MANUAL_PRC")) Then
                    manualPrc = 0.0
                Else
                    manualPrc = relLn.Item("MANUAL_PRC")
                End If

                If IsDBNull(relLn.Item("ORIG_PRC")) OrElse (Not IsDBNull(relLn.Item("ORIG_PRC")) AndAlso relLn.Item("ORIG_PRC") = 0) Then
                    origPrc = relLn.Item("RET_PRC")
                End If
                If IsDBNull(relLn.Item("MANUAL_PRC")) OrElse (Not IsDBNull(relLn.Item("MANUAL_PRC")) AndAlso relLn.Item("MANUAL_PRC") = 0) Then
                    manualPrc = relLn.Item("RET_PRC")
                End If

                orig_sql = "INSERT INTO TEMP_ITM (ITM_CD, QTY, RET_PRC, ORIG_PRC, MANUAL_PRC,TREATED, SESSION_ID, slsp1, slsp1_pct"

                If IsDBNull(relLn("PACKAGE_PARENT")) Then
                    pkgParentLine = String.Empty
                ElseIf Not String.IsNullOrEmpty(pkgParentLine) Then
                    orig_sql = orig_sql & ", PACKAGE_PARENT"
                End If
                orig_sql = orig_sql & ", DISC_AMT"

                orig_sql = orig_sql & ") VALUES('" & relLn.Item("ITM_CD").ToString & "'"

                If retprc <> 0.0 AndAlso retprc <> relLn.Item("RET_PRC_DISC") Then
                    orig_sql = orig_sql & "," & relLn.Item("QTY").ToString & "," & retprc & "," & origPrc & "," & retprc & ",'"
                Else
                    orig_sql = orig_sql & "," & relLn.Item("QTY").ToString & "," & relLn.Item("RET_PRC") & "," & origPrc & "," & manualPrc & ",'"
                End If

                orig_sql = orig_sql & relLn.Item("TREATED").ToString & "','" & Session.SessionID.ToString() & "', '" & Session("slsp1") & "', 100 "

                If Not IsDBNull(relLn("PACKAGE_PARENT")) AndAlso Not String.IsNullOrEmpty(pkgParentLine) Then
                    orig_sql = orig_sql & "," & pkgParentLine
                End If

                Dim discAmt = relLn.Item("DISC_AMT").ToString
                If IsDBNull(relLn.Item("DISC_AMT")) Then
                    orig_sql = orig_sql & "," & 0.0
                Else
                    If Not retprc = 0.0 AndAlso Not retprc = relLn.Item("RET_PRC_DISC") Then
                        orig_sql = orig_sql & "," & 0.0
                    Else
                        orig_sql = orig_sql & "," & relLn.Item("DISC_AMT").ToString
                    End If
                End If

                orig_sql = orig_sql & ") RETURNING ROW_ID, rowid INTO :INSERTED_ROWID, :row_id"
                objsql = DisposablesManager.BuildOracleCommand(orig_sql, conn)
                objsql.Parameters.Add(":INSERTED_ROWID", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                objsql.Parameters(":INSERTED_ROWID").Value = System.DBNull.Value
                objsql.Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                objsql.Parameters(":row_id").Value = System.DBNull.Value
                objsql.ExecuteNonQuery()

                ln1RowId = objsql.Parameters(":row_id").Value.ToString
                'Added the same if block because rowid will be initialized after the insert
                If retprc <> 0.0 AndAlso retprc <> relLn.Item("RET_PRC_DISC") Then
                    Dim RelationshipLine As New RelationshipLineDetails()
                    RelationshipLine.ItemCD = relLn.Item("ITM_CD").ToString
                    RelationshipLine.Price = relLn.Item("RET_PRC")
                    RelationshipLine.RowId = Convert.ToInt64(objsql.Parameters(":INSERTED_ROWID").Value)
                    RelanPriceChange.Add(RelationshipLine)
                End If

                If Not ln2disc Is Nothing Then
                    strWhereClause = ("LNSEQ_NUM = '" & relLn.Item("LINE") & "' AND ITM_CD = '" & relLn.Item("ITM_CD") & "'")
                    If ln2disc.Select(strWhereClause).Length > 0 Then
                        dtLnsFiltered = ln2disc.Select(strWhereClause)
                        For Each discln As DataRow In dtLnsFiltered
                            If discln.Item("LNSEQ_NUM") = relLn.Item("LINE") AndAlso discln.Item("ITM_CD") = relLn.Item("ITM_CD") Then
                                discln("ROW_ID") = Convert.ToInt64(objsql.Parameters(":INSERTED_ROWID").Value)
                                'SalesUtils.GetTempItmRowSeqNum(ln1RowId)
                            End If
                        Next
                    End If
                End If

                If String.Equals(relLn("ITM_TP_CD").ToString.ToUpper, "PKG") Then
                    pkgParentLine = objsql.Parameters(":INSERTED_ROWID").Value.ToString
                End If

                objsql.Parameters.Clear()

                ' extract the TEMP_ITM.ROW_ID (not the Oracle rowid) in case need to link warranty
                If relLn("LINE") < 2 Then
                    ln1RowSeqNum = SalesUtils.GetTempItmRowSeqNum(ln1RowId)
                End If
                If IsNumeric(relLn.Item("QTY").ToString) Then
                    conv_qty = CDbl(relLn.Item("QTY").ToString)
                End If

                sql = "SELECT * FROM ITM where ITM_CD='" & relLn.Item("ITM_CD").ToString & "'"
                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

                    Do While itmDatRdr.Read()

                        If IsSerialType(itmDatRdr.Item("SERIAL_TP").ToString) Then
                            If conv_qty > 1 Then
                                For z = 1 To conv_qty - 1
                                    objsql = DisposablesManager.BuildOracleCommand(orig_sql, conn)
                                    objsql.ExecuteNonQuery()
                                Next
                            End If
                        End If
                        sql = "UPDATE TEMP_ITM SET "
                        sql = sql & "VE_CD ='" & itmDatRdr.Item("VE_CD").ToString & "', "
                        If Determine_Taxability(itmDatRdr.Item("ITM_TP_CD").ToString) = "Y" Then
                            If lbl_tax_cd.Text & "" <> "" Then
                                sql = sql & "TAX_CD ='" & lbl_tax_cd.Text & "', "
                            End If
                            If Session("TAX_RATE") & "" <> "" Then
                                sql = sql & "TAX_PCT =" & Session("TAX_RATE") & ", "
                            End If
                            sql = sql & "TAXABLE_AMT =" & relLn.Item("RET_PRC") & ", "
                        End If

                        sql = sql & "warr_itm_link = '" & relLn.Item("warr_itm_link").ToString & "', "

                        If (Not IsNothing(relLn.Item("warr_row_link"))) AndAlso
                            relLn.Item("warr_row_link").ToString.isNotEmpty AndAlso
                            IsNumeric(relLn.Item("warr_row_link")) Then
                            sql = sql & "warr_row_link = " & (CInt(relLn.Item("warr_row_link").ToString()) + ln1RowSeqNum - 1) & ", "
                        End If

                        If IsNumeric(itmDatRdr.Item("POINT_SIZE").ToString) Then
                            sql = sql & "DEL_PTS =" & itmDatRdr.Item("POINT_SIZE").ToString & ", "
                        End If
                        sql = sql & "COMM_CD ='" & itmDatRdr.Item("COMM_CD").ToString & "', "
                        sql = sql & "SPIFF ='" & itmDatRdr.Item("SPIFF").ToString & "', "
                        sql = sql & "MNR_CD ='" & itmDatRdr.Item("MNR_CD").ToString & "', "
                        sql = sql & "CAT_CD ='" & itmDatRdr.Item("CAT_CD").ToString & "', "
                        sql = sql & "VSN ='" & Replace(itmDatRdr.Item("VSN").ToString, "'", "''") & "', "
                        sql = sql & "DES ='" & Replace(itmDatRdr.Item("DES").ToString, "'", "''") & "', "
                        sql = sql & "ITM_TP_CD ='" & itmDatRdr.Item("ITM_TP_CD").ToString & "', "
                        sql = sql & "TREATABLE ='" & itmDatRdr.Item("TREATABLE").ToString & "', "
                        sql = sql & "BULK_TP_ITM ='" & itmDatRdr.Item("BULK_TP_ITM").ToString & "', "
                        If "Y".Equals(itmDatRdr.Item("warrantable").ToString) Then
                            sql = sql & "warrantable ='" & itmDatRdr.Item("warrantable").ToString & "', "
                        End If
                        sql = sql & "SERIAL_TP ='" & itmDatRdr.Item("SERIAL_TP").ToString & "' "
                        If IsSerialType(itmDatRdr.Item("SERIAL_TP").ToString) Then
                            sql = sql & ", QTY=1 "
                        End If
                        sql = sql & "WHERE SESSION_ID='" & Session.SessionID.ToString() & "' "
                        sql = sql & "AND ITM_CD='" & relLn.Item("ITM_CD").ToString & "' "
                        sql = sql & "AND VSN IS NULL"
                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        objsql.ExecuteNonQuery()
                    Loop
                    itmDatRdr.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Next
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        '''''*****************************************************************
        'Insert into MULTI_DISC table
        If ln2disc.Rows.Count > 0 Then
            theSalesBiz.InserttoMultiDisc(Session.SessionID.ToString().Trim, ln2disc)
        End If
        ''''*****************************************************************
        Dim cust_tp_cd As String = ""
        sql = "SELECT CUST_TP_CD FROM CUST WHERE CUST_CD='" & txt_Cust_cd.Text.ToString.Trim & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

            If itmDatRdr.Read() Then
                cust_tp_cd = itmDatRdr.Item("CUST_TP_CD")
            Else
                cust_tp_cd = "I"
            End If
            itmDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        sql = "INSERT INTO CUST_INFO (SESSION_ID, CUST_CD, CORP_NAME, FNAME, LNAME, CUST_TP, ADDR1, ADDR2, CITY, ST, ZIP, HPHONE, BPHONE, EMAIL, BILL_FNAME, BILL_LNAME, BILL_ADDR1, BILL_ADDR2, BILL_CITY, BILL_ST, BILL_ZIP) "
        sql = sql & "VALUES (:SESSION_ID, :CUST_CD, :CORP_NAME, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL, :FNAME, :LNAME, :ADDR1, :ADDR2, :CITY, :ST, :ZIP) "

        With cmdDeleteItems
            .Connection = conn
            .CommandText = sql
        End With
        cmdDeleteItems.Parameters.Add(":SESSION_ID", OracleType.VarChar)
        cmdDeleteItems.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim
        cmdDeleteItems.Parameters.Add(":CUST_CD", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CUST_CD").Value = UCase(txt_Cust_cd.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":FNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":FNAME").Value = UCase(txt_f_name.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":CORP_NAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CORP_NAME").Value = UCase(txt_corp_name.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":LNAME", OracleType.VarChar)
        cmdDeleteItems.Parameters(":LNAME").Value = UCase(txt_l_name.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":CUST_TP", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CUST_TP").Value = cust_tp_cd
        cmdDeleteItems.Parameters.Add(":ADDR1", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ADDR1").Value = UCase(txt_addr1.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ADDR2", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ADDR2").Value = UCase(txt_addr2.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":CITY", OracleType.VarChar)
        cmdDeleteItems.Parameters(":CITY").Value = UCase(txt_city.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ST", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ST").Value = UCase(cbo_State.SelectedValue.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":ZIP", OracleType.VarChar)
        cmdDeleteItems.Parameters(":ZIP").Value = UCase(txt_zip.Text.ToString.Trim)
        cmdDeleteItems.Parameters.Add(":HPHONE", OracleType.VarChar)
        cmdDeleteItems.Parameters(":HPHONE").Value = StringUtils.FormatPhoneNumber(txt_h_phone.Text.ToString)
        cmdDeleteItems.Parameters.Add(":BPHONE", OracleType.VarChar)
        cmdDeleteItems.Parameters(":BPHONE").Value = StringUtils.FormatPhoneNumber(txt_b_phone.Text.ToString)
        cmdDeleteItems.Parameters.Add(":EMAIL", OracleType.VarChar)
        cmdDeleteItems.Parameters(":EMAIL").Value = UCase(txt_email.Text.ToString.Trim)
        cmdDeleteItems.ExecuteNonQuery()

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & lbl_tax_cd.Text & "' "
        sql = sql & "AND TAT.DEL_TAX = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

            If itmDatRdr.Read() Then
                Session("DEL_TAX") = itmDatRdr.Item("SumOfPCT")
            Else
                Session("DEL_TAX") = "N"
            End If
            itmDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & lbl_tax_cd.Text & "' "
        sql = sql & "AND TAT.SU_TAX = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        'Determine the taxability of the setup and delivery charges
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            itmDatRdr = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If itmDatRdr.Read() Then
                Session("SU_TAX") = itmDatRdr.Item("SumOfPCT")
            Else
                Session("SU_TAX") = "N"
            End If
            itmDatRdr.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

        'transfers the comments from the Relationship to the Sales Order
        If (ASPxPageControl1.ActiveTabIndex <> 4) Then
            'if the focus is NOT in the comments tab, the comments have to
            'be retrieved first, otherwise there will be no comments below
            Pull_Comments()
        End If

        Dim relComments As New StringBuilder()
        ' --- these are the RELATIONSHIP SALES comments
        relComments.Append(txt_cust_comments.Text)
        If (txt_cust_comments.Text.Trim.Length > 0 AndAlso txt_cust_past.Text.Trim.Length > 0) Then
            relComments.Append(", ")
        End If
        relComments.Append(txt_cust_past.Text.Trim).Append(" ")
        ' --- these are the INTERNAL SLSPSN Comments
        If (txt_comments.Text.Trim.Length > 0 OrElse txt_past_Comments.Text.Trim.Length > 0) Then
            relComments.Append("(Salesperson Notes:")
            relComments.Append(txt_comments.Text.Trim)
            If (txt_comments.Text.Trim.Length > 0 AndAlso txt_past_Comments.Text.Trim.Length > 0) Then
                relComments.Append(", ")
            End If
            relComments.Append(txt_past_Comments.Text.Trim).Append(")")
        End If
        Session("scomments") = relComments.ToString()

        Session("LEAD") = System.DBNull.Value
        If Not IsNothing(RelanPriceChange) AndAlso RelanPriceChange.Count > 0 Then
            Session("priceChangeDictionary") = RelanPriceChange
        End If
        If Not (Session("itemid") Is Nothing) Then
            Dim droppedSkusMsg As String = CheckForDroppedSkus(Session.SessionID.ToString(), False)
            ViewState("droppedSkusMsg") = droppedSkusMsg
        End If
        If needTorecalDiscount AndAlso ln2disc.Rows.Count > 0 Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            Dim errorMsg As String = String.Format(Resources.POSMessages.MSG0053, String.Join(",", dictCurrentPrice.Select(Function(pair) String.Format("{0}", pair.Key)).ToArray()))
            ucMsgPopup.DisplayConfirmMsg(errorMsg, "DISCOUNT")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            msgPopup.ShowOnPageLoad = True
        Else
            dropSKUpopup()
        End If

    End Sub

    Protected Sub btn_quote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_quote.Click

        Dim UpPanel As UpdateProgress
        UpPanel = Master.FindControl("UpdateProgress1")
        UpPanel.Visible = False
        Dim EMAIL As String = "False"
        If chk_email.Checked = True Then
            EMAIL = "True"
        Else
            EMAIL = "False"
        End If
        Dim default_invoice As String = ""
        If default_invoice & "" = "" Then
            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim sql As String = ""
            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn2.Open()

            sql = "select store_cd, quote_file, default_file from quote_admin where store_cd='" & txt_store.Text & "'"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                Do While MyDataReader.Read
                    If MyDataReader.Item("quote_file").ToString & "" = "" Then
                        default_invoice = MyDataReader.Item("DEFAULT_FILE").ToString
                    Else
                        default_invoice = MyDataReader.Item("quote_file").ToString
                    End If
                Loop
                MyDataReader.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            If String.IsNullOrEmpty(default_invoice) Then
                sql = "select store_cd, quote_file, default_file from quote_admin"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDataReader.Read Then
                        default_invoice = MyDataReader.Item("DEFAULT_FILE").ToString
                    End If
                    MyDataReader.Close()
                Catch ex As Exception
                    conn2.Close()
                    Throw
                End Try
            End If
            conn2.Close()
        End If
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "Rel_Prospect", "window.open('Invoices/" & default_invoice & "?DEL_DOC_NUM=" & txt_del_doc_num.Text.ToString & "&EMAIL=" & EMAIL & "','frmQuote345432543" & "','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub

    Public Function Determine_Taxability(ByVal itm_tp_cd As String)

        Determine_Taxability = "N"
        Dim sql As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If itm_tp_cd = "PKG" Or itm_tp_cd = "NLN" Then
            Determine_Taxability = "N"
            Exit Function
        End If

        If itm_tp_cd = "INV" Then
            Determine_Taxability = "Y"
            Exit Function
        End If

        conn.Open()
        sql = "select inventory from itm_tp where itm_tp_cd='" & itm_tp_cd & "' and inventory='Y'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                MyDataReader.Close()
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try

        Dim TAT_QUERY As String = ""
        Select Case itm_tp_cd
            Case AppConstants.Sku.TP_LAB
                TAT_QUERY = "LAB_TAX"
            Case AppConstants.Sku.TP_FAB
                TAT_QUERY = "FAB_TAX"
            Case AppConstants.Sku.TP_NLT
                TAT_QUERY = "NLT_TAX"
            Case AppConstants.Sku.TP_WAR
                TAT_QUERY = "WAR_TAX"
            Case AppConstants.Sku.TP_PAR
                TAT_QUERY = "PARTS_TAX"
            Case AppConstants.Sku.TP_MIL
                TAT_QUERY = "MILEAGE_TAX"
            Case Else
                Determine_Taxability = "N"
                conn.Close()
                Exit Function
        End Select

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & lbl_tax_cd.Text & "' "
        sql = sql & "AND TAT." & TAT_QUERY & " = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                MyDataReader.Close()
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Sub determine_tax(ByVal zip_cd As String, Optional ByVal conversion As String = "N")

        If cbo_tax_exempt.Value & "" = "" Then

            Dim sql As String = ""
            Dim dbReader As OracleDataReader
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
            dbConnection.Open()

            If String.IsNullOrEmpty(zip_cd) Then
                If Session("PD") = "P" Then
                    If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                        Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                        zip_cd = store.addr.postalCd
                    End If
                Else
                    zip_cd = theCustomerBiz.GetCustomerZip(Session.SessionID.ToString, Session("CUST_CD"))
                End If
            End If
            zip_cd = UCase(zip_cd)

            '** check if the zip is setup to use written store for it's taxes
            Dim Use_Written As String = "N"
            If zip_cd.ToString.isNotEmpty() Then
                sql = "SELECT * FROM ZIP WHERE ZIP_CD='" & zip_cd & "'"
                dbCommand.CommandText = sql

                Try
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If dbReader.Read() Then
                        If Session("PD") = "P" Then
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                        Else
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                        End If
                    End If
                    dbReader.Close()

                    If Use_Written = "Y" Then
                        If Session("store_cd") & "" <> "" And conversion = "Y" Then
                            sql = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
                        Else
                            sql = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & txt_store.Text & "'"
                        End If
                        dbCommand.CommandText = sql
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If dbReader.Read() Then
                            zip_cd = dbReader.Item("ZIP_CD")
                        End If
                        dbReader.Close()
                    End If
                Catch ex As Exception
                    dbConnection.Close()
                    Throw
                End Try

                zip_cd = UCase(zip_cd)
                Dim MyTable As New DataTable
                ' ** retrieve tax codes by zip
                Dim ds As DataSet = theSalesBiz.GetTaxCodesByZip(zip_cd)
                MyTable = ds.Tables(0)

                If (MyTable.Rows.Count = 1) Then

                    Dim taxCd As String = MyTable.Rows(0).Item("TAX_CD").ToString
                    Dim taxRate As String = MyTable.Rows(0).Item("SumofPCT").ToString
                    lbl_tax_rate.Text = taxRate
                    lbl_tax_cd.Text = taxCd
                    lnkModifyTax.Text = If(String.IsNullOrEmpty(taxCd), If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString), taxCd)
                    txtTaxCd.Value = taxCd & " - " & MyTable.Rows(0).Item("DES").ToString
                    Session("TAX_RATE") = taxRate
                    Session("TAX_CD") = taxCd
                    '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                    '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                    Session("MANUAL_TAX_CD") = Nothing

                    'Add tax rates to the inventory, if items exist                
                    Try
                        'set tax for take-with line to zero
                        sql = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_AMT=0, TAX_PCT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
                        dbCommand.CommandText = sql
                        dbCommand.ExecuteNonQuery()

                        sql = "UPDATE TEMP_ITM SET TAXABLE_AMT=RET_PRC, TAX_CD='" & taxCd & "', TAX_PCT=" & taxRate & " WHERE session_id='" & Session.SessionID.ToString & "'"
                        dbCommand.CommandText = sql
                        dbCommand.ExecuteNonQuery()
                    Catch ex As Exception
                        dbConnection.Close()
                        Throw
                    End Try
                Else
                    lbl_tax_rate.Text = ""
                    lbl_tax_cd.Text = ""
                    Session("TAX_RATE") = ""
                    Session("TAX_CD") = ""
                    Session("MANUAL_TAX_CD") = ""
                    lblTax.Text = "0.00"
                End If
            End If
            dbConnection.Close()
        Else
            'means it is tax exempt, so clear out the tax values and update temp item
            lbl_tax_rate.Text = ""
            lbl_tax_cd.Text = ""
            lblTax.Text = "0.00"
            Session("TAX_RATE") = ""
            Session("TAX_CD") = ""
            Session("MANUAL_TAX_CD") = ""


            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            dbConnection.Open()
            Dim sql As String = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_PCT=0, TAX_AMT=0 WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        End If

    End Sub

    Protected Sub chk_email_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If chk_email.Checked = True Then
            btnConfigureEmail.Enabled = True
        Else
            btnConfigureEmail.Enabled = False
        End If

    End Sub

    Protected Sub Line_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        conn.Open()

        Dim textdata As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        If textdata.Text & "" <> "" Then
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            sql = "SELECT * FROM RELATIONSHIP_LINES_DESC WHERE REL_NO='" & txt_del_doc_num.Text & "' AND LINE='" & dgItem.Cells(RelLnGrid.LN_NUM).Text & "'"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    sql = "UPDATE RELATIONSHIP_LINES_DESC SET DESCRIPTION=:DESCRIPTION WHERE REL_NO='" & txt_del_doc_num.Text & "' AND LINE='" & dgItem.Cells(RelLnGrid.LN_NUM).Text & "'"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.Parameters.Add(":DESCRIPTION", OracleType.Clob)
                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.ExecuteNonQuery()
                Else
                    sql = "INSERT INTO RELATIONSHIP_LINES_DESC (REL_NO, LINE, DESCRIPTION) VALUES(:REL_NO, :LINE, :DESCRIPTION)"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objSql.Parameters.Add(":REL_NO", OracleType.Clob)
                    objSql.Parameters.Add(":LINE", OracleType.VarChar)
                    objSql.Parameters.Add(":DESCRIPTION", OracleType.VarChar)

                    objSql.Parameters(":DESCRIPTION").Value = textdata.Text
                    objSql.Parameters(":LINE").Value = dgItem.Cells(RelLnGrid.LN_NUM).Text
                    objSql.Parameters(":REL_NO").Value = txt_del_doc_num.Text
                    objSql.ExecuteNonQuery()
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_total_margins_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim sql As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim frt_factor As Double = 0
        Dim repl_cst As Double = 0
        Dim ret_prc As Double = 0
        Dim qty As Double = 0
        Dim total_ret_prc As Double = 0
        Dim total_repl_cst As Double = 0

        conn.Open()
        conn2.Open()

        sql = "select ret_prc, itm_cd, qty from relationship_lines where rel_no='" & txt_del_doc_num.Text & "'"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read()
                If IsNumeric(MyDataReader2.Item("RET_PRC").ToString) Then ret_prc = CDbl(MyDataReader2.Item("RET_PRC").ToString)
                If IsNumeric(MyDataReader2.Item("QTY").ToString) Then qty = CDbl(MyDataReader2.Item("QTY").ToString)

                sql = "select repl_cst, frt_fac from itm where itm_cd='" & MyDataReader2.Item("ITM_CD").ToString & "'"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read() Then
                    If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                    If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                    If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                End If
                MyDataReader.Close()
                total_ret_prc = total_ret_prc + (ret_prc * qty)
                total_repl_cst = total_repl_cst + (repl_cst * qty)
            Loop
            MyDataReader2.Close()
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        conn.Close()
        conn2.Close()
        lbl_gm.Text = "Retail: " & FormatCurrency(total_ret_prc, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(total_ret_prc - total_repl_cst, 2) & "<br />"
        lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((total_ret_prc - total_repl_cst) / total_ret_prc, 2) & "<br />"
        ASPxPopupControl6.ShowOnPageLoad = True

    End Sub

    Protected Sub txt_zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        determine_tax(txt_zip.Text)
        calculate_total()

    End Sub

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(txt_zip.Text) Then

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            conn.Open()

            Dim sql As String = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & txt_zip.Text & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDataReader.Read() Then
                    txt_city.Text = MyDataReader.Item("CITY").ToString
                    cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                End If
                conn.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    'sorts the Purchase History grid
    Protected Sub DataGrid1_SortCommand(source As Object, e As DataGridSortCommandEventArgs) Handles DataGrid1.SortCommand
        DataGrid1.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(e.SortExpression).ToString()) 'Rebind our DataGrid
    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter

        objConnect = DisposablesManager.BuildOracleConnection

        objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sql = "select a.ar_trn_pk, a.ivc_cd, a.post_dt, a.trn_tp_cd, a.mop_cd, a.stat_cd, "
        sql = sql & "nvl(DECODE(T.DC_CD,'C',-A.AMT,'D',A.AMT,0),0) AS AMT "
        sql = sql & "from "
        sql = sql & "ar_trn_tp t "
        sql = sql & ", ar_trn a "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and a.cust_cd='" & txt_Cust_cd.Text & "' "

        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows = 0 Then
            DataGrid1.Visible = False
            lbl_warning.Visible = True
            lbl_warning.Text = "No records were found matching your search criteria.<br />"
        Else
            Source.Sort = SortField
            DataGrid1.DataSource = Source
            DataGrid1.DataBind()
            lbl_warning.Visible = False
        End If
        'Close connection           
        objConnect.Close()
        txt_acct_balance.Text = FormatCurrency(running_balance, 2)

        'Below code is for in purchase order tab while binding data to grid lbl_warning.visible=false .
        'if any line item is dropped user not able to get information because of label is not visible.
        'Code review done by laxmikanth.
        If Not (txt_del_doc_num.Text.Trim) = String.Empty Then

            Dim droppedSkusMsg As String = CheckForDroppedSkus(txt_del_doc_num.Text.Trim)
            If droppedSkusMsg.isNotEmpty() Then
                If lbl_warning.Visible = True Then
                    lbl_warning.Text = lbl_warning.Text & " " & droppedSkusMsg
                Else
                    lbl_warning.Text = droppedSkusMsg
                    lbl_warning.Visible = True
                End If
            End If
        End If
    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged(ByVal source As Object, ByVal e As DevExpress.Web.ASPxTabControl.TabControlEventArgs)

        If txt_Cust_cd.Text & "" <> "" Then
            If ASPxPageControl1.ActiveTabIndex = 2 Then
                ucDiscount.ReqSource = "RELN"
                ucDiscount.LoadDiscount("O")
            ElseIf ASPxPageControl1.ActiveTabIndex = 3 Then
                'Set up default column sorting   
                If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                    BindData("post_dt asc")
                Else
                    BindData(Session("SortOrder"))
                End If
            ElseIf ASPxPageControl1.ActiveTabIndex = 4 Then
                If IsNothing(Session("SortOrder2")) Or IsDBNull(Session("SortOrder2")) Then
                    GridView1_Binddata("rel_no asc")
                Else
                    GridView1_Binddata(Session("SortOrder2"))
                End If
            ElseIf ASPxPageControl1.ActiveTabIndex = 5 Then
                Pull_Comments()
            End If
        End If
    End Sub

    Public Sub GridView1_Binddata(ByVal SortField As String)

        txt_quote_summary.Text = "0.00"
        Dim sql As String
        Dim Source As DataView
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        ds = New DataSet

        Dim FOLLOW_UP_DT As String = ""

        Gridview1.DataSource = ""
        'CustTable.Visible = False
        Gridview1.Visible = True
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "SELECT REL_NO, WR_DT, FOLLOW_UP_DT, NVL(PROSPECT_TOTAL,0) as PROSPECT_TOTAL, STORE_CD, DECODE(REL_STATUS,'O','Open','C','Converted','F','Closed','Unknown') As REL_STATUS FROM RELATIONSHIP WHERE "
        sql = sql & "CUST_CD = '" & txt_Cust_cd.Text & "' ORDER BY WR_DT"

        conn.Open()

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim MyTable As DataTable
        Dim numrows As Integer
        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Assign sort expression to Session              
        Session("SortOrder2") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Source.Sort = SortField
                gridQuoteHistory.DataSource = Source
                gridQuoteHistory.DataBind()
            Else
                gridQuoteHistory.Visible = False
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub gridQuoteHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridQuoteHistory.ItemDataBound

        ' TODO - this is a different datagrid, not lines
        If IsNumeric(e.Item.Cells(3).Text) Then
            quote_balance = FormatNumber(quote_balance + e.Item.Cells(3).Text, 2)
        End If
        If Len(e.Item.Cells(6).Text) > 10 Then
            Dim hpl_ivc As HyperLink = CType(e.Item.FindControl("Hyperlink2"), HyperLink)
            hpl_ivc.NavigateUrl = "~/Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & e.Item.Cells(6).Text
            hpl_ivc.Text = e.Item.Cells(6).Text
        End If

        txt_quote_summary.Text = FormatCurrency(quote_balance, 2)

    End Sub

    Function SortData(ByVal Field As String) As String

        Dim so As String = Session("SortOrder2")

        If Field = so Then
            SortData = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortData = Replace(Field, "desc", "asc")
        Else
            SortData = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder2") = SortData

    End Function

    Protected Sub gridQuoteHistory_SortCommand(source As Object, e As DataGridSortCommandEventArgs) Handles gridQuoteHistory.SortCommand

        gridQuoteHistory.CurrentPageIndex = 0 'To sort from top
        GridView1_Binddata(SortData(e.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Public Sub Pull_Comments()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim DEL_DOC_NUM As String = txt_del_doc_num.Text
        conn.Open()

        sql = "SELECT * FROM RELATIONSHIP_COMMENTS where REL_NO='" & DEL_DOC_NUM & "' ORDER BY SUBMIT_DT ASC"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim curr_dt As String = ""
        Dim cmnt_cnt As Integer = 0

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                If curr_dt & "" = "" Then curr_dt = MyDataReader.Item("SUBMIT_DT").ToString
                If curr_dt <> MyDataReader.Item("SUBMIT_DT").ToString Then
                    txt_past_Comments.Text = txt_past_Comments.Text & vbCrLf & vbCrLf & MyDataReader.Item("SUBMIT_DT") & ": " & MyDataReader.Item("COMMENTS")
                Else
                    If cmnt_cnt = 0 Then
                        txt_past_Comments.Text = MyDataReader.Item("SUBMIT_DT") & ": " & MyDataReader.Item("COMMENTS")
                    Else
                        txt_past_Comments.Text = txt_past_Comments.Text & MyDataReader.Item("COMMENTS").ToString
                    End If
                End If
                curr_dt = MyDataReader.Item("SUBMIT_DT").ToString
                cmnt_cnt = cmnt_cnt + 1
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        sql = "SELECT FOLLOW_UP_DT, COMMENTS FROM RELATIONSHIP where REL_NO='" & DEL_DOC_NUM & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                If IsDate(MyDataReader.Item("FOLLOW_UP_DT").ToString) Then
                    txt_follow_up_dt.Text = FormatDateTime(MyDataReader.Item("FOLLOW_UP_DT").ToString, DateFormat.ShortDate)
                End If
                txt_cust_past.Text = MyDataReader.Item("COMMENTS").ToString
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub btn_Save_Comments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_comments.Click

        Dim sql As String
        Dim objSql As OracleCommand
        Dim DEL_DOC_NUM As String = txt_del_doc_num.Text
        Dim x As Integer, y As Integer
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim lngSEQNUM As Integer, lngComments As Integer
        Dim strComments As String, strMidCmnts As String

        x = 0
        y = 1

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        If txt_comments.Text & "" <> "" Then
            lngSEQNUM = 1
            strComments = txt_comments.Text.ToString
            lngComments = Len(strComments)

            DEL_DOC_NUM = txt_del_doc_num.Text

            If lngComments > 0 Then
                x = 1
                lngSEQNUM = 1

                Do Until x > lngComments
                    strMidCmnts = Mid(strComments, x, 254)
                    strMidCmnts = Replace(strMidCmnts, "'", "")

                    sql = "INSERT INTO RELATIONSHIP_COMMENTS (REL_NO, FOLLOW_UP_DT, COMMENTS) "
                    sql = sql & "VALUES('" & DEL_DOC_NUM & "',SYSDATE,'"
                    sql = sql & Replace(strMidCmnts, """", """""") & "')"

                    sql = UCase(sql)

                    'Perform ERP update
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()

                    x = x + 254
                    lngSEQNUM = lngSEQNUM + 1
                Loop
            End If
        End If

        sql = "UPDATE RELATIONSHIP SET FOLLOW_UP_DT = TO_DATE('" & txt_follow_up_dt.Text & "','mm/dd/RRRR'), "
        sql = sql & "COMMENTS='" & Replace(Replace(txt_cust_comments.Text & "  " & txt_cust_past.Text, "'", "''"), """", """""") & "' "
        sql = sql & "WHERE REL_NO='" & DEL_DOC_NUM & "'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        conn.Close()
        txt_comments.Text = ""
        txt_past_Comments.Text = ""
        txt_cust_past.Text = ""
        txt_cust_comments.Text = ""
        Pull_Comments()
    End Sub

    Protected Sub btn_copy_Click(sender As Object, e As EventArgs) Handles btn_copy.Click

        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            If Not (txt_del_doc_num.Text.Trim) = String.Empty Then

                Dim droppedSkusMsg As String = CheckForDroppedSkus(txt_del_doc_num.Text.Trim)
                If droppedSkusMsg.isNotEmpty() Then
                    'This code is written with reference to Admin.aspx.vb as suggested by Laxmikantha.
                    Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                    ucMsgPopup.DisplayAlertMsg(droppedSkusMsg, "")
                    Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                    msgComponent.ForeColor = Color.Red
                    msgPopup.ShowOnPageLoad = True
                    'Reference ends here
                    ViewState("redirect") = False
                Else
                    'show the Relationship_Copy popup
                    ASPxPopupControl2.ShowOnPageLoad = True
                End If
            End If
        End If

    End Sub

    Protected Sub btnConfigureEmail_Click(sender As Object, e As EventArgs) Handles btnConfigureEmail.Click

        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            ASPxPopupControl1.ShowOnPageLoad = True
        End If
    End Sub

    ''' <summary>
    ''' Fires when user clicks on the icon button that brings up the 'Update Tax' popup.
    ''' </summary>
    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkModifyTax.Click

        If (Not (Convert.ToBoolean(ViewState("CanModifyTax")))) Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
            ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
            msgPopup.ShowOnPageLoad = True
        Else
            Dim _hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
            _hidIsInvokeManually.Value = "true"

            Dim _hidDelDocNo As HiddenField = CType(ucTaxUpdate.FindControl("hidDelDocNo"), HiddenField)
            _hidDelDocNo.Value = System.Web.HttpUtility.HtmlEncode(txt_del_doc_num.Text)

            Dim _hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
            _hidTaxDt.Value = System.Web.HttpUtility.HtmlEncode(txt_wr_dt.Text.Trim())

            ucTaxUpdate.LoadTaxData()
            PopupControlTax.ShowOnPageLoad = True
        End If
    End Sub

    Private Sub DisplayMsg(ByVal msg As String, popup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl)
        popup.Text = msg
        popup.ShowOnPageLoad = True
    End Sub

    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated

        Initialize()

        Dim _hidTaxCd As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxCd"), HiddenField)
        Session("TAX_CD") = _hidTaxCd.Value
        lbl_tax_cd.Text = _hidTaxCd.Value

        Dim _hidTaxDesc As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDesc"), HiddenField)
        txtTaxCd.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(_hidTaxCd.Value & " - " & _hidTaxDesc.Value))
        lnkModifyTax.Text = If(String.IsNullOrEmpty(_hidTaxCd.Value), If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString), _hidTaxCd.Value.ToString)

        Dim _hidTaxRate As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxRate"), HiddenField)
        Session("TAX_RATE") = _hidTaxRate.Value

        calculate_total()
        'TODO:AXK need to ask DSA if this needs to be done again:
        'Dim txPcts As TaxUtils.taxPcts = SetTxPcts(Session("tran_dt"), Session("tax_cd")) ' local pass thru

        PopupControlTax.ShowOnPageLoad = False

    End Sub

    Protected Sub ucMsgPopup_Choice(sender As Object, e As EventArgs, choice As Usercontrols_MessagePopup.MessageResponse) Handles ucMsgPopup.Choice
        'OK button clcik event

        If ucMsgPopup.MessageResponse.YES_OK_SELECTION = choice Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            If Not IsNothing(Session("dictCurrentPrice")) Then
                ConvertToOrder()
            ElseIf Not ViewState("needTorecalDiscount") Is Nothing Then
                ReapplyDiscountsOnCovtoOrder(ReapplyDiscountRequestDtc.LnChng.Prc, String.Empty)
                dropSKUpopup()
            Else
                If Not ViewState("redirect") Is Nothing AndAlso ViewState("redirect") = True Then
                    Response.Redirect("startup.aspx")
                Else
                    ASPxPopupControl2.ShowOnPageLoad = True
                End If
            End If
        ElseIf ucMsgPopup.MessageResponse.NO_SELECTION = choice Then
            If Not ViewState("needTorecalDiscount") Is Nothing Then
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                theSalesBiz.DeleteTempLnDiscount(Session.SessionID.ToString.Trim)
                theSalesBiz.ReapplyDiscountForRelConvSOE(Session.SessionID.ToString.Trim)
                Response.Redirect("startup.aspx")
            End If
        End If
    End Sub

    Public Sub dropSKUpopup()
        If Not (ViewState("droppedSkusMsg") = String.Empty) Then
            Dim droppedSkusMsg As String = ViewState("droppedSkusMsg")
            'This code is written with reference to Admin.aspx.vb as suggested by Laxmikantha.
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            ucMsgPopup.DisplayAlertMsg(droppedSkusMsg, "")
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = Color.Red
            msgPopup.ShowOnPageLoad = True
            'Reference ends here
            ViewState("redirect") = True
        Else
            Response.Redirect("startup.aspx")
        End If
    End Sub
    ''' <summary>
    ''' Recalculates the discounts (when applicable)
    ''' </summary>
    ''' <param name="dtLines">the table of all lines in the current order stored in the session </param>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">used only for add line, last line that existed before discounts to be recalc'd</param>
    ''' MM-9870
    Private Sub ReapplyDiscounts(ByRef dtLines As DataTable, ByVal action As ReapplyDiscountRequestDtc.LnChng, Optional ByVal lnSeq As String = "", Optional ByVal IsCanceled As Boolean = False)

        Dim relnTbl As DataSet = Session("ORIG_RELN")
        Dim relnWrDt As Date = relnTbl.Tables(0).Rows(0)("WR_DT")   'soDatRow("so_wr_dt")
        Dim discRequest As ReapplyDiscountRequestDtc = New ReapplyDiscountRequestDtc(action, Session("pd"), dtLines, Session("DISC_APPLIED_RELN"),
                                                                                     lnSeq, relnWrDt)
        Dim isSOM As Boolean = False 'for relationship it is False , for SOM it is True.
        'MM-9870
        Dim discResponse As ReapplyDiscountResponseDtc
        If IsCanceled Then
            discRequest.discApplied = Session("discountsApplied")
            discResponse = theSalesBiz.CheckDiscountsForLineChangeSOM(discRequest, isSOM, True)
        Else
            discResponse = theSalesBiz.CheckDiscountsForLineChangeSOM(discRequest, isSOM)
        End If

        'update the session variables with the objects that have been updated and returned as part of the response
        dtLines = discResponse.orderLines

        '*** this is to ensure that the discount tab gets refreshed properly since the active tab changed does not fire.
        If Not IsNothing(discResponse.discountsApplied) Then

            Session("DISC_APPLIED_RELN") = discResponse.discountsApplied
            ucDiscount.BindDiscountsApplied(discResponse.discountsApplied)
        End If
        If discResponse.message.isNotEmpty Then
            lbl_desc.Text = discResponse.message   'SHU'd lines not updateable
        End If

        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub

    Protected Sub ucDiscount_DiscountApplied(sender As Object, e As EventArgs) Handles ucDiscount.DiscountApplied

        SessVar.Remove(SessVar.discReCalcVarNm)
        'MM-5661
        ViewState("IsDiscountAdded") = True
        bindgrid()
        DoAfterBindgrid()
        calculate_total()
    End Sub

    Protected Sub ucDiscount_CancelledManagerApproval(sender As Object, e As EventArgs) Handles ucDiscount.CancelledManagerApproval
        Session("mgrApproval") = "N"
        lbl_desc.Text = Resources.POSMessages.MSG0038
    End Sub
    Protected Sub ucDiscount_Canceled(sender As Object, e As EventArgs) Handles ucDiscount.Canceled
        Session("IsCanceled") = True
        WasPCCanceled()
    End Sub

    Private Sub ShowWarrantyPopup(focusWarr As String)
        Dim itmCd As String
        Dim isInventory As Boolean
        Dim isWarrantable As Boolean

        Dim dtWarrantable As New DataTable
        dtWarrantable.Columns.Add("ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("ITM_TP_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_TEXT", GetType(String))
        dtWarrantable.Columns.Add("IS_PREV_SEL", GetType(Boolean))

        Dim dtRelItems As New DataTable
        Dim drWarrantable As DataRow

        If Not IsNothing(Session("REL_LN")) Then
            Dim dsRelItems As DataSet = CType(Session("REL_LN"), DataSet)
            If dsRelItems.Tables.Count > 0 Then dtRelItems = dsRelItems.Tables(0)

            For Each dr As DataRow In dtRelItems.Rows
                If dr.RowState <> DataRowState.Deleted Then
                    itmCd = dr("ITM_CD")
                    isInventory = IIf("Y".Equals(dr.Item("INVENTORY") & ""), True, False)
                    isWarrantable = IIf("Y".Equals(dr.Item("WARRANTABLE") & ""), True, False)

                    If isInventory AndAlso isWarrantable Then
                        If SalesUtils.hasWarranty(itmCd) Then
                            If SysPms.isOne2ManyWarr Then
                                Dim where As String = "ITM_TP_CD= '" & SkuUtils.ITM_TP_WAR & "'"
                                Dim warrantyRows() As DataRow = dtRelItems.Select(where)
                                Dim tempWarrObj As TempWarrDtc

                                For i As Integer = 0 To warrantyRows.GetUpperBound(0)
                                    Dim warrantyRow As DataRow = (warrantyRows(i))

                                    If (theMerchBiz.IsValWarrForItm(itmCd, warrantyRow("ITM_CD").ToString)) Then
                                        tempWarrObj = New TempWarrDtc()
                                        tempWarrObj.sessId = warrantyRow("REL_NO")
                                        tempWarrObj.itmCd = warrantyRow("ITM_CD").ToString
                                        tempWarrObj.lnNum = warrantyRow("LINE").ToString
                                        tempWarrObj.retPrc = CDbl(warrantyRow("ret_prc"))
                                        tempWarrObj.vsn = warrantyRow("VSN").ToString
                                        tempWarrObj.days = warrantyRow("WARR_DAYS")
                                        HBCG_Utils.CreateTempWarr(tempWarrObj)
                                    End If
                                Next i
                            End If

                            drWarrantable = dtWarrantable.NewRow()
                            drWarrantable("ROW_ID") = dr("LINE")
                            drWarrantable("ITM_CD") = itmCd
                            drWarrantable("ITM_TP_CD") = dr("ITM_TP_CD")
                            drWarrantable("WARR_ROW_ID") = dr("WARR_ROW_LINK")
                            drWarrantable("WARR_ITM_CD") = dr("WARR_ITM_LINK")

                            If dtWarrantable.Rows.Count > 0 Then
                                Dim filterExp As String = "IS_PREV_SEL = FALSE AND WARR_ROW_ID = '" & dr("LINE") & "' AND WARR_ITM_CD = '" & dr("WARR_ITM_LINK") & "'"
                                Dim drFiltered As DataRow() = dtWarrantable.Select(filterExp)
                                drWarrantable("IS_PREV_SEL") = drFiltered.Length > 0
                            Else
                                drWarrantable("IS_PREV_SEL") = False
                            End If

                            dtWarrantable.Rows.Add(drWarrantable)
                        End If
                    End If
                End If
            Next
        End If

        If String.IsNullOrEmpty(focusWarr) AndAlso dtRelItems.Rows.Count > 0 Then
            Dim sortExp As String = "LINE DESC"
            Dim dt As DataTable = dtRelItems.Select(Nothing, sortExp).CopyToDataTable()
            focusWarr = dt.Rows(0)("LINE").ToString()
        End If

        If dtWarrantable.Rows.Count > 0 Then
            Dim _popupWarranties As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucWarranties.FindControl("popupWarranties")
            _popupWarranties.Modal = True

            Dim _hidDelDocNum As HiddenField = _popupWarranties.FindControl("hidDelDocNum")
            _hidDelDocNum.Value = txt_del_doc_num.Text

            Dim _hidFocusItem As HiddenField = _popupWarranties.FindControl("hidFocusItem")
            _hidFocusItem.Value = focusWarr

            _popupWarranties.ShowOnPageLoad = True
            ucWarranties.LoadWarranties(dtWarrantable)
        ElseIf Not IsNothing(Session("ITMCDLIST")) Then
            Dim relatedSku As String = Session("ITMCDLIST")
            Session("ITMCDLIST") = Nothing

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Protected Sub ucWarranties_WarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.WarrantyAdded
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarrInfo As DataTable = CType(Session("WARRINFO"), DataTable)
            Dim sortExp As String = "WARR_ROW_ID, IS_PREV_SEL"
            Dim dtWarrInfoSorted As DataTable = dtWarrInfo.Select(Nothing, sortExp).CopyToDataTable()

            SetupForDiscReCalc()

            Dim dsRelItems As New DataSet
            Dim dtRelItems As New DataTable
            Dim drWarrantable() As DataRow

            If Not IsNothing(Session("REL_LN")) Then
                dsRelItems = CType(Session("REL_LN"), DataSet)
                If dsRelItems.Tables.Count > 0 Then dtRelItems = dsRelItems.Tables(0)
            End If

            Dim warrRowIdSeqNum As String = String.Empty
            Dim warrItmCd As String
            Dim drWarrRowId() As DataRow
            Dim newRelLn As DataRow

            For Each dr As DataRow In dtWarrInfoSorted.Rows
                warrItmCd = String.Empty
                newRelLn = Nothing
                If Not IsDBNull(dr("IS_PREV_SEL")) AndAlso dr("IS_PREV_SEL") = True Then
                    warrItmCd = dr("WARR_ITM_CD")
                    drWarrRowId = dtWarrInfoSorted.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" & warrItmCd & "'")

                    If drWarrRowId.Length > 0 Then
                        dr("WARR_ROW_ID") = drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID")
                    Else
                        dr("WARR_ROW_ID") = String.Empty
                    End If
                End If

                warrRowIdSeqNum = String.Empty

                '***Check for Warranties being added or not after returning from warranties popup *******
                If Not IsDBNull(dr("WARR_ITM_CD")) AndAlso Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                    If String.IsNullOrEmpty(dr("WARR_ROW_ID")) Then
                        '*** Implies that a new warranty sku is being added after the popup selection. In this case a new 
                        '*** line for the warranty sku needs to be added. Then, the link info on the warrantable sku 
                        '*** also needs to be updated.
                        If Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                            '--means a warrantable sku was selected
                            newRelLn = AddLine(dr("WARR_ITM_CD"), 1, 0, "", "", False)
                            dr("WARR_ROW_ID") = newRelLn("LINE")
                        End If
                    End If
                End If

                drWarrantable = dtRelItems.Select("LINE = " & dr("ROW_ID"))

                If drWarrantable.Length > 0 Then
                    drWarrantable(0)("WARR_ROW_LINK") = dr("WARR_ROW_ID")
                    drWarrantable(0)("WARR_ITM_LINK") = dr("WARR_ITM_CD")
                End If

                drWarrantable = Nothing
            Next

            'Remove the voided Warranties
            If dtRelItems.Rows.Count > 0 Then
                Dim drWarSKUs As DataRow() = dtRelItems.Select("ITM_TP_CD = '" & AppConstants.Sku.TP_WAR & "'")

                For Each dr As DataRow In drWarSKUs
                    If dtRelItems.Select("WARR_ROW_LINK = '" & dr("LINE") & "'").Length < 1 Then
                        Dim drWarRow As DataRow() = dtRelItems.Select("LINE = '" & dr("LINE") & "'")
                        If drWarRow.Length > 0 Then
                            For Each dgItem As DataGridItem In Gridview1.Items
                                If dgItem.Cells(RelLnGrid.LN_NUM).Text = drWarRow(0)("LINE") Then
                                    deleteRow(dgItem.Cells(RelLnGrid.LN_NUM).Text)
                                    dtRelItems.AcceptChanges()
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                Next
            End If

            Session("WARRINFO") = dsRelItems
            DoDiscReCalcNowOrLater()
            bindgrid()
            DoAfterBindgrid()
            calculate_total()
        End If

        Session.Remove("WARRINFO")

        If Not IsNothing(Session("ITMCDLIST")) Then
            Dim relatedSku As String = Session("ITMCDLIST")
            Session("ITMCDLIST") = Nothing

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Protected Sub ucWarranties_NoWarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.NoWarrantyAdded
        If Not String.IsNullOrEmpty(Session("ITMCDLIST")) Then
            Dim relatedSku As String = Session("ITMCDLIST")
            Session("ITMCDLIST") = Nothing

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Private Sub ShowRelatedSkuPopUp(itemcd As String)
        If Not String.IsNullOrEmpty(itemcd) Then
            ucRelatedSku.LoadRelatedSKUData(itemcd.Replace("'", String.Empty).TrimStart(",").TrimEnd(","))
        End If
    End Sub

    Protected Sub ucRelatedSku_AddSelection(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSku.AddSelection
        Dim skus As New StringBuilder

        For Each obj As Object In selectedRelSKUs
            If Not IsNothing(obj) AndAlso Not String.IsNullOrEmpty(obj) Then
                skus.Append("," & UCase(obj.ToString()))
            End If
        Next

        If Not String.IsNullOrEmpty(skus.ToString()) Then
            Session("ITMCDLIST") = skus.ToString()
            SetupForDiscReCalc()

            Dim showWarrPopup As Boolean = False
            Dim focusedLn As String = String.Empty
            Dim newRelLn As DataRow = Nothing
            For Each newItemCd As String In skus.ToString().TrimStart(",").TrimEnd(",").Split(",")
                newRelLn = AddLine(newItemCd, 1, String.Empty, String.Empty, String.Empty, String.Empty)
                If Not IsNothing(newRelLn) AndAlso Not showWarrPopup Then
                    showWarrPopup = "Y".Equals(newRelLn("INVENTORY").ToString) _
                                        AndAlso "Y".Equals(newRelLn("WARRANTABLE").ToString) AndAlso SalesUtils.hasWarranty(newRelLn("ITM_CD"))
                End If

                If Not IsNothing(newRelLn) AndAlso String.IsNullOrEmpty(focusedLn) Then focusedLn = newRelLn("LINE").ToString
            Next

            If showWarrPopup Then ShowWarrantyPopup(String.Empty)
            DoDiscReCalcNowOrLater()
            bindgrid()
            DoAfterBindgrid()
            calculate_total()
        End If
    End Sub

    ''' <summary>
    ''' Recalculates the discounts (when price is updated while relationship is convert to order)
    ''' </summary>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">the ID for the line that triggered this event</param>
    Private Sub ReapplyDiscountsOnCovtoOrder(ByVal action As ReapplyDiscountRequestDtc.LnChng, ByVal lnSeq As String)
        Dim needTorecalDiscount As Boolean = ViewState("needTorecalDiscount")
        ViewState("needTorecalDiscount") = Nothing
        If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd")) Then
            Dim discRequest As ReapplyDiscountRequestDtc
            discRequest = New ReapplyDiscountRequestDtc(action, Session("pd"), Session.SessionID.ToString.Trim, lnSeq)
            theSalesBiz.CheckDiscountsForLineChangeSOE(discRequest)
        End If
        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub
End Class
