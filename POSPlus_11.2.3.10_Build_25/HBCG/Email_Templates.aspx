<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Email_Templates.aspx.vb"
    ValidateRequest="false" Inherits="Email_Templates" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Template:">
                        </dx:ASPxLabel>
                    </td>
                    <td valign="middle">
                        <dx:ASPxComboBox ID="cbo_template" runat="server" AutoPostBack="True" Width="301px"
                            IncrementalFilteringMode ="StartsWith">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="From:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_from" runat="server" CssClass="style5" Width="376px" MaxLength="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="To:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_to" runat="server" CssClass="style5" Width="376px" MaxLength="2500"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="CC:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_cc" runat="server" CssClass="style5" Width="376px" MaxLength="2500"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="BCC:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_bcc" runat="server" CssClass="style5" Width="376px" MaxLength="2500"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Subject:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_subject" runat="server" CssClass="style5" MaxLength="250" Width="376px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Body:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_body" runat="server" MaxLength="5000" TextMode="MultiLine"
                            Width="376px" Height="100px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 68px">
                        &nbsp;
                    </td>
                    <td align="right">
                        <dx:ASPxButton ID="btn_submit" runat="server" Text="Save Changes">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <dx:ASPxLabel ID="lbl_error" runat="server" Width="459px" ForeColor="#FF0033">
        </dx:ASPxLabel>
        <br />
        <dx:ASPxLabel ID="lbl_ord_tp" runat="server" Visible="False" Width="1px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lbl_slsp2" runat="server" Visible="False" Width="1px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lbl_cust_cd" runat="server" Visible="False" Width="1px">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lbl_slsp1" runat="server" Visible="False" Width="1px">
        </dx:ASPxLabel>
        <br />
        <dx:ASPxLabel ID="lbl_del_doc_num" runat="server" Visible="False" Width="1px">
        </dx:ASPxLabel>
    </form>
</body>
</html>
