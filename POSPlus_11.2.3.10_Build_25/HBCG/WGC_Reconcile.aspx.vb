Imports System.Xml
Imports World_Gift_Utils
Imports HBCG_Utils
Imports System.Data.OracleClient

Partial Class WGC_Reconcile
    Inherits POSBasePage
    Dim lbl_1335t As Double = 0
    Dim lbl_1336t As Double = 0
    Dim lbl_1337t As Double = 0
    Dim lbl_1338t As Double = 0
    Dim lbl_1339t As Double = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Try
                txt_co_cd.Text = ConfigurationManager.AppSettings("co_cd").ToString
            Catch ex As Exception
                'Do Nothing
            End Try
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim oAdp As OracleDataAdapter

        Try
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn2.Open()
            objSql.CommandText = "SELECT a.ivc_cd, a.post_dt, b.MOP_CD, b.mop_tp, a.acct_num, a.amt " &
                                 "FROM ar_trn a, mop b " &
                                 "WHERE CO_CD=:CO_CD AND trn_tp_cd IN ('DEP','PMT') " &
                                 "AND post_dt between TO_DATE(:START_DT,'mm/dd/RRRR') AND TO_DATE(:END_DT,'mm/dd/RRRR') " &
                                 "AND a.mop_cd=b.mop_cd and b.mop_tp='GC'"
            objSql.Connection = conn2
            objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
            objSql.Parameters(":CO_CD").Value = txt_co_cd.Text
            objSql.Parameters.Add(":START_DT", OracleType.VarChar)
            objSql.Parameters(":START_DT").Value = FormatDateTime(cbo_start_dt.Value, DateFormat.ShortDate)
            objSql.Parameters.Add(":END_DT", OracleType.VarChar)
            objSql.Parameters(":END_DT").Value = FormatDateTime(cbo_end_dt.Value, DateFormat.ShortDate)

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            DataGrid1.DataSource = ds
            DataGrid1.DataBind()

            sql = "select a.gl_acct_cd, sum(a.amt) as AMT, a.DC_CD "
            sql = sql & "from gl_trn a  "
            sql = sql & "where CO_CD='" & txt_co_cd.Text & "' and gl_acct_cd IN ('1335-00','1336-00','1337-00','1338-00','1339-00') "
            sql = sql & "group by gl_acct_cd, DC_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds2)
            ASPxGridView1.DataSource = ds2
            ASPxGridView1.DataBind()

            btn_post_gl.Enabled = True
        Catch
            lbl_response.Text = Err.Description
        End Try

    End Sub

    Protected Sub DataGrid1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles DataGrid1.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If e.GetValue("ACCT_NUM").ToString() & "" <> "" Then
            Dim lbl_original As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(DataGrid1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_original"), DevExpress.Web.ASPxEditors.ASPxLabel)
            Dim lbl_original_tran As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(DataGrid1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_original_tran"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_original) And Not IsNothing(lbl_original_tran) Then
                Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim sql As String
                Dim MyDatareader As OracleDataReader

                Try
                    conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                    conn2.Open()

                    'jkl
                    'sql = "select a.trn_tp_cd, sum(a.amt) as AMT "
                    'sql = sql & "from ar_trn a  "
                    'sql = sql & "where CO_CD='" & txt_co_cd.Text & "' and a.mop_cd is NULL "
                    'sql = sql & "and a.acct_num='" & e.GetValue("ACCT_NUM").ToString() & "' "
                    'sql = sql & "and a.post_dt > TO_DATE('06/1/2011','mm/dd/RRRR') "
                    'sql = sql & "group by trn_tp_cd"

                    'objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objSql.CommandText = "select a.trn_tp_cd, sum(a.amt) as AMT from ar_trn a where CO_CD=:CO_CD and a.mop_cd is NULL and a.acct_num=:ACCT_NUM and a.post_dt > TO_DATE('06/1/2011','mm/dd/RRRR') group by trn_tp_cd"
                    objSql.Connection = conn2
                    objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                    objSql.Parameters(":CO_CD").Value = txt_co_cd.Text
                    objSql.Parameters.Add(":ACCT_NUM", OracleType.VarChar)
                    objSql.Parameters(":ACCT_NUM").Value = e.GetValue("ACCT_NUM").ToString()

                    MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

                    If Not MyDatareader.HasRows Then
                        lbl_original_tran.Text = "PURCHASED"
                        lbl_1338t = lbl_1338t + e.GetValue("AMT").ToString
                    End If
                    Dim x As Integer = 1
                    Do While MyDatareader.Read
                        lbl_original.Text = lbl_original.Text & FormatCurrency(CDbl(MyDatareader.Item("AMT").ToString), 2)
                        lbl_original_tran.Text = lbl_original_tran.Text & MyDatareader.Item("TRN_TP_CD").ToString
                        Select Case MyDatareader.Item("TRN_TP_CD").ToString
                            Case "GCA"
                                lbl_1335t = lbl_1335t + e.GetValue("AMT").ToString
                            Case "GCD"
                                lbl_1336t = lbl_1336t + e.GetValue("AMT").ToString
                            Case "GCP"
                                lbl_1337t = lbl_1337t + e.GetValue("AMT").ToString
                        End Select
                        If x > 1 Then
                            lbl_original.Text = lbl_original.Text & vbCrLf
                            lbl_original_tran.Text = lbl_original_tran.Text & vbCrLf
                        End If
                        x = x + 1
                    Loop
                    MyDatareader.Close()
                    conn2.Close()
                Catch

                End Try
            End If
        End If
        lbl_1338.Text = FormatNumber(CDbl(lbl_1338t.ToString), 2)
        lbl_1335.Text = FormatNumber(CDbl(lbl_1335t.ToString), 2)
        lbl_1336.Text = FormatNumber(CDbl(lbl_1336t.ToString), 2)
        lbl_1337.Text = FormatNumber(CDbl(lbl_1337t.ToString), 2)


    End Sub

    Protected Sub btn_post_gl_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
