Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.IO

Partial Class Customer_Upload
    Inherits POSBasePage
    Dim FileName As String = ""

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_preview.Click

        Data_Preview()

    End Sub

    Public Sub Data_Preview()

        Dim myDataset As New DataSet()
        Dim strConn As String = ""


        If LCase(Right(cbo_excel_files.Value, 3)) = "xls" Then
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 8.0"
        ElseIf LCase(Right(cbo_excel_files.Value, 4)) = "xlsx" Then
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 12.0"
        Else
            Exit Sub
        End If

        ''You must use the $ after the object you reference in the spreadsheet
        Dim sql As String = "SELECT * FROM [" & cbo_excel_worksheet.Value & "$]"
        Dim myData As New OleDbDataAdapter("SELECT * FROM [" & cbo_excel_worksheet.Value & "]", strConn)
        myData.TableMappings.Add("Table", "ExcelTest")
        myData.Fill(myDataset)

        ASPxGridView1.DataSource = myDataset.Tables(0).DefaultView
        ASPxGridView1.DataBind()

    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ASPxUploadControl1.FileUploadComplete

        If e.IsValid Then
            FileName = ASPxUploadControl1.UploadedFiles(0).PostedFile.FileName.ToString
            ASPxUploadControl1.UploadedFiles(0).PostedFile.SaveAs(MapPath("Uploads/" & FileName))
            Populate_Uploads()
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If HBCG_Utils.Find_Security("ITEM", Session("EMP_CD")) = "N" Then
            ASPxUploadControl1.Enabled = False
            cbo_excel_files.Enabled = False
            cbo_excel_worksheet.Enabled = False
            btn_preview.Enabled = False
            ASPxButton1.Enabled = False
            lbl_error.Text = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        Populate_Uploads()

    End Sub

    Public Sub Populate_Uploads()

        Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Uploads\")

        cbo_excel_files.Items.Clear()

        With cbo_excel_files
            .DataSource = dirInfo.GetFiles("*.xls*")
            .ValueField = "Name"
            .TextField = "Name"
            .DataBind()
        End With

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Protected Sub cbo_excel_files_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_excel_files.SelectedIndexChanged

        If cbo_excel_files.Value & "" <> "" Then
            cbo_excel_worksheet.Enabled = True
            Dim objExcelConn As New OleDb.OleDbConnection
            Dim i As Integer
            Dim sSName As String = ""
            Dim dtTables As DataTable

            If LCase(Right(cbo_excel_files.Value, 3)) = "xls" Then
                objExcelConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 8.0"
            ElseIf LCase(Right(cbo_excel_files.Value, 4)) = "xlsx" Then
                objExcelConn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 12.0"
            Else
                Exit Sub
            End If

            objExcelConn.Open()

            dtTables = objExcelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

            If dtTables.Rows.Count > 0 Then
                cbo_excel_worksheet.Items.Clear()

                With cbo_excel_worksheet
                    .DataSource = dtTables
                    .ValueField = "TABLE_NAME"
                    .TextField = "TABLE_NAME"
                    .DataBind()
                End With
            End If
        Else
            cbo_excel_worksheet.Enabled = False
        End If

    End Sub

    Protected Sub ASPxButton1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxButton1.Click


        If txt_fname_override.Text & "" = "" And cbo_fname.Value = "100" Then
            lbl_error.Text = "You must map a value to First Name to proceed."
            Exit Sub
        ElseIf txt_lname_override.Text & "" = "" And cbo_lname.Value = "100" Then
            lbl_error.Text = "You must map a value to Last Name to proceed."
            Exit Sub
        End If

        Dim conn2 As New OleDb.OleDbConnection
        If LCase(Right(cbo_excel_files.Value, 3)) = "xls" Then
            conn2.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 8.0"
        ElseIf LCase(Right(cbo_excel_files.Value, 4)) = "xlsx" Then
            conn2.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Request.ServerVariables("APPL_PHYSICAL_PATH") & "uploads\" & cbo_excel_files.Value & ";Extended Properties=Excel 12.0"
        Else
            Exit Sub
        End If

        Dim Null_Count As Double = 0

        Try
            ''You must use the $ after the object you reference in the spreadsheet
            Dim sql As String = "SELECT * FROM [" & cbo_excel_worksheet.Value & "]"
            Dim objsql As OleDb.OleDbCommand
            Dim MyDatareader2 As OleDb.OleDbDataReader
            'Set SQL OBJECT 
            objsql = New OleDb.OleDbCommand(sql, conn2)

            'Open Connection 
            conn2.Open()
            'Execute DataReader 
            MyDatareader2 = objsql.ExecuteReader

            'Store Values in String Variables 
            Do While MyDatareader2.Read()
                If Null_Count >= 3 Then
                    txt_log.Text = txt_log.Text & "Import found 3 consecutive empty rows, closing process."
                    Exit Do
                End If

                If UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString) & "" = "" And UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString) & "" = "" Then
                    Null_Count = Null_Count + 1
                Else
                    Null_Count = 0

                    Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

                    Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

                    Dim ds2 As New DataSet
                    Dim oAdp As OracleDataAdapter
                    Dim mytable As DataTable
                    Dim numrows As Integer
                    Dim search_criteria As Boolean = False

                    objConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                    objConnection.Open()
                    sql = "SELECT CUST_CD FROM CUST WHERE LNAME='" & Replace(UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString), "'", "") & "' "
                    If cbo_zip.Value <> "100" Then
                        If UCase(MyDatareader2(CInt(cbo_zip.Value)).ToString) & "" <> "" Then
                            sql = sql & "AND ZIP_CD='" & UCase(MyDatareader2(CInt(cbo_zip.Value)).ToString) & "' "
                            search_criteria = True
                        End If
                    End If
                    If cbo_addr1.Value <> "100" Then
                        If UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString) & "" <> "" And InStr(UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString), " ") > 0 Then
                            sql = sql & "AND SUBSTR(ADDR1,1,INSTR(ADDR1,' ')-1)='" & Microsoft.VisualBasic.Left(UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString), (InStr(UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString), " ") - 1)) & "'"
                            search_criteria = True
                        End If
                    End If
                    If search_criteria = False Then
                        sql = sql & "AND FNAME='" & Replace(UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString), "'", "") & "' "
                    End If

                    objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                    oAdp = DisposablesManager.BuildOracleDataAdapter(objsql2)
                    oAdp.Fill(ds2)
                    mytable = New DataTable
                    mytable = ds2.Tables(0)
                    numrows = mytable.Rows.Count
                    Dim addr1 As String = ""

                    If numrows <> 1 Then
                        Dim cust_cd As String = ""
                        If CInt(cbo_addr1.Value) = 100 Then
                            addr1 = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString) & "" <> "" Then
                                addr1 = Left(UCase(MyDatareader2(CInt(cbo_addr1.Value)).ToString), 30)
                            Else
                                addr1 = ""
                            End If
                        End If

                        cust_cd = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"), addr1, Left(UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString), 20),
                                                                  Left(UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString), 15))

                        sql = "INSERT INTO CUST (CUST_CD, FNAME, LNAME, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR, ACCT_OPN_DT, CUST_TP_CD) "
                        sql = sql & "VALUES(:CUST_CD, :FNAME, :LNAME, :ADDR1, :ADDR2, :CITY, :ST_CD, :ZIP_CD, :HOME_PHONE, :BUS_PHONE, :EMAIL_ADDR, SYSDATE, 'I')"

                        objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)

                        objsql2.Parameters.Add(":CUST_CD", OracleType.VarChar)
                        objsql2.Parameters(":CUST_CD").Value = cust_cd
                        objsql2.Parameters.Add(":FNAME", OracleType.VarChar)
                        objsql2.Parameters(":FNAME").Value = Left(UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString), 15)
                        objsql2.Parameters.Add(":LNAME", OracleType.VarChar)
                        objsql2.Parameters(":LNAME").Value = Left(UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString), 20)
                        objsql2.Parameters.Add(":ADDR1", OracleType.VarChar)
                        objsql2.Parameters(":ADDR1").Value = addr1
                        objsql2.Parameters.Add(":ADDR2", OracleType.VarChar)
                        If CInt(cbo_addr2.Value) = 100 Then
                            objsql2.Parameters(":ADDR2").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_addr2.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":ADDR2").Value = Left(UCase(MyDatareader2(CInt(cbo_addr2.Value)).ToString), 30)
                            Else
                                objsql2.Parameters(":ADDR2").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":CITY", OracleType.VarChar)
                        If CInt(cbo_city.Value) = 100 Then
                            objsql2.Parameters(":CITY").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_city.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":CITY").Value = Left(UCase(MyDatareader2(CInt(cbo_city.Value)).ToString), 20)
                            Else
                                objsql2.Parameters(":CITY").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":ST_CD", OracleType.VarChar)
                        If CInt(cbo_state.Value) = 100 Then
                            objsql2.Parameters(":ST_CD").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_state.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":ST_CD").Value = Left(UCase(MyDatareader2(CInt(cbo_state.Value)).ToString), 2)
                            Else
                                objsql2.Parameters(":ST_CD").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":ZIP_CD", OracleType.VarChar)
                        If CInt(cbo_zip.Value) = 100 Then
                            objsql2.Parameters(":ZIP_CD").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_zip.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":ZIP_CD").Value = Left(UCase(MyDatareader2(CInt(cbo_zip.Value)).ToString), 10)
                            Else
                                objsql2.Parameters(":ZIP_CD").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":HOME_PHONE", OracleType.VarChar)
                        If CInt(cbo_hphone.Value) = 100 Then
                            objsql2.Parameters(":HOME_PHONE").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_hphone.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":HOME_PHONE").Value = Left(UCase(MyDatareader2(CInt(cbo_hphone.Value)).ToString), 12)
                            Else
                                objsql2.Parameters(":HOME_PHONE").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":BUS_PHONE", OracleType.VarChar)
                        If CInt(cbo_bphone.Value) = 100 Then
                            objsql2.Parameters(":BUS_PHONE").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_bphone.Value).ToString)) & "" <> "" Then
                                objsql2.Parameters(":BUS_PHONE").Value = Left(UCase(MyDatareader2(CInt(cbo_bphone.Value)).ToString), 12)
                            Else
                                objsql2.Parameters(":BUS_PHONE").Value = ""
                            End If
                        End If
                        objsql2.Parameters.Add(":EMAIL_ADDR", OracleType.VarChar)
                        If CInt(cbo_email.Value) = 100 Then
                            objsql2.Parameters(":EMAIL_ADDR").Value = ""
                        Else
                            If UCase(MyDatareader2(CInt(cbo_email.Value)).ToString) & "" <> "" Then
                                objsql2.Parameters(":EMAIL_ADDR").Value = Left(UCase(MyDatareader2(CInt(cbo_email.Value)).ToString), 60)
                            Else
                                objsql2.Parameters(":EMAIL_ADDR").Value = ""
                            End If
                        End If

                        objsql2.ExecuteNonQuery()
                        objConnection.Close()
                        txt_log.Text = txt_log.Text & "Customer " & UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString) & " " & UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString) & " added (" & cust_cd & ")" & vbCrLf
                    Else
                        txt_log.Text = txt_log.Text & "Customer " & UCase(MyDatareader2(CInt(cbo_fname.Value)).ToString) & " " & UCase(MyDatareader2(CInt(cbo_lname.Value)).ToString) & " NOT ADDED" & vbCrLf
                    End If
                End If
            Loop
            MyDatareader2.Close()
        Catch
            Throw
        End Try

        'Close Connection 
        conn2.Close()

    End Sub
End Class
