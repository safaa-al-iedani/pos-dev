Imports System.Data.OracleClient

Partial Class Relationship_Comments
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lbl_REL_NO.Text = Request("DEL_DOC_NUM")
            Pull_Comments()
        End If

    End Sub

    Public Sub Pull_Comments()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDatareader
        Dim DEL_DOC_NUM As String

        DEL_DOC_NUM = Request("del_doc_num")

        'Open Connection 
        conn.Open()

        sql = "SELECT * FROM RELATIONSHIP_COMMENTS where REL_NO='" & DEL_DOC_NUM & "' ORDER BY SUBMIT_DT ASC"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                txt_past_Comments.Text = txt_past_Comments.Text & MyDataReader.Item("SUBMIT_DT") & ": " & MyDataReader.Item("COMMENTS") & vbCrLf & vbCrLf
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        txt_past_Comments.Enabled = False

        sql = "SELECT FOLLOW_UP_DT, COMMENTS FROM RELATIONSHIP where REL_NO='" & DEL_DOC_NUM & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                If IsDate(MyDataReader.Item("FOLLOW_UP_DT").ToString) Then
                    txt_follow_up_dt.Text = FormatDateTime(MyDataReader.Item("FOLLOW_UP_DT").ToString, DateFormat.ShortDate)
                End If
                txt_cust_past.Text = MyDataReader.Item("COMMENTS").ToString
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        txt_cust_past.Enabled = False

        'Close Connection 
        conn.Close()

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Response.Redirect("relationship_maintenance.aspx?query_returned=Y&del_doc_num=" & Request("del_doc_num"))

    End Sub

    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim DEL_DOC_NUM As String

        DEL_DOC_NUM = Request("del_doc_num")

        'Open Connection 
        conn.Open()

        If txt_comments.Text & "" <> "" Then
            sql = "INSERT INTO RELATIONSHIP_COMMENTS (REL_NO, FOLLOW_UP_DT, COMMENTS) "
            sql = sql & "VALUES('" & lbl_REL_NO.Text & "',TO_DATE('" & txt_follow_up_dt.Text & "','mm/dd/RRRR'),'"
            sql = sql & Replace(Replace(txt_comments.Text, "'", "''"), """", """""") & "')"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()
        End If

        sql = "UPDATE RELATIONSHIP SET FOLLOW_UP_DT = TO_DATE('" & txt_follow_up_dt.Text & "','mm/dd/RRRR'), "
        sql = sql & "COMMENTS='" & Replace(Replace(txt_cust_comments.Text & "  " & txt_cust_past.Text, "'", "''"), """", """""") & "' "
        sql = sql & "WHERE REL_NO='" & lbl_REL_NO.Text & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        conn.Close()
        txt_comments.Text = ""
        txt_past_Comments.Text = ""
        txt_cust_past.Text = ""
        txt_cust_comments.Text = ""
        Pull_Comments()

    End Sub

End Class
