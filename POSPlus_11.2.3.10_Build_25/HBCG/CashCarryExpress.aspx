<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CashCarryExpress.aspx.vb" Inherits="CashCarryExpress" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Src="~/Usercontrols/MultiWarranties.ascx" TagPrefix="ucWar" TagName="MultiWarranties" %>
<%@ Register Src="~/Usercontrols/RelatedSKU.ascx" TagPrefix="ucRelSku" TagName="RelatedSKU" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="Scripts/jquery.unobtrusive-ajax.js"></script>
    <title>Cash and Carry Express</title>
</head>
<body>
    <script language="javascript" type="text/javascript">
        function SelectAndClosePopup3() {
            ASPxPopupControl3.Hide();
        }
        function SelectAndClosePopup1() {
            ASPxPopupControl5.Hide();
        }
        function WarrantySelect(s, e) {
            if (s.GetChecked()) {
                var isOne2ManyWarr = "<%=SysPms.isOne2ManyWarr%>";
                if (!isOne2ManyWarr) {
                    s.SetChecked(false);
                }
                else {
                    __doPostBack(s.name, s.GetChecked());
                }
            }
            else {
                __doPostBack(s.name, s.GetChecked());
            }
        }
    </script>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td align="left" valign="top" width="80%">
                    <div style="padding-left: 8px; padding-top: 8px;">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <a href="newmain.aspx">
                                        <img src="<% = ConfigurationManager.AppSettings("company_logo").ToString %>" /></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </td>
                <td align="right" valign="top" nowrap>
                    <div style="padding-right: 13px; padding-top: 8px">
                        <dx:ASPxLabel ID="lbl_Header1" runat="server" Text="" Font-Size="Medium">
                        </dx:ASPxLabel>
                        <br />
                        <dx:ASPxLabel ID="lbl_Header2" runat="server" Text="" Font-Size="Medium">
                        </dx:ASPxLabel>
                    </div>
                </td>
                <td align="right" valign="top">
                    <div style="padding-right: 13px; padding-top: 8px">
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server" Font-Size="Large" Height="61px" HorizontalAlign="Center"
                            VerticalAlign="Middle">
                            <Items>
                                <dx:MenuItem Text="Logout" NavigateUrl="logout.aspx">
                                    <Image Url="~/images/login_icon.gif" Width="25px">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                </td>
            </tr>
        </table>
        <table width="100%" height="100%">
            <tr>
                <td valign="top">
                    <asp:HiddenField ID="hidItemCd" runat="server" />
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ContentHeight="100%" Height="100%"
                        Width="100%">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <dx:ASPxGridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                                    KeyFieldName="row_id" OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="row_id" FieldName="row_id" VisibleIndex="0"
                                            Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="itm_cd" FieldName="itm_cd" VisibleIndex="1"
                                            Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="ITM_TP_CD" Caption="ITM_TP_CD" VisibleIndex="2" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="bulk_tp_itm" FieldName="bulk_tp_itm" VisibleIndex="3"
                                            Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="serial_tp" FieldName="serial_tp" VisibleIndex="4"
                                            Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="" VisibleIndex="6" Width="100%">
                                            <DataItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <th>VSN</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Qty</th>
                                                        <th>Price</th>
                                                        <th>Warr</th>
                                                        <th>Remove</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="left" valign="top" width="150">
                                                                            <asp:Image ID="img_picture" runat="server" Height="100px" ImageUrl="~/images/image_not_found.jpg"
                                                                                Width="133px" />
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vsn") %>'></asp:Label>
                                                                            &nbsp;&nbsp;
                                                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'></asp:Label>
                                                                            <br />
                                                                            SKU:
                                                                        <asp:Label ID="lbl_SKU" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itm_cd") %>'></asp:Label>
                                                                            &nbsp; &nbsp; &nbsp;&nbsp; Vendor:
                                                                        <asp:Label ID="lbl_ve_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ve_cd") %>'></asp:Label>
                                                                            <br />
                                                                            <asp:Label ID="lbl_warr" runat="server" Text='Warr SKU:' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbl_warr_sku" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.warr_itm_link") %>'></asp:Label>
                                                                            <asp:Label ID="lbl_warrable" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.warrantable") %>'></asp:Label>
                                                                            <%--<asp:Label ID="lbl_warrCb" runat="server" Text="Warr:" Visible="true"></asp:Label>
                                                                            <dx:ASPxCheckBox  ID="SelectWarr" runat="server" Enabled="true" AutoPostBack="false" OnCheckedChanged="CheckWarrClicked"
                                                                               recursive="false" width="50px"></dx:ASPxCheckBox>--%>
                                                                            <br />
                                                                            <asp:ImageButton ID="img_po" runat="server" CommandName="remove_po" Height="20" ImageUrl="images/icons/symbol-delete.png"
                                                                                Visible="False" Width="20" /><asp:Label ID="lbl_PO" runat="server" Text=''></asp:Label>
                                                                            <br />
                                                                            <asp:ImageButton ID="img_gm" runat="server" CommandName="find_gm" Height="20" ImageUrl="images/icons/money.gif"
                                                                                Visible="True" Width="20" />
                                                                            <br />
                                                                            <asp:Label ID="lbl_carton" runat="server" Text="Leave In Carton:" Visible="false"></asp:Label>
                                                                            <asp:CheckBox ID="chk_carton" runat="server" AutoPostBack="true" OnCheckedChanged="Carton_Update"
                                                                                Visible="false" />
                                                                            <div id="slsp_section" runat="server" visible="False">
                                                                                <br />
                                                                                Slsp1
                                                                            <br />
                                                                                <asp:DropDownList ID="cbo_slsp1" runat="server" CssClass="style5" OnSelectedIndexChanged="Salesperson_Update"
                                                                                    Width="180px">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="slsp1_pct" runat="server" AutoPostBack="true" CssClass="style5"
                                                                                    OnTextChanged="txt_pct_1_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.slsp1") %>'
                                                                                    Width="25"></asp:TextBox>%
                                                                            <br />
                                                                                Slsp2
                                                                            <br />
                                                                                <asp:DropDownList ID="cbo_slsp2" runat="server" CssClass="style5" OnSelectedIndexChanged="Salesperson_Update"
                                                                                    Width="180px">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="slsp2_pct" runat="server" AutoPostBack="true" CssClass="style5"
                                                                                    OnTextChanged="txt_pct_2_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.slsp2") %>'
                                                                                    Width="25"></asp:TextBox>%
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="txt_line_desc" runat="server" AutoPostBack="true" CssClass="style5"
                                                                                Height="50px" OnTextChanged="Line_Update" TextMode="MultiLine" Visible="False"
                                                                                Width="100%"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:Label ID="lbl_itm_warning" runat="server" Font-Size="Smaller" ForeColor="#C0C000"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="Image1" runat="server" CommandName="inventory_lookup" Height="20"
                                                                ImageUrl="images/icons/DocumentsBlack.PNG" Width="20" Visible="False" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="Image2" runat="server" CommandName="find_inventory" Height="20"
                                                                ImageUrl="images/icons/compare_inventory.gif" Width="20" Visible="False" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="store_cd" runat="server" AutoPostBack="true" Font-Size="Large" OnTextChanged="Store_Update"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.store_cd") %>' Width="20" Visible="False"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="loc_cd" runat="server" AutoPostBack="true" Font-Size="Large" OnTextChanged="Loc_Update"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.loc_cd") %>' Width="40" Visible="False"></asp:TextBox>
                                                        </td>
                                                        <td valign="top" width="60px">
                                                            <asp:TextBox ID="qty" runat="server" AutoPostBack="true" OnTextChanged="Qty_Update"
                                                                Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.qty"),0) %>' Width="23px"></asp:TextBox>
                                                        </td>
                                                        <td valign="top" width="100px">
                                                            <asp:TextBox ID="ret_prc" runat="server" AutoPostBack="true" OnTextChanged="Price_Update"
                                                                Text='<%# Replace(FormatNumber(DataBinder.Eval(Container, "DataItem.ret_prc"),2),",","") %>'
                                                                Width="45px"></asp:TextBox>
                                                        </td>
                                                        <td valign="top" width="50px">
                                                            <%--<asp:Label ID="lbl_warrCb" runat="server" Text="Warr:" Visible="true"></asp:Label>--%>
                                                            <%--<dx:aspxCheckBox ID="SelectWarr" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="CheckWarrClicked"
                                                            width="50px" CssClass="style5" Onload="prepWarrChkBox" ></dx:aspxCheckBox>--%>
                                                            <dx:ASPxCheckBox ID="SelectWarr" runat="server" Enabled="true" AutoPostBack="false"
                                                                ClientSideEvents-CheckedChanged="WarrantySelect">
                                                            </dx:ASPxCheckBox>
                                                            <asp:HiddenField ID="prevWarrCb" runat="server" />
                                                        </td>
                                                        <td valign="top" width="50px">
                                                            <asp:ImageButton ID="img_delete" runat="server" Style="cursor: pointer;" OnClick="Delete_Items"
                                                                ImageUrl="images/icons/Trashcan_30.png" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsPager Mode="ShowAllRecords">
                                    </SettingsPager>
                                    <Settings GridLines="Horizontal" ShowColumnHeaders="False" VerticalScrollableHeight="450"
                                        ShowVerticalScrollBar="True" />
                                    <SettingsText EmptyDataRow="Please scan or enter your first item" />
                                    <Styles>
                                        <AlternatingRow Enabled="True">
                                        </AlternatingRow>
                                    </Styles>
                                </dx:ASPxGridView>
                            </dx:PanelContent>
                        </PanelCollection>
                        <HeaderTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="500px" DefaultButton="cmd_add" Height="30px">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <dx:ASPxTextBox ID="txt_SKU" runat="server" Width="270px" Height="32px" Font-Size="Large">
                                            </dx:ASPxTextBox>
                                            &nbsp;
                                        </td>
                                        <td valign="top">
                                            <dx:ASPxButton ID="cmd_add" runat="server" Text="Add Item" Height="26px" OnClick="cmd_add_Click"
                                                Font-Size="Large" Width="100px">
                                            </dx:ASPxButton>
                                        </td>
                                        <td valign="top">
                                            <asp:ImageButton ID="ImageButton1" runat="server" Height="34px" ImageUrl="~/images/search-icon.png"
                                                Width="36px" OnClick="ImageButton1_Click" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lbl_warnings" runat="server" ForeColor="#FF0066" Text="" Width="300px">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </HeaderTemplate>
                    </dx:ASPxRoundPanel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu6" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Credit Card">
                                            <Image Width="50px" Url="~/images/icons/creditcard.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu7" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Check">
                                            <Image Width="50px" Url="~/images/icons/check.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu1" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Cash/Other">
                                            <Image Width="50px" Url="~/images/icons/cash.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu3" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Gift Card">
                                            <Image Width="50px" Url="~/images/icons/giftcard.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu8" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Finance">
                                            <Image Width="50px" Url="~/images/icons/finance.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu4" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Cancel">
                                            <Image Width="50px" Url="~/images/icons/banned.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                            <td>
                                <dx:ASPxMenu ID="ASPxMenu5" runat="server" Font-Size="Large" Height="111px" VerticalAlign="Middle"
                                    HorizontalAlign="Right" Width="181px">
                                    <Items>
                                        <dx:MenuItem Text="Checkout">
                                            <Image Width="50px" Url="~/images/icons/tick.png">
                                            </Image>
                                        </dx:MenuItem>
                                    </Items>
                                    <ItemStyle VerticalAlign="Middle" />
                                </dx:ASPxMenu>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 1453px">
                    <div id="payment_header" visible="false" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Payment Amount: ">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox runat="server" ID="lbl_pmt_amt" Width="170px">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxComboBox ID="mop_drp" runat="server" IncrementalFilteringMode="StartsWith">
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="check_details" visible="false" runat="server">
                        <table>
                            <tr>
                                <td>Check #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_check" runat="server" CssClass="style5" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Bank Routing #:
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="style5" MaxLength="9" Width="194px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="TextBox3" runat="server" CssClass="style5" MaxLength="25" Width="194px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Type:
                                </td>
                                <td align="left">&nbsp;<asp:DropDownList ID="DropDownList2" runat="server" CssClass="style5">
                                    <asp:ListItem Selected="True" Value="01">Personal</asp:ListItem>
                                    <asp:ListItem Value="90">Business</asp:ListItem>
                                </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Approval #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>DL State:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_dl_state" runat="server" CssClass="style5" MaxLength="2" Width="20px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>DL #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_dl_number" runat="server" CssClass="style5" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lbl_chk_warnings" runat="server" CssClass="style5" Font-Bold="True"
                            ForeColor="Red"></asp:Label>
                        <br />
                        <asp:Button ID="btn_save_chk" runat="server" CssClass="style5" Text="Save Check Details" />
                        <br />
                        <br />
                    </div>
                    <div id="finance_details" visible="false" runat="server">
                        <table>
                            <tr>
                                <td style="height: 24px"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                                </td>
                                <td style="height: 24px">
                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5">
                                        <asp:ListItem Value="O">Open</asp:ListItem>
                                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                                        <asp:ListItem Value="I">Installment</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_finance_company" runat="server" Width="318px"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Promotion Plan:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_promo" runat="server" Width="318px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_fi_acct" runat="server"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label192 %>" />:
                                <asp:TextBox ID="txt_fi_exp_dt" runat="server" CssClass="style5" MaxLength="4" Width="40px"></asp:TextBox>
                                    &nbsp;MMYY
                                </td>
                            </tr>
                            <tr>
                                <td>Approval #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_fi_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;CVV: &nbsp; &nbsp;
                                <asp:TextBox ID="txt_fi_sec_cd" runat="server" CssClass="style5" MaxLength="10" Width="33px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>DL State:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_fi_dl_st" runat="server" CssClass="style5" MaxLength="2" Width="20px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>DL #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_fi_dl_no" runat="server" CssClass="style5" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Save Finance" />
                        <asp:Label ID="lbl_fi_warning" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                        <asp:Label ID="lbl_GE_tran_id" runat="server" CssClass="style5" Visible="False"></asp:Label>
                    </div>
                    <div id="cash_details" visible="false" runat="server">
                        <table>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btn_cash_submit" runat="server" Text="Save Payment" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="gift_Card_Details" visible="false" runat="server">
                        <table class="style5">
                            <tr>
                                <td align="left">
                                    <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Card Number:">
                                    </dx:ASPxLabel>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txt_gc" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"
                                        OnTextChanged="txt_gc_TextChanged"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <br />
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btn_gc_Submit" runat="server" Text="Save">
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                    <dx:ASPxLabel ID="ASPxLabel15" runat="server" Width="100%">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="cc_details" visible="false" runat="server">
                        <table>
                            <tr>
                                <td valign="middle" align="center">
                                    <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr>
                                            <td colspan="7" align="left">
                                                <dx:ASPxLabel ID="lbl_amount" runat="server" Text="" Width="100%" Font-Bold="True">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="left">
                                                <br />
                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Credit Card: ">
                                                </dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lbl_card_type" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="left">
                                                <asp:TextBox ID="TextBox1" runat="server" Width="194px" BorderColor="#999999" BorderStyle="Solid"
                                                    BorderWidth="1px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="Label2" runat="server" Text="Exp Date: ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="29px" MaxLength="2">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>/
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" Width="29px" MaxLength="2">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<dx:ASPxLabel ID="ASPxLabel9" runat="server"
                                                Text="SID: ">
                                            </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_security_code" runat="server" Width="40px" MaxLength="4">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="center">
                                                <dx:ASPxLabel ID="lbl_Full_Name" runat="server" Width="202px">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <dx:ASPxButton ID="btn_save" runat="server" Text="Save" Width="100px" UseSubmitBehavior="False">
                                                </dx:ASPxButton>
                                            </td>
                                            <td colspan="4">
                                                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Clear" Width="100px">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="center">
                                                <dx:ASPxLabel ID="lbl_tran_id" runat="server" Visible="False">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Manual Authorization:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="3" align="left">
                                                <dx:ASPxTextBox ID="txt_auth" runat="server" MaxLength="6" Width="49px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Please swipe or enter the credit card information and click Save when finished"
                                        Width="100%" Font-Size="XX-Small">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Label ID="lbl_slsp_breakout" runat="server" Text="Show Salesperson Breakout:"
                        Visible="False"></asp:Label>
                    <asp:CheckBox ID="chk_slsp_view_all" runat="server" AutoPostBack="true" OnCheckChanged="Carton_Update"
                        OnCheckedChanged="chk_slsp_view_all_CheckedChanged" Visible="False" />
                    <dx:ASPxButton ID="btn_total_margins" runat="server" Text="Show Total Margins" Visible="False"
                        OnClick="Button1_Click">
                    </dx:ASPxButton>
                    <asp:Label ID="lbl_Status" runat="server" Width="100%"></asp:Label>
                    <br />
                    <asp:Label ID="lbl_Follow_Up" runat="server" Width="100%"></asp:Label>
                    <asp:TextBox ID="txt_Follow_up" runat="server" Height="51px" Width="98%" Rows="3"
                        TextMode="MultiLine" Font-Names="tahoma" CssClass="style5"></asp:TextBox>
                    <br />
                    <asp:Label ID="lbl_fin_co" runat="server" Visible="False" Width="1px"></asp:Label>
                    <asp:Label ID="lbl_default_invoice" runat="server" Visible="False" Width="1px"></asp:Label>
                    <asp:Label ID="lbl_del_doc_num" runat="server" Visible="False" Width="1px"></asp:Label>
                    <asp:Label ID="lbl_split" runat="server" Visible="False" Width="1px"></asp:Label>
                    <asp:Label ID="lbl_fin" runat="server" Visible="False" Width="1px"></asp:Label>
                    <asp:Label ID="lbl_wdr" runat="server" Visible="False" Width="1px"></asp:Label><br />
                </td>
                <td>
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <div style="display: table; width: 100%; text-align: right;">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; width: 65%;">
                                            <dx:ASPxLabel runat="server" Text="Sub-Total:" Font-Size="Large"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lblSubtotal" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; text-align: right;">
                                            <div style="display: inline-block; width: 100%; margin-right: 0px; padding-right: 0px;">
                                                <table cellpadding="0" cellspacing="0" style="float: right;">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxLabel runat="server" Text="Tax" Font-Size="Large"></dx:ASPxLabel>
                                                        </td>
                                                        <td id="tdTaxCd" runat="server" style="text-wrap: avoid;">
                                                            <dx:ASPxLabel runat="server" Text="(" Style="padding-left: 5px;" Font-Size="Large"></dx:ASPxLabel>
                                                            <asp:LinkButton ID="lnkModifyTax" runat="server" Font-Size="Large" ToolTip="Click to modify the tax code.">
                                                                <%= If(IsNothing(Session("TAX_CD")), "None", If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString))%>
                                                            </asp:LinkButton>
                                                            <dx:ASPxLabel runat="server" Text=")" Font-Size="Large"></dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel runat="server" Text=":" Font-Size="Large"></dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lblTax" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                    <div style="display: table-row;">
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel runat="server" Text="Delivery:" Font-Size="Large"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lblDelivery" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div style="display: table; width: 100%; text-align: right;">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; width: 65%;">
                                            <dx:ASPxLabel runat="server" Text="Grand Total:" Font-Size="Large"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lblTotal" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                    <div style="display: table-row;">
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel runat="server" Text="Payments:" Font-Size="Large"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lbl_Payments" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                    <div style="display: table-row;">
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel runat="server" Text="Balance:" Font-Size="Large"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell;">
                                            <dx:ASPxLabel ID="lbl_balance" runat="server" Font-Size="Large"
                                                Text='<%= System.Globalization.RegionInfo.CurrentRegion.CurrencySymbol + "0.00" %>'>
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                            </dx:PanelContent>
                        </PanelCollection>
                        <HeaderTemplate>
                            <div style="padding-left: 13px; padding-top: 4px">
                                &nbsp;
                            </div>
                        </HeaderTemplate>
                    </dx:ASPxRoundPanel>
                </td>
            </tr>
        </table>
        <br />
        <dx:ASPxLabel ID="lbl_questions" runat="server" EncodeHtml="False" Text="" Width="100%">
        </dx:ASPxLabel>
        <asp:CustomValidator ID="vld_price" runat="server" ErrorMessage="CustomValidator"
            Visible="false">
        </asp:CustomValidator><br />

        <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
        <asp:HiddenField ID="hidIsWarrChecked" runat="server" />
        <ucWar:MultiWarranties runat="server" ID="ucMultiWarranties" ClientIDMode="Static" />
        <ucRelSku:RelatedSKU runat="server" ID="ucRelatedSKU" ClientIDMode="Static" />

        <dxpc:ASPxPopupControl ID="ASPxPopupControl6" runat="server" CloseAction="CloseButton"
            HeaderText="Gross Margin" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="317px">
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <asp:Label ID="lbl_gm" runat="server"></asp:Label>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
            HeaderText="Approval Password" Height="179px" Modal="True" PopupAction="None"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowCloseButton="False"
            Width="370px">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl7" runat="server" CloseAction="None" HeaderText="Select Serial Number"
            Height="179px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" ShowCloseButton="False" Width="370px">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <dx:ASPxLabel ID="lbl_ser_store_cd" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lbl_ser_loc_cd" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lbl_ser_row_id" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exit Without Selecting Serial #">
                    </dx:ASPxButton>
                    <dx:ASPxLabel ID="lbl_ser_itm_cd" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="346px">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="SER_NUM" VisibleIndex="0" Visible="False" Caption="Serial Number">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="" VisibleIndex="0">
                                <DataItemTemplate>
                                    &nbsp;<dx:ASPxHyperLink ID="hpl_serial" runat="server" Text=" Select" Visible="True">
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsPager Mode="ShowAllRecords" RenderMode="Lightweight" Visible="False">
                        </SettingsPager>
                        <Settings ShowVerticalScrollBar="True"></Settings>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_ser_err" runat="server" Text="" Visible="True" Width="100%">
                    </dx:ASPxLabel>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
            HeaderText="Related SKUS" Height="340px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="680px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" CloseAction="CloseButton"
            HeaderText="SKU Lookup" Height="340px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="650px" ContentUrl="Order.aspx?minimal=true&referrer=CashCarryExpress.aspx">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl5" runat="server" CloseAction="CloseButton"
            HeaderText="Related SKUS" Height="340px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="680px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="PopupTax" runat="server" CloseAction="CloseButton"
            HeaderText="Tax Update" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <div style="width: 100%; height: 420px; overflow-y: scroll;">
                        <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
    </form>
</body>
</html>
