<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Reporting.aspx.vb" Inherits="Reporting" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" class="style5">
        <tr>
            <td width="50%">
                <u>
                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Sales">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
            <td>
                <u>
                    <dx:ASPxMenu ID="ASPxMenu4" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Relationships">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink10" runat="server" NavigateUrl="Reports/SalesReport.aspx"
                    Text="Sales Order Report">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink9" runat="server" NavigateUrl="Reports/Best_Sellers.aspx"
                    Text="Top 10 Analysis">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink8" runat="server" NavigateUrl="Reports/Vendor_analysis.aspx"
                    Text="Vendor Analysis">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink20" NavigateUrl="Business_Review.aspx" runat="server"
                    Text="Business Review" Visible="true">
                </dx:ASPxHyperLink>
                <br />
            </td>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink6" runat="server" NavigateUrl="Reports/CRMPreview.aspx"
                    Text="CRM Preview">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink7" runat="server" NavigateUrl="Reports/CRMReport.aspx"
                    Text="CRM Report">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink12" runat="server" NavigateUrl="Relationship_Reporting.aspx"
                    Text="Relationship Reporting" Visible="true">
                </dx:ASPxHyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <u>
                    <dx:ASPxMenu ID="ASPxMenu2" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Payment">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
            <td>
                <u>
                    <dx:ASPxMenu ID="ASPxMenu5" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Kiosks">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink5" runat="server" NavigateUrl="Reports/CashDrawerReport.aspx"
                    Text="Daily Reconciliation">
                </dx:ASPxHyperLink>
                <br />
                <%--<a href="Reports/SalesRegisterReport.aspx" class="style5">Sales Register Report</a><br />--%>
                <dx:ASPxHyperLink ID="ASPxHyperLink4" runat="server" NavigateUrl="Reports/CashReceipts.aspx"
                    Text="Daily Cash Receipts">
                </dx:ASPxHyperLink>
                <br />
            </td>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" NavigateUrl="KioskAnalysis.aspx"
                    Text="Kiosk Analysis">
                </dx:ASPxHyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <u>
                    <dx:ASPxMenu ID="ASPxMenu3" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Credit">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
            <td valign="top">
                <u>
                    <dx:ASPxMenu ID="ASPxMenu6" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Admin">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink2" runat="server" NavigateUrl="Reports/QuickScreen.aspx"
                    Text="Quick Screen Reports">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink3" runat="server" NavigateUrl="QS_Update.aspx"
                    Text="Quick Screen Update">
                </dx:ASPxHyperLink>
                <br />
            </td>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink11" runat="server" NavigateUrl="Email_TransactionLogs.aspx"
                    Text="Email Audit Logs">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink13" runat="server" NavigateUrl="Reports/Audit_Logs.aspx"
                    Text="System Audit Logs">
                </dx:ASPxHyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <u>
                    <dx:ASPxMenu ID="ASPxMenu7" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="Settlement">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
            <td valign="top">
                <u>
                    <dx:ASPxMenu ID="ASPxMenu8" runat="server" Width="149px">
                        <Items>
                            <dx:MenuItem Text="">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </u>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <dx:ASPxHyperLink ID="ASPxHyperLink14" runat="server" NavigateUrl="Settlement_Balancing.aspx"
                    Text="Balancing">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="ASPxHyperLink15" runat="server" NavigateUrl="Settlement_Batch_History.aspx"
                    Text="Batch History">
                </dx:ASPxHyperLink>
                <br />
                 <dx:ASPxHyperLink ID="ASPxHyperLink16" runat="server" NavigateUrl="Settlement_Funding.aspx"
                    Text="Funding">
                </dx:ASPxHyperLink>
                <br />
                 <dx:ASPxHyperLink ID="ASPxHyperLink17" runat="server" NavigateUrl="Settlement_Promo_Plans.aspx"
                    Text="Promotion Plan Maintenance">
                </dx:ASPxHyperLink>
                <br />
                 <dx:ASPxHyperLink ID="ASPxHyperLink18" runat="server" NavigateUrl="Settlement_Release_Hold.aspx"
                    Text="Release Hold">
                </dx:ASPxHyperLink>
                <br />
            </td>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <%--<br />
    <a href="Picture_Upload.aspx" class="style5">Upload Images</a>
    <br />
    <br />--%>
    <%--<a href="Campaign_main.aspx" class="style5">Launch Campaign</a>--%>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
