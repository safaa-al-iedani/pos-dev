Imports System.Data.OracleClient
Imports ErrorManager

Partial Class NoWizard2
    Inherits System.Web.UI.MasterPage

    'MASTER FOR all Non-entry (Regular master for entry; NoWizardNoAjax for file uploads)

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("EMP_CD") & "" = "" Then
            ''mm - Sep 20, 2016 - backbutton security concern
            ClearCache()
            Response.Redirect("login.aspx")
        Else
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetNoStore()
        End If
        'mm end

        If isEmpty(Session("str_sup_user_flag")) Then
            Dim str_co_grp_cd As String
            Dim str_co_cd As String
            Dim str_sup_user_flag As String
            Dim str_flag As String

            If (isNotEmpty(Session("EMP_CD"))) Then
                str_flag = LeonsBiz.lucy_check_sup_user(UCase(Session("EMP_CD")), str_co_grp_cd, str_co_cd, str_sup_user_flag) 'lucy use the function even not check super user
            End If
            ' Daniela Nov 7, 2014 Hide if not super user
            If isNotEmpty(str_sup_user_flag) Then
                Session("str_sup_user_flag") = str_sup_user_flag
            Else
                Session("str_sup_user_flag") = "N"
            End If
        End If

        If Session("str_sup_user_flag") = "N" Then
            ' Daniela start
            'ASPxMenu1.Items.FindByText("Reporting").ClientVisible = False
            'ASPxMenu1.Items.FindByText("Utilities").ClientVisible = False
            ASPxMenu1.Items.FindByText(Resources.LibResources.Label469).ClientVisible = False
            ASPxMenu1.Items.FindByText(Resources.LibResources.Label639).ClientVisible = False
            ' Daniela end
            reporting.Visible = False
            utilities.Visible = False
        End If

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
        End If

        img_logo.ImageUrl = "~/" & ConfigurationManager.AppSettings("company_logo").ToString
        'Daniela move up
        'If Session("EMP_CD") & "" = "" Then
        '    Response.Redirect("login.aspx")
        'End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "* TRAIN MODE *"
            lbl_header.ForeColor = Color.Red
        End If


        If Not IsPostBack Then
            Dim sql As String
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            If Session("EMP_FNAME") & "" = "" Or Session("EMP_LNAME") & "" = "" Then
                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If

                sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("EMP_CD") = MyDataReader.Item("EMP_CD").ToString
                        Session("EMP_FNAME") = MyDataReader.Item("FNAME").ToString
                        Session("EMP_LNAME") = MyDataReader.Item("LNAME").ToString
                        Session("HOME_STORE_CD") = MyDataReader.Item("HOME_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                sql = "SELECT SHIP_TO_STORE_CD FROM STORE WHERE STORE_CD='" & UCase(Session("HOME_STORE_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("pd_store_cd") = MyDataReader.Item("SHIP_TO_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            End If
            If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
                ' Daniela french
                'lbl_Header1.Text = "Welcome " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
                lbl_Header1.Text = Resources.LibResources.Label660 & " " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            End If
            If Session("HOME_STORE_CD") & "" <> "" Then
                lbl_Header2.Text = Resources.LibResources.Label573 & ": " & Session("HOME_STORE_CD")
            End If
            lbl_sysDt.Text = FormatDateTime(Today, DateFormat.ShortDate).ToString

            Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = arpMain.FindControl("lbl_Round_Header")
            Dim _hplexit As HyperLink = arpMain.FindControl("hpl_exit")

            If Request("D") = "D" Then
                _lblHeader.Text = Session("MP")
            End If
            If Request("D") = "I" Then
                _lblHeader.Text = Session("IP")
            End If

            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink1").ToString) Then
                ASPxMenu1.RootItem.Items(4).Text = ConfigurationManager.AppSettings("link1_desc").ToString
                ASPxMenu1.RootItem.Items(4).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink1").ToString
                ASPxMenu1.RootItem.Items(4).Target = "_new"
            End If
            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink2").ToString) Then
                ASPxMenu1.RootItem.Items(5).Text = ConfigurationManager.AppSettings("link2_desc").ToString
                ASPxMenu1.RootItem.Items(5).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink2").ToString
                ASPxMenu1.RootItem.Items(5).Target = "_new"
            End If

            Dim strURL As String
            Dim arrayURL As Array
            Dim pagename As String

            strURL = Request.ServerVariables("SCRIPT_NAME")
            arrayURL = Split(strURL, "/", -1, 1)
            pagename = arrayURL(UBound(arrayURL))

            Dim pgInfo As SystemUtils.PageInfo = SystemUtils.GetPageInfo(pagename, Session("emp_cd"))

            If pgInfo.allowPageEntry Then

                _lblHeader.Text = pgInfo.label
                Page.Title = pgInfo.title
                If pgInfo.hyPrLnk.NavigateUrl.isNotEmpty Then

                    _hplexit.NavigateUrl = pgInfo.hyPrLnk.NavigateUrl
                    _hplexit.Visible = pgInfo.hyPrLnk.Visible

                    If pgInfo.hyPrLnk.Text.isNotEmpty Then
                        _hplexit.Text = pgInfo.hyPrLnk.Text
                    End If
                End If

            Else
                Response.Redirect("newmain.aspx")
            End If

            ' Did not resolve a nice way to handle this yet
            If LCase(pagename) = "independentpaymentprocessing.aspx" Then
                '    lblTabID.Text = "PAYMENT PROCESSING"
                '    Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payment Processing"
                '    'ASPxMenu1.RootItem.Items(0).Text = ""
                '    'ASPxMenu1.RootItem.Items(0).NavigateUrl = ""
                '    'ASPxMenu1.RootItem.Items(1).Text = ""
                '    'ASPxMenu1.RootItem.Items(1).NavigateUrl = ""
                '    'ASPxMenu1.RootItem.Items(2).Text = ""
                '    'ASPxMenu1.RootItem.Items(2).NavigateUrl = ""
                ' Daniela April 1 comment
                'ASPxMenu1.RootItem.Items(3).Text = "Quick Screen"
                'ASPxMenu1.RootItem.Items(3).NavigateUrl = "GEQuickCredit.aspx"
            End If
        End If

    End Sub

    Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton concern
        Session.Abandon()
        Response.ClearHeaders()
        Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
        Response.AddHeader("Pragma", "no-cache")

        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
    End Sub

End Class

