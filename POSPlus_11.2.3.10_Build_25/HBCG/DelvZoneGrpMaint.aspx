<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="DelvZoneGrpMaint.aspx.vb" Inherits="DelvZoneGrpMaint" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>

    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <%--<td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
            </td>--%>
            <td style="width: 338px">
                     <asp:DropDownList ID="txt_zone_grp" runat="server"  Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
          </td>
        </tr>
        <tr>
            <%--<td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
            </td>--%>
            <td style="width: 338px">
                <asp:DropDownList ID="txt_zone_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <%--<td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" meta:resourcekey="Label3">
                </dx:ASPxLabel>
            </td>--%>
            <td style="width: 338px">
            <asp:DropDownList ID="txt_distr_centre" runat="server"  Style="position: relative" AppendDataBoundItems="true">
           </asp:DropDownList>
           </td>
        </tr>
         <tr>
            <%--<td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" meta:resourcekey="Label3">
                </dx:ASPxLabel>
            </td>--%>
            <td style="width: 338px">
           <asp:DropDownList ID="txt_store_grp_cd" runat="server"  Style="position: relative" AppendDataBoundItems="true">
           </asp:DropDownList>
           </td>
        </tr>
    </table>
    <br />
     <asp:HiddenField ID="hf_store_grp_cd" runat="server" />
   
    
    <dx:ASPxGridView ID="GridView2" runat="server" keyfieldname="ZONE_GRP;ZONE_CD;DAY;TIMESLOT;DISTR_CENTRE;ROW_ID" AutoGenerateColumns="False" Width="98%" ClientInstanceName="GridView2" OnInitNewRow="GridView2_InitNewRow" oncelleditorinitialize="GridView2_CellEditorInitialize" OnBatchUpdate="GridView2_BatchUpdate" OnParseValue="GridView2_ParseValue">
         <SettingsEditing Mode="Batch" />
         <SettingsCommandButton>
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>" ></UpdateButton>
         <CancelButton Text="<%$ Resources:LibResources, Label886 %>"></CancelButton>
         <NewButton Text="<%$ Resources:LibResources, Label328 %>"></NewButton>
         <DeleteButton Text="<%$ Resources:LibResources, Label150 %>"></DeleteButton>
        </SettingsCommandButton>
        <Columns>
             <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label708 %>" FieldName="STORE_GRP_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="3">
                    <ValidationSettings>
                        <RequiredField IsRequired="True"  ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label901 %>" FieldName="ZONE_GRP" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="5">
                    <ValidationSettings>
                        <RequiredField IsRequired="True"  ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="^[a-zA-Z0-9]{5,}$" ErrorText="<%$ Resources:LibResources, Label910 %>"  />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label902 %>" FieldName="ZONE_CD" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="3">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label903 %>" FieldName="DAY" VisibleIndex="3">
                <PropertiesTextEdit MaxLength="1">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="^[1-9]" ErrorText="<%$ Resources:LibResources, Label910 %>" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label904 %>" FieldName="TIMESLOT" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="2">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                         <RegularExpression ValidationExpression="^[0-9]{0,2}$" ErrorText="<%$ Resources:LibResources, Label910 %>"  />
                    </ValidationSettings>
                </PropertiesTextEdit> 
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label905 %>" FieldName="DISTR_CENTRE" VisibleIndex="5">
                <PropertiesTextEdit MaxLength="3">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn visible="false"  FieldName="ROW_ID" >
                           </dx:GridViewDataTextColumn>
               
            
            <dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowDeleteButton="True" VisibleIndex="14"/>
                  
        </Columns>
        
        <SettingsBehavior AllowSort="False" />
       <%--<SettingsText ConfirmDelete="Are you sure you wish to delete this row?" />
       <%-- <SettingsPager Visible="true" Mode="ShowPager">
        </SettingsPager>--%>
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
         <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="<%$ Resources:LibResources, Label908 %>" />
    </dx:ASPxGridView>
    <br />
    <table>
        <tr>
           
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label887 %>">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
         
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
            </td>
           
            
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </dx:ASPxLabel>

    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
