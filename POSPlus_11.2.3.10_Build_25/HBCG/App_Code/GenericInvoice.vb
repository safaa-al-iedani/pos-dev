Public Class GenericInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Dim comments_processed As Boolean
    Dim triggers_processed As Boolean
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle

    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand

    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand

    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents xrPgHdrTbl1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrPgHdrTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrPgHdrTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_so_store_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrStoreSite As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrSlspLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp1_email As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_email As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPgHdrLbl1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrCustTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrCustInfoTtlTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrBillToTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrShipToTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrCustInfoTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrBillToTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_bill_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrShipToTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrTransInfoTbl As DevExpress.XtraReports.UI.XRTable

    Private WithEvents xrTransInfoTtlTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrWrDtTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrShipViaTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTermsTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPoTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrShipDtTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSeqNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xrTransInfoDataTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrSoWrDtTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_sale_last_four As DevExpress.XtraReports.UI.XRTableCell  ' seqNum
    Private WithEvents xrPuDelTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPuDelDtTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_terms As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_po_num As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xrDocNumCustCdTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrDocNumTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrDocNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrDocNumTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrDocNumTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCustCdTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrCustCdTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrCustCdTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCustCdTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrSoLnTtlTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSoLnTtlTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrSoLnQtyTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnDesTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnUnitPrcTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnExtPrcTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnSerNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnItmTtlTcel As DevExpress.XtraReports.UI.XRTableCell

    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrSoLnTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSoLnLine1Trow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ser_num As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vend As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xrSoLnLine2Trow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell27 As DevExpress.XtraReports.UI.XRTableCell   ' TODO - should be spacing ???
    Private WithEvents xrTableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell48 As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo  ' Page number info
    Private WithEvents xrCustInitLnLbl As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xrCmntPanl As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xrCmntTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrLFiTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_promo As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLFiPromTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrFiAcctTtlNDataLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_fin_co As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xr_promo_app As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrApprovalTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_promo_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrFiAmtTtlLbl As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrBalanceDetailTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSubTotalTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrSubTotalTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTransChrgsTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTransChrgsTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTaxChrgTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTaxChrgTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTotOrderTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTotOrderTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTotPmtsTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTotPmtsTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_payment_holder As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrBalanceTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrBalanceTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xrPmtTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrPmtTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrPmtTpTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrExpDtTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrApprovalTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrCardNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPmtAmtTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrMerchIdTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrRefTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPmtDtTtlTcel As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_card_no As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_exp_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_appr_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_merchant As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ref As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pmt_dt As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrPrtNameTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCustSigTtlNLnLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTermNCondTtlLbl As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine  ' spacing ?
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel  ' spacing ?
    Private WithEvents xr_triggers As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    'Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    'Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell

    'Required by the Designer
    Private components As System.ComponentModel.IContainer
    Private resourceFileName As String
    Private resourc As System.Resources.ResourceManager

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "GenericInvoice.resx"
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrPgHdrTbl1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrPgHdrTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrPgHdrTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrStoreSite = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_email = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp1_email = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrSlspLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPgHdrLbl1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_addr = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_store_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xrSoLnTtlTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSoLnTtlTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrSoLnQtyTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnItmTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnDesTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnSerNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnUnitPrcTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnExtPrcTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrDocNumCustCdTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrDocNumTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrDocNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrDocNumTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrDocNumTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCustCdTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrCustCdTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCustCdTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCustCdTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTransInfoTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTransInfoTtlTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrWrDtTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSeqNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrShipViaTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrShipDtTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTermsTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPoTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTransInfoDataTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrSoWrDtTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_sale_last_four = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPuDelTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPuDelDtTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_terms = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_po_num = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCustTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrCustInfoTtlTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrBillToTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrShipToTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCustInfoTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrBillToTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_bill_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrShipToTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xrSoLnTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSoLnLine1Trow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vend = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ser_num = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnLine2Trow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_des = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrCustInitLnLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xr_promo_app = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrApprovalTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_promo_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrFiAmtTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_promo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLFiPromTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrFiAcctTtlNDataLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fin_co = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLFiTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pmt_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrPrtNameTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrBalanceDetailTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSubTotalTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrSubTotalTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTransChrgsTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTransChrgsTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTaxChrgTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTaxChrgTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTotOrderTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTotOrderTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTotPmtsTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTotPmtsTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_payment_holder = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrBalanceTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrBalanceTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCmntPanl = New DevExpress.XtraReports.UI.XRPanel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCmntTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ref = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_merchant = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_appr_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_exp_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCustSigTtlNLnLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.xr_triggers = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPmtTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrPmtTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrPmtDtTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPmtTpTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCardNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrExpDtTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrApprovalTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPmtAmtTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrMerchIdTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrRefTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTermNCondTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_card_no = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_mop = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.xrPgHdrTbl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrSoLnTtlTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrDocNumCustCdTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTransInfoTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrCustTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrSoLnTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrBalanceDetailTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 33.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPgHdrTbl1, Me.xr_ord_tp})
        Me.PageHeader.HeightF = 155.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPgHdrTbl1
        '
        Me.xrPgHdrTbl1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrPgHdrTbl1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.00002!)
        Me.xrPgHdrTbl1.Name = "xrPgHdrTbl1"
        Me.xrPgHdrTbl1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrPgHdrTrow})
        Me.xrPgHdrTbl1.SizeF = New System.Drawing.SizeF(796.0001!, 116.0!)
        Me.xrPgHdrTbl1.StylePriority.UseBorders = False
        '
        'xrPgHdrTrow
        '
        Me.xrPgHdrTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrPgHdrTcel})
        Me.xrPgHdrTrow.Name = "xrPgHdrTrow"
        Me.xrPgHdrTrow.Weight = 1.0R
        '
        'xrPgHdrTcel
        '
        Me.xrPgHdrTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrStoreSite, Me.xr_slsp2_email, Me.xr_slsp1_email, Me.xrSlspLbl, Me.xr_slsp2, Me.xr_slsp, Me.xrPgHdrLbl1, Me.xr_store_phone, Me.xr_store_addr, Me.xr_store_name, Me.xr_so_store_cd})
        Me.xrPgHdrTcel.Name = "xrPgHdrTcel"
        Me.xrPgHdrTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrPgHdrTcel.Text = "xrPgHdrTcel"
        Me.xrPgHdrTcel.Weight = 5.74R
        '
        'xrStoreSite
        '
        Me.xrStoreSite.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrStoreSite.ForeColor = System.Drawing.Color.DimGray
        Me.xrStoreSite.LocationFloat = New DevExpress.Utils.PointFloat(0.6666501!, 74.00001!)
        Me.xrStoreSite.Name = "xrStoreSite"
        Me.xrStoreSite.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreSite.SizeF = New System.Drawing.SizeF(230.0!, 17.0!)
        Me.xrStoreSite.StylePriority.UseBorders = False
        Me.xrStoreSite.StylePriority.UseFont = False
        Me.xrStoreSite.StylePriority.UseForeColor = False
        Me.xrStoreSite.StylePriority.UseTextAlignment = False
        Me.xrStoreSite.Text = "http://www.mattressking.net"
        Me.xrStoreSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_slsp2_email
        '
        Me.xr_slsp2_email.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_email.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2_email.LocationFloat = New DevExpress.Utils.PointFloat(591.0666!, 75.0!)
        Me.xr_slsp2_email.Name = "xr_slsp2_email"
        Me.xr_slsp2_email.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_email.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp2_email.StylePriority.UseBorders = False
        Me.xr_slsp2_email.StylePriority.UseFont = False
        Me.xr_slsp2_email.StylePriority.UseForeColor = False
        Me.xr_slsp2_email.StylePriority.UseTextAlignment = False
        Me.xr_slsp2_email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_slsp1_email
        '
        Me.xr_slsp1_email.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp1_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp1_email.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp1_email.LocationFloat = New DevExpress.Utils.PointFloat(591.0666!, 22.99999!)
        Me.xr_slsp1_email.Name = "xr_slsp1_email"
        Me.xr_slsp1_email.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp1_email.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp1_email.StylePriority.UseBorders = False
        Me.xr_slsp1_email.StylePriority.UseFont = False
        Me.xr_slsp1_email.StylePriority.UseForeColor = False
        Me.xr_slsp1_email.StylePriority.UseTextAlignment = False
        Me.xr_slsp1_email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrSlspLbl
        '
        Me.xrSlspLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrSlspLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrSlspLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrSlspLbl.LocationFloat = New DevExpress.Utils.PointFloat(499.0667!, 58.99998!)
        Me.xrSlspLbl.Name = "xrSlspLbl"
        Me.xrSlspLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSlspLbl.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xrSlspLbl.StylePriority.UseBorders = False
        Me.xrSlspLbl.StylePriority.UseFont = False
        Me.xrSlspLbl.StylePriority.UseForeColor = False
        Me.xrSlspLbl.StylePriority.UseTextAlignment = False
        Me.xrSlspLbl.Text = "Sales Rep:"
        Me.xrSlspLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_slsp2
        '
        Me.xr_slsp2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(591.0666!, 57.99999!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp2.StylePriority.UseBorders = False
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseForeColor = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_slsp
        '
        Me.xr_slsp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(591.0666!, 5.999979!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseBorders = False
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseForeColor = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPgHdrLbl1
        '
        Me.xrPgHdrLbl1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPgHdrLbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrPgHdrLbl1.ForeColor = System.Drawing.Color.DimGray
        Me.xrPgHdrLbl1.LocationFloat = New DevExpress.Utils.PointFloat(484.0667!, 5.999979!)
        Me.xrPgHdrLbl1.Name = "xrPgHdrLbl1"
        Me.xrPgHdrLbl1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPgHdrLbl1.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xrPgHdrLbl1.StylePriority.UseBorders = False
        Me.xrPgHdrLbl1.StylePriority.UseFont = False
        Me.xrPgHdrLbl1.StylePriority.UseForeColor = False
        Me.xrPgHdrLbl1.StylePriority.UseTextAlignment = False
        Me.xrPgHdrLbl1.Text = "Order Taken By:"
        Me.xrPgHdrLbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_phone.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.6666501!, 57.00003!)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.SizeF = New System.Drawing.SizeF(230.0!, 17.0!)
        Me.xr_store_phone.StylePriority.UseBorders = False
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_addr
        '
        Me.xr_store_addr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_addr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_addr.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_addr.LocationFloat = New DevExpress.Utils.PointFloat(0.6666501!, 40.00003!)
        Me.xr_store_addr.Name = "xr_store_addr"
        Me.xr_store_addr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr.SizeF = New System.Drawing.SizeF(283.0!, 17.0!)
        Me.xr_store_addr.StylePriority.UseBorders = False
        Me.xr_store_addr.StylePriority.UseFont = False
        Me.xr_store_addr.StylePriority.UseForeColor = False
        Me.xr_store_addr.StylePriority.UseTextAlignment = False
        Me.xr_store_addr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_name
        '
        Me.xr_store_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_name.LocationFloat = New DevExpress.Utils.PointFloat(0.6666501!, 10.00001!)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.SizeF = New System.Drawing.SizeF(384.0!, 30.00001!)
        Me.xr_store_name.StylePriority.UseBorders = False
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.StylePriority.UseTextAlignment = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_so_store_cd
        '
        Me.xr_so_store_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_store_cd.LocationFloat = New DevExpress.Utils.PointFloat(775.7334!, 6.00001!)
        Me.xr_so_store_cd.Name = "xr_so_store_cd"
        Me.xr_so_store_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_store_cd.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_so_store_cd.StylePriority.UseFont = False
        Me.xr_so_store_cd.Visible = False
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(417.0!, 33.0!)
        Me.xr_ord_tp.StylePriority.UseBorders = False
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES INVOICE"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrSoLnTtlTbl, Me.xrDocNumCustCdTbl, Me.xrTransInfoTbl, Me.xrCustTbl, Me.xr_slsp2_hidden, Me.xr_ord_srt, Me.xr_ord_tp_hidden, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname})
        Me.GroupHeader1.HeightF = 242.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xrSoLnTtlTbl
        '
        Me.xrSoLnTtlTbl.BackColor = System.Drawing.Color.RosyBrown
        Me.xrSoLnTtlTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 217.0!)
        Me.xrSoLnTtlTbl.Name = "xrSoLnTtlTbl"
        Me.xrSoLnTtlTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSoLnTtlTrow})
        Me.xrSoLnTtlTbl.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
        Me.xrSoLnTtlTbl.StylePriority.UseBackColor = False
        Me.xrSoLnTtlTbl.StylePriority.UseTextAlignment = False
        Me.xrSoLnTtlTbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrSoLnTtlTrow
        '
        Me.xrSoLnTtlTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrSoLnQtyTtlTcel, Me.xrSoLnItmTtlTcel, Me.xrSoLnDesTtlTcel, Me.xrSoLnSerNumTtlTcel, Me.xrSoLnUnitPrcTtlTcel, Me.xrSoLnExtPrcTtlTcel})
        Me.xrSoLnTtlTrow.Name = "xrSoLnTtlTrow"
        Me.xrSoLnTtlTrow.Weight = 1.0R
        '
        'xrSoLnQtyTtlTcel
        '
        Me.xrSoLnQtyTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnQtyTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnQtyTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnQtyTtlTcel.Name = "xrSoLnQtyTtlTcel"
        Me.xrSoLnQtyTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnQtyTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnQtyTtlTcel.Text = "QTY"
        Me.xrSoLnQtyTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnQtyTtlTcel.Weight = 0.125R
        '
        'xrSoLnItmTtlTcel
        '
        Me.xrSoLnItmTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnItmTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnItmTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnItmTtlTcel.Name = "xrSoLnItmTtlTcel"
        Me.xrSoLnItmTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnItmTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnItmTtlTcel.Text = "SKU"
        Me.xrSoLnItmTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnItmTtlTcel.Weight = 0.34625000000000011R
        '
        'xrSoLnDesTtlTcel
        '
        Me.xrSoLnDesTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnDesTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnDesTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnDesTtlTcel.Name = "xrSoLnDesTtlTcel"
        Me.xrSoLnDesTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnDesTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnDesTtlTcel.Text = "Description"
        Me.xrSoLnDesTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnDesTtlTcel.Weight = 1.3475R
        '
        'xrSoLnSerNumTtlTcel
        '
        Me.xrSoLnSerNumTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnSerNumTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnSerNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnSerNumTtlTcel.Name = "xrSoLnSerNumTtlTcel"
        Me.xrSoLnSerNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnSerNumTtlTcel.Text = "Serial Number"
        Me.xrSoLnSerNumTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnSerNumTtlTcel.Weight = 0.49625R
        '
        'xrSoLnUnitPrcTtlTcel
        '
        Me.xrSoLnUnitPrcTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnUnitPrcTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnUnitPrcTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnUnitPrcTtlTcel.Name = "xrSoLnUnitPrcTtlTcel"
        Me.xrSoLnUnitPrcTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnUnitPrcTtlTcel.Text = "Unit Price"
        Me.xrSoLnUnitPrcTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnUnitPrcTtlTcel.Weight = 0.34625000000000006R
        '
        'xrSoLnExtPrcTtlTcel
        '
        Me.xrSoLnExtPrcTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnExtPrcTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnExtPrcTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnExtPrcTtlTcel.Name = "xrSoLnExtPrcTtlTcel"
        Me.xrSoLnExtPrcTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnExtPrcTtlTcel.Text = "Total"
        Me.xrSoLnExtPrcTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnExtPrcTtlTcel.Weight = 0.33875R
        '
        'xrDocNumCustCdTbl
        '
        Me.xrDocNumCustCdTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrDocNumCustCdTbl.LocationFloat = New DevExpress.Utils.PointFloat(533.3333!, 7.999992!)
        Me.xrDocNumCustCdTbl.Name = "xrDocNumCustCdTbl"
        Me.xrDocNumCustCdTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrDocNumTrow, Me.xrCustCdTrow})
        Me.xrDocNumCustCdTbl.SizeF = New System.Drawing.SizeF(263.0001!, 100.0!)
        Me.xrDocNumCustCdTbl.StylePriority.UseBorders = False
        '
        'xrDocNumTrow
        '
        Me.xrDocNumTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrDocNumTtlTcel, Me.xrDocNumTcel})
        Me.xrDocNumTrow.Name = "xrDocNumTrow"
        Me.xrDocNumTrow.Weight = 1.0R
        '
        'xrDocNumTtlTcel
        '
        Me.xrDocNumTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrDocNumTtlTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrDocNumTtlLbl})
        Me.xrDocNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrDocNumTtlTcel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrDocNumTtlTcel.Name = "xrDocNumTtlTcel"
        Me.xrDocNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrDocNumTtlTcel.StylePriority.UseBackColor = False
        Me.xrDocNumTtlTcel.StylePriority.UseFont = False
        Me.xrDocNumTtlTcel.StylePriority.UseForeColor = False
        Me.xrDocNumTtlTcel.Weight = 1.08R
        '
        'xrDocNumTtlLbl
        '
        Me.xrDocNumTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDocNumTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrDocNumTtlLbl.Name = "xrDocNumTtlLbl"
        Me.xrDocNumTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrDocNumTtlLbl.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrDocNumTtlLbl.StylePriority.UseBorders = False
        Me.xrDocNumTtlLbl.Text = " Invoice No.:"
        '
        'xrDocNumTcel
        '
        Me.xrDocNumTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_del_doc_num})
        Me.xrDocNumTcel.Name = "xrDocNumTcel"
        Me.xrDocNumTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrDocNumTcel.Weight = 1.0899999999999999R
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 14.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(129.0!, 24.0!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrCustCdTrow
        '
        Me.xrCustCdTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrCustCdTtlTcel, Me.xrCustCdTcel})
        Me.xrCustCdTrow.Name = "xrCustCdTrow"
        Me.xrCustCdTrow.Weight = 1.0R
        '
        'xrCustCdTtlTcel
        '
        Me.xrCustCdTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrCustCdTtlTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrCustCdTtlLbl})
        Me.xrCustCdTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCustCdTtlTcel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrCustCdTtlTcel.Name = "xrCustCdTtlTcel"
        Me.xrCustCdTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrCustCdTtlTcel.StylePriority.UseBackColor = False
        Me.xrCustCdTtlTcel.StylePriority.UseFont = False
        Me.xrCustCdTtlTcel.StylePriority.UseForeColor = False
        Me.xrCustCdTtlTcel.Weight = 1.08R
        '
        'xrCustCdTtlLbl
        '
        Me.xrCustCdTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustCdTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrCustCdTtlLbl.Name = "xrCustCdTtlLbl"
        Me.xrCustCdTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustCdTtlLbl.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrCustCdTtlLbl.StylePriority.UseBorders = False
        Me.xrCustCdTtlLbl.Text = " Customer ID:"
        '
        'xrCustCdTcel
        '
        Me.xrCustCdTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cust_cd})
        Me.xrCustCdTcel.Name = "xrCustCdTcel"
        Me.xrCustCdTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrCustCdTcel.Weight = 1.0899999999999999R
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(129.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTransInfoTbl
        '
        Me.xrTransInfoTbl.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTransInfoTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTransInfoTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 158.0!)
        Me.xrTransInfoTbl.Name = "xrTransInfoTbl"
        Me.xrTransInfoTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTransInfoTtlTrow, Me.xrTransInfoDataTrow})
        Me.xrTransInfoTbl.SizeF = New System.Drawing.SizeF(800.0!, 42.0!)
        Me.xrTransInfoTbl.StylePriority.UseBorderColor = False
        Me.xrTransInfoTbl.StylePriority.UseBorders = False
        '
        'xrTransInfoTtlTrow
        '
        Me.xrTransInfoTtlTrow.BackColor = System.Drawing.Color.Tan
        Me.xrTransInfoTtlTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrWrDtTtlTcel, Me.xrSeqNumTtlTcel, Me.xrShipViaTtlTcel, Me.xrShipDtTtlTcel, Me.xrTermsTtlTcel, Me.xrPoTtlTcel})
        Me.xrTransInfoTtlTrow.Name = "xrTransInfoTtlTrow"
        Me.xrTransInfoTtlTrow.StylePriority.UseBackColor = False
        Me.xrTransInfoTtlTrow.Weight = 1.0R
        '
        'xrWrDtTtlTcel
        '
        Me.xrWrDtTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrWrDtTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrWrDtTtlTcel.Name = "xrWrDtTtlTcel"
        Me.xrWrDtTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrWrDtTtlTcel.StylePriority.UseBackColor = False
        Me.xrWrDtTtlTcel.StylePriority.UseFont = False
        Me.xrWrDtTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrWrDtTtlTcel.Text = "Date"
        Me.xrWrDtTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrWrDtTtlTcel.Weight = 0.5R
        '
        'xrSeqNumTtlTcel
        '
        Me.xrSeqNumTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSeqNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSeqNumTtlTcel.Name = "xrSeqNumTtlTcel"
        Me.xrSeqNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSeqNumTtlTcel.StylePriority.UseBackColor = False
        Me.xrSeqNumTtlTcel.StylePriority.UseFont = False
        Me.xrSeqNumTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSeqNumTtlTcel.Text = "Order No."
        Me.xrSeqNumTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSeqNumTtlTcel.Weight = 0.5R
        '
        'xrShipViaTtlTcel
        '
        Me.xrShipViaTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrShipViaTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrShipViaTtlTcel.Name = "xrShipViaTtlTcel"
        Me.xrShipViaTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrShipViaTtlTcel.StylePriority.UseBackColor = False
        Me.xrShipViaTtlTcel.StylePriority.UseFont = False
        Me.xrShipViaTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrShipViaTtlTcel.Text = "Ship Via"
        Me.xrShipViaTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrShipViaTtlTcel.Weight = 0.5R
        '
        'xrShipDtTtlTcel
        '
        Me.xrShipDtTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrShipDtTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrShipDtTtlTcel.Name = "xrShipDtTtlTcel"
        Me.xrShipDtTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrShipDtTtlTcel.StylePriority.UseBackColor = False
        Me.xrShipDtTtlTcel.StylePriority.UseFont = False
        Me.xrShipDtTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrShipDtTtlTcel.Text = "Ship Date"
        Me.xrShipDtTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrShipDtTtlTcel.Weight = 0.5R
        '
        'xrTermsTtlTcel
        '
        Me.xrTermsTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTermsTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTermsTtlTcel.Name = "xrTermsTtlTcel"
        Me.xrTermsTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTermsTtlTcel.StylePriority.UseBackColor = False
        Me.xrTermsTtlTcel.StylePriority.UseFont = False
        Me.xrTermsTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTermsTtlTcel.Text = "Terms"
        Me.xrTermsTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTermsTtlTcel.Weight = 0.5R
        '
        'xrPoTtlTcel
        '
        Me.xrPoTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrPoTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPoTtlTcel.Name = "xrPoTtlTcel"
        Me.xrPoTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrPoTtlTcel.StylePriority.UseBackColor = False
        Me.xrPoTtlTcel.StylePriority.UseFont = False
        Me.xrPoTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrPoTtlTcel.Text = "P.O."
        Me.xrPoTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrPoTtlTcel.Weight = 0.5R
        '
        'xrTransInfoDataTrow
        '
        Me.xrTransInfoDataTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrSoWrDtTcel, Me.xr_sale_last_four, Me.xrPuDelTcel, Me.xrPuDelDtTcel, Me.xr_terms, Me.xr_po_num})
        Me.xrTransInfoDataTrow.Name = "xrTransInfoDataTrow"
        Me.xrTransInfoDataTrow.Weight = 1.0R
        '
        'xrSoWrDtTcel
        '
        Me.xrSoWrDtTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_so_wr_dt})
        Me.xrSoWrDtTcel.Multiline = True
        Me.xrSoWrDtTcel.Name = "xrSoWrDtTcel"
        Me.xrSoWrDtTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoWrDtTcel.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrSoWrDtTcel.Weight = 0.5R
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 2.0!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseBorders = False
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.StylePriority.UseForeColor = False
        Me.xr_so_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_so_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_sale_last_four
        '
        Me.xr_sale_last_four.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sale_last_four.Name = "xr_sale_last_four"
        Me.xr_sale_last_four.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_sale_last_four.StylePriority.UseFont = False
        Me.xr_sale_last_four.StylePriority.UseTextAlignment = False
        Me.xr_sale_last_four.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sale_last_four.Weight = 0.5R
        '
        'xrPuDelTcel
        '
        Me.xrPuDelTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pd})
        Me.xrPuDelTcel.Name = "xrPuDelTcel"
        Me.xrPuDelTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrPuDelTcel.Weight = 0.5R
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.Black
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrPuDelDtTcel
        '
        Me.xrPuDelDtTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pu_del_dt})
        Me.xrPuDelDtTcel.Name = "xrPuDelDtTcel"
        Me.xrPuDelDtTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrPuDelDtTcel.Weight = 0.5R
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseBorders = False
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.StylePriority.UseForeColor = False
        Me.xr_pu_del_dt.StylePriority.UseTextAlignment = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xr_terms
        '
        Me.xr_terms.Name = "xr_terms"
        Me.xr_terms.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_terms.StylePriority.UseTextAlignment = False
        Me.xr_terms.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_terms.Weight = 0.5R
        '
        'xr_po_num
        '
        Me.xr_po_num.Name = "xr_po_num"
        Me.xr_po_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_po_num.Weight = 0.5R
        '
        'xrCustTbl
        '
        Me.xrCustTbl.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.xrCustTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrCustTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
        Me.xrCustTbl.Name = "xrCustTbl"
        Me.xrCustTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrCustInfoTtlTrow, Me.xrCustInfoTrow})
        Me.xrCustTbl.SizeF = New System.Drawing.SizeF(500.0!, 142.0!)
        Me.xrCustTbl.StylePriority.UseBorderColor = False
        Me.xrCustTbl.StylePriority.UseBorders = False
        '
        'xrCustInfoTtlTrow
        '
        Me.xrCustInfoTtlTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrBillToTtlTcel, Me.xrShipToTtlTcel})
        Me.xrCustInfoTtlTrow.Name = "xrCustInfoTtlTrow"
        Me.xrCustInfoTtlTrow.Weight = 0.55185659411011523R
        '
        'xrBillToTtlTcel
        '
        Me.xrBillToTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrBillToTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrBillToTtlTcel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrBillToTtlTcel.Name = "xrBillToTtlTcel"
        Me.xrBillToTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrBillToTtlTcel.StylePriority.UseBackColor = False
        Me.xrBillToTtlTcel.StylePriority.UseFont = False
        Me.xrBillToTtlTcel.StylePriority.UseForeColor = False
        Me.xrBillToTtlTcel.Text = " Bill To:"
        Me.xrBillToTtlTcel.Weight = 2.5R
        '
        'xrShipToTtlTcel
        '
        Me.xrShipToTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrShipToTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrShipToTtlTcel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrShipToTtlTcel.Name = "xrShipToTtlTcel"
        Me.xrShipToTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrShipToTtlTcel.StylePriority.UseBackColor = False
        Me.xrShipToTtlTcel.StylePriority.UseFont = False
        Me.xrShipToTtlTcel.StylePriority.UseForeColor = False
        Me.xrShipToTtlTcel.Text = " Ship To:"
        Me.xrShipToTtlTcel.Weight = 2.5R
        '
        'xrCustInfoTrow
        '
        Me.xrCustInfoTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrBillToTcel, Me.xrShipToTcel})
        Me.xrCustInfoTrow.Name = "xrCustInfoTrow"
        Me.xrCustInfoTrow.Weight = 3.99359795134443R
        '
        'xrBillToTcel
        '
        Me.xrBillToTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_bill_corp_name, Me.xr_bill_addr1, Me.xr_bill_city, Me.xr_bill_state, Me.xr_bill_zip, Me.xr_bill_h_phone, Me.xr_bill_b_phone, Me.xr_bill_addr2, Me.xr_bill_full_name})
        Me.xrBillToTcel.Name = "xrBillToTcel"
        Me.xrBillToTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrBillToTcel.Text = "xrTableCell18"
        Me.xrBillToTcel.Weight = 2.5R
        '
        'xr_bill_corp_name
        '
        Me.xr_bill_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_corp_name.CanShrink = True
        Me.xr_bill_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 0.0!)
        Me.xr_bill_corp_name.Name = "xr_bill_corp_name"
        Me.xr_bill_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_corp_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_corp_name.StylePriority.UseBorders = False
        Me.xr_bill_corp_name.StylePriority.UseFont = False
        '
        'xr_bill_addr1
        '
        Me.xr_bill_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 34.75992!)
        Me.xr_bill_addr1.Name = "xr_bill_addr1"
        Me.xr_bill_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr1.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_addr1.StylePriority.UseBorders = False
        Me.xr_bill_addr1.StylePriority.UseFont = False
        '
        'xr_bill_city
        '
        Me.xr_bill_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_city.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 69.75996!)
        Me.xr_bill_city.Name = "xr_bill_city"
        Me.xr_bill_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_city.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
        Me.xr_bill_city.StylePriority.UseBorders = False
        Me.xr_bill_city.StylePriority.UseFont = False
        '
        'xr_bill_state
        '
        Me.xr_bill_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_state.CanGrow = False
        Me.xr_bill_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_state.LocationFloat = New DevExpress.Utils.PointFloat(153.0!, 69.75996!)
        Me.xr_bill_state.Name = "xr_bill_state"
        Me.xr_bill_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_bill_state.StylePriority.UseBorders = False
        Me.xr_bill_state.StylePriority.UseFont = False
        Me.xr_bill_state.Text = "xr_state"
        '
        'xr_bill_zip
        '
        Me.xr_bill_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_zip.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 69.75996!)
        Me.xr_bill_zip.Name = "xr_bill_zip"
        Me.xr_bill_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_bill_zip.StylePriority.UseBorders = False
        Me.xr_bill_zip.StylePriority.UseFont = False
        Me.xr_bill_zip.Text = "xr_zip"
        '
        'xr_bill_h_phone
        '
        Me.xr_bill_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_h_phone.CanShrink = True
        Me.xr_bill_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 86.75996!)
        Me.xr_bill_h_phone.Name = "xr_bill_h_phone"
        Me.xr_bill_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_bill_h_phone.StylePriority.UseBorders = False
        Me.xr_bill_h_phone.StylePriority.UseFont = False
        Me.xr_bill_h_phone.Text = "xr_h_phone"
        '
        'xr_bill_b_phone
        '
        Me.xr_bill_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 103.7599!)
        Me.xr_bill_b_phone.Name = "xr_bill_b_phone"
        Me.xr_bill_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_bill_b_phone.StylePriority.UseBorders = False
        Me.xr_bill_b_phone.StylePriority.UseFont = False
        Me.xr_bill_b_phone.Text = "xr_b_phone"
        '
        'xr_bill_addr2
        '
        Me.xr_bill_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr2.CanShrink = True
        Me.xr_bill_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 52.75995!)
        Me.xr_bill_addr2.Name = "xr_bill_addr2"
        Me.xr_bill_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr2.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_addr2.StylePriority.UseBorders = False
        Me.xr_bill_addr2.StylePriority.UseFont = False
        '
        'xr_bill_full_name
        '
        Me.xr_bill_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_full_name.CanShrink = True
        Me.xr_bill_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.000007947286!, 17.75998!)
        Me.xr_bill_full_name.Name = "xr_bill_full_name"
        Me.xr_bill_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_full_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_full_name.StylePriority.UseBorders = False
        Me.xr_bill_full_name.StylePriority.UseFont = False
        '
        'xrShipToTcel
        '
        Me.xrShipToTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_full_name, Me.xr_b_phone, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_addr1, Me.xr_corp_name})
        Me.xrShipToTcel.Name = "xrShipToTcel"
        Me.xrShipToTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrShipToTcel.Text = "xrShipToTcel"
        Me.xrShipToTcel.Weight = 2.5R
        '
        'xr_full_name
        '
        Me.xr_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_full_name.CanShrink = True
        Me.xr_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.75998!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_full_name.StylePriority.UseBorders = False
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.7599!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseBorders = False
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_h_phone.CanShrink = True
        Me.xr_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.75996!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseBorders = False
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 68.75993!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseBorders = False
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(157.0!, 68.75993!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseBorders = False
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.75993!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
        Me.xr_city.StylePriority.UseBorders = False
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.75999!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_addr2.StylePriority.UseBorders = False
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_addr1
        '
        Me.xr_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.75992!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_addr1.StylePriority.UseBorders = False
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_corp_name
        '
        Me.xr_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_corp_name.CanShrink = True
        Me.xr_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_corp_name.Name = "xr_corp_name"
        Me.xr_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_corp_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_corp_name.StylePriority.UseBorders = False
        Me.xr_corp_name.StylePriority.UseFont = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 150.0!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 150.0!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 142.0!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 125.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 125.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 125.0!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrSoLnTbl, Me.xr_disc_amt, Me.xr_void_flag})
        Me.Detail.HeightF = 50.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrSoLnTbl
        '
        Me.xrSoLnTbl.BackColor = System.Drawing.Color.RosyBrown
        Me.xrSoLnTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrSoLnTbl.Name = "xrSoLnTbl"
        Me.xrSoLnTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSoLnLine1Trow, Me.xrSoLnLine2Trow})
        Me.xrSoLnTbl.SizeF = New System.Drawing.SizeF(800.0!, 50.0!)
        Me.xrSoLnTbl.StylePriority.UseBackColor = False
        Me.xrSoLnTbl.StylePriority.UseTextAlignment = False
        Me.xrSoLnTbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrSoLnLine1Trow
        '
        Me.xrSoLnLine1Trow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_qty, Me.xr_sku, Me.xr_vend, Me.xr_vsn, Me.xr_ser_num, Me.xr_ret, Me.xr_ext})
        Me.xrSoLnLine1Trow.Name = "xrSoLnLine1Trow"
        Me.xrSoLnLine1Trow.Weight = 1.0R
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_qty.Weight = 0.125R
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sku.Weight = 0.34625000000000011R
        '
        'xr_vend
        '
        Me.xr_vend.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vend.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vend.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vend.Name = "xr_vend"
        Me.xr_vend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_vend.StylePriority.UseBackColor = False
        Me.xr_vend.StylePriority.UseBorderColor = False
        Me.xr_vend.StylePriority.UseBorders = False
        Me.xr_vend.StylePriority.UseFont = False
        Me.xr_vend.StylePriority.UseTextAlignment = False
        Me.xr_vend.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vend.Weight = 0.21625R
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vsn.Weight = 1.1312499999999999R
        '
        'xr_ser_num
        '
        Me.xr_ser_num.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ser_num.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ser_num.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ser_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ser_num.Name = "xr_ser_num"
        Me.xr_ser_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ser_num.StylePriority.UseBackColor = False
        Me.xr_ser_num.StylePriority.UseBorderColor = False
        Me.xr_ser_num.StylePriority.UseBorders = False
        Me.xr_ser_num.StylePriority.UseFont = False
        Me.xr_ser_num.StylePriority.UseTextAlignment = False
        Me.xr_ser_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ser_num.Weight = 0.49624999999999997R
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ret.Weight = 0.34625000000000006R
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ext.Weight = 0.33875R
        '
        'xrSoLnLine2Trow
        '
        Me.xrSoLnLine2Trow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell27, Me.xrTableCell43, Me.xr_des, Me.xrTableCell48})
        Me.xrSoLnLine2Trow.Name = "xrSoLnLine2Trow"
        Me.xrSoLnLine2Trow.Weight = 1.0R
        '
        'xrTableCell27
        '
        Me.xrTableCell27.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell27.Name = "xrTableCell27"
        Me.xrTableCell27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell27.StylePriority.UseBackColor = False
        Me.xrTableCell27.Weight = 0.125R
        '
        'xrTableCell43
        '
        Me.xrTableCell43.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell43.Name = "xrTableCell43"
        Me.xrTableCell43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell43.StylePriority.UseBackColor = False
        Me.xrTableCell43.Weight = 0.34250000000000008R
        '
        'xr_des
        '
        Me.xr_des.BackColor = System.Drawing.Color.Transparent
        Me.xr_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_des.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_des.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_des.Name = "xr_des"
        Me.xr_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_des.StylePriority.UseBackColor = False
        Me.xr_des.StylePriority.UseBorderColor = False
        Me.xr_des.StylePriority.UseBorders = False
        Me.xr_des.StylePriority.UseFont = False
        Me.xr_des.StylePriority.UseTextAlignment = False
        Me.xr_des.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_des.Weight = 1.35125R
        '
        'xrTableCell48
        '
        Me.xrTableCell48.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell48.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell48.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell48.Name = "xrTableCell48"
        Me.xrTableCell48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell48.StylePriority.UseBackColor = False
        Me.xrTableCell48.StylePriority.UseBorderColor = False
        Me.xrTableCell48.StylePriority.UseBorders = False
        Me.xrTableCell48.Weight = 1.18125R
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(817.0!, 8.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(800.0!, 8.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrCustInitLnLbl, Me.xrPageInfo2})
        Me.PageFooter.HeightF = 34.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrCustInitLnLbl
        '
        Me.xrCustInitLnLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustInitLnLbl.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCustInitLnLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrCustInitLnLbl.LocationFloat = New DevExpress.Utils.PointFloat(616.0!, 17.0!)
        Me.xrCustInitLnLbl.Name = "xrCustInitLnLbl"
        Me.xrCustInitLnLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustInitLnLbl.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
        Me.xrCustInitLnLbl.StylePriority.UseBorders = False
        Me.xrCustInitLnLbl.StylePriority.UseFont = False
        Me.xrCustInitLnLbl.StylePriority.UseForeColor = False
        Me.xrCustInitLnLbl.StylePriority.UseTextAlignment = False
        Me.xrCustInitLnLbl.Text = "CUSTOMER INITIAL:_____________"
        Me.xrCustInitLnLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(642.0!, 0.0!)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.SizeF = New System.Drawing.SizeF(154.0!, 17.0!)
        Me.xrPageInfo2.StylePriority.UseBorders = False
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.StylePriority.UseForeColor = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_promo_app, Me.xrApprovalTtlLbl, Me.xr_promo_amt, Me.xrFiAmtTtlLbl, Me.xr_promo, Me.xrLFiPromTtlLbl, Me.xrFiAcctTtlNDataLbl, Me.xr_fin_co, Me.xrLFiTtlLbl, Me.xr_pmt_dt, Me.xrLine2, Me.xrPrtNameTtlLbl, Me.xrBalanceDetailTbl, Me.xrCmntPanl, Me.xr_ref, Me.xr_merchant, Me.xr_pmt_amt, Me.xr_appr_cd, Me.xr_exp_dt, Me.xrCustSigTtlNLnLbl, Me.xrLine1, Me.xrLine7, Me.xr_triggers, Me.xrLabel27, Me.xrPmtTbl, Me.xrTermNCondTtlLbl, Me.xr_card_no, Me.xr_mop, Me.xrLine5})
        Me.ReportFooter.HeightF = 342.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_promo_app
        '
        Me.xr_promo_app.BorderWidth = 0
        Me.xr_promo_app.CanShrink = True
        Me.xr_promo_app.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_promo_app.ForeColor = System.Drawing.Color.DimGray
        Me.xr_promo_app.LocationFloat = New DevExpress.Utils.PointFloat(432.0!, 113.3333!)
        Me.xr_promo_app.Multiline = True
        Me.xr_promo_app.Name = "xr_promo_app"
        Me.xr_promo_app.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_promo_app.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_promo_app.SizeF = New System.Drawing.SizeF(117.0!, 16.33331!)
        Me.xr_promo_app.StylePriority.UseBorderWidth = False
        Me.xr_promo_app.StylePriority.UseFont = False
        Me.xr_promo_app.StylePriority.UseForeColor = False
        '
        'xrApprovalTtlLbl
        '
        Me.xrApprovalTtlLbl.BorderWidth = 0
        Me.xrApprovalTtlLbl.CanShrink = True
        Me.xrApprovalTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrApprovalTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrApprovalTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 113.3333!)
        Me.xrApprovalTtlLbl.Multiline = True
        Me.xrApprovalTtlLbl.Name = "xrApprovalTtlLbl"
        Me.xrApprovalTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrApprovalTtlLbl.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xrApprovalTtlLbl.SizeF = New System.Drawing.SizeF(73.99997!, 16.33331!)
        Me.xrApprovalTtlLbl.StylePriority.UseBorderWidth = False
        Me.xrApprovalTtlLbl.StylePriority.UseFont = False
        Me.xrApprovalTtlLbl.StylePriority.UseForeColor = False
        Me.xrApprovalTtlLbl.Text = "APPROVAL:"
        '
        'xr_promo_amt
        '
        Me.xr_promo_amt.BorderWidth = 0
        Me.xr_promo_amt.CanShrink = True
        Me.xr_promo_amt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_promo_amt.ForeColor = System.Drawing.Color.DimGray
        Me.xr_promo_amt.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 96.33331!)
        Me.xr_promo_amt.Multiline = True
        Me.xr_promo_amt.Name = "xr_promo_amt"
        Me.xr_promo_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_promo_amt.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_promo_amt.SizeF = New System.Drawing.SizeF(117.0!, 16.33331!)
        Me.xr_promo_amt.StylePriority.UseBorderWidth = False
        Me.xr_promo_amt.StylePriority.UseFont = False
        Me.xr_promo_amt.StylePriority.UseForeColor = False
        '
        'xrFiAmtTtlLbl
        '
        Me.xrFiAmtTtlLbl.BorderWidth = 0
        Me.xrFiAmtTtlLbl.CanShrink = True
        Me.xrFiAmtTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrFiAmtTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrFiAmtTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 96.33331!)
        Me.xrFiAmtTtlLbl.Multiline = True
        Me.xrFiAmtTtlLbl.Name = "xrFiAmtTtlLbl"
        Me.xrFiAmtTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrFiAmtTtlLbl.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xrFiAmtTtlLbl.SizeF = New System.Drawing.SizeF(73.99997!, 16.33331!)
        Me.xrFiAmtTtlLbl.StylePriority.UseBorderWidth = False
        Me.xrFiAmtTtlLbl.StylePriority.UseFont = False
        Me.xrFiAmtTtlLbl.StylePriority.UseForeColor = False
        Me.xrFiAmtTtlLbl.Text = "$ AMT:"
        '
        'xr_promo
        '
        Me.xr_promo.BorderWidth = 0
        Me.xr_promo.CanShrink = True
        Me.xr_promo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_promo.ForeColor = System.Drawing.Color.DimGray
        Me.xr_promo.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 79.99998!)
        Me.xr_promo.Multiline = True
        Me.xr_promo.Name = "xr_promo"
        Me.xr_promo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_promo.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_promo.SizeF = New System.Drawing.SizeF(192.0!, 16.3333!)
        Me.xr_promo.StylePriority.UseBorderWidth = False
        Me.xr_promo.StylePriority.UseFont = False
        Me.xr_promo.StylePriority.UseForeColor = False
        '
        'xrLFiPromTtlLbl
        '
        Me.xrLFiPromTtlLbl.BorderWidth = 0
        Me.xrLFiPromTtlLbl.CanShrink = True
        Me.xrLFiPromTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLFiPromTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrLFiPromTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 61.33334!)
        Me.xrLFiPromTtlLbl.Multiline = True
        Me.xrLFiPromTtlLbl.Name = "xrLFiPromTtlLbl"
        Me.xrLFiPromTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLFiPromTtlLbl.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xrLFiPromTtlLbl.SizeF = New System.Drawing.SizeF(192.0!, 16.3333!)
        Me.xrLFiPromTtlLbl.StylePriority.UseBorderWidth = False
        Me.xrLFiPromTtlLbl.StylePriority.UseFont = False
        Me.xrLFiPromTtlLbl.StylePriority.UseForeColor = False
        Me.xrLFiPromTtlLbl.Text = "PROMOTION INFORMATION:"
        '
        'xrFiAcctTtlNDataLbl
        '
        Me.xrFiAcctTtlNDataLbl.BorderWidth = 0
        Me.xrFiAcctTtlNDataLbl.CanShrink = True
        Me.xrFiAcctTtlNDataLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrFiAcctTtlNDataLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrFiAcctTtlNDataLbl.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 43.66668!)
        Me.xrFiAcctTtlNDataLbl.Multiline = True
        Me.xrFiAcctTtlNDataLbl.Name = "xrFiAcctTtlNDataLbl"
        Me.xrFiAcctTtlNDataLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrFiAcctTtlNDataLbl.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xrFiAcctTtlNDataLbl.SizeF = New System.Drawing.SizeF(192.0!, 16.3333!)
        Me.xrFiAcctTtlNDataLbl.StylePriority.UseBorderWidth = False
        Me.xrFiAcctTtlNDataLbl.StylePriority.UseFont = False
        Me.xrFiAcctTtlNDataLbl.StylePriority.UseForeColor = False
        Me.xrFiAcctTtlNDataLbl.Text = "ACCT#: XXXXXXXXXXXXXXXX"
        '
        'xr_fin_co
        '
        Me.xr_fin_co.BorderWidth = 0
        Me.xr_fin_co.CanShrink = True
        Me.xr_fin_co.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fin_co.ForeColor = System.Drawing.Color.DimGray
        Me.xr_fin_co.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 25.66668!)
        Me.xr_fin_co.Multiline = True
        Me.xr_fin_co.Name = "xr_fin_co"
        Me.xr_fin_co.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fin_co.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_fin_co.SizeF = New System.Drawing.SizeF(192.0!, 16.3333!)
        Me.xr_fin_co.StylePriority.UseBorderWidth = False
        Me.xr_fin_co.StylePriority.UseFont = False
        Me.xr_fin_co.StylePriority.UseForeColor = False
        '
        'xrLFiTtlLbl
        '
        Me.xrLFiTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLFiTtlLbl.BorderWidth = 0
        Me.xrLFiTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLFiTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrLFiTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 7.999992!)
        Me.xrLFiTtlLbl.Name = "xrLFiTtlLbl"
        Me.xrLFiTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLFiTtlLbl.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLFiTtlLbl.StylePriority.UseBorders = False
        Me.xrLFiTtlLbl.StylePriority.UseBorderWidth = False
        Me.xrLFiTtlLbl.StylePriority.UseFont = False
        Me.xrLFiTtlLbl.StylePriority.UseForeColor = False
        Me.xrLFiTtlLbl.Text = "FINANCE:"
        '
        'xr_pmt_dt
        '
        Me.xr_pmt_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pmt_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_dt.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 168.0!)
        Me.xr_pmt_dt.Multiline = True
        Me.xr_pmt_dt.Name = "xr_pmt_dt"
        Me.xr_pmt_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_dt.SizeF = New System.Drawing.SizeF(78.76666!, 17.0!)
        Me.xr_pmt_dt.StylePriority.UseBorders = False
        Me.xr_pmt_dt.StylePriority.UseFont = False
        Me.xr_pmt_dt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLine2
        '
        Me.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 254.0!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(458.0!, 9.0!)
        Me.xrLine2.StylePriority.UseBorders = False
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'xrPrtNameTtlLbl
        '
        Me.xrPrtNameTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPrtNameTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPrtNameTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrPrtNameTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 244.0!)
        Me.xrPrtNameTtlLbl.Name = "xrPrtNameTtlLbl"
        Me.xrPrtNameTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPrtNameTtlLbl.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
        Me.xrPrtNameTtlLbl.StylePriority.UseBorders = False
        Me.xrPrtNameTtlLbl.StylePriority.UseFont = False
        Me.xrPrtNameTtlLbl.StylePriority.UseForeColor = False
        Me.xrPrtNameTtlLbl.Text = "PRINT NAME:"
        '
        'xrBalanceDetailTbl
        '
        Me.xrBalanceDetailTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrBalanceDetailTbl.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 8.0!)
        Me.xrBalanceDetailTbl.Name = "xrBalanceDetailTbl"
        Me.xrBalanceDetailTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSubTotalTrow, Me.xrTransChrgsTrow, Me.xrTaxChrgTrow, Me.xrTotOrderTrow, Me.xrTotPmtsTrow, Me.xrBalanceTrow})
        Me.xrBalanceDetailTbl.SizeF = New System.Drawing.SizeF(250.0!, 124.0!)
        Me.xrBalanceDetailTbl.StylePriority.UseBorders = False
        '
        'xrSubTotalTrow
        '
        Me.xrSubTotalTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrSubTotalTtlTcel, Me.xr_subtotal})
        Me.xrSubTotalTrow.Name = "xrSubTotalTrow"
        Me.xrSubTotalTrow.Weight = 1.0R
        '
        'xrSubTotalTtlTcel
        '
        Me.xrSubTotalTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSubTotalTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSubTotalTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSubTotalTtlTcel.Name = "xrSubTotalTtlTcel"
        Me.xrSubTotalTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSubTotalTtlTcel.StylePriority.UseBackColor = False
        Me.xrSubTotalTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSubTotalTtlTcel.StylePriority.UseFont = False
        Me.xrSubTotalTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSubTotalTtlTcel.Text = "SubTotal:"
        Me.xrSubTotalTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrSubTotalTtlTcel.Weight = 1.08R
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 1.0899999999999999R
        '
        'xrTransChrgsTrow
        '
        Me.xrTransChrgsTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTransChrgsTtlTcel, Me.xr_del})
        Me.xrTransChrgsTrow.Name = "xrTransChrgsTrow"
        Me.xrTransChrgsTrow.Weight = 1.0R
        '
        'xrTransChrgsTtlTcel
        '
        Me.xrTransChrgsTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTransChrgsTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTransChrgsTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTransChrgsTtlTcel.Name = "xrTransChrgsTtlTcel"
        Me.xrTransChrgsTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTransChrgsTtlTcel.StylePriority.UseBackColor = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseFont = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTransChrgsTtlTcel.Text = "Shipping:"
        Me.xrTransChrgsTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTransChrgsTtlTcel.Weight = 1.08R
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 1.0899999999999999R
        '
        'xrTaxChrgTrow
        '
        Me.xrTaxChrgTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTaxChrgTtlTcel, Me.xr_tax})
        Me.xrTaxChrgTrow.Name = "xrTaxChrgTrow"
        Me.xrTaxChrgTrow.Weight = 1.0R
        '
        'xrTaxChrgTtlTcel
        '
        Me.xrTaxChrgTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTaxChrgTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTaxChrgTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTaxChrgTtlTcel.Name = "xrTaxChrgTtlTcel"
        Me.xrTaxChrgTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTaxChrgTtlTcel.StylePriority.UseBackColor = False
        Me.xrTaxChrgTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTaxChrgTtlTcel.StylePriority.UseFont = False
        Me.xrTaxChrgTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTaxChrgTtlTcel.Text = "Tax:"
        Me.xrTaxChrgTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTaxChrgTtlTcel.Weight = 1.08R
        '
        'xr_tax
        '
        Me.xr_tax.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_tax.StylePriority.UseBorderColor = False
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseTextAlignment = False
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax.Weight = 1.0899999999999999R
        '
        'xrTotOrderTrow
        '
        Me.xrTotOrderTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTotOrderTtlTcel, Me.xr_grand_total})
        Me.xrTotOrderTrow.Name = "xrTotOrderTrow"
        Me.xrTotOrderTrow.Weight = 1.0R
        '
        'xrTotOrderTtlTcel
        '
        Me.xrTotOrderTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTotOrderTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTotOrderTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTotOrderTtlTcel.Name = "xrTotOrderTtlTcel"
        Me.xrTotOrderTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTotOrderTtlTcel.StylePriority.UseBackColor = False
        Me.xrTotOrderTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTotOrderTtlTcel.StylePriority.UseFont = False
        Me.xrTotOrderTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTotOrderTtlTcel.Text = "Total Sale:"
        Me.xrTotOrderTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTotOrderTtlTcel.Weight = 1.08R
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 1.0899999999999999R
        '
        'xrTotPmtsTrow
        '
        Me.xrTotPmtsTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTotPmtsTtlTcel, Me.xr_payment_holder})
        Me.xrTotPmtsTrow.Name = "xrTotPmtsTrow"
        Me.xrTotPmtsTrow.Weight = 1.0R
        '
        'xrTotPmtsTtlTcel
        '
        Me.xrTotPmtsTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTotPmtsTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTotPmtsTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTotPmtsTtlTcel.Name = "xrTotPmtsTtlTcel"
        Me.xrTotPmtsTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTotPmtsTtlTcel.StylePriority.UseBackColor = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseFont = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTotPmtsTtlTcel.Text = "Payments:"
        Me.xrTotPmtsTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTotPmtsTtlTcel.Weight = 1.08R
        '
        'xr_payment_holder
        '
        Me.xr_payment_holder.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_payment_holder.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_payments})
        Me.xr_payment_holder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payment_holder.Name = "xr_payment_holder"
        Me.xr_payment_holder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_payment_holder.StylePriority.UseBorderColor = False
        Me.xr_payment_holder.StylePriority.UseFont = False
        Me.xr_payment_holder.Weight = 1.0899999999999999R
        '
        'xr_payments
        '
        Me.xr_payments.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_payments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.Black
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(126.0!, 21.0!)
        Me.xr_payments.StylePriority.UseBorders = False
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.StylePriority.UseTextAlignment = False
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrBalanceTrow
        '
        Me.xrBalanceTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrBalanceTtlTcel, Me.xr_balance})
        Me.xrBalanceTrow.Name = "xrBalanceTrow"
        Me.xrBalanceTrow.Weight = 1.0R
        '
        'xrBalanceTtlTcel
        '
        Me.xrBalanceTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrBalanceTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrBalanceTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrBalanceTtlTcel.Name = "xrBalanceTtlTcel"
        Me.xrBalanceTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrBalanceTtlTcel.StylePriority.UseBackColor = False
        Me.xrBalanceTtlTcel.StylePriority.UseBorderColor = False
        Me.xrBalanceTtlTcel.StylePriority.UseFont = False
        Me.xrBalanceTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrBalanceTtlTcel.Text = "Balance:"
        Me.xrBalanceTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrBalanceTtlTcel.Weight = 1.08R
        '
        'xr_balance
        '
        Me.xr_balance.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_balance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_balance.StylePriority.UseBorderColor = False
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.Weight = 1.0899999999999999R
        '
        'xrCmntPanl
        '
        Me.xrCmntPanl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCmntPanl.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrCmntTtlLbl})
        Me.xrCmntPanl.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 7.999992!)
        Me.xrCmntPanl.Name = "xrCmntPanl"
        Me.xrCmntPanl.SizeF = New System.Drawing.SizeF(350.0!, 125.0!)
        Me.xrCmntPanl.StylePriority.UseBorders = False
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(350.0!, 17.0!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'xrCmntTtlLbl
        '
        Me.xrCmntTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCmntTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCmntTtlLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrCmntTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrCmntTtlLbl.Name = "xrCmntTtlLbl"
        Me.xrCmntTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCmntTtlLbl.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrCmntTtlLbl.StylePriority.UseBorders = False
        Me.xrCmntTtlLbl.StylePriority.UseFont = False
        Me.xrCmntTtlLbl.StylePriority.UseForeColor = False
        Me.xrCmntTtlLbl.Text = "COMMENTS:"
        '
        'xr_ref
        '
        Me.xr_ref.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ref.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_ref.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 168.0!)
        Me.xr_ref.Multiline = True
        Me.xr_ref.Name = "xr_ref"
        Me.xr_ref.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ref.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
        Me.xr_ref.StylePriority.UseBorders = False
        Me.xr_ref.StylePriority.UseFont = False
        Me.xr_ref.StylePriority.UseTextAlignment = False
        Me.xr_ref.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_merchant
        '
        Me.xr_merchant.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_merchant.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_merchant.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 168.0!)
        Me.xr_merchant.Multiline = True
        Me.xr_merchant.Name = "xr_merchant"
        Me.xr_merchant.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_merchant.SizeF = New System.Drawing.SizeF(141.0!, 17.0!)
        Me.xr_merchant.StylePriority.UseBorders = False
        Me.xr_merchant.StylePriority.UseFont = False
        Me.xr_merchant.StylePriority.UseTextAlignment = False
        Me.xr_merchant.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_amt.LocationFloat = New DevExpress.Utils.PointFloat(498.5166!, 168.0!)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.SizeF = New System.Drawing.SizeF(93.4834!, 17.0!)
        Me.xr_pmt_amt.StylePriority.UseBorders = False
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_appr_cd
        '
        Me.xr_appr_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_appr_cd.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_appr_cd.LocationFloat = New DevExpress.Utils.PointFloat(406.5166!, 168.0!)
        Me.xr_appr_cd.Multiline = True
        Me.xr_appr_cd.Name = "xr_appr_cd"
        Me.xr_appr_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_appr_cd.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xr_appr_cd.StylePriority.UseBorders = False
        Me.xr_appr_cd.StylePriority.UseFont = False
        Me.xr_appr_cd.StylePriority.UseTextAlignment = False
        Me.xr_appr_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_exp_dt
        '
        Me.xr_exp_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_exp_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_exp_dt.LocationFloat = New DevExpress.Utils.PointFloat(340.5166!, 168.0!)
        Me.xr_exp_dt.Multiline = True
        Me.xr_exp_dt.Name = "xr_exp_dt"
        Me.xr_exp_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_exp_dt.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
        Me.xr_exp_dt.StylePriority.UseBorders = False
        Me.xr_exp_dt.StylePriority.UseFont = False
        Me.xr_exp_dt.StylePriority.UseTextAlignment = False
        Me.xr_exp_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrCustSigTtlNLnLbl
        '
        Me.xrCustSigTtlNLnLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustSigTtlNLnLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCustSigTtlNLnLbl.ForeColor = System.Drawing.Color.DimGray
        Me.xrCustSigTtlNLnLbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 283.0!)
        Me.xrCustSigTtlNLnLbl.Name = "xrCustSigTtlNLnLbl"
        Me.xrCustSigTtlNLnLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustSigTtlNLnLbl.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
        Me.xrCustSigTtlNLnLbl.StylePriority.UseBorders = False
        Me.xrCustSigTtlNLnLbl.StylePriority.UseFont = False
        Me.xrCustSigTtlNLnLbl.StylePriority.UseForeColor = False
        Me.xrCustSigTtlNLnLbl.Text = "CUSTOMER SIGNATURE:"
        '
        'xrLine1
        '
        Me.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 292.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(458.0!, 9.0!)
        Me.xrLine1.StylePriority.UseBorders = False
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.SizeF = New System.Drawing.SizeF(800.0!, 2.0!)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xr_triggers
        '
        Me.xr_triggers.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_triggers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_triggers.ForeColor = System.Drawing.Color.Gray
        Me.xr_triggers.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 325.0!)
        Me.xr_triggers.Multiline = True
        Me.xr_triggers.Name = "xr_triggers"
        Me.xr_triggers.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_triggers.SizeF = New System.Drawing.SizeF(797.0!, 17.0!)
        Me.xr_triggers.StylePriority.UseBorders = False
        Me.xr_triggers.StylePriority.UseFont = False
        Me.xr_triggers.StylePriority.UseForeColor = False
        '
        'xrLabel27
        '
        Me.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel27.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 192.0!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(800.0!, 33.0!)
        Me.xrLabel27.StylePriority.UseBorders = False
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPmtTbl
        '
        Me.xrPmtTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrPmtTbl.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPmtTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 150.0!)
        Me.xrPmtTbl.Name = "xrPmtTbl"
        Me.xrPmtTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrPmtTrow})
        Me.xrPmtTbl.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
        Me.xrPmtTbl.StylePriority.UseBorders = False
        Me.xrPmtTbl.StylePriority.UseFont = False
        '
        'xrPmtTrow
        '
        Me.xrPmtTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrPmtDtTtlTcel, Me.xrPmtTpTtlTcel, Me.xrCardNumTtlTcel, Me.xrExpDtTtlTcel, Me.xrApprovalTtlTcel, Me.xrPmtAmtTtlTcel, Me.xrMerchIdTtlTcel, Me.xrRefTtlTcel})
        Me.xrPmtTrow.Name = "xrPmtTrow"
        Me.xrPmtTrow.Weight = 1.0R
        '
        'xrPmtDtTtlTcel
        '
        Me.xrPmtDtTtlTcel.BorderColor = System.Drawing.Color.DimGray
        Me.xrPmtDtTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPmtDtTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPmtDtTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrPmtDtTtlTcel.Name = "xrPmtDtTtlTcel"
        Me.xrPmtDtTtlTcel.StylePriority.UseBorderColor = False
        Me.xrPmtDtTtlTcel.StylePriority.UseBorders = False
        Me.xrPmtDtTtlTcel.StylePriority.UseFont = False
        Me.xrPmtDtTtlTcel.StylePriority.UseForeColor = False
        Me.xrPmtDtTtlTcel.Text = "Date"
        Me.xrPmtDtTtlTcel.Weight = 0.098458333333333328R
        '
        'xrPmtTpTtlTcel
        '
        Me.xrPmtTpTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPmtTpTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPmtTpTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrPmtTpTtlTcel.Name = "xrPmtTpTtlTcel"
        Me.xrPmtTpTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPmtTpTtlTcel.StylePriority.UseBorders = False
        Me.xrPmtTpTtlTcel.StylePriority.UseFont = False
        Me.xrPmtTpTtlTcel.StylePriority.UseForeColor = False
        Me.xrPmtTpTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrPmtTpTtlTcel.Text = "Payment Type"
        Me.xrPmtTpTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrPmtTpTtlTcel.Weight = 0.13070829900105793R
        '
        'xrCardNumTtlTcel
        '
        Me.xrCardNumTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCardNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCardNumTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrCardNumTtlTcel.Name = "xrCardNumTtlTcel"
        Me.xrCardNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCardNumTtlTcel.StylePriority.UseBorders = False
        Me.xrCardNumTtlTcel.StylePriority.UseFont = False
        Me.xrCardNumTtlTcel.StylePriority.UseForeColor = False
        Me.xrCardNumTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrCardNumTtlTcel.Text = " Card Number"
        Me.xrCardNumTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrCardNumTtlTcel.Weight = 0.196479097366333R
        '
        'xrExpDtTtlTcel
        '
        Me.xrExpDtTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrExpDtTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrExpDtTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrExpDtTtlTcel.Name = "xrExpDtTtlTcel"
        Me.xrExpDtTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrExpDtTtlTcel.StylePriority.UseBorders = False
        Me.xrExpDtTtlTcel.StylePriority.UseFont = False
        Me.xrExpDtTtlTcel.StylePriority.UseForeColor = False
        Me.xrExpDtTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrExpDtTtlTcel.Text = " Exp Dt"
        Me.xrExpDtTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrExpDtTtlTcel.Weight = 0.082500036239624014R
        '
        'xrApprovalTtlTcel
        '
        Me.xrApprovalTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrApprovalTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrApprovalTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrApprovalTtlTcel.Name = "xrApprovalTtlTcel"
        Me.xrApprovalTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrApprovalTtlTcel.StylePriority.UseBorders = False
        Me.xrApprovalTtlTcel.StylePriority.UseFont = False
        Me.xrApprovalTtlTcel.StylePriority.UseForeColor = False
        Me.xrApprovalTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrApprovalTtlTcel.Text = " Approval"
        Me.xrApprovalTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrApprovalTtlTcel.Weight = 0.11500000381469726R
        '
        'xrPmtAmtTtlTcel
        '
        Me.xrPmtAmtTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPmtAmtTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPmtAmtTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrPmtAmtTtlTcel.Name = "xrPmtAmtTtlTcel"
        Me.xrPmtAmtTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPmtAmtTtlTcel.StylePriority.UseBorders = False
        Me.xrPmtAmtTtlTcel.StylePriority.UseFont = False
        Me.xrPmtAmtTtlTcel.StylePriority.UseForeColor = False
        Me.xrPmtAmtTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrPmtAmtTtlTcel.Text = "Amount"
        Me.xrPmtAmtTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrPmtAmtTtlTcel.Weight = 0.11568756357828777R
        '
        'xrMerchIdTtlTcel
        '
        Me.xrMerchIdTtlTcel.BorderColor = System.Drawing.SystemColors.ControlText
        Me.xrMerchIdTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrMerchIdTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrMerchIdTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrMerchIdTtlTcel.Name = "xrMerchIdTtlTcel"
        Me.xrMerchIdTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrMerchIdTtlTcel.StylePriority.UseBorderColor = False
        Me.xrMerchIdTtlTcel.StylePriority.UseBorders = False
        Me.xrMerchIdTtlTcel.StylePriority.UseFont = False
        Me.xrMerchIdTtlTcel.StylePriority.UseForeColor = False
        Me.xrMerchIdTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrMerchIdTtlTcel.Text = "Merchant ID"
        Me.xrMerchIdTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrMerchIdTtlTcel.Weight = 0.17933333333333334R
        '
        'xrRefTtlTcel
        '
        Me.xrRefTtlTcel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRefTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrRefTtlTcel.ForeColor = System.Drawing.Color.DimGray
        Me.xrRefTtlTcel.Name = "xrRefTtlTcel"
        Me.xrRefTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrRefTtlTcel.StylePriority.UseBorders = False
        Me.xrRefTtlTcel.StylePriority.UseFont = False
        Me.xrRefTtlTcel.StylePriority.UseForeColor = False
        Me.xrRefTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrRefTtlTcel.Text = "Ref"
        Me.xrRefTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrRefTtlTcel.Weight = 0.081833333333333341R
        '
        'xrTermNCondTtlLbl
        '
        Me.xrTermNCondTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTermNCondTtlLbl.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTermNCondTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 308.0!)
        Me.xrTermNCondTtlLbl.Name = "xrTermNCondTtlLbl"
        Me.xrTermNCondTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTermNCondTtlLbl.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
        Me.xrTermNCondTtlLbl.StylePriority.UseBorders = False
        Me.xrTermNCondTtlLbl.StylePriority.UseFont = False
        Me.xrTermNCondTtlLbl.Text = "TERMS AND CONDITIONS OF SALE:"
        Me.xrTermNCondTtlLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_card_no
        '
        Me.xr_card_no.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_card_no.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_card_no.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 168.0!)
        Me.xr_card_no.Multiline = True
        Me.xr_card_no.Name = "xr_card_no"
        Me.xr_card_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_card_no.SizeF = New System.Drawing.SizeF(157.5166!, 17.0!)
        Me.xr_card_no.StylePriority.UseBorders = False
        Me.xr_card_no.StylePriority.UseFont = False
        Me.xr_card_no.StylePriority.UseTextAlignment = False
        Me.xr_card_no.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_mop
        '
        Me.xr_mop.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_mop.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop.LocationFloat = New DevExpress.Utils.PointFloat(78.76666!, 168.0!)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.SizeF = New System.Drawing.SizeF(104.2333!, 17.0!)
        Me.xr_mop.StylePriority.UseBorders = False
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 142.0!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(800.0!, 2.0!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 10.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'GenericInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 33, 10)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        CType(Me.xrPgHdrTbl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrSoLnTtlTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrDocNumCustCdTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTransInfoTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrCustTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrSoLnTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrBalanceDetailTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_po_num.DataBindings.Add("Text", DataSource, "SO.USR_FLD_1")
        xr_terms.DataBindings.Add("Text", DataSource, "CUST.OPEN_AR_TERM_CD")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_corp_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CORP")
        xr_slsp1_email.DataBindings.Add("Text", DataSource, "EMP.EMAIL_ADDR")
        xr_bill_corp_name.DataBindings.Add("Text", DataSource, "CUST.CORP_NAME")
        xr_bill_full_name.DataBindings.Add("Text", DataSource, "CUST.CUST_FULL_NAME")
        xr_bill_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_bill_addr2.DataBindings.Add("Text", DataSource, "CUST.BILL_ADDR2")
        xr_bill_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_bill_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_bill_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_bill_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_bill_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_sku.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_vend.DataBindings.Add("Text", DataSource, "ITM.VE_CD")
        xr_des.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_ser_num.DataBindings.Add("Text", DataSource, "SO_LN.SER_NUM")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_so_store_cd.DataBindings.Add("Text", DataSource, "SO.so_store_cd")
        xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")
        xr_store_addr.DataBindings.Add("Text", DataSource, "STORE.STORE_ADDRESS")
        xr_store_phone.DataBindings.Add("Text", DataSource, "STORE.STORE_PHONE")
        xr_promo_amt.DataBindings.Add("Text", DataSource, "SO.ORIG_FI_AMT")
        xr_promo_app.DataBindings.Add("Text", DataSource, "SO.APPROVAL_CD")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "Pick-Up"
            Case "D"
                sender.text = "Delivery"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint

        If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "select sum(NVL(CUST_TAX_CHG,0))+sum(NVL(STORE_TAX_CHG,0)) AS TAX_CHG FROM SO_LN WHERE DEL_DOC_NUM='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            sql = sql & "AND SO_LN.VOID_FLAG='N'"

            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read Then
                    If IsNumeric(MyDataReader2.Item("TAX_CHG").ToString) Then
                        sender.text = FormatNumber(MyDataReader2.Item("TAX_CHG").ToString, 2)
                    End If
                End If
                MyDataReader2.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If

    End Sub

    'Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
    '    If Not IsNumeric(sender.text) Then
    '        sender.text = "0.00"
    '    End If
    'End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        sender.text = FormatNumber(xr_qty.Text * xr_ret.Text, 2)
        subtotal = subtotal + (xr_qty.Text * xr_ret.Text)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint, xr_payment_holder.BeforePrint

        'If processed_payments = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        'sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                    xrPmtTbl.Visible = False
                    xrLabel27.Visible = False
                End If
            Else
                sender.text = FormatNumber(0, 2)
                xrPmtTbl.Visible = False
                xrLabel27.Visible = False
            End If
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        Dim pmts As Double = 0

        If IsNumeric(xr_payments.Text) Then
            pmts = CDbl(xr_payments.Text)
        End If
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        sender.text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)

    End Sub

    'Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

    '    If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
    '        If CDbl(xr_disc_amt.Text) > 0 Then
    '            Dim conn As New System.Data.OracleClient.OracleConnection
    '            Dim objSql2 As System.Data.OracleClient.OracleCommand
    '            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
    '            Dim sql As String

    '            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '            conn.Open()

    '            sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

    '            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

    '            Try
    '                'Execute DataReader 
    '                MyDataReader2 = objSql2.ExecuteReader
    '                If MyDataReader2.Read Then
    '                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
    '                        sender.visible = True
    '                        sender.text = MyDataReader2.Item("DES").ToString & " (" & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
    '                    Else
    '                        sender.visible = False
    '                    End If
    '                End If
    '            Catch
    '                Throw
    '            End Try
    '        End If
    '    End If

    'End Sub

    Private Sub xr_mop_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop.BeforePrint

        'If mop_processed = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, TO_CHAR(POST_DT,'MM-DD-YY') AS POST_DT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.MOP_CD from ar_trn a, mop b, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If MyDataReader2.Item("MOP_CD").ToString = "WC" Then
                    xr_ord_tp.Text = "WILL CALL"
                End If
                If MyDataReader2.Item("DES").ToString & "" <> "" Then
                    sender.text = sender.text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                Else
                    sender.text = sender.text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
                End If
                If String.IsNullOrEmpty(MyDataReader2.Item("HOST_REF_NUM").ToString) Then
                    xr_ref.Text = xr_ref.Text & Constants.vbCrLf
                Else
                    xr_ref.Text = xr_ref.Text & MyDataReader2.Item("HOST_REF_NUM").ToString & Constants.vbCrLf
                End If
                If String.IsNullOrEmpty(MyDataReader2.Item("POST_DT").ToString) Then
                    xr_pmt_dt.Text = xr_pmt_dt.Text & Constants.vbCrLf
                Else
                    xr_pmt_dt.Text = xr_pmt_dt.Text & MyDataReader2.Item("POST_DT").ToString & Constants.vbCrLf
                End If
                xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
                If Len(MyDataReader2.Item("BNK_CRD_NUM").ToString) = 19 Then
                    xr_card_no.Text = xr_card_no.Text & Microsoft.VisualBasic.Strings.Right(MyDataReader2.Item("BNK_CRD_NUM").ToString, 16) & Constants.vbCrLf
                ElseIf MyDataReader2.Item("CHK_NUM").ToString & "" <> "" Then
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("CHK_NUM").ToString & Constants.vbCrLf
                Else
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                End If
                If InStr(MyDataReader2.Item("MID").ToString, "MID:") > 0 Then
                    merchant_id = Replace(MyDataReader2.Item("MID").ToString, "MID:", "")
                    If IsNumeric(merchant_id) Then merchant_id = CDbl(merchant_id)
                    xr_merchant.Text = xr_merchant.Text & merchant_id & Constants.vbCrLf
                Else
                    xr_merchant.Text = xr_merchant.Text & Constants.vbCrLf
                End If
                'xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                If Not String.IsNullOrEmpty(MyDataReader2.Item("EXP_DT").ToString) Then
                    xr_exp_dt.Text = xr_exp_dt.Text & "xx/xx" & Constants.vbCrLf
                Else
                    xr_exp_dt.Text = xr_exp_dt.Text & Constants.vbCrLf
                End If
                xr_appr_cd.Text = xr_appr_cd.Text & MyDataReader2.Item("APP_CD").ToString & Constants.vbCrLf
            Loop
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE IN('D','S') ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
            Catch
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Private Sub xr_triggers_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_triggers.BeforePrint

        If triggers_processed <> True Then
            Dim conn2 As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            If xr_ord_srt.Text & "" <> "" Then
                sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                sql = sql & "VALUES('" & xr_del_doc_num.Text & "','SORT_CODE','" & xr_ord_srt.Text & "')"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
            End If

            If xr_pd.Text & "" <> "" Then
                sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                sql = sql & "VALUES('" & xr_del_doc_num.Text & "','P_D','" & Microsoft.VisualBasic.Strings.Left(xr_pd.Text, 1) & "')"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
            End If

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','STORE_CD','" & Microsoft.VisualBasic.Strings.Mid(xr_del_doc_num.Text, 6, 2) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "SELECT c.ITM_TP_CD, b.MJR_CD, c.MNR_CD, a.ITM_CD, c.DROP_DT FROM SO_LN a, INV_MNR b, ITM c "
            sql = sql & "WHERE a.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND c.MNR_CD=b.MNR_CD "
            sql = sql & "AND a.ITM_CD=c.ITM_CD"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_TP_CD','" & MyDataReader2.Item("ITM_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MAJOR_CD','" & MyDataReader2.Item("MJR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MINOR_CD','" & MyDataReader2.Item("MNR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_CD','" & MyDataReader2.Item("ITM_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ORD_TP_CD','" & xr_ord_tp.Text.ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DROP_DT").ToString) Then
                        sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                        sql = sql & "VALUES('" & xr_del_doc_num.Text & "','DROP_DT','" & MyDataReader2.Item("DROP_DT").ToString & "')"

                        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql2.ExecuteNonQuery()
                    End If
                Loop
            Catch
                Throw
            End Try

            sql = "select trigger_response, TRIGGER_ID "
            sql = sql & "from invoice_triggers, invoice_triggers_temp "
            sql = sql & "where invoice_triggers.trigger_field = invoice_triggers_temp.trigger_field "
            sql = sql & "and invoice_triggers.trigger_value=invoice_triggers_temp.trigger_value "
            sql = sql & "and del_doc_num='" & xr_del_doc_num.Text & "' "
            sql = sql & "GROUP BY TRIGGER_ID, trigger_response"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_triggers.Text = xr_triggers.Text & MyDataReader2.Item("TRIGGER_RESPONSE").ToString & Constants.vbCrLf
                Loop
            Catch
                Throw
            End Try

            If Not String.IsNullOrEmpty(xr_triggers.Text) Then
                xr_triggers.Text = xr_triggers.Text & Constants.vbCrLf
            End If

            sql = "DELETE FROM INVOICE_TRIGGERS_TEMP WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            triggers_processed = True
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES INVOICE"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT"
        End Select

    End Sub

    Private Sub xr_store_addr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr.BeforePrint

        xr_store_addr.Text = StrConv(xr_store_addr.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = xr_store_name.Text

    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2_hidden.BeforePrint

        If sender.Text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME, EMP.ADDR2, EMP.EMAIL_ADDR "
            sql = sql & "FROM SO, EMP WHERE SO.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' "
            sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD2  "

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_slsp2.Text = MyDataReader2.Item("FULL_NAME").ToString
                    xr_slsp2_email.Text = MyDataReader2.Item("EMAIL_ADDR").ToString.ToLower
                Loop
            Catch
                Throw
            End Try
        Else
            xr_slsp2.Text = xr_slsp.Text
            xr_slsp2_email.Text = xr_slsp1_email.Text
        End If

    End Sub

    Private Sub xr_sale_last_four_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_sale_last_four.BeforePrint

        If Len(xr_del_doc_num.Text) = 11 Then
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 4)
        Else
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 5)
        End If

    End Sub

    Private Sub xr_slsp1_email_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp1_email.BeforePrint

        If sender.text & "" <> "" Then
            sender.text = sender.text.ToString.ToLower
        End If

    End Sub

    Private Sub xr_fin_co_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_fin_co.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select asp_promo.des, asp.name from so_asp, asp_promo, asp, so "
        sql = sql & "where so_asp.del_doc_num='" & xr_del_doc_num.Text & "' and asp.cust_cd=so.fin_cust_cd "
        sql = sql & "and so_asp.del_doc_num=so.del_doc_num and asp_promo.promo_cd=SO_ASP.PROMO_CD and asp.as_cd=asp_promo.as_cd"

        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                xr_fin_co.Text = MyDataReader2.Item("NAME").ToString
                xr_promo.Text = MyDataReader2.Item("DES").ToString.ToLower
            Loop
        Catch
            Throw
        End Try

    End Sub

End Class