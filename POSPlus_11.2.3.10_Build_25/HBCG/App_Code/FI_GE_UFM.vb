Public Class FI_GE_UFM
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim Body_text As String = ""

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_text As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_sales_assoc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_text_complete As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Private WithEvents xr_cust As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_sal_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "FI_GE_UFM.resx"
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_text = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_text_complete = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xr_sal_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_sales_assoc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_text})
        Me.Detail.HeightF = 2.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_text
        '
        Me.xr_text.CanShrink = True
        Me.xr_text.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_text.LocationFloat = New DevExpress.Utils.PointFloat(758.0!, 0.0!)
        Me.xr_text.Name = "xr_text"
        Me.xr_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_text.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_text.StylePriority.UseFont = False
        Me.xr_text.Visible = False
        '
        'xr_text_complete
        '
        Me.xr_text_complete.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_text_complete.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_text_complete.Multiline = True
        Me.xr_text_complete.Name = "xr_text_complete"
        Me.xr_text_complete.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_text_complete.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
        Me.xr_text_complete.StylePriority.UseFont = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel10, Me.xrLabel9, Me.xrLabel8, Me.xrLabel7, Me.xrLine4, Me.xrLine3, Me.xrLine2, Me.xrLine1, Me.xrPageBreak1, Me.xrLabel1})
        Me.ReportFooter.HeightF = 264.9167!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 50.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.StylePriority.UseTextAlignment = False
        Me.xrLabel10.Text = "DATE"
        Me.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel9
        '
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 125.0!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.StylePriority.UseTextAlignment = False
        Me.xrLabel9.Text = "DATE"
        Me.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 125.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(375.0!, 25.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.StylePriority.UseTextAlignment = False
        Me.xrLabel8.Text = "JOINT CARDHOLDER SIGNATURE"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 50.0!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(375.0!, 25.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "PRIMARY CARDHOLDER SIGNATURE"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLine4
        '
        Me.xrLine4.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 117.0!)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.SizeF = New System.Drawing.SizeF(167.0!, 8.0!)
        '
        'xrLine3
        '
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 42.0!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(167.0!, 8.0!)
        '
        'xrLine2
        '
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 117.0!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(484.0!, 8.0!)
        '
        'xrLine1
        '
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 42.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(484.0!, 8.0!)
        '
        'xrPageBreak1
        '
        Me.xrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 225.0!)
        Me.xrPageBreak1.Name = "xrPageBreak1"
        '
        'xrLabel1
        '
        Me.xrLabel1.ForeColor = System.Drawing.Color.White
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 225.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "xrLabel1"
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_sal_dt, Me.xr_cust, Me.xrLabel4, Me.xr_sales_assoc, Me.xrLabel2, Me.xr_full_name, Me.xrLabel6, Me.xrLabel5, Me.xr_del_doc_num, Me.xr_ord_tp})
        Me.ReportHeader.HeightF = 165.8333!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xr_sal_dt
        '
        Me.xr_sal_dt.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sal_dt.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 59.0!)
        Me.xr_sal_dt.Name = "xr_sal_dt"
        Me.xr_sal_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sal_dt.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_sal_dt.StylePriority.UseFont = False
        Me.xr_sal_dt.StylePriority.UseTextAlignment = False
        Me.xr_sal_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_cust
        '
        Me.xr_cust.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 42.0!)
        Me.xr_cust.Name = "xr_cust"
        Me.xr_cust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_cust.StylePriority.UseFont = False
        Me.xr_cust.StylePriority.UseTextAlignment = False
        Me.xr_cust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel4
        '
        Me.xrLabel4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(445.0!, 108.0!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(133.0!, 16.0!)
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseForeColor = False
        Me.xrLabel4.Text = "SALES ASSOCIATE:"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_sales_assoc
        '
        Me.xr_sales_assoc.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sales_assoc.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 108.0!)
        Me.xr_sales_assoc.Name = "xr_sales_assoc"
        Me.xr_sales_assoc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sales_assoc.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_sales_assoc.StylePriority.UseFont = False
        Me.xr_sales_assoc.StylePriority.UseTextAlignment = False
        Me.xr_sales_assoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(486.0!, 92.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseForeColor = False
        Me.xrLabel2.Text = "SOLD TO:"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_full_name
        '
        Me.xr_full_name.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 91.0!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseFont = False
        Me.xr_full_name.StylePriority.UseTextAlignment = False
        Me.xr_full_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(478.0!, 59.0!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseForeColor = False
        Me.xrLabel6.Text = "SALES DATE:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(478.0!, 42.0!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseForeColor = False
        Me.xrLabel5.Text = "ACCOUNT #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 25.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 25.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES ORDER #:"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_text_complete})
        Me.GroupFooter1.HeightF = 17.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 0.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 0.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'FI_GE_UFM
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportFooter, Me.ReportHeader, Me.GroupFooter1, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 0, 0)
        Me.Version = "11.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

#End Region

    Public Sub BindUFMVariables()

        'Bind the database fields to their corresponding report labels
        xr_text.DataBindings.Add("Text", DataSource, "TEXT")
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "DEL_DOC_NUM")

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT SO.SO_WR_DT, SO.CUST_CD, CUST.FNAME, CUST.LNAME, EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME "
        sql = sql & "FROM SO, EMP, CUST WHERE SO.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' "
        sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD1  "
        sql = sql & "AND SO.CUST_CD=CUST.CUST_CD "
        sql = sql & "ORDER BY SO.DEL_DOC_NUM"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                xr_full_name.Text = MyDataReader2.Item("FNAME").ToString & " " & MyDataReader2.Item("LNAME").ToString
                xr_cust.Text = MyDataReader2.Item("CUST_CD").ToString
                If IsDate(MyDataReader2.Item("SO_WR_DT").ToString) Then
                    xr_sal_dt.Text = FormatDateTime(MyDataReader2.Item("SO_WR_DT").ToString, DateFormat.ShortDate)
                End If
                xr_sales_assoc.Text = MyDataReader2.Item("FULL_NAME").ToString
            End If
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Private Sub xr_text_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_text.BeforePrint

        If Me.GetCurrentColumnValue("TEXT").ToString & "" = "" Then
            Body_text = Body_text & Chr(10) & Chr(13)
        Else
            Body_text = Body_text & Me.GetCurrentColumnValue("TEXT").ToString & " "
        End If

    End Sub

    Private Sub xr_text_complete_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_text_complete.BeforePrint

        xr_text_complete.Text = Body_text

    End Sub
End Class