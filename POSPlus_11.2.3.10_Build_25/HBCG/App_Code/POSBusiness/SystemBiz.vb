﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient

Public Class SystemBiz

    Private theSystemDac As SystemDac

    ''' <summary>
    ''' Loads and caches the system parameters when called from login
    ''' </summary>
    Public Sub LoadSysPms()

        Dim aSysPm As String

        'these parameters have no name, therefore have to be obtained this way
        SysPms.isCashierActiv = StringUtils.ResolveYNToBoolean(GetSysPm("POS1", 1, 1))
        SysPms.isPrcByStoreGrp = StringUtils.ResolveYNToBoolean(GetSysPm("MERCH1", 48, 1))
        SysPms.reapplyDisc = StringUtils.ResolveYNToBoolean(GetSysPm("SALES1", 209, 1))
        SysPms.isSpiffEnabled = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SPIFF_ENABLED))
        SysPms.isSpiffBySKU = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SPIFF_BY_SKU))

        'MM-5693
        SysPms.isAutoFinalizeMCRMDB = StringUtils.ResolveYNToBoolean(GetSysPm("SALES1", 188, 1))

        SysPms.isRFEnabled = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.RF_ENABLED))
        SysPms.isSerialEnabled = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SERIAL_ENABLED))
        SysPms.isOne2ManyWarr = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.ONE_WARR_TO_MANY))
        SysPms.isDelvSchedEnabled = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.DELV_SCHED_ENABLED))
        SysPms.isDelvByPoints = "P".Equals(GetSysPm(SysPmsConstants.DELV_STOPS_POINTS))

        If SysPms.isDelvByPoints Then
            SysPms.pointOverScheduling = GetSysPm(SysPmsConstants.POINT_OVER_SCHEDULING)
        End If

        SysPms.preventFinalSOonIST = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.PREVENT_FINALIZE_SO_LINKEDTO_IST))
        SysPms.priorityFillForInvAvail = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.PRIORITY_FILL_FOR_INV_AVAIL))
        SysPms.isPriorityFillByZone = "Z".Equals(GetSysPm(SysPmsConstants.PRIORITY_FILL_BY)) ' Z = by zone, S = by store

        SysPms.commissionOnDelvCharge = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.COMMISSION_ON_DELV_CHARGE))
        SysPms.commissionOnSetupCharge = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.COMMISSION_ON_SETUP_CHARGE))

        SysPms.allowMultDisc = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.ALLOW_MULT_DISC))
        SysPms.multiDiscCd = GetSysPm(SysPmsConstants.MULTI_DISC_CD)
        'MM-5661
        SysPms.discWithAppSec = GetSysPm("SALES1", 26, 1)
        SysPms.specialOrderDropCd = GetSysPm(SysPmsConstants.SPECIAL_ORDER_DROP_CD)
        'SysPms.shuDate = GetSysPm(SysPmsConstants.SHU_DATE)  'last time Sales History ran  - DO NOT USE HERE - SALES HISTORY DATE MUST ALWAYS USE CURRENT DATABASE VALUE - CANNOT BE LOADED ONCE
        ' USE SessVar.shuDt instead 
        SysPms.skipTransDate = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SKIP_TRANS_DT))

        '====================================================================================================
        'For Integer expected values, added check, because if NO value has been set in E1 for the SYSPM, 
        'the APP will NOT launch, it throws error "Input string was not in a correct format"
        aSysPm = GetSysPm(SysPmsConstants.SPECIAL_ORDER_NUM_DAYS)
        SysPms.specialOrderNumDays = If(String.IsNullOrEmpty(aSysPm), 0, CInt(aSysPm))

        aSysPm = GetSysPm(SysPmsConstants.NUM_DAYS_POST_TRANSDT)
        SysPms.numDaysPostDtTrans = If(String.IsNullOrEmpty(aSysPm), 0, CInt(aSysPm))

        aSysPm = GetSysPm(SysPmsConstants.CONFIRMATION_DAYS)
        SysPms.numDaysToConfirmSO = If(String.IsNullOrEmpty(aSysPm), 0, CInt(aSysPm))

        SysPms.cmtNewItems = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.NEW_ITEMS_ADDED))
        SysPms.cmtSkuChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SKU_CHNG))
        SysPms.cmtQtyChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.QTY_CHNG))
        SysPms.cmtPriceChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.PRICE_CHNG))
        SysPms.cmtTaxChargeChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.TAX_CHARGE_CHNG))
        SysPms.cmtDelChargeChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.DELV_CHARGE_CHNG))
        SysPms.cmtSetupChargeChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.SETUP_CHARGE_CHNG))
        SysPms.cmtPuDelFlagChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.PU_DEL_FLAG_CHNG))
        SysPms.cmtPuDelDateChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.PU_DEL_DATE_CHNG))
        SysPms.cmtZoneCdChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.ZONE_CD_CHNG))
        SysPms.cmtFinCompanyChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.FIN_COMPANY_CHNG))
        SysPms.cmtFinAmountChng = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.FIN_AMOUNT_CHNG))
        SysPms.deliveryChargeApproval = GetSysPm("SALES1", 205, 1)
        SysPms.allowCODBalance = GetSysPm("SOM1", 19, 1)

        '=====================================================================================================
        '=================== THE FOLLOWING VALUES ARE CURRENTLY DEFINED AS GP-PARMs IN E1. ===================
        ' Later, any value using the PTS_POS_KEY may change to be System Parameters, at that point, only the
        ' way these values are read will have to change (in this class).
        '=====================================================================================================
        aSysPm = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.RELATIONSHIP_DISPLAY_DAYS)
        SysPms.numDaysRelship = (If(String.IsNullOrEmpty(aSysPm) OrElse Not IsNumeric(aSysPm), 0, CInt(aSysPm)))

        aSysPm = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.SEARCH_DROP_ITM_DEFAULT)
        SysPms.searchDropSkus = StringUtils.ResolveYNToBoolean(aSysPm)

        'App time out will default to 10 minutes if no value was defined in E1
        'NOTE: Timeout is needed in seconds when used. Therefore this variable represents seconds
        aSysPm = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.APP_TIMEOUT)
        SysPms.appTimeOut = (If(String.IsNullOrEmpty(aSysPm) OrElse aSysPm = 0, 600, CInt(aSysPm) * 60)).ToString()
        'Extra time added to compensate for app processing time, not accounted to user
        SysPms.appTimeOut = SysPms.appTimeOut + 45

        SysPms.paidInFullCalc = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.PAID_IN_FULL_CALC)
        SysPms.sbobRowProcess = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.SBOB_ROW_PROCESS)
        SysPms.sbobPoRcmConfirm = GetGpParm(SysPmsConstants.PTS_POS_KEY, SysPmsConstants.SBOB_PO_RCM_CONFIRM)

        '============================ This set is base E1 parameters, not PTS_POS. ============================
        Dim endOfTime As String = GetGpParm(SysPmsConstants.END_OF_TIME, SysPmsConstants.END_OF_TIME)
        SysPms.endOfTime = DateTime.Parse(endOfTime.Substring(0, 7) + "20" + endOfTime.Substring(7, 2))         'sets century correctly
        SysPms.begOfTime = DateTime.Parse(GetGpParm(SysPmsConstants.BEG_OF_TIME, SysPmsConstants.BEG_OF_TIME))  'century default to 19
        SysPms.maxExpDt = GetGpParm(SysPmsConstants.PTS_ARASM_KEY, SysPmsConstants.DEF_MAX_EXP_DT)
        SysPms.updateTruckDate = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.UPDATE_TRUCK_DATE))
        SysPms.crmAffectDelAvail = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.CRM_AFFECT_DEL_AVAIL))
        SysPms.enablePickup = StringUtils.ResolveYNToBoolean(GetSysPm(SysPmsConstants.ENABLE_PICKUP))
        SysPms.warrantyStartDate = GetSysPm(SysPmsConstants.WARRANTY_START_DATE)
        SysPms.PadDaysForReceivingPOandIST = GetSysPm(SysPmsConstants.AVAIL_LEAD_DAYS)
        '=====================================================================================================
        '=============================== END OF VALUES DEFINED AS GP-PARMs IN E1. ============================
        '=====================================================================================================

    End Sub

    ''' <summary>
    ''' Returns the value of the system parameter from E1 using SYSPM_REF - is the most
    ''' efficient way to call for system parameter info if the param has a name.
    ''' </summary>
    ''' <param name="paramName">system parameter name</param>
    ''' <returns>the value of the system parameter from E1</returns>
    Public Function GetSysPm(ByVal paramName As String) As String
        Return GetSystemDac().GetSysPm(paramName)
    End Function

    ''' <summary>
    ''' Returns the value of the system parameter from E1 without using SYSPM_REF. This is used when 
    ''' there is not a distinct name available for the system parameter.
    ''' </summary>
    ''' <param name="key">Subsystem key to get the parameter from </param>
    ''' <param name="startPos">starting position to read the value</param>
    ''' <param name="length">number of characters to fetch</param>
    ''' <returns>The value of the system parameter requested</returns>
    Public Function GetSysPm(ByVal key As String, ByVal startPos As Integer, ByVal length As Integer) As String
        Return GetSystemDac().GetSysPm(key, startPos, length)
    End Function

    ''' <summary>
    '''Returns from the GP PARMS table, the ONE value for the key and parm passed in
    ''' </summary>
    ''' <param name="key">the key to look the parameter on</param>
    ''' <param name="parm">the parameter name</param>
    ''' <returns>the corresponding value found or an empty string if no match found</returns>
    Public Function GetGpParm(ByVal key As String, ByVal parm As String) As String
        Return (GetSystemDac().GetGpParm(key, parm)).Trim()
    End Function

    ''' <summary>
    '''Returns from the GP PARMS table, the MULTIPLE values that match the key and parm passed in
    ''' </summary>
    ''' <param name="key">the key to look the parameter on</param>
    ''' <param name="parm">the parameter name to be used in a LIKE clause</param>
    ''' <returns>the corresponding values found or an empty array if no matches found</returns>
    Public Function GetGpParmValues(ByVal key As String, ByVal parm As String) As String()
        Return (GetSystemDac().GetGpParmValues(key, parm))
    End Function


    ''' <summary>
    ''' Inserts a record to the CRM.AUDIT_LOG table with details passed in.   
    ''' The data will be automatically saved in the database.
    ''' </summary>
    ''' <param name="columnId">the column ID to be saved</param>
    ''' <param name="columnDesc">the column DESCRIPTION to be saved</param>
    ''' <param name="empCd">the current employee code</param>
    Public Sub SaveAuditLogComment(ByVal columnId As String,
                                   ByVal columnDesc As String,
                                   Optional ByVal empCd As String = "")
        If empCd & "" = "" Then empCd = "UNKNOWN"
        GetSystemDac().SaveAuditLogComment(columnId, columnDesc, empCd)
    End Sub

    ''' <summary>
    ''' Inserts a record to the CRM.AUDIT_LOG table with details passed in
    ''' If the command passed in, is part of a single transaction, the data will not be
    ''' automatically saved in the database, rather is only queued in the command object 
    ''' passed in, and the caller is in charge of doing the commit or rollback.
    ''' </summary>
    ''' <param name="dbCommand">the database command to be used </param>
    ''' <param name="columnId">the column ID to be saved</param>
    ''' <param name="columnDesc">the column DESCRIPTION to be saved</param>
    ''' <param name="empCd">the current employee code</param>
    Public Sub SaveAuditLogComment(ByRef dbCommand As OracleCommand,
                                   ByVal columnId As String,
                                   ByVal columnDesc As String,
                                   Optional ByVal empCd As String = "")

        If empCd & "" = "" Then empCd = "UNKNOWN"
        GetSystemDac().SaveAuditLogComment(dbCommand, columnId, columnDesc, empCd)
    End Sub

    ''' <summary>
    ''' Retreives the list of all companies setup in the system with their code and descriptions.
    ''' </summary>
    ''' <returns>a list of all the companies defined in the system.</returns>
    Public Function GetCompanies() As DataSet
        Return GetSystemDac().GetCompanies()
    End Function

    ''' <summary>
    ''' Performs validation and retrieval of necessary employee info for the login process.
    ''' </summary>
    ''' <param name="companyCd">the code of the company logging into.</param>
    ''' <param name="companyPwd">teh password entered for the selected company</param>
    ''' <param name="userInit">the login user's initials.</param>
    ''' <returns>the response object with the employee details for the user logging in.</returns> 
    Public Function DoLogin(ByVal companyCd As String,
                                ByVal companyPwd As String,
                                ByVal userInit As String) As LoginResponseDtc

        Dim response As New LoginResponseDtc
        Dim isCoPwdValid As Boolean = True

        If (companyPwd.isNotEmpty) Then
            Dim coPwd As String = GetCompanyPwd(companyCd)
            If coPwd.isNotEmpty AndAlso Not (UCase(coPwd).Equals(UCase(companyPwd))) Then
                response.message = (Resources.POSErrors.ERR0002)
                'TODO -see if can insert Auto_HBCG_Comments("LOGIN_FAILURE", UCase(userInit) & " - UserName not found in the system*", UCase(userInit))
                isCoPwdValid = False
            End If
        End If

        If isCoPwdValid Then
            Dim emp As EmployeeDtc = GetSystemDac.GetEmployeeInfo(userInit)
            If IsNothing(emp) Then
                response.message = (Resources.POSErrors.ERR0001)
            Else
                response.emp = emp
            End If
        End If

        Return response
    End Function
    ''' <summary>
    ''' Finds out if the employee passed in, has access to the page indicated
    ''' </summary>
    ''' <param name="empCd">the employee code in question</param>
    ''' <param name="pName">the page name to check for access</param>
    ''' <returns>TRUE, if employee has access; FALSE otherwise</returns>
    Public Function HasPageAccess(ByVal empCd As String, pName As String) As Boolean
        Return GetSystemDac().HasPageAccess(empCd, pName)
    End Function

    ''' <summary>
    ''' Retrieves the company password for the passed-in company code.
    ''' </summary>
    ''' <returns>the company password.</returns>
    Public Function GetCompanyPwd(ByVal coCd As String) As String
        Return GetSystemDac().GetCompanyPwd(coCd)
    End Function


    ''' <summary>
    ''' Retrieves all the cash drawers for the specified store.
    ''' </summary>
    ''' <param name="storeCd">the store for which to get cash drawers</param>
    ''' <returns>a dataset containing all the  cash drawers.</returns>
    Public Function GetCashDrawers(ByVal storeCd As String) As DataSet
        Return GetSystemDac().GetCashDrawers(storeCd)
    End Function

    ''' <summary>
    ''' Returns the company code for the passed-in store.
    ''' </summary>
    ''' <param name="storeCd">the store for which to get the company code</param>
    ''' <returns>the company code</returns>
    Public Function GetCompanyCd(ByVal storeCd As String) As String
        Return GetSystemDac().GetCompanyCd(storeCd)
    End Function

    ''' <summary>
    ''' Returns the employee code for the employee initials passed in
    ''' </summary>
    ''' <param name="empInit">the employee initials to retrieve the emp code for</param>
    ''' <returns>the corresponding Employee code</returns>
    Public Function GetEmployeeCode(ByVal empInit As String) As String

        Dim empCd As String = String.Empty
        If ((Not IsDBNull(empInit)) AndAlso empInit.isNotEmpty()) Then
            empCd = GetSystemDac().GetEmployeeCode(empInit)
        End If
        Return empCd

    End Function

    ''' <summary>
    ''' Determines whether the employee based on the passed-in password and date is 
    ''' a valid 'approver' within the effective and end date for that emp type. If 
    ''' successful, the emloyee code of the approver is returned.
    ''' </summary>
    ''' <param name="empPass">emp password</param>
    ''' <param name="empEffDt">the effective date to use for validation</param>
    ''' <returns>the emp_cd of the 'approver' </returns
    Public Function GetApprover(ByVal empPass As String, ByVal empEffDt As String) As String
        Return GetSystemDac().GetApprover(empPass, empEffDt)
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetSystemDac() As SystemDac
        If (theSystemDac Is Nothing) Then
            theSystemDac = New SystemDac()
        End If

        Return theSystemDac
    End Function


End Class
