﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports OrderUtils
Imports System
Imports System.Linq

Public Class SalesBiz

    Private theInvBiz As InventoryBiz
    Private theCommentsDac As CommentsDac
    Private theSalesDac As SalesDac
    Private theStoreDac As StoreDac
    Private theTMDac As TransportationDac
    Private theTaxDac As TaxDac
    Private theSkuDac As SKUDac
    Private theDiscDac As DiscountDac


    ''' <summary>
    ''' Validates the store last balanced date against the transaction date provided
    ''' If valid, a blank string is returned; if the store has been closed thru the 
    ''' transaction date, then a store closed thru message is returned
    ''' </summary>
    ''' <param name="storeCd">the store code to be checked</param>
    ''' <param name="tranDt">the transaction date to used in the check</param>
    ''' <returns>a message if the store has been closed or an empty string if not</returns>
    Public Function ValidateStoreClosedDate(ByVal storeCd As String, ByVal tranDt As String) As String

        Dim msg As String = String.Empty
        If (Not String.IsNullOrEmpty(storeCd)) Then
            Dim storeInfo As StoreData = GetStoreInfo(storeCd)
            Dim lastBalDt As Date = storeInfo.lastBalDt
            If (lastBalDt >= tranDt) Then
                msg = String.Format(Resources.POSMessages.MSG0008, storeCd, FormatDateTime(lastBalDt.ToString, DateFormat.ShortDate))
            End If
        End If

        Return msg
    End Function

    ''' <summary>
    ''' Applies the rules for the Confirmation Status vlaue.
    ''' </summary>
    ''' <param name="request">custom object with details needed to perform the validation</param>
    ''' <returns>custom object with the validation results</returns>
    Public Function ValidateConfirmationStatus(ByVal request As ConfStatusRequestDtc) As ConfStatusResponseDtc

        Dim response As New ConfStatusResponseDtc
        Dim currConfCodeInfo() As DataRow = request.confirmationCodes.Select("CODE = '" & request.currConfCode & "'")
        Dim confirmdCodeInfo() As DataRow = request.confirmationCodes.Select("IS_CONFIRMED ='Y'")
        Dim isConfirmedOrAbove As Boolean = (currConfCodeInfo.Length > 0 AndAlso confirmdCodeInfo.Length > 0 AndAlso
                                             (currConfCodeInfo(0)("CODE") = confirmdCodeInfo(0)("CODE") OrElse
                                              currConfCodeInfo(0)("SEQ#") > confirmdCodeInfo(0)("SEQ#")))
        Dim confCodeWasChanged As Boolean = (request.currConfCode <> request.origConfCode)
        Dim todaysDate As Date = Date.Now
        Dim zoneLeadDays As Integer = GetTMDac().GetZoneLeadDays(request.pkpDelZone)
        Dim daysToConfirm As Integer = SysPms.numDaysToConfirmSO + zoneLeadDays
        Dim confFinalCode As String = String.Empty
        confFinalCode = (If(IsDBNull(confirmdCodeInfo(0)("CODE")), String.Empty, confirmdCodeInfo(0)("CODE").ToString)).Trim().ToUpper()
        'Date rule: Confirmed status can only be set during period:  
        '(P/D date - (ConfirmationDays + ZoneLead days)), inclusive of the P/D date 'MM-11513
        Dim isDateWithinRange As Boolean = (todaysDate.ToShortDateString <= request.pkpDelDate AndAlso
                                            todaysDate.ToShortDateString >= request.pkpDelDate.AddDays(-daysToConfirm))
        response.allowSaveOrder = True 'By default allow the sales order to save!!
        'response.IsConfirmationValidationPassed = True
        Dim isQualified As Boolean = True
        If isDateWithinRange Then
            response.allowSaveOrder = True
            response.confCode = request.currConfCode    'keeps the code selected 
        Else
            If (confCodeWasChanged AndAlso isConfirmedOrAbove) Then
                'Conf Code changed to Confirmed 
                response.allowSaveOrder = False
                response.confCode = request.origConfCode    'restores the original code
                response.message = Resources.POSErrors.ERR0007

            ElseIf (confCodeWasChanged AndAlso Not isConfirmedOrAbove) OrElse (Not confCodeWasChanged) Then
                'Conf Code changed but not to Confirmed OR Conf Code didn't change at all
                'response.allowSaveOrder = False
                ' Daniela April 7 as per Jacquie 
                If request.currConfCode = "UNC" Then
                    response.allowSaveOrder = True
                Else
                    response.allowSaveOrder = False
                End If
                'response.confCode = request.origConfCode ' request.confirmationCodes.Rows(0)("CODE")       'resets to first unconfirmed code
                ' Daniela change to Unconfirm
                response.confCode = "UNC"
                response.message = Resources.POSMessages.MSG0005
                isQualified = False
                response.confirmationErrorMessage.Append(Resources.POSErrors.ERR0052)
            End If
            response.message = response.message + "<BR />"
        End If

        'For SAL documents additional rules (Reservations & PaidInFull) apply, response object will change accordingly
        If (AppConstants.Order.TYPE_SAL = request.orderType) Then
            ' ---- Checks for Reservations
            Dim isFullyReserved As Boolean = True
            Dim lnsTable = request.lnsData.Tables(0)
            For Each ln As DataRow In lnsTable.Rows
                If (Not "Y".Equals(ln("VOID_FLAG"))) Then  'only process Non-Voided lines
                    If (AppConstants.Sku.TP_INV.Equals(ln("ITM_TP_CD")) AndAlso
                        (IsDBNull(ln("STORE_CD")) OrElse String.IsNullOrEmpty(ln("STORE_CD")) OrElse
                         IsDBNull(ln("LOC_CD")) OrElse String.IsNullOrEmpty(ln("LOC_CD")))) Then
                        isFullyReserved = False
                        Exit For   'no need to keep on searching, found a non-reserved line
                    End If
                End If
            Next

            If (confCodeWasChanged AndAlso Not confFinalCode = request.currConfCode.Trim.ToUpper()) Then
                'if the confirmation status is not changed to Confirmed
                response.allowSaveOrder = response.allowSaveOrder And True
            Else
                'Status is chaged to final
                'Check for other conditions
                '1. Order must be paid in full and lines are filled
                '2. OR order is not paid in full and user has CONFRM_PIF  security  and  lines are filled
                If confCodeWasChanged AndAlso request.isPaidInFull AndAlso isFullyReserved Then
                    response.allowSaveOrder = response.allowSaveOrder And True
                ElseIf confCodeWasChanged AndAlso Not request.isPaidInFull AndAlso isFullyReserved AndAlso request.hasConfirmPIFSecurity Then
                    response.allowSaveOrder = response.allowSaveOrder And True
                ElseIf confCodeWasChanged Then
                    response.allowSaveOrder = False
                    If Not request.isPaidInFull Then response.message = response.message + Resources.POSErrors.ERR0009 + "<BR />"
                    If Not isFullyReserved Then response.message = response.message + Resources.POSErrors.ERR0008 + "<BR />"
                    response.confCode = request.origConfCode    'restores the original code
                ElseIf Not confCodeWasChanged AndAlso isConfirmedOrAbove Then

                    Dim msg As String = response.confirmationErrorMessage.ToString()
                    Dim index As Integer
                    If Not isFullyReserved Then
                        isQualified = False
                        index = msg.IndexOf(Resources.POSErrors.ERR0049)
                        If index = -1 Then
                            'Dont append the duplicate messages!!
                            response.confirmationErrorMessage.Append(Resources.POSErrors.ERR0049)
                        End If
                    ElseIf isFullyReserved AndAlso Not request.isPaidInFull AndAlso Not request.hasConfirmPIFSecurity Then
                        isQualified = False

                        Dim comp As StringComparison = StringComparison.Ordinal
                        Index = msg.IndexOf(Resources.POSErrors.ERR0050)
                        If index = -1 Then
                            'Dont append the duplicate messages!!
                            response.confirmationErrorMessage.Append(Resources.POSErrors.ERR0050)
                        End If
                    End If
                    response.IsConfirmationValidationPassed = isQualified
                    If Not isQualified Then
                        Dim unConfirmed() As DataRow = request.confirmationCodes.Select("IS_CONFIRMED ='N'", "SEQ# Asc")
                        If unConfirmed.Any() Then
                            response.confCode = unConfirmed(0)("CODE")
                        End If
                    End If
                End If
            End If
        End If
        Return response
    End Function

    ''' <summary>
    ''' Performs the required validation required for an ARS store.
    ''' Validates the inventory for all the lines created in the session passed-in. Checks things such as 
    ''' the delv/pickup used, if based on current inventory, lines needs to be linked to a PO, or reserved, etc. 
    ''' The lines are updated accordingly and in some cases an error message is returned.
    ''' </summary>
    ''' <param name="sessionId">the current session to retrieve the lines on the current document</param>
    ''' <param name="request">custom object with the details required for the validation</param>
    ''' <returns>custom object with the results of the validation</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Public Function ValidateARSInventory(ByVal sessionId As String,
                                         ByVal request As ArsValidationRequestDtc) As ArsValidationResponseDtc
        'get all the SO lines for the sessionId passed in
        Dim tempItmDataset As DataSet = GetSalesDac().GetTempItmData(sessionId)
        Return ValidateARSInventory(tempItmDataset, request, True)
    End Function

    ''' <summary>
    ''' Performs the required validation required for an ARS store.
    ''' Validates the inventory for all the non-voided and non-PO linked lines found in the dataset passed-in. 
    ''' Checks things such as the delv/pickup used, if based on current inventory, lines needs to be linked to 
    ''' a PO, or reserved, etc.  The dataset is updated accordingly and in some cases an error message is returned.
    ''' </summary>
    ''' <param name="lnsData">dataset with the table that has the orderlines to check</param>
    ''' <param name="request">custom object with the details required for the validation</param>
    ''' <returns>custom object with the results of the validation</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER MANAGEMENT ********************</remarks>
    Public Function ValidateARSInventory(ByVal lnsData As DataSet,
                                         ByVal request As ArsValidationRequestDtc) As ArsValidationResponseDtc
        Return ValidateARSInventory(lnsData, request, False)
    End Function

    ''' <summary>
    ''' Saves the comments passed to the SO_CMNT table and flags them as type 'S'
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="soKeys">the sales key to be used when obtaining the next sequence number</param>
    ''' <param name="comments">the comments text</param>
    ''' <param name="storeCd">the store code where this comment was generated</param>
    ''' <param name="transDt">the effective date of the transaction</param>
    Public Sub SaveSalesComments(ByRef dbAtomicCommand As OracleCommand,
                                 ByVal soKeys As OrderUtils.SalesOrderKeys,
                                 ByVal comments As String,
                                 ByVal storeCd As String,
                                 ByVal transDt As String)

        '------------------------------------------------------------------------------------------------------
        'NOTE: For Sales/Delivery comments checks on the length because it should not generate the key
        '      by calling GetWrDtStoreSeqKey(soKeys),  unless there are comments available
        '------------------------------------------------------------------------------------------------------
        If (Len(comments) > 0) Then
            GetCommentsDac().SaveSOComments(dbAtomicCommand, GetWrDtStoreSeqKey(soKeys), "S", comments, storeCd, transDt)
        End If
    End Sub

    ''' <summary>
    ''' Saves the comments passed to the SO_CMNT table and flags them as type 'D'
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="soKeys">the sales key to be used when obtaining the next sequence number</param>
    ''' <param name="comments">the comments text</param>
    ''' <param name="storeCd">the store code where this comment was generated</param>
    ''' <param name="transDt">the effective date of the transaction</param>
    Public Sub SaveDeliveryComments(ByRef dbAtomicCommand As OracleCommand,
                                    ByVal soKeys As OrderUtils.SalesOrderKeys,
                                    ByVal comments As String,
                                    ByVal storeCd As String,
                                    ByVal transDt As String)

        '------------------------------------------------------------------------------------------------------
        'NOTE: For Sales/Delivery comments checks on the length because it should not generate the key
        '      by calling GetWrDtStoreSeqKey(soKeys),  unless there are comments available
        '------------------------------------------------------------------------------------------------------
        If (Len(comments) > 0) Then
            GetCommentsDac().SaveSOComments(dbAtomicCommand, GetWrDtStoreSeqKey(soKeys), "D", comments, storeCd, transDt)
        End If
    End Sub

    ''' <summary>
    ''' Saves the AR comments passed in to the CUST_CMNT table
    ''' </summary>
    ''' <param name="dbAtomicCommand">the database command to be used</param>
    ''' <param name="empCd">the employee code keying the sale</param>
    ''' <param name="custCd">the customer code in the sale</param>
    ''' <param name="comments">the AR comments</param>
    Public Sub SaveARComments(ByRef dbAtomicCommand As OracleCommand,
                              ByVal empCd As String,
                              ByVal custCd As String,
                              ByVal comments As String)
        GetCommentsDac().SaveARComments(dbAtomicCommand, empCd, custCd, comments)
    End Sub

    ''' <summary>
    ''' Saves a comment that contains a Finance message to the SO_CMNT table
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="soKeys">sales order keys to obtain the next sequence</param>
    ''' <param name="delDocNum">document number for the comments being inserted</param>
    ''' <param name="promoCd">the promotion code for the finance payment</param>
    ''' <param name="storeCd">the store code where the order was keyed</param>
    ''' <param name="transDt">the date when the transaction occurred</param>
    Public Sub SaveSOFinanceComments(ByRef dbAtomicCommand As OracleCommand,
                                     ByVal soKeys As OrderUtils.SalesOrderKeys,
                                     ByVal delDocNum As String,
                                     ByVal promoCd As String,
                                     ByVal storeCd As String,
                                     ByVal transDt As String)
        GetCommentsDac().SaveSOFinanceComments(dbAtomicCommand, soKeys, delDocNum, promoCd, storeCd, transDt)
    End Sub
    'Alice added on May 16, 2019 for request 81
    Public Sub SaveInvalidInvoiceComments(ByRef dbAtomicCommand As OracleCommand,
                                     ByVal soKeys As OrderUtils.SalesOrderKeys,
                                     ByVal delDocNum As String,
                                     ByVal InvalidText As String,
                                     ByVal storeCd As String,
                                     ByVal transDt As String)
        GetCommentsDac().SaveInvalidInvoiceComments(dbAtomicCommand, soKeys, delDocNum, InvalidText, storeCd, transDt)
    End Sub

    ''' <summary>
    '''  Saves the marketing codes and categories, that were used for the order
    ''' </summary>
    ''' <param name="dbAtomicCommand">command that is part of a single transaction</param>
    ''' <param name="writtenDt">the SO written date </param>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="soSeqNum">the SO sequence number</param>
    ''' <param name="mktCat">the marketing category</param>
    ''' <param name="mktSubCat">the marketing subcategory</param>
    Public Sub SaveSOMarketingInfo(ByRef dbAtomicCommand As OracleCommand,
                                   ByVal writtenDt As String,
                                   ByVal storeCd As String,
                                   ByVal soSeqNum As String,
                                   ByVal mktCat As String,
                                   ByVal mktSubCat As String)

        If (mktCat.isNotEmpty) Then
            GetSalesDac().SaveSOMarketingInfo(dbAtomicCommand,
                                              writtenDt,
                                              storeCd,
                                              soSeqNum,
                                              mktCat,
                                              mktSubCat)
        End If
    End Sub

    ''' <summary>
    ''' REPLACES THE ORIGINAL SKU WITH THE NEW SKU
    ''' </summary>
    ''' <param name="delDocNum">the document being processed</param>
    ''' <param name="skuToBeReplaced">SKU being replaced</param>
    ''' <param name="newSku">the sku that will be used to replace</param>
    ''' <param name="delDocLineNum">the line number for the SKU being replaced</param>
    ''' <returns>number of records modified by this process</returns>
    Public Function RemoveComponent(
                              ByVal drNew As DataRow,
                              ByRef objSql As OracleCommand) As Integer
        Return GetSalesDac().RemoveComponent(drNew, objSql)
    End Function

    ''' <summary>
    ''' Creates a new row into the TEMP_ITM table and related tables, using the details passed in.   
    ''' This method should be called when after performing a reservation, either the item is partially
    ''' reserved or it is reserved from multiple store/locations, and therefore in order to properly
    ''' represent these reservations, new lines have to be created.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    ''' <param name="lnId">the line id to obtain the duplicate details from (aka row_id)</param>
    ''' <param name="qtyOnNew">the quantity for the new line</param>
    ''' <param name="discCd">the discount code to be applied to the line</param>
    ''' <param name="storeCd">store where this line was reserved from or empty if line is not reserved</param>
    ''' <param name="locCd">location where this line was reserved from or empty if line is not reserved</param>
    ''' <param name="resId">the reservation ID, is line is reserved</param>
    Public Sub CreateNewTempItemForInvSplit(ByVal sessionId As String,
                                            ByVal lnId As String, ByVal qtyOnNew As Double,
                                            ByVal discCd As String, ByVal storeCd As String,
                                            ByVal locCd As String, ByVal resId As String)

        ' Inserts a record in the TEMP_ITM table
        Dim newLnId As String = GetSalesDac().InsertTempItemForInvSplit(sessionId, lnId, qtyOnNew, storeCd, locCd, resId)

        ' Inserts records on the related tables
        If (Not IsNothing(SysPms.multiDiscCd) AndAlso SysPms.multiDiscCd.Equals(discCd)) Then

            Dim splitLnReq As SplitLineRequestDtc = New SplitLineRequestDtc
            splitLnReq.newDelDocNum = sessionId
            splitLnReq.origDelDocNum = sessionId ' not splitting docs so these are same
            splitLnReq.origLnNum = lnId
            splitLnReq.newLnNum = newLnId
            splitLnReq.soDiscCd = discCd
            GetDiscountDac().InsertMultiDiscRow(splitLnReq)
        End If
    End Sub

    ''' <summary>
    ''' Updates the TEMP_ITEM table by setting the values passed in in the row that matches the row id value passed in
    ''' </summary>
    ''' <param name="aRowId">the ROW_ID value, not to be confused with ORACLEs rowId</param>
    ''' <param name="qty">the quantity to be persisted</param>
    ''' <param name="storeCd">store code to be persisted</param>
    ''' <param name="locCd">location code to be persisted</param>
    ''' <param name="serialNum">serial number to be persisted</param>
    ''' <param name="outId">outlet ID to be persisted</param>
    ''' <param name="resId">reservation ID assigned to the reservation</param>
    ''' <param name="poCd">the PO cd to be persisted</param>
    ''' <param name="poLn">the PO line to create the link against</param>
    Public Sub UpdateTempItmTable(ByVal aRowId As String,
                                  ByVal qty As String,
                                  ByVal storeCd As String,
                                  ByVal locCd As String,
                                  ByVal serialNum As String,
                                  ByVal outId As String,
                                  ByVal resId As String,
                                  ByVal poCd As String,
                                  ByVal poLn As String)
        GetSalesDac().UpdateTempItmTable(aRowId, qty, storeCd, locCd, serialNum, outId, resId, poCd, poLn)
    End Sub

    ''' <summary>
    ''' Finds out if a record exists for the document passed in, in the SO_ASP table
    ''' </summary>
    ''' <param name="delDocNum">the document number to find</param>
    ''' <returns>true if document exists in the database, false otherwise</returns>
    Public Function ExistsInSO_ASP(ByVal delDocNum As String) As Boolean
        Return GetSalesDac().ExistsInSO_ASP(delDocNum)
    End Function

    ''' <summary>
    ''' Obtains the name of the invoice corresponding to the store passed in, 
    ''' as defined in the INVOICE_ADMIN table
    ''' </summary>
    ''' <param name="storeCd">the store code to find the invoice name for</param>
    ''' <returns>
    ''' the invoice name for the store; or if none assigned, the default invoice
    ''' </returns>
    Public Function GetDefaultInvoiceName(ByVal storeCd As String) As String
        Return GetSalesDac().GetDefaultInvoiceName(storeCd)
    End Function

    ''' <summary>
    ''' Obtains the name of the promo-addendum invoice corresponding to the promotion and
    ''' finance company passed in store passed in, as defined in the PROMO_ADDENDUMS table
    ''' </summary>
    ''' <param name="promoCd">a finance promotion code</param>
    ''' <param name="finCo">a finance company code</param>
    ''' <param name="wdr">true if WDR invoice should be returned; false otherwise</param>
    ''' <returns>the promo-addendum invoice name</returns>
    Public Function GetAddendumInvoiceName(ByVal promoCd As String, ByVal finCo As String, ByVal wdr As Boolean) As String
        Return GetSalesDac().GetAddendumInvoiceName(promoCd, finCo, wdr)
    End Function

    ''' <summary>
    ''' Fetches the store details, for the store code passed in.
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieved details for</param>
    ''' <returns>the store details or an empty object if no matches are found</returns>
    Public Function GetStoreInfo(ByVal storeCd As String) As StoreData

        ' TODO - we need to cache the store info so do not get repeatedly but need to work out session variable groupings first/with
        Return GetStoreDac().GetStoreInfo(storeCd)
    End Function

    ''' <summary>
    ''' Gets All the stores defined in the system.
    ''' </summary>
    ''' <returns>a list of stores available in the system</returns>
    Public Function GetStores() As DataSet
        Return GetStoreDac().GetEmpStores(String.Empty)
    End Function

    ''' <summary>
    ''' Gets the stores accessible for the employee based on the store based security enabled and access for the employee.
    ''' This list of stores should work for most, if not all, store list dropdowns
    ''' </summary>
    ''' <param name="empCd">the employee code to fetch stores for</param>
    ''' <returns>a list of stores available for the employee</returns>
    Public Function GetEmpStores(ByVal empCd As String) As DataSet
        Return GetStoreDac().GetEmpStores(empCd)
    End Function

    ''' <summary>
    ''' Retrievesand returns all the order sort codes setup in the databse.
    ''' </summary>
    ''' <returns>a dataset of all the order sort codes</returns>
    Public Function GetOrderSortCodes() As DataSet
        Return GetSalesDac().GetOrderSortCodes()
    End Function

    ''' <summary>
    ''' Queries the TEMP_ITEM table to get the serial numbers that have been added so far to the order
    ''' represented by the session ID passed in.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="sessionId">the session ID to query the serial numbers for</param>
    ''' <returns>A comma delimited list of Serial Numbers or an empty string if none found</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER ENTRY *****</remarks>
    Public Function GetSerialNumbersInUse(ByVal sessionId As String) As String

        Dim serialNumbersInUse As String = ""
        If (sessionId.isNotEmpty()) Then
            serialNumbersInUse = GetSalesDac().GetSerialNumbersInUse(sessionId)
        End If
        Return serialNumbersInUse

    End Function

    ''' <summary>
    ''' Iterates thru the lines found within the table in the dataset passed in, and gathers 
    ''' the serial number found on each of them.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="lnsData">dataset that contains a table with orderlines to gather serial #s from</param>
    ''' <returns>A comma delimited list of Serial Numbers or an empty string if none found</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER MANAGEMENT *****</remarks>
    Public Function GetSerialNumbersInUse(ByVal lnsData As DataSet) As String

        Dim serialNumbersInUse As String = ""
        Dim lnsTable As DataTable = lnsData.Tables(0)

        For Each lnRow As DataRow In lnsTable.Rows
            If (Not IsDBNull(lnRow("SERIAL_NUM")) AndAlso Not String.IsNullOrWhiteSpace(lnRow("SERIAL_NUM"))) Then
                serialNumbersInUse = serialNumbersInUse & "'" & lnRow("SERIAL_NUM") & "',"
            End If
        Next

        'removes the last comma
        If (Not String.IsNullOrWhiteSpace(serialNumbersInUse)) Then
            serialNumbersInUse = Left(serialNumbersInUse, Len(serialNumbersInUse) - 1)
        End If

        Return serialNumbersInUse

    End Function

    ''' <summary>
    ''' Queries the TEMP_ITEM table to get the outlet IDs that have been added so far to the order
    ''' represented by the session ID passed in.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="sessionId">the session ID to query the outlet IDs for</param>
    ''' <returns>A comma delimited list of Outlet IDs or an empty string if none found</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER ENTRY *****</remarks>
    Public Function GetOutletIdsInUse(ByVal sessionId As String) As String

        Dim outletIdsInUse As String = ""
        If (sessionId.isNotEmpty()) Then
            outletIdsInUse = GetSalesDac().GetOutletIDsInUse(sessionId)
        End If
        Return outletIdsInUse

    End Function

    ''' <summary>
    ''' Iterates thru the lines found within the table in the dataset passed in, and gathers 
    ''' the outlet IDs found on each of them.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="lnsData">dataset that contains a table with orderlines to gather outlet ids from</param>
    ''' <returns>A comma delimited list of Outlet Ids or an empty string if none found</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER MANAGEMENT *****</remarks>
    Public Function GetOutletIdsInUse(ByVal lnsData As DataSet) As String

        Dim outletIdsInUse As String = ""
        Dim lnsTable As DataTable = lnsData.Tables(0)

        For Each lnRow As DataRow In lnsTable.Rows
            If (Not IsDBNull(lnRow("OUT_ID")) AndAlso Not String.IsNullOrWhiteSpace(lnRow("OUT_ID")) AndAlso
                (lnRow.RowState = DataRowState.Added OrElse
                 Not ("Y".Equals(lnRow("VOID_FLAG", DataRowVersion.Original).ToString + "")))) Then

                outletIdsInUse = outletIdsInUse & "'" & lnRow("OUT_ID") & "',"
            End If
        Next

        'removes the last comma
        If (Not String.IsNullOrWhiteSpace(outletIdsInUse)) Then
            outletIdsInUse = Left(outletIdsInUse, Len(outletIdsInUse) - 1)
        End If

        Return outletIdsInUse

    End Function

    ''' <summary>
    ''' Updates the tmp_itm table based on the session id passed-in.
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <remarks>This method currently clears the avail_dt column value.</remarks>
    Public Sub ClearARSAvailDate(ByVal sessionId As String)
        GetSalesDac().ClearARSAvailDate(sessionId)
    End Sub

    ''' <summary>
    ''' Retrieves information for the package header and its components and creates temp_itm records for each. 
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    Public Function AddPkgCmpntsToTempItm(ByVal headerInfo As SoHeaderDtc,
                                          ByVal pkgSku As String,
                                          ByVal sessID As String, ByVal storeCd As String, Optional ByRef errorMessage As String = "",
                                          Optional ByVal includeARSAvailDate As Boolean = True) As DataTable

        Dim pkgCmpntDataTable As New DataTable

        If pkgSku.isNotEmpty Then
            pkgCmpntDataTable = GetSalesDac().AddPkgCmpntsToTempItm(headerInfo, pkgSku, sessID, storeCd, errorMessage, includeARSAvailDate)
        End If

        Return pkgCmpntDataTable

    End Function

    ' Commented ouf for now - needs to be revisited for invoice print vs pkg cmpnt price split
    ' ''' <summary>
    ' ''' Retrieves information for the package header and its components and Update so_ln table according to breaking out oackage price system parmeter "Upon Commit". 
    ' ''' </summary>
    ' ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ' ''' <param name="pkgSku">the package sku</param>
    ' ''' <param name="sessionID">the ID of the current session.</param>
    ' ''' <param name="DEL_DOC_NUM">newly created deldocnumber in orderstage screen on Commit time</param>
    ' ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ' ''' <returns>a data table of component SKUs (item codes)</returns>
    'Public Function UpdatePkgCmpntsToSOLNAfterInvoice(ByVal headerInfo As SoHeaderDtc,
    '                                      ByVal pkgSku As String,
    '                                      ByVal sessID As String,
    '                                      ByVal del_doc_num As String,
    '                                      Optional ByVal includeARSAvailDate As Boolean = True) As DataTable

    '    Dim pkgCmpntDataTable As New DataTable

    '    If pkgSku.isNotEmpty Then
    '        pkgCmpntDataTable = GetSalesDac().UpdatePkgCmpntsToSOLNAfterInvoice(headerInfo, pkgSku, sessID, del_doc_num, includeARSAvailDate)
    '    End If

    '    Return pkgCmpntDataTable

    'End Function
    ''' <summary>
    ''' Retrieves information for the package header and its components and update SO_LN table if the System parmeter breaking out oackage price selected option set to "Upon Comit 'C'" .
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="DEL_DOC_NUM">DelDOCNumber</param>
    ''' <param name="solnDataset">Session dataset to get latest SOM screen details</param>
    ''' ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    Public Function UpdatePkgCmpntsToSOLNonUponCommit(ByVal headerInfo As SoHeaderDtc,
                                          ByVal pkgSku As String,
                                          ByVal sessID As String,
                                           ByVal DEL_DOC_NUM As String, ByVal solnDataset As DataSet,
                                          Optional ByVal includeARSAvailDate As Boolean = True) As DataTable
        If pkgSku.isEmpty Then
            Return (New DataTable)
        Else
            Return GetSalesDac().UpdatePkgCmpntsToSOLNonUponCommit(headerInfo, pkgSku,
                                                                   sessID, DEL_DOC_NUM,
                                                                   solnDataset, includeARSAvailDate)
        End If
    End Function

    ''' <summary>
    ''' Retrieves ALL of the data in the temp-itm table
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    Public Function GetTempItmData(ByVal sessionId As String) As DataSet
        Return GetSalesDac().GetTempItmData(sessionId)
    End Function

    ''' <summary>
    ''' Retrieves  a subset of the data in the temp-itm table specifically when
    ''' a package components has been inserted previously.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    Public Function GetTempItemDataForPkgCmpnts(ByVal sessionId As String) As DataSet
        Return GetSalesDac().GetTempItemDataForPkgCmpnts(sessionId)
    End Function

    ' Daniela
    Public Function GetTempItemDataForPkgCmpntsUrl(ByVal sessionId As String) As DataSet
        Return GetSalesDac().GetTempItemDataForPkgCmpntsUrl(sessionId)
    End Function
    ''' <summary>
    ''' Checks if the order passed-in is considered 'paid in full' as per the rules specified
    ''' in the stored procedure it calls.
    ''' ======================
    ''' IMPORTANT: For an SOE document, the CUSTOM process should always be called, because the
    ''' order has not yet been saved to E1, since the non-custom relies on the data in AR_TRAN, 
    ''' for an SOE order, calling the non-custom, will always appear as non-paid in full
    ''' ======================
    ''' </summary>
    ''' <param name="req">request object for paid in full determination</param>
    ''' <param name="pifCustom">OPTIONAL value to indicate if CUSTOM processing should be used.
    ''' It defaults to FALSE, which means the System Parameter PAID_IN_FULL_CALC_CUSTOM will be
    ''' used to determine which API to call.  When passed in as TRUE, the system parameter is
    ''' ignored, and custom logic is used.
    ''' <returns>a flag indicating if the specified order is considered 'paid in full' or not.</returns>
    Public Function IsPaidInFull(ByVal req As PaidInFullReq, Optional ByVal pifCustom As Boolean = False) As Boolean
        'MM-8687
        If req.requestFromSOE AndAlso SysPmsConstants.PAID_IN_FULL_CALC_CUSTOM <> SysPms.paidInFullCalc Then
            'if the request is from SOE, since AR_TRAN records are not available, following the logic of Cash and carry module
            Return If(req.grandTotal - req.paidAmount > 0, False, True)
        ElseIf (pifCustom OrElse SysPmsConstants.PAID_IN_FULL_CALC_CUSTOM = SysPms.paidInFullCalc) Then
            Return GetSalesDac().IsPaidInFullCustom(req.delDocNum, req.ordTpCd, req.custCd)
        Else
            'this calculation relies on the AR_TRAN records, CANNOT be used for SOE processing
            Return GetSalesDac().IsPaidInFull(req)
        End If
    End Function

    ''' <summary>
    ''' Returns the max available date that there is in the temp_itm table for the current session id.
    ''' </summary>
    ''' <param name="sessionId">the current session id to use to retrieve the data</param>
    ''' <returns>the max avail date value, if any exist.</returns>
    Public Function GetMaxAvailDate(ByVal sessionId As String) As Date
        Return GetSalesDac().GetMaxAvailDate(sessionId)
    End Function

    ''' <summary>
    ''' Retrieves the items corresponding to the sessionId passed in, and gathers those that
    ''' have been dropped and allowed to sale, but that have their flag PREVENT_PO = 'Y' 
    ''' </summary>
    ''' <param name="sessionId">the session to retrieve the items for</param>
    ''' <returns>a list of Item codes that are dropped and flagged as prevent_po = 'Y'</returns>
    Public Function GetPreventedItemsForARS(ByVal sessionId As String) As List(Of String)
        'get all the SO lines for the sessionId passed in
        Dim tempItmDataset As DataSet = GetSalesDac().GetTempItmData(sessionId)
        Dim distinctItems() As DataRow = tempItmDataset.Tables(0).Select()
        Return GetSkuDac().GetPreventedItemsForARS((From row In distinctItems.AsEnumerable Select row.Field(Of String)("ITM_CD")).Distinct.ToArray)
    End Function

    ''' <summary>
    ''' Filters from the order lines table passed in, those items that have been
    ''' dropped and allowed to sale, but that have their flag PREVENT_PO = 'Y' 
    ''' </summary>
    ''' <param name="sessionId">the order lines of the order being processed</param>
    ''' <returns>a list of Item codes that are dropped and flagged as prevent_po = 'Y'</returns>
    Public Function GetPreventedItemsToBeReserved(ByVal orderLns As DataSet) As List(Of String)
        Dim distinctItems() As DataRow = orderLns.Tables(0).Select()
        Return GetSkuDac().GetPreventedItemsForARS((From row In distinctItems.AsEnumerable Select row.Field(Of String)("ITM_CD")).Distinct.ToArray)
    End Function

    ''' <summary>
    ''' Retrieves from the SALES.SO_CONF_STAT table, all the records flagged as Active
    ''' </summary>
    ''' <returns>A set of confirmation status codes; or an empty dataset if none found</returns>
    Public Function GetConfirmationCodes() As DataSet
        Return GetSalesDac().GetConfirmationCodes()
    End Function

    ''' <summary>
    ''' Retrieves from the SALES.SO_CONF_STAT table, the first Active record and 
    ''' returns its code.
    ''' </summary>
    ''' <returns>The first confirmation status code; or an empty string if none found</returns>
    Public Function GetFirstConfirmationCode() As String
        Return GetSalesDac().GetFirstConfirmationCode()
    End Function

    ''' <summary>
    ''' Reserves the one line represented by the reservation request object.   There will
    ''' be cases when a line is reserved from multiple store-location combinations, for
    ''' those cases, there will be new lines created to reflect the reservations accordingly.
    ''' </summary>
    ''' <param name="sessionId">the sessionID for the order currently being processed</param>
    ''' <param name="discCd">the header discount code (if any) being used for the current order</param>
    ''' <param name="resRequest">custom object with details for the line being reserved</param>
    ''' <returns>custom object with details of the reservation made</returns>
    ''' <remarks>****** USE THIS METHOD DURING SALES ORDER ENTRY ******</remarks>
    Public Function ReserveLine(ByVal sessionId As String,
                                ByVal discCd As String,
                                ByVal resRequest As ResRequestDtc) As ResResponseDtc

        Dim resResponse As ResResponseDtc = GetInvBiz().RequestSoftReserve(resRequest)
        If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then

            Dim newLnStoreCd As String = String.Empty
            Dim newLnLocCd As String = String.Empty
            Dim newLnResId As String = String.Empty
            Dim resLnStoreCd As String = String.Empty
            Dim resLnLocCd As String = String.Empty
            Dim resLnQty As String = String.Empty
            Dim invRow As DataRow
            Dim cnt As Integer = 0

            'The reservations will be multiple when fulfilled from multiple store-location
            'in which case, new lines are created to properly reflect the reserved values
            For Each invRow In resResponse.reservations.Rows
                If cnt = 0 Then   ' first row only 
                    'has to cache these to update the line triggering reservations
                    resLnStoreCd = UCase(invRow("STORE_CD").ToString)
                    resLnLocCd = UCase(invRow("LOC_CD").ToString)
                    resLnQty = invRow("QTY")
                Else
                    'if here means either that 
                    '1) line was split due to only a partial reservation OR
                    '2) line was split due to being reserved from multiple store-location
                    newLnStoreCd = UCase(invRow("STORE_CD").ToString)
                    newLnLocCd = UCase(invRow("LOC_CD").ToString)
                    newLnResId = If(newLnStoreCd.isNotEmpty() AndAlso
                                    newLnLocCd.isNotEmpty(), resResponse.resId, String.Empty)

                    CreateNewTempItemForInvSplit(sessionId,
                                                 resRequest.lineNum,
                                                 CDbl(invRow("QTY").ToString),
                                                 discCd,
                                                 newLnStoreCd, newLnLocCd, newLnResId)
                End If
                cnt = cnt + 1
            Next

            '-- Updates TEMP_ITEM for the line that started the reservation call
            UpdateTempItmTable(resRequest.lineNum, resLnQty, resLnStoreCd, resLnLocCd,
                               String.Empty, String.Empty, resResponse.resId,
                               String.Empty, String.Empty)
        End If

        Return resResponse
    End Function

    ''' <summary>
    ''' Reserves the one line represented by the reservation request object.   There will
    ''' be cases when a line is reserved from multiple store-location combinations, for
    ''' those cases, there will be new lines created to reflect the reservations accordingly.
    ''' </summary>
    ''' <param name="sessionId">the sessionID for the order currently being processed</param>
    ''' <param name="discCd">the header discount code (if any) being used for the current order</param>
    ''' <param name="resRequest">custom object with details for the line being reserved</param>
    ''' <returns>custom object with details of the reservation made</returns>
    ''' <remarks>****** USE THIS METHOD DURING SALES ORDER ENTRY ******</remarks>
    Public Function ReserveLine(ByVal sessionId As String,
                                ByVal discCd As String,
                                ByVal resRequest As ResRequestDtc, ByRef splitErrMsg As StringBuilder) As ResResponseDtc

        Dim resResponse As ResResponseDtc = GetInvBiz().RequestSoftReserve(resRequest)
        Dim newLnStoreCd As String = String.Empty
        Dim newLnLocCd As String = String.Empty
        Dim newLnResId As String = String.Empty
        Dim resLnStoreCd As String = String.Empty
        Dim resLnLocCd As String = String.Empty
        Dim resLnQty As String = String.Empty
        Dim invRow As DataRow
        Dim cnt As Integer = 0
        If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then
            If SessVar.isArsEnabled AndAlso resResponse.reservations.Rows.Count > 1 Then
                'Do not split the line and reserve!!
                If Not resResponse.reservations.Columns.Contains("RES_ID") Then
                    resResponse.reservations.Columns.Add("RES_ID", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("ITM_CD") Then
                    resResponse.reservations.Columns.Add("ITM_CD", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("SERIAL_NUM") Then
                    resResponse.reservations.Columns.Add("SERIAL_NUM", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("OUT_ID") Then
                    resResponse.reservations.Columns.Add("OUT_ID", GetType(String))
                End If
                resResponse.isForARSStore = True
                resResponse.isLineSplitted = True
                'remove the soft reservation
                For Each invRow In resResponse.reservations.Rows
                    If Not IsDBNull(resResponse.resId) AndAlso resResponse.resId.Trim <> String.Empty Then
                        invRow("RES_ID") = resResponse.resId
                        invRow("ITM_CD") = resRequest.itemCd
                        invRow("SERIAL_NUM") = resRequest.lineNum
                        invRow("OUT_ID") = resRequest.outId
                        UnreserveLine(invRow)
                    End If
                Next
                If splitErrMsg.Length = 0 Then
                    splitErrMsg.Append(vbCrLf)
                End If
                splitErrMsg.Append(String.Format(Resources.POSErrors.ERR0044, resRequest.itemCd))
                splitErrMsg.Append(vbCrLf)
            Else

                'The reservations will be multiple when fulfilled from multiple store-location
                'in which case, new lines are created to properly reflect the reserved values
                For Each invRow In resResponse.reservations.Rows
                    If cnt = 0 Then   ' first row only 
                        'has to cache these to update the line triggering reservations
                        resLnStoreCd = UCase(invRow("STORE_CD").ToString)
                        resLnLocCd = UCase(invRow("LOC_CD").ToString)
                        resLnQty = invRow("QTY")
                    Else
                        'if here means either that 
                        '1) line was split due to only a partial reservation OR
                        '2) line was split due to being reserved from multiple store-location
                        newLnStoreCd = UCase(invRow("STORE_CD").ToString)
                        newLnLocCd = UCase(invRow("LOC_CD").ToString)
                        newLnResId = If(newLnStoreCd.isNotEmpty() AndAlso
                                        newLnLocCd.isNotEmpty(), resResponse.resId, String.Empty)

                        CreateNewTempItemForInvSplit(sessionId,
                                                     resRequest.lineNum,
                                                     CDbl(invRow("QTY").ToString),
                                                     discCd,
                                                     newLnStoreCd, newLnLocCd, newLnResId)
                    End If
                    cnt = cnt + 1
                Next

                '-- Updates TEMP_ITEM for the line that started the reservation call
                UpdateTempItmTable(resRequest.lineNum, resLnQty, resLnStoreCd, resLnLocCd,
                                   String.Empty, String.Empty, resResponse.resId,
                                   String.Empty, String.Empty)
            End If
        End If
        Return resResponse
    End Function

    ''' <summary>
    ''' Checks if the line passed in has soft-reservations attached, and if so, removes them
    ''' Since the reservation is removed, the Store/Location are also cleared from the line
    ''' </summary>
    ''' <param name="lnDetails">a Datarow that contains the line to remove reservations when present</param>
    ''' <returns>TRUE if line was reserved and it is now unreserved; FALSE if line was NOT reserved</returns>
    Public Function UnreserveLine(ByRef lnDetails As DataRow) As Boolean

        Dim unreserveOccurred As Boolean = False
        'In SOM, there can be lines that were retrieved already reserved, these lines will NOT have
        'a RES_ID, since they are hard-reserved not soft-reserved.    Also, for OUTLET locations, 
        'currently the E1 API cannot do soft-reserve, therefore there is also no RES_ID present.
        'Due to the cases where there is no RES_ID, has to check Store/Location being present
        'in order to clear the values on the record
        Dim storeCd As String = If(Not IsDBNull(lnDetails("STORE_CD")), lnDetails("STORE_CD").ToString, String.Empty)
        Dim locCd As String = If(Not IsDBNull(lnDetails("LOC_CD")), lnDetails("LOC_CD").ToString, String.Empty)
        Dim resId As String = If(Not IsDBNull(lnDetails("RES_ID")), (lnDetails("RES_ID")).ToString, String.Empty)
        Dim isSoftRes As Boolean = If(resId.isNotEmpty(), True, False)

        'First, removes the SOFT-RES if it exists
        If (isSoftRes) Then
            Dim request As UnResRequestDtc = InventoryUtils.GetRequestForUnreserve(lnDetails("RES_ID"),
                                                                    lnDetails("ITM_CD"),
                                                                    storeCd,
                                                                    locCd,
                                                                    lnDetails("QTY"))
            GetInvBiz().RemoveSoftRes(request)
        End If

        'Now clears the reservation, so that inventory is restored
        If (isSoftRes OrElse (storeCd.isNotEmpty() AndAlso locCd.isNotEmpty())) Then
            lnDetails("RES_ID") = System.DBNull.Value
            lnDetails("STORE_CD") = System.DBNull.Value
            lnDetails("LOC_CD") = System.DBNull.Value
            lnDetails("SERIAL_NUM") = System.DBNull.Value
            lnDetails("OUT_ID") = System.DBNull.Value
            unreserveOccurred = True
        End If

        Return unreserveOccurred
    End Function

    ''' <summary>
    ''' Retrieves the list of all THE NON-EXPIRED tax codes setup in the system with
    ''' their code and descriptions.
    ''' </summary>
    ''' <returns>a dataset of all the active tax codes defined in the system.</returns>
    Public Function GetTaxCodesByDate(ByVal effectiveTaxDt As String) As DataSet
        Return GetTaxDac().GetTaxCodesByDate(effectiveTaxDt)
    End Function

    ''' <summary>
    ''' Retrieves the list of all THE NON-EXPIRED tax codes and it's details.
    ''' </summary>
    ''' <returns>a list of all the active tax codes defined in the system.</returns>
    Public Function GetTaxCodesByZip(ByVal postalCd As String)
        Return GetTaxDac().GetTaxCodesByZip(postalCd)
    End Function


    ''' <summary>
    ''' Retrieves the list of all tax exempt codes setup in the system with
    ''' their code and descriptions.
    ''' </summary>
    ''' <returns>the dataset of all the tax exempt codes defined in the system.</returns>
    Public Function GetTaxExemptCodes() As DataSet
        Return GetTaxDac().GetTaxExemptCodes()
    End Function

    ''' <summary>
    ''' Retrieves the sum of the tax percent defined for the passed-in tax code that 
    ''' is defined for a 'Delivery' charge.
    ''' </summary>
    ''' <param name="taxCd">the current tax code</param>
    ''' <returns>the sum of the tax percent associated to a delivery charge using the passed-in tax code</returns>
    Public Function GetTaxPctForDelv(ByVal taxCd As String) As Double
        Return GetTaxDac().GetTaxPctForDelv(taxCd)
    End Function

    ''' <summary>
    ''' Retrieves the sum of the tax percent defined for the passed-in tax code that 
    ''' is defined for a 'Setup' charge.
    ''' </summary>
    ''' <param name="taxCd">the current tax code</param>
    ''' <returns>the sum of the tax percent associated to a setup charge using the passed-in tax code</returns>
    Public Function GetTaxPctForSetup(ByVal taxCd As String) As Double
        Return GetTaxDac().GetTaxPctForSetup(taxCd)
    End Function

    ''' <summary>
    ''' Creates a PO Link for the document line passed in
    ''' </summary>
    ''' <param name="oraCmd">the command object to be used, usually part of a single transaction</param>
    ''' <param name="strDocTpCd">the document type being processed</param>
    ''' <param name="strDelDocNo">the document number of the document being processed</param>
    ''' <param name="strDelDocLnNo">the line number for the line being processed</param>
    ''' <param name="dtPdTransfer">the date when this is taking place</param>
    ''' <param name="intQty">the quantity of the line being processed</param>
    ''' <param name="strPOCd">the PO code where the document line is linked to</param>
    ''' <param name="strPOLn">the PO line where the document line is linked to</param>
    ''' <param name="isSOM">TRUE if processing SOM; FALSE for SOE</param>
    Public Sub AddPOLink(ByRef oraCmd As OracleCommand, ByVal strDocTpCd As String,
                         ByVal strDelDocNo As String, ByVal strDelDocLnNo As String,
                         ByVal dtPdTransfer As Date, ByVal intQty As Integer,
                         ByVal strPOCd As String, ByVal strPOLn As String,
                         Optional ByVal isSOM As Boolean = False)

        'for SOM, first it has to make sure that it's not linked, and if it is, removes the link
        If (isSOM) Then
            RemovePOLink(oraCmd, strDocTpCd, strDelDocNo, strDelDocLnNo, dtPdTransfer, intQty)
        End If

        'then adds the link
        GetSalesDac().AddPOLink(oraCmd, strDocTpCd, strDelDocNo, strDelDocLnNo, dtPdTransfer, intQty, strPOCd, strPOLn)
    End Sub

    ''' <summary>
    ''' Removes the PO Link for the document line passed in
    ''' </summary>
    ''' <param name="oraCmd">the command object to be used, usually part of a single transaction</param>
    ''' <param name="strDocTpCd">the document type being processed</param>
    ''' <param name="strDelDocNo">the document number of the document being processed</param>
    ''' <param name="strDelDocLnNo">the line number for the line being processed</param>
    ''' <param name="dtPdTransfer">the date when this is taking place</param>
    ''' <param name="intQty">the quantity of the line being processed</param>
    Public Sub RemovePOLink(ByRef oraCmd As OracleCommand, ByVal strDocTpCd As String,
                            ByVal strDelDocNo As String, ByVal strDelDocLnNo As String,
                            ByVal dtPdTransfer As Date, ByVal intQty As Integer)

        Dim objSalesDac As SalesDac = GetSalesDac()
        'retrieves the POLink (if any) for the document line passed in, and if found, removes it   
        Dim poLink As List(Of String) = objSalesDac.GetPOLink(strDelDocNo, strDelDocLnNo)

        'only if line is found linked, removes the link
        If (poLink.Count = 2) Then
            objSalesDac.RemovePOLink(oraCmd, strDocTpCd, strDelDocNo, strDelDocLnNo, dtPdTransfer, intQty, poLink.Item(0), poLink.Item(1))
        End If
    End Sub

    ''' <summary>
    ''' Retrieves the list of all active/non-expired discounts in the system.
    ''' </summary>
    ''' <returns>a dataset containing the list of all the discounts in the system.</returns> 
    Public Function GetDiscounts() As DataTable
        Return GetDiscountDac().GetDiscounts(SysPms.allowMultDisc)
    End Function

    ''' <summary>
    ''' Conditional Minor SKUs can be voided if at least one conditional minor SKU remains on the order 
    '''    or if all discounted minors are voided  Discounted minor SKUs can be voided without limitation.  
    '''    If discounted minor SKU voided for substitution reasons and substitute SKU is not discounted then 
    '''    store can do a manual manager adjustment or create an MCR.
    ''' Determines if the line passed-in can be allowed to be voided based on conditions such as whether it 
    ''' has a conditional Minor SKU, the associated discounted lines have been SHU'd, or if other conditional minor SKUs also exist
    ''' on the order.  
    ''' </summary>
    ''' <param name="soWrDt">the sales order written date to test if SHU'd</param>
    ''' <param name="lineToBeVoided">the datarow representing the line that is being voided</param>
    ''' <param name="orderLinesDatTbl">the table that contains ALL order lines in the current order</param>
    ''' <param name="ordLines2Disc">the table that contains ALL order line discounts (SO_LN2DISC) already applied to the current order</param>
    ''' <returns>flag indicating if the line passed-in can be voided or not.</returns>
    Public Function CanBeVoided(ByVal soWrDt As String, ByVal lineToBeVoided As DataRow,
                                ByVal orderLinesDatTbl As DataTable,
                                ByVal ordLines2Disc As DataTable) As Boolean

        ' TODO - there is some question as to how this shold work if line minor is both conditional and discounted - waiting for Scott's response; 
        '   meanwhile, if the line to be voided is a conditional minor on the discount, there are no other non-void lines that are conditional 
        '   minors for the discount and there is a discounted minor line that is SHU'd, then the void is not allowed; user has to void the discounted line first
        '   per scott, the both sides is really to allow $discounts on by minor discounts;  
        Dim rtnVal As Boolean = True
        Dim whereClause As String
        Dim orderLines() As DataRow
        Dim minors As DataTable 'is all minors
        Dim mnrs As DataRow()   ' is only a subset of  minors -either Conditional or regular
        Dim hasCondMinor As Boolean = False
        Dim hdrDiscCd As String = String.Empty
        Dim hdrDisc As DiscountDtc = GetHeaderDiscount(ordLines2Disc, False)
        If Not IsNothing(hdrDisc) Then
            hdrDiscCd = hdrDisc.cd
        End If

        ' determine if doc is SHUd
        Dim ShuDt As Date = SessVar.shuDt
        Dim isShud As Boolean = False
        If DateDiff("d", FormatDateTime(soWrDt, DateFormat.ShortDate), ShuDt) >= 0 Then
            isShud = True
        End If

        ' if doc is not SHU'd, then the process will take care of itself; if doc is SHU'd, then we can't allow modifications that will try to update
        '    SHU'd lines so stop from removing last conditional minor if discounted lines are SHU'd
        ' TODO - consider using byMinor and/or condMnr in hdrDisc object to avoid hitting minor table to validate is by minor and has conditional  - seems obvious but not doing now at hte 10th hour
        If isShud Then
            ' if there is a header discount on the order
            '    and the hdr discount is on the line to be voided
            If hdrDiscCd.isNotEmpty AndAlso
                HasDiscount(ordLines2Disc, lineToBeVoided("DEL_DOC_LN#"), hdrDiscCd) Then

                'first get the minors for the header discount 
                minors = GetDiscountDac().GetMinorsTbl(hdrDiscCd)

                'now check if the minors retrieved are conditional         
                If SystemUtils.dataTblIsNotEmpty(minors) Then

                    mnrs = minors.Select("condOrDisc = '" & AppConstants.Discount.MNR_TP_COND & "'")   ' check if there are any 'C'onditional minors
                    hasCondMinor = mnrs.Length > 0
                End If
            End If

            'if the order has a conditional discount 
            If hasCondMinor Then

                'determine if the line's minor exists in the conditional minors list
                Dim minorCount As Integer = 0
                Dim voidLnIsCondMnr As Boolean = False
                Dim nonVoidLines() As DataRow = orderLinesDatTbl.Select("VOID_FLAG = 'N'")

                ' loop thru minors to determine if voiding line is conditional minor and to see if there are other conditional minor entries
                For Each minor As DataRow In mnrs

                    If Not voidLnIsCondMnr Then  ' once it's true, then don't change it back - if in list, then we are done with this part but need to continue loop looking for other conditional minor lines
                        'check if the line being voided has the same minor as any of the conditional minors
                        voidLnIsCondMnr = (minor("mnr_cd").ToString = lineToBeVoided("mnr_cd").ToString)
                    End If

                    'Check if there are any lines on the order with matching conditional minor
                    For Each nonVoidLn As DataRow In nonVoidLines

                        ' if this is a matching conditional minor and discount is percent based, then include
                        ' if matching conditional but discount is amount based, then include only if
                        '    new or modified discount;  dollar discounts are not modified if the doc is SHU'd 
                        '    so new lines or existing lines if they were new after the dollar discount is SHU'd should not be considered as part of the dollar discount; 
                        '    can tell existing because they were not added to the discount when the line was added
                        If minor("mnr_cd").ToString = nonVoidLn("mnr_cd").ToString AndAlso
                            (hdrDisc.tpCd = AppConstants.Discount.TP_PCT OrElse
                             ((SessVar.existHdrDiscCd.isEmpty OrElse SessVar.existHdrDiscCd <> hdrDiscCd) OrElse
                              (nonVoidLn("NEW_LINE") = "N" AndAlso HasDiscount(ordLines2Disc, nonVoidLn("del_doc_ln#").ToString, hdrDiscCd)))) Then

                            minorCount += 1
                        End If
                    Next
                Next

                'if there was more than one order line found that has a conditional minor, return true, it is OK to remove this one
                'But if the count was only one and it is the voiding line, we have to further check to see if there are any non-voided discounted SHU'd lines 
                If minorCount = 1 AndAlso voidLnIsCondMnr Then

                    'now do the retrieval of regular discount minors to see if there are discountable lines with even one of them; we already know minor is not empty
                    mnrs = minors.Select("condOrDisc = '" & AppConstants.Discount.MNR_TP_DISC & "'")   ' check if there are any 'D' regular discount minors            

                    Dim isLnShuD As Boolean = False
                    For Each minor As DataRow In mnrs

                        'Check if there are any lines on the order with matching discount
                        For Each nonVoidLn As DataRow In nonVoidLines

                            'if line is SHU'd and have matching minor for even one line for this discount, then cannot void
                            'if not SHu'd or not this discount, allow void
                            If (minor("mnr_cd").ToString = nonVoidLn("mnr_cd").ToString) AndAlso
                                nonVoidLn("SHU").ToString() = "Y" AndAlso
                                nonVoidLn("DEL_DOC_LN#") <> lineToBeVoided("DEL_DOC_LN#") AndAlso
                                HasDiscount(ordLines2Disc, nonVoidLn("DEL_DOC_LN#"), hdrDiscCd) Then

                                isLnShuD = True
                                Exit For
                            End If
                        Next
                        'if a line that was SHU'd is found, then void cannot be allowed - simply exit
                        If isLnShuD Then

                            rtnVal = False
                            Exit For
                        End If
                    Next
                End If
            End If
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' Determines if the passed-in line has any discounts or not.
    ''' </summary>
    ''' <param name="ordLines2Disc">the table that contains ALL discounts applied to the current order</param>  
    ''' <param name="delDocLnNum">the line number of the line to be checked for</param>
    ''' <param name="discCd">if input, then look if line has this specific discount</param>
    ''' <returns>flag indicating if the line has any discounts</returns>
    Private Function HasDiscount(ByVal ordLines2Disc As DataTable, ByVal delDocLnNum As String, Optional ByVal discCd As String = "") As Boolean

        Dim rtnVal As Boolean = False
        Dim whereClause As String = ("LNSEQ_NUM = " & delDocLnNum)
        If discCd.isNotEmpty Then
            whereClause = whereClause & " AND DISC_CD = '" & discCd & "' "
        End If
        rtnVal = (ordLines2Disc.Select(whereClause).Length > 0)

        Return rtnVal
    End Function

    ''' <summary>
    ''' Retrieves all the details for the discounts that have been added so far to the order
    ''' represented by the session ID or del-doc# passed-in.
    ''' </summary>
    ''' <param name="id">either the session ID, if in order entry OR the del-doc# if in order mgmt.
    '''  The id is used to use to query the discount details</param>
    ''' <param name="discCd">the discount code found in the current session</param>
    ''' <param name="isOrderEntry">TRUE if calling when in order entry; FALSE if in Order Management</param>
    ''' <returns>a datatable with the discount details</returns>
    Public Function GetDiscountsApplied(ByVal id As String,
                                        ByVal discCd As String,
                                        ByVal isOrderEntry As Boolean,
                                        ByVal isRELN As Boolean) As DataTable

        Dim discApplied As DataTable = DiscountUtils.GetLineDiscountTable()
        Dim discounts As New DataTable

        If isOrderEntry Then
            discounts = GetDiscountDac().GetDiscountsAppliedForSOE(id)

        ElseIf discCd.isNotEmpty AndAlso Not isRELN Then
            'when in SOE, if the discount code in the E1 SysPm equals the discount passed in, then it
            'means that upon save in SOE, records where inserted in the SO_LN2DISC table, if the value
            'is different, then upon save in SOE, no records were inserted, therefore they need to be 
            'recreated based on the data found in the tables.
            discounts = If(SysPms.multiDiscCd = discCd,
                            GetDiscountDac().GetDiscountsAppliedForSOM(id),
                            GetDiscountDac().GetDiscountsAppliedForSOM(id, discCd))
        ElseIf isRELN AndAlso discCd.isNotEmpty Then
            discounts = If(SysPms.multiDiscCd = discCd,
                           GetDiscountDac().GetDiscountsAppliedForRELN(id),
                           GetDiscountDac().GetDiscountsAppliedForRELN(id, discCd))
        ElseIf isRELN Then
            discounts = GetDiscountDac().GetDiscountsAppliedForRELN(id)
        End If

        Dim seqNum As String = String.Empty

        If (discounts.Rows.Count > 0) Then
            'Column aliases in both SOE & SOM retrieval are kept equal for this process to work
            For Each dr As DataRow In discounts.Rows
                'Daniela
                If Not IsDBNull(dr("SEQ_NUM")) Then
                    seqNum = dr("SEQ_NUM")
                Else
                    seqNum = 1
                End If
                DiscountUtils.AddDiscountRow(discApplied,
                                             dr.Item("DISC_CD").ToString,
                                             dr.Item("DISC_DES").ToString,
                                             dr.Item("DISC_LEVEL").ToString(),
                                             CDbl(dr.Item("DISC_AMT")),
                                             CInt(dr.Item("DISC_SEQ_NUM")),
                                             CInt(dr.Item("DISC_PCT")),
                                             dr.Item("ITM_CD").ToString,
                                             dr.Item("ITM_DES").ToString,
                                             CInt(dr.Item("QTY")),
                                             CDbl(dr.Item("ITM_RETAIL")),
                                             CInt(dr.Item("LNSEQ_NUM")),
                                             seqNum,
                                             If(discounts.Columns.Contains("TPCD"), If(IsDBNull(dr.Item("TPCD")), String.Empty, dr.Item("TPCD")), ""),
                                             If(discounts.Columns.Contains("DISCAMT"), dr.Item("DISCAMT"), 0.0),
                                             False,
                                             True)
            Next
        End If
        Return discApplied
    End Function

    ''' <summary>
    ''' Retrieves the header discount code for the ID passed in, as follows: 
    ''' 1) For SOE, the value is fetches from the CRM.MULTI_DISC table for the sessionID given
    ''' 2) For SOM, the value is retrieved from the SO table for the delDocNum given
    ''' </summary>
    ''' <param name="sessionId">the id to retrieve the discount for</param>
    ''' <param name="isOrderEntry">TRUE if calling when in order entry; FALSE if in Order Management</param>
    ''' <returns>the header discount code found; or an empty string if no header discount exist</returns>
    Public Function GetHeaderDiscountCode(ByVal id As String, ByVal isOrderEntry As Boolean, Optional ByVal isSOM As Boolean = False) As String

        If isOrderEntry AndAlso Not isSOM Then

            If (SysPms.multiDiscCd = SessVar.discCd) Then

                'here when SysPms.allowMultiDisc = Y, therefore has to check the actual data
                Return GetDiscountDac().GetHeaderDiscountCodeForSOE(id)
            Else
                'here when SysPms.allowMultiDisc = N, therefore disc-cd is the actual discount applied
                Return SessVar.discCd
            End If

        ElseIf isSOM Then
            Return GetDiscountDac().GetSoHeaderDiscountCodeForSOM(id)
        Else
            Return GetDiscountDac().GetRlnHeaderDiscountCodeForRELN(id)
        End If
    End Function

    ''' <summary>
    ''' Retrieves the 'H' type records from the Discounts table (SO_LN2DISC) passed in.
    ''' Since there can only be ONE header per transaction, if records are
    ''' found, reads the first one and returns the discount code for it.
    ''' </summary>
    ''' <param name="ordLn2Disc">a datatable that contains the discounts on the order</param>
    ''' <param name="includeDeleted">if true, then do not check for deleted; if false, do not return a deleted discount</param> 
    ''' <returns>the header discount code found; or an empty string if no header discount exist</returns>
    Public Function GetHeaderDiscountCode(ByVal ordLn2Disc As DataTable, Optional ByVal includeDeleted As Boolean = True) As String

        Dim hdrDiscCd As String = String.Empty
        If (Not IsNothing(ordLn2Disc) AndAlso ordLn2Disc.Rows.Count > 0) Then

            Dim qryStr As New StringBuilder
            qryStr.Append("DISC_LEVEL='H'")
            If Not includeDeleted Then
                qryStr.Append(" AND disc_is_deleted = false")
            End If
            Dim drhdrDisc As DataRow() = ordLn2Disc.Select(qryStr.ToString)
            For Each dr As DataRow In drhdrDisc

                hdrDiscCd = dr("disc_cd")
                Exit For                    'no need to keep on searching only ONE is allowed
            Next
        End If

        Return hdrDiscCd
    End Function

    ''' <summary>
    ''' Retrieves the 'H' type records from the Discounts table passed in.
    ''' Since there can only be ONE header per transaction, if records are
    ''' found, reads the first one and returns the discount code for it.
    ''' </summary>
    ''' <param name="ordLn2Disc">a datatable that contains the discounts on the order</param>
    ''' <param name="includeDeleted">if true, then do not check for deleted; if false, do not return a deleted discount</param>
    ''' <returns>the header discount record if found</returns>
    Public Function GetHeaderDiscount(ByVal ordLn2Disc As DataTable, Optional ByVal includeDeleted As Boolean = True) As DiscountDtc

        Dim hdrDisc As DiscountDtc
        Dim hdrDiscCd As String = GetHeaderDiscountCode(ordLn2Disc, includeDeleted)

        If hdrDiscCd.isNotEmpty Then

            hdrDisc = GetDiscountDac.GetDiscount(hdrDiscCd) 'don't have the data to load from the row calling DiscountUtils.GetDiscountDtc(dr)
        End If
        Return hdrDisc
    End Function

    ''' <summary>
    ''' Retrieves from the database, all the 'discountable' lines matching the sessionID passed-in. 
    ''' The resulting lines are the ones that the given discount can be applied to.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to get the lines added in this session</param>
    ''' <param name="discount">the discount details object</param>
    ''' <returns>the data table populated with the eligible order lines</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Public Function GetDiscountableLines(ByVal sessionId As String, ByVal discount As DiscountDtc) As DataTable

        Dim discountableLines As DataTable = SalesUtils.GetOrderLinesTable()
        Dim orderLines As DataTable = GetDiscountDac().GetDiscountableLines(sessionId, discount)

        If (orderLines.Rows.Count > 0) Then
            For Each dr As DataRow In orderLines.Rows

                'Need to update these melow snippent with the utility method
                Dim dblQty As Double = 0
                If Not IsDBNull(dr.Item("QTY")) Then
                    Double.TryParse(dr.Item("QTY"), dblQty)
                End If

                Dim dblOrigPrc As Double = 0
                If Not IsDBNull(dr.Item("ORIG_PRC")) Then
                    Double.TryParse(dr.Item("ORIG_PRC"), dblOrigPrc)
                End If

                Dim dblUnitPrc As Double = 0
                If Not IsDBNull(dr.Item("UNIT_PRC")) Then
                    Double.TryParse(dr.Item("UNIT_PRC"), dblUnitPrc)
                End If

                SalesUtils.AddOrderLineRow(discountableLines,
                                           dr.Item("DEL_DOC_LN#").ToString,
                                           dr.Item("ITM_CD").ToString,
                                           dr.Item("DES").ToString,
                                           dblQty, dblOrigPrc, dblUnitPrc)
            Next
        End If
        Return discountableLines
    End Function

    ''' <summary>
    ''' Retrieves all the 'discountable' lines from the lines dataset passed-in. The resulting lines 
    ''' are the ones that the given discount can be applied to.
    ''' </summary>
    ''' <param name="dtLines">the dataset of order lines for the current order being processed</param>
    ''' <param name="discount">the discount details object</param>
    ''' <returns>the data table populated with the eligible order lines</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER MANAGEMENT *****</remarks>
    Public Function GetDiscountableLines(ByVal dtLines As DataTable, ByVal discount As DiscountDtc) As DataTable
        'if not using remove 
        ' empty table of structure necessary for discounting
        Dim discountableLines As DataTable = SalesUtils.GetOrderLinesTable()
        If (Not IsNothing(discount)) Then

            Dim minors As New DataTable
            'if the discount is setup by minor, then need to get all the minors for this discount
            If (discount.isByMinor) Then minors = GetDiscountDac().GetMinorsTbl(discount.cd)

            'gathers the eligible lines from the datatable passed in
            For Each dr As DataRow In dtLines.Rows
                Dim isSOM As Boolean = True
                If (DiscountUtils.IsDiscountableLine(dr, discount, minors, isSOM)) Then

                    SalesUtils.AddOrderLineRow(discountableLines,
                                               dr.Item("DEL_DOC_LN#").ToString,
                                               dr.Item("ITM_CD").ToString,
                                               If(String.IsNullOrEmpty(dr.Item("DES").ToString), dr.Item("ITM_CD").ToString, dr.Item("DES").ToString),
                                               CDbl(dr.Item("QTY")),
                                               CInt(dr.Item("ORIG_PRC")),
                                               CInt(dr.Item("UNIT_PRC")))
                End If
            Next
        End If

        Return discountableLines
    End Function
    ''' <summary>
    ''' Retrieves all the 'discountable' lines from the lines dataset passed-in. The resulting lines 
    ''' are the ones that the given discount can be applied to.
    ''' </summary>
    ''' <param name="dtLines">the dataset of order lines for the current order being processed</param>
    ''' <param name="discount">the discount details object</param>
    ''' <returns>the data table populated with the eligible order lines</returns>
    ''' <remarks>***** USE THIS METHOD DURING Relationship MANAGEMENT *****</remarks>
    Public Function GetDiscountableLines(ByVal dtLines As DataTable, ByVal discount As DiscountDtc, ByVal isSOM As Boolean) As DataTable

        ' empty table of structure necessary for discounting
        Dim discountableLines As DataTable = SalesUtils.GetOrderLinesTable()
        If (Not IsNothing(discount)) Then

            Dim minors As New DataTable
            'if the discount is setup by minor, then need to get all the minors for this discount
            If (discount.isByMinor) Then minors = GetDiscountDac().GetMinorsTbl(discount.cd)

            'gathers the eligible lines from the datatable passed in
            For Each dr As DataRow In dtLines.Rows
                'MM-9484
                If dr.RowState <> DataRowState.Deleted Then
                    If (DiscountUtils.IsDiscountableLine(dr, discount, minors, isSOM)) Then

                        SalesUtils.AddOrderLineRow(discountableLines,
                                                   If(dr.Table.Columns.Contains("DEL_DOC_LN#"), dr.Item("DEL_DOC_LN#"), dr.Item("LINE")),
                                                   dr.Item("ITM_CD").ToString,
                                                   If(String.IsNullOrEmpty(dr.Item("DES").ToString), dr.Item("ITM_CD").ToString, dr.Item("DES").ToString),
                                                   CDbl(dr.Item("QTY")),
                                                   CInt(dr.Item("ORIG_PRC")),
                                                   If(dr.Table.Columns.Contains("RET_PRC"), CInt(dr.Item("RET_PRC")), CInt(dr.Item("UNIT_PRC"))))
                    End If
                End If
            Next
        End If

        Return discountableLines
    End Function

    ''' <summary>
    ''' Applies a HEADER discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    Public Function AddHeaderDiscount(ByVal request As AddDiscountRequestDtc, ByVal isSOM As Boolean) As AddDiscountResponseDtc

        '--------------- Continues processing only IF the following ------------------------
        ' A discount object was provided with all the discount details
        ' if processing in SOE, the session ID must be provided
        ' if processing in SOM, the table that contains the order lines must be provided
        If (IsNothing(request.discount) OrElse
            ((request.isSOE AndAlso String.IsNullOrEmpty(request.sessionId)) OrElse
             (Not request.isSOE AndAlso IsNothing(request.orderLines)))) Then

            'this should never happen IF the UI makes sure proper data is provided
            'no need to use the resource file here
            Throw New Exception("INVALID DATA PROVIDED")
        Else
            If (request.isSOE) AndAlso Not isSOM Then
                Return AddDiscountForSOE(request, True)
            Else
                Return GetDiscountDac().AddHeaderDiscountForSOM(request, isSOM)
            End If
        End If
    End Function

    ''' <summary>
    ''' Applies a LINE discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    Public Function AddLineDiscount(ByVal request As AddDiscountRequestDtc, ByVal isSOM As Boolean) As AddDiscountResponseDtc

        '--------------- Continues processing only IF the following ------------------------
        ' A discount object was provided with all the discount details
        ' An ArrayList containing all the LnSeqNum for the lines being discounted
        ' if processing in SOE, the session ID must be provided
        ' if processing in SOM, the table that contains the order lines must be provided
        If (IsNothing(request.discount) OrElse
            request.lnSeqNums.Count = 0 OrElse
            ((request.isSOE AndAlso String.IsNullOrEmpty(request.sessionId)) OrElse
             (Not request.isSOE AndAlso IsNothing(request.orderLines)))) Then

            'this should never happen IF the UI makes sure proper data is provided
            'no need to use the resource file here
            Throw New Exception("INVALID DATA PROVIDED")
        Else
            If (request.isSOE) Then
                Return AddDiscountForSOE(request, False)
            Else
                Return GetDiscountDac().AddLineDiscountForSOM(request, isSOM)
            End If
        End If
    End Function

    ''' <summary>
    ''' Removes the discounts represented by the details passed in.   Every object in the list
    ''' corresponds to a discount that will be removed.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve the discounts for</param>
    ''' <param name="pkpDelFlag">For a Pickup order, P; For Delivery D</param>
    ''' <param name="lstDiscToRemove">the list of discounts to remove</param>
    ''' <returns>custom object with the details of the call to remove the discount</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER ENTRY (Line or Header Discount) *****</remarks>
    ''' MM-9484
    Public Function RemoveDiscount(ByVal sessionId As String,
                                   ByVal pkpDelFlag As String,
                                   ByVal lstDiscToRemove As List(Of RemoveDiscountDtc),
                                 Optional ByVal isSOE As Boolean = False) As RemoveDiscountResponseDtc

        Dim response As RemoveDiscountResponseDtc = New RemoveDiscountResponseDtc()
        If (lstDiscToRemove.Count > 0) Then
            GetDiscountDac().RemoveDiscount(sessionId, pkpDelFlag, lstDiscToRemove, isSOE)
        End If
        '==== Retrieves ALL discounts again, so the UI displays the remaining discounts
        Dim discApplied = GetDiscountDac().GetDiscountsAppliedForSOE(sessionId)
        response.discountsApplied = discApplied

        Return response
    End Function

    ''' <summary>
    ''' Removes the discounts represented by the details passed in.   Every object in the list
    ''' corresponds to a discount that will be removed.
    ''' </summary>
    ''' <param name="dtOrderLines">the table that contains ALL order lines in the current order</param>
    ''' <param name="dtDiscApplied">the table that contains ALL discounts applied to the current order</param>
    ''' <param name="pkpDelFlag">For a Pickup order, P; For Delivery D</param>
    ''' <param name="lstDiscToRemove">the list of discounts to remove</param>
    ''' <returns>custom object with the details of the call to remove the discount</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER MANAGEMENT (Line or Header Discount) *****</remarks>
    ''' MM-9870
    Public Function RemoveDiscount(ByVal dtOrderLines As DataTable,
                                   ByVal dtDiscApplied As DataTable,
                                   ByVal pkpDelFlag As String,
                                   ByVal lstDiscToRemove As List(Of RemoveDiscountDtc),
                                   ByVal isSOM As Boolean,
                                   Optional ByVal iSCanceled As Boolean = False) As RemoveDiscountResponseDtc

        Return GetDiscountDac().RemoveDiscount(dtOrderLines, dtDiscApplied, pkpDelFlag, lstDiscToRemove, isSOM, iSCanceled)
    End Function

    ''' <summary>
    ''' Checks if discounts have to reapplied after a line change was made
    ''' </summary>
    ''' <param name="request">a request with the details to do the check and reapply discounts</param>
    ''' <returns>TRUE if discounts were updated; FALSE otherwise</returns>
    ''' MM-9870
    Public Function CheckDiscountsForLineChangeSOE(ByVal request As ReapplyDiscountRequestDtc, Optional ByVal iSCanceled As Boolean = False, Optional ByVal seq As List(Of Integer) = Nothing) As Boolean

        Dim rtnVal As Boolean = False
        Dim sessId As String = request.sessionId
        Dim pkpDel As String = request.pkpDel
        Dim lnSeqNum As String = request.lnSeqNum
        Dim dtDiscApplied As DataTable
        Dim seqNum As Integer
        Select Case request.lnAction
            Case ReapplyDiscountRequestDtc.LnChng.Add
                'For ADD, if at least one of the lines added, matches the header discount in place, recalculates
                seqNum = GetDiscountDac().CheckDiscountsForLineAddSOE(sessId, lnSeqNum)
                If (seqNum > 0) Then
                    GetDiscountDac().ReapplyDiscountsForSOE(sessId, pkpDel, seqNum)
                    rtnVal = True
                End If

            Case ReapplyDiscountRequestDtc.LnChng.Void
                'For a VOID line scenario, the RemoveDiscount flow takes care of removal and calculations
                If iSCanceled Then
                    dtDiscApplied = GetDiscountDac().GetDiscountsToDelete(sessId, True)
                Else
                    dtDiscApplied = GetDiscountDac().GetDiscountsToDelete(sessId)
                End If
                If (dtDiscApplied.Rows.Count > 0) Then
                    Dim response As RemoveDiscountResponseDtc
                    Dim lstDiscToRemove As New List(Of RemoveDiscountDtc)
                    'MM-9870
                    If Not IsNothing(seq) AndAlso seq.Count() > 0 Then
                        For i As Integer = seq.Count - 1 To 0 Step -1
                            For j As Integer = dtDiscApplied.Rows.Count() - 1 To 0 Step -1
                                If dtDiscApplied.Rows(j)("SEQ_NUM") = seq(i) Then
                                    dtDiscApplied.Rows.Remove(dtDiscApplied.Rows(j))
                                End If
                            Next

                        Next
                    End If
                    For Each dr As DataRow In dtDiscApplied.Rows
                        lstDiscToRemove.Add(New RemoveDiscountDtc(dr("LNSEQ_NUM"), dr("DISC_CD"), dr("DISC_AMT"), dr("SEQ_NUM")))
                    Next
                    'removes the discounts and recalculates any discounts that still remain in the order
                    'MM-9484
                    response = RemoveDiscount(sessId, pkpDel, lstDiscToRemove, True)
                    rtnVal = response.message.isEmpty()
                End If

            Case ReapplyDiscountRequestDtc.LnChng.Qty
                'For a Quantity change scenario, only if the line modified has AMOUNT discounts
                'they need to be redistributed again accordingly.
                dtDiscApplied = GetDiscountDac().GetDiscountsAppliedForSOE(sessId, lnSeqNum)
                If (dtDiscApplied.Rows.Count > 0) Then
                    'when the DISC_PCT field is zero, it means it is an AMOUNT discount
                    Dim amtDiscounts As Integer = dtDiscApplied.Select("DISC_PCT = 0").Length
                    If (amtDiscounts > 0) Then
                        GetDiscountDac().ReapplyDiscountsForSOE(sessId, pkpDel)
                        rtnVal = True
                    End If
                End If

            Case ReapplyDiscountRequestDtc.LnChng.Prc
                'For a Price change scenario, as long as there are AMOUNT/PERCENT discounts, 
                'recalculation is needed to redistribute again accordingly.
                If (SysPms.reapplyDisc) Then
                    dtDiscApplied = GetDiscountDac().GetDiscountsAppliedForSOE(sessId, lnSeqNum)
                    If (dtDiscApplied.Rows.Count > 0) Then
                        GetDiscountDac().ReapplyDiscountsForSOE(sessId, pkpDel)
                        rtnVal = True
                    End If
                End If

        End Select

        Return rtnVal
    End Function

    ''' <summary>
    ''' Checks if discounts have to reapplied after a line change was made.
    ''' </summary>
    ''' <param name="request">a request with the details to do the check and reapply discounts</param>
    ''' <returns>custom object with the details passed-in after discounts were updated.</returns>
    ''' MM-9870
    Public Function CheckDiscountsForLineChangeSOM(ByVal request As ReapplyDiscountRequestDtc, ByVal isSOM As Boolean, Optional ByVal IsCanceled As Boolean = False) As ReapplyDiscountResponseDtc

        Dim sessId As String = request.sessionId
        Dim pkpDel As String = request.pkpDel
        Dim dtDiscApplied As DataTable
        Dim response As ReapplyDiscountResponseDtc = New ReapplyDiscountResponseDtc()
        response.orderLines = request.orderLines
        response.discountsApplied = request.discApplied

        'no need to go thru this if NO discounts have been added to the current order
        If (IsNothing(request.discApplied) OrElse request.discApplied.Rows.Count = 0) Then Return response
        '--------------------------------------

        Select Case request.lnAction
            Case ReapplyDiscountRequestDtc.LnChng.Add
                'For ADD, if at least one of the lines added, matches the header discount in place, recalculates
                If isSOM Then
                    If CheckDiscountsForLineAddSOM(request.lnSeqNum, request.orderLines, request.discApplied, request.soWrDt) Then
                        'recalculates all discounts in the order
                        response = GetDiscountDac().ReapplyDiscountsForSOM(request.orderLines, request.discApplied, pkpDel, isSOM)
                    End If
                Else
                    If CheckDiscountsForLineAddSOM(request.lnSeqNum, request.orderLines, request.discApplied, request.soWrDt, isSOM) Then
                        'recalculates all discounts in the order
                        response = GetDiscountDac().ReapplyDiscountsForSOM(request.orderLines, request.discApplied, pkpDel, isSOM)
                    End If
                End If


            Case ReapplyDiscountRequestDtc.LnChng.Void
                'For a VOID line scenario, the RemoveDiscount flow takes care of removal and calculations
                If isSOM Then
                    'MM-9870
                    If IsCanceled Then
                        dtDiscApplied = GetDiscountsToDelete(request.orderLines, request.discApplied, isSOM, IsCanceled)
                    Else
                        dtDiscApplied = GetDiscountsToDelete(request.orderLines, request.discApplied, isSOM)
                    End If
                Else
                    'MM-9870
                    If IsCanceled Then
                        dtDiscApplied = GetDiscountsToDeleteFromRELN(request.orderLines, request.discApplied, True)
                    Else
                        dtDiscApplied = GetDiscountsToDeleteFromRELN(request.orderLines, request.discApplied)
                    End If
                End If

                If (dtDiscApplied.Rows.Count > 0) Then
                    Dim removeResponse As RemoveDiscountResponseDtc
                    Dim lstDiscToRemove As New List(Of RemoveDiscountDtc)
                    For Each dr As DataRow In dtDiscApplied.Rows
                        lstDiscToRemove.Add(New RemoveDiscountDtc(dr("LNSEQ_NUM"), dr("DISC_CD"), dr("DISC_AMT"), dr("SEQ_NUM")))
                    Next
                    'removes the discounts and recalculates any discounts that still remain in the order
                    'MM-9870
                    If IsCanceled Then
                        removeResponse = RemoveDiscount(request.orderLines, request.discApplied, request.pkpDel, lstDiscToRemove, isSOM, IsCanceled)
                    Else
                        removeResponse = RemoveDiscount(request.orderLines, request.discApplied, request.pkpDel, lstDiscToRemove, isSOM)
                    End If
                    response.orderLines = removeResponse.orderLines
                    response.discountsApplied = removeResponse.discountsApplied
                    ' if the void of the line eliminates the queried header discount, then remove the sess variable 
                    If SessVar.existHdrDiscCd.isNotEmpty AndAlso GetHeaderDiscountCode(response.discountsApplied) <> SessVar.existHdrDiscCd Then
                        SessVar.Remove(SessVar.existHdrDiscCdVarNm)
                    End If
                End If

            Case ReapplyDiscountRequestDtc.LnChng.Qty
                'For a Quantity change scenario, only if the line modified has AMOUNT discounts
                'they need to be redistributed again accordingly.

                dtDiscApplied = request.discApplied
                If (dtDiscApplied.Rows.Count > 0) Then
                    'when the DISC_PCT field is zero, it means it is an AMOUNT discount
                    Dim amtDiscounts As Integer = dtDiscApplied.Select("DISC_PCT = 0").Length
                    If (amtDiscounts > 0) Then
                        'recalculates all discounts in the order
                        response = GetDiscountDac().ReapplyDiscountsForSOM(request.orderLines, dtDiscApplied, pkpDel, isSOM)
                    End If
                End If

            Case ReapplyDiscountRequestDtc.LnChng.Prc
                'For a Price change scenario, as long as there are AMOUNT/PERCENT discounts, 
                'recalculation is needed to redistribute again accordingly.
                dtDiscApplied = request.discApplied

                If (SysPms.reapplyDisc AndAlso dtDiscApplied.Rows.Count > 0) Then
                    'recalculates all discounts in the order
                    response = GetDiscountDac().ReapplyDiscountsForSOM(request.orderLines, dtDiscApplied, pkpDel, isSOM)
                End If
        End Select

        Return response
    End Function

    ''' <summary>
    ''' Finds out if new discount records have to be added due to the last new lines added to the order.
    ''' If new discount records have to be added, then adds them and notifies the caller with a TRUE.
    ''' The new lines are considered to be the ones inserted after the LnSeqNum passed in.
    ''' These discounts get added here IF applicable.  This method runs after an ADD line
    ''' event triggered in the UI and ONLY IF a HEADER discount (PCT or AMT) exists in the order
    ''' </summary>
    ''' <param name="dtOrderLines">table of order lines</param>
    ''' <param name="dtDiscApplied">table of discountable order lines to which here will be added new lines if applicable 
    ''' so they are included for re-calc</param>
    ''' <param name="lnSeqNum">the last line in the order before the last set of ADD lines occurred; a set
    ''' can be one line or a line and it's treatment and warranty lines</param>
    ''' <param name="soWrDt">the sales order written date for comparison to SHU date; SHU'd lines cannot have changes to qty or price</param>
    ''' <returns>TRUE if discounts were added; FALSE if not</returns>
    Private Function CheckDiscountsForLineAddSOM(ByVal lnSeqNum As String, ByVal dtOrderLines As DataTable,
                                         ByRef dtDiscApplied As DataTable, ByVal soWrDt As Date) As Boolean

        Dim discableLnsAdded As Boolean = False  ' there is at least one discountable line added so discount must be re-applied
        Dim discCd, discDes, discTp, discLevel As String
        Dim discAmt As Double

        Dim discount As DiscountDtc = GetHeaderDiscount(dtDiscApplied, False)
        If (Not IsNothing(discount)) AndAlso discount.cd.isNotEmpty() Then

            ' === Finds out if discounts records are to be added and adds them if so adds discounted line list
            '    if discount is expired (exp dt is last day able to use), then it does not apply to new line so do not need to re-calc for line add
            '    if existing $ discount and document is SHU'd (shu'd if dt on same dt as SHU) and the discount is applied to any SHU'd lines, then do not re-calc; 
            '        this assumes discount is completely applied and cannot change prices on SHU'd lines
            Dim expdt As DateTime
            Dim shuDt As Date = SessVar.shuDt ' set local to avoid re-hit for every ref here
            Dim numDays As Integer = DateDiff("d", FormatDateTime(shuDt, DateFormat.ShortDate), soWrDt)  'soWrDt minus shuDt
            Dim docIsNotShuD As Boolean = numDays > 0

            If (discount.expDt.isEmpty OrElse Date.TryParse(discount.expDt, expdt)) AndAlso
                (docIsNotShuD OrElse discount.tpCd <> AppConstants.Discount.TP_CD_DOLLAR OrElse discount.cd <> SessVar.existHdrDiscCd OrElse
                  Not IsDiscShud(discount.cd, dtDiscApplied, dtOrderLines, True)) Then   ' if dollar Discount and doc is SHU'd and disc was on queried order and has SHU'd lines

                numDays = DateDiff("d", FormatDateTime(Today.Date, DateFormat.ShortDate), expdt)    ' expDt minus today
                '  if not expired     then process for add line;   if exp dt >= today's date then process
                If numDays >= 0 Then

                    Dim lastLnSeqBeforeAdd = CInt(lnSeqNum)
                    Dim discountableLines As DataTable = GetDiscountableLines(dtOrderLines, discount)
                    Dim hdrDiscSeqNum As Integer = GetHeaderDiscountSeqNum(dtDiscApplied, False)
                    '== uses a single transaction, so that all discounts get added or none if an error
                    For Each dr As DataRow In discountableLines.Rows

                        'only those lines that were added after the one passed in, are checked
                        ' If(IsNumeric(dr.Item("DISCAMT")), CDbl(dr.Item("DISCAMT")), 0.0)
                        If (CInt(dr("LN_SEQNUM")) > lastLnSeqBeforeAdd) Then
                            DiscountUtils.AddDiscountRow(dtDiscApplied, discount.cd, discount.desc, AppConstants.Discount.LEVEL_HEADER, discount.amt, hdrDiscSeqNum,
                                                    If(AppConstants.Discount.TP_PCT = discount.tpCd, discount.amt, 0),
                                                    dr.Item("ITM_CD").ToString, dr.Item("DES").ToString, CInt(dr.Item("QTY")),
                                                    CDbl(dr.Item("ORIG_PRC")), CInt(dr.Item("LN_SEQNUM")), hdrDiscSeqNum, False, True)
                            discableLnsAdded = True
                        End If
                    Next
                End If
            End If
        End If

        Return discableLnsAdded
    End Function

    ''' <summary>
    ''' Finds out if new discount records have to be added due to the last new lines added to the order.
    ''' If new discount records have to be added, then adds them and notifies the caller with a TRUE.
    ''' The new lines are considered to be the ones inserted after the LnSeqNum passed in.
    ''' These discounts get added here IF applicable.  This method runs after an ADD line
    ''' event triggered in the UI and ONLY IF a HEADER discount (PCT or AMT) exists in the order
    ''' </summary>
    ''' <param name="dtOrderLines">table of order lines</param>
    ''' <param name="dtDiscApplied">table of discountable order lines to which here will be added new lines if applicable 
    ''' so they are included for re-calc</param>
    ''' <param name="lnSeqNum">the last line in the order before the last set of ADD lines occurred; a set
    ''' can be one line or a line and it's treatment and warranty lines</param>
    ''' <param name="soWrDt">the sales order written date for comparison to SHU date; SHU'd lines cannot have changes to qty or price</param>
    ''' <param name="isSOM">adding line to Relationship line</param>
    ''' <returns>TRUE if discounts were added; FALSE if not</returns>
    Private Function CheckDiscountsForLineAddSOM(ByVal lnSeqNum As String, ByVal dtOrderLines As DataTable,
                                         ByRef dtDiscApplied As DataTable, ByVal soWrDt As Date, ByVal isSOM As Boolean) As Boolean

        Dim discableLnsAdded As Boolean = False  ' there is at least one discountable line added so discount must be re-applied
        Dim discCd, discDes, discTp, discLevel As String
        Dim discAmt As Double

        Dim discount As DiscountDtc = GetHeaderDiscount(dtDiscApplied, False)
        If (Not IsNothing(discount)) AndAlso discount.cd.isNotEmpty() Then

            ' === Finds out if discounts records are to be added and adds them if so adds discounted line list
            '    if discount is expired (exp dt is last day able to use), then it does not apply to new line so do not need to re-calc for line add
            Dim expdt As DateTime
            Dim numDays As Integer


            If (discount.expDt.isEmpty OrElse Date.TryParse(discount.expDt, expdt)) AndAlso
               (discount.tpCd <> AppConstants.Discount.TP_CD_DOLLAR OrElse discount.cd <> SessVar.existHdrDiscCd OrElse
                Not IsDiscShud(discount.cd, dtDiscApplied, dtOrderLines, False)) Then

                numDays = DateDiff("d", FormatDateTime(Today.Date, DateFormat.ShortDate), expdt)    ' expDt minus today
                '  if not expired     then process for add line;   if exp dt >= today's date then process
                If numDays >= 0 Then

                    Dim lastLnSeqBeforeAdd = CInt(lnSeqNum)
                    Dim discountableLines As DataTable = GetDiscountableLines(dtOrderLines, discount, isSOM)
                    Dim hdrDiscSeqNum As Integer = GetHeaderDiscountSeqNum(dtDiscApplied, False)
                    '== uses a single transaction, so that all discounts get added or none if an error
                    For Each dr As DataRow In discountableLines.Rows

                        'only those lines that were added after the one passed in, are checked
                        ' If(IsNumeric(dr.Item("DISCAMT")), CDbl(dr.Item("DISCAMT")), 0.0)
                        If (CInt(dr("LN_SEQNUM")) > lastLnSeqBeforeAdd) Then
                            DiscountUtils.AddDiscountRow(dtDiscApplied, discount.cd, discount.desc, AppConstants.Discount.LEVEL_HEADER, discount.amt, hdrDiscSeqNum,
                                                    If(AppConstants.Discount.TP_PCT = discount.tpCd, discount.amt, 0),
                                                    dr.Item("ITM_CD").ToString, dr.Item("DES").ToString, CInt(dr.Item("QTY")),
                                                    CDbl(dr.Item("ORIG_PRC")), CInt(dr.Item("LN_SEQNUM")), hdrDiscSeqNum, False, True)
                            discableLnsAdded = True
                        End If
                    Next
                End If
            End If
        End If

        Return discableLnsAdded
    End Function

    ''' <summary>
    ''' detemines if any of the order lines that have the input discount code have been SHU'd
    ''' </summary>
    ''' <param name="discCd">discount code to determine if on SHU'd line</param>
    ''' <param name="ordLn2Disc">a datatable that contains the discounted lines by discount code (SO_LN2DISC) on the order</param>
    ''' <param name="ordLns">a datatable of order lines</param> 
    ''' <returns>true if a line with the input discount code is SHU'd; false otherwise</returns>
    Private Function IsDiscShud(ByVal discCd As String, ByVal ordLn2Disc As DataTable, ByVal ordLnsDatTbl As DataTable, ByVal isSOM As Boolean) As Boolean

        Dim discShud As Boolean = False
        If (SystemUtils.dataTblIsNotEmpty(ordLn2Disc) AndAlso SystemUtils.dataTblIsNotEmpty(ordLnsDatTbl)) Then

            Dim prevLnNum As Integer = 0
            Dim qryStr As New StringBuilder
            qryStr.Append("disc_cd ='" & discCd & "' AND disc_is_deleted = false")
            Dim discDr As DataRow() = ordLn2Disc.Select(qryStr.ToString, "LNSEQ_NUM")
            Dim ordLns As DataRow()
            For Each discLn As DataRow In discDr
                If isSOM Then
                    ' since can be multiple discounts per line, if we've evaluated a line, then do not bother to re-acquire the SHU flag for the line
                    If prevLnNum <> discLn("LNSEQ_NUM") Then
                        qryStr.Clear()
                        qryStr.Append("void_flag <> 'Y' AND shu = 'Y' AND [del_doc_ln#] = '" & discLn("LNSEQ_NUM") & "'")
                        ordLns = ordLnsDatTbl.Select(qryStr.ToString, "DEL_DOC_LN#")
                        prevLnNum = discLn("LNSEQ_NUM")
                    End If
                    If ordLns.Length > 0 Then
                        discShud = True
                        Exit For                    'no need to keep on searching, if one line with this discount is SHU'd, then 
                    End If
                Else
                    ' since can be multiple discounts per line, if we've evaluated a line, then do not bother to re-acquire the SHU flag for the line
                    If prevLnNum <> discLn("LNSEQ_NUM") Then
                        qryStr.Clear()
                        qryStr.Append("[LINE] = '" & discLn("LNSEQ_NUM") & "'")
                        ordLns = ordLnsDatTbl.Select(qryStr.ToString, "LINE")
                        prevLnNum = discLn("LNSEQ_NUM")
                    End If
                    If ordLns.Length > 0 Then
                        discShud = False
                        Exit For                    'no need to keep on searching, if one line with this discount is SHU'd, then 
                    End If
                End If
                ' since can be multiple discounts per line, if we've evaluated a line, then do not bother to re-acquire the SHU flag for the line
                'If prevLnNum <> discLn("LNSEQ_NUM") Then

                '    qryStr.Clear()
                '    qryStr.Append("void_flag <> 'Y' AND shu = 'Y' AND [del_doc_ln#] = '" & discLn("LNSEQ_NUM") & "'")
                '    ordLns = ordLnsDatTbl.Select(qryStr.ToString, "DEL_DOC_LN#")
                '    prevLnNum = discLn("LNSEQ_NUM")
                'End If

            Next
        End If

        Return discShud
    End Function

    ''' <summary>
    ''' Fetches from TEMP_ITM, the last ROW_ID value that indicates the last line that
    ''' was added for the order represented by the sessionID passed in
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve the discounts for</param>
    ''' <returns>the ROW_ID for the last line on a given order</returns>
    Public Function GetLastLnSeqNum(ByVal sessionId As String) As String
        Return GetSalesDac().GetLastLnSeqNum(sessionId)
    End Function

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM and all related tables that match the sessionID passed in.
    ''' This operation clears the 'temp' order from the system, including payments, warranties, discounts, etc.
    ''' </summary>
    ''' <param name="dbCommand">the database command object to queue the delete operations on</param>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    Public Function DeleteTempOrder(ByRef dbCommand As OracleCommand, ByVal sessionId As String) As Boolean
        Return GetSalesDac().DeleteTempOrder(dbCommand, sessionId)
    End Function

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM and all related tables that match the sessionID passed in.
    ''' This operation clears the 'temp' order from the system, including payments, warranties, discounts, etc.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    Public Function DeleteTempOrder(ByVal sessionId As String) As Boolean
        Return GetSalesDac().DeleteTempOrder(sessionId)
    End Function

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM that match the sessionID passed in.
    ''' This operation clears only the lines for a temp order.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    Public Sub DeleteTempItems(ByVal sessionId As String)
        GetSalesDac().DeleteTempItems(sessionId)
    End Sub

    ''' <summary>
    ''' Extracts and returns the order line details from the temp_itm table along with Sort codes as a 
    ''' comma-separated string. This is for ORDER ENTRY when creating a regular, non-rel'ship transaction.  
    ''' </summary>
    ''' <param name="sessionID">the current session id/param>
    ''' <param name="SortDescriptionAlso">if true, returns sort code descriptions along with the sort codes
    '''                                    Else returns only the sort codes</param>
    ''' <param name="orderTypes">indicates whether the order is of type 'SAL','CRM', 'MCR', 'MDB'</param>
    ''' <returns>Dataset of the order line info from the temp-itm table</returns>
    Public Function GetLineItemsForSOE(ByVal sessionID As String, ByVal SortDescriptionAlso As Boolean, ByVal orderTypeCd As String)

        Dim i, j As Integer
        Dim linesDataSet As DataSet = GetSalesDac().GetLineItemsForSOE(sessionID, SortDescriptionAlso, orderTypeCd)

        For i = 0 To linesDataSet.Tables(0).Rows.Count
            For j = i + 1 To linesDataSet.Tables(0).Rows.Count - 1
                If (linesDataSet.Tables(0).Rows(i).Item("row_id").ToString = linesDataSet.Tables(0).Rows(j).Item("PACKAGE_PARENT").ToString) Then
                    linesDataSet.Tables(0).Rows(j).Item("TYPECODE") = "CMP"
                    linesDataSet.Tables(0).Rows(j).Item("PREVENT") = linesDataSet.Tables(0).Rows(i).Item("PREVENT").ToString
                End If
            Next j
        Next i

        Return linesDataSet
    End Function

    ''' <summary>
    ''' Add the relationship price column to the dataset
    ''' </summary>
    ''' <param name="linesDataset"></param>
    ''' <param name="RelanPriceChange"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AddRelationshipPrice(ByRef linesDataset As DataSet, ByRef RelanPriceChange As List(Of RelationshipLineDetails)) As String
        Dim priceModifideSku As New StringBuilder

        If Not IsNothing(RelanPriceChange) AndAlso RelanPriceChange.Count > 0 Then
            For Each line As DataRow In linesDataset.Tables(0).Rows
                Dim relanLine As RelationshipLineDetails = RelanPriceChange.Find(Function(c) c.RowId = Convert.ToInt64(line.Item("row_id")))
                If Not IsNothing(relanLine) Then
                    line.Item("RelationshipPrice") = FormatCurrency(relanLine.Price, 2)
                    priceModifideSku.Append(relanLine.ItemCD)
                    priceModifideSku.Append(",")
                End If
            Next
        End If

        If priceModifideSku.Length > 0 Then
            Return priceModifideSku.ToString.TrimEnd(",")
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts and returns the order line details from the line and related tables along 
    ''' with Sort codes as a comma-separated string.
    ''' </summary>
    ''' <param name="DelDocNumber">the del Doc#/Order Id</param>
    ''' <returns>Dataset of the order line info from the various tables</returns>
    Public Function GetLineItemsForSOM(ByVal DelDocNumber As String)

        Dim i, j As Integer
        Dim linesDataSet As DataSet = GetSalesDac().GetLineItemsForSOM(DelDocNumber)

        For i = 0 To linesDataSet.Tables(0).Rows.Count
            For j = i + 1 To linesDataSet.Tables(0).Rows.Count - 1
                If (linesDataSet.Tables(0).Rows(i).Item("ITM_CD").ToString = linesDataSet.Tables(0).Rows(j).Item("PKG_SOURCE").ToString) Then
                    linesDataSet.Tables(0).Rows(j).Item("TYPECODE") = "CMP"
                End If
            Next j
        Next i

        Return linesDataSet
    End Function

    Public Sub splitSoLnPkgCmpntPrcs(ByVal hdrInf As SoHeaderDtc)
        theSalesDac.SplitSoLnPkgCmpntPrcs(hdrInf)
    End Sub

    ''' <summary>
    ''' Extracts and returns the realtionship line details from RELATIONSHIP_LINES table along with Sort codes
    ''' comma-separated string. This is for RELATIONSHIP MAINTENANCE
    ''' </summary>
    ''' <param name="RelationID">the Relationship ID</param>
    ''' <param name="SortDescriptionAlso">if true, returns sort code descriptions along with the sort codes
    '''                                    Else returns only the sort codes</param>
    ''' <returns>Dataset of relationship line info</returns>
    ''' <remarks>Currently used for RELATIONSHIP MAINTENANCE only.</remarks>
    Public Function GetRelationLineItems(ByVal RelationID As String, ByVal SortDescriptionAlso As Boolean)

        Dim ds As DataSet = New DataSet()
        Dim salesDAC As SalesDac = New SalesDac()
        ds = salesDAC.GetRelationLineItems(RelationID, SortDescriptionAlso)
        For Each row In ds.Tables(0).Rows
            row("SortCodes") = StringUtils.EliminateCommas(row("SortCodes"))
            row("sortDescription") = StringUtils.EliminateCommas(row("sortDescription"))
        Next

        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  items details from SO for exchange
    ''' </summary>
    ''' <param name="docNum">Del Doc Number</param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetOriginalItemsForExchange(ByVal docNum As String)

        Dim ds As New DataSet
        Dim salesDAC As SalesDac = New SalesDac()
        ds = salesDAC.GetOriginalItemsForExchange(docNum)

        Return ds
    End Function

    'MM-5672
    ''' <summary>
    ''' To get store and location from INV_XREF table 
    ''' </summary>
    ''' <param name="itemCode">Item Code</param>
    ''' <param name="delDocNumber">Del Doc Number</param>
    ''' <param name="delDocLineNumber">Del Doc Line number</param>
    ''' <param name="store">Store</param>
    ''' <param name="location">Location code</param>
    ''' <remarks></remarks>
    Public Sub GetStoreLocation(ByVal itemCode As String,
                                ByVal delDocNumber As String, ByVal delDocLineNumber As Integer,
                                ByRef store As String, ByRef location As String)
        GetStoreDac().GetStoreLocation(itemCode, delDocNumber, delDocLineNumber, store, location)
    End Sub
    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Calculates the Discounts for the lines passed in and updates the TEMP_ITM and MULTI_DISC
    ''' tables accordingly.
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <param name="isHeader">Header/Line</param>
    ''' <returns>custom object with the results of calling the Add discounts process</returns>
    ''' <remarks>******* USED TO ADD HEADER AND LINE DISCOUNTS DURING ORDER ENTRY *******</remarks>
    Private Function AddDiscountForSOE(ByVal request As AddDiscountRequestDtc,
                                       ByVal isHeader As Boolean) As AddDiscountResponseDtc

        Dim response As AddDiscountResponseDtc = If(isHeader,
                                                    GetDiscountDac().AddHeaderDiscountForSOE(request),
                                                    GetDiscountDac().AddLineDiscountForSOE(request))

        'if the discounts applied were to be retrieved directly in the DiscountsDac, the common 
        'Table structure required would not be in place, therefore this value gets set here.
        '==== Retrieves ALL discounts again, so the UI displays the newly added ones
        response.discountsApplied = GetDiscountsApplied(request.sessionId, String.Empty, True, False)

        Return response
    End Function

    ''' <summary>
    ''' Performs the required validation required for an ARS store.
    ''' Validates the inventory for all the non-voided and non-PO linked lines found in the dataset passed-in. 
    ''' Checks things such as the delv/pickup used, if based on current inventory, lines needs to be linked to 
    ''' a PO, or reserved, etc.  The dataset is updated accordingly and in some cases an error message is returned.
    ''' </summary>
    ''' <param name="lnsData">dataset with the table that has the orderlines to check</param>
    ''' <param name="request">custom object with the details required for the validation</param>
    ''' <param name="isOrderEntry">TRUE if doing order entry processing; FALSE otherwise</param>
    ''' <returns>custom object with the results of the validation</returns>
    Private Function ValidateARSInventory(ByVal lnsData As DataSet,
                                          ByVal arsRequest As ArsValidationRequestDtc,
                                          ByVal isOrderEntry As Boolean) As ArsValidationResponseDtc

        Dim arsResponse As New ArsValidationResponseDtc
        'the rules to apply follow:
        '1) Is the Delivery / Pickup date greater than the latest available date ?
        '2) paid in full  (PO Linked lines are left alone when applying this rule)
        '   2.1 Yes,  Call to E1 ARS API for non PO linked lines
        '   2.2 No,   NO Call to E1 ARS API, only un-reserve stock and leave po links
        '             The exception for the unreserve is OUTLET and DROPPED items are left alone

        'when the order being processed HAS dropped items, it needs to be calling the E1 API for proper processing
        If (arsRequest.maxAvailDate <> DateTime.MinValue AndAlso
            arsRequest.maxAvailDate > arsRequest.checkInvReq.puDelDt AndAlso
            arsRequest.droppedItemsFlaggedPreventPO.Count = 0) Then
            'Scheduled pkp/del dt cannot be earlier than maxAvailDt. Please choose a different date.
            'when NON dropped items exist, the date needs to be changed at this point
            arsResponse.message = Resources.POSErrors.ERR0006
        Else
            Dim chkInvResponse As CheckInvResponseDtc
            Dim chkInvRequest As CheckInvRequestDtc = arsRequest.checkInvReq
            Dim isPaidInFull As Boolean = arsRequest.checkInvReq.isPaidInFull
            Dim isValidationPassed As Boolean = True
            Dim validationMessage As String = If(Not isPaidInFull, Resources.POSErrors.ERR0005, String.Empty)
            Dim validationMessageInserts As New ArrayList()
            Dim isInvTpLn, isNonPOLinkedLn, isNonVoidedLn, isDroppedItem, isOutLoc As Boolean
            Dim isLnUpdated As Boolean
            Dim invStsCd As Integer
            Dim storeCd As String
            Dim locCd As String
            Dim unReserved As Boolean = False
            For Each soLn As DataRow In lnsData.Tables(0).Rows
                unReserved = False
                'outlet locations should NOT be unreserved during this process
                isOutLoc = If(Not IsDBNull(soLn("OUT_ID")) AndAlso soLn("OUT_ID").ToString.isNotEmpty, True, False)
                If (isOutLoc) Then Continue For

                'when in SOE, voided lines are NOT saved, hence the TRUE below
                isNonVoidedLn = If(isOrderEntry, True, ("N" = soLn("VOID_FLAG")))
                isNonPOLinkedLn = IsDBNull(soLn("PO_CD"))
                isInvTpLn = AppConstants.Sku.TP_INV = soLn("ITM_TP_CD")

                ' --- Process only those lines that are Non Voided, Non PO-Linked and Inventoriable
                If (isInvTpLn AndAlso isNonPOLinkedLn AndAlso isNonVoidedLn) Then

                    isLnUpdated = False  'needed for SOE, to know if the TEMP_ITM table needs updating
                    isDroppedItem = arsRequest.droppedItemsFlaggedPreventPO.Contains((soLn("ITM_CD")).ToString)
                    storeCd = If(IsDBNull(soLn("STORE_CD")), "", soLn("STORE_CD"))
                    locCd = If(IsDBNull(soLn("LOC_CD")), "", soLn("LOC_CD"))

                    'when the order is paid in full OR item is dropped, it has to call the API, where validation happens
                    'If (isPaidInFull OrElse isDroppedItem) Then
                    chkInvRequest.itemCd = soLn("ITM_CD")
                    chkInvRequest.qty = soLn("QTY")
                    chkInvRequest.storeCd = storeCd
                    chkInvRequest.locCd = locCd
                    chkInvResponse = GetInvBiz().CheckInventoryForARS(chkInvRequest)

                    invStsCd = chkInvResponse.invStatusCode
                    If (AppConstants.TO_BE_ORDERED = invStsCd OrElse AppConstants.MAKE_TO_ORDER = invStsCd) Then
                        'means that No stock reservation, clear store-loc, etc.
                        isLnUpdated = UnreserveLine(soLn)
                        unReserved = True
                    ElseIf (AppConstants.RESERVE_TO_INV = invStsCd) Then
                        'means reservations have to be created for non-reserved lines
                        'for reserved lines, leaves the reservation as is
                        If (storeCd.isEmpty() AndAlso locCd.isEmpty()) Then
                            'updates the response, so that the caller can process the reservations
                            arsResponse.lnsToReserve.Add(If(isOrderEntry, soLn("ROW_ID"), soLn("DEL_DOC_LN#")))
                        End If

                    ElseIf (AppConstants.RESERVE_TO_PO = invStsCd) Then
                        'means that the line needs to be linked to the returned PO 
                        isLnUpdated = True  'updates to the lines are happening 

                        'removes the reservation (if any)
                        UnreserveLine(soLn)
                        unReserved = True
                        'links to the PO values provided by the E1 API
                        soLn("STORE_CD") = System.DBNull.Value
                        soLn("LOC_CD") = System.DBNull.Value
                        soLn("RES_ID") = System.DBNull.Value
                        soLn("OUT_ID") = System.DBNull.Value
                        soLn("SERIAL_NUM") = System.DBNull.Value
                        soLn("PO_CD") = chkInvResponse.poCd
                        soLn("PO_LN") = chkInvResponse.poLnNum
                    ElseIf (AppConstants.CANNOT_BE_ORDERED = invStsCd) Then
                        'since SKU has been dropped, removes reservation (if any)
                        isLnUpdated = UnreserveLine(soLn)
                        unReserved = True
                        isValidationPassed = False
                        validationMessage = Resources.POSErrors.ERR0023
                        If (Not validationMessageInserts.Contains(soLn("itm_cd").ToString())) Then
                            validationMessageInserts.Add(soLn("itm_cd").ToString())
                        End If
                    End If

                    'if the order is not paid in full and item is dropped then un reserve the item
                    If (isPaidInFull OrElse isDroppedItem) Then
                        'Do nothing
                    Else
                        'lands here when the order is NOT paid in full and it NOT a dropped item
                        If Not unReserved Then
                            isLnUpdated = UnreserveLine(soLn)
                        End If
                    End If

                    'next processing only required for Order Entry based on results above
                    If (isOrderEntry AndAlso isLnUpdated) Then
                        Dim poLn As String = If((Not IsDBNull(soLn("PO_LN")) AndAlso CInt(soLn("PO_LN")) <> 0), soLn("PO_LN"), "")

                        GetSalesDac().UpdateTempItmTable(soLn("ROW_ID"), soLn("QTY"),
                                                         If(Not IsDBNull(soLn("STORE_CD")), soLn("STORE_CD"), ""),
                                                         If(Not IsDBNull(soLn("LOC_CD")), soLn("LOC_CD"), ""),
                                                         If(Not IsDBNull(soLn("SERIAL_NUM")), soLn("SERIAL_NUM"), ""),
                                                         If(Not IsDBNull(soLn("OUT_ID")), soLn("OUT_ID"), ""),
                                                         If(Not IsDBNull(soLn("RES_ID")), soLn("RES_ID"), ""),
                                                         If(Not IsDBNull(soLn("PO_CD")), soLn("PO_CD"), ""),
                                                         poLn)
                    End If

                End If   'If (isInvTpLn AndAlso isNonVoidedLn AndAlso isNonPOLinkedLn)
            Next
            arsResponse.lnsDataset = lnsData
            arsResponse.isValid = isValidationPassed
            arsResponse.message = String.Format(validationMessage, String.Join(",", validationMessageInserts.ToArray()))
        End If

        Return arsResponse
    End Function

    ''' <summary>
    ''' Retrieves all the discounts that are tied to an order line that has been marked for void and hence
    ''' has to be deleted too.
    ''' </summary>
    ''' <param name="dtOrderLines">the table that contains ALL order lines in the current order</param>
    ''' <param name="dtDiscApplied">the table that contains ALL discounts applied to the current order</param>
    ''' <returns>table containing the discount objects that need to be deleted as part of the line void in ORDER MANAGEMENT</returns>
    ''' MM-9870
    Private Function GetDiscountsToDelete(ByVal dtOrderLines As DataTable,
                                         ByVal dtDiscApplied As DataTable,
                                         ByVal isSOM As Boolean,
                                         Optional ByVal IsCanceled As Boolean = False) As DataTable

        Dim discountsTable As DataTable = DiscountUtils.GetLineDiscountTable()
        Dim lstRows As DataRow()
        Dim strWhereClause As String

        If (dtDiscApplied.Select("DISC_IS_DELETED = false").Length() > 0) Then

            'select all non-deleted discounts
            lstRows = dtDiscApplied.Select("DISC_IS_DELETED = false")
            If (lstRows.Length > 0) Then
                For Each drDisc As DataRow In lstRows

                    'Check if this discount is tied to a line that has been marked for void/delete. If so, 
                    'add it to the list of discounts to delete
                    If isSOM Then
                        strWhereClause = ("[DEL_DOC_LN#] = " & drDisc("LNSEQ_NUM") & " AND VOID_FLAG = 'Y' ")
                    Else
                        strWhereClause = ("[LINE] = " & drDisc("LNSEQ_NUM") & " ")
                    End If
                    'strWhereClause = ("[DEL_DOC_LN#] = " & drDisc("LNSEQ_NUM") & " AND VOID_FLAG = 'Y' ")
                    If (dtOrderLines.Select(strWhereClause).Length > 0) Then
                        discountsTable.ImportRow(drDisc)
                    End If
                Next
                'MM-9870
                'Geting discount lines to be removed  and Importing into the discountsTable table
                If IsCanceled Then
                    Dim disdr = From u In dtDiscApplied.AsEnumerable()
                             Where u.Field(Of String)("Approved") = "N"
                             Select u
                    If (Not IsNothing(disdr)) Then
                        For Each dr In disdr
                            discountsTable.ImportRow(dr)
                        Next
                    End If
                End If
            End If

        End If

        Return discountsTable
    End Function
    ''' <summary>
    ''' Retrieves details from the Session("DISC_APPLIED_RELN") Applied table for those discount records
    ''' that are NOT associated to any Session("REL_LN") record, due to a line void event.
    ''' </summary>
    ''' <param name="dtRelnLines">the Relationship line data details</param>
    ''' <param name="dtDiscApplied">the discount applied for Relationship lines data details</param>
    ''' <returns>table containing the orphaned discounts details</returns>
    ''' MM-9870
    Public Function GetDiscountsToDeleteFromRELN(ByVal dtRelnLines As DataTable, ByVal dtDiscApplied As DataTable, Optional ByVal IsCanceled As Boolean = False) As DataTable

        Dim discountsTable As DataTable = DiscountUtils.GetLineDiscountTable()
        Dim lstRows As DataRow()
        Dim strWhereClause As String

        If (dtDiscApplied.Select("DISC_IS_DELETED = false").Length() > 0) Then

            'select all non-deleted discounts
            lstRows = dtDiscApplied.Select("DISC_IS_DELETED = false")

            If (lstRows.Length > 0) Then

                For Each drDisc As DataRow In dtDiscApplied.Rows
                    strWhereClause = ("[LINE] = " & drDisc("LNSEQ_NUM") & " ")
                    If Not dtRelnLines.Select(strWhereClause).Length > 0 Then
                        drDisc.Item("DISC_IS_DELETED") = True
                        discountsTable.ImportRow(drDisc)
                    End If
                Next
                'MM-9870
                'Geting discount lines to be removed and Importing in to the discountsTable table
                If IsCanceled Then
                    Dim disdr = From u In dtDiscApplied.AsEnumerable()
                             Where u.Field(Of String)("Approved") = "N"
                             Select u
                    If (Not IsNothing(disdr)) Then
                        For Each dr In disdr
                            discountsTable.ImportRow(dr)
                        Next
                    End If
                End If
            End If
        End If
        Return discountsTable
    End Function


    ''' <summary>
    ''' This is method is to check whether the cash drawer is closed or not
    ''' </summary>
    ''' <param name="companyCode">company code</param>
    ''' <param name="storeCode">store code</param>
    ''' <param name="cashDrawerCode">cash drawer code</param>
    ''' <param name="postDate">post date</param>
    ''' <returns>True if the cash drawer is open, False otherwise</returns>
    Public Function IsCashDrawerActive(ByVal companyCode As String, ByVal storeCode As String,
                                ByVal cashDrawerCode As String, ByVal postDate As Date) As Boolean
        If SysPms.isCashierActiv Then
            Return GetSalesDac().IsCashDrawerActive(companyCode, storeCode, cashDrawerCode, postDate)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' This is method is to get promotion details when promo price is consider against to retail price. 
    ''' </summary>
    ''' <param name="dr">DataRow</param>
    ''' <param name="effDt">Promo calender effective date</param>
    ''' <returns>DataRow</returns>
    ''' As per one of the JIRA requirement(7445) to get promotion calender and store or  strore group level package price
    ''' To re-use the method PopulatePromoDetails which was in the page Price_Tag.aspx.vb, now is moved to SalesBIZ and SalesDac files
    Public Function getPromoDetails(ByRef dr As DataRow, ByVal effDt As Object)
        Return GetSalesDac().getPromoDetails(dr, effDt)
    End Function

    ''' <summary>
    ''' This is method is to check whether the warranty is linked to other sales order other than original order.
    ''' </summary>
    ''' <param name="Deldocnum">sale order id (del doc number)</param>
    ''' <returns>True if the warranty is linked to any other sales order, other than the oridinal sales order</returns>
    Public Function CheckWarrLink(ByVal delDocNum As String, Optional ByVal IsWarr As Boolean = False) As Boolean
        Return GetSalesDac().CheckWarrLink(delDocNum, IsWarr)
    End Function

    ''' <summary>
    ''' This method will return the exist warranty list for this session
    ''' </summary>
    ''' <param name="sessionId">the session Id for getting the exist warranty list</param>
    ''' <returns>A datatable filled with exist warranty list</returns>
    ''' <remarks></remarks>
    Public Function GetTempExistWarrs(ByVal sessionId As String) As DataTable
        Return GetSalesDac().GetTempExistWarrs(sessionId)
    End Function

    ''' <summary>
    ''' Deletes ALL records from TEMP_WARR tables that match the sessionID passed in.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    ''' <remarks></remarks>
    Public Sub DeleteTempWarrEntry(ByVal sessionId As String)
        GetSalesDac().DeleteTempWarrEntry(sessionId)
    End Sub

    Private Function GetInvBiz() As InventoryBiz
        If (theInvBiz Is Nothing) Then
            theInvBiz = New InventoryBiz()
        End If

        Return theInvBiz
    End Function

    Private Function GetSalesDac() As SalesDac
        If (theSalesDac Is Nothing) Then
            theSalesDac = New SalesDac()
        End If

        Return theSalesDac
    End Function

    Private Function GetStoreDac() As StoreDac
        If (theStoreDac Is Nothing) Then
            theStoreDac = New StoreDac()
        End If

        Return theStoreDac
    End Function

    Private Function GetCommentsDac() As CommentsDac
        If (theCommentsDac Is Nothing) Then
            theCommentsDac = New CommentsDac()
        End If

        Return theCommentsDac
    End Function

    Private Function GetTMDac() As TransportationDac
        If (theTMDac Is Nothing) Then
            theTMDac = New TransportationDac()
        End If

        Return theTMDac
    End Function

    Private Function GetTaxDac() As TaxDac
        If (theTaxDac Is Nothing) Then
            theTaxDac = New TaxDac()
        End If

        Return theTaxDac
    End Function

    Private Function GetDiscountDac() As DiscountDac
        If (theDiscDac Is Nothing) Then
            theDiscDac = New DiscountDac()
        End If

        Return theDiscDac
    End Function

    Private Function GetSkuDac() As SKUDac
        If (theSkuDac Is Nothing) Then
            theSkuDac = New SKUDac()
        End If

        Return theSkuDac
    End Function
    ''' <summary>
    ''' Retrieves information for the package header and its components and creates temp_itm records for each. 
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    'Public Function GetComponentSplitPrices(
    '                                        ByVal pkgSku As String,
    '                                        ByVal pkgPrice As String,
    '                                        ByVal pkgSplitMethod As String,
    '                                        ByRef conn As OracleConnection,
    '                                        ByRef errorMessage As String) As DataSet


    '    Return GetSalesDac().GetComponentSplitPrices(pkgSku, pkgPrice, pkgSplitMethod, conn, errorMessage)
    'End Function

    ' ''' <summary>
    ' ''' Retrieves information for the package header for the deleted item in SOE. This retrives the data from temp_ITM table, package headder row only. this is usefull when the packet split is set to UPON COMMIT!
    ' ''' </summary>
    ' ''' <param name="headerInfo">This Method extracts the package headder details for the given RowID of the temp_itm table.</param>
    ' ''' <param name="pkgPrice">Reference type, which holds the package price stored in the package headder row of temp_itm table</param>
    ' ''' <param name="pkgSplitMethod">reference type, the package split method type.</param>
    ' ''' <param name="pkgParentItm">Reference type, holds the package headder itm code</param>
    ' ''' <param name="conn">Reference type, Connection object</param>
    ' ''' <returns>a data table of component SKUs (item codes)</returns>
    ' ''' 
    'Public Function GetTmpItmHeadderDetails(
    '                                        ByVal skuRowID As String,
    '                                        ByRef pkgPrice As String,
    '                                        ByRef pkgSplitMethod As String,
    '                                        ByRef pkgParentItm As String,
    '                                          ByRef pkgRowID As String,
    '                                        ByRef conn As OracleConnection
    '                                        )


    '    Return GetSalesDac().GetTmpItmHeadderDetails(skuRowID, pkgPrice, pkgSplitMethod, pkgParentItm, pkgRowID, conn)
    'End Function

    ' ''' <summary>
    ' ''' Updates the package price and tax in headder row. this is usefull when the packet split is set to UPON COMMIT!
    ' ''' </summary>
    ' ''' <param name="skuRowID">Row id to be updated.</param>
    ' ''' <param name="pkgPrice">New price to be updated</param>
    ' ''' <param name="taxPct">Tax percentage</param>
    ' ''' <param name="conn">Reference type, Connection object</param>
    ' ''' <returns>a data table of component SKUs (item codes)</returns>
    ' ''' 
    'Public Function UpdateNewPkgHdrPrcForTmpOrder(
    '                                        ByVal skuRowID As String,
    '                                        ByRef pkgPrice As String,
    '                                        ByRef taxPct As String,
    '                                        ByRef conn As OracleConnection
    '                                        )


    '    Return GetSalesDac().UpdateNewPkgHdrPrcForTmpOrder(skuRowID, pkgPrice, taxPct, conn)
    'End Function
    ' ''' <summary>
    ' ''' Adds the session data for package details
    ' ''' </summary>
    ' ''' <param name="skuRowID">Row id to be updated.</param>
    ' ''' <param name="pkgPrice">New price to be updated</param>
    ' ''' ''' <param name="packageSplitItems">Data table with package splitted allon g with the prices</param>
    ' ''' <returns></returns>
    ' ''' 
    'Public Function addSplitDataToSession(ByVal PkgSkuRowID As String, ByVal dtPackageSplitItems As DataTable)
    '    If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
    '        Dim packageSplitData As DataTable = DirectCast(Web.HttpContext.Current.Session("PKGSPLITDATA"), DataTable)
    '        Dim newColumn As New Data.DataColumn("PACKAGEROWID", GetType(System.String))
    '        newColumn.DefaultValue = PkgSkuRowID
    '        Dim packageSplitItems As DataTable = dtPackageSplitItems.Copy()

    '        '--------------------------
    '        Dim filteredRows As DataRow()
    '        Dim filterText As String = ""
    '        filterText = "PACKAGEROWID = '" + PkgSkuRowID + "' "
    '        Dim pkgPrc As Double = 0.0
    '        '--------------------------

    '        If packageSplitItems.Columns.Contains("PACKAGEROWID") Then
    '            pkgPrc = packageSplitItems.Compute("Sum(RET_PRC)", "PACKAGEROWID = '" + PkgSkuRowID + "'")
    '            For Each dr As DataRow In packageSplitItems.Rows
    '                dr("PACKAGEROWID") = PkgSkuRowID
    '                dr("PCT") = (CDbl(dr("RET_PRC").ToString()) / CDbl(pkgPrc)) * 100
    '            Next
    '        Else
    '            packageSplitItems.Columns.Add(newColumn)
    '            packageSplitItems.Columns.Add("PCT", GetType(Double))
    '            pkgPrc = packageSplitItems.Compute("Sum(RET_PRC)", "PACKAGEROWID = '" + PkgSkuRowID + "'")
    '            'packageSplitItems.Columns.Add("RATIO", GetType(Double))
    '            For Each dr As DataRow In packageSplitItems.Rows
    '                dr("PACKAGEROWID") = PkgSkuRowID
    '                dr("PCT") = (CDbl(dr("RET_PRC").ToString()) / CDbl(pkgPrc)) * 100
    '            Next
    '        End If
    '        Dim newRow As DataRow
    '        If Not packageSplitData Is Nothing Then
    '            For Each dr As DataRow In packageSplitItems.Rows
    '                'dr("PCT") = (CDbl(dr("RET_PRC").ToString()) / CDbl(pkgPrice)) * 100
    '                newRow = packageSplitData.NewRow
    '                newRow = dr
    '                packageSplitData.ImportRow(newRow)
    '            Next
    '            Web.HttpContext.Current.Session("PKGSPLITDATA") = packageSplitData
    '        Else
    '            Web.HttpContext.Current.Session("PKGSPLITDATA") = packageSplitItems
    '        End If
    '    End If
    'End Function
    ' ''' <summary>
    ' ''' Updates the shares of the item when and componenet is removed form package SKU
    ' ''' </summary>
    ' ''' <param name="skuRowID">Row id to be updated.</param>
    ' ''' <param name="pkgPrice">New price to be updated</param>
    ' ''' ''' <param name="packageSplitItems">Data table with package splitted allon g with the prices</param>
    ' ''' <returns></returns>
    ' ''' 
    'Public Function updateSplitItmShareOnDelete(ByVal pkgRowID As String, ByVal deletedSKU As String)
    '    Dim removedPrice As Double
    '    If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
    '        Dim packageSplitData As DataTable = DirectCast(Web.HttpContext.Current.Session("PKGSPLITDATA"), DataTable)
    '        If packageSplitData IsNot Nothing Then
    '            Dim filteredRows As DataRow()
    '            Dim sumObject As Double = 0.0
    '            Dim filterText As String = "PACKAGEROWID = '" + pkgRowID + "' AND ITM_CD = '" + deletedSKU.ToUpper() + "'"
    '            filteredRows = packageSplitData.Select(filterText)
    '            If filteredRows.Length > 0 Then
    '                removedPrice = filteredRows(0)("RET_PRC")
    '                filteredRows(0)("RET_PRC") = 0
    '                'filteredRows(0)("RATIO") = 0.0
    '                filteredRows(0)("PCT") = 0.0
    '            End If
    '            Web.HttpContext.Current.Session("PKGSPLITDATA") = packageSplitData
    '        End If
    '    End If
    '    Return removedPrice
    'End Function

    ' ''' <summary>
    ' ''' Adds the session data for package details
    ' ''' </summary>
    ' ''' <param name="skuRowID">Row id to be updated.</param>
    ' ''' <param name="pkgPrice">New price to be updated</param>
    ' ''' ''' <param name="packageSplitItems">Data table with package splitted allon g with the prices</param>
    ' ''' <returns></returns>
    ' ''' 
    'Public Function updateSplitItmShareWhenPriceChange(ByVal pkgRowID As String, ByVal pkgNewPrc As Double)
    '    If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
    '        Dim packageSplitData As DataTable = DirectCast(Web.HttpContext.Current.Session("PKGSPLITDATA"), DataTable)
    '        If packageSplitData IsNot Nothing Then
    '            Dim filteredRows As DataRow()
    '            Dim filterText As String = ""
    '            filterText = "PACKAGEROWID = '" + pkgRowID + "' "
    '            Dim ratio As Double = packageSplitData.Compute("Sum(PCT)", "PACKAGEROWID = '" + pkgRowID + "'")
    '            filteredRows = packageSplitData.Select(filterText)
    '            For Each dr In filteredRows
    '                dr("PCT") = Math.Round((CDbl(dr("PCT").ToString()) / CDbl(ratio)) * 100, 2)
    '                'dr("RATIO") = (CDbl(dr("PCT").ToString()) / CDbl(ratio))
    '                dr("RET_PRC") = Math.Round((dr("PCT") / 100) * pkgNewPrc, 2)
    '            Next
    '            Dim total As Double = packageSplitData.Compute("Sum(RET_PRC)", "PACKAGEROWID = '" + pkgRowID + "'")
    '            If total > pkgNewPrc Then
    '                filteredRows(filteredRows.Length - 1)("RET_PRC") = filteredRows(filteredRows.Length - 1)("RET_PRC") - Math.Abs(total - pkgNewPrc)
    '            Else
    '                filteredRows(filteredRows.Length - 1)("RET_PRC") = filteredRows(filteredRows.Length - 1)("RET_PRC") + Math.Abs(total - pkgNewPrc)
    '            End If
    '            Web.HttpContext.Current.Session("PKGSPLITDATA") = packageSplitData
    '        End If
    '    End If
    'End Function

    ' ''' <summary>
    ' ''' Removes the packlage split lines from the session
    ' ''' </summary>
    ' ''' <param name="skuRowID">Row id to be updated.</param>
    ' ''' <remarks></remarks>
    'Public Function removeAllSplitPkgItms(ByVal pkgRowID As String)
    '    If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
    '        Dim packageSplitData As DataTable = DirectCast(Web.HttpContext.Current.Session("PKGSPLITDATA"), DataTable)
    '        Dim filteredRows As DataRow()
    '        Dim sumObject As Double = 0.0
    '        Dim filterText As String = ""
    '        filterText = "PACKAGEROWID = '" + pkgRowID + "' "
    '        filteredRows = packageSplitData.Select(filterText)
    '        For Each dr In filteredRows
    '            packageSplitData.Rows.Remove(dr)
    '        Next
    '        Web.HttpContext.Current.Session("PKGSPLITDATA") = packageSplitData
    '    End If
    'End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sessionId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSalesItemForSOE(sessionId As String) As DataTable
        Return GetSalesDac().GetSalesItemForSOE(sessionId)
    End Function

    ''' <summary>
    ''' All the entered warrantable SKUs with comma (',') separator
    ''' </summary>
    ''' <param name="warrCd">Selected Warranty Code</param>
    ''' <param name="sessionId">sessionId</param>
    ''' <returns>A datatable with SKUs which are having the selected warranty</returns>
    ''' <remarks></remarks>
    Public Function GetItemsForWarranty(warrCd As String, enteredSKUs As String) As DataTable
        Return GetSalesDac().GetItemsForWarranty(warrCd, enteredSKUs)
    End Function

    ''' <summary>
    ''' Returns the Return Cause Code(s)
    ''' </summary>
    ''' <returns>dataset with return cause code(s)</returns>
    ''' <remarks> To display the return cause code(s) on sales exchange screen</remarks>
    Public Function GetCRMCauseCodes() As DataTable
        Return GetSalesDac().GetCRMCauseCodes
    End Function

    ''' <summary>
    ''' Returns the SUM of the Retail for an Item from TEMP_ITM table
    ''' </summary>
    ''' <param name="itemCode">item code</param>
    ''' <param name="sessionId">session id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalItemRetail(ByVal itemCode As String, ByVal sessionId As String) As Double
        Return GetSalesDac().GetTotalItemRetail(itemCode, sessionId)
    End Function
    ''' <summary>
    ''' Inserts into RELATIONSHIP_LN2DISC table, all discounts that were created for the relationship passed in
    ''' </summary>
    ''' <param name="REL_NO">the relationship number the lines belong to</param>
    ''' <param name="sessionId"> sessionId will help to get detail of that session</param>
    Public Sub ProcessRelnDiscounts(ByVal relnum As String, ByVal sessionId As String)
        GetSalesDac().ProcessRelnDiscounts(relnum, sessionId)
    End Sub
    ''' <summary>
    ''' Inserts into RELATIONSHIP_LN2DISC table, all discounts that were created for the document passed in
    ''' </summary>
    ''' <param name="REL_NO">the document number the lines belong to</param>
    ''' ''' <param name="REL_NO">the document number the lines belong to</param>
    Public Sub ProcessRelnDiscounts(ByVal relnum As String, ByVal relnTable As DataTable)
        GetSalesDac().ProcessRelnDiscounts(relnum, relnTable)
    End Sub

    ''' <summary>
    ''' Update GL_DOC_TRN table if it is finalized Order
    ''' </summary>
    ''' <param name="Sql"></param>
    ''' <param name="dbAtomicCommand"></param>
    ''' <param name="del_doc_num"></param>
    ''' <param name="CO_CD"></param>
    ''' <remarks></remarks>
    Public Sub UpdateGldoctrn(ByRef dbAtomicCommand As OracleCommand, ByVal del_doc_num As String, ByVal CO_CD As String)
        GetSalesDac().UpdateGldoctrn(dbAtomicCommand, del_doc_num, CO_CD)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lineNum"></param>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateWarrReference(ByVal lineNum As Integer, ByVal delDocNum As String, ByVal isWarr As Boolean) As String
        Dim errorMsg As String = ""
        If isWarr Then
            Dim dtDelDocNum As DataTable = GetSalesDac().GetAllWarrantedLines(lineNum, delDocNum)
            If Not IsNothing(dtDelDocNum) AndAlso dtDelDocNum.Rows.Count > 0 Then
                Dim results = (From row In dtDelDocNum.AsEnumerable()
                   Select Col1 = row.Field(Of String)("DEL_DOC_NUM")
                   ).Distinct().ToList

                If Not IsNothing(results) Then
                    If results.Count = 1 Then
                        If results(0).ToString.ToUpper <> delDocNum.ToUpper Then
                            errorMsg = Resources.POSMessages.MSG0050
                        End If
                    ElseIf results.Count > 1 Then
                        errorMsg = Resources.POSMessages.MSG0050
                    End If
                End If
            End If
        Else
            Dim soLnWarr As SoLineWarranty = GetSalesDac().GetSoWarrantyLine(lineNum, delDocNum)
            If soLnWarr.warDelDocNUM <> delDocNum Then
                If soLnWarr.statCd = AppConstants.Order.STATUS_FINAL Then
                    errorMsg = Resources.POSMessages.MSG0051
                Else
                    errorMsg = Resources.POSMessages.MSG0052
                End If
            End If
        End If
        Return errorMsg
    End Function

    Public Function ValidateSalesOrder(ByVal delDocNum As String) As Boolean
        Return GetSalesDac().ValidateSalesOrder(delDocNum)
    End Function

    ''' <summary>
    ''' Deletes ALL records from MULTI_TEMP that match the sessionID passed in.
    ''' This operation clears only the lines for a multiple discount lines.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    ''' <param name="lstDISC_CD">the list of DISC_CD expired</param>
    Public Function DeleteTempLnDiscount(ByVal sessionId As String, ByVal lstDISC_CD As ArrayList) As Boolean
        Return GetDiscountDac().DeleteTempLnDiscount(sessionId, lstDISC_CD)
    End Function

    ''' <summary>
    ''' Deletes ALL records from MULTI_TEMP that match the sessionID passed in.
    ''' This operation clears only the lines for a multiple discount lines.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    Public Function DeleteTempLnDiscount(ByVal sessionId As String) As Boolean
        Return GetDiscountDac().DeleteTempLnDiscount(sessionId)
    End Function

    ''' <summary>
    ''' Returns the query used to retrieve Discounts Applied to an SOE order (or an specific line)
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <returns>query to be used when retrieving SOE applied discounts</returns>
    Public Function GetDiscountsAppliedForSOCreation(ByVal sessionId As String) As DataTable
        Dim dtDiscount As New DataTable
        dtDiscount = GetDiscountDac().GetDiscountsAppliedForSOCreation(sessionId)
        Return dtDiscount
    End Function

    ''' <summary>
    '''If all the discount applied are expired then replace the retail price with orginal price and 
    ''' discount amount as null.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    Public Function ReapplyDiscountForRelConvSOE(ByVal sessionId As String) As Boolean
        GetDiscountDac().ReapplyDiscountForRelConvSOE(sessionId)
    End Function
    ''' <summary>
    '''Insert into the MULTI_DISC table on when relationship converted to sales order.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <param name="ln2disc">relationship discount details.</param>
    Public Function InserttoMultiDisc(ByVal sessionId As String, ByVal ln2disc As DataTable)
        GetDiscountDac().InserttoMultiDisc(sessionId, ln2disc)
    End Function

    ''' <summary>
    ''' Get the last DEL_DOC_LN# from SO_LN table for the input delDocNum
    ''' </summary>
    ''' <param name="delDocNum">DelDocNum</param>
    ''' <returns>DelDocLn</returns>
    ''' <remarks></remarks>
    Public Function GetCurrDelDocLine(ByVal delDocNum As String) As Integer
        Return GetSalesDac().GetCurrDelDocLine(delDocNum)
    End Function

    Public Function LookupOrders(ByVal req As OrderRequestDtc) As DataSet
        Return (GetSalesDac().LookupOrders(req))
    End Function


    Public Function LookupOrders(ByVal req As OrderRequestDtc, opt As Integer) As DataSet
        Return (GetSalesDac().LookupOrders2(req, opt))
    End Function

    Public Function UpdateLnTaxAmt(ByRef cmdUpdateItems As OracleCommand, ByVal retPrc As Double, ByVal rowid As Integer) As Integer
        Return GetSalesDac().UpdateLnTaxAmt(cmdUpdateItems, retPrc, rowid)
    End Function

    ''' <summary>
    ''' Update the warranty details for the sales order creation section
    ''' </summary>
    ''' <param name="sessionID">Session ID</param>
    ''' <param name="rowid">New row id of the temp table </param>
    ''' <param name="voidedLines">Total number of lined voided</param>
    ''' <param name="firstRow ">First row sequence of the order</param>
    ''' <remarks></remarks>
    ''' 
    Public Function UpdateWarrDetailsForNonSale(ByRef cmdUpdate As OracleCommand, ByVal sessionID As String, ByVal rowid As Integer, ByVal voidedLines As Integer, ByVal orgiDocNumber As String, ByVal orgiDocLineNumber As String, ByVal firstRow As Integer)
        Return GetSalesDac().UpdateWarrDetailsForNonSale(cmdUpdate, sessionID, rowid, voidedLines, orgiDocNumber, orgiDocLineNumber, firstRow)
    End Function

    Public Sub UpdateWarrLinkForSplitSales(ByRef cmdUpdate As OracleCommand, ByVal sessionID As String, ByVal orgiDocNumber As String, ByVal firstRow As Integer)
        GetSalesDac().UpdateWarrLinkForSplitSales(cmdUpdate, sessionID, orgiDocNumber, firstRow)
    End Sub
    ''' <summary>
    ''' GetHeaderDiscountSeqNum TO GET the SeqNum for the Header Discount in the discount gridview
    ''' </summary>
    ''' <param name="ordLn2Disc">ordLn2Disc datatable</param>
    ''' <param name="includeDeleted"></param>
    ''' <returns>hdrDiscSeqNum</returns>
    ''' <remarks></remarks>
    Public Function GetHeaderDiscountSeqNum(ByVal ordLn2Disc As DataTable, Optional ByVal includeDeleted As Boolean = True) As Integer

        Dim hdrDiscSeqNum As Integer
        If (Not IsNothing(ordLn2Disc) AndAlso ordLn2Disc.Rows.Count > 0) Then

            Dim qryStr As New StringBuilder
            qryStr.Append("DISC_LEVEL='H'")
            If Not includeDeleted Then
                qryStr.Append(" AND disc_is_deleted = false")
            End If
            Dim drhdrDisc As DataRow() = ordLn2Disc.Select(qryStr.ToString)
            For Each dr As DataRow In drhdrDisc

                hdrDiscSeqNum = dr("SEQ_NUM")
                Exit For                    'no need to keep on searching only ONE is allowed
            Next
        End If

        Return hdrDiscSeqNum
    End Function

    ''' <summary>
    ''' OverLoaded Method:Validates the store last balanced date against the transaction date provided
    ''' If valid, a blank string is returned; if the store has been closed thru the 
    ''' transaction date, then a store closed thru message is returned
    ''' </summary>
    ''' <param name="storeCd">the store code to be checked</param>
    ''' <param name="tranDt">the transaction date to used in the check</param>
    ''' <param name="storeInfo">the stores data</param>
    ''' <returns>a message if the store has been closed or an empty string if not</returns>
    Public Function ValidateStoreClosedDate(ByVal storeCd As String, ByVal tranDt As String, ByVal storeInfo As StoreData) As String

        Dim msg As String = String.Empty
        If (Not String.IsNullOrEmpty(storeCd)) Then
            Dim lastBalDt As Date = storeInfo.lastBalDt
            If (lastBalDt >= tranDt) Then
                msg = String.Format(Resources.POSMessages.MSG0008, storeCd, FormatDateTime(lastBalDt.ToString, DateFormat.ShortDate))
            End If
        End If

        Return msg
    End Function
    ''' <summary>
    ''' SkuCheckForTakeWith
    ''' </summary>
    ''' <param name="sku"></param>
    ''' <param name="dbConnection"></param>
    ''' <param name="dbcommand"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SkuCheckForTakeWith(ByVal sku As String, ByRef dbcommand As OracleCommand) As Boolean
        Return GetSalesDac().SkuCheckForTakeWith(sku, dbcommand)
    End Function
    '' <summary>
    '' This method returns the SKU price.
    '' </summary>
    '' <param name="itemCd">Item code for which the price is to be queried.</param>
    '' <returns></returns>
    Public Function GetSKUPrice(ByVal itemCd As String) As Double
        Dim skuDAC As SKUDac = New SKUDac()
        Return skuDAC.GetSKUPrice(itemCd)
    End Function

    ''' <summary>
    ''' This method returns the package price based on the component reference
    ''' </summary>
    ''' <param name="PackageParentRow">Line Number of the component Parent Line</param>
    ''' <returns></returns>
    Public Function GetPackageDetailsByComponentForSOE(ByVal PackageParentRow As Integer) As PackageItem
        Return GetSalesDac().GetPackageDetailsByComponentForSOE(PackageParentRow)
    End Function
    Public Sub UpdatePackagePrice(ByVal packageItem As PackageItem, ByVal packageParentRow As Integer, ByVal cmdUpdate As OracleCommand)
        GetSalesDac().UpdatePackagePrice(packageItem, packageParentRow, cmdUpdate)
    End Sub

    Public Sub InsertAccountingRecord(ObjAccount As AccountingDtc, conn As OracleConnection)
        GetSalesDac().InsertAccountingEntries(ObjAccount, conn)
    End Sub

    'Alice added on Jan 08, 2019 for request 2260
    Public Function Validate_MCCL_SO_Access(ByVal delDocNum As String) As Boolean
        Return GetSalesDac().Validate_MCCL_SO_Access(delDocNum)
    End Function

    ''' <summary>Get sales order booking status code</summary> #ITREQUEST-337
    Public Function getPOLineBookingStatus(ByVal po_cd As String, ByVal po_ln As String) As String
        Return GetSalesDac().getPOLineBookingStatus(po_cd, po_ln)
    End Function

    ''' <summary>Get sales order booking quantity</summary> #ITREQUEST-337
    Public Function getPOLineBookingQty(ByVal po_cd As String, ByVal po_ln As String) As Integer
        Return GetSalesDac().getPOLineBookingQty(po_cd, po_ln)
    End Function

End Class

