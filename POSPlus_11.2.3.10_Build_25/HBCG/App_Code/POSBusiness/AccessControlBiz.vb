﻿Imports Microsoft.VisualBasic

Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTabControl
Imports System.Collections.Generic
Imports System.Linq
Imports jda.mm.order.DataTransferClass

<Serializable()>
Public Class AccessControlBiz
    Private Const versionNumber As String = "11.2"
    Private Const applicationType As String = "POS+" & versionNumber
    Private theAccessControlDac As AccessControlDac

    Public Sub DeleteRecord(ByVal pageName As String)
        Dim accessInfo As New AccessControlBiz()
        If pageName.Contains("SalesOrderMaintenance.aspx") Then
            'For SOM+ page                                       
            accessInfo.DeleteRecordsforPage("SOM+")
        ElseIf pageName.Contains("SalesRewrite.aspx") Then
            'For SORW+ page                   
            accessInfo.DeleteRecordsforPage("SORW+")
        Else
            'For other page - future pupose - If required
        End If
    End Sub

    ''' <summary>
    ''' Method is written to add the record(request) to the table
    ''' </summary>
    ''' <param name="originCode">Screen name</param>
    ''' <param name="docType">Document type</param>
    ''' <param name="docValue">Document value</param>
    ''' <param name="employeeCode">Employee code</param>
    ''' <param name="sessionID">Session id</param>    
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>
    Public Sub InsertPageRequest(ByVal originCode As String, ByVal docType As String, ByVal docValue As String, ByVal employeeCode As String, ByVal sessionID As String, ByVal desc As String)
        GetAccessControlDac().InsertPageRequest(originCode, docType, docValue, employeeCode, sessionID, applicationType, desc)
    End Sub

    ''' <summary>
    ''' Method is written to verify whether the user has the access to this page or not
    ''' </summary>
    ''' <param name="originCode">Screen name</param>
    ''' <param name="docType">Document type</param>
    ''' <param name="docValue">Document value</param>
    ''' <param name="employeeCode">Employee code</param>
    ''' <param name="sessionID">Session id</param>        
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>  
    Public Sub VerifyRequest(ByVal originCode As String, ByVal docType As String, ByVal docValue As String, ByRef employeeCode As String, ByVal sessionID As String, Optional ByVal desc As String = "")
        'Public Function VerifyRequest(ByVal originCode As String, ByVal docType As String, ByVal docValue As String, ByRef employeeCode As String, ByVal sessionID As String, Optional ByVal desc As String = "") As Boolean
        GetAccessControlDac().VerifyRequest(originCode, docType, docValue, employeeCode, sessionID, applicationType, desc)
    End Sub

    ''' <summary>
    ''' Deletes the document in AUDIT_LOCK2LOG table
    ''' </summary>
    ''' <param name="docValue">Document value</param>    
    ''' <param name="sessionID">Session id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteRequest(ByVal docValue As String, ByVal sessionID As String) As Integer
        DeleteRequest = GetAccessControlDac().DeleteRequest(docValue, sessionID)
    End Function

    ''' <summary>
    ''' To delete the records when user logs out from the application
    ''' </summary>
    ''' <param name="sessionIDs">session id's</param>
    ''' <returns>delete record count</returns>
    ''' <remarks></remarks>
    Public Function DeleteRequest(ByVal sessionIDs As String) As Integer
        DeleteRequest = GetAccessControlDac().DeleteRequest(sessionIDs)
    End Function

    ''' <summary>
    ''' Method written to update the time stamp when user does the modifications on the doc
    ''' </summary>
    ''' <param name="docValue">Document value</param>    
    ''' <param name="sessionID">Session id</param>
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>
    Public Function UpdateRequest(ByVal docValue As String, ByVal sessionID As String, Optional desc As String = "") As Boolean
        Dim result As Boolean = False
        result = GetAccessControlDac().UpdateRequest(docValue, sessionID, desc)
        Return result
    End Function

    ''' <summary>
    ''' TO delete the records based on the page name
    ''' </summary>
    ''' <param name="pageName">Origin Code</param>    
    ''' <remarks></remarks>
    Public Function DeleteRecordsforPage(ByVal pageName As String) As List(Of AccessControlDtc)
        Dim browserIds As String = String.Empty
        Dim browserIDCollection As List(Of AccessControlDtc) = New List(Of AccessControlDtc)

        browserIDCollection = TryCast(HttpContext.Current.Session("deleteBrowserID"), List(Of AccessControlDtc))

        If pageName.Contains("SalesOrderMaintenance.aspx") Then
            'For SOM+ page                                               
            pageName = "SOM+"
        ElseIf pageName.Contains("SalesRewrite.aspx") Then
            'For SORW+ page                                               
            pageName = "SORW+"
        Else
            'For other page - future pupose - If required
        End If

        If Not (browserIDCollection) Is Nothing Then
            browserIds = RemoveUserData(browserIDCollection, pageName)
        Else
            ' If no del_doc_nums found for the pageName then exit from here
            Exit Function
        End If

        Dim finalNumbers As String = String.Empty
        If (browserIds.Length >= 1) Then
            finalNumbers = Convert.ToString(browserIds.Substring(0, browserIds.Length - 1))
            ' Remove the SALE from the table.
            GetAccessControlDac().DeleteRequest(finalNumbers)
        End If
        Return browserIDCollection
    End Function

    ''' <summary>
    ''' Removes the user data from the session
    ''' </summary>
    ''' <param name="usersInfoList">list of user data</param>
    ''' <param name="pageName">origin code</param>    
    ''' <returns>the browser id(s) to delete from the log table</returns>
    ''' <remarks></remarks>
    Private Function RemoveUserData(ByVal usersInfoList As List(Of AccessControlDtc), ByVal pageName As String) As String
        'Shreekanth - 5/8/2014
        'Below logic needs to be modified to c0mpare using equal method

        'To get the BrowserID(s) from the userInfoList based on the screen name and employee initials
        Dim deleteBrowserIDs = (From userInfo In usersInfoList
            Where userInfo.ScreenName = Convert.ToString(pageName) AndAlso userInfo.EmployeeInitials.Equals(Convert.ToString(HttpContext.Current.Session("EMP_INIT")))
            Select userInfo.BrowserId).ToList()

        Dim browserIds As New StringBuilder
        For Each item In deleteBrowserIDs
            Dim deleteCount = usersInfoList.RemoveAll(Function(a) a.BrowserId = Convert.ToString(item))
            If (deleteCount > 0) Then
                browserIds.Append("'" & Convert.ToString(item) & "',")
            End If
        Next

        HttpContext.Current.Session("deleteBrowserID") = usersInfoList
        Return Convert.ToString(browserIds)
    End Function

    ''' <summary>
    ''' TO delete the records when user logs out of the page
    ''' </summary>    
    ''' <remarks></remarks>
    Public Sub DeleteAllRecords()
        Dim delete_del_doc_nums As New StringBuilder
        Dim deleteBrowserID As List(Of AccessControlDtc) = New List(Of AccessControlDtc)
        deleteBrowserID = TryCast(HttpContext.Current.Session("deleteBrowserID"), List(Of AccessControlDtc))
        If Not (deleteBrowserID) Is Nothing Then
            For Each item In deleteBrowserID
                delete_del_doc_nums.Append("'" & Convert.ToString(item.BrowserId & "',"))
            Next

            Dim finalNumbers As String = String.Empty
            If (delete_del_doc_nums.Length >= 1) Then
                finalNumbers = delete_del_doc_nums.ToString().Substring(0, delete_del_doc_nums.Length - 1).ToString()
                ' Remove the SALE from the table.
                GetAccessControlDac().DeleteRequest(finalNumbers)
            Else
                ' If no del_doc_nums found for the pageName then exit from here
                Exit Sub
            End If
        End If
    End Sub

    ''' <summary>
    ''' Method to Disable the controls on the page
    ''' </summary>
    ''' <param name="controls">Collection of the controls</param>
    ''' <param name="Enabled">Its a default parameter which is set to FALSE </param>
    ''' <remarks></remarks>
    Public Sub DisableFields(controls As ControlCollection, Optional Enabled As Boolean = False)
        'Shreekanth - 5/8/2014
        'Below logic needs to be modified to check the type
        'type.Equals(GetType(DevExpress.Web.ASPxTabControl.ASPxPageControl))

        For Each control As Control In controls
            Dim type As System.Type = control.GetType

            If type.Equals(GetType(DevExpress.Web.ASPxEditors.ASPxButton)) Then
                Dim rdb As ASPxButton = CType(control, ASPxButton)
                rdb.Enabled = Enabled
            ElseIf type.Equals(GetType(DevExpress.Web.ASPxEditors.ASPxDateEdit)) Then
                Dim rdb As ASPxDateEdit = CType(control, ASPxDateEdit)
                rdb.Enabled = Enabled
            ElseIf type.Equals(GetType(DevExpress.Web.ASPxEditors.ASPxCheckBox)) Then
                Dim rdb As ASPxCheckBox = CType(control, ASPxCheckBox)
                rdb.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.TextBox)) Then
                Dim txt As TextBox = CType(control, TextBox)
                txt.Enabled = Enabled
            ElseIf type.Equals(GetType(RadioButtonList)) Then
                Dim rdbl As RadioButtonList = CType(control, RadioButtonList)
                rdbl.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.RadioButton)) Then
                Dim rdb As RadioButton = CType(control, RadioButton)
                rdb.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.CheckBox)) Then
                Dim ckb As CheckBox = CType(control, CheckBox)
                ckb.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.DropDownList)) Then
                Dim list As DropDownList = CType(control, DropDownList)
                list.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.Button)) Then
                Dim btn As Button = CType(control, Button)
                btn.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.DataGrid)) Then
                Dim grid As DataGrid = CType(control, DataGrid)
                grid.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.ImageButton)) Then
                Dim imgBtn As ImageButton = CType(control, ImageButton)
                imgBtn.Enabled = Enabled
            ElseIf type.Equals(GetType(DevExpress.Web.ASPxTabControl.ASPxPageControl)) Then
                Dim pageCtrl As ASPxPageControl = CType(control, ASPxPageControl)
                pageCtrl.Enabled = Enabled
            ElseIf type.Equals(GetType(System.Web.UI.WebControls.LinkButton)) Then
                Dim lnkBtn As LinkButton = CType(control, LinkButton)
                lnkBtn.Enabled = Enabled
            End If

        Next
    End Sub

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetAccessControlDac() As AccessControlDac
        If (theAccessControlDac Is Nothing) Then
            theAccessControlDac = New AccessControlDac()
        End If

        Return theAccessControlDac
    End Function

End Class
