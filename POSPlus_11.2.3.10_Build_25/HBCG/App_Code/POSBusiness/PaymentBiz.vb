﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class PaymentBiz

    Private thePaymentDac As PaymentDac

    ''' <summary>
    ''' Finds out if the session passed in, contains finance payments newly keyed
    ''' </summary>
    ''' <param name="sessionId">the session to check the payments for</param>
    ''' <returns>true if FInance exists; false otherwise</returns>
    Public Function HasFinancePayment(ByVal sessionId As String) As Boolean
        Return GetPaymentDac().HasFinancePayment(sessionId)
    End Function

    ''' <summary>
    ''' Retrieves from the CRM.PAYMENT table, the finance payment and its details
    ''' for the sessionID passed in. The Deposit finances (DP) are ignored
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve finance payments for</param>
    ''' <returns>a datatable with the non-DP finance payment found; or an empty table, if none</returns>
    Public Function GetNonDPFinanceDetails(ByVal sessionId As String) As DataTable
        Return GetPaymentDac().GetNonDPFinanceDetails(sessionId)
    End Function

    ''' <summary>
    ''' Retrieves from the AR_TRN table the corresponding account number for the document passed in
    ''' </summary>
    ''' <param name="delDoc">the document number to find the finance account number for</param>
    ''' <param name="ordTp">type of document.  Refer to AppConstants.Order.Type for valid types</param>
    ''' <returns>the finance account number or an empty string if none found</returns>
    Public Function GetFinanceAccount(ByVal delDoc As String, Optional ordTp As String = AppConstants.Order.TYPE_SAL) As String
        Return GetPaymentDac().GetFinanceAccount(delDoc, ordTp)
    End Function

    ''' <summary>
    ''' Finds out if the credit card number passed in is valid.   It validates against the tables
    ''' ASP_MEDIUM_FORMAT and ASP_MEDIUM2MOP
    ''' </summary>
    ''' <param name="storeCd">the store code to use in the validation</param>
    ''' <param name="mopCd">the method of payment code</param>
    ''' <param name="cardNum">the credit card number</param>
    ''' <returns>TRUE if the card is valid; FALSE otherwise</returns>
    ''' <remarks></remarks>
    Public Function IsValidCreditCard(ByVal storeCd As String, ByVal mopCd As String, ByVal cardNum As String) As Boolean
        Return GetPaymentDac().IsValidCreditCard(storeCd, mopCd, cardNum)
    End Function

    ''' <summary>
    ''' Deletes from the PAYMENT table the row that matches the identifier passed in.
    ''' </summary>
    ''' <param name="rowSeqNum">unique row identifier for the row that will be removed</param>
    ''' <remarks>rowSeqNum is the one found in ROW_ID column within the PAYMENT table</remarks>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayment(ByVal rowSeqNum As String) As Integer
        Return GetPaymentDac().DeletePayment(rowSeqNum)
    End Function

    ''' <summary>
    ''' Deletes from the PAYMENT table the rows that match the sessionId passed in.
    ''' </summary>
    ''' <param name="sessionId">sessionId for which to remove the payments</param>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayments(ByVal sessionId As String) As Integer
        Return GetPaymentDac().DeletePayments(sessionId)
    End Function

    ''' <summary>
    ''' Deletes from the PAYMENT table the rows that match the sessionId passed in.
    ''' </summary>
    ''' <param name="sessionId">sessionId for which to remove the payments</param>
    ''' <param name="dbCmd">oracle database command</param>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayments(ByVal sessionId As String, ByVal dbCmd As OracleCommand) As Integer
        Return GetPaymentDac().DeletePayments(sessionId, dbCmd)
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetPaymentDac() As PaymentDac
        If (thePaymentDac Is Nothing) Then
            thePaymentDac = New PaymentDac()
        End If

        Return thePaymentDac
    End Function


End Class
