﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Public Class SKUBiz

    Private theSkuDac As SKUDac

    ''' <summary>
    ''' Retrieves the correct price to be used for the ITEM passed in
    ''' </summary>
    ''' <param name="homeStoreCd">the home store of user logged in, required when no store code is present</param>
    ''' <param name="wrtnStoreCd">the store code where the item is being entered</param>
    ''' <param name="custTpPrCd">the customer type price code (when available)</param>
    ''' <param name="itemCd">the ITEM code for which pricing is being retrieved</param>
    ''' <param name="retPrice">the current price of the ITEM, as found in the ITM table</param>
    ''' <returns>the correct Retail Price for this ITEM</returns>
    ''' <remarks>this API call, takes into account the Calendars in place for the SKU</remarks>
    Public Function GetCurrentPricing(ByVal homeStoreCd As String,
                                      ByVal wrtnStoreCd As String,
                                      ByVal custTpPrCd As String,
                                      ByVal effDate As String,
                                      ByVal itemCd As String,
                                      ByVal itemRetPrice As Double) As Double
        Dim currentPrice As Double = itemRetPrice
        If (wrtnStoreCd.isNotEmpty()) Then
            currentPrice = GetSkuDac().GetCurrentPricing(wrtnStoreCd, custTpPrCd, effDate, itemCd, itemRetPrice)

        ElseIf (homeStoreCd.isNotEmpty()) Then
            currentPrice = GetSkuDac().GetCurrentPricing(homeStoreCd, custTpPrCd, effDate, itemCd, itemRetPrice)
        End If

        Return currentPrice
    End Function

    ''' <summary>
    ''' Check the price change for the releationship price to the current price before convert to order
    ''' </summary>
    ''' <param name="delDocNum"></param>
    ''' <param name="homeStoreCd"></param>
    ''' <param name="wrtnStoreCd"></param>
    ''' <param name="custTpPrCd"></param>
    ''' <param name="effDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CheckPriceChange(ByVal delDocNum As String, ByVal homeStoreCd As String, ByVal custTpPrCd As String, ByVal effDate As String) As Dictionary(Of String, Double)
        Dim dictCurrentPrice As Dictionary(Of String, Double)
       If (homeStoreCd.isNotEmpty()) Then
            dictCurrentPrice = GetSkuDac().CheckPriceChange(delDocNum, homeStoreCd, custTpPrCd, effDate)
        End If
        Return dictCurrentPrice
    End Function

    ''' <summary>
    ''' Filters the list of item codes passed in, to only those that are sellable
    ''' which means their flag PREVENT_SALE = N
    ''' </summary>
    ''' <param name="itemCds">a list of item codes</param>
    ''' <returns>a list with the remaining item codes, of the ones still sellable</returns>
    ''' <remarks></remarks>
    Public Function GetSellableItems(ByVal itemCds As List(Of String)) As List(Of String)
        Return GetSkuDac().GetSellableItems(itemCds)
    End Function

    ''' <summary>
    ''' Finds out if the item passed is allowed to be sold and placed on an order. 
    ''' To NOT be allowed for sale, means that its dropped date is before the current date
    ''' and the PREVENT_SALE flag = 'Y' 
    ''' An item can be sold even if dropped, as long as the PREVENT_SALE flag = 'N'
    ''' </summary>
    ''' <param name="itmCd">the item to find if sellable or not</param>
    ''' <returns>TRUE if item can be sold; FALSE otherwise</returns>
    Public Function IsSellable(ByVal itmCd As String) As Boolean

        If (String.IsNullOrEmpty(itmCd)) Then
            Return False
        Else
            Return GetSkuDac().IsSellable(itmCd)
        End If
    End Function

    ''' <summary>
    ''' Fetches the Related Items for the list of item codes passed in.
    ''' </summary>
    ''' <param name="items">the items to retrieve Related items for</param>
    ''' <param name="storeCd">store where items are being processed, needed to get correct SPIFF value</param>
    ''' <returns>A datatable containing the Item details for all the Related Items found</returns>
    Public Function GetRelatedItems(ByVal items() As String, ByVal storeCd As String) As DataTable
        Return GetSkuDac().GetRelatedItemsDetail(items, storeCd)
    End Function

    ''' <summary>
    ''' Gets the ITEM details for the each of the ITEMS passed in the array of strings.
    ''' The StoreCd is required, if the System Parameter for the SPIFF is set to Store.
    ''' </summary>
    ''' <param name="items">Array of strings, where each element represets an item code</param>
    ''' <param name="storeCd">store where items are being processed, needed to get correct SPIFF value</param>
    ''' <returns>A datatable containing the Item details for all the Items passed in</returns>
    ''' <remarks>IMPORTANT: ITEMS ARE NOT VALIDATED.  IT IS ASSUMED THEY EXIST IN E1 ALREADY</remarks>
    Public Function GetItemInfo(ByVal items() As String, ByVal storeCd As String) As DataTable

        If (SysPms.isSpiffBySKU OrElse (Not SysPms.isSpiffBySKU AndAlso storeCd.isNotEmpty())) Then
            Return GetSkuDac().GetItemInfo(items, storeCd)
        Else
            Return (New DataTable())
        End If
    End Function

    ''' <summary>
    ''' Retrieves from the RELATIONSHIP_LINES table, the SKUs that are found to be dropped for the 
    ''' relationship number passed in.
    ''' The custom object returned, includes the number of dropped SKUs, as well as the number of those that
    ''' have the PREVENT_FLAG = Y as well as the PREVENT_PO = Y
    ''' </summary>
    ''' <param name="relNum">the relationship number to fetch the dropped SKUs for</param>
    ''' <returns>a custom object with details of the dropped SKUs</returns>
    Public Function GetDroppedItemsForRelationship(ByVal relNum As String) As DroppedSkuResponseDtc
        'retrieves the data from the Database
        Dim droppedSkus As DataTable = GetSkuDac().GetDroppedItemsForRelationship(relNum)
        'gathers counts using the table of dropped SKUs found above
        Return GetDroppedItemDetails(droppedSkus)
    End Function

    ''' <summary>
    ''' Retrieves from the TEMP_ITM table, the SKUs that are found to be dropped for the Session ID passed in.
    ''' The custom object returned, includes the number of dropped SKUs, as well as the number of those that
    ''' have the PREVENT_FLAG = Y as well as the PREVENT_PO = Y
    ''' </summary>
    ''' <param name="sessionId">the session id to fetch the dropped SKUs for</param>
    ''' <returns>a custom object with details of the dropped SKUs</returns>
    Public Function GetDroppedItemsForSession(ByVal sessionId As String) As DroppedSkuResponseDtc
        'retrieves the data from the Database
        Dim droppedSkus As DataTable = GetSkuDac().GetDroppedItemsForSession(sessionId)
        'gathers counts using the table of dropped SKUs found above
        Return GetDroppedItemDetails(droppedSkus)
    End Function

    ''' <summary>
    ''' Extracts and returns the  Sort codes in comma seperated string
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>Dataset</returns>
    Public Function GetItemSortCodes(ByVal itemcds As ArrayList)

        Dim delimitedItems As String
        Dim ds As New DataSet
        Try
            ds = GetSkuDac().GetItemSortCodes(itemcds)
        Catch ex As Exception
            Throw
        Finally
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  Sort codes in comma seperated string
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>string</returns>
    Public Function GetItemSortCodesInString(ByVal itemcds As ArrayList)

        Dim itmSortCodes As DataSet = New DataSet()
        Dim SortCodes As StringBuilder = New StringBuilder()
        itmSortCodes = GetItemSortCodes(itemcds)
        For Each element In itmSortCodes.Tables(0).Rows
            SortCodes.Append(element(0).ToString()).Append(" - ").Append(element(1).ToString()).Append(Environment.NewLine())
        Next
        Return SortCodes
    End Function

    ''' <summary>
    ''' Extracts and returns the  items details Item table with it's sort codes
    ''' </summary>
    ''' <param name="itms">Itmems </param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetItemDetailsWithSortCodes(ByVal itms As ArrayList)

        Dim ds As New DataSet
        ds = GetSkuDac().GetItemDetailsWithSortCodes(itms)
        Return ds

    End Function

    ''' <summary>
    ''' Extracts and returns the  items details Item table with it's sort codes
    ''' </summary>
    ''' <param name="itms">Itmems </param>
    ''' <param name="storeCd">the store to be used for SPIFF retrieval</param>
    ''' <returns>Dataset</returns>
    Public Function GetItemDetailsWithSortCodesForSOM(ByVal itms As ArrayList, ByVal storeCd As String)

        Dim ds As New DataSet
        ds = GetSkuDac().GetItemDetailsWithSortCodesForSOM(itms, storeCd)
        Dim sortCode As String
        'replace repeated comma's in the string
        For Each row As DataRow In ds.Tables(0).Rows
            sortCode = StringUtils.EliminateCommas(row("SortCode").ToString())
            row("SortCode") = sortCode
            sortCode = StringUtils.EliminateCommas(row("SortDescription").ToString())
            row("SortDescription") = sortCode
        Next
        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  Sort codes only in comma seperated string
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>string</returns>
    ''' <remarks></remarks>
    Public Function GetItemSortCodesCSV(ByVal itemcds As ArrayList)

        Dim itmSortCodes As DataSet = New DataSet()
        Dim SortCodes As StringBuilder = New StringBuilder()
        itmSortCodes = GetItemSortCodes(itemcds)
        For Each element In itmSortCodes.Tables(0).Rows
            SortCodes.Append(element(0).ToString()).Append(", ")
        Next
        Return StringUtils.EliminateCommas(SortCodes.ToString())
    End Function

    ' ''' <summary>
    ' ''' Extracts and returns the Not Prevented SKU and Prevented SKUs
    ' ''' </summary>
    ' ''' <param name="itm_CD">sku</param>
    Public Function GetPreventedValidandInvalidItemsForsalesExchange(ByVal sku As String, ByRef validSKU As String, ByRef inValidSKU As String, ByRef totalInvalidSKUCount As Integer)

        Dim ds As New DataSet
        Dim valid As New StringBuilder()
        Dim inValid As New StringBuilder()
        Dim validCount As Integer = 0
        Dim inValidCount As Integer = 0
        ds = GetSkuDac().GetPreventedValidandInvalidItemsForsalesExchange(sku)
        If Not ds Is Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("PREVENT").ToString = "Y" Then
                        If inValidCount > 0 Then
                            inValid.Append(", ")
                        End If
                        inValid.Append(row("itm_cd").ToString)
                        inValidCount = inValidCount + 1
                    Else
                        If validCount > 0 Then
                            valid.Append(", ")
                        End If
                        valid.Append(row("itm_cd").ToString)
                        validCount = validCount + 1
                    End If
                Next
            End If
        End If
        validSKU = valid.ToString
        inValidSKU = inValid.ToString
        totalInvalidSKUCount = inValidCount
    End Function


    ''' ************************************************************
    ''' ***********  PRIVATE HELPER METHODS BELOW ******************
    ''' ************************************************************

    ''' <summary>
    ''' Gather counts from the table of dropped SKUs that is passed in.
    ''' </summary>
    ''' <param name="droppedSkus">a table of dropped SKUs</param>
    ''' <returns>a custom object with details of the dropped SKUs</returns>
    Private Function GetDroppedItemDetails(ByVal droppedSkus As DataTable) As DroppedSkuResponseDtc

        Dim rtnVal As New DroppedSkuResponseDtc
        Dim prevSaleCnt As Integer = 0
        Dim prevPoCnt As Integer = 0
        Dim prevSaleSkus As String = String.Empty
        Dim prevPoSkus As String = String.Empty

        For Each dRow As DataRow In droppedSkus.Rows

            If ("Y" = UCase(dRow("PREVENT_SALE"))) Then
                prevSaleCnt = prevSaleCnt + 1
                prevSaleSkus = prevSaleSkus + dRow("ITM_CD") + ", "
            End If

            If ("Y" = UCase(dRow("PREVENT_PO"))) Then
                prevPoCnt = prevPoCnt + 1
                prevPoSkus = prevPoSkus + dRow("ITM_CD") + ", "
            End If
        Next
        rtnVal.totalCount = droppedSkus.Rows.Count
        rtnVal.prevSaleCount = prevSaleCnt
        rtnVal.prevPOCount = prevPoCnt
        rtnVal.prevSaleSkus = If(prevSaleSkus.isNotEmpty(), Left(prevSaleSkus, prevSaleSkus.Trim.Length - 1), prevSaleSkus)
        rtnVal.prevPOSkus = If(prevPoSkus.isNotEmpty(), Left(prevPoSkus, prevPoSkus.Trim.Length - 1), prevPoSkus)

        Return rtnVal
    End Function

    ''' <summary>
    ''' Getting the input item details from item table
    ''' </summary>
    ''' <param name="items">Multiple SKUs as a String Array</param>
    ''' <returns>Data Table of the items detail</returns>
    ''' <remarks></remarks>
    Public Function GetItemsDetail(items As String()) As DataTable
        Return GetSkuDac().GetItemsDetail(items)
    End Function

    Private Function GetSkuDac() As SKUDac
        If (theSkuDac Is Nothing) Then
            theSkuDac = New SKUDac()
        End If

        Return theSkuDac
    End Function

End Class

