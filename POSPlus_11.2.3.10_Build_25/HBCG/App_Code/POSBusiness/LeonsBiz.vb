﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Data.OracleClient
Imports World_Gift_Utils
Imports HBCG_Utils

Imports SD_Utils

'Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp

Imports System.Data
Imports System
Imports ErrorManager
Imports System.Web
Imports System.IO
Imports AppExtensions
Imports ca.leons.www
Imports SessionIntializer


Public Class LeonsBiz

    Public Function get_term_id(ByVal p_strIp As String) As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_term_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_pos.getTerminal"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_ip", OracleType.Char)).Value = p_strIp
        myCMD.Parameters.Add("p_terminal", OracleType.VarChar, 20).Direction = ParameterDirection.Output

        myCMD.ExecuteNonQuery()

        str_term_id = myCMD.Parameters("p_terminal").Value

        If isNotEmpty(str_term_id) Then
            str_term_id.ToLower()
        End If

        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try

            MyDA.Fill(Ds)
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()

            Return str_term_id

        Catch x
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
            Return ""
        End Try
    End Function

    Public Function GetIPAddress() As String
        Dim strHostName As String

        Dim strIPAddress As String

        strHostName = System.Net.Dns.GetHostName()

        strIPAddress = System.Net.Dns.GetHostEntry(strHostName).AddressList(0).ToString()

        Return strIPAddress

    End Function

    Public Function GetCustomerName(ByVal custCd As String) As String
        Dim custName As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT CUST_CD, FNAME, LNAME FROM CUST WHERE CUST_CD=upper(:CUST_CD)"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = custCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                custName = dbReader.Item("LNAME").ToString & " " & dbReader.Item("CUST_CD").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return custName
    End Function

    Public Function GetPaymentStore(ByVal ipAdress As String) As String
        Dim pmtStore As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT STORE_CD FROM GP_PINPAD_MAPPING WHERE PC_IPADDR=:PC_IPADDR"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":PC_IPADDR", OracleType.VarChar)
            dbCommand.Parameters(":PC_IPADDR").Value = ipAdress
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                pmtStore = dbReader.Item("STORE_CD").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return pmtStore
    End Function


    Public Shared Function GetEmpInit(ByVal p_emp_cd As String) As String
        Dim str_emp_init As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT emp_init FROM emp WHERE emp_cd=upper(:p_emp_cd)"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            dbCommand.Parameters(":p_emp_cd").Value = p_emp_cd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                ' Daniela Sales Order fix
                ' str_emp_init = dbReader.Item("emp_init").ToString & " " & dbReader.Item("emp_init").ToString
                str_emp_init = dbReader.Item("emp_init").ToString
            Else
                Return ""
            End If
            Return str_emp_init
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception

            Throw ex
        Finally
            Try
                dbReader.Close()
                dbCommand.Dispose()
                dbConnection.Close()
                dbConnection.Dispose()
            Catch
            End Try
        End Try
    End Function

    Public Shared Function GetEmpTpCd(ByVal p_emp_cd As String, ByVal p_tp_cd As String) As String
        Dim str_emp_tp As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select EMP_TP_CD from EMP$EMP_TP where emp_cd = :p_emp_cd and EMP_TP_CD = :p_tp_cd and rownum = 1"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":p_emp_cd", OracleType.VarChar)
            dbCommand.Parameters(":p_emp_cd").Value = p_emp_cd.ToUpper
            dbCommand.Parameters.Add(":p_tp_cd", OracleType.VarChar)
            dbCommand.Parameters(":p_tp_cd").Value = p_tp_cd.ToUpper
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                str_emp_tp = dbReader.Item("EMP_TP_CD").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return str_emp_tp
    End Function

    Public Shared Function GetStorePilot(ByVal p_store_cd As String) As String
        Dim isPilot As String = ""
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select STD_TENDERRETAIL_UTIL.isPosPilot(:p_store_cd) is_pilot from dual"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":p_store_cd", OracleType.VarChar)
            dbCommand.Parameters(":p_store_cd").Value = p_store_cd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                isPilot = dbReader.Item("is_pilot")
            Else
                Return "N"
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return isPilot
    End Function

    Public Sub leons_insert_cmnt(ByRef p_transDt As String, ByRef p_storeCd As String, ByRef p_strSO_SEQ_NUM As String, ByRef p_lngSEQNUM As Integer, ByRef p_lngComments As String, ByRef p_str_del_doc_num As String, ByRef p_commentType As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.insSoCmnt"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_so_wr_dt", OracleType.Char)).Value = p_transDt
        myCMD.Parameters.Add(New OracleParameter("p_so_store_cd", OracleType.Char)).Value = p_storeCd
        myCMD.Parameters.Add(New OracleParameter("p_so_seq_num", OracleType.Char)).Value = p_strSO_SEQ_NUM
        myCMD.Parameters.Add(New OracleParameter("p_seq", OracleType.Char)).Value = p_lngSEQNUM
        myCMD.Parameters.Add(New OracleParameter("p_txt", OracleType.Char)).Value = p_lngComments
        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.Char)).Value = p_str_del_doc_num
        myCMD.Parameters.Add(New OracleParameter("p_perm", OracleType.Char)).Value = ""
        myCMD.Parameters.Add(New OracleParameter("p_cmnt_tp", OracleType.Char)).Value = p_commentType.Trim
        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.Char)).Value = ""
        myCMD.Parameters.Add(New OracleParameter("p_origcd", OracleType.Char)).Value = ""

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    Public Shared Function GetskuCompanyCode(ByVal p_sku As String)
        'sabrina add new function

        Dim myind As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_multi_co.isBrickItm"
        sycmd.CommandType = CommandType.StoredProcedure

        sycmd.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
        sycmd.Parameters("p_itm").Size = 10
        sycmd.Parameters.Add("p_ind", OracleType.VarChar).Direction = ParameterDirection.ReturnValue
        sycmd.Parameters("p_ind").Size = 1

        sycmd.Parameters.Add(New OracleParameter("p_itm", OracleType.VarChar)).Value = p_sku

        Try
            syda.Fill(syds)

        Catch ex As Exception
            Throw New Exception("Error in isBrickItm ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
                conn.Dispose()
            End If
        End Try

        myind = sycmd.Parameters("p_ind").Value.ToString
        sycmd.Cancel()
        sycmd.Dispose()

        If myind = "Y" Then
            Return "BW1"
        Else
            Return "LFL"
        End If

    End Function

    Public Shared Function Find_Merchandise_Images(ByVal SKU As String, ByVal ZipCd As String, ByVal LangCode As String)

        Exit Function

        Find_Merchandise_Images = ""
        'sabrina add
        Dim service As New SKUToUrlServiceSoapClient
        service.Open()

        Try ' Daniela add try

            Dim output As New ProductResponse
            Dim args As New ProductRequest
            Dim mylist As New ArrayOfString

            If SKU = "BRKLAMP" Or SKU = "BRKLAMP2" Or SKU = "BRICK_L" Then
                Find_Merchandise_Images = "~/merch_images/box.gif"
                'Daniela close
                service.Close()
                Return Find_Merchandise_Images
                Exit Function

            Else
                mylist.Add(SKU)
                args.SKUArray = mylist
                args.PostalCode = ZipCd
                args.ReturnFrenchItems = LangCode
                args.CompanyCode = GetskuCompanyCode(SKU)

                output = service.GetProductsFromSKUsPostalCode(args)

                'For Each Product In output.ProductList
                If Not IsNothing(output.ProductList(0).ImageUrl) Then
                    Find_Merchandise_Images = output.ProductList(0).ImageUrl
                    'Daniela close
                    service.Close()
                    Return Find_Merchandise_Images
                    Exit Function
                End If
            End If

            service.Close()
            ' Daniela add try
        Catch ex As Exception
        Finally
            ' Do nothing if errors
            service.Close()
        End Try

    End Function

    ''' <summary>
    ''' Retrieves the list of all active/non-expired discounts in the system.
    ''' </summary>
    ''' <returns>a dataset containing the list of all the discounts in the system.</returns> 
    Public Function GetDiscounts(ByVal coCd As String) As DataTable
        Return GetDiscounts(SysPms.allowMultDisc, coCd)
    End Function

    Public Function GetDiscounts(ByVal includeLineLevel As Boolean, ByVal coCd As String) As DataTable

        Dim discDataSet As New DataSet
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT DISC_CD AS CD, DISC_CD || ' - ' || DES AS DES, TP_CD, NVL(AMT,0) AS AMT, ")
        sql.Append("DISC_LEVEL, BY_MNR, COND_DISC, APPLY_PACKAGE FROM DISC a ")
        sql.Append("WHERE exists ( select 1 ")
        sql.Append("from   co_grp$disc ")
        sql.Append("where  co_grp_cd  in (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & coCd & "'),co_cd)) ")
        sql.Append("and    disc_cd     =  a.disc_cd) ")
        sql.Append("and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ")
        If (Not includeLineLevel) Then
            sql.Append(" AND DISC_LEVEL <> 'L' ")
        End If
        sql.Append("ORDER BY DES")
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql.ToString, dbConnection)
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discDataSet)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
            dbConnection.Dispose()
        End Try

        Return discDataSet.Tables(0)
    End Function

    Public Function sec_check_itm(ByRef p_itm_cd As String, ByRef p_user As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isValidItm2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_itm", OracleType.VarChar)).Value = p_itm_cd
        myCMD.Parameters.Add(New OracleParameter("p_empcd", OracleType.VarChar)).Value = p_user
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_y_n").Value) = False Then
                str_y_n = myCMD.Parameters("p_y_n").Value
            End If
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
            Return str_y_n
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Shared Function GetGroupCode(ByVal coCd As String) As String
        Dim grpCd As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select CO_GRP_CD from co_grp where co_cd = upper(:CO_CD)"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
            dbCommand.Parameters(":CO_CD").Value = coCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                grpCd = dbReader.Item("CO_GRP_CD").ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try
        Return grpCd
    End Function

    Public Shared Function lucy_check_store(ByRef p_emp_cd As String, ByRef p_store_cd As String, ByRef p_valid_flag As String)
        ' Lucy 
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isvalidstore2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_store_cd", OracleType.VarChar)).Value = p_store_cd
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                p_valid_flag = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_valid_flag

    End Function
    Public Shared Sub lucy_check_payment(ByRef p_yn As String, ByRef p_session_id As String)
        'lucy
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        Try
            ' Daniela added TDF and GC mop code
            sql = "select mop_cd from payment where sessionid='" & p_session_id & "'"
            sql = sql & " and (mop_cd = 'VI' or (mop_cd='FI'  and FI_ACCT_NUM is not null  and APPROVAL_CD is not null ) or mop_cd='MC' or mop_cd='FV' or mop_cd='AMX' or mop_cd='DC' or mop_cd = 'TDF' or mop_cd = 'GC')"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()

            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                p_yn = "NO"

            Else

                p_yn = "YES"

            End If
        Catch x
            Throw
        Finally
            MyDataReader.Close()
            MyDataReader.Dispose()
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Public Shared Function lucy_check_itm(ByRef p_itm_cd As String, ByRef p_user As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isValidItm2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_itm", OracleType.VarChar)).Value = p_itm_cd
        myCMD.Parameters.Add(New OracleParameter("p_empcd", OracleType.VarChar)).Value = p_user
        myCMD.Parameters.Add("p_y_n", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try
            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_y_n").Value) = False Then
                str_y_n = myCMD.Parameters("p_y_n").Value
            End If
        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return str_y_n
    End Function

    Public Shared Function lucy_check_sup_user(ByRef p_emp_cd As String, ByRef p_co_grp_cd As String, ByRef p_co_cd As String, ByRef p_sup_flag As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add("p_co_grp_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_co_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_co_grp_cd").Value) = False Then
                p_co_grp_cd = myCMD.Parameters("p_co_grp_cd").Value
            End If

            If IsDBNull(myCMD.Parameters("p_co_cd").Value) = False Then
                p_co_cd = myCMD.Parameters("p_co_cd").Value
            End If

            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                p_sup_flag = myCMD.Parameters("p_ind").Value
            End If

        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_sup_flag

    End Function
    Public Shared Function lucy_Query_String_Prepare(ByVal query_val As String)
        'lucy 26-mar-15
        lucy_Query_String_Prepare = ""
        Dim x As Integer
        For x = 1 To Len(query_val)
            If Mid(query_val, x, 1) = "%" Then
                lucy_Query_String_Prepare = lucy_Query_String_Prepare & Mid(query_val, x, 1)
            Else
                lucy_Query_String_Prepare = query_val
                ' lucy_Query_String_Prepare = lucy_Query_String_Prepare & Regex.Replace(Mid(query_val, x, 1), "\D", String.Empty)
            End If
        Next

        Return lucy_Query_String_Prepare

    End Function
    Public Shared Function lucy_RetrieveSalespersons(ByRef p_emp_cd As String) As DataSet
        'lucy copied from RetrieveSalespersons()

        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        Dim str_co_grp_cd As String
        Dim str_co_cd As String
        Dim str_sup_user_flag As String
        Dim str_flag As String

        str_flag = lucy_check_sup_user(UCase(p_emp_cd), str_co_grp_cd, str_co_cd, str_sup_user_flag) 'use the function even not check super user
        '  If str_co_grp_cd = "BRK" Then   '  the Brick    ,as per John Luft, 17-oct-14, the same for Leon and Corp

        'sql = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
        '   "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
        '  "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
        '  "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') " &
        '  "AND emp.home_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd= :str_co_grp_cd)) " &
        '  "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY FullNAME"

        '  cmd.Parameters.Add(":str_co_grp_cd", OracleType.VarChar)
        ' cmd.Parameters(":str_co_grp_cd").Value = UCase(str_co_grp_cd)
        '  Else    'Leon, only select the sales person from the same company

        ' Daniela add EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') as per Inn Dec 23
        sql = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
             "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
             "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
             "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
              "AND emp.home_store_cd in (select store_cd from store where co_cd = :str_co_cd) " &
              "and EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') " &
             "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') ORDER BY FullNAME"

        cmd.Parameters.Add(":str_co_cd", OracleType.VarChar)
        cmd.Parameters(":str_co_cd").Value = UCase(str_co_cd)
        '  End If

        ' end lucy add

        Try
            With cmd
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw
        Finally
            cmd.Cancel()
            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return ds
    End Function

    Public Shared Function lucy_RetrieveSalespersons(ByRef p_emp_cd As String, ByRef p_co_cd As String) As DataSet
        'Franchise changes

        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        ' Daniela add EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') as per Inn Dec 23
        sql = "SELECT EMP.HOME_STORE_CD || ' ' || EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
             "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
             "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
             "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
              "AND emp.home_store_cd in (select store_cd from store where co_cd = :str_co_cd) " &
              "and EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') " &
             "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') ORDER BY FullNAME"

        cmd.Parameters.Add(":str_co_cd", OracleType.VarChar)
        cmd.Parameters(":str_co_cd").Value = UCase(p_co_cd)

        Try
            With cmd
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw
        Finally
            cmd.Cancel()
            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return ds
    End Function

    Public Shared Function lucy_RetrieveSalespersons(ByRef p_emp_cd As String, ByRef p_co_cd As String, ByRef ord_tp_cd As String) As DataSet
        'Franchise changes
        'Alice updated on May 10,2019 for request 81 to add EMP_SLSP.SLSP_DEPT_CD <> 'CSR' to sql query

        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String

        If Not AppConstants.Order.TYPE_SAL.Equals(ord_tp_cd) Then

            '' Daniela add EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') as per Inn Dec 23
            'sql = "SELECT EMP.HOME_STORE_CD || ' ' || EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
            '     "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
            '     "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
            '     "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
            '     "AND EMP.EMP_CD=:emp_cd " &
            '      "And EMP_SLSP.slsp_dept_cd Not In ('DRIVER','HELPER') " &
            '     "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') "

            'cmd.Parameters.Add(":emp_cd", OracleType.VarChar)
            'cmd.Parameters(":emp_cd").Value = UCase(p_emp_cd)

            sql = "SELECT EMP.HOME_STORE_CD || ' ' || EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
            "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
            "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
            "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
             "AND emp.home_store_cd in (select store_cd from store where co_cd = :str_co_cd) " &
             "and EMP_SLSP.slsp_dept_cd not in ('DRIVER','HELPER') " &
            "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
            "ORDER BY FullNAME"

            cmd.Parameters.Add(":str_co_cd", OracleType.VarChar)
            cmd.Parameters(":str_co_cd").Value = UCase(p_co_cd)

        Else
            ' Daniela add EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') as per Inn Dec 23
            sql = "SELECT EMP.HOME_STORE_CD || ' ' || EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD " &
                 "FROM EMP, EMP_SLSP, EMP$EMP_TP " &
                 "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD " &
                 "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
                  "AND emp.home_store_cd in (select store_cd from store where co_cd = :str_co_cd) " &
                  "and EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') " &
                 "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') " &
                 "ORDER BY FullNAME"

            cmd.Parameters.Add(":str_co_cd", OracleType.VarChar)
            cmd.Parameters(":str_co_cd").Value = UCase(p_co_cd)
        End If





        Try
            With cmd
                .Connection = conn
                .CommandText = sql
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw
        Finally
            cmd.Cancel()
            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return ds
    End Function


    Public Shared Function lucy_invent_itm(ByRef p_itm_cd As String) As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()


        sql = "select inventory from itm where itm_cd='" & UCase(p_itm_cd) & "' and inventory='Y' "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                ' lucy_lbl.Text = "Please clear the sku and click on Add Item to scan a RFID"
                Return "Y"

            Else

                Return "N"
                ' lucy_lbl.Text = "Please Click on Add Item to scan a RFID"
            End If
            'MyDataReader.Close()

        Catch
            Throw

        Finally
            MyDataReader.Close()
            conn.Close()
            conn.Dispose()
        End Try


    End Function

    Public Shared Function GetPostalCode()
        'sabrina add new function

        Dim myinit As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        Try

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
               ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            conn.Open()

            sycmd.Connection = conn
            sycmd.CommandText = "std_tenderretail_util.getZipCd"
            sycmd.CommandType = CommandType.StoredProcedure

            sycmd.Parameters.Add("p_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            sycmd.Parameters("p_cd").Size = 20
            sycmd.Parameters.Add("p_postal_cd", OracleType.VarChar).Direction = ParameterDirection.ReturnValue
            sycmd.Parameters("p_postal_cd").Size = 20

            sycmd.Parameters.Add(New OracleParameter("p_cd", OracleType.VarChar)).Value = HttpContext.Current.Session("EMP_CD").ToString

            syda.Fill(syds)

            myinit = sycmd.Parameters("p_postal_cd").Value.ToString
            sycmd.Cancel()
            sycmd.Dispose()

        Catch ex As Exception
            Throw New Exception("Error in GetPostalCode ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                sycmd.Cancel()
                sycmd.Dispose()
                conn.Close()
                conn.Dispose()
            End If
        End Try

        Return myinit

    End Function

    Public Shared Function GetLangCode()
        'sabrina add new function
        Dim mylang As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        Try

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
               ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            conn.Open()

            sycmd.Connection = conn
            sycmd.CommandText = "std_tenderretail_util.getLang"
            sycmd.CommandType = CommandType.StoredProcedure

            sycmd.Parameters.Add("p_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            sycmd.Parameters("p_cd").Size = 20
            sycmd.Parameters.Add("p_lang_cd", OracleType.VarChar).Direction = ParameterDirection.ReturnValue
            sycmd.Parameters("p_lang_cd").Size = 10

            'sycmd.Parameters.Add(New OracleParameter("p_init", OracleType.VarChar)).Value = HttpContext.Current.Session("EMP_CD").ToString
            sycmd.Parameters.Add(New OracleParameter("p_cd", OracleType.VarChar)).Value = HttpContext.Current.Session("EMP_CD").ToString

            syda.Fill(syds)

            mylang = sycmd.Parameters("p_lang_cd").Value

            sycmd.Cancel()
            sycmd.Dispose()
            conn.Close()
            conn.Dispose()

        Catch ex As Exception
            Throw New Exception("Error in GetLangCode ", ex)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
                conn.Dispose()
            End If
        End Try

        Return mylang

    End Function

    Public Function get_csh_dwr(ByVal p_emp_init As String, ByVal p_co_cd As String) As String

        Dim csh_dwr As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT CSH_DWR_CD FROM cshr_activation WHERE cshr_init ='" & p_emp_init & "'"
        sql = sql & " and co_cd = '" & p_co_cd & "'"
        sql = sql & " and trunc(to_date(post_dt,'DD-MON-RRRR')) = trunc(sysdate) "
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                csh_dwr = dbReader.Item("CSH_DWR_CD")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return csh_dwr
    End Function

    Public Function get_pu_del_store(ByVal pd_store As String, ByVal pd_zone As String, ByVal str_co_cd As String) As String
        Dim pu_del_store As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        ' check if the pd_store is right for the pd_zone
        Dim sql As String = "SELECT delivery_store_cd FROM zone WHERE delivery_store_cd=upper(:pd_store) " &
            "and ZONE.ZONE_CD = :pd_zone " &
            "AND zone.delivery_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd =:str_co_cd)))"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":pd_store", OracleType.VarChar)
            dbCommand.Parameters(":pd_store").Value = pd_store
            dbCommand.Parameters.Add(":pd_zone", OracleType.VarChar)
            dbCommand.Parameters(":pd_zone").Value = pd_zone
            dbCommand.Parameters.Add(":str_co_cd", OracleType.VarChar)
            dbCommand.Parameters(":str_co_cd").Value = str_co_cd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                pu_del_store = pd_store
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try

        ' if the pd_store is not right for the pd_zone, get a right one
        If isEmpty(pu_del_store) Then
            ' Dim str_store_cd As String = Session("HOME_STORE_CD")

            sql = "SELECT min(delivery_store_cd) as delivery_store_cd  FROM zone WHERE zone_cd=upper(:pd_zone)" &
                 "AND zone.delivery_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd =:str_co_cd)))"
            Try
                dbConnection.Open()
                dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                dbCommand.Parameters.Add(":pd_zone", OracleType.VarChar)
                dbCommand.Parameters(":pd_zone").Value = pd_zone
                dbCommand.Parameters.Add(":str_co_cd", OracleType.VarChar)
                dbCommand.Parameters(":str_co_cd").Value = str_co_cd
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If dbReader.Read() Then
                    pu_del_store = dbReader.Item("delivery_store_cd").ToString
                Else

                    Return pd_store
                End If
                dbReader.Close()
                dbCommand.Dispose()
            Catch ex As Exception
                Throw ex
            Finally
                dbConnection.Close()
            End Try
        End If
        Return pu_del_store
    End Function
    Public Function isLeonsUser(ByVal p_co_cd As String) As String

        'sabrina R1930 (new function)
        'Dnaiela Nov 6 improve performance
        'Dim co_ind As String = String.Empty

        'Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        'Dim dbCommand As OracleCommand
        'Dim dbReader As OracleDataReader

        'Dim sql As String = "SELECT s.CO_CD as CO_CD FROM emp e, store s where e.home_store_cd = s.store_cd "
        'sql = sql & " and e.emp_init = '" & p_emp_init & "'"

        'Try
        '    dbConnection.Open()
        '    dbCommand = DisposablesManager.BuildOracleCommand(Sql, dbConnection)
        '    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

        '    If dbReader.Read() Then
        '        co_ind = dbReader.Item("co_cd")
        '    Else
        '        Return ""
        '    End If
        '    dbReader.Close()
        '    dbCommand.Dispose()
        '    dbConnection.Close()
        'Catch ex As Exception
        '    Throw ex
        'Finally
        '    dbReader.Close()
        '    dbCommand.Dispose()
        '    dbConnection.Close()
        'End Tryhttp://localhost:64873/HBCG/ads.xml

        If p_co_cd = "LFL" Then
            Return "Y"
        Else
            Return "N"
        End If


    End Function


    Public Function isvalidDCSplan(ByVal p_plan As String) As String

        'sabrina R1999 (new function)
        Dim v_ind As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' as valid_ind FROM brick_dcs_plan where plan_cd = '" & p_plan & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("valid_ind")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If


    End Function

    Public Shared Function GetApprovalCode(ByVal delDocNum As String) As String
        Dim apprCode As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select APPROVAL_CD from SO where del_doc_num = :DEL_DOC_NUM"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            dbCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                apprCode = dbReader.Item("APPROVAL_CD").ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try
        Return apprCode
    End Function


    Public Shared Function GetFtpIpAdress(ByVal envName As String) As String
        Dim ipAdr As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select value_1 ip_adress from LEONS_SYSTEM where key = :ENV"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":ENV", OracleType.VarChar)
            dbCommand.Parameters(":ENV").Value = envName
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                ipAdr = dbReader.Item("ip_adress").ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try

        Return ipAdr

    End Function
    Public Shared Function isValidStr2(ByRef p_usr As String, ByRef p_str As String) As String
        'sabrina new function feb-2016
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co2.isValidStr2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_usr", OracleType.VarChar)).Value = p_usr
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return v_ind

    End Function

    ''' <summary>
    ''' Methods to upload file to FTP Server
    ''' </summary>
    ''' <param name="_FileName">local source file name</param>
    ''' <param name="_UploadPath">Upload FTP path including Host name</param>
    ''' <param name="_FTPUser">FTP login username</param>
    ''' <param name="_FTPPass">FTP login password</param>

    Public Sub UploadFile(ByVal _FileName As String, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)
        Dim _FileInfo As New System.IO.FileInfo(_FileName)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile

        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = _FileInfo.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte

        ' Opens a file stream (System.IO.FileStream) to read the file to be uploaded
        Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()

        Try
            ' Stream to which the file to be upload is written
            Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

            ' Read from the file stream 2kb at a time
            Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)

            ' Till Stream content ends
            Do While contentLen <> 0
                ' Write Content from the file stream to the FTP Upload Stream
                _Stream.Write(buff, 0, contentLen)
                contentLen = _FileStream.Read(buff, 0, buffLength)
            Loop

            ' Close the file stream and the Request Stream
            _Stream.Close()
            _Stream.Dispose()
            _FileStream.Close()
            _FileStream.Dispose()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Shared Function GetHomeStore(ByVal p_usr As String) As String

        'sabrina - new R2335
        Dim v_str As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT HOME_STORE_CD from emp " &
                            " where emp_init = '" & p_usr & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_str = dbReader.Item("HOME_STORE_CD")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return v_str

    End Function
    Public Shared Function isQuebecStore(ByVal p_str As String) As String

        'sabrina - new R2335
        Dim v_st As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT ST from store " &
                            " where store_cd = '" & p_str & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_st = dbReader.Item("ST")
            Else
                v_st = ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_st & "" = "" Then
            Return "N"
        ElseIf v_st = "QC" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function

    Public Function ValidatePhoneNumber(ByVal txt As String) As Boolean

        ' Assume the field is valid.
        ValidatePhoneNumber = False

        ' Check short format.
        If (txt Like "###-####") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "###-###-####") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "(###)###-####") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "##########") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "###########") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "############") Then
            ValidatePhoneNumber = True
        End If
        If (txt Like "#######") Then
            ValidatePhoneNumber = True
        End If

    End Function
    Public Shared Function GetStoreCoCode(ByVal p_str As String) As String

        'sabrina - new
        Dim v_co_cd As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT CO_CD from store " &
                            " where store_cd = '" & p_str & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_co_cd = dbReader.Item("CO_CD")
            Else
                v_co_cd = ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_co_cd & "" = "" Then
            Return ""
        Else
            Return v_co_cd
        End If

    End Function
    Public Shared Function isLeonCoGrp(ByRef p_co_grp As String) As String
        'sabrina new 
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_role.isLeonCoGrp"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_coGrp", OracleType.VarChar)).Value = p_co_grp
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return v_ind

    End Function
    Public Shared Function isLeonCorpStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isLeonCorpStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isLeonFranStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isLeonFranStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isLeonCommStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isLeonCommStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isLeonInternetStr(ByVal p_str As String) As String
        'sabrina new function mar2017 - Req2857

        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT count(*) cnt  " &
                            "  from store_grp$store " &
                            " where store_cd = '" & p_str & "' " &
                            "   and store_grp_cd in ('WW') "
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()
        If IsDBNull(v_value) Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Public Shared Function isBrickFranStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isBrickFranStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isBrickCorpStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isBrickCorpStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isBrickCommStr(ByRef p_str As String) As String
        'sabrina new function mar2017 - Req2857
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception
        Dim v_ind As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co3.isBrickCommStr"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.VarChar)).Value = p_str
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                v_ind = myCMD.Parameters("p_ind").Value
            End If
        Catch x
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return v_ind

    End Function
    Public Shared Function isBrickInternetStr(ByVal p_str As String) As String
        'sabrina new function mar2017 - Req2857

        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT count(*) cnt  " &
                            "  from store_grp$store " &
                            " where store_cd = '" & p_str & "' " &
                            "   and store_grp_cd in ('BRI') "
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()
        If IsDBNull(v_value) Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function

    Public Shared Function getUserType(ByVal p_usr As String, ByVal str_co_grp_cd As String,
                                        ByVal str_co_cd As String, ByVal str_sup_user_flag As String,
                                        ByVal home_store_cd As String)

        Dim USER_TYPE As Double = UserTypes.UNDEFINED_USR

        If isNotEmpty(str_sup_user_flag) And str_sup_user_flag = "Y" Then

            Return UserTypes.SUPER_USR

        End If

        If isNotEmpty(str_co_grp_cd) And str_co_grp_cd = "LFS" Then

            Return UserTypes.SHARED_SVS_USR

        End If

        If isNotEmpty(home_store_cd) And home_store_cd = "00" Then

            Return UserTypes.LEONS_HO_USR

        End If

        If isNotEmpty(home_store_cd) And home_store_cd = "10" Then

            Return UserTypes.BRICK_HO_USR

        End If

        Dim userTypeId As Double

        If (isNotEmpty(home_store_cd) And isNotEmpty(str_co_grp_cd)) Then
            userTypeId = getUserTypeByGrp(home_store_cd, str_co_grp_cd)
        End If

        If isNotEmpty(userTypeId) Then
            Return userTypeId
        End If

        ' Return UNDEFINED / should not happen
        Return USER_TYPE

    End Function

    Public Shared Function getUserTypeByGrp(ByVal home_store_cd As String, ByVal str_co_grp_cd As String) As String
        Dim userTp As Double = -1
    
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select user_id from ( " +
                "select b.user_id " +
                "from   store_grp$store a, mccl_user_type b " +
                "where a.store_cd = :home_store_cd and a.store_grp_cd = b.store_grp_cd and co_grp_cd = :str_co_grp_cd " +
                "order by b.store_type_order " +
                ") where rownum < 2 "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":home_store_cd", OracleType.VarChar)
            dbCommand.Parameters(":home_store_cd").Value = home_store_cd
            dbCommand.Parameters.Add(":str_co_grp_cd", OracleType.VarChar)
            dbCommand.Parameters(":str_co_grp_cd").Value = str_co_grp_cd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                userTp = If(IsDBNull(dbReader.Item("user_id")), 0, CDbl(dbReader.Item("user_id")))
            Else
                Return userTp
            End If

        Catch ex As Exception
            Return userTp
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try

        Return userTp

    End Function

    Private Class UserTypes
      
        Public Const SUPER_USR As Integer = 0
        Public Const LEONS_HO_USR As Integer = 1
        Public Const BRICK_HO_USR As Integer = 2
        Public Const LEONS_CORP_STR_USR As Integer = 3
        Public Const BRICK_CORP_STR_USR As Integer = 4
        Public Const LEONS_FR_STR_USR As Integer = 5
        Public Const BRICK_FR_STR_USR As Integer = 6
        Public Const SHARED_SVS_USR As Integer = 7
        Public Const UNDEFINED_USR As Integer = -1
        
    End Class

    ''' <summary>
    ''' Finds out if the store and location passed in, correspond to an Showroom type location
    ''' </summary>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="locCd">the location code</param>
    ''' <returns>TRUE if location is Showroom; FALSE otherwise</returns>
    Public Function IsShowroomLocation(ByVal storeCd As String, ByVal locCd As String) As Boolean

        Dim isShowroom As Boolean = False
        Dim sql As String = "SELECT loc_tp_cd FROM loc WHERE STORE_CD = :storeCd AND LOC_CD = :locCd and pick = :pick and loc_tp_cd = :loc_tp_cd"

        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":pick", OracleType.VarChar)
            dbCommand.Parameters.Add(":loc_tp_cd", OracleType.VarChar)
            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbCommand.Parameters(":locCd").Value = UCase(locCd)
            dbCommand.Parameters(":pick").Value = "N"
            dbCommand.Parameters(":loc_tp_cd").Value = "S"
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                isShowroom = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Return False
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try
        Return isShowroom

    End Function

    Public Function GetCustomerType(ByVal custCd As String, ByVal sessionVal As String) As String
        Dim custType As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT CUST_TP FROM CUST_INFO WHERE CUST_CD=upper(:CUST_CD) and session_id = '" & sessionVal & "' "
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = custCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                custType = dbReader.Item("CUST_TP").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return custType
    End Function
    Public Function GetCustomerType(ByVal custCd As String) As String
        Dim custType As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT CUST_TP_CD FROM CUST WHERE CUST_CD=upper(:CUST_CD)"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = custCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                custType = dbReader.Item("CUST_TP_CD").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return custType
    End Function


    Public Function GetURL(ByVal sku As String) As String
        Dim itmURL As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select image_url from WEB_CACHE where itm_cd = upper(:sku) and (select value_1 from leons_system where key = 'IMG_LK_POS') = 'Y'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":sku", OracleType.VarChar)
            dbCommand.Parameters(":sku").Value = sku
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                itmURL = dbReader.Item("image_url").ToString
            Else
                Return ""
            End If
            'dbReader.Close()
            'dbCommand.Dispose()
        Catch ex As Exception
            'Throw ex
            'do not throw exception
            Return ""
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbConnection.Dispose()
        End Try
        Return itmURL
    End Function
    Public Function ValidateEmail(ByVal emailAddress As String) As Boolean
        Try
            Dim vEmailAddress As New System.Net.Mail.MailAddress(emailAddress)
            If emailAddress.IndexOf("@") > -1 Then
                If (emailAddress.IndexOf(".", emailAddress.IndexOf("@")) > emailAddress.IndexOf("@")) AndAlso emailAddress.Split(".").Length > 0 AndAlso emailAddress.Split(".")(1) <> "" Then
                    Return True
                End If
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Shared Function CheckStoreGroup(ByVal store_grp_cd As String, ByVal store_cd As String) As Boolean

        Dim isStoreGroup As Boolean = False

        Dim sql As String = "select 1 from store_grp$store where store_grp_cd= :store_grp_cd and " +
            " store_cd = :store_cd"

        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":store_grp_cd", OracleType.VarChar)
            dbCommand.Parameters.Add(":store_cd", OracleType.VarChar)
            dbCommand.Parameters(":store_grp_cd").Value = UCase(store_grp_cd)
            dbCommand.Parameters(":store_cd").Value = UCase(store_cd)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                isStoreGroup = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Return False
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
            dbCommand.Dispose()
        End Try
        Return isStoreGroup
    End Function
End Class

