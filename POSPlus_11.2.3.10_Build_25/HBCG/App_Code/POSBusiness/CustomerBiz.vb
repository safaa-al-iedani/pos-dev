﻿Imports Microsoft.VisualBasic

Public Class CustomerBiz

    Private theCustomerDac As CustomerDac

    ''' <summary>
    ''' Reads from the TEMP_ITM table the customer zip code, that corresponds to the customer and
    ''' session id values passed in
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custCd">the customer code to get the zip code for</param>
    ''' <returns>the ZIP code value, or an empty string if none found</returns>
    Public Function GetCustomerZip(ByVal sessionId As String, ByVal custCd As String)

        Dim theZip As String = String.Empty
        If (sessionId.isNotEmpty AndAlso custCd.isNotEmpty) Then
            theZip = GetCustomerDac().GetCustomerZip(sessionId, custCd)
        End If
        Return (UCase(theZip))
    End Function

    ''' <summary>
    ''' Retrieves the CUSTOMER details from the AR.CUST table, for the customer
    ''' that matches the customer code passed in.
    ''' </summary>
    ''' <param name="custCd">the customer code</param>
    ''' <returns>the CUSTOMER details or an empty object if no match is found</returns>
    Public Function GetCustomer(ByVal custCd As String) As CustomerDtc
        Return GetCustomerDac().GetCustomer(custCd)
    End Function

    ''' <summary>
    ''' Retrieves the CUSTOMER details from the CRM.CUST_INFO table, for the
    ''' customer that matches the customer code and the session id passed in.
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custCd">the customer code</param>
    ''' <returns>the CUSTOMER details or an empty object if no match is found</returns>
    Public Function GetCustomer(ByVal sessionId As String, ByVal custCd As String) As CustomerDtc
        Return GetCustomerDac().GetCustomer(sessionId, custCd)
    End Function

    ''' <summary>
    ''' Gets the drivers license details that match the session id from the CRM.PAYMENT table
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <returns>the Driver License details; or an empty object if no matches found</returns>
    Public Function GetDriverLicense(ByVal sessionId As String) As DriverLicenseDtc
        Return GetCustomerDac().GetDriverLicense(sessionId)
    End Function

    ''' <summary>
    ''' Updates the E1 CUST table with the details passed in
    ''' </summary>
    ''' <param name="customer">customer details as obtained from the CRM.CUST_INFO table</param>
    ''' <param name="dLicense">the driver license details for this customer</param>
    ''' <param name="taxExemptCd">the tax-exempt code for this customer</param>
    ''' <param name="taxExemptId">the tax-exempt ID for this customer</param>
    ''' <remarks></remarks>
    Public Sub UpdateCustomer(ByVal customer As CustomerDtc,
                              ByVal dLicense As DriverLicenseDtc,
                              ByVal taxExemptCd As String,
                              ByVal taxExemptId As String)
        GetCustomerDac().UpdateCustomer(customer, dLicense, taxExemptCd, taxExemptId)
    End Sub

    ''' <summary>
    ''' Inserts a CUSTOMER record in the AR.CUST table, using the details passed in
    ''' and returns the customer code created for this new customer
    ''' </summary>
    ''' <param name="custInfo">the customer details to create a record with</param>
    ''' <returns>the customer details updated after the insert operation</returns>
    Public Function InsertCustomer(ByVal custInfo As CustomerDtc) As CustomerDtc
        Return GetCustomerDac().InsertCustomer(custInfo)
    End Function

    ''' <summary>
    ''' Inserts a CUSTOMER record in the CRM.CUST_INFO table, using the details passed in
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custInfo">the customer details to create a record with</param>
    Public Sub InsertCustomer(ByVal sessionId As String, ByVal custInfo As CustomerDtc)
        GetCustomerDac().InsertCustomer(sessionId, custInfo)
    End Sub



    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetCustomerDac() As CustomerDac
        If (theCustomerDac Is Nothing) Then
            theCustomerDac = New CustomerDac()
        End If

        Return theCustomerDac
    End Function


End Class
