﻿Imports Microsoft.VisualBasic
Imports System.Linq
Imports System.Data.OracleClient
Imports System.Collections.Generic

Public Class PackageBiz
    'Public Function GetPackageAndComponentPrice(ByVal package As PackageItem, ByVal storeCode As String, ByVal RemovedItem As String) As PackageItem
    '    'Step 1 : get the package details
    '    'Step 2 : Get each line share
    '    'Step 3 : Remove the deleted component share
    '    Dim PKGNewPrice = package.PackagePrice
    '    Dim precision = 3
    '    Dim PackagePrice As Double = 0
    '    Dim sku = package.PackageSKU.Trim().ToUpper()
    '    Dim newPrices As PackageItem = New PackageItem()
    '    Dim pkgCmpntsDataSet = New DataSet()
    '    Dim retprc As Double = 0
    '    If Not ConfigurationManager.AppSettings("package_breakout").ToString.Equals(AppConstants.PkgSplit.ON_COMMIT) Then
    '        Throw New Exception("This can be used only when the package splitting is set to Upon Commit.")
    '    End If
    '    Try
    '        Using oraConn As OracleConnection = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString), objSql As OracleCommand = New OracleCommand("", oraConn)
    '            Dim dsRetPrice = New DataSet
    '            Dim sDac As SalesDac = New SalesDac()
    '            Dim theSlaesBiz As SalesBiz = New SalesBiz()
    '            oraConn.Open()
    '            Dim errorMessage As String = ""
    '            'Original Package component details
    '            pkgCmpntsDataSet = sDac.GetPkgComponentData(sku, oraConn, storeCode, errorMessage)
    '            retprc = theSlaesBiz.GetSKUPrice(sku)

    '        End Using

    '        Dim share As DataColumn = New DataColumn("Share", Type.GetType("System.Double"))
    '        pkgCmpntsDataSet.Tables(0).Columns.Add(share)

    '        Dim NewPrice As DataColumn = New DataColumn("NewPrice", Type.GetType("System.Double"))
    '        NewPrice.DefaultValue = 0.0
    '        pkgCmpntsDataSet.Tables(0).Columns.Add(NewPrice)

    '        Dim OrderOfOccurence As DataColumn = New DataColumn("Occurence", Type.GetType("System.Int32"))
    '        pkgCmpntsDataSet.Tables(0).Columns.Add(OrderOfOccurence)

    '        Dim Deleted As DataColumn = New DataColumn("Deleted", Type.GetType("System.Int32"))
    '        pkgCmpntsDataSet.Tables(0).Columns.Add(Deleted)

    '        'Remove the references of the deleted items!!

    '        'Removed Occurences
    '        For Each itm As Component In package.Components
    '            'Dim row() = pkgCmpntsDataSet.Tables(0).Select("Deleted <> 1 AND ITM_CD='" & itm.SKU & "'")
    '            Dim row() = pkgCmpntsDataSet.Tables(0).Select("Deleted is NULL AND ITM_CD='" & itm.SKU & "'")
    '            If row.Count > 0 Then
    '                row(0)("Deleted") = 1
    '            End If
    '        Next
    '        Dim rowRemoved() = pkgCmpntsDataSet.Tables(0).Select("Deleted is NOT NULL AND ITM_CD='" & RemovedItem & "'")
    '        If rowRemoved.Any() Then
    '            rowRemoved(rowRemoved.Count - 1)("Deleted") = "0" 'current row being removed
    '        End If

    '        Dim lastOccurance As Integer = 0
    '        Dim i As Integer = 1
    '        'Original share of each item of the package!!
    '        '
    '        For Each row As DataRow In pkgCmpntsDataSet.Tables(0).Rows
    '            If row("ITM_TP_CD") <> "FAB" AndAlso row("Deleted") & "" <> "" Then
    '                row("Share") = Math.Round((row("CURR_PRC") / retprc), precision, MidpointRounding.AwayFromZero)
    '                'row("NewPrice") = Math.Round((row("Share") * PKGNewPrice), precision, MidpointRounding.AwayFromZero)
    '                lastOccurance = lastOccurance + 1
    '            Else
    '                row("Share") = 0
    '            End If
    '            row("Occurence") = i
    '            i = i + 1
    '        Next row

    '        Dim totalShare As Double = (From dr In pkgCmpntsDataSet.Tables(0).AsEnumerable()
    '                                    Select Convert.ToDouble(dr("Share"))).Sum()



    '        'If the total share calculated is <> 1 (meaning not 100%) due to the fraction calculation, then adjust the last row
    '        If totalShare <> 1 Then
    '            Dim differenceShare = Math.Round(1.0 - totalShare, precision, MidpointRounding.AwayFromZero)
    '            Dim dr = pkgCmpntsDataSet.Tables(0).Rows(lastOccurance - 1)
    '            dr("Share") = dr("Share") + differenceShare
    '            dr("NewPrice") = Math.Round((dr("Share") * PKGNewPrice), precision, MidpointRounding.AwayFromZero)
    '        End If




    '        Dim dt As DataTable = pkgCmpntsDataSet.Tables(0)
    '        'Check the deleted items 


    '        'Dim Res = (From n In dt.AsEnumerable()
    '        '           Join m In package.Components
    '        '           On n.Field(Of Int32)("Occurence") Equals m.OrderOfOccurance)
    '        Dim Res = (From n In dt.AsEnumerable()
    '                   Join m In package.Components
    '                   On n.Field(Of String)("ITM_CD") Equals m.SKU)


    '        ''Make note of remaining items of deletion of lines
    '        Dim componentsRemaining As List(Of String) = (From x In Res Select x.n.Field(Of String)("itm_cd")).Distinct.ToList()

    '        'update the share of the existing items and set it as 0 for removed items

    '        For Each row As DataRow In pkgCmpntsDataSet.Tables(0).Rows
    '            If componentsRemaining.Contains(row("itm_cd")) & "" AndAlso row("Deleted") & "" <> "" Then
    '                Dim component As Component = New Component()
    '                component.SKU = row("itm_cd") & ""
    '                component.Price = row("NewPrice")
    '                component.OrderOfOccurance = row("Occurence")
    '                newPrices.Components.Add(component)
    '                'componentsRemaining.Remove(row("itm_cd") & "")
    '            Else
    '                row("Share") = 0.0
    '                row("NewPrice") = 0.0
    '            End If
    '        Next row


    '        pkgCmpntsDataSet.AcceptChanges()
    '        'this linq query gets the package price
    '        PackagePrice = (From dr In pkgCmpntsDataSet.Tables(0).AsEnumerable()
    '                        Select Convert.ToDouble(dr("NewPrice"))).Sum()

    '        newPrices.PackageSKU = package.PackageSKU
    '        newPrices.PackagePrice = PackagePrice

    '        Dim aciveItemsShare = (From dr In dt.AsEnumerable()
    '                               Select Convert.ToDouble(dr("Share"))).Sum()

    '        newPrices.Components = (From m In dt.AsEnumerable() Where m.Field(Of Double)("Share") > 0
    '                                Select New Component() With {
    '                      .SKU = m.Field(Of String)("ITM_CD"),
    '                      .Price = Math.Round(((m.Field(Of Double)("Share") / aciveItemsShare) * PackagePrice), precision, MidpointRounding.AwayFromZero),
    '                      .OrderOfOccurance = m.Field(Of Int32)("Occurence")
    '                      }).ToList()
    '        Dim pkgPrice As Double = (From x In newPrices.Components Select x.Price).Sum()
    '        If Not pkgPrice = newPrices.PackagePrice Then
    '            'Adjust the price of last component
    '            Dim lastItemPrice = newPrices.Components(newPrices.Components.Count - 1).Price
    '            lastItemPrice = lastItemPrice + (newPrices.PackagePrice - lastItemPrice)
    '            newPrices.Components(newPrices.Components.Count - 1).Price = lastItemPrice
    '        Else
    '            'Do nothing
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return newPrices
    'End Function


    Public Function GetPackageAndComponentPrice(ByVal package As PackageItem, ByVal storeCode As String, ByVal itemRemoved As String) As PackageItem

        Dim newPackagePricing As PackageItem = New PackageItem()
        newPackagePricing = DistributePriceToPackageItems(package, storeCode)
        Dim cmp = (From item In newPackagePricing.Components
                   Where item.SKU = itemRemoved
                   Select item).FirstOrDefault()
        newPackagePricing.Components.Remove(cmp)
        newPackagePricing.PackagePrice = (From n In newPackagePricing.Components Select n.Price).Sum()
        Return (newPackagePricing)
    End Function

    Public Function DistributePriceToPackageItems(ByVal package As PackageItem, ByVal StoreCode As String) As PackageItem
        Dim PKGNewPrice = package.PackagePrice
        Dim precision = 2
        Dim PackagePrice As Double = PKGNewPrice
        Dim sku = package.PackageSKU.Trim().ToUpper()
        Dim newPrices As PackageItem = New PackageItem()
        Dim pkgCmpntsDataSet = New DataSet()
        Dim retprc As Double = 0
        If Not ConfigurationManager.AppSettings("package_breakout").ToString.Equals(AppConstants.PkgSplit.ON_COMMIT) Then
            Throw New Exception("This can be used only when the package splitting is set to Upon Commit.")
        End If
        Try
            Using oraConn As OracleConnection = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString), objSql As OracleCommand = New OracleCommand("", oraConn)
                Dim dsRetPrice = New DataSet
                Dim sDac As SalesDac = New SalesDac()
                Dim theSlaesBiz As SalesBiz = New SalesBiz()
                oraConn.Open()
                Dim errorMessage As String = ""
                'Original Package component details
                pkgCmpntsDataSet = sDac.GetPkgComponentData(sku, oraConn, StoreCode, errorMessage)
                If pkgCmpntsDataSet Is Nothing Then
                    Throw New Exception("Unable to retrive the package components.")
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
        retprc = PKGNewPrice
        Dim share As DataColumn = New DataColumn("Share", Type.GetType("System.Double"))
        pkgCmpntsDataSet.Tables(0).Columns.Add(share)

        Dim NewPrice As DataColumn = New DataColumn("NewPrice", Type.GetType("System.Double"))
        NewPrice.DefaultValue = 0.0
        pkgCmpntsDataSet.Tables(0).Columns.Add(NewPrice)

        Dim OrderOfOccurence As DataColumn = New DataColumn("Occurence", Type.GetType("System.Int32"))
        pkgCmpntsDataSet.Tables(0).Columns.Add(OrderOfOccurence)

        Dim Deleted As DataColumn = New DataColumn("Deleted", Type.GetType("System.Int32"))
        pkgCmpntsDataSet.Tables(0).Columns.Add(Deleted)


        Dim PackageTotlaPrice = (From dr In pkgCmpntsDataSet.Tables(0).AsEnumerable()
                                 Select Convert.ToDouble(dr("CURR_PRC"))).Sum()

        For Each row As DataRow In pkgCmpntsDataSet.Tables(0).Rows
            row("Share") = row("CURR_PRC") / PackageTotlaPrice
        Next row

        Dim i As Int32 = 1



        'loop through the input components and mark the items that are removed!!
        'Here 1 indicated item found and not removed.
        For Each itm As PackageComponent In package.Components
            'Dim row() = pkgCmpntsDataSet.Tables(0).Select("Deleted <> 1 AND ITM_CD='" & itm.SKU & "'")
            Dim row() = pkgCmpntsDataSet.Tables(0).Select("Deleted is NULL AND ITM_CD='" & itm.SKU & "'")
            If row.Count > 0 Then
                row(0)("Deleted") = 1
            Else

            End If
        Next
        Dim filterDel As String = "Deleted = 1"
        Dim shares() As DataRow = pkgCmpntsDataSet.Tables(0).Select(filterDel)

        Dim newShare = (From dr In shares
                        Select Convert.ToDouble(dr("Share"))).Sum()
        For Each dr In shares
            dr("Share") = dr("Share") / newShare
        Next


        For Each row As DataRow In pkgCmpntsDataSet.Tables(0).Rows
            If row("ITM_TP_CD") <> "FAB" AndAlso row("Deleted") & "" <> "" Then
                'row("Share") = Math.Round((row("CURR_PRC") / PKGNewPrice), precision, MidpointRounding.AwayFromZero)
                row("NewPrice") = Math.Round((row("Share") * PKGNewPrice), precision, MidpointRounding.AwayFromZero)
            Else
                row("Share") = 0
                row("NewPrice") = 0
            End If
            row("Occurence") = i
            i = i + 1
        Next row

        Dim totalShare As Double = (From drTotalShare In pkgCmpntsDataSet.Tables(0).AsEnumerable() Select Convert.ToDouble(drTotalShare("Share"))).Sum()
        'If the total share calculated is <> 1 (meaning not 100%) due to the fraction calculation, then adjust the last row
        Dim drValidRows() As DataRow = pkgCmpntsDataSet.Tables(0).Select("deleted is not null")
        If totalShare <> 1 Then
            Dim differenceShare = 1.0 - totalShare
            Dim dr = drValidRows(drValidRows.Length - 1)
            dr("Share") = dr("Share") + differenceShare
            dr("NewPrice") = Math.Round((dr("Share") * PKGNewPrice), precision, MidpointRounding.AwayFromZero)
        End If
        pkgCmpntsDataSet.AcceptChanges()
        'this linq query gets the package price


        newPrices.PackageSKU = package.PackageSKU
        newPrices.PackagePrice = PKGNewPrice
        Dim dt As DataTable = pkgCmpntsDataSet.Tables(0)
        Dim aciveItemsShare = (From dr In dt.AsEnumerable()
                               Select Convert.ToDouble(dr("Share") & "")).Sum()

        newPrices.Components = (From m In dt.AsEnumerable() Where m.Field(Of Double)("Share") & "" > 0
                                Select New PackageComponent() With {
                          .SKU = m.Field(Of String)("ITM_CD"),
                          .Price = Math.Round(((m.Field(Of Double)("Share") / aciveItemsShare) * PKGNewPrice), precision, MidpointRounding.AwayFromZero),
                          .OrderOfOccurance = m.Field(Of Int32)("Occurence"),
                          .ItemType = m.Field(Of String)("ITM_TP_CD")
                          }).ToList()
        Dim pkgPrice As Double = (From x In newPrices.Components Select x.Price).Sum()
        pkgPrice = Math.Round(pkgPrice, precision, MidpointRounding.AwayFromZero)
        If Not pkgPrice = newPrices.PackagePrice Then
            'Adjust the price of last component
            Dim lastItemPrice = newPrices.Components(newPrices.Components.Count - 1).Price
            lastItemPrice = Math.Round(lastItemPrice + (newPrices.PackagePrice - pkgPrice), precision, MidpointRounding.AwayFromZero)
            newPrices.Components(newPrices.Components.Count - 1).Price = lastItemPrice
        Else
            'Do nothing
        End If
        Return newPrices
    End Function
End Class


