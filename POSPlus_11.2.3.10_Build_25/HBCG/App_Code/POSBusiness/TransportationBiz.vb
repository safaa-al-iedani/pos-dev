﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data
Imports System.Data.OracleClient

Public Class TransportationBiz

    Private theTransportationDac As TransportationDac
    Private theStoreDac As StoreDac

    ''' <summary>
    ''' Extract and returns the zone code for the postal code provided if only one found 
    '''  and returns the number of rows found; if multiple rows found, then need to redirect to 'Delivery.aspx'
    '''  for user selection of zone; if there are no zones found for the postal code but the POS+ syspm to access all zones
    '''  is enabled, then will extract from all zones and return the zone code and/or number of rows from that query
    ''' </summary>
    ''' <param name="postalCd">postal code to find a zone code for</param>
    ''' <returns>Returns a response object from requesting the zone for a postal code; includes zone code if only
    '''  one zone and the number of rows found</returns>
    Public Function GetZoneCd4Zip(ByVal postalCd As String) As ZoneFromZipResp

        Dim zoneZip As New ZoneFromZipResp
        zoneZip.zoneCd = String.Empty
        zoneZip.numRows = 0

        If postalCd.isNotEmpty Then

            Dim zoneDs As DataSet = GetZones4ZipOrAll(postalCd)
            zoneZip.numRows = zoneDs.Tables(0).Rows.Count

            If zoneZip.numRows = 1 Then
                'means only one zone retrieved

                zoneZip.zoneCd = zoneDs.Tables(0).Rows(0)("ZONE_CD").ToString()
                'MM-7300
                zoneZip.zoneDes = zoneDs.Tables(0).Rows(0)("DES").ToString()
            End If
        End If

        Return zoneZip
    End Function

    ''' <summary>
    ''' Returns the dataset from requesting the zone for a postal code; if there are no zones found for the postal code 
    ''' but the POS+ syspm to access all zones is enabled, then will extract from all zones 
    ''' </summary>
    ''' <param name="postalCd">postal code to find zip codes for</param>
    ''' <returns>Returns a dataset from requesting the zone for a postal code/returns>
    ''' <remarks>only processes if the postal code is supplied</remarks>
    Public Function GetZones4ZipOrAll(ByVal postalCd As String) As DataSet

        Dim zoneDs As New DataSet

        If postalCd.isNotEmpty Then

            Try
                zoneDs = GetZoneCodesForZip(postalCd)
                Dim numRows As Integer = 0

                numRows = zoneDs.Tables(0).Rows.Count

                ' if we didn't find any but we are allowed to then try any zone, get them all
                If numRows = 0 AndAlso ConfigurationManager.AppSettings("open_zones").ToString = "Y" Then

                    zoneDs = GetZoneCodes()
                End If

            Catch ex As Exception
                Throw
            End Try
        End If

        Return zoneDs
    End Function

    ''' <summary>
    ''' Extracts the pickup/delivery store code based on the input;  
    ''' 1) if a pickup, then return the pickup store from the written store information
    ''' 2) if a pickup and there is no pickup store on written store, then the pickup store is the written store
    ''' 3) if a delivery, then return the delivery store from the zone
    ''' 4) if a delivery and no zone supplied or there is no delivery store on the zone, then use the ship_to_store_cd from the written store 
    ''' </summary>
    ''' <param name="wrStoreCd">written store code to find the pickup/delivery store for</param>
    ''' <param name="puDel">pickup/delivery flag, either P or D</param>
    ''' <param name="zoneCd">zone code to find the delivery store for if it is a delivery</param>
    ''' <returns>the pickup/delivery store code</returns>
    ''' <remarks>no guarantee this will produce a value, return store code may be empty</remarks>
    Public Function GetPuDelStore(ByVal wrStoreCd As String, ByVal puDel As String, ByVal zoneCd As String) As String

        Dim puDelStoreCd As String = ""

        ' TODO - hopefully at some point, we should be able to pass in the session store object but don't hit the db until it becomes necessary
        If puDel = AppConstants.PICKUP AndAlso wrStoreCd.isNotEmpty Then

            Dim wrStore As StoreData = GetStoreDac.GetStoreInfo(wrStoreCd.Trim)

            If wrStore.puStoreCd.isEmpty Then

                puDelStoreCd = wrStoreCd

            Else
                puDelStoreCd = wrStore.puStoreCd
            End If

        ElseIf puDel = AppConstants.DELIVERY Then

            If zoneCd.isNotEmpty Then

                Dim zone As ZoneDtc = GetZone(zoneCd)

                If zone.zoneCd.isNotEmpty AndAlso zone.delStoreCd.isNotEmpty Then

                    puDelStoreCd = zone.delStoreCd
                End If
            End If

            If zoneCd.isEmpty AndAlso wrStoreCd.isNotEmpty Then

                Dim wrStore As StoreData = GetStoreDac.GetStoreInfo(wrStoreCd.Trim)
                puDelStoreCd = wrStore.shipToStoreCd
            End If
        End If

        Return puDelStoreCd
    End Function
    Public Function lucy_get_co_cd(ByRef p_store_cd As String) As String


        Dim str_co_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()


        sql = "select co_cd from store where store_cd='" & p_store_cd & "' "


        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                str_co_cd = MyDataReader.Item("co_cd")


            Else
                str_co_cd = ""

            End If
            MyDataReader.Close()

        Catch
            conn.Close()
            Throw

        End Try
        Return str_co_cd

    End Function

    ''' <summary>
    ''' Extracts the pickup/delivery zip code based on the input;  
    ''' 1) if a pickup, then the zip code from the pickup store address
    ''' 2) if a delivery, then if have customer zip, then use customer zip
    ''' 3) if a delivery and there is no customer zip, then use alternate delivery zip if provided
    ''' </summary>
    ''' <param name="puDel">pickup/delivery flag, either P or D</param>
    ''' <param name="puDelStoreCd">pickup/delivery store code</param>
    ''' <param name="custZip">customer zip code if customer available</param>
    ''' <param name="delZip">alternative delivery zip code used for delivery zone if nothing avail prior</param>
    ''' <returns>the pickup/delivery zip code</returns>
    ''' <remarks>no guarantee this will produce a value, return zip code may be empty</remarks>
    Public Function GetPuDelZip(ByVal puDel As String, ByVal puDelStoreCd As String, ByVal custZip As String, ByVal delZip As Object) As String

        Dim puDelZipCd As String = ""

        If puDel <> "" Then

            If puDelStoreCd.isNotEmpty AndAlso puDel = AppConstants.PICKUP Then

                Dim storeObj As StoreData = GetStoreDac.GetStoreInfo(puDelStoreCd.Trim)

                puDelZipCd = storeObj.addr.postalCd.ToUpper

            ElseIf puDel = AppConstants.DELIVERY Then

                If custZip.isNotEmpty Then

                    puDelZipCd = custZip

                ElseIf Not SystemUtils.isEmpty(delZip) Then

                    puDelZipCd = delZip
                End If
            End If
        End If

        Return puDelZipCd
    End Function

    ''' <summary>
    ''' Retrieves the transportation points based on the order lines found in the
    ''' current session.   Uses the TEMP_ITM table for this.
    ''' </summary>
    ''' <param name="sessionId">the session id to fetch the order lines</param>
    ''' <returns>the number of points for the session passed in</returns>
    Public Function GetDeliveryPoints(ByVal sessionId As String) As Integer

        Dim points As Integer = 1
        If SysPms.isDelvByPoints Then
            points = GetTransportationDac().GetDeliveryPoints(sessionId)
        End If
        Return points

    End Function

    ''' <summary>
    ''' Schedules the delivery or pickup points or stops, as applicable
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="zoneCd">the zone code for this pickup/delivery</param>
    ''' <param name="puDel">the flag that indicates if pickup or delivery</param>
    ''' <param name="puDelDt">the pickup/delivery date</param>
    ''' <param name="trnTp">the transaction type CRM / SAL / EEX ... </param>
    ''' <param name="pointOrStops">the number of points for this pickup/delivery</param>
    Public Sub SchedPuDel(ByRef cmd As OracleCommand, ByVal zoneCd As String, ByVal puDel As String,
                          ByVal puDelDt As Date, ByVal trnTp As String, ByVal pointOrStops As Double)

        ' currently only DELM is processed, not PUM
        If AppConstants.DELIVERY.Equals(puDel) Then
            GetTransportationDac().SchedPuDel(cmd, zoneCd, puDel, puDelDt, trnTp, pointOrStops)
        End If
    End Sub

    ''' <summary>
    ''' Unschedules the delivery or pickup points or stops, as applicable
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="zoneCd">the zone code for this pickup/delivery</param>
    ''' <param name="puDel">the flag that indicates if pickup or delivery</param>
    ''' <param name="puDelDt">the pickup/delivery date</param>
    ''' <param name="trnTp">the transaction type CRM / SAL </param>
    ''' <param name="pointOrStops">the number of points for this pickup/delivery</param>
    Public Sub UnschedPuDel(ByRef cmd As OracleCommand, ByVal zoneCd As String, ByVal puDel As String,
                            ByVal puDelDt As Date, ByVal trnTp As String, ByVal pointOrStops As Double)

        ' currently only DELM is processed, not PUM
        If AppConstants.DELIVERY.Equals(puDel) Then
            GetTransportationDac().UnschedPuDel(cmd, zoneCd, puDel, puDelDt, trnTp, pointOrStops)
        End If
    End Sub

    ''' <summary>
    ''' Retreves the zone details for the postal code passed-in
    ''' </summary>
    ''' <param name="postalCd">the postal code for which to get the zones.</param>
    ''' <returns>a dataset consisting of the zone info for the postal code specified</returns>
    Public Function GetZoneCodesForZip(ByVal postalCd As String) As DataSet

        Return GetTransportationDac().GetZoneCodesForZip(postalCd)
    End Function
    ''' <summary>
    ''' Retreves the zone details for the postal code passed-in which is marked with appropriate delivery types
    ''' </summary>
    ''' <param name="postalCd">the postal code for which to get the zones.</param>
    '''  <param name="saleType">Sale type(P/D).</param>
    ''' <returns>a dataset consisting of the zone info for the postal code specified</returns>
    Public Function GetZonesFromZPFMForZip(ByVal postalCd As String, ByRef p_co_cd As String, ByVal saleType As String) As DataSet

        Return GetTransportationDac().GetZonesFromZPFMForZip(postalCd, p_co_cd, saleType)
    End Function

    ''' <summary>
    ''' Retrieves the zone details for ALL the zones defined in the system.
    ''' </summary>
    ''' <returns>a dataset consisting of the zone info for ALL the zones.
    ''' </returns>
    Public Function GetZoneCodes() As DataSet
        Return GetTransportationDac().GetZoneCodes()
    End Function

    ''' <summary>
    ''' Retrieves the zone details for the zone requested.
    ''' </summary>
    ''' <returns>an object of information for the zone requested
    ''' </returns>
    Public Function GetZone(ByVal zoneCd As String) As ZoneDtc
        Return GetTransportationDac().GetZone(zoneCd)
    End Function

    ''' <summary>
    ''' Returns the ZONE.DAYS_LEAD value, that correspond to the zone code
    ''' passed in; a zero gets returned if no matching zone is found.
    ''' </summary>
    ''' <param name="zoneCd">the zone to retrieve the lead days for</param>
    ''' <returns>the DAYS_LEAD value; a zero is returned if zone is not found</returns>
    Public Function GetZoneLeadDays(ByVal zoneCd As String) As String
        Return GetTransportationDac().GetZoneLeadDays(zoneCd)
    End Function

    ''' <summary>
    ''' Updating the truck stop date for the delivery document number
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="trkStopDate">the truck stop date for updating the Truck Delivery System</param>
    ''' <param name="trkNum">the truck number for updating the Truck Delivery System</param>
    ''' <param name="stopNum">the truck stop number for updating the Truck Delivery System</param>
    ''' <param name="delDocNum">the delivery document number for updating the Truck Delivery System</param>
    ''' <remarks></remarks>
    Public Sub UpdateTruckDeliverySystem(ByRef cmd As OracleCommand, ByVal trkStopDate As Date, _
                                         ByVal trkNum As String, ByVal stopNum As String, ByVal delDocNum As String)
        If Not String.IsNullOrEmpty(delDocNum) And IsDate(trkStopDate) Then
            GetTransportationDac().UpdateTruckDeliverySystem(cmd, trkStopDate, trkNum, stopNum, delDocNum)
        End If
    End Sub

    ''' <summary>
    ''' Clearing the truck stop date for the delivery document number
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="delDocNum">the delivery document number for clearing the Truck Delivery System</param>
    ''' <remarks></remarks>
    Public Sub ClearTruckDeliverySystem(ByRef cmd As OracleCommand, ByVal delDocNum As String)
        If Not String.IsNullOrEmpty(delDocNum) Then
            GetTransportationDac().ClearTruckDeliverySystem(cmd, delDocNum)
        End If
    End Sub

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetTransportationDac() As TransportationDac
        If (theTransportationDac Is Nothing) Then
            theTransportationDac = New TransportationDac()
        End If

        Return theTransportationDac
    End Function

    Private Function GetStoreDac() As StoreDac
        If (theStoreDac Is Nothing) Then
            theStoreDac = New StoreDac()
        End If

        Return theStoreDac
    End Function
    ''' <summary>
    ''' Get the Delivery points details.
    ''' </summary>
    ''' <param name="zoneCd"></param>
    ''' <param name="delDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDeliveryPointsDetails(ByVal zoneCd As String, ByVal delDate As String) As DeliveryPointDetails
        Return GetTransportationDac().GetDeliveryPointsDetails(zoneCd, delDate)
    End Function

    ''' <summary>
    ''' Returns the delivery dates from DEL table
    ''' </summary>
    ''' <param name="zoneCd">zone code</param>
    ''' <param name="delDate">delivery date</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDeliveryDates(ByVal zoneCd As String, ByVal delDate As Date) As DataSet
        Return GetTransportationDac().GetDeliveryDates(zoneCd, delDate)
    End Function

    Public Function GetDisableDeliveryDates(ByVal points As Integer, ByVal remainingPoints As Integer, ByVal exmpCD As String) As Boolean
        Dim retVal As Boolean
        If SysPms.isDelvByPoints Then
            If SysPms.pointOverScheduling = "W" Or SysPms.pointOverScheduling = "N" Then
                Dim extraPoints As Integer = points - remainingPoints
                If CDbl(FormatNumber(remainingPoints)) <= 0 And (exmpCD = "&nbsp;" OrElse String.IsNullOrEmpty(exmpCD)) Then
                    retVal = False
                ElseIf CDbl(FormatNumber(remainingPoints)) <= 0 And exmpCD <> "&nbsp;" AndAlso Not String.IsNullOrEmpty(exmpCD) Then
                    retVal = True
                ElseIf CDbl(FormatNumber(remainingPoints)) > 0 And CDbl(FormatNumber(remainingPoints)) <= CDbl(FormatNumber(points)) Then
                    retVal = True
                ElseIf CDbl(FormatNumber(remainingPoints)) > 0 And CDbl(FormatNumber(remainingPoints)) >= CDbl(FormatNumber(points)) Then
                    retVal = True
                End If
            End If

            If SysPms.pointOverScheduling = "P" Then
                If CDbl(FormatNumber(remainingPoints)) <= 0 Then
                    retVal = False
                ElseIf CDbl(FormatNumber(remainingPoints)) < CDbl(FormatNumber(points)) Then
                    retVal = False
                Else
                    retVal = True
                End If
            End If
        Else
            If CDbl(FormatNumber(remainingPoints)) <= 0 And (exmpCD = "&nbsp;" OrElse String.IsNullOrEmpty(exmpCD)) Then
                retVal = False
            ElseIf CDbl(FormatNumber(remainingPoints)) <= 0 And exmpCD <> "&nbsp;" AndAlso Not String.IsNullOrEmpty(exmpCD) Then
                retVal = True
            ElseIf CDbl(FormatNumber(remainingPoints)) > 0 Then
                retVal = True
            End If
        End If
        Return retVal
    End Function

    ''' <summary>
    ''' Retrieves the zone details for ALL the zones defined in the system.
    ''' </summary>
    ''' <returns>a dataset consisting of the zone info for ALL the zones.
    ''' </returns>
    Public Function GetZoneCodes(ByVal empInit As String) As DataSet
        ' Daniela Feb 16
        Return GetTransportationDac().GetZoneCodes(empInit)
    End Function

    Public Function lucy_GetZones4ZipOrAll(ByVal postalCd As String, ByRef p_co_cd As String) As DataSet
        'lucy copied from GetZones4ZipOrAll, add one more parameter
        ' Daniela fix multi company logic
        Dim zoneDs As New DataSet

        If postalCd.isNotEmpty Then

            Try
                zoneDs = lucy_GetZoneCodesForZip(postalCd, p_co_cd)
                Dim numRows As Integer = 0

                numRows = zoneDs.Tables(0).Rows.Count

                ' if we didn't find any but we are allowed to then try any zone, get them all
                If numRows = 0 AndAlso ConfigurationManager.AppSettings("open_zones").ToString = "Y" Then

                    zoneDs = GetTransportationDac().lucy_GetZoneCodes(p_co_cd)
                End If

            Catch ex As Exception
                Throw
            End Try
        End If

        Return zoneDs

    End Function

    Public Function lucy_GetZoneCodesForZip(ByVal postalCd As String, ByRef p_co_cd As String) As DataSet
        'lucy copied from  GetZoneCodesForZip
        Return GetTransportationDac().lucy_GetZoneCodesForZip(postalCd, p_co_cd)
    End Function
   
    Public Function CheckForDeliveryAvaDT(ByVal gPointCnt As Double, ByVal delDate As Date, ByVal zone As String) As Boolean
        Dim delAvDT As Boolean = False
        Dim exmpCD As String = String.Empty
        Dim remPointStop As Double = GetTransportationDac().GetRemPointsForDeliveryDT(gPointCnt, delDate, zone, exmpCD)
        If remPointStop > 0 Then
            'using existing method GetDisableDeliveryDates
            delAvDT = GetDisableDeliveryDates(gPointCnt, remPointStop, exmpCD)
        End If
        Return delAvDT
    End Function

    ''' <summary>
    ''' Overloaded method: Set the pickup/delivery store code based on the store data passed.
    ''' 1) if a pickup, then return the pickup store from the written store information
    ''' 2) if a pickup and there is no pickup store on written store, then the pickup store is the written store
    ''' 3) if a delivery, then return the delivery store from the zone
    ''' 4) if a delivery and no zone supplied or there is no delivery store on the zone, then use the ship_to_store_cd from the written store 
    ''' </summary>
    ''' <param name="wrStoreCd">written store code to find the pickup/delivery store for</param>
    ''' <param name="puDel">pickup/delivery flag, either P or D</param>
    ''' <param name="zoneCd">zone code to find the delivery store for if it is a delivery</param>
    ''' <param name="wrStore">written store data</param>
    ''' <returns>the pickup/delivery store code</returns>
    ''' <remarks>no guarantee this will produce a value, return store code may be empty</remarks>
    Public Function GetPuDelStore(ByVal wrStoreCd As String, ByVal puDel As String, ByVal zoneCd As String, ByVal wrStore As StoreData) As String

        Dim puDelStoreCd As String = ""

        ' TODO - hopefully at some point, we should be able to pass in the session store object but don't hit the db until it becomes necessary
        If puDel = AppConstants.PICKUP AndAlso wrStoreCd.isNotEmpty Then
            If wrStore.puStoreCd.isEmpty Then
                puDelStoreCd = wrStoreCd
            Else
                puDelStoreCd = wrStore.puStoreCd
            End If
        ElseIf puDel = AppConstants.DELIVERY Then
            If zoneCd.isNotEmpty Then
                Dim zone As ZoneDtc = GetZone(zoneCd)
                If zone.zoneCd.isNotEmpty AndAlso zone.delStoreCd.isNotEmpty Then
                    puDelStoreCd = zone.delStoreCd
                End If
            End If

            If zoneCd.isEmpty AndAlso wrStoreCd.isNotEmpty Then
                puDelStoreCd = wrStore.puStoreCd
            End If
        End If

        Return puDelStoreCd
    End Function
End Class
