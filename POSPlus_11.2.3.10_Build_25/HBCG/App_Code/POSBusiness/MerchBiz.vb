﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class MerchBiz

    Private theMerchDac As MerchDac

    ''' <summary>
    ''' Determines if the passed-in warranty has been defined in the system(ITM2ITM_WARR table)
    ''' with the specified warrantable sku. 
    ''' </summary>
    ''' <param name="itmCd">item code of the warrantable line</param>
    ''' <param name="warrItmCd">>item code of the warranty line</param>
    ''' <returns>true if a match was found for the specified warranty and warrantable skus, false otherwise</returns>
    Public Function IsValWarrForItm(ByVal itmCd As String, ByVal warrItmCd As String) As Boolean
        Return GetMerchDac().IsValWarrForItm(itmCd, warrItmCd)
    End Function


    Private Function GetMerchDac() As MerchDac
        If (theMerchDac Is Nothing) Then
            theMerchDac = New MerchDac()
        End If

        Return theMerchDac
    End Function

End Class
