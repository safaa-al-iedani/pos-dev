﻿Imports System.Data.OracleClient
Imports Microsoft.VisualBasic

Public Class POBiz

    Private objPODac As PODac

    Private Function GetPODac() As PODac
        If (objPODac Is Nothing) Then
            objPODac = New PODac()
        End If
        Return objPODac
    End Function

    ''' <summary>
    ''' Getting the PO Line details
    ''' </summary>
    ''' <param name="strPO_CD">PO_CD</param>
    ''' <returns>PO Line details as a DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetPOLineDetails(ByVal strPO_CD As String) As DataTable
        Return GetPODac().GetPOLineDetails(strPO_CD)
    End Function
    ''' <summary>
    ''' GetPOArrivalDate - Returns the PO arrival date for the given PO order.
    ''' </summary>
    ''' <param name="purchaseOrder">PO Number</param>
    ''' <returns></returns>
    Public Function GetPOArrivalDate(ByVal purchaseOrder As String) As String
        Return GetPODac().GetPOArrivalDate(purchaseOrder)
    End Function
    ''' <summary>
    ''' GetPOArrivalDateForTempLineItem -- Returns the PO arrival date for the given line number
    ''' </summary>
    ''' <param name="LineNumber">TEMP_ITM row number</param>
    ''' <returns></returns>
    Public Function GetPOArrivalDateForTempLineItem(ByVal LineNumber As String) As String
        Return GetPODac().GetPOArrivalDateForTempLineItem(LineNumber)
    End Function
    ''' <summary>
    ''' This method call the dac to check the PO availability
    ''' </summary>
    ''' <param name="objSqlCommand"></param>
    ''' <param name="PurchaseOrderCode"></param>
    ''' <param name="POLine"></param>
    ''' <param name="ItemCode"></param>
    ''' <param name="Quantity"></param>
    ''' <returns></returns>
    Public Function IsPOAvailable(ByRef objSqlCommand As OracleCommand, ByVal PurchaseOrderCode As String, ByVal POLine As String, ByVal ItemCode As String, ByVal Quantity As String) As Boolean
        Return GetPODac().IsPOAvailable(objSqlCommand, PurchaseOrderCode, POLine, ItemCode, Quantity)
    End Function
End Class
