﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Public Class WPPEBiz

#Region " Private variables "
    Private p_co_cd As System.String
    Private p_prc_cd As System.String
    Private p_itm_cd As System.String
    Private p_corp As System.Decimal
    Private p_corp_fran As System.Decimal
    Private p_fran As System.Decimal
    Private p_que As System.Decimal
    Private p_que_fran As System.Decimal
    Private p_other As System.Decimal
    Private p_eff_dt As System.String
    Private p_end_dt As System.String
    Private p_upd_dt As System.String
    Private p_upd_time As System.String
    Private p_emp_cd As System.String
#End Region

#Region " Public variables "

    Public Property co_cd() As System.String
        Get
            Return p_co_cd
        End Get
        Set(ByVal Value As System.String)
            p_co_cd = Value
        End Set
    End Property
    Public Property prc_cd() As System.String
        Get
            Return p_prc_cd
        End Get
        Set(ByVal Value As System.String)
            p_prc_cd = Value
        End Set
    End Property
    Public Property itm_cd() As System.String
        Get
            Return p_itm_cd
        End Get
        Set(ByVal Value As System.String)
            p_itm_cd = Value
        End Set
    End Property
    Public Property corp() As System.Decimal
        Get
            Return p_corp
        End Get
        Set(ByVal Value As System.Decimal)
            p_corp = Value
        End Set
    End Property
    Public Property corp_fran() As System.Decimal
        Get
            Return p_corp_fran
        End Get
        Set(ByVal Value As System.Decimal)
            p_corp_fran = Value
        End Set
    End Property
    Public Property fran() As System.Decimal
        Get
            Return p_fran
        End Get
        Set(ByVal Value As System.Decimal)
            p_fran = Value
        End Set
    End Property
    Public Property que() As System.Decimal
        Get
            Return p_que
        End Get
        Set(ByVal Value As System.Decimal)
            p_que = Value
        End Set
    End Property
    Public Property que_fran() As System.Decimal
        Get
            Return p_que_fran
        End Get
        Set(ByVal Value As System.Decimal)
            p_que_fran = Value
        End Set
    End Property
    Public Property other() As System.Decimal
        Get
            Return p_other
        End Get
        Set(ByVal Value As System.Decimal)
            p_other = Value
        End Set
    End Property
    Public Property eff_dt() As System.String
        Get
            Return p_eff_dt
        End Get
        Set(ByVal Value As System.String)
            p_eff_dt = Value
        End Set
    End Property
    Public Property end_dt() As System.String
        Get
            Return p_end_dt
        End Get
        Set(ByVal Value As System.String)
            p_end_dt = Value
        End Set
    End Property
    Public Property upd_dt() As System.String
        Get
            Return p_upd_dt
        End Get
        Set(ByVal Value As System.String)
            p_upd_dt = Value
        End Set
    End Property
    Public Property upd_time() As System.String
        Get
            Return p_upd_time
        End Get
        Set(ByVal Value As System.String)
            p_upd_time = Value
        End Set
    End Property
    Public Property emp_cd() As System.String
        Get
            Return p_emp_cd
        End Get
        Set(ByVal Value As System.String)
            p_emp_cd = Value
        End Set
    End Property


#End Region

    Public Shared Function CreateWPPE(ByVal p_details As List(Of WPPEBiz)) As Boolean
        Return WPPEDac.InsertWPPE(p_details)
    End Function

    Public Shared Function GetWPPEStoreGroup(ByVal p_co_cd As String) As DataSet
        Return WPPEDac.GetStoreGroup(p_co_cd)
    End Function
    Public Shared Function isValid_co_cd(ByVal p_co_cd As String, ByVal p_emp_init As String) As String
        Return WPPEDac.isValid_co_cd(p_co_cd, p_emp_init)
    End Function
    Public Shared Function isvalidSKU(ByVal p_SKU As String, ByVal p_emp_init As String) As String
        Return WPPEDac.isvalidSKU(p_SKU, p_emp_init)
    End Function
    Public Shared Function isvalidPromotionCode(ByVal po_cd As String) As String
        Return WPPEDac.isvalidPromotionCode(po_cd)
    End Function

    Public Shared Function GetSearchResult(ByVal curWPPE As WPPEBiz, ByVal p_vsn As String) As DataSet
        Return WPPEDac.GetSearchResult(curWPPE, p_vsn)
    End Function

    Public Shared Function GetPromotionDesc(ByVal po_cd As String) As DataSet
        Return WPPEDac.GetPromotionDesc(po_cd)
    End Function
End Class
