﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Data.OracleClient

''' <summary>
''' This class encapsulates any business logic pertaining to invenotry related functions. e.g
''' reservations, availability, etc.
''' </summary>

Public Class InventoryBiz

    Private theInventoryDac As InventoryDac
    Private theTranDac As TransportationDac
    Private theTranBiz As TransportationBiz = New TransportationBiz()

    ''' <summary>
    ''' Determines based on the input whether the zone code is required for this transaction; 
    ''' 1) if zone priority fill is enabled, then zone code is required
    ''' 2) if ARS store is being processed for a delivery, then zone code is required 
    ''' </summary>
    ''' <param name="puDel">pickup/delivery flag, either P or D</param>
    ''' <param name="isArsEnabled">true indicates Advanced Res logic is enabled for current store in process</param>
    ''' <param name="ordTpCd">order type code, like SAL, CRM, MDB, MCR, RES</param>
    ''' <returns>true if the zone code is required; false otherwise</returns>
    Public Function RequireZoneCd(ByVal puDel As String, ByVal isArsEnabled As Boolean, ByVal ordTpCd As String) As Boolean

        Dim rtnVal As Boolean = False

        If (SysPms.isPriorityFillByZone OrElse isArsEnabled OrElse AppConstants.DELIVERY.Equals(puDel)) AndAlso
            Not (AppConstants.Order.TYPE_MCR = ordTpCd OrElse AppConstants.Order.TYPE_MDB = ordTpCd) Then

            rtnVal = True
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' gets the zone code for the processing based on the available input
    ''' 1) calls standard pickup/delivery extraction which uses store zip for pickup and customer zip for delivery to get the zip code
    ''' 2) if zip code found, then gets the zones for the zip and takes the first one
    '''     a) if no zone code is found for the zip and the syspm in POS+ set to check all zones, then takes the first one from all the zones
    ''' 3) if zip code not found based on normal logic, then gets the first zone code for the pickup/delivery store provided
    ''' 4) if zip code still not found, then gets the first zone code for the written (or other) store code provided
    ''' 5) if zip code still not found but all zones are available by POS+ syspm, then gets the first zone code in the zone table
    ''' 6) there is no guarantee that this will provide a zone code but it is unlikely it will not.
    ''' </summary>
    ''' <param name="puDel">pickup/delivery flag, either P or D</param>
    ''' <param name="puDelStoreCd">pickup/delivery store code</param>
    ''' <param name="custZip">customer zip code if customer available</param>
    ''' <param name="delZip">alternative delivery zip code used for delivery zone if nothing avail prior</param>
    ''' <param name="storeCd">the store to get the zone code for if no other methods are successful, maybe written store</param>
    ''' <returns>the zone code found</returns>
    Public Function GetPriFillZoneCode(ByVal puDel As String, ByVal puDelStoreCd As String, ByVal custZip As String, ByVal delZip As Object, ByVal storeCd As String) As String

        Dim zoneCd As String = ""

        Dim zipCd As String = theTranBiz.GetPuDelZip(puDel, puDelStoreCd, custZip, delZip)

        If zipCd.isNotEmpty Then

            Dim zoneDs As DataSet = theTranBiz.GetZones4ZipOrAll(zipCd)

            Dim numRows As Integer = zoneDs.Tables(0).Rows.Count
            ' if there is one, we'll take it, if more than one, we'll take the first
            If numRows > 0 Then

                zoneCd = zoneDs.Tables(0).Rows(0)("ZONE_CD").ToString()
            End If
        End If
        ' last ditch effort - get based on store code
        If zoneCd.isEmpty Then

            zoneCd = GetInventoryDac().GetPriFillZoneCode(puDelStoreCd)
            If zoneCd.isEmpty Then

                zoneCd = GetInventoryDac().GetPriFillZoneCode(storeCd)
                If zoneCd.isEmpty AndAlso ConfigurationManager.AppSettings("open_zones").ToString = "Y" Then

                    Dim zoneDs As DataSet = GetTranDac.GetZoneCodes(True)
                    If SystemUtils.dataSetHasRows(zoneDs) AndAlso SystemUtils.dataRowIsNotEmpty(zoneDs.Tables(0).Rows(0)) Then

                        zoneCd = zoneDs.Tables(0).Rows(0)("ZONE_CD")
                    End If
                End If
                'if we don't have it now, then that's all we can do
            End If
        End If

        Return zoneCd
    End Function

    ''' <summary>
    ''' Gets the Lead days defined for the ITEM passed in.
    ''' </summary>
    ''' <param name="itemCode">the item code to get the lead days for</param>
    ''' <param name="storeCode">store to be used when retrieving the lead days</param>
    ''' <returns>number of lead days for the item and store passed in</returns>
    Public Function GetItemLeadDays(ByVal itemCode As String, ByVal storeCode As String) As Integer
        Return GetInventoryDac().GetItemLeadDays(itemCode, storeCode)
    End Function

    ''' <summary>
    ''' Gets the Lead days defined for the zone passed in.
    ''' </summary>
    ''' <param name="zoneCd">the zone code to get the lead days for</param>
    ''' <returns>number of lead days for the zone passed in</returns>
    Public Function GetZoneLeadDays(ByVal zoneCd As String) As Integer
        Return GetInventoryDac().GetZoneLeadDays(zoneCd)
    End Function

    ''' <summary>
    ''' Extracts the warehouse inventory details for a SKU; if priority fill enabled, will use 
    ''' </summary>
    ''' <param name="req">object of information used for the request of the priority fill information</param>
    ''' <returns>dataset of warehouse inventory rows</returns>
    Public Function GetInvPriFill(ByVal req As PriFillReq) As DataSet

        Dim puDel As String = req.puDel
        Dim zoneCd As String = req.zoneCd
        Dim storeCd As String = req.puDelStoreCd

        If storeCd = String.Empty Then
            storeCd = req.homeStoreCd
            puDel = "D"
        End If

        If zoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone Then
            zoneCd = GetPriFillZoneCode(puDel, req.puDelStoreCd, "", "", req.homeStoreCd)

            If zoneCd.isEmpty Then  'if zone code is still empty, tries again
                zoneCd = GetPriFillZoneCode(puDel, storeCd, "", "", "")
            End If
        End If

        ' Not a guarantee that zoneCd is not empty but it's a much better chance
        If AppConstants.Loc.TYPE_CD_W = req.locTpCd Then
            Return GetInventoryDac().GetInvPriFill(req.itmCd, puDel, zoneCd, storeCd, req.takeWith)
        Else
            Return GetInventoryDac().GetShwPriFill(req.itmCd, puDel, zoneCd, storeCd, req.takeWith)
        End If

    End Function

    ''' <summary>
    ''' Extracts the warehouse inventory details for a SKU; if priority fill enabled, will use 
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve inventory details for</param>
    ''' <param name="puDel">indicates if the item is placed on a Pickup or Delivery</param>
    ''' <param name="priFillZoneCd">the zone code for P/D.  Required when priority fill by ZONE is used.</param>
    ''' <param name="priFillStoreCd">the store to be used.  Required when priority fill by STORE is used.</param>
    ''' <param name="takenWith">indicates if the merchandise was taken by the customer; Def: 'N'o.</param>
    ''' <returns>dataset of warehouse inventory rows</returns>
    Public Function GetInvPriFill(ByVal itmCd As String, ByVal puDel As String,
                                  ByVal priFillZoneCd As String, ByVal priFillStoreCd As String,
                                  ByVal takenWith As String) As DataSet

        If (priFillZoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone) Then
            priFillZoneCd = GetPriFillZoneCode(puDel, priFillStoreCd, "", "", "")
        End If
        Return GetInventoryDac().GetInvPriFill(itmCd, puDel, priFillZoneCd, priFillStoreCd, takenWith)
    End Function

    ''' <summary>
    ''' Extracts the SHOWROOM inventory details for a SKU; if priority fill enabled, will use 
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve inventory details for</param>
    ''' <param name="puDel">indicates if the item is placed on a Pickup or Delivery</param>
    ''' <param name="priFillZoneCd">the store to be used.  Required when priority fill by ZONE is used.</param>
    ''' <param name="priFillStoreCd">the store to be used.  Required when priority fill by STORE is used.</param>
    ''' <param name="takenWith">indicates if the merchandise was taken by the customer; Def: 'N'o.</param>
    ''' <returns>dataset of showroom inventory details</returns>
    Public Function GetShwPriFill(ByVal itmCd As String, ByVal puDel As String,
                                  ByVal priFillZoneCd As String, ByVal priFillStoreCd As String,
                                  ByVal takenWith As String) As DataSet

        If (priFillZoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone) Then
            priFillZoneCd = GetPriFillZoneCode(puDel, priFillStoreCd, "", "", "")
        End If
        Return GetInventoryDac().GetShwPriFill(itmCd, puDel, priFillZoneCd, priFillStoreCd, takenWith)
    End Function

    ''' <summary>
    ''' Finds out if the store and location passed in, correspond to an 'O'utlet type location
    ''' </summary>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="locCd">the location code</param>
    ''' <returns>TRUE if location is Outlet; FALSE otherwise</returns>
    Public Function IsOutletLocation(ByVal storeCd As String, ByVal locCd As String) As Boolean
        Dim isOutLoc As Boolean = False
        If (storeCd.isNotEmpty AndAlso locCd.isNotEmpty) Then
            isOutLoc = GetInventoryDac().IsOutletLocation(storeCd, locCd)
        End If
        Return isOutLoc
    End Function

    ''' <summary>
    ''' Finds out if the store and location passed in, correspond to an 'O'utlet type location
    ''' </summary>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="locCd">the location code</param>
    ''' <param name="command">command object</param>
    ''' <returns>TRUE if location is Outlet; FALSE otherwise</returns>
    Public Function IsOutletLocation(ByVal storeCd As String, ByVal locCd As String, ByRef command As OracleCommand) As Boolean            
        IsOutletLocation = GetInventoryDac().IsOutletLocation(storeCd, locCd, command)
        Return IsOutletLocation
    End Function

    ''' <summary>
    ''' Retrieves from the OUT_CD table the outlet codes and descriptions
    ''' </summary>
    ''' <returns>a dataaset including the outlet codes and descriptions</returns>
    Public Function GetOutletCodes() As DataSet
        Return GetInventoryDac().GetOutletCodes()
    End Function

    ''' <summary>
    ''' Retrieves from the OUT table, the outlet IDs that match the ITEM/STORE/LOCATION
    ''' passed in, excluding the ones in the list passed in.
    ''' </summary>
    ''' <param name="itemCd">item code to fetch the outlet IDs for</param>
    ''' <param name="storeCd">store code to fetch the outlet IDs for</param>
    ''' <param name="locCd">location code to fetch the outlet IDs for</param>
    ''' <param name="outletIdsToExclude">comma delimited list of outlet IDs to exclude during retrieval</param>
    ''' <returns>A dataset including the outlet IDs found; or an empty dataset if none found</returns>
    Public Function GetOutletIdsAvailable(ByVal itemCd As String,
                                          ByVal storeCd As String,
                                          ByVal locCd As String,
                                          ByVal outletIdsToExclude As String) As DataSet

        Return GetInventoryDac().GetOutletIdsAvailable(itemCd, storeCd, locCd, outletIdsToExclude)
    End Function

    ''' <summary>
    ''' Retrieves from the INV_XREF table, the serial numbers that match the ITEM/STORE/LOCATION
    ''' passed in, excluding the ones in the list passed in.
    ''' </summary>
    ''' <param name="itemCd">item code to fetch the serial numbers for</param>
    ''' <param name="storeCd">store code to fetch the serial numbers for</param>
    ''' <param name="locCd">location code to fetch the serial numbers for</param>
    ''' <param name="serialNumbersToExclude">comma delimited list of serial numbers to exclude during retrieval</param>
    ''' <returns>A dataset including the serial numbers found; or an empty dataset if none found</returns>
    Public Function GetSerialNumbersAvailable(ByVal itemCd As String,
                                              ByVal storeCd As String,
                                              ByVal locCd As String,
                                              ByVal serialNumbersToExclude As String) As DataSet

        Return GetInventoryDac().GetSerialNumbersAvailable(itemCd, storeCd, locCd, serialNumbersToExclude)
    End Function

    ''' <summary>
    ''' Returns Picking Status flag from RF.  Validates that RF is enabled and the input store is an RF warehouse
    ''' Extracts non-void RF_PICK records for the doc and line and SKU.  
    ''' </summary>
    ''' <param name="docNum">the document number where the item belongs to</param>
    ''' <param name="lnNum">the line number where the item belongs to</param>
    ''' <param name="itmCd">the item code to get the RF status for</param>
    ''' <param name="storeCd">the store code to be used</param>
    ''' <returns>
    ''' If all records have a checked status then return status output will be Checked.
    ''' If all records have a (not blank) status but at least one has an exception status, then the return status will be Exception.
    ''' If all records have a (not blank) status and no exceptions (and not all checked), then the return status will be Picked.
    ''' If not all records have a (not blank) status then the return status will be Scheduled.
    ''' If there are no RF pick records for the doc and line and SKU, then the return status will be blank. 
    ''' </returns>
    Public Function GetRfPickStatus(ByVal docNum As String, ByVal lnNum As String,
                                    ByVal itmCd As String, ByVal storeCd As String, ByVal ptsRfStatus As String) As String

        Dim pickStatus As String = ""
        If storeCd.isNotEmpty AndAlso IsRfStore(storeCd) Then  ' RF store confirms also if RF is enabled
            pickStatus = GetInventoryDac().GetRfPickStatus(docNum, lnNum, itmCd, storeCd)
        ElseIf Convert.ToBoolean(ptsRfStatus) Then
            pickStatus = GetInventoryDac().GetNonRfStatus(docNum, lnNum)
        End If
        Return pickStatus

    End Function

    ''' <summary>
    ''' Retrieves the inventory found in the ITM_FIFL table for the item and 
    ''' location type passed id.
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve inventory for</param>
    ''' <param name="locTpCd">refer to LOC.LOC_TP_CD for valid types</param>
    ''' <returns>a dataset including the inventory details; or an empty dataset if none found</returns>
    Public Function GetStoreData(ByVal itmCd As String, ByVal locTpCd As String) As DataSet
        Return GetInventoryDac().GetStoreData(itmCd, locTpCd)
    End Function

    ''' <summary>
    ''' Retrieves from the SO and SO_LN tables, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve SALES records for</param>
    ''' <returns>A dataset including the SALES records found; or an empty dataset if none found</returns>
    Public Function GetSalesData(ByVal itmCd As String) As DataSet
        Return GetInventoryDac().GetSalesData(itmCd)
    End Function

    ''' <summary>
    ''' Retrieves from the PO and related tables, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve PO records for</param>
    ''' <returns>A dataset including the PO records found; or an empty dataset if none found</returns>
    Public Function GetPoData(ByVal itmCd As String) As DataSet
        Return GetInventoryDac().GetPoData(itmCd)
    End Function

    ''' <summary>
    ''' Retrieves from the OUT table, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve outlet records for</param>
    ''' <returns>A dataset including the outlet records found; or an empty dataset if none found</returns>
    Public Function GetOutletData(ByVal itmCd As String) As DataSet
        Return GetInventoryDac().GetOutletData(itmCd)
    End Function

    ''' <summary>
    ''' Retrieves from the IST table, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve IST records for</param>
    ''' <returns>A dataset including the IST records found; or an empty dataset if none found</returns>
    Public Function GetIstData(ByVal itmCd As String) As DataSet
        Return GetInventoryDac().GetIstData(itmCd)
    End Function

    ''' <summary>
    ''' Finds out if the location passed in is valid, for the corresponding store
    ''' </summary>
    ''' <param name="storeCd">the store code to check the location code against</param>
    ''' <param name="locCd">the location code to validate</param>
    ''' <returns>TRUE if location is a valid one for the store; FALSE otherwise</returns>
    Public Function IsValidLocation(ByVal storeCd As String, locCd As String) As Boolean
        Dim isValid As Boolean = False
        If (storeCd.isNotEmpty AndAlso locCd.isNotEmpty) Then
            isValid = GetInventoryDac().IsValidLocation(storeCd, locCd)
        End If
        Return isValid
    End Function

    ''' <summary>
    ''' Determines if the PO has been confirmed and the confirmation status is open
    ''' </summary>
    ''' <param name="poCd">the PO number to be verify</param>
    ''' <returns>TRUE if PO has been confirmed and confirmation status is open; FALSE otherwise</returns>
    Public Function IsPOConfirmed(ByVal poCd As String) As Boolean
        Return GetInventoryDac().IsPOConfirmed(poCd)
    End Function

    ''' <summary>
    ''' Retrieves ALL records from the ITM_RES table, that match the item cd
    ''' passed in.   Returns an empty dataset if no matches are found
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve the soft-reservations for</param>
    ''' <returns>A dataset filled with the soft-reservations details</returns>
    Public Function GetReservations(ByVal itmCd As String) As DataSet
        Return GetInventoryDac().GetReservations(itmCd)
    End Function

    ''' <summary>
    ''' Creates a SOFT reservation.  Records are added to the ITEM_RES table for every
    ''' different store-location-quantity combination.  Further auto-reservations will
    ''' consider these reservations as non-available inventory until these rows are removed
    ''' either because a 'hard' reservation gets created, or line reserved is unreserved
    ''' </summary>
    ''' <param name="request">custom object with details to perform a soft reservation</param>
    ''' <returns>custom object with the results of the reservation</returns>
    Public Function RequestSoftReserve(ByVal request As ResRequestDtc) As ResResponseDtc

        If (request.isAutoSoftRes) Then
            Return GetInventoryDac().RequestAutoSoftRes(request)
        Else
            Return GetInventoryDac().RequestDirectedSoftRes(request)
        End If

    End Function

    ''' <summary>
    ''' Removes the ITM_RES records that match the details passed in
    ''' </summary>
    ''' <param name="reservations">details require to remove a soft reservation</param>
    Public Sub RemoveSoftRes(ByVal reservations As List(Of String))
        GetInventoryDac().RemoveSoftRes(reservations)
    End Sub

    ''' <summary>
    ''' Removes the ITM_RES records that match the details passed in
    ''' </summary>
    ''' <param name="request">details require to remove a soft reservation</param>
    Public Sub RemoveSoftRes(ByVal request As UnResRequestDtc)
        GetInventoryDac().RemoveSoftRes(request)
    End Sub

    ''' <summary>
    ''' Calls an API to calculate availability for the specified item.
    ''' </summary>
    ''' <param name="request">details required to calculate availability</param>
    ''' <returns>object with the results of the API call</returns>
    Public Function CalcAvail(ByVal request As CalcAvailRequestDtc) As String

        Dim response As String = ""
        Dim results As CalcAvailResponseDtc = GetInventoryDac().CalcAvail(request)
        Dim rowCnt As Integer = results.calcAvailData.Rows.Count

        If (rowCnt > 0) Then
            Dim availDt As String = ""
            Dim src As String = ""
            Dim bldCmnt As New StringBuilder("<pre>Availability: <br>Qty on Estim. Date from Source: ")

            For Each row As DataRow In results.calcAvailData.Rows
                src = row("FILL_TYPE").ToString + ""
                If src = "FIF" Then
                    src = "Inventory"
                ElseIf src = "POI" OrElse src = "SPO" Then
                    src = "PO/IST " + row("POS_IST_DOC").ToString
                Else
                    src = ""
                End If
                availDt = row("AVAIL_DT").ToString + ""
                If IsDate(availDt) Then availDt = FormatDateTime(availDt, DateFormat.ShortDate)
                bldCmnt.Append(vbCrLf).Append((row("FILL_QTY").ToString + "").PadLeft(6)).Append(availDt.PadLeft(15)).Append(src.PadLeft(21))
            Next
            bldCmnt.Append("</pre>")
            response = bldCmnt.ToString
        Else
            response = (Resources.POSMessages.MSG0001)
        End If
        Return response

    End Function

    ''' <summary>
    ''' Calls an API to calculate availability for a store that has been setup to use
    ''' advanced reservation logic (ARS). 
    ''' </summary>
    ''' <param name="itmCd">the item being checked for availability</param>
    ''' <param name="pickupDelFlag">Indicates whether it is a 'P'ickup or 'D'elivery</param>
    ''' <param name="zoneCd">the zone of the pickup store or delivery postal code</param>
    ''' <param name="qty">the qty needed</param>
    ''' <param name="storeCd">the pickup/delivery store</param>
    ''' <returns>the available date for the specified item</returns>
    Public Function CalcAvailARS(itmCd As String, pickupDelFlag As String,
                                 zoneCd As String, qty As Double, storeCd As String) As Date

        Return GetInventoryDac().CalcAvailARS(itmCd, pickupDelFlag, zoneCd, qty, storeCd)

    End Function    

    ''' <summary>
    ''' Validates the inventory for an ARS enabled store as per the business rules defined in the API. The API checks things
    ''' such as the delv/pickup date being valid, or if based on current inventory, the line needs to be linked to
    ''' a PO, or can be reserved, etc.
    ''' </summary>
    ''' <param name="request">the request object with all the data needed for checking inventory</param>
    ''' <returns>the check inventory response object.</returns>
    Public Function CheckInventoryForARS(ByVal request As CheckInvRequestDtc) As CheckInvResponseDtc
        Return GetInventoryDac().CheckInventoryForARS(request)
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' 'Returns true if the store provided is an RF warehouse, false otherwise
    ''' </summary>
    ''' <returns>true if the store is RF enabled, false otherwise</returns>
    ' MM-7088
    Public Function IsRfStore(ByVal storeCd As String) As Boolean

        Dim isStoreRfEnabled As Boolean = False
        If SysPms.isRFEnabled Then
            isStoreRfEnabled = GetInventoryDac().IsRfStore(storeCd)
        End If
        Return isStoreRfEnabled

    End Function

    Private Function GetInventoryDac() As InventoryDac
        If (theInventoryDac Is Nothing) Then
            theInventoryDac = New InventoryDac()
        End If
        Return theInventoryDac
    End Function

    Private Function GetTranDac() As TransportationDac
        If (theTranDac Is Nothing) Then
            theTranDac = New TransportationDac()
        End If
        Return theTranDac
    End Function

    Public Function GetPTSRFStatus() As Boolean
        Dim ptsRf As Boolean = False
        ptsRf = GetInventoryDac().GetPTSRFStatus()
        Return ptsRf

    End Function


    ''' <summary>
    ''' Alice add for request 151
    ''' </summary>
    Public Function GetBackOrder(ByVal del_doc_num As String) As String
        Dim backorder As String = "NO"
        Dim backorderDS As DataSet = GetInventoryDac.GetBackOrder(del_doc_num)
        If backorderDS Is Nothing Then
            backorder = "NO"
        Else
            If backorderDS.Tables(0).Rows.Count > 0 Then
                For Each Row As DataRow In backorderDS.Tables(0).Rows
                    If Row("backorder").ToString() = "YES" Then 'if one item of order is backorder then whole order is backorder
                        backorder = "YES"
                        Exit For
                    End If
                Next
            Else
                backorder = "NO"
            End If

        End If
        Return backorder
    End Function
End Class
