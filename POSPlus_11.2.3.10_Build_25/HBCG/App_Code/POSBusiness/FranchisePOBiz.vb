﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
''' <summary>
''' Feb 08, 2018 - mariam
''' For Franchise Purchase Order Entry
''' 
''' </summary>
Public Class FranchisePOBiz

    Private objFrPODac As FranchisePODac

    Private Function GetFrPODac() As FranchisePODac
        If (objFrPODac Is Nothing) Then
            objFrPODac = New FranchisePODac()
        End If
        Return objFrPODac
    End Function
    Public Function GetStores_MCCL1(ByVal p_supUserFlag As String, ByVal p_userType As String, ByVal p_coCD As String) As DataSet
        Return GetFrPODac().GetStores_MCCL1(p_supUserFlag, p_userType, p_coCD)
    End Function

    Public Function GetStores_FRPOE(ByVal p_supUserFlag As String, ByVal emp_init As String) As DataSet
        Return GetFrPODac().GetStores_FROPE(p_supUserFlag, emp_init)
    End Function

    Public Function GetBuyersByCompCD(ByVal p_CompanyCode As String) As DataSet
        Return GetFrPODac.GetBuyersByCompCD(p_CompanyCode)
    End Function
    Public Function GetVendorCD_LEFranchise() As DataSet
        Return GetFrPODac.GetVendorCD_LEFranchise()
    End Function
    Public Function GetOrderingFromCorpStoreCode(ByVal p_FrStoreNum As String) As DataSet
        Return GetFrPODac.GetOrderingFromCorpStoreCode(p_FrStoreNum)
    End Function

    Public Function GetZoneCodesForCorpStore(ByVal p_StoreCode As String) As DataSet
        Return GetFrPODac.GetZoneCodesForCorpStore(p_StoreCode)
    End Function
    Public Function GetVSNByItemSKU(ByVal p_item As String) As DataSet
        Return GetFrPODac.GetVSNByItemSKU(p_item)
    End Function
    Public Function GetPOCostByItemAndStore(ByVal p_item As String, ByVal p_storeCd As String) As Double
        Return GetFrPODac.GetPOCostByItemAndStore(p_item, p_storeCd)
    End Function
    Public Function GetBareCost(ByVal p_item As String, ByVal p_orderingstoreCd As String) As Double
        Return FranchisePODac.GetBareCost(p_item, p_orderingstoreCd)
    End Function
    Public Function GetPercentageValue(ByVal p_item As String, ByVal p_storeCd As String) As Double
        Return FranchisePODac.GetPercentageValue(p_item, p_storeCd)
    End Function
    Public Function GetLandedCost(ByVal par_itm As String, ByVal par_storecode As String) As Double
        Return GetFrPODac.GetLandedCost(par_itm, par_storecode)
    End Function
    Public Function GetTaxCodeByVendorCd(ByVal p_vendorCd As String) As String
        Return GetFrPODac.GetTaxCodeByVendorCd(p_vendorCd)
    End Function
    Public Function GetTaxCodeByCustCd(ByVal p_custCd As String) As String
        Return GetFrPODac.GetTaxCodeByCustCd(p_custCd)
    End Function
    'Mariam Added
    Public Function GetZonesForCorpStoreCd_Franchise(ByVal p_CorpStoreCode As String) As DataSet
        Return GetFrPODac.GetZonesForCorpStoreCd_Franchise(p_CorpStoreCode)
    End Function
    'Alice Added
    Public Function GetZonesForCorpStoreCd_Franchise1(ByVal p_CorpStoreCode As String) As DataSet
        Return GetFrPODac.GetZonesForCorpStoreCd_Franchise1(p_CorpStoreCode)
    End Function
    'Alice Added
    Public Function GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(ByVal p_CorpStoreCode As String, ByVal p_FranchiseStoreCd As String) As DataSet
        Return GetFrPODac.GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(p_CorpStoreCode, p_FranchiseStoreCd)
    End Function
    'Mariam Added
    Public Function GetCorpStores() As DataSet
        Return GetFrPODac().GetCorpStores()
    End Function
    'Alice added
    Public Function GetCorpStoresforFrPOEntry(ByVal p_supUserFlag As String, ByVal fr_store_cd As String) As DataSet
        Return GetFrPODac().GetCorpStoresforFrPOEntry(p_supUserFlag, fr_store_cd)
    End Function
    'Mariam Added
    Public Function CreateFranchisePO(ByVal p_details As List(Of FRShippingSch_DetDtc), ByRef p_transdet As FRShippingSch_TransDtc) As String
        Dim str As String
        str = GetFrPODac.CreateFR_ShippingSchedule(p_details, p_transdet)
        Return str
    End Function
   


    Public Function GetApprUserInitByPass(ByVal p_password As String) As String
        Return GetFrPODac.GetApprUserInitByPass(p_password)
    End Function

   

    ''' <summary>
    ''' Feb 08, 2018 - mariam
    ''' Insert Purchase Order Items into FRPO and FRPO_LN tables
    ''' 
    ''' </summary>
    Public Function CreateFranchisePO(ByVal p_cartItems As List(Of CartItemDtc), ByRef p_transdet As TransDtc) As String
        Dim str As String
        str = GetFrPODac.CreateFranchisePO(p_cartItems, p_transdet)
        Return str
    End Function
    'check is Valid item
    Public Function isvaliditmFR(ByVal p_itm As String, ByVal p_emp_init As String) As String
        Return GetFrPODac.isvaliditmFR(p_itm, p_emp_init)
    End Function

    'prevent any item where the item inventory type = ‘N’; only allow if the item inventory type = ‘Y’
    Public Function isvalidItemforInventoryType(ByVal p_itm As String) As String
        Return GetFrPODac.isvalidItemforInventoryType(p_itm)
    End Function


    'check is Valid item
    Public Function isvalidApproverPassword(ByVal p_Approverpassword As String) As String
        Return GetFrPODac.isvalidApproverPassword(p_Approverpassword)
    End Function

    Public Function GetTableRowCount(ByVal p_TableName As String) As UInt16
        Return GetFrPODac.GetTableRowCount(p_TableName)
    End Function
    Public Function GenerateConfirmationNum(ByVal p_FRStoreCd) As String
        Const Const_LEON As String = "LEON"
        Const Const_TABLENAME As String = "FRPO"
        Const Const_UniqueSeq_AA As String = "AA"
        Const Const_COLUMNNAME As String = "PO_CD"


        Dim objFRPOBiz As FranchisePOBiz = New FranchisePOBiz

        Dim confirmationNum As String = String.Empty
        Dim UniqueSeq As String = String.Empty
        Dim mon As String = String.Empty
        Dim dd As String = String.Empty
        Dim y As String = String.Empty
        Dim lastConfirmationNum As String = String.Empty
        Dim colName As String = String.Empty
        Dim tableName As String = String.Empty

        'Dim objTrans As OracleTransaction = Nothing
        Try
            ''generate Confirmation # - 13 characters
            '1 - mm ; 2-dd; 3-y; 4-FranchiseStoreCode; 5-LEON; 6-2 char system generated AA., AB...ZZ, AA(rollback after ZZ)
            mon = Today.Date.Month.ToString().PadLeft(2, "0")
            dd = Today.Date.Day.ToString().PadLeft(2, "0")
            y = Today.Date.Year.ToString().Substring(3)             'last one digit of year if 2018, need only 8

            'Get these 2 chars from select last 2 digits of usr_fld_1
            lastConfirmationNum = GetValueByLastRow(Const_COLUMNNAME, Const_TABLENAME)

            If lastConfirmationNum.Trim().Length = 0 Then
                UniqueSeq = Const_UniqueSeq_AA
            Else
                UniqueSeq = objFRPOBiz.GenerateSeqChar(lastConfirmationNum.Substring(Math.Max(0, lastConfirmationNum.Length - 2)))
            End If

            confirmationNum = mon + dd + y.Substring(0, 1) + p_FRStoreCd + Const_LEON + UniqueSeq
        Catch ex As Exception
            Throw ex
        End Try
        Return confirmationNum
    End Function
    Public Function GetValueByLastRow(ByVal p_columnName As String, ByVal p_tableName As String) As String
        Return GetFrPODac.GetValueByLastRow(p_columnName, p_tableName)
    End Function
    'generate 2 unique characters
    Public Function GenerateSeqChar(ByVal strInput As String) As String
        'GenerateSeqChar = ConvertBase10ToBase26(ConvertBase26ToBase10(str) + 1)
        Static ltrs As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Dim first As Integer = ltrs.IndexOf(strInput(0))
        Dim last As Integer = ltrs.IndexOf(strInput(1))
        last += 1
        If last = ltrs.Length Then
            last = 0
            first += 1
        End If
        If first = ltrs.Length Then
            first = 0
            Return ltrs(first) & ltrs(last)
        End If

        Return ltrs(first) & ltrs(last)
    End Function

    ''Generate Confirmation# for Franchise Shipping Schedule, Mariam added
    Public Function GenerateConfirmNum_FSS() As String
        Const Const_LEON As String = "LEON"
        Const Const_TABLENAME As String = "Fr_Shipping_Schedule"
        Const Const_UniqueSeq_AA As String = "AA"
        Const Const_COLUMNNAME As String = "CONFIRM_NUM"
        Const Const_Zero As String = "00"

        Dim objFRPOBiz As FranchisePOBiz = New FranchisePOBiz

        Dim confirmationNum As String = String.Empty
        Dim UniqueSeq As String = String.Empty
        Dim mon As String = String.Empty
        Dim dd As String = String.Empty
        Dim y As String = String.Empty
        Dim lastConfirmationNum As String = String.Empty
        Dim colName As String = String.Empty
        Dim tableName As String = String.Empty

        'Dim objTrans As OracleTransaction = Nothing
        Try
            ''generate Confirmation # - 13 characters
            '1 - mm ; 2-dd; 3-y; 4-FranchiseStoreCode; 5-LEON; 6-2 char system generated AA., AB...ZZ, AA(rollback after ZZ)
            mon = Today.Date.Month.ToString().PadLeft(2, "0")
            dd = Today.Date.Day.ToString().PadLeft(2, "0")
            y = Today.Date.Year.ToString().Substring(3)             'last one digit of year if 2018, need only 8

            'Get these 2 chars from select last 2 digits of usr_fld_1
            lastConfirmationNum = GetValueByLastRow(Const_COLUMNNAME, Const_TABLENAME)

            If lastConfirmationNum.Trim().Length = 0 Or lastConfirmationNum = "0" Then
                UniqueSeq = Const_UniqueSeq_AA
            Else
                UniqueSeq = objFRPOBiz.GenerateSeqChar(lastConfirmationNum.Substring(Math.Max(0, lastConfirmationNum.Length - 2)))
            End If

            confirmationNum = mon + dd + y.Substring(0, 1) + Const_Zero + UniqueSeq
        Catch ex As Exception
            Throw ex
        End Try
        Return confirmationNum
    End Function
End Class
