Public Class XtraReport2
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents winControlContainer1 As DevExpress.XtraReports.UI.WinControlContainer
    Private WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrCheckBox1 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrCheckBox6 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrCheckBox4 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrCheckBox3 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_setup As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del_doc_num_bar As DevExpress.XtraReports.UI.XRBarCode

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "XtraReport2.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.XtraReport2.ResourceManager
        Dim code39Generator1 As DevExpress.XtraPrinting.BarCode.Code39Generator = New DevExpress.XtraPrinting.BarCode.Code39Generator
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_ext = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ret = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xr_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tax = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_setup = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel25 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel24 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel23 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel22 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel21 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel20 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCheckBox6 = New DevExpress.XtraReports.UI.XRCheckBox
        Me.xrCheckBox4 = New DevExpress.XtraReports.UI.XRCheckBox
        Me.xrCheckBox3 = New DevExpress.XtraReports.UI.XRCheckBox
        Me.xrCheckBox1 = New DevExpress.XtraReports.UI.XRCheckBox
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.winControlContainer1 = New DevExpress.XtraReports.UI.WinControlContainer
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.xr_del_doc_num_bar = New DevExpress.XtraReports.UI.XRBarCode
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_ext, Me.xr_ret, Me.xr_desc, Me.xr_loc, Me.xr_store, Me.xr_SKU, Me.xr_vsn, Me.xr_qty})
        Me.Detail.Height = 38
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ext
        '
        Me.xr_ext.Location = New System.Drawing.Point(900, 0)
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.Size = New System.Drawing.Size(100, 17)
        Me.xr_ext.Text = "EXTENDED"
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ret
        '
        Me.xr_ret.Location = New System.Drawing.Point(792, 0)
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.Size = New System.Drawing.Size(100, 17)
        Me.xr_ret.Text = "RETAIL"
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_desc
        '
        Me.xr_desc.Location = New System.Drawing.Point(242, 17)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.Size = New System.Drawing.Size(425, 17)
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_loc
        '
        Me.xr_loc.Location = New System.Drawing.Point(175, 0)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.Size = New System.Drawing.Size(33, 17)
        Me.xr_loc.Text = "LOC"
        '
        'xr_store
        '
        Me.xr_store.Location = New System.Drawing.Point(133, 0)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.Size = New System.Drawing.Size(33, 17)
        Me.xr_store.Text = "ST"
        '
        'xr_SKU
        '
        Me.xr_SKU.Location = New System.Drawing.Point(50, 0)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.Size = New System.Drawing.Size(75, 17)
        Me.xr_SKU.Text = "SKU"
        '
        'xr_vsn
        '
        Me.xr_vsn.Location = New System.Drawing.Point(242, 0)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.Size = New System.Drawing.Size(425, 17)
        Me.xr_vsn.Text = "VSN"
        '
        'xr_qty
        '
        Me.xr_qty.Location = New System.Drawing.Point(8, 0)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.Size = New System.Drawing.Size(33, 17)
        Me.xr_qty.Text = "QTY"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLine4, Me.xrLine3, Me.xrLabel18, Me.xrLabel17, Me.xrLabel16, Me.xrLabel15, Me.xrLabel14, Me.xrLabel13, Me.xrLabel12, Me.xr_store_phone, Me.xr_slsp, Me.xrLabel7, Me.xr_pd, Me.xr_pu_del_dt, Me.xrLabel6, Me.xr_cust_cd, Me.xrLabel5, Me.xrLabel4, Me.xrLabel2, Me.xr_b_phone, Me.xrLabel3, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_del_doc_num, Me.xr_lname, Me.xr_fname, Me.xr_so_wr_dt, Me.xr_addr1})
        Me.PageHeader.Height = 158
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLine4
        '
        Me.xrLine4.Location = New System.Drawing.Point(0, 150)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.Size = New System.Drawing.Size(1000, 8)
        '
        'xrLine3
        '
        Me.xrLine3.Location = New System.Drawing.Point(0, 124)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.Size = New System.Drawing.Size(1000, 8)
        '
        'xrLabel18
        '
        Me.xrLabel18.Location = New System.Drawing.Point(175, 133)
        Me.xrLabel18.Name = "xrLabel18"
        Me.xrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel18.Size = New System.Drawing.Size(33, 17)
        Me.xrLabel18.Text = "LOC"
        '
        'xrLabel17
        '
        Me.xrLabel17.Location = New System.Drawing.Point(133, 133)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.Size = New System.Drawing.Size(33, 17)
        Me.xrLabel17.Text = "ST"
        '
        'xrLabel16
        '
        Me.xrLabel16.Location = New System.Drawing.Point(900, 133)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel16.Text = "EXTENDED"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel15
        '
        Me.xrLabel15.Location = New System.Drawing.Point(792, 133)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel15.Text = "RETAIL"
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel14
        '
        Me.xrLabel14.Location = New System.Drawing.Point(50, 133)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel14.Text = "SKU"
        '
        'xrLabel13
        '
        Me.xrLabel13.Location = New System.Drawing.Point(242, 133)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel13.Text = "DESCRIPTION"
        '
        'xrLabel12
        '
        Me.xrLabel12.Location = New System.Drawing.Point(8, 133)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.Size = New System.Drawing.Size(33, 17)
        Me.xrLabel12.Text = "QTY"
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Location = New System.Drawing.Point(792, 102)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.Size = New System.Drawing.Size(208, 17)
        Me.xr_store_phone.Text = "xr_store_phone"
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_slsp
        '
        Me.xr_slsp.Location = New System.Drawing.Point(792, 85)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.Size = New System.Drawing.Size(208, 17)
        Me.xr_slsp.Text = "xr_slsp"
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel7
        '
        Me.xrLabel7.Location = New System.Drawing.Point(792, 68)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.Size = New System.Drawing.Size(208, 17)
        Me.xrLabel7.Text = "Sales Associate:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_pd
        '
        Me.xr_pd.Location = New System.Drawing.Point(792, 51)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.Size = New System.Drawing.Size(100, 17)
        Me.xr_pd.Text = "xr_pd"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Location = New System.Drawing.Point(900, 51)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.Size = New System.Drawing.Size(100, 17)
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xrLabel6
        '
        Me.xrLabel6.Location = New System.Drawing.Point(792, 34)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel6.Text = "Sale Date:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.Location = New System.Drawing.Point(900, 17)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.Size = New System.Drawing.Size(100, 17)
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        '
        'xrLabel5
        '
        Me.xrLabel5.Location = New System.Drawing.Point(792, 17)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel5.Text = "Account #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel4
        '
        Me.xrLabel4.Location = New System.Drawing.Point(792, 0)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel4.Text = "Order #:"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel2
        '
        Me.xrLabel2.Location = New System.Drawing.Point(0, 68)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.Size = New System.Drawing.Size(33, 16)
        Me.xrLabel2.Text = "(H) "
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Location = New System.Drawing.Point(33, 85)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.Size = New System.Drawing.Size(217, 17)
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xrLabel3
        '
        Me.xrLabel3.Location = New System.Drawing.Point(0, 85)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.Size = New System.Drawing.Size(33, 16)
        Me.xrLabel3.Text = "(B) "
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Location = New System.Drawing.Point(33, 68)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.Size = New System.Drawing.Size(217, 17)
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Location = New System.Drawing.Point(208, 51)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.Size = New System.Drawing.Size(67, 17)
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Location = New System.Drawing.Point(175, 51)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.Size = New System.Drawing.Size(25, 17)
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Location = New System.Drawing.Point(0, 51)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.Size = New System.Drawing.Size(167, 17)
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Location = New System.Drawing.Point(0, 34)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.Location = New System.Drawing.Point(900, 0)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.Size = New System.Drawing.Size(100, 17)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_lname
        '
        Me.xr_lname.Location = New System.Drawing.Point(142, 0)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.Size = New System.Drawing.Size(133, 17)
        Me.xr_lname.Text = "xr_lname"
        '
        'xr_fname
        '
        Me.xr_fname.Location = New System.Drawing.Point(0, 0)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.Size = New System.Drawing.Size(133, 17)
        Me.xr_fname.Text = "xr_fname"
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.Location = New System.Drawing.Point(900, 34)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.Size = New System.Drawing.Size(100, 17)
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Location = New System.Drawing.Point(0, 17)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrPageInfo1.Location = New System.Drawing.Point(833, 0)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrPageInfo1.Size = New System.Drawing.Size(154, 17)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.Location = New System.Drawing.Point(833, 17)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.Size = New System.Drawing.Size(154, 17)
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_balance, Me.xr_payments, Me.xr_grand_total, Me.xr_tax, Me.xr_setup, Me.xr_del, Me.xr_subtotal, Me.xrLine5, Me.xrLabel25, Me.xrLabel24, Me.xrLabel23, Me.xrLabel22, Me.xrLabel21, Me.xrLabel20, Me.xrLabel19, Me.xrLabel10, Me.xrLine1, Me.xrLabel9, Me.xrCheckBox6, Me.xrCheckBox4, Me.xrCheckBox3, Me.xrCheckBox1})
        Me.ReportFooter.Height = 184
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_balance
        '
        Me.xr_balance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_balance.Location = New System.Drawing.Point(883, 117)
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_balance.Size = New System.Drawing.Size(117, 17)
        Me.xr_balance.Text = "xr_balance"
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_payments
        '
        Me.xr_payments.Location = New System.Drawing.Point(883, 85)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.Size = New System.Drawing.Size(117, 17)
        Me.xr_payments.Text = "xr_payments"
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_grand_total
        '
        Me.xr_grand_total.Location = New System.Drawing.Point(883, 68)
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.Size = New System.Drawing.Size(117, 17)
        Me.xr_grand_total.Text = "xr_grand_total"
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_tax
        '
        Me.xr_tax.Location = New System.Drawing.Point(883, 51)
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.Size = New System.Drawing.Size(117, 17)
        Me.xr_tax.Text = "xr_tax"
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_setup
        '
        Me.xr_setup.Location = New System.Drawing.Point(883, 34)
        Me.xr_setup.Name = "xr_setup"
        Me.xr_setup.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_setup.Size = New System.Drawing.Size(117, 17)
        Me.xr_setup.Text = "xr_setup"
        Me.xr_setup.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_del
        '
        Me.xr_del.Location = New System.Drawing.Point(883, 17)
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.Size = New System.Drawing.Size(117, 17)
        Me.xr_del.Text = "xr_del"
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_subtotal
        '
        Me.xr_subtotal.Location = New System.Drawing.Point(883, 0)
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.Size = New System.Drawing.Size(117, 17)
        Me.xr_subtotal.Text = "xr_subtotal"
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine5
        '
        Me.xrLine5.Location = New System.Drawing.Point(875, 102)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.Size = New System.Drawing.Size(125, 9)
        '
        'xrLabel25
        '
        Me.xrLabel25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xrLabel25.Location = New System.Drawing.Point(775, 117)
        Me.xrLabel25.Name = "xrLabel25"
        Me.xrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel25.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel25.Text = "Balance:"
        Me.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel24
        '
        Me.xrLabel24.Location = New System.Drawing.Point(775, 85)
        Me.xrLabel24.Name = "xrLabel24"
        Me.xrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel24.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel24.Text = "Payments:"
        Me.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel23
        '
        Me.xrLabel23.Location = New System.Drawing.Point(775, 68)
        Me.xrLabel23.Name = "xrLabel23"
        Me.xrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel23.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel23.Text = "Total Sale:"
        Me.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel22
        '
        Me.xrLabel22.Location = New System.Drawing.Point(775, 51)
        Me.xrLabel22.Name = "xrLabel22"
        Me.xrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel22.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel22.Text = "Sales Tax:"
        Me.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel21
        '
        Me.xrLabel21.Location = New System.Drawing.Point(775, 34)
        Me.xrLabel21.Name = "xrLabel21"
        Me.xrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel21.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel21.Text = "Setup:"
        Me.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel20
        '
        Me.xrLabel20.Location = New System.Drawing.Point(775, 17)
        Me.xrLabel20.Name = "xrLabel20"
        Me.xrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel20.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel20.Text = "Delivery:"
        Me.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel19
        '
        Me.xrLabel19.Location = New System.Drawing.Point(775, 0)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel19.Text = "SubTotal:"
        Me.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel10
        '
        Me.xrLabel10.Location = New System.Drawing.Point(0, 142)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.Size = New System.Drawing.Size(17, 17)
        Me.xrLabel10.Text = "X."
        '
        'xrLine1
        '
        Me.xrLine1.Location = New System.Drawing.Point(17, 150)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.Size = New System.Drawing.Size(508, 9)
        '
        'xrLabel9
        '
        Me.xrLabel9.Location = New System.Drawing.Point(8, 108)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.Size = New System.Drawing.Size(517, 17)
        Me.xrLabel9.Text = "I understand the terms and conditions of my purchase:"
        '
        'xrCheckBox6
        '
        Me.xrCheckBox6.Location = New System.Drawing.Point(8, 75)
        Me.xrCheckBox6.Name = "xrCheckBox6"
        Me.xrCheckBox6.Size = New System.Drawing.Size(517, 16)
        Me.xrCheckBox6.Text = " 30 day return policy subject to a 25% return charge and delivery pick up fee*"
        '
        'xrCheckBox4
        '
        Me.xrCheckBox4.Location = New System.Drawing.Point(8, 50)
        Me.xrCheckBox4.Name = "xrCheckBox4"
        Me.xrCheckBox4.Size = New System.Drawing.Size(517, 16)
        Me.xrCheckBox4.Text = " Warranties for commercial use are not offered by our manufacturers."
        '
        'xrCheckBox3
        '
        Me.xrCheckBox3.Location = New System.Drawing.Point(8, 25)
        Me.xrCheckBox3.Name = "xrCheckBox3"
        Me.xrCheckBox3.Size = New System.Drawing.Size(517, 16)
        Me.xrCheckBox3.Text = " Clearance center and floor samples are sold AS-IS, no service or returns."
        '
        'xrCheckBox1
        '
        Me.xrCheckBox1.Location = New System.Drawing.Point(8, 0)
        Me.xrCheckBox1.Name = "xrCheckBox1"
        Me.xrCheckBox1.Size = New System.Drawing.Size(517, 17)
        Me.xrCheckBox1.Text = " Special Orders are not subject to cancellation or return.  Deposit is non-refund" & _
            "able."
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.Location = New System.Drawing.Point(858, 67)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.Size = New System.Drawing.Size(133, 25)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Sales Receipt"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'pictureBox1
        '
        Me.pictureBox1.BackColor = System.Drawing.Color.White
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(264, 72)
        Me.pictureBox1.TabIndex = 0
        Me.pictureBox1.TabStop = False
        '
        'winControlContainer1
        '
        Me.winControlContainer1.Location = New System.Drawing.Point(0, 0)
        Me.winControlContainer1.Name = "winControlContainer1"
        Me.winControlContainer1.Size = New System.Drawing.Size(275, 75)
        Me.winControlContainer1.WinControl = Me.pictureBox1
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_del_doc_num_bar, Me.xrLine2, Me.xrLabel1, Me.xrPageInfo2, Me.xrPageInfo1, Me.winControlContainer1})
        Me.ReportHeader.Height = 108
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrLine2
        '
        Me.xrLine2.Location = New System.Drawing.Point(0, 100)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.Size = New System.Drawing.Size(1000, 8)
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Size = New System.Drawing.Size(100, 23)
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Size = New System.Drawing.Size(100, 23)
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xr_del_doc_num_bar
        '
        Me.xr_del_doc_num_bar.AutoModule = True
        Me.xr_del_doc_num_bar.Location = New System.Drawing.Point(825, 42)
        Me.xr_del_doc_num_bar.Name = "xr_del_doc_num_bar"
        Me.xr_del_doc_num_bar.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.xr_del_doc_num_bar.ShowText = False
        Me.xr_del_doc_num_bar.Size = New System.Drawing.Size(175, 25)
        code39Generator1.WideNarrowRatio = 3.0!
        Me.xr_del_doc_num_bar.Symbology = code39Generator1
        Me.xr_del_doc_num_bar.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'XtraReport2
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1})
        Me.Version = "8.1"
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_setup.DataBindings.Add("Text", DataSource, "SO.SETUP_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_SKU.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_store.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_desc.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_del_doc_num_bar.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_del_doc_num_bar.AutoModule = True

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "Pickup:"
            Case "D"
                sender.text = "Delivery:"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_setup.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub
End Class