Public Class SO_LN
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "SO_LN.resx"
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ret = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ext = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_ext, Me.xr_ret, Me.xr_desc, Me.xr_loc, Me.xr_store, Me.xr_SKU, Me.xr_vsn, Me.xr_qty})
        Me.Detail.Height = 52
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_qty
        '
        Me.xr_qty.Location = New System.Drawing.Point(8, 8)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.Size = New System.Drawing.Size(33, 17)
        Me.xr_qty.Text = "QTY"
        '
        'xr_vsn
        '
        Me.xr_vsn.Location = New System.Drawing.Point(242, 8)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.Size = New System.Drawing.Size(425, 17)
        Me.xr_vsn.Text = "VSN"
        '
        'xr_SKU
        '
        Me.xr_SKU.Location = New System.Drawing.Point(42, 8)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.Size = New System.Drawing.Size(75, 17)
        Me.xr_SKU.Text = "SKU"
        '
        'xr_store
        '
        Me.xr_store.Location = New System.Drawing.Point(125, 8)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.Size = New System.Drawing.Size(33, 17)
        Me.xr_store.Text = "ST"
        '
        'xr_loc
        '
        Me.xr_loc.Location = New System.Drawing.Point(175, 8)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.Size = New System.Drawing.Size(33, 17)
        Me.xr_loc.Text = "LOC"
        '
        'xr_desc
        '
        Me.xr_desc.Location = New System.Drawing.Point(242, 25)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.Size = New System.Drawing.Size(425, 17)
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_ret
        '
        Me.xr_ret.Location = New System.Drawing.Point(675, 8)
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.Size = New System.Drawing.Size(100, 25)
        Me.xr_ret.Text = "RETAIL"
        '
        'xr_ext
        '
        Me.xr_ext.Location = New System.Drawing.Point(775, 8)
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.Size = New System.Drawing.Size(100, 25)
        Me.xr_ext.Text = "EXTENDED"
        '
        'SO_LN
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail})
        Me.Landscape = True
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "8.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

#End Region


  

    Private Sub SO_LN_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint

        

    End Sub
End Class