﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class MerchDac
    Inherits BaseDac

    ''' <summary>
    ''' Determines if the passed-in warranty has been defined in the system(ITM2ITM_WARR table)
    ''' with the specified warrantable sku. 
    ''' </summary>
    ''' <param name="itmCd">item code of the warrantable line</param>
    ''' <param name="warrItmCd">>item code of the warranty line</param>
    ''' <returns>true if a match was found for the specified warranty and warrantable skus, false otherwise</returns>
    Public Function IsValWarrForItm(ByVal itmCd As String, ByVal warrItmCd As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim dbSql As String = "SELECT war_itm_cd FROM ITM2ITM_WARR WHERE itm_cd = :itmCd AND war_itm_cd = :warrItmCd "

        Using dbConn As OracleConnection = GetConnection(), dbCmd As New OracleCommand(dbSql, dbConn)

            Dim dbRdr As OracleDataReader
            dbConn.Open()

            dbCmd.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCmd.Parameters(":itmCd").Value = itmCd
            dbCmd.Parameters.Add(":warrItmCd", OracleType.VarChar)
            dbCmd.Parameters(":warrItmCd").Value = warrItmCd

            dbRdr = DisposablesManager.BuildOracleDataReader(dbCmd)

            If dbRdr.Read() Then
                rtnVal = (Not IsDBNull(dbRdr.Item("war_itm_cd").ToString)) AndAlso dbRdr.Item("war_itm_cd").ToString.isNotEmpty
            End If
            dbRdr.Close()
        End Using

        Return rtnVal
    End Function


End Class
