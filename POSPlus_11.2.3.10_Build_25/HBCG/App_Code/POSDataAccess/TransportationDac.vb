﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.OracleClient

Public Class TransportationDac
    Inherits BaseDac

    ''' <summary>
    ''' Retrieves the transportation points based on the order lines found in the
    ''' current session.   Uses the TEMP_ITM table for this.
    ''' </summary>
    ''' <param name="sessionId">the session id to fetch the order lines</param>
    ''' <returns>the number of points for the session passed in</returns>
    Public Function GetDeliveryPoints(ByVal sessionId As String) As Integer

        Dim points As Integer = 0
        Dim sql As String = "SELECT SUM(DEL_PTS*QTY) As PointCount FROM TEMP_ITM WHERE SESSION_ID = :sessionId"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read() AndAlso IsNumeric(dbReader.Item("PointCount").ToString)) Then
                points = CInt(dbReader.Item("PointCount").ToString)
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return points

    End Function

    ''' <summary>
    ''' Schedules the delivery or pickup points or stops, as applicable
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="zoneCd">the zone code for this pickup/delivery</param>
    ''' <param name="puDel">the flag that indicates if pickup or delivery</param>
    ''' <param name="puDelDt">the pickup/delivery date</param>
    ''' <param name="trnTp">the transaction type CRM / SAL / EEX ...</param>
    ''' <param name="pointOrStops">the number of points for this pickup/delivery</param>
    Public Sub SchedPuDel(ByRef cmd As OracleCommand, ByVal zoneCd As String, ByVal puDel As String,
                          ByVal puDelDt As Date, ByVal trnTp As String, ByVal pointOrStops As Double)

        '   trn_tp = ord_tp_cd, SAL, CRM
        cmd.CommandText = "bt_warehouse.schedule_pickup_delivery"
        cmd.Parameters.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("pu_del_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("pu_del_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            cmd.Parameters.Add("trn_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("points_i", OracleType.Number).Direction = ParameterDirection.Input

            cmd.Parameters("zone_cd_i").Value = zoneCd
            cmd.Parameters("pu_del_flag_i").Value = puDel
            cmd.Parameters("pu_del_dt_i").Value = puDelDt
            cmd.Parameters("trn_type_i").Value = trnTp
            cmd.Parameters("points_i").Value = pointOrStops

            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        Catch oex As OracleException
            Throw oex
        End Try
    End Sub

    ''' <summary>
    ''' Unschedules the delivery or pickup points or stops, as applicable
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="zoneCd">the zone code for this pickup/delivery</param>
    ''' <param name="puDel">the flag that indicates if pickup or delivery</param>
    ''' <param name="puDelDt">the pickup/delivery date</param>
    ''' <param name="trnTp">the transaction type CRM / SAL </param>
    ''' <param name="pointOrStops">the number of points for this pickup/delivery</param>
    Public Sub UnschedPuDel(ByRef cmd As OracleCommand, ByVal zoneCd As String, ByVal puDel As String,
                            ByVal puDelDt As Date, ByVal trnTp As String, ByVal pointOrStops As Double)

        cmd.CommandText = "bt_warehouse.unschedule_pu_del"
        cmd.Parameters.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("pu_del_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("pu_del_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            cmd.Parameters.Add("trn_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("points_i", OracleType.Number).Direction = ParameterDirection.Input

            cmd.Parameters("zone_cd_i").Value = zoneCd
            cmd.Parameters("pu_del_flag_i").Value = puDel
            cmd.Parameters("pu_del_dt_i").Value = puDelDt
            cmd.Parameters("trn_type_i").Value = trnTp
            cmd.Parameters("points_i").Value = pointOrStops

            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        Catch oex As OracleException
            Throw oex
        End Try
    End Sub

    ''' <summary>
    ''' Retrieves from the ZIP2ZONE table, all the zones that match the corresponding postal
    ''' code of the store passed in
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieve zone codes for</param>
    ''' <param name="onlyFirstRec">retrieve only the first record if true; otherwise return all records; opt; def: false</param>
    ''' <returns>a dataset with the matching zones; or an empty dataset if none found</returns>
    ''' <remarks></remarks>
    Public Function GetZoneCodesForStore(ByVal storeCd As String, Optional ByVal onlyFirstRec As Boolean = False) As DataSet

        Dim zones As New DataSet
        If (storeCd.isNotEmpty()) Then
            Dim sql As New StringBuilder
            sql.Append("SELECT zone_cd FROM zip2zone WHERE zip_cd = (SELECT zip_cd FROM store WHERE store_cd = :storeCd) ")
            If onlyFirstRec Then
                sql.Append("AND ROWNUM < 2 ")
            End If
            Dim dbConnection As OracleConnection = GetConnection()
            Dim dbCommand As OracleCommand = GetCommand(sql.ToString, dbConnection)
            Dim dbAdapter As OracleDataAdapter

            Try
                dbConnection.Open()
                dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
                dbCommand.Parameters(":storeCd").Value = UCase(storeCd)

                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(zones)
                dbAdapter.Dispose()

            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If
        Return zones
    End Function

    Public Function lucy_GetZoneCodes(ByRef p_co_cd As String, Optional ByVal onlyFirstRec As Boolean = False) As DataSet
        'lucy copied from GetZoneCodes
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        sql.Append("SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG, " &
                                    "NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG ")
        If onlyFirstRec Then
            sql.Append("AND ROWNUM < 2 ")
        End If
        'lucy add this line
        sql.Append("FROM ZONE where zone.delivery_store_cd in (select store_cd from store where co_cd in ")
        sql.Append("select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='" & p_co_cd & "')))")
        sql.Append("FROM ZONE ORDER BY ZONE_CD")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

    Public Function lucy_GetZoneCodes(ByRef p_co_cd As String, ByRef p_co_grp_cd As String, Optional ByVal onlyFirstRec As Boolean = False) As DataSet
        'Franchise changes
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        sql.Append("SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG, " &
                                    "NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG ")
        If onlyFirstRec Then
            sql.Append("AND ROWNUM < 2 ")
        End If
        sql.Append("FROM ZONE where zone.delivery_store_cd in (select store_cd from store where co_cd ='" & p_co_cd & "')")
        sql.Append("FROM ZONE ORDER BY ZONE_CD")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

    Public Function lucy_GetZoneCodesForZip(ByVal postalCd As String, p_co_cd As String) As DataSet
        'lucy copied from GetZoneCodesForZip, for multi company
        ' group by eliminates duplicates
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter

        ' Nov 15 MCCL added
      
        Dim coCd As String = p_co_cd

        Dim sql As String = ""

        ' Nov 15 improve performance
        If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
            sql = "SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG, " &
                              "NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG " &
                              "FROM ZONE, ZIP2ZONE WHERE ZONE.ZONE_CD = ZIP2ZONE.ZONE_CD " &
                              " AND ZIP2ZONE.ZIP_CD = :postalCd ORDER BY ZONE_CD"
        End If
        If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
            sql = "SELECT Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, " +
                "NVL(Z.DEFAULT_SETUP_CHG, 0) AS DEFAULT_SETUP_CHG, " +
                "NVL(Z.DEFAULT_DEL_CHG, 0) AS DEFAULT_DEL_CHG " +
                "FROM ZONE z, ZIP2ZONE, zone_grp zg " +
                "WHERE z.ZONE_CD = ZIP2ZONE.ZONE_CD " +
                    "AND ZIP2ZONE.ZIP_CD = :postalCd " +
                    "and z.zone_cd = zg.zone_cd " +
                    "and zg.store_grp_cd = (select cg2.zone_grp from CO_GRP$ZONE_GRP cg2 " +
                    "where cg2.co_cd = :coCd and cg2.zone_grp = zg.store_grp_cd) " +
                    "order by z.zone_cd "
        End If


        Try
            dbConnection.Open()
            dbCommand.CommandText = sql

            dbCommand.Parameters.Add(":postalCd", OracleType.VarChar)
            dbCommand.Parameters(":postalCd").Value = UCase(postalCd)

            ' Parameters only if not Super User
            If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
                dbCommand.Parameters.Add(":coCd", OracleType.VarChar)
                dbCommand.Parameters(":coCd").Value = coCd
            End If

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function



    ''' <summary>
    ''' Retreives the zone details for the postal code passed-in. 
    ''' </summary>
    ''' <param name="postalCd">the postal code for which to get the zones.</param>
    ''' <returns>a dataset consisting of the zone info for the postal code specified. Could contain one, many or no records.
    ''' </returns>
    Public Function GetZoneCodesForZip(ByVal postalCd As String) As DataSet

        ' group by eliminates duplicates
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        ' Nov 15 improve performance
        Dim sql As String = ""
        Dim coCd As String = HttpContext.Current.Session("CO_CD")

        If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
            sql = "SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG, " &
                                 "NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG " &
                                 "FROM ZONE, ZIP2ZONE WHERE ZONE.ZONE_CD = ZIP2ZONE.ZONE_CD " &
                                 "AND ZIP2ZONE.ZIP_CD = :postalCd GROUP BY ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, DEFAULT_DEL_CHG, DEFAULT_SETUP_CHG ORDER BY ZONE_CD"
        End If
        If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
            sql = "SELECT distinct Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, " +
                "NVL(Z.DEFAULT_SETUP_CHG, 0) AS DEFAULT_SETUP_CHG, " +
                "NVL(Z.DEFAULT_DEL_CHG, 0) AS DEFAULT_DEL_CHG " +
                "FROM ZONE z, ZIP2ZONE, zone_grp zg " +
                "WHERE z.ZONE_CD = ZIP2ZONE.ZONE_CD " +
                    "AND ZIP2ZONE.ZIP_CD = :postalCd " +
                    "and z.zone_cd = zg.zone_cd " +
                    "and zg.store_grp_cd = (select cg2.zone_grp from CO_GRP$ZONE_GRP cg2 " +
                    "where cg2.co_cd = :coCd and cg2.zone_grp = zg.store_grp_cd) "
        End If

        Try
            dbConnection.Open()
            dbCommand.CommandText = Sql
            dbCommand.Parameters.Add(":postalCd", OracleType.VarChar)
            dbCommand.Parameters(":postalCd").Value = UCase(postalCd)

            ' Parameters only if not Super User
            If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
                dbCommand.Parameters.Add(":coCd", OracleType.VarChar)
                dbCommand.Parameters(":coCd").Value = coCd
            End If

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function
    ''' <summary>
    ''' Retreives the zone details for the postal code passed-in. 
    ''' </summary>
    ''' <param name="postalCd">the postal code for which to get the zones.</param>
    ''' <returns>a dataset consisting of the zone info for the postal code specified. Could contain one, many or no records.
    ''' </returns>
    ''' </returns>
    Public Function GetZonesFromZPFMForZip(ByVal postalCd As String, ByRef p_co_cd As String, ByVal SaleType As String) As DataSet
        'MCCL added NOV 24, 2017
        ' group by eliminates duplicates
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()
        'sql.Append(" SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG, ")
        'sql.Append(" NVL(DEFAULT_DEL_CHG, 0) As DEFAULT_DEL_CHG ")
        'sql.Append(" FROM ZONE, ZIP2ZONE,ZONE$PRI WHERE ZONE.ZONE_CD = ZIP2ZONE.ZONE_CD and zone$pri.zone_cd =  ZIP2ZONE.ZONE_CD And PD_CD = :SaleType")
        ''sql.Append(" And zone.delivery_store_cd in (select store_cd from store where co_cd ='" & p_co_cd & "') ")
        'sql.Append(" And zone.delivery_store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='" & p_co_cd & "'))) ")
        'sql.Append(" And ZIP2ZONE.ZIP_CD = : postalCd Group BY ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, DEFAULT_DEL_CHG, DEFAULT_SETUP_CHG ORDER BY ZONE_CD")

        sql.Append(" SELECT distinct Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, NVL(Z.DEFAULT_SETUP_CHG, 0) AS DEFAULT_SETUP_CHG, ")
            sql.Append(" NVL(Z.DEFAULT_DEL_CHG, 0) AS DEFAULT_DEL_CHG ")
            sql.Append(" FROM ZONE z, ZIP2ZONE, zone_grp zg , ZONE$PRI zp ")
            sql.Append(" WHERE z.ZONE_CD = ZIP2ZONE.ZONE_CD AND ZIP2ZONE.ZIP_CD = :postalCd and z.zone_cd = zg.zone_cd and ")
            sql.Append(" zg.store_grp_cd = (select cg2.zone_grp")
            sql.Append(" from CO_GRP$ZONE_GRP cg2  where cg2.co_cd = :coCd and cg2.zone_grp = zg.store_grp_cd) and ")
            sql.Append(" zp.PD_CD = :SaleType and zp.zone_cd = z.zone_cd ")
            sql.Append(" order by z.zone_cd ")
        
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString()
            dbCommand.Parameters.Add(":postalCd", OracleType.VarChar)
            dbCommand.Parameters(":postalCd").Value = UCase(postalCd)
            dbCommand.Parameters.Add(":SaleType", OracleType.VarChar)
            dbCommand.Parameters(":SaleType").Value = UCase(SaleType)
            ' Parameters only if not Super User
            dbCommand.Parameters.Add(":coCd", OracleType.VarChar)
            dbCommand.Parameters(":coCd").Value = p_co_cd
        
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

    ''' <summary>
    ''' Retrieves the zone details for ALL the zones defined in the system; only the first record if input indicates
    ''' </summary>
    ''' <param name="onlyFirstRec">retrieve only the first record if true; otherwise return all records; opt; def: false</param>
    ''' <returns>a dataset consisting of the zone info for ALL the zones.
    ''' </returns>
    Public Function GetZoneCodes(Optional ByVal onlyFirstRec As Boolean = False) As DataSet

        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        sql.Append(" SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG, ")
        sql.Append(" NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG ")
        sql.Append(" FROM ZONE  ")
        If onlyFirstRec Then
            sql.Append(" WHERE  ROWNUM < 2 ")
        End If
        sql.Append("  ORDER BY ZONE_CD ")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

    ''' <summary>
    ''' Returns the ZONE.DAYS_LEAD value, that correspond to the zone code
    ''' passed in; a zero gets returned if no matching zone is found.
    ''' </summary>
    ''' <param name="zoneCd">the zone to retrieve the lead days for</param>
    ''' <returns>the DAYS_LEAD value; a zero is returned if zone is not found</returns>
    Public Function GetZoneLeadDays(ByVal zoneCd As String) As Integer

        Dim leadDays As Integer = 0
        If (IsDBNull(zoneCd) OrElse String.IsNullOrEmpty(zoneCd)) Then
            'zoneCd is not valid, no need to continue checking, returns zero
        Else
            Dim zone As ZoneDtc = GetZone(zoneCd)
            If zone.zoneCd.isNotEmpty Then
                leadDays = zone.leadDays
            End If
        End If
        Return leadDays

    End Function

    ''' <summary>
    ''' Returns the ZONE record that correspond to the zone code passed in
    ''' </summary>
    ''' <param name="zoneCd">the zone to retrieve the record for</param>
    ''' <returns>zone object of one or none records of zone information</returns>
    Public Function GetZone(ByVal zoneCd As String) As ZoneDtc

        Dim zone As New ZoneDtc
        Dim sql As String = "SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG, " &
                            "NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG, days_lead " &
                            "FROM ZONE WHERE zone_cd = :zoneCd "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            Dim dbReader As OracleDataReader

            dbCommand.Parameters.Add(":zoneCd", OracleType.VarChar)
            dbCommand.Parameters(":zoneCd").Value = zoneCd

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then

                zone.zoneCd = dbReader.Item("zone_cd")
                zone.des = If(IsDBNull(dbReader.Item("des")), String.Empty, dbReader.Item("des"))
                zone.delStoreCd = If(IsDBNull(dbReader.Item("delivery_store_cd")), String.Empty, dbReader.Item("delivery_store_cd"))
                zone.defltDelChg = If(IsDBNull(dbReader.Item("default_del_chg")), 0, dbReader.Item("default_del_chg"))
                zone.defltSetupChg = If(IsDBNull(dbReader.Item("default_setup_chg")), 0, dbReader.Item("default_setup_chg"))
                zone.leadDays = If(IsDBNull(dbReader.Item("days_lead")), 0, dbReader.Item("days_lead"))

            Else
                zone.zoneCd = ""
                zone.des = String.Empty
                zone.delStoreCd = String.Empty
                zone.defltDelChg = 0.0
                zone.defltSetupChg = 0.0
                zone.leadDays = 0
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return zone
    End Function

    ''' <summary>
    ''' Updating the truck stop date for the delivery document number
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="trkStopDate">the truck stop date for updating the Truck Delivery System</param>
    ''' <param name="trkNum">the truck number for updating the Truck Delivery System</param>
    ''' <param name="stopNum">the truck stop number for updating the Truck Delivery System</param>
    ''' <param name="delDocNum">the delivery document number for updating the Truck Delivery System</param>
    ''' <remarks></remarks>
    Public Sub UpdateTruckDeliverySystem(ByRef cmd As OracleCommand, ByVal trkStopDate As Date, _
                                         ByVal trkNum As String, ByVal stopNum As String, ByVal delDocNum As String)

        Dim sbQuery As New StringBuilder
        sbQuery.Append("UPDATE   TRK_STOP TRK1")
        sbQuery.Append("   SET   TRK1.DT = :TRK_STOP_DT,")
        sbQuery.Append("         TRK1.SEQ# =")
        sbQuery.Append("            (SELECT   NVL (MAX (TRK2.SEQ#), 0) + 1")
        sbQuery.Append("               FROM   TRK_STOP TRK2")
        sbQuery.Append("              WHERE       TRK2.DT = :TRK_STOP_DT")
        sbQuery.Append("                      AND TRK2.TRK_NUM = :TRK_NUM")
        sbQuery.Append("                      AND TRK2.STOP# = :STOP_NUM)")
        sbQuery.Append(" WHERE   TRK1.DT <> :TRK_STOP_DT AND TRK1.DEL_DOC_NUM = :DEL_DOC_NUM")

        cmd.Parameters.Clear()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sbQuery.ToString()

        Try
            cmd.Parameters.Add(":TRK_STOP_DT", OracleType.DateTime).Direction = ParameterDirection.Input
            cmd.Parameters.Add(":TRK_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add(":STOP_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input

            cmd.Parameters(":TRK_STOP_DT").Value = trkStopDate
            cmd.Parameters(":TRK_NUM").Value = trkNum
            cmd.Parameters(":STOP_NUM").Value = stopNum
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum

            Dim affctedRowsCnt As Integer = cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        Catch oraEx As OracleException
            Throw oraEx
        End Try

    End Sub

    ''' <summary>
    ''' Clearing the truck stop date for the delivery document number
    ''' </summary>
    ''' <param name="cmd">the database command to use for this process</param>
    ''' <param name="delDocNum">the delivery document number for clearing the Truck Delivery System</param>
    ''' <remarks></remarks>
    Public Sub ClearTruckDeliverySystem(ByRef cmd As OracleCommand, ByVal delDocNum As String)

        Dim sbQuery As New StringBuilder
        sbQuery.Append("DELETE FROM   TRK_STOP")
        sbQuery.Append("      WHERE   DEL_DOC_NUM = :DEL_DOC_NUM")

        cmd.Parameters.Clear()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sbQuery.ToString()

        Try
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        End Try

    End Sub
    ''' <summary>
    ''' Get the Delivery points details for the zone
    ''' </summary>
    ''' <param name="zoneCd"></param>
    ''' <param name="delDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDeliveryPointsDetails(ByVal zoneCd As String, ByVal delDate As String) As DeliveryPointDetails
        Dim deliveryPointsDetails As DeliveryPointDetails = Nothing
        Dim sqlQuery As New StringBuilder
        sqlQuery.Append("SELECT DEL_DT, DAY, DEL_STOPS, ACTUAL_STOPS, CLOSED, (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS,EXMPT_DEL_CD ")
        sqlQuery.Append("FROM DEL ")
        sqlQuery.Append("WHERE ZONE_CD = :zoneCd AND DEL_DT = TO_DATE(:delDate,'mm/dd/RRRR') ")
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sqlQuery.ToString, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":zoneCd", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":zoneCd").Value = zoneCd.ToUpper
            dbCommand.Parameters.Add(":delDate", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":delDate").Value = delDate
            Dim dbReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read Then
                deliveryPointsDetails = New DeliveryPointDetails
                deliveryPointsDetails.ActualStops = dbReader.Item("ACTUAL_STOPS").ToString
                deliveryPointsDetails.RemainingStops = dbReader.Item("REMAINING_STOPS").ToString
                deliveryPointsDetails.ExmptDelCd = dbReader.Item("EXMPT_DEL_CD").ToString
            End If
        Catch ex As Exception
            Throw
        Finally
            If dbConnection.State = ConnectionState.Open Then
                dbConnection.Close()
            End If
        End Try
        Return deliveryPointsDetails
    End Function

    ''' <summary>
    ''' Returns the delivery dates from DEL table
    ''' </summary>
    ''' <param name="zoneCd">zone code</param>
    ''' <param name="delDate">delivery date</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDeliveryDates(ByVal zoneCd As String, ByVal delDate As Date) As DataSet
        Dim sqlQuery As New StringBuilder
        sqlQuery.Append("SELECT DEL_DT, DAY, DEL_STOPS, ACTUAL_STOPS,  CLOSED, (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS,EXMPT_DEL_CD FROM DEL ")
        sqlQuery.Append(" WHERE (ZONE_CD = :zoneCode) AND (DEL_DT >= :deliveryDate) AND NVL(CLOSED, 'N') = 'N' ORDER BY DEL_DT")

        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sqlQuery.ToString(), dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":zoneCode", OracleType.VarChar)
            dbCommand.Parameters(":zoneCode").Value = UCase(zoneCd)

            dbCommand.Parameters.Add(":deliveryDate", OracleType.DateTime)
            dbCommand.Parameters(":deliveryDate").Value = UCase(delDate)

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function
    Public Function GetRemPointsForDeliveryDT(ByVal gPointCnt As Double, ByVal delDate As Date, ByVal zone As String, ByRef exmpCD As String) As Double

        Dim sql As String = "SELECT (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS,EXMPT_DEL_CD   FROM  DEL WHERE ZONE_CD =:ZONECODE AND DEL_DT = TO_DATE(:DELDATE,'DD-MM-RRRR') AND NVL(CLOSED,'N')='N' "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim remPointStop As Double = 0.0


        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            Dim dbReader As OracleDataReader

            dbCommand.Parameters.Add(":DELDATE", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DELDATE").Value = delDate
            dbCommand.Parameters.Add(":ZONECODE", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":ZONECODE").Value = zone

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() AndAlso dbReader.HasRows Then
                If gPointCnt < dbReader.Item("REMAINING_STOPS") OrElse gPointCnt = dbReader.Item("REMAINING_STOPS") Then
                    remPointStop = dbReader.Item("REMAINING_STOPS")
                    exmpCD = If(IsDBNull(dbReader.Item("EXMPT_DEL_CD")), String.Empty, dbReader.Item("EXMPT_DEL_CD"))
                End If
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return remPointStop

    End Function

    ''' <summary>
    ''' Retrieves the zone details for ALL the zones defined in the system; only the first record if input indicates
    ''' </summary>
    ''' <param name="onlyFirstRec">retrieve only the first record if true; otherwise return all records; opt; def: false</param>
    ''' <returns>a dataset consisting of the zone info for ALL the zones.
    ''' </returns>
    Public Function GetZoneCodes(ByVal empInit As String, Optional ByVal onlyFirstRec As Boolean = False) As DataSet

        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        sql.Append("SELECT ZONE.ZONE_CD, DES, DELIVERY_STORE_CD, NVL(DEFAULT_DEL_CHG,0) AS DEFAULT_DEL_CHG, " &
                                    "NVL(DEFAULT_SETUP_CHG,0) AS DEFAULT_SETUP_CHG ")

        'sql.Append("FROM ZONE ORDER BY ZONE_CD") ' Daniela comment for multi comp logic Feb16
        sql.Append("FROM ZONE WHERE std_multi_co.isValidCODelZne ('" & empInit & "', zone_cd) = 'Y' ") ' Daniela comment for multi comp logic Feb16
        If onlyFirstRec Then
            sql.Append("AND ROWNUM < 2 ")
        End If
        sql.Append("ORDER BY ZONE_CD")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

End Class

