﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.OracleClient

Public Class TaxDac
    Inherits BaseDac

    ''' <summary>
    ''' Retrieves the list of all THE NON-EXPIRED tax codes based on the tax date passed-in, with
    ''' their code and descriptions.
    ''' </summary>
    ''' <returns>a list of all the active tax codes defined in the system.</returns>
    Public Function GetTaxCodesByDate(ByVal effectiveTaxDt As String) As DataSet

        Dim taxCodes As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        Dim currentDt As Date = FormatDateTime(Now(), DateFormat.ShortDate)
        Dim taxDt As Date = currentDt
        If IsDate(effectiveTaxDt) Then taxDt = effectiveTaxDt

        ' MM-4240
        ' Below query has been modified to get the tax method of the tax code

        sql.Append(" SELECT TAX_CD$TAT.TAX_CD, SUM (TAT.PCT) AS SumOfPCT, TAX_CD.DES, TAX_CD.TAX_METHOD ")
        sql.Append("  FROM TAT, TAX_CD$TAT, TAX_CD ")
        sql.Append("  WHERE TAT.EFF_DT <= TO_DATE ('" & currentDt & "',  'mm/dd/RRRR') ")
        sql.Append("        AND NVL (TAT.END_DT, TO_DATE ('12/31/2049', 'mm/dd/RRRR')) >= TO_DATE ('" & taxDt & "', 'mm/dd/RRRR') ")
        sql.Append("        AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
        sql.Append("        AND TAX_CD$TAT.TAX_CD = TAX_CD.TAX_CD ")
        sql.Append(" GROUP BY TAX_CD$TAT.TAX_CD, TAX_CD.DES, TAX_CD.TAX_METHOD ")
        sql.Append(" ORDER BY TAX_CD.DES ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(taxCodes)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return taxCodes

    End Function
    ''' <summary>
    ''' Retrieves the list of all THE NON-EXPIRED tax codes based on the tax date passed-in, with
    ''' their code and descriptions.
    ''' </summary>
    ''' <returns>a list of all the active tax codes defined in the system.</returns>
    Public Function GetDeliveryTax(ByVal effectiveTaxDt As String, taxCd As String) As Double

        Dim taxCodes As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As New StringBuilder
        Dim currentDt As Date = FormatDateTime(Now(), DateFormat.ShortDate)
        Dim tax As Double = 0
        sql.Append("SELECT NVL(SUM(tat.pct),0.0) AS SumOfPCT ")
        sql.Append("FROM TAT, TAX_CD$TAT ")
        sql.Append("WHERE TAT.EFF_DT <= :tax_dt And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= :tax_dt ")
        sql.Append("AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
        sql.Append("AND TAX_CD$TAT.TAX_CD= :tax_cd And TAT.DEL_TAX ='Y' ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbCommand.Parameters.Add(":tax_cd", OracleType.VarChar)
            dbCommand.Parameters(":tax_cd").Value = taxCd
            dbCommand.Parameters.Add(":tax_dt", OracleType.DateTime)
            dbCommand.Parameters(":tax_dt").Value = effectiveTaxDt
            dbAdapter.Fill(taxCodes)
            If taxCodes.Tables(0).Rows.Count > 0 Then
                tax = CDbl(taxCodes.Tables(0).Rows(0)("SumOfPCT").ToString())
            Else
                tax = 0.0
            End If
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return tax

    End Function

    ''' <summary>
    ''' Retrieves the list of all THE NON-EXPIRED tax codes and it's details.
    ''' </summary>
    ''' <returns>a list of all the active tax codes defined in the system.</returns>
    Public Function GetTaxCodesByZip(ByVal postalCd As String)

        Dim taxCodes As New DataSet
        'Dim currentDt As Date = FormatDateTime(Now(), DateFormat.ShortDate)
        Dim sql As New StringBuilder
        ' MM-4240
        ' Below query has been modified to get the tax method of the tax code

        'Daniela fix frenc date issue

        'sql.Append(" SELECT ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT, TAX_CD.DES, TAX_CD.TAX_METHOD ")
        'sql.Append("  FROM TAT, TAX_CD$TAT, ZIP2TAX, TAX_CD ")
        'sql.Append("  WHERE TAT.EFF_DT <= TO_DATE ('" & currentDt & "',  'mm/dd/RRRR') ")
        'sql.Append("        AND NVL (TAT.END_DT, TO_DATE ('12/31/2049', 'mm/dd/RRRR')) >= TO_DATE ('" & currentDt & "', 'mm/dd/RRRR') ")
        'sql.Append("        AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
        'sql.Append("        AND TAX_CD$TAT.TAX_CD = TAX_CD.TAX_CD ")
        'sql.Append("        AND TAX_CD$TAT.TAX_CD = ZIP2TAX.TAX_CD ")
        'sql.Append("        AND  ZIP2TAX.ZIP_CD=:postalCd ")
        'sql.Append(" GROUP BY  ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, TAX_CD.DES, TAX_CD.TAX_METHOD ")

        sql.Append(" SELECT ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT, TAX_CD.DES, TAX_CD.TAX_METHOD ")
        sql.Append("  FROM TAT, TAX_CD$TAT, ZIP2TAX, TAX_CD ")
        sql.Append("  WHERE TAT.EFF_DT <= TO_DATE (to_char(sysdate,'mm/dd/RRRR'),  'mm/dd/RRRR') ")
        sql.Append("        AND NVL (TAT.END_DT, TO_DATE ('12/31/2049', 'mm/dd/RRRR')) >= TO_DATE (to_char(sysdate,'mm/dd/RRRR'), 'mm/dd/RRRR') ")
        sql.Append("        AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
        sql.Append("        AND TAX_CD$TAT.TAX_CD = TAX_CD.TAX_CD ")
        sql.Append("        AND TAX_CD$TAT.TAX_CD = ZIP2TAX.TAX_CD ")
        sql.Append("        AND  ZIP2TAX.ZIP_CD=:postalCd ")
        sql.Append(" GROUP BY  ZIP2TAX.ZIP_CD, TAX_CD$TAT.TAX_CD, TAX_CD.DES, TAX_CD.TAX_METHOD ")

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql.ToString, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":postalCd", OracleType.VarChar)
            dbCommand.Parameters(":postalCd").Value = UCase(postalCd)

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(taxCodes)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return taxCodes

    End Function

    ''' <summary>
    ''' Retrieves the sum of the tax percent defined for the passed-in tax code that 
    ''' is defined for a 'Delivery' charge.
    ''' </summary>
    ''' <param name="taxCd">the current tax code</param>
    ''' <returns>the sum of the tax percent associated to a delivery charge using the passed-in tax code</returns>
    Public Function GetTaxPctForDelv(ByVal taxCd As String) As Double

        Return GetTaxPctForSetupDelv(taxCd, False)

    End Function

    ''' <summary>
    ''' Retrieves the sum of the tax percent defined for the passed-in tax code that 
    ''' is defined for a 'Setup' charge.
    ''' </summary>
    ''' <param name="taxCd">the current tax code</param>
    ''' <returns>the sum of the tax percent associated to a setup charge using the passed-in tax code</returns>
    Public Function GetTaxPctForSetup(ByVal taxCd As String) As Double

        Return GetTaxPctForSetupDelv(taxCd, True)

    End Function

    ''' <summary>
    ''' Retrieves the list of all tax exempt codes setup in the system with
    ''' their code and descriptions.
    ''' </summary>
    ''' <returns>the dataset of all the tax exempt codes defined in the system.</returns>
    Public Function GetTaxExemptCodes() As DataSet

        Dim taxExemptCodes As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim sql As String = "SELECT tet_cd, des, tet_cd || ' - ' || des as full_desc FROM TET ORDER BY tet_cd "
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(taxExemptCodes)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return taxExemptCodes

    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Retrieves the sum of the tax percent defined for the passed-in tax code that 
    ''' is defined for a 'Setup' or 'Delivery'.
    ''' </summary>
    ''' <param name="taxCd">the current tax code</param>
    ''' <param name="isSetup">a flag to indicate if the taxes are for a delivery or setup. 'True' means a setup.</param>
    ''' <returns>the sum of the tax percent associated to either setup or delviery, using the passed-in tax code</returns>
    Private Function GetTaxPctForSetupDelv(ByVal taxCd As String,
                                           ByVal isSetup As Boolean) As Double

        Dim taxPct As Double = 0.0
        Dim sql As New StringBuilder
        Dim dbReader As OracleDataReader
        Dim delvSetupClause As String = " AND TAT.DEL_TAX = 'Y' "
        If (isSetup) Then
            delvSetupClause = "  AND TAT.SU_TAX = 'Y' "
        End If

        sql.Append("   SELECT TAX_CD$TAT.TAX_CD, SUM (TAT.PCT) AS SumOfPCT ")
        sql.Append("     FROM TAT, TAX_CD$TAT ")
        sql.Append("    WHERE     TAT.EFF_DT <= SYSDATE ")
        sql.Append("          AND NVL (TAT.END_DT, TO_DATE ('12/31/2049', 'mm/dd/RRRR')) >= SYSDATE ")
        sql.Append("          AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
        sql.Append("          AND TAX_CD$TAT.TAX_CD = :taxCd ")
        sql.Append(delvSetupClause)
        sql.Append(" GROUP BY TAX_CD$TAT.TAX_CD ")

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql.ToString, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":taxCd", OracleType.VarChar)
            dbCommand.Parameters(":taxCd").Value = UCase(taxCd)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                taxPct = dbReader.Item("SumOfPCT")
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return taxPct

    End Function
End Class
