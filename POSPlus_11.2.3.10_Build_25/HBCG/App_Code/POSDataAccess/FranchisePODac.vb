﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Linq
Imports System.Collections.Generic

''' <summary>
''' Feb 08, 2018 - mariam - #2872
''' Insert Franchise Purchase Order Entry
''' 
''' </summary>
Public Class FranchisePODac
    Inherits BaseDac

    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' Gets the stores within the same company code assiged to the user's home store code. Apply MCCL #1
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Function GetStores_MCCL1(ByVal p_supUserFlag As String, ByVal p_userType As String, ByVal p_coCD As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            'dbConnection.Open()

            'sqlString = New StringBuilder("SELECT store_cd, store_cd || ' - ' || store_name as store_des ")
            'sqlString.Append("FROM store s ")
            'sqlString.Append("WHERE store_cd =UPPER(:x_homeStoreCode) ")

            'sql = sqlString.ToString()

            'dbCommand.Parameters.Add(":x_homeStoreCode", OracleType.VarChar)
            'dbCommand.Parameters(":x_homeStoreCode").Value = p_homeStoreCode

            'dbCommand.CommandText = sql
            'dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            'dbAdapter.Fill(storesDataSet)
            'dbAdapter.Dispose()

            If (isNotEmpty(p_supUserFlag) And p_supUserFlag.Equals("Y")) Then
                sql = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd "
            End If

            If (String.IsNullOrEmpty(p_supUserFlag) Or Not p_supUserFlag.Equals("Y")) Then

                If (isNotEmpty(p_userType) And p_userType > 0) Then

                    sql = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
                   "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
                   "where a.user_id = :userType and s.co_cd= '" & p_coCD & "' and " +
                   "s.store_cd = b.store_cd and " +
                   "a.store_grp_cd = b.store_grp_cd order by s.store_cd "

                    dbCommand.Parameters.Add(":userType", OracleType.VarChar)
                    dbCommand.Parameters(":userType").Value = p_userType

                Else
                    sql = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
                 "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
                 "where s.co_cd= '" & p_coCD & "' and " +
                 "s.store_cd = b.store_cd and " +
                 "a.store_grp_cd = b.store_grp_cd order by s.store_cd "

                End If



            End If

            ' Daniela end

            With dbCommand
                .Connection = dbConnection
                .CommandText = sql
            End With

            ' Parameters only if not Super User


            dbConnection.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return ds
    End Function
    'Alice add on july 6
    Public Function GetStores_FROPE(ByVal p_supUserFlag As String, ByVal emp_init As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try

            If (isNotEmpty(p_supUserFlag) And p_supUserFlag.Equals("Y")) Then
                sql = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd "
            Else
                sql = "select store_cd, store_name, store_cd || ' - ' || store_name as full_desc from store s inner join emp e on s.store_cd=e.home_store_cd where e.emp_init=:emp_init"

                dbCommand.Parameters.Add(":emp_init", OracleType.VarChar)
                dbCommand.Parameters(":emp_init").Value = emp_init
            End If

            ' Daniela end

            With dbCommand
                .Connection = dbConnection
                .CommandText = sql
            End With

            ' Parameters only if not Super User


            dbConnection.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' Gets Buyers where emp_tp_cd = 'BUY'
    ''' return dataset for dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Function GetBuyersByCompCD(ByVal p_CompanyCode As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            'sqlString = New StringBuilder("select emp_init  ")
            'sqlString.Append("from emp e, emp$emp_tp emptp  ")
            'sqlString.Append(" where emp_tp_cd = 'BUY' and e.emp_cd = emptp.emp_cd ")

            'Alice updated on Jun 20,2018
            sqlString = New StringBuilder(" select e.emp_init from emp e inner join emp$emp_tp tp on e.emp_cd=tp.emp_cd inner join store s on s.store_cd=e.home_store_cd")
            sqlString.Append(" where tp.emp_tp_cd = 'BUY' and s.co_cd='" & p_CompanyCode & "'")



            sql = sqlString.ToString()

            Try
                With dbCommand
                    .Connection = dbConnection
                    .CommandText = sql
                End With
                dbConnection.Open()
                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(ds)
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try


        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' Gets Buyers where emp_tp_cd = 'BUY'
    ''' return dataset for dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Shared Function GetVendorCD_LEFranchise() As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam

        Dim dbConnection As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            sqlString = New StringBuilder("select ve_cd,  ve_cd || ' - ' || ve_name as ve_name, tat_cd   ")
            sqlString.Append("from VE  ")
            sqlString.Append(" where ve.ve_cust_cd like '%LEON%S FRANCHISE%' ")

            sql = sqlString.ToString()

            Try
                With dbCommand
                    .Connection = dbConnection
                    .CommandText = sql
                End With
                dbConnection.Open()
                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(ds)
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            'dbCommand.Parameters(":x_homeStoreCode").Value = p_homeStoreCode

        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try

        Return ds
    End Function
    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' 
    ''' return dataset for dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Shared Function GetOrderingFromCorpStoreCode(ByVal p_FrStoreNum As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam

        Dim dbConnection As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder

        sqlString = New StringBuilder("select fss.corp_store_cd, fss.corp_store_cd  || ' - ' || s.store_name as store_name    ")
        sqlString.Append("from FR_Shipping_Schedule fss, store s ")
        sqlString.Append(" where fss.corp_store_cd = s.store_cd and fss.store_num = :p_FrStoreNum ")

        sql = sqlString.ToString()


        With dbCommand
            .Connection = dbConnection
            .CommandText = sql
            .Parameters.Add(":p_FrStoreNum", OracleType.VarChar)
            .Parameters(":p_FrStoreNum").Value = p_FrStoreNum
        End With
        dbConnection.Open()
        dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
        dbAdapter.Fill(ds)
        dbConnection.Close()

        'dbCommand.Parameters(":x_homeStoreCode").Value = p_homeStoreCode
        Return ds
    End Function
    ''' <summary>
    ''' Retrieves from the ZIP2ZONE table, all the zones that match the corresponding postal
    ''' code of the store passed in
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieve zone codes for</param>
    ''' <param name="onlyFirstRec">retrieve only the first record if true; otherwise return all records; opt; def: false</param>
    ''' <returns>a dataset with the matching zones; or an empty dataset if none found</returns>
    ''' <remarks></remarks>
    Public Function GetZoneCodesForCorpStore(ByVal p_storeCd As String) As DataSet

        Dim dbConnection As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String

        If (p_storeCd.isNotEmpty()) Then
            sql = "SELECT fss.del_zone_cd FROM zip2zone z2, FR_shipping_schedule fss WHERE z2.zone_cd = fss.del_zone_cd and  zip_cd = (SELECT zip_cd FROM store WHERE store_cd = :storeCd) "
            'sql = "SELECT fss.del_zone_cd FROM zip2zone z2, FR_shipping_schedule fss WHERE z2.zone_cd = fss.del_zone_cd and  zip_cd = (SELECT zip_cd FROM store WHERE store_cd = 'SC') "

            Try
                With dbCommand
                    .Connection = dbConnection
                    .CommandText = sql
                    .Parameters.Add(":storeCd", OracleType.VarChar)
                    .Parameters(":storeCd").Value = p_storeCd
                End With
                dbConnection.Open()
                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(ds)
                dbConnection.Close()
            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Jan 17, 2018 - mariam
    ''' Gets VSN, Desc based on Item SKU
    ''' return dataset 
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Shared Function GetVSNByItemSKU(ByVal p_item As String) As DataSet
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder
        Dim itemSku As String = String.Empty

        sqlString = New StringBuilder(" select i.vsn, i.des   ")
        sqlString.Append("from  itm i  ")
        sqlString.Append(" where i.itm_cd = :itmCd ")

        sql = sqlString.ToString()
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = p_item

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return ds
    End Function
    ''' <summary>
    ''' Jan 17, 2018 - mariam
    ''' Gets VSN, Desc, Repl_Prc based on Item SKU
    ''' return PO cost
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Shared Function GetPOCostByItemAndStore(ByVal p_item As String, ByVal p_storeCd As String) As Double
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder
        Dim itemSku As String = String.Empty
        Dim POCost As Double = 0.0
        Dim bareCost As Double = 0.0
        Dim percent As Double = 0.0

        sqlString = New StringBuilder("  select  distinct  i.itm_cd, fr.store_cd, fr.whse_store_cd,  i.vsn, i.des, i.repl_cst,  i.mnr_cd, mjr.mjr_cd, mjr.prod_grp_cd  ")
        sqlString.Append(", case  mjr.prod_grp_cd when '1FURN' THEN fr.MATT_PCT  ")
        sqlString.Append(" when '2APPL' THEN fr.APPL_PCT  ")
        sqlString.Append(" when '3ELEC' THEN fr.ELEC_PCT  ")
        sqlString.Append(" when '4MATT' THEN fr.MATT_PCT  ")
        sqlString.Append(" END as PCT  ")
        sqlString.Append("from  store_grp$store sgs  ")
        sqlString.Append(" , store_grp sg  ")
        sqlString.Append(" , store_grp_repl_clndr sgrc, itm i  ")
        sqlString.Append(" , inv_mnr mnr, inv_mjr mjr, frtbl fr  ")
        sqlString.Append(" where i.itm_cd = :itmCd ")
        sqlString.Append(" and sgs.store_cd = :storeCd and sg.pricing_store_grp = 'Y'  ")
        sqlString.Append(" and sgs.store_cd = fr.store_cd  ")
        sqlString.Append(" and sg.store_grp_cd = sgs.store_grp_cd  ")
        sqlString.Append(" and sgs.store_grp_cd = sgrc.store_grp_cd  ")
        sqlString.Append(" and i.itm_cd = sgrc.itm_cd ")
        sqlString.Append(" and i.mnr_cd = mnr.mnr_cd ")
        sqlString.Append(" and mnr.mjr_cd = mjr.mjr_cd ")

        sql = sqlString.ToString()
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = p_item

            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters(":storeCd").Value = p_storeCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)

            'Caluculate PO Cost
            bareCost = Convert.ToDouble(ds.Tables(0).Rows(0)("repl_cst"))
            percent = Convert.ToDouble(ds.Tables(0).Rows(0)("PCT"))
            POCost = bareCost + bareCost * percent / 100
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return POCost
    End Function

    'Alice added on May 22, 2018

    Public Shared Function GetPercentageValue(ByVal p_item As String, ByVal p_frStoreCd As String) As Double
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter

        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder
        Dim itemSku As String = String.Empty
        Dim percent As Double = 0.0


        sqlString = New StringBuilder("SELECT case  prod_grp_cd when '1FURN' THEN  (select MATT_PCT from frtbl where STORE_CD=:frstoreCd)")
        sqlString.Append(" when '2APPL' THEN   (select APPL_PCT from frtbl where STORE_CD=:frstoreCd)")
        sqlString.Append(" when '3ELEC' THEN ( select ELEC_PCT from frtbl where STORE_CD=:frstoreCd)")
        sqlString.Append(" when '4MATT' THEN  ( select MATT_PCT from frtbl where STORE_CD=:frstoreCd)")
        sqlString.Append(" End As PCT")
        sqlString.Append(" FROM (select mjr.prod_grp_cd from itm i inner join inv_mnr mnr on i.mnr_cd = mnr.mnr_cd inner join inv_mjr mjr on mnr.mjr_cd = mjr.mjr_cd where i.itm_cd =:itmCd) ")

        sql = sqlString.ToString()
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = p_item

            dbCommand.Parameters.Add(":frstoreCd", OracleType.VarChar)
            dbCommand.Parameters(":frstoreCd").Value = p_frStoreCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)

            'Caluculate bareCost Cost
            percent = Convert.ToDouble(ds.Tables(0).Rows(0)("PCT"))

        Catch ex As Exception
            Throw
        End Try

        dbCommand.Dispose()
            dbConnection.Close()

            Return percent
    End Function

    Public Shared Function GetBareCost(ByVal p_item As String, ByVal p_orderingstoreCd As String) As Double
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter

        Dim ds As New DataSet
        Dim sql As String
        Dim sqlString As StringBuilder
        Dim itemSku As String = String.Empty
        Dim bareCost As Double = 0.0


        sqlString = New StringBuilder("select aa.store_grp_cd,aa.store_cd,aa.dt, aa.repl_cst, i.itm_cd, i.vsn, i.des, i.mnr_cd from ")
        sqlString.Append("(select sgrc.*, sgs.store_cd from store_grp$store sgs inner join store_grp_repl_clndr sgrc on sgs.store_grp_cd=sgrc.store_grp_cd ")
        sqlString.Append("where sgs.store_cd=:orderingstoreCd and sgs.store_GRP_CD IN  (select store_GRP_CD from store_grp where pricing_store_grp='Y') ")
        sqlString.Append(" and itm_cd=:itmCd order by dt desc) aa inner join itm i on i.itm_cd=aa.itm_cd where rownum = 1 ")


        sql = sqlString.ToString()
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = p_item

            dbCommand.Parameters.Add(":orderingstoreCd", OracleType.VarChar)
            dbCommand.Parameters(":orderingstoreCd").Value = p_orderingstoreCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)

            'Caluculate bareCost Cost
            bareCost = Convert.ToDouble(ds.Tables(0).Rows(0)("repl_cst"))

        Catch ex As Exception
            Throw
        End Try
        dbCommand.Dispose()
            dbConnection.Close()

            Return bareCost
    End Function
    ''' <summary>
    ''' Jan 25, 2018 - mariam
    ''' Gets landed cost based on Item SKU
    ''' return Landed cost
    ''' </summary>
    ''' <param name="StoreCode">the Ordering from Corporate Store code value to fetch stores for</param>
    Public Shared Function GetLandedCost(ByVal par_itm As String, ByVal par_storecode As String) As Double

        Dim returnVal As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Try
            Dim sql As String = "STD_CST.getLandedCst"
            Dim cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("p_itm", OracleType.VarChar, 50).Direction = ParameterDirection.Input
            cmd.Parameters.Add("p_str", OracleType.VarChar, 50).Direction = ParameterDirection.Input
            cmd.Parameters.Add("ret_landedcost", OracleType.Number).Direction = ParameterDirection.ReturnValue

            cmd.Parameters("p_itm").Value = par_itm
            cmd.Parameters("p_str").Value = par_storecode

            conn.Open()
            cmd.ExecuteNonQuery()
            returnVal = cmd.Parameters("ret_landedcost").Value
        Catch ex As Exception
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnVal
    End Function
    ''' <summary>
    ''' Jan 31, 2018 - mariam
    ''' Gets Tax Code by Vendor code
    ''' return Landed cost
    ''' </summary>
    Public Shared Function GetTaxCodeByVendorCd(ByVal p_vendorCd As String) As String

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = "select  tat_cd from VE where ve_cd =  :vendorCd"
        Dim taxCode As String

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
            dbCommand.Parameters.Add(":vendorCd", OracleType.VarChar)
            dbCommand.Parameters(":vendorCd").Value = p_vendorCd
            taxCode = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return taxCode
    End Function
    ''' <summary>
    ''' Feb 22, 2018 - mariam
    ''' Get SO Tax Code by Customer code
    ''' return Landed cost
    ''' </summary>
    Public Function GetTaxCodeByCustCd(ByVal p_CustCd As String) As String

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim sqlString As New StringBuilder
        Dim sql As String
        Dim taxCode As String = String.Empty

        sqlString.Append("select z.tax_cd from Zip2Tax z, Store s, Cust c ")
        sqlString.Append("where s.Zip_CD = z.Zip_CD ")
        sqlString.Append("and s.Fran_Cust_CD = c.cust_CD ")
        sqlString.Append("and s.Fran_Cust_CD = :custCd ")
        sql = sqlString.ToString()

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":custCd", OracleType.VarChar)
            dbCommand.Parameters(":custCd").Value = p_CustCd
            taxCode = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return taxCode
    End Function
    ''' <summary>
    ''' Feb 08, 2018 - mariam
    ''' Insert Purchase Order Items into FRPO and FRPO_LN tables
    ''' 
    ''' </summary>
    Public Function CreateFranchisePO(ByVal bulkItem As List(Of CartItemDtc), ByRef p_transdet As TransDtc) As String

        Dim objFRPOBiz As FranchisePOBiz = New FranchisePOBiz
        Dim UniqueSeq As String = String.Empty

        Dim query As String = String.Empty
        Dim res As String = String.Empty
        Dim msg As String = String.Empty
        Dim mon As String = String.Empty
        Dim dd As String = String.Empty
        Dim y As String = String.Empty

        Dim message As String = String.Empty

        Try

            query = "insert into frpo_ln ( ID, LN#, DEL_DOC_NUM, ITM_CD, QTY, PO_CST, PO_EXT_CST, SO_CST, SO_EXT_CST, CONFIRM_NUM, ARRIVAL_DT ) values "
            query = query + " (:id, :ln#, :deldocnum, :itm_cd, :qty,  :po_cst,  :po_ext_cst,  :so_cst,  :so_ext_cst, :confirm_number, :arrival_dt)"
            Using objCon As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                objCon.Open()
                Using objTrans = objCon.BeginTransaction(IsolationLevel.ReadCommitted)
                    'Using objCmd As OracleCommand = New OracleCommand(query, objCon)
                    Dim objCmd As OracleCommand = New OracleCommand(query, objCon)
                    Dim result As Integer = 0
                    Dim insTrans As Boolean = False
                    objCmd.Transaction = objTrans
                    objCmd.CommandType = CommandType.Text

                    'Loop List
                    For Each item As CartItemDtc In bulkItem
                        result = 0
                        'Parameters
                        objCmd.Parameters.Add("id", OracleType.Number).Value = item.ID
                        objCmd.Parameters.Add("ln#", OracleType.Number).Value = Convert.ToInt16(item.LineNo)
                        objCmd.Parameters.Add("itm_cd", OracleType.VarChar).Value = item.ItemCode
                        objCmd.Parameters.Add("deldocnum", OracleType.VarChar).Value = p_transdet.Del_Doc_Num
                        objCmd.Parameters.Add("qty", OracleType.Number).Value = Convert.ToInt16(item.Qty)
                        objCmd.Parameters.Add("po_cst", OracleType.Number).Value = Convert.ToDouble(item.PO_CST)
                        objCmd.Parameters.Add("po_ext_cst", OracleType.Number).Value = Convert.ToDouble(item.PO_Ext_CST)
                        objCmd.Parameters.Add("so_cst", OracleType.Number).Value = Convert.ToDouble(item.SO_CST)
                        objCmd.Parameters.Add("so_ext_cst", OracleType.Number).Value = Convert.ToDouble(item.SO_Ext_CST)
                        objCmd.Parameters.Add("confirm_number", OracleType.VarChar).Value = p_transdet.Confirm_Num
                        objCmd.Parameters.Add("arrival_dt", OracleType.DateTime).Value = DateTime.Today()
                        result = objCmd.ExecuteNonQuery()
                        'is item inserted

                        If result = 0 Then
                            Exit For
                        End If
                    Next

                    'if all items pass insert
                    If result = 0 Then
                        'msg error return
                        msg = "error"
                        objTrans.Rollback()
                        objCmd.Cancel()
                        objCon.Close()
                        Return msg
                        Exit Function
                    Else
                        'insert transdetails
                        Dim sql As New StringBuilder
                        'Dim message As String = String.Empty

                        'Insert Transaction Details
                        sql.Append("INSERT INTO FRPO (")
                        sql.Append("STORE_CD,  ")
                        sql.Append("STORE_NUM, ")
                        sql.Append("CUST_CD, ")
                        sql.Append("BUYER_INIT, ")
                        sql.Append("CORP_STORE_CD, ")
                        sql.Append("CORP_ZONE_CD, ")
                        sql.Append("VE_CD,     ")
                        sql.Append("SHIP_VIA, ")
                        sql.Append("PO_SRT_CD, ")
                        sql.Append("PO_CD , ")
                        sql.Append("ENTER_DATE, ")
                        sql.Append("USR_FLD_1, USR_FLD_2,   USR_FLD_3,  USR_FLD_4,  USR_FLD_5, ")
                        sql.Append(" DEL_DOC_NUM,  ID,  SUB_PO_CST,     TOT_PO_CST,    PO_TAX_CD, PO_TAX_AMT, ")
                        sql.Append("SUB_SO_CST,  ")
                        sql.Append("TOT_SO_CST, ")
                        sql.Append("SO_TAX_CD, ")
                        sql.Append("SO_TAX_AMT, ")
                        sql.Append("APRV_PSWD, ")
                        sql.Append("APRV_USER_INIT )")

                        sql.Append("VALUES( :STORE_CD,  ")
                        sql.Append(":STORE_NUM, ")
                        sql.Append(":CUST_CD, ")
                        sql.Append(":BUYER_INIT, ")
                        sql.Append(":CORP_STORE_CD, ")
                        sql.Append(":CORP_ZONE_CD, ")
                        sql.Append(":VE_CD, ")
                        sql.Append(":SHIP_VIA,  ")
                        sql.Append(":PO_SRT_CD, ")
                        sql.Append(":PO_CD ,  ")
                        sql.Append(":ENTER_DATE, ")
                        sql.Append(":USR_FLD_1, :USR_FLD_2, :USR_FLD_3, :USR_FLD_4, :USR_FLD_5, ")
                        sql.Append(":DEL_DOC_NUM, :ID,        :SUB_PO_CST,     :TOT_PO_CST, :PO_TAX_CD, :PO_TAX_AMT, ")
                        sql.Append(":SUB_SO_CST, ")
                        sql.Append(":TOT_SO_CST, ")
                        sql.Append(":SO_TAX_CD, ")
                        sql.Append(": SO_TAX_AMT, ")
                        sql.Append(":APRV_PSWD, ")
                        sql.Append(":APRV_USER_INIT ) ")

                        Dim objCmdTrans As OracleCommand = New OracleCommand(sql.ToString(), objCon)
                        objCmdTrans.Transaction = objTrans
                        objCmdTrans.CommandType = CommandType.Text

                        '26 parameters
                        objCmdTrans.Parameters.Add("STORE_CD", OracleType.VarChar).Value = p_transdet.FR_Store_CD
                        objCmdTrans.Parameters.Add("STORE_NUM", OracleType.VarChar).Value = p_transdet.FR_Store_Num
                        objCmdTrans.Parameters.Add("CUST_CD", OracleType.VarChar).Value = p_transdet.FR_Cust_CD
                        objCmdTrans.Parameters.Add("BUYER_INIT", OracleType.VarChar).Value = p_transdet.Buyer_Init
                        objCmdTrans.Parameters.Add("CORP_STORE_CD", OracleType.VarChar).Value = p_transdet.Corp_Store_CD
                        objCmdTrans.Parameters.Add("CORP_ZONE_CD", OracleType.VarChar).Value = p_transdet.Corp_Zone_CD

                        objCmdTrans.Parameters.Add("VE_CD", OracleType.VarChar).Value = p_transdet.VE_CD
                        objCmdTrans.Parameters.Add("SHIP_VIA", OracleType.VarChar).Value = p_transdet.Ship_Via
                        objCmdTrans.Parameters.Add("PO_SRT_CD", OracleType.VarChar).Value = p_transdet.PO_SRT_CD
                        objCmdTrans.Parameters.Add("PO_CD", OracleType.VarChar).Value = p_transdet.Confirm_Num
                        objCmdTrans.Parameters.Add("ENTER_DATE", OracleType.DateTime).Value = p_transdet.Create_Date

                        objCmdTrans.Parameters.Add("USR_FLD_1", OracleType.VarChar).Value = p_transdet.Approval_User_Init + " " + p_transdet.Confirm_Num
                        objCmdTrans.Parameters.Add("USR_FLD_2", OracleType.VarChar).Value = p_transdet.Comments2
                        objCmdTrans.Parameters.Add("USR_FLD_3", OracleType.VarChar).Value = p_transdet.Comments3
                        objCmdTrans.Parameters.Add("USR_FLD_4", OracleType.VarChar).Value = p_transdet.Comments4
                        objCmdTrans.Parameters.Add("USR_FLD_5", OracleType.VarChar).Value = p_transdet.Comments5

                        objCmdTrans.Parameters.Add("DEL_DOC_NUM", OracleType.VarChar).Value = p_transdet.Del_Doc_Num
                        objCmdTrans.Parameters.Add("ID", OracleType.Number).Value = Convert.ToInt64(p_transdet.ID)

                        objCmdTrans.Parameters.Add("SUB_PO_CST", OracleType.Number).Value = Convert.ToDouble(p_transdet.PO_Sub_Total)
                        objCmdTrans.Parameters.Add("TOT_PO_CST", OracleType.Number).Value = Convert.ToDouble(p_transdet.PO_Total)
                        objCmdTrans.Parameters.Add("PO_TAX_CD", OracleType.VarChar).Value = p_transdet.PO_TAX_CD
                        objCmdTrans.Parameters.Add("PO_TAX_AMT", OracleType.Number).Value = Convert.ToDouble(p_transdet.PO_TAX_AMT)

                        objCmdTrans.Parameters.Add("SUB_SO_CST", OracleType.Number).Value = Convert.ToDouble(p_transdet.SO_Sub_Total)
                        objCmdTrans.Parameters.Add("TOT_SO_CST", OracleType.Number).Value = Convert.ToDouble(p_transdet.SO_Total)
                        objCmdTrans.Parameters.Add("SO_TAX_CD", OracleType.VarChar).Value = p_transdet.SO_TAX_CD
                        objCmdTrans.Parameters.Add("SO_TAX_AMT", OracleType.Number).Value = Convert.ToDouble(p_transdet.SO_TAX_AMT)

                        objCmdTrans.Parameters.Add("APRV_PSWD", OracleType.VarChar).Value = p_transdet.Approval_Pswd
                        objCmdTrans.Parameters.Add("APRV_USER_INIT", OracleType.VarChar).Value = p_transdet.Approval_User_Init

                        Dim resultFRPO As Integer = objCmdTrans.ExecuteNonQuery()
                        If resultFRPO = "1" Then
                            message = "Successfully Created Franchise Purchase Order"
                            objTrans.Commit()
                        Else
                            message = "Error While Creating Franchise Purchase Order"
                            objTrans.Rollback()
                            objCmdTrans.Cancel()
                            objCon.Close()
                            Return message
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception

            Return "Error While Creating Franchise Purchase Order in FranchisePODac-CreateFranchisePO. " & ex.Message.ToString()

        Finally
        End Try

        Return message
    End Function

    'Get emp_init by App_Pass
    Public Function GetApprUserInitByPass(ByVal p_UserPass As String) As String
        'Select emp_init where app_pass =p_UserPass
        Dim empInit As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = "Select emp_cd from emp where app_pass  =  :appPass"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
            dbCommand.Parameters.Add(":appPass", OracleType.VarChar)
            dbCommand.Parameters(":appPass").Value = p_UserPass
            empInit = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return empInit
    End Function

    Public Function GetTableRowCount(ByVal p_TableName As String) As Long
        'Select emp_init where app_pass =p_UserPass
        Dim count As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = "select 1 from " & p_TableName & " where rownum = 1 "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
            count = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return count
    End Function

    'check is Valid item
    Public Function isvaliditmFR(ByVal p_itm As String, ByVal p_emp_init As String) As String
        '1 - max 9 char - in front end
        '2 - itm_tp.inventory ="Y"
        '3 - std_multi_co.isValiditm logic
        '4 - itm$itm_srt.itm_srt_cd = "LFL"
        '5 - NO items itm.mnr_cd="97xxxx" - validate in front end
        '6 - cannot select an item if it contains the an item sort code = ‘FRA’ (ITM$ITM_SRT.ITM_SRT_CD = ‘FRA’)

        Const Const_97 As String = "97"
        Dim v_value As String = "N"

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand2 As OracleCommand
        Dim dbReader2 As OracleDataReader
        Dim sql2 As StringBuilder = New StringBuilder()

        Dim sql As String = " SELECT std_multi_co.isValiditm('" & p_emp_init & "','" & p_itm & "') v_data "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("v_data")
            Else
                v_value = "N"
            End If

        Catch
            v_value = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        'dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        Else  'Alice update on Sep 27, 2018 for rule 2 and 4
            sql2.Append("select i.itm_cd,  i.mnr_cd, isrt.itm_srt_cd, itp.inventory ")
            sql2.Append("from itm i, itm$itm_srt isrt, itm_tp itp ")
            sql2.Append("where i.itm_cd = isrt.itm_cd and i.itm_tp_cd = itp.itm_tp_cd and substr(mnr_cd,0,2)!= " & Const_97 & " and isrt.itm_srt_cd='LFL' and itp.inventory='Y' and i.itm_cd= :itm_cd ")


            dbCommand2 = DisposablesManager.BuildOracleCommand(sql2.ToString(), dbConnection)
            dbCommand2.Parameters.Add(":itm_cd", OracleType.VarChar)
            dbCommand2.Parameters(":itm_cd").Value = p_itm
            dbReader2 = DisposablesManager.BuildOracleDataReader(dbCommand2)

            If dbReader2.Read() Then
                v_value = dbReader2.Item("itm_cd") + ";"
                v_value = v_value + dbReader2.Item("mnr_cd") + ";"
                v_value = v_value + dbReader2.Item("itm_srt_cd") + ";"
                v_value = v_value + dbReader2.Item("inventory")
            Else
                v_value = "N"
            End If
        End If

        Return v_value
    End Function

    'prevent any item where the item inventory type = ‘N’; only allow if the item inventory type = ‘Y’
    Public Function isvalidItemforInventoryType(ByVal p_itm As String) As String
        Dim v_value As String = "N"

        Dim cnt As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) count from itm i inner join itm_tp p on i.itm_tp_cd =p.itm_tp_cd where p.inventory='Y' and i.itm_cd=:itm_cd "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":itm_cd", OracleType.VarChar)
            dbCommand.Parameters(":itm_cd").Value = p_itm

            cnt = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw ex
            Return v_value
        Finally
            dbConnection.Close()
        End Try

        If cnt > 0 Then
            v_value = "Y"
        Else
            v_value = "N"
        End If

        Return v_value
    End Function


    Public Function isvalidApproverPassword(ByVal p_Approverpassword As String) As String
        '1 - max 9 char - in front end
        '2 - itm_tp.inventory ="Y"
        '3 - std_multi_co.isValiditm logic
        '4 - itm$itm_srt.itm_srt_cd = "LFL"
        '5 - NO items itm.mnr_cd="97xxxx" - validate in front end

        Dim v_value As String = "N"

        Dim cnt As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) count from emp e, emp$emp_tp etp where app_pass=:Approverpassword and e.emp_cd = etp.emp_cd and etp.emp_tp_cd='APP' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":Approverpassword", OracleType.VarChar)
            dbCommand.Parameters(":Approverpassword").Value = p_Approverpassword

            cnt = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw ex
            Return v_value
        Finally
            dbConnection.Close()
        End Try

        If cnt = 1 Then
            v_value = "Y"
        Else
            v_value = "N"
        End If

        Return v_value
    End Function


    Public Function GetValueByLastRow(ByVal p_columnName As String, ByVal p_TableName As String) As String
        'Select emp_init where app_pass =p_UserPass
        Dim result As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = "select " & p_columnName & " from " & p_TableName & " where rowid = (select max(rowid) from " & p_TableName & ")"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)
            result = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return result
    End Function

    'Mariam Added
    Public Function GetZonesForCorpStoreCd_Franchise(ByVal p_CorpStoreCd As String) As DataSet
        'MCCL added NOV 24, 2017
        ' group by eliminates duplicates
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT distinct Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, Z.ZONE_CD || ' - ' || Z.DES as full_desc ")
        sql.Append(" FROM ZONE z, ZIP2ZONE, zone_grp zg  ")
        sql.Append(" WHERE z.ZONE_CD = ZIP2ZONE.ZONE_CD ")
        sql.Append("     AND ZIP2ZONE.ZIP_CD = (select zip_cd from store where store_cd=:p_CorpStoreCd) ")
        sql.Append("     and z.zone_cd = zg.zone_cd  ")

        Try
            With dbCommand
                .Connection = dbConnection
                .CommandText = sql.ToString()
                '.Parameters.Add(":p_FrStoreNum", OracleType.VarChar)
                '.Parameters(":p_FrStoreNum").Value = p_CorpStoreCd

                'Alice update on May 29, 2018
                .Parameters.Add(":p_CorpStoreCd", OracleType.VarChar)
                .Parameters(":p_CorpStoreCd").Value = p_CorpStoreCd


            End With

            dbConnection.Open()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function


    'Alice Added on May 30, 2018
    Public Function GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(ByVal p_CorpStoreCd As String, ByVal p_FranchiseStoreCd As String) As DataSet

        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT distinct Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, Z.ZONE_CD || ' - ' || Z.DES as full_desc ")
        sql.Append(" FROM ZONE z inner join zone_grp zg on z.zone_cd=zg.zone_cd WHERE z.DELIVERY_STORE_CD=:p_CorpStoreCd ")
        sql.Append("And z.zone_cd in (select zz.zone_cd from store s inner join ZIP2ZONE zz on s.zip_cd=zz.zip_cd where s.store_cd=:p_FranchiseStoreCd)")

        Try
            With dbCommand
                .Connection = dbConnection
                .CommandText = sql.ToString()
                '.Parameters.Add(":p_FrStoreNum", OracleType.VarChar)
                '.Parameters(":p_FrStoreNum").Value = p_CorpStoreCd

                'Alice update on May 29, 2018
                .Parameters.Add(":p_CorpStoreCd", OracleType.VarChar)
                .Parameters(":p_CorpStoreCd").Value = p_CorpStoreCd
                .Parameters.Add(":p_FranchiseStoreCd", OracleType.VarChar)
                .Parameters(":p_FranchiseStoreCd").Value = p_FranchiseStoreCd


            End With

            dbConnection.Open()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function
    'Alice Added
    Public Function GetZonesForCorpStoreCd_Franchise1(ByVal p_CorpStoreCd As String) As DataSet
        'MCCL added NOV 24, 2017
        ' group by eliminates duplicates
        Dim zones As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT distinct Z.ZONE_CD, Z.DES, Z.DELIVERY_STORE_CD, Z.ZONE_CD || ' - ' || Z.DES as full_desc ")
        sql.Append(" FROM ZONE z inner join zone_grp zg on z.zone_cd=zg.zone_cd WHERE z.DELIVERY_STORE_CD=:p_CorpStoreCd ORDER BY z.ZONE_CD ")

        Try
            With dbCommand
                .Connection = dbConnection
                .CommandText = sql.ToString()
                '.Parameters.Add(":p_FrStoreNum", OracleType.VarChar)
                '.Parameters(":p_FrStoreNum").Value = p_CorpStoreCd

                'Alice update on May 29, 2018
                .Parameters.Add(":p_CorpStoreCd", OracleType.VarChar)
                .Parameters(":p_CorpStoreCd").Value = p_CorpStoreCd


            End With

            dbConnection.Open()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(zones)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return zones
    End Function

    ''' <summary>
    ''' Apr 23, 2018 - mariam
    ''' Gets the stores within the same company code assiged to the user's home store code. Apply MCCL #1
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Function GetCorpStores() As DataSet
        Const _Const_CorpStore = "COR"
        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try

            sql = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc  " +
                    " from store s, store_grp$store sg " +
                    " where sg.store_grp_cd = '" & _Const_CorpStore & "' and s.store_cd = sg.store_cd"
            sql = sql & " order by s.store_cd"

            With dbCommand
                .Connection = dbConnection
                .CommandText = sql
            End With


            dbConnection.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Jun 20, 2018 - alice
    ''' Get corporate stores codes associated to the Franchise Store # from the FR_SHIPPING_SCHEDULE table 
    ''' </summary>

    Public Function GetCorpStoresforFrPOEntry(ByVal p_supUserFlag As String, ByVal fr_store_cd As String) As DataSet

        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            If (isNotEmpty(p_supUserFlag) And p_supUserFlag.Equals("Y")) Then
                sql = "select store_cd, store_name, store_cd || ' - ' || store_name as full_desc from store" +
                       " where store_cd in (select store_num from fr_shipping_schedule) and store_cd in ( select store_cd from  store_grp$store where store_grp_cd='COR') order by store_cd"
            Else
                sql = " select store_cd, store_name, store_cd || ' - ' || store_name as full_desc from store" +
                      " where store_cd in (select order_co_store from FR_Co_Store where fr_store_cd=:fr_store_cd) order by store_cd"

                dbCommand.Parameters.Add(":fr_store_cd", OracleType.VarChar)
                dbCommand.Parameters(":fr_store_cd").Value = fr_store_cd
            End If



            With dbCommand
                .Connection = dbConnection
                .CommandText = sql
            End With


            dbConnection.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Apr 30, 2018 - mariam
    ''' Insert Purchase Order Items into FR_Shipping_Schedule and FR_Shipping_Schedule_ln tables
    ''' 
    ''' </summary>
    Public Function CreateFR_ShippingSchedule(ByVal bulkItem As List(Of FRShippingSch_DetDtc), ByRef p_trans As FRShippingSch_TransDtc) As String
        'Dim objCmd As OracleCommand
        Const Const_LEON As String = "LEON"
        Dim objFRPOBiz As FranchisePOBiz = New FranchisePOBiz
        Dim UniqueSeq As String = String.Empty

        Dim query As String = String.Empty
        Dim res As String = String.Empty
        Dim msg As String = String.Empty
        Dim mon As String = String.Empty
        Dim dd As String = String.Empty
        Dim y As String = String.Empty
        Dim confirmationNum As String = String.Empty
        Dim message As String = String.Empty
        Dim sb As StringBuilder = New StringBuilder()
        'Dim objTrans As OracleTransaction = Nothing
        Try
            'Get Approver User Init
            'ApprUserInit = GetApprUserInitByPass(p_transdet.Approval_Pswd)

            ''generate Confirmation # - 13 characters
            '1 - mm ; 2-dd; 3-y; 4-FranchiseStoreCode; 5-LEON; 6-2 char system generated AA., AB...ZZ, AA(rollback after ZZ)

            confirmationNum = objFRPOBiz.GenerateConfirmNum_FSS()

            p_trans.Confirm_Num = confirmationNum
            ' p_transdet.Approval_User_Init = ApprUserInit

            sb.Append("insert into fr_shipping_schedule_ln ( CONFIRM_NUM, DAY_OF_WEEK, CORP_STORE_CD, DEL_ZONE_CD, UPD_DT ) ")
            sb.Append(" values( ")
            sb.Append(" :CONFIRM_NUM, :DAY_OF_WEEK, :CORP_STORE_CD, :DEL_ZONE_CD, :UPD_DT )")
            query = sb.ToString()

            Using objCon As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                objCon.Open()
                Using objTrans = objCon.BeginTransaction(IsolationLevel.ReadCommitted)
                    'Using objCmd As OracleCommand = New OracleCommand(query, objCon)
                    Dim objCmd As OracleCommand = New OracleCommand(query, objCon)
                    Dim result As Integer = 0
                    Dim insTrans As Boolean = False
                    objCmd.Transaction = objTrans
                    objCmd.CommandType = CommandType.Text

                    'Loop List
                    For Each det As FRShippingSch_DetDtc In bulkItem
                        result = 0
                        'Parameters
                        '' :CONFIRM_NUM, :DAY_OF_WEEK, :CORP_STORE_CD, :DEL_ZONE_CD, :UPD_DT

                        objCmd.Parameters.Add("CONFIRM_NUM", OracleType.VarChar).Value = p_trans.Confirm_Num
                        objCmd.Parameters.Add("DAY_OF_WEEK", OracleType.VarChar).Value = det.Day_Of_Week
                        objCmd.Parameters.Add("CORP_STORE_CD", OracleType.VarChar).Value = det.Corp_Store_CD
                        objCmd.Parameters.Add("DEL_ZONE_CD", OracleType.VarChar).Value = det.Corp_Del_Zone_CD
                        objCmd.Parameters.Add("UPD_DT", OracleType.DateTime).Value = det.Updated_Date

                        result = objCmd.ExecuteNonQuery()
                    Next

                    'if all items pass insert
                    If result = 0 Then
                        'msg error return
                        msg = "error"
                        objTrans.Rollback()
                        objCmd.Cancel()
                        objCon.Close()
                        Return msg
                        Exit Function
                    Else
                        'insert transdetails
                        'Dim message As String = String.Empty

                        'Insert Transaction Details
                        sb = New StringBuilder
                        sb.Append("insert into fr_shipping_schedule (STORE_NUM, CO_CD, CUST_CD, CITY, ")
                        sb.Append(" APRV_PSWD, APRV_USER_INIT, CONFIRM_NUM, UPD_DT, UPD_BY ) values( ")
                        sb.Append(" :STORE_NUM, :CO_CD, :CUST_CD, :CITY, :APRV_PSWD, :APRV_USER_INIT, :CONFIRM_NUM, :UPD_DT, :UPD_BY )")

                        query = sb.ToString()
                        Dim objCmdTrans As OracleCommand = New OracleCommand(query, objCon)
                        objCmdTrans.Transaction = objTrans
                        objCmdTrans.CommandType = CommandType.Text

                        objCmdTrans.Parameters.Add("STORE_NUM", OracleType.VarChar).Value = p_trans.FR_Store_Num
                        objCmdTrans.Parameters.Add("CO_CD", OracleType.VarChar).Value = p_trans.FR_CO_CD
                        objCmdTrans.Parameters.Add("CUST_CD", OracleType.VarChar).Value = p_trans.FR_Cust_CD
                        objCmdTrans.Parameters.Add("CITY", OracleType.VarChar).Value = p_trans.City
                        objCmdTrans.Parameters.Add("APRV_PSWD", OracleType.VarChar).Value = p_trans.Approval_Pswd
                        objCmdTrans.Parameters.Add("APRV_USER_INIT", OracleType.VarChar).Value = p_trans.Approval_User_Init
                        objCmdTrans.Parameters.Add("CONFIRM_NUM", OracleType.VarChar).Value = p_trans.Confirm_Num
                        '' objCmdTrans.Parameters.Add("UPD_DT", OracleType.DateTime).Value = p_trans.Upd_Dt      'check what date to save
                        objCmdTrans.Parameters.Add("UPD_DT", OracleType.DateTime).Value = DateTime.Today()
                        objCmdTrans.Parameters.Add("UPD_BY", OracleType.VarChar).Value = p_trans.Upd_By

                        Dim resultFRPO As Integer = objCmdTrans.ExecuteNonQuery()
                        If resultFRPO = "1" Then
                            message = "Successfully Created Franchise Purchase Order"
                            objTrans.Commit()
                        Else
                            message = "Error While Creating Franchise Purchase Order"
                            objTrans.Rollback()
                            objCmdTrans.Cancel()
                            objCon.Close()
                            Return message
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception

            Return "Error While Creating Franchise Purchase Order in FranchisePODac-CreateFr_ShippingSchedule. " & ex.Message.ToString()

        Finally
        End Try

        Return message
    End Function

End Class


