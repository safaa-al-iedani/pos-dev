﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Collections.Generic

Public Class WPPEDac
    Inherits BaseDac

    ''' <summary>
    ''' Inserts a WPPE record to the WPPE table
    ''' </summary>

    Public Shared Function InsertWPPE(ByVal listWPPEs As List(Of WPPEBiz)) As Boolean
        Dim success As Boolean = True
        Dim sql As New StringBuilder
        sql.Append("INSERT INTO WPPE (")
        sql.Append("CO_CD,  ")
        sql.Append("PRC_CD,  ")
        sql.Append("ITM_CD, ")
        sql.Append("CORP, ")
        sql.Append("FRAN_CORP, ")
        sql.Append("FRAN, ")
        sql.Append("QUEBEC, ")
        sql.Append("QUE_FRAN, ")
        sql.Append("OTHER, ")
        sql.Append("EFF_DT, ")
        sql.Append("END_DT, ")
        sql.Append("UPD_DT, ")
        sql.Append("UPD_TIME, ")
        sql.Append("EMP_CD ) ")
        sql.Append("VALUES( :CO_CD,  ")
        sql.Append(":PRC_CD, ")
        sql.Append(":ITM_CD, ")
        sql.Append(":CORP, ")
        sql.Append(":FRAN_CORP, ")
        sql.Append(":FRAN, ")
        sql.Append(":QUEBEC, ")
        sql.Append(":QUE_FRAN, ")
        sql.Append(":OTHER, ")
        sql.Append(":EFF_DT, ")
        sql.Append(":END_DT, ")
        sql.Append(":UPD_DT, ")
        sql.Append(":UPD_TIME,  ")
        sql.Append(":EMP_CD) ")

        Try

            Using objCon As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                objCon.Open()
                Using objTrans = objCon.BeginTransaction(IsolationLevel.ReadCommitted)

                    Dim objCmd As OracleCommand = New OracleCommand(sql.ToString(), objCon)
                    objCmd.CommandType = CommandType.Text
                    objCmd.Transaction = objTrans


                    For Each curWPPE As WPPEBiz In listWPPEs

                        objCmd.Parameters.Add("CO_CD", OracleType.VarChar).Value = curWPPE.co_cd.ToUpper()
                        objCmd.Parameters.Add("PRC_CD", OracleType.VarChar).Value = curWPPE.prc_cd.ToUpper()
                        objCmd.Parameters.Add("ITM_CD", OracleType.VarChar).Value = curWPPE.itm_cd
                        objCmd.Parameters.Add("CORP", OracleType.Double).Value = curWPPE.corp
                        objCmd.Parameters.Add("FRAN_CORP", OracleType.Double).Value = curWPPE.corp_fran
                        objCmd.Parameters.Add("FRAN", OracleType.Double).Value = curWPPE.fran
                        objCmd.Parameters.Add("QUEBEC", OracleType.Double).Value = curWPPE.que
                        objCmd.Parameters.Add("QUE_FRAN", OracleType.Double).Value = curWPPE.que_fran
                        objCmd.Parameters.Add("OTHER", OracleType.Double).Value = curWPPE.other
                        objCmd.Parameters.Add("EFF_DT", OracleType.VarChar).Value = curWPPE.eff_dt
                        objCmd.Parameters.Add("END_DT", OracleType.VarChar).Value = curWPPE.end_dt
                        objCmd.Parameters.Add("UPD_DT", OracleType.VarChar).Value = curWPPE.upd_dt
                        objCmd.Parameters.Add("UPD_TIME", OracleType.VarChar).Value = curWPPE.upd_time
                        objCmd.Parameters.Add("EMP_CD", OracleType.VarChar).Value = curWPPE.emp_cd
                        If objCmd.ExecuteNonQuery() = 0 Then
                            success = False
                        End If
                    Next

                    If success Then
                        objTrans.Commit()
                    Else
                        objTrans.Rollback()
                    End If
                    objCmd.Cancel()

                End Using
                objCon.Close()
            End Using

        Catch ex As Exception
            success = False
        End Try

        Return success

    End Function

    Public Shared Function GetStoreGroup(ByVal p_co_cd As String) As DataSet

        Dim ds As New DataSet
        Dim sql As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Try

            Select Case p_co_cd
                Case "LFL"
                    sql = "select store_grp_cd,  store_grp_cd||' - '||des name from store_grp where pricing_store_grp='Y' and (SUBSTR(store_grp_cd, 1, 2)='ZO' OR SUBSTR(store_grp_cd, 1, 1)='F')  order by store_grp_cd "

                Case "BW1"
                    sql = " select store_grp_cd,  store_grp_cd||' - '||des name from store_grp where pricing_store_grp='Y' and (SUBSTR(store_grp_cd, 1, 2)='ZB' OR SUBSTR(store_grp_cd, 1, 2)='ZC' OR SUBSTR(store_grp_cd, 1, 1)='B')  order by store_grp_cd "

                Case Else
                    sql = "select store_grp_cd,  store_grp_cd||' - '||des name from store_grp where pricing_store_grp='Y' order by store_grp_cd "
            End Select

            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        End Try
        dbCommand.Dispose()
        dbConnection.Close()
        Return ds
    End Function

    Public Shared Function isValid_co_cd(ByVal p_co_cd As String, ByVal p_emp_init As String) As String

        Dim v_IND As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' ind from dual " &
                            " where std_multi_co2.isvalidco('" & p_emp_init & "','" & p_co_cd & "') = 'Y' "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_IND = dbReader.Item("ind")
            Else
                v_IND = "N"
            End If
        Catch
            v_IND = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If String.IsNullOrEmpty(v_IND) Then
            Return "N"
        Else
            Return v_IND
        End If

    End Function
    Public Shared Function isvalidSKU(ByVal p_SKU As String, ByVal p_emp_init As String) As String
        '1 - max 9 char
        '2 - must exist in ITM
        '3 - apply std_multi_co.isValiditm logic
        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) as count from itm where itm_cd='" & p_SKU & "' AND std_multi_co.isValiditm('" & p_emp_init & "','" & p_SKU & "')='Y'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("count")
            End If
        Catch
            v_value = 0
        End Try
        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Public Shared Function isvalidPromotionCode(ByVal po_cd As String) As String

        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) as count from PROM_PRC_TP where prc_cd='" & po_cd & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("count")
            End If
        Catch
            v_value = 0
        End Try
        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function

    Public Shared Function GetSearchResult(ByVal curWPPE As WPPEBiz, ByVal p_vsn As String) As DataSet
        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim proceed As Boolean = False
        Dim LikeQry As String
        Dim sql As String = String.Empty
        Try


            If curWPPE.co_cd = String.Empty And curWPPE.prc_cd = String.Empty And curWPPE.itm_cd = String.Empty And p_vsn = String.Empty And curWPPE.eff_dt = String.Empty And curWPPE.end_dt = String.Empty Then
                sql = "select w.*, i.vsn, i.des from wppe w inner join itm i on w.itm_cd=i.itm_cd "
            Else
                sql = "select w.*, i.vsn, i.des from wppe w inner join itm i on w.itm_cd=i.itm_cd where "
            End If


            If Not String.IsNullOrEmpty(curWPPE.co_cd) Then
                If proceed = True Then sql = sql & " AND "
                If InStr(curWPPE.co_cd, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "w.co_cd " & LikeQry & " '" & Replace(curWPPE.co_cd.ToUpper(), "'", "''") & "' "
                proceed = True
            End If


            If Not String.IsNullOrEmpty(curWPPE.prc_cd) Then
                If proceed = True Then sql = sql & " AND "
                If InStr(curWPPE.prc_cd, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "w.prc_cd " & LikeQry & " '" & Replace(curWPPE.prc_cd.ToUpper(), "'", "''") & "' "
                proceed = True
            End If


            If Not String.IsNullOrEmpty(curWPPE.itm_cd) Then
                If proceed = True Then sql = sql & "AND "
                If InStr(curWPPE.itm_cd, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "w.itm_cd " & LikeQry & " '" & Replace(curWPPE.itm_cd.ToUpper(), "'", "''") & "' "
                proceed = True
            End If

            If Not String.IsNullOrEmpty(p_vsn) Then
                If proceed = True Then sql = sql & "AND "
                If InStr(p_vsn, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "i.vsn " & LikeQry & " '" & Replace(p_vsn.ToUpper(), "'", "''") & "' "
                proceed = True
            End If

            If Not String.IsNullOrEmpty(curWPPE.eff_dt) Then
                If proceed = True Then sql = sql & "AND "
                LikeQry = "="
                sql = sql & "w.eff_dt " & LikeQry & " '" & Replace(curWPPE.eff_dt, "'", "''") & "' "
                proceed = True
            End If

            If Not String.IsNullOrEmpty(curWPPE.end_dt) Then
                If proceed = True Then sql = sql & "AND "
                LikeQry = "="
                sql = sql & "w.end_dt " & LikeQry & " '" & Replace(curWPPE.end_dt, "'", "''") & "' "
                proceed = True
            End If



            sql = sql & (" order by w.co_cd ")


            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        End Try
        dbCommand.Dispose()
        dbConnection.Close()
        Return ds

    End Function

    Public Shared Function GetPromotionDesc(ByVal po_cd As String) As DataSet
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbAdapter As OracleDataAdapter
        Dim ds As New DataSet
        Dim sql As String
        Dim itemSku As String = String.Empty

        sql = "select * from PROM_PRC_TP where prc_cd=:po_cd"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(sql), dbConnection)
            dbCommand.Parameters.Add(":po_cd", OracleType.VarChar)
            dbCommand.Parameters(":po_cd").Value = po_cd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return ds
    End Function

End Class
