﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Linq

Public Class DiscountDac
    Inherits BaseDac

    ''' <summary>
    ''' Retrieves the list of all active/non-expired discounts in the system.  The return list 
    ''' includes for sure, the levels 'H' and 'E'.   The level 'L' will be included based on
    ''' the parameter provided
    ''' </summary>
    ''' <param name="includeLineLevel">TRUE to include the L level discount; FALSE to exclude them</param>
    ''' <returns>a datatable containing the non-expired discounts in the system.</returns>
    Public Function GetDiscounts(ByVal includeLineLevel As Boolean) As DataTable

        Dim discDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT DISC_CD AS CD, DISC_CD || ' - ' || DES AS DES, TP_CD, NVL(AMT,0) AS AMT, ")
        sql.Append("DISC_LEVEL, BY_MNR, COND_DISC, APPLY_PACKAGE FROM DISC ")
        sql.Append("WHERE EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ")
        If (Not includeLineLevel) Then
            sql.Append(" AND DISC_LEVEL <> 'L' ")
        End If
        sql.Append("ORDER BY DES")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discDataSet)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discDataSet.Tables(0)
    End Function

    Public Function GetDiscounts(ByVal includeLineLevel As Boolean, ByVal empInit As String) As DataTable
        ' Daniela MCCL
        Dim discDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT DISC_CD AS CD, DISC_CD || ' - ' || DES AS DES, TP_CD, NVL(AMT,0) AS AMT, ")
        sql.Append("DISC_LEVEL, BY_MNR, COND_DISC, APPLY_PACKAGE FROM DISC ")
        sql.Append("WHERE Std_multi_co.isValidDisc(upper('" & empInit & "'), DISC_CD) = 'Y' and EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') ")
        If (Not includeLineLevel) Then
            sql.Append(" AND DISC_LEVEL <> 'L' ")
        End If
        sql.Append("ORDER BY DES")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discDataSet)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discDataSet.Tables(0)
    End Function
    ''' <summary>
    ''' Retrieves the detail information for the selected discount
    ''' </summary>
    ''' <param name="discCd">the discount code</param>
    ''' <returns>an object of the discount information for the discount code requested </returns>
    Public Function GetDiscount(ByVal discCd As String) As DiscountDtc

        Dim disc As DiscountDtc
        Dim discDatSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT DISC_CD AS CD, DISC_CD || ' - ' || DES AS DES, TP_CD, NVL(AMT,0) AS AMT, ")
        sql.Append("DISC_LEVEL, BY_MNR, COND_DISC, APPLY_PACKAGE, EXP_DT FROM DISC ")
        sql.Append("WHERE disc_cd = :discCd ")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters(":discCd").Value = discCd
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discDatSet)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        If SystemUtils.dataSetHasRows(discDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(discDatSet.Tables(0).Rows(0)) Then

            disc = DiscountUtils.GetDiscountDtc(discDatSet.Tables(0).Rows(0))
        End If
        Return disc
    End Function

    ''' <summary>
    ''' Retrieves details from the CRM.MULTI_DISC table for those discount records
    ''' that are NOT associated to any CRM.TEMP_ITM record, due to a line void event.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <returns>table containing the orphaned discounts details</returns>
    Public Function GetDiscountsToDelete(ByVal sessionId As String, Optional ByVal isCanceled As Boolean = False) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT LINE_ID as LNSEQ_NUM, DISC_CD, DISC_AMT, SEQ_NUM ")
        dbSql.Append("FROM MULTI_DISC ")
        'MM-9870
        If isCanceled Then
            dbSql.Append("WHERE session_id = :sessionId AND line_id IN ")
        Else
            dbSql.Append("WHERE session_id = :sessionId AND line_id NOT IN ")
        End If

        dbSql.Append("   (SELECT ROW_ID FROM TEMP_ITM WHERE session_id = :sessionId)")

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql.ToString()

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function

    ''' <summary>
    ''' Finds out if new discount records have to be added due to the last new lines added to the order.
    ''' If new discount records have to be added, then adds them and notifies the caller with a TRUE.
    ''' The new lines are considered to be the ones inserted after the LnSeqNum passed in.
    ''' These discounts get added here IF applicable.  This method runs after an ADD line
    ''' event triggered in the UI and ONLY IF a HEADER discount (PCT or AMT) exists in the order
    ''' </summary>
    ''' <param name="sessionId">the session ID for the order being processed</param>
    ''' <param name="lnSeqNum">the last line in the order before the last ADD line occurred</param>
    ''' <returns>TRUE if discounts were added; FALSE if not</returns>
    Public Function CheckDiscountsForLineAddSOE(ByVal sessionId As String, ByVal lnSeqNum As String) As Integer

        Dim discountsAdded As Boolean = False
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim discount As DiscountDtc = Nothing
        Dim byMnr, isCond, applyPkg As Boolean
        Dim discCd, discDes, discTp, discLevel, expDt As String
        Dim discAmt As Double
        Dim seqNum As Integer = 0

        'dbSql.Append("SELECT * FROM DISC WHERE DISC_CD = ")
        'dbSql.Append(" (SELECT DISC_CD FROM MULTI_DISC WHERE session_id = :sessionId AND ROWNUM = 1) ")
        dbSql.Append("select D.*,MD.SEQ_NUM from DISC D Inner Join MULTI_DISC MD on D.DISC_CD=MD.DISC_CD")
        dbSql.Append(" where MD.DISC_LEVEL='H' and SESSION_ID=:sessionId and rownum=1")
        Try
            ' === Retrieves the current HEADER discount on the Order
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql.ToString()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            While (dbReader.Read)
                discLevel = AppConstants.Discount.LEVEL_HEADER   'it can only be header
                byMnr = If(IsDBNull(dbReader("by_mnr")) OrElse "N" = dbReader("by_mnr"), False, True)
                isCond = If(IsDBNull(dbReader("cond_disc")) OrElse "N" = dbReader("cond_disc"), False, True)
                applyPkg = If(IsDBNull(dbReader("apply_package")) OrElse "N" = dbReader("apply_package"), False, True)
                discCd = If(IsDBNull(dbReader("disc_cd")), String.Empty, dbReader("disc_cd"))
                discDes = If(IsDBNull(dbReader("des")), discCd, (discCd & " - " & dbReader("des")))
                discTp = If(IsDBNull(dbReader("tp_cd")), String.Empty, dbReader("tp_cd"))
                discAmt = If(IsDBNull(dbReader("amt")), 0.0, dbReader("amt"))
                expDt = If(IsDBNull(dbReader("exp_dt")), String.Empty, dbReader("exp_dt"))
                seqNum = dbReader("SEQ_NUM")
                discount = DiscountUtils.GetDiscountDtc(discCd, discDes, discAmt, discTp, discLevel, byMnr, isCond, applyPkg, expDt)
            End While
            dbReader.Close()

            ' === Finds out if discounts records are to be added and adds them if so.
            If (Not IsNothing(discount)) Then
                Dim lastLnSeqBeforeAdd = CInt(lnSeqNum)
                Dim discountableLines As DataTable = GetDiscountableLines(sessionId, discount)

                '== uses a single transaction, so that all discounts get added or none if an error
                dbCommand.Transaction = dbConnection.BeginTransaction
                For Each dr As DataRow In discountableLines.Rows
                    'only those lines that were added after the one passed in, are checked
                    If (CInt(dr("DEL_DOC_LN#")) > lastLnSeqBeforeAdd) Then
                        InsertMultiDiscRow(dbCommand, discount, discAmt, sessionId, dr("DEL_DOC_LN#"), seqNum)
                        discountsAdded = True
                    End If
                Next
                dbCommand.Transaction.Commit()
            End If

        Catch ex As Exception
            If (Not IsNothing(dbCommand.Transaction)) Then dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return seqNum
    End Function

    ''' <summary>
    ''' Retrieves all the details for the discounts that have been added so far to the order
    ''' represented by the session ID passed-in.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <param name="lnSeqNum">the line number for which to retrieve the applied discounts</param>
    ''' <returns>table containing the applied discounts details</returns>
    Public Function GetDiscountsAppliedForSOE(ByVal sessionId As String, Optional lnSeqNum As String = "") As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As String = String.Empty
        Try
            dbConnection.Open()

            If (lnSeqNum.isNotEmpty()) Then
                dbCommand.Parameters.Add(":lnSeqNum", OracleType.VarChar)
                dbCommand.Parameters(":lnSeqNum").Value = lnSeqNum
                dbSql = GetDiscountsAppliedForSOEQuery(sessionId, True)
            Else
                dbSql = GetDiscountsAppliedForSOEQuery(sessionId)
            End If
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function

    ''' <summary>
    ''' Retrieves all the discount information for all the lines in the order passed-in.
    ''' </summary>
    ''' <param name="delDocNum">the delDocNum  to use to query the discount details</param>
    ''' <returns>the data table of the discount details</returns>
    Public Function GetDiscountsAppliedForSOM(ByVal delDocNum As String) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT ROWNUM AS ROW_IDX, sld.DEL_DOC_LN# as LNSEQ_NUM, sld.DISC_CD,  ")
        dbSql.Append("d.DISC_CD || ' - ' || NVL(d.DES, '') as DISC_DES,  ")
        dbSql.Append("NVL(sld.DISC_AMT,0) as DISC_AMT, SEQ_NUM as DISC_SEQ_NUM, NVL(sld.DISC_PCT,0) as DISC_PCT, ")
        dbSql.Append("NVL(sld.DISC_LEVEL, DECODE(d.DISC_LEVEL, 'E', 'L', d.DISC_LEVEL)) as DISC_LEVEL, ")
        dbSql.Append("sl.QTY, sl.ITM_CD,  i.DES as ITM_DES, NVL(sl.UNIT_PRC,0) as ITM_RETAIL, sld.SEQ_NUM ")
        dbSql.Append("FROM DISC d, itm i, SO_LN sl, SO_LN2DISC sld ")
        dbSql.Append("WHERE sld.DEL_DOC_NUM = sl.DEL_DOC_NUM ")
        dbSql.Append("AND sld.del_doc_ln# = sl.del_doc_ln# ")
        dbSql.Append("AND sld.disc_cd = d.disc_cd  ")
        dbSql.Append("AND sl.itm_cd = i.itm_cd ")
        dbSql.Append("AND sld.DEL_DOC_NUM = :delDocNum ")
        dbSql.Append("ORDER BY sld.DEL_DOC_LN#")

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function
    ''' <summary>
    ''' Retrieves all the discount information for all the lines in the order passed-in.
    ''' </summary>
    ''' <param name="delDocNum">the delDocNum  to use to query the discount details</param>
    ''' <returns>the data table of the discount details</returns>
    Public Function GetDiscountsAppliedForRELN(ByVal relnum As String) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT ROWNUM AS ROW_IDX, rld.LINE as LNSEQ_NUM, rld.DISC_CD,  ")
        dbSql.Append("d.DISC_CD || ' - ' || NVL(d.DES, '') as DISC_DES,  ")
        dbSql.Append("NVL(rld.DISC_AMT,0) as DISC_AMT, SEQ_NUM as DISC_SEQ_NUM, NVL(rld.DISC_PCT,0) as DISC_PCT, ")
        dbSql.Append("NVL(rld.DISC_LEVEL, DECODE(d.DISC_LEVEL, 'E', 'L', d.DISC_LEVEL)) as DISC_LEVEL, ")
        dbSql.Append("rl.QTY, rl.ITM_CD,  i.DES as ITM_DES, NVL(rl.RET_PRC,0) as ITM_RETAIL, d.TP_CD as TPCD, NVL(d.AMT,0) as DISCAMT,rld.SEQ_NUM  ")
        dbSql.Append("FROM DISC d, itm i,RELATIONSHIP_LINES rl, RELATIONSHIP_LN2DISC rld ")
        dbSql.Append("WHERE rld.REL_NO = rl.REL_NO ")
        dbSql.Append("AND rld.LINE = rl.LINE ")
        dbSql.Append("AND rld.disc_cd = d.disc_cd  ")
        dbSql.Append("AND rl.itm_cd = i.itm_cd ")
        dbSql.Append("AND rld.REL_NO = :REL_NO ")
        dbSql.Append("ORDER BY rld.LINE")

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":REL_NO", OracleType.VarChar)
            dbCommand.Parameters(":REL_NO").Value = relnum

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function


    ''' <summary>
    ''' When the E1 System Paramter to 'Allow Multi Discount' entry is set to N, then when
    ''' discounts are applied in SOE, they don't end up as records in the SO_LN2DISC table,
    ''' however in SOM, these records have to be recreated from the data, so that the user
    ''' is able to remove the discount if desired.
    ''' </summary>
    ''' <param name="delDocNum">the delDocNum  to use to query the discount details</param>
    ''' <param name="discCd">the discount code to retrieve the details for</param>
    ''' <returns>the data table of the discount details</returns>
    Public Function GetDiscountsAppliedForSOM(ByVal delDocNum As String, ByVal discCd As String) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        'Daniela add seq_num
        dbSql.Append("SELECT ROWNUM AS ROW_IDX, sl.DEL_DOC_LN# as LNSEQ_NUM, i.DES as ITM_DES, ")
        dbSql.Append("sl.ITM_CD, sl.QTY, NVL(sl.UNIT_PRC,0) as ITM_RETAIL, ")
        dbSql.Append("999 as DISC_SEQ_NUM, d.DISC_CD, 'H' as DISC_LEVEL, NVL(sl.DISC_AMT,0) as DISC_AMT, ")
        dbSql.Append("d.DISC_CD || ' - ' || NVL(d.DES, '') as DISC_DES, ")
        dbSql.Append("DECODE(d.TP_CD, 'P', d.AMT, 0) as DISC_PCT, null SEQ_NUM ")
        dbSql.Append("FROM itm i, DISC d, SO_LN sl ")
        dbSql.Append("WHERE sl.DEL_DOC_NUM = :delDocNum ")
        dbSql.Append("AND sl.DISC_AMT > 0 ")
        dbSql.Append("AND sl.itm_cd = i.itm_cd ")
        dbSql.Append("AND d.DISC_CD = :discCd ")
        dbSql.Append("ORDER BY sl.DEL_DOC_LN#")

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters(":discCd").Value = discCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function

    ''' <summary>
    ''' When the E1 System Paramter to 'Allow Multi Discount' entry is set to N, then when
    ''' discounts are applied in SOE, they don't end up as records in the SO_LN2DISC table,
    ''' however in Srelationship, these records have to be recreated from the data, so that the user
    ''' is able to remove the discount if desired.
    ''' </summary>
    ''' <param name="delDocNum">the delDocNum  to use to query the discount details</param>
    ''' <param name="discCd">the discount code to retrieve the details for</param>
    ''' <returns>the data table of the discount details</returns>
    Public Function GetDiscountsAppliedForRELN(ByVal relnum As String, ByVal discCd As String) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()
        'Daniela add seq_num
        dbSql.Append("SELECT ROWNUM AS ROW_IDX, rl.LINE as LNSEQ_NUM, i.DES as ITM_DES, ")
        dbSql.Append("rl.ITM_CD, rl.QTY, NVL(rl.RET_PRC,0) as ITM_RETAIL, ")
        dbSql.Append("999 as DISC_SEQ_NUM, d.DISC_CD, 'H' as DISC_LEVEL, NVL(rl.DISC_AMT,0) as DISC_AMT, ")
        dbSql.Append("d.DISC_CD || ' - ' || NVL(d.DES, '') as DISC_DES, ")
        dbSql.Append("DECODE(d.TP_CD, 'P', d.AMT, 0) as DISC_PCT, null SEQ_NUM ")
        dbSql.Append("FROM itm i, DISC d, RELATIONSHIP_LINES rl ")
        dbSql.Append("WHERE rl.REL_NO = :REL_NO ")
        dbSql.Append("AND rl.DISC_AMT > 0 ")
        dbSql.Append("AND rl.itm_cd = i.itm_cd ")
        dbSql.Append("AND d.DISC_CD = :discCd ")
        dbSql.Append("ORDER BY rl.LINE ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":REL_NO", OracleType.VarChar)
            dbCommand.Parameters(":REL_NO").Value = relnum
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters(":discCd").Value = discCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function

    ''' <summary>
    ''' Retrieves the 'H' type records from the CRM.MULTI_DISC table for the sessionID
    ''' passed in.  Since there can only be ONE header per transaction, if records are
    ''' found, reads the first one and returns the discount code for it.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve the discounts for</param>
    ''' <returns>the header discount code found; or an empty string if no header discount exist</returns>
    Public Function GetHeaderDiscountCodeForSOE(ByVal sessionId As String) As String

        Dim hdrDiscCd As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim dbSql As String = "SELECT disc_cd FROM multi_disc WHERE disc_level = 'H' AND session_id = :sessionId"
        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If (dbReader.Read()) Then
                hdrDiscCd = dbReader("disc_cd")
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return hdrDiscCd
    End Function

    ''' <summary>
    ''' Fetches the value from the SO.DISC_CD for the document number passed in and returns it;
    ''' an empty string will be returned if no value is found.
    ''' </summary>
    ''' <param name="delDocNum">the document number to retrieve the discount for</param>
    ''' <returns>the header discount code found; or an empty string if no header discount exist</returns>
    Public Function GetSoHeaderDiscountCodeForSOM(ByVal delDocNum As String) As String

        Dim hdrDiscCd As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim dbSql As String = "SELECT disc_cd FROM so WHERE del_doc_num = :delDocNum"
        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If (dbReader.Read()) Then
                hdrDiscCd = If(IsDBNull(dbReader("disc_cd")), String.Empty, dbReader("disc_cd"))
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return hdrDiscCd
    End Function
    ''' <summary>
    ''' Fetches the value from the Relationship.DISC_CD for the document number passed in and returns it;
    ''' an empty string will be returned if no value is found.
    ''' </summary>
    ''' <param name="REL_NO">the Relationship number to retrieve the discount for</param>
    ''' <returns>the header discount code found; or an empty string if no header discount exist</returns>
    Public Function GetRlnHeaderDiscountCodeForRELN(ByVal REL_NO As String) As String

        Dim hdrDiscCd As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim dbSql As String = "SELECT DISC_CD FROM RELATIONSHIP WHERE REL_NO = :REL_NO"
        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql
            dbCommand.Parameters.Add(":REL_NO", OracleType.VarChar)
            dbCommand.Parameters(":REL_NO").Value = REL_NO
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If (dbReader.Read()) Then
                hdrDiscCd = If(IsDBNull(dbReader("DISC_CD")), String.Empty, dbReader("DISC_CD"))
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return hdrDiscCd
    End Function


    ''' <summary>
    ''' Recalculates ALL discounts found in the order represented by the session ID passed in.  This 
    ''' operation will be required when user changes prices, quantities, add new lines, etc.
    ''' </summary>
    ''' <param name="sessionId">the session id to retrieve discounts and discounted lines for</param>
    ''' <param name="pkpDelFlag">For a Pickup order, P; For Delivery D</param>
    Public Sub ReapplyDiscountsForSOE(ByVal sessionId As String, ByVal pkpDelFlag As String)

        Dim dtLnsDiscounted As DataTable
        Dim dtDiscounts As DataTable

        '=== Recalculates ALL discounts found in the SOE order represented by the sessionID passed in
        dtDiscounts = GetDiscountsToReapply(sessionId)
        If (dtDiscounts.Rows.Count > 0) Then
            dtLnsDiscounted = GetDiscountedLines(sessionId)
            'MM-9484
            dtLnsDiscounted = ReapplyDiscounts(dtLnsDiscounted, dtDiscounts, pkpDelFlag, False, True)

            '=== Saves to the TEMP_ITM and MULTI_DISC tables the updates made
            UpdateOrderLinesAndDiscounts(sessionId, dtLnsDiscounted)
        End If
    End Sub



    ''' <summary>
    ''' ReapplyDiscountsForSOM Reapplying Discount for the line items once delete the line items from the discount gridview in SOM
    ''' </summary>
    ''' <param name="dtOrderLines">Orderline datatable</param>
    ''' <param name="dtDiscApplied">Discount Applied lines datatable</param>
    ''' <param name="pkpDelFlag"></param>
    ''' <remarks></remarks>
    Public Function ReapplyDiscountsForSOM(ByVal dtOrderLines As DataTable,
                                           ByVal dtDiscApplied As DataTable,
                                           ByVal pkpDelFlag As String,
                                           ByVal isSOM As Boolean) As ReapplyDiscountResponseDtc

        Dim response As ReapplyDiscountResponseDtc = New ReapplyDiscountResponseDtc()
        Dim dtLns2Discounts As DataTable ' these are equivalent to the SO_LN2DISC and/or the entries in the Discount page
        Dim dtDiscounts As DataTable = dtDiscApplied
        Dim msg As String = ""

        '=== Reapplies the DISCOUNTS and Updates the DISC_APPLIED and ORDER_LINES table accordingly
        dtLns2Discounts = GetDiscountsToReapply(dtOrderLines, dtDiscApplied, isSOM)

        If (dtLns2Discounts.Rows.Count > 0) Then
            dtDiscounts = dtDiscApplied.Select("DISC_IS_DELETED = false").CopyToDataTable()
            dtLns2Discounts = ReapplyDiscounts(dtLns2Discounts, dtDiscounts, pkpDelFlag, isSOM)

            '=== Saves to the in-memory discounts and lines tables the updates made
            msg = UpdateOrderLinesAndDiscounts(dtOrderLines, dtDiscounts, dtLns2Discounts, isSOM)
        End If

        response.discountsApplied = dtDiscounts
        response.orderLines = dtOrderLines
        If msg.isNotEmpty Then
            response.message = response.message & " " & msg
        End If

        Return response

    End Function

    Public Function ReapplyDiscountsForSOM(ByVal dtOrderLines As DataTable,
                                          ByVal dtDiscApplied As DataTable,
                                          ByVal pkpDelFlag As String,
                                          ByVal isSOM As Boolean,
                                          ByVal lstDiscToRemove As List(Of RemoveDiscountDtc)) As ReapplyDiscountResponseDtc

        Dim response As ReapplyDiscountResponseDtc = New ReapplyDiscountResponseDtc()
        Dim dtLns2Discounts As DataTable ' these are equivalent to the SO_LN2DISC and/or the entries in the Discount page
        Dim dtDiscounts As DataTable = dtDiscApplied
        Dim msg As String = ""

        '=== Reapplies the DISCOUNTS and Updates the DISC_APPLIED and ORDER_LINES table accordingly
        dtLns2Discounts = GetDiscountsToReapply(dtOrderLines, dtDiscApplied, isSOM)

        If (dtLns2Discounts.Rows.Count > 0) Then
            dtDiscounts = dtDiscApplied.Select("DISC_IS_DELETED = false").CopyToDataTable()
            dtLns2Discounts = ReapplyDiscounts(dtLns2Discounts, dtDiscounts, pkpDelFlag, isSOM)

            '=== Saves to the in-memory discounts and lines tables the updates made
            msg = UpdateOrderLinesAndDiscounts(dtOrderLines, dtDiscounts, dtLns2Discounts, isSOM)
        End If


        response.discountsApplied = dtDiscounts
        response.orderLines = dtOrderLines
        If msg.isNotEmpty Then
            response.message = response.message & " " & msg
        End If

        Return response

    End Function


    ''' <summary>
    ''' Retrieves from the database, all the 'discountable' lines matching the sessionID passed-in. 
    ''' The resulting lines are the ones that the given discount can be applied to.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to get the lines added in this session</param>
    ''' <param name="discount">the discount details object</param>
    ''' <returns>the data table populated with the eligible order lines</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Public Function GetDiscountableLines(ByVal sessionId As String, ByVal discount As DiscountDtc) As DataTable

        Dim discountableLines As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Try
            dbConnection.Open()

            'the next call, adds the query string as well as the parameters to the command object
            PrepareCommandForDiscountableLines(dbCommand, discount, sessionId)

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountableLines)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountableLines.Tables(0)
    End Function

    ''' <summary>
    ''' Applies a HEADER discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Public Function AddHeaderDiscountForSOE(ByVal request As AddDiscountRequestDtc) As AddDiscountResponseDtc

        Dim response As AddDiscountResponseDtc = Nothing
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dsLnsToDiscount As New DataSet

        Try
            dbConnection.Open()

            'the next call, adds the query string as well as the parameters to the command object
            PrepareCommandForDiscountableLines(dbCommand, request.discount, request.sessionId, True)

            '==== Retrieves the lines that are eligible for the discount being applied
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dsLnsToDiscount)
            dbAdapter.Dispose()

            '==== Calls the E1 Discounts APIs and updates the CRM schema tables accordingly
            response = AddDiscountForSOE(dbConnection, request, dsLnsToDiscount.Tables(0))

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return response
    End Function

    ''' <summary>
    ''' Applies a LINE discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Public Function AddLineDiscountForSOE(ByVal request As AddDiscountRequestDtc) As AddDiscountResponseDtc

        Dim response As AddDiscountResponseDtc = Nothing
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim dsLnsToDiscount As New DataSet
        Dim lnSeqNums As String = StringUtils.GetDelimitedString(request.lnSeqNums)

        dbSql.Append("SELECT t.ROW_ID as DEL_DOC_LN#, t.ITM_CD, t.DES, NVL(t.QTY, 0) AS QTY, NVL(t.ORIG_PRC, 0) AS ORIG_PRC, ")
        dbSql.Append("       NVL(t.RET_PRC, 0) AS UNIT_PRC, NVL(i.REPL_CST, 0) AS REPL_CST, NVL(i.PU_DISC_PCNT, 0) AS PU_DISC_PCNT, ")
        dbSql.Append("       i.MNR_CD, t.PACKAGE_PARENT as PKG_SOURCE ")
        dbSql.Append("FROM TEMP_ITM t, ITM i ")
        dbSql.Append("WHERE t.ITM_CD = i.ITM_CD AND session_id = :sessionId ")
        dbSql.Append("AND ROW_ID IN ( ").Append(lnSeqNums).Append(" )")

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = request.sessionId
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)

            '==== Retrieves the lines details for the lines being discounted
            dbAdapter.Fill(dsLnsToDiscount)
            dbAdapter.Dispose()

            '==== Calls the E1 Discounts APIs and updates the CRM schema tables accordingly
            response = AddDiscountForSOE(dbConnection, request, dsLnsToDiscount.Tables(0))

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return response
    End Function

    ''' <summary>
    ''' Applies a HEADER discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER MANAGEMENT ********************</remarks>
    Public Function AddHeaderDiscountForSOM(ByVal request As AddDiscountRequestDtc, ByVal isSOM As Boolean) As AddDiscountResponseDtc
        Return AddDiscountForSOM(request, False, isSOM)
    End Function

    ''' <summary>
    ''' Applies a LINE discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER MANAGEMENT ********************</remarks>
    Public Function AddLineDiscountForSOM(ByVal request As AddDiscountRequestDtc, ByVal isSOM As Boolean) As AddDiscountResponseDtc
        Return AddDiscountForSOM(request, True, isSOM)
    End Function

    ''' <summary>
    ''' Removes the discounts represented by the details passed in.   Every object in the list
    ''' corresponds to a discount that will be removed.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve the discounts for</param>
    ''' <param name="pkpDelFlag">For a Pickup order, P; For Delivery D</param>
    ''' <param name="lstDiscToRemove">the list of discounts to remove</param>
    ''' <remarks>***** USE THIS METHOD DURING ORDER ENTRY (Line or Header Discount) *****</remarks>
    ''' MM-9484
    Public Sub RemoveDiscount(ByVal sessionId As String,
                              ByVal pkpDelFlag As String,
                              ByVal lstDiscToRemove As List(Of RemoveDiscountDtc),
                             Optional ByVal isSOE As Boolean = False)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dtLnsDiscounted As DataTable
        Dim dtDiscounts As DataTable

        Try
            dbConnection.Open()

            '=== updates the TEMP_ITM and MULTI_DISC tables to reflect the discounts removed
            dbCommand.Transaction = dbConnection.BeginTransaction
            For Each objRemoveDisc As RemoveDiscountDtc In lstDiscToRemove
                UpdateOrderLineDiscount(dbCommand, objRemoveDisc.discAmt, sessionId, objRemoveDisc.lnSeqNum, False)
            Next
            DeleteMultiDiscRow(dbCommand, lstDiscToRemove)
            dbCommand.Transaction.Commit()

            For Each item As RemoveDiscountDtc In lstDiscToRemove
                '=== Recalculates the remaining discounts 
                dtDiscounts = GetDiscountsToReapply(sessionId, item.seqNum)
                If (dtDiscounts.Rows.Count > 0) Then
                    dtLnsDiscounted = GetDiscountedLines(sessionId, item.seqNum)
                    dtLnsDiscounted = ReapplyDiscounts(dtLnsDiscounted, dtDiscounts, pkpDelFlag, False, isSOE)

                    '=== Saves to the TEMP_ITM and MULTI_DISC tables the updates made
                    UpdateOrderLineDiscounts(sessionId, dtLnsDiscounted)
                End If
            Next
            UpdateOrderLinesRetPrice(sessionId, dbCommand, dbConnection)
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Removes the discounts represented by the details passed in.   Every object in the list
    ''' corresponds to a discount that will be removed.
    ''' </summary>
    ''' <param name="dtOrderLines">the table that contains ALL order lines in the current order</param>
    ''' <param name="dtDiscApplied">the table that contains ALL discounts applied to the current order</param>
    ''' <param name="pkpDelFlag">For a Pickup order, P; For Delivery D</param>
    ''' <param name="lstDiscToRemove">the list of discounts to remove</param>
    ''' <returns>custom object with the details of the call to remove the discount</returns>
    ''' <remarks>***** USE THIS METHOD DURING ORDER MANAGEMENT (Line or Header Discount) *****</remarks>
    ''' 'MM-9870
    Public Function RemoveDiscount(ByVal dtOrderLines As DataTable,
                                   ByVal dtDiscApplied As DataTable,
                                   ByVal pkpDelFlag As String,
                                   ByVal lstDiscToRemove As List(Of RemoveDiscountDtc),
                                   ByVal isSOM As Boolean,
                                  Optional ByVal isCanceled As Boolean = False) As RemoveDiscountResponseDtc

        Dim response As RemoveDiscountResponseDtc = New RemoveDiscountResponseDtc()
        Dim dtLnsDiscounted As DataTable
        Dim dtDiscounts As DataTable
        Dim drDiscApplied As DataRow
        Dim resp As New LineDiscountUpdateResponseDtc
        Dim msg As New StringBuilder

        '=== updates the ORDER_LINES and DISCOUNT_APPLIED tables to reflect the discount removed
        '1) if discount was retrieved from DB (created in previous session), marks it for delete OR
        '2) if discount was created this session, deletes the row from the table
        For Each objRemoveDisc As RemoveDiscountDtc In lstDiscToRemove

            resp = UpdateOrderLineDiscount(dtOrderLines, objRemoveDisc.discAmt, objRemoveDisc.lnSeqNum, isSOM, False)
            msg.Append(resp.message)
            ' if find a non-success reponse, then update to that for output
            If response.status = DiscountUtils.DiscountStatus.Success Then
                response.status = resp.status
            End If
            ' if the line was updated, then continue with removing hte discounted detail;  if the line was not updated, then do not remove the discount record
            '   in reality, this should have been stopped at the UI because we should not be voiding discounts with SHU'd lines that are not voided or 
            '   having a SHU'd line discount trie
            If resp.isUpdated Then

                drDiscApplied = Nothing
                ' find the disc on the line (SO_LN2DISC or equivalent) to remove
                'MM-9870
                For Each dr As DataRow In dtDiscApplied.Rows
                    If isCanceled Then
                        If (objRemoveDisc.discCd = dr.Item("DISC_CD").ToString AndAlso
                        objRemoveDisc.seqNum = dr.Item("SEQ_NUM").ToString AndAlso
                        CDbl(objRemoveDisc.discAmt) = CDbl(dr.Item("DISC_AMT"))) Then

                            drDiscApplied = dr
                            Exit For
                        End If
                    Else
                        If (objRemoveDisc.discCd = dr.Item("DISC_CD").ToString AndAlso
                        objRemoveDisc.lnSeqNum = dr.Item("LNSEQ_NUM").ToString AndAlso
                        CDbl(objRemoveDisc.discAmt) = CDbl(dr.Item("DISC_AMT"))) Then

                            drDiscApplied = dr
                            Exit For
                        End If
                    End If

                Next

                If (Not IsNothing(drDiscApplied)) Then

                    If (drDiscApplied.Item("DISC_IS_EXISTING")) Then

                        drDiscApplied.Item("DISC_IS_DELETED") = True
                    Else
                        dtDiscApplied.Rows.Remove(drDiscApplied)
                    End If
                End If
            End If
            If msg.Length > 0 Then
                msg.Remove(msg.Length - 1, 1) ' remove last comma
                If response.message.Length > 0 Then
                    response.message = response.message & "; "
                End If
                response.message = response.message & Resources.POSMessages.MSG0033 & msg.ToString
            End If
        Next

        '=== Reapplies the DISCOUNTS and Updates the DISC_APPLIED and ORDER_LINES table accordingly
        'if any discount need to be removed from discount table if discountedline is removed from relationship and reapply discount which is there
        RemoveDiscountsDeleteFromRELN(dtOrderLines, dtDiscApplied, isSOM)
        Dim reAppResp As ReapplyDiscountResponseDtc = New ReapplyDiscountResponseDtc()
        reAppResp = ReapplyDiscountsForSOM(dtOrderLines, dtDiscApplied, pkpDelFlag, isSOM, lstDiscToRemove)
        response.orderLines = reAppResp.orderLines
        response.discountsApplied = reAppResp.discountsApplied

        Return response
    End Function

    ''' <summary>
    ''' Retrieves all minors attached to the passed-in discount code and returns the code
    ''' for the minor.
    ''' </summary>
    ''' <param name="discountCd">the discount to get minors for</param>
    ''' <returns>returns a list of the minor codes</returns>
    Public Function GetMinorsLst(ByVal discountCd As String) As ArrayList

        Dim minors As New ArrayList()

        Try
            Dim mnrTbl As DataTable = GetMinorsTbl(discountCd)
            If SystemUtils.dataTblIsNotEmpty(mnrTbl) Then

                For Each mnrRow In mnrTbl.Rows

                    minors.Add(mnrRow("mnr_cd").ToString())
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return minors
    End Function

    ''' <summary>
    ''' Retrieves all minors attached to the passed-in discount code.
    ''' </summary>
    ''' <param name="discountCd">the discount to get minors for</param>
    ''' <returns>returns a datatable of minors and indicators if conditional minor or discounted minor</returns>
    Public Function GetMinorsTbl(ByVal discountCd As String) As DataTable

        Dim minors As New DataTable
        Dim mnrDatSet As DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim oraDatAdr As OracleDataAdapter

        Try
            dbConnection.Open()

            dbCommand.CommandText = GetMinorsQuery(True)

            dbCommand.Parameters.Add(":discountCd", OracleType.VarChar)
            dbCommand.Parameters(":discountCd").Value = discountCd

            mnrDatSet = New DataSet
            oraDatAdr = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oraDatAdr.Fill(mnrDatSet)

            If SystemUtils.dataSetHasRows(mnrDatSet) Then
                minors = mnrDatSet.Tables(0)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            oraDatAdr.Dispose()
            CloseResources(dbConnection, dbCommand)
        End Try

        Return minors
    End Function

    ''' <summary>
    ''' Inserts a record in the CRM.MULTI_DISC table, after a split of lines was triggered due
    ''' to the reservation of inventory
    ''' </summary>
    ''' <param name="splitRequest">custom object with the details to create the record</param>
    Public Sub InsertMultiDiscRow(ByVal splitRequest As SplitLineRequestDtc)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("INSERT INTO multi_disc (session_id, line_id, disc_cd, disc_desc, disc_amt, disc_pct, disc_level ) ")
        sql.Append("SELECT :newDelDocNum, :newlnNum, disc_cd, disc_desc, disc_amt, disc_pct, disc_level ")
        sql.Append("FROM multi_disc WHERE session_id = :origDelDocNum AND line_id = :origlnNum")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString

            dbCommand.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":origlnNum", OracleType.Number)
            dbCommand.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":newlnNum", OracleType.Number)

            dbCommand.Parameters(":origDelDocNum").Value = splitRequest.origDelDocNum
            dbCommand.Parameters(":origlnNum").Value = CInt(splitRequest.origLnNum)
            dbCommand.Parameters(":newDelDocNum").Value = splitRequest.newDelDocNum
            dbCommand.Parameters(":newlnNum").Value = CInt(splitRequest.newLnNum)
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

    End Sub

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Reapplies the discounts that already exist on the order.  This action gets triggered by 
    ''' multiple events, such as price change, quantity change, removal of discounts, 
    ''' voiding of lines, etc
    ''' </summary>
    ''' <param name="dtLnsDiscounted">a table that contains LINE and DISCOUNT details combined, 
    '''                               one row for every discount that exists in the order</param>
    ''' <param name="dtDiscApplied">the discounts found for an order to be ordered by Amount then Percent</param>
    ''' <param name="pdFlag">if order being processed is Pickup then P; if delivery, D</param>
    ''' <returns>dtLnsDiscounted table passed in, with the amounts updated accordingly</returns>
    ''' <remarks>**** TO BE USED FOR SOE AND SOM, make sure to provide required columns on each table ****</remarks>
    ''' MM-9484
    Private Function ReapplyDiscounts(ByVal dtLnsDiscounted As DataTable,
                                      ByVal dtDiscApplied As DataTable,
                                      ByVal pdFlag As String,
                                      Optional ByVal isSOM As Boolean = False,
                                      Optional ByVal isSOE As Boolean = False) As DataTable

        Dim dbConnection As OracleConnection = GetConnection()
        Dim calcResponse As CalculateDiscountsResponse
        Dim dtLnsFiltered As DataTable
        Dim dtLnsRequest As DataTable
        Dim dtLnsResult As DataTable
        Dim dtDiscounts As DataTable
        Dim drLnUpdated As DataRow()
        Dim cmd As OracleCommand = GetCommand("", dbConnection)

        Dim lstDiscCdApplied As New ArrayList()
        Dim sbApiResponseMsg As StringBuilder = New StringBuilder()
        Dim strDiscCdAndLevel As String = String.Empty
        Dim strWhereClause As String = String.Empty
        Dim discAmt As Double
        Dim dtLnsDiscountedFiltered As DataTable
        Dim discSeqNum As Integer
        '==== Reapplies the discounts again, first the Amounts, then the percent types
        Try
            dbConnection.Open()

            If dtDiscApplied.Columns.Contains("APPLIED_BY") Then
                dtDiscounts = (dtDiscApplied.Select("DISC_CD IS NOT NULL", "APPLIED_BY")).CopyToDataTable
            Else
                'add a new column for sorting to get around the disc_pct = 0 instead of null for 'amt' type of discounts
                ' sort by amt first and then percent;  it gives a lower percent amt by applying amt disc first so is better for store
                dtDiscApplied.Columns.Add("SORT_COL", Type.GetType("System.String"), "IIF(DISC_PCT=0, 'A', 'P')")
                dtDiscounts = (dtDiscApplied.Select("DISC_IS_DELETED = false", "SORT_COL")).CopyToDataTable
                dtDiscApplied.Columns.Remove("SORT_COL")
            End If

            For Each drDisc As DataRow In dtDiscounts.Rows

                'MM-9484
                If isSOE Then
                    strDiscCdAndLevel = drDisc("DISC_CD") & drDisc("DISC_LEVEL")
                    strWhereClause = ("DISC_CD = '" & drDisc("DISC_CD") & "' AND DISC_LEVEL = '" & drDisc("DISC_LEVEL") & "'")
                Else
                    strDiscCdAndLevel = drDisc("DISC_CD") & drDisc("DISC_LEVEL") & drDisc("DISC_SEQ_NUM").ToString
                    strWhereClause = ("DISC_CD = '" & drDisc("DISC_CD") & "' AND DISC_LEVEL = '" & drDisc("DISC_LEVEL") & "' AND DISC_SEQ_NUM= '" & drDisc("DISC_SEQ_NUM") & "' ")
                    discSeqNum = drDisc("DISC_SEQ_NUM")
                End If

                If (Not lstDiscCdApplied.Contains(strDiscCdAndLevel)) Then     'to process a discount ONCE for all related lines

                    If (dtLnsDiscounted.Select(strWhereClause).Length > 0) Then
                        dtLnsFiltered = dtLnsDiscounted.Select(strWhereClause).CopyToDataTable()
                        dtLnsRequest = GetTableToCalculateDiscounts(dtLnsFiltered, pdFlag, drDisc("DISC_CD"), isSOM)
                        calcResponse = CalculateDiscounts(dtLnsRequest, cmd)
                        dtLnsResult = calcResponse.dtLnsResult

                        sbApiResponseMsg.Append(calcResponse.statusMsg).Append(". ")

                        'using the results from applying the discount, updates the lines, so that the next
                        'discount is calculated using the correct retail
                        For Each drLnRes As DataRow In dtLnsResult.Rows
                            discAmt = If(IsNumeric(drLnRes("DISCAMT")), CDbl(drLnRes("DISCAMT")), 0.0)

                            '-- update the OrderLines row accordingly
                            For Each drLnDis As DataRow In dtLnsDiscounted.Rows

                                If (drLnDis("DEL_DOC_LN#") = drLnRes("DELLN")) Then
                                    'updates ALL matching lines, there could be many due to multiple discounts on a line
                                    drLnDis("UNIT_PRC") = drLnDis("UNIT_PRC") - discAmt
                                    drLnDis("TOTAL_DISC_AMT") = drLnDis("TOTAL_DISC_AMT") + discAmt

                                    'as for discount amount, only the one being processed should be updated
                                    'MM-9484

                                    If isSOE Then
                                        If (drLnDis("DISC_CD") = drDisc("DISC_CD")) Then drLnDis("DISC_AMT") = discAmt
                                    Else
                                        If (drLnDis("DISC_CD") = drDisc("DISC_CD") AndAlso drLnDis("DISC_SEQ_NUM") = discSeqNum) Then drLnDis("DISC_AMT") = discAmt
                                    End If
                                End If
                            Next
                        Next
                    End If

                    'adds it to this list, to avoid double processing the same discount
                    lstDiscCdApplied.Add(strDiscCdAndLevel)
                End If
            Next
            '==========================================================================================
            '  At this point the dtLnsDiscounted table, has been updated with ALL discounts reapplied
            '  the caller of this method MUST process the results accordingly
            '==========================================================================================
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection)
        End Try

        Return dtLnsDiscounted
    End Function

    ''' <summary>
    ''' Retrieves the discounts that exist in the order represented by the sessionID passed in
    ''' </summary>
    ''' <param name="sessionId">the sessionId to retrieve the discounts for</param>
    ''' <returns>a datatable that includes the discount details</returns>
    Private Function GetDiscountsToReapply(ByVal sessionId As String) As DataTable

        Dim dtDiscounts As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT md.DISC_CD, md.DISC_LEVEL, d.APPLY_PACKAGE, ")
        dbSql.Append("       DECODE(NVL(md.DISC_PCT,0), 0, 'A', 'P') AS APPLIED_BY ")
        dbSql.Append("FROM multi_disc md, disc d ")
        dbSql.Append("WHERE md.DISC_CD = d.DISC_CD AND session_id = :sessionId ")
        dbSql.Append("ORDER BY APPLIED_BY, DISC_CD ")
        Try
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dtDiscounts)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtDiscounts
    End Function

    ''' <summary>
    ''' Retrieves from the in-memory discounts and order lines table for a retrieved order, all non-voided 
    ''' lines and it's discount info as long it is found to be discounted.
    ''' Applies the needed rules for SOM and relationship, such as discount expiration date, as well as the lines being SHU'd
    ''' among other things.
    ''' </summary>
    ''' <param name="dtOrderLines">the table that contains ALL order lines in the current order</param>
    ''' <param name="dtDiscApplied">the table that contains ALL discounts applied to the current order</param>
    ''' <returns>a datatable containing all discounted lines for which discount should be recalculated</returns>
    ''' <remarks>IMPORTANT: Use ONLY to reapply discounts IN ORDER MGMT, the UNIT_PRICE IS OVERRIDEN</remarks>
    Private Function GetDiscountsToReapply(ByVal dtOrderLines As DataTable,
                                           ByVal dtDiscApplied As DataTable,
                                           ByVal isSOM As Boolean) As DataTable

        If (dtDiscApplied.Select("DISC_IS_DELETED = false").Length() <= 0) Then
            'since all discounts are flagged as voided no need to continue
            Return (New DataTable())
        End If

        Dim dtLnsToReapplyDiscountsFor As DataTable
        Dim dtLnsToVerifyDiscountsFor As DataTable
        Dim dtNonVoidDiscApplied As DataTable
        Dim drOrderLn As DataRow
        Dim strWhereClause As String
        Dim strDiscCdAndLevel As String
        Dim lstDiscLevelVerified As New ArrayList()
        Dim discDtc As DiscountDtc
        Dim includeOnlyNonSHUdLines As Boolean
        Dim includeAllLines As Boolean

        '====== CREATES A COMPOSITE TABLE WITH ALL NON-VOIDED DISCOUNTED LINES ======
        'Includes line and discount details, cloned after discount since most of its columns
        'are needed during the reapply process.  Although ALL discounted lines are added at this point,
        'some lines may be excluded after applying the bus. rules
        dtLnsToVerifyDiscountsFor = (dtDiscApplied.Select("DISC_IS_DELETED = false", "DISC_CD ASC")).CopyToDataTable
        dtLnsToVerifyDiscountsFor.Columns.Add("DEL_DOC_LN#", Type.GetType("System.Int32"))
        dtLnsToVerifyDiscountsFor.Columns.Add("DES")
        dtLnsToVerifyDiscountsFor.Columns.Add("ORIG_PRC", Type.GetType("System.Double"))
        dtLnsToVerifyDiscountsFor.Columns.Add("UNIT_PRC", Type.GetType("System.Double"))
        dtLnsToVerifyDiscountsFor.Columns.Add("TOTAL_DISC_AMT", Type.GetType("System.Double"))
        dtLnsToVerifyDiscountsFor.Columns.Add("REPL_CST", Type.GetType("System.Double"))
        dtLnsToVerifyDiscountsFor.Columns.Add("PU_DISC_PCNT", Type.GetType("System.Double"))
        dtLnsToVerifyDiscountsFor.Columns.Add("MNR_CD")
        dtLnsToVerifyDiscountsFor.Columns.Add("VOID_FLAG")
        dtLnsToVerifyDiscountsFor.Columns.Add("PKG_SOURCE")
        dtLnsToVerifyDiscountsFor.Columns.Add("APPLIED_BY")
        dtLnsToVerifyDiscountsFor.Columns.Add("SHU")

        Dim drfiltered() As DataRow
        'completes populating the table, with the line details
        For Each dr As DataRow In dtLnsToVerifyDiscountsFor.Rows
            If isSOM Then
                strWhereClause = ("[DEL_DOC_LN#] = " & dr("LNSEQ_NUM"))
            Else
                strWhereClause = ("[LINE] = " & dr("LNSEQ_NUM"))
            End If

            drfiltered = dtOrderLines.Select(strWhereClause)

            If drfiltered.Length > 0 Then
                drOrderLn = drfiltered(0)

                If isSOM Then
                    dr("DEL_DOC_LN#") = drOrderLn.Item("DEL_DOC_LN#")
                Else
                    dr("DEL_DOC_LN#") = drOrderLn.Item("LINE")
                End If
                dr("QTY") = drOrderLn.Item("QTY")
                dr("DES") = drOrderLn.Item("DES")
                dr("ORIG_PRC") = drOrderLn.Item("ORIG_PRC")
                dr("UNIT_PRC") = drOrderLn.Item("MANUAL_PRC")
                dr("TOTAL_DISC_AMT") = 0
                dr("REPL_CST") = drOrderLn.Item("REPL_CST")
                dr("PU_DISC_PCNT") = drOrderLn.Item("PU_DISC_PCNT")
                dr("MNR_CD") = drOrderLn.Item("MNR_CD")
                If isSOM Then
                    dr("VOID_FLAG") = drOrderLn.Item("VOID_FLAG")
                Else
                    dr("VOID_FLAG") = "N"
                End If
                If isSOM Then
                    dr("PKG_SOURCE") = If(IsDBNull(drOrderLn.Item("PACKAGE_PARENT")), _
                                               drOrderLn.Item("PKG_SOURCE"), drOrderLn.Item("PACKAGE_PARENT"))
                Else
                    dr("PKG_SOURCE") = If(IsDBNull(drOrderLn.Item("PACKAGE_PARENT")), _
                                               String.Empty, drOrderLn.Item("PACKAGE_PARENT"))
                End If

                If isSOM Then
                    dr("APPLIED_BY") = If(dr("DISC_PCT").ToString = 0, AppConstants.Discount.TP_AMT, AppConstants.Discount.TP_PCT)
                    dr("SHU") = drOrderLn.Item("SHU")
                Else
                    dr("APPLIED_BY") = AppConstants.Discount.TP_AMT
                    dr("SHU") = "N"
                End If
            End If
        Next

        '====== ADDS the lines that should have their discount recalculated, based on bus. rules ======
        dtLnsToReapplyDiscountsFor = dtLnsToVerifyDiscountsFor.Clone()   'creates empty shell, fills below based on rules
        ' ---
        ' Rules being applied
        'NOTE:For Relationship SHU'd rules is not applicable.
        ' 1) When discount is LINE type, ONLY NON-SHU'd lines associated to the discount are sent
        ' 2) When discount is HEADER type,
        '    2.1) And Discount is Conditional, ALL lines associated to the discount are sent, regardless of SHU status
        '    2.2) When Discount is NON-Conditional 
        '         2.2.1) If it is a Percent discount, ONLY NON-SHU'd lines associated to the discount are sent
        '         2.2.2) If it is an Amount discount, lines associated to the discounts are sent
        '                2.2.2.1) ALL, if none of them are SHU'd  or  NONE, if at least one is SHU'd
        ' ---
        dtNonVoidDiscApplied = dtDiscApplied.Select("DISC_IS_DELETED = false").CopyToDataTable  'to iterate by discount/level
        For Each dr As DataRow In dtNonVoidDiscApplied.Rows

            strDiscCdAndLevel = dr("DISC_CD") + dr("DISC_LEVEL")
            If (Not lstDiscLevelVerified.Contains(strDiscCdAndLevel)) Then   'to process a discount ONCE for all related lines

                'adds it to this list, to avoid double processing the same discount
                lstDiscLevelVerified.Add(strDiscCdAndLevel)
                includeAllLines = False
                includeOnlyNonSHUdLines = False

                strWhereClause = ("DISC_CD = '" & dr("DISC_CD") & "' AND DISC_LEVEL = '" & dr("DISC_LEVEL") & "'")
                Dim discLnsForDiscLevel As DataRow() = dtLnsToVerifyDiscountsFor.Select(strWhereClause)

                If (AppConstants.Discount.LEVEL_LINE = dr("DISC_LEVEL")) Then
                    includeOnlyNonSHUdLines = True

                ElseIf (AppConstants.Discount.LEVEL_HEADER = dr("DISC_LEVEL")) Then

                    discDtc = GetDiscount(dr("DISC_CD"))    'has to fetch the discount to see if conditional
                    If (Not IsNothing(discDtc) AndAlso discDtc.isCondDisc) Then
                        includeAllLines = True
                    ElseIf (CDbl(dr("DISC_PCT")) > 0) Then  'discount applied by percent
                        includeOnlyNonSHUdLines = True
                    Else
                        includeAllLines = (Not ContainsSHUdLines(discLnsForDiscLevel))
                    End If
                End If

                'adds the discounted lines based on the rules applied above
                For Each drDiscLn As DataRow In discLnsForDiscLevel
                    If (includeAllLines OrElse (includeOnlyNonSHUdLines AndAlso _
                                                Not IsDBNull(drDiscLn("SHU")) AndAlso ("N" = drDiscLn("SHU")))) Then
                        dtLnsToReapplyDiscountsFor.ImportRow(drDiscLn)
                    End If
                Next
            End If
        Next

        Return (dtLnsToReapplyDiscountsFor)

    End Function

    ''' <summary>
    ''' Finds out if the list of datarows passed in, includes a line that has been flagged
    ''' as SHU'd.   The table passed in is expected to have a column named "SHUD" 
    ''' </summary>
    ''' <param name="discLns">list of rows to be inpected</param>
    ''' <returns>TRUE if at least one row is found to be SHU'd; FALSE otherwise</returns>
    Private Function ContainsSHUdLines(ByVal discLns As DataRow()) As Boolean
        Dim foundSHUdLines As Boolean = False
        For Each drDiscLn As DataRow In discLns
            If Not IsDBNull(drDiscLn("SHU")) AndAlso ("Y" = drDiscLn("SHU")) Then
                foundSHUdLines = True
                Exit For
            End If
        Next
        Return foundSHUdLines
    End Function

    ''' <summary>
    ''' Retrieves from the TEMP_ITM table, the lines that match the sessionID passed in,
    ''' and that are found to be discounted (records found in the MULTI_DISC table)
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to get the lines added in this session</param>
    ''' <returns>a datatable containing all discounted lines, includes discount details for each</returns>
    ''' <remarks>IMPORTANT: Use ONLY to reapply discounts, the UNIT_PRICE IS OVERRIDEN</remarks>
    Private Function GetDiscountedLines(ByVal sessionId As String) As DataTable

        Dim dtLnsDiscounted As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        '==== Since all the discounts are being applied again, restores original price (on unit_prc) to start over
        dbSql.Append("SELECT t.ROW_ID as DEL_DOC_LN#, t.ITM_CD, t.DES, NVL(t.QTY, 0) AS QTY, NVL(t.ORIG_PRC, 0) AS ORIG_PRC, ")
        dbSql.Append("       NVL(t.MANUAL_PRC, 0) AS UNIT_PRC, 0 AS TOTAL_DISC_AMT, NVL(i.REPL_CST, 0) AS REPL_CST, ")
        dbSql.Append("       NVL(i.PU_DISC_PCNT, 0) AS PU_DISC_PCNT, i.MNR_CD, t.PACKAGE_PARENT as PKG_SOURCE, ")
        dbSql.Append("       md.DISC_CD, md.DISC_AMT, md.DISC_PCT, md.DISC_DESC, md.DISC_LEVEL, ")
        dbSql.Append("       DECODE(NVL(md.DISC_PCT,0), 0, 'A', 'P') AS APPLIED_BY ")
        dbSql.Append("FROM TEMP_ITM t, MULTI_DISC md, ITM i ")
        dbSql.Append("WHERE t.ITM_CD = i.ITM_CD AND t.SESSION_ID = md.SESSION_ID AND ")
        dbSql.Append("      md.LINE_ID = t.ROW_ID AND md.session_id = :sessionId ")
        dbSql.Append("ORDER BY APPLIED_BY, DISC_CD ")

        Try
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dtLnsDiscounted)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtLnsDiscounted
    End Function

    ''' <summary>
    ''' Calculates the Discounts for the lines passed in and updates the TEMP_ITM and MULTI_DISC
    ''' tables accordingly
    ''' </summary>
    ''' <param name="dbConnection">an OPEN connection to use for this operation</param>
    ''' <param name="request">custom object with details to apply the discount</param>
    ''' <param name="dtLnsToDiscount">the lines that the discount will be applied to</param>
    ''' <returns>custom object with the results of the calculations</returns>
    ''' <remarks>******* USED TO ADD HEADER AND LINE DISCOUNTS DURING ORDER ENTRY *******</remarks>
    Private Function AddDiscountForSOE(ByRef dbConnection As OracleConnection,
                                       ByVal request As AddDiscountRequestDtc,
                                       ByVal dtLnsToDiscount As DataTable) As AddDiscountResponseDtc

        Dim addResponse As AddDiscountResponseDtc = New AddDiscountResponseDtc()
        If (dtLnsToDiscount.Rows.Count > 0) Then

            Dim dtLnsRequest As DataTable = GetTableToCalculateDiscounts(dtLnsToDiscount, request.pkpDel, request.discount.cd)
            Dim calcResponse As CalculateDiscountsResponse = CalculateDiscounts(dbConnection, dtLnsRequest)
            Dim dtLnsResult As DataTable = calcResponse.dtLnsResult

            addResponse.numLnsDiscounted = dtLnsResult.Rows.Count
            addResponse.message = calcResponse.statusMsg
            addResponse.status = calcResponse.status

            '==== UPDATES the corresponding CRM schema tables at this point
            If (DiscountUtils.DiscountStatus.HasError <> calcResponse.status AndAlso dtLnsResult.Rows.Count > 0) Then
                Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
                Try
                    'For data integrity a single transaction is used at this point
                    dbCommand.Transaction = dbConnection.BeginTransaction

                    Dim seqNum As Integer = GetDiscountSeqNum(dbCommand, request.sessionId)
                    '=== updates the TEMP_ITM and MULTI_DISC tables with the discount created
                    For Each dr As DataRow In dtLnsResult.Rows
                        UpdateOrderLineDiscount(dbCommand, dr.Item("DISCAMT"), request.sessionId, dr.Item("DELLN"))
                        InsertMultiDiscRow(dbCommand, request.discount, dr.Item("DISCAMT"), request.sessionId, dr.Item("DELLN"), seqNum)
                    Next
                    dbCommand.Transaction.Commit()
                Catch ex As Exception
                    dbCommand.Transaction.Rollback()
                    Throw ex
                Finally
                    CloseResources(dbCommand)
                End Try
            End If
        End If

        Return addResponse
    End Function
    'MM-9484
    ''' <summary>
    ''' GetDiscountSeqNumForSOM to get the Sequence Number for the discount lines added in the datatable
    ''' </summary>
    ''' <param name="dt">datatable with discount lines</param>
    ''' <returns>Return the Sequence Number</returns>
    ''' <remarks></remarks>
    Public Function GetDiscountSeqNumForSOM(ByVal dt As DataTable)
        Dim seqNum As Integer
        If (dt.Rows.Count() = 0) Then
            seqNum = 1
        Else
            seqNum = (From u In dt.AsEnumerable()
                      Select u.Field(Of Integer)("SEQ_NUM")).Max()
            seqNum = seqNum + 1
        End If
        Return seqNum
    End Function
    ''' <summary>
    ''' Applies a discount to the order details indicated in the request object passed in
    ''' </summary>
    ''' <param name="request">custom object with the details to add the discount</param>
    ''' <param name="isLineDiscount">Header/Line</param>
    ''' <returns>custom object with the results of adding the discount</returns>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER MANAGEMENT ********************</remarks>
    Public Function AddDiscountForSOM(ByVal request As AddDiscountRequestDtc,
                                      ByVal isLineDiscount As Boolean,
                                      ByVal isSOM As Boolean) As AddDiscountResponseDtc

        Dim response As AddDiscountResponseDtc = New AddDiscountResponseDtc()
        Dim lnSeqNums As String = If(isLineDiscount, StringUtils.GetDelimitedString(request.lnSeqNums), String.Empty)
        Dim delDocNum As String = ""
        Dim minors As New DataTable
        'if the discount is setup by minor, then need to get all the minors for this discount
        If (request.discount.isByMinor) Then minors = GetMinorsTbl(request.discount.cd)

        '---- first gathers those lines that the discount can be applied to
        Dim dtLns As DataTable = request.orderLines
        Dim dtLnsToDiscount As DataTable = dtLns.Clone()

        For Each dr As DataRow In dtLns.Rows
            '-line has to be discountable, as well as if ByLine the LnSeq has to match
            'MM-9484
            If dr.RowState <> DataRowState.Deleted Then
                Dim lnseqNUM As Boolean
                If isSOM Then
                    lnseqNUM = lnSeqNums.Contains(dr.Item("DEL_DOC_LN#"))
                    delDocNum = dr.Item("del_doc_num")
                Else
                    lnseqNUM = lnSeqNums.Contains(dr.Item("LINE"))
                    delDocNum = dr.Item("REL_NO")
                End If

                If (DiscountUtils.IsDiscountableLine(dr, request.discount, minors, isSOM) AndAlso
                    (Not isLineDiscount OrElse
                     (isLineDiscount AndAlso lnseqNUM))) Then

                    dtLnsToDiscount.ImportRow(dr)
                End If
            End If
        Next
        'MM-9484
        Dim seqNum As Integer = GetDiscountSeqNumForSOM(request.discApplied)
        '---- When discountable lines are found, calls E1 API to calculate the discount and
        '---- updates the ORDERLINES and DISCAPPLIED tables passed in the request object
        If (dtLnsToDiscount.Rows.Count > 0) Then

            Dim dbConnection As OracleConnection = GetConnection()
            Dim calcResponse As New CalculateDiscountsResponse
            Dim dtLnsRequest As DataTable
            Dim dtLnsResult As DataTable
            Dim drDisc As DataRow
            Dim drLnUpdated As DataRow
            Dim discAmt As Double

            Try
                dbConnection.Open()
                dtLnsRequest = GetTableToCalculateDiscounts(dtLnsToDiscount, request.pkpDel, request.discount.cd, isSOM)
                calcResponse = CalculateDiscounts(dbConnection, dtLnsRequest)
                dtLnsResult = calcResponse.dtLnsResult

                response.status = calcResponse.status
                response.message = calcResponse.statusMsg
                response.numLnsDiscounted = dtLnsResult.Rows.Count

                CloseResources(dbConnection)
            Catch ex As Exception
                CloseResources(dbConnection)
                Throw ex
            End Try

            '--- when NO errors are encountered during the calculation, updates the tables passed in
            If (DiscountUtils.DiscountStatus.HasError <> calcResponse.status AndAlso dtLnsResult.Rows.Count > 0) Then

                Dim resp As New LineDiscountUpdateResponseDtc
                Dim msg As New StringBuilder
                For Each dr As DataRow In dtLnsResult.Rows

                    discAmt = If(IsNumeric(dr.Item("DISCAMT")), CDbl(dr.Item("DISCAMT")), 0.0)

                    '-- update the OrderLines row accordingly
                    resp = UpdateOrderLineDiscount(request.orderLines, discAmt, dr.Item("DELLN"), isSOM)
                    drLnUpdated = resp.orderLine
                    msg.Append(resp.message)
                    ' if find a non-success reponse, then update to that for output
                    If response.status = DiscountUtils.DiscountStatus.Success Then
                        response.status = resp.status
                    End If

                    '-- update the Discounts Applied accordingly
                    'NOTE: value 999 passed to the call below, gets set properly when discount is saved
                    ' if we didn't update the line, then put zero discount amt on the applied discount entry, still need the record
                    'MM-9484
                    ' Dim seqNum As Integer = CalDiscountSeqNumForSOM(request.discApplied)
                    DiscountUtils.AddDiscountRow(request.discApplied,
                                                 request.discount.cd,
                                                 request.discount.desc,
                                                 request.discount.level,
                                                 If(resp.isUpdated, discAmt, 0),
                                                 seqNum,
                                                 If(AppConstants.Discount.TP_PCT = request.discount.tpCd, request.discount.amt, 0),
                                                 drLnUpdated.Item("ITM_CD"),
                                                 If(IsDBNull(drLnUpdated.Item("DES")), request.discount.cd, drLnUpdated.Item("DES")),
                                                 drLnUpdated("QTY"),
                                                 If(isSOM, drLnUpdated.Item("UNIT_PRC"), drLnUpdated.Item("RET_PRC")),
                                                 If(isSOM, drLnUpdated.Item("DEL_DOC_LN#"), drLnUpdated.Item("LINE")),
                                                 seqNum,
                                                 String.Empty,
                                                 False,
                                                 False)
                Next
                If msg.Length > 0 Then
                    msg.Remove(msg.Length - 1, 1) ' remove last comma
                    If response.message.Length > 0 Then
                        response.message = response.message & "; "
                    End If
                    response.message = response.message & Resources.POSMessages.MSG0033 & msg.ToString
                End If
            End If
        End If

        response.discountsApplied = request.discApplied
        response.orderLines = request.orderLines

        Return response
    End Function

    ''' <summary>
    ''' Updates the dtOrderLines row that matches the sequence of the drDiscline passed in, to
    ''' reflect the discount amount that has been applied or removed from the given line.
    ''' </summary>
    ''' <param name="dtOrderLines">Table with all order lines in the order</param>
    ''' <param name="discAmt">the discount amount that was added/removed in the line</param>
    ''' <param name="lnSeqNum">the sequence number of the line that will be updated</param>
    ''' <param name="addDiscount">TRUE if discount was added; FALSE if discount was removed</param>
    ''' <returns>the datarow representing the order line that was updated this time</returns>
    Private Function UpdateOrderLineDiscount(ByRef dtOrderLines As DataTable,
                                             ByVal discAmt As Double,
                                             ByVal lnSeqNum As String,
                                             ByVal isSOM As Boolean,
                                             Optional ByVal addDiscount As Boolean = True
                                              ) As LineDiscountUpdateResponseDtc

        Dim oldDiscAmt As Double
        Dim oldUnitPrc As Double
        Dim RetPrc As Double
        Dim msg As New StringBuilder
        Dim resp As New LineDiscountUpdateResponseDtc
        Dim colRef As String
        Dim line As Int32 = 0

        If isSOM Then
            colRef = "DEL_DOC_LN#"
        Else
            colRef = "LINE"
        End If

        For Each dr As DataRow In dtOrderLines.Rows
            If dr.RowState <> DataRowState.Deleted Then
                line = dr.Item(colRef)
                If (lnSeqNum = line) Then
                    If isSOM Then
                        oldDiscAmt = If(IsDBNull(dr.Item("DISC_AMT")), 0, dr.Item("DISC_AMT"))
                        oldUnitPrc = If(IsDBNull(dr.Item("UNIT_PRC")), 0, dr.Item("UNIT_PRC"))
                        If (dr.Item("UNIT_PRC") < dr.Item("ITM_RET_PRC")) Then
                            RetPrc = dr.Item("ITM_RET_PRC")
                        Else
                            RetPrc = dr.Item("UNIT_PRC")
                        End If
                    Else
                        oldDiscAmt = If(IsDBNull(dr.Item("DISC_AMT")), 0, dr.Item("DISC_AMT"))
                        oldUnitPrc = If(IsDBNull(dr.Item("RET_PRC")), 0, dr.Item("RET_PRC"))
                        If (dr.Item("RET_PRC") < dr.Item("ITM_RET_PRC")) Then
                            RetPrc = dr.Item("ITM_RET_PRC")
                        Else
                            RetPrc = dr.Item("RET_PRC")
                        End If
                    End If

                    If isSOM Then
                            If dr("SHU") = "N" OrElse discAmt = 0 Then

                                dr.Item("DISC_AMT") = If(addDiscount, oldDiscAmt + discAmt, oldDiscAmt - discAmt)
                                If (addDiscount) Then
                                    If (oldUnitPrc - discAmt) < 0.0 Then
                                        dr.Item("UNIT_PRC") = 0.0
                                    Else
                                        dr.Item("UNIT_PRC") = oldUnitPrc - discAmt
                                    End If
                                Else
                                If (discAmt >= RetPrc) Then
                                    dr.Item("UNIT_PRC") = RetPrc
                                Else
                                    dr.Item("UNIT_PRC") = oldUnitPrc + discAmt
                                    End If
                                End If
                            resp.isUpdated = True

                            'if line voiding, mark as update so remove so_ln2disc (applied disc), but don't bother updating the line
                        ElseIf dr("VOID_FLAG") = "Y" Then
                            resp.isUpdated = True

                        Else ' If discAmt <> 0 Then

                            resp.isUpdated = False
                            msg.Append(" " & lnSeqNum & "/" & dr("ITM_CD") & ",")
                            resp.status = DiscountUtils.DiscountStatus.Warning
                        End If
                    Else

                        If Not discAmt = 0 Then

                                dr.Item("DISC_AMT") = If(addDiscount, oldDiscAmt + discAmt, oldDiscAmt - discAmt)
                                If (addDiscount) Then
                                    If (oldUnitPrc - discAmt) < 0.0 Then
                                        dr.Item("RET_PRC") = 0.0
                                    Else
                                        dr.Item("RET_PRC") = oldUnitPrc - discAmt
                                    End If
                                Else
                                If (discAmt >= RetPrc) Then
                                    dr.Item("RET_PRC") = RetPrc
                                Else
                                    dr.Item("RET_PRC") = oldUnitPrc + discAmt
                                    End If
                                End If
                            resp.isUpdated = True

                        Else ' If discAmt <> 0 Then
                            If dr.Table.Columns.Contains("SHU") Then
                                resp.isUpdated = False
                                msg.Append(" " & lnSeqNum & "/" & dr("ITM_CD") & ",")
                                resp.status = DiscountUtils.DiscountStatus.Warning
                            Else
                                resp.isUpdated = True
                            End If
                        End If

                    End If

                    resp.orderLine = dr  ' if SHU'd, make no changes but still keep the line in discounted set
                    Exit For        'found the line no need to continue
                End If
                'this is for relationship line voiding
                If Not discAmt = 0 Then
                    resp.isUpdated = True
                Else ' If discAmt <> 0 Then 
                    resp.isUpdated = False
                    'msg.Append(" " & lnSeqNum & "/" & dr("ITM_CD") & ",")
                    resp.status = DiscountUtils.DiscountStatus.Warning
                End If
            End If
        Next

        resp.message = msg.ToString
        Return resp
    End Function
    ''' <summary>
    ''' Updates the CRM.TEMP_ITM table to reflect the discount that has been applied or removed from
    ''' the given line.
    ''' </summary>
    ''' <param name="dbCommand">the command being used, usually part of a single transaction operation</param>
    ''' <param name="discAmt">the calculated discount amount for this particular order line</param>
    ''' <param name="sessionId">the current session id where this discount was created</param>
    ''' <param name="lnSeqNum">the line ID for which the discount is being added</param>
    ''' <param name="addDiscount">TRUE if discount was added; FALSE if discount was removed</param>
    Private Sub UpdateOrderLineDiscount(ByRef dbCommand As OracleCommand, ByVal discAmt As String,
                                        ByVal sessionId As String, ByVal lnSeqNum As String,
                                        Optional ByVal addDiscount As Boolean = True)

        Dim amt As Double = If(IsNumeric(discAmt), CDbl(discAmt), 0.0)
        Dim sql As StringBuilder = New StringBuilder()
        If (addDiscount) Then
            sql.Append("UPDATE TEMP_ITM SET DISC_AMT = NVL(DISC_AMT,0) + :discAmt, RET_PRC=CASE WHEN NVL(RET_PRC,0) - :discAmt < 0.0 THEN 0.0 ELSE NVL(RET_PRC,0) - :discAmt END ")
        Else
            sql.Append("UPDATE TEMP_ITM SET DISC_AMT = NVL(DISC_AMT,0) - :discAmt, RET_PRC=CASE WHEN NVL(:discAmt,0) >= MANUAL_PRC THEN MANUAL_PRC  WHEN NVL(RET_PRC,0) + :discAmt >= MANUAL_PRC THEN MANUAL_PRC ELSE NVL(RET_PRC,0) + :discAmt END ")
        End If
        sql.Append("WHERE row_id = :aRowId ")

        dbCommand.Parameters.Clear()
        dbCommand.CommandText = sql.ToString()

        dbCommand.Parameters.Add(":aRowId", OracleType.VarChar)
        dbCommand.Parameters(":aRowId").Value = lnSeqNum

        dbCommand.Parameters.Add(":discAmt", OracleType.Number)
        dbCommand.Parameters(":discAmt").Value = amt
        dbCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Updates the CRM.TEMP_ITM and CRM.MULTI_DISC tables to reflect the changes made to
    ''' the discount amounts after reapplying all discounts in the order.
    ''' </summary>
    ''' <param name="sessionId">the current session id where this discount was created</param>
    ''' <param name="dtLnsAfterReapplyDisc">table withe updated discount amounts</param>
    Private Sub UpdateOrderLinesAndDiscounts(ByVal sessionId As String, ByVal dtLnsAfterReapplyDisc As DataTable)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim amt As Double
        Dim retPrc As Double

        '=============================================================================================
        '====== The table being passed in, is a combination of the TEMP_ITM / MULTI_DISC tables ======
        '=== There could be multiple of the same line when the line has multiple discounts applied ===
        '=============================================================================================
        Try
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction

            '=== Updates the TEMP_ITM table
            dbSql.Append("UPDATE temp_itm SET disc_amt = :discAmt, ret_prc = :retPrc ")
            dbSql.Append("WHERE session_id = :sessId AND row_id = :lnSeqNum")

            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnSeqNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":discAmt", OracleType.Number)
            dbCommand.Parameters.Add(":retPrc", OracleType.Number)
            dbCommand.Parameters(":sessId").Value = sessionId

            For Each drLnDis As DataRow In dtLnsAfterReapplyDisc.Rows
                strLnSeq = drLnDis("DEL_DOC_LN#")  'table may have multiple of same line, needs to process ONCE per line
                If (Not lstLnSeq.Contains(strLnSeq)) Then

                    amt = If(IsNumeric(drLnDis("TOTAL_DISC_AMT")), CDbl(drLnDis("TOTAL_DISC_AMT")), 0.0)
                    retPrc = If(IsNumeric(drLnDis("UNIT_PRC")) AndAlso CDbl(drLnDis("UNIT_PRC")) > 0.0, CDbl(drLnDis("UNIT_PRC")), 0.0)

                    dbCommand.Parameters(":lnSeqNum").Value = strLnSeq
                    dbCommand.Parameters(":discAmt").Value = amt
                    dbCommand.Parameters(":retPrc").Value = retPrc
                    dbCommand.ExecuteNonQuery()

                    lstLnSeq.Add(strLnSeq)      'to avoid processing the same line again
                End If
            Next

            '=== Updates the  MULTI_DISC table
            dbSql.Clear()
            dbSql.Append("UPDATE multi_disc SET disc_amt = :discAmt ")
            dbSql.Append("WHERE session_id = :sessId AND line_id = :lnSeqNum AND disc_cd = :discCd ")

            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnSeqNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":discAmt", OracleType.Number)
            dbCommand.Parameters(":sessId").Value = sessionId

            For Each drLnDis As DataRow In dtLnsAfterReapplyDisc.Rows
                amt = If(IsNumeric(drLnDis("DISC_AMT")), CDbl(drLnDis("DISC_AMT")), 0.0)
                dbCommand.Parameters(":discAmt").Value = amt
                dbCommand.Parameters(":lnSeqNum").Value = drLnDis("DEL_DOC_LN#")
                dbCommand.Parameters(":discCd").Value = drLnDis("DISC_CD")
                dbCommand.ExecuteNonQuery()
            Next

            dbCommand.Transaction.Commit()
        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub


    ''' <summary>
    ''' Updates the the in-memory discounts and order lines table for a retrieved order to reflect the changes made to
    ''' the discount amounts after reapplying all discounts in the order. 
    ''' </summary>
    ''' <param name="dtOrderLines">the table that contains ALL order lines in the current order</param>
    ''' <param name="dtLnsAfterReapplyDisc">table withe updated discount amounts</param>
    Private Function UpdateOrderLinesAndDiscounts(ByVal dtOrderLines As DataTable,
                                             ByVal dtDiscApplied As DataTable,
                                             ByVal dtLnsAfterReapplyDisc As DataTable,
                                             ByVal isSOM As Boolean) As String

        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim strDiscLnSeq As String = String.Empty
        Dim strFltrSeq As String = String.Empty
        Dim amt, origAmt As Double
        Dim retPrc As Double
        Dim discAmt As Double
        Dim strWhereClause As String
        Dim ordLns4LnNum As DataRow()
        Dim msg As New StringBuilder
        Dim isShud As Boolean = False

        For Each drLnDis As DataRow In dtLnsAfterReapplyDisc.Rows

            strLnSeq = drLnDis("DEL_DOC_LN#")   'table may have multiple of same line, needs to process ONCE per line
            strDiscLnSeq = drLnDis("DISC_SEQ_NUM")
            strFltrSeq = drLnDis("DEL_DOC_LN#").ToString & drLnDis("DISC_SEQ_NUM").ToString
            '== update the lines table
            If (Not lstLnSeq.Contains(strFltrSeq)) Then
                If isSOM Then
                    strWhereClause = ("[DEL_DOC_LN#] = " & drLnDis("DEL_DOC_LN#"))
                Else
                    strWhereClause = ("[LINE] = " & drLnDis("DEL_DOC_LN#"))
                End If

                ordLns4LnNum = dtOrderLines.Select(strWhereClause)

                If (ordLns4LnNum.Length > 0) Then
                    'Update the line disc amt and price but only if the line is not SHU'd
                    If isSOM Then
                        If ordLns4LnNum(0)("SHU") = "N" Then

                            isShud = False
                            For Each drLn As DataRow In ordLns4LnNum

                                amt = If(IsNumeric(drLnDis("TOTAL_DISC_AMT")), CDbl(drLnDis("TOTAL_DISC_AMT")), 0.0)
                                retPrc = If(IsNumeric(drLnDis("UNIT_PRC")), CDbl(drLnDis("UNIT_PRC")), 0.0)
                                drLn.Item("UNIT_PRC") = retPrc
                                drLn.Item("DISC_AMT") = amt
                            Next

                        Else ' if the line is SHU'd, do NOT update

                            isShud = True
                            amt = 0
                            origAmt = 0
                            For Each drLn As DataRow In ordLns4LnNum

                                origAmt = origAmt + If(IsNumeric(drLn("DISC_AMT")), CDbl(drLn("DISC_AMT")), 0.0)
                                amt = amt + If(IsNumeric(drLnDis("TOTAL_DISC_AMT")), CDbl(drLnDis("TOTAL_DISC_AMT")), 0.0)
                            Next
                            ' if it was to be discounted (amt > 0) and the discounted amt changed then leave a message
                            If amt > 0 AndAlso origAmt <> amt Then
                                msg.Append(" " & strLnSeq & "/" & drLnDis("ITM_CD") & ",")
                            End If
                        End If
                    Else
                        'for relationship , also don't have SHU'd column
                        isShud = False
                        For Each drLn As DataRow In ordLns4LnNum

                            amt = If(IsNumeric(drLnDis("TOTAL_DISC_AMT")), CDbl(drLnDis("TOTAL_DISC_AMT")), 0.0)
                            retPrc = If(IsNumeric(drLnDis("UNIT_PRC")), CDbl(drLnDis("UNIT_PRC")), 0.0)
                            drLn.Item("DISC_AMT") = amt
                            drLn.Item("RET_PRC") = retPrc
                        Next
                    End If
                End If
                lstLnSeq.Add(strLnSeq)      'to avoid processing the same line again
                '           End If

                If isShud = False Then ' if we didn't update the line, then we don't want to update it's discounts either

                    '== update the discounts table
                    strWhereClause = ("DISC_CD = '" & drLnDis("DISC_CD") & "' AND LNSEQ_NUM = " & strLnSeq & " AND DISC_SEQ_NUM=" & strDiscLnSeq)
                    ordLns4LnNum = dtDiscApplied.Select(strWhereClause)

                    If (ordLns4LnNum.Length > 0) Then

                        For Each drDisc As DataRow In ordLns4LnNum

                            discAmt = If(IsNumeric(drLnDis("DISC_AMT")), CDbl(drLnDis("DISC_AMT")), 0.0)
                            drDisc.Item("DISC_AMT") = discAmt
                            drDisc.Item("QTY") = drLnDis("QTY")
                            drDisc.Item("ITM_RETAIL") = drLnDis("UNIT_PRC")
                        Next
                    End If
                End If
            End If
        Next

        If msg.Length > 0 Then
            msg.Remove(msg.Length - 1, 1) ' remove last comma
            Return Resources.POSMessages.MSG0033 & msg.ToString
        Else
            Return String.Empty
        End If

    End Function

    ''' <summary>
    ''' Inserts into the CRM.MULTI_DISC table, a row with the discounts passed in
    ''' </summary>
    ''' <param name="dbCommand">the command being used, usually part of a single transaction operation</param>
    ''' <param name="discount">discount object for details like code, description, etc</param>
    ''' <param name="discAmt">the calculated discount amount for this particular order line</param>
    ''' <param name="sessionId">the current session id where this discount was created</param>
    ''' <param name="lnSeqNum">the line ID for which the discount is being added</param>
    Private Sub InsertMultiDiscRow(ByRef dbCommand As OracleCommand,
                                   ByVal discount As DiscountDtc,
                                   ByVal discAmt As String,
                                   ByVal sessionId As String,
                                   ByVal lnSeqNum As String)

        Dim sbSql As New StringBuilder()
        dbCommand.Parameters.Clear()
        dbCommand.CommandType = CommandType.Text
        dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
        dbCommand.Parameters(":SESSION_ID").Value = sessionId

        dbCommand.Parameters.Add(":LINE_ID", OracleType.VarChar)
        dbCommand.Parameters(":LINE_ID").Value = lnSeqNum

        dbCommand.Parameters.Add(":DISC_CD", OracleType.VarChar)
        dbCommand.Parameters(":DISC_CD").Value = discount.cd

        dbCommand.Parameters.Add(":DISC_DESC", OracleType.VarChar)
        dbCommand.Parameters(":DISC_DESC").Value = discount.desc

        dbCommand.Parameters.Add(":DISC_LEVEL", OracleType.VarChar)
        dbCommand.Parameters(":DISC_LEVEL").Value = discount.level

        dbCommand.Parameters.Add(":DISC_AMT", OracleType.VarChar)
        dbCommand.Parameters(":DISC_AMT").Value = discAmt

        sbSql.Append("INSERT INTO MULTI_DISC (SESSION_ID, LINE_ID, DISC_CD, DISC_DESC, DISC_LEVEL, DISC_AMT")

        If (AppConstants.Discount.TP_PCT = discount.tpCd) Then
            sbSql.Append(", DISC_PCT) VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT, :DISC_PCT )")

            dbCommand.Parameters.Add(":DISC_PCT", OracleType.Double)
            dbCommand.Parameters(":DISC_PCT").Value = discount.amt      'the % setup for this discount

        Else
            sbSql.Append(")           VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT)")
        End If

        dbCommand.CommandText = sbSql.ToString()
        dbCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Deletes from the CRM.MULTI_DISC table, the row that matches the details passed in.
    ''' </summary>
    ''' <param name="dbCommand">the command being used, usually part of a single transaction operation</param>
    ''' <param name="lnSeqNum">the line ID for which the discount is being removed</param>
    ''' <param name="discCd">the discount code being removed</param>
    Private Sub DeleteMultiDiscRow(ByRef dbCommand As OracleCommand, ByVal lnSeqNum As String, ByVal discCd As String)

        dbCommand.Parameters.Clear()
        dbCommand.CommandText = "DELETE FROM MULTI_DISC WHERE line_id = :lineId AND disc_cd = :discCd"

        dbCommand.Parameters.Add(":lineId", OracleType.VarChar)
        dbCommand.Parameters(":lineId").Value = lnSeqNum

        dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
        dbCommand.Parameters(":discCd").Value = discCd
        dbCommand.ExecuteNonQuery()

    End Sub

    ''' <summary>
    ''' Calculates the Discoounts and returns the result in the form of dataset, table(0) will have the result.
    ''' </summary>
    ''' <param name="dbConnection">an OPEN connection to be used for database access</param>
    ''' <param name="lineItems">the table which contains the items for which discount needs to be applied!</param>
    ''' <returns>an object containing all the results from the API call after discount was applied</returns>
    Private Function CalculateDiscounts(ByVal dbConnection As OracleConnection,
                                        ByVal lineItems As DataTable) As CalculateDiscountsResponse

        Dim response As New CalculateDiscountsResponse
        Dim cmd As OracleCommand = GetCommand("", dbConnection)
        Dim dsResult As DataSet = New DataSet()
        Dim xmlOutPut As String

        Dim XMLResult As OracleParameter = New OracleParameter(":XMLResult", OracleType.Clob, 100000, "")
        Dim ErrorMessage As OracleParameter = New OracleParameter(":VError", OracleType.VarChar, 1000, "")
        Dim DiscountErrorStatus As OracleParameter = New OracleParameter(":DiscountErrorStatus", OracleType.VarChar, 1000, "")
        Dim DiscountErrorMessage As OracleParameter = New OracleParameter(":DiscountErrorMessage", OracleType.VarChar, 1000, "")

        XMLResult.Direction = ParameterDirection.Output
        ErrorMessage.Direction = ParameterDirection.Output
        DiscountErrorStatus.Direction = ParameterDirection.Output
        DiscountErrorMessage.Direction = ParameterDirection.Output
        Try
            lineItems.Columns.Add("SrNo", GetType(String))
            Dim lc As Integer = 1
            For Each row As DataRow In lineItems.Rows
                row("SrNo") = lc.ToString()
                lc = lc + 1
            Next

            cmd.CommandType = CommandType.Text
            cmd.CommandText = GetDiscountsApiQuery(lineItems)
            cmd.Parameters.Add(XMLResult)
            cmd.Parameters.Add(ErrorMessage)
            cmd.Parameters.Add(DiscountErrorStatus)
            cmd.Parameters.Add(DiscountErrorMessage)
            cmd.ExecuteNonQuery()

            If (String.IsNullOrEmpty(ErrorMessage.Value.ToString().Trim())) Then
                Dim xmlData = "<?xml version=""1.0"" encoding=""utf-8""?>"
                xmlOutPut = XMLResult.Value.Value.ToString()
                xmlData = xmlData + "<root>" + xmlOutPut + "</root>"
                dsResult.ReadXml(New XmlTextReader(New StringReader(xmlData)))

                ' === populates the values that will be send back to the caller 
                response.dtLnsResult = dsResult.Tables(0)

                If (Not IsDBNull(DiscountErrorMessage.Value) AndAlso
                    Not String.IsNullOrEmpty(DiscountErrorMessage.Value)) Then
                    response.statusMsg = DiscountErrorMessage.Value
                End If

                If DiscountErrorStatus.Value.ToString().Equals("0") Then

                    response.status = If(response.statusMsg.isEmpty(),
                                         DiscountUtils.DiscountStatus.Success,
                                         DiscountUtils.DiscountStatus.Warning)
                Else
                    response.status = DiscountUtils.DiscountStatus.HasError
                End If
            Else
                response.status = DiscountUtils.DiscountStatus.HasError
                response.statusMsg = ErrorMessage.Value.ToString().Trim()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(cmd)
        End Try

        Return response
    End Function

    ''' <summary>
    ''' Calculates the Discoounts and returns the result in the form of dataset, table(0) will have the result.
    ''' </summary>
    ''' <param name="dbConnection">an OPEN connection to be used for database access</param>
    ''' <param name="lineItems">the table which contains the items for which discount needs to be applied!</param>
    ''' <returns>an object containing all the results from the API call after discount was applied</returns>
    Private Function CalculateDiscounts(ByVal lineItems As DataTable, ByRef cmd As OracleCommand) As CalculateDiscountsResponse

        Dim response As New CalculateDiscountsResponse
        'Dim cmd As OracleCommand = GetCommand("", dbConnection)
        Dim dsResult As DataSet = New DataSet()
        Dim xmlOutPut As String
        cmd.Parameters.Clear()
        Dim XMLResult As OracleParameter = New OracleParameter(":XMLResult", OracleType.Clob, 100000, "")
        Dim ErrorMessage As OracleParameter = New OracleParameter(":VError", OracleType.VarChar, 1000, "")
        Dim DiscountErrorStatus As OracleParameter = New OracleParameter(":DiscountErrorStatus", OracleType.VarChar, 1000, "")
        Dim DiscountErrorMessage As OracleParameter = New OracleParameter(":DiscountErrorMessage", OracleType.VarChar, 1000, "")

        XMLResult.Direction = ParameterDirection.Output
        ErrorMessage.Direction = ParameterDirection.Output
        DiscountErrorStatus.Direction = ParameterDirection.Output
        DiscountErrorMessage.Direction = ParameterDirection.Output
        Try
            lineItems.Columns.Add("SrNo", GetType(String))
            Dim lc As Integer = 1
            For Each row As DataRow In lineItems.Rows
                row("SrNo") = lc.ToString()
                lc = lc + 1
            Next

            cmd.CommandType = CommandType.Text
            cmd.CommandText = GetDiscountsApiQuery(lineItems)
            cmd.Parameters.Add(XMLResult)
            cmd.Parameters.Add(ErrorMessage)
            cmd.Parameters.Add(DiscountErrorStatus)
            cmd.Parameters.Add(DiscountErrorMessage)
            cmd.ExecuteNonQuery()

            If (String.IsNullOrEmpty(ErrorMessage.Value.ToString().Trim())) Then
                Dim xmlData = "<?xml version=""1.0"" encoding=""utf-8""?>"
                xmlOutPut = XMLResult.Value.Value.ToString()
                xmlData = xmlData + "<root>" + xmlOutPut + "</root>"
                dsResult.ReadXml(New XmlTextReader(New StringReader(xmlData)))

                ' === populates the values that will be send back to the caller 
                response.dtLnsResult = dsResult.Tables(0)

                If (Not IsDBNull(DiscountErrorMessage.Value) AndAlso
                    Not String.IsNullOrEmpty(DiscountErrorMessage.Value)) Then
                    response.statusMsg = DiscountErrorMessage.Value
                End If

                If DiscountErrorStatus.Value.ToString().Equals("0") Then

                    response.status = If(response.statusMsg.isEmpty(),
                                         DiscountUtils.DiscountStatus.Success,
                                         DiscountUtils.DiscountStatus.Warning)
                Else
                    response.status = DiscountUtils.DiscountStatus.HasError
                End If
            Else
                response.status = DiscountUtils.DiscountStatus.HasError
                response.statusMsg = ErrorMessage.Value.ToString().Trim()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            cmd.Parameters.Clear()
        End Try

        Return response
    End Function

    ''' <summary>
    ''' Returns the query used to retrieve Discounts Applied to an SOE order (or an specific line)
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <param name="discForLine">TRUE if discounts are for a given line; FALSE if discounts are for the whole order</param>
    ''' <returns>query to be used when retrieving SOE applied discounts</returns>
    Private Function GetDiscountsAppliedForSOEQuery(ByVal sessionId As String, Optional discForLine As Boolean = False) As String

        Dim dbSql As StringBuilder = New StringBuilder()
        dbSql.Append(" SELECT ROWNUM AS ROW_IDX, ROW_ID as LNSEQ_NUM , md.DISC_CD,  NVL(md.DISC_DESC, '') as DISC_DES, ")
        dbSql.Append(" md.DISC_LEVEL, ")
        dbSql.Append(" NVL(md.DISC_AMT,0) as DISC_AMT, md.LINE_ID as DISC_SEQ_NUM, NVL(md.DISC_PCT,0) as DISC_PCT, ")
        dbSql.Append(" ITM_CD, NVL(DES,'') AS ITM_DES, TEMP_ITM.QTY, TEMP_ITM.MNR_CD, NVL(RET_PRC,0) as ITM_RETAIL, md.SEQ_NUM  ")
        dbSql.Append(" FROM MULTI_DISC md, TEMP_ITM  ")
        dbSql.Append(" WHERE TEMP_ITM.ROW_ID = md.LINE_ID ")
        dbSql.Append(" AND md.session_id = TEMP_ITM.session_id ")
        dbSql.Append(" AND md.session_id = :sessionId ")
        If (discForLine) Then
            dbSql.Append(" AND TEMP_ITM.ROW_ID = :lnSeqNum ")
        End If
        dbSql.Append(" ORDER BY ROW_ID ")

        Return dbSql.ToString()
    End Function

    ''' <summary>
    ''' Prepares the command object passed in, by adding the corresponding query string as well as its parameters
    ''' based on the details of the discount that is to be processed
    ''' </summary>
    ''' <param name="dbCommand">Command object that will be used to retrieved Discountable Lines</param>
    ''' <param name="discount">discount object with the details of the discount that is being applied</param>
    ''' <param name="sessionId">the session ID for the order being processed</param>
    ''' <param name="addItemDetails">TRUE if additional details from the item table should be included</param>
    ''' <remarks>******************** USE THIS METHOD DURING ORDER ENTRY ********************</remarks>
    Private Sub PrepareCommandForDiscountableLines(ByRef dbCommand As OracleCommand,
                                                   ByVal discount As DiscountDtc,
                                                   ByVal sessionId As String,
                                                   Optional addItemDetails As Boolean = False)

        Dim dbSql As StringBuilder = New StringBuilder()

        'builds query to retrieve lines, taking into account the discount attributes
        'exclude any lines that are pkg headers and it's components if the discount is configured to exclude pkgs
        'if there were any minors setup, then include only the lines with matching minor codes.
        dbSql.Append("SELECT CAST(t.ROW_ID AS VARCHAR2(50)) AS DEL_DOC_LN#, t.ITM_CD, NVL(t.DES, t.ITM_CD) AS DES, NVL(t.QTY,0) AS QTY, ")
        dbSql.Append("NVL(t.ORIG_PRC,0) AS ORIG_PRC, NVL(t.RET_PRC,0) AS UNIT_PRC ")

        If (addItemDetails) Then
            dbSql.Append(", CAST(t.PACKAGE_PARENT as VARCHAR2(50)) AS PKG_SOURCE, 'N' AS VOID_FLAG, i.MNR_CD ")
            dbSql.Append(", NVL(i.REPL_CST,0) AS REPL_CST, NVL(i.PU_DISC_PCNT,0) AS PU_DISC_PCNT ")
            dbSql.Append(" FROM TEMP_ITM t, ITM i")
        Else
            dbSql.Append(" FROM TEMP_ITM t")
        End If

        If (discount.applyToPkg) Then
            dbSql.Append(" WHERE session_id = :sessionId ")
        Else
            dbSql.Append(" WHERE session_id = :sessionId AND t.ITM_TP_CD <> '").Append(SkuUtils.ITM_TP_PKG).Append("' AND  t.PACKAGE_PARENT is NULL ")
        End If

        If (addItemDetails) Then
            dbSql.Append(" AND t.ITM_CD = i.ITM_CD ")
        End If

        'if the discount is setup by minor, needs to include Minor conditions in the query
        If (discount.isByMinor AndAlso (GetMinorsLst(discount.cd)).Count > 0) Then

            dbSql.Append(" AND t.MNR_CD IN ( ").Append(GetMinorsQuery()).Append(" )")

            dbCommand.Parameters.Add(":discountCd", OracleType.VarChar)
            dbCommand.Parameters(":discountCd").Value = discount.cd
        End If

        dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
        dbCommand.Parameters(":sessionId").Value = sessionId
        dbCommand.CommandText = dbSql.ToString()

    End Sub

    ''' <summary>
    ''' Returns the query to be used when retrieving the minors for a given discount cd
    ''' </summary>
    ''' <param name="includeCondOrDiscFlg">indicates if the minor represents a discounted minor or a conditional minor; needed for SHU'd docs - cannot discount
    ''' already SHU'd lines but SHU'd lines can be used as conditional for a new line to be discounted; used in Order Mgmt</param>
    ''' <returns>query for Minors</returns>
    Private Function GetMinorsQuery(Optional ByVal includeCondOrDiscFlg As Boolean = False) As String

        Dim dbSql As StringBuilder = New StringBuilder()

        If includeCondOrDiscFlg Then

            dbSql.Append("SELECT mnr_cd, '").Append(AppConstants.Discount.MNR_TP_DISC).Append("' AS condOrDisc FROM DISC_MNR WHERE disc_cd = :discountCd ")
            dbSql.Append("UNION ALL SELECT mnr_cd, '").Append(AppConstants.Discount.MNR_TP_COND).Append("' AS condOrDisc FROM COND_MNR WHERE disc_cd = :discountCd")
        Else
            dbSql.Append("SELECT mnr_cd FROM DISC_MNR WHERE disc_cd = :discountCd ")
            dbSql.Append("UNION ALL SELECT mnr_cd FROM COND_MNR WHERE disc_cd = :discountCd")
        End If

        Return dbSql.ToString()
    End Function

    ''' <summary>
    ''' Prepares the query to access the E1 DISCOUNT packaes
    ''' </summary>
    ''' <param name="discountTable">the table which contains the items for which discount needs to be applied!</param>
    ''' <returns>the prepared query</returns>
    Private Function GetDiscountsApiQuery(discountTable As DataTable) As String
        Dim sbQuery As StringBuilder = New StringBuilder()
        sbQuery.Append(" DECLARE  ")
        sbQuery.Append(" v_del_ln   int; ")
        sbQuery.Append(" V_Qty   int; ")
        sbQuery.Append(" v_autodoc_rec   bt_util.autodoc_rectype; ")
        sbQuery.Append(" v_XML   CLOB := NULL;  ")
        sbQuery.Append(" TEMP_DOC_NUM    VARCHAR2(20); ")
        sbQuery.Append(" result    VARCHAR2(4000) := '' ; ")
        sbQuery.Append(" v_so_ln_rec     bt_soln_manager.so_ln_recsubtype; ")
        sbQuery.Append(" return_status  NUMBER     ; ")
        sbQuery.Append(" return_mesg    VARCHAR2(82)  ; ")
        sbQuery.Append(" v_unit_prc varchar(20); ")
        sbQuery.Append(" v_pu_disc_amt number ; ")
        sbQuery.Append(" v_disc_amt    number ; ")
        sbQuery.Append(" v_itm_cd_io VARCHAR2(20)  ; ")
        sbQuery.Append(" v_repl_cst_io   number; ")
        sbQuery.Append(" v_pu_disc_pcnt  number  ; ")
        sbQuery.Append(" v_pu_del_io  VARCHAR2(1); ")
        sbQuery.Append(" V_disc_cd disc.disc_cd%TYPE; ")
        sbQuery.Append(" mnr_cd_i   disc_mnr.mnr_cd%TYPE; ")
        sbQuery.Append(" void_flag_i   so_ln.void_flag%TYPE; ")
        sbQuery.Append(" pkg_source_i   VARCHAR2(20); ")
        sbQuery.Append(" is_shu_i   VARCHAR2(1); ")
        sbQuery.Append(" BEGIN ")
        sbQuery.Append(" v_autodoc_rec.store_cd := '" & HttpContext.Current.Session("HOME_STORE_CD") & "'; ")
        sbQuery.Append(" v_autodoc_rec.doc_date  :=  sysdate; ")
        sbQuery.Append(" bt_util.autodoc( v_autodoc_rec ); ")
        ' Maybe below should have called bt_sale version but this is a quick fix that works
        sbQuery.Append(" bt_soln_manager.del_so_ln_tab; ")
        sbQuery.Append(" TEMP_DOC_NUM   :=  v_autodoc_rec.doc_num; ")

        'initilize the variables
        For Each dr As DataRow In discountTable.Rows

            If (dr("qty") & "" <> "0" AndAlso dr("qty") & "" <> "") Then
                sbQuery.Append(" v_del_ln := " + dr("SrNo").ToString() + "; ")
                sbQuery.Append(" V_Qty := " + If(dr("qty").ToString() = String.Empty, "0", dr("qty").ToString()) + "; ")
                sbQuery.Append(" v_itm_cd_io := '" + dr("itm_cd").ToString() + "'; ")
                sbQuery.Append(" v_repl_cst_io :=" + If(dr("repl_cst").ToString() = String.Empty Or dr("repl_cst").ToString().ToUpper() = "NULL", "0", dr("repl_cst").ToString()) + "; ")
                sbQuery.Append(" v_pu_disc_pcnt := " + If(dr("pu_disc_pcnt").ToString().ToUpper().Trim() = "NULL" Or dr("pu_disc_pcnt").ToString().ToUpper().Trim() = String.Empty, "0", dr("pu_disc_pcnt").ToString()) + "; ")
                sbQuery.Append(" v_unit_prc := '" + dr("unit_prc").ToString() + "'; ")
                sbQuery.Append(" v_pu_del_io :='" + dr("pu_del").ToString() + "'; ")
                sbQuery.Append(" v_pu_disc_amt := " + If(dr("pu_disc_amt").ToString() = String.Empty, "0", dr("pu_disc_amt").ToString()) + "; ")
                sbQuery.Append(" V_disc_cd   := '" + dr("disc_cd").ToString() + "'; ")
                sbQuery.Append(" v_disc_amt := '" + dr("disc_amt").ToString() + "'; ")

                sbQuery.Append(" mnr_cd_i := '" + dr("mnr_cd").ToString() + "'; ")
                sbQuery.Append(" void_flag_i := '" + dr("void_flag").ToString() + "'; ")
                sbQuery.Append(" pkg_source_i := '" + If(dr("pkg_source").ToString().ToUpper() = "NULL", "", dr("pkg_source").ToString()) + "'; ")

                sbQuery.Append(" v_autodoc_rec.store_cd  := '" & HttpContext.Current.Session("HOME_STORE_CD") & "'; ")
                sbQuery.Append(" v_autodoc_rec.doc_date  :=  sysdate; ")

                'MM-7350- Add the flag to indicate if the line item is SHU'd or not
                sbQuery.Append(" is_shu_i  := '" + dr("IS_SHU").ToString().ToUpper() + "';")

                sbQuery.Append(" bt_sale.ins_so_ln_rec (TEMP_DOC_NUM,")
                sbQuery.Append(" v_del_ln,")
                sbQuery.Append(" V_Qty,")
                sbQuery.Append(" v_itm_cd_io,")
                sbQuery.Append(" v_repl_cst_io,")
                sbQuery.Append(" v_pu_disc_pcnt,")
                sbQuery.Append(" v_unit_prc,")
                sbQuery.Append(" v_pu_del_io,")
                sbQuery.Append(" v_pu_disc_amt,")
                sbQuery.Append(" V_disc_cd,")
                sbQuery.Append(" v_disc_amt,")
                sbQuery.Append(" return_status,")
                sbQuery.Append("return_mesg,")
                sbQuery.Append(" void_flag_i,")
                sbQuery.Append(" pkg_source_i,")
                'Order type
                sbQuery.Append(" '" + AppConstants.Order.TYPE_SAL + "',")
                sbQuery.Append(" NULL,") 'land_repl_cst_i  parameter default is NULL
                sbQuery.Append(" is_shu_i);")
            End If
            'test script
            'sbQuery.Append(" dbms_outPut.put_line('return_status ' ||return_status||' / '||return_mesg); ")
            'test script ends here
        Next
        'Get All the Items for which the Discount is calculated
        'Call the Pakage and gather the out put and form an XML string which is sent as BLOB and extract the same to table!
        ' Thiscall is made once for the entire batch! 
        sbQuery.Append(" bt_sale.apply_disc(TEMP_DOC_NUM,V_disc_cd,return_status, return_mesg ); ")

        sbQuery.Append("SELECT   return_status INTO :DiscountErrorStatus FROM DUAL;  ")
        sbQuery.Append("SELECT   return_mesg INTO :DiscountErrorMessage FROM DUAL;  ")

        'Loop again to read the contents
        For Each dr As DataRow In discountTable.Rows
            If (dr("qty") & "" <> "0" AndAlso dr("qty") & "" <> "") Then
                'initilize the parameters to call the bt_sale.get_so_ln_tab_rec package!
                sbQuery.Append(" v_del_ln      := " + dr("SrNo").ToString() + "; ")
                sbQuery.Append(" bt_sale.get_so_ln_tab_rec (   v_del_ln,           ")
                sbQuery.Append("                               v_qty ,      ")
                sbQuery.Append("                               v_itm_cd_io, ")
                sbQuery.Append("                               v_unit_prc,  ")
                sbQuery.Append("                               v_repl_cst_io, ")
                sbQuery.Append("                               v_pu_disc_pcnt,")
                sbQuery.Append("                               v_disc_amt,    ")
                sbQuery.Append("                               v_pu_disc_amt, ")
                sbQuery.Append("                               v_pu_del_io,   ")
                sbQuery.Append("                               return_status, ")
                sbQuery.Append("                               return_mesg    ")
                sbQuery.Append("                          );")
                'sbQuery.Append("result:=result|| '<line><deldocno>'||v_del_ln||'</deldocno><qty>'||v_qty||'</qty><itmcd>'||v_itm_cd_io||'</itmcd><unitprice>'||v_unit_prc||'</unitprice>")
                sbQuery.Append("v_XML:=  v_XML ||'<LINE><DELLN>'||" + dr("del_ln").ToString() + "||'</DELLN><QTY>'||v_qty||'</QTY><ITMCD>'||v_itm_cd_io||'</ITMCD><UNITPRICE>'||v_unit_prc||'</UNITPRICE>")
                sbQuery.Append("<REPLCST>'||v_repl_cst_io||'</REPLCST><PUDISCPCNT>'||v_pu_disc_pcnt||'</PUDISCPCNT><DISCAMT>'||v_disc_amt||'</DISCAMT><PUDISCAMT>'||v_pu_disc_amt||'</PUDISCAMT><PUDEL>'||v_pu_del_io||'</PUDEL>")
                sbQuery.Append("<RETURNSTATUS>'||return_status||'</RETURNSTATUS><RETURNMESG>'||return_mesg||'</RETURNMESG></LINE> '; ")
            End If
        Next
        'test script TO_CLOB(
        'sbQuery.Append(" dbms_output.put_line('TEMP_DOC_NUM '||TEMP_DOC_NUM||' / '||v_itm_cd_io||'/'||v_disc_amt||' / '||v_unit_prc); ")
        'test script Ends
        'sbQuery.Append("result:= 'TEMP_DOC_NUM '||TEMP_DOC_NUM||' / '||v_itm_cd_io||'/'||v_disc_amt||' / '||v_unit_prc  ; ")
        sbQuery.Append("SELECT   v_XML INTO :XMLResult FROM DUAL;  ")
        sbQuery.Append(" exception ")
        sbQuery.Append(" when others then ")
        sbQuery.Append("result :='error :'||SQLERRM ; ")
        sbQuery.Append("SELECT   result INTO :VError FROM DUAL;  ")
        'sbQuery.Append(":VError :='error :'||SQLERRM ; ")
        sbQuery.Append(" END; ")

        Return System.Text.RegularExpressions.Regex.Replace(sbQuery.ToString(), "\s+", " ")
    End Function

    ''' <summary>
    ''' Creates a datatable in the format expected when calling the Discounts API, using the line
    ''' details passed in.
    ''' </summary>
    ''' <param name="linesToDiscount">contains the lines that are being discounted</param>
    ''' <param name="pkpDel">indicates if the order is Pickup or Delivery</param>
    ''' <param name="discCd">the code of the discount that is being applied</param>
    ''' <returns>a datatable in the format expected when calling the Discounts APIs</returns>
    Private Function GetTableToCalculateDiscounts(ByVal linesToDiscount As DataTable,
                                                  ByVal pkpDel As String,
                                                  ByVal discCd As String,
                                                  Optional ByVal isSOM As Boolean = False) As DataTable
        Dim rowIdx As Integer = 0
        Dim calcDiscRow As DataRow
        Dim calcDiscTable As DataTable = GetTemplateTable()

        For Each dr As DataRow In linesToDiscount.Rows

            calcDiscRow = calcDiscTable.NewRow()
            If dr.Table.Columns.Contains("DEL_DOC_LN#") Then
                calcDiscRow("DEL_LN") = dr.Item("DEL_DOC_LN#")
                calcDiscRow("UNIT_PRC") = Math.Round(dr.Item("UNIT_PRC"), 4, MidpointRounding.AwayFromZero) ' dr.Item("UNIT_PRC")

            ElseIf dr.Table.Columns.Contains("LINE") Then
                calcDiscRow("DEL_LN") = dr.Item("LINE")
                calcDiscRow("UNIT_PRC") = Math.Round(dr.Item("RET_PRC"), 4, MidpointRounding.AwayFromZero) ' dr.Item("RET_PRC")
            End If

            calcDiscRow("DISC_AMT") = If(dr.Table.Columns.Contains("DISC_AMT") AndAlso Not (IsNothing(dr("DISC_AMT")) OrElse String.IsNullOrEmpty(dr("DISC_AMT").ToString())), dr("DISC_AMT"), "0")
            calcDiscRow("ITM_CD") = dr.Item("ITM_CD")
            calcDiscRow("QTY") = dr.Item("QTY")
            calcDiscRow("REPL_CST") = dr.Item("REPL_CST")
            calcDiscRow("PU_DEL") = pkpDel
            calcDiscRow("PU_DISC_AMT") = "0"
            calcDiscRow("PU_DISC_PCNT") = dr.Item("PU_DISC_PCNT")
            calcDiscRow("DISC_CD") = discCd
            calcDiscRow("MNR_CD") = dr.Item("MNR_CD")


            If linesToDiscount.Columns.Contains("VOID_FLAG") Then
                calcDiscRow("VOID_FLAG") = dr.Item("VOID_FLAG")
            End If
            If linesToDiscount.Columns.Contains("PKG_SOURCE") Then
                calcDiscRow("PKG_SOURCE") = dr.Item("PKG_SOURCE")
            End If

            calcDiscRow("RETURN_STATUS") = ""
            calcDiscRow("RETURN_MESG") = ""

            'MM-7350- Add the flag to indicate if the line item is SHU'd or not, which will used to call E1 Discount calculation API
            If dr.Table.Columns.Contains("SHU") Then
                calcDiscRow("IS_SHU") = If(dr.Table.Columns.Contains("SHU") AndAlso Not (IsNothing(dr("SHU")) OrElse String.IsNullOrEmpty(dr("SHU").ToString())), dr.Item("SHU"), "N")
            Else
                calcDiscRow("IS_SHU") = "N"
            End If
            calcDiscTable.Rows.InsertAt(calcDiscRow, rowIdx)
            rowIdx = rowIdx + 1
        Next

        Return calcDiscTable
    End Function

    ''' <summary>
    ''' Prepares and returns empty table structure in which the item details needs to be filled
    ''' </summary>
    ''' <returns>Empty data table</returns>
    ''' <remarks> ***** THIS TABLE IS NEEDED TO CALL THE E1 DISCOUNTS API ***** </remarks>
    Private Function GetTemplateTable() As DataTable
        Dim Discount As DataTable = New DataTable()
        Discount.Columns.Add("del_ln", Type.GetType("System.String"))
        Discount.Columns.Add("qty", Type.GetType("System.String"))
        Discount.Columns.Add("itm_cd", Type.GetType("System.String"))
        Discount.Columns.Add("repl_cst", Type.GetType("System.String"))
        Discount.Columns.Add("pu_disc_pcnt", Type.GetType("System.String"))
        Discount.Columns.Add("unit_prc", Type.GetType("System.String"))
        Discount.Columns.Add("pu_del", Type.GetType("System.String"))
        Discount.Columns.Add("pu_disc_amt", Type.GetType("System.String"))
        Discount.Columns.Add("disc_cd", Type.GetType("System.String"))
        Discount.Columns.Add("disc_amt", Type.GetType("System.String"))
        Discount.Columns.Add("MNR_CD", Type.GetType("System.String"))
        Discount.Columns.Add("VOID_FLAG", Type.GetType("System.String"))
        Discount.Columns.Add("PKG_SOURCE", Type.GetType("System.String"))
        Discount.Columns.Add("RETURN_STATUS", Type.GetType("System.String"))
        Discount.Columns.Add("RETURN_MESG", Type.GetType("System.String"))
        Discount.Columns.Add("IS_SHU", Type.GetType("System.String"))
        Return Discount
    End Function

    ''' <summary>
    ''' Inner class used to hold the resulting values from calling the E1 APIs to
    ''' calculate discounts.
    ''' </summary>
    Private Class CalculateDiscountsResponse
        Property status As DiscountUtils.DiscountStatus
        Property statusMsg As String = String.Empty
        Property dtLnsResult As New DataTable
    End Class
    'MM-6380 Start
    ''' <summary>
    ''' Retrieves all the discount information for all the lines in the relationship passed-in.
    ''' </summary>
    ''' <param name="delDocNum">the delDocNum  to use to query the discount details</param>
    ''' <returns>the data table of the discount details</returns>
    Public Function GetDiscountsAppliedForRelationship(ByVal delDocNum As String) As DataTable

        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT ROWNUM AS ROW_IDX, RELD.LINE as LNSEQ_NUM, RELD.DISC_CD,  ")
        dbSql.Append("d.DISC_CD || ' - ' || NVL(d.DES, '') as DISC_DES,  ")
        dbSql.Append("NVL(RELD.DISC_AMT,0) as DISC_AMT, SEQ_NUM as DISC_SEQ_NUM, NVL(RELD.DISC_PCT,0) as DISC_PCT, ")
        dbSql.Append("NVL(RELD.DISC_LEVEL, DECODE(d.DISC_LEVEL, 'E', 'L', d.DISC_LEVEL)) as DISC_LEVEL, ")
        dbSql.Append("RELN.QTY, RELN.ITM_CD,  i.DES as ITM_DES, NVL(RELN.RET_PRC,0) as ITM_RETAIL ")
        dbSql.Append("FROM DISC d, itm i, RELATIONSHIP_LINES RELN, 2)	RELATIONSHIP_LINES_DESC RELD ")
        dbSql.Append("WHERE RELD.REL_NO = RELN.REL_NO ")
        dbSql.Append("AND RELD.del_doc_ln# = RELN.LINE ")
        dbSql.Append("AND RELD.disc_cd = d.disc_cd  ")
        dbSql.Append("AND RELN.itm_cd = i.itm_cd ")
        dbSql.Append("AND RELD.REL_NO = :REL_NO ")
        dbSql.Append("ORDER BY RELD.DEL_DOC_LN#")
        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":REL_NO", OracleType.VarChar)
            dbCommand.Parameters(":REL_NO").Value = delDocNum

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)
            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable
    End Function

    ''' <summary>
    ''' Returns the query used to retrieve Discounts Applied to an SOE order (or an specific line)
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <returns>query to be used when retrieving SOE applied discounts</returns>
    Public Function GetDiscountsAppliedForSOCreation(ByVal sessionId As String) As DataTable
        Dim discountsTable As DataTable
        Dim discountsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()
        Try
            dbConnection.Open()
            dbSql.Append(" SELECT ROWNUM AS ROW_IDX, ROW_ID as LNSEQ_NUM , md.DISC_CD,  NVL(md.DISC_DESC, '') as DISC_DES, ")
            dbSql.Append(" md.DISC_LEVEL, ")
            dbSql.Append(" NVL(md.DISC_AMT,0) as DISC_AMT, md.LINE_ID as DISC_SEQ_NUM, NVL(md.DISC_PCT,0) as DISC_PCT, ")
            dbSql.Append(" TEMP_ITM.ITM_CD AS ITM_CD, TEMP_ITM.QTY, TEMP_ITM.MNR_CD, NVL(RET_PRC,0) as ITM_RETAIL, ")
            dbSql.Append(" CASE WHEN D.EXP_DT >= TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR') THEN 'N' ELSE 'Y' END DISC_EXP ")
            dbSql.Append(" FROM MULTI_DISC md, TEMP_ITM ,DISC D  ")
            dbSql.Append(" WHERE TEMP_ITM.ROW_ID = md.LINE_ID ")
            dbSql.Append(" AND md.session_id = TEMP_ITM.session_id ")
            dbSql.Append(" AND md.DISC_CD = D.DISC_CD ")
            dbSql.Append(" AND md.session_id = :sessionId ")
            dbSql.Append(" ORDER BY ROW_ID ")

            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql.ToString()

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(discountsDataSet)

            If (IsNothing(discountsDataSet.Tables(0))) Then
                discountsTable = New DataTable()
            Else
                discountsTable = discountsDataSet.Tables(0)
            End If

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return discountsTable

    End Function
    ''' <summary>
    ''' Deletes ALL records from MULTI_TEMP that match the sessionID passed in.
    ''' This operation clears only the lines for a multiple discount lines.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    ''' <param name="lstDISC_CD">the list of DISC_CD expired</param>
    Public Function DeleteTempLnDiscount(ByVal sessionId As String, ByVal lstDISC_CD As ArrayList) As Boolean
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)

        Try
            dbConnection.Open()
            For Each discLn In lstDISC_CD
                dbCommand.Parameters.Clear()
                dbCommand.CommandText = "DELETE FROM MULTI_DISC WHERE SESSION_ID = :SESSION_ID AND disc_cd = :DISC_CD"

                dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
                dbCommand.Parameters(":SESSION_ID").Value = sessionId

                dbCommand.Parameters.Add(":DISC_CD", OracleType.VarChar)
                dbCommand.Parameters(":DISC_CD").Value = discLn

                dbCommand.ExecuteNonQuery()
            Next
        Catch EX As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return True
    End Function
    ''' <summary>
    ''' Deletes ALL records from MULTI_TEMP that match the sessionID passed in.
    ''' This operation clears only the lines for a multiple discount lines.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    Public Function DeleteTempLnDiscount(ByVal sessionId As String) As Boolean
        Dim dtLnsDiscounted As New ArrayList()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Clear()
            dbCommand.CommandText = "DELETE FROM MULTI_DISC WHERE SESSION_ID = :SESSION_ID "

            dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
            dbCommand.Parameters(":SESSION_ID").Value = sessionId

            dbCommand.ExecuteNonQuery()
        Catch EX As Exception
            Throw EX
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return True
    End Function
    ''' <summary>
    '''If all the discount applied are expired then replace the retail price with orginal price and 
    ''' discount amount as null.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    Public Function ReapplyDiscountForRelConvSOE(ByVal sessionId As String) As Boolean
        Dim dtLnsDiscounted As DataTable
        Dim dtDiscounts As DataTable

        '=== Recalculates ALL discounts found in the SOE order represented by the sessionID passed in
        dtDiscounts = GetDiscountsToReapply(sessionId)
        If (dtDiscounts.Rows.Count = 0) Then
            dtLnsDiscounted = GetDiscountedLines(sessionId, True)
            '=== Saves to the TEMP_ITM and MULTI_DISC tables the updates made
            UpdateOrderLines(sessionId, dtLnsDiscounted)
        End If
    End Function

    Private Sub UpdateOrderLines(ByVal sessionId As String, ByVal dtLnsAfterReapplyDisc As DataTable)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim amt As Double
        Dim retPrc As Double

        '=============================================================================================
        '====== The table being passed in, is a combination of the TEMP_ITM / MULTI_DISC tables ======
        '=== There could be multiple of the same line when the line has multiple discounts applied ===
        '=============================================================================================
        Try
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction

            '=== Updates the TEMP_ITM table
            dbSql.Append("UPDATE temp_itm SET disc_amt = :discAmt, ret_prc = :retPrc ")
            dbSql.Append("WHERE session_id = :sessId AND row_id = :lnSeqNum")

            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnSeqNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":discAmt", OracleType.Number)
            dbCommand.Parameters.Add(":retPrc", OracleType.Number)
            dbCommand.Parameters(":sessId").Value = sessionId

            For Each drLnDis As DataRow In dtLnsAfterReapplyDisc.Rows
                strLnSeq = drLnDis("DEL_DOC_LN#")  'table may have multiple of same line, needs to process ONCE per line

                If (Not lstLnSeq.Contains(strLnSeq)) Then
                    amt = If(IsNumeric(drLnDis("TOTAL_DISC_AMT")), CDbl(drLnDis("TOTAL_DISC_AMT")), 0.0)
                    retPrc = If(IsNumeric(drLnDis("RET_PRC_DISC")), CDbl(drLnDis("RET_PRC_DISC")), 0.0)
                    dbCommand.Parameters(":lnSeqNum").Value = strLnSeq
                    dbCommand.Parameters(":discAmt").Value = amt
                    dbCommand.Parameters(":retPrc").Value = retPrc
                    dbCommand.ExecuteNonQuery()
                    lstLnSeq.Add(strLnSeq)      'to avoid processing the same line again
                End If

            Next
            dbCommand.Transaction.Commit()
        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub
    ''' <summary>
    ''' Retrieves from the TEMP_ITM table, the lines that match the sessionID passed in,
    ''' and that are found to be discounted (records found in the MULTI_DISC table)
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to get the lines added in this session</param>
    ''' <returns>a datatable containing all discounted lines</returns>
    ''' <remarks>IMPORTANT: Use ONLY to reapply discounts, the UNIT_PRICE IS OVERRIDEN</remarks>
    Private Function GetDiscountedLines(ByVal sessionId As String, ByVal discountApplied As Boolean) As DataTable

        Dim dtLnsDiscounted As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()
        If discountApplied Then

            '==== Since all the discounts are being applied again, restores original price (on unit_prc) to start over
            dbSql.Append("SELECT t.ROW_ID as DEL_DOC_LN#, t.ITM_CD, t.DES, NVL(t.QTY, 0) AS QTY, NVL(t.ORIG_PRC, 0) AS ORIG_PRC, ")
            dbSql.Append("       NVL(t.MANUAL_PRC, 0) AS UNIT_PRC, 0 AS TOTAL_DISC_AMT, NVL(i.REPL_CST, 0) AS REPL_CST, ")
            dbSql.Append("       NVL(i.PU_DISC_PCNT, 0) AS PU_DISC_PCNT, i.MNR_CD, t.PACKAGE_PARENT as PKG_SOURCE , NVL(t.ret_prc, 0) + NVL(t.DISC_AMT, 0) AS RET_PRC_DISC ")
            dbSql.Append("FROM TEMP_ITM t, ITM i ")
            dbSql.Append("WHERE t.ITM_CD = i.ITM_CD AND ")
            dbSql.Append("      t.session_id = :sessionId ")
            dbSql.Append("ORDER BY ROW_ID ")

            Try
                dbCommand.CommandText = dbSql.ToString
                dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
                dbCommand.Parameters(":sessionId").Value = sessionId

                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(dtLnsDiscounted)
                dbAdapter.Dispose()
            Catch ex As Exception
                Throw ex
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If
        Return dtLnsDiscounted
    End Function
    ''' <summary>
    '''Insert into the MULTI_DISC table on when relationship converted to sales order.
    ''' </summary>
    ''' <param name="sessionId">the session ID to use to query the discount details</param>
    ''' <param name="ln2disc">relationship discount details.</param>
    Public Function InserttoMultiDisc(ByVal sessionId As String, ByVal ln2disc As DataTable)
        Dim sbSql As New StringBuilder()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Try
            dbConnection.Open()
            For Each discLn As DataRow In ln2disc.Rows

                dbCommand.CommandType = CommandType.Text
                dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
                dbCommand.Parameters(":SESSION_ID").Value = sessionId

                dbCommand.Parameters.Add(":LINE_ID", OracleType.VarChar)
                dbCommand.Parameters(":LINE_ID").Value = discLn.Item("ROW_ID")

                dbCommand.Parameters.Add(":DISC_CD", OracleType.VarChar)
                dbCommand.Parameters(":DISC_CD").Value = discLn.Item("DISC_CD")

                dbCommand.Parameters.Add(":DISC_DESC", OracleType.VarChar)
                dbCommand.Parameters(":DISC_DESC").Value = discLn.Item("DISC_DES")

                dbCommand.Parameters.Add(":DISC_LEVEL", OracleType.VarChar)
                dbCommand.Parameters(":DISC_LEVEL").Value = discLn.Item("DISC_LEVEL")

                dbCommand.Parameters.Add(":DISC_AMT", OracleType.VarChar)
                dbCommand.Parameters(":DISC_AMT").Value = discLn.Item("DISC_AMT")

                dbCommand.Parameters.Add(":SEQ_NUM", OracleType.VarChar)
                dbCommand.Parameters(":SEQ_NUM").Value = discLn.Item("SEQ_NUM")

                sbSql.Append("INSERT INTO MULTI_DISC (SESSION_ID, LINE_ID, DISC_CD, DISC_DESC, DISC_LEVEL, DISC_AMT,SEQ_NUM ")

                If (AppConstants.Discount.TP_PCT = discLn.Item("TYPE")) Then
                    sbSql.Append(", DISC_PCT) VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT,:SEQ_NUM, :DISC_PCT )")

                    dbCommand.Parameters.Add(":DISC_PCT", OracleType.Double)
                    dbCommand.Parameters(":DISC_PCT").Value = discLn.Item("AMT")       'the % setup for this discount

                Else
                    sbSql.Append(")           VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT,:SEQ_NUM )")
                End If
                dbCommand.CommandText = sbSql.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.Parameters.Clear()
                sbSql.Clear()
            Next
        Catch EX As Exception
            Throw EX
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Function
    ''' <summary>
    ''' Retrieves details from the Session("DISC_APPLIED_RELN") Applied table for those discount records
    ''' that are NOT associated to any Session("REL_LN") record, due to a line void event.
    ''' </summary>
    ''' <param name="dtRelnLines">the Relationship line data details</param>
    ''' <param name="dtDiscApplied">the discount applied for Relationship lines data details</param>
    ''' <returns>table containing the orphaned discounts details</returns>
    Public Function RemoveDiscountsDeleteFromRELN(ByVal dtRelnLines As DataTable, ByVal dtDiscApplied As DataTable, ByVal IsSOM As Boolean)

        Dim discountsTable As DataTable = DiscountUtils.GetLineDiscountTable()
        Dim discountRemoveTable As DataTable = DiscountUtils.GetLineDiscountTable()
        Dim lstRows As DataRow()
        Dim strWhereClause As String
        Dim lstDiscTobeDeleted As New ArrayList()
        Dim lc As Integer = 0
        If (dtDiscApplied.Select("DISC_IS_DELETED = false").Length() > 0) Then

            'select all non-deleted discounts
            lstRows = dtDiscApplied.Select("DISC_IS_DELETED = false")

            If (lstRows.Length > 0) Then
                For Each drDisc As DataRow In dtDiscApplied.Rows
                    If IsSOM Then
                        strWhereClause = ("[DEL_DOC_LN#] = " & drDisc("LNSEQ_NUM") & " ")
                    Else
                        strWhereClause = ("[LINE] = " & drDisc("LNSEQ_NUM") & " ")
                    End If
                    If Not dtRelnLines.Select(strWhereClause).Length > 0 Then
                        lstDiscTobeDeleted.Add(drDisc)
                    End If
                Next

                For Each dr As DataRow In lstDiscTobeDeleted
                    dtDiscApplied.Rows.Remove(dr)
                Next

            End If
        End If
        dtDiscApplied.AcceptChanges()
    End Function
    ''' <summary>
    ''' DeleteMultiDiscRow
    ''' </summary>
    ''' <param name="dbCommand">Oracle Command Object</param>
    ''' <param name="lstDiscToRemove">list of discounts to remove</param>
    ''' <remarks></remarks>
    Private Sub DeleteMultiDiscRow(ByRef dbCommand As OracleCommand, ByVal lstDiscToRemove As List(Of RemoveDiscountDtc))
        dbCommand.Parameters.Clear()

        Dim sbQry As New StringBuilder
        sbQry.Append("DELETE FROM MULTI_DISC WHERE ")

        Dim cnt As Integer = 0
        For Each objRemoveDisc As RemoveDiscountDtc In lstDiscToRemove

            If cnt > 0 Then
                sbQry.Append(" OR ")
            End If
            sbQry.Append("(")
            sbQry.Append(" LINE_ID = :line_id" & cnt.ToString())
            sbQry.Append(" AND SEQ_NUM = :seq_num" & cnt.ToString())
            sbQry.Append(")")
            dbCommand.Parameters.Add(":line_id" & cnt.ToString(), OracleType.VarChar)
            dbCommand.Parameters(":line_id" & cnt.ToString()).Value = objRemoveDisc.lnSeqNum
            dbCommand.Parameters.Add(":seq_num" & cnt.ToString(), OracleType.Number)
            dbCommand.Parameters(":seq_num" & cnt.ToString()).Value = objRemoveDisc.seqNum

            cnt = cnt + 1
        Next

        dbCommand.CommandText = sbQry.ToString()
        dbCommand.ExecuteNonQuery()
    End Sub

    Private Function GetDiscountsToReapply(ByVal sessionId As String, ByVal seqNum As Integer) As DataTable

        Dim dtDiscounts As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT md.DISC_CD, md.DISC_LEVEL, d.APPLY_PACKAGE, ")
        dbSql.Append("       DECODE(NVL(md.DISC_PCT,0), 0, 'A', 'P') AS APPLIED_BY ")
        dbSql.Append("FROM multi_disc md, disc d ")
        dbSql.Append("WHERE md.DISC_CD = d.DISC_CD AND session_id = :sessionId AND SEQ_NUM = :seqNum  ")
        dbSql.Append("ORDER BY APPLIED_BY, DISC_CD ")
        Try
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.Parameters.Add(":seqNum", OracleType.Number)
            dbCommand.Parameters(":seqNum").Value = seqNum

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dtDiscounts)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtDiscounts
    End Function

    Private Function GetDiscountedLines(ByVal sessionId As String, ByVal seqNum As Integer) As DataTable

        Dim dtLnsDiscounted As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        '==== Since all the discounts are being applied again, restores original price (on unit_prc) to start over
        dbSql.Append("SELECT t.ROW_ID as DEL_DOC_LN#, t.ITM_CD, t.DES, NVL(t.QTY, 0) AS QTY, NVL(t.ORIG_PRC, 0) AS ORIG_PRC, ")
        dbSql.Append("       NVL(t.MANUAL_PRC, 0) AS UNIT_PRC, 0 AS TOTAL_DISC_AMT, NVL(i.REPL_CST, 0) AS REPL_CST, ")
        dbSql.Append("       NVL(i.PU_DISC_PCNT, 0) AS PU_DISC_PCNT, i.MNR_CD, t.PACKAGE_PARENT as PKG_SOURCE, ")
        dbSql.Append("       md.DISC_CD, md.DISC_AMT, md.DISC_PCT, md.DISC_DESC, md.DISC_LEVEL, ")
        dbSql.Append("       DECODE(NVL(md.DISC_PCT,0), 0, 'A', 'P') AS APPLIED_BY,md.SEQ_NUM  ")
        dbSql.Append("FROM TEMP_ITM t, MULTI_DISC md, ITM i ")
        dbSql.Append("WHERE t.ITM_CD = i.ITM_CD AND t.SESSION_ID = md.SESSION_ID AND ")
        dbSql.Append("      md.LINE_ID = t.ROW_ID AND md.session_id = :sessionId AND md.SEQ_NUM = :seqNum ")
        dbSql.Append("ORDER BY APPLIED_BY, DISC_CD ")

        Try
            dbCommand.CommandText = dbSql.ToString
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.Parameters.Add(":seqNum", OracleType.VarChar)
            dbCommand.Parameters(":seqNum").Value = seqNum

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dtLnsDiscounted)
            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtLnsDiscounted
    End Function

    Private Sub InsertMultiDiscRow(ByRef dbCommand As OracleCommand,
                                       ByVal discount As DiscountDtc,
                                       ByVal discAmt As String,
                                       ByVal sessionId As String,
                                       ByVal lnSeqNum As String,
                                       ByVal seqNum As Integer)

        Dim sbSql As New StringBuilder()
        dbCommand.Parameters.Clear()
        dbCommand.CommandType = CommandType.Text
        dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
        dbCommand.Parameters(":SESSION_ID").Value = sessionId

        dbCommand.Parameters.Add(":LINE_ID", OracleType.VarChar)
        dbCommand.Parameters(":LINE_ID").Value = lnSeqNum

        dbCommand.Parameters.Add(":DISC_CD", OracleType.VarChar)
        dbCommand.Parameters(":DISC_CD").Value = discount.cd

        dbCommand.Parameters.Add(":DISC_DESC", OracleType.VarChar)
        dbCommand.Parameters(":DISC_DESC").Value = discount.desc

        dbCommand.Parameters.Add(":DISC_LEVEL", OracleType.VarChar)
        dbCommand.Parameters(":DISC_LEVEL").Value = discount.level

        dbCommand.Parameters.Add(":DISC_AMT", OracleType.VarChar)
        dbCommand.Parameters(":DISC_AMT").Value = discAmt

        dbCommand.Parameters.Add(":SEQ_NUM", OracleType.VarChar)
        dbCommand.Parameters(":SEQ_NUM").Value = seqNum

        sbSql.Append("INSERT INTO MULTI_DISC (SESSION_ID, LINE_ID, DISC_CD, DISC_DESC, DISC_LEVEL, DISC_AMT, SEQ_NUM ")

        If (AppConstants.Discount.TP_PCT = discount.tpCd) Then
            sbSql.Append(", DISC_PCT) VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT,:SEQ_NUM, :DISC_PCT )")

            dbCommand.Parameters.Add(":DISC_PCT", OracleType.Double)
            dbCommand.Parameters(":DISC_PCT").Value = discount.amt      'the % setup for this discount

        Else
            sbSql.Append(")           VALUES(:SESSION_ID, :LINE_ID, :DISC_CD, :DISC_DESC, :DISC_LEVEL, :DISC_AMT,:SEQ_NUM)")
        End If

        dbCommand.CommandText = sbSql.ToString()
        dbCommand.ExecuteNonQuery()
    End Sub

    Private Sub UpdateOrderLineDiscounts(ByVal sessionId As String, ByVal dtLnsAfterReapplyDisc As DataTable)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim amt As Double
        Dim retPrc As Double

        '=============================================================================================
        '====== The table being passed in, is a combination of the TEMP_ITM / MULTI_DISC tables ======
        '=== There could be multiple of the same line when the line has multiple discounts applied ===
        '=============================================================================================
        Try
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction

            dbSql.Append("UPDATE multi_disc SET disc_amt = :discAmt ")
            dbSql.Append("WHERE session_id = :sessId AND line_id = :lnSeqNum AND disc_cd = :discCd AND SEQ_NUM=:seqNum  ")

            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnSeqNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":discAmt", OracleType.Number)
            dbCommand.Parameters.Add(":seqNum", OracleType.Number)
            dbCommand.Parameters(":sessId").Value = sessionId

            For Each drLnDis As DataRow In dtLnsAfterReapplyDisc.Rows
                amt = If(IsNumeric(drLnDis("DISC_AMT")), CDbl(drLnDis("DISC_AMT")), 0.0)
                dbCommand.Parameters(":discAmt").Value = amt
                dbCommand.Parameters(":lnSeqNum").Value = drLnDis("DEL_DOC_LN#")
                dbCommand.Parameters(":discCd").Value = drLnDis("DISC_CD")
                dbCommand.Parameters(":seqNum").Value = drLnDis("SEQ_NUM")
                dbCommand.ExecuteNonQuery()
            Next

            dbCommand.Transaction.Commit()
        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    Private Sub UpdateOrderLinesRetPrice(ByVal sessionId As String)
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim amt As Double
        Dim retPrc As Double

        '=============================================================================================
        '====== The table being passed in, is a combination of the TEMP_ITM / MULTI_DISC tables ======
        '=== There could be multiple of the same line when the line has multiple discounts applied ===
        '=============================================================================================
        Try
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction

            '=== Updates the TEMP_ITM table
            dbSql.Append("select LINE_ID, sum(DISC_AMT) as discAmt from MULTI_DISC  where SESSION_ID=: sessId  group by LINE_ID ")
            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters(":sessId").Value = sessionId
            Dim dtReader As OracleDataReader
            dtReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            While dtReader.Read
                dbSql.Clear()
                dbCommand.Parameters.Clear()
                dbSql.Append("Update temp_itm set RET_PRC=CASE WHEN NVL(MANUAL_PRC,0) - :discAmt < 0.0 THEN 0.0 WHEN NVL(:discAmt,0) > MANUAL_PRC THEN MANUAL_PRC ELSE NVL(MANUAL_PRC,0) - :discAmt END ,DISC_AMT=:discAmt where ROW_ID=:lineID")
                dbCommand.CommandText = dbSql.ToString
                dbCommand.Parameters.Add(":discAmt", OracleType.Number)
                dbCommand.Parameters.Add(":lineID", OracleType.Number)
                dbCommand.Parameters(":discAmt").Value = dtReader("discAmt")
                dbCommand.Parameters(":lineID").Value = dtReader("LINE_ID")
                dbCommand.ExecuteNonQuery()
            End While
            dbCommand.Transaction.Commit()
        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

    End Sub
    Private Sub UpdateOrderLinesRetPrice(ByVal sessionId As String, ByRef dbCommand As OracleCommand, ByRef dbConnection As OracleConnection)
        'Dim dbConnection As OracleConnection = GetConnection()
        ' Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim lstLnSeq As New ArrayList()
        Dim strLnSeq As String = String.Empty
        Dim amt As Double
        Dim retPrc As Double

        '=============================================================================================
        '====== The table being passed in, is a combination of the TEMP_ITM / MULTI_DISC tables ======
        '=== There could be multiple of the same line when the line has multiple discounts applied ===
        '=============================================================================================
        Try
            ' dbConnection.Open()

            dbCommand.Parameters.Clear()
            dbCommand.Transaction = dbConnection.BeginTransaction

            '=== Updates the TEMP_ITM table
            dbSql.Append("select LINE_ID, sum(DISC_AMT) as discAmt from MULTI_DISC  where SESSION_ID=: sessId  group by LINE_ID ")
            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters(":sessId").Value = sessionId
            Dim dtReader As OracleDataReader
            dtReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            While dtReader.Read
                dbSql.Clear()
                dbCommand.Parameters.Clear()
                dbSql.Append("Update temp_itm set RET_PRC= CASE WHEN NVL(MANUAL_PRC,0) - :discAmt < 0.0 THEN 0.0 WHEN NVL(:discAmt,0) > MANUAL_PRC THEN MANUAL_PRC  ELSE NVL(MANUAL_PRC,0) - :discAmt END,DISC_AMT=:discAmt where ROW_ID=:lineID")
                dbCommand.CommandText = dbSql.ToString
                dbCommand.Parameters.Add(":discAmt", OracleType.Number)
                dbCommand.Parameters.Add(":lineID", OracleType.Number)
                dbCommand.Parameters(":discAmt").Value = dtReader("discAmt")
                dbCommand.Parameters(":lineID").Value = dtReader("LINE_ID")
                dbCommand.ExecuteNonQuery()
            End While
            dbCommand.Transaction.Commit()
        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw ex
        Finally
            ' CloseResources(dbConnection, dbCommand)
            dbCommand.Parameters.Clear()
        End Try

    End Sub

    Private Function GetDiscountSeqNum(ByRef dbCommand As OracleCommand, ByVal sessionId As String) As Integer
        Dim dtReader As OracleDataReader
        Dim dbSql As StringBuilder = New StringBuilder()
        Dim seqNum As Integer
        Try
            '=== Updates the TEMP_ITM table
            dbCommand.Parameters.Clear()
            dbSql.Append("select NVL(MAX(SEQ_NUM),0)+1  as SEQNUM FROM  MULTI_DISC where SESSION_ID=:sessId")
            dbCommand.CommandText = dbSql.ToString()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters(":sessId").Value = sessionId
            dtReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            While dtReader.Read
                seqNum = dtReader("SEQNUM")
            End While
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return seqNum
    End Function
    Public Sub ReapplyDiscountsForSOE(ByVal sessionId As String, ByVal pkpDelFlag As String, ByVal seqNum As Integer)

        Dim dtLnsDiscounted As DataTable
        Dim dtDiscounts As DataTable

        '=== Recalculates ALL discounts found in the SOE order represented by the sessionID passed in
        dtDiscounts = GetDiscountsToReapply(sessionId, seqNum)
        If (dtDiscounts.Rows.Count > 0) Then
            dtLnsDiscounted = GetDiscountedLines(sessionId, seqNum)
            'MM-9484
            dtLnsDiscounted = ReapplyDiscounts(dtLnsDiscounted, dtDiscounts, pkpDelFlag, False, True)

            '=== Saves to the TEMP_ITM and MULTI_DISC tables the updates made
            UpdateOrderLineDiscounts(sessionId, dtLnsDiscounted)
        End If
        UpdateOrderLinesRetPrice(sessionId)
    End Sub
End Class
