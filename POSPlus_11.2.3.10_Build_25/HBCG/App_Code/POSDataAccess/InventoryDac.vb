﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports System.Text

Public Class InventoryDac
    Inherits BaseDac

    Private theTMDac As TransportationDac

    ''' <summary>
    ''' Extracts the WAREHOUSE inventory details for a SKU; if priority fill enabled, will use 
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve inventory details for</param>
    ''' <param name="puDel">indicates if the item is placed on a Pickup or Delivery</param>
    ''' <param name="priFillZoneCd">the zone code for P/D.  Required when priority fill by ZONE is used.</param>
    ''' <param name="priFillStoreCd">the store to be used.  Required when priority fill by STORE is used.</param>
    ''' <param name="takenWith">indicates if the merchandise was taken by the customer; Def: 'N'o.</param>
    ''' <returns>dataset of warehouse inventory details</returns>
    Public Function GetInvPriFill(ByVal itmCd As String, ByVal puDel As String,
                                  ByVal priFillZoneCd As String, ByVal priFillStoreCd As String,
                                  ByVal takenWith As String) As DataSet

        Dim invDataSet As New DataSet
        Dim sql As String = "bt_inv.get_item_whs_inv"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure
            dbCommand.Parameters.Add("inv_curtype", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_flg_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input


            dbCommand.Parameters("itm_cd_i").Value = itmCd
            dbCommand.Parameters("pu_del_i").Value = puDel
            dbCommand.Parameters("pri_fill_store_cd_i").Value = priFillStoreCd
            dbCommand.Parameters("pri_fill_zone_cd_i").Value = priFillZoneCd
            dbCommand.Parameters("store_cd_i").Value = System.DBNull.Value
            dbCommand.Parameters("taken_with_i").Value = takenWith

            'Indicator flag (Y/N) for pririty fill logic. When this flag is 'N' it will not
            'use priority fill logic regardless of the system parameters
            dbCommand.Parameters("pri_fill_flg_i").Value = "Y"

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(invDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return invDataSet

    End Function

    ''' <summary>
    ''' Gets the Lead days defined for the ITEM passed in.
    ''' </summary>
    ''' <param name="itemCode">the item code to get the lead days for</param>
    ''' <param name="storeCode">store to be used when retrieving the lead days</param>
    ''' <returns>number of lead days for the item and store passed in</returns>
    Public Function GetItemLeadDays(ByVal itemCode, ByVal storeCode) As Integer

        Dim leadDays As Integer = 0
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As String = "SELECT bt_item_avail.get_lead_days(:itm_cd_i,:store_cd_i) as leadDays from dual"

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(dbSql)
            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar)
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar)

            dbCommand.Parameters("itm_cd_i").Value = itemCode
            dbCommand.Parameters("store_cd_i").Value = storeCode

            Dim result As Object = dbCommand.ExecuteScalar()
            If (Not IsNothing(result)) Then
                leadDays = CInt(result)
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return leadDays
    End Function

    ''' <summary>
    ''' Gets the Lead days defined for the zone passed in.
    ''' </summary>
    ''' <param name="zoneCd">the zone code to get the lead days for</param>
    ''' <returns>number of lead days for the zone passed in</returns>
    Public Function GetZoneLeadDays(ByVal zoneCd As String) As Integer

        Dim leadDays As Integer = 0

        If zoneCd.isNotEmpty Then

            Dim dbConn As OracleConnection = GetConnection()
            Dim dbCmd As OracleCommand = GetCommand("", dbConn)
            Dim dbSql As String = "SELECT bt_adv_res.get_zone_lead_days(:zone_cd_i) AS leadDays FROM dual"

            Try
                dbConn.Open()
                dbCmd.CommandText = UCase(dbSql)
                dbCmd.Parameters.Add("zone_cd_i", OracleType.VarChar)
                dbCmd.Parameters("zone_cd_i").Value = zoneCd

                Dim result As Object = dbCmd.ExecuteScalar()
                If (Not IsNothing(result)) Then
                    leadDays = CInt(result)
                End If
            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConn, dbCmd)
            End Try
        End If

        Return leadDays
    End Function

    ''' <summary>
    ''' Extracts the SHOWROOM inventory details for a SKU; if priority fill enabled, will use 
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve inventory details for</param>
    ''' <param name="puDel">indicates if the item is placed on a Pickup or Delivery</param>
    ''' <param name="priFillZoneCd">the zone code for P/D.  Required when priority fill by ZONE is used.</param>
    ''' <param name="priFillStoreCd">the store to be used.  Required when priority fill by STORE is used.</param>
    ''' <param name="takenWith">indicates if the merchandise was taken by the customer; Def: 'N'o.</param>
    ''' <returns>dataset of showroom inventory details</returns>
    Public Function GetShwPriFill(ByVal itmCd As String, ByVal puDel As String,
                                  ByVal priFillZoneCd As String, ByVal priFillStoreCd As String,
                                  ByVal takenWith As String) As DataSet

        Dim invDataSet As New DataSet
        Dim sql As String = "bt_inv.get_item_shw_inv"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure
            dbCommand.Parameters.Add("inv_curtype", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pri_fill_flg_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input


            dbCommand.Parameters("itm_cd_i").Value = itmCd
            dbCommand.Parameters("pu_del_i").Value = puDel
            dbCommand.Parameters("pri_fill_store_cd_i").Value = priFillStoreCd
            dbCommand.Parameters("pri_fill_zone_cd_i").Value = priFillZoneCd
            dbCommand.Parameters("store_cd_i").Value = System.DBNull.Value
            dbCommand.Parameters("taken_with_i").Value = takenWith

            'Indicator flag (Y/N) for pririty fill logic. When this flag is 'N' it will not
            'use priority fill logic regardless of the system parameters
            dbCommand.Parameters("pri_fill_flg_i").Value = "Y"

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(invDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return invDataSet

    End Function

    ''' <summary>
    ''' Retrieves the inventory found in the ITM_FIFL table for the item and 
    ''' location type passed id.
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve inventory for</param>
    ''' <param name="locTpCd">refer to LOC.LOC_TP_CD for valid types</param>
    ''' <returns>a dataset including the inventory details</returns>
    Public Function GetStoreData(ByVal itmCd As String, ByVal locTpCd As String) As DataSet

        Dim invDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT ITM_FIFL.STORE_CD, ITM_FIFL.LOC_CD, NVL(ITM_FIFL.QTY,0) QTY ")
        sql.Append("FROM ITM_FIFL, LOC ")
        sql.Append("WHERE ITM_FIFL.ITM_CD = :itmCd ")
        sql.Append("AND LOC.LOC_CD=ITM_FIFL.LOC_CD AND LOC.LOC_TP_CD = :locTpCd ")
        sql.Append("AND LOC.STORE_CD=ITM_FIFL.STORE_CD ")
        sql.Append("ORDER BY ITM_FIFL.STORE_CD, ITM_FIFL.LOC_CD")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locTpCd", OracleType.VarChar)

            dbCommand.Parameters(":itmCd").Value = itmCd
            dbCommand.Parameters(":locTpCd").Value = locTpCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(invDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return invDataSet

    End Function

    ''' <summary>
    ''' Retrieves from the SO and SO_LN tables, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve SALES records for</param>
    ''' <returns>A dataset including the SALES records found; or an empty dataset if none found</returns>
    Public Function GetSalesData(ByVal itmCd As String) As DataSet

        Dim salesRecords As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT SO.DEL_DOC_NUM, SO_LN.QTY, SO_LN.STORE_CD, SO_LN.LOC_CD, TO_CHAR(SO.PU_DEL_DT, 'MM/DD/RRRR') AS PU_DEL_DT, SO.SHIP_TO_L_NAME ")
        sql.Append(" FROM SO, SO_LN ")
        sql.Append(" WHERE SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM AND SO_LN.STORE_CD IS NULL AND SO_LN.VOID_FLAG='N' ")
        sql.Append("   AND SO.STAT_CD='O' AND SO.ORD_TP_CD='SAL' AND SO_LN.ITM_CD = :itmCd ")
        sql.Append(" ORDER BY SO.DEL_DOC_NUM ASC ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(salesRecords)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return salesRecords

    End Function

    ''' <summary>
    ''' Retrieves from the PO and related tables, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve PO records for</param>
    ''' <returns>A dataset including the PO records found; or an empty dataset if none found</returns>
    Public Function GetPoData(ByVal itmCd As String) As DataSet

        Dim poRecords As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT   PO_CD,")
        sql.Append("         LN#,")
        sql.Append("         TOTAL_INCOMING,")
        sql.Append("         ARRIVAL_DT,")
        sql.Append("         DEST_STORE_CD")
        sql.Append("  FROM   (SELECT   PL.PO_CD,")
        sql.Append("                   PL.LN#,")
        sql.Append("                   (NVL (PL.QTY_ORD, 0)")
        sql.Append("                    - (SELECT   NVL (SUM (QTY), 0)")
        sql.Append("                         FROM   PO_LN2SO_IST_V PSI")
        sql.Append("                        WHERE       PSI.PO_CD = PL.PO_CD")
        sql.Append("                                AND PSI.PO_LN# = PL.LN#")
        sql.Append("                                AND PSI.ITM_CD = PL.ITM_CD")
        sql.Append("                                AND PSI.STORE_CD IS NULL")
        sql.Append("                                AND PSI.LOC_CD IS NULL)")
        sql.Append("                    - (SELECT   NVL (SUM (NVL (PA1.QTY, 0)), 0)")
        sql.Append("                         FROM   PO_LN$ACTN_HST PA1")
        sql.Append("                        WHERE       PA1.PO_CD = PL.PO_CD")
        sql.Append("                                AND PA1.LN# = PL.LN#")
        sql.Append("                                AND PA1.PO_ACTN_TP_CD = 'RCV'))")
        sql.Append("                      TOTAL_INCOMING,")
        sql.Append("                   TO_CHAR (PL.ARRIVAL_DT, 'MM/DD/RRRR') AS ARRIVAL_DT,")
        sql.Append("                   PL.DEST_STORE_CD")
        sql.Append("            FROM      PO")
        sql.Append("                   LEFT JOIN")
        sql.Append("                      PO_LN PL")
        sql.Append("                   ON PO.PO_CD = PL.PO_CD")
        sql.Append("           WHERE   PO.STAT_CD = 'O' AND PL.ITM_CD = :itmCd)")
        sql.Append(" WHERE   TOTAL_INCOMING > 0")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(poRecords)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return poRecords

    End Function

    ''' <summary>
    ''' Retrieves from the OUT table, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve outlet records for</param>
    ''' <returns>A dataset including the outlet records found; or an empty dataset if none found</returns>
    Public Function GetOutletData(ByVal itmCd As String) As DataSet

        Dim outletRecords As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append("SELECT ID_CD, STORE_CD, LOC_CD, OUT_CD, QTY, CST, NVL(spiff,0) AS SPIFF, crpt_ser_num AS SER_NUM, FIFL_DT, FIFO_DT, OUT_COMM_CD ")
        sql.Append("FROM OUT WHERE ITM_CD = :itmCd ORDER BY OUT_CD, ID_CD")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(outletRecords)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return outletRecords

    End Function

    ''' <summary>
    ''' Retrieves from the IST table, all records that match the item code passed in
    ''' </summary>
    ''' <param name="itmCd">the item code to retrieve IST records for</param>
    ''' <returns>A dataset including the IST records found; or an empty dataset if none found</returns>
    Public Function GetIstData(ByVal itmCd As String) As DataSet

        Dim istRecords As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT a.IST_WR_DT, TO_CHAR(a.TRANSFER_DT, 'MM/DD/RRRR') AS TRANSFER_DT, a.DEST_STORE_CD, ")
        sql.Append("        a.SRT_CD, a.FINAL_DT, a.STAT_CD, a.DOC_NUM, a.TRANSFER_DT, a.ZONE_CD, b.LN#, b.ITM_CD, ")
        sql.Append("        b.OLD_STORE_CD, b.OLD_LOC_CD, b.QTY, b.CRPT_ID_NUM, b.UNIT_CST, b.VOID_FLAG, ")
        sql.Append("        b.NEW_STORE_CD, b.NEW_LOC_CD, b.CRPT_ID_NUM ")
        sql.Append(" FROM IST a, IST_LN b ")
        sql.Append(" WHERE a.IST_SEQ_NUM = b.IST_SEQ_NUM AND a.IST_STORE_CD = b.IST_STORE_CD AND a.IST_WR_DT = b.IST_WR_DT ")
        sql.Append("       AND b.DEST_FLAG='W' AND a.STAT_CD='O' AND b.OLD_STORE_CD IS NOT NULL AND b.ITM_CD = :itmCd ")
        sql.Append(" ORDER BY a.DEST_STORE_CD ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(istRecords)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return istRecords

    End Function

    ''' <summary>
    ''' Finds out if the location passed in is valid, for the corresponding store
    ''' </summary>
    ''' <param name="storeCd">the store code to check the location code against</param>
    ''' <param name="locCd">the location code to validate</param>
    ''' <returns>TRUE if location is a valid one for the store; FALSE otherwise</returns>
    Public Function IsValidLocation(ByVal storeCd As String, locCd As String) As Boolean

        Dim isValid As Boolean = False
        Dim sql As String = "SELECT STORE_CD, LOC_CD FROM LOC WHERE STORE_CD = :storeCd AND LOC_CD = :locCd"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)

            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbCommand.Parameters(":locCd").Value = UCase(locCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                isValid = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isValid

    End Function


    ''' <summary>
    ''' Determines if the PO has been confirmed and the confirmation status is open
    ''' </summary>
    ''' <param name="poCd">the PO number to be verify</param>
    ''' <returns>TRUE if PO has been confirmed and confirmation status is open; FALSE otherwise</returns>
    Public Function IsPOConfirmed(ByVal poCd As String) As Boolean

        Dim isConfirmed As Boolean = False
        Dim sql As String = "SELECT 1 FROM rcv_conf rc, rcv_info ri WHERE rc.conf_num = ri.conf_num AND rc.stat_cd = 'O' AND ri.po_cd = :poCd "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":poCd", OracleType.VarChar)
            dbCommand.Parameters(":poCd").Value = UCase(poCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                isConfirmed = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isConfirmed

    End Function

    ''' <summary>
    ''' Retrieves ALL records from the ITM_RES table, that match the item cd
    ''' passed in.   Returns an empty dataset if no matches are found
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve the soft-reservations for</param>
    ''' <returns>A dataset filled with the soft-reservations details</returns>
    Public Function GetReservations(ByVal itmCd As String) As DataSet

        Dim reservations As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append(" SELECT i.itm_cd, i.store_cd, i.loc_cd, i.qty, l.loc_tp_cd locTp ")
        sql.Append(" FROM itm_res i, loc l ")
        sql.Append(" WHERE i.itm_cd = :itmCd AND i.store_cd = l.store_cd AND i.loc_cd = l.loc_cd ")
        sql.Append(" ORDER BY l.loc_tp_cd ASC ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(reservations)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return reservations

    End Function

    ''' <summary>
    ''' Retrieves from the INV_XREF table, the serial numbers that match the ITEM/STORE/LOCATION
    ''' passed in, excluding the ones in the list passed in.
    ''' </summary>
    ''' <param name="itemCd">item code to fetch the serial numbers for</param>
    ''' <param name="storeCd">store code to fetch the serial numbers for</param>
    ''' <param name="locCd">location code to fetch the serial numbers for</param>
    ''' <param name="serialNumbersToExclude">comma delimited list of serial numbers to exclude during retrieval</param>
    ''' <returns>A dataset including the serial numbers found; or an empty dataset if none found</returns>
    Public Function GetSerialNumbersAvailable(ByVal itemCd As String,
                                              ByVal storeCd As String,
                                              ByVal locCd As String,
                                              ByVal serialNumbersToExclude As String) As DataSet
        Dim serialNumbers As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT ser_num FROM inv_xref ")
        sql.Append("WHERE ITM_CD = :itmCd AND STORE_CD = :storeCd AND LOC_CD = :locCd AND disposition='FIF' ")
        If (Not String.IsNullOrWhiteSpace(serialNumbersToExclude)) Then
            sql.Append(" AND ser_num NOT IN(").Append(serialNumbersToExclude).Append(") ")
        End If
        sql.Append("ORDER BY ser_num")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)

            dbCommand.Parameters(":itmCd").Value = itemCd
            dbCommand.Parameters(":storeCd").Value = storeCd
            dbCommand.Parameters(":locCd").Value = locCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(serialNumbers)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return serialNumbers

    End Function

    ''' <summary>
    ''' Retrieves from the OUT table, the outlet IDs that match the ITEM/STORE/LOCATION
    ''' passed in, excluding the ones in the list passed in.
    ''' </summary>
    ''' <param name="itemCd">item code to fetch the outlet IDs for</param>
    ''' <param name="storeCd">store code to fetch the outlet IDs for</param>
    ''' <param name="locCd">location code to fetch the outlet IDs for</param>
    ''' <param name="outletIdsToExclude">comma delimited list of outlet IDs to exclude during retrieval</param>
    ''' <returns>A dataset including the outlet IDs found; or an empty dataset if none found</returns>
    Public Function GetOutletIdsAvailable(ByVal itemCd As String,
                                          ByVal storeCd As String,
                                          ByVal locCd As String,
                                          ByVal outletIdsToExclude As String) As DataSet
        Dim outletIds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("SELECT id_cd, out_cd, out_comm_cd, crpt_ser_num AS SER_NUM, NVL(spiff,0) AS SPIFF FROM out ")
        sql.Append("WHERE ITM_CD = :itmCd AND STORE_CD = :storeCd AND LOC_CD = :locCd ")
        If (Not String.IsNullOrWhiteSpace(outletIdsToExclude)) Then
            sql.Append(" AND id_cd NOT IN(").Append(outletIdsToExclude).Append(") ")
        End If
        sql.Append("ORDER BY id_cd")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)

            dbCommand.Parameters(":itmCd").Value = itemCd
            dbCommand.Parameters(":storeCd").Value = storeCd
            dbCommand.Parameters(":locCd").Value = locCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(outletIds)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return outletIds

    End Function

    ''' <summary>
    ''' Retrieves from the OUT_CD table the outlet codes and descriptions
    ''' </summary>
    ''' <returns>a dataaset including the outlet codes and descriptions</returns>
    Public Function GetOutletCodes() As DataSet

        Dim outletCodes As New DataSet
        Dim sql As String = "SELECT out_cd, des as OUT_DES FROM out_cd ORDER BY out_cd "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(outletCodes)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return outletCodes

    End Function

    ''' <summary>
    ''' Finds out if the store and location passed in, correspond to an 'O'utlet type location
    ''' </summary>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="locCd">the location code</param>
    ''' <returns>TRUE if location is Outlet; FALSE otherwise</returns>
    Public Function IsOutletLocation(ByVal storeCd As String, ByVal locCd As String) As Boolean

        Dim isOutlet As Boolean = False
        Dim sql As String = "SELECT loc_tp_cd FROM loc WHERE STORE_CD = :storeCd AND LOC_CD = :locCd"
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)

            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbCommand.Parameters(":locCd").Value = UCase(locCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                isOutlet = "O".Equals(dbReader.Item("LOC_TP_CD"))
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isOutlet

    End Function

    ''' <summary>
    ''' Finds out if the store and location passed in, correspond to an 'O'utlet type location
    ''' </summary>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="locCd">the location code</param>
    ''' <param name="dbCommand">command object</param>
    ''' <returns>TRUE if location is Outlet; FALSE otherwise</returns>
    ''' <summary>
    Public Function IsOutletLocation(ByVal storeCd As String, ByVal locCd As String, ByRef dbCommand As OracleCommand) As Boolean
        Dim sql As String = "SELECT LOC_TP_CD FROM LOC WHERE STORE_CD = :storeCd AND LOC_CD = :locCd"
        Dim dbReader As OracleDataReader
        dbCommand.Parameters.Clear()
        dbCommand.CommandType = CommandType.Text
        dbCommand.CommandText = sql

        Try
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)

            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbCommand.Parameters(":locCd").Value = UCase(locCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                IsOutletLocation = "O".Equals(dbReader.Item("LOC_TP_CD"))
            End If
        Catch ex As Exception
            Throw
        Finally
            dbReader.Close()
        End Try
        Return IsOutletLocation

    End Function


    ''' <summary>
    ''' 'Returns true if the store input is an RF warehouse, false otherwise
    ''' </summary>
    ''' <returns>true if the store is RF enabled, false otherwise</returns>
    Public Function IsRfStore(ByVal storeCd As String) As Boolean

        Dim isEnabled As Boolean = False
        Dim sql As String = "SELECT NVL(RF_WHSE,'N') AS RF_WHSE FROM STORE WHERE STORE_CD = :storeCd "
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                isEnabled = StringUtils.ResolveYNToBoolean(dbReader.Item("RF_WHSE").ToString.ToUpper)
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return isEnabled
    End Function

    ''' <summary>
    ''' Returns Picking Status flag from RF.  Validates that RF is enabled and the input store is an RF warehouse
    ''' Extracts non-void RF_PICK records for the doc and line and SKU.  
    ''' </summary>
    ''' <param name="docNum">the document number where the item belongs to</param>
    ''' <param name="lnNum">the line number where the item belongs to</param>
    ''' <param name="itemCd">the item code to get the RF status for</param>
    ''' <param name="storeCd">the store code to be used</param>
    ''' <returns>
    ''' If all records have a checked status then return status output will be Checked.
    ''' If all records have a (not blank) status but at least one has an exception status, then the return status will be Exception.
    ''' If all records have a (not blank) status and no exceptions (and not all checked), then the return status will be Picked.
    ''' If not all records have a (not blank) status then the return status will be Scheduled.
    ''' If there are no RF pick records for the doc and line and SKU, then the return status will be blank. 
    ''' </returns>
    Public Function GetRfPickStatus(ByVal docNum As String, ByVal lnNum As String,
                                    ByVal itmCd As String, ByVal storeCd As String) As String

        Dim pickStatus As String = ""
        Dim check, pick, exception, scheduled As String
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append("SELECT NVL(SUM(DECODE(STAT_CD,'CHK',1,0)),0) AS CHK, ").Append(
                          "NVL(SUM(DECODE(STAT_CD,'PCK',1,0)),0) AS PCK, ").Append(
                          "NVL(SUM(DECODE(STAT_CD,'EXC',1,0)),0) AS EXC, ").Append(
                          "NVL(SUM(DECODE(STAT_CD,'PCK',0,'CHK',0,'EXC',0,1)),0) AS SCHED ").Append(
                    " FROM(RF_PICK) ").Append(
                    " WHERE DOC_NUM = :docNum AND LN# = :lnNum AND ITM_CD = :itmCd AND NVL(VOID_FLAG,'N') != 'Y' ")
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString

            dbCommand.Parameters.Add(":docNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)

            dbCommand.Parameters(":docNum").Value = UCase(docNum)
            dbCommand.Parameters(":lnNum").Value = CInt(lnNum)
            dbCommand.Parameters(":itmCd").Value = UCase(itmCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                check = dbReader.Item("CHK")
                pick = dbReader.Item("PCK")
                exception = dbReader.Item("EXC")
                scheduled = dbReader.Item("SCHED")
            End If

            ' don't need to compare line qty here as E1 SOM does not; 
            ' seems like it does not really reflect status but matches E1 SOM???  
            If scheduled = 0 AndAlso (pick + check + exception > 0) Then
                If pick = 0 AndAlso exception = 0 Then
                    pickStatus = AppConstants.Inv.PICK_STATUS_CHECKED       'all records = check
                ElseIf exception > 0 Then
                    pickStatus = AppConstants.Inv.PICK_STATUS_EXCEPTION     'all records = exception
                Else
                    pickStatus = AppConstants.Inv.PICK_STATUS_PICKED        'all records = pick or check
                End If
            ElseIf scheduled > 0 OrElse pick > 0 OrElse exception > 0 OrElse check > 0 Then
                pickStatus = AppConstants.Inv.PICK_STATUS_SCHEDULED         'at least one rec not pck/chk
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return pickStatus

    End Function

    ''' <summary>
    ''' Creates a SOFT reservation. When inventory is available, a record is added to 
    ''' the ITEM_RES table indicating the store-location-quantity reserved.
    ''' Further soft-reservations will consider these reservations as non-available 
    ''' inventory until these rows are removed, either because a 'hard' reservation 
    ''' gets created, or line reserved is unreserved
    ''' </summary>
    ''' <param name="request">custom object with details to perform a soft reservation</param>
    ''' <returns>custom object with the results of the reservation</returns>
    Public Function RequestDirectedSoftRes(ByVal request As ResRequestDtc) As ResResponseDtc

        Dim response As ResResponseDtc = New ResResponseDtc
        Dim sql As String = "bt_iface_inv.directed_soft_res"
        Dim dbAdapter As OracleDataAdapter
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim reservations As DataSet = New DataSet

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure

            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("loc_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("qty_order_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("group_qty_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("res_id_o", OracleType.VarChar, AppConstants.Inv.ITM_RES_RES_ID_LEN).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("soft_res_cur_o", OracleType.Cursor).Direction = ParameterDirection.Output

            'NOTE: loc_tp_cd_i  needs to be either W, S, O  for Warehouse/Showroom/Outlet respectively
            'however in the client there is no provision to capture the type at this point

            dbCommand.Parameters("itm_cd_i").Value = request.itemCd
            dbCommand.Parameters("store_cd_i").Value = request.storeCd
            dbCommand.Parameters("loc_cd_i").Value = request.locCd
            dbCommand.Parameters("loc_tp_cd_i").Value = DBNull.Value
            dbCommand.Parameters("qty_order_i").Value = request.qty
            dbCommand.Parameters("pu_del_i").Value = request.puDel
            dbCommand.Parameters("group_qty_i").Value = "Y"

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(reservations)
            response.resId = dbCommand.Parameters("res_id_o").Value.ToString
            response.reservations = reservations.Tables(0)
        Catch ex As Exception
            Throw
        Finally
            dbAdapter.Dispose()
            CloseResources(dbConnection, dbCommand)
        End Try
        Return response

    End Function

    ''' <summary>
    ''' Creates SOFT reservations.  When inventory is available, one record is added to 
    ''' the ITEM_RES table for every different store-location-quantity combination.  
    ''' Further auto-reservations will consider these reservations as non-available inventory
    ''' until these rows are removed either because a 'hard' reservation gets created, 
    ''' or line reserved is unreserved
    ''' </summary>
    ''' <param name="resRequests">list of custom objects with details to perform a soft reservation</param>
    ''' <returns>list of custom object with the results of the reservation</returns>
    Public Function RequestAutoSoftRes(ByVal resRequests As List(Of ResRequestDtc)) As List(Of ResResponseDtc)

        Dim results As New List(Of ResResponseDtc)
        Dim dbConnection As OracleConnection = GetConnection()
        Try
            dbConnection.Open()
            For Each request As ResRequestDtc In resRequests
                results.Add(RequestAutoSoftRes(dbConnection, request))
            Next
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection)
        End Try
        Return results

    End Function

    ''' <summary>
    ''' Creates a SOFT reservation.  When inventory is available, one record is added to 
    ''' the ITEM_RES table for every different store-location-quantity combination.  
    ''' Further auto-reservations will consider these reservations as non-available inventory
    ''' until these rows are removed either because a 'hard' reservation gets created, 
    ''' or line reserved is unreserved
    ''' </summary>
    ''' <param name="request">custom object with details to perform a soft reservation</param>
    ''' <returns>custom object with the results of the reservation</returns>
    Public Function RequestAutoSoftRes(ByVal request As ResRequestDtc) As ResResponseDtc
        Dim response As ResResponseDtc
        Dim dbConnection As OracleConnection = GetConnection()
        Try
            dbConnection.Open()
            response = RequestAutoSoftRes(dbConnection, request)
        Catch oracleException As OracleException
            If (Not IsNothing(HttpContext.Current.Session("SaveSalesOrder"))) AndAlso (HttpContext.Current.Session("SaveSalesOrder")) = True Then
                ' When user is trying to save the sales order then exception need not to be thrown
                ' Do Nothing
            Else
                'If user is Reserving an Inventory then exception needs to be thrown
                Throw
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection)
        End Try
        Return response
    End Function

    ''' <summary>
    ''' Removes the ITM_RES records that match the reservation ids passed in
    ''' </summary>
    ''' <param name="reservationIds">a list of reservation ids to remove records for</param>
    ''' <remarks>this should be used when voiding/saving an order</remarks>
    Public Sub RemoveSoftRes(ByVal reservationIds As List(Of String))

        Dim dbConnection As OracleConnection = GetConnection()
        Try
            dbConnection.Open()
            For Each resId As String In reservationIds
                RemoveSoftRes(dbConnection, resId, String.Empty, 0, String.Empty, String.Empty, String.Empty)
            Next
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection)
        End Try

    End Sub

    ''' <summary>
    ''' Removes the ITM_RES records that match the details passed in
    ''' </summary>
    ''' <param name="request">details require to remove a soft reservation</param>
    Public Sub RemoveSoftRes(ByVal request As UnResRequestDtc)

        Dim dbConnection As OracleConnection = GetConnection()
        Try
            dbConnection.Open()
            RemoveSoftRes(dbConnection,
                          request.resId, request.itemCd,
                          request.qty, request.storeCd,
                          request.locCd, request.locTpCd)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection)
        End Try
    End Sub

    ''' <summary>
    ''' Calls an API to calculate availability for the specified item.
    ''' </summary>
    ''' <param name="request">details required to calculate availability</param>
    ''' <returns>object with the results of the API call</returns>
    Public Function CalcAvail(ByVal request As CalcAvailRequestDtc) As CalcAvailResponseDtc

        Dim response As CalcAvailResponseDtc = New CalcAvailResponseDtc()
        Dim commentOut As String = ""
        Dim sql As String = "bt_iface_item_avail.get_item_avail_info"
        Dim calcAvailData As New DataSet
        Dim dbAdapter As OracleDataAdapter
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        dbCommand.CommandType = CommandType.StoredProcedure

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("chk_alt_id_flg_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("needed_qty_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("needed_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("wr_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pd_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("tran_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_flg_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("del_doc_ln_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("chk_spec_ord_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("res_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("fill_store_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("id_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cmnt_o", OracleType.VarChar, 80).Direction = ParameterDirection.Output  ' currently in E1 max is 45
            dbCommand.Parameters.Add("itm_avl_curtype", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters("itm_cd_i").Value = request.itemCd
            dbCommand.Parameters("chk_alt_id_flg_i").Value = "N"
            dbCommand.Parameters("needed_qty_i").Value = request.qty
            dbCommand.Parameters("needed_dt_i").Value = request.reqDate
            dbCommand.Parameters("wr_store_cd_i").Value = request.storeCd
            dbCommand.Parameters("pd_store_cd_i").Value = request.puDelStoreCd
            dbCommand.Parameters("zone_cd_i").Value = request.zoneCd
            dbCommand.Parameters("tran_dt_i").Value = Today.Date
            dbCommand.Parameters("pu_del_flg_i").Value = request.puDel
            dbCommand.Parameters("del_doc_num_i").Value = request.delDocNum
            dbCommand.Parameters("del_doc_ln_i").Value = request.delDocLnNum
            dbCommand.Parameters("chk_spec_ord_flag_i").Value = "Y"
            dbCommand.Parameters("res_num_i").Value = System.DBNull.Value
            dbCommand.Parameters("fill_store_i").Value = System.DBNull.Value
            dbCommand.Parameters("id_num_i").Value = request.combId

            dbCommand.ExecuteNonQuery()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(calcAvailData)

            response.commentOut = dbCommand.Parameters("cmnt_o").Value.ToString
            response.calcAvailData = calcAvailData.Tables(0)

            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return response

    End Function

    ''' <summary>
    ''' Calls an API to calculate availability for the specified item. This applies when the 
    ''' store that has been setup to use advanced reservation logic (ARS). 
    ''' </summary>
    ''' <param name="itmCd">the item being checked for availability</param>
    ''' <param name="pickupDelFlag">Indicates whether it is a 'P'ickup or 'D'elivery</param>
    ''' <param name="zoneCd">the zone of the pickup store or delivery postal code</param>
    ''' <param name="qty">the qty needed</param>
    ''' <param name="storeCd">the pickup/delivery store</param>
    ''' <returns>the available date for the specified item</returns>
    Public Function CalcAvailARS(itmCd As String, pickupDelFlag As String,
                                 zoneCd As String, qty As Double, storeCd As String) As Date

        Dim availDate As Date = Nothing
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand


        dbConnection.Open()
        dbCommand = GetCommand("bt_adv_res.get_available_date", dbConnection)
        dbCommand.CommandType = CommandType.StoredProcedure

        Try
            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("avail_dt_o", OracleType.DateTime).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters("itm_cd_i").Value = itmCd
            dbCommand.Parameters("pu_del_i").Value = pickupDelFlag
            dbCommand.Parameters("zone_cd_i").Value = IIf(zoneCd Is Nothing, "", zoneCd)
            dbCommand.Parameters("qty_i").Value = qty
            dbCommand.Parameters("store_cd_i").Value = storeCd

            dbCommand.ExecuteNonQuery()
            availDate = dbCommand.Parameters("avail_dt_o").Value

            dbCommand.Parameters.Clear()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return availDate

    End Function

    ''' <summary>
    ''' Validates the inventory for an ARS enabled store as per the business rules defined in the API. The API checks things
    ''' such as the delv/pickup date being valid, or if based on current inventory, the line needs to be linked to
    ''' a PO, or can be reserved, etc.
    ''' </summary>
    ''' <param name="request">the request object with all the data needed for checking inventory</param>
    ''' <returns>the check inventory response object</returns>
    Public Function CheckInventoryForARS(ByVal request As CheckInvRequestDtc) As CheckInvResponseDtc

        Dim response As CheckInvResponseDtc = New CheckInvResponseDtc()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("bt_adv_res.check_inventory", dbConnection)

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure
            dbCommand.Parameters.Add("paid_full_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_store_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("pu_del_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input

            dbCommand.Parameters.Add("msg_cd_o", OracleType.Number).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("msg_text_o", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("avbl_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("po_cd_o", OracleType.VarChar, 30).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("po_ln#_o", OracleType.Number).Direction = ParameterDirection.Output
            dbCommand.Parameters.Add("po_avbl_qty_o", OracleType.Number).Direction = ParameterDirection.Output

            dbCommand.Parameters("paid_full_i").Value = IIf(request.isPaidInFull, "Y", "N")
            dbCommand.Parameters("pu_del_store_i").Value = request.puDelStoreCd
            dbCommand.Parameters("pu_del_i").Value = request.puDel
            dbCommand.Parameters("pu_del_dt_i").Value = request.puDelDt
            dbCommand.Parameters("itm_cd_i").Value = request.itemCd
            dbCommand.Parameters("zone_cd_i").Value = request.zoneCd
            dbCommand.Parameters("store_cd_i").Value = request.storeCd
            dbCommand.Parameters("loc_cd_i").Value = request.locCd
            dbCommand.Parameters("qty_i").Value = request.qty
            dbCommand.ExecuteNonQuery()

            response.invStatusCode = dbCommand.Parameters("msg_cd_o").Value
            response.invStatusText = dbCommand.Parameters("msg_text_o").Value
            response.availDt = If(IsDBNull(dbCommand.Parameters("avbl_dt_o").Value), New Date(), dbCommand.Parameters("avbl_dt_o").Value)
            response.poCd = If(IsDBNull(dbCommand.Parameters("po_cd_o").Value), "", dbCommand.Parameters("po_cd_o").Value)
            response.poLnNum = If(IsDBNull(dbCommand.Parameters("po_ln#_o").Value), 0, dbCommand.Parameters("po_ln#_o").Value)
            response.poQty = If(IsDBNull(dbCommand.Parameters("po_avbl_qty_o").Value), 0, dbCommand.Parameters("po_avbl_qty_o").Value)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return response
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Creates a SOFT reservation.  When inventory is available, one record is added to 
    ''' the ITEM_RES table for every different store-location-quantity combination.  
    ''' Further auto-reservations will consider these reservations as non-available inventory
    ''' until these rows are removed either because a 'hard' reservation gets created, 
    ''' or line reserved is unreserved
    ''' </summary>
    ''' <param name="dbConnection">the connection to be used for this process</param>
    ''' <param name="request">custom object with details to perform a soft reservation</param>
    ''' <returns>custom object with the results of the reservation</returns>
    Private Function RequestAutoSoftRes(ByVal dbConnection As OracleConnection,
                                        ByVal request As ResRequestDtc) As ResResponseDtc

        'Note - looks like if not priority fill, the api will default to zone priority fill ???

        Dim response As ResResponseDtc = New ResResponseDtc
        Dim reservations As DataSet = New DataSet
        Dim sql As String = "bt_iface_inv.soft_auto_res"
        Dim dbAdapter As OracleDataAdapter
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        dbCommand.CommandType = CommandType.StoredProcedure

        dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("qty_order_i", OracleType.Number).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("pri_fill_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("pri_fill_zone_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("taken_with_i", OracleType.VarChar, 1).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("loc_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("auto_res_cur", OracleType.Cursor).Direction = ParameterDirection.Output
        dbCommand.Parameters.Add("res_id_o", OracleType.VarChar, AppConstants.Inv.ITM_RES_RES_ID_LEN).Direction = ParameterDirection.Output
        dbCommand.Parameters.Add("group_qty_i", OracleType.VarChar).Direction = ParameterDirection.Input

        dbCommand.Parameters("itm_cd_i").Value = request.itemCd
        dbCommand.Parameters("qty_order_i").Value = request.qty
        dbCommand.Parameters("pu_del_i").Value = request.puDel
        dbCommand.Parameters("pri_fill_store_cd_i").Value = request.puDelStoreCd
        dbCommand.Parameters("pri_fill_zone_cd_i").Value = If(request.zoneCd = Nothing, DBNull.Value, request.zoneCd)
        dbCommand.Parameters("taken_with_i").Value = DBNull.Value
        dbCommand.Parameters("store_cd_i").Value = DBNull.Value  ' leave null or will only return info from this store
        dbCommand.Parameters("loc_tp_cd_i").Value = IIf(request.locTpCd.isEmpty, DBNull.Value, request.locTpCd.Trim)
        dbCommand.Parameters("group_qty_i").Value = "Y"

        dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
        dbAdapter.Fill(reservations)

        response.reservations = reservations.Tables(0)
        response.resId = IIf(reservations.Tables(0).Rows.Count > 0, dbCommand.Parameters("res_id_o").Value.ToString, String.Empty)

        dbCommand.Dispose()
        dbAdapter.Dispose()

        Return response
    End Function

    ''' <summary>
    ''' Removes the ITM_RES records that match the details passed in
    ''' </summary>
    ''' <param name="dbConnection">the connection to be used for this process</param>
    ''' <param name="resId">the reservation id to delete records for</param>
    ''' <param name="itemCd">the item to remove the reservation for</param>
    ''' <param name="qty">number of pieces to unreserve</param>
    ''' <param name="storeCd">the store code where the reservation exists</param>
    ''' <param name="locCd">the location code where the reservation exists</param>
    ''' <param name="locTpCd">the location type code where the reservation exists</param>
    ''' <remarks>the caller is in charge of opening/closing the connection object</remarks>
    Private Sub RemoveSoftRes(ByVal dbConnection As OracleConnection,
                              ByVal resId As String, ByVal itemCd As String,
                              ByVal qty As Double, ByVal storeCd As String,
                              ByVal locCd As String, ByVal locTpCd As String)

        Dim sql As String = "bt_inv.remove_soft_res"
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        dbCommand.CommandType = CommandType.StoredProcedure

        dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("loc_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("res_id_i", OracleType.VarChar).Direction = ParameterDirection.Input
        dbCommand.Parameters.Add("qty_unRes", OracleType.Number).Direction = ParameterDirection.ReturnValue

        dbCommand.Parameters("itm_cd_i").Value = IIf(itemCd.isEmpty, DBNull.Value, itemCd.Trim)
        dbCommand.Parameters("qty_i").Value = IIf(qty <= 0, DBNull.Value, qty)
        dbCommand.Parameters("store_cd_i").Value = IIf(storeCd.isEmpty, DBNull.Value, storeCd.Trim)
        dbCommand.Parameters("loc_cd_i").Value = IIf(locCd.isEmpty, DBNull.Value, locCd.Trim)
        dbCommand.Parameters("loc_tp_cd_i").Value = IIf(locTpCd.isEmpty, DBNull.Value, locTpCd.Trim)
        dbCommand.Parameters("res_id_i").Value = resId      'this is always provided, no need to check
        dbCommand.ExecuteNonQuery()

        dbCommand.Dispose()
    End Sub

    ''' <summary>
    ''' Gets the first zone code found for the store passed in
    ''' </summary>
    ''' <param name="storeCd">the store to get the zone code for</param>
    ''' <returns>the zone code found</returns>
    Public Function GetPriFillZoneCode(ByVal storeCd As String) As String

        Dim zoneCd As String = ""

        If storeCd.isNotEmpty Then

            Dim zones As DataSet = GetTMDac().GetZoneCodesForStore(storeCd, True)
            For Each zoneRow In zones.Tables(0).Rows

                zoneCd = zoneRow("zone_cd")
                Exit For   'needs the first one only
            Next
        End If

        Return zoneCd
    End Function

    Private Function GetTMDac() As TransportationDac
        If (theTMDac Is Nothing) Then
            theTMDac = New TransportationDac()
        End If

        Return theTMDac
    End Function

    Public Function GetPTSRFStatus() As Boolean
        Dim dbReader As OracleDataReader
        Dim ptsRf As Boolean = False
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        Dim sql As String = "select VALUE from GP_PARMS where Key = 'PTS_RF'"
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim result As Object = cmd.ExecuteScalar
        ptsRf = StringUtils.ResolveYNToBoolean(result.ToString.ToUpper)
        conn.Close()
        Return ptsRf
    End Function

    Public Function GetNonRfStatus(ByVal docNum As String, ByVal lnNum As String) As String
        Dim retStatus As String = String.Empty
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("btt_so_ln_stat.get_pick_stat", conn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("del_doc_num_i", OracleType.NVarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("ln#_i", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters.Add("v_pick_status", OracleType.NVarChar, 1).Direction = ParameterDirection.ReturnValue
        cmd.Parameters("del_doc_num_i").Value = docNum
        cmd.Parameters("ln#_i").Value = lnNum
        cmd.ExecuteNonQuery()
        retStatus = cmd.Parameters("v_pick_status").Value.ToString()
        conn.Close()
        Select Case retStatus
            Case "S"
                retStatus = AppConstants.Inv.PICK_STATUS_SCHEDULED
            Case "E"
                retStatus = AppConstants.Inv.PICK_STATUS_EXCEPTION
            Case "P"
                retStatus = AppConstants.Inv.PICK_STATUS_PICKED
            Case "C"
                retStatus = AppConstants.Inv.PICK_STATUS_CHECKED
        End Select

        Return retStatus
    End Function

    ''' <summary>
    ''' Add a Back Order column to SBOB and SBOBM that indicates either ‘Y’es order has some inventory items that are Backordered or ‘N’ order has no inventory items that are considered backordered.
    ''' Alice add for request 151
    ''' </summary>

    Public Function GetBackOrder(ByVal del_doc_num As String) As DataSet

        Dim invDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String

        sql = "SELECT so_store_cd,del_doc_num,pu_del_dt,del_doc_ln#,itm_cd,qty,drop_cd,link_po_arr_dt,itm_avail_dt,fully_paid, "
        sql = sql & "(case when drop_cd is not null and itm_avail_dt >= trunc(sysdate) + 365 then 'YES' "
        sql = sql & "when fully_paid = 'N' then 'NO' "
        sql = sql & "when (pu_del_dt <= trunc(sysdate) + 14 + nvl(days_lead, 0)) and pu_del_dt + 2 <= nvl(link_po_arr_dt, pu_del_dt) and pu_del_dt + 2 < itm_avail_dt then 'YES' "
        sql = sql & "when pu_del_dt + 7 <= nvl(link_po_arr_dt, pu_del_dt + 7) and pu_del_dt + 7 < itm_avail_dt then 'YES' "
        sql = sql & "else 'NO' end) backorder "
        sql = sql & "from (select so.cust_cd, "
        sql = sql & "so.so_store_cd, "
        sql = sql & "so.del_doc_num, "
        sql = sql & "so.pu_del_dt, "
        sql = sql & "sl.del_doc_ln#,"
        sql = sql & "sl.itm_cd, "
        sql = sql & "sl.qty, "
        sql = sql & "i.drop_cd, "
        sql = sql & "(select pl.arrival_dt "
        sql = sql & "From po_ln pl, so_ln$po_ln sp "
        sql = sql & "Where sp.del_doc_num = sl.del_doc_num "
        sql = sql & "And sp.del_doc_ln# = sl.del_doc_ln# "
        sql = sql & "And sp.po_cd = pl.po_cd "
        sql = sql & "And sp.ln# = pl.ln#) link_po_arr_dt, "
        sql = sql & "bt_adv_res.get_available_date(sl.itm_cd, "
        sql = sql & "so.pu_del, "
        sql = sql & "so.ship_to_zone_cd, "
        sql = sql & "sl.qty, "
        sql = sql & "so.pu_del_store_cd) itm_avail_dt, "
        sql = sql & "slp.po_cd, "
        sql = sql & "slp.ln#, "
        sql = sql & "z.days_lead, "
        sql = sql & "custom_paid.is_order_paid_in_full('SAL', "
        sql = sql & "so.del_doc_num, "
        sql = sql & "so.cust_cd) fully_paid "
        sql = sql & "From so, store_detail sd, so_ln sl, itm i, zone z, so_ln$po_ln slp "
        sql = sql & "Where so.del_doc_num = sl.del_doc_num "
        sql = sql & "And so.del_doc_num ='" & del_doc_num & "' "
        sql = sql & "And so.stat_cd = 'O' "
        sql = sql & "And so.so_store_cd = sd.store_cd "
        sql = sql & "And so.ship_to_zone_cd = z.zone_cd "
        sql = sql & "And so.ord_tp_cd = 'SAL' "
        sql = sql & "And so.pu_del_dt <= trunc(sysdate) + 364 "
        sql = sql & "And sl.itm_cd = i.itm_cd "
        sql = sql & "And i.inventory = 'Y' "
        sql = sql & "And sl.void_flag = 'N' "
        sql = sql & "And sl.del_doc_num = slp.del_doc_num(+) "
        sql = sql & "And sl.del_doc_ln# = slp.del_doc_ln#(+) "
        sql = sql & "And sl.store_cd Is null) "
        sql = UCase(sql)

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(invDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return invDataSet

    End Function

End Class
