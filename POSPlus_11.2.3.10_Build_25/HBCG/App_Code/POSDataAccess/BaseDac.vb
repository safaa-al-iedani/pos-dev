﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.OracleClient


Public MustInherit Class BaseDac

    Private Const ERPConnection As String = "ERP"

    ''' <summary>
    ''' Creates a connection using the connection string defined in the configuration file
    ''' </summary>
    ''' <returns>a new oracle connection</returns>
    Protected Function GetConnection() As OracleConnection
        Return GetConnection(GetConnectionString())
    End Function

    ''' <summary>
    ''' Creates a connection using the user name and password passed in.
    ''' </summary>
    ''' <param name="username">the user needed to establish the connection</param>
    ''' <param name="password">the password needed to establish the connection</param>
    ''' <returns>a new oracle connection</returns>
    Protected Function GetConnection(ByVal username As String, ByVal password As String) As OracleConnection
        Return GetConnection(GetConnectionString(username, password))
    End Function

    ''' <summary>
    ''' Gets the connection string as defined in the configuration file
    ''' </summary>
    ''' <returns>the connection string</returns>
    Protected Function GetConnectionString() As String
        Return ConfigurationManager.ConnectionStrings(ERPConnection).ConnectionString
    End Function

    ''' <summary>
    ''' Builds the connection string using the user name and password passed in.
    ''' </summary>
    ''' <param name="username">the user that will be used when creating the connection</param>
    ''' <param name="password">the password to be used when creating the connection</param>
    ''' <returns>the connection string</returns>
    Protected Function GetConnectionString(ByVal username As String, ByVal password As String) As String

        ' Gets the connection string from the Configuration file and replaces the
        ' user name and password in it.
        Dim connStringBuilder As OracleConnectionStringBuilder = New OracleConnectionStringBuilder()
        connStringBuilder.ConnectionString = GetConnectionString()
        connStringBuilder.UserID = username
        connStringBuilder.Password = password
        Return connStringBuilder.ToString()

    End Function

    ''' <summary>
    ''' Creates an Oracle Command, with the default type CommandType.Text using the command text provided
    ''' </summary>
    ''' <param name="cmdText">the query statement to be used in this command object</param>
    ''' <returns>a new oracle command</returns>
    Protected Function GetCommand(ByVal cmdText As String) As OracleCommand
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = cmdText
        Return (cmd)
    End Function

    ''' <summary>
    ''' Creates an Oracle Command, with the default type CommandType.Text using the command text provided
    ''' and attaches the connection passed in
    ''' </summary>
    ''' <param name="connection">the connection to be attached to this command</param>
    ''' <param name="cmdText">the query statement to be used in this command object</param>
    ''' <returns>a new oracle command</returns>
    Protected Function GetCommand(ByVal cmdText As String, ByRef connection As OracleConnection) As OracleCommand
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = cmdText
        cmd.Connection = connection
        Return (cmd)
    End Function

    ''' <summary>
    ''' Convert the string value passed in to the proper format, as required by the database
    ''' </summary>
    ''' <param name="value">>the string value to be checked/formatted</param>
    ''' <returns>the value ready for storage in the database</returns>
    Protected Function GetDbStringValue(ByVal value As String) As Object
        If (String.IsNullOrEmpty(value)) Then
            Return DBNull.Value
        Else
            Return (value.Trim())
        End If
    End Function

    ''' <summary>
    ''' Convert the date value passed in to the proper format, as required by the database
    ''' </summary>
    ''' <param name="value">the date value to be checked</param>
    ''' <returns>the value ready for storage in the database</returns>
    ''' <remarks></remarks>
    Protected Function GetDbDateValue(ByVal value As DateTime?) As Object
        If (value Is Nothing OrElse value = DateTime.MinValue) Then
            Return DBNull.Value
        Else
            Return (FormatDate(value))
        End If
    End Function

    ''' <summary>
    ''' Convert the date value passed in to the proper format, as required by the database
    ''' </summary>
    ''' <param name="value">the date value to be checked</param>
    ''' <returns>the value ready for storage in the database</returns>
    ''' <remarks></remarks>
    Protected Function GetDbDateValue(ByVal value As DateTime) As Object
        If (value = DateTime.MinValue) Then
            Return DBNull.Value
        Else
            Return (FormatDate(value))
        End If
    End Function

    ''' <summary>
    ''' Converts the list of strings passed in, to a comma delimited list and encloses
    ''' every value within quotes, so that the string is ready to be used as part of
    ''' a SQL statement that needs an IN clause
    ''' </summary>
    ''' <param name="value">list of string values to be prepared for an SQL IN clause</param>
    ''' <returns>comma delimited string where each value is enclosed in quotes</returns>
    Protected Function GetDelimitedString(ByVal value() As String) As String

        Dim stringList() As String = DirectCast(value, String())

        'Convert the values into CSV -preparing the string for IN clause, ex 'itm1','itm2'...
        Dim commaDelimitedValues As String = (String.Join(",", stringList).Replace(",", "','"))

        Return "'" + commaDelimitedValues + "'"
    End Function

    ''' <summary>
    ''' Releases the DB resources
    ''' </summary>
    ''' <param name="conn">the connection to be closed and disposed of</param>
    ''' <param name="cmd">the command to be disposed</param>
    ''' <remarks></remarks>
    Protected Sub CloseResources(ByRef conn As OracleConnection, ByRef cmd As OracleCommand)
        CloseResources(conn)
        CloseResources(cmd)
    End Sub

    ''' <summary>
    ''' Releases the DB resources
    ''' </summary>
    ''' <param name="conn">the connection to be closed and disposed of</param>
    ''' <remarks></remarks>
    Protected Sub CloseResources(ByRef conn As OracleConnection)
        If (conn IsNot Nothing AndAlso conn.State = ConnectionState.Open) Then conn.Close()
        conn.Dispose()
    End Sub

    ''' <summary>
    ''' Releases the DB resources
    ''' </summary>
    ''' <param name="cmd">the command to be disposed</param>
    ''' <remarks></remarks>
    Protected Sub CloseResources(ByRef cmd As OracleCommand)
        If cmd IsNot Nothing Then cmd.Dispose()
    End Sub

    ''' <summary>
    ''' Convert true or false values to Y and N respectively, since BT integration can only handle primitive types.
    ''' It can only be used if N equates to False and Y equates to True.
    ''' </summary>
    ''' <param name="inpChar">Input True or False</param>
    ''' <returns>if the input value is a true then Y; otherwise N </returns>
    Protected Function ResolveBooleanToYN(ByVal input As Boolean) As String

        Return (IIf(input = True, "Y", "N"))
    End Function


    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Creates a connection using the connection string passed in.
    ''' </summary>
    ''' <param name="connectionString">the connection string to be used for the connection</param>
    ''' <returns>a new oracle connection</returns>
    ''' <remarks>this method throws an exception if the connection string is not valid</remarks>
    Private Function GetConnection(ByVal connectionString As String) As OracleConnection
        If (connectionString Is Nothing OrElse connectionString.Trim = "") Then
            Throw New Exception("Connection Error")
        Else
            Return DisposablesManager.BuildOracleConnection(connectionString)
        End If
    End Function


    ''' <summary>
    ''' Formats the date passed in, as expected by the database
    ''' </summary>
    ''' <param name="DateTime">the date to be formatted</param>
    ''' <returns>the formatted date in the format expected by the database</returns>
    Private Function FormatDate(ByVal value As DateTime) As Object
        Return (value.ToString("dd-MMM-yyyy"))
    End Function

End Class
