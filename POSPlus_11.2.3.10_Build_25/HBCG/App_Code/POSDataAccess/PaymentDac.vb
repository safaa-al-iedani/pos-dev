﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class PaymentDac
    Inherits BaseDac

    ''' <summary>
    ''' Finds out if the session passed in, contains finance payments newly keyed
    ''' </summary>
    ''' <param name="sessionId">the session to check the payments for</param>
    ''' <returns>true if FInance exists; false otherwise</returns>
    Public Function HasFinancePayment(ByVal sessionId As String) As Boolean

        Dim hasFinance As Boolean = False
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT COUNT(row_id) as finPmtCount FROM payment p, settlement_mop sm ")
        dbSql.Append("WHERE sessionid =: sessionId AND p.mop_tp = 'FI' AND p.mop_tp = sm.mop_tp ")
        dbSql.Append("  AND p.mop_cd = sm.mop_cd AND sm.finance_deposit_tp = 'N'")

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql.ToString

            Dim objCount As Object = dbCommand.ExecuteScalar()
            hasFinance = (Not IsNothing(objCount) AndAlso Not IsDBNull(objCount) AndAlso CInt(objCount) > 0)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return hasFinance
    End Function

    ''' <summary>
    ''' Retrieves from the CRM.PAYMENT table, the finance payment and its details
    ''' for the sessionID passed in. The Deposit finances (DP) are ignored
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve finance payments for</param>
    ''' <returns>a datatable with the non-DP finance payment found; or an empty table, if none</returns>
    Public Function GetNonDPFinanceDetails(ByVal sessionId As String) As DataTable

        Dim finPmtsDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT fin_co, fin_promo_cd, fi_acct_num, approval_cd, amt ")
        dbSql.Append("FROM payment p, settlement_mop sm ")
        dbSql.Append("WHERE p.mop_tp = 'FI' AND p.mop_cd = sm.mop_cd AND ")
        dbSql.Append("      finance_deposit_tp <> 'Y' AND sessionid = :sessionId")

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql.ToString
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(finPmtsDataSet)

            dbAdapter.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return finPmtsDataSet.Tables(0)
    End Function

    ''' <summary>
    ''' Retrieves from the AR_TRN table the corresponding account number for the document passed in
    ''' </summary>
    ''' <param name="delDoc">the document number to find the finance account number for</param>
    ''' <param name="ordTp">type of document.  Refer to AppConstants.Order.Type for valid types</param>
    ''' <returns>the finance account number or an empty string if none found</returns>
    Public Function GetFinanceAccount(ByVal delDoc As String, Optional ordTp As String = AppConstants.Order.TYPE_SAL) As String

        Dim finAcct As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        'Dim sql As String = "SELECT BNK_CRD_NUM FROM AR_TRN " ' Daniela encript issue
        Dim sql As String = "SELECT std_des3.decryptCrdNum(BNK_CRD_NUM) BNK_CRD_NUM FROM AR_TRN " ' Daniela encript issue 
        If (AppConstants.Order.TYPE_SAL = ordTp OrElse AppConstants.Order.TYPE_MDB = ordTp) Then
            sql = sql & " WHERE IVC_CD=:IVC_CD AND MOP_CD='FI' "
        Else
            sql = sql & " WHERE ADJ_IVC_CD=:IVC_CD AND MOP_CD='FI' "
        End If

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":IVC_CD", OracleType.VarChar)
            dbCommand.Parameters(":IVC_CD").Value = UCase(delDoc)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                finAcct = dbReader.Item("BNK_CRD_NUM").ToString
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return finAcct

    End Function

    ''' <summary>
    ''' Finds out if the credit card number passed in is valid.   It validates against the tables
    ''' ASP_MEDIUM_FORMAT and ASP_MEDIUM2MOP
    ''' </summary>
    ''' <param name="storeCd">the store code to use in the validation</param>
    ''' <param name="mopCd">the method of payment code</param>
    ''' <param name="cardNum">the credit card number</param>
    ''' <returns>TRUE if the card is valid; FALSE otherwise</returns>
    ''' <remarks></remarks>
    Public Function IsValidCreditCard(ByVal storeCd As String, ByVal mopCd As String, ByVal cardNum As String) As Boolean

        Dim isValid As Boolean = False
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append(" SELECT a.medium_prefix, a.medium_digits FROM asp_medium_format a, asp_medium2mop b ")
        sql.Append(" WHERE a.medium_tp_cd=b.medium_tp_cd AND b.store_cd = :storeCd AND b.mop_cd = :mopCd ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":mopCd", OracleType.VarChar)

            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbCommand.Parameters(":mopCd").Value = UCase(mopCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.HasRows()) Then
                Do While dbReader.Read()
                    If dbReader.Item("MEDIUM_PREFIX").ToString & "" <> "" And IsNumeric(dbReader.Item("MEDIUM_DIGITS").ToString) Then
                        If (Left(cardNum, Len(dbReader.Item("MEDIUM_PREFIX").ToString)) = dbReader.Item("MEDIUM_PREFIX").ToString AndAlso
                            Len(cardNum) = CDbl(dbReader.Item("MEDIUM_DIGITS").ToString)) Then

                            isValid = True
                            Exit Do
                        End If
                    End If
                Loop
            Else
                'if no rows are found is considered valid
                isValid = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isValid

    End Function

    ''' <summary>
    ''' Deletes from the PAYMENT table the row that matches the identifier passed in.
    ''' </summary>
    ''' <param name="rowSeqNum">unique row identifier for the row that will be removed</param>
    ''' <remarks>rowSeqNum is the one found in ROW_ID column within the PAYMENT table</remarks>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayment(ByVal rowSeqNum As String) As Integer

        Dim rowCount As Integer = 0
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As String = "DELETE FROM payment WHERE row_id = :rowSeqNum"

        Try
            dbConnection.Open()
            dbCommand.CommandText = dbSql
            dbCommand.Parameters.Add(":rowSeqNum", OracleType.VarChar)
            dbCommand.Parameters(":rowSeqNum").Value = rowSeqNum
            rowCount = dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return rowCount
    End Function

    ''' <summary>
   ''' Deletes from the PAYMENT table the rows that match the sessionId passed in using passed in connection.
    ''' </summary>
    ''' <param name="sessionId">sessionId for which to remove the payments</param>
    ''' <param name="dbcmd">oracle database command</param>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayments(ByVal sessionId As String, ByVal dbCmd As OracleCommand) As Integer

        Dim rowCount As Integer = 0

        Dim dbSql As String = "DELETE FROM payment WHERE sessionId = :sessionId"

        Try
            dbCmd.CommandText = dbSql
            dbCmd.Parameters.Clear()
            dbCmd.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCmd.Parameters(":sessionId").Value = sessionId
            rowCount = dbCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        End Try

        Return rowCount
    End Function

    ''' <summary>        
	''' Deletes from the PAYMENT table the rows that match the sessionId passed in.
    ''' </summary>
    ''' <param name="sessionId">sessionId for which to remove the payments</param>
    ''' <returns>number of records deleted</returns>
    Public Function DeletePayments(ByVal sessionId As String) As Integer

        Dim rowCount As Integer = 0
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)

        Try
            dbConnection.Open()
            rowCount = DeletePayments(sessionId, dbCommand)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return rowCount
    End Function

End Class
