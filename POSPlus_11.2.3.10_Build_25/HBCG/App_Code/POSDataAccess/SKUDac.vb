﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports System.Linq

Public Class SKUDac
    Inherits BaseDac

    ''' <summary>
    ''' This method, takes a Command object (Querry is assigned to Command object), executes the query
    ''' and returns a dataset back containing the results.   Made private, to prevent usage from outside
    ''' this class.
    ''' If an exception occurs, it throws it back to calling program
    ''' </summary>
    ''' <param name="dbCommand">Oracle Command object, which should include Query and parameters(if any)</param>
    ''' <returns>Dataset with resulting data</returns>
    Private Function ExecuteDataset(ByVal dbCommand As OracleCommand)

        Dim dbConnection As OracleConnection
        Dim dbAdapter As OracleDataAdapter
        Dim dSet As DataSet = New DataSet()

        Try
            dbConnection = GetConnection()
            dbCommand.Connection = dbConnection
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(dSet)

        Catch ex As Exception
            Throw ex
        Finally
            If ConnectionState.Open = dbConnection.State Then dbConnection.Close()
            dbAdapter.Dispose()
        End Try

        Return dSet
    End Function

    ''' <summary>
    ''' Retrieves the correct price to be used for the ITEM passed in
    ''' </summary>
    ''' <param name="storeCd">the store to use to retrieve pricing</param>
    ''' <param name="custTpPrCd">the customer type price code (when available)</param>
    ''' <param name="itemCd">the ITEM code for which pricing is being retrieved</param>
    ''' <param name="retPrice">the current price of the ITEM, as found in the ITM table</param>
    ''' <returns>the correct Retail Price for this ITEM</returns>
    ''' <remarks>this API call, takes into account the Calendars in place for the SKU</remarks>
    Public Function GetCurrentPricing(ByVal storeCd As String,
                                      ByVal custTpPrCd As String,
                                      ByVal effDate As String,
                                      ByVal itemCd As String,
                                      ByVal itemRetPrice As Double) As Double
        Dim currentPrice As Double = 0.0
        Dim dbConnecton As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("bt_price.current_price", dbConnecton)

        Try
            dbConnecton.Open()
            dbCommand.CommandType = CommandType.StoredProcedure

            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cash_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("eff_date_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("itm_ret_prc_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cust_tp_prc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("err_msg_io", OracleType.VarChar, 250).Direction = ParameterDirection.InputOutput
            dbCommand.Parameters.Add("return_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("v_price", OracleType.Number).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters("itm_cd_i").Value = itemCd
            dbCommand.Parameters("store_cd_i").Value = storeCd
            dbCommand.Parameters("cash_flag_i").Value = "N"
            dbCommand.Parameters("eff_date_i").Value = effDate
            dbCommand.Parameters("itm_ret_prc_i").Value = itemRetPrice
            dbCommand.Parameters("cust_tp_prc_cd_i").Value = custTpPrCd
            dbCommand.Parameters("err_msg_io").Value = DBNull.Value
            dbCommand.Parameters("return_type_i").Value = DBNull.Value
            dbCommand.ExecuteNonQuery()

            currentPrice = CDbl(dbCommand.Parameters("v_price").Value)

        Catch ex As Exception
            'Throw
            'Daniela get error details
            Throw New Exception("Error in GetCurrentPricing " + itemCd + ";" + storeCd + ";" + custTpPrCd, ex)
        Finally
            CloseResources(dbConnecton, dbCommand)
        End Try

        Return currentPrice
    End Function

    ''' <summary>
    ''' Over loadded method with the command object :Retrieves the correct price to be used for the ITEM passed in
    ''' </summary>
    ''' <param name="storeCd"></param>
    ''' <param name="custTpPrCd"></param>
    ''' <param name="effDate"></param>
    ''' <param name="itemCd"></param>
    ''' <param name="itemRetPrice"></param>
    ''' <param name="dbCommand"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCurrentPricing(ByVal storeCd As String,
                                      ByVal custTpPrCd As String,
                                      ByVal effDate As String,
                                      ByVal itemCd As String,
                                      ByVal itemRetPrice As Double,
                                      ByRef dbCommand As OracleCommand) As Double
        Dim currentPrice As Double = 0.0

        Try
            dbCommand.Parameters.Clear()
            dbCommand.CommandText = "bt_price.current_price"
            dbCommand.CommandType = CommandType.StoredProcedure

            dbCommand.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cash_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("eff_date_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("itm_ret_prc_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cust_tp_prc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("err_msg_io", OracleType.VarChar, 250).Direction = ParameterDirection.InputOutput
            dbCommand.Parameters.Add("return_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("v_price", OracleType.Number).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters("itm_cd_i").Value = itemCd
            dbCommand.Parameters("store_cd_i").Value = storeCd
            dbCommand.Parameters("cash_flag_i").Value = "N"
            dbCommand.Parameters("eff_date_i").Value = effDate
            dbCommand.Parameters("itm_ret_prc_i").Value = itemRetPrice
            dbCommand.Parameters("cust_tp_prc_cd_i").Value = custTpPrCd
            dbCommand.Parameters("err_msg_io").Value = DBNull.Value
            dbCommand.Parameters("return_type_i").Value = DBNull.Value
            dbCommand.ExecuteNonQuery()

            currentPrice = CDbl(dbCommand.Parameters("v_price").Value)

        Catch ex As Exception
            Throw
        End Try

        Return currentPrice
    End Function

    ''' <summary>
    ''' Check the price change for the releationship price to the current price before convert to order
    ''' </summary>
    ''' <param name="delDocNum"></param>
    ''' <param name="StoreCd"></param>
    ''' <param name="custTpPrCd"></param>
    ''' <param name="effDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CheckPriceChange(ByVal delDocNum As String, ByVal StoreCd As String, ByVal custTpPrCd As String, ByVal effDate As String) As Dictionary(Of String, Double)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsqlCommand As OracleCommand
        Dim linesDatRdr As OracleDataReader
        Dim objDataAdapter As OracleDataAdapter
        Dim relationLine As New DataSet
        Dim sql As String
        Dim dictCurrentPrice As New Dictionary(Of String, Double)
        Dim theSalesDac As SalesDac = New SalesDac()
        Dim retprc As Double

        Dim viewPkgComp As DataView
        Dim tempPkgComp As DataSet
        Dim expression As String
        Dim compPriceRow As DataRow
        Dim view As DataView
        Dim tempPkgCompRow As DataTable
        Try
            conn.Open()
            sql = "SELECT RL.RET_PRC,RL.ITM_CD,I.RET_PRC AS ITM_RET_PRC,RL.ITM_TP_CD,RL.LINE,RL.PACKAGE_PARENT,RL.RET_PRC + NVL(RL.DISC_AMT,0) as RET_PRC_DISC FROM RELATIONSHIP_LINES RL,ITM I WHERE I.ITM_CD=RL.ITM_CD AND RL.REL_NO=:DELNUM ORDER BY LINE"
            objsqlCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            objsqlCommand.Parameters.Add(":delNum", OracleType.VarChar)
            objsqlCommand.Parameters(":delNum").Value = delDocNum
            objDataAdapter = DisposablesManager.BuildOracleDataAdapter(objsqlCommand)
            objDataAdapter.Fill(relationLine)

            'Get all the RELATIONSHIP LINES except the pkg components.
            Dim queryInv = From row In relationLine.Tables(0).AsEnumerable() _
                       Where row.Field(Of String)("Package_parent").isEmpty
                       Select row
            view = queryInv.AsDataView()

            For Each DataRow In view
                If DataRow("ITM_TP_CD") = "PKG" AndAlso DataRow("RET_PRC_DISC") = 0 Then
                    'Get PKG Items from the RELATIONSHIP_LINES table
                    Dim queryPkgComp = From row In relationLine.Tables(0).AsEnumerable() _
                                        Where row.Field(Of String)("Package_parent") = DataRow("LINE").ToString
                                        Select row
                    viewPkgComp = queryPkgComp.AsDataView()

                    'Get PKG Items from the Item table
                    tempPkgComp = theSalesDac.GetPkgComponentData(DataRow("ITM_CD"), conn, StoreCd, String.Empty)

                    For Each pkgCompRow In viewPkgComp
                        expression = "ITM_CD='" & pkgCompRow("ITM_CD") & "'"
                        tempPkgCompRow = tempPkgComp.Tables(0).Select(expression).Take(1).CopyToDataTable
                        If Convert.ToDouble(tempPkgCompRow(0)("CURR_PRC")) <> pkgCompRow("RET_PRC") Then
                            If Not dictCurrentPrice.ContainsKey(pkgCompRow("ITM_CD").ToString) Then
                                dictCurrentPrice.Add(pkgCompRow("ITM_CD").ToString, tempPkgCompRow(0)("CURR_PRC"))
                            End If
                        End If
                    Next
                Else
                    retprc = GetCurrentPricing(StoreCd, custTpPrCd, effDate, DataRow("ITM_CD").ToString, DataRow("ITM_RET_PRC"), objsqlCommand)
                    If retprc <> DataRow("RET_PRC_DISC") Then
                        If Not dictCurrentPrice.ContainsKey(DataRow("ITM_CD").ToString) Then
                            dictCurrentPrice.Add(DataRow("ITM_CD").ToString, retprc)
                        End If
                    End If
                End If
            Next

        Catch ex As Exception
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return dictCurrentPrice
    End Function

    ''' <summary>
    ''' Filters the list of item codes passed in, to only those that are sellable
    ''' which means their flag PREVENT_SALE = N
    ''' </summary>
    ''' <param name="items">a list of item codes</param>
    ''' <returns>a list with the remaining item codes, of the ones still sellable</returns>
    Public Function GetSellableItems(ByVal items As List(Of String)) As List(Of String)
        Dim sellableItems As New List(Of String)
        Dim dbConnecton As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbSql As New StringBuilder()

        dbSql.Append("SELECT i.itm_cd, ")
        dbSql.Append("  CASE WHEN (i.drop_dt <= SYSDATE) AND UPPER(d.prevent_sale) = 'Y' THEN 'Y' ELSE 'N' END prevent_sale ")
        dbSql.Append("  FROM itm i LEFT JOIN drop_cd d ON d.drop_cd = i.drop_cd ")
        dbSql.Append(" WHERE UPPER(i.ITM_CD) IN (")

        Try
            dbConnecton.Open()
            dbCommand = GetCommand(String.Empty, dbConnecton)

            Dim cnt As Integer = 0
            For Each item As String In items
                If cnt > 0 Then dbSql.Append(", ")
                dbSql.Append(" :ITM_CD" & cnt.ToString())
                dbCommand.Parameters.Add("ITM_CD" & cnt.ToString(), OracleType.VarChar).Direction = ParameterDirection.Input
                dbCommand.Parameters("ITM_CD" & cnt.ToString()).Value = item.Trim().ToUpper()
                cnt = cnt + 1
            Next

            dbSql.Append(")")

            dbCommand.CommandText = dbSql.ToString()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            While (dbReader.Read())
                If ("N" = dbReader("prevent_sale")) Then
                    sellableItems.Add(dbReader("itm_cd"))
                End If
            End While
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnecton, dbCommand)
        End Try

        Return sellableItems
    End Function

    ''' <summary>
    ''' Finds out if the item passed is allowed to be sold and placed on an order. 
    ''' To NOT be allowed for sale, means that its dropped date is before the current date
    ''' and the PREVENT_SALE flag = 'Y' 
    ''' An item can be sold even if dropped, as long as the PREVENT_SALE flag = 'N'
    ''' </summary>
    ''' <param name="itmCd">the item to find if sellable or not</param>
    ''' <returns>TRUE if item can be sold; FALSE otherwise</returns>
    Public Function IsSellable(ByVal itmCd As String) As Boolean

        Dim preventSale As Boolean = False
        Dim dbConnecton As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand
        Dim dSet As DataSet
        Dim dbSql As New StringBuilder()

        dbSql.Append("SELECT NVL(d.prevent_sale, 'N') AS prevent_sale ")
        dbSql.Append("  FROM itm i, drop_cd d ")
        dbSql.Append(" WHERE i.drop_cd = d.drop_cd AND i.drop_dt <= SYSDATE ")
        dbSql.Append("   AND UPPER(i.itm_cd) = UPPER(:itmCd) ")
        Try
            dbConnecton.Open()
            dbCommand = GetCommand(dbSql.ToString, dbConnecton)
            dbCommand.Parameters.Add("itmCd", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("itmCd").Value = itmCd.Trim()

            Dim result As Object = dbCommand.ExecuteScalar()
            If (Not IsNothing(result)) Then preventSale = ("Y" = CStr(result)) 'PREVENT_SALE = Y

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnecton, dbCommand)
        End Try

        Return (Not preventSale)
    End Function

    ''' <summary>
    ''' Filters the list of items passed in, to return only those that that have been dropped.
    ''' </summary>
    ''' <param name="items">a list of item codes that will be filtered</param>
    ''' <returns>the item codes that are found to be dropped</returns>
    Public Function GetPreventedItemsForARS(ByVal items() As String) As List(Of String)

        Dim droppedItems As New List(Of String)
        Dim dbCommand As OracleCommand
        Dim dSet As DataSet
        Dim dbSql As New StringBuilder()
        Dim orderItems() As String = DirectCast(items, String())

        'Convert the ITM_CDs into CSV -preparing the string for IN clause, ex 'itm1','itm2'...
        Dim commaDelimitedItems As String = (String.Join(",", orderItems).Replace(",", "','"))

        dbSql.Append(" SELECT I.ITM_CD")
        dbSql.Append(" FROM ITM i JOIN DROP_CD D ON D.DROP_CD = I.DROP_CD ")
        dbSql.Append(" WHERE i.ITM_CD IN ( '" & commaDelimitedItems & "' ) AND ")
        dbSql.Append(" i.DROP_DT <= SYSDATE ")
        dbSql.Append(" UNION ")
        dbSql.Append(" SELECT ITM_CD FROM ITM$ITM_SRT SRT JOIN ITM_SRT isrt on srt.ITM_SRT_CD = isrt.ITM_SRT_CD  ")
        dbSql.Append(" WHERE  ITM_CD  in('" & commaDelimitedItems & "') AND   isrt.MTO = 'Y'")

        Try
            dbCommand = GetCommand(dbSql.ToString)
            dSet = ExecuteDataset(dbCommand)

            If (Not IsNothing(dSet.Tables(0)) AndAlso dSet.Tables(0).Rows.Count > 0) Then
                For Each dr As DataRow In dSet.Tables(0).Rows
                    droppedItems.Add(dr("itm_cd"))
                Next
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbCommand)
        End Try

        Return droppedItems
    End Function

    ''' <summary>
    ''' Retrieves from the RELATIONSHIP_LINES table, the SKUs that are found to be dropped for the 
    ''' relationship number passed in.
    ''' The table returned, includes ITM_CD, DROP_CD, DROP_DT, PREVENT_SALE, PREVENT_PO details, 
    ''' so that the caller can process accordingly.
    ''' </summary>
    ''' <param name="relNum">the relationship number to fetch the dropped SKUs for</param>
    ''' <returns>a datatable containing the details of the dropped SKUs, or an empty one if none found</returns>
    Public Function GetDroppedItemsForRelationship(ByVal relNum As String) As DataTable

        Dim droppedItems As New DataTable
        Dim dbCommand As OracleCommand
        Dim dSet As DataSet
        Dim dbSql As New StringBuilder()

        dbSql.Append("SELECT i.itm_cd, i.drop_cd, i.drop_dt, NVL(d.prevent_sale, 'N') AS prevent_sale, ")
        dbSql.Append("  NVL(d.prevent_po, 'N') AS prevent_po")
        dbSql.Append("  FROM RELATIONSHIP_LINES r, itm i, drop_cd d ")
        dbSql.Append(" WHERE r.itm_cd = i.itm_cd AND i.drop_cd = d.drop_cd AND i.drop_dt <= SYSDATE ")
        dbSql.Append("   AND UPPER(r.rel_no) = UPPER(:relNum) ")
        Try
            dbCommand = GetCommand(dbSql.ToString)
            dbCommand.Parameters.Add("relNum", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("relNum").Value = relNum
            dSet = ExecuteDataset(dbCommand)
            If (Not IsNothing(dSet.Tables(0))) Then droppedItems = dSet.Tables(0)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbCommand)
        End Try

        Return droppedItems
    End Function

    ''' <summary>
    ''' Retrieves from the TEMP_ITM table, the SKUs that are found to be dropped for the Session ID passed in.
    ''' The table returned, includes ITM_CD, DROP_CD, DROP_DT, PREVENT_SALE, PREVENT_PO details, so that the
    ''' caller can process accordingly.
    ''' </summary>
    ''' <param name="sessionId">the session id to fetch the dropped SKUs for</param>
    ''' <returns>a datatable containing the details of the dropped SKUs, or an empty one if none found</returns>
    Public Function GetDroppedItemsForSession(ByVal sessionId As String) As DataTable

        Dim droppedItems As New DataTable
        Dim dbCommand As OracleCommand
        Dim dSet As DataSet
        Dim dbSql As New StringBuilder()

        dbSql.Append("SELECT i.itm_cd, i.drop_cd, i.drop_dt, NVL(d.prevent_sale, 'N') AS prevent_sale, ")
        dbSql.Append("  NVL(d.prevent_po, 'N') AS prevent_po")
        dbSql.Append("  FROM temp_itm t, itm i, drop_cd d ")
        dbSql.Append(" WHERE t.itm_cd = i.itm_cd AND i.drop_cd = d.drop_cd AND i.drop_dt <= SYSDATE ")
        dbSql.Append("   AND UPPER(t.session_id) = UPPER(:sessionId) ")
        Try
            dbCommand = GetCommand(dbSql.ToString)
            dbCommand.Parameters.Add("sessionId", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("sessionId").Value = sessionId
            dSet = ExecuteDataset(dbCommand)
            If (Not IsNothing(dSet.Tables(0))) Then droppedItems = dSet.Tables(0)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbCommand)
        End Try

        Return droppedItems
    End Function

    ''' <summary>
    ''' Gets the ITEM details for the each of the ITEMS passed in the array of strings
    ''' </summary>
    ''' <param name="items">Array of strings, where each element represets an item code</param>
    ''' <param name="storeCd">store where items are being processed, needed to get correct SPIFF value</param>
    ''' <returns>A datatable containing the Item details for all the Items passed in</returns>
    ''' <remarks>IMPORTANT: ITEMS ARE NOT VALIDATED.  IT IS ASSUMED THEY EXIST IN E1 ALREADY</remarks>
    Public Function GetItemInfo(ByVal items() As String, ByVal storeCd As String) As DataTable

        Dim itemDetails As New DataTable
        Dim dbCommand As OracleCommand
        Dim dbSql As New StringBuilder()
        Dim dSet As DataSet
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "itm$spiff_clndr")

        dbSql.Append(" SELECT i.itm_cd, i.itm_tp_cd, i.des, i.vsn, i.ret_prc, i.adv_prc, i.repl_cst, i.ve_cd, i.mnr_cd, i.cat_cd, ")
        dbSql.Append("        i.treatable, NVL(i.warrantable,'N') AS warrantable, i.inventory, NVL(i.bulk_tp_itm,'N') AS bulk_tp_itm, ")
        dbSql.Append("        NVL(i.serial_tp, '') AS serial_tp, i.siz, i.finish, i.cover, i.grade, i.style_cd, ")
        dbSql.Append("        i.frame_tp_itm, i.drop_cd, i.drop_dt, i.user_questions, i.point_size,")
        dbSql.Append("        i.comm_cd, NVL(s.spiff_amt, i.spiff) AS spiff ")
        dbSql.Append(" FROM itm i LEFT JOIN ").Append(spiffTable).Append(" s ON i.itm_cd = s.itm_cd ")
        dbSql.Append(If(Not SysPms.isSpiffBySKU, " AND UPPER(s.store_cd) = UPPER(:storeCd) ", " "))
        dbSql.Append("      AND TO_DATE(SYSDATE) BETWEEN TO_DATE(s.eff_dt) AND TO_DATE(s.end_dt) ")
        dbSql.Append(" WHERE i.itm_cd IN (").Append(GetDelimitedString(items)).Append(") ")
        dbSql.Append(" ORDER BY 1")

        Try
            dbCommand = GetCommand(dbSql.ToString)
            If (Not SysPms.isSpiffBySKU) Then
                dbCommand.Parameters.Add("storeCd", OracleType.VarChar).Direction = ParameterDirection.Input
                dbCommand.Parameters("storeCd").Value = storeCd.Trim
            End If
            dSet = ExecuteDataset(dbCommand)
            If (Not IsNothing(dSet.Tables(0))) Then itemDetails = dSet.Tables(0)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbCommand)
        End Try

        Return itemDetails
    End Function

    ''' <summary>
    ''' Fetches the Related Items for the list of item codes passed in.
    ''' </summary>
    ''' <param name="items">the items to retrieve Related items for</param>
    ''' <param name="storeCd">store where items are being processed, needed to get correct SPIFF value</param>
    ''' <returns>A datatable containing the Item details for all the Related Items found</returns>
    Public Function GetRelatedItems(ByVal items() As String, ByVal storeCd As String) As DataTable

        Dim relatedItemDetails As New DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sqlE1RelSkus As New StringBuilder()
        Dim sqlPOSRelSkus As New StringBuilder()
        Dim relatedItemCodes As New List(Of String)
        Dim commaDelimItems = GetDelimitedString(items)

        ' Related SKUs that were setup using the E1 application (ITM2RSKU table)
        sqlE1RelSkus.Append(" SELECT UNIQUE(i2rs.related_itm_cd) as itm_cd")
        sqlE1RelSkus.Append(" FROM itm2rsku i2rs ")
        sqlE1RelSkus.Append(" WHERE i2rs.itm_cd IN (").Append(commaDelimItems).Append(")")

        ' Related SKUs that were setup using the POS+ Utilities menu (RELATED_SKUS table)
        sqlPOSRelSkus.Append(" WITH cts AS  ( SELECT i.mnr_cd, imnr.mjr_cd FROM itm i, inv_mnr imnr ")
        sqlPOSRelSkus.Append("                 WHERE i.mnr_cd = imnr.mnr_cd AND i.itm_cd IN (").Append(commaDelimItems).Append(")")
        sqlPOSRelSkus.Append("                 GROUP BY imnr.mjr_cd, i.mnr_cd ) ")
        sqlPOSRelSkus.Append(" SELECT UNIQUE(itm_cd) FROM related_skus, cts ")
        sqlPOSRelSkus.Append("  WHERE rel_tp = 'MNR' AND rel_cd IN(cts.mnr_cd) ")
        sqlPOSRelSkus.Append("     OR rel_tp = 'MJR' AND rel_cd IN(cts.mjr_cd) ")

        Try
            dbConnection.Open()

            ' retrieves the E1 Related Items
            dbCommand.CommandText = sqlE1RelSkus.ToString
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            While (dbReader.Read())
                relatedItemCodes.Add(dbReader("itm_cd"))
            End While

            ' retrieves the POS Related items
            dbCommand.CommandText = sqlPOSRelSkus.ToString
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            While (dbReader.Read())
                'avoids adding duplicate items
                If (Not relatedItemCodes.Contains(dbReader("itm_cd"))) Then
                    relatedItemCodes.Add(dbReader("itm_cd"))
                End If
            End While
            dbReader.Close()

            ' eliminates from the list the SKUs that have the flag PREVENT_SALE = Y
            relatedItemCodes = GetSellableItems(relatedItemCodes)

            ' retrieves the ITEM details for the available related SKUs
            If (relatedItemCodes.Count > 0) Then
                relatedItemDetails = GetItemInfo(relatedItemCodes.ToArray(), storeCd)
            End If

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return relatedItemDetails
    End Function

    ''' <summary>
    ''' Fetches the Related Items having required details(ITM_CD, RELATED_ITM_CD, VSN, DES, RET_PRC)
    ''' for the list of item codes passed in.
    ''' </summary>
    ''' <param name="items">the items to retrieve Related items for</param>
    ''' <param name="storeCd">store where items are being processed, needed to get correct SPIFF value</param>
    ''' <returns>A datatable containing the Item details(ITM_CD, RELATED_ITM_CD, VSN, DES, RET_PRC)
    ''' for all the Related Items found</returns>
    ''' <remarks></remarks>
    Public Function GetRelatedItemsDetail(ByVal items() As String, ByVal storeCd As String) As DataTable
        Dim dtRelatedItms As New DataTable

        Dim sbSQL As New StringBuilder

        sbSQL.Append(" WITH CTS")
        sbSQL.Append("        AS (  SELECT   I.ITM_CD,")
        sbSQL.Append("                       I.MNR_CD,")
        sbSQL.Append("                       IMNR.MJR_CD,")
        sbSQL.Append("                       I.VSN,")
        sbSQL.Append("                       I.DES,")
        sbSQL.Append("                       I.RET_PRC")
        sbSQL.Append("                FROM   ITM I, INV_MNR IMNR")
        sbSQL.Append("               WHERE   I.MNR_CD = IMNR.MNR_CD")
        sbSQL.Append("                       AND I.ITM_CD IN (" & GetDelimitedString(items) & ")")
        sbSQL.Append("            GROUP BY   I.ITM_CD,")
        sbSQL.Append("                       IMNR.MJR_CD,")
        sbSQL.Append("                       I.MNR_CD,")
        sbSQL.Append("                       I.VSN,")
        sbSQL.Append("                       I.DES,")
        sbSQL.Append("                       I.RET_PRC)")
        sbSQL.Append(" SELECT   DISTINCT CTS.ITM_CD,")
        sbSQL.Append("                   RELATED_SKUS.ITM_CD AS RELATED_ITM_CD,")
        sbSQL.Append("                   CTS.VSN,")
        sbSQL.Append("                   CTS.DES,")
        sbSQL.Append("                   CTS.RET_PRC")
        sbSQL.Append("   FROM      RELATED_SKUS")
        sbSQL.Append("          JOIN")
        sbSQL.Append("             CTS")
        sbSQL.Append("          ON RELATED_SKUS.ITM_CD = CTS.ITM_CD")
        sbSQL.Append("  WHERE   REL_TP = 'MNR' AND REL_CD IN (CTS.MNR_CD)")
        sbSQL.Append("          OR REL_TP = 'MJR' AND REL_CD IN (CTS.MJR_CD)")
        sbSQL.Append(" UNION")
        sbSQL.Append(" SELECT   DISTINCT I2RS.ITM_CD,")
        sbSQL.Append("                   I2RS.RELATED_ITM_CD,")
        sbSQL.Append("                   ITM.VSN,")
        sbSQL.Append("                   ITM.DES,")
        sbSQL.Append("                   ITM.RET_PRC")
        sbSQL.Append("   FROM      ITM2RSKU I2RS")
        sbSQL.Append("          JOIN")
        sbSQL.Append("             itm")
        sbSQL.Append("          ON I2RS.RELATED_ITM_CD = ITM.ITM_CD")
        sbSQL.Append("  WHERE   I2RS.ITM_CD IN (" & GetDelimitedString(items) & ")")

        Dim oraConn As OracleConnection = GetConnection()
        Dim oraCmd As OracleCommand = DisposablesManager.BuildOracleCommand(sbSQL.ToString, oraConn)

        Dim oAdp As OracleDataAdapter

        Try
            oraConn.Open()

            oAdp = DisposablesManager.BuildOracleDataAdapter(oraCmd)
            oAdp.Fill(dtRelatedItms)
        Catch ex As Exception
            Throw
        Finally
            oAdp.Dispose()
            oraConn.Close()
        End Try

        Return dtRelatedItms
    End Function

    ''' <summary>
    ''' Extracts and returns the  Sort codes.
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetItemSortCodes(ByVal salesType As ArrayList)

        Dim delimitedItems As String
        Dim itmSql As StringBuilder = New StringBuilder()
        Dim ds As New DataSet
        Dim objSql As OracleCommand
        Try
            delimitedItems = String.Join("','", salesType.ToArray())
            If delimitedItems.EndsWith("'") Then
                'Do nothing
            Else
                delimitedItems = delimitedItems + "'"
            End If

            If delimitedItems.StartsWith("'") Then
                'Do nothing
            Else
                delimitedItems = "'" + delimitedItems
            End If
        Catch ex As Exception
            'Dec = False
            Throw New Exception("SalesType cannot be empty / Array list contains invalid type", ex)
        End Try

        itmSql.Append(" select  isrt.itm_srt_cd as SortCode, isrt.DES Description,ITM_CD")
        itmSql.Append(" from ITM$itm_srt srt join itm_srt isrt on srt.ITM_SRT_CD = isrt.ITM_SRT_CD  ")
        itmSql.Append(" where  ITM_CD  in( ").Append(delimitedItems.ToUpper()).Append(") and DISPLAY_IN_POS  = 'Y'   ORDER BY case when upper(isrt.MTO) = 'Y' and upper(DISPLAY_IN_POS) = 'Y' then 1 else 0 end desc ")
        Try
            ds = New DataSet
            objSql = GetCommand(itmSql.ToString())
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  Sort codes in comma seperated string
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>string</returns>
    ''' <remarks></remarks>
    Public Function GetItemSortCodesInString(ByVal itemcds As ArrayList)

        Dim itmSortCodes As DataSet = New DataSet()
        Dim SortCodes As StringBuilder = New StringBuilder()
        itmSortCodes = GetItemSortCodes(itemcds)
        For Each element In itmSortCodes.Tables(0).Rows
            SortCodes.Append(element(0).ToString()).Append(" - ").Append(element(1).ToString()).Append(Environment.NewLine())
        Next
        Return SortCodes
    End Function

    ''' <summary>
    ''' Extracts and returns the  Item Details along with the sort code and description
    ''' </summary>
    ''' <param name="itemcds">Item Cd's</param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetItemDetailsWithSortCodes(ByVal salesType As ArrayList)

        Dim delimitedItems As String = String.Empty
        Dim itmSql As StringBuilder = New StringBuilder()
        Dim ds As New DataSet
        Dim objSql As OracleCommand

        Try
            delimitedItems = String.Join("','", salesType.ToArray())
            If delimitedItems.EndsWith("'") Then
                'Do nothing
            Else
                delimitedItems = delimitedItems + "'"
            End If

            If delimitedItems.StartsWith("'") Then
                'Do nothing
            Else
                delimitedItems = "'" + delimitedItems
            End If
            If delimitedItems.isEmpty() Then
                Throw New Exception("SalesType cannot be empty / Array list contains invalid type")
            End If
        Catch ex As Exception
            'Dec = False
            Throw New Exception("SalesType cannot be empty / Array list contains invalid type", ex)
        End Try

        itmSql.Append(" With CTS as  (  ")
        itmSql.Append(" SELECT   ")
        itmSql.Append(" ITM_CD, COMM_CD, SPIFF, VE_CD, VSN, DES, REPL_CST, ITM_TP_CD, RET_PRC, ADV_PRC, NVL(WARRANTABLE,'N') as WARRANTABLE, NVL(WARR_DAYS, 0) AS WARR_DAYS,  inventory, ")
        itmSql.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD, ','), ',') SortCode, ")
        itmSql.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_Desc, ','), ',') SortDescription, TREATABLE, SERIAL_TP, ")
        itmSql.Append(" MNR_CD,PU_DISC_PCNT,MANUAL_PRC,TYPECODE,MAX_DISC_PCNT,ITM_RET_PRC ")
        itmSql.Append(" FROM (  ")
        itmSql.Append(" SELECT NVL(isrt.ITM_SRT_CD,'') AS ITM_SRT_CD,NVL(isrt.ITM_SRT_CD,'')|| '-'|| NVL(isrt.des,'') AS ITM_SRT_Desc, t.SERIAL_TP ,t.Inventory, ")
        itmSql.Append(" t.ITM_CD, t.COMM_CD,  t.VE_CD, t.VSN, t.DES, t.REPL_CST,  ")
        itmSql.Append(" t.SPIFF, t.ITM_TP_CD, t.RET_PRC, t.ADV_PRC, NVL(t.WARRANTABLE,'N') as WARRANTABLE, NVL(t.WARR_DAYS, 0) AS WARR_DAYS, t.TREATABLE, ")
        itmSql.Append(" ROW_NUMBER () OVER (PARTITION BY t.ITM_CD  ORDER BY t.ITM_CD, case when upper(isrt.MTO) = 'Y' and upper(DISPLAY_IN_POS) = 'Y' then 1 else 0 end desc) rnum,")
        itmSql.Append(" COUNT(1) over(PARTITION BY t.ITM_CD) tot,T.MNR_CD AS MNR_CD,NVL(T.PU_DISC_PCNT,0) AS PU_DISC_PCNT,")
        itmSql.Append(" NVL (ret_prc, 0) AS MANUAL_PRC, T.ITM_TP_CD AS TYPECODE, T.MAX_DISC_PCNT,T.ret_prc as ITM_RET_PRC   ")
        itmSql.Append(" FROM  itm T ")
        itmSql.Append(" left join ITM$itm_srt srt on srt.ITM_CD = t.itm_cd  left join itm_srt isrt on  srt.ITM_SRT_CD = isrt.ITM_SRT_CD  and upper(DISPLAY_IN_POS) = 'Y' ")
        itmSql.Append(" where upper(t.itm_cd) in(" + delimitedItems.ToUpper() + ")")
        itmSql.Append(" )   ")
        itmSql.Append(" WHERE rnum = tot   ")
        itmSql.Append(" START WITH rnum = 1   ")
        itmSql.Append(" CONNECT BY PRIOR rnum = rnum -1   ")
        itmSql.Append(" AND PRIOR ITM_CD = ITM_CD   ")
        itmSql.Append(" )SELECT * FROM CTS ")

        Try
            ds = New DataSet
            objSql = GetCommand(itmSql.ToString)
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  items details from SOM 
    ''' </summary>
    ''' <param name="salesType">Sales Order Types</param>
    ''' <param name="storeCd">the store to be used for SPIFF retrieval</param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetItemDetailsWithSortCodesForSOM(ByVal salesType As ArrayList, ByVal storeCd As String)

        Dim delimitedItems As String = String.Empty
        Dim itmSql As StringBuilder = New StringBuilder()
        Dim ds As New DataSet
        Dim objSql As OracleCommand
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")

        Try
            delimitedItems = String.Join("','", salesType.ToArray())
            If delimitedItems.EndsWith("'") Then
                'Do nothing
            Else
                delimitedItems = delimitedItems + "'"
            End If

            If delimitedItems.StartsWith("'") Then
                'Do nothing
            Else
                delimitedItems = "'" + delimitedItems
            End If
            If delimitedItems.isEmpty() Then
                Throw New Exception("SalesType cannot be empty / Array list contains invalid type")
            End If
        Catch ex As Exception
            'Dec = False
            Throw New Exception("SalesType cannot be empty / Array list contains invalid type", ex)
        End Try

        itmSql.Append(" With CTS as  (  ")
        itmSql.Append(" SELECT  ITM_CD, COMM_CD, SPIFF, VE_CD, VSN, DES, REPL_CST,  ")
        itmSql.Append(" ITM_TP_CD, RET_PRC, DROP_CD, DROP_DT, ADV_PRC, SERIAL_TP, inventory, point_size,  ")
        itmSql.Append(" WARRANTABLE, WARR_DAYS,MAX_DISC_PCNT, MNR_CD,")
        itmSql.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD, ','), ',')  SortCode,  ")
        itmSql.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_Desc, ','), ',') SortDescription  ")
        itmSql.Append(" FROM (  ")
        itmSql.Append(" SELECT  case when UPPER (DISPLAY_IN_POS) = 'Y' then NVL (isrt.ITM_SRT_CD, '') else '' end ITM_SRT_CD,")
        itmSql.Append(" case when UPPER (DISPLAY_IN_POS) = 'Y' then NVL (isrt.ITM_SRT_CD, '')|| '-'|| NVL (isrt.des, '')else '' end ITM_SRT_Desc,")
        itmSql.Append(" t.ITM_CD, t.COMM_CD, t.SPIFF, t.VE_CD, t.VSN, t.DES, t.REPL_CST,  t.ITM_TP_CD, t.RET_PRC, t.DROP_CD, t.DROP_DT, t.ADV_PRC, NVL(t.SERIAL_TP,'') AS SERIAL_TP, t.inventory, t.point_size, NVL(t.WARRANTABLE,'N') as WARRANTABLE, NVL(t.WARR_DAYS, 0) AS WARR_DAYS,MAX_DISC_PCNT, MNR_CD, ")
        itmSql.Append("  ROW_NUMBER () OVER (PARTITION BY t.ITM_CD   ORDER BY t.ITM_CD,  case when upper(isrt.MTO) = 'Y' and upper(DISPLAY_IN_POS) = 'Y' then 1 else 0 end desc) rnum,")
        itmSql.Append(" COUNT(1) over(PARTITION BY t.ITM_CD) tot ")
        itmSql.Append(" FROM  itm T ")
        itmSql.Append(" left join ITM$itm_srt srt on srt.ITM_CD = t.itm_cd  left join itm_srt isrt on  srt.ITM_SRT_CD = isrt.ITM_SRT_CD  ")
        itmSql.Append(" where upper(t.itm_cd) in(" + delimitedItems.ToUpper() + ") ")
        itmSql.Append(" ) ")
        itmSql.Append(" WHERE rnum = tot ")
        itmSql.Append(" START WITH rnum = 1 ")
        itmSql.Append(" CONNECT BY PRIOR rnum = rnum -1 ")
        itmSql.Append(" AND PRIOR ITM_CD = ITM_CD")
        itmSql.Append(" )")
        itmSql.Append(" SELECT  i.ITM_CD, COMM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF, VE_CD, VSN, DES, REPL_CST, ITM_TP_CD, RET_PRC, DROP_CD, DROP_DT, ")
        itmSql.Append(" ADV_PRC, SERIAL_TP, INVENTORY, POINT_SIZE, WARRANTABLE, WARR_DAYS,MAX_DISC_PCNT, MNR_CD, SORTCODE, SORTDESCRIPTION")
        itmSql.Append(" FROM   CTS i ")
        itmSql.Append(" LEFT JOIN  " + spiffTable + "  sp ON I.ITM_CD = SP.ITM_CD AND TO_DATE(sysdate)  BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT) ")
        itmSql.Append(If(SysPms.isSpiffBySKU, " ", (" AND STORE_CD = '" & storeCd & "' ")))
        Try
            ds = New DataSet
            objSql = GetCommand(itmSql.ToString)
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Extracts prevent = Y OR N  from itm table for the list of SKU is entered by user.
    ''' </summary>
    ''' <param name="itm_cd">oParam</param>
    ''' <returns>Dataset</returns>
    Public Function GetPreventedValidandInvalidItemsForsalesExchange(ByVal sku As String) As DataSet

        Dim sql As StringBuilder = New StringBuilder()
        Dim ds As New DataSet
        'Dim objSql As OracleCommand
        Dim param As String = ""
        sql.Append(" select itm.itm_cd")
        sql.Append(" , CASE WHEN (itm.drop_dt <= sysdate) AND UPPER(d.prevent_sale) = 'Y' then 'Y' else 'N' end PREVENT ")
        sql.Append(" FROM  itm itm left join drop_cd D on  D.DROP_CD=itm.DROP_CD ")
        sql.Append(" where upper(itm.itm_cd) in (")
        Dim lc As Int32 = 0
        Dim items As String() = sku.Split(",")
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand()

        'to prevent SQL injection we are following this looping is done
        For Each itm In items
            param = ":itm" & lc
            sql.Append("UPPER(").Append(param).Append(")").Append(If(lc = items.Length - 1, "", ","))
            lc = lc + 1
        Next
        sql.Append(")") ' Close of IN
        objSql = GetCommand(sql.ToString())
        lc = 0
        For Each itm In items
            param = "itm" & lc
            param = param.ToUpper
            Dim oPram As OracleParameter = New OracleParameter()
            oPram.ParameterName = param
            oPram.Direction = ParameterDirection.Input
            oPram.DbType = OracleType.VarChar
            oPram.Value = itm.ToUpper
            objSql.Parameters.Add(oPram)
            param = ":itm" & lc
            lc = lc + 1
        Next

        Try
            ds = New DataSet
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Getting the input item details from item table
    ''' </summary>
    ''' <param name="items">Multiple SKUs as a String Array</param>
    ''' <returns>Data Table of the items detail</returns>
    ''' <remarks></remarks>
    Public Function GetItemsDetail(items As String()) As DataTable
        Dim dtItem As New DataTable

        Dim sbSql As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbConnection As OracleConnection = GetConnection()
        Dim daOra As OracleDataAdapter

        sbSql.Append("SELECT   ITM_CD,")
        sbSql.Append("         COMM_CD,")
        sbSql.Append("         SPIFF,")
        sbSql.Append("         VE_CD,")
        sbSql.Append("         VSN,")
        sbSql.Append("         DES,")
        sbSql.Append("         REPL_CST,")
        sbSql.Append("         SPIFF,")
        sbSql.Append("         ITM_TP_CD,")
        sbSql.Append("         RET_PRC,")
        sbSql.Append("         ADV_PRC,")
        sbSql.Append("         NVL (SERIAL_TP, '') AS SERIAL_TP,")
        sbSql.Append("         INVENTORY,")
        sbSql.Append("         POINT_SIZE,")
        sbSql.Append("         NVL (WARRANTABLE, 'N') AS WARRANTABLE,")
        sbSql.Append("         NVL (WARR_DAYS, 0) AS WARR_DAYS,")
        sbSql.Append("         MAX_DISC_PCNT")
        sbSql.Append("  FROM   ITM")
        sbSql.Append(" WHERE   ITM_CD IN (")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(String.Empty, dbConnection)
            dbCommand.Parameters.Clear()

            Dim cnt As Integer = 0
            For Each item As String In items
                If cnt > 0 Then sbSql.Append(", ")

                sbSql.Append(" :ITM_CD" & cnt.ToString())

                dbCommand.Parameters.Add("ITM_CD" & cnt.ToString(), OracleType.VarChar).Direction = ParameterDirection.Input
                dbCommand.Parameters("ITM_CD" & cnt.ToString()).Value = item.Trim().ToUpper()

                cnt = cnt + 1
            Next

            sbSql.Append(")")

            dbCommand.CommandText = sbSql.ToString()
            daOra = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            daOra.Fill(dtItem)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtItem
    End Function

    ''' <summary>
    ''' This method returns the SKU price.
    ''' </summary>
    ''' <param name="itemCd">Item code for which the price is to be queried.</param>
    ''' <returns></returns>
    Public Function GetSKUPrice(ByVal itemCd As String) As Double

        Dim sqlSB As String = String.Empty
        Dim retailPrice As Double = 0.0
        sqlSB = " SELECT   RET_PRC  FROM ITM  WHERE  ITM_CD =  '" & itemCd.Trim().ToUpper() & "'"


        Using oraConn As OracleConnection = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString), objSql As OracleCommand = New OracleCommand("", oraConn)
            oraConn.Open()
            Dim da = DisposablesManager.BuildOracleDataAdapter(sqlSB.ToString(), oraConn)
            Dim dsRetPrice As DataSet = New DataSet()
            da.Fill(dsRetPrice)
            retailPrice = dsRetPrice.Tables(0).Rows(0)("RET_PRC")
        End Using
        Return retailPrice
    End Function

End Class
