﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Linq
Imports StringUtils
Imports OrderUtils

Public Class SalesDac
    Inherits BaseDac

    Private theInventoryDac As InventoryDac

    ''' <summary>
    '''  Saves the marketing codes and categories, that were used for the order
    ''' </summary>
    ''' <param name="dbAtomicCommand">command that is part of a single transaction</param>
    ''' <param name="writtenDt">the SO written date </param>
    ''' <param name="storeCd">the store code</param>
    ''' <param name="soSeqNum">the SO sequence number</param>
    ''' <param name="mktCat">the marketing category</param>
    ''' <param name="mktSubCat">the marketing subcategory</param>
    Public Sub SaveSOMarketingInfo(ByRef dbAtomicCommand As OracleCommand,
                                   ByVal writtenDt As String,
                                   ByVal storeCd As String,
                                   ByVal soSeqNum As String,
                                   ByVal mktCat As String,
                                   ByVal mktSubCat As String)

        Dim mktCategories(), mktCodes() As String
        Dim sql As String
        sql = "INSERT INTO SO$MKT_CAT (SO_WR_DT, SO_STORE_CD,  SO_SEQ_NUM, MKT_CD, MKT_CAT_CD) "
        sql = sql & "VALUES(:SO_WR_DT, :SO_STORE_CD, :SO_SEQ_NUM, :MKT_CD, :MKT_CAT_CD) "

        dbAtomicCommand.CommandText = sql
        dbAtomicCommand.Parameters.Add(":SO_WR_DT", OracleType.DateTime)
        dbAtomicCommand.Parameters.Add(":SO_STORE_CD", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":SO_SEQ_NUM", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":MKT_CD", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":MKT_CAT_CD", OracleType.VarChar)

        dbAtomicCommand.Parameters(":SO_WR_DT").Value = FormatDateTime(writtenDt, DateFormat.ShortDate)
        dbAtomicCommand.Parameters(":SO_STORE_CD").Value = storeCd
        dbAtomicCommand.Parameters(":SO_SEQ_NUM").Value = soSeqNum

        'all the marketing categories and codes are stored as a single, comma-delimited string
        'So, each one has to be persisted as a single record
        mktCodes = Split(mktSubCat, ",")
        mktCategories = Split(mktCat, ",")

        For i As Integer = 0 To mktCategories.Length - 1

            If (mktCategories(i).isNotEmpty) Then
                dbAtomicCommand.Parameters(":MKT_CD").Value = mktCodes(i)
                dbAtomicCommand.Parameters(":MKT_CAT_CD").Value = mktCategories(i)
                dbAtomicCommand.ExecuteNonQuery()
            End If

        Next
        dbAtomicCommand.Parameters.Clear()

    End Sub

    ''' <summary>
    ''' Finds out if a record exists for the document passed in, in the SO_ASP table
    ''' </summary>
    ''' <param name="delDocNum">the document number to find</param>
    ''' <returns>true if document exists in the database, false otherwise</returns>
    Public Function ExistsInSO_ASP(ByVal delDocNum As String) As Boolean

        Dim existsInDB As Boolean = False
        Dim dbSql As String = "SELECT DEL_DOC_NUM FROM SO_ASP WHERE DEL_DOC_NUM = :delDocNum"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbCommand.Parameters(":delDocNum").Value = delDocNum

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                existsInDB = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return existsInDB

    End Function

    ''' <summary>
    ''' Obtains the name of the invoice corresponding to the store passed in, 
    ''' as defined in the INVOICE_ADMIN table
    ''' </summary>
    ''' <param name="storeCd">the store code to find the invoice name for</param>
    ''' <returns>
    ''' the invoice name for the store; or if none assigned, the default invoice
    ''' </returns>
    Public Function GetDefaultInvoiceName(ByVal storeCd As String) As String

        Dim invoiceName As String = ""
        Dim dbSql As String = "SELECT store_cd, invoice_file, default_file FROM invoice_admin WHERE store_cd= :storeCd"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters(":storeCd").Value = storeCd

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                If dbReader.Item("invoice_file").ToString & "" = "" Then
                    invoiceName = dbReader.Item("default_file").ToString
                Else
                    invoiceName = dbReader.Item("invoice_file").ToString
                End If
            End If

            If String.IsNullOrEmpty(invoiceName) Then
                dbSql = "SELECT store_cd, invoice_file, default_file FROM invoice_admin"
                dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    invoiceName = dbReader.Item("default_file").ToString
                End If
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return invoiceName
    End Function


    ''' <summary>
    ''' Obtains the name of the promo-addendum invoice corresponding to the promotion and
    ''' finance company passed in store passed in, as defined in the PROMO_ADDENDUMS table
    ''' </summary>
    ''' <param name="promoCd">a finance promotion code</param>
    ''' <param name="finCo">a finance company code</param>
    ''' <param name="wdr">true if WDR invoice should be returned; false otherwise</param>
    ''' <returns>the promo-addendum invoice name</returns>
    Public Function GetAddendumInvoiceName(ByVal promoCd As String, ByVal finCo As String, ByVal wdr As Boolean) As String

        Dim invoiceName As String = ""
        Dim dbSql As String = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD= :promoCd AND AS_CD= :finCo"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim dbReader As OracleDataReader
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":promoCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":finCo", OracleType.VarChar)
            dbCommand.Parameters(":promoCd").Value = promoCd
            dbCommand.Parameters(":finCo").Value = finCo

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read) Then
                If wdr = True Then
                    invoiceName = dbReader.Item("INVOICE_NAME_WDR").ToString
                Else
                    invoiceName = dbReader.Item("INVOICE_NAME").ToString
                End If
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return invoiceName
    End Function

    ''' <summary>
    ''' Retrievesand returns all the order sort codes setup in the databse.
    ''' </summary>
    ''' <returns>a dataset of all the order sort codes</returns>
    Public Function GetOrderSortCodes() As DataSet

        Dim sortCodesDataSet As New DataSet
        Dim sql As String = "SELECT ord_srt_cd, des, ord_srt_cd || ' - ' || des as full_desc FROM ord_srt_maint WHERE ACTIVE='Y' ORDER BY ord_srt_cd"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbAdapter As OracleDataAdapter

        Try
            dbConnection.Open()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(sortCodesDataSet)

            If IsNothing(sortCodesDataSet.Tables(0)) OrElse sortCodesDataSet.Tables(0).Rows.Count = 0 Then
                sql = "SELECT ord_srt_cd, des, ord_srt_cd || ' - ' || des as full_desc FROM ord_srt_cd ORDER BY ord_srt_cd"
                dbCommand.CommandText = sql
                dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                dbAdapter.Fill(sortCodesDataSet)
            End If
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return sortCodesDataSet
    End Function

    ''' <summary>
    ''' Queries the TEMP_ITEM table to get the outlet IDs that have been added so far to the order
    ''' represented by the session ID passed in.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="sessionId">the session ID to query the outlet ids for</param>
    ''' <returns>A comma delimited list of Outlet IDs or an empty string if none found</returns>
    Public Function GetOutletIDsInUse(ByVal sessionId As String) As String

        Dim outletNumbers As String = ""
        Dim sql As String = "SELECT out_id FROM TEMP_ITM WHERE SESSION_ID = :sessionId"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader
        Dim outId As String = ""

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            Do While dbReader.Read()
                outId = dbReader.Item("OUT_ID").ToString
                If (Not String.IsNullOrWhiteSpace(outId)) Then
                    outletNumbers = outletNumbers & "'" & outId & "',"
                End If
            Loop

            'removes the last comma
            If (Not String.IsNullOrWhiteSpace(outletNumbers)) Then
                outletNumbers = Left(outletNumbers, Len(outletNumbers) - 1)
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return outletNumbers

    End Function

    ''' <summary>
    ''' Queries the TEMP_ITEM table to get the serial numbers that have been added so far to the order
    ''' represented by the session ID passed in.
    ''' This info is needed so that the display excludes these IDs from the list presented to the user
    ''' </summary>
    ''' <param name="sessionId">the session ID to query the serial numbers for</param>
    ''' <returns>A comma delimited list of Serial Numbers or an empty string if none found</returns>
    Public Function GetSerialNumbersInUse(ByVal sessionId As String) As String

        Dim serialNumbers As String = ""
        Dim sql As String = "SELECT serial_num FROM TEMP_ITM WHERE SESSION_ID = :sessionId"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader
        Dim sNum As String = ""

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            Do While dbReader.Read()
                sNum = dbReader.Item("serial_num").ToString
                If (Not String.IsNullOrWhiteSpace(sNum)) Then
                    serialNumbers = serialNumbers & "'" & sNum & "',"
                End If
            Loop

            'removes the last comma
            If (Not String.IsNullOrWhiteSpace(serialNumbers)) Then
                serialNumbers = Left(serialNumbers, Len(serialNumbers) - 1)
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return serialNumbers

    End Function

    ''' <summary>
    ''' Creates a new row into the TEMP_ITM table, using the details passed in.   This method should
    ''' be called when after performing a reservation, either the item is partially reserved or it is
    ''' reserved from multiple store/locations, and therefore in order to properly represent these
    ''' reservations, new lines have to be created.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    ''' <param name="lnId">the line id to obtain the duplicate details from (aka row_id)</param>
    ''' <param name="qtyOnNew">the quantity for the new line</param>
    ''' <param name="storeCd">store where this line was reserved from or empty if line is not reserved</param>
    ''' <param name="locCd">location where this line was reserved from or empty if line is not reserved</param>
    ''' <param name="resId">the reservation ID, is line is reserved</param>
    ''' <returns>the new line id for the row created during this call</returns>
    ''' <remarks></remarks>
    Public Function InsertTempItemForInvSplit(ByVal sessionId As String, ByVal lnId As String,
                                              ByVal qtyOnNew As Double,
                                              ByVal storeCd As String, ByVal locCd As String,
                                              ByVal resId As String) As String
        Dim newLnId As String = ""
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder()

        sql.Append("INSERT INTO temp_itm (session_id, itm_cd, qty, ve_cd, mnr_cd, cat_cd, ret_prc, orig_prc, manual_prc, vsn, des, new_special_order, ")
        sql.Append("warr_itm_link, warr_row_link, warrantable, siz, finish, cover, grade, style_cd, disc_amt, itm_tp_cd, treatable, ")
        sql.Append("treated, related_complete, treated_by, store_cd,  loc_cd, take_with, avail_dt, del_pts, cost, comm_cd, so_ln_no, ")
        sql.Append("tax_cd, taxable_amt, tax_pct, spiff, package_parent, leave_carton, slsp1, slsp2, slsp1_pct, slsp2_pct, serial_tp, ")
        sql.Append("bulk_tp_itm, po_cd, tax_amt, id_num, orig_so_ln_seq, res_id) ")
        sql.Append("SELECT session_id, itm_cd, :qtyOnNew, ve_cd, mnr_cd, cat_cd, ret_prc, orig_prc, manual_prc, vsn, des, new_special_order, ")
        sql.Append("warr_itm_link, warr_row_link, warrantable, siz, finish, cover, grade, style_cd, disc_amt, itm_tp_cd, treatable, ")
        sql.Append("treated, related_complete, treated_by, :storeCd,  :locCd, take_with, avail_dt, del_pts, cost, comm_cd, so_ln_no, ")
        sql.Append("tax_cd, taxable_amt, tax_pct, spiff, package_parent, leave_carton, slsp1, slsp2, slsp1_pct, slsp2_pct, serial_tp, ")
        sql.Append("bulk_tp_itm, po_cd, tax_amt, id_num, orig_so_ln_seq, :resId ")
        sql.Append("FROM temp_itm WHERE session_id = :sessionId AND row_id = :lnId ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString

            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters.Add(":lnId", OracleType.VarChar)
            dbCommand.Parameters.Add(":qtyOnNew", OracleType.Number)
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":resId", OracleType.Number)

            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.Parameters(":lnId").Value = lnId
            dbCommand.Parameters(":qtyOnNew").Value = qtyOnNew
            dbCommand.Parameters(":storeCd").Value = storeCd
            dbCommand.Parameters(":locCd").Value = locCd
            dbCommand.Parameters(":resId").Value = If(storeCd.isNotEmpty AndAlso resId.isNotEmpty, CInt(resId), DBNull.Value)  'if no store, then no reservation exists
            dbCommand.ExecuteNonQuery()

            dbCommand.Parameters.Clear()
            dbCommand.CommandText = "SELECT MAX(row_id) AS idOfNewRow FROM temp_itm WHERE session_id = :sessionId "
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                newLnId = CInt(dbReader("idOfNewRow").ToString)
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return newLnId

    End Function

    ''' <summary>
    ''' Updates the TEMP_ITEM table by setting the values passed in in the row that matches the
    ''' row id value passed in
    ''' </summary>
    ''' <param name="aRowId">the ROW_ID value, not to be confused with ORACLEs rowId</param>
    ''' <param name="qty">the quantity to be persisted</param>
    ''' <param name="storeCd">store code to be persisted</param>
    ''' <param name="locCd">location code to be persisted</param>
    ''' <param name="serialNum">serial number to be persisted</param>
    ''' <param name="outId">outlet ID to be persisted</param>
    ''' <param name="resId">the reservation ID assigned to this item</param>
    ''' <param name="poCd">the PO cd to be persisted</param>
    ''' <param name="poLn">the PO Line to link to</param>
    Public Sub UpdateTempItmTable(ByVal aRowId As String,
                                  ByVal qty As String,
                                  ByVal storeCd As String,
                                  ByVal locCd As String,
                                  ByVal serialNum As String,
                                  ByVal outId As String,
                                  ByVal resId As String,
                                  ByVal poCd As String,
                                  ByVal poLn As String)
        Dim sql As New StringBuilder
        sql.Append("UPDATE temp_itm ")
        sql.Append("SET qty = :qty, store_cd = :storeCd, loc_cd = :locCd, serial_num = :serialNum, ")
        sql.Append("    out_id = :outId, res_id = :resId,  po_Cd = :poCd, po_ln = :poLn  ")
        sql.Append("WHERE row_id = :aRowId")
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql.ToString, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":qty", OracleType.Number)
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":locCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":serialNum", OracleType.VarChar)
            dbCommand.Parameters.Add(":outId", OracleType.VarChar)
            dbCommand.Parameters.Add(":resId", OracleType.VarChar)
            dbCommand.Parameters.Add(":aRowId", OracleType.Number)
            dbCommand.Parameters.Add(":poCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":poLn", OracleType.Number)

            dbCommand.Parameters(":qty").Value = If(qty.isEmpty, System.DBNull.Value, CDbl(qty))
            dbCommand.Parameters(":storeCd").Value = If(storeCd.isEmpty, System.DBNull.Value, storeCd)
            dbCommand.Parameters(":locCd").Value = If(locCd.isEmpty, System.DBNull.Value, locCd)
            dbCommand.Parameters(":serialNum").Value = If(serialNum.isEmpty, System.DBNull.Value, serialNum)
            dbCommand.Parameters(":outId").Value = If(outId.isEmpty, System.DBNull.Value, outId)
            dbCommand.Parameters(":resId").Value = If(storeCd.isNotEmpty AndAlso resId.isNotEmpty, resId, System.DBNull.Value)  'no store, then no reservation exists
            dbCommand.Parameters(":aRowId").Value = If(CInt(aRowId) = 0, System.DBNull.Value, CInt(aRowId))
            dbCommand.Parameters(":poCd").Value = If(poCd.isEmpty, System.DBNull.Value, poCd)
            dbCommand.Parameters(":poLn").Value = If(poLn.isEmpty, System.DBNull.Value, CInt(poLn))
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM and all related tables that match the sessionID passed in.
    ''' This operation clears the 'temp' order from the system, including payments, warranties, discounts, etc.
    ''' </summary>
    ''' <param name="dbCommand">the database command object to queue the delete operations on</param>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    Public Function DeleteTempOrder(ByRef dbCommand As OracleCommand, ByVal sessionId As String) As Boolean
        Try
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            'NOTE: the Payment table has column name as SESSIONID, whereas all other are SESSION_ID
            dbCommand.CommandText = "DELETE FROM payment WHERE sessionid = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM email_templates WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM package_breakout WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM cust_info WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM multi_disc WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM temp_warr WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM temp_master WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            dbCommand.CommandText = "DELETE FROM temp_itm WHERE session_id = :sessionId"
            dbCommand.ExecuteNonQuery()

            ' Daniela remove temp_item_url data
            'Feb 28 perforamnce fix
            'dbCommand.CommandText = "DELETE FROM WEB_CACHE WHERE session_id = :sessionId"
            'dbCommand.ExecuteNonQuery()

            dbCommand.Parameters.Clear()
        Catch ex As Exception
            Throw
        End Try

        Return True
    End Function

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM and all related tables that match the sessionID passed in.
    ''' This operation clears the 'temp' order from the system, including payments, warranties, discounts, etc.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    Public Function DeleteTempOrder(ByVal sessionId As String) As Boolean

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction()
            DeleteTempOrder(dbCommand, sessionId)
            dbCommand.Transaction.Commit()

        Catch ex As Exception
            dbCommand.Transaction.Rollback()
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return True
    End Function

    ''' <summary>
    ''' Deletes ALL records from CRM.TEMP_ITM that match the sessionID passed in.
    ''' This operation clears only the lines for a temp order.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp items</param>
    Public Sub DeleteTempItems(ByVal sessionId As String)

        Dim dbSql As String = "DELETE FROM temp_itm WHERE session_id = :sessionId"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Fetches from TEMP_ITM, the last ROW_ID value that indicates the last line that
    ''' was added for the order represented by the sessionID passed in
    ''' </summary>
    ''' <param name="sessionId">the sessionID to retrieve the discounts for</param>
    ''' <returns>the ID for the last line on the given order</returns>
    Public Function GetLastLnSeqNum(ByVal sessionId As String) As String

        Dim lastLnSeq As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As String = "SELECT MAX(ROW_ID) AS LN_SEQ FROM temp_itm WHERE session_id = :sessionId "

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.CommandText = dbSql
            lastLnSeq = Convert.ToString(dbCommand.ExecuteScalar())

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return lastLnSeq
    End Function

    ''' <summary>
    ''' Clears the avail date for an ARS store in the tmp_itm table based on the session id passed-in.
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <remarks>This method currently clears the avail_dt column value.</remarks>
    Public Sub ClearARSAvailDate(ByVal sessionId As String)

        Dim sql As New StringBuilder
        sql.Append("UPDATE temp_itm ")
        sql.Append("SET avail_dt = ''  ")
        sql.Append("WHERE session_id = :sessId")
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql.ToString, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessId", OracleType.VarChar)
            dbCommand.Parameters(":sessId").Value = sessionId
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

    End Sub


    ''' <summary>
    ''' Retrieves information for the package header and its components and creates temp_itm records for each. 
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    Public Function AddPkgCmpntsToTempItm(ByVal headerInfo As SoHeaderDtc,
                                          ByVal pkgSku As String,
                                          ByVal sessID As String, ByVal storeCd As String, ByRef errorMessage As String,
                                          Optional ByVal includeARSAvailDate As Boolean = True) As DataTable

        Dim pkgHeaderDataSet As DataSet
        Dim pkgHeaderDataTable As DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand
        Dim point_size As String
        Dim comm_cd As String
        Dim tax_cd As String
        Dim tax_pct As Double
        Dim taxable_amt As Double
        Dim Calc_Ret As Double
        Dim warrantable As String
        Dim isInventory As Boolean
        Dim dtSelItm As DataTable
        Dim SPIFF As String
        Try
            dbConnection.Open()

            pkgSku = IIf(Right(pkgSku, 1) = ",", Left(pkgSku, Len(pkgSku) - 1), pkgSku)
            pkgSku = pkgSku.Replace("'", String.Empty)

            '** Get the Row id of the package header
            Dim pkgRowId As Integer = GetPkgHeaderRowId(sessID, pkgSku, dbConnection)

            '** Get the item details for the package header with its list of components
            pkgHeaderDataSet = GetPkgHeaderData(pkgSku, storeCd, dbConnection)
            pkgHeaderDataTable = pkgHeaderDataSet.Tables(0)

            '** Get all the detailed info for the pkg components by calling the API
            Dim expression As String
            Dim foundRows() As DataRow
            Dim pkgCmpntsDataSet As DataSet = GetPkgComponentData(pkgSku, dbConnection, storeCd, errorMessage)
            ' MM-6305
            If (pkgCmpntsDataSet IsNot Nothing AndAlso pkgCmpntsDataSet.Tables.Count > 0 AndAlso pkgCmpntsDataSet.Tables(0).Rows.Count > 0) Then

                For Each dr As DataRow In pkgCmpntsDataSet.Tables(0).Rows

                    expression = "ITM_CD = '" + dr("ITM_CD").ToString + "'"
                    foundRows = pkgHeaderDataTable.Select(expression)
                    SPIFF = foundRows(0)("SPIFF").ToString
                    point_size = ""
                    tax_cd = ""
                    tax_pct = 0
                    taxable_amt = 0

                    dtSelItm = (From drPkgCmpt In pkgHeaderDataTable.AsEnumerable() Where drPkgCmpt.Field(Of String)("ITM_CD").ToUpper.Equals(dr("ITM_CD").ToString.ToUpper) Select drPkgCmpt).CopyToDataTable
                    isInventory = IIf("Y".Equals(dtSelItm.Rows(0).Item("INVENTORY").ToString), True, False)

                    ' TODO  - change to call HBCG_Utils.CreateTempItm if possible
                    If dtSelItm.Rows(0).Item("POINT_SIZE").ToString & "" <> "" Then
                        point_size = dtSelItm.Rows(0).Item("POINT_SIZE").ToString
                    End If

                    If dtSelItm.Rows(0).Item("COMM_CD").ToString & "" <> "" Then
                        comm_cd = dtSelItm.Rows(0).Item("COMM_CD").ToString
                    End If

                    ' Daniela tax_extempt fix
                    'If (TaxUtils.Determine_Taxability(dr("ITM_TP_CD").ToString, Date.Today, headerInfo.taxCd) = "Y" And String.IsNullOrEmpty(headerInfo.taxExemptCd)) Then
                    If (TaxUtils.Determine_Taxability(dr("ITM_TP_CD").ToString, Date.Today, headerInfo.taxCd) = "Y") Then

                        tax_cd = headerInfo.taxCd
                        tax_pct = TaxUtils.getTaxRate(dr("itm_tp_cd"), headerInfo.taxRates)

                        If ((ConfigurationManager.AppSettings("package_breakout").ToString + "").Equals("N") OrElse
                            (ConfigurationManager.AppSettings("package_breakout").ToString + "").Equals("C")) Then
                            taxable_amt = 0
                        Else
                            If Not String.IsNullOrEmpty(dr("curr_prc")) Then
                                Double.TryParse(dr("curr_prc"), taxable_amt)
                            End If
                        End If
                        Calc_Ret = taxable_amt * (tax_pct / 100)
                    Else  '2018 JDA dry Run added 
                        If ((ConfigurationManager.AppSettings("package_breakout").ToString + "").Equals("N") OrElse
                            (ConfigurationManager.AppSettings("package_breakout").ToString + "").Equals("C")) Then
                            taxable_amt = 0
                        Else
                            If Not String.IsNullOrEmpty(dr("curr_prc")) Then
                                Double.TryParse(dr("curr_prc"), taxable_amt)
                            End If
                            Calc_Ret = taxable_amt
                        End If
                    End If
                    'Calc_Ret = taxable_amt * (tax_pct / 100)
                    warrantable = IIf("Y".Equals(dtSelItm.Rows(0).Item("warrantable").ToString + ""), "Y", "N")

                    'calculate availability for ARS store, if any                
                    Dim availDate As Date
                    If includeARSAvailDate AndAlso isInventory AndAlso headerInfo.isARSEnabled AndAlso Not (headerInfo.isCashNCarry) Then

                        availDate = GetInventoryDac().CalcAvailARS(dr.Item("ITM_CD"),
                                                                headerInfo.puDel,
                                                                headerInfo.zoneCd,
                                                                dr.Item("alt_id_qty"),
                                                                headerInfo.puDelStore)
                    End If

                    Dim qry1 As New StringBuilder
                    'inserting package items here, need to add the sort codes here....
                    qry1.Append("INSERT INTO temp_itm (session_id,")
                    qry1.Append("ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC,MANUAL_PRC, ")
                    qry1.Append("VSN,DES,SIZ,")
                    qry1.Append("FINISH, COVER, GRADE,")
                    qry1.Append("STYLE_CD,ITM_tp_CD,TREATABLE,DEL_PTS,")
                    qry1.Append("COST, COMM_CD, tax_cd, tax_pct, taxable_amt,")
                    qry1.Append("SPIFF, QTY, PACKAGE_PARENT, SLSP1, SLSP1_PCT, TAX_AMT, ")
                    If headerInfo.slsp2 & "" <> "" Then
                        qry1.Append("SLSP2, SLSP2_PCT,")
                    End If
                    qry1.Append("BULK_TP_ITM, SERIAL_TP, warrantable, avail_dt)  values ('" & sessID & "',")

                    Dim qry2 As New StringBuilder
                    qry2.Append(qry1.ToString)
                    qry2.Append("'" & dr("ITM_CD") & "','" & dr("VE_CD") & "','" & dr("mnr_cd") & "','" & dr("cat_cd") & "', " & taxable_amt & ", :origPrc, :manualPrc,")
                    qry2.Append("'" & Replace(dr("vsn").ToString, "'", "''") & "','" & Replace(dr("des").ToString, "'", "''") & "','" & Replace(dr("siz").ToString, "'", "''") & "',")
                    qry2.Append("'" & Replace(dr("finish").ToString, "'", "''") & "','" & Replace(dr("cover").ToString, "'", "''") & "','" & Replace(dr("grade").ToString, "'", "''") & "','',")
                    qry2.Append("'" & dr("itm_tp_cd") & "','" & dr("treatable_flg") & "','" & point_size & "',")
                    qry2.Append("'" & dr("repl_cst") & "','" & comm_cd & "','" & tax_cd & "'," & tax_pct & ",'" & taxable_amt & "',")
                    qry2.Append("'" & dr("spiff") & "','" & dr("alt_id_qty") & "'," & pkgRowId & ",'" & headerInfo.slsp1 & "','" & headerInfo.slspPct1 & "',ROUND(" & Calc_Ret & ", " & TaxUtils.Tax_Constants.taxPrecision & "),")
                    If headerInfo.slsp2 & "" <> "" Then
                        qry2.Append("'" & headerInfo.slsp2 & "','" & headerInfo.slspPct2 & "',")
                    End If
                    qry2.Append("'" & dtSelItm.Rows(0).Item("BULK_TP_ITM").ToString & "','" & dtSelItm.Rows(0).Item("SERIAL_TP").ToString)
                    qry2.Append("','" & warrantable & "'," & " :avail_dt)")

                    dbCommand = DisposablesManager.BuildOracleCommand(qry2.ToString, dbConnection)
                    dbCommand.Parameters.Add(":avail_dt", OracleType.DateTime).Direction = ParameterDirection.Input
                    dbCommand.Parameters(":avail_dt").Value = IIf(availDate = DateTime.MinValue, DBNull.Value, availDate)
                    dbCommand.Parameters.Add(":origPrc", OracleType.Number).Direction = ParameterDirection.Input
                    dbCommand.Parameters(":origPrc").Value = taxable_amt
                    dbCommand.Parameters.Add(":manualPrc", OracleType.Number).Direction = ParameterDirection.Input
                    dbCommand.Parameters(":manualPrc").Value = taxable_amt
                    dbCommand.ExecuteNonQuery()

                Next
            End If
        Catch generatedExceptionName As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return pkgHeaderDataTable

    End Function


    ''' <summary>
    ''' Returns the row id of the passed-in package sku line in the temp_itm table.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="conn">the db connection to use for querying</param>
    ''' <returns>the unique row ID of the specified package sku</returns>
    Private Function GetPkgHeaderRowId(ByVal sessionId As String,
                                       ByVal pkgSku As String,
                                       ByRef conn As OracleConnection) As Integer

        Dim pkgRowId As Integer = 0
        Dim sql As String = "SELECT ROW_ID FROM TEMP_ITM WHERE SESSION_ID = :sessionId " &
                            "AND ITM_CD = :itmCd  AND PACKAGE_PARENT IS NULL ORDER BY ROW_ID DESC "

        Dim dbCommand As OracleCommand = GetCommand(sql, conn)
        Dim dbReader As OracleDataReader

        dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
        dbCommand.Parameters(":sessionId").Value = sessionId
        dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
        dbCommand.Parameters(":itmCd").Value = pkgSku

        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

        If dbReader.Read() Then
            pkgRowId = dbReader.Item("ROW_ID")
        End If

        Return pkgRowId

    End Function

    ''' <summary>
    ''' Retrieves ALL of the data in the temp-itm table
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    Public Function GetTempItmData(ByVal sessionId As String) As DataSet

        Dim tempItmDataset As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbAdapter As OracleDataAdapter
        Dim dbCommand As OracleCommand
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append(" SELECT *  FROM temp_itm WHERE session_id =  :sessId ")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sql.ToString, dbConnection)
            dbCommand.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("sessId").Value = sessionId

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(tempItmDataset)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return tempItmDataset
    End Function


    ''' <summary>
    ''' Retrieves  a subset of the data in the temp-itm table specifically when
    ''' a package components has been inserted previously.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    Public Function GetTempItemDataForPkgCmpnts(ByVal sessionId As String) As DataSet

        Dim tempItmDataset As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        'sql.Append("SELECT ti.ITM_CD, ti.COMM_CD,SUM(QTY) QTY, ti.RET_PRC, ti.VE_CD, ti.ITM_TP_CD, ti.VSN, ")
        sql.Append("SELECT ti.ITM_CD, ti.COMM_CD, QTY, ti.RET_PRC, ti.VE_CD, ti.ITM_TP_CD, ti.VSN, ")
        sql.Append("ti.DEL_PTS, ti.DES, ti.RET_PRC, ITM.INVENTORY, itm.mnr_cd, ITM.TREATABLE, ITM.WARRANTABLE ")
        sql.Append("FROM itm, TEMP_ITM ti ")
        sql.Append("WHERE ti.itm_cd = itm.itm_cd ")
        sql.Append("AND  SESSION_ID = :sessId ")
        'sql.Append("GROUP BY ti.ITM_CD, ti.COMM_CD, ti.RET_PRC, ti.VE_CD, ti.ITM_TP_CD, ti.VSN, ti.DEL_PTS, ")
        'sql.Append("ti.DES, ti.RET_PRC, ITM.INVENTORY, itm.mnr_cd, ITM.TREATABLE, ITM.WARRANTABLE ")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("sessId").Value = sessionId

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(tempItmDataset)
            dbAdapter.Dispose()
            'Remove the read items from the temp table
            sql.Clear()
            sql.Append("DELETE FROM TEMP_ITM WHERE SESSION_ID = :sessId")
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return tempItmDataset
    End Function

    ''' <summary>
    ''' Checks if the order passed-in is considered 'paid in full' as per the
    ''' rules specified in the custom stored procedure it calls.
    ''' </summary>
    ''' <param name="req">request object for paid in full determination</param>
    ''' <returns>a flag indicating if the specified order is considered 'paid in full' or not.</returns>
    Public Function IsPaidInFull(ByVal req As PaidInFullReq) As Boolean

        Dim isOrderPaidInFull As Boolean = False
        Dim sql As String = "bt_sales_util.is_paid_in_full"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure
            dbCommand.Parameters.Add("returnValue", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cust_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("order_type_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("fi_amt_i", OracleType.Number).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("fi_co_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("app_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input

            dbCommand.Parameters("del_doc_num_i").Value = req.delDocNum
            dbCommand.Parameters("cust_cd_i").Value = req.custCd
            dbCommand.Parameters("store_cd_i").Value = req.writtenStoreCd
            dbCommand.Parameters("order_type_cd_i").Value = req.ordTpCd
            dbCommand.Parameters("fi_amt_i").Value = IIf(IsNumeric(req.origFinAmt), req.origFinAmt, System.DBNull.Value)
            dbCommand.Parameters("fi_co_cd_i").Value = req.finCoCd
            dbCommand.Parameters("app_cd_i").Value = req.finApp
            dbCommand.ExecuteNonQuery()

            'get the return value from the stored function call
            isOrderPaidInFull = IIf(dbCommand.Parameters("returnValue").Value() = "Y", True, False)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isOrderPaidInFull

    End Function

    ''' <summary>
    ''' Checks if the order passed-in is considered 'paid in full' as per the
    ''' rules specified in the custom stored procedure it calls.
    ''' </summary>
    ''' <param name="delDocNum">the current del doc num</param>
    ''' <param name="ordTypeCd">the type of order such as 'SAL', 'CRM', 'MCR', etc.</param>
    ''' <param name="custCd">the current customer</param>
    ''' <returns>a flag indicating if the specified order is considered 'paid in full' or not.</returns>
    Public Function IsPaidInFullCustom(ByVal delDocNum As String, ByVal ordTypeCd As String, ByVal custCd As String) As Boolean

        Dim isOrderPaidInFull As Boolean = False
        Dim sql As String = "custom_paid.is_order_paid_in_full"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)

        Try
            dbConnection.Open()
            dbCommand.CommandType = CommandType.StoredProcedure
            dbCommand.Parameters.Add("returnValue", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue

            dbCommand.Parameters.Add("ord_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters.Add("cust_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input

            dbCommand.Parameters("ord_tp_cd_i").Value = ordTypeCd
            dbCommand.Parameters("del_doc_num_i").Value = delDocNum
            dbCommand.Parameters("cust_cd_i").Value = custCd
            dbCommand.ExecuteNonQuery()

            'get the retail price from the stored function call
            isOrderPaidInFull = IIf(dbCommand.Parameters("returnValue").Value() = "Y", True, False)

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return isOrderPaidInFull

    End Function

    ''' <summary>
    ''' This is method is to check whether the cash drawer is closed or not
    ''' </summary>
    ''' <param name="companyCode">company code</param>
    ''' <param name="storeCode">store code</param>
    ''' <param name="cashDrawerCode">cash drawer code</param>
    ''' <param name="postDate">post date</param>
    ''' <returns>True if the cash drawer is open, False otherwise</returns>
    Public Function IsCashDrawerActive(ByVal companyCode As String, ByVal storeCode As String,
                                       ByVal cashDrawerCode As String, ByVal postDate As Date) As Boolean

        Dim isDrawerActive As Boolean = False
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As StringBuilder = New StringBuilder()

        dbSql.Append("SELECT CLOSED FROM CSH_DWR_BAL ")
        dbSql.Append("WHERE CO_CD=:CO_CD AND STORE_CD=:STORE_CD AND CSH_DWR_CD=:CSH_DWR_CD ")
        dbSql.Append("AND POST_DT = TO_DATE(:POST_DT ,'mm/dd/RRRR')")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(dbSql.ToString)
            dbCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
            dbCommand.Parameters.Add(":STORE_CD", OracleType.VarChar)
            dbCommand.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
            dbCommand.Parameters.Add(":POST_DT", OracleType.VarChar)

            dbCommand.Parameters(":CO_CD").Value = UCase(companyCode)
            dbCommand.Parameters(":STORE_CD").Value = UCase(storeCode)
            dbCommand.Parameters(":CSH_DWR_CD").Value = cashDrawerCode
            dbCommand.Parameters(":POST_DT").Value = FormatDateTime(postDate, DateFormat.ShortDate)

            Dim drawerStatus As Object = dbCommand.ExecuteScalar()
            If (Not IsNothing(drawerStatus)) Then
                isDrawerActive = ("N".Equals(Convert.ToString(drawerStatus)))
            End If

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return isDrawerActive
    End Function

    ''' <summary>
    ''' Returns the max available date that there is in the temp_itm table for the current session id.
    ''' </summary>
    ''' <param name="sessionId">the current session id to use to retrieve the data</param>
    ''' <returns>the max avail date value, if any exist.</returns>
    Public Function GetMaxAvailDate(ByVal sessionId As String) As Date

        Dim maxDate As New Date
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append(" SELECT MAX (avail_dt) as MAX_AVAIL_DT ")
        sql.Append("   FROM temp_itm ")
        sql.Append("  WHERE session_id = :session_id ")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sql.ToString, dbConnection)
            dbCommand.Parameters.Add(":session_id", OracleType.VarChar)
            dbCommand.Parameters(":session_id").Value = sessionId

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                maxDate = IIf(IsDBNull(dbReader.Item("MAX_AVAIL_DT")), maxDate, dbReader.Item("MAX_AVAIL_DT"))
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            dbReader.Close()
            CloseResources(dbConnection, dbCommand)
        End Try

        Return maxDate

    End Function

    ''' <summary>
    ''' Retrieves from the SALES.SO_CONF_STAT table, all the records flagged as Active
    ''' </summary>
    ''' <returns>A set of confirmation status codes; or an empty dataset if none found</returns>
    Public Function GetConfirmationCodes() As DataSet

        Dim confirmationCodes As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()
        sql.Append("SELECT seq#, conf_stat_cd code, conf_stat_cd ||' - '|| des description, is_changeable, is_confirmed ")
        sql.Append("FROM so_conf_stat WHERE UPPER(is_active) = 'Y' ")
        sql.Append("ORDER BY seq# ASC")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString()
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(confirmationCodes)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return confirmationCodes
    End Function

    ''' <summary>
    ''' Retrieves from the SALES.SO_CONF_STAT table, the first Active record and 
    ''' returns its code.
    ''' </summary>
    ''' <returns>The first confirmation status code; or an empty string if none found</returns>
    Public Function GetFirstConfirmationCode() As String

        Dim confirmationCode As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder
        sql.Append("SELECT conf_stat_cd FROM so_conf_stat ")
        sql.Append("WHERE UPPER(is_active) = 'Y' AND ROWNUM < 2 ")
        sql.Append("ORDER BY SEQ# ASC")

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                confirmationCode = dbReader.Item("conf_stat_cd")
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return confirmationCode
    End Function


    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Retrieves the main 'ITEM' specific data for the passed-in package header sku and also its
    ''' list of components. This only contains some basic info about the components.
    ''' </summary>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="storeCd">the store code to be used for SPIFF retrieval</param>
    ''' <param name="conn">the db connection to use for querying</param>
    ''' <returns>the dataset with some of the item info for the  specified package components.
    ''' </returns>
    Private Function GetPkgHeaderData(ByVal pkgSku As String,
                                      ByVal storeCd As String,
                                      ByRef conn As OracleConnection) As DataSet

        Dim pkgDataSet = New DataSet
        Dim sqlSb = New StringBuilder
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")

        sqlSb.Append("SELECT ITM.ITM_CD, BULK_TP_ITM, SERIAL_TP, VE_CD, MNR_CD, CAT_CD, package.PKG_ITM_CD, ").Append(
           " NVL(package.RET_PRC,0) AS RET_PRC, PACKAGE.QTY, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ").Append(
           " ITM_TP_CD, TREATABLE, ITM.DROP_CD, ITM.DROP_DT, ITM.USER_QUESTIONS, POINT_SIZE, REPL_CST, ").Append(
           " COMM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF, NVL(warrantable,'N') AS warrantable, inventory ").Append(
           " FROM itm join package on package.cmpnt_itm_cd=itm.itm_cd ").Append(
           " LEFT JOIN " + spiffTable + " sp ON  package.cmpnt_itm_cd = SP.ITM_CD AND TO_DATE(sysdate) BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT) ").Append(
           If(SysPms.isSpiffBySKU, " ", (" AND STORE_CD = '" & storeCd & "' "))).Append(
           " WHERE pkg_itm_cd = :itmCd ").Append(
           " ORDER BY pkg_itm_cd, seq")

        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)
        dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
        dbCommand.Parameters(":itmCd").Value = pkgSku

        Dim dbAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
        dbAdapter.Fill(pkgDataSet)

        Return pkgDataSet

    End Function


    ''' <summary>
    ''' Retrieves all the details relating to the passed-in package sku and also it's components. It calls
    ''' an API to get the pakcage component information. 
    ''' </summary>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="conn">the db connection to use for querying</param>
    ''' <returns>the dataset with all the detailed item and other info for the
    '''          specified package sku and its components.</returns>
    Public Function GetPkgComponentData(ByVal pkgSku As String,
                                         ByRef conn As OracleConnection, ByVal store As String, ByRef errorMessage As String) As DataSet
        Try
            Dim compntsDataSet As New DataSet
            Dim dbAdapter As OracleDataAdapter
            Dim dbReader As OracleDataReader
            Dim retPrice As Double = 0
            Dim pkgSplitMethod As String = String.Empty
            Dim effectiveDate = Date.Now
            'This method will check for package price in promotion calender and store or  strore group level package price
            retPrice = GetPromoPrice(pkgSku, store, effectiveDate, pkgSplitMethod)

            Dim sql As StringBuilder = New StringBuilder()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            If IsNothing(retPrice) OrElse retPrice = 0 Then
                sql.Append("Select itm_cd ,nvl((SELECT PRC FROM   ITM$RET_CLNDR WHERE  ITM_CD    = :itmCd AND    STORE_CD  = :st")
                sql.Append(" AND    EFF_DT   <= TO_DATE(sysdate,'DD-MON-RR') AND    END_DT   >= TO_DATE(sysdate,'DD-MON-RR') AND ROWNUM = 1),")
                sql.Append(" nvl((SELECT SGRC.PRC FROM   STORE_GRP$STORE SGS,STORE_GRP SG,STORE_GRP_RET_CLNDR SGRC WHERE  ITM_CD = :itmCd")
                sql.Append(" AND EFF_DT <= TO_DATE(sysdate,'DD-MON-RR') AND END_DT >= TO_DATE(sysdate,'DD-MON-RR')")
                sql.Append(" AND SGRC.STORE_GRP_CD = SG.STORE_GRP_CD AND    SG.PRICING_STORE_GRP = 'Y' AND SG.STORE_GRP_CD = SGS.STORE_GRP_CD")
                sql.Append(" AND SGS.STORE_CD = :st AND ROWNUM = 1),I.ret_prc))  AS RET_PRC,")
                sql.Append(" ADV_PRC, PKG_SPLIT_METHOD from itm I where itm_cd = :itmCd")
                cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
                cmd.Parameters.Add(":itmCd", OracleType.VarChar)
                cmd.Parameters(":itmCd").Value = pkgSku
                cmd.Parameters.Add(":st", OracleType.VarChar)
                cmd.Parameters(":st").Value = store
                dbReader = DisposablesManager.BuildOracleDataReader(cmd)

                Do While dbReader.Read()
                    If IsNumeric(dbReader.Item("RET_PRC")) Then
                        retPrice = CDbl(dbReader.Item("RET_PRC").ToString)
                    End If

                    If Not (IsDBNull(dbReader.Item("PKG_SPLIT_METHOD"))) Then
                        pkgSplitMethod = dbReader.Item("PKG_SPLIT_METHOD")
                    End If
                Loop
            End If

            'cmd.Parameters.Clear()
            'cmd = DisposablesManager.BuildOracleCommand("bt_iface_sale.get_all_pkg_cmpnt_info", conn)
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("pkg_cmpnt_info_cur", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

            'cmd.Parameters.Add("pkg_itm_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
            'cmd.Parameters.Add("pkg_prc_i", OracleType.Number).Direction = ParameterDirection.Input
            'cmd.Parameters.Add("pkg_split_method_i", OracleType.NVarChar).Direction = ParameterDirection.Input

            'cmd.Parameters("pkg_itm_cd_i").Value = pkgSku
            'cmd.Parameters("pkg_prc_i").Value = retPrice
            'cmd.Parameters("pkg_split_method_i").Value = pkgSplitMethod

            'dbAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
            'dbAdapter.Fill(compntsDataSet)
            compntsDataSet = GetPkgComponentData(pkgSku, retPrice, pkgSplitMethod, conn, errorMessage)
            Return compntsDataSet
        Catch ex As OracleException
            'MM-6305
            Select Case ex.Code
                Case 20101
                    errorMessage = HBCG_Utils.FormatOracleException(ex.Message.ToString())
            End Select
        Catch ex2 As Exception
            Throw
        End Try

    End Function

    ''' <summary>
    ''' Finds out if the document line passed, has been linked to a PO already, if so returns the PO_CD
    ''' </summary>
    ''' <param name="delDocNo">the document number where the line belongs to</param>
    ''' <param name="delDocLnNo">the document line to be checked for PO links</param>
    ''' <returns>the PO_CD found for the document and line passed in; an empty string if no PO link is found</returns>
    Public Function GetPOLink(ByVal delDocNo As String, ByVal delDocLnNo As String) As List(Of String)

        Dim poLink As New List(Of String)
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbSql As String = "SELECT PO_CD, LN# FROM SO_LN$PO_LN WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN#"

        Try
            dbConnection.Open()
            dbCommand = GetCommand(dbSql, dbConnection)
            dbCommand.Parameters.Add(New OracleParameter(":DEL_DOC_NUM", OracleType.VarChar)).Value = delDocNo
            dbCommand.Parameters.Add(New OracleParameter(":DEL_DOC_LN#", OracleType.Number)).Value = delDocLnNo

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                poLink.Add(dbReader("PO_CD"))
                poLink.Add(dbReader("LN#"))
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return poLink
    End Function

    ''' <summary>
    ''' Creates a PO Link for the document line passed in
    ''' </summary>
    ''' <param name="oraCmd">the command object to be used, usually part of a single transaction</param>
    ''' <param name="strDocTpCd">the document type being processed</param>
    ''' <param name="strDelDocNo">the document number of the document being processed</param>
    ''' <param name="strDelDocLnNo">the line number for the line being processed</param>
    ''' <param name="dtPdTransfer">the date when this is taking place</param>
    ''' <param name="intQty">the quantity of the line being processed</param>
    ''' <param name="strPOCd">the PO code where the document line is linked to</param>
    ''' <param name="strPOLn">the PO line where the document line is linked to</param>
    Public Sub AddPOLink(ByRef oraCmd As OracleCommand, ByVal strDocTpCd As String,
                         ByVal strDelDocNo As String, ByVal strDelDocLnNo As String,
                         ByVal dtPdTransfer As Date, ByVal intQty As Integer,
                         ByVal strPOCd As String, ByVal strPOLn As String)
        Try
            oraCmd.CommandType = CommandType.StoredProcedure
            oraCmd.CommandText = "bt_po_link.insert_recs"

            oraCmd.Parameters.Clear()
            oraCmd.Parameters.Add(New OracleParameter("doc_tp_i", OracleType.VarChar)).Value = strDocTpCd
            oraCmd.Parameters.Add(New OracleParameter("doc_num_i", OracleType.VarChar)).Value = strDelDocNo
            oraCmd.Parameters.Add(New OracleParameter("doc_ln#_i", OracleType.Number)).Value = strDelDocLnNo
            oraCmd.Parameters.Add(New OracleParameter("pd_transfer_dt_i", OracleType.DateTime)).Value = dtPdTransfer
            oraCmd.Parameters.Add(New OracleParameter("qty_i", OracleType.Number)).Value = intQty
            oraCmd.Parameters.Add(New OracleParameter("po_cd_i", OracleType.VarChar)).Value = strPOCd
            oraCmd.Parameters.Add(New OracleParameter("po_ln#_i", OracleType.Number)).Value = strPOLn
            oraCmd.Parameters.Add(New OracleParameter("se_part_seq_i", OracleType.Number)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_seq_num_i", OracleType.VarChar)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_wr_dt_i", OracleType.DateTime)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_store_cd_i", OracleType.VarChar)).Value = System.DBNull.Value
            oraCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Removes the PO Link for the document line passed in
    ''' </summary>
    ''' <param name="oraCmd">the command object to be used, usually part of a single transaction</param>
    ''' <param name="strDocTpCd">the document type being processed</param>
    ''' <param name="strDelDocNo">the document number of the document being processed</param>
    ''' <param name="strDelDocLnNo">the line number for the line being processed</param>
    ''' <param name="dtPdTransfer">the date when this is taking place</param>
    ''' <param name="intQty">the quantity of the line being processed</param>
    ''' <param name="strPOCd">the PO code where the document line is linked to</param>
    ''' <param name="strPOLn">the PO line where the document line is linked to</param>
    Public Sub RemovePOLink(ByRef oraCmd As OracleCommand, ByVal strDocTpCd As String,
                            ByVal strDelDocNo As String, ByVal strDelDocLnNo As String,
                            ByVal dtPdTransfer As Date, ByVal intQty As Integer,
                            ByVal strPOCd As String, ByVal strPOLn As String)
        Try
            oraCmd.CommandType = CommandType.StoredProcedure
            oraCmd.CommandText = "bt_po_link.delete_recs"

            oraCmd.Parameters.Clear()
            oraCmd.Parameters.Add(New OracleParameter("doc_tp_i", OracleType.VarChar)).Value = strDocTpCd
            oraCmd.Parameters.Add(New OracleParameter("doc_num_i", OracleType.VarChar)).Value = strDelDocNo
            oraCmd.Parameters.Add(New OracleParameter("doc_ln#_i", OracleType.Number)).Value = strDelDocLnNo
            oraCmd.Parameters.Add(New OracleParameter("pd_transfer_dt_i", OracleType.DateTime)).Value = dtPdTransfer
            oraCmd.Parameters.Add(New OracleParameter("qty_i", OracleType.Number)).Value = intQty
            oraCmd.Parameters.Add(New OracleParameter("po_cd_i", OracleType.VarChar)).Value = strPOCd
            oraCmd.Parameters.Add(New OracleParameter("po_ln#_i", OracleType.Number)).Value = strPOLn
            oraCmd.Parameters.Add(New OracleParameter("se_part_seq_i", OracleType.Number)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_seq_num_i", OracleType.VarChar)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_wr_dt_i", OracleType.DateTime)).Value = System.DBNull.Value
            oraCmd.Parameters.Add(New OracleParameter("ist_store_cd_i", OracleType.VarChar)).Value = System.DBNull.Value
            oraCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetInventoryDac() As InventoryDac
        If (theInventoryDac Is Nothing) Then
            theInventoryDac = New InventoryDac()
        End If
        Return theInventoryDac
    End Function

    '----------SQL Helper functions---------------------'
    ''' <summary>
    ''' Executes the Query and returns the dataset and throws the exception back to calling program
    ''' </summary>
    ''' <param name="SqlCmd">Oracle Command object, which should include Query and parameters, if any. </param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    ''' 
    ' this procedure takes Command object(Querry is assigned to Command onject), executes the query and returns the dataset back. this is a private method to prevent it's usage from outside the class
    Private Function ExecuteDataset(ByVal sqlCmd As OracleCommand)
        Dim conn As OracleConnection
        'Dim objSql As OracleCommand
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Try
            conn = GetConnection()
            sqlCmd.Connection = conn
            oAdp = DisposablesManager.BuildOracleDataAdapter(sqlCmd)
            oAdp.Fill(ds)

        Catch ex As Exception
            Throw ex
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            oAdp.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the order line details from the temp_itm table along with Sort codes as a 
    ''' comma-separated string. This is for ORDER ENTRY when creating a regular, non-rel'ship transation.
    ''' </summary>
    ''' <param name="sessionID">the current session id/param>
    ''' <param name="SortDescriptionAlso">if true, returns sort code descriptions along with the sort codes
    '''                                    Else returns only the sort codes</param>
    ''' <param name="orderTypes">indicates whether the order is of type 'SAL','CRM', 'MCR', 'MDB'</param>
    ''' <returns>Dataset of the order line info from the temp-itm table</returns>
    ''' <remark>This is used for ORDER ENTRY for a regular sale transaction- not a rel'ship/quote</remark>
    ''' 
    'this OVERLOADED method consumes the session and an boolean to check whether the resultant dataset to have sort desription or not. 
    'The Query used in this method uses concept called CTE to combine the sort codes and description into comma seperated string
    Public Function GetLineItemsForSOE(ByVal sessionID As String, ByVal SortDescriptionAlso As Boolean, ByVal orderTypes As String)

        Dim sqlSb As New StringBuilder
        Dim objSql As OracleCommand
        Dim ds As New DataSet

        Try
            sqlSb.Append("  With CTS as  ( ")
            sqlSb.Append(" SELECT  ")
            sqlSb.Append(" ITM_CD, ")
            If Not orderTypes Is Nothing AndAlso orderTypes.Trim().isEmpty() = False Then
                sqlSb.Append(" CASE WHEN '" + orderTypes.ToUpper() + "' IN ('SAL','CRM') THEN  LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD, ','), ',') ELSE '' END ITM_SRT_CD, ")
                If SortDescriptionAlso = False Then
                    'do nothing
                Else
                    sqlSb.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD||'-'||des, ','), ',') SortDesc, ")
                End If
            Else
                sqlSb.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD, ','), ',') ITM_SRT_CD, ")
                If SortDescriptionAlso = False Then
                    'do nothing
                Else
                    sqlSb.Append(" LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD||'-'||des, ','), ',') SortDesc, ")
                End If
            End If
            sqlSb.Append(" row_id ")
            sqlSb.Append(" FROM ( ")
            sqlSb.Append(" SELECT t.ITM_CD,  NVL(isrt.ITM_SRT_CD,'') AS ITM_SRT_CD, NVL(isrt.des,'') AS des,t.ROW_ID, ")
            sqlSb.Append(" ROW_NUMBER () OVER (PARTITION BY t.ITM_CD, t.ROW_ID ORDER BY t.ITM_CD, t.ROW_ID) rnum,")
            sqlSb.Append(" COUNT(1) over(PARTITION BY t.ITM_CD,t.ROW_ID) tot  ")
            sqlSb.Append(" FROM   temp_itm t    ")
            sqlSb.Append(" left join ITM$itm_srt srt on srt.ITM_CD = t.itm_cd   ")
            sqlSb.Append(" join itm_srt isrt on  srt.ITM_SRT_CD = isrt.ITM_SRT_CD  ")
            sqlSb.Append(" where upper(session_id) = upper(:sessId) and upper(DISPLAY_IN_POS) = 'Y' order by case when upper(isrt.MTO) = 'Y' and upper(DISPLAY_IN_POS) = 'Y' then 1 else 0 end desc ")
            sqlSb.Append(" )  ")
            sqlSb.Append(" WHERE rnum = tot  ")
            sqlSb.Append(" START WITH rnum = 1  ")
            sqlSb.Append(" CONNECT BY PRIOR rnum = rnum -1  ")
            sqlSb.Append(" AND PRIOR ITM_CD = ITM_CD  ")
            sqlSb.Append(" AND PRIOR ROW_ID=ROW_ID ")
            sqlSb.Append(" ) ")
            sqlSb.Append(" SELECT  t.ITM_CD, t.VE_CD, t.VSN, t.DES, t.SLSP1, t.SLSP2, t.STORE_CD, t.LOC_CD, t.ROW_ID, NVL(t.RET_PRC,0) AS RET_PRC,  ")
            sqlSb.Append(" NVL(t.ORIG_PRC,0) AS ORIG_PRC, NVL(t.QTY,0) AS QTY,  t.ITM_TP_CD, t.TREATABLE, t.TREATED, t.TAKE_WITH, t.NEW_SPECIAL_ORDER, t.PACKAGE_PARENT,  ")
            sqlSb.Append(" NVL(t.TAXABLE_AMT,0) AS TAXABLE_AMT, t.tax_amt,  t.LEAVE_CARTON, NVL(t.SLSP1_PCT,0) AS SLSP1_PCT,  ")
            sqlSb.Append(" NVL(t.SLSP2_PCT,0) AS SLSP2_PCT, t.SERIAL_TP, t.SERIAL_NUM, t.ID_NUM, t.BULK_TP_ITM, t.OUT_ID,   ")
            sqlSb.Append(" t.WARR_ROW_LINK, t.WARR_ITM_LINK, NVL(t.warrantable,'N') AS warrantable, i.inventory, t.avail_dt, t.res_id,  t.prc_chg_app_cd, ")
            sqlSb.Append(" NVL(CTS.ITM_SRT_CD,'-') sortcodes ")
            If SortDescriptionAlso = False Then
                'do nothing
            Else
                sqlSb.Append(" ,NVL(SortDesc,'-') sortdesc ")
            End If
            sqlSb.Append(" , CASE WHEN (i.drop_dt <= sysdate) AND UPPER(d.prevent_sale) = 'Y' then 'Y' else 'N' end PREVENT ")
            sqlSb.Append(" , i.ITM_TP_CD as TYPECODE, i.MNR_CD, MAX_DISC_PCNT ")
            ' Daniela add URL sql
            sqlSb.Append(" , (select image_url from WEB_CACHE where t.itm_cd = itm_cd and rownum <= 1 and (select value_1 from leons_system where key = 'IMG_LK_POS') = 'Y') As itm_url ")
            ' End Daniela
            sqlSb.Append(" FROM itm i join temp_itm t on  i.itm_cd = t.itm_cd ")
            sqlSb.Append(" left join CTS on i.itm_cd = CTS.itm_cd and CTS.Row_id = t.row_id")
            sqlSb.Append(" left join drop_cd D on  D.DROP_CD=I.DROP_CD where upper(session_id) = upper(:sessId) Order by t.row_id")
            objSql = GetCommand(sqlSb.ToString())
            objSql.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
            objSql.Parameters("sessId").Value = sessionID
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the order line details from the line and related tables(such as so_ln, item, warranty, sort code, etc.)
    ''' along with Sort codes as a  comma-separated string. This is for ORDER MGMT.
    ''' </summary>
    ''' <param name="DelDocNumber">the del Doc#/Order Id</param>
    ''' <param name="SortDescriptionAlso">if true, returns sort code descriptions along with the sort codes
    '''                                    Else returns only the sort codes</param>
    ''' <param name="orderTypes">a list of the different order types such as 'SAL','CRM'</param>
    ''' <returns>Dataset of the order line info from the various tables</returns>
    ''' <remarks>**** Currently used for ORDER MGMT only. ****</remarks>
    ''' 
    'This oveloaded methods takes DelDocNumber, Salestype and a boolean SortDescriptionAlso as parameter and builds the query dynamically. the query uses a concept called CTE(Common table Expression which works on the principle of dynamic table
    ' This Query featches the Sort Codes in comma seperated format.
    Public Function GetLineItemsForSOM(ByVal DelDocNumber As String)

        Dim sqlSb As New StringBuilder
        Dim objSql As OracleCommand
        Dim ds As DataSet = New DataSet

        Try
            sqlSb.Append(" SELECT SL.DEL_DOC_NUM, ")
            sqlSb.Append("          SL.SPIFF, ")
            sqlSb.Append("          SL.FIFO_DT, ")
            sqlSb.Append("          SL.FIFL_DT, ")
            sqlSb.Append("          SL.FIFO_CST, ")
            sqlSb.Append("          ITM.INVENTORY, ")
            sqlSb.Append("          SL.UNIT_PRC AS MAX_RET_PRC, ")
            sqlSb.Append("          NVL(SL.DISC_AMT, 0) AS DISC_AMT, ")
            sqlSb.Append("          SL.COMM_CD, ")
            sqlSb.Append("          SL.PICKED, ")
            sqlSb.Append("          SL.DEL_DOC_LN#, ")
            sqlSb.Append("          SL.ITM_CD, ")
            sqlSb.Append("          SL.TREATED_BY_ITM_CD, ")
            sqlSb.Append("          ITM.VE_CD, ")
            sqlSb.Append("          ITM.VSN, ")
            sqlSb.Append("          ITM.DES, ")
            sqlSb.Append("          ITM.TREATABLE, ")
            sqlSb.Append("          ITM.ITM_TP_CD, ")
            sqlSb.Append("          ITM.BULK_TP_ITM, ")
            sqlSb.Append("          NVL(ITM.WARR_DAYS, 0) as WARR_DAYS, ")
            sqlSb.Append("          ITM.DROP_CD, ")
            sqlSb.Append("          ITM.DROP_DT, ")
            sqlSb.Append("          SL.UNIT_PRC, ")
            sqlSb.Append("          ITM.RET_PRC as ITM_RET_PRC, ")
            sqlSb.Append("          NVL (SL.UNIT_PRC, 0) + NVL (SL.DISC_AMT, 0) AS ORIG_PRC, ")
            sqlSb.Append("          NVL (SL.UNIT_PRC, 0) + NVL (SL.DISC_AMT, 0) AS MANUAL_PRC, ")
            sqlSb.Append("          SL.QTY, ")
            sqlSb.Append("          SL.VOID_FLAG, ")
            sqlSb.Append("          SL.STORE_CD, ")
            sqlSb.Append("          SL.LOC_CD, ")
            sqlSb.Append("          SL.TAX_CD, ")
            sqlSb.Append("          'N' AS NEW_LINE, ")
            sqlSb.Append("          0 AS TAXABLE_AMT, ")
            sqlSb.Append("          0 AS TAX_PCT, ")
            sqlSb.Append("          SL.CUST_TAX_CHG, ")
            sqlSb.Append("          SL.ADDON_WR_DT, ")
            sqlSb.Append("          SL.QTY AS MAX_CRM_QTY, ")
            sqlSb.Append("          SL.VOID_FLAG AS VOID_FLAG2, ")
            sqlSb.Append("          'N' AS INVPICKVOID, ")
            sqlSb.Append("          SL.STORE_CD AS STORE_CD2, ")
            sqlSb.Append("          SL.LOC_CD AS LOC_CD2, ")
            sqlSb.Append("          SL.SER_NUM AS SERIAL_NUM, ")
            sqlSb.Append("          NULL AS PACKAGE_PARENT, ")
            sqlSb.Append("          NVL (SL.OOC_QTY, 0) AS OOC_QTY, ")
            sqlSb.Append("          NVL (ITM.SERIAL_TP, '') AS SERIAL_TP, ")
            sqlSb.Append("          OUT_ID_CD AS OUT_ID, ")
            sqlSb.Append("          OUT_CD, ")
            sqlSb.Append("          '' AS RES_ID, ")
            sqlSb.Append("          '' AS PO_CD, ")
            sqlSb.Append("          '' AS PO_DESC, ")
            sqlSb.Append("          '' AS POArrival, ")
            sqlSb.Append("          '' AS PO_LN, ")
            sqlSb.Append("          ALT_DOC_LN# AS ORIG_SO_LN_NUM, ")
            sqlSb.Append("          VOID_DT, ")
            sqlSb.Append("          SL.TAX_BASIS, ")
            sqlSb.Append("          SL.TAX_RESP, ")
            sqlSb.Append("          FILL_DT, ")
            sqlSb.Append("          TREATED_BY_LN#, ")
            sqlSb.Append("          TREATS_ITM_CD, ")
            sqlSb.Append("          TREATS_LN#, ")
            sqlSb.Append("          SL.PRC_CHG_APP_CD, ")
            sqlSb.Append("          NVL (WARRANTABLE, 'N') AS WARRANTABLE, ")
            sqlSb.Append("          SW.WARR_DEL_DOC_LN# AS WARR_BY_LN, ")
            sqlSb.Append("          SW.WARR_DEL_DOC_NUM AS WARR_BY_DOC_NUM , ")
            sqlSb.Append("           NVL ( ")
            sqlSb.Append("             (SELECT SL2.ITM_CD ")
            sqlSb.Append("                FROM SO_LN SL2, SO_LN2WARR SW2 ")
            sqlSb.Append("               WHERE     SL2.DEL_DOC_NUM = SW2.WARR_DEL_DOC_NUM ")
            sqlSb.Append("                     AND SL2.DEL_DOC_LN# = SW2.WARR_DEL_DOC_LN# ")
            sqlSb.Append("                     AND SL.DEL_DOC_NUM = SW2.DEL_DOC_NUM ")
            sqlSb.Append("                     AND SL.DEL_DOC_LN# = SW2.DEL_DOC_LN#), ")
            sqlSb.Append("             NVL (SL.REF_ITM_CD, ''))  AS WARR_BY_SKU, ")
            sqlSb.Append("          LV_IN_CARTON, ")
            sqlSb.Append("          DELIVERY_POINTS, ")
            sqlSb.Append("          SL.DISC_AMT AS SL_DISC_AMT, ")
            sqlSb.Append("          SL.SO_EMP_SLSP_CD1 AS LN_SLSP1, ")
            sqlSb.Append("          SL.SO_EMP_SLSP_CD2 AS LN_SLSP2, ")
            sqlSb.Append("          NVL (SL.PCT_OF_SALE1, 0) AS LN_SLSP1_PCT, ")
            sqlSb.Append("          NVL (SL.PCT_OF_SALE2, 0) AS LN_SLSP2_PCT, ")
            sqlSb.Append("          SL.PKG_SOURCE,")
            sqlSb.Append("          ITM.ITM_TP_CD AS TYPECODE, ")
            sqlSb.Append("          ITM.MNR_CD, ")
            sqlSb.Append("          NVL(ITM.REPL_CST,0) AS REPL_CST, ")
            sqlSb.Append("          NVL(ITM.PU_DISC_PCNT,0) AS PU_DISC_PCNT, ")
            sqlSb.Append("          MAX_DISC_PCNT, ")
            ' change below to reflect if SHU'd line
            sqlSb.Append("          CASE WHEN  SO.SO_WR_DT <= :SHUDATE  AND SL.ADDON_WR_DT IS NULL THEN 'Y' ")
            sqlSb.Append("              WHEN  SO.SO_WR_DT <= :SHUDATE AND SL.ADDON_WR_DT <= :SHUDATE THEN 'Y' ")
            sqlSb.Append("              ELSE 'N' END SHU ")
            sqlSb.Append(" , (select image_url from WEB_CACHE where itm_cd = ITM.ITM_CD and rownum <= 1 and (select value_1 from leons_system where key = 'IMG_LK_POS') = 'Y') itm_url ") ' Daniela added pictures url
            sqlSb.Append("     FROM SO, SO_LN2WARR SW, ITM, SO_LN SL  ")
            sqlSb.Append("WHERE       SL.DEL_DOC_NUM = :DEL_DOC_NUM ")
            sqlSb.Append("AND SO.DEL_DOC_NUM = SL.DEL_DOC_NUM ")
            sqlSb.Append("AND SL.ITM_CD = ITM.ITM_CD ")
            sqlSb.Append("AND SL.DEL_DOC_NUM = SW.DEL_DOC_NUM(+) ")
            sqlSb.Append("AND SL.DEL_DOC_LN# = SW.DEL_DOC_LN#(+) ")
            sqlSb.Append("ORDER BY   DEL_DOC_LN# ")

            objSql = GetCommand(sqlSb.ToString())
            objSql.Parameters.Add("DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            objSql.Parameters("DEL_DOC_NUM").Value = DelDocNumber
            objSql.Parameters.Add(":SHUDATE", OracleType.DateTime).Direction = ParameterDirection.Input
            objSql.Parameters(":SHUDATE").Value = SessVar.shuDt

            ds = ExecuteDataset(objSql)
            ds.Tables(0).Columns.Add("SortCodes", Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("SortDescription", Type.GetType("System.String"))

            Dim FilterText As String
            If ds.Tables(0).Rows.Count > 0 Then
                Dim itmes As String = String.Join(",", (From row In ds.Tables(0).AsEnumerable Select row("ITM_CD")).ToArray)
                Dim listItems As New ArrayList(itmes.Split(","))
                Dim sku As SKUDac = New SKUDac()
                Dim itemSorCodes As DataSet = sku.GetItemSortCodes(listItems)
                Dim distincCodes() As DataRow
                For Each dr As DataRow In ds.Tables(0).Rows
                    FilterText = "ITM_CD='" + dr("itm_cd").ToString() + "'"
                    distincCodes = itemSorCodes.Tables(0).Select(FilterText)
                    dr("SortCodes") = String.Join(", ", (From row In distincCodes.AsEnumerable Select row("SortCode")).Distinct.ToArray)
                    dr("SortDescription") = String.Join(", ", (From row In distincCodes.AsEnumerable Select row("Description")).Distinct.ToArray)
                Next
            End If

            ds.AcceptChanges()
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try

        Return ds
    End Function

    '------------- Relationship related methods --------------------------------------------'
    ''' <summary>
    ''' Extracts and returns the realtionship line details from RELATIONSHIP_LINES table along with Sort codes
    ''' comma-separated string. This is for RELATIONSHIP MAINTENANCE
    ''' </summary>
    ''' <param name="RelationID">the Relationship ID</param>
    ''' <param name="SortDescriptionAlso">if true, returns sort code descriptions along with the sort codes
    '''                                    Else returns only the sort codes</param>
    ''' <returns>Dataset of relationship line info</returns>
    ''' <remarks>Currently used for RELATIONSHIP MAINTENANCE only.</remarks>
    '''the query uses a concept called CTE(Common table Expression which works on the principle of dynamic table
    Public Function GetRelationLineItems(ByVal RelationID As String, ByVal SortDescriptionAlso As Boolean)
        Dim sqlSb As New StringBuilder
        Dim objSql As OracleCommand
        Dim ds As DataSet
        ds = New DataSet
        sqlSb = New StringBuilder()
        Try
            sqlSb.Append("   With CTS as  ( ")
            sqlSb.Append("                     SELECT  ")
            sqlSb.Append("                     ITM_CD,  LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_CD, ','), ',') ITM_SRT_CD,LTRIM(SYS_CONNECT_BY_PATH(ITM_SRT_DESC, ','), ',') ITM_SRT_DESC,")
            sqlSb.Append("                     REL_NO ")
            sqlSb.Append("                     FROM ( ")
            sqlSb.Append("                     SELECT t.ITM_CD,t.REL_NO,  ")
            sqlSb.Append("                      case when UPPER (DISPLAY_IN_POS) = 'Y' then NVL (isrt.ITM_SRT_CD, '') else '' end ITM_SRT_CD, ")
            sqlSb.Append("                      case when UPPER (DISPLAY_IN_POS) = 'Y' then NVL (isrt.ITM_SRT_CD, '')|| '-'|| NVL (isrt.des, '')else '' end ITM_SRT_Desc,")
            sqlSb.Append("                      ROW_NUMBER () OVER (PARTITION BY t.ITM_CD, t.REL_NO  ORDER BY t.ITM_CD, t.REL_NO) rnum,")
            sqlSb.Append("                     COUNT(1) over(PARTITION BY t.ITM_CD,t.REL_NO,t.line) tot  ")
            sqlSb.Append("                     FROM   RELATIONSHIP_LINES t    ")
            sqlSb.Append("                     left join ITM$itm_srt srt on srt.ITM_CD = t.itm_cd ")
            sqlSb.Append("                     left join itm_srt isrt on  srt.ITM_SRT_CD = isrt.ITM_SRT_CD  ")
            sqlSb.Append("                     where upper(REL_NO) = upper(:REL_NO) order by case when upper(isrt.MTO) = 'Y' and upper(DISPLAY_IN_POS) = 'Y' then 1 else 0 end desc ")
            sqlSb.Append("                     )  ")
            sqlSb.Append("                     WHERE rnum = tot  ")
            sqlSb.Append("                     START WITH rnum = 1  ")
            sqlSb.Append("                     CONNECT BY PRIOR rnum = rnum -1  ")
            sqlSb.Append("                     AND PRIOR ITM_CD = ITM_CD  ")
            sqlSb.Append("                     AND PRIOR REL_NO=REL_NO )")
            sqlSb.Append(" SELECT rl.*, NVL (rl.ret_prc, 0) + NVL (rl.DISC_AMT, 0) AS orig_prc, itm.inventory, itm.warrantable, NVL(itm.warr_days, 0) AS warr_days,NVL(ITM_SRT_CD,'-') SortCodes ")
            If SortDescriptionAlso = True Then
                sqlSb.Append(" ,NVL(ITM_SRT_DESC,'-')SortDescription ")
            Else
                'do nothing
            End If
            sqlSb.Append("  ,ITM.MNR_CD AS MNR_CD,NVL(ITM.REPL_CST,0) AS REPL_CST,NVL(ITM.PU_DISC_PCNT,0) AS PU_DISC_PCNT,rl.DISC_AMT  ")
            sqlSb.Append("  ,NVL (rl.ret_prc, 0) + NVL (rl.DISC_AMT, 0) AS MANUAL_PRC, ITM.ITM_TP_CD AS TYPECODE, ITM.MAX_DISC_PCNT,ITM.ret_prc as ITM_RET_PRC,rl.RET_PRC + NVL(rl.DISC_AMT,0) as RET_PRC_DISC   ")
            sqlSb.Append(" FROM itm, RELATIONSHIP_LINES rl ,CTS ")
            sqlSb.Append(" where ")
            sqlSb.Append(" upper(rl.REL_NO) = upper(:REL_NO) AND ")
            sqlSb.Append(" itm.itm_cd = rl.itm_cd ")
            sqlSb.Append(" And CTS.REL_NO = rl.rel_no And cts.itm_cd  = itm.itm_cd ")
            sqlSb.Append(" ORDER BY LINE   ")
            objSql = GetCommand(sqlSb.ToString)
            objSql.Parameters.Add("REL_NO", OracleType.VarChar).Direction = ParameterDirection.Input
            objSql.Parameters("REL_NO").Value = RelationID
            ds = ExecuteDataset(objSql)
        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' Extracts and returns the  items details from SO for exchange
    ''' </summary>
    ''' <param name="docNum">Del Doc Number</param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function GetOriginalItemsForExchange(ByVal docNum As String)
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim objSql As OracleCommand
        Dim ds As New DataSet

        sqlSb.Append("  SELECT   sl.DEL_DOC_NUM,")
        sqlSb.Append("           sl.COMM_CD,")
        sqlSb.Append("           ITM.POINT_SIZE,")
        sqlSb.Append("           sl.PICKED,")
        sqlSb.Append("           sl.DEL_DOC_LN#,")
        sqlSb.Append("           sl.ITM_CD,")
        sqlSb.Append("           sl.TREATED_BY_ITM_CD,")
        sqlSb.Append("           ITM.VE_CD,")
        sqlSb.Append("           ITM.VSN,")
        sqlSb.Append("           ITM.DES,")
        sqlSb.Append("           ITM.TREATABLE,")
        sqlSb.Append("           ITM.ITM_TP_CD,")
        sqlSb.Append("           NVL (ITM.WARR_DAYS, 0) AS WARR_DAYS,")
        sqlSb.Append("           sl.UNIT_PRC,")
        sqlSb.Append("           sl.QTY,")
        sqlSb.Append("           sl.VOID_FLAG,")
        sqlSb.Append("           sl.STORE_CD,")
        sqlSb.Append("           sl.LOC_CD,")
        sqlSb.Append("           sl.cust_tax_chg,")
        sqlSb.Append("           NVL (serial_tp, '') AS serial_tp,")
        sqlSb.Append("           itm.inventory,")
        sqlSb.Append("           sl.delivery_points,")
        sqlSb.Append("           '' AS new_line,")
        sqlSb.Append("           NVL (sl.spiff, '') AS spiff,")
        sqlSb.Append("           pkg_source,")
        sqlSb.Append("           so_emp_slsp_cd1 AS ln_slsp1,")
        sqlSb.Append("           so_emp_slsp_cd2 AS ln_slsp2,")
        sqlSb.Append("           pct_of_sale1 AS ln_slsp1_pct,")
        sqlSb.Append("           pct_of_sale2 AS ln_slsp2_pct,")
        sqlSb.Append("           NVL (warrantable, 'N') AS WARRANTABLE,")
        sqlSb.Append("           sw.warr_del_Doc_ln# AS WARR_BY_LN,")
        sqlSb.Append("           sw.warr_del_doc_num AS WARR_BY_DOC_NUM,")
        sqlSb.Append("           NVL (")
        sqlSb.Append("              (SELECT   sl2.itm_cd ")
        sqlSb.Append("                 FROM   so_ln sl2 ")
        sqlSb.Append("                WHERE       sl2.del_doc_num = sw.warr_del_doc_num ")
        sqlSb.Append("                        AND sl2.del_doc_ln# = sw.warr_del_Doc_ln#), ")
        sqlSb.Append("              NVL (sl.ref_itm_cd, '')")
        sqlSb.Append("           )")
        sqlSb.Append("              AS WARR_BY_SKU,")
        ' sqlSb.Append("              case when dc.PREVENT_SALE='Y' and ITM.DROP_DT <= sysdate then 'Y' else 'N' End prevent")
        sqlSb.Append("              case when dc.PREVENT_SALE='Y' and ITM.DROP_DT <= sysdate then 'Y' else 'N' End prevent,")
        sqlSb.Append("              case when ITM.ITM_TP_CD in('CPT') then 'Y' else 'N' end AllowFraction ")  '2018 JDA add
        sqlSb.Append("    FROM   SO_LN2WARR sw, SO_LN sl, ITM,  drop_cd dc")
        sqlSb.Append("   WHERE   sl.DEL_DOC_NUM = :del_doc_num ")
        sqlSb.Append("           AND sl.ITM_CD = ITM.ITM_CD")
        sqlSb.Append("           AND sl.DEL_DOC_NUM = sw.DEL_DOC_NUM(+)")
        sqlSb.Append("           AND sl.DEL_DOC_LN# = sw.DEL_DOC_LN#(+)")
        sqlSb.Append("           AND sl.VOID_FLAG = 'N'")
        sqlSb.Append("           AND itm.drop_cd = dc.drop_cd(+)")
        sqlSb.Append(" ORDER BY   DEL_DOC_LN# ")

        Try
            objSql = GetCommand(sqlSb.ToString)
            objSql.Parameters.Add("del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
            objSql.Parameters("del_doc_num").Value = docNum.ToUpper()
            ds = ExecuteDataset(objSql)
            ds.Tables(0).Columns.Add("ITM_SRT_Desc", Type.GetType("System.String"))
            Dim FilterText As String
            If ds.Tables(0).Rows.Count > 0 Then
                Dim itmes As String = String.Join(",", (From row In ds.Tables(0).AsEnumerable Select row("ITM_CD")).ToArray)
                Dim listItems As New ArrayList(itmes.Split(","))
                Dim sku As SKUDac = New SKUDac()
                Dim itemSorCodes As DataSet = sku.GetItemSortCodes(listItems)
                Dim distincCodes() As DataRow
                For Each dr As DataRow In ds.Tables(0).Rows
                    FilterText = "ITM_CD='" + dr("itm_cd").ToString() + "'"
                    distincCodes = itemSorCodes.Tables(0).Select(FilterText)
                    dr("ITM_SRT_Desc") = String.Join(", ", (From row In distincCodes.AsEnumerable Select row("Description")).Distinct.ToArray)
                Next

            End If
            ds.AcceptChanges()

        Catch ex As Exception
            Throw
        Finally
            objSql.Dispose()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' REMOVES THE ORIGINAL SKU TO REPLACE THE NEW SKU.
    ''' </summary>
    ''' <param name="DelDocNum">Del Doc Number</param>
    ''' <param name="SkuToBeReplaced">SKU being replaced</param>
    ''' <param name="NewSKU">New SKU/param>
    ''' <param name="DelDocLineNum">Item Line number</param>
    ''' <returns>int</returns>
    ''' <remarks></remarks>
    Public Function RemoveComponent(ByVal drNew As DataRow, ByRef objSql As OracleCommand) As Integer
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim RowsAffected As Integer
        Dim DelDocNum As String = drNew("DEL_DOC_NUM").ToString()
        Dim DelDocLineNum As String = drNew("del_Doc_ln#").ToString()
        Dim dTemp As DataTable = New DataTable()


        'Dim dbConnection As OracleConnection = GetConnection()
        Try
            sqlSb.Append(" DELETE FROM SO_LN ")
            sqlSb.Append(" WHERE DEL_DOC_NUM = :DelDocNum AND del_Doc_ln# = :DelDocLineNum")
            objSql.Parameters.Add(":DelDocNum", OracleType.VarChar)
            objSql.Parameters(":DelDocNum").Value = DelDocNum
            objSql.Parameters.Add(":DelDocLineNum", OracleType.VarChar)
            objSql.Parameters(":DelDocLineNum").Value = DelDocLineNum
            objSql.CommandText = sqlSb.ToString()
            objSql.CommandType = CommandType.Text
            RowsAffected = objSql.ExecuteNonQuery()
            objSql.Parameters.Clear()
        Catch ex As Exception
            Throw ex
        Finally
            'If dbConnection.State = ConnectionState.Open Then
            '    'dbConnection.Close()
            'End If
        End Try
        Return RowsAffected
    End Function

    ''' <summary>
    ''' Retrieves and returns all the non-void SO_LN records that are either package or component records.
    ''' </summary>
    ''' <param name="delDocNum">document for lines to be extracted</param>
    ''' <param name="dbCmd">database command for single atomic transaction process</param>
    ''' <returns>a dataset of all the package and component records for the specified document</returns>
    Public Function GetSoLnPkgNCmpnts(ByVal delDocNum As String, ByRef dbCmd As OracleCommand) As DataSet

        Dim datSet As New DataSet
        Dim sql As New StringBuilder
        sql.Append("SELECT del_Doc_ln#, sl.itm_cd, itm_tp_cd, pkg_source, unit_prc, qty, cust_tax_chg, disc_amt, ").Append(
            "DECODE(itm_tp_cd, 'PKG', i.pkg_split_method, '') AS pkg_Split_Method, i.BLOCK_RETAIL as isRetailPriceBlocked ").Append(
            "FROM itm i, so_ln sl WHERE del_doc_num = :docNum AND i.itm_cd = sl.itm_cd ").Append(
            "AND void_flag != 'Y' AND (sl.pkg_source IS NOT NULL OR itm_tp_cd = 'PKG') ").Append(
            "ORDER BY DEL_DOC_LN#  asc ")

        Dim dbAdapter As OracleDataAdapter
        dbCmd.CommandType = CommandType.Text
        dbCmd.CommandText = sql.ToString
        dbCmd.Parameters.Clear()
        dbCmd.Parameters.Add(":docNum", OracleType.VarChar)
        dbCmd.Parameters(":docNum").Value = delDocNum
        dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCmd)

        Try
            dbAdapter.Fill(datSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            dbAdapter.Dispose()
            Throw ex
        End Try

        Return datSet

    End Function
    ''' <summary>
    ''' Updates the so_ln table component and package SKU prices and line taxes  
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="soLnRow">datarow with key for the update and the price and tax charge to be applied</param>
    ''' <param name="dbCmd">database command for single atomic transaction process</param>
    Public Sub UpdtSoLnPkgCmpntsPrc(ByVal headerInfo As SoHeaderDtc, ByVal soLnRow As DataRow, ByRef dbCmd As OracleCommand)

        Try
            Dim sql As String = String.Empty
            Dim sql2 As String = String.Empty
            dbCmd.CommandType = CommandType.Text
            dbCmd.Parameters.Clear()
            dbCmd.Parameters.Add(":docNum", OracleType.VarChar)
            dbCmd.Parameters(":docNum").Value = headerInfo.delDocNum
            dbCmd.Parameters.Add(":lnNum", OracleType.Number)
            dbCmd.Parameters(":lnNum").Value = soLnRow("del_doc_ln#")

            ' TODO - need to create so_ln2disc records for the priced and discounted components and set the pkg so_ln2disc amt to 0
            ' potential logic
            '   if multi disc, loop thru pkg so_ln2disc and apportion by pkg so_ln2discs, line portion w/in discont pkg portion - not perfect 
            '' if multiple discount on sale, then need to create line so_ln2disc records also
            'If SysPms.allowMultDisc AndAlso AppConstants.Order.TYPE_SAL = headerInfo.orderTpCd AndAlso headerInfo.discCd = SysPms.multiDiscCd Then

            '    If soLnRow.Item("pkg_source").ToString.isNotEmpty AndAlso CDbl(soLnRow.Item("disc_amt").ToString) > 0 Then

            '        'sql = "UPDATE so_ln SET unit_prc = :unitPrc, tax_basis = :unitPrc, cust_tax_chg = :custTxChg, disc_amt = :discAmt WHERE del_doc_num = :docNum AND del_doc_ln# = :lnNum "
            '        'dbCmd.Parameters.Add(":unitPrc", OracleType.Number)
            '        'dbCmd.Parameters(":unitPrc").Value = soLnRow("unit_prc")
            '        'dbCmd.Parameters.Add(":custTxChg", OracleType.Number)
            '        'dbCmd.Parameters(":custTxChg").Value = soLnRow("cust_tax_chg")
            '        'dbCmd.Parameters.Add(":discAmt", OracleType.Number)
            '        'dbCmd.Parameters(":discAmt").Value = soLnRow("disc_amt")
            '        'ProcessSoLnDiscounts(headerInfo.delDocNum, dbCmd)

            '    ElseIf soLnRow.Item("ITM_TP_CD") = AppConstants.Sku.TP_PKG Then

            '        sql2 = "UPDATE so_ln2disc SET disc_amt = 0 WHERE del_doc_num = :docNum AND del_doc_ln# = :lnNum "
            '    End If
            'End If

            If soLnRow.Item("pkg_source").ToString.isNotEmpty AndAlso CDbl(soLnRow.Item("unit_prc").ToString) > 0 Then

                sql = "UPDATE so_ln SET unit_prc = :unitPrc, tax_basis = :unitPrc, cust_tax_chg = :custTxChg, disc_amt = :discAmt WHERE del_doc_num = :docNum AND del_doc_ln# = :lnNum "
                dbCmd.Parameters.Add(":unitPrc", OracleType.Number)
                dbCmd.Parameters(":unitPrc").Value = soLnRow("unit_prc")
                dbCmd.Parameters.Add(":custTxChg", OracleType.Number)
                dbCmd.Parameters(":custTxChg").Value = soLnRow("cust_tax_chg")
                dbCmd.Parameters.Add(":discAmt", OracleType.Number)
                dbCmd.Parameters(":discAmt").Value = soLnRow("disc_amt")

            ElseIf soLnRow.Item("ITM_TP_CD") = AppConstants.Sku.TP_PKG Then

                sql = "UPDATE so_ln SET unit_prc = 0, tax_basis = 0, cust_tax_chg = 0, disc_amt = 0 WHERE del_doc_num = :docNum AND del_doc_ln# = :lnNum "
            End If

            If Not String.IsNullOrEmpty(sql) Then
                dbCmd.CommandText = sql
                dbCmd.ExecuteNonQuery()
            End If
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Retrieves all the details relating to the passed-in package sku and also it's components. It calls
    ''' an API to get the pakcage component information. 
    ''' </summary>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="conn">the db connection to use for querying</param>
    ''' <param name="solnDataset">the session dataset of SO_LN details</param>
    ''' <param name="del_doc_num">del_doc_num</param>
    ''' <returns>the dataset with all the detailed item and other info for the
    '''          specified package sku and its components.</returns>
    Public Function GetPkgCmpntsRetPrc(ByVal pkgSku As String,
                                         ByRef conn As OracleConnection,
                                         ByVal solnDataset As DataSet,
                                         ByVal del_doc_num As String, ByVal store As String) As DataSet

        Dim compntsDataSet As New DataSet
        Dim dbAdapter As OracleDataAdapter
        Dim dbReader As OracleDataReader
        Dim retPrice As Double = 0
        Dim pkgSplitMethod As String = String.Empty
        Dim sql As StringBuilder = New StringBuilder()
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim effectiveDate As Date = Now.Date

        'Check the dataset is empty or not 
        If solnDataset Is Nothing Then
            sql.Append("SELECT SO.UNIT_PRC,ITM.PKG_SPLIT_METHOD ")
            sql.Append("  FROM ITM ITM Join SO_LN SO on ITM.ITM_CD=SO.ITM_CD where SO.DEL_DOC_NUM =:delDocNum and ITM.ITM_TP_CD='PKG'")

            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
            cmd.Parameters(":delDocNum").Value = del_doc_num

            dbReader = DisposablesManager.BuildOracleDataReader(cmd)


            Do While dbReader.Read()
                If IsNumeric(dbReader.Item("UNIT_PRC")) Then
                    If dbReader.Item("UNIT_PRC") > 0 Then
                        retPrice = CDbl(dbReader.Item("UNIT_PRC").ToString)
                    End If
                End If

                If Not (IsDBNull(dbReader.Item("PKG_SPLIT_METHOD"))) Then
                    pkgSplitMethod = dbReader.Item("PKG_SPLIT_METHOD")
                End If
            Loop
        Else  ' if data set is empty get details from SO_LN table 
            sql.Append("SELECT PKG_SPLIT_METHOD ")
            sql.Append("  FROM ITM where ITM_CD=:itmCd and ITM_TP_CD='PKG'")

            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":itmCd", OracleType.VarChar)
            cmd.Parameters(":itmCd").Value = pkgSku

            dbReader = DisposablesManager.BuildOracleDataReader(cmd)

            Do While dbReader.Read()
                If Not (IsDBNull(dbReader.Item("PKG_SPLIT_METHOD"))) Then
                    pkgSplitMethod = dbReader.Item("PKG_SPLIT_METHOD")
                End If
            Loop
            For Each dr As DataRow In solnDataset.Tables(0).Rows
                If (dr("ITM_CD") = pkgSku) Then

                    If IsNumeric(dr("UNIT_PRC")) Then
                        retPrice = CDbl(dr("UNIT_PRC").ToString)
                    End If
                End If
            Next
        End If

        'Calling API here
        cmd.Parameters.Clear()
        cmd = DisposablesManager.BuildOracleCommand("bt_iface_sale.get_all_pkg_cmpnt_info", conn)

        compntsDataSet = GetPkgCmpntsRetPrc(pkgSku, pkgSplitMethod, CDbl(retPrice), cmd)

        'cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.Add("pkg_cmpnt_info_cur", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

        'cmd.Parameters.Add("pkg_itm_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
        'cmd.Parameters.Add("pkg_prc_i", OracleType.Number).Direction = ParameterDirection.Input
        'cmd.Parameters.Add("pkg_split_method_i", OracleType.NVarChar).Direction = ParameterDirection.Input

        'cmd.Parameters("pkg_itm_cd_i").Value = pkgSku
        'cmd.Parameters("pkg_prc_i").Value = retPrice
        'cmd.Parameters("pkg_split_method_i").Value = pkgSplitMethod

        'dbAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
        'dbAdapter.Fill(compntsDataSet)

        Return compntsDataSet

    End Function

    ''' <summary>
    ''' Retrieves all the details relating to the passed-in package sku's components. It calls
    ''' an API to get the package component information. 
    ''' </summary>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="pkgSplitMethod">price split method specified for the package</param>
    ''' <param name="pkgRetPrc">the package retail price</param>
    ''' <param name="dbCmd">database command for single atomic transaction process</param>
    ''' <returns>the dataset with all the detailed item and other info for the
    '''          specified package sku and its components.</returns>
    Public Function GetPkgCmpntsRetPrc(ByVal pkgSku As String, ByVal pkgSplitMethod As String,
                                       ByVal pkgRetPrc As Double, ByRef dbCmd As OracleCommand) As DataSet

        Dim pkgCmpnts As New DataSet
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        Try
            'Calling API here
            dbCmd.Parameters.Clear()
            dbCmd.CommandText = "bt_iface_sale.get_all_pkg_cmpnt_info"
            dbCmd.CommandType = CommandType.StoredProcedure
            dbCmd.Parameters.Add("pkg_cmpnt_info_cur", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

            dbCmd.Parameters.Add("pkg_itm_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("pkg_prc_i", OracleType.Number).Direction = ParameterDirection.Input
            'dbCmd.Parameters.Add("pkg_so_wr_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            'dbCmd.Parameters.Add("pkg_so_store_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("pkg_split_method_i", OracleType.NVarChar).Direction = ParameterDirection.Input

            dbCmd.Parameters("pkg_itm_cd_i").Value = pkgSku
            dbCmd.Parameters("pkg_prc_i").Value = pkgRetPrc
            'dbCmd.Parameters("pkg_so_wr_dt_i").Value = HttpContext.Current.Session("tran_dt")
            'dbCmd.Parameters("pkg_so_store_cd_i").Value = HttpContext.Current.Session("store_cd")
            dbCmd.Parameters("pkg_split_method_i").Value = pkgSplitMethod
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCmd)
            dbAdapter.Fill(pkgCmpnts)

        Catch
            Throw
        Finally
            dbAdapter.Dispose()
        End Try

        Return pkgCmpnts

    End Function

    ''' <summary>
    ''' Splits the package price to the components on the SO_LN table
    ''' </summary>
    ''' <param name="hdrInf">object of information relating to the invoice/del_doc_num being processed</param>
    Public Sub SplitSoLnPkgCmpntPrcs(ByVal hdrInf As SoHeaderDtc)

        Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(dbConn)
        Try

            dbConn.Open()
            dbCmd.Transaction = dbConn.BeginTransaction()

            ' Get pkg and component lines
            Dim pkgCmpntDatSet As DataSet = GetSoLnPkgNCmpnts(hdrInf.delDocNum, dbCmd)

            If SystemUtils.dataSetHasRows(pkgCmpntDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(pkgCmpntDatSet.Tables(0).Rows(0)) Then

                ' get the packages that need to have price split 
                Dim pkgs() As DataRow    ' internal SQL result - array of datarows
                pkgs = pkgCmpntDatSet.Tables(0).Select("itm_tp_cd = 'PKG' AND unit_prc > 0 ", "del_Doc_ln#")
                Dim soLnCmpntsArr() As DataRow    ' internal SQL result - array of datarows
                'cmpnts = pkgCmpntDatSet.Tables(0).Select("itm_tp_cd != 'PKG' AND unit_prc = 0 AND pkg_source IS NOT NULL ORDER BY del_Doc_ln#")

                Dim pkg As DataRow
                Dim pndx As Integer = 0
                Dim cmpntPrcsDatSet As New DataSet
                Dim cndx As Integer = 0
                Dim pkgLnNum, nxtPkgLnNum As Integer
                Dim taxPct, taxChg As Double
                Dim accumUnitPrcs As Double = 0
                Dim remDisc As Double = 0
                Dim modResult As Double = 0
                Dim tmpCmpntLnDisc As Double = 0
                Dim cmpntUnitDisc As Double = 0
                Dim pkg_tax_chg As Double = 0
                Dim effectiveDate As Date
                Dim retPrice As Double = 0

                ' if there are packages, then process till done
                Do While pndx <= pkgs.GetUpperBound(0)

                    pkg = pkgs(pndx)
                    pkgLnNum = pkg("del_Doc_ln#")
                    If pndx < pkgs.GetUpperBound(0) Then
                        nxtPkgLnNum = pkgs(pndx + 1)("del_Doc_ln#")
                    Else
                        nxtPkgLnNum = 10000 ' this is one more than max del_Doc_ln# can be
                    End If
                    ' get the pkg price split api results for the sku

                    cmpntPrcsDatSet = GetPkgCmpntsRetPrc(pkg("ITM_CD"),
                      IIf(pkg("pkg_split_method").ToString.isEmpty, AppConstants.PkgSplit.METHOD_PERCENT, pkg("pkg_split_method")),
                      pkg("unit_prc"), dbCmd)
                    Erase soLnCmpntsArr

                    ' get the components for that package from the so_ln table entries
                    soLnCmpntsArr = pkgCmpntDatSet.Tables(0).Select("itm_tp_cd <> 'PKG' AND unit_prc = 0 AND pkg_source IS NOT NULL " &
                                                             " AND [del_doc_ln#] > " & pkgLnNum & " AND [del_Doc_ln#] < " & nxtPkgLnNum, "del_doc_ln#")

                    pkg_tax_chg = 0
                    If SystemUtils.isNumber(pkg("CUST_TAX_CHG").ToString) AndAlso CDbl(pkg("CUST_TAX_CHG").ToString) > 0 Then
                        pkg_tax_chg = CDbl(pkg("CUST_TAX_CHG").ToString)
                    End If
                    remDisc = If(IsDBNull(pkg("disc_amt")), 0, pkg("disc_amt"))

                    Dim currSoLnItem As DataRow = Nothing

                    ' match the components split prices to the component lines
                    For Each cmpntPrcDr As DataRow In cmpntPrcsDatSet.Tables(0).Rows
                        'For Each cmpntPrcDr As DataRow In dtSplitPriceData.Rows



                        'Find the non-voided SO_LN item which matches with the current Package component item
                        currSoLnItem = (From soitem In soLnCmpntsArr
                                        Where soitem("ITM_CD") = cmpntPrcDr("ITM_CD") And soitem("UNIT_PRC") = 0 And soitem("QTY") > 0
                                        Select soitem
                                        ).FirstOrDefault

                        If currSoLnItem IsNot Nothing Then

                            ' TODO - the process of component price split expects qty 1 on each line because the explosion splits to qty 1 but the application allows user to change qty; since this is a unit_prc 
                            '    this is a problem if the user does change the qty
                            If currSoLnItem("QTY") = 1 Then
                                '
                                currSoLnItem("UNIT_PRC") = cmpntPrcDr("CURR_PRC")
                                'currSoLnItem("UNIT_PRC") = cmpntPrcDr("RET_PRC")
                            Else
                                currSoLnItem("UNIT_PRC") = Math.Round(cmpntPrcDr("CURR_PRC") / currSoLnItem("QTY"), 2, MidpointRounding.AwayFromZero)
                                'currSoLnItem("UNIT_PRC") = Math.Round(cmpntPrcDr("RET_PRC") / currSoLnItem("QTY"), 2, MidpointRounding.AwayFromZero)
                            End If
                            accumUnitPrcs = accumUnitPrcs + currSoLnItem("UNIT_PRC")

                            ' figure out the tax charge, only needed if line tax
                            If pkg_tax_chg > 0 AndAlso ConfigurationManager.AppSettings("tax_line").ToString = "Y" AndAlso
                                hdrInf.taxExemptCd.isEmpty AndAlso CDbl(currSoLnItem("UNIT_PRC")) > 0 Then

                                taxPct = TaxUtils.getTaxRate(currSoLnItem("itm_tp_cd"), hdrInf.taxRates)
                                taxChg = Math.Round(CDbl(currSoLnItem("UNIT_PRC")) * (taxPct / 100), TaxUtils.Tax_Constants.taxPrecision)
                                If pkg_tax_chg >= taxChg Then
                                    currSoLnItem("CUST_TAX_CHG") = taxChg
                                    pkg_tax_chg = pkg_tax_chg - taxChg
                                Else
                                    currSoLnItem("CUST_TAX_CHG") = pkg_tax_chg
                                    pkg_tax_chg = 0
                                End If
                            End If
                            ' apply discount portion to this line
                            If remDisc > 0 Then
                                ' discount amt on SO_LN is unit discount amt  - extra calc here to avoid penny rounding issues
                                '   1) get a unit component disc and update to line
                                '   2) calc the line disc amt and use to reduce remaining discount from package hdr
                                '   3) apply either the remaining discount from package or the calculated line disc, whichever is smaller

                                cmpntUnitDisc = Math.Round(((currSoLnItem("UNIT_PRC") * currSoLnItem("QTY")) / pkg("unit_prc") * pkg("disc_amt")) / currSoLnItem("QTY"), 2, MidpointRounding.AwayFromZero)

                                tmpCmpntLnDisc = Math.Round(cmpntUnitDisc * currSoLnItem("QTY"), 2, MidpointRounding.AwayFromZero)
                                If remDisc - tmpCmpntLnDisc > 0 Then

                                    currSoLnItem("DISC_AMT") = cmpntUnitDisc
                                    remDisc = Math.Round(remDisc - tmpCmpntLnDisc, 2, MidpointRounding.AwayFromZero)
                                Else
                                    currSoLnItem("DISC_AMT") = remDisc
                                    remDisc = 0
                                End If
                            End If
                            UpdtSoLnPkgCmpntsPrc(hdrInf, currSoLnItem, dbCmd)
                        End If
                    Next

                    'If accumUnitPrcs <> pkg("unit_prc") Then
                    '    ' TODO - have to split this by pennies same as discounts
                    'End If

                    ' if have remaining disc then apply leftover pennies to 
                    '    1) find line with qty 1 and add as is
                    '    2) find line with qty evenly divisible into remDisc
                    '    3) if do not find above would have to split hte line to get exact penny but not doing this now
                    ' TODO future - there should be a generic routine to do leftover penny application - we have need of it in multiple places, taxes, discounts, component lines
                    If remDisc > 0 Then

                        cndx = 0
                        ' skip the records with qty > 1 and not remDisc not evenly divisible by qty
                        Do While cndx <= soLnCmpntsArr.GetUpperBound(0) AndAlso soLnCmpntsArr(cndx)("QTY") > 1

                            ' multiply times 100 to get rid of the decimal point
                            modResult = Math.Round((remDisc * 100) Mod CDbl(soLnCmpntsArr(cndx)("QTY")), 2, MidpointRounding.AwayFromZero)
                            If modResult = 0.0 Then
                                Exit Do
                            End If
                            cndx = cndx + 1
                        Loop
                        If cndx <= soLnCmpntsArr.GetUpperBound(0) Then
                            ' found a line can apply remaining pennies to

                            If soLnCmpntsArr(cndx)("QTY") = 1 Then
                                soLnCmpntsArr(cndx)("DISC_AMT") = soLnCmpntsArr(cndx)("DISC_AMT") + remDisc
                            Else
                                ' have to turn it into the unit discount
                                soLnCmpntsArr(cndx)("DISC_AMT") = soLnCmpntsArr(cndx)("DISC_AMT") +
                                    Math.Round(remDisc / soLnCmpntsArr(cndx)("QTY"), 2, MidpointRounding.AwayFromZero)
                            End If
                            UpdtSoLnPkgCmpntsPrc(hdrInf, soLnCmpntsArr(cndx), dbCmd)

                            ' else  if there's any leftover discount, it will be lost here, this would need to split the lines so can apply discount by unit equally
                            ' however note that in current processing the package components are split out to qty 1 on a line so this should not be needed
                        End If
                    End If
                    ' update the package price to zero
                    UpdtSoLnPkgCmpntsPrc(hdrInf, pkgs(pndx), dbCmd)
                    pndx = pndx + 1 ' move to next pkg
                Loop

            End If
            dbCmd.Transaction.Commit()

        Catch generatedExceptionName As Exception
            ' Daniela comment do not throw error so that the Sales Order is displayed on screen
            'dbCmd.Transaction.Rollback()
            'Throw generatedExceptionName
            dbCmd.Transaction.Commit()

        Finally
            dbCmd.Dispose()
            dbConn.Close()
            dbConn.Dispose()
        End Try

    End Sub

    ''' <summary>
    ''' Retrieves information for the package header and its components and Update soLnDatSet Dataset according to breaking out oackage price system parmeter "Upon Commit". 
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="DEL_DOC_NUM">the DEL_DOC_NUM.</param>
    ''' <param name="soLnDatSet">the soLnDatSet which have latest data of SOM screen.</param>
    ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    Public Function UpdatePkgCmpntsToSOLNonUponCommit(ByVal headerInfo As SoHeaderDtc,
                                          ByVal pkgSku As String,
                                          ByVal sessID As String,
                                          ByVal DEL_DOC_NUM As String,
                                          ByVal soLnDatSet As DataSet,
                                          Optional ByVal includeARSAvailDate As Boolean = True) As DataTable

        Dim pkgHeaderDataSet As DataSet
        Dim pkgHeaderDataTable As DataTable
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim point_size As String
        Dim comm_cd As String
        Dim tax_cd As String
        Dim tax_pct As Double
        Dim taxable_amt As Double
        Dim Calc_Ret As Double
        Dim warrantable As String
        Dim isInventory As Boolean
        Dim dtSelItm As DataTable
        Dim SPIFF As String

        Try
            dbConnection.Open()

            pkgSku = IIf(Right(pkgSku, 1) = ",", Left(pkgSku, Len(pkgSku) - 1), pkgSku)
            pkgSku = pkgSku.Replace("'", String.Empty)

            If soLnDatSet Is Nothing Then
                soLnDatSet = GetLineItemsForSOM(DEL_DOC_NUM)

                For i = 0 To soLnDatSet.Tables(0).Rows.Count
                    For j = i + 1 To soLnDatSet.Tables(0).Rows.Count - 1

                        If (soLnDatSet.Tables(0).Rows(i).Item("ITM_CD").ToString = soLnDatSet.Tables(0).Rows(j).Item("PKG_SOURCE").ToString) Then
                            soLnDatSet.Tables(0).Rows(j).Item("TYPECODE") = "CMP"
                        End If
                    Next j
                Next i
            Else
                '** Get all the detailed info for the pkg components by calling the API
                Dim expression As String
                Dim foundRows() As DataRow
                Dim solnRowID As Integer
                Dim pkgCmpntsDataSet As DataSet = GetPkgCmpntsRetPrc(pkgSku, dbConnection, soLnDatSet, DEL_DOC_NUM, headerInfo.puDelStore)
                For Each drsoln As DataRow In soLnDatSet.Tables(0).Rows
                    If drsoln("ITM_CD").ToString = pkgSku.ToString And drsoln("UNIT_PRC") <> 0 Then
                        solnRowID = drsoln("DEL_DOC_LN#")
                    End If
                Next
                For Each dr As DataRow In pkgCmpntsDataSet.Tables(0).Rows
                    expression = dr("ITM_CD").ToString

                    For Each drsoln As DataRow In soLnDatSet.Tables(0).Rows

                        If (drsoln("ITM_CD").ToString = expression.ToString) AndAlso Not IsDBNull(drsoln("PACKAGE_PARENT")) AndAlso (drsoln("PACKAGE_PARENT") = solnRowID) AndAlso
                                         (drsoln("DEL_DOC_NUM").ToString = DEL_DOC_NUM) Then
                            tax_cd = ""
                            tax_pct = 0
                            taxable_amt = 0

                            isInventory = If("Y".Equals(drsoln("INVENTORY").ToString), True, False)

                            If (TaxUtils.Determine_Taxability(dr("ITM_TP_CD").ToString, Date.Today, headerInfo.taxCd) = "Y" And String.IsNullOrEmpty(headerInfo.taxExemptCd)) Then

                                tax_cd = headerInfo.taxCd
                                tax_pct = TaxUtils.getTaxRate(dr("itm_tp_cd"), headerInfo.taxRates)

                                If ((ConfigurationManager.AppSettings("package_breakout").ToString + "").Equals("N")) Then
                                    taxable_amt = 0
                                Else
                                    If Not String.IsNullOrEmpty(dr("curr_prc")) Then
                                        Double.TryParse(dr("curr_prc"), taxable_amt)
                                    End If
                                End If
                            End If
                            Calc_Ret = taxable_amt * (tax_pct / 100)
                            'Updating so_ln table for Package Commponet
                            drsoln("UNIT_PRC") = taxable_amt
                            drsoln("ORIG_PRC") = taxable_amt
                            drsoln("TAXABLE_AMT") = taxable_amt
                            drsoln("CUST_TAX_CHG") = Math.Round(Calc_Ret, TaxUtils.Tax_Constants.taxPrecision)
                            drsoln("TAX_BASIS") = taxable_amt
                            drsoln("MAX_RET_PRC") = taxable_amt
                        End If
                    Next
                Next

                If ("C".Equals(ConfigurationManager.AppSettings("package_breakout").ToString + "")) Then
                    For Each drsoln As DataRow In soLnDatSet.Tables(0).Rows
                        If ((drsoln("ITM_CD").ToString = pkgSku.ToString) AndAlso
                            (drsoln("DEL_DOC_NUM").ToString = DEL_DOC_NUM) AndAlso
                            (drsoln("UNIT_PRC").ToString <> 0)) Then

                            'Updating so_ln table for Package header
                            drsoln("UNIT_PRC") = 0
                            drsoln("ORIG_PRC") = 0
                            drsoln("TAXABLE_AMT") = 0
                            drsoln("CUST_TAX_CHG") = 0
                            drsoln("TAX_BASIS") = 0
                        End If
                    Next
                End If

            End If
        Catch generatedExceptionName As Exception
            Throw generatedExceptionName
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return pkgHeaderDataTable

    End Function
    ''' <summary>
    ''' This is method is to get promotion details when promo price is consider against to retail price. 
    ''' </summary>
    ''' <param name="dr">DataRow</param>
    ''' <param name="effDt">Promo calender effective date</param>
    ''' <returns>DataRow</returns>
    ''' As per one of the JIRA requirement(7445) to get promotion calender and store or  strore group level package price
    ''' To re-use the method PopulatePromoDetails which was in the page Price_Tag.aspx.vb, now is moved to SalesBIZ and SalesDac files
    'effDt passed as object since the date value could be null.  if defined As Date throws an exception
    'when trying to cast the date read from a datarow
    Public Function getPromoDetails(ByRef dr As DataRow, ByVal effDt As Object)
        ' Obtains promotion price, code and description using the item and store code - no E1 api available for this.
        ' Note that it is possible not to have a promotional price for the store and effective date
        ' The values found (if any) are set on the passed-in row
        Dim promoFound As Boolean = False
        Dim promoPrc As String = ""
        Dim promoCd As String = ""
        Dim promoDes As String = ""
        Dim pkgSplitMethod As String = ""
        Dim sql As String = ""
        Dim itemCd As String = dr("ITM_CD")
        Dim storeCd As String = dr("STORE_CD")

        If (IsDBNull(effDt)) Then
            effDt = Date.Now
        End If

        If (SysPms.isPrcByStoreGrp) Then
            sql = GetPromPrcStoreQuery(itemCd, storeCd, effDt)
            promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)

            If Not promoFound Then
                sql = GetPromPrcStoreGrpQuery(itemCd, storeCd, effDt)
                promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)
            End If
        Else
            sql = GetPromPrcItmQuery(itemCd, storeCd, effDt)
            promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)
        End If

        If promoFound Then
            dr("promotion_cd") = promoCd
            dr("promotion_desc") = promoDes
            dr("promotion_ret_prc") = promoPrc
        End If
    End Function

    ''' <summary>
    ''' This is method is to get promotion details when promo price is consider against to retail price. 
    ''' </summary>
    ''' <param name="dr">DataRow</param>
    ''' <param name="effDt">Promo calender effective date</param>
    ''' <returns>DataRow</returns>
    ''' As per one of the JIRA requirement(7445) to get promotion calender and store or  strore group level package price
    ''' To re-use the method PopulatePromoDetails which was in the page Price_Tag.aspx.vb, now is moved to SalesBIZ and SalesDac files
    Public Function GetPromoPrice(ByVal pkgSku As String, ByVal store As String, ByVal effDt As Object, ByRef pkgSplitMethod As String)
        Dim promoFound As Boolean = False
        Dim promoPrc As String = ""
        Dim promoCd As String = ""
        Dim promoDes As String = ""
        Dim sql As String = ""
        Dim itemCd As String = pkgSku
        Dim storeCd As String = store

        If (IsDBNull(effDt)) Then
            effDt = Date.Now
        End If

        If (SysPms.isPrcByStoreGrp) Then
            sql = GetPromPrcStoreQuery(itemCd, storeCd, effDt)
            promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)

            If Not promoFound Then
                sql = GetPromPrcStoreGrpQuery(itemCd, storeCd, effDt)
                promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)
            End If
        Else
            sql = GetPromPrcItmQuery(itemCd, storeCd, effDt)
            promoFound = GetPromPrc(sql, promoPrc, promoCd, promoDes, pkgSplitMethod)
        End If

        If promoFound Then
            Return promoPrc
        End If
    End Function
    Private Function GetPromPrcStoreQuery(ByVal itemCd As String, ByVal storeCd As String,
                                                                           ByVal effDt As Date) As String

        'Builds and returns the promotion price query from store using the passed-in item and store code

        ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range against in GetPrcDtRangeQuery; here prom_prc_tp
        Dim sql As String = "SELECT ed.DES, s.PRC, ed.PRC_CD ,I.PKG_SPLIT_METHOD " &
                                 "FROM PROM_PRC_TP ed, STORE_PROM_PRC s ,ITM I " &
                                  "WHERE ed.PRC_CD = s.PRC_CD "
        sql = sql & " AND S.ITM_CD = '" & itemCd & "' AND STORE_CD = '" & storeCd & "' AND I.ITM_CD='" & itemCd & "' " & GetPrcDtRangeQuery(effDt)
        sql = sql & " ORDER BY ed.prc_cd "  'order by courtesy of pcr 75020

        Return sql
    End Function
    Private Function GetPromPrcStoreGrpQuery(ByVal itemCd As String, ByVal storeCd As String,
                                                                                   ByVal effDt As Date) As String

        'Builds and returns the promotion price query from store group using the passed-in item and store code
        ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range 
        ' against in GetPrcDtRangeQuery; here prom_prc_tp

        Dim sql As String = "SELECT ed.DES, s.PRC, ed.PRC_CD,I.PKG_SPLIT_METHOD " &
                            "FROM PROM_PRC_TP ed, STORE_GRP_PROM_PRC s,ITM I " &
                            "WHERE ed.PRC_CD = s.PRC_CD "
        sql = sql & " AND s.ITM_CD = '" & itemCd & "' AND I.ITM_CD='" & itemCd & "' " & GetPrcDtRangeQuery(effDt) &
                            " AND STORE_GRP_CD IN " &
                                            "(SELECT sg.STORE_GRP_CD FROM STORE_GRP$STORE sgs, store_grp sg  " &
                                             "WHERE sgs.STORE_CD ='" & storeCd &
                                             "' AND sg.PRICING_STORE_GRP = 'Y' AND sg.store_grp_cd = sgs.store_grp_cd) "
        sql = sql & " ORDER BY ed.prc_cd " 'order by courtesy of pcr 75020

        Return sql
    End Function
    Private Function GetPromPrcItmQuery(ByVal itemCd As String, ByVal storeCd As String,
                                                                        ByVal effDt As Date) As String

        'Builds and returns the promotion price query using the passed-in item and store code from the itm promo calendar

        ' PROGRAMMING NOTE: use the ed alias to indicate which table using effective date range against in GetPrcDtRangeQuery; here store$prom_clndr
        Dim sql As String = "SELECT p.des, i.prc, i.prc_cd,IT.PKG_SPLIT_METHOD FROM itm$prom_prc_tp i, store$prom_clndr ed, prom_prc_tp p, ITM IT WHERE ed.prc_cd  = i.prc_cd "

        sql = sql & " AND p.prc_cd = i.prc_cd AND p.prc_cd = ed.prc_cd "

        sql = sql & "AND i.ITM_CD = '" & itemCd & "' AND STORE_CD ='" & storeCd & "' AND IT.ITM_CD='" & itemCd & "' " & GetPrcDtRangeQuery(effDt) &
            " ORDER BY i.prc_cd " 'order by courtesy of pcr 75020

        Return sql
    End Function

    Private Function GetPromPrc(ByVal sql As String, ByRef promPrc As String,
                                ByRef promCd As String, ByRef promDes As String, ByRef packageSplitMethod As String) As Boolean

        'Runs the sql statement 
        Dim resultFound As Boolean = False
        Dim conn As OracleConnection = GetConnection()
        Dim rdr As OracleDataReader = Nothing

        Try
            'create command object to work with SELECT
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Connection.Open()

            'get the DataReader object from command object
            rdr = DisposablesManager.BuildOracleDataReader(cmd)


            If rdr.HasRows Then
                rdr.Read()
                promPrc = rdr.Item("PRC")
                promCd = rdr.Item("PRC_CD")
                promDes = rdr.Item("DES")
                packageSplitMethod = If(IsDBNull(rdr.Item("PKG_SPLIT_METHOD")), rdr.Item("PKG_SPLIT_METHOD"), "")
                resultFound = True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not IsNothing(rdr) Then
                rdr.Close()
            End If
            conn.Close()
        End Try

        Return resultFound
    End Function
    Private Function GetPrcDtRangeQuery(ByVal effDt As Date) As String

        'Builds and returns the date range for pricing

        ' PROGRAMMING NOTE: the ed alias must be setup in calling routine for the table to be used for dates
        Dim sql As String = " AND ed.eff_dt <= TO_DATE('" & FormatDateTime(effDt, 2) & "','mm/dd/yyyy')" &
                            " AND ed.end_dt >= TO_DATE('" & FormatDateTime(effDt, 2) & "','mm/dd/yyyy') "

        Return sql
    End Function

    ''' <summary>
    ''' Retrieves information for the package header and its components and creates temp_itm records for each. 
    ''' </summary>
    ''' <param name="headerInfo">The object containing the SO/Order header information.</param>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="sessionID">the ID of the current session.</param>
    ''' <param name="includeARSAvailDate">a flag to indicate if the avail date for an ARS store should be retrieved or not. Default is true.</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    Public Function GetComponentSplitPrices(
                                            ByVal pkgSku As String,
                                            ByVal pkgPrice As String,
                                            ByVal pkgSplitMethod As String,
                                            ByRef conn As OracleConnection,
                                            ByRef errorMessage As String) As DataSet
        Dim pkgHeaderDataSet As DataSet
        pkgHeaderDataSet = GetPkgComponentData(pkgSku, pkgPrice, pkgSplitMethod, conn, errorMessage)

        Return pkgHeaderDataSet

    End Function

    ''' <summary>
    ''' Retrieves all the details relating to the passed-in package sku and also it's components. It calls
    ''' an API to get the pakcage component information. 
    ''' </summary>
    ''' <param name="pkgSku">the package sku</param>
    ''' <param name="pkgPrice">Package Price</param>
    ''' <param name="pkgsplitMethod">Package Split Method</param>
    ''' <param name="conn">the db connection to use for querying</param>
    ''' <returns>the dataset with all the detailed item and other info for the
    '''          specified package sku and its components.</returns>
    Private Function GetPkgComponentData(ByVal pkgSku As String,
                                         ByVal pkgPrice As String,
                                         ByVal pkgsplitMethod As String,
                                         ByRef conn As OracleConnection,
                                         ByRef errorMessage As String) As DataSet

        Dim compntsDataSet As New DataSet
        Dim dbAdapter As OracleDataAdapter
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder()
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        Try
            cmd.Parameters.Clear()
            cmd = DisposablesManager.BuildOracleCommand("bt_iface_sale.get_all_pkg_cmpnt_info", conn)
            cmd.CommandType = CommandType.StoredProcedure
            compntsDataSet = GetPkgCmpntsRetPrc(pkgSku, pkgsplitMethod, CDbl(pkgPrice), cmd)
        Catch ex As OracleException
            'MM-6305
            Select Case ex.Code
                Case 20101
                    errorMessage = HBCG_Utils.FormatOracleException(ex.Message.ToString())
            End Select
        Catch ex2 As Exception
            Throw
        End Try
        Return compntsDataSet
    End Function
    ''' <summary>
    ''' Updates the package price and tax in headder row. this is usefull when the packet split is set to UPON COMMIT!
    ''' </summary>
    ''' <param name="skuRowID">Row id to be updated.</param>
    ''' <param name="pkgPrice">New price to be updated</param>
    ''' <param name="taxPct">Tax percentage</param>
    ''' <param name="conn">Reference type, Connection object</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    ''' 
    Public Function UpdateNewPkgHdrPrcForTmpOrder(
                                            ByVal skuRowID As String,
                                            ByRef amtToReduceFromPkg As String,
                                            ByRef taxPct As String,
                                            ByRef conn As OracleConnection
                                            )

        Dim sbQuery = New StringBuilder()
        sbQuery.Append(" UPDATE TEMP_ITM SET RET_PRC  = RET_PRC - " + amtToReduceFromPkg)
        'sbQuery.Append(" , TAX_AMT = (RET_PRC - " + pkgPrice + ") * " + taxPct)
        sbQuery.Append(" WHERE ROW_ID =  " + skuRowID)
        'sbQuery.Append(" IN(SELECT PACKAGE_PARENT FROM TEMP_ITM WHERE ROW_ID = " + pkgItmSkuRowID + " ) ")
        Dim cmd As OracleCommand = GetCommand(sbQuery.ToString(), conn)
        cmd.ExecuteNonQuery()
    End Function

    ''' <summary>
    ''' Retrieves information for the package header for the deleted item in SOE. This retrives the data from temp_ITM table, package headder row only. this is usefull when the packet split is set to UPON COMMIT!
    ''' </summary>
    ''' <param name="headerInfo">This Method extracts the package headder details for the given RowID of the temp_itm table.</param>
    ''' <param name="pkgPrice">Reference type, which holds the package price stored in the package headder row of temp_itm table</param>
    ''' <param name="pkgSplitMethod">reference type, the package split method type.</param>
    ''' <param name="pkgParentItm">Reference type, holds the package headder itm code</param>
    ''' <param name="conn">Reference type, Connection object</param>
    ''' <returns>a data table of component SKUs (item codes)</returns>
    ''' 
    Public Function GetTmpItmHeadderDetails(
                                            ByVal skuRowID As String,
                                            ByRef pkgPrice As String,
                                            ByRef pkgSplitMethod As String,
                                            ByRef pkgParentItm As String,
                                            ByRef pkgRowID As String,
                                            ByRef conn As OracleConnection
                                            )
        Dim sbQuery As StringBuilder = New StringBuilder()
        sbQuery.Append(" SELECT NVL(I.PKG_SPLIT_METHOD,'') PKG_SPLIT_METHOD,T1.ITM_CD as PACKAGESKU, T1.ROW_ID PKGROWID,  T1.RET_PRC NEWPKGPRICE ")
        sbQuery.Append(" FROM TEMP_ITM T1 ")
        sbQuery.Append(" JOIN TEMP_ITM T2 ON T1.ROW_ID = T2.PACKAGE_PARENT ")
        sbQuery.Append(" JOIN ITM I ON T1.ITM_CD = I.ITM_CD  ")
        sbQuery.Append(" WHERE T2.ROW_ID =" + skuRowID)
        'sbQuery.Append(" AND T2.RET_PRC = 0   ")
        Dim taxability As String = String.Empty
        Dim tax_cd As String
        Dim tax_pct As Double
        Dim cmd As OracleCommand = GetCommand(sbQuery.ToString(), conn)
        Dim datRdr As OracleDataReader
        datRdr = DisposablesManager.BuildOracleDataReader(cmd)

        Dim OriginalPackagePrice As String = String.Empty
        If (datRdr.Read()) Then
            pkgSplitMethod = datRdr.Item("PKG_SPLIT_METHOD").ToString
            pkgPrice = datRdr.Item("NEWPKGPRICE").ToString
            pkgParentItm = datRdr.Item("packageSKU").ToString
            pkgRowID = datRdr.Item("PKGROWID").ToString
        End If
        datRdr.Close()
        Dim ds As DataSet = GetPkgComponentData(pkgParentItm, pkgPrice, pkgSplitMethod, conn, "")
        Return ds
    End Function

    ''' <summary>
    ''' This is method is to check whether the warranty is linked to other sales order other than original order.
    ''' </summary>
    ''' <param name="Deldocnum">sale order id (del doc number)</param>
    ''' <returns>True if the warranty is linked to any other sales order, other than the oridinal sales order</returns>
    Public Function CheckWarrLink(ByVal delDocNum As String, Optional ByVal Iswarr As Boolean = False) As Boolean
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim checkWarrLinkFlag As Boolean = False

        If Iswarr Then
            sqlSb.Append("select count(1) as WarrLinkCount from SO_LN2WARR where WARR_DEL_DOC_NUM=:docNum and DEL_DOC_NUM =:docNum")
        Else
            sqlSb.Append("select count(1) as WarrLinkCount from SO_LN2WARR where WARR_DEL_DOC_NUM=:docNum and DEL_DOC_NUM !=:docNum")
        End If

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbCommand.Parameters.Add(":docNum", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":docNum").Value = delDocNum.ToUpper()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                Dim WarrLinkCount As Integer = dbReader.GetValue(0)
                If (WarrLinkCount > 0) Then
                    checkWarrLinkFlag = True
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return checkWarrLinkFlag
    End Function

    ''' <summary>
    ''' This method will return the exist warranty list for this session
    ''' </summary>
    ''' <param name="sessionId">the session Id for getting the exist warranty list</param>
    ''' <returns>A datatable filled with exist warranty list</returns>
    ''' <remarks></remarks>
    Public Function GetTempExistWarrs(ByVal sessionId As String) As DataTable
        Dim sbSql As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dtWarr As New DataTable

        ' add ln_num to itm_cd to make same SKU different in usage, add % to be able to strip off the trailer and get the SKU
        sbSql.Append("  SELECT   ITM_CD,")
        sbSql.Append("           RET_PRC,")
        sbSql.Append("              TO_CHAR (RET_PRC, '$9999.99')")
        sbSql.Append("           || '   --  '")
        sbSql.Append("           || VSN")
        sbSql.Append("           || ' ('")
        sbSql.Append("           || DAYS")
        sbSql.Append("           || ' DAYS)'")
        sbSql.Append("              AS DES,")
        sbSql.Append("           LN_NUM")
        sbSql.Append("    FROM   TEMP_WARR")
        sbSql.Append("   WHERE   SESSION_ID = :SESSID ")
        sbSql.Append("ORDER BY   DAYS")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sbSql.ToString, dbConnection)
            dbCommand.Parameters.Add(":SESSID", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":SESSID").Value = sessionId
            oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(dtWarr)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return dtWarr
    End Function

    ''' <summary>
    ''' Deletes ALL records from TEMP_WARR tables that match the sessionID passed in.
    ''' </summary>
    ''' <param name="sessionId">the sessionID to delete the temp order</param>
    ''' <remarks></remarks>
    Public Sub DeleteTempWarrEntry(ByVal sessionId As String)
        Dim sql As String = "DELETE FROM TEMP_WARR WHERE SESSION_ID = :SESSID"
        Dim dbCommand As OracleCommand
        Dim dbConnection As OracleConnection = GetConnection()

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":SESSID", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":SESSID").Value = sessionId
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' this method will return the Confirmed Status Code
    ''' </summary>
    ''' <returns>Confirmed status code</returns>
    Public Function getConfirmedStatusCode() As String
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim ConfirmedCode As String = String.Empty
        sqlSb.Append("SELECT CONF_STAT_CD FROM SO_CONF_STAT WHERE IS_CONFIRMED = 'Y'")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                ConfirmedCode = dbReader.GetValue(0)
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return ConfirmedCode
    End Function

    ''' <summary>
    ''' Returns the Return Cause Code(s)
    ''' </summary>
    ''' <returns>datatable with return cause code(s)</returns>
    ''' <remarks>To display the return cause code(s) on sales exchange screen</remarks>
    Public Function GetCRMCauseCodes() As DataTable
        Dim dbSql As String = "SELECT CRM_CAUSE_CD, DES, CRM_CAUSE_CD || ' - ' || DES AS FULL_DESC FROM CRM_CAUSE ORDER BY CRM_CAUSE_CD"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim dtRetCauseCd As New DataTable
        Try
            dbConnection.Open()
            With dbCommand
                .Connection = dbConnection
                .CommandText = dbSql
            End With

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(dtRetCauseCd)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return dtRetCauseCd
    End Function

    ''' <summary>
    ''' Returns the SUM of the Retail for an Item from TEMP_ITM table
    ''' </summary>
    ''' <param name="itemCode">item code</param>
    ''' <param name="sessionId">session id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalItemRetail(ByVal itemCode As String, ByVal sessionId As String) As Double
        Dim dbSql As String = "SELECT NVL(SUM(RET_PRC), 0) RET_PRC FROM TEMP_ITM WHERE SESSION_ID = :SESSION_ID AND ITM_CD = :ITM_CD "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim maxCredit As Double = 0.0

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":SESSION_ID", OracleType.VarChar)
            dbCommand.Parameters(":SESSION_ID").Value = sessionId

            dbCommand.Parameters.Add(":ITM_CD", OracleType.VarChar)
            dbCommand.Parameters(":ITM_CD").Value = itemCode

            maxCredit = Convert.ToDouble(dbCommand.ExecuteScalar())
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return maxCredit
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sessionId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSalesItemForSOE(sessionId As String) As DataTable
        Dim dtSKUs As New DataTable

        Dim sbSql As New StringBuilder
        sbSql.Append("  SELECT   TEMP_ITM.ROW_ID,")
        sbSql.Append("           ITM.ITM_CD,")
        sbSql.Append("           ITM.ITM_TP_CD,")
        sbSql.Append("           ITM.INVENTORY,")
        sbSql.Append("           ITM.WARRANTABLE,")
        sbSql.Append("           TEMP_ITM.PACKAGE_PARENT,")
        sbSql.Append("           TEMP_ITM.WARR_ROW_LINK AS WARR_ROW_ID,")
        sbSql.Append("           TEMP_ITM.WARR_ITM_LINK AS WARR_ITM_CD")
        sbSql.Append("    FROM      TEMP_ITM")
        sbSql.Append("           JOIN")
        sbSql.Append("              ITM")
        sbSql.Append("           ON TEMP_ITM.ITM_CD = ITM.ITM_CD")
        sbSql.Append("   WHERE   SESSION_ID = :SESSION_ID")
        sbSql.Append(" ORDER BY   TEMP_ITM.ROW_ID")

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sbSql.ToString(), dbConnection)

        Try
            dbConnection.Open()
            With dbCommand
                .Connection = dbConnection
                .CommandText = sbSql.ToString()
                .Parameters.Add(":SESSION_ID", OracleType.VarChar).Direction = ParameterDirection.Input
                .Parameters(":SESSION_ID").Value = sessionId
            End With

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(dtSKUs)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return dtSKUs

    End Function

    ''' <summary>
    ''' All the entered warrantable SKUs with comma (',') separator
    ''' </summary>
    ''' <param name="warrCd">Selected Warranty Code</param>
    ''' <param name="sessionId">sessionId</param>
    ''' <returns>A datatable with SKUs which are having the selected warranty</returns>
    ''' <remarks></remarks>
    Public Function GetItemsForWarranty(warrCd As String, items As String) As DataTable
        Dim dtSKUs As New DataTable

        Dim sbSql As New StringBuilder
        sbSql.Append("SELECT T.ITM_CD, T.ITM_CD || '  -  ' || DES AS ITEM FROM ITM2ITM_WARR T LEFT JOIN ITM I ON T.ITM_CD = I.ITM_CD ")
        sbSql.Append("WHERE WAR_ITM_CD = :WAR_ITM_CD AND T.ITM_CD IN (" & items & ")")
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sbSql.ToString(), dbConnection)

        Try
            dbConnection.Open()
            With dbCommand
                .Connection = dbConnection
                .CommandText = sbSql.ToString()
                .Parameters.Add(":WAR_ITM_CD", OracleType.VarChar).Direction = ParameterDirection.Input
                .Parameters(":WAR_ITM_CD").Value = warrCd
            End With

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            oAdp.Fill(dtSKUs)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return dtSKUs
    End Function

    ''' <summary>
    ''' Inserts into RELATIONSHIP_LN2DISC table, all discounts that were created for the relationship passed in
    ''' </summary>
    ''' <param name="REL_NO">the relationship number the lines belong to</param>
    ''' <param name="sessionId"> sessionId will help to get detail of that session</param>
    Public Sub ProcessRelnDiscounts(ByVal relnum As String, ByVal sessionId As String)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand(conn)
        'Dim MyDatareader As OracleDataReader

        Dim dbReader As OracleDataReader
        Dim sbSql As StringBuilder = New StringBuilder()
        Dim LINE As Integer = 1
        Dim ITM_CD As String = String.Empty
        Dim discPct As Object
        'Dim discSeqNum As Integer = 1

        Try
            If Not String.IsNullOrEmpty(sessionId) Then

                sbSql.Append("SELECT TEMP_ITM.SO_LN_NO, MULTI_DISC.DISC_AMT, MULTI_DISC.DISC_CD, MULTI_DISC.DISC_PCT, MULTI_DISC.DISC_LEVEL,TEMP_ITM.ITM_CD ,MULTI_DISC.SEQ_NUM  ")
                sbSql.Append("FROM TEMP_ITM, MULTI_DISC ")
                sbSql.Append("WHERE TEMP_ITM.SESSION_ID = :sessId AND TEMP_ITM.ROW_ID = MULTI_DISC.LINE_ID ")
                sbSql.Append("ORDER BY row_id")

                conn.Open()
                objsql.CommandText = sbSql.ToString()
                objsql.Parameters.Add(":sessId", OracleType.VarChar)
                objsql.Parameters(":sessId").Value = sessionId.ToString.Trim
                dbReader = DisposablesManager.BuildOracleDataReader(objsql)   'has to use same command, otherwise SO_LN_NO values are empty


                If (dbReader.HasRows()) Then
                    sbSql.Clear()
                    'sbSql.Append("INSERT INTO RELATIONSHIP_LINES_DESC (REL_NO, LINE, DISC_CD, DISC_AMT, DISC_PCT, DISC_LEVEL, SEQ_NUM) ")
                    'sbSql.Append("VALUES(:REL_NO, :LINE, :discCd, :discAmt, :discPct, :discLevel, :discSeqNum)")
                    sbSql.Append("INSERT INTO RELATIONSHIP_LN2DISC (REL_NO, LINE, DISC_CD, DISC_AMT, DISC_PCT, DISC_LEVEL, SEQ_NUM) ")
                    sbSql.Append("VALUES(:REL_NO, :LINE, :discCd, :discAmt, :discPct, :discLevel, :discSeqNum)")

                    objsql.CommandText = sbSql.ToString()
                    objsql.Parameters.Clear()
                    objsql.Parameters.Add(":REL_NO", OracleType.VarChar)
                    objsql.Parameters.Add(":LINE", OracleType.VarChar)
                    objsql.Parameters.Add(":discCd", OracleType.VarChar)
                    objsql.Parameters.Add(":discAmt", OracleType.VarChar)
                    objsql.Parameters.Add(":discPct", OracleType.VarChar)
                    objsql.Parameters.Add(":discLevel", OracleType.VarChar)
                    objsql.Parameters.Add(":discSeqNum", OracleType.VarChar)

                    Do While dbReader.Read()
                        'first line being processed
                        'If LINE.isEmpty Then LINE = If(dbReader.Item("SO_LN_NO").ToString.isEmpty, 1, dbReader.Item("SO_LN_NO"))
                        If ITM_CD.isEmpty Then ITM_CD = dbReader.Item("ITM_CD").ToString

                        ''second to last line being processed
                        If ITM_CD <> dbReader.Item("ITM_CD").ToString Then
                            ITM_CD = dbReader.Item("ITM_CD").ToString
                            'discSeqNum = 1     'to start a new sequence number for the next order line
                            LINE += 1
                        End If

                        discPct = DBNull.Value
                        If (Not IsDBNull(dbReader.Item("DISC_PCT"))) Then discPct = dbReader.Item("DISC_PCT").ToString

                        objsql.Parameters(":REL_NO").Value = relnum
                        objsql.Parameters(":LINE").Value = LINE
                        objsql.Parameters(":discCd").Value = dbReader.Item("DISC_CD").ToString
                        objsql.Parameters(":discAmt").Value = dbReader.Item("DISC_AMT").ToString
                        objsql.Parameters(":discPct").Value = discPct
                        objsql.Parameters(":discLevel").Value = dbReader.Item("DISC_LEVEL").ToString
                        'objsql.Parameters(":discSeqNum").Value = discSeqNum
                        objsql.Parameters(":discSeqNum").Value = dbReader.Item("SEQ_NUM").ToString
                        objsql.ExecuteNonQuery()

                        'discSeqNum += 1

                    Loop
                End If
                dbReader.Close()
                objsql.Parameters.Clear()
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Inserts into RELATIONSHIP_LN2DISC table, all discounts that were created for the document passed in
    ''' </summary>
    ''' <param name="REL_NO">the document number the lines belong to</param>
    ''' ''' <param name="REL_NO">the document number the lines belong to</param>
    Public Sub ProcessRelnDiscounts(ByVal relnum As String, ByVal relnTable As DataTable)

        Dim dtDisc As DataTable = relnTable
        Dim sbSql As StringBuilder = New StringBuilder()
        Dim relnNum As String = String.Empty
        Dim discPct As Object
        Dim discSeqNum As Integer = 1

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(conn)
        Dim LINE As Integer = 1
        Dim ITM_CD As String = String.Empty


        '=== Sorts discounts by the lnSeqNum
        Dim discRows = (From row In dtDisc.AsEnumerable() Order By row.Item("LNSEQ_NUM") Ascending Select row)
        dtDisc = discRows.AsDataView().ToTable()
        Try
            '=== Deletes all previous discounts
            sbSql.Append("DELETE FROM RELATIONSHIP_LN2DISC WHERE REL_NO = :REL_NO")
            conn.Open()
            dbCommand.CommandText = sbSql.ToString()
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.AddWithValue(":REL_NO", relnum)
            dbCommand.ExecuteNonQuery()

            '=== Inserts ALL non-deleted discounts
            sbSql.Clear()
            sbSql.Append("INSERT INTO RELATIONSHIP_LN2DISC (REL_NO, LINE, DISC_CD, DISC_AMT, DISC_PCT, DISC_LEVEL, SEQ_NUM) ")
            sbSql.Append("VALUES(:REL_NO, :LINE, :discCd, :discAmt, :discPct, :discLevel,  :discSeqNum)")

            dbCommand.CommandText = sbSql.ToString()
            dbCommand.Parameters.Clear()
            dbCommand.Parameters.Add(":REL_NO", OracleType.VarChar)
            dbCommand.Parameters.Add(":LINE", OracleType.VarChar)
            dbCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbCommand.Parameters.Add(":discAmt", OracleType.VarChar)
            dbCommand.Parameters.Add(":discPct", OracleType.VarChar)
            dbCommand.Parameters.Add(":discLevel", OracleType.VarChar)
            dbCommand.Parameters.Add(":discSeqNum", OracleType.VarChar)

            For Each dr As DataRow In dtDisc.Rows
                'first line being processed
                If relnNum.isEmpty Then relnNum = dr.Item("LNSEQ_NUM").ToString

                'second to last line being processed
                If (dr.Item("DISC_IS_DELETED")) Then Continue For 'skips deleted discounts

                If relnNum <> dr.Item("LNSEQ_NUM").ToString Then
                    relnNum = dr.Item("LNSEQ_NUM").ToString
                    discSeqNum = 1     'to start a new sequence number for the next order line
                End If

                discPct = DBNull.Value
                If (Not IsDBNull(dr.Item("DISC_PCT"))) Then discPct = dr.Item("DISC_PCT").ToString

                dbCommand.Parameters(":REL_NO").Value = relnum
                dbCommand.Parameters(":LINE").Value = relnNum
                dbCommand.Parameters(":discCd").Value = dr.Item("DISC_CD").ToString
                dbCommand.Parameters(":discAmt").Value = dr.Item("DISC_AMT").ToString
                dbCommand.Parameters(":discPct").Value = discPct
                dbCommand.Parameters(":discLevel").Value = dr.Item("DISC_LEVEL").ToString
                dbCommand.Parameters(":discSeqNum").Value = discSeqNum
                dbCommand.ExecuteNonQuery()

                discSeqNum = discSeqNum + 1
            Next
            dbCommand.Parameters.Clear()
        Catch ex As Exception
            conn.Open()
        End Try
    End Sub

    ''' <summary>
    ''' Get All the warranted line for the line.
    ''' </summary>
    ''' <param name="lineNum"></param>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllWarrantedLines(ByVal lineNum As Integer, ByVal delDocNum As String) As DataTable
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dtDelDocNum As DataTable = New DataTable
        Dim dtAdaptr As OracleDataAdapter
        sqlSb.Append("SELECT DEL_DOC_NUM FROM SO_LN2WARR WHERE WARR_DEL_DOC_NUM=:DOCNUM AND WARR_DEL_DOC_LN#=:LNNUM")
        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
            dbCommand.Parameters.Add(":LNNUM", OracleType.Int32).Direction = ParameterDirection.Input
            dbCommand.Parameters(":LNNUM").Value = lineNum
            dtAdaptr = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dtAdaptr.Fill(dtDelDocNum)
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return dtDelDocNum
    End Function

    ''' <summary>
    ''' Overloaded method:Get All the warranted line for the line.
    ''' </summary>
    ''' <param name="dbCommand"></param>
    ''' <param name="lineNum"></param>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllWarrantedLines(ByRef dbCommand As OracleCommand, ByVal lineNum As Integer, ByVal delDocNum As String) As DataTable
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dtDelDocNum As DataTable = New DataTable
        Dim dtAdaptr As OracleDataAdapter
        sqlSb.Append("SELECT DEL_DOC_NUM FROM SO_LN2WARR WHERE WARR_DEL_DOC_NUM=:DOCNUM AND WARR_DEL_DOC_LN#=:LNNUM")
        Try
            dbCommand.Parameters.Clear()
            dbCommand.CommandText = sqlSb.ToString()
            dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
            dbCommand.Parameters.Add(":LNNUM", OracleType.Int32).Direction = ParameterDirection.Input
            dbCommand.Parameters(":LNNUM").Value = lineNum
            dtAdaptr = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dtAdaptr.Fill(dtDelDocNum)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return dtDelDocNum
    End Function

    ''' <summary>
    ''' Get the warranty line and the status for that sale.
    ''' </summary>
    ''' <param name="dbCommand"></param>
    ''' <param name="lineNum"></param>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSoWarrantyLine(ByRef dbCommand As OracleCommand, ByVal lineNum As Integer, ByVal delDocNum As String) As SoLineWarranty
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbReader As OracleDataReader
        Dim soLnWarr As SoLineWarranty = New SoLineWarranty()
        sqlSb.Append("SELECT SLN.WARR_DEL_DOC_NUM,SLN.WARR_DEL_DOC_LN#,S.STAT_CD FROM SO_LN2WARR SLN ")
        sqlSb.Append(" INNER JOIN SO S ON  SLN.WARR_DEL_DOC_NUM=S.DEL_DOC_NUM WHERE SLN.DEL_DOC_NUM =:DOCNUM AND SLN.DEL_DOC_LN#=:LNNUM")
        Try
            dbCommand.Parameters.Clear()
            dbCommand.CommandText = sqlSb.ToString()
            dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
            dbCommand.Parameters.Add(":LNNUM", OracleType.Int32).Direction = ParameterDirection.Input
            dbCommand.Parameters(":LNNUM").Value = lineNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                soLnWarr.warDelDocNUM = dbReader("WARR_DEL_DOC_NUM")
                soLnWarr.warDelDocLn = dbReader("WARR_DEL_DOC_LN#")
                soLnWarr.statCd = dbReader("STAT_CD")
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return soLnWarr
    End Function

    ''' <summary>
    ''' Over Loaded method:Get the warranty line and the status for that sale.
    ''' </summary>
    ''' <param name="lineNum"></param>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSoWarrantyLine(ByVal lineNum As Integer, ByVal delDocNum As String) As SoLineWarranty
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim soLnWarr As SoLineWarranty = New SoLineWarranty()
        sqlSb.Append("SELECT SLN.WARR_DEL_DOC_NUM,SLN.WARR_DEL_DOC_LN#,S.STAT_CD FROM SO_LN2WARR SLN ")
        sqlSb.Append(" INNER JOIN SO S ON  SLN.WARR_DEL_DOC_NUM=S.DEL_DOC_NUM WHERE SLN.DEL_DOC_NUM =:DOCNUM AND SLN.DEL_DOC_LN#=:LNNUM")
        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
            dbCommand.Parameters.Add(":LNNUM", OracleType.Int32).Direction = ParameterDirection.Input
            dbCommand.Parameters(":LNNUM").Value = lineNum
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() Then
                soLnWarr.warDelDocNUM = dbReader("WARR_DEL_DOC_NUM")
                soLnWarr.warDelDocLn = dbReader("WARR_DEL_DOC_LN#")
                soLnWarr.statCd = dbReader("STAT_CD")
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return soLnWarr
    End Function

    ''' <summary>
    ''' Validate the split sale has any refernce in other document.
    ''' </summary>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateSalesOrder(ByVal delDocNum As String) As Boolean
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()
        Dim soLnWarr As SoLineWarranty = New SoLineWarranty()
        Dim retuenVal As Boolean = False
        sqlSb.Append("SELECT * FROM SO_LN2WARR WHERE DEL_DOC_NUM=:DOCNUM AND DEL_DOC_NUM!=WARR_DEL_DOC_NUM")
        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows Then
                Return True
            Else
                sqlSb.Clear()
                dbCommand.Parameters.Clear()
                sqlSb.Append("SELECT * FROM SO_LN2WARR WHERE WARR_DEL_DOC_NUM=:DOCNUM AND DEL_DOC_NUM!=WARR_DEL_DOC_NUM")
                dbCommand.CommandText = sqlSb.ToString
                dbCommand.Parameters.Add(":DOCNUM", OracleType.VarChar).Direction = ParameterDirection.Input
                dbCommand.Parameters(":DOCNUM").Value = delDocNum.ToUpper()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If dbReader.HasRows Then
                    Return True
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return retuenVal
    End Function

    ''' <summary>
    ''' Get the last DEL_DOC_LN# from SO_LN table for the input delDocNum
    ''' </summary>
    ''' <param name="delDocNum">DelDocNum</param>
    ''' <returns>DelDocLn</returns>
    ''' <remarks></remarks>
    Public Function GetCurrDelDocLine(ByVal delDocNum As String) As Integer
        Dim currLine As Integer = 1

        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim dbConnection As OracleConnection = GetConnection()

        sqlSb.Append("  SELECT   DEL_DOC_LN#")
        sqlSb.Append("    FROM   SO_LN")
        sqlSb.Append("   WHERE   SO_DOC_NUM = :SO_DOC_NUM")
        sqlSb.Append(" ORDER BY   DEL_DOC_LN# DESC")

        Try
            dbConnection.Open()
            dbCommand = GetCommand(sqlSb.ToString, dbConnection)
            dbCommand.Parameters.Add(":SO_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters(":SO_DOC_NUM").Value = delDocNum.ToUpper()
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.HasRows AndAlso dbReader.Read() AndAlso
                IsNumeric(dbReader.Item("DEL_DOC_LN#").ToString) Then
                currLine = CInt((dbReader.Item("DEL_DOC_LN#").ToString))
            End If
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return currLine
    End Function
    'MM-5693
    ''' <summary>
    ''' Updating GL_DOC_TRN table if it is finalized Order
    ''' </summary>
    ''' <param name="Sql"></param>
    ''' <param name="dbAtomicCommand"></param>
    ''' <param name="del_doc_num"></param>
    ''' <remarks></remarks>
    Public Sub UpdateGldoctrn(ByRef dbAtomicCommand As OracleCommand, ByVal del_doc_num As String, ByVal CO_CD As String)
        Dim Sql As String = "INSERT INTO GL_DOC_TRN (DOC_NUM, CO_CD, POST_DT, ORIGIN_CD, PROCESSED) "
        Sql = Sql & "VALUES (:DOC_NUM, :CO_CD, SYSDATE, :ORIGIN_CD, :PROCESSED)"

        dbAtomicCommand.CommandText = Sql
        dbAtomicCommand.Parameters.Add(":DOC_NUM", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
        dbAtomicCommand.Parameters.Add(":PROCESSED", OracleType.VarChar)

        dbAtomicCommand.Parameters(":DOC_NUM").Value = del_doc_num
        dbAtomicCommand.Parameters(":CO_CD").Value = CO_CD
        dbAtomicCommand.Parameters(":ORIGIN_CD").Value = "PSOE"
        dbAtomicCommand.Parameters(":PROCESSED").Value = "N"
        dbAtomicCommand.ExecuteNonQuery()
        dbAtomicCommand.Parameters.Clear()
    End Sub

    Public Function GetTempItemDataForPkgCmpntsUrl(ByVal sessionId As String) As DataSet

        Dim tempItmDataset As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        ' Daniela changed to add URL
        sql.Append("SELECT ITM_CD , COMM_CD, QTY, RET_PRC, VE_CD, ITM_TP_CD, VSN, DEL_PTS, DES, RET_PRC, INVENTORY, mnr_cd, ")
        sql.Append("(select image_url from WEB_CACHE t where t.itm_cd = a.ITM_CD and rownum <= 1 and (select value_1 from leons_system where key = 'IMG_LK_POS') = 'Y') itm_url ")
        sql.Append("from ( ")
        sql.Append("SELECT ti.ITM_CD, ti.COMM_CD,SUM(QTY) QTY, ti.RET_PRC, ti.VE_CD, ti.ITM_TP_CD, ti.VSN, ti.DEL_PTS, ti.DES, ti.RET_PRC AS RECPRC2, ITM.INVENTORY, itm.mnr_cd ")
        sql.Append("FROM itm, TEMP_ITM ti ")
        sql.Append("WHERE ti.itm_cd = itm.itm_cd ")
        sql.Append("AND  SESSION_ID = :sessId ")
        sql.Append("GROUP BY ti.ITM_CD, ti.COMM_CD, ti.RET_PRC, ti.VE_CD, ti.ITM_TP_CD, ti.VSN, ti.DEL_PTS, ti.DES, ti.RET_PRC, ITM.INVENTORY, itm.mnr_cd ")
        sql.Append(") a")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add("sessId", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCommand.Parameters("sessId").Value = sessionId

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(tempItmDataset)
            dbAdapter.Dispose()
            'Remove the read items from the temp table
            sql.Clear()
            sql.Append("DELETE FROM TEMP_ITM WHERE SESSION_ID = :sessId")
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return tempItmDataset
    End Function


    Public Function LookupOrders(ByVal req As OrderRequestDtc) As DataSet
        ' Left to test as web service (faster testing)
        'Public Function OrdLookup(ByVal delDocNum As String, ByVal wrDt As String, ByVal ordTp As String, ByVal custCd As String, ByVal statCd As String,
        '                          ByVal shipFName As String, ByVal shipLName As String, ByVal shipHPhone As String, ByVal shipBPhone As String,
        '                          ByVal shipAddr1 As String, ByVal shipAddr2 As String, ByVal shipCity As String, ByVal shipState As String,
        '                          ByVal shipZip As String, ByVal puDel As String, ByVal corpName As String, ByVal soStore As String,
        '                          ByVal puDelDt As String) As DataSet

        'Dim req As New OrderQueryRequest
        'req.docNum = delDocNum
        'req.empCdOp = "DSACD"
        'req.wrDt = wrDt
        'req.ordTp = ordTp
        'req.custCd = custCd
        'req.statCd = statCd
        'req.shipFName = shipFName
        'req.shipLName = shipLName

        'req.shipHPhone = shipHPhone
        'req.shipBPhone = shipBPhone
        'req.shipAddr1 = shipAddr1
        'req.shipAddr2 = shipAddr2
        'req.shipcity = shipCity
        'req.shipState = shipState
        'req.shipZip = shipZip
        'req.puDel = puDel
        'req.puDelDt = puDelDt
        'req.soStore = soStore
        'req.corpName = corpName

        Dim soDatSet As DataSet

        If IsNothing(req) OrElse (isEmpty(req.docNum) AndAlso isEmpty(req.wrDt) AndAlso isEmpty(req.ordTp) AndAlso
                                  isEmpty(req.custCd) AndAlso isEmpty(req.statCd) AndAlso isEmpty(req.shipFName) AndAlso
                                  isEmpty(req.shipLName) AndAlso isEmpty(req.shipHPhone) AndAlso isEmpty(req.shipBPhone) AndAlso
                                  isEmpty(req.shipAddr1) AndAlso isEmpty(req.shipAddr2) AndAlso isEmpty(req.shipcity) AndAlso
                                  isEmpty(req.shipState) AndAlso isEmpty(req.shipZip) AndAlso
                                  isEmpty(req.slspInit1) AndAlso isEmpty(req.slspInit2) AndAlso isEmpty(req.puDel) AndAlso
                                  isEmpty(req.puDelDt) AndAlso isEmpty(req.corpName) AndAlso isEmpty(req.soStore)) AndAlso
                                  isEmpty(req.custTpCd) AndAlso isEmpty(req.allForSlspCd) AndAlso isEmpty(req.orderUserField1) AndAlso isEmpty(req.orderUserField2) AndAlso
                                  isEmpty(req.orderUserField3) AndAlso isEmpty(req.orderUserField4) AndAlso isEmpty(req.orderUserField5) AndAlso req.orderQueryRequestTo <> "0" AndAlso
                                  req.orderQueryRequestFrom <> "0" AndAlso isEmpty(req.orderZone) AndAlso
                                  isEmpty(req.custTpCd) AndAlso isEmpty(req.allForSlspCd) AndAlso isEmpty(req.confStatusCd) AndAlso isEmpty(req.poNumber) AndAlso isEmpty(req.emailAddr) Then
            ' DO NOTHING - NO INPUT

        Else
            Dim sqlSelStmtSb As New StringBuilder
            Dim sqlStmtSb As New StringBuilder
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand


            sqlSelStmtSb.Append("SELECT  ")

            If (req.queryPaidInFull) Then
                sqlSelStmtSb.Append("CASE WHEN (SELECT value FROM gp_parms WHERE key = 'PTS_POS+' AND parm = 'PAID_IN_FULL_CALC') <>  'CUSTOM' ")
                sqlSelStmtSb.Append("THEN bt_sales_util.is_paid_in_full(so.del_doc_num, so.cust_cd, so.so_store_cd, so.ord_tp_cd, so.orig_fi_amt, so.fin_cust_cd, so.approval_cd) ")
                sqlSelStmtSb.Append("ELSE custom_paid.is_order_paid_in_full(so.ord_tp_cd, so.del_doc_num, so.cust_cd) ")
                sqlSelStmtSb.Append("END PaidInFull, ")
            End If

            sqlSelStmtSb.Append("del_doc_num,  NVL(so.conf_stat_cd,'-') confirmed, so_wr_dt, stat_cd, ord_tp_cd, so_store_cd, so_seq_num, so.cust_cd, str.co_cd ")

            If req.shipToName Then
                sqlSelStmtSb.Append(", NVL(ship_to_corp, ship_to_l_name ||', ' || ship_to_f_name) AS full_name ")
            End If
            If req.shipToHPhone Then
                sqlSelStmtSb.Append(", ship_to_h_phone ")
            End If
            If req.shipToZone Then
                sqlSelStmtSb.Append(", ship_to_zone_cd, ship_to_zip_cd ")
            End If
            If req.puDelInfo Then
                sqlSelStmtSb.Append(", pu_del_dt, so.pu_del, so.pu_del_store_cd ")
            End If
            If req.slspCds Then
                sqlSelStmtSb.Append(", so.so_emp_slsp_cd1 AS hdr_slsp_cd1, so.so_emp_slsp_cd2 AS hdr_slsp_cd2 ")
            End If
            If req.slspPcts Then
                sqlSelStmtSb.Append(", NVL(so.pct_of_sale1, 0) AS hdr_slsp1_pct, NVL(so.pct_of_sale2, 0) AS hdr_slsp2_pct ")
            End If
            If req.billToCustName Then
                sqlSelStmtSb.Append(", NVL(corp_name, c.lname ||', ' || c.fname) AS billTo_name ")
            End If
            If req.delSetupChgs Then
                sqlSelStmtSb.Append(", NVL(del_chg, 0) AS del_chg, NVL(setup_chg, 0) AS setup_chg ")
            End If
            If req.taxChgs Then
                sqlSelStmtSb.Append(", NVL(tax_chg, 0) AS tax_chg ")
            End If
            If req.finInfo Then
                sqlSelStmtSb.Append(", NVL(orig_fi_amt, 0) AS fi_amt, fin_cust_cd, approval_cd ")
            End If
            sqlStmtSb.Append(" FROM store str, cust c, ")

            If req.orderQueryRequestFrom <> "0" AndAlso req.orderQueryRequestTo <> "0" Then
                sqlStmtSb.Append(" so_conf_stat ST1, ")
            End If
            If req.slspInit1.isNotEmpty Then
                sqlStmtSb.Append("emp es1, ")
            End If
            If req.slspInit2.isNotEmpty Then
                sqlStmtSb.Append("emp es2, ")
            End If
            ' Daniela force use index on so June 18
            ' Daniela limit records retrieved June 18
            'sqlStmtSb.Append("so WHERE ord_tp_cd NOT IN('SER','RES','EEX') AND c.cust_cd = so.cust_cd AND so.so_store_cd = str.store_cd ")
            sqlStmtSb.Append("so WHERE rownum <= 100 and so.del_doc_num > '0' and ord_tp_cd NOT IN ('SER','RES','EEX') AND c.cust_cd = so.cust_cd AND so.so_store_cd = str.store_cd ")

            'lucy start add 
            Dim str_co_grp_cd As String
            Dim str_co_cd As String
            Dim str_sup_user_flag As String

            'str_co_grp_cd = HttpContext.Current.Session("str_co_grp_cd")
            '   str_co_cd = HttpContext.Current.Session("CO_CD")
            'str_sup_user_flag = HttpContext.Current.Session("str_sup_user_flag")

            'If HttpContext.Current.Session("str_sup_user_flag") <> "Y" Then  ' not super user
            '    If str_co_grp_cd = "BRK" Then   '  the Brick 
            '        'July 27 performance change
            '        'sqlStmtSb.Append(" AND so.so_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd= :str_co_grp_cd))  ")
            '        sqlStmtSb.Append(" and exists (select 1 from CO_GRP WHERE CO_GRP_CD= :str_co_grp_cd and str.CO_CD = CO_GRP.co_cd) ")
            '        cmd.Parameters.Add(":str_co_grp_cd", OracleType.VarChar)
            '        cmd.Parameters(":str_co_grp_cd").Value = UCase(str_co_grp_cd)

            '    Else    'not the Brick
            '        'sqlStmtSb.Append(" AND so.so_store_cd in(select store_cd from store where co_cd = :str_co_cd)  ")
            '        sqlStmtSb.Append(" and str.co_cd = :str_co_cd ")
            '        cmd.Parameters.Add(":str_co_cd", OracleType.VarChar)
            '        cmd.Parameters(":str_co_cd").Value = UCase(str_co_cd)
            '    End If
            'End If
            ' end lucy add

            If req.orderQueryRequestFrom.Trim() <> "0" AndAlso req.orderQueryRequestTo.Trim() <> "0" Then
                sqlStmtSb.Append(" AND ST1.CONF_STAT_CD = SO.CONF_STAT_CD  ")
            ElseIf req.orderQueryRequestFrom.Trim() <> "0" Or req.orderQueryRequestTo.Trim() <> "0" Then
                sqlStmtSb.Append(" AND SO.CONF_STAT_CD = :CONFSTATUS ")
                cmd.Parameters.Add(":CONFSTATUS", OracleType.VarChar)
                cmd.Parameters(":CONFSTATUS").Value = UCase(req.orderQueryRequestFrom)
            End If

            If req.orderUserField1.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.USR_FLD_1 = :USR_FLD_1 ")
                cmd.Parameters.Add(":USR_FLD_1", OracleType.VarChar)
                cmd.Parameters(":USR_FLD_1").Value = UCase(req.orderUserField1)
            End If
            If req.orderUserField2.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.USR_FLD_2 = :USR_FLD_2 ")
                cmd.Parameters.Add(":USR_FLD_2", OracleType.VarChar)
                cmd.Parameters(":USR_FLD_2").Value = UCase(req.orderUserField2)
            End If
            If req.orderUserField3.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.USR_FLD_3 = :USR_FLD_3 ")
                cmd.Parameters.Add(":USR_FLD_3", OracleType.VarChar)
                cmd.Parameters(":USR_FLD_3").Value = UCase(req.orderUserField3)
            End If
            If req.orderUserField4.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.USR_FLD_4 = :USR_FLD_4 ")
                cmd.Parameters.Add(":USR_FLD_4", OracleType.VarChar)
                cmd.Parameters(":USR_FLD_4").Value = UCase(req.orderUserField4)
            End If
            If req.orderUserField5.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.USR_FLD_5 = :USR_FLD_5 ")
                cmd.Parameters.Add(":USR_FLD_5", OracleType.VarChar)
                cmd.Parameters(":USR_FLD_5").Value = UCase(req.orderUserField5)
            End If
            If req.orderZone.Trim().isNotEmpty Then
                sqlStmtSb.Append(" AND  SO.SHIP_TO_ZONE_CD = :SHIP_TO_ZONE_CD ")
                cmd.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
                cmd.Parameters(":SHIP_TO_ZONE_CD").Value = UCase(req.orderZone)
            End If

            If req.slspInit1.isNotEmpty Then
                sqlStmtSb.Append("AND es1.emp_cd (+) = so.so_emp_slsp_cd1 ")
            End If
            If req.slspInit2.isNotEmpty Then
                sqlStmtSb.Append("AND es2.emp_cd (+) = so.so_emp_slsp_cd2 ")
            End If
            If Not String.IsNullOrEmpty(req.confStatusCd) Then
                sqlStmtSb.Append(" AND SO.CONF_STAT_CD = '" & req.confStatusCd & "' ")
            End If

            If req.docNum.isNotEmpty Then
                sqlStmtSb.Append("AND DEL_DOC_NUM ").Append(equalOrLike(req.docNum)).Append(" :docNum ")
                cmd.Parameters.Add(":docNum", OracleType.VarChar)
                cmd.Parameters(":docNum").Value = UCase(req.docNum)
            End If
            If req.wrDt.isNotEmpty AndAlso IsDate(req.wrDt) Then
                sqlStmtSb.Append("AND SO_WR_DT ").Append(equalOrLike(req.wrDt)).Append(" :wrDt ")
                cmd.Parameters.Add(":wrDt", OracleType.DateTime)
                cmd.Parameters(":wrDt").Value = UCase(req.wrDt)
            End If
            If req.ordTp.isNotEmpty Then
                sqlStmtSb.Append("AND ORD_TP_CD ").Append(equalOrLike(req.ordTp)).Append(" :ordTp ")
                cmd.Parameters.Add(":ordTp", OracleType.VarChar)
                cmd.Parameters(":ordTp").Value = UCase(req.ordTp)
            End If
            If req.custCd.isNotEmpty Then
                sqlStmtSb.Append("AND so.CUST_CD ").Append(equalOrLike(req.custCd)).Append(" :custCd ")
                cmd.Parameters.Add(":custCd", OracleType.VarChar)
                cmd.Parameters(":custCd").Value = UCase(req.custCd)
            End If
            If req.custTpCd.isNotEmpty Then
                sqlStmtSb.Append("AND c.CUST_TP_CD ").Append(equalOrLike(req.custTpCd)).Append(" :custTpCd ")
                cmd.Parameters.Add(":custTpCd", OracleType.VarChar)
                cmd.Parameters(":custTpCd").Value = UCase(req.custTpCd)
            End If
            If req.statCd.isNotEmpty Then
                sqlStmtSb.Append("AND STAT_CD ").Append(equalOrLike(req.statCd)).Append(" :statCd ")
                cmd.Parameters.Add(":statCd", OracleType.VarChar)
                cmd.Parameters(":statCd").Value = UCase(req.statCd)
            End If
            If req.shipFName.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_F_NAME ").Append(equalOrLike(req.shipFName)).Append(" :shipFName ")
                cmd.Parameters.Add(":shipFName", OracleType.VarChar)
                cmd.Parameters(":shipFName").Value = UCase(req.shipFName)
            End If
            If req.shipLName.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_L_NAME ").Append(equalOrLike(req.shipLName)).Append(" :shipLName ")
                cmd.Parameters.Add(":shipLName", OracleType.VarChar)
                cmd.Parameters(":shipLName").Value = UCase(req.shipLName)
            End If
            If req.shipHPhone.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_H_PHONE ").Append(equalOrLikeForPhone(req.shipHPhone)).Append(" :shipHPhone ")
                cmd.Parameters.Add(":shipHPhone", OracleType.VarChar)
                cmd.Parameters(":shipHPhone").Value = StringUtils.AppendWildChar(req.shipHPhone)
            End If
            If req.shipBPhone.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_B_PHONE ").Append(equalOrLikeForPhone(req.shipBPhone)).Append(" :shipBPhone ")
                cmd.Parameters.Add(":shipBPhone", OracleType.VarChar)
                cmd.Parameters(":shipBPhone").Value = StringUtils.AppendWildChar(req.shipBPhone)
            End If
            If req.shipAddr1.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_ADDR1 ").Append(equalOrLike(req.shipAddr1)).Append(" :shipAddr1 ")
                cmd.Parameters.Add(":shipAddr1", OracleType.VarChar)
                cmd.Parameters(":shipAddr1").Value = UCase(req.shipAddr1)
            End If
            If req.shipAddr2.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_ADDR2 ").Append(equalOrLike(req.shipAddr2)).Append(" :shipAddr2 ")
                cmd.Parameters.Add(":shipAddr2", OracleType.VarChar)
                cmd.Parameters(":shipAddr2").Value = UCase(req.shipAddr2)  '
            End If
            If req.shipcity.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_CITY ").Append(equalOrLike(req.shipcity)).Append(" :shipcity ")
                cmd.Parameters.Add(":shipcity", OracleType.VarChar)
                cmd.Parameters(":shipcity").Value = UCase(req.shipcity)
            End If
            If req.shipState.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_ST_CD ").Append(equalOrLike(req.shipState)).Append(" :shipState ")
                cmd.Parameters.Add(":shipState", OracleType.VarChar)
                cmd.Parameters(":shipState").Value = UCase(req.shipState)
            End If
            If req.shipZip.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_ZIP_CD ").Append(equalOrLike(req.shipZip)).Append(" :shipZip ")
                cmd.Parameters.Add(":shipZip", OracleType.VarChar)
                cmd.Parameters(":shipZip").Value = UCase(req.shipZip)
            End If
            If req.slspInit1.isNotEmpty Then
                sqlStmtSb.Append("AND es1.emp_init ").Append(equalOrLike(req.slspInit1)).Append(" :slsp1 ")
                cmd.Parameters.Add(":slsp1", OracleType.VarChar)
                cmd.Parameters(":slsp1").Value = UCase(req.slspInit1)
            End If
            If req.slspInit2.isNotEmpty Then
                sqlStmtSb.Append("AND es2.emp_init ").Append(equalOrLike(req.slspInit2)).Append(" :slsp2 ")
                cmd.Parameters.Add(":slsp2", OracleType.VarChar)
                cmd.Parameters(":slsp2").Value = UCase(req.slspInit2)
            End If
            If req.puDel.isNotEmpty Then
                sqlStmtSb.Append("AND pu_del ").Append(equalOrLike(req.puDel)).Append(" :puDel ")
                cmd.Parameters.Add(":puDel", OracleType.VarChar)
                cmd.Parameters(":puDel").Value = UCase(req.puDel)
            End If
            If req.poNumber.isNotEmpty Then
                sqlStmtSb.Append("AND SO.ORDER_PO_NUM ").Append(equalOrLike(req.poNumber)).Append(" :poNum ")
                cmd.Parameters.Add(":poNum", OracleType.VarChar)
                cmd.Parameters(":poNum").Value = UCase(req.poNumber)
            End If
            If req.emailAddr.isNotEmpty Then
                sqlStmtSb.Append("AND C.EMAIL_ADDR ").Append(equalOrLike(req.emailAddr)).Append(" :emailAddr ")
                cmd.Parameters.Add(":emailAddr", OracleType.VarChar)
                cmd.Parameters(":emailAddr").Value = UCase(req.emailAddr)
            End If

            If req.queryOrderDateRange = True Then
                If req.puDelDt.isNotEmpty AndAlso IsDate(req.puDelDt) AndAlso req.orderDateTo.Trim.isNotEmpty AndAlso IsDate(req.orderDateTo.Trim) Then
                    sqlStmtSb.Append(" AND pu_del_DT BETWEEN :puDelDtFROM AND :puDelDtTO ") '.Append(equalOrLike(req.puDelDt)).Append(" :puDelDt ")
                    cmd.Parameters.Add(":puDelDtFROM", OracleType.DateTime)
                    cmd.Parameters(":puDelDtFROM").Value = UCase(req.puDelDt) + " 00:00:00" ' begining of date
                    cmd.Parameters.Add(":puDelDtTO", OracleType.DateTime)
                    cmd.Parameters(":puDelDtTO").Value = UCase(req.orderDateTo) + " 23:59:59" ' end of date
                End If
            Else
                If req.puDelDt.isNotEmpty AndAlso IsDate(req.puDelDt) Then
                    sqlStmtSb.Append("AND pu_del_DT ").Append(equalOrLike(req.puDelDt)).Append(" :puDelDt ")
                    cmd.Parameters.Add(":puDelDt", OracleType.DateTime)
                    cmd.Parameters(":puDelDt").Value = UCase(req.puDelDt)
                End If
            End If
            If req.soStore.isNotEmpty Then
                sqlStmtSb.Append("AND so_store_cd ").Append(equalOrLike(req.soStore)).Append(" :soStore ")
                cmd.Parameters.Add(":soStore", OracleType.VarChar)
                cmd.Parameters(":soStore").Value = UCase(req.soStore)
            End If
            If req.corpName.isNotEmpty Then
                sqlStmtSb.Append("AND SHIP_TO_CORP ").Append(equalOrLike(req.corpName)).Append(" :corpName ")
                cmd.Parameters.Add(":corpName", OracleType.VarChar)
                cmd.Parameters(":corpName").Value = UCase(req.corpName)
            End If
            ' if set to "ALL" (reserved init in E1), then query all
            If req.allForSlspCd.isNotEmpty AndAlso Not ("ALL".Equals(req.allForSlspCd)) Then

                sqlStmtSb.Append("AND ((so_emp_slsp_cd1 = :slspCd1 OR so_emp_slsp_cd2 = :slspCd1 ) ").Append(
                    "OR 1 = (SELECT 1 FROM so_ln sl1, so so1 WHERE so1.del_doc_num = so.del_doc_num ").Append(
                    "AND so1.del_doc_num = sl1.del_doc_num AND rownum < 2 ").Append(
                    "AND (sl1.so_emp_slsp_cd1 = :slspCd1 OR sl1.so_emp_slsp_cd2 = :slspCd1))) ")

                cmd.Parameters.Add(":slspCd1", OracleType.VarChar)
                cmd.Parameters(":slspCd1").Value = UCase(req.allForSlspCd)
            End If

            'Daniela improve performance
            'If Not SystemUtils.hasAllStoreAccess(req.empCdOp) Then
            '    sqlStmtSb.Append("AND SO_STORE_CD IN (select store_grp$store.store_cd from store_grp$store, emp2store_grp ").Append(
            '        "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd and emp2store_grp.emp_cd = :empCdOp )")
            '    cmd.Parameters.Add(":empCdOp", OracleType.VarChar)
            '    cmd.Parameters(":empCdOp").Value = UCase(req.empCdOp)
            'End If
            'Daniela improve performance
            If Not SystemUtils.hasAllStoreAccess(req.empCdOp) Then
                sqlStmtSb.Append("AND exists (select 1 from STORE_GRP$STORE, EMP2STORE_GRP ").Append(
                  "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd and emp2store_grp.emp_cd = :empCdOp and SO_STORE_CD = STORE_GRP$STORE.STORE_CD) ")
                cmd.Parameters.Add(":empCdOp", OracleType.VarChar)
                cmd.Parameters(":empCdOp").Value = UCase(req.empCdOp)
            End If

            If req.orderQueryRequestFrom <> "0" AndAlso req.orderQueryRequestTo <> "0" Then
                sqlStmtSb.Append(" AND SEQ# between (SELECT Seq# FROM SO_CONF_STAT WHERE CONF_STAT_CD =upper(:rangefrom))  and  (SELECT Seq# FROM SO_CONF_STAT WHERE CONF_STAT_CD =upper(:rangeto))  ")
                cmd.Parameters.Add(":rangefrom", OracleType.VarChar)
                cmd.Parameters(":rangefrom").Value = UCase(req.orderQueryRequestFrom)
                cmd.Parameters.Add(":rangeto", OracleType.VarChar)
                cmd.Parameters(":rangeto").Value = UCase(req.orderQueryRequestTo)
            End If

            ' MCCL DB Nov 27, 2017

            Dim userType As Double = -1

            If (isNotEmpty(HttpContext.Current.Session("USER_TYPE"))) Then
                userType = HttpContext.Current.Session("USER_TYPE")
            End If
            Dim coCd = HttpContext.Current.Session("CO_CD")

            If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
                sqlStmtSb.Append(" AND SO_STORE_CD in (select s.store_cd from store s, MCCL_STORE_GROUP a, store_grp$store b ")
                sqlStmtSb.Append("where SO_STORE_CD = s.store_cd and a.store_grp_cd = b.store_grp_cd and ")
                sqlStmtSb.Append("s.store_cd = b.store_cd and a.user_id = :userType ")

                ' Leons Franchise exception
                If isNotEmpty(userType) And userType = 5 Then
                    sqlStmtSb.Append(" and '" & coCd & "' = s.co_cd ")
                End If
                sqlStmtSb.Append(" ) ")
                cmd.Parameters.Add(":userType", OracleType.VarChar)
                cmd.Parameters(":userType").Value = userType
            End If

            sqlStmtSb.Append(" ORDER BY UPPER(SHIP_TO_L_NAME), UPPER(SHIP_TO_F_NAME) ")

            soDatSet = New DataSet
            Try
                cmd.CommandText = UCase(sqlSelStmtSb.ToString + sqlStmtSb.ToString)

                Dim oAdp As OracleDataAdapter
                Using conn As OracleConnection = GetConnection()
                    cmd.Connection = conn
                    oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
                    oAdp.Fill(soDatSet)
                End Using
            Finally
                cmd.Dispose()
            End Try
        End If

        Return soDatSet
    End Function

    ''' <summary>
    ''' Alternate version of find orders, that looks only the header for salesperson match and 
    ''' hints at Oracle fto use certain indexes found to be needed to avoid full table scan 
    ''' of the SO, based on on observations by CB
    '''     condition off the ord_tp_cd NOT IN('SER','RES','EEX') when redundant
    '''     remove second SO from subquery and join to SO in main query (option 1)
    '''     re-order tables 
    '''     omit the subquery against the so_ln table (option 2)
    ''' 
    ''' 
    ''' </summary>
    ''' <param name="qoption">
    ''' Controls some technical aspects of the query
    ''' 1 (or 3): include the subquery on so_ln
    ''' 2 (or 3): hint the new indexes
    ''' 2 omits the subquery on so_ln 
    ''' 
    ''' options other than (2) are historical artefacts: this function is used by SBOB with option 2.
    ''' 
    ''' </param>
    ''' <param name="req"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' see JIRA MM-9935.  this version/option is based on the understanding that the salesperson(s) 
    ''' on the SO *header* are what matters for the SBOB use case, and the observation that querying to 
    ''' the so lines for additional salesperson(s) is costly.
    '''  </remarks>
    Public Function LookupOrders2(ByVal req As OrderRequestDtc, qoption As Integer) As DataSet

        Dim soDatSet As DataSet

        If IsNothing(req) OrElse (isEmpty(req.docNum) AndAlso isEmpty(req.wrDt) AndAlso isEmpty(req.ordTp) AndAlso
                                  isEmpty(req.custCd) AndAlso isEmpty(req.statCd) AndAlso isEmpty(req.shipFName) AndAlso
                                  isEmpty(req.shipLName) AndAlso isEmpty(req.shipHPhone) AndAlso isEmpty(req.shipBPhone) AndAlso
                                  isEmpty(req.shipAddr1) AndAlso isEmpty(req.shipAddr2) AndAlso isEmpty(req.shipcity) AndAlso
                                  isEmpty(req.shipState) AndAlso isEmpty(req.shipZip) AndAlso
                                  isEmpty(req.slspInit1) AndAlso isEmpty(req.slspInit2) AndAlso isEmpty(req.puDel) AndAlso
                                  isEmpty(req.puDelDt) AndAlso isEmpty(req.corpName) AndAlso isEmpty(req.soStore)) AndAlso
                                  isEmpty(req.custTpCd) AndAlso isEmpty(req.allForSlspCd) AndAlso isEmpty(req.orderUserField1) AndAlso isEmpty(req.orderUserField2) AndAlso
                                  isEmpty(req.orderUserField3) AndAlso isEmpty(req.orderUserField4) AndAlso isEmpty(req.orderUserField5) AndAlso req.orderQueryRequestTo <> "0" AndAlso
                                  req.orderQueryRequestFrom <> "0" AndAlso isEmpty(req.orderZone) AndAlso
                                  isEmpty(req.custTpCd) AndAlso isEmpty(req.allForSlspCd) AndAlso isEmpty(req.confStatusCd) Then
            ' DO NOTHING - NO INPUT
            Return soDatSet
        End If

        Dim sqlSelStmtSb As New StringBuilder
        Dim sqlStmtSb As New StringBuilder
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand


        sqlSelStmtSb.Append("SELECT ")
        ' Daniela comment for performance
        'If qoption = 2 OrElse qoption = 3 Then
        '    sqlSelStmtSb.Append("/*+ ordered index(so so_nu16 so so_nu17) */ ")
        'End If
        If (req.queryPaidInFull) Then
            sqlSelStmtSb.Append("CASE WHEN (SELECT value FROM gp_parms WHERE key = 'PTS_POS+' AND parm = 'PAID_IN_FULL_CALC') <>  'CUSTOM' ")
            sqlSelStmtSb.Append("THEN bt_sales_util.is_paid_in_full(so.del_doc_num, so.cust_cd, so.so_store_cd, so.ord_tp_cd, so.orig_fi_amt, so.fin_cust_cd, so.approval_cd) ")
            sqlSelStmtSb.Append("ELSE custom_paid.is_order_paid_in_full(so.ord_tp_cd, so.del_doc_num, so.cust_cd) ")
            sqlSelStmtSb.Append("END PaidInFull, ")
        End If

        sqlSelStmtSb.Append("del_doc_num,  NVL(so.conf_stat_cd,'-') confirmed, so_wr_dt, stat_cd, ord_tp_cd, so_store_cd, so_seq_num, so.cust_cd, str.co_cd ")

        If req.shipToName Then
            sqlSelStmtSb.Append(", NVL(ship_to_corp, ship_to_l_name ||', ' || ship_to_f_name) AS full_name ")
        End If
        If req.shipToHPhone Then
            sqlSelStmtSb.Append(", ship_to_h_phone ")
        End If
        If req.shipToZone Then
            sqlSelStmtSb.Append(", ship_to_zone_cd, ship_to_zip_cd ")
        End If
        If req.puDelInfo Then
            sqlSelStmtSb.Append(", pu_del_dt, so.pu_del, so.pu_del_store_cd ")
        End If
        If req.slspCds Then
            sqlSelStmtSb.Append(", so.so_emp_slsp_cd1 AS hdr_slsp_cd1, so.so_emp_slsp_cd2 AS hdr_slsp_cd2 ")
        End If
        If req.slspPcts Then
            sqlSelStmtSb.Append(", NVL(so.pct_of_sale1, 0) AS hdr_slsp1_pct, NVL(so.pct_of_sale2, 0) AS hdr_slsp2_pct ")
        End If
        If req.billToCustName Then
            sqlSelStmtSb.Append(", NVL(corp_name, c.lname ||', ' || c.fname) AS billTo_name ")
        End If
        If req.delSetupChgs Then
            sqlSelStmtSb.Append(", NVL(del_chg, 0) AS del_chg, NVL(setup_chg, 0) AS setup_chg ")
        End If
        If req.taxChgs Then
            sqlSelStmtSb.Append(", NVL(tax_chg, 0) AS tax_chg ")
        End If
        If req.finInfo Then
            sqlSelStmtSb.Append(", NVL(orig_fi_amt, 0) AS fi_amt, fin_cust_cd, approval_cd ")
        End If
        sqlStmtSb.Append("FROM SO, cust c, store str")

        If req.orderQueryRequestFrom <> "0" AndAlso req.orderQueryRequestTo <> "0" Then
            sqlStmtSb.Append(", so_conf_stat ST1")
        End If
        If req.slspInit1.isNotEmpty Then
            sqlStmtSb.Append(", emp es1")
        End If
        If req.slspInit2.isNotEmpty Then
            sqlStmtSb.Append(", emp es2")
        End If
        'sqlStmtSb.Append(" WHERE c.cust_cd = so.cust_cd AND so.so_store_cd = str.store_cd ")
        'Daniela improve performance
        sqlStmtSb.Append(" WHERE rownum <= 200 and so.del_doc_num > '0' and c.cust_cd = so.cust_cd AND so.so_store_cd = str.store_cd ")

        If req.ordTp.isEmpty Then
            ' otherwise this is redundant see below
            sqlStmtSb.Append(" AND ord_tp_cd NOT IN ('SER','RES','EEX') ")
        End If

        If req.orderQueryRequestFrom.Trim() <> "0" AndAlso req.orderQueryRequestTo.Trim() <> "0" Then
            sqlStmtSb.Append(" AND ST1.CONF_STAT_CD = SO.CONF_STAT_CD  ")
        ElseIf req.orderQueryRequestFrom.Trim() <> "0" Or req.orderQueryRequestTo.Trim() <> "0" Then
            sqlStmtSb.Append(" AND SO.CONF_STAT_CD = :CONFSTATUS ")
            cmd.Parameters.Add(":CONFSTATUS", OracleType.VarChar)
            cmd.Parameters(":CONFSTATUS").Value = UCase(req.orderQueryRequestFrom)
        End If

        If req.orderUserField1.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.USR_FLD_1 = :USR_FLD_1 ")
            cmd.Parameters.Add(":USR_FLD_1", OracleType.VarChar)
            cmd.Parameters(":USR_FLD_1").Value = UCase(req.orderUserField1)
        End If
        If req.orderUserField2.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.USR_FLD_2 = :USR_FLD_2 ")
            cmd.Parameters.Add(":USR_FLD_2", OracleType.VarChar)
            cmd.Parameters(":USR_FLD_2").Value = UCase(req.orderUserField2)
        End If
        If req.orderUserField3.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.USR_FLD_3 = :USR_FLD_3 ")
            cmd.Parameters.Add(":USR_FLD_3", OracleType.VarChar)
            cmd.Parameters(":USR_FLD_3").Value = UCase(req.orderUserField3)
        End If
        If req.orderUserField4.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.USR_FLD_4 = :USR_FLD_4 ")
            cmd.Parameters.Add(":USR_FLD_4", OracleType.VarChar)
            cmd.Parameters(":USR_FLD_4").Value = UCase(req.orderUserField4)
        End If
        If req.orderUserField5.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.USR_FLD_5 = :USR_FLD_5 ")
            cmd.Parameters.Add(":USR_FLD_5", OracleType.VarChar)
            cmd.Parameters(":USR_FLD_5").Value = UCase(req.orderUserField5)
        End If
        If req.orderZone.Trim().isNotEmpty Then
            sqlStmtSb.Append(" AND  SO.SHIP_TO_ZONE_CD = :SHIP_TO_ZONE_CD ")
            cmd.Parameters.Add(":SHIP_TO_ZONE_CD", OracleType.VarChar)
            cmd.Parameters(":SHIP_TO_ZONE_CD").Value = UCase(req.orderZone)
        End If

        If req.slspInit1.isNotEmpty Then
            sqlStmtSb.Append("AND es1.emp_cd (+) = so.so_emp_slsp_cd1 ")
        End If
        If req.slspInit2.isNotEmpty Then
            sqlStmtSb.Append("AND es2.emp_cd (+) = so.so_emp_slsp_cd2 ")
        End If
        If Not String.IsNullOrEmpty(req.confStatusCd) Then
            sqlStmtSb.Append(" AND SO.CONF_STAT_CD = '" & req.confStatusCd & "' ")
        End If

        If req.docNum.isNotEmpty Then
            sqlStmtSb.Append("AND DEL_DOC_NUM ").Append(equalOrLike(req.docNum)).Append(" :docNum ")
            cmd.Parameters.Add(":docNum", OracleType.VarChar)
            cmd.Parameters(":docNum").Value = UCase(req.docNum)
        End If
        If req.wrDt.isNotEmpty AndAlso IsDate(req.wrDt) Then
            sqlStmtSb.Append("AND SO_WR_DT ").Append(equalOrLike(req.wrDt)).Append(" :wrDt ")
            cmd.Parameters.Add(":wrDt", OracleType.DateTime)
            cmd.Parameters(":wrDt").Value = UCase(req.wrDt)
        End If
        If req.ordTp.isNotEmpty Then
            sqlStmtSb.Append("AND ORD_TP_CD ").Append(equalOrLike(req.ordTp)).Append(" :ordTp ")
            cmd.Parameters.Add(":ordTp", OracleType.VarChar)
            cmd.Parameters(":ordTp").Value = UCase(req.ordTp)
        End If
        If req.custCd.isNotEmpty Then
            sqlStmtSb.Append("AND so.CUST_CD ").Append(equalOrLike(req.custCd)).Append(" :custCd ")
            cmd.Parameters.Add(":custCd", OracleType.VarChar)
            cmd.Parameters(":custCd").Value = UCase(req.custCd)
        End If
        If req.custTpCd.isNotEmpty Then
            sqlStmtSb.Append("AND c.CUST_TP_CD ").Append(equalOrLike(req.custTpCd)).Append(" :custTpCd ")
            cmd.Parameters.Add(":custTpCd", OracleType.VarChar)
            cmd.Parameters(":custTpCd").Value = UCase(req.custTpCd)
        End If
        If req.statCd.isNotEmpty Then
            sqlStmtSb.Append("AND STAT_CD ").Append(equalOrLike(req.statCd)).Append(" :statCd ")
            cmd.Parameters.Add(":statCd", OracleType.VarChar)
            cmd.Parameters(":statCd").Value = UCase(req.statCd)
        End If
        If req.shipFName.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_F_NAME ").Append(equalOrLike(req.shipFName)).Append(" :shipFName ")
            cmd.Parameters.Add(":shipFName", OracleType.VarChar)
            cmd.Parameters(":shipFName").Value = UCase(req.shipFName)
        End If
        If req.shipLName.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_L_NAME ").Append(equalOrLike(req.shipLName)).Append(" :shipLName ")
            cmd.Parameters.Add(":shipLName", OracleType.VarChar)
            cmd.Parameters(":shipLName").Value = UCase(req.shipLName)
        End If
        If req.shipHPhone.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_H_PHONE ").Append(equalOrLikeForPhone(req.shipHPhone)).Append(" :shipHPhone ")
            cmd.Parameters.Add(":shipHPhone", OracleType.VarChar)
            cmd.Parameters(":shipHPhone").Value = StringUtils.AppendWildChar(req.shipHPhone)
        End If
        If req.shipBPhone.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_B_PHONE ").Append(equalOrLikeForPhone(req.shipBPhone)).Append(" :shipBPhone ")
            cmd.Parameters.Add(":shipBPhone", OracleType.VarChar)
            cmd.Parameters(":shipBPhone").Value = StringUtils.AppendWildChar(req.shipBPhone)
        End If
        If req.shipAddr1.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_ADDR1 ").Append(equalOrLike(req.shipAddr1)).Append(" :shipAddr1 ")
            cmd.Parameters.Add(":shipAddr1", OracleType.VarChar)
            cmd.Parameters(":shipAddr1").Value = StringUtils.Query_String_Prepare(req.shipAddr1)
        End If
        If req.shipAddr2.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_ADDR2 ").Append(equalOrLike(req.shipAddr2)).Append(" :shipAddr2 ")
            cmd.Parameters.Add(":shipAddr2", OracleType.VarChar)
            cmd.Parameters(":shipAddr2").Value = UCase(req.shipAddr2)  '
        End If
        If req.shipcity.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_CITY ").Append(equalOrLike(req.shipcity)).Append(" :shipcity ")
            cmd.Parameters.Add(":shipcity", OracleType.VarChar)
            cmd.Parameters(":shipcity").Value = UCase(req.shipcity)
        End If
        If req.shipState.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_ST_CD ").Append(equalOrLike(req.shipState)).Append(" :shipState ")
            cmd.Parameters.Add(":shipState", OracleType.VarChar)
            cmd.Parameters(":shipState").Value = UCase(req.shipState)
        End If
        If req.shipZip.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_ZIP_CD ").Append(equalOrLike(req.shipZip)).Append(" :shipZip ")
            cmd.Parameters.Add(":shipZip", OracleType.VarChar)
            cmd.Parameters(":shipZip").Value = UCase(req.shipZip)
        End If
        If req.slspInit1.isNotEmpty Then
            sqlStmtSb.Append("AND es1.emp_init ").Append(equalOrLike(req.slspInit1)).Append(" :slsp1 ")
            cmd.Parameters.Add(":slsp1", OracleType.VarChar)
            cmd.Parameters(":slsp1").Value = UCase(req.slspInit1)
        End If
        If req.slspInit2.isNotEmpty Then
            sqlStmtSb.Append("AND es2.emp_init ").Append(equalOrLike(req.slspInit2)).Append(" :slsp2 ")
            cmd.Parameters.Add(":slsp2", OracleType.VarChar)
            cmd.Parameters(":slsp2").Value = UCase(req.slspInit2)
        End If
        If req.puDel.isNotEmpty Then
            sqlStmtSb.Append("AND pu_del ").Append(equalOrLike(req.puDel)).Append(" :puDel ")
            cmd.Parameters.Add(":puDel", OracleType.VarChar)
            cmd.Parameters(":puDel").Value = UCase(req.puDel)
        End If
        If req.queryOrderDateRange = True Then
            If req.puDelDt.isNotEmpty AndAlso IsDate(req.puDelDt) AndAlso req.orderDateTo.Trim.isNotEmpty AndAlso IsDate(req.orderDateTo.Trim) Then
                sqlStmtSb.Append(" AND pu_del_DT BETWEEN :puDelDtFROM AND :puDelDtTO ") '.Append(equalOrLike(req.puDelDt)).Append(" :puDelDt ")
                cmd.Parameters.Add(":puDelDtFROM", OracleType.DateTime)
                cmd.Parameters(":puDelDtFROM").Value = UCase(req.puDelDt) + " 00:00:00" ' begining of date
                cmd.Parameters.Add(":puDelDtTO", OracleType.DateTime)
                cmd.Parameters(":puDelDtTO").Value = UCase(req.orderDateTo) + " 23:59:59" ' end of date
            End If
        Else
            If req.puDelDt.isNotEmpty AndAlso IsDate(req.puDelDt) Then
                sqlStmtSb.Append("AND pu_del_DT ").Append(equalOrLike(req.puDelDt)).Append(" :puDelDt ")
                cmd.Parameters.Add(":puDelDt", OracleType.DateTime)
                cmd.Parameters(":puDelDt").Value = UCase(req.puDelDt)
            End If
        End If
        If req.soStore.isNotEmpty Then
            sqlStmtSb.Append("AND so_store_cd ").Append(equalOrLike(req.soStore)).Append(" :soStore ")
            cmd.Parameters.Add(":soStore", OracleType.VarChar)
            cmd.Parameters(":soStore").Value = UCase(req.soStore)
        End If
        If req.corpName.isNotEmpty Then
            sqlStmtSb.Append("AND SHIP_TO_CORP ").Append(equalOrLike(req.corpName)).Append(" :corpName ")
            cmd.Parameters.Add(":corpName", OracleType.VarChar)
            cmd.Parameters(":corpName").Value = UCase(req.corpName)
        End If
        ' if set to "ALL" (reserved init in E1), then query all
        If req.allForSlspCd.isNotEmpty AndAlso Not ("ALL".Equals(req.allForSlspCd)) Then
            'IT Request 2580
            sqlStmtSb.Append("AND (so_emp_slsp_cd1 like :slspCd1 OR so_emp_slsp_cd2 like :slspCd1 ) ")
            If qoption = 1 OrElse qoption = 3 Then
                sqlStmtSb.Append("AND ((so_emp_slsp_cd1 = :slspCd1 OR so_emp_slsp_cd2 = :slspCd1 ) ")
                ' mm-9936
                sqlStmtSb.Append(
                    "OR 1 = (SELECT 1 FROM so_ln sl1 ").Append(
                    "WHERE so.del_doc_num = sl1.del_doc_num AND rownum < 2 ").Append(
                    "AND (sl1.so_emp_slsp_cd1 like :slspCd1 OR sl1.so_emp_slsp_cd2 like :slspCd1))) ")
            Else
                sqlStmtSb.Append("AND (so_emp_slsp_cd1 like :slspCd1 OR so_emp_slsp_cd2 like :slspCd1 ) ")
            End If
            cmd.Parameters.Add(":slspCd1", OracleType.VarChar)
            cmd.Parameters(":slspCd1").Value = UCase(req.allForSlspCd)
        End If

        'If Not SystemUtils.hasAllStoreAccess(req.empCdOp) Then
        '    sqlStmtSb.Append("AND SO_STORE_CD IN (select store_grp$store.store_cd from store_grp$store, emp2store_grp ").Append(
        '        "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd and emp2store_grp.emp_cd = :empCdOp )")
        '    cmd.Parameters.Add(":empCdOp", OracleType.VarChar)
        '    cmd.Parameters(":empCdOp").Value = UCase(req.empCdOp)
        'End If
        'Daniela improve performance
        If Not SystemUtils.hasAllStoreAccess(req.empCdOp) Then
            sqlStmtSb.Append("AND exists (select 1 from STORE_GRP$STORE, EMP2STORE_GRP ").Append(
              "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd and emp2store_grp.emp_cd = :empCdOp and SO_STORE_CD = STORE_GRP$STORE.STORE_CD) ")
            cmd.Parameters.Add(":empCdOp", OracleType.VarChar)
            cmd.Parameters(":empCdOp").Value = UCase(req.empCdOp)
        End If

        If req.orderQueryRequestFrom <> "0" AndAlso req.orderQueryRequestTo <> "0" Then
            sqlStmtSb.Append(" AND SEQ# between (SELECT Seq# FROM SO_CONF_STAT WHERE CONF_STAT_CD =upper(:rangefrom))  and  (SELECT Seq# FROM SO_CONF_STAT WHERE CONF_STAT_CD =upper(:rangeto))  ")
            cmd.Parameters.Add(":rangefrom", OracleType.VarChar)
            cmd.Parameters(":rangefrom").Value = UCase(req.orderQueryRequestFrom)
            cmd.Parameters.Add(":rangeto", OracleType.VarChar)
            cmd.Parameters(":rangeto").Value = UCase(req.orderQueryRequestTo)
        End If
        sqlStmtSb.Append(" ORDER BY UPPER(SHIP_TO_L_NAME), UPPER(SHIP_TO_F_NAME) ")

        soDatSet = New DataSet
        Try
            cmd.CommandText = UCase(sqlSelStmtSb.ToString + sqlStmtSb.ToString)

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
                oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                cmd.Connection = conn
                oAdp.Fill(soDatSet)
            End Using
        Finally
            cmd.Dispose()
        End Try

        Return soDatSet
    End Function

    ''' <summary>
    ''' update line leval tax
    ''' </summary>
    ''' <param name="cmdUpdateItems"></param>
    ''' <param name="retPrc"></param>
    ''' <param name="rowid"></param>
    ''' <remarks></remarks>
    Public Function UpdateLnTaxAmt(ByRef cmdUpdateItems As OracleCommand, ByVal retPrc As Double, ByVal rowid As Integer) As Integer
        Dim modifideRows As Integer
        Try
            With cmdUpdateItems
                .CommandText = "UPDATE temp_itm set taxable_amt=:retPrc, TAX_AMT=ROUND((:retPrc *(NVL(tax_pct,0)/100))," & TaxUtils.Tax_Constants.taxPrecision & ") WHERE tax_pct > 0 and row_id = :idNum"
                .Parameters.Clear()
                .Parameters.Add(":retPrc", OracleType.Double)
                .Parameters.Add(":idNum", OracleType.Number)
                .Parameters(":retPrc").Value = retPrc
                .Parameters(":idNum").Value = rowid
                modifideRows = cmdUpdateItems.ExecuteNonQuery()
            End With
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return modifideRows
    End Function

    ''' <summary>
    ''' Update the warranty details for the sales order creation section
    ''' </summary>
    ''' <param name="sessionID">Session ID</param>
    ''' <param name="rowid">New row id of the temp table </param>
    ''' <param name="voidedLines">Total number of lined voided</param>
    ''' <param name="firstRow ">First row sequence of the order</param>
    ''' <remarks>This procedure updates the warr related column of the temp_itm table for rows imported for referenced non sale docs</remarks>

    Public Function UpdateWarrDetailsForNonSale(ByRef cmdUpdate As OracleCommand, ByVal sessionID As String, ByVal rowid As Integer, ByVal voidedLines As Integer, ByVal orgiDocNumber As String, ByVal orgiDocLineNumber As String, ByVal firstRow As Integer)
        Dim sql As StringBuilder = New StringBuilder
        sql.Append(" select ln1.itm_cd Warr,ln.itm_cd Item,WARR_DEL_DOC_NUM,WARR_DEL_DOC_LN# ln from SO_LN2WARR w ")
        sql.Append(" join so_ln ln on w.del_doc_num = ln.del_doc_num and w.DEL_DOC_LN# = ln.DEL_DOC_LN#  ")
        sql.Append(" Join so_ln ln1 on ln1.DEL_DOC_LN# =  w.WARR_DEL_DOC_LN# and ln1.del_doc_num = ln.del_doc_num ")
        sql.Append(" where w.del_doc_num = :DELDOCNUMBER")
        sql.Append(" AND w.DEL_DOC_LN# = :DEL_DOC_LINE_NUMBER")
        Dim dbReader As OracleDataReader
        Try
            cmdUpdate.Parameters.Clear()
            cmdUpdate.Parameters.Add(":DELDOCNUMBER", OracleType.VarChar)
            cmdUpdate.Parameters(":DELDOCNUMBER").Value = UCase(orgiDocNumber)
            cmdUpdate.Parameters.Add(":DEL_DOC_LINE_NUMBER", OracleType.VarChar)
            cmdUpdate.Parameters(":DEL_DOC_LINE_NUMBER").Value = UCase(orgiDocLineNumber)
            cmdUpdate.CommandText = sql.ToString()
            dbReader = DisposablesManager.BuildOracleDataReader(cmdUpdate)
            cmdUpdate.Parameters.Clear()
            If dbReader.HasRows Then
                dbReader.Read()
                sql.Clear()
                sql.Append("UPDATE TEMP_ITM SET warr_itm_link = :itmCd, warr_row_link = :warrRowNum WHERE row_id = :selSkuRowNum")
                cmdUpdate.CommandText = sql.ToString()
                cmdUpdate.Parameters.Add(":itmCd", OracleType.VarChar)
                cmdUpdate.Parameters(":itmCd").Value = UCase(dbReader.Item("Warr"))
                cmdUpdate.Parameters.Add(":warrRowNum", OracleType.Int32)
                'this is to update the proper reference number, in the end - 1 indicate, if the line number is 2, then actual line shuld be -1 of it.
                cmdUpdate.Parameters(":warrRowNum").Value = firstRow + dbReader.Item("ln") - voidedLines - 1
                cmdUpdate.Parameters.Add(":selSkuRowNum", OracleType.Int32)
                cmdUpdate.Parameters(":selSkuRowNum").Value = UCase(rowid)
                cmdUpdate.ExecuteNonQuery()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            cmdUpdate.Parameters.Clear()
            sql.Clear()
        End Try

    End Function
    Public Sub UpdateWarrLinkForSplitSales(ByRef cmdUpdate As OracleCommand, ByVal sessionID As String, ByVal orgiDocNumber As String, ByVal firstRow As Integer)
        Dim sql As StringBuilder = New StringBuilder

        firstRow = firstRow - 1

        sql.Append(" declare ")
        sql.Append(" cursor warrCursor is ")
        sql.Append(" with CTE as ( ")
        sql.Append(" select :firstRow + rownum As updated_row_Num, ln.DEL_DOC_NUM, ln.DEL_DOC_LN# + :firstRow as old_warr_row_link ")
        sql.Append(" from so_ln ln where del_doc_num = :del_doc_num) select updated_Row_Num,old_warr_row_link from CTE ")
        sql.Append(" where updated_Row_Num <> old_warr_row_link;")
        sql.Append(" updated_row_num integer;")
        sql.Append(" old_warr_row_link integer;")
        sql.Append(" begin ")
        sql.Append(" OPEN warrCursor; Loop ")
        sql.Append(" FETCH warrCursor INTO updated_Row_Num, old_warr_row_link;")
        sql.Append(" update temp_itm set WARR_ROW_LINK = updated_Row_Num where WARR_ROW_LINK = old_warr_row_link and session_id = :sessionId; ")
        sql.Append(" EXIT WHEN warrCursor%NOTFOUND; ")
        sql.Append(" End Loop;CLOSE warrCursor;End;")


        Try
            cmdUpdate.Parameters.Clear()
            cmdUpdate.Parameters.Add(":firstRow", OracleType.Int32)
            cmdUpdate.Parameters(":firstRow").Value = firstRow
            cmdUpdate.Parameters.Add(":del_doc_num", OracleType.VarChar)
            cmdUpdate.Parameters(":del_doc_num").Value = UCase(orgiDocNumber)
            cmdUpdate.Parameters.Add(":sessionId", OracleType.VarChar)
            cmdUpdate.Parameters(":sessionId").Value = sessionID
            cmdUpdate.CommandText = sql.ToString()
            cmdUpdate.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdUpdate.Parameters.Clear()
            sql.Clear()
        End Try

    End Sub
    ''' <summary>
    ''' SkuCheckForTakeWith
    ''' </summary>
    ''' <param name="sku"></param>
    ''' <param name="dbConnection"></param>
    ''' <param name="dbcommand"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SkuCheckForTakeWith(ByVal sku As String, ByRef dbcommand As OracleCommand) As Boolean
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder
        Dim TakeWith As Boolean = False
        Try
            sql.Append("SELECT TREATMENT_ITM_CD AS ITM_CD FROM INV_MNR_CAT WHERE TREATMENT_ITM_CD='").Append(sku).Append("' UNION SELECT WAR_ITM_CD AS ITM_CD FROM ITM2ITM_WARR WHERE WAR_ITM_CD='").Append(sku).Append("'")
            dbcommand.CommandText = sql.ToString
            dbReader = DisposablesManager.BuildOracleDataReader(dbcommand)

            If (dbReader.Read()) Then
                If Not IsNothing(dbReader.Item("ITM_CD").ToString) AndAlso Not String.IsNullOrEmpty(dbReader.Item("ITM_CD").ToString) Then
                    TakeWith = True
                End If
            End If
            sql.Clear()
            Return TakeWith
        Catch ex As Exception

        End Try
    End Function
    ''' <summary>
    ''' This method returns the package price based on the component reference
    ''' </summary>
    ''' <param name="PackageParentRow">Line Number of the component Parent Line</param>
    ''' <returns></returns>
    Public Function GetPackageDetailsByComponentForSOE(ByVal PackageParentRow As Integer) As PackageItem
        Dim ds As DataSet = New DataSet()
        Dim package As PackageItem = New PackageItem()
        Using oraConn As OracleConnection = New OracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString), objSql As OracleCommand = New OracleCommand("", oraConn)
            oraConn.Open()

            Dim sql = "Select row_id,itm_cd,ret_prc  from temp_Itm where row_id = " & PackageParentRow & " Order by row_id"
            objSql.CommandText = sql
            Dim da = DisposablesManager.BuildOracleDataAdapter(objSql)
            da.Fill(ds)
            package.PackageSKU = ds.Tables(0).Rows(0)("itm_cd") & ""
            package.PackagePrice = ds.Tables(0).Rows(0)("ret_prc") & ""
            Dim parentRow As Integer = ds.Tables(0).Rows(0)("row_id") & ""
            sql = "select (row_id - " & PackageParentRow & "-1) row_id,itm_cd, ret_prc, package_parent  from temp_itm where session_id ='" & System.Web.HttpContext.Current.Session.SessionID & "' and package_parent=" & parentRow & " order by 1"
            objSql.CommandText = sql
            Dim dsComponents As DataSet = New DataSet()
            da.Fill(dsComponents)

            package.Components = (From m In dsComponents.Tables(0).AsEnumerable()
                                  Select New PackageComponent() With {
                                      .SKU = m.Field(Of String)("ITM_CD"),
                                      .OrderOfOccurance = m.Field(Of Decimal)("row_id")
                                      }).ToList()
        End Using
        Return package
    End Function

    Public Sub UpdatePackagePrice(ByVal packageItem As PackageItem, ByVal packageParentRow As Integer, ByVal cmdUpdate As OracleCommand)
        Try
            cmdUpdate.Parameters.Clear()
            Dim Sql = "UPDATE TEMP_ITM SET ret_prc = :retPrice WHERE row_id = :selSkuRowNum"
            cmdUpdate.CommandText = Sql.ToString()
            cmdUpdate.Parameters.Add(":retPrice", OracleType.Double)
            cmdUpdate.Parameters(":retPrice").Value = packageItem.PackagePrice
            cmdUpdate.Parameters.Add(":selSkuRowNum", OracleType.Double)
            cmdUpdate.Parameters(":selSkuRowNum").Value = packageParentRow
            cmdUpdate.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GetAccountingSQL(ObjAccount As AccountingDtc) As String
        Dim objSql As New StringBuilder
        objSql.AppendLine()
        objSql.AppendLine("INSERT INTO AR_TRN (")
        objSql.AppendLine("CO_CD, CUST_CD, CNTR_CD, MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, TRN_TP_CD, ")
        objSql.AppendLine("BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, DOC_SEQ_NUM, AMT, POST_DT, STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ")
        objSql.AppendLine("IVC_CD, BNK_CRD_NUM, APP_CD, DES, ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ")
        objSql.AppendLine("ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM)")

        objSql.AppendLine("SELECT CO_CD, CUST_CD, CNTR_CD, :MOP_CD, EMP_CD_CSHR, EMP_CD_APP, EMP_CD_OP, ORIGIN_STORE, CSH_DWR_CD, :TRN_TP_CD, ")
        objSql.AppendLine("BNK_ACCT_CD, BAT_DOC_NUM, BNK_TRN_TP_CD, ")

        If Not String.IsNullOrEmpty(ObjAccount.DocumentSeqNumber) Then
            objSql.AppendLine(":DOC_SEQ_NUM,")
        Else
            objSql.AppendLine("DOC_SEQ_NUM, ")
        End If

        objSql.AppendLine(":NEW_AMT, TO_DATE('" & ObjAccount.PostDate & "', 'mm/dd/RRRR'), :STAT_CD, AR_TP, BNK_NUM, CHK_NUM, ")
        objSql.AppendLine(":NEW_IVC_CD, BNK_CRD_NUM, APP_CD, :DES, :ADJ_IVC_CD, CHNG_OUT, PMT_STORE, ARBR_DT, WR_DT, POST_ID_NUM, ")
        objSql.AppendLine(":ORIGIN_CD, EXP_DT, REF_NUM, HOST_REF_NUM, BALANCED, CREATE_DT, LN_NUM, ACCT_NUM ")
        objSql.AppendLine("FROM AR_TRN WHERE AR_TRN_PK=:AR_TRN_PK")

        Return objSql.ToString
    End Function

    Public Sub InsertAccountingEntries(ObjAccount As AccountingDtc, conn As OracleConnection)

        Using cmd As New OracleCommand(GetAccountingSQL(ObjAccount), conn)
            cmd.Parameters.Add(":NEW_AMT", OracleType.Double)
            cmd.Parameters.Add(":AR_TRN_PK", OracleType.VarChar)
            cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
            cmd.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
            cmd.Parameters.Add(":STAT_CD", OracleType.VarChar)
            cmd.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
            cmd.Parameters.Add(":NEW_IVC_CD", OracleType.VarChar)
            cmd.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
            cmd.Parameters.Add(":DES", OracleType.VarChar)

            If Not String.IsNullOrEmpty(ObjAccount.AccountKey) Then
                cmd.Parameters(":AR_TRN_PK").Value = CDbl(ObjAccount.AccountKey)
            Else
                cmd.Parameters(":AR_TRN_PK").Value = ObjAccount.AccountKey & ""
            End If

            If Not String.IsNullOrEmpty(ObjAccount.DocumentSeqNumber) Then
                cmd.Parameters.Add(":DOC_SEQ_NUM", OracleType.VarChar)
                cmd.Parameters(":DOC_SEQ_NUM").Value = ObjAccount.DocumentSeqNumber
            End If

            cmd.Parameters(":NEW_AMT").Value = CDbl(ObjAccount.Amount)
            cmd.Parameters(":MOP_CD").Value = ObjAccount.MopCode
            cmd.Parameters(":TRN_TP_CD").Value = ObjAccount.TransactionTypeCode
            cmd.Parameters(":STAT_CD").Value = ObjAccount.StatusCode
            cmd.Parameters(":ORIGIN_CD").Value = ObjAccount.OriginCode
            cmd.Parameters(":NEW_IVC_CD").Value = ObjAccount.IvcCode
            cmd.Parameters(":DES").Value = ObjAccount.Description
            cmd.Parameters(":ADJ_IVC_CD").Value = ObjAccount.AdjIvcCode

            cmd.ExecuteNonQuery()
        End Using
    End Sub

    'Alice added on Jan 08, 2019 for request 2260
    ''' <summary>
    ''' Ensure Leon’s can not enter a Brick Sales Order to exchange.
    '''	Ensure Brick can Not enter a Leon's Sales Order to exchange.
    '''	Ensure  Brick Corporate And Brick Franchises can enter exchanges on any order within their company group.
    '''	Ensure Leon's franchise can only exchange orders within their company code. (Confirm we don’t want any exceptions with Donna).
    '''	Do not allow Exchange on orders between different company groups.  This includes LSS, the LSS user would still have to cashier activate to the store where the exchange is to be done.
    ''' </summary>
    Public Function Validate_MCCL_SO_Access(ByVal delDocNum As String) As Boolean
        Dim sqlSb As StringBuilder = New StringBuilder()
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim retuenVal As Boolean = False
        sqlSb.Append("SELECT * FROM STORE STR, CUST C, SO ")
        sqlSb.Append("WHERE C.CUST_CD = SO.CUST_CD ")
        'sqlSb.Append("And SO.SO_STORE_CD = STR.STORE_CD ")
        sqlSb.Append("And SO.PU_DEL_STORE_CD = STR.STORE_CD ")
        sqlSb.Append("AND DEL_DOC_NUM ").Append(equalOrLike(delDocNum)).Append(" :docNum ")
        cmd.Parameters.Add(":docNum", OracleType.VarChar)
        cmd.Parameters(":docNum").Value = UCase(delDocNum)
        Dim emp_cd As String = HttpContext.Current.Session("EMP_CD")

        If Not SystemUtils.hasAllStoreAccess(emp_cd) Then
            sqlSb.Append(" And exists(select 1 from STORE_GRP$STORE, EMP2STORE_GRP ").Append(
                  "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd And emp2store_grp.emp_cd = : empCd and SO_STORE_CD = STORE_GRP$STORE.STORE_CD) ")
            cmd.Parameters.Add(":empCd", OracleType.VarChar)
            cmd.Parameters(":empCd").Value = UCase(emp_cd)
        End If

        Dim userType As Double = -1

        If (isNotEmpty(HttpContext.Current.Session("USER_TYPE"))) Then
            userType = HttpContext.Current.Session("USER_TYPE")
        End If
        Dim coCd As String = HttpContext.Current.Session("CO_CD")

        If (isNotEmpty(HttpContext.Current.Session("str_sup_user_flag")) And Not HttpContext.Current.Session("str_sup_user_flag").Equals("Y")) Then
            sqlSb.Append(" AND SO_STORE_CD in (select s.store_cd from store s, MCCL_STORE_GROUP a, store_grp$store b ")
            sqlSb.Append("where SO_STORE_CD = s.store_cd and a.store_grp_cd = b.store_grp_cd and ")
            sqlSb.Append("s.store_cd = b.store_cd and a.user_id = :userType ")

            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                sqlSb.Append(" and '" & coCd & "' = s.co_cd ")
            End If
            sqlSb.Append(" ) ")
            cmd.Parameters.Add(":userType", OracleType.VarChar)
            cmd.Parameters(":userType").Value = userType
        End If

        Try
            Dim dbReader As OracleDataReader

            cmd.CommandText = sqlSb.ToString()


            Using conn As OracleConnection = GetConnection()
                cmd.Connection = conn
                dbReader = DisposablesManager.BuildOracleDataReader(cmd)
                If dbReader.HasRows Then
                    retuenVal = True
                Else
                    retuenVal = False
                End If
            End Using


        Catch ex As Exception
            retuenVal = False
        Finally
            cmd.Dispose()
        End Try
        Return retuenVal
    End Function

    ''' <summary>Get sales order booking status code</summary>
    ''' <returns>Sales order booking status code</returns>
    Public Function getPOLineBookingStatus(ByVal po_cd As String, ByVal po_ln As String) As String
        ' Retur empty booking status if "po_cd" or "po_ln" is empty
        If String.IsNullOrEmpty(po_cd) OrElse String.IsNullOrEmpty(po_ln) Then
            Return String.Empty
        End If

        Dim dbConn As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim bookingStatusSql As New StringBuilder

        ' Open DB connection
        dbConn.Open()

        ' Prepare sql to get latest record from sale order history
        bookingStatusSql.Append("select std_shcm.getpoactntpcd(:po_cd, :po_ln) AS status from dual")

        ' Set sql to commandText
        dbCommand.CommandText = bookingStatusSql.ToString()

        ' Bind params to sql
        dbCommand.Parameters.Add(":po_cd", OracleType.VarChar)
        dbCommand.Parameters.Add(":po_ln", OracleType.Number)

        ' Bind params values to SQL
        dbCommand.Parameters(":po_cd").Value = po_cd
        dbCommand.Parameters(":po_ln").Value = po_ln.ToString

        ' create Data reader
        Dim dbReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)
        Dim bookingStatus As String = String.Empty

        If (dbReader.Read()) Then
            bookingStatus = dbReader.Item("status").ToString
        End If

        ' Close DB connection
        dbConn.Close()

        Return bookingStatus
    End Function

    ''' <summary>Get sales order booking quantity - #ITREQUEST-337</summary>
    ''' <returns>Sales order booking quantity</returns>
    Public Function getPOLineBookingQty(ByVal po_cd As String, ByVal po_ln As String) As Integer
        ' Retur empty booking status if "po_cd" or "po_ln" is empty
        If String.IsNullOrEmpty(po_cd) OrElse String.IsNullOrEmpty(po_ln) Then
            Return String.Empty
        End If

        Dim dbConn As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim bookingStatusSql As New StringBuilder

        ' Open DB connection
        dbConn.Open()

        ' Prepare sql to get latest record from sale order history
        bookingStatusSql.Append("select std_shcm.getbook_qty(:po_cd, :po_ln) AS quantity from dual")

        ' Set sql to commandText
        dbCommand.CommandText = bookingStatusSql.ToString()

        ' Bind params to sql
        dbCommand.Parameters.Add(":po_cd", OracleType.VarChar)
        dbCommand.Parameters.Add(":po_ln", OracleType.Number)

        ' Bind params values to SQL
        dbCommand.Parameters(":po_cd").Value = po_cd
        dbCommand.Parameters(":po_ln").Value = po_ln.ToString

        ' create Data reader
        Dim dbReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)
        Dim bookingQuantity As Integer = 0

        If (dbReader.Read() AndAlso Not IsDBNull(dbReader.Item("quantity"))) Then
            If Integer.TryParse(dbReader.Item("quantity"), bookingQuantity) Then
                ' Return booking quantity
                Return bookingQuantity
            End If
        End If

        ' Close DB connection
        dbConn.Close()

        ' Return booking quantity
        Return bookingQuantity
    End Function
End Class
