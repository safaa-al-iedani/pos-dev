﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.OracleClient
Imports OrderUtils

Public Class CommentsDac
    Inherits BaseDac

    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    ''' <summary>
    ''' Saves the comments passed to the SO_CMNT table
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="cmntKey">the comment key to be used when obtaining the next sequence</param>
    ''' <param name="commentType">either 'S' for Sales or 'D' for Delivery</param>
    ''' <param name="comments">the comments text</param>
    ''' <param name="storeCd">the store code where this comment was generated</param>
    ''' <param name="transDt">the effective date of the transaction</param>
    Public Sub SaveSOComments(ByRef dbAtomicCommand As OracleCommand,
                              ByVal cmntKey As WrDtStoreSeqKey,
                              ByVal commentType As String,
                              ByVal comments As String,
                              ByVal storeCd As String,
                              ByVal transDt As String)

        ' Daniela Oct 24, 2014 use format
        Dim lngComments As Integer = Len(comments)

        If lngComments > 0 Then

            Dim lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey)
            Dim strSO_SEQ_NUM = cmntKey.seqNum

            'Dim sql As String
            'Dim lngSEQNUM As Integer = getNextSoCmntSeq(dbAtomicCommand, cmntKey)
            'Dim strSO_SEQ_NUM As String = cmntKey.seqNum
            'Dim strMidCmnts As String
            'Dim seqNum As Integer = 1

            'Do Until seqNum > lngComments

            LeonsBiz.leons_insert_cmnt(transDt, storeCd, strSO_SEQ_NUM, lngSEQNUM, comments, "", commentType.Trim)

            'strMidCmnts = Mid(comments, seqNum, 70)
            'strMidCmnts = Replace(strMidCmnts, "'", "")

            'Sql = "INSERT INTO SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM, CMNT_TYPE) Values ( " & _
            '            "TO_DATE('" & transDt & "','mm/dd/RRRR')" & ",'" & storeCd & "','" & strSO_SEQ_NUM & "'," & _
            '            lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & _
            '            strMidCmnts & "','','" & commentType.Trim & "')"

            'dbAtomicCommand.CommandText = UCase(Sql)
            'dbAtomicCommand.ExecuteNonQuery()

            'seqNum = seqNum + 70
            'lngSEQNUM = lngSEQNUM + 1
            'Loop
        End If
    End Sub

    ''' <summary>
    ''' Saves a comment that contains a Finance message to the SO_CMNT table
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="soKeys">sales order keys to obtain the next sequence</param>
    ''' <param name="delDocNum">document number for the comments being inserted</param>
    ''' <param name="promoCd">the promotion code for the finance payment</param>
    Public Sub SaveSOFinanceComments(ByRef dbAtomicCommand As OracleCommand,
                                     ByVal soKeys As SalesOrderKeys,
                                     ByVal delDocNum As String,
                                     ByVal promoCd As String,
                                     ByVal storeCd As String,
                                     ByVal transDt As String)
        Dim sql As String
        Dim cmntKey = New WrDtStoreSeqKey
        cmntKey.wrDt = soKeys.soKey.wrDt
        cmntKey.storeCd = soKeys.soKey.storeCd
        cmntKey.seqNum = soKeys.soKey.seqNum

        Dim lngSEQNUM As Integer = getNextSoCmntSeq(dbAtomicCommand, cmntKey)
        Dim strSO_SEQ_NUM As String = soKeys.soKey.seqNum
        Dim strMidCmnts As String = "Promotion-Code Assigned to WDR Deposit = " & promoCd
        Dim seqNum As Integer = 1

        sql = "INSERT INTO SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM) Values ( " & _
                    "TO_DATE('" & transDt & "','mm/dd/RRRR')" & ",'" & storeCd & "','" & strSO_SEQ_NUM & "'," & _
                    lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & _
                    strMidCmnts & "', '" & delDocNum & "')"  ' finance can stay on specific doc for now

        dbAtomicCommand.CommandText = UCase(sql)
        dbAtomicCommand.ExecuteNonQuery()
    End Sub

    ''' <summary> Alice added on May 16, 2019 for request 81
    ''' Saves a comment for CRM/MCR/MDB transaction which was based on invalid original invoice number
    ''' </summary>
    ''' <param name="dbAtomicCommand">database command to be used</param>
    ''' <param name="soKeys">sales order keys to obtain the next sequence</param>
    ''' <param name="delDocNum">document number for the comments being inserted</param>

    Public Sub SaveInvalidInvoiceComments(ByRef dbAtomicCommand As OracleCommand,
                                     ByVal soKeys As SalesOrderKeys,
                                     ByVal delDocNum As String,
                                     ByVal InvalidText As String,
                                     ByVal storeCd As String,
                                     ByVal transDt As String)


        Dim sql As String
            Dim cmntKey = New WrDtStoreSeqKey
            cmntKey.wrDt = soKeys.soKey.wrDt
            cmntKey.storeCd = soKeys.soKey.storeCd
            cmntKey.seqNum = soKeys.soKey.seqNum

            Dim lngSEQNUM As Integer = getNextSoCmntSeq(dbAtomicCommand, cmntKey)
            Dim strSO_SEQ_NUM As String = soKeys.soKey.seqNum
            Dim seqNum As Integer = 1

            sql = "INSERT INTO SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM) Values ( " &
                        "TO_DATE('" & transDt & "','mm/dd/RRRR')" & ",'" & storeCd & "','" & strSO_SEQ_NUM & "'," &
                        lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" &
                        InvalidText & "', '" & delDocNum & "')"

            dbAtomicCommand.CommandText = UCase(sql)
            dbAtomicCommand.ExecuteNonQuery()

    End Sub


    ''' <summary>
    ''' Saves the AR comments passed in to the CUST_CMNT table
    ''' </summary>
    ''' <param name="dbAtomicCommand">the database command to be used</param>
    ''' <param name="empCd">the employee code keying the sale</param>
    ''' <param name="custCd">the customer code in the sale</param>
    ''' <param name="comments">the AR comments</param>
    Public Sub SaveARComments(ByRef dbAtomicCommand As OracleCommand,
                              ByVal empCd As String,
                              ByVal custCd As String,
                              ByVal comments As String)

        Dim lngComments As Integer = Len(comments)
        If lngComments > 0 Then

            Dim dbConnection As OracleConnection = GetConnection()
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String = "Select Max(SEQ#) from CUST_CMNT where CUST_CD = '" & custCd & "'"

            Dim lngSEQNUM As Integer = 1
            Dim strMidCmnts As String
            Dim seqNum As Integer = 1

            Try
                dbConnection.Open()
                dbCommand = GetCommand(sql, dbConnection)
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If dbReader.Read() AndAlso dbReader.IsDBNull(0) = False Then lngSEQNUM = dbReader(0) + 1
                dbReader.Close()

                Do Until seqNum > lngComments
                    strMidCmnts = Mid(comments, seqNum, 60)
                    strMidCmnts = Replace(strMidCmnts, "'", "")

                    sql = "INSERT INTO CUST_CMNT (CUST_CD, SEQ#, CMNT_DT, TEXT, EMP_CD_OP) Values ('" & _
                                custCd & "','" & lngSEQNUM & "'," & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & _
                                "','mm/dd/RRRR')" & " ,'" & strMidCmnts & "','" & empCd & "')"

                    'Alice update for ticket 582470 to avoid the uniquie constraint(cust_cmnt_pk)violated error
                    'ticket 582470 sale order has account comments and select “energy star rebate” payment, “ESR payment” assign “ESTAR” to the approve_cd field which trigger another theSalesBiz.SaveARcomments action, 
                    'the two SaveARcomments using same dbAtomicCommand which run on one ExecuteNonQuery caused two inserts have same sequence number for same customer, it casued oracle error. 
                    'change start
                    'dbAtomicCommand.CommandText = sql
                    'dbAtomicCommand.ExecuteNonQuery()
                    dbCommand.CommandText = sql
                    dbCommand.ExecuteNonQuery()
                    'change end
                    seqNum = seqNum + 60
                    lngSEQNUM = lngSEQNUM + 1
                Loop

            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Gets the next sequence number to be used when inserting a comment in the SO_CMT table
    ''' </summary>
    ''' <param name="cmd">the database command that should be used</param>
    ''' <param name="soCmntKey">the comment key to be used when genearting the sequence number</param>
    ''' <returns>the next sequence number that should be used</returns>
    Public Function getNextSoCmntSeq(ByRef cmd As OracleCommand, ByVal soCmntKey As WrDtStoreSeqKey) As Integer
        'Returns the next so_cmnt seq# to use for insertion to an SO_doc_num
        'Use this when inserting rows within a transaction

        Dim retVal As Integer = 0
        Dim dbReader As OracleDataReader
        Dim sql As New StringBuilder
        sql.Append("SELECT (NVL(MAX(cmnt.seq#), 0) + 1) AS seqNum FROM so_cmnt cmnt ").Append(
            "WHERE cmnt.so_wr_dt = :wrDt AND cmnt.so_store_cd = :storeCd AND cmnt.so_seq_num = :soSeqNum ")

        cmd.Parameters.Clear()
        cmd.CommandText = sql.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add(":storeCd", OracleType.VarChar)
        cmd.Parameters.Add(":soSeqNum", OracleType.VarChar)
        cmd.Parameters.Add(":wrDt", OracleType.DateTime)

        cmd.Parameters(":storeCd").Value = soCmntKey.storeCd
        cmd.Parameters(":soSeqNum").Value = soCmntKey.seqNum
        cmd.Parameters(":wrDt").Value = FormatDateTime(soCmntKey.wrDt, DateFormat.ShortDate)

        dbReader = DisposablesManager.BuildOracleDataReader(cmd)

        If dbReader.HasRows Then
            dbReader.Read()
            retVal = dbReader("seqNum").ToString
        End If
        dbReader.Close()
        cmd.Parameters.Clear()

        Return retVal
    End Function


End Class
