﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class PODac
    Inherits BaseDac

    ''' <summary>
    ''' Getting the PO Line details
    ''' </summary>
    ''' <param name="purchaseOrder">PO_CD</param>
    ''' <returns>PO Line details as a DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetPOLineDetails(ByVal strPO_CD As String) As DataTable
        Dim dt As New DataTable

        Dim sbSql As New StringBuilder

        sbSql.Append("   SELECT   SP.DEL_DOC_NUM,")
        sbSql.Append("            PO.PO_CD,")
        sbSql.Append("            PO_LN.LN#,")
        sbSql.Append("            PO_LN.ITM_CD,")
        sbSql.Append("            ITM.VSN,")
        sbSql.Append("            PO_LN.DEST_STORE_CD,")
        sbSql.Append("            PO_LN.ARRIVAL_DT,")
        sbSql.Append("            PO_LN.QTY_ORD,")
        sbSql.Append("            PO_LN.SHIP_DT,")
        sbSql.Append("            PO_LN.UNIT_CST")
        sbSql.Append("    FROM               PO")
        sbSql.Append("                    LEFT JOIN")
        sbSql.Append("                       PO_LN")
        sbSql.Append("                    ON PO.PO_CD = PO_LN.PO_CD")
        sbSql.Append("                 LEFT JOIN")
        sbSql.Append("                    SO_LN$PO_LN SP")
        sbSql.Append("                 ON PO.PO_CD = SP.PO_CD AND PO_LN.LN# = SP.LN#")
        sbSql.Append("              LEFT JOIN")
        sbSql.Append("                 ITM")
        sbSql.Append("              ON PO_LN.ITM_CD = ITM.ITM_CD")
        sbSql.Append("           LEFT JOIN")
        sbSql.Append("              SO_LN")
        sbSql.Append("           ON     SP.DEL_DOC_NUM = SO_LN.DEL_DOC_NUM")
        sbSql.Append("              AND SP.DEL_DOC_LN# = SO_LN.DEL_DOC_LN#")
        sbSql.Append("   WHERE   SO_LN.STORE_CD IS NULL")
        sbSql.Append("              AND SO_LN.LOC_CD IS NULL")
        sbSql.Append("              AND PO.PO_CD = :PO_CD")
        sbSql.Append(" ORDER BY   PO_LN.LN# ASC")

        Dim conn As OracleConnection = GetConnection()
        Dim comm As OracleCommand = GetCommand(sbSql.ToString(), conn)
        Dim ds As New DataSet

        Try
            conn.Open()

            comm.Parameters.Clear()
            comm.Parameters.Add(":PO_CD", OracleType.VarChar)
            comm.Parameters(":PO_CD").Value = strPO_CD

            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(comm)
            oAdp.Fill(ds)
            oAdp.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(conn, comm)
        End Try

        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If

        Return dt
    End Function
    ''' <summary>
    ''' GetPOArrivalDate - Returns the PO arrival date for the given PO order.
    ''' </summary>
    ''' <param name="purchaseOrder">PO Number</param>
    ''' <returns></returns>
    Public Function GetPOArrivalDate(ByVal purchaseOrder As String) As String
        Dim POArrivalDate As String = String.Empty
        Dim sbSql As New StringBuilder
        sbSql.Append("SELECT ARRIVAL_DT ")
        sbSql.Append("FROM PO, PO_LN ")
        sbSql.Append("WHERE PO.PO_CD=PO_LN.PO_CD ")
        sbSql.Append("AND PO.PO_CD = :PO_CD")
        Dim ds As New DataSet
        Using conn As OracleConnection = GetConnection(), comm As OracleCommand = GetCommand(sbSql.ToString(), conn)
            conn.Open()
            comm.Parameters.Clear()
            comm.Parameters.Add(":PO_CD", OracleType.VarChar)
            comm.Parameters(":PO_CD").Value = purchaseOrder
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(comm)
            oAdp.Fill(ds)
            oAdp.Dispose()
        End Using

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            POArrivalDate = ds.Tables(0).Rows(0)("ARRIVAL_DT") & ""
        End If

        Return POArrivalDate
    End Function
    ''' <summary>
    ''' GetPOArrivalDateForTempLineItem -- Returns the PO arrival date for the given line number
    ''' </summary>
    ''' <param name="RowNumber">TEMP_ITM row number</param>
    ''' <returns></returns>
    Public Function GetPOArrivalDateForTempLineItem(ByVal RowNumber As String) As String
        Dim POArrivalDate As String = String.Empty
        Dim sbSql As New StringBuilder
        Dim ds As New DataSet
        sbSql.Append("SELECT ARRIVAL_DT ")
        sbSql.Append("FROM PO JOIN PO_LN ON PO.PO_CD=PO_LN.PO_CD  ")
        sbSql.Append("WHERE  ")
        sbSql.Append("PO.PO_CD IN(SELECT PO_CD FROM TEMP_ITM WHERE ROW_ID = :ROW_ID ) ")
        Using conn As OracleConnection = GetConnection(), comm As OracleCommand = GetCommand(sbSql.ToString(), conn)
            conn.Open()
            comm.Parameters.Clear()
            comm.Parameters.Add(":ROW_ID", OracleType.VarChar)
            comm.Parameters(":ROW_ID").Value = RowNumber
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(comm)
            oAdp.Fill(ds)
            oAdp.Dispose()
        End Using

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            POArrivalDate = ds.Tables(0).Rows(0)("ARRIVAL_DT") & ""
        End If

        Return POArrivalDate
    End Function

    ''' <summary>
    ''' This method returns boolean value based on the PO availability.
    ''' </summary>
    ''' <param name="objSqlCommand"></param>
    ''' <param name="PurchaseOrderCode"></param>
    ''' <param name="POLine"></param>
    ''' <param name="ItemCode"></param>
    ''' <param name="Quantity"></param>
    ''' <returns></returns>
    Public Function IsPOAvailable(ByRef objSqlCommand As OracleCommand, ByVal PurchaseOrderCode As String, ByVal POLine As String, ByVal ItemCode As String, ByVal Quantity As String) As Boolean
        Dim sqlQueryBuilder As StringBuilder = New StringBuilder()
        Dim MyDatareader As OracleDataReader
        Dim POAvailable As Boolean = True

        objSqlCommand.Parameters.Clear()
        sqlQueryBuilder.Length = 0
        sqlQueryBuilder.Append("select  NVL(sum(TOTAL_INCOMING)-SUM(QTY_RCV+QTY_SOLD),0) as TOTAL_AVAIL  FROM ")
        sqlQueryBuilder.Append("( ")
        sqlQueryBuilder.Append("SELECT a.PO_CD, b.LN#, b.QTY_ORD As Total_Incoming, 0 AS QTY_RCV, 0 AS QTY_SOLD   ")
        sqlQueryBuilder.Append("FROM PO a, PO_LN b  ")
        sqlQueryBuilder.Append("WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '").Append(ItemCode).Append("'  ")
        sqlQueryBuilder.Append("AND a.PO_CD = '").Append(PurchaseOrderCode).Append("'  ")
        sqlQueryBuilder.Append("union all  ")
        sqlQueryBuilder.Append("SELECT a.PO_CD, b.LN#, 0 As Total_Incoming, 0 AS QTY_RCV, d.QTY AS QTY_SOLD  ")
        sqlQueryBuilder.Append("FROM PO a, PO_LN b, SO_LN$PO_LN c, SO_LN d ")
        sqlQueryBuilder.Append("WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '").Append(ItemCode).Append("'  ")
        sqlQueryBuilder.Append("AND c.PO_CD = b.PO_CD ")
        sqlQueryBuilder.Append("AND b.LN# = c.LN# ")
        sqlQueryBuilder.Append("AND c.DEL_DOC_NUM = d.DEL_DOC_NUM ")
        sqlQueryBuilder.Append("AND d.DEL_DOC_LN# = c.DEL_DOC_LN# ")
        sqlQueryBuilder.Append("AND a.PO_CD = '").Append(PurchaseOrderCode).Append("'  ")
        sqlQueryBuilder.Append("union all ")
        sqlQueryBuilder.Append("SELECT a.PO_CD, b.LN#, 0 As Total_Incoming, c.QTY AS QTY_RCV, 0 AS QTY_SOLD   ")
        sqlQueryBuilder.Append("FROM PO a, PO_LN b, PO_LN$ACTN_HST c  ")
        sqlQueryBuilder.Append("WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '").Append(ItemCode).Append("'  ")
        sqlQueryBuilder.Append("AND c.PO_CD = b.PO_CD ")
        sqlQueryBuilder.Append("AND b.LN# = c.LN# ")
        sqlQueryBuilder.Append("and c.PO_ACTN_TP_CD = 'RCV' ")
        sqlQueryBuilder.Append("AND a.PO_CD = '").Append(PurchaseOrderCode).Append("'  ")
        sqlQueryBuilder.Append(") PO_DATA  ")
        sqlQueryBuilder.Append("WHERE LN# = ").Append(POLine)

        'sqlQueryBuilder.Append(" HAVING SUM(TOTAL_INCOMING)-SUM(QTY_RCV+QTY_SOLD)>=").Append(Quantity)
        objSqlCommand.CommandText = sqlQueryBuilder.ToString

        Try

            MyDatareader = objSqlCommand.ExecuteReader

            If MyDatareader.Read() Then
                If CDbl(MyDatareader.Item("TOTAL_AVAIL").ToString) >= CDbl(Quantity) Then
                    POAvailable = True
                Else
                    POAvailable = False
                End If
            Else
                POAvailable = False
            End If

        Catch ex As Exception
            Throw
        Finally
            objSqlCommand.Parameters.Clear()
            objSqlCommand.CommandText = ""
        End Try
        Return POAvailable
    End Function

End Class
