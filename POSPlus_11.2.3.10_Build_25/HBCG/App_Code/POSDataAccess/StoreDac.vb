﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.OracleClient

Public Class StoreDac
    Inherits BaseDac

    Private theSystemDac As SystemDac

    ''' <summary>
    ''' Fetches the store details, for the store code passed in.
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieved details for</param>
    ''' <returns>the store details or an empty object if no matches are found</returns>
    Public Function GetStoreInfo(ByVal storeCd As String) As StoreData

        Dim store As New StoreData
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String

        ' extracted all columns required by invoice printing - can add to but do not remove unless coordinated with invoice printing
        sql = " SELECT co_cd, store_name, addr1, addr2, city, st, zip_cd, county, lst_bal_dt, "
        sql = sql + " ship_to_store_cd, pickup_store_cd, default_loc, zone_cd, enable_ars "
        sql = sql + " FROM store WHERE store_cd = :storeCd "

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCommand.Parameters(":storeCd").Value = UCase(storeCd)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


            If dbReader.Read() Then
                store.addr = New AddressDtc
                store.addr.addr1 = dbReader.Item("ADDR1").ToString()
                store.addr.addr2 = dbReader.Item("ADDR2").ToString()
                store.addr.city = dbReader.Item("CITY").ToString()
                store.addr.stCd = dbReader.Item("ST").ToString()
                store.addr.postalCd = dbReader.Item("ZIP_CD").ToString()
                store.addr.county = dbReader.Item("COUNTY").ToString()

                store.cd = storeCd
                store.coCd = dbReader.Item("CO_CD").ToString()
                store.name = dbReader.Item("STORE_NAME").ToString()
                store.lastBalDt = dbReader.Item("LST_BAL_DT").ToString()
                store.shipToStoreCd = dbReader.Item("SHIP_TO_STORE_CD").ToString()
                store.puStoreCd = dbReader.Item("PICKUP_STORE_CD").ToString()
                store.defaultLoc = dbReader.Item("DEFAULT_LOC").ToString()
                store.zoneCd = dbReader.Item("ZONE_CD").ToString()
                store.enableARS = StringUtils.ResolveYNToBoolean(dbReader.Item("ENABLE_ARS").ToString())
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return store
    End Function

    ''' <summary>
    ''' Gets the stores accessible for the employee based on the store based security enabled and access for the employee.
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' NOTE: if no employee is provided, ALL stores defined in the system.
    ''' </summary>
    ''' <param name="empCd">the employee code to fetch stores for</param>
    ''' <returns>a list of stores available for the employee</returns>
    Public Function GetEmpStores(ByVal empCd As String) As DataSet

        Dim storesDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String

        Try
            dbConnection.Open()

            sql = "SELECT store_cd, store_cd || ' - ' || store_name AS store_des FROM store "
            If (Not String.IsNullOrEmpty(empCd) AndAlso Not GetSystemDac().HasAllStoreAccess(empCd)) Then
                sql = sql + " WHERE STORE_CD IN "
                sql = sql + "(SELECT store_grp$store.store_cd FROM store_grp$store, emp2store_grp WHERE "
                sql = sql + " emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd AND emp2store_grp.emp_cd = :empCd )"

                dbCommand.Parameters.Add(":empCd", OracleType.VarChar)
                dbCommand.Parameters(":empCd").Value = UCase(empCd)
            End If
            sql = sql + " ORDER BY STORE_CD "

            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(storesDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return storesDataSet
    End Function

    'MM-5672
    ''' <summary>
    ''' To get store and location from INV_XREF table 
    ''' </summary>
    ''' <param name="itemCode">Item Code</param>
    ''' <param name="delDocNumber">Del Doc Number</param>
    ''' <param name="delDocLineNumber">Del Doc Line number</param>
    ''' <param name="store">Store</param>
    ''' <param name="location">Location code</param>
    ''' <remarks></remarks>
    Public Sub GetStoreLocation(ByVal itemCode As String, ByVal delDocNumber As String, ByVal delDocLineNumber As Integer, ByRef store As String, ByRef location As String)

        Dim dbSql As String = "SELECT STORE_CD, LOC_CD FROM INV_XREF WHERE DISPOSITION = 'PEN' AND RF_ID_CD IS NOT NULL AND DEL_DOC_NUM = :delDocNumber AND ITM_CD = :itemCode AND DEL_DOC_LN# = :delDocLineNumber"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(dbSql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":delDocNumber", OracleType.VarChar)
            dbCommand.Parameters(":delDocNumber").Value = delDocNumber

            dbCommand.Parameters.Add(":itemCode", OracleType.VarChar)
            dbCommand.Parameters(":itemCode").Value = itemCode

            dbCommand.Parameters.Add(":delDocLineNumber", OracleType.Int32)
            dbCommand.Parameters(":delDocLineNumber").Value = delDocLineNumber

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader IsNot Nothing AndAlso dbReader.HasRows) Then
                If dbReader.Read() Then
                    If Not dbReader.Item("STORE_CD") Is DBNull.Value Then
                        store = dbReader.Item("STORE_CD")
                    End If
                    If Not dbReader.Item("LOC_CD") Is DBNull.Value Then
                        location = dbReader.Item("LOC_CD")
                    End If
                End If
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub


    ''' <summary>
    ''' Gets the stores within the same company code assiged to the user's home store code. Apply MCCL #1
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' NOTE: if no employee is provided, ALL stores defined in the system.
    ''' </summary>
    ''' <param name="empCd">the employee code to fetch stores for</param>
    ''' <returns>a list of stores available for the employee</returns>
    Public Function GetStores(ByVal empCd As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim storesDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String

        Try
            dbConnection.Open()

            sql = "SELECT store_cd, store_cd || ' - ' || store_name AS store_des FROM store "
            If (Not String.IsNullOrEmpty(empCd) AndAlso Not GetSystemDac().HasAllStoreAccess(empCd)) Then
                sql = sql + " WHERE STORE_CD IN "
                sql = sql + "(SELECT store_grp$store.store_cd FROM store_grp$store, emp2store_grp WHERE "
                sql = sql + " emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd AND emp2store_grp.emp_cd = :empCd )"

                dbCommand.Parameters.Add(":empCd", OracleType.VarChar)
                dbCommand.Parameters(":empCd").Value = UCase(empCd)
            End If
            sql = sql + " ORDER BY STORE_CD "

            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(storesDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return storesDataSet
    End Function

    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' Gets the stores within the same company code assiged to the user's home store code. Apply MCCL #1
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Function GetStores_MCCL1(ByVal p_homeStoreCode As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim storesDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            dbConnection.Open()

            sqlString = New StringBuilder("SELECT store_cd, store_cd || ' - ' || store_name as store_des ")
            sqlString.Append("FROM store s ")
            sqlString.Append("WHERE store_cd =UPPER(:x_homeStoreCode) ")

            sql = sqlString.ToString()

            dbCommand.Parameters.Add(":x_homeStoreCode", OracleType.VarChar)
            dbCommand.Parameters(":x_homeStoreCode").Value = p_homeStoreCode

            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(storesDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return storesDataSet
    End Function

    ''' <summary>
    ''' Jan 12, 2018 - mariam
    ''' Gets the stores within the same company code assiged to the user's home store code. Apply MCCL #1
    ''' This list of stores should work for most, if not all, store list dropdowns.
    ''' </summary>
    ''' <param name="homeStoreCode">the homeStoreCode to fetch stores for</param>
    Public Function GetFrStore_Detail(ByVal p_StoreCode As String) As DataSet
        'To edit it on Jan 12, 2018 Fri by Mariam
        Dim storesDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String
        Dim sqlString As StringBuilder

        Try
            dbConnection.Open()

            sqlString = New StringBuilder("select fr.whse_store_cd, fr.cust_cd as fr_cust_cd, str.store_name ")
            sqlString.Append("from FRTBL fr, store str ")
            sqlString.Append("WHERE fr.store_cd =UPPER(:x_StoreCode) and fr.store_cd = str.store_cd ")

            sql = sqlString.ToString()

            dbCommand.Parameters.Add(":x_StoreCode", OracleType.VarChar)
            dbCommand.Parameters(":x_StoreCode").Value = p_StoreCode

            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(storesDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return storesDataSet
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    Private Function GetSystemDac() As SystemDac
        If (theSystemDac Is Nothing) Then
            theSystemDac = New SystemDac()
        End If

        Return theSystemDac
    End Function


End Class
