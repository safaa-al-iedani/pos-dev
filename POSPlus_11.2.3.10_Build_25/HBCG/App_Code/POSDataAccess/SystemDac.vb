﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OracleClient

Public Class SystemDac
    Inherits BaseDac

    ''' <summary>
    '''Returns from the GP PARMS table, the value for the key and parm passed in
    ''' </summary>
    ''' <param name="key">the key to look the parameter on</param>
    ''' <param name="parm">the parameter name</param>
    ''' <returns>the corresponding value found or an empty string if no match found</returns>
    Public Function GetGpParm(ByVal key As String, ByVal parm As String) As String

        Dim gpParmValue As String = String.Empty
        If (Not String.IsNullOrEmpty(key) AndAlso Not String.IsNullOrEmpty(parm)) Then

            Dim dbConnection As OracleConnection = GetConnection()
            Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
            Dim dbReader As OracleDataReader
            Dim sql As StringBuilder = New StringBuilder()

            sql.Append("SELECT value FROM gp_parms WHERE key = :KEY AND parm = :PARM ")
            sql.Append(" AND value NOT LIKE '%UPDATE WITH VALUE%'")

            Try
                dbConnection.Open()
                dbCommand.CommandText = sql.ToString()
                dbCommand.Parameters.Add(":KEY", OracleType.VarChar)
                dbCommand.Parameters(":KEY").Value = key.Trim
                dbCommand.Parameters.Add(":PARM", OracleType.VarChar)
                dbCommand.Parameters(":PARM").Value = parm.Trim
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


                If (dbReader.Read()) Then
                    gpParmValue = dbReader.Item("value")
                End If
                dbReader.Close()

            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If

        Return gpParmValue
    End Function

    ''' <summary>
    '''Returns from the GP PARMS table, the values that match the key and parm passed in
    ''' </summary>
    ''' <param name="key">the key to look the parameter on</param>
    ''' <param name="parm">the parameter name to be used in a LIKE clause</param>
    ''' <returns>the corresponding values found or an empty array if no matches found</returns>
    Public Function GetGpParmValues(ByVal key As String, ByVal parm As String) As String()

        Dim gpParmValues As New List(Of String)
        If (Not String.IsNullOrEmpty(key) AndAlso Not String.IsNullOrEmpty(parm)) Then

            Dim dbConnection As OracleConnection = GetConnection()
            Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
            Dim dbReader As OracleDataReader
            Dim sql As String = "SELECT value FROM gp_parms WHERE key = :key AND parm LIKE :parm"

            Try
                dbConnection.Open()
                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":KEY", OracleType.VarChar)
                dbCommand.Parameters(":KEY").Value = key.Trim
                dbCommand.Parameters.Add(":PARM", OracleType.VarChar)
                dbCommand.Parameters(":PARM").Value = parm.Trim
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


                While dbReader.Read()
                    gpParmValues.Add(dbReader("value").ToString)
                End While
                dbReader.Close()

            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If

        Return gpParmValues.ToArray
    End Function

    ''' <summary>
    ''' 'Returns the value of the system parameter from E1 using SYSPM_REF - is the most
    ''' efficient way to call for system parameter info if the param has a name.
    ''' </summary>
    ''' <param name="paramName">system parameter name</param>
    ''' <returns>the value of the system parameter from E1</returns>
    Public Function GetSysPm(ByVal paramName As String) As String

        Dim sysPmValue As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As String = "bt_syspm.param_value"

        dbCommand.CommandText = sql
        dbCommand.CommandType = CommandType.StoredProcedure

        dbCommand.Parameters.Add("returnValue", OracleType.VarChar, 50).Direction = ParameterDirection.ReturnValue
        dbCommand.Parameters.Add("param_name_i", OracleType.VarChar).Direction = ParameterDirection.Input

        dbCommand.Parameters("param_name_i").Value = paramName

        Try
            dbConnection.Open()
            dbCommand.ExecuteNonQuery()
            sysPmValue = dbCommand.Parameters("returnValue").Value.ToString

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return sysPmValue
    End Function


    ''' <summary>
    ''' Returns the value of the system parameter from E1 without using SYSPM_REF. This is used when 
    ''' there is not a distinct name available for the system parameter.
    ''' </summary>
    ''' <param name="key">Subsystem key to get the parameter from </param>
    ''' <param name="startPos">starting position to read the value</param>
    ''' <param name="length">number of characters to fetch</param>
    ''' <returns>The value of the system parameter requested</returns>
    Public Function GetSysPm(ByVal key As String, ByVal startPos As Integer, ByVal length As Integer) As String

        Dim sysPmValue As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String
        sql = "SELECT RTRIM(SUBSTR(DATA, " & startPos & ", " & length & ")) SYSPM_VAL " &
               " FROM sys_params " &
              " WHERE key = '" & key & "'  "

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If Not dbReader.HasRows Then
                Throw New Exception("Invalid system parameter key value " & key) ' err cd 3085
            Else
                dbReader.Read()
                sysPmValue = dbReader.Item("SYSPM_VAL").ToString
                If String.IsNullOrEmpty(sysPmValue) Then
                    Throw New Exception("System parameter cannot be null for subsystem " & key) ' no default of syspm values, err cd 3087
                End If
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return sysPmValue
    End Function

    ''' <summary>
    ''' Finds out if the employee passed in, has access to the page indicated
    ''' </summary>
    ''' <param name="empCd">the employee code in question</param>
    ''' <param name="pName">the page name to check for access</param>
    ''' <returns>TRUE, if employee has access; FALSE otherwise</returns>
    Public Function HasPageAccess(ByVal empCd As String, pName As String) As Boolean

        Dim hasAccess As Boolean = False
        If (Not String.IsNullOrEmpty(empCd) AndAlso Not String.IsNullOrEmpty(pName)) Then
            Dim dbConnection As OracleConnection = GetConnection()
            Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
            Dim dbReader As OracleDataReader
            Dim sql As String = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD = :empCd AND " & pName & "='Y'"

            Try
                dbConnection.Open()
                dbCommand.CommandText = UCase(sql)
                dbCommand.Parameters.Add(":empCd", OracleType.VarChar)
                dbCommand.Parameters(":empCd").Value = empCd

                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If dbReader.HasRows Then hasAccess = True
                dbReader.Close()

            Catch ex As Exception
                Throw
            Finally
                CloseResources(dbConnection, dbCommand)
            End Try
        End If
        Return hasAccess
    End Function

    ''' <summary>
    ''' Finds out if the passed in employee code has access to ALL stores.
    ''' </summary>
    ''' <param name="empCd">the employee code to find out what store access he has</param>
    ''' <returns>true, if employee has access to ALL stores; false otherwise</returns>
    Public Function HasAllStoreAccess(ByVal empCd As String) As Boolean

        Dim allStoreAccess As Boolean = False

        If ("N".Equals(ConfigurationManager.AppSettings("store_based")) OrElse
            "Y".Equals(ConfigurationManager.AppSettings("store_based")) AndAlso SkipDataSecurity(empCd)) Then

            allStoreAccess = True
        End If

        Return allStoreAccess
    End Function

    ''' <summary>
    ''' Inserts a record to the CRM.AUDIT_LOG table with details passed in.   
    ''' The data will be automatically saved in the database.
    ''' </summary>
    ''' <param name="columnId">the column ID to be saved</param>
    ''' <param name="columnDesc">the column DESCRIPTION to be saved</param>
    ''' <param name="empCd">the current employee code</param>
    Public Sub SaveAuditLogComment(ByVal columnId As String, ByVal columnDesc As String, ByVal empCd As String)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Try
            dbConnection.Open()
            SaveAuditLogComment(dbCommand, columnId, columnDesc, empCd)

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

    End Sub

    ''' <summary>
    ''' Inserts a record to the CRM.AUDIT_LOG table with details passed in
    ''' If the command passed in, is part of a single transaction, the data will not be
    ''' automatically saved in the database, rather is only queued in the command object 
    ''' passed in, and the caller is in charge of doing the commit or rollback.
    ''' </summary>
    ''' <param name="dbCommand">the database command to be used </param>
    ''' <param name="columnId">the column ID to be saved</param>
    ''' <param name="columnDesc">the column DESCRIPTION to be saved</param>
    ''' <param name="empCd">the current employee code</param>
    Public Sub SaveAuditLogComment(ByRef dbCommand As OracleCommand,
                                   ByVal columnId As String,
                                   ByVal columnDesc As String,
                                   ByVal empCd As String)
        Dim sql As String
        sql = "INSERT INTO AUDIT_LOG (AUDIT_COLUMN_ID, AUDIT_COLUMN_DESC, AUDIT_USER) VALUES "
        sql = sql & "(:AUDIT_COLUMN_ID, :AUDIT_COLUMN_DESC, :AUDIT_USER) "
        dbCommand.CommandText = sql

        dbCommand.Parameters.Add(":AUDIT_COLUMN_ID", OracleType.VarChar)
        dbCommand.Parameters.Add(":AUDIT_COLUMN_DESC", OracleType.VarChar)
        dbCommand.Parameters.Add(":AUDIT_USER", OracleType.VarChar)

        dbCommand.Parameters(":AUDIT_COLUMN_ID").Value = columnId
        dbCommand.Parameters(":AUDIT_COLUMN_DESC").Value = columnDesc
        dbCommand.Parameters(":AUDIT_USER").Value = empCd

        dbCommand.ExecuteNonQuery()
        dbCommand.Parameters.Clear()
    End Sub

    ''' <summary>
    ''' Retrieves the list of all companies setup in the system with their code and descriptions.
    ''' </summary>
    ''' <returns>a list of all the companies defined in the system.</returns>
    Public Function GetCompanies() As DataSet

        Dim coDataSet As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String = "SELECT co_cd, des, co_cd || ' - ' || des as full_desc FROM co ORDER BY co_cd"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(coDataSet)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return coDataSet

    End Function

    ''' <summary>
    ''' Retrieves the company password for the passed-in company code.
    ''' </summary>
    ''' <returns>the company password.</returns>
    Public Function GetCompanyPwd(ByVal coCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim sql As String = "SELECT CO.PASSWORD FROM CO WHERE CO_CD=:CO_CD"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":CO_CD", OracleType.VarChar)
            dbCommand.Parameters(":CO_CD").Value = UCase(coCd)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


            If (dbReader.Read()) Then
                rtnVal = dbReader.Item("PASSWORD").ToString.Trim
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return rtnVal

    End Function

    ''' <summary>
    ''' Retrieves the employee details  based on the user intials.
    ''' </summary>
    ''' <param name="userInit">the user intials to use to get employee info</param>
    ''' <returns>the employee object with all the data</returns>
    Public Function GetEmployeeInfo(ByVal userInit As String) As EmployeeDtc

        Dim employee As EmployeeDtc = Nothing
        Dim sql As String = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD, SKIP_DATA_SECURITY, PRG "
        sql += "FROM EMP WHERE EMP_INIT= :EMP_INIT AND TERMDATE IS NULL"

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            'dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":EMP_INIT", OracleType.VarChar)
            dbCommand.Parameters(":EMP_INIT").Value = UCase(userInit)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


            If (dbReader.Read()) Then
                employee = New EmployeeDtc()
                employee.fName = dbReader.Item("FNAME").ToString.Trim
                employee.lName = dbReader.Item("LNAME").ToString.Trim
                employee.cd = dbReader.Item("EMP_CD").ToString.Trim
                employee.init = userInit
                employee.homeStore = dbReader.Item("HOME_STORE_CD").ToString.Trim
                employee.skipDataSecurity = StringUtils.ResolveYNToBoolean(dbReader.Item("SKIP_DATA_SECURITY").ToString.Trim)
                employee.skipSecurity = StringUtils.ResolveYNToBoolean(dbReader.Item("PRG").ToString.Trim)
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return employee
    End Function

    ''' <summary>
    ''' Returns the employee code for the employee initials passed in
    ''' </summary>
    ''' <param name="empInit">the employee initials to retrieve the emp code for</param>
    ''' <returns>the corresponding Employee code</returns>
    Public Function GetEmployeeCode(ByVal empInit As String) As String

        Dim empCd As String = String.Empty
        Dim sql As String = "SELECT emp_cd FROM emp WHERE emp_init = :empInit"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":empInit", OracleType.VarChar)
            dbCommand.Parameters(":empInit").Value = empInit.ToUpper

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                empCd = dbReader("EMP_CD").ToString
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return empCd

    End Function

    ''' <summary>
    ''' Retrieves all the cash drawers for the specified store.
    ''' </summary>
    ''' <param name="storeCd">the store for which to get cash drawers</param>
    ''' <returns>a dataset containing all the  cash drawers.</returns>
    Public Function GetCashDrawers(ByVal storeCd As String) As DataSet

        Dim ds As New DataSet
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As String = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE STORE_CD=:STORE_CD order by csh_dwr_cd"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":STORE_CD", OracleType.VarChar)
            dbCommand.Parameters(":STORE_CD").Value = storeCd

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(ds)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return ds

    End Function

    ''' <summary>
    ''' Returns the company code for the passed-in store.
    ''' </summary>
    ''' <param name="storeCd">the store for which to get the company code</param>
    ''' <returns>the company code</returns>
    Public Function GetCompanyCd(ByVal storeCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim sql As String = "SELECT CO_CD FROM STORE WHERE STORE_CD=:STORE_CD"
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":STORE_CD", OracleType.VarChar)
            dbCommand.Parameters(":STORE_CD").Value = UCase(storeCd)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


            If (dbReader.Read()) Then
                rtnVal = dbReader.Item("CO_CD").ToString.Trim
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return rtnVal

    End Function

    ''' <summary>
    ''' Determines whether the employee based on the passed-in password and date is 
    ''' a valid 'approver' within the effective and end date for that emp type. If 
    ''' successful, the emloyee code of the approver is returned.
    ''' </summary>
    ''' <param name="empPass">emp password</param>
    ''' <param name="empEffDt">the effective date to use for validation</param>
    ''' <returns>the emp_cd of the 'approver' </returns>
    Public Function GetApprover(ByVal empPass As String, ByVal empEffDt As Date) As String

        Dim rtnVal As String = ""

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbReader As OracleDataReader
        Dim sql As New StringBuilder

        sql.Append(" SELECT EMP.EMP_CD ")
        sql.Append("  FROM  EMP$EMP_TP T, EMP   ")
        sql.Append(" WHERE T.EMP_CD = EMP.EMP_CD   ")
        sql.Append("     AND EMP_TP_CD ='APP'  ")
        sql.Append("     AND APP_PASS = :EMP_PASS ")
        sql.Append("     AND TO_DATE(:EMP_EFF_DT, 'mm/dd/RRRR') BETWEEN EFF_DT AND END_DT")

        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(":EMP_PASS", OracleType.VarChar)
            dbCommand.Parameters(":EMP_PASS").Value = empPass.ToUpper()
            dbCommand.Parameters.Add(":EMP_EFF_DT", OracleType.VarChar)
            dbCommand.Parameters(":EMP_EFF_DT").Value = FormatDateTime(empEffDt, DateFormat.ShortDate)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read) Then
                rtnVal = dbReader.Item("EMP_CD").ToString
            End If
        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return rtnVal
    End Function

    ' ===================================================================================
    ' =============================== PRIVATE Section ===================================
    ' ===================================================================================

    ''' <summary>
    ''' Finds out if the passed in employee code has the privilege to skip data security or not.
    ''' </summary>
    ''' <param name="empCd">the employee code to find out if data security should be skipped</param>
    ''' <returns>true, if data security should be skipped; false otherwise</returns>
    Private Function SkipDataSecurity(ByVal empCd As String) As Boolean

        Dim skipSecurity As Boolean = False
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT SKIP_DATA_SECURITY FROM EMP WHERE EMP_CD = :empCd "

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":empCd", OracleType.VarChar)
            dbCommand.Parameters(":empCd").Value = UCase(empCd)

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read AndAlso "Y".Equals(dbReader.Item("SKIP_DATA_SECURITY").ToString)) Then
                skipSecurity = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try

        Return skipSecurity
    End Function

End Class

