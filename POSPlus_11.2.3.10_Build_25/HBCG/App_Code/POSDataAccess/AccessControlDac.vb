﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

<Serializable()>
Public Class AccessControlDac
    Inherits BaseDac

    ''' <summary>
    ''' Method is written to add the record(request) to the table
    ''' </summary>
    ''' <param name="originCode">Screen name</param>
    ''' <param name="docType">Document type</param>
    ''' <param name="docValue">Document value</param>
    ''' <param name="employeeCode">Employee code</param>
    ''' <param name="sessionID">Session id</param>
    ''' <param name="applicationType">Application Type</param>
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>
    Public Sub InsertPageRequest(ByVal originCode As String, ByVal docType As String, ByVal docValue As String, ByVal employeeCode As String, ByVal sessionID As String, ByVal applicationType As String, ByVal desc As String)
        Dim sql As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim recordCount As Integer = 0

        Try
            ' Open Connection
            dbConnection.Open()

            sql = "INSERT INTO AUDIT_LOCK2LOG (APP_TYPE, SESSION_ID, EMP_CD, DOC_TYPE, DOC_VALUE, ORIGIN_CD, LOGON_TIME, DES) VALUES (:APP_TYPE, :SESSION_ID, :EMP_CD, :DOC_TYPE, :DOC_VALUE, :ORIGIN_CD, SYSDATE, :DES) RETURNING LOGON_TIME INTO :OUT_LOGON_TIME "
            dbCommand.CommandText = sql.ToString

            dbCommand.Parameters.Add(":APP_TYPE", GetDbStringValue(applicationType))
            dbCommand.Parameters.Add(":SESSION_ID", GetDbStringValue(sessionID.Trim()))
            dbCommand.Parameters.Add(":EMP_CD", GetDbStringValue(employeeCode))
            dbCommand.Parameters.Add(":DOC_TYPE", GetDbStringValue(docType))
            dbCommand.Parameters.Add(":DOC_VALUE", GetDbStringValue(docValue.Trim()))
            dbCommand.Parameters.Add(":ORIGIN_CD", GetDbStringValue(originCode))
            dbCommand.Parameters.Add(":DES", GetDbStringValue(desc))
            dbCommand.Parameters.Add(":OUT_LOGON_TIME", OracleType.DateTime).Direction = ParameterDirection.Output
            dbCommand.Parameters(":OUT_LOGON_TIME").Value = System.DBNull.Value

            recordCount = dbCommand.ExecuteNonQuery()
            HttpContext.Current.Session("LOGON_TIME") = dbCommand.Parameters(":OUT_LOGON_TIME").Value

            If recordCount >= 1 Then
                HttpContext.Current.Session("provideAccess") = 0
            Else
                HttpContext.Current.Session("provideAccess") = 1
            End If
            dbCommand.Parameters.Clear()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Method is written to verify whether the user has the access to this page or not
    ''' </summary>
    ''' <param name="originCode">Screen name</param>
    ''' <param name="docType">Document type</param>
    ''' <param name="docValue">Document value</param>
    ''' <param name="employeeCode">Employee code</param>
    ''' <param name="sessionID">Session id</param>        
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>    
    Public Sub VerifyRequest(ByVal originCode As String, ByVal docType As String, ByVal docValue As String, ByRef employeeCode As String, ByVal sessionID As String, ByVal applicationType As String, Optional ByVal desc As String = "")
        Dim dbConnection As OracleConnection = GetConnection()
        Dim sql As String = "SELECT EMP_CD, DOC_VALUE, SESSION_ID, LOGON_TIME FROM AUDIT_LOCK2LOG WHERE DOC_VALUE= :DOC_VALUE "
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim MyDataReader As OracleDataReader = Nothing

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(New OracleParameter(":DOC_VALUE", GetDbStringValue(docValue)))
            Dim currentDate As String = HttpContext.Current.Session("LOGON_TIME")
            MyDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If MyDataReader.HasRows Then
                If MyDataReader.Read() Then
                    Dim accessID As String = String.Empty
                    Dim logonTime As String = String.Empty
                    accessID = Convert.ToString(MyDataReader("SESSION_ID"))
                    logonTime = Convert.ToString(MyDataReader("LOGON_TIME"))
                    If (accessID.Trim().Equals(sessionID.Trim())) AndAlso (logonTime.Equals(currentDate)) Then
                        HttpContext.Current.Session("provideAccess") = 0
                    Else
                        HttpContext.Current.Session("provideAccess") = 1
                        employeeCode = MyDataReader("EMP_CD")
                    End If
                End If
            Else
                InsertPageRequest(originCode, docType, docValue, Convert.ToString(HttpContext.Current.Session("EMP_CD")), Convert.ToString(sessionID), applicationType, desc)
            End If
            dbConnection.Close()
        Catch ex As Exception
            'Throw
            ' Daniela prevent logout
        Finally
            CloseResources(dbConnection, dbCommand)
            MyDataReader.Close()
        End Try
    End Sub


    Private Sub GetCurrentTime(ByVal docValue As String, ByVal sessionID As String)
        Dim dbConnection As OracleConnection = GetConnection()
        Dim sql As String = "SELECT LOGON_TIME FROM AUDIT_LOCK2LOG WHERE DOC_VALUE= :DOC_VALUE AND SESSION_ID = :SESSION_ID "
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(New OracleParameter(":DOC_VALUE", GetDbStringValue(docValue)))
            dbCommand.Parameters.Add(New OracleParameter(":SESSION_ID", GetDbStringValue(Convert.ToString(sessionID))))
            Dim currentTime As Object = dbCommand.ExecuteScalar
            If (Not IsNothing(currentTime)) Then
                HttpContext.Current.Session("LOGON_TIME") = currentTime
            End If
            dbConnection.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Deletes the document in AUDIT_LOCK2LOG table
    ''' </summary>
    ''' <param name="docValue">Document value</param>    
    ''' <param name="sessionID">Session id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteRequest(ByVal docValue As String, ByVal sessionID As String) As Integer
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As String = String.Empty
        Dim recordCount As Integer

        Try
            dbConnection.Open()
            sql = "DELETE FROM AUDIT_LOCK2LOG WHERE DOC_VALUE = :DOC_VALUE AND SESSION_ID = :SESSION_ID "
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(New OracleParameter(":DOC_VALUE", GetDbStringValue(docValue)))
            dbCommand.Parameters.Add(New OracleParameter(":SESSION_ID", GetDbStringValue(sessionID)))
            recordCount = dbCommand.ExecuteNonQuery()
            dbCommand.Parameters.Clear()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return recordCount
    End Function

    ''' <summary>
    ''' To delete the records when user logs out from the application
    ''' </summary>
    ''' <param name="sessionIDs">session id's</param>
    ''' <returns>delete record count</returns>
    ''' <remarks></remarks>
    Public Function DeleteRequest(ByVal sessionIDs As String) As Integer
        Dim dbConnection As OracleConnection = GetConnection()
        Dim sql As String = String.Empty
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim recordCount As Integer

        Try
            dbConnection.Open()
            sql = "DELETE FROM AUDIT_LOCK2LOG WHERE SESSION_ID IN(" & sessionIDs & ")"
            dbCommand.CommandText = sql.ToString
            recordCount = dbCommand.ExecuteNonQuery()
            dbCommand.Parameters.Clear()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return recordCount
    End Function

    ''' <summary>
    ''' Method written to update the time stamp when user does the modifications on the doc
    ''' </summary>
    ''' <param name="docValue">Document value</param>    
    ''' <param name="sessionID">Session id</param>
    ''' <param name="desc">Description</param>
    ''' <remarks></remarks>
    'Public Sub UpdateRequest(ByVal docValue As String, ByVal sessionID As String, Optional desc As String = "")
    Public Function UpdateRequest(ByVal docValue As String, ByVal sessionID As String, Optional desc As String = "") As Boolean
        Dim dbConnection As OracleConnection = GetConnection()
        Dim sql As String = String.Empty
        Dim result As Boolean = False
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim MyDataReader As OracleDataReader = Nothing

        Try
            dbConnection.Open()
            'If order is finalized OR VOIDED, do not update
            sql = "SELECT STAT_CD FROM SO WHERE DEL_DOC_NUM = :DOC_VALUE and STAT_CD in('F','V') "
            dbCommand.CommandText = sql.ToString
            dbCommand.Parameters.Add(New OracleParameter(":DOC_VALUE", GetDbStringValue(docValue)))
            MyDataReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If Not MyDataReader.HasRows Then
                dbCommand.Parameters.Clear()
                sql = "UPDATE AUDIT_LOCK2LOG SET LOGON_TIME = SYSDATE, DES=:DES WHERE DOC_VALUE = :DOC_VALUE AND SESSION_ID = :SESSION_ID AND LOGON_TIME =  :LOGON_TIME  RETURNING  LOGON_TIME INTO :OUT_LOGON_TIME"
                dbCommand.CommandText = sql.ToString
                dbCommand.Parameters.Add(New OracleParameter(":DOC_VALUE", GetDbStringValue(docValue)))
                dbCommand.Parameters.Add(New OracleParameter(":SESSION_ID", GetDbStringValue(sessionID)))
                dbCommand.Parameters.Add(New OracleParameter(":DES", GetDbStringValue(desc)))
                dbCommand.Parameters.Add(New OracleParameter(":LOGON_TIME", HttpContext.Current.Session("LOGON_TIME")))
                dbCommand.Parameters.Add(":OUT_LOGON_TIME", OracleType.DateTime).Direction = ParameterDirection.Output
                Dim updateRowCount As String = dbCommand.ExecuteNonQuery()
                If (updateRowCount = 0) Then
                    result = False
                Else
                    result = True
                    HttpContext.Current.Session("LOGON_TIME") = dbCommand.Parameters(":OUT_LOGON_TIME").Value
                End If
                dbCommand.Parameters.Clear()
            Else
                'This will set to true since the order is finalized/voided, no entry would be found in lock table
                result = True

            End If
        Catch ex As Exception
            ' Daniela prevent yellow traingle
            'Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Convert the string value passed in to the proper format, as required by the database
    ''' </summary>
    ''' <param name="value">the string value to be checked/formatted</param>
    ''' <returns>the value ready for storage in the database</returns>
    Private Function GetDbStringValue(value As String) As Object
        If String.IsNullOrEmpty(value) Then
            Return DBNull.Value
        Else
            Return (value.Trim())
        End If
    End Function

End Class
