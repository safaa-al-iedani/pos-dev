﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data.OracleClient
Imports StringUtils

Public Class CustomerDac
    Inherits BaseDac

    ''' <summary>
    ''' Reads from the TEMP_ITM table the customer zip code, that corresponds to the customer and
    ''' session id values passed in
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custCd">the customer code to get the zip code for</param>
    ''' <returns>the ZIP code value, or an empty string if none found</returns>
    Public Function GetCustomerZip(ByVal sessionId As String, ByVal custCd As String)

        Dim theZip As String
        Dim sql As String = "SELECT * FROM CUST_INFO WHERE Session_ID = :sessionId AND CUST_CD = :custCd "
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand(sql, dbConnection)
        Dim dbReader As OracleDataReader

        Try
            dbConnection.Open()
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters.Add(":custCd", OracleType.VarChar)

            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.Parameters(":custCd").Value = custCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                theZip = dbReader.Item("ZIP").ToString
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return theZip

    End Function

    ''' <summary>
    ''' Retrieves the CUSTOMER details from the AR.CUST table, for the customer
    ''' that matches the customer code passed in.
    ''' </summary>
    ''' <param name="custCd">the customer code</param>
    ''' <returns>the CUSTOMER details or an empty object if no match is found</returns>
    Public Function GetCustomer(ByVal custCd As String) As CustomerDtc

        Dim theCustomer As CustomerDtc = New CustomerDtc()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String
        sql = "SELECT CUST_CD, CUST_TP_CD, FNAME, LNAME, EMAIL_ADDR, CORP_NAME, HOME_PHONE, BUS_PHONE, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD "
        sql += "FROM CUST WHERE cust_cd = :custCd"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":custCd", OracleType.VarChar)
            dbCommand.Parameters(":custCd").Value = custCd

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                theCustomer.custCd = dbReader.Item("CUST_CD").ToString.Trim
                theCustomer.custTp = dbReader.Item("CUST_TP_CD").ToString.Trim
                theCustomer.fname = dbReader.Item("FNAME").ToString.Trim
                theCustomer.lname = dbReader.Item("LNAME").ToString.Trim
                theCustomer.email = dbReader.Item("EMAIL_ADDR").ToString.Trim
                theCustomer.corpName = dbReader.Item("CORP_NAME").ToString.Trim

                theCustomer.hphone = FormatPhoneNumber(dbReader.Item("HOME_PHONE").ToString.Trim)
                theCustomer.bphone = FormatPhoneNumber(dbReader.Item("BUS_PHONE").ToString.Trim)
                theCustomer.addr1 = dbReader.Item("ADDR1").ToString.Trim
                theCustomer.addr2 = dbReader.Item("ADDR2").ToString.Trim
                theCustomer.city = dbReader.Item("CITY").ToString.Trim
                theCustomer.state = dbReader.Item("ST_CD").ToString.Trim
                theCustomer.zip = dbReader.Item("ZIP_CD").ToString.Trim
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return theCustomer

    End Function


    ''' <summary>
    ''' Retrieves the CUSTOMER details from the CRM.CUST_INFO table, for the
    ''' customer that matches the customer code and the session id passed in.
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custCd">the customer code</param>
    ''' <returns>the CUSTOMER details or an empty object if no match is found</returns>
    Public Function GetCustomer(ByVal sessionId As String, ByVal custCd As String) As CustomerDtc

        Dim theCustomer As CustomerDtc = New CustomerDtc()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT * FROM CUST_INFO WHERE Session_ID = :sessionId AND CUST_CD = :custCd"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters.Add(":custCd", OracleType.VarChar)

            dbCommand.Parameters(":sessionId").Value = sessionId
            dbCommand.Parameters(":custCd").Value = custCd

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                theCustomer.fname = dbReader.Item("FNAME").ToString.Trim
                theCustomer.lname = dbReader.Item("LNAME").ToString.Trim
                theCustomer.addr1 = dbReader.Item("ADDR1").ToString.Trim
                theCustomer.addr2 = dbReader.Item("ADDR2").ToString.Trim
                theCustomer.city = dbReader.Item("CITY").ToString.Trim
                theCustomer.state = dbReader.Item("ST").ToString.Trim
                theCustomer.zip = dbReader.Item("ZIP").ToString.Trim
                theCustomer.hphone = FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                theCustomer.bphone = FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)

                theCustomer.billFname = dbReader.Item("BILL_FNAME").ToString.Trim
                theCustomer.billLname = dbReader.Item("BILL_LNAME").ToString.Trim
                theCustomer.billAddr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                theCustomer.billAddr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                theCustomer.billCity = dbReader.Item("BILL_CITY").ToString.Trim
                theCustomer.billState = dbReader.Item("BILL_ST").ToString.Trim
                theCustomer.billZip = dbReader.Item("BILL_ZIP").ToString.Trim
                theCustomer.billHphone = FormatPhoneNumber(dbReader.Item("BILL_HPHONE").ToString.Trim)
                theCustomer.billBphone = FormatPhoneNumber(dbReader.Item("BILL_BPHONE").ToString.Trim)

                theCustomer.email = dbReader.Item("EMAIL").ToString.Trim
                theCustomer.custCd = dbReader.Item("CUST_CD").ToString.Trim
                theCustomer.custTp = dbReader.Item("CUST_TP").ToString.Trim
                theCustomer.custTp = dbReader.Item("CUST_TP_CD").ToString.Trim
                theCustomer.corpName = dbReader.Item("CORP_NAME").ToString.Trim
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return theCustomer

    End Function


    ''' <summary>
    ''' Gets the drivers license details that match the session id from the CRM.PAYMENT table
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <returns>the Driver License details; or an empty object if no matches found</returns>
    Public Function GetDriverLicense(ByVal sessionId As String) As DriverLicenseDtc

        Dim theDriverLicense As DriverLicenseDtc = New DriverLicenseDtc()
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT DL_STATE, DL_LICENSE_NO FROM PAYMENT WHERE SessionID = :sessionId AND DL_STATE IS NOT NULL AND DL_LICENSE_NO IS NOT NULL"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.Parameters.Add(":sessionId", OracleType.VarChar)
            dbCommand.Parameters(":sessionId").Value = sessionId

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If (dbReader.Read()) Then
                theDriverLicense.state = dbReader.Item("DL_STATE").ToString.Trim
                theDriverLicense.number = dbReader.Item("DL_LICENSE_NO").ToString.Trim
            End If

            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return theDriverLicense

    End Function

    ''' <summary>
    ''' Updates the E1 CUST table with the details passed in
    ''' </summary>
    ''' <param name="customer">customer details as obtained from the CRM.CUST_INFO table</param>
    ''' <param name="dLicense">the driver license details for this customer</param>
    ''' <param name="taxExemptCd">the tax-exempt code for this customer</param>
    ''' <param name="taxExemptId">the tax-exempt ID for this customer</param>
    ''' <remarks></remarks>
    Public Sub UpdateCustomer(ByVal customer As CustomerDtc,
                              ByVal dLicense As DriverLicenseDtc,
                              ByVal taxExemptCd As String,
                              ByVal taxExemptId As String)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim dbSql As String = ""

        Try
            dbConnection.Open()
            dbSql = "UPDATE CUST SET DL_NUMBER='" & dLicense.number & "', DL_ST_CD='" & dLicense.state & "'"
            If taxExemptCd & "" <> "" AndAlso taxExemptId & "" <> "" Then
                dbSql = dbSql & ", TET_ID#='" & taxExemptId & "', TET_CD='" & taxExemptCd & "'"
            End If
            If Not String.IsNullOrEmpty(customer.email) Then dbSql = dbSql & ", EMAIL_ADDR='" & customer.email & "'"
            If Not String.IsNullOrEmpty(customer.corpName) Then dbSql = dbSql & ", CORP_NAME='" & Replace(customer.corpName, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billFname) Then dbSql = dbSql & ", FNAME='" & Replace(customer.billFname, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billLname) Then dbSql = dbSql & ", LNAME='" & Replace(customer.billLname, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billAddr1) Then dbSql = dbSql & ", ADDR1='" & Replace(customer.billAddr1, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billAddr2) Then dbSql = dbSql & ", ADDR2='" & Replace(customer.billAddr2, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billCity) Then dbSql = dbSql & ", CITY='" & Replace(customer.billCity, "'", "''") & "'"
            If Not String.IsNullOrEmpty(customer.billState) Then dbSql = dbSql & ", ST_CD='" & customer.billState & "'"
            If Not String.IsNullOrEmpty(customer.billZip) Then dbSql = dbSql & ", ZIP_CD='" & customer.billZip & "'"
            dbSql = dbSql & " WHERE CUST_CD='" & customer.custCd & "'"
            dbSql = UCase(dbSql)
            dbCommand.CommandText = dbSql
            dbCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub

    ''' <summary>
    ''' Inserts a CUSTOMER record in the AR.CUST table, using the details passed in
    ''' and returns the updated customer details, which includes the newly customer 
    ''' code created for this new customer
    ''' </summary>
    ''' <param name="custInfo">the customer details to create a record with</param>
    ''' <returns>the customer details updated after the insert operation</returns>
    Public Function InsertCustomer(ByVal custInfo As CustomerDtc) As CustomerDtc

        Dim custCd As String = String.Empty
        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As String
        sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
        sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

        Try
            ' the customer code will get created within a single transaction, this way only
            ' if operations succeed, then the customer code is issued
            dbConnection.Open()
            dbCommand.Transaction = dbConnection.BeginTransaction

            custInfo.custCd = String.Empty  'GetCustomerCode()

            dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
            dbCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
            dbCommand.Parameters.Add(":FNAME", OracleType.VarChar)
            dbCommand.Parameters.Add(":LNAME", OracleType.VarChar)
            dbCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
            dbCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
            dbCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
            dbCommand.Parameters.Add(":CITY", OracleType.VarChar)
            dbCommand.Parameters.Add(":ST", OracleType.VarChar)
            dbCommand.Parameters.Add(":ZIP", OracleType.VarChar)
            dbCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
            dbCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
            dbCommand.Parameters.Add(":EMAIL", OracleType.VarChar)

            dbCommand.Parameters(":CUST_CD").Value = custInfo.custCd.Trim
            dbCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
            dbCommand.Parameters(":FNAME").Value = UCase(custInfo.fname)
            dbCommand.Parameters(":LNAME").Value = UCase(custInfo.lname)
            dbCommand.Parameters(":CUST_TP").Value = custInfo.custTpCd
            dbCommand.Parameters(":ADDR1").Value = UCase(custInfo.addr1)
            dbCommand.Parameters(":ADDR2").Value = UCase(custInfo.addr2)
            dbCommand.Parameters(":CITY").Value = UCase(custInfo.city)
            dbCommand.Parameters(":ST").Value = UCase(custInfo.state)
            dbCommand.Parameters(":ZIP").Value = custInfo.zip
            dbCommand.Parameters(":HPHONE").Value = custInfo.hphone
            dbCommand.Parameters(":BPHONE").Value = custInfo.bphone
            dbCommand.Parameters(":EMAIL").Value = UCase(custInfo.email)

            dbCommand.CommandText = sql
            dbCommand.ExecuteNonQuery()
            dbCommand.Transaction.Commit()

        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
        Return custInfo

    End Function


    ''' <summary>
    ''' Inserts a CUSTOMER record in the CRM.CUST_INFO table, using the details passed in
    ''' </summary>
    ''' <param name="sessionId">the current session</param>
    ''' <param name="custInfo">the customer details to create a record with</param>
    Public Sub InsertCustomer(ByVal sessionId As String, ByVal custInfo As CustomerDtc)

        Dim dbConnection As OracleConnection = GetConnection()
        Dim dbCommand As OracleCommand = GetCommand("", dbConnection)
        Dim sql As String
        sql = "INSERT INTO CUST_INFO (SESSION_ID, CUST_CD, CUST_TP, CUST_TP_CD, FNAME, LNAME, ADDR1, ADDR2, CITY, ST, ZIP, HPHONE, BPHONE, EMAIL) "
        sql = sql & "VALUES("
        sql = sql & "'" & sessionId.Trim & "',"
        sql = sql & "'" & custInfo.custCd & "',"
        sql = sql & "'I',"
        sql = sql & "'" & custInfo.custTpCd & "',"
        sql = sql & "'" & UCase(custInfo.fname) & "',"
        sql = sql & "'" & UCase(custInfo.lname) & "',"
        sql = sql & "'" & UCase(custInfo.addr1) & "',"
        sql = sql & "'" & UCase(custInfo.addr2) & "',"
        sql = sql & "'" & UCase(custInfo.city) & "',"
        sql = sql & "'" & UCase(custInfo.state) & "',"
        sql = sql & "'" & UCase(custInfo.zip) & "',"
        sql = sql & "'" & UCase(custInfo.hphone) & "',"
        sql = sql & "'" & UCase(custInfo.bphone) & "',"
        sql = sql & "'" & UCase(custInfo.email) & "')"

        Try
            dbConnection.Open()
            dbCommand.CommandText = sql
            dbCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            CloseResources(dbConnection, dbCommand)
        End Try
    End Sub


End Class
