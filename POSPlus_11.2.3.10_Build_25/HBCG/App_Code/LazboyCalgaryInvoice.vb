Public Class LazboyCalgaryInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Dim comments_processed As Boolean
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable7 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable8 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableRow18 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_cod As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vend As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_financing As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xr_disc_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payment_holder As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_loc_cd As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_store_cd As DevExpress.XtraReports.UI.XRTableCell
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "LazboyCalgaryInvoice.resx"
        Me.xr_financing = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_disc_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTable7 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_vend = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_store_cd = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_loc_cd = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell43 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_des = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell48 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_tp_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_wr_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTable8 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_tax = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_balance = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableRow18 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_cod = New DevExpress.XtraReports.UI.XRTableCell
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_corp_name = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bill_full_name = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_corp_name = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xr_payment_holder = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xr_financing
        '
        Me.xr_financing.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_financing.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_financing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_financing.Name = "xr_financing"
        Me.xr_financing.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_financing.StylePriority.UseBorderColor = False
        Me.xr_financing.StylePriority.UseBorders = False
        Me.xr_financing.StylePriority.UseFont = False
        Me.xr_financing.StylePriority.UseTextAlignment = False
        Me.xr_financing.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_financing.Weight = 1.0899999999999999
        '
        'xr_payments
        '
        Me.xr_payments.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_payments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.Black
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(3.999964!, 0.5776723!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(86.33337!, 15.7917!)
        Me.xr_payments.StylePriority.UseBorders = False
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.StylePriority.UseTextAlignment = False
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_disc_desc, Me.xrTable7, Me.xr_disc_amt, Me.xr_void_flag})
        Me.Detail.HeightF = 55.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_disc_desc
        '
        Me.xr_disc_desc.CanShrink = True
        Me.xr_disc_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_desc.ForeColor = System.Drawing.Color.Red
        Me.xr_disc_desc.LocationFloat = New DevExpress.Utils.PointFloat(106.7083!, 36.45833!)
        Me.xr_disc_desc.Multiline = True
        Me.xr_disc_desc.Name = "xr_disc_desc"
        Me.xr_disc_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_desc.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_disc_desc.SizeF = New System.Drawing.SizeF(641.0!, 17.0!)
        Me.xr_disc_desc.StylePriority.UseFont = False
        Me.xr_disc_desc.StylePriority.UseForeColor = False
        Me.xr_disc_desc.StylePriority.UseTextAlignment = False
        Me.xr_disc_desc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrTable7
        '
        Me.xrTable7.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTable7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable7.Name = "xrTable7"
        Me.xrTable7.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow12, Me.xrTableRow19})
        Me.xrTable7.SizeF = New System.Drawing.SizeF(765.0!, 36.45833!)
        Me.xrTable7.StylePriority.UseBackColor = False
        Me.xrTable7.StylePriority.UseTextAlignment = False
        Me.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow12
        '
        Me.xrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_qty, Me.xr_sku, Me.xr_vend, Me.xr_vsn, Me.xr_store_cd, Me.xr_loc_cd, Me.xr_ret, Me.xr_ext})
        Me.xrTableRow12.Name = "xrTableRow12"
        Me.xrTableRow12.Weight = 1
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.Transparent
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_qty.Weight = 0.36965640599462607
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.Transparent
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sku.Weight = 0.39454829013305726
        '
        'xr_vend
        '
        Me.xr_vend.BackColor = System.Drawing.Color.Transparent
        Me.xr_vend.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vend.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_vend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vend.Name = "xr_vend"
        Me.xr_vend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_vend.StylePriority.UseBackColor = False
        Me.xr_vend.StylePriority.UseBorderColor = False
        Me.xr_vend.StylePriority.UseBorders = False
        Me.xr_vend.StylePriority.UseFont = False
        Me.xr_vend.StylePriority.UseTextAlignment = False
        Me.xr_vend.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vend.Weight = 0.19802817391534758
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.Transparent
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vsn.Weight = 1.2538036678367113
        '
        'xr_store_cd
        '
        Me.xr_store_cd.BackColor = System.Drawing.Color.Transparent
        Me.xr_store_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_cd.Name = "xr_store_cd"
        Me.xr_store_cd.StylePriority.UseBackColor = False
        Me.xr_store_cd.StylePriority.UseFont = False
        Me.xr_store_cd.Weight = 0.10842846197819406
        '
        'xr_loc_cd
        '
        Me.xr_loc_cd.BackColor = System.Drawing.Color.Transparent
        Me.xr_loc_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc_cd.Name = "xr_loc_cd"
        Me.xr_loc_cd.StylePriority.UseBackColor = False
        Me.xr_loc_cd.StylePriority.UseFont = False
        Me.xr_loc_cd.Weight = 0.12476237973964532
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.Transparent
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ret.Weight = 0.33321148883293239
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.Transparent
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_ext.Weight = 0.33321129861139442
        '
        'xrTableRow19
        '
        Me.xrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell27, Me.xrTableCell43, Me.xr_des, Me.xrTableCell48})
        Me.xrTableRow19.Name = "xrTableRow19"
        Me.xrTableRow19.Weight = 1
        '
        'xrTableCell27
        '
        Me.xrTableCell27.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell27.Name = "xrTableCell27"
        Me.xrTableCell27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell27.StylePriority.UseBackColor = False
        Me.xrTableCell27.Weight = 0.125
        '
        'xrTableCell43
        '
        Me.xrTableCell43.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell43.Name = "xrTableCell43"
        Me.xrTableCell43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell43.StylePriority.UseBackColor = False
        Me.xrTableCell43.Weight = 0.83723280944027423
        '
        'xr_des
        '
        Me.xr_des.BackColor = System.Drawing.Color.Transparent
        Me.xr_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_des.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_des.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_des.Name = "xr_des"
        Me.xr_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_des.StylePriority.UseBackColor = False
        Me.xr_des.StylePriority.UseBorderColor = False
        Me.xr_des.StylePriority.UseBorders = False
        Me.xr_des.StylePriority.UseFont = False
        Me.xr_des.StylePriority.UseTextAlignment = False
        Me.xr_des.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_des.Weight = 0.856517190559726
        '
        'xrTableCell48
        '
        Me.xrTableCell48.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell48.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell48.Name = "xrTableCell48"
        Me.xrTableCell48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell48.StylePriority.UseBackColor = False
        Me.xrTableCell48.StylePriority.UseBorderColor = False
        Me.xrTableCell48.StylePriority.UseBorders = False
        Me.xrTableCell48.Weight = 1.296900167041908
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(31.62498!, 36.45833!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(14.62498!, 36.45833!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel4, Me.xr_pd, Me.xr_pu_del_dt, Me.xr_slsp1, Me.xrLabel3, Me.xr_ord_tp_cd, Me.xr_wr_dt, Me.xrLabel2, Me.xr_del_doc_num, Me.xr_cust_cd})
        Me.PageHeader.HeightF = 104.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel4
        '
        Me.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel4.CanGrow = False
        Me.xrLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(423.1137!, 79.00003!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(91.88614!, 17.00001!)
        Me.xrLabel4.StylePriority.UseBorders = False
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseTextAlignment = False
        Me.xrLabel4.Text = "Delivery Date:"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.CanGrow = False
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(629.0303!, 45.00001!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(44.08331!, 17.00001!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(514.9998!, 79.00003!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseBorders = False
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xr_slsp1
        '
        Me.xr_slsp1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp1.CanGrow = False
        Me.xr_slsp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp1.LocationFloat = New DevExpress.Utils.PointFloat(514.9999!, 62.00002!)
        Me.xr_slsp1.Name = "xr_slsp1"
        Me.xr_slsp1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp1.SizeF = New System.Drawing.SizeF(158.1138!, 17.00001!)
        Me.xr_slsp1.StylePriority.UseBorders = False
        Me.xr_slsp1.StylePriority.UseFont = False
        Me.xr_slsp1.StylePriority.UseTextAlignment = False
        Me.xr_slsp1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLabel3
        '
        Me.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel3.CanGrow = False
        Me.xrLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(423.1137!, 62.00002!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(91.88614!, 17.00001!)
        Me.xrLabel3.StylePriority.UseBorders = False
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.StylePriority.UseTextAlignment = False
        Me.xrLabel3.Text = "Salesperson:"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_ord_tp_cd
        '
        Me.xr_ord_tp_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp_cd.CanGrow = False
        Me.xr_ord_tp_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_cd.LocationFloat = New DevExpress.Utils.PointFloat(559.7387!, 45.00001!)
        Me.xr_ord_tp_cd.Name = "xr_ord_tp_cd"
        Me.xr_ord_tp_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_cd.SizeF = New System.Drawing.SizeF(44.08331!, 17.00001!)
        Me.xr_ord_tp_cd.StylePriority.UseBorders = False
        Me.xr_ord_tp_cd.StylePriority.UseFont = False
        Me.xr_ord_tp_cd.StylePriority.UseTextAlignment = False
        Me.xr_ord_tp_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_wr_dt
        '
        Me.xr_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_wr_dt.CanGrow = False
        Me.xr_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(461.9887!, 45.00001!)
        Me.xr_wr_dt.Name = "xr_wr_dt"
        Me.xr_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_wr_dt.SizeF = New System.Drawing.SizeF(89.91672!, 17.00001!)
        Me.xr_wr_dt.StylePriority.UseBorders = False
        Me.xr_wr_dt.StylePriority.UseFont = False
        Me.xr_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLabel2
        '
        Me.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel2.CanGrow = False
        Me.xrLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(423.1137!, 45.00001!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(38.87503!, 17.00001!)
        Me.xrLabel2.StylePriority.UseBorders = False
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseTextAlignment = False
        Me.xrLabel2.Text = "Date:"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(544.1137!, 28.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(113.375!, 17.0!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(423.1138!, 28.00001!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(102.0001!, 17.00001!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(705.9166!, 70.70831!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(529.1138!, 139.7083!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseBorders = False
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_h_phone.CanShrink = True
        Me.xr_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 139.7083!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseBorders = False
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(555.1137!, 122.7083!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseBorders = False
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(529.1138!, 122.7083!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseBorders = False
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 122.7083!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_city.StylePriority.UseBorders = False
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 104.7083!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr2.StylePriority.UseBorders = False
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(689.9166!, 70.70831!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(672.9167!, 70.70831!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 87.70828!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr1.StylePriority.UseBorders = False
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrLabel10})
        Me.ReportFooter.HeightF = 34.00001!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_comments
        '
        Me.xr_comments.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(747.7083!, 17.0!)
        Me.xr_comments.StylePriority.UseBorders = False
        Me.xr_comments.StylePriority.UseFont = False
        '
        'xrLabel10
        '
        Me.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(342.7083!, 17.0!)
        Me.xrLabel10.StylePriority.UseBorders = False
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xrTable8
        '
        Me.xrTable8.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable8.LocationFloat = New DevExpress.Utils.PointFloat(665.6666!, 94.9617!)
        Me.xrTable8.Name = "xrTable8"
        Me.xrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow13, Me.xrTableRow14, Me.xrTableRow15, Me.xrTableRow5, Me.xrTableRow16, Me.xrTableRow3, Me.xrTableRow4, Me.xrTableRow17, Me.xrTableRow18})
        Me.xrTable8.SizeF = New System.Drawing.SizeF(90.33337!, 147.3244!)
        Me.xrTable8.StylePriority.UseBorders = False
        Me.xrTable8.StylePriority.UseFont = False
        '
        'xrTableRow13
        '
        Me.xrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_subtotal})
        Me.xrTableRow13.Name = "xrTableRow13"
        Me.xrTableRow13.Weight = 1
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseBorders = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 1.0899999999999999
        '
        'xrTableRow14
        '
        Me.xrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_del})
        Me.xrTableRow14.Name = "xrTableRow14"
        Me.xrTableRow14.Weight = 1
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseBorders = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 1.0899999999999999
        '
        'xrTableRow15
        '
        Me.xrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_tax})
        Me.xrTableRow15.Name = "xrTableRow15"
        Me.xrTableRow15.Weight = 1
        '
        'xr_tax
        '
        Me.xr_tax.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.StylePriority.UseBorderColor = False
        Me.xr_tax.StylePriority.UseBorders = False
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseTextAlignment = False
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax.Weight = 1.0899999999999999
        '
        'xrTableRow5
        '
        Me.xrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell8})
        Me.xrTableRow5.Name = "xrTableRow5"
        Me.xrTableRow5.Weight = 1
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.StylePriority.UseBorders = False
        Me.xrTableCell8.Weight = 1.0899999999999999
        '
        'xrTableRow16
        '
        Me.xrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_grand_total})
        Me.xrTableRow16.Name = "xrTableRow16"
        Me.xrTableRow16.Weight = 1
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseBorders = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 1.0899999999999999
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell7})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.StylePriority.UseBorders = False
        Me.xrTableRow3.Weight = 1
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_payments})
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Weight = 1.0899999999999999
        '
        'xrTableRow4
        '
        Me.xrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_balance})
        Me.xrTableRow4.Name = "xrTableRow4"
        Me.xrTableRow4.Weight = 1
        '
        'xr_balance
        '
        Me.xr_balance.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.StylePriority.UseBorders = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.Weight = 1.0899999999999999
        '
        'xrTableRow17
        '
        Me.xrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_financing})
        Me.xrTableRow17.Name = "xrTableRow17"
        Me.xrTableRow17.Weight = 1
        '
        'xrTableRow18
        '
        Me.xrTableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_cod})
        Me.xrTableRow18.Name = "xrTableRow18"
        Me.xrTableRow18.Weight = 1
        '
        'xr_cod
        '
        Me.xr_cod.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_cod.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cod.Name = "xr_cod"
        Me.xr_cod.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cod.StylePriority.UseBorderColor = False
        Me.xr_cod.StylePriority.UseBorders = False
        Me.xr_cod.StylePriority.UseFont = False
        Me.xr_cod.StylePriority.UseTextAlignment = False
        Me.xr_cod.Text = "0.00"
        Me.xr_cod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_cod.Weight = 1.0899999999999999
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_slsp2_hidden, Me.xr_ord_srt, Me.xr_ord_tp_hidden, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname, Me.xr_bill_corp_name, Me.xr_bill_b_phone, Me.xr_bill_h_phone, Me.xr_bill_zip, Me.xr_bill_state, Me.xr_bill_city, Me.xr_bill_addr2, Me.xr_bill_addr1, Me.xr_bill_full_name, Me.xr_full_name, Me.xr_b_phone, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_addr1, Me.xr_corp_name})
        Me.GroupHeader1.HeightF = 246.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(689.9166!, 95.70831!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(672.9167!, 95.70831!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(680.9166!, 87.70831!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_bill_corp_name
        '
        Me.xr_bill_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_corp_name.CanShrink = True
        Me.xr_bill_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 53.7083!)
        Me.xr_bill_corp_name.Name = "xr_bill_corp_name"
        Me.xr_bill_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_corp_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_corp_name.StylePriority.UseBorders = False
        Me.xr_bill_corp_name.StylePriority.UseFont = False
        '
        'xr_bill_b_phone
        '
        Me.xr_bill_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(132.9167!, 139.7083!)
        Me.xr_bill_b_phone.Name = "xr_bill_b_phone"
        Me.xr_bill_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_bill_b_phone.StylePriority.UseBorders = False
        Me.xr_bill_b_phone.StylePriority.UseFont = False
        Me.xr_bill_b_phone.Text = "xr_b_phone"
        '
        'xr_bill_h_phone
        '
        Me.xr_bill_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_h_phone.CanShrink = True
        Me.xr_bill_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 139.7083!)
        Me.xr_bill_h_phone.Name = "xr_bill_h_phone"
        Me.xr_bill_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_bill_h_phone.StylePriority.UseBorders = False
        Me.xr_bill_h_phone.StylePriority.UseFont = False
        Me.xr_bill_h_phone.Text = "xr_h_phone"
        '
        'xr_bill_zip
        '
        Me.xr_bill_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_zip.LocationFloat = New DevExpress.Utils.PointFloat(160.0!, 122.7083!)
        Me.xr_bill_zip.Name = "xr_bill_zip"
        Me.xr_bill_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_bill_zip.StylePriority.UseBorders = False
        Me.xr_bill_zip.StylePriority.UseFont = False
        Me.xr_bill_zip.Text = "xr_zip"
        '
        'xr_bill_state
        '
        Me.xr_bill_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_state.CanGrow = False
        Me.xr_bill_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_state.LocationFloat = New DevExpress.Utils.PointFloat(127.0!, 122.7083!)
        Me.xr_bill_state.Name = "xr_bill_state"
        Me.xr_bill_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_bill_state.StylePriority.UseBorders = False
        Me.xr_bill_state.StylePriority.UseFont = False
        Me.xr_bill_state.Text = "xr_state"
        '
        'xr_bill_city
        '
        Me.xr_bill_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_city.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 122.7083!)
        Me.xr_bill_city.Name = "xr_bill_city"
        Me.xr_bill_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_city.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_bill_city.StylePriority.UseBorders = False
        Me.xr_bill_city.StylePriority.UseFont = False
        '
        'xr_bill_addr2
        '
        Me.xr_bill_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr2.CanShrink = True
        Me.xr_bill_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 105.7083!)
        Me.xr_bill_addr2.Name = "xr_bill_addr2"
        Me.xr_bill_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr2.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_addr2.StylePriority.UseBorders = False
        Me.xr_bill_addr2.StylePriority.UseFont = False
        '
        'xr_bill_addr1
        '
        Me.xr_bill_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 87.70828!)
        Me.xr_bill_addr1.Name = "xr_bill_addr1"
        Me.xr_bill_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr1.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_addr1.StylePriority.UseBorders = False
        Me.xr_bill_addr1.StylePriority.UseFont = False
        '
        'xr_bill_full_name
        '
        Me.xr_bill_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_full_name.CanShrink = True
        Me.xr_bill_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_full_name.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 70.70831!)
        Me.xr_bill_full_name.Name = "xr_bill_full_name"
        Me.xr_bill_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_full_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_full_name.StylePriority.UseBorders = False
        Me.xr_bill_full_name.StylePriority.UseFont = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_full_name.CanShrink = True
        Me.xr_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 70.70831!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseBorders = False
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xr_corp_name
        '
        Me.xr_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_corp_name.CanShrink = True
        Me.xr_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(408.1138!, 53.7083!)
        Me.xr_corp_name.Name = "xr_corp_name"
        Me.xr_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_corp_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_corp_name.StylePriority.UseBorders = False
        Me.xr_corp_name.StylePriority.UseFont = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_payment_holder, Me.xrLabel1, Me.xrTable8})
        Me.PageFooter.HeightF = 273.125!
        Me.PageFooter.Name = "PageFooter"
        '
        'xr_payment_holder
        '
        Me.xr_payment_holder.LocationFloat = New DevExpress.Utils.PointFloat(529.1138!, 247.0!)
        Me.xr_payment_holder.Name = "xr_payment_holder"
        Me.xr_payment_holder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payment_holder.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xr_payment_holder.Text = "xr_payment_holder"
        Me.xr_payment_holder.Visible = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 253.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(500.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseBorders = False
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "25% RESTOCKING FEE ON RETURNS, SUBJECT TO MGMT APPROVAL"
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 14.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 48.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'LazboyCalgaryInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(51, 33, 14, 48)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "10.1"
        Me.Watermark.ImageTransparency = 209
        Me.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Zoom
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_financing.DataBindings.Add("Text", DataSource, "SO.ORIG_FI_AMT", "{0:0.00}")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_slsp1.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_corp_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CORP")
        xr_bill_corp_name.DataBindings.Add("Text", DataSource, "CUST.CORP_NAME")
        xr_bill_full_name.DataBindings.Add("Text", DataSource, "CUST.CUST_FULL_NAME")
        xr_bill_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_bill_addr2.DataBindings.Add("Text", DataSource, "CUST.ADDR2")
        xr_bill_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_bill_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_bill_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_bill_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_bill_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_store_cd.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc_cd.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_sku.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_vend.DataBindings.Add("Text", DataSource, "ITM.VE_CD")
        xr_des.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_ord_tp_cd.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        sender.text = FormatNumber(xr_qty.Text * xr_ret.Text, 2)
        subtotal = subtotal + (xr_qty.Text * xr_ret.Text)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatNumber(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatNumber(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint, xr_payment_holder.BeforePrint

        'If processed_payments = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        'sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                End If
            Else
                sender.text = FormatNumber(0, 2)
            End If
            MyDataReader2.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        Dim grand_total As Double = 0
        Dim payments As Double = 0
        Dim finance As Double = 0
        If IsNumeric(xr_grand_total.Text) Then grand_total = CDbl(xr_grand_total.Text)
        If IsNumeric(xr_payments.Text) Then payments = CDbl(xr_payments.Text)
        If IsNumeric(xr_financing.Text) Then finance = CDbl(xr_financing.Text)

        xr_balance.Text = FormatNumber(grand_total - (payments + finance), 2)

    End Sub

    Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_disc_desc.BeforePrint

        If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
            If CDbl(xr_disc_amt.Text) > 0 Then
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
                            sender.visible = True
                            sender.text = MyDataReader2.Item("DES").ToString & " ( DISCOUNTED " & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
                        Else
                            sender.text = ""
                        End If
                    Else
                        sender.text = ""
                    End If
                    MyDataReader2.Close()
                    conn.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
            End If
        End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='S' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
                MyDataReader2.Close()
                conn.Close()
            Catch
                conn.Close()
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_cod_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_cod.BeforePrint

        Dim balance As Double = 0
        Dim finance As Double = 0
        If IsNumeric(xr_balance.Text) Then balance = CDbl(xr_balance.Text)
        If IsNumeric(xr_financing.Text) Then finance = CDbl(xr_financing.Text)

        xr_cod.Text = FormatNumber(balance + finance, 2)

    End Sub
End Class