﻿Public Class BigSandyInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim triggers_processed As Boolean
    Dim subtotal As Double
    Dim comments_processed As Boolean
    Dim ds As New DataSet
    Private WithEvents xr_triggers As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_card_no As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_exp_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_mop_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ref As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_appr_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_merchant As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_financed As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_stat_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ship_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "BigSandyInvoice.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.BigSandyInvoice.ResourceManager
        Dim xrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xr_triggers = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_mop = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_card_no = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_exp_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_mop_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ref = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_appr_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_merchant = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_financed = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.xr_del = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_stat_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ship_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_void_flag, Me.xr_ext, Me.xr_disc_amt, Me.xr_SKU, Me.xr_disc_desc, Me.xr_ret, Me.xr_vsn, Me.xr_qty, Me.xr_store, Me.xr_desc, Me.xr_loc})
        Me.Detail.HeightF = 54.16667!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 25.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'xr_ext
        '
        Me.xr_ext.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.LocationFloat = New DevExpress.Utils.PointFloat(712.5!, 0.0!)
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.Text = "EXTENDED"
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(37.5!, 25.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_SKU
        '
        Me.xr_SKU.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_SKU.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 0.0!)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xr_SKU.StylePriority.UseFont = False
        Me.xr_SKU.Text = "SKU"
        Me.xr_SKU.WordWrap = False
        '
        'xr_disc_desc
        '
        Me.xr_disc_desc.CanShrink = True
        Me.xr_disc_desc.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_desc.ForeColor = System.Drawing.Color.Red
        Me.xr_disc_desc.LocationFloat = New DevExpress.Utils.PointFloat(412.5!, 25.0!)
        Me.xr_disc_desc.Name = "xr_disc_desc"
        Me.xr_disc_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_desc.SizeF = New System.Drawing.SizeF(367.0!, 17.0!)
        Me.xr_disc_desc.StylePriority.UseFont = False
        Me.xr_disc_desc.StylePriority.UseForeColor = False
        Me.xr_disc_desc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ret
        '
        Me.xr_ret.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.LocationFloat = New DevExpress.Utils.PointFloat(621.5!, 0.0!)
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.Text = "RETAIL"
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_vsn
        '
        Me.xr_vsn.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.SizeF = New System.Drawing.SizeF(375.0!, 17.0!)
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.Text = "VSN"
        '
        'xr_qty
        '
        Me.xr_qty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.LocationFloat = New DevExpress.Utils.PointFloat(587.5!, 0.0!)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.Text = "QTY"
        '
        'xr_store
        '
        Me.xr_store.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store.LocationFloat = New DevExpress.Utils.PointFloat(87.5!, 0.0!)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_store.StylePriority.UseFont = False
        Me.xr_store.Text = "ST"
        '
        'xr_desc
        '
        Me.xr_desc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_desc.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 25.0!)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
        Me.xr_desc.StylePriority.UseFont = False
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_loc
        '
        Me.xr_loc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_loc.StylePriority.UseFont = False
        Me.xr_loc.Text = "LOC"
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPictureBox1})
        Me.TopMargin.HeightF = 78.75001!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.HeightF = 0.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'PageFooter
        '
        Me.PageFooter.HeightF = 73.95834!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_triggers, Me.xrLabel27, Me.xrLine1, Me.xrLabel9, Me.xr_mop, Me.xr_card_no, Me.xrTable1, Me.xr_exp_dt, Me.xrPanel1, Me.xr_mop_dt, Me.xr_ref, Me.xr_appr_cd, Me.xr_pmt_amt, Me.xr_merchant, Me.xr_financed, Me.xr_payments, Me.xr_balance, Me.xrLabel8, Me.xrLabel25, Me.xr_grand_total, Me.xr_tax, Me.xrLine5, Me.xr_del, Me.xrLabel19, Me.xr_subtotal, Me.xrLabel22, Me.xrLabel23, Me.xrLabel24, Me.xrLabel20})
        Me.ReportFooter.HeightF = 311.0416!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'xr_triggers
        '
        Me.xr_triggers.CanShrink = True
        Me.xr_triggers.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_triggers.ForeColor = System.Drawing.Color.Black
        Me.xr_triggers.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 290.3333!)
        Me.xr_triggers.Name = "xr_triggers"
        Me.xr_triggers.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_triggers.SizeF = New System.Drawing.SizeF(802.0!, 17.0!)
        Me.xr_triggers.StylePriority.UseFont = False
        Me.xr_triggers.StylePriority.UseForeColor = False
        Me.xr_triggers.StylePriority.UseTextAlignment = False
        Me.xr_triggers.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel27
        '
        Me.xrLabel27.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel27.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 212.5!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(800.0!, 33.0!)
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLine1
        '
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(135.0!, 275.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(458.0!, 9.0!)
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLabel9
        '
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 262.5!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(133.0!, 17.0!)
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "CUSTOMER SIGNATURE:"
        '
        'xr_mop
        '
        Me.xr_mop.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop.LocationFloat = New DevExpress.Utils.PointFloat(67.0!, 181.0!)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.SizeF = New System.Drawing.SizeF(141.0!, 17.0!)
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_card_no
        '
        Me.xr_card_no.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_card_no.LocationFloat = New DevExpress.Utils.PointFloat(209.0!, 181.0!)
        Me.xr_card_no.Multiline = True
        Me.xr_card_no.Name = "xr_card_no"
        Me.xr_card_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_card_no.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_card_no.StylePriority.UseFont = False
        Me.xr_card_no.StylePriority.UseTextAlignment = False
        Me.xr_card_no.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTable1
        '
        Me.xrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 162.5!)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow3})
        Me.xrTable1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
        Me.xrTable1.StylePriority.UseBorders = False
        Me.xrTable1.StylePriority.UseFont = False
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell14, Me.xrTableCell7, Me.xrTableCell10, Me.xrTableCell8, Me.xrTableCell9, Me.xrTableCell11, Me.xrTableCell12, Me.xrTableCell13})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.Weight = 1.0R
        '
        'xrTableCell14
        '
        Me.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell14.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell14.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell14.Name = "xrTableCell14"
        Me.xrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell14.StylePriority.UseBorders = False
        Me.xrTableCell14.StylePriority.UseFont = False
        Me.xrTableCell14.StylePriority.UseForeColor = False
        Me.xrTableCell14.Text = "Date"
        Me.xrTableCell14.Weight = 0.083458333333333329R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell7.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell7.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell7.StylePriority.UseBorders = False
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UseForeColor = False
        Me.xrTableCell7.StylePriority.UseTextAlignment = False
        Me.xrTableCell7.Text = "Payment Type"
        Me.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell7.Weight = 0.17595833333333333R
        '
        'xrTableCell10
        '
        Me.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell10.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell10.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell10.Name = "xrTableCell10"
        Me.xrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell10.StylePriority.UseBorders = False
        Me.xrTableCell10.StylePriority.UseFont = False
        Me.xrTableCell10.StylePriority.UseForeColor = False
        Me.xrTableCell10.StylePriority.UseTextAlignment = False
        Me.xrTableCell10.Text = " Card Number"
        Me.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell10.Weight = 0.2085R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell8.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell8.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell8.StylePriority.UseBorders = False
        Me.xrTableCell8.StylePriority.UseFont = False
        Me.xrTableCell8.StylePriority.UseForeColor = False
        Me.xrTableCell8.StylePriority.UseTextAlignment = False
        Me.xrTableCell8.Text = " Exp Dt"
        Me.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell8.Weight = 0.084249999999999978R
        '
        'xrTableCell9
        '
        Me.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell9.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell9.StylePriority.UseBorders = False
        Me.xrTableCell9.StylePriority.UseFont = False
        Me.xrTableCell9.StylePriority.UseForeColor = False
        Me.xrTableCell9.StylePriority.UseTextAlignment = False
        Me.xrTableCell9.Text = " Approval"
        Me.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell9.Weight = 0.11399999999999999R
        '
        'xrTableCell11
        '
        Me.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell11.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell11.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseBorders = False
        Me.xrTableCell11.StylePriority.UseFont = False
        Me.xrTableCell11.StylePriority.UseForeColor = False
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "Amount"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell11.Weight = 0.095166666666666677R
        '
        'xrTableCell12
        '
        Me.xrTableCell12.BorderColor = System.Drawing.SystemColors.ControlText
        Me.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell12.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell12.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell12.StylePriority.UseBorderColor = False
        Me.xrTableCell12.StylePriority.UseBorders = False
        Me.xrTableCell12.StylePriority.UseFont = False
        Me.xrTableCell12.StylePriority.UseForeColor = False
        Me.xrTableCell12.StylePriority.UseTextAlignment = False
        Me.xrTableCell12.Text = "Merchant ID"
        Me.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell12.Weight = 0.15683333333333335R
        '
        'xrTableCell13
        '
        Me.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell13.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell13.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell13.Name = "xrTableCell13"
        Me.xrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell13.StylePriority.UseBorders = False
        Me.xrTableCell13.StylePriority.UseFont = False
        Me.xrTableCell13.StylePriority.UseForeColor = False
        Me.xrTableCell13.StylePriority.UseTextAlignment = False
        Me.xrTableCell13.Text = "Ref"
        Me.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell13.Weight = 0.081833333333333341R
        '
        'xr_exp_dt
        '
        Me.xr_exp_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_exp_dt.LocationFloat = New DevExpress.Utils.PointFloat(377.0!, 181.0!)
        Me.xr_exp_dt.Multiline = True
        Me.xr_exp_dt.Name = "xr_exp_dt"
        Me.xr_exp_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_exp_dt.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
        Me.xr_exp_dt.StylePriority.UseFont = False
        Me.xr_exp_dt.StylePriority.UseTextAlignment = False
        Me.xr_exp_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrPanel1
        '
        Me.xrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrLabel10})
        Me.xrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 0.0!)
        Me.xrPanel1.Name = "xrPanel1"
        Me.xrPanel1.SizeF = New System.Drawing.SizeF(478.8333!, 125.0!)
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(476.875!, 17.0!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xr_mop_dt
        '
        Me.xr_mop_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop_dt.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 181.0!)
        Me.xr_mop_dt.Multiline = True
        Me.xr_mop_dt.Name = "xr_mop_dt"
        Me.xr_mop_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop_dt.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_mop_dt.StylePriority.UseFont = False
        Me.xr_mop_dt.StylePriority.UseTextAlignment = False
        Me.xr_mop_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_ref
        '
        Me.xr_ref.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_ref.LocationFloat = New DevExpress.Utils.PointFloat(735.0!, 181.0!)
        Me.xr_ref.Multiline = True
        Me.xr_ref.Name = "xr_ref"
        Me.xr_ref.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ref.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
        Me.xr_ref.StylePriority.UseFont = False
        Me.xr_ref.StylePriority.UseTextAlignment = False
        Me.xr_ref.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_appr_cd
        '
        Me.xr_appr_cd.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_appr_cd.LocationFloat = New DevExpress.Utils.PointFloat(443.0!, 181.0!)
        Me.xr_appr_cd.Multiline = True
        Me.xr_appr_cd.Name = "xr_appr_cd"
        Me.xr_appr_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_appr_cd.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.xr_appr_cd.StylePriority.UseFont = False
        Me.xr_appr_cd.StylePriority.UseTextAlignment = False
        Me.xr_appr_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_amt.LocationFloat = New DevExpress.Utils.PointFloat(535.0!, 181.0!)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_merchant
        '
        Me.xr_merchant.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_merchant.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 181.0!)
        Me.xr_merchant.Multiline = True
        Me.xr_merchant.Name = "xr_merchant"
        Me.xr_merchant.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_merchant.SizeF = New System.Drawing.SizeF(124.0!, 17.0!)
        Me.xr_merchant.StylePriority.UseFont = False
        Me.xr_merchant.StylePriority.UseTextAlignment = False
        Me.xr_merchant.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_financed
        '
        Me.xr_financed.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.xr_financed.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_financed.ForeColor = System.Drawing.Color.DimGray
        Me.xr_financed.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 93.0!)
        Me.xr_financed.Name = "xr_financed"
        Me.xr_financed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_financed.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_financed.StylePriority.UseBorders = False
        Me.xr_financed.StylePriority.UseFont = False
        Me.xr_financed.StylePriority.UseForeColor = False
        Me.xr_financed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_payments
        '
        Me.xr_payments.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.DimGray
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 75.5!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.Text = "xr_payments"
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_balance
        '
        Me.xr_balance.CanGrow = False
        Me.xr_balance.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.ForeColor = System.Drawing.Color.Black
        Me.xr_balance.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 125.0!)
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_balance.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseForeColor = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.Text = "balance"
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.WordWrap = False
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(536.0!, 93.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.Text = "FINANCED:"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel25
        '
        Me.xrLabel25.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel25.ForeColor = System.Drawing.Color.Black
        Me.xrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(496.0!, 125.0!)
        Me.xrLabel25.Name = "xrLabel25"
        Me.xrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel25.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel25.StylePriority.UseFont = False
        Me.xrLabel25.StylePriority.UseForeColor = False
        Me.xrLabel25.Text = "**BALANCE** :"
        Me.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_grand_total
        '
        Me.xr_grand_total.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.ForeColor = System.Drawing.Color.Black
        Me.xr_grand_total.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 57.0!)
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseForeColor = False
        Me.xr_grand_total.Text = "xr_grand_total"
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_tax
        '
        Me.xr_tax.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.ForeColor = System.Drawing.Color.DimGray
        Me.xr_tax.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 35.0!)
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseForeColor = False
        Me.xr_tax.Text = "xr_tax"
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 150.0!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(800.0!, 2.0!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'xr_del
        '
        Me.xr_del.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.ForeColor = System.Drawing.Color.DimGray
        Me.xr_del.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 17.5!)
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseForeColor = False
        Me.xr_del.Text = "xr_del"
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel19
        '
        Me.xrLabel19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel19.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(521.0!, 0.0!)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel19.StylePriority.UseFont = False
        Me.xrLabel19.StylePriority.UseForeColor = False
        Me.xrLabel19.Text = "SUB-TOTAL:"
        Me.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_subtotal
        '
        Me.xr_subtotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.ForeColor = System.Drawing.Color.DimGray
        Me.xr_subtotal.LocationFloat = New DevExpress.Utils.PointFloat(646.0!, 0.0!)
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseForeColor = False
        xrSummary1.FormatString = "{0:#.00}"
        xrSummary1.IgnoreNullValues = True
        Me.xr_subtotal.Summary = xrSummary1
        Me.xr_subtotal.Text = "xr_subtotal"
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel22
        '
        Me.xrLabel22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel22.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(521.0!, 35.0!)
        Me.xrLabel22.Name = "xrLabel22"
        Me.xrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel22.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel22.StylePriority.UseFont = False
        Me.xrLabel22.StylePriority.UseForeColor = False
        Me.xrLabel22.Text = "SALES TAX:"
        Me.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel23
        '
        Me.xrLabel23.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel23.ForeColor = System.Drawing.Color.Black
        Me.xrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(521.0!, 57.0!)
        Me.xrLabel23.Name = "xrLabel23"
        Me.xrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel23.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel23.StylePriority.UseFont = False
        Me.xrLabel23.StylePriority.UseForeColor = False
        Me.xrLabel23.Text = "*TOTAL SALE*:"
        Me.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel24
        '
        Me.xrLabel24.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel24.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(521.0!, 75.5!)
        Me.xrLabel24.Name = "xrLabel24"
        Me.xrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel24.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel24.StylePriority.UseFont = False
        Me.xrLabel24.StylePriority.UseForeColor = False
        Me.xrLabel24.Text = "PAYMENTS:"
        Me.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel20
        '
        Me.xrLabel20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel20.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(496.0!, 17.5!)
        Me.xrLabel20.Name = "xrLabel20"
        Me.xrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel20.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel20.StylePriority.UseFont = False
        Me.xrLabel20.StylePriority.UseForeColor = False
        Me.xrLabel20.Text = "DELIVERY CHARGE:"
        Me.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_disc_cd, Me.xr_full_name, Me.xrLabel1, Me.xr_ord_tp_hidden, Me.xr_slsp2, Me.xr_ord_srt, Me.xrLine4, Me.xrLabel16, Me.xrLabel15, Me.xrLabel17, Me.xrLine3, Me.xrLabel18, Me.xrLabel34, Me.xr_ship_zip, Me.xr_ship_full_name, Me.xr_ship_b_phone, Me.xr_ship_h_phone, Me.xr_ship_addr2, Me.xr_stat_cd, Me.xr_slsp2_hidden, Me.xr_ship_city, Me.xr_ship_addr1, Me.xr_ship_state, Me.xr_city, Me.xr_addr2, Me.xr_state, Me.xr_h_phone, Me.xr_zip, Me.xr_del_doc_num, Me.xr_fname, Me.xr_ord_tp, Me.xr_so_wr_dt, Me.xr_lname, Me.xr_addr1, Me.xr_slsp, Me.xrLabel7, Me.xrLabel12, Me.xrLabel14, Me.xrLabel13, Me.xr_pd, Me.xrLabel5, Me.xr_b_phone, Me.xr_cust_cd, Me.xr_pu_del_dt, Me.xrLabel6})
        Me.GroupHeader1.HeightF = 170.5!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(547.9167!, 9.000015!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_cd.StylePriority.UseFont = False
        Me.xr_disc_cd.Visible = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.5!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(230.2083!, 17.0!)
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.StylePriority.UseTextAlignment = False
        Me.xrLabel1.Text = "BILL TO:"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 12.5!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_slsp2
        '
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 104.0!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(175.0!, 17.0!)
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 37.5!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xrLine4
        '
        Me.xrLine4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 162.5!)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.SizeF = New System.Drawing.SizeF(800.0!, 8.0!)
        '
        'xrLabel16
        '
        Me.xrLabel16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel16.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(709.0!, 137.5!)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseForeColor = False
        Me.xrLabel16.Text = "TOTAL"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel15
        '
        Me.xrLabel15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel15.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(621.5!, 137.5!)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.StylePriority.UseForeColor = False
        Me.xrLabel15.Text = "PRICE EACH"
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel17
        '
        Me.xrLabel17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel17.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(87.5!, 137.5!)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xrLabel17.StylePriority.UseFont = False
        Me.xrLabel17.StylePriority.UseForeColor = False
        Me.xrLabel17.Text = "ST"
        '
        'xrLine3
        '
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(800.0!, 8.0!)
        '
        'xrLabel18
        '
        Me.xrLabel18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel18.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 137.5!)
        Me.xrLabel18.Name = "xrLabel18"
        Me.xrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel18.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xrLabel18.StylePriority.UseFont = False
        Me.xrLabel18.StylePriority.UseForeColor = False
        Me.xrLabel18.Text = "LOC"
        '
        'xrLabel34
        '
        Me.xrLabel34.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel34.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 0.0!)
        Me.xrLabel34.Name = "xrLabel34"
        Me.xrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel34.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel34.StylePriority.UseFont = False
        Me.xrLabel34.StylePriority.UseForeColor = False
        Me.xrLabel34.StylePriority.UseTextAlignment = False
        Me.xrLabel34.Text = "SHIP TO:"
        Me.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ship_zip
        '
        Me.xr_ship_zip.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_zip.LocationFloat = New DevExpress.Utils.PointFloat(487.5!, 69.5!)
        Me.xr_ship_zip.Name = "xr_ship_zip"
        Me.xr_ship_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_zip.SizeF = New System.Drawing.SizeF(50.00003!, 17.00001!)
        Me.xr_ship_zip.StylePriority.UseFont = False
        Me.xr_ship_zip.Text = "xr_zip"
        '
        'xr_ship_full_name
        '
        Me.xr_ship_full_name.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_full_name.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 17.5!)
        Me.xr_ship_full_name.Name = "xr_ship_full_name"
        Me.xr_ship_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_full_name.SizeF = New System.Drawing.SizeF(230.2083!, 17.0!)
        Me.xr_ship_full_name.StylePriority.UseFont = False
        '
        'xr_ship_b_phone
        '
        Me.xr_ship_b_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 104.0!)
        Me.xr_ship_b_phone.Name = "xr_ship_b_phone"
        Me.xr_ship_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_ship_b_phone.StylePriority.UseFont = False
        Me.xr_ship_b_phone.Text = "xr_b_phone"
        '
        'xr_ship_h_phone
        '
        Me.xr_ship_h_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 86.5!)
        Me.xr_ship_h_phone.Name = "xr_ship_h_phone"
        Me.xr_ship_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_ship_h_phone.StylePriority.UseFont = False
        Me.xr_ship_h_phone.Text = "xr_h_phone"
        '
        'xr_ship_addr2
        '
        Me.xr_ship_addr2.CanShrink = True
        Me.xr_ship_addr2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_addr2.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 52.0!)
        Me.xr_ship_addr2.Name = "xr_ship_addr2"
        Me.xr_ship_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_addr2.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_ship_addr2.SizeF = New System.Drawing.SizeF(229.2083!, 17.00001!)
        Me.xr_ship_addr2.StylePriority.UseFont = False
        Me.xr_ship_addr2.Text = "xr_addr2"
        '
        'xr_stat_cd
        '
        Me.xr_stat_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_stat_cd.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 25.0!)
        Me.xr_stat_cd.Name = "xr_stat_cd"
        Me.xr_stat_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_stat_cd.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_stat_cd.StylePriority.UseFont = False
        Me.xr_stat_cd.Visible = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 37.5!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ship_city
        '
        Me.xr_ship_city.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_city.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 69.5!)
        Me.xr_ship_city.Name = "xr_ship_city"
        Me.xr_ship_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_city.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_ship_city.StylePriority.UseFont = False
        Me.xr_ship_city.Text = "xr_city"
        '
        'xr_ship_addr1
        '
        Me.xr_ship_addr1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_addr1.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 34.5!)
        Me.xr_ship_addr1.Name = "xr_ship_addr1"
        Me.xr_ship_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_addr1.SizeF = New System.Drawing.SizeF(229.2083!, 17.00001!)
        Me.xr_ship_addr1.StylePriority.UseFont = False
        Me.xr_ship_addr1.Text = "xr_addr1"
        '
        'xr_ship_state
        '
        Me.xr_ship_state.CanGrow = False
        Me.xr_ship_state.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_ship_state.LocationFloat = New DevExpress.Utils.PointFloat(462.5!, 69.5!)
        Me.xr_ship_state.Name = "xr_ship_state"
        Me.xr_ship_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ship_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_ship_state.StylePriority.UseFont = False
        Me.xr_ship_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.CanShrink = True
        Me.xr_city.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 69.5!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 52.0!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(229.2083!, 17.00001!)
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_state
        '
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 69.5!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.5!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 69.5!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(50.00003!, 17.00001!)
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(687.5!, 0.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 12.5!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(584.5!, 0.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(100.0!, 17.00001!)
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES ORDER #:"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(687.5!, 34.5!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(574.5!, 0.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.5!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(229.2083!, 17.00001!)
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_slsp
        '
        Me.xr_slsp.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 86.5!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(175.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 69.5!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "SALES ASSOCIATE:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel12
        '
        Me.xrLabel12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel12.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(587.5!, 137.5!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseForeColor = False
        Me.xrLabel12.Text = "QTY"
        '
        'xrLabel14
        '
        Me.xrLabel14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel14.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 137.5!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseForeColor = False
        Me.xrLabel14.Text = "SKU"
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(312.5!, 137.5!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseForeColor = False
        Me.xrLabel13.Text = "DESCRIPTION"
        '
        'xr_pd
        '
        Me.xr_pd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.DimGray
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(587.5!, 51.0!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(584.5!, 17.5!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseForeColor = False
        Me.xrLabel5.Text = "ACCOUNT #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 104.0!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(687.5!, 17.5!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(687.5!, 52.0!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(584.5!, 34.5!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseForeColor = False
        Me.xrLabel6.Text = "SALES DATE:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 77.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        Me.topMarginBand1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.topMarginBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.999999!, 10.00001!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(153.125!, 68.75!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize
        '
        'BigSandyInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1})
        Me.Margins = New System.Drawing.Printing.Margins(22, 26, 79, 100)
        Me.Version = "11.2"
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_financed.DataBindings.Add("Text", DataSource, "SO.ORIG_FI_AMT", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_ship_full_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_FULL_NAME")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_ship_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_ship_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_ship_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_ship_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_ship_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_ship_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_ship_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_fname.DataBindings.Add("Text", DataSource, "CUST.FNAME")
        xr_lname.DataBindings.Add("Text", DataSource, "CUST.LNAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "CUST.ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_SKU.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_store.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_desc.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_stat_cd.DataBindings.Add("Text", DataSource, "SO.STAT_CD")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "PICKUP:"
            Case "D"
                sender.text = "DELIVERY:"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        ElseIf xr_ord_tp_hidden.Text = "CRM" Or xr_ord_tp_hidden.Text = "MCR" Then
            sender.text = -sender.text
        End If

    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint

        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        ElseIf xr_ord_tp_hidden.Text = "CRM" Or xr_ord_tp_hidden.Text = "MCR" Then
            sender.text = -sender.text
        End If

    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint

        'If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
        If xr_void_flag.Text = "Y" Then
            sender.text = "0.00"
        End If

    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        If xr_ord_tp_hidden.Text = "CRM" Or xr_ord_tp_hidden.Text = "MCR" Then
            sender.text = FormatNumber(xr_qty.Text * -Me.GetCurrentColumnValue("UNIT_PRC").ToString, 2)
            subtotal = subtotal + -(xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)
        Else
            sender.text = FormatNumber(xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString, 2)
            subtotal = subtotal + (xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)
        End If

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint

        sender.text = FormatCurrency(subtotal, 2)

    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint

        If xr_stat_cd.Text = "V" Then
            sender.text = FormatCurrency(0, 2)
        Else
            sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
        End If

    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c, so b "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        sql = sql & "and b.cust_cd=a.cust_cd "
        sql = sql & "and b.del_doc_num='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = b.del_doc_num "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = b.del_doc_num "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = b.del_doc_num "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = b.del_doc_num "
            Case Else
                sql = sql & "and  a.ivc_cd = b.del_doc_num "
        End Select
        sql = sql & "and a.trn_tp_cd <> '" & xr_ord_tp_hidden.Text & "'"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                End If
            End If
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Private Sub xr_financed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_financed.BeforePrint

        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        ElseIf xr_stat_cd.Text = "V" Or xr_stat_cd.Text = "F" Then
            sender.text = "0.00"
        Else
            sender.Text = FormatCurrency(sender.text, 2)
        End If

        Dim pmts As Double = 0

        If IsNumeric(xr_payments.Text) Then
            pmts = CDbl(xr_payments.Text)
        End If
        If IsNumeric(xr_financed.Text) Then
            pmts = pmts + CDbl(xr_financed.Text)
        End If
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        If xr_ord_tp_hidden.Text = "MCR" Or xr_ord_tp_hidden.Text = "CRM" Then
            pmts = -pmts
        End If
        If xr_stat_cd.Text = "V" Then
            xr_balance.Text = FormatCurrency(-pmts, 2)
        Else
            xr_balance.Text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)
        End If

    End Sub

    Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_disc_desc.BeforePrint

        If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
            If CDbl(xr_disc_amt.Text) > 0 Then
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
                            sender.visible = True
                            sender.text = MyDataReader2.Item("DES").ToString & " (" & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
                        Else
                            sender.visible = False
                        End If
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            End If
        Else
            sender.text = ""
        End If

    End Sub

    Private Sub xr_mop_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            If Not String.IsNullOrEmpty(dr("REF_NUM").ToString) Then
                sender.text = sender.text & dr("REF_NUM").ToString & Constants.vbCrLf
            ElseIf dr("DES").ToString & "" <> "" Then
                sender.text = sender.text & dr("DES").ToString & Constants.vbCrLf
            Else
                sender.text = sender.text & dr("TRN_DES").ToString & Constants.vbCrLf
            End If
        Next

        'Dim conn As New System.Data.OracleClient.OracleConnection
        'Dim objSql2 As System.Data.OracleClient.OracleCommand
        'Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        'Dim sql As String
        ''Dim merchant_id As String = ""

        'conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'conn.Open()

        'sql = "select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.POST_DT from ar_trn a, mop b, ar_trn_tp c, so d "
        'sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        'sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        'sql = sql & "and d.cust_cd=a.cust_cd "
        'sql = sql & "and d.del_doc_num='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        'Select Case xr_ord_tp_hidden.Text
        '    Case "SAL"
        '        sql = sql & "and  a.ivc_cd = d.del_doc_num "
        '    Case "CRM"
        '        sql = sql & "and  a.adj_ivc_cd = d.del_doc_num "
        '    Case "MCR"
        '        sql = sql & "and  a.adj_ivc_cd = d.del_doc_num "
        '    Case "MDB"
        '        sql = sql & "and  a.ivc_cd = d.del_doc_num "
        '    Case Else
        '        sql = sql & "and  a.ivc_cd = d.del_doc_num "
        'End Select
        'sql = sql & "and a.trn_tp_cd <> '" & xr_ord_tp_hidden.Text & "'"

        ''Set SQL OBJECT 
        'objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        'Try
        '    'Execute DataReader 
        '    MyDataReader2 = objSql2.ExecuteReader
        '    Do While MyDataReader2.Read
        '        If Not String.IsNullOrEmpty(MyDataReader2.Item("REF_NUM").ToString) Then
        '            sender.text = sender.text & MyDataReader2.Item("REF_NUM").ToString & Constants.vbCrLf
        '        ElseIf MyDataReader2.Item("DES").ToString & "" <> "" Then
        '            sender.text = sender.text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
        '        Else
        '            sender.text = sender.text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
        '        End If
        '        'If String.IsNullOrEmpty(MyDataReader2.Item("HOST_REF_NUM").ToString) Then
        '        '    xr_ref.Text = xr_ref.Text & Constants.vbCrLf
        '        'Else
        '        '    xr_ref.Text = xr_ref.Text & MyDataReader2.Item("HOST_REF_NUM").ToString & Constants.vbCrLf
        '        'End If
        '        'xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
        '        'If Len(MyDataReader2.Item("BNK_CRD_NUM").ToString) = 19 Then
        '        '    xr_card_no.Text = xr_card_no.Text & Microsoft.VisualBasic.Strings.Right(MyDataReader2.Item("BNK_CRD_NUM").ToString, 16) & Constants.vbCrLf
        '        'ElseIf MyDataReader2.Item("CHK_NUM").ToString & "" <> "" Then
        '        '    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("CHK_NUM").ToString & Constants.vbCrLf
        '        'Else
        '        '    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
        '        'End If
        '        'If InStr(MyDataReader2.Item("MID").ToString, "MID:") > 0 Then
        '        '    merchant_id = Replace(MyDataReader2.Item("MID").ToString, "MID:", "")
        '        '    If IsNumeric(merchant_id) Then merchant_id = CDbl(merchant_id)
        '        '    xr_merchant.Text = xr_merchant.Text & merchant_id & Constants.vbCrLf
        '        'Else
        '        '    xr_merchant.Text = xr_merchant.Text & Constants.vbCrLf
        '        'End If
        '        'xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
        '        'If Not String.IsNullOrEmpty(MyDataReader2.Item("EXP_DT").ToString) Then
        '        '    xr_exp_dt.Text = xr_exp_dt.Text & "xx/xx" & Constants.vbCrLf
        '        'Else
        '        '    xr_exp_dt.Text = xr_exp_dt.Text & Constants.vbCrLf
        '        'End If
        '        'If IsDate(MyDataReader2.Item("POST_DT").ToString) Then
        '        '    xr_mop_dt.Text = xr_mop_dt.Text & FormatDateTime(MyDataReader2.Item("POST_DT").ToString, DateFormat.ShortDate) & Constants.vbCrLf
        '        'Else
        '        '    xr_mop_dt.Text = xr_mop_dt.Text & Constants.vbCrLf
        '        'End If
        '        'xr_appr_cd.Text = xr_appr_cd.Text & MyDataReader2.Item("APP_CD").ToString & Constants.vbCrLf
        '    Loop
        'Catch
        '    conn.Close()
        '    Throw
        'End Try
        'conn.Close()

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='D' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
            comments_processed = True
        End If

    End Sub

    Private Sub xr_triggers_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_triggers.BeforePrint

        If triggers_processed <> True Then
            Dim conn2 As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            conn.Open()

            If xr_ord_srt.Text & "" <> "" Then
                sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                sql = sql & "VALUES('" & xr_del_doc_num.Text & "','SORT_CD','" & xr_ord_srt.Text & "')"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
            End If

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','P_D','" & Microsoft.VisualBasic.Strings.Left(xr_pd.Text, 1) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','STORE_CD','" & Microsoft.VisualBasic.Strings.Mid(xr_del_doc_num.Text, 6, 2) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "SELECT c.ITM_TP_CD, b.MJR_CD, c.MNR_CD, a.ITM_CD, c.DROP_DT FROM SO_LN a, INV_MNR b, ITM c "
            sql = sql & "WHERE a.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND c.MNR_CD=b.MNR_CD "
            sql = sql & "AND a.ITM_CD=c.ITM_CD AND a.VOID_FLAG='N'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_TP_CD','" & MyDataReader2.Item("ITM_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MAJOR_CD','" & MyDataReader2.Item("MJR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MINOR_CD','" & MyDataReader2.Item("MNR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_CD','" & MyDataReader2.Item("ITM_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DROP_DT").ToString) Then
                        sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                        sql = sql & "VALUES('" & xr_del_doc_num.Text & "','DROP_DT','" & MyDataReader2.Item("DROP_DT").ToString & "')"

                        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql2.ExecuteNonQuery()
                    End If
                Loop
            Catch
                conn.Close()
                conn2.Close()
                Throw
            End Try

            sql = "select trigger_response, TRIGGER_ID, TO_NUMBER(REPLACE(TRIGGER_EQ,'=','1')) "
            sql = sql & "from invoice_triggers, invoice_triggers_temp "
            sql = sql & "where invoice_triggers.trigger_field = invoice_triggers_temp.trigger_field "
            sql = sql & "and invoice_triggers.trigger_value=invoice_triggers_temp.trigger_value "
            sql = sql & "and del_doc_num='" & xr_del_doc_num.Text & "' "
            sql = sql & "GROUP BY TRIGGER_ID, trigger_response, TO_NUMBER(REPLACE(TRIGGER_EQ,'=','1')) "
            sql = sql & "ORDER BY TO_NUMBER(REPLACE(TRIGGER_EQ,'=','1')) ASC"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_triggers.Text = xr_triggers.Text & MyDataReader2.Item("TRIGGER_RESPONSE").ToString & Constants.vbCrLf
                Loop
            Catch
                conn.Close()
                conn2.Close()
                Throw
            End Try

            If Not String.IsNullOrEmpty(xr_triggers.Text) Then
                xr_triggers.Text = xr_triggers.Text & Constants.vbCrLf
            End If

            sql = "DELETE FROM INVOICE_TRIGGERS_TEMP WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            triggers_processed = True
            conn.Close()
            conn2.Close()
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES ORDER #:"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO #:"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT #:"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT #:"
        End Select

    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2.BeforePrint

        If xr_slsp2_hidden.Text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME "
            sql = sql & "FROM SO, EMP WHERE SO.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' "
            sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD2  "

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("FULL_NAME").ToString
                Loop
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Private Sub xr_balance_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        If Not IsNumeric(sender.text) Then
            Dim pmts As Double = 0

            If IsNumeric(xr_payments.Text) Then
                pmts = CDbl(xr_payments.Text)
            Else
                xr_payments.Text = 0
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c, so b "
                sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
                sql = sql & "and b.cust_cd=a.cust_cd "
                sql = sql & "and b.del_doc_num='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
                Select Case xr_ord_tp_hidden.Text
                    Case "SAL"
                        sql = sql & "and  a.ivc_cd = b.del_doc_num "
                    Case "CRM"
                        sql = sql & "and  a.adj_ivc_cd = b.del_doc_num "
                    Case "MCR"
                        sql = sql & "and  a.adj_ivc_cd = b.del_doc_num "
                    Case "MDB"
                        sql = sql & "and  a.ivc_cd = b.del_doc_num "
                    Case Else
                        sql = sql & "and  a.ivc_cd = b.del_doc_num "
                End Select
                sql = sql & "and a.trn_tp_cd <> '" & xr_ord_tp_hidden.Text & "'"

                'Set SQL OBJECT 
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                            pmts = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                        Else
                            pmts = FormatNumber(0, 2)
                        End If
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            End If

            If xr_stat_cd.Text = "V" Then
                xr_balance.Text = FormatCurrency(-xr_payments.Text, 2)
            Else
                xr_balance.Text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)
            End If
        End If

    End Sub

    Private Sub xr_pmt_amt_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_pmt_amt.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            sender.Text = sender.Text & FormatCurrency(dr("PMT_AMT").ToString, 2) & Constants.vbCrLf
        Next

    End Sub

    Public Sub populate_ds()

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.POST_DT from ar_trn a, mop b, ar_trn_tp c, so d "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        sql = sql & "and d.cust_cd=a.cust_cd "
        sql = sql & "and d.del_doc_num='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = d.del_doc_num "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = d.del_doc_num "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = d.del_doc_num "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = d.del_doc_num "
            Case Else
                sql = sql & "and  a.ivc_cd = d.del_doc_num "
        End Select
        sql = sql & "and a.trn_tp_cd <> '" & xr_ord_tp_hidden.Text & "'"

        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim oAdp As System.Data.OracleClient.OracleDataAdapter
        ds = New DataSet

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql2)
        oAdp.Fill(ds)

    End Sub

    Private Sub xr_ref_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_ref.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            If String.IsNullOrEmpty(dr("HOST_REF_NUM").ToString) Then
                sender.Text = sender.Text & Constants.vbCrLf
            Else
                sender.Text = sender.Text & dr("HOST_REF_NUM").ToString & Constants.vbCrLf
            End If
        Next

    End Sub

    Private Sub xr_card_no_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_card_no.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            If Len(dr("BNK_CRD_NUM").ToString) = 19 Then
                sender.Text = sender.Text & Microsoft.VisualBasic.Strings.Right(dr("BNK_CRD_NUM").ToString, 16) & Constants.vbCrLf
            ElseIf dr("CHK_NUM").ToString & "" <> "" Then
                sender.Text = sender.Text & dr("CHK_NUM").ToString & Constants.vbCrLf
            Else
                sender.Text = sender.Text & dr("BNK_CRD_NUM").ToString & Constants.vbCrLf
            End If
        Next
        

    End Sub

    Private Sub xr_merchant_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_merchant.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        Dim merchant_id As String = ""

        For Each dr In ds.Tables(0).Rows
            If InStr(dr("MID").ToString, "MID:") > 0 Then
                merchant_id = Replace(dr("MID").ToString, "MID:", "")
                If IsNumeric(merchant_id) Then merchant_id = CDbl(merchant_id)
                sender.Text = sender.Text & merchant_id & Constants.vbCrLf
            Else
                sender.Text = sender.Text & Constants.vbCrLf
            End If
        Next

    End Sub

    Private Sub xr_exp_dt_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_exp_dt.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            If Not String.IsNullOrEmpty(dr("EXP_DT").ToString) Then
                sender.Text = sender.Text & "xx/xx" & Constants.vbCrLf
            Else
                sender.Text = sender.Text & Constants.vbCrLf
            End If
        Next

    End Sub

    Private Sub xr_mop_dt_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop_dt.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            If IsDate(dr("POST_DT").ToString) Then
                sender.Text = sender.Text & FormatDateTime(dr("POST_DT").ToString, DateFormat.ShortDate) & Constants.vbCrLf
            Else
                sender.Text = sender.Text & Constants.vbCrLf
            End If
        Next
        
    End Sub

    Private Sub xr_appr_cd_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_appr_cd.BeforePrint

        If ds.Tables.Count < 1 Then
            populate_ds()
        End If

        For Each dr In ds.Tables(0).Rows
            sender.Text = sender.Text & dr("APP_CD").ToString & Constants.vbCrLf
        Next

    End Sub

    Private Sub xr_tax_BeforePrint1(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint

        If xr_ord_tp_hidden.Text = "CRM" Or xr_ord_tp_hidden.Text = "MCR" Then
            sender.text = FormatCurrency(-Me.GetCurrentColumnValue("TAX_CHG").ToString, 2)
        Else
            sender.text = FormatCurrency(Me.GetCurrentColumnValue("TAX_CHG").ToString, 2)
        End If

    End Sub
End Class