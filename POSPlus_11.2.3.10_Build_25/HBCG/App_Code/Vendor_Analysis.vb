Imports DevExpress.XtraReports.UI
Imports System.Data.OracleClient
Imports DevExpress.XtraCharts

Public Class Vendor_Analysis

    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim csh_totals As Double = 0
    Dim bc_totals As Double = 0
    Dim disc_totals As Double = 0
    Dim amex_totals As Double = 0
    Dim check_totals As Double = 0
    Dim other_totals As Double = 0
    Dim rcsh_totals As Double = 0
    Dim rbc_totals As Double = 0
    Dim rdisc_totals As Double = 0
    Dim ramex_totals As Double = 0
    Dim rcheck_totals As Double = 0
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xr_vendor As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrChart1 As DevExpress.XtraReports.UI.XRChart
    Dim rother_totals As Double = 0

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_date As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Vendor_Analysis.resx"
        Dim xyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram
        Dim series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series
        Dim pointSeriesLabel1 As DevExpress.XtraCharts.PointSeriesLabel = New DevExpress.XtraCharts.PointSeriesLabel
        Dim splineSeriesView1 As DevExpress.XtraCharts.SplineSeriesView = New DevExpress.XtraCharts.SplineSeriesView
        Dim series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series
        Dim pointSeriesLabel2 As DevExpress.XtraCharts.PointSeriesLabel = New DevExpress.XtraCharts.PointSeriesLabel
        Dim splineSeriesView2 As DevExpress.XtraCharts.SplineSeriesView = New DevExpress.XtraCharts.SplineSeriesView
        Dim pointSeriesLabel3 As DevExpress.XtraCharts.PointSeriesLabel = New DevExpress.XtraCharts.PointSeriesLabel
        Dim splineSeriesView3 As DevExpress.XtraCharts.SplineSeriesView = New DevExpress.XtraCharts.SplineSeriesView
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_date = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.xr_vendor = New DevExpress.XtraReports.UI.XRLabel
        Me.xrChart1 = New DevExpress.XtraReports.UI.XRChart
        CType(Me.xrChart1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(xyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(pointSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(splineSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(pointSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(splineSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(pointSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(splineSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrChart1})
        Me.Detail.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detail.HeightF = 371.875!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_vendor, Me.xrLabel3, Me.xr_date, Me.xrLabel1, Me.xrPageInfo1})
        Me.PageHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageHeader.HeightF = 75.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseFont = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(200.0!, 25.0!)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.Text = "Vendor Analysis"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_date
        '
        Me.xr_date.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_date.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
        Me.xr_date.Name = "xr_date"
        Me.xr_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_date.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_date.StylePriority.UseFont = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(42.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Date:"
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(842.0!, 0.0!)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo1.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'PageFooter
        '
        Me.PageFooter.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageFooter.HeightF = 30.0!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.Transparent
        Me.GroupHeader1.HeightF = 0.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.StylePriority.UseBackColor = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.HeightF = 0.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'ReportFooter
        '
        Me.ReportFooter.HeightF = 0.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 50.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 50.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'xr_vendor
        '
        Me.xr_vendor.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vendor.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 17.00001!)
        Me.xr_vendor.Name = "xr_vendor"
        Me.xr_vendor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vendor.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_vendor.StylePriority.UseFont = False
        '
        'xrChart1
        '
        Me.xrChart1.BorderColor = System.Drawing.SystemColors.ControlText
        Me.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None
        xyDiagram1.AxisX.Range.SideMarginsEnabled = True
        xyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        xyDiagram1.AxisY.Range.SideMarginsEnabled = True
        xyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        Me.xrChart1.Diagram = xyDiagram1
        Me.xrChart1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrChart1.Name = "xrChart1"
        pointSeriesLabel1.LineVisible = True
        series1.Label = pointSeriesLabel1
        series1.Name = "Series 1"
        series1.View = splineSeriesView1
        pointSeriesLabel2.LineVisible = True
        series2.Label = pointSeriesLabel2
        series2.Name = "Series 2"
        series2.View = splineSeriesView2
        Me.xrChart1.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1, series2}
        pointSeriesLabel3.LineVisible = True
        Me.xrChart1.SeriesTemplate.Label = pointSeriesLabel3
        Me.xrChart1.SeriesTemplate.View = splineSeriesView3
        Me.xrChart1.SizeF = New System.Drawing.SizeF(989.9999!, 371.875!)
        '
        'Vendor_Analysis
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "10.1"
        CType(xyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(pointSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(splineSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(pointSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(splineSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(pointSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(splineSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrChart1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        'xr_date.DataBindings.Add("Text", DataSource, "MONTH")
        'xr_vendor.DataBindings.Add("Text", DataSource, "VENDOR")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim start_timestamp As Date
        start_timestamp = Now.AddDays(-30)

        ds = New DataSet
        conn.Open()

        sql = "SELECT TO_CHAR(WR_DT, 'Day') AS WR_DT, SUM(QTY) AS ORIG_COUNT, STORE_CD "
        sql = sql & "FROM SALES_REPORT_ARCHIVE "
        sql = sql & "WHERE WR_DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "GROUP BY TO_CHAR(WR_DT, 'Day'), STORE_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        Dim dt As DataTable = ds.Tables(0)

        ds.WriteXml("c:\test.xml", XmlWriteMode.WriteSchema)

        Dim view As New DataView(dt)
        view.Sort = "WR_DT ASC"

        ' Generate a data table and bind the series to it.
        xrChart1.DataSource = view

        ' Specify data members to bind the series.
        xrChart1.SeriesDataMember = "STORE_CD"
        xrChart1.SeriesTemplate.ArgumentDataMember = "WR_DT"
        xrChart1.SeriesTemplate.ValueDataMembers(0) = dt.Columns("ORIG_COUNT").ColumnName
        'xrChart1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))
        xrChart1.SeriesTemplate.ChangeView(ViewType.Line)
        'xrChart1.SeriesTemplate.View = New LineSeriesView

    End Sub


End Class