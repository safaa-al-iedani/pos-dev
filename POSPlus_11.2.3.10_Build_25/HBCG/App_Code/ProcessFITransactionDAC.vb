Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.IO
Imports HBCG_Utils

Public Class ProcessFITransactionDAC
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()
    Public Sub New(ByRef requestXML As XmlDocument)
        _requestXML = requestXML
    End Sub

    Public Function SendRequestAndGetResponse(ByRef requestDS As RequestAllianceDataSet) As XmlDocument

        Dim ADS_Connection_String As String = Get_Connection_String("/HandleCode/ADS/xSale", "PrimaryConnection")
        Dim oWebReq As System.Net.HttpWebRequest
        oWebReq = CType(System.Net.HttpWebRequest.Create(ADS_Connection_String), System.Net.HttpWebRequest)
        Dim xml_string As String = ""

        xml_string = "<?xml version=""1.0"" encoding=""utf-8""?>"
        xml_string = xml_string & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
        xml_string = xml_string & "<soap:Body>"
        xml_string = xml_string & "<ns1:sales20 xmlns:ns1=""http://axes.alldata.net/web-services"" version=""2.0"">"
        xml_string = xml_string & "<request>"
        xml_string = xml_string & "<clientHeader>"
        xml_string = xml_string & "<clientName>LEVINS</clientName>"
        xml_string = xml_string & "<timestamp>" & requestDS.Request(0).timeStamp & "</timestamp>"
        xml_string = xml_string & "</clientHeader>"
        xml_string = xml_string & "<storeNumber>" & requestDS.Request(0).storeNumber & "</storeNumber>"
        xml_string = xml_string & "<accountNumber>" & requestDS.Request(0).accountNumber & "</accountNumber>"
        xml_string = xml_string & "<transactionAmount>" & requestDS.Request(0).transactionAmount & "</transactionAmount>"
        xml_string = xml_string & "<swipeInd>" & requestDS.Request(0).swipeInd & "</swipeInd>"
        xml_string = xml_string & "</request>"
        xml_string = xml_string & "</ns1:sales20>"
        xml_string = xml_string & "</soap:Body>"
        xml_string = xml_string & "</soap:Envelope>"

        _requestXML.RemoveAll()
        _requestXML.LoadXml(xml_string)
        Dim reqBytes As Byte()
        reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
        oWebReq.ContentLength = reqBytes.Length
        oWebReq.Method = "POST"
        oWebReq.ContentType = "text/xml"
        oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
        Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
        oReqStream.Write(reqBytes, 0, reqBytes.Length)
        Dim responseBytes As Byte()
        Try
            Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
            If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
                responseBytes = processResponse(oWebResp)
                BuildResponseDocument(responseBytes)
            End If
            oWebResp.Close()
        Catch ex As Exception
            _responseXML.LoadXml("<?xml version=""1.0"" encoding=""utf-8""?><returnCode></returnCode>")
            Return _responseXML
        End Try
        Return _responseXML

    End Function

    Public Sub BuildResponseDocument(ByRef responseBytes As Byte())

        Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
        _responseXML.LoadXml(strResp)

    End Sub

    Private Function processResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()
        Dim memStream As New System.IO.MemoryStream()
        Const BUFFER_SIZE As Integer = 4096
        Dim iRead As Integer = 0
        Dim idx As Integer = 0
        Dim iSize As Int64 = 0
        memStream.SetLength(BUFFER_SIZE)
        While (True)
            Dim respBuffer(BUFFER_SIZE) As Byte
            Try
                iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
            Catch e As System.Exception
                Throw e
            End Try
            If (iRead = 0) Then
                Exit While
            End If
            iSize += iRead
            memStream.SetLength(iSize)
            memStream.Write(respBuffer, 0, iRead)
            idx += iRead
        End While

        Dim content As Byte() = memStream.ToArray()
        memStream.Close()
        Return content
    End Function
End Class
