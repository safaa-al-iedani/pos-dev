﻿Imports Microsoft.VisualBasic

Public Class TurnersQuote
    Inherits DevExpress.XtraReports.UI.XtraReport

    Dim subtotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Dim comments_processed As Boolean
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable3 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable4 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sale_last_four As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell28 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_po_num As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable5 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable6 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell36 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell37 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell39 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell40 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell41 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable7 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ser_num As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable8 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vend As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_email As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_long_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_line_no As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xr_payment_holder As DevExpress.XtraReports.UI.XRTableCell
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "TurnersQuote.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.TurnersQuote.ResourceManager
        Me.xr_payment_holder = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_line_no = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_long_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable7 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vend = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ser_num = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_des = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_slsp_email = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp_ext = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_addr = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable8 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xr_store_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable6 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell36 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell41 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell37 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell40 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell39 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable5 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sale_last_four = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_po_num = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable3 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xr_payment_holder
        '
        Me.xr_payment_holder.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_payment_holder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payment_holder.Name = "xr_payment_holder"
        Me.xr_payment_holder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_payment_holder.StylePriority.UseBorderColor = False
        Me.xr_payment_holder.StylePriority.UseFont = False
        Me.xr_payment_holder.Weight = 2.17R
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_line_no, Me.xr_long_desc, Me.xrTable7, Me.xr_disc_amt, Me.xr_void_flag})
        Me.Detail.HeightF = 69.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_line_no
        '
        Me.xr_line_no.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_line_no.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 58.0!)
        Me.xr_line_no.Name = "xr_line_no"
        Me.xr_line_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_line_no.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_line_no.StylePriority.UseFont = False
        Me.xr_line_no.Visible = False
        '
        'xr_long_desc
        '
        Me.xr_long_desc.BackColor = System.Drawing.Color.Empty
        Me.xr_long_desc.CanShrink = True
        Me.xr_long_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_long_desc.LocationFloat = New DevExpress.Utils.PointFloat(92.0!, 50.0!)
        Me.xr_long_desc.Multiline = True
        Me.xr_long_desc.Name = "xr_long_desc"
        Me.xr_long_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_long_desc.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_long_desc.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
        Me.xr_long_desc.StylePriority.UseBackColor = False
        Me.xr_long_desc.StylePriority.UseFont = False
        '
        'xrTable7
        '
        Me.xrTable7.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTable7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable7.Name = "xrTable7"
        Me.xrTable7.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow12, Me.xrTableRow19})
        Me.xrTable7.SizeF = New System.Drawing.SizeF(791.0!, 50.0!)
        Me.xrTable7.StylePriority.UseBackColor = False
        Me.xrTable7.StylePriority.UseTextAlignment = False
        Me.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow12
        '
        Me.xrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_qty, Me.xr_sku, Me.xr_vend, Me.xr_vsn, Me.xr_ser_num, Me.xr_ret, Me.xr_ext})
        Me.xrTableRow12.Name = "xrTableRow12"
        Me.xrTableRow12.Weight = 1.0R
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_qty.Weight = 0.125R
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sku.Weight = 0.34625000000000011R
        '
        'xr_vend
        '
        Me.xr_vend.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vend.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vend.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vend.Name = "xr_vend"
        Me.xr_vend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_vend.StylePriority.UseBackColor = False
        Me.xr_vend.StylePriority.UseBorderColor = False
        Me.xr_vend.StylePriority.UseBorders = False
        Me.xr_vend.StylePriority.UseFont = False
        Me.xr_vend.StylePriority.UseTextAlignment = False
        Me.xr_vend.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vend.Weight = 0.21625R
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vsn.Weight = 1.1312499999999999R
        '
        'xr_ser_num
        '
        Me.xr_ser_num.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ser_num.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ser_num.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ser_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ser_num.Name = "xr_ser_num"
        Me.xr_ser_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ser_num.StylePriority.UseBackColor = False
        Me.xr_ser_num.StylePriority.UseBorderColor = False
        Me.xr_ser_num.StylePriority.UseBorders = False
        Me.xr_ser_num.StylePriority.UseFont = False
        Me.xr_ser_num.StylePriority.UseTextAlignment = False
        Me.xr_ser_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ser_num.Weight = 0.49624999999999997R
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ret.Weight = 0.34625000000000006R
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ext.Weight = 0.305R
        '
        'xrTableRow19
        '
        Me.xrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell27, Me.xrTableCell43, Me.xr_des, Me.xrTableCell48})
        Me.xrTableRow19.Name = "xrTableRow19"
        Me.xrTableRow19.Weight = 1.0R
        '
        'xrTableCell27
        '
        Me.xrTableCell27.BackColor = System.Drawing.Color.White
        Me.xrTableCell27.Name = "xrTableCell27"
        Me.xrTableCell27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell27.StylePriority.UseBackColor = False
        Me.xrTableCell27.Weight = 0.125R
        '
        'xrTableCell43
        '
        Me.xrTableCell43.BackColor = System.Drawing.Color.White
        Me.xrTableCell43.Name = "xrTableCell43"
        Me.xrTableCell43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell43.StylePriority.UseBackColor = False
        Me.xrTableCell43.Weight = 0.34250000000000008R
        '
        'xr_des
        '
        Me.xr_des.BackColor = System.Drawing.Color.White
        Me.xr_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_des.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_des.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_des.Name = "xr_des"
        Me.xr_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_des.StylePriority.UseBackColor = False
        Me.xr_des.StylePriority.UseBorderColor = False
        Me.xr_des.StylePriority.UseBorders = False
        Me.xr_des.StylePriority.UseFont = False
        Me.xr_des.StylePriority.UseTextAlignment = False
        Me.xr_des.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_des.Weight = 1.35125R
        '
        'xrTableCell48
        '
        Me.xrTableCell48.BackColor = System.Drawing.Color.White
        Me.xrTableCell48.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell48.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell48.Name = "xrTableCell48"
        Me.xrTableCell48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell48.StylePriority.UseBackColor = False
        Me.xrTableCell48.StylePriority.UseBorderColor = False
        Me.xrTableCell48.StylePriority.UseBorders = False
        Me.xrTableCell48.Weight = 1.1475R
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(67.0!, 58.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 58.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2, Me.xr_ord_tp})
        Me.PageHeader.HeightF = 178.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTable2
        '
        Me.xrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 83.0!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow4})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(783.0!, 92.0!)
        Me.xrTable2.StylePriority.UseBorders = False
        '
        'xrTableRow4
        '
        Me.xrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell14, Me.xrTableCell15})
        Me.xrTableRow4.Name = "xrTableRow4"
        Me.xrTableRow4.Weight = 1.0R
        '
        'xrTableCell14
        '
        Me.xrTableCell14.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPictureBox1})
        Me.xrTableCell14.Name = "xrTableCell14"
        Me.xrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell14.Text = "xrTableCell14"
        Me.xrTableCell14.Weight = 2.23R
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(4.0!, 4.0!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(217.0!, 83.0!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        Me.xrPictureBox1.StylePriority.UseBorders = False
        '
        'xrTableCell15
        '
        Me.xrTableCell15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_slsp_email, Me.xr_slsp_ext, Me.xrLabel1, Me.xr_slsp2, Me.xrLabel21, Me.xr_store_phone, Me.xr_store_addr, Me.xr_store_name})
        Me.xrTableCell15.Name = "xrTableCell15"
        Me.xrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell15.Text = "xrTableCell15"
        Me.xrTableCell15.Weight = 5.6000000000000005R
        '
        'xr_slsp_email
        '
        Me.xr_slsp_email.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp_email.CanShrink = True
        Me.xr_slsp_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_email.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp_email.LocationFloat = New DevExpress.Utils.PointFloat(385.0!, 41.0!)
        Me.xr_slsp_email.Name = "xr_slsp_email"
        Me.xr_slsp_email.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_email.SizeF = New System.Drawing.SizeF(173.0!, 17.0!)
        Me.xr_slsp_email.StylePriority.UseBorders = False
        Me.xr_slsp_email.StylePriority.UseFont = False
        Me.xr_slsp_email.StylePriority.UseForeColor = False
        Me.xr_slsp_email.StylePriority.UseTextAlignment = False
        Me.xr_slsp_email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_slsp_ext
        '
        Me.xr_slsp_ext.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp_ext.CanShrink = True
        Me.xr_slsp_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_ext.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp_ext.LocationFloat = New DevExpress.Utils.PointFloat(385.0!, 24.0!)
        Me.xr_slsp_ext.Name = "xr_slsp_ext"
        Me.xr_slsp_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_ext.SizeF = New System.Drawing.SizeF(173.0!, 17.0!)
        Me.xr_slsp_ext.StylePriority.UseBorders = False
        Me.xr_slsp_ext.StylePriority.UseFont = False
        Me.xr_slsp_ext.StylePriority.UseForeColor = False
        Me.xr_slsp_ext.StylePriority.UseTextAlignment = False
        Me.xr_slsp_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel1
        '
        Me.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(310.0!, 8.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseBorders = False
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.StylePriority.UseTextAlignment = False
        Me.xrLabel1.Text = "Sales Rep:"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_slsp2
        '
        Me.xr_slsp2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(385.0!, 7.0!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(173.0!, 17.0!)
        Me.xr_slsp2.StylePriority.UseBorders = False
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseForeColor = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel21
        '
        Me.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.xrLabel21.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 66.0!)
        Me.xrLabel21.Name = "xrLabel21"
        Me.xrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel21.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xrLabel21.StylePriority.UseBorders = False
        Me.xrLabel21.StylePriority.UseFont = False
        Me.xrLabel21.StylePriority.UseForeColor = False
        Me.xrLabel21.StylePriority.UseTextAlignment = False
        Me.xrLabel21.Text = "http://www.turnerfurniture.com/"
        Me.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_phone.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_phone.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 49.0!)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xr_store_phone.StylePriority.UseBorders = False
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_addr
        '
        Me.xr_store_addr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_addr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_addr.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_addr.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 32.0!)
        Me.xr_store_addr.Name = "xr_store_addr"
        Me.xr_store_addr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.xr_store_addr.StylePriority.UseBorders = False
        Me.xr_store_addr.StylePriority.UseFont = False
        Me.xr_store_addr.StylePriority.UseForeColor = False
        Me.xr_store_addr.StylePriority.UseTextAlignment = False
        Me.xr_store_addr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_name
        '
        Me.xr_store_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_name.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 15.0!)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.xr_store_name.StylePriority.UseBorders = False
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.StylePriority.UseTextAlignment = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 49.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(417.0!, 33.0!)
        Me.xr_ord_tp.StylePriority.UseBorders = False
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "*SALES QUOTE*"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 0.0!)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.SizeF = New System.Drawing.SizeF(154.0!, 17.0!)
        Me.xrPageInfo2.StylePriority.UseBorders = False
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.StylePriority.UseForeColor = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 125.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.Black
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseBorders = False
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.StylePriority.UseForeColor = False
        Me.xr_pu_del_dt.StylePriority.UseTextAlignment = False
        Me.xr_pu_del_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(129.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.0!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseBorders = False
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_h_phone.CanShrink = True
        Me.xr_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.0!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseBorders = False
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(147.0!, 69.0!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseBorders = False
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(121.0!, 69.0!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseBorders = False
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 69.0!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_city.StylePriority.UseBorders = False
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.0!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr2.StylePriority.UseBorders = False
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 14.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(129.0!, 24.0!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 125.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 125.0!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 2.0!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseBorders = False
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.StylePriority.UseForeColor = False
        Me.xr_so_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_so_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr1.StylePriority.UseBorders = False
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(530.0!, 17.0!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel8, Me.xrTable8, Me.xrPanel1, Me.xrLine7, Me.xrLine5})
        Me.ReportFooter.HeightF = 215.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(792.0!, 33.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseTextAlignment = False
        Me.xrLabel8.Text = "**  Please Note:  Information contained on this quote is subject to change.  Plea" & _
            "se verify with your salesperson current pricing information."
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrTable8
        '
        Me.xrTable8.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable8.LocationFloat = New DevExpress.Utils.PointFloat(542.0!, 8.0!)
        Me.xrTable8.Name = "xrTable8"
        Me.xrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow13, Me.xrTableRow14, Me.xrTableRow15, Me.xrTableRow16, Me.xrTableRow17})
        Me.xrTable8.SizeF = New System.Drawing.SizeF(250.0!, 105.0!)
        Me.xrTable8.StylePriority.UseBorders = False
        '
        'xrTableRow13
        '
        Me.xrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell42, Me.xr_subtotal})
        Me.xrTableRow13.Name = "xrTableRow13"
        Me.xrTableRow13.Weight = 1.0R
        '
        'xrTableCell42
        '
        Me.xrTableCell42.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell42.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell42.Name = "xrTableCell42"
        Me.xrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell42.StylePriority.UseBackColor = False
        Me.xrTableCell42.StylePriority.UseBorderColor = False
        Me.xrTableCell42.StylePriority.UseFont = False
        Me.xrTableCell42.StylePriority.UseTextAlignment = False
        Me.xrTableCell42.Text = "SubTotal:"
        Me.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell42.Weight = 1.08R
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 1.0899999999999999R
        '
        'xrTableRow14
        '
        Me.xrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell44, Me.xr_del})
        Me.xrTableRow14.Name = "xrTableRow14"
        Me.xrTableRow14.Weight = 1.0R
        '
        'xrTableCell44
        '
        Me.xrTableCell44.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell44.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell44.Name = "xrTableCell44"
        Me.xrTableCell44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell44.StylePriority.UseBackColor = False
        Me.xrTableCell44.StylePriority.UseBorderColor = False
        Me.xrTableCell44.StylePriority.UseFont = False
        Me.xrTableCell44.StylePriority.UseTextAlignment = False
        Me.xrTableCell44.Text = "Shipping:"
        Me.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell44.Weight = 1.08R
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 1.0899999999999999R
        '
        'xrTableRow15
        '
        Me.xrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell47, Me.xr_tax})
        Me.xrTableRow15.Name = "xrTableRow15"
        Me.xrTableRow15.Weight = 1.0R
        '
        'xrTableCell47
        '
        Me.xrTableCell47.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell47.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell47.Name = "xrTableCell47"
        Me.xrTableCell47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell47.StylePriority.UseBackColor = False
        Me.xrTableCell47.StylePriority.UseBorderColor = False
        Me.xrTableCell47.StylePriority.UseFont = False
        Me.xrTableCell47.StylePriority.UseTextAlignment = False
        Me.xrTableCell47.Text = "Tax:"
        Me.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell47.Weight = 1.08R
        '
        'xr_tax
        '
        Me.xr_tax.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_tax.StylePriority.UseBorderColor = False
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseTextAlignment = False
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax.Weight = 1.0899999999999999R
        '
        'xrTableRow16
        '
        Me.xrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell49, Me.xr_grand_total})
        Me.xrTableRow16.Name = "xrTableRow16"
        Me.xrTableRow16.Weight = 1.0R
        '
        'xrTableCell49
        '
        Me.xrTableCell49.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell49.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell49.Name = "xrTableCell49"
        Me.xrTableCell49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell49.StylePriority.UseBackColor = False
        Me.xrTableCell49.StylePriority.UseBorderColor = False
        Me.xrTableCell49.StylePriority.UseFont = False
        Me.xrTableCell49.StylePriority.UseTextAlignment = False
        Me.xrTableCell49.Text = "Quote Total:"
        Me.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell49.Weight = 1.08R
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 1.0899999999999999R
        '
        'xrTableRow17
        '
        Me.xrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_payment_holder})
        Me.xrTableRow17.Name = "xrTableRow17"
        Me.xrTableRow17.Weight = 1.0R
        '
        'xrPanel1
        '
        Me.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrLabel10})
        Me.xrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
        Me.xrPanel1.Name = "xrPanel1"
        Me.xrPanel1.SizeF = New System.Drawing.SizeF(533.0!, 125.0!)
        Me.xrPanel1.StylePriority.UseBorders = False
        '
        'xrLabel10
        '
        Me.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel10.StylePriority.UseBorders = False
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.SizeF = New System.Drawing.SizeF(792.0!, 2.0!)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 168.0417!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(792.0!, 2.0!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0.0R
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0.0R
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0.0R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0.0R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0.0R
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0.0R
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0.0R
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_store_cd, Me.xrTable6, Me.xrTable5, Me.xrTable4, Me.xrTable3, Me.xr_slsp2_hidden, Me.xr_ord_srt, Me.xr_ord_tp_hidden, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname})
        Me.GroupHeader1.HeightF = 242.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_store_cd
        '
        Me.xr_store_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_cd.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 125.0!)
        Me.xr_store_cd.Name = "xr_store_cd"
        Me.xr_store_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_cd.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_store_cd.StylePriority.UseFont = False
        Me.xr_store_cd.Text = "xr_lname"
        Me.xr_store_cd.Visible = False
        '
        'xrTable6
        '
        Me.xrTable6.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTable6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 217.0!)
        Me.xrTable6.Name = "xrTable6"
        Me.xrTable6.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow11})
        Me.xrTable6.SizeF = New System.Drawing.SizeF(791.0!, 25.0!)
        Me.xrTable6.StylePriority.UseBackColor = False
        Me.xrTable6.StylePriority.UseTextAlignment = False
        Me.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow11
        '
        Me.xrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell36, Me.xrTableCell41, Me.xrTableCell37, Me.xrTableCell40, Me.xrTableCell38, Me.xrTableCell39})
        Me.xrTableRow11.Name = "xrTableRow11"
        Me.xrTableRow11.Weight = 1.0R
        '
        'xrTableCell36
        '
        Me.xrTableCell36.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell36.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell36.Name = "xrTableCell36"
        Me.xrTableCell36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell36.StylePriority.UseBackColor = False
        Me.xrTableCell36.StylePriority.UseBorderColor = False
        Me.xrTableCell36.StylePriority.UseFont = False
        Me.xrTableCell36.StylePriority.UseTextAlignment = False
        Me.xrTableCell36.Text = "QTY"
        Me.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell36.Weight = 0.125R
        '
        'xrTableCell41
        '
        Me.xrTableCell41.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell41.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell41.Name = "xrTableCell41"
        Me.xrTableCell41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell41.StylePriority.UseBackColor = False
        Me.xrTableCell41.StylePriority.UseBorderColor = False
        Me.xrTableCell41.StylePriority.UseFont = False
        Me.xrTableCell41.StylePriority.UseTextAlignment = False
        Me.xrTableCell41.Text = "SKU"
        Me.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell41.Weight = 0.34625000000000011R
        '
        'xrTableCell37
        '
        Me.xrTableCell37.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell37.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell37.Name = "xrTableCell37"
        Me.xrTableCell37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell37.StylePriority.UseBackColor = False
        Me.xrTableCell37.StylePriority.UseBorderColor = False
        Me.xrTableCell37.StylePriority.UseFont = False
        Me.xrTableCell37.StylePriority.UseTextAlignment = False
        Me.xrTableCell37.Text = "Description"
        Me.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell37.Weight = 1.3475R
        '
        'xrTableCell40
        '
        Me.xrTableCell40.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell40.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell40.Name = "xrTableCell40"
        Me.xrTableCell40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell40.StylePriority.UseBackColor = False
        Me.xrTableCell40.StylePriority.UseBorderColor = False
        Me.xrTableCell40.StylePriority.UseFont = False
        Me.xrTableCell40.StylePriority.UseTextAlignment = False
        Me.xrTableCell40.Text = "Serial Number"
        Me.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell40.Weight = 0.49625R
        '
        'xrTableCell38
        '
        Me.xrTableCell38.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell38.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell38.Name = "xrTableCell38"
        Me.xrTableCell38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell38.StylePriority.UseBackColor = False
        Me.xrTableCell38.StylePriority.UseBorderColor = False
        Me.xrTableCell38.StylePriority.UseFont = False
        Me.xrTableCell38.StylePriority.UseTextAlignment = False
        Me.xrTableCell38.Text = "Unit Price"
        Me.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell38.Weight = 0.34625000000000006R
        '
        'xrTableCell39
        '
        Me.xrTableCell39.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell39.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell39.Name = "xrTableCell39"
        Me.xrTableCell39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell39.StylePriority.UseBackColor = False
        Me.xrTableCell39.StylePriority.UseBorderColor = False
        Me.xrTableCell39.StylePriority.UseFont = False
        Me.xrTableCell39.StylePriority.UseTextAlignment = False
        Me.xrTableCell39.Text = "Total"
        Me.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell39.Weight = 0.305R
        '
        'xrTable5
        '
        Me.xrTable5.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable5.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 8.0!)
        Me.xrTable5.Name = "xrTable5"
        Me.xrTable5.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow9, Me.xrTableRow10})
        Me.xrTable5.SizeF = New System.Drawing.SizeF(258.0!, 100.0!)
        Me.xrTable5.StylePriority.UseBorders = False
        '
        'xrTableRow9
        '
        Me.xrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell32, Me.xrTableCell33})
        Me.xrTableRow9.Name = "xrTableRow9"
        Me.xrTableRow9.Weight = 1.0R
        '
        'xrTableCell32
        '
        Me.xrTableCell32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell32.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel2})
        Me.xrTableCell32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell32.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell32.Name = "xrTableCell32"
        Me.xrTableCell32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell32.StylePriority.UseBackColor = False
        Me.xrTableCell32.StylePriority.UseFont = False
        Me.xrTableCell32.StylePriority.UseForeColor = False
        Me.xrTableCell32.Weight = 1.08R
        '
        'xrLabel2
        '
        Me.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel2.StylePriority.UseBorders = False
        Me.xrLabel2.Text = "Quote No.:"
        '
        'xrTableCell33
        '
        Me.xrTableCell33.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_del_doc_num})
        Me.xrTableCell33.Name = "xrTableCell33"
        Me.xrTableCell33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell33.Weight = 1.0899999999999999R
        '
        'xrTableRow10
        '
        Me.xrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell34, Me.xrTableCell35})
        Me.xrTableRow10.Name = "xrTableRow10"
        Me.xrTableRow10.Weight = 1.0R
        '
        'xrTableCell34
        '
        Me.xrTableCell34.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell34.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel3})
        Me.xrTableCell34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell34.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell34.Name = "xrTableCell34"
        Me.xrTableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell34.StylePriority.UseBackColor = False
        Me.xrTableCell34.StylePriority.UseFont = False
        Me.xrTableCell34.StylePriority.UseForeColor = False
        Me.xrTableCell34.Weight = 1.08R
        '
        'xrLabel3
        '
        Me.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel3.StylePriority.UseBorders = False
        Me.xrLabel3.Text = "Customer ID:"
        '
        'xrTableCell35
        '
        Me.xrTableCell35.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cust_cd})
        Me.xrTableCell35.Name = "xrTableCell35"
        Me.xrTableCell35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell35.Weight = 1.0899999999999999R
        '
        'xrTable4
        '
        Me.xrTable4.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTable4.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 158.0!)
        Me.xrTable4.Name = "xrTable4"
        Me.xrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow7, Me.xrTableRow8})
        Me.xrTable4.SizeF = New System.Drawing.SizeF(792.0!, 42.0!)
        Me.xrTable4.StylePriority.UseBorderColor = False
        Me.xrTable4.StylePriority.UseBorders = False
        '
        'xrTableRow7
        '
        Me.xrTableRow7.BackColor = System.Drawing.Color.Tan
        Me.xrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell20, Me.xrTableCell25, Me.xrTableCell21, Me.xrTableCell24, Me.xrTableCell22, Me.xrTableCell23})
        Me.xrTableRow7.Name = "xrTableRow7"
        Me.xrTableRow7.StylePriority.UseBackColor = False
        Me.xrTableRow7.Weight = 1.0R
        '
        'xrTableCell20
        '
        Me.xrTableCell20.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell20.Name = "xrTableCell20"
        Me.xrTableCell20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell20.StylePriority.UseBackColor = False
        Me.xrTableCell20.StylePriority.UseFont = False
        Me.xrTableCell20.StylePriority.UseTextAlignment = False
        Me.xrTableCell20.Text = "Date"
        Me.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell20.Weight = 0.5R
        '
        'xrTableCell25
        '
        Me.xrTableCell25.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell25.Name = "xrTableCell25"
        Me.xrTableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell25.StylePriority.UseBackColor = False
        Me.xrTableCell25.StylePriority.UseFont = False
        Me.xrTableCell25.StylePriority.UseTextAlignment = False
        Me.xrTableCell25.Text = "Order No."
        Me.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell25.Weight = 0.5R
        '
        'xrTableCell21
        '
        Me.xrTableCell21.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell21.Name = "xrTableCell21"
        Me.xrTableCell21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell21.StylePriority.UseBackColor = False
        Me.xrTableCell21.StylePriority.UseFont = False
        Me.xrTableCell21.StylePriority.UseTextAlignment = False
        Me.xrTableCell21.Text = "Ship Via"
        Me.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell21.Weight = 0.5R
        '
        'xrTableCell24
        '
        Me.xrTableCell24.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell24.Name = "xrTableCell24"
        Me.xrTableCell24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell24.StylePriority.UseBackColor = False
        Me.xrTableCell24.StylePriority.UseFont = False
        Me.xrTableCell24.StylePriority.UseTextAlignment = False
        Me.xrTableCell24.Text = "Ship Date"
        Me.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell24.Weight = 0.5R
        '
        'xrTableCell22
        '
        Me.xrTableCell22.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell22.Name = "xrTableCell22"
        Me.xrTableCell22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell22.StylePriority.UseBackColor = False
        Me.xrTableCell22.StylePriority.UseFont = False
        Me.xrTableCell22.StylePriority.UseTextAlignment = False
        Me.xrTableCell22.Text = "Terms"
        Me.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell22.Weight = 0.5R
        '
        'xrTableCell23
        '
        Me.xrTableCell23.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell23.Name = "xrTableCell23"
        Me.xrTableCell23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell23.StylePriority.UseBackColor = False
        Me.xrTableCell23.StylePriority.UseFont = False
        Me.xrTableCell23.StylePriority.UseTextAlignment = False
        Me.xrTableCell23.Text = "P.O."
        Me.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell23.Weight = 0.47R
        '
        'xrTableRow8
        '
        Me.xrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell26, Me.xr_sale_last_four, Me.xrTableCell28, Me.xrTableCell29, Me.xrTableCell30, Me.xr_po_num})
        Me.xrTableRow8.Name = "xrTableRow8"
        Me.xrTableRow8.Weight = 1.0R
        '
        'xrTableCell26
        '
        Me.xrTableCell26.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_so_wr_dt})
        Me.xrTableCell26.Multiline = True
        Me.xrTableCell26.Name = "xrTableCell26"
        Me.xrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell26.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrTableCell26.Weight = 0.5R
        '
        'xr_sale_last_four
        '
        Me.xr_sale_last_four.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sale_last_four.Name = "xr_sale_last_four"
        Me.xr_sale_last_four.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_sale_last_four.StylePriority.UseFont = False
        Me.xr_sale_last_four.StylePriority.UseTextAlignment = False
        Me.xr_sale_last_four.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sale_last_four.Weight = 0.5R
        '
        'xrTableCell28
        '
        Me.xrTableCell28.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pd})
        Me.xrTableCell28.Name = "xrTableCell28"
        Me.xrTableCell28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell28.Weight = 0.5R
        '
        'xrTableCell29
        '
        Me.xrTableCell29.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pu_del_dt})
        Me.xrTableCell29.Name = "xrTableCell29"
        Me.xrTableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell29.Weight = 0.5R
        '
        'xrTableCell30
        '
        Me.xrTableCell30.Name = "xrTableCell30"
        Me.xrTableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell30.StylePriority.UseTextAlignment = False
        Me.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell30.Weight = 0.5R
        '
        'xr_po_num
        '
        Me.xr_po_num.Name = "xr_po_num"
        Me.xr_po_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_po_num.Weight = 0.47R
        '
        'xrTable3
        '
        Me.xrTable3.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.xrTable3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
        Me.xrTable3.Name = "xrTable3"
        Me.xrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow5, Me.xrTableRow6})
        Me.xrTable3.SizeF = New System.Drawing.SizeF(500.0!, 142.0!)
        Me.xrTable3.StylePriority.UseBorderColor = False
        Me.xrTable3.StylePriority.UseBorders = False
        '
        'xrTableRow5
        '
        Me.xrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell16})
        Me.xrTableRow5.Name = "xrTableRow5"
        Me.xrTableRow5.Weight = 0.55185659411011523R
        '
        'xrTableCell16
        '
        Me.xrTableCell16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell16.Name = "xrTableCell16"
        Me.xrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell16.StylePriority.UseBackColor = False
        Me.xrTableCell16.StylePriority.UseFont = False
        Me.xrTableCell16.StylePriority.UseForeColor = False
        Me.xrTableCell16.Text = "Customer Information:"
        Me.xrTableCell16.Weight = 5.0R
        '
        'xrTableRow6
        '
        Me.xrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell18})
        Me.xrTableRow6.Name = "xrTableRow6"
        Me.xrTableRow6.Weight = 3.99359795134443R
        '
        'xrTableCell18
        '
        Me.xrTableCell18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_corp_name, Me.xr_b_phone, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_addr1, Me.xr_full_name})
        Me.xrTableCell18.Name = "xrTableCell18"
        Me.xrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell18.Text = "xrTableCell18"
        Me.xrTableCell18.Weight = 5.0R
        '
        'xr_corp_name
        '
        Me.xr_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_corp_name.CanShrink = True
        Me.xr_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_corp_name.Name = "xr_corp_name"
        Me.xr_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_corp_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_corp_name.StylePriority.UseBorders = False
        Me.xr_corp_name.StylePriority.UseFont = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_full_name.CanShrink = True
        Me.xr_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseBorders = False
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 150.0!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 150.0!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 142.0!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPageInfo2})
        Me.PageFooter.HeightF = 65.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 39.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 25.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'TurnersQuote
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(29, 29, 39, 25)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "REL_NO")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "WR_DT", "{0:MM/dd/yyyy}")
        xr_corp_name.DataBindings.Add("Text", DataSource, "CORP_NAME")
        xr_fname.DataBindings.Add("Text", DataSource, "FNAME")
        xr_lname.DataBindings.Add("Text", DataSource, "LNAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "CITY")
        xr_state.DataBindings.Add("Text", DataSource, "ST")
        xr_zip.DataBindings.Add("Text", DataSource, "ZIP")
        xr_h_phone.DataBindings.Add("Text", DataSource, "HPHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "BPHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "CUST_CD")
        xr_qty.DataBindings.Add("Text", DataSource, "QTY")
        xr_sku.DataBindings.Add("Text", DataSource, "ITM_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "VSN")
        xr_des.DataBindings.Add("Text", DataSource, "DES")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SLSP1")
        xr_ret.DataBindings.Add("Text", DataSource, "RET_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "EXT_PRC", "{0:0.00}")
        xr_fname.DataBindings.Add("Text", DataSource, "FNAME")
        xr_line_no.DataBindings.Add("Text", DataSource, "LINE")
        xr_comments.DataBindings.Add("Text", DataSource, "COMMENTS")
        xr_store_cd.DataBindings.Add("Text", DataSource, "STORE_CD")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "Pick-Up"
            Case "D"
                sender.text = "Delivery"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        If IsNumeric(xr_qty.Text) And IsNumeric(xr_ret.Text) Then
            sender.text = FormatNumber(xr_qty.Text * xr_ret.Text, 2)
            subtotal = subtotal + (xr_qty.Text * xr_ret.Text)
        End If

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_store_addr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr.BeforePrint

        xr_store_addr.Text = StrConv(xr_store_addr.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = StrConv(xr_store_name.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2_hidden.BeforePrint

        If sender.text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME, EMP.ADDR2, EMP.EMAIL_ADDR "
            sql = sql & "FROM EMP WHERE EMP_INIT='" & xr_slsp2_hidden.Text & "' "

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read Then
                    xr_slsp2.Text = MyDataReader2.Item("FULL_NAME").ToString
                    xr_slsp_ext.Text = MyDataReader2.Item("ADDR2").ToString
                    xr_slsp_email.Text = MyDataReader2.Item("EMAIL_ADDR").ToString.ToLower
                End If
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Private Sub xr_sale_last_four_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_sale_last_four.BeforePrint

        If Len(xr_del_doc_num.Text) = 11 Then
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 4)
        Else
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 5)
        End If

    End Sub

    Private Sub xr_store_cd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_cd.BeforePrint

        If sender.Text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT STORE.STORE_NAME, 'Tel. ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 || ', ' || STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS STORE_ADDRESS "
            sql = sql & "FROM STORE, STORE_PHONE WHERE STORE.STORE_CD='" & sender.Text & "' "
            sql = sql & "AND STORE.STORE_CD = STORE_PHONE.STORE_CD (+) "
            sql = sql & "AND STORE_PHONE.PHONE_TP_CD(+)='PH' "

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_store_name.Text = MyDataReader2.Item("STORE_NAME").ToString
                    xr_store_phone.Text = MyDataReader2.Item("STORE_PHONE").ToString
                    xr_store_addr.Text = MyDataReader2.Item("STORE_ADDRESS").ToString
                Loop
                conn.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Private Sub xrLabel8_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        sender.text = sender.text & "This estimate is valid for 30 days from estimate date.   " & vbCrLf

    End Sub

    Private Sub xr_long_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_long_desc.BeforePrint

        If xr_line_no.Text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.Open()

            sql = "SELECT * FROM RELATIONSHIP_LINES_DESC where REL_NO='" & xr_del_doc_num.Text & "' AND LINE='" & xr_line_no.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read Then
                    sender.text = MyDataReader2.Item("DESCRIPTION").ToString
                Else
                    sender.text = ""
                End If
                conn.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

    End Sub

End Class
