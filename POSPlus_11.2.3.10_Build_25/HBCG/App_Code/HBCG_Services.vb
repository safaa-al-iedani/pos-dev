Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports System.Linq

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Inventory_Lookup
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function Retrieve_SKU(ByVal SKU As String) As DataSet

        Dim SQL As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Grab SKU information to display to the user
        SQL = "SELECT ITM.ITM_CD, ITM.VE_CD, ITM.RET_PRC, ITM.VSN, ITM.DES, ITM.FINISH, ITM.ITM_TP_CD "
        SQL = SQL & "FROM ITM WHERE ITM.ITM_CD=:ITM_CD"

        oAdp = Nothing
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
        objSql.Parameters(":ITM_CD").Value = SKU

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            If MyDataReader.Read() Then
                Return ds
            Else
                ds.Clear()
                Return ds
            End If
            'Close Connection 
            conn.Close()
            ' close the already open datareader
            MyDataReader.Close()
            objSql.Cancel()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            ds.Clear()
            Return ds
        End Try

    End Function

    <WebMethod()> _
    Public Function Retrieve_Customer(ByVal CUST_CD As String) As DataSet

        Dim SQL As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'Grab SKU information to display to the user
        SQL = "SELECT * "
        SQL = SQL & "FROM CUST WHERE CUST_CD=:CUST_CD"

        oAdp = Nothing
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
        objSql.Parameters(":CUST_CD").Value = CUST_CD

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            If MyDataReader.Read() Then
                Return ds
            Else
                ds.Clear()
                Return ds
            End If
            'Close Connection 
            conn.Close()
            ' close the already open datareader
            MyDataReader.Close()
            objSql.Cancel()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            ds.Clear()
            conn.Close()
            Return ds
        End Try

    End Function

    ''' <summary>
    ''' This web method will store all selected warranty(s) details to a session
    ''' </summary>
    ''' <param name="ctrlName">the Id of the selected radio button</param>
    ''' <param name="warrItmCd">selected warranty item code</param>
    ''' <param name="warrItmText">selected warranty item text</param>
    ''' <param name="sourcePage">Source Page</param>
    ''' <returns>List of containing data for Link Warranty popup</returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)> _
    Public Function AddWarranty(ctrlName As String, warrItmCd As String, warrItmText As String) As Object
        Dim linkwarr2skus = Nothing

        If warrItmCd.Equals("W") Then warrItmCd = String.Empty

        Dim strClientId() As String = ctrlName.Split("_")

        Dim itmCd As String = strClientId(1)
        Dim rowNo As String = strClientId(2)

        If Not IsNothing(HttpContext.Current.Session("WARRINFO")) Then
            Dim dtWarr As DataTable = HttpContext.Current.Session("WARRINFO")
            Dim drFilter() As DataRow = dtWarr.Select("ITM_CD = '" + itmCd + "' AND ROW_ID = '" + rowNo + "'")

            Dim warRowNo As String = String.Empty

            If Not strClientId(0).ToLower().Contains("rdoprevwarr") Then
                Dim drWarrRowId() As DataRow = dtWarr.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" + warrItmCd + "'")
                warRowNo = IIf(drWarrRowId.Length > 0, drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID"), String.Empty)
            ElseIf drFilter(0)("WARR_ITM_CD").Equals(warrItmCd) Then
                warRowNo = drFilter(0)("WARR_ROW_ID")
            End If

            drFilter(0)("WARR_ITM_CD") = IIf(String.IsNullOrEmpty(warrItmCd), DBNull.Value, warrItmCd)
            drFilter(0)("WARR_ITM_TEXT") = IIf(String.IsNullOrEmpty(warrItmCd), DBNull.Value, warrItmText)
            drFilter(0)("WARR_ROW_ID") = IIf(String.IsNullOrEmpty(warrItmCd), DBNull.Value, warRowNo)
            drFilter(0)("IS_PREV_SEL") = IIf(String.IsNullOrEmpty(warrItmCd), False, Not strClientId(0).ToLower().Contains("rdoprevwarr"))

            HttpContext.Current.Session("WARRINFO") = dtWarr
            Dim enteredSKUs As String = "'" & String.Join("','", From itm In dtWarr.Select("ROW_ID <> '" + rowNo + "'") Select itm.Field(Of String)("ITM_CD")) & "'"

            If Not IsNothing(warrItmCd) AndAlso strClientId(0).IndexOf("rdoPrevWarr") >= 0 Then
                Dim objSalesBiz As New SalesBiz()
                Dim dtSKUs As DataTable = objSalesBiz.GetItemsForWarranty(warrItmCd, enteredSKUs)

                linkwarr2skus = (From dtw In dtWarr.AsEnumerable
                          Join dts In dtSKUs.AsEnumerable
                          On dtw.Field(Of String)("ITM_CD") Equals dts.Field(Of String)("ITM_CD")
                          Where dtw.Field(Of String)("ROW_ID") <> rowNo
                          Select New With {Key .ROW_ID = dtw.Field(Of String)("ROW_ID"), _
                                           Key .ITM_CD = dts.Field(Of String)("ITM_CD"), _
                                           Key .ITEM = dts.Field(Of String)("ITEM")}).ToList()

                HttpContext.Current.Session("LINKWARR2SKUS") = linkwarr2skus

            End If
        End If

        Return linkwarr2skus
    End Function
    <WebMethod()> _
    Public Function btn_reloadE1Syspm_Click() As Boolean
        Try
            Dim theSystemBiz As New SystemBiz()
            theSystemBiz.LoadSysPms()
            System.Web.HttpRuntime.UnloadAppDomain()
        Catch ex As Exception
            Throw ex
        End Try
        Return True
    End Function
   
End Class
