Public Class PICR
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim comments_processed As Boolean
    Private WithEvents xrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_stat_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_barcode As DevExpress.XtraReports.UI.XRBarCode
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xrLine6 As DevExpress.XtraReports.UI.XRLine
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "PICR.resx"
        Dim code39Generator1 As DevExpress.XtraPrinting.BarCode.Code39Generator = New DevExpress.XtraPrinting.BarCode.Code39Generator
        Dim xrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xr_so_barcode = New DevExpress.XtraReports.UI.XRBarCode
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.xrLabel26 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel28 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.xr_stat_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.xrLine6 = New DevExpress.XtraReports.UI.XRLine
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLine6, Me.xr_SKU, Me.xr_disc_amt, Me.xr_void_flag, Me.xr_desc, Me.xr_loc, Me.xr_store, Me.xr_vsn, Me.xr_qty})
        Me.Detail.HeightF = 47.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_SKU
        '
        Me.xr_SKU.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_SKU.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xr_SKU.StylePriority.UseFont = False
        Me.xr_SKU.Text = "SKU"
        Me.xr_SKU.WordWrap = False
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 25.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 25.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'xr_desc
        '
        Me.xr_desc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_desc.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 18.0!)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.SizeF = New System.Drawing.SizeF(400.0!, 17.0!)
        Me.xr_desc.StylePriority.UseFont = False
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_loc
        '
        Me.xr_loc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc.LocationFloat = New DevExpress.Utils.PointFloat(645.125!, 0.0!)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.SizeF = New System.Drawing.SizeF(63.87494!, 17.00001!)
        Me.xr_loc.StylePriority.UseFont = False
        Me.xr_loc.Text = "LOC"
        '
        'xr_store
        '
        Me.xr_store.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 0.0!)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_store.StylePriority.UseFont = False
        Me.xr_store.Text = "ST"
        '
        'xr_vsn
        '
        Me.xr_vsn.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.SizeF = New System.Drawing.SizeF(400.0!, 17.0!)
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.Text = "VSN"
        '
        'xr_qty
        '
        Me.xr_qty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.Text = "QTY"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_so_barcode, Me.xrLabel10, Me.xrLabel4, Me.xr_store_name, Me.xrLine2})
        Me.PageHeader.HeightF = 59.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_so_barcode
        '
        Me.xr_so_barcode.AutoModule = True
        Me.xr_so_barcode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 9.000015!)
        Me.xr_so_barcode.Name = "xr_so_barcode"
        Me.xr_so_barcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.xr_so_barcode.ShowText = False
        Me.xr_so_barcode.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
        code39Generator1.CalcCheckSum = False
        code39Generator1.WideNarrowRatio = 3.0!
        Me.xr_so_barcode.Symbology = code39Generator1
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.xrLabel10.ForeColor = System.Drawing.Color.Black
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(299.0!, 34.00002!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(409.9999!, 17.00001!)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel4
        '
        Me.xrLabel4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.ForeColor = System.Drawing.Color.Black
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 17.00001!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(410.0!, 17.00001!)
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseForeColor = False
        Me.xrLabel4.Text = "WAREHOUSE PICK SLIP"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_store_name
        '
        Me.xr_store_name.Font = New System.Drawing.Font("Calibri", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.Black
        Me.xr_store_name.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.0!)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.SizeF = New System.Drawing.SizeF(410.0!, 17.00001!)
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine2
        '
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LineWidth = 2
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.99999!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(708.9999!, 7.999992!)
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(555.9999!, 0.0!)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.SizeF = New System.Drawing.SizeF(154.0!, 17.0!)
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.StylePriority.UseForeColor = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 33.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "CUSTOMER:"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine4
        '
        Me.xrLine4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 135.0!)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.SizeF = New System.Drawing.SizeF(710.0!, 8.0!)
        '
        'xrLine3
        '
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 108.0!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(710.0!, 7.999992!)
        '
        'xrLabel18
        '
        Me.xrLabel18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel18.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(645.125!, 117.0!)
        Me.xrLabel18.Name = "xrLabel18"
        Me.xrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel18.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xrLabel18.StylePriority.UseFont = False
        Me.xrLabel18.StylePriority.UseForeColor = False
        Me.xrLabel18.Text = "LOC"
        '
        'xrLabel17
        '
        Me.xrLabel17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel17.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 117.0!)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xrLabel17.StylePriority.UseFont = False
        Me.xrLabel17.StylePriority.UseForeColor = False
        Me.xrLabel17.Text = "ST"
        '
        'xrLabel14
        '
        Me.xrLabel14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel14.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 118.0!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseForeColor = False
        Me.xrLabel14.Text = "SKU"
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(317.0!, 118.0!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseForeColor = False
        Me.xrLabel13.Text = "DESCRIPTION"
        '
        'xrLabel12
        '
        Me.xrLabel12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel12.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 117.0!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseForeColor = False
        Me.xrLabel12.Text = "QTY"
        '
        'xr_slsp
        '
        Me.xr_slsp.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(534.9999!, 71.99999!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(175.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 71.99999!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "SALES ASSOCIATE:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pd
        '
        Me.xr_pd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.DimGray
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(509.9999!, 50.99999!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 50.99999!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(509.9999!, 34.00002!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseForeColor = False
        Me.xrLabel6.Text = "SALES DATE:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 17.00001!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(509.9999!, 17.00001!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseForeColor = False
        Me.xrLabel5.Text = "ACCOUNT #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 52.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(74.0!, 16.0!)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseForeColor = False
        Me.xrLabel2.Text = "HOME TEL:"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 68.0!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel3.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.StylePriority.UseForeColor = False
        Me.xrLabel3.Text = "ALT TEL:"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 51.0!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(289.0!, 34.0!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 34.0!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 34.0!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(284.0!, 17.0!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 0.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 33.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 33.0!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 34.00002!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 17.0!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLine5, Me.xrLine1, Me.xrLabel9, Me.xrLabel8, Me.xrLine7, Me.xrLabel19, Me.xr_subtotal})
        Me.ReportFooter.HeightF = 138.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrLine5
        '
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 108.0!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(183.0!, 25.0!)
        '
        'xrLine1
        '
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 42.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(183.0!, 25.0!)
        '
        'xrLabel9
        '
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 108.0!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "VERIFIED BY:"
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 42.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.Text = "PICKED BY:"
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.SizeF = New System.Drawing.SizeF(709.4167!, 9.999974!)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xrLabel19
        '
        Me.xrLabel19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel19.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(377.4167!, 9.999974!)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
        Me.xrLabel19.StylePriority.UseFont = False
        Me.xrLabel19.StylePriority.UseForeColor = False
        Me.xrLabel19.Text = "TOTAL MERCHANDISE VALUE"
        Me.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_subtotal
        '
        Me.xr_subtotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.ForeColor = System.Drawing.Color.DimGray
        Me.xr_subtotal.LocationFloat = New DevExpress.Utils.PointFloat(592.4167!, 9.999974!)
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseForeColor = False
        xrSummary1.FormatString = "{0:#.00}"
        xrSummary1.IgnoreNullValues = True
        Me.xr_subtotal.Summary = xrSummary1
        Me.xr_subtotal.Text = "xr_subtotal"
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xrLabel26
        '
        Me.xrLabel26.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel26.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xrLabel26.Name = "xrLabel26"
        Me.xrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel26.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel26.StylePriority.UseFont = False
        Me.xrLabel26.StylePriority.UseForeColor = False
        Me.xrLabel26.Text = "ADDRESS:"
        Me.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel28
        '
        Me.xrLabel28.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel28.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
        Me.xrLabel28.Name = "xrLabel28"
        Me.xrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel28.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel28.StylePriority.UseFont = False
        Me.xrLabel28.StylePriority.UseForeColor = False
        Me.xrLabel28.Text = "CITY/ST/ZIP:"
        Me.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_stat_cd, Me.xr_slsp2_hidden, Me.xr_slsp2, Me.xr_ord_srt, Me.xr_ord_tp, Me.xr_ord_tp_hidden, Me.xr_full_name, Me.xrLabel28, Me.xrLabel26, Me.xr_disc_cd, Me.xrLabel1, Me.xrLine4, Me.xrLine3, Me.xrLabel18, Me.xrLabel17, Me.xrLabel14, Me.xrLabel13, Me.xrLabel12, Me.xr_slsp, Me.xrLabel7, Me.xr_pd, Me.xr_pu_del_dt, Me.xrLabel6, Me.xr_cust_cd, Me.xrLabel5, Me.xrLabel2, Me.xr_b_phone, Me.xrLabel3, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_del_doc_num, Me.xr_lname, Me.xr_addr1, Me.xr_so_wr_dt, Me.xr_fname})
        Me.GroupHeader1.HeightF = 143.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_stat_cd
        '
        Me.xr_stat_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_stat_cd.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 42.0!)
        Me.xr_stat_cd.Name = "xr_stat_cd"
        Me.xr_stat_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_stat_cd.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_stat_cd.StylePriority.UseFont = False
        Me.xr_stat_cd.Visible = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 67.0!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_slsp2
        '
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(535.0!, 89.0!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(175.0!, 17.0!)
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 58.0!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(487.0833!, 0.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(122.9167!, 17.0!)
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES ORDER #:"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 50.0!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(325.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPageInfo2})
        Me.PageFooter.HeightF = 17.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 70.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 70.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'xrLine6
        '
        Me.xrLine6.LocationFloat = New DevExpress.Utils.PointFloat(0.9999116!, 39.0!)
        Me.xrLine6.Name = "xrLine6"
        Me.xrLine6.SizeF = New System.Drawing.SizeF(708.0!, 3.125!)
        '
        'PICR
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(70, 70, 70, 70)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "10.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "CUST.FNAME")
        xr_lname.DataBindings.Add("Text", DataSource, "CUST.LNAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "CUST.ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_SKU.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_store.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_desc.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_stat_cd.DataBindings.Add("Text", DataSource, "SO.STAT_CD")
        xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "PICKUP:"
            Case "D"
                sender.text = "DELIVERY:"
        End Select

    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint

        'If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
        If xr_void_flag.Text = "Y" Then
            sender.text = "0.00"
        End If
        subtotal = subtotal + (xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint

        sender.text = FormatCurrency(subtotal, 2)

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES ORDER #:"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO #:"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT #:"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT #:"
        End Select

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = StrConv(xr_store_name.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2.BeforePrint

        If xr_slsp2_hidden.Text & "" <> "" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME "
            sql = sql & "FROM SO, EMP WHERE SO.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' "
            sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD2  "

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("FULL_NAME").ToString
                Loop
            Catch
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

    End Sub

    Private Sub xrLabel10_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrLabel10.BeforePrint

        sender.text = FormatDateTime(Now, DateFormat.LongDate) & FormatDateTime(Now, DateFormat.LongTime)

    End Sub

    Private Sub xr_so_barcode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_so_barcode.BeforePrint

        If Len(xr_del_doc_num.Text) >= 11 Then
            xr_so_barcode.Text = xr_del_doc_num.Text
        End If

    End Sub
End Class