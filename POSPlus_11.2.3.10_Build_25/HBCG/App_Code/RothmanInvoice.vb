Imports DevExpress.XtraReports.UI
Imports System.Drawing.Printing

Public Class RothmanInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim g_pmtTotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Dim comments_processed As Boolean
    Dim triggers_processed As Boolean
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle

    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand

    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand

    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents xrPgHdrTbl1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrPgHdrTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrPgHdrLogoTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPgHdrTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_so_store_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPgHdrLbl1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_city_st_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_seq_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_billToSt As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_billToSt_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tax_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_corp_name As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xrShipToLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrDocNumCustCdTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrDocNumTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCustCdTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrCustCdTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrSoLnTtlTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSoLnTtlTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrSoLnQtyTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnDesTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnUnitPrcTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnExtPrcTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnSerNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrSoLnItmTtlTcel As DevExpress.XtraReports.UI.XRTableCell

    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrSoLnTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSoLnLine1Trow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pre_disc_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vend As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    ' Page number info

    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xrCmntPanl As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xrBorderPanl As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrBalanceDetailTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrSubTotalTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrSubTotalTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTransChrgsTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTransChrgsTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTotOrderTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTotOrderTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPmtTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrPmtTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTotPmtsTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrPmtTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrFinalDetailTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrBalanceTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrBalanceTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRTableCell

    Private WithEvents xrPrtNameTtlLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCustSigTtlNLnLbl As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTermNCondTtlLbl As DevExpress.XtraReports.UI.XRLabel

    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine  ' spacing ?
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel  ' spacing ?
    Private WithEvents xrCustTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrCustInfoTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrShipToTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrDocNumTrow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrDocNumTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrCustCdTtlTcel As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTaxLbl As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTaxBaseInfo As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_setup As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_ord_srt_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLogoPicBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents winControlContainer1 As DevExpress.XtraReports.UI.WinControlContainer
    Private WithEvents richTextBox1 As System.Windows.Forms.RichTextBox

    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    'Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    'Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    'Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell

    'Required by the Designer
    Private components As System.ComponentModel.IContainer
    Private resourceFileName As String
    Private resourc As System.Resources.ResourceManager

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "RothmanInvoice.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.RothmanInvoice.ResourceManager
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrPgHdrTbl1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrPgHdrTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrPgHdrLogoTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLogoPicBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xrPgHdrTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_store_city_st_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_seq_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_store_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrShipToLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPgHdrLbl1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrSoLnTtlTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSoLnTtlTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrSoLnQtyTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnItmTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnDesTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnSerNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnUnitPrcTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrSoLnExtPrcTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrDocNumCustCdTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrDocNumTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrDocNumTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCustCdTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrCustCdTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCustTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrCustInfoTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrShipToTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCustCdTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrDocNumTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrSoLnTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSoLnLine1Trow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_loc = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vend = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_des = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_pre_disc_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xrBorderPanl = New DevExpress.XtraReports.UI.XRPanel()
        Me.xrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xr_billToSt_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_tax_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPrtNameTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrBalanceDetailTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrSubTotalTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrSubTotalTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTaxLbl = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTaxBaseInfo = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTransChrgsTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTransChrgsTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_setup = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTotOrderTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTotOrderTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPmtTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTotPmtsTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_mop = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrFinalDetailTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrBalanceTrow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrBalanceTtlTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_ord_srt_des = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrCmntPanl = New DevExpress.XtraReports.UI.XRPanel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCustSigTtlNLnLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTermNCondTtlLbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.winControlContainer1 = New DevExpress.XtraReports.UI.WinControlContainer()
        Me.richTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.xrPmtTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrPmtTcel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_billToSt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.xrPgHdrTbl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrSoLnTtlTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrDocNumCustCdTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrCustTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrSoLnTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrBalanceDetailTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrFinalDetailTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 25.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPgHdrTbl1, Me.xrLine3})
        Me.PageHeader.HeightF = 108.1251!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPgHdrTbl1
        '
        Me.xrPgHdrTbl1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPgHdrTbl1.LocationFloat = New DevExpress.Utils.PointFloat(0.00006357829!, 0.0!)
        Me.xrPgHdrTbl1.Name = "xrPgHdrTbl1"
        Me.xrPgHdrTbl1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrPgHdrTrow})
        Me.xrPgHdrTbl1.SizeF = New System.Drawing.SizeF(1022.208!, 100.5833!)
        Me.xrPgHdrTbl1.StylePriority.UseBorders = False
        '
        'xrPgHdrTrow
        '
        Me.xrPgHdrTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrPgHdrLogoTcel, Me.xrPgHdrTcel})
        Me.xrPgHdrTrow.Name = "xrPgHdrTrow"
        Me.xrPgHdrTrow.Weight = 1.0R
        '
        'xrPgHdrLogoTcel
        '
        Me.xrPgHdrLogoTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLogoPicBox1})
        Me.xrPgHdrLogoTcel.Name = "xrPgHdrLogoTcel"
        Me.xrPgHdrLogoTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPgHdrLogoTcel.Text = "xrPgHdrLogo1"
        Me.xrPgHdrLogoTcel.Weight = 3.1322016653735312R
        '
        'xrLogoPicBox1
        '
        Me.xrLogoPicBox1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLogoPicBox1.Image = CType(resources.GetObject("xrLogoPicBox1.Image"), System.Drawing.Image)
        Me.xrLogoPicBox1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 3.499985!)
        Me.xrLogoPicBox1.Name = "xrLogoPicBox1"
        Me.xrLogoPicBox1.SizeF = New System.Drawing.SizeF(298.1874!, 93.95828!)
        Me.xrLogoPicBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        Me.xrLogoPicBox1.StylePriority.UseBorders = False
        '
        'xrPgHdrTcel
        '
        Me.xrPgHdrTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_store_city_st_zip, Me.xr_so_seq_num, Me.xrLabel3, Me.xr_store_phone, Me.xr_store_addr1, Me.xr_so_store_cd, Me.xr_ord_tp, Me.xr_so_wr_dt, Me.xrLine1})
        Me.xrPgHdrTcel.Name = "xrPgHdrTcel"
        Me.xrPgHdrTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPgHdrTcel.Text = "xrPgHdrTcel"
        Me.xrPgHdrTcel.Weight = 7.1202062988281254R
        '
        'xr_store_city_st_zip
        '
        Me.xr_store_city_st_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_city_st_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.xr_store_city_st_zip.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_city_st_zip.LocationFloat = New DevExpress.Utils.PointFloat(14.81256!, 20.49999!)
        Me.xr_store_city_st_zip.Name = "xr_store_city_st_zip"
        Me.xr_store_city_st_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_city_st_zip.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xr_store_city_st_zip.StylePriority.UseBorders = False
        Me.xr_store_city_st_zip.StylePriority.UseFont = False
        Me.xr_store_city_st_zip.StylePriority.UseForeColor = False
        Me.xr_store_city_st_zip.StylePriority.UseTextAlignment = False
        Me.xr_store_city_st_zip.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_so_seq_num
        '
        Me.xr_so_seq_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_seq_num.CanGrow = False
        Me.xr_so_seq_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_seq_num.LocationFloat = New DevExpress.Utils.PointFloat(333.8331!, 34.99994!)
        Me.xr_so_seq_num.Name = "xr_so_seq_num"
        Me.xr_so_seq_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_seq_num.SizeF = New System.Drawing.SizeF(74.66681!, 16.99998!)
        Me.xr_so_seq_num.StylePriority.UseBorders = False
        Me.xr_so_seq_num.StylePriority.UseFont = False
        Me.xr_so_seq_num.StylePriority.UseTextAlignment = False
        Me.xr_so_seq_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel3
        '
        Me.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel3.ForeColor = System.Drawing.Color.Black
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(431.8125!, 10.00001!)
        Me.xrLabel3.Multiline = True
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(268.1019!, 81.99999!)
        Me.xrLabel3.StylePriority.UseBorders = False
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.StylePriority.UseForeColor = False
        Me.xrLabel3.StylePriority.UsePadding = False
        Me.xrLabel3.StylePriority.UseTextAlignment = False
        Me.xrLabel3.Text = "AFTER DELIVERY/PICKUP: FOR WARRANTY OR SERVICE CONTACT: 636-696-4222          800" & _
    "-788-6776"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.xr_store_phone.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_phone.LocationFloat = New DevExpress.Utils.PointFloat(14.81253!, 37.5!)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xr_store_phone.StylePriority.UseBorders = False
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_addr1
        '
        Me.xr_store_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.xr_store_addr1.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_addr1.LocationFloat = New DevExpress.Utils.PointFloat(14.81256!, 3.499985!)
        Me.xr_store_addr1.Name = "xr_store_addr1"
        Me.xr_store_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr1.SizeF = New System.Drawing.SizeF(291.0!, 17.0!)
        Me.xr_store_addr1.StylePriority.UseBorders = False
        Me.xr_store_addr1.StylePriority.UseFont = False
        Me.xr_store_addr1.StylePriority.UseForeColor = False
        Me.xr_store_addr1.StylePriority.UseTextAlignment = False
        Me.xr_store_addr1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_so_store_cd
        '
        Me.xr_so_store_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_store_cd.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 10.0!)
        Me.xr_so_store_cd.Name = "xr_so_store_cd"
        Me.xr_so_store_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_store_cd.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_so_store_cd.StylePriority.UseFont = False
        Me.xr_so_store_cd.Visible = False
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(14.8126!, 68.00003!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(417.0!, 33.0!)
        Me.xr_ord_tp.StylePriority.UseBorders = False
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.StylePriority.UseTextAlignment = False
        Me.xr_ord_tp.Text = "SALES TICKET"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.xr_so_wr_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(322.7064!, 3.499985!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseBorders = False
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.StylePriority.UseForeColor = False
        Me.xr_so_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_so_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xrLine1
        '
        Me.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(14.81256!, 59.00002!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(407.8939!, 9.0!)
        Me.xrLine1.StylePriority.UseBorders = False
        Me.xrLine1.StylePriority.UseForeColor = False
        Me.xrLine1.Tag = "X"
        '
        'xrLine3
        '
        Me.xrLine3.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine3.LineWidth = 2
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 101.0!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(1023.958!, 5.124985!)
        Me.xrLine3.StylePriority.UseForeColor = False
        '
        'xrShipToLbl
        '
        Me.xrShipToLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrShipToLbl.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xrShipToLbl.LocationFloat = New DevExpress.Utils.PointFloat(1.999998!, 0.000004768372!)
        Me.xrShipToLbl.Name = "xrShipToLbl"
        Me.xrShipToLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrShipToLbl.SizeF = New System.Drawing.SizeF(68.00001!, 17.0!)
        Me.xrShipToLbl.StylePriority.UseBorders = False
        Me.xrShipToLbl.StylePriority.UseFont = False
        Me.xrShipToLbl.StylePriority.UseTextAlignment = False
        Me.xrShipToLbl.Text = "SOLD TO:"
        Me.xrShipToLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(14.81263!, 17.99997!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xrLabel7.StylePriority.UseBorders = False
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel4
        '
        Me.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel4.CanGrow = False
        Me.xrLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(337.7915!, 34.99991!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(74.66681!, 16.99998!)
        Me.xrLabel4.StylePriority.UseBorders = False
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseTextAlignment = False
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_slsp2
        '
        Me.xr_slsp2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(280.0!, 102.9998!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(190.0!, 17.00001!)
        Me.xr_slsp2.StylePriority.UseBorders = False
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseForeColor = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_slsp1
        '
        Me.xr_slsp1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp1.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp1.LocationFloat = New DevExpress.Utils.PointFloat(92.66661!, 102.9998!)
        Me.xr_slsp1.Name = "xr_slsp1"
        Me.xr_slsp1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp1.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp1.StylePriority.UseBorders = False
        Me.xr_slsp1.StylePriority.UseFont = False
        Me.xr_slsp1.StylePriority.UseForeColor = False
        Me.xr_slsp1.StylePriority.UseTextAlignment = False
        Me.xr_slsp1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrPgHdrLbl1
        '
        Me.xrPgHdrLbl1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPgHdrLbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.xrPgHdrLbl1.ForeColor = System.Drawing.Color.Black
        Me.xrPgHdrLbl1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 102.9998!)
        Me.xrPgHdrLbl1.Name = "xrPgHdrLbl1"
        Me.xrPgHdrLbl1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPgHdrLbl1.SizeF = New System.Drawing.SizeF(91.99994!, 17.0!)
        Me.xrPgHdrLbl1.StylePriority.UseBorders = False
        Me.xrPgHdrLbl1.StylePriority.UseFont = False
        Me.xrPgHdrLbl1.StylePriority.UseForeColor = False
        Me.xrPgHdrLbl1.StylePriority.UseTextAlignment = False
        Me.xrPgHdrLbl1.Text = "Salesperson:"
        Me.xrPgHdrLbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel15, Me.xrSoLnTtlTbl, Me.xrDocNumCustCdTbl, Me.xrCustTbl, Me.xr_slsp2_hidden, Me.xr_ord_srt, Me.xr_ord_tp_hidden, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname, Me.xrLine5, Me.xr_cust_cd})
        Me.GroupHeader1.HeightF = 166.1248!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xrLabel15
        '
        Me.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel15.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(134.0879!, 0.0!)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.SizeF = New System.Drawing.SizeF(363.9123!, 15.75999!)
        Me.xrLabel15.StylePriority.UseBorders = False
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.Text = " PLEASE KEEP THIS RECEIPT FOR YOUR RECORDS     "
        '
        'xrSoLnTtlTbl
        '
        Me.xrSoLnTtlTbl.BackColor = System.Drawing.Color.RosyBrown
        Me.xrSoLnTtlTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 141.1248!)
        Me.xrSoLnTtlTbl.Name = "xrSoLnTtlTbl"
        Me.xrSoLnTtlTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSoLnTtlTrow})
        Me.xrSoLnTtlTbl.SizeF = New System.Drawing.SizeF(1023.958!, 25.0!)
        Me.xrSoLnTtlTbl.StylePriority.UseBackColor = False
        Me.xrSoLnTtlTbl.StylePriority.UseTextAlignment = False
        Me.xrSoLnTtlTbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrSoLnTtlTrow
        '
        Me.xrSoLnTtlTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrSoLnQtyTtlTcel, Me.xrTableCell1, Me.xrSoLnItmTtlTcel, Me.xrSoLnDesTtlTcel, Me.xrSoLnSerNumTtlTcel, Me.xrSoLnUnitPrcTtlTcel, Me.xrSoLnExtPrcTtlTcel})
        Me.xrSoLnTtlTrow.Name = "xrSoLnTtlTrow"
        Me.xrSoLnTtlTrow.Weight = 1.0R
        '
        'xrSoLnQtyTtlTcel
        '
        Me.xrSoLnQtyTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnQtyTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnQtyTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnQtyTtlTcel.Name = "xrSoLnQtyTtlTcel"
        Me.xrSoLnQtyTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnQtyTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnQtyTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnQtyTtlTcel.Text = "QTY"
        Me.xrSoLnQtyTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnQtyTtlTcel.Weight = 0.15806893640328917R
        '
        'xrTableCell1
        '
        Me.xrTableCell1.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell1.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.StylePriority.UseBackColor = False
        Me.xrTableCell1.StylePriority.UseBorderColor = False
        Me.xrTableCell1.StylePriority.UseBorders = False
        Me.xrTableCell1.StylePriority.UseFont = False
        Me.xrTableCell1.Text = "LOADED"
        Me.xrTableCell1.Weight = 0.18849455636138474R
        '
        'xrSoLnItmTtlTcel
        '
        Me.xrSoLnItmTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnItmTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnItmTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnItmTtlTcel.Name = "xrSoLnItmTtlTcel"
        Me.xrSoLnItmTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnItmTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnItmTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnItmTtlTcel.Text = "LOC"
        Me.xrSoLnItmTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnItmTtlTcel.Weight = 0.18281058839273145R
        '
        'xrSoLnDesTtlTcel
        '
        Me.xrSoLnDesTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnDesTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnDesTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnDesTtlTcel.Name = "xrSoLnDesTtlTcel"
        Me.xrSoLnDesTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnDesTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnDesTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnDesTtlTcel.Text = "SKU"
        Me.xrSoLnDesTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnDesTtlTcel.Weight = 0.28274181255306546R
        '
        'xrSoLnSerNumTtlTcel
        '
        Me.xrSoLnSerNumTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnSerNumTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnSerNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnSerNumTtlTcel.Name = "xrSoLnSerNumTtlTcel"
        Me.xrSoLnSerNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnSerNumTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnSerNumTtlTcel.Text = "ITEM"
        Me.xrSoLnSerNumTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnSerNumTtlTcel.Weight = 1.5892356445816R
        '
        'xrSoLnUnitPrcTtlTcel
        '
        Me.xrSoLnUnitPrcTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnUnitPrcTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnUnitPrcTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnUnitPrcTtlTcel.Name = "xrSoLnUnitPrcTtlTcel"
        Me.xrSoLnUnitPrcTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnUnitPrcTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnUnitPrcTtlTcel.Text = "PRICE"
        Me.xrSoLnUnitPrcTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnUnitPrcTtlTcel.Weight = 0.29298075382462935R
        '
        'xrSoLnExtPrcTtlTcel
        '
        Me.xrSoLnExtPrcTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSoLnExtPrcTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSoLnExtPrcTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSoLnExtPrcTtlTcel.Name = "xrSoLnExtPrcTtlTcel"
        Me.xrSoLnExtPrcTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseBackColor = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseFont = False
        Me.xrSoLnExtPrcTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSoLnExtPrcTtlTcel.Text = "EXT. PRICE"
        Me.xrSoLnExtPrcTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrSoLnExtPrcTtlTcel.Weight = 0.30566770788329978R
        '
        'xrDocNumCustCdTbl
        '
        Me.xrDocNumCustCdTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrDocNumCustCdTbl.LocationFloat = New DevExpress.Utils.PointFloat(635.0!, 0.0!)
        Me.xrDocNumCustCdTbl.Name = "xrDocNumCustCdTbl"
        Me.xrDocNumCustCdTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrDocNumTrow, Me.xrCustCdTrow, Me.xrTableRow1})
        Me.xrDocNumCustCdTbl.SizeF = New System.Drawing.SizeF(388.9582!, 135.9998!)
        Me.xrDocNumCustCdTbl.StylePriority.UseBorders = False
        '
        'xrDocNumTrow
        '
        Me.xrDocNumTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell3, Me.xrDocNumTtlTcel})
        Me.xrDocNumTrow.Name = "xrDocNumTrow"
        Me.xrDocNumTrow.Weight = 1.0248001098632813R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.BackColor = System.Drawing.Color.Black
        Me.xrTableCell3.Font = New System.Drawing.Font("Tahoma", 11.75!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell3.ForeColor = System.Drawing.Color.White
        Me.xrTableCell3.Multiline = True
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.StylePriority.UseBackColor = False
        Me.xrTableCell3.StylePriority.UseFont = False
        Me.xrTableCell3.StylePriority.UseForeColor = False
        Me.xrTableCell3.StylePriority.UseTextAlignment = False
        Me.xrTableCell3.Text = "ARRIVAL DATES FOR INCOMING STOCK/SPECIAL ORDERS"
        Me.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell3.Weight = 1.0744045549437766R
        '
        'xrDocNumTtlTcel
        '
        Me.xrDocNumTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrDocNumTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrDocNumTtlTcel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrDocNumTtlTcel.Name = "xrDocNumTtlTcel"
        Me.xrDocNumTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDocNumTtlTcel.StylePriority.UseBackColor = False
        Me.xrDocNumTtlTcel.StylePriority.UseFont = False
        Me.xrDocNumTtlTcel.StylePriority.UseForeColor = False
        Me.xrDocNumTtlTcel.Weight = 0.00559544497947923R
        '
        'xrCustCdTrow
        '
        Me.xrCustCdTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrCustCdTtlTcel})
        Me.xrCustCdTrow.Name = "xrCustCdTrow"
        Me.xrCustCdTrow.Weight = 0.87519989013671873R
        '
        'xrCustCdTtlTcel
        '
        Me.xrCustCdTtlTcel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrCustCdTtlTcel.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrCustCdTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCustCdTtlTcel.ForeColor = System.Drawing.Color.Black
        Me.xrCustCdTtlTcel.Name = "xrCustCdTtlTcel"
        Me.xrCustCdTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100.0!)
        Me.xrCustCdTtlTcel.StylePriority.UseBackColor = False
        Me.xrCustCdTtlTcel.StylePriority.UseBorders = False
        Me.xrCustCdTtlTcel.StylePriority.UseFont = False
        Me.xrCustCdTtlTcel.StylePriority.UseForeColor = False
        Me.xrCustCdTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrCustCdTtlTcel.Text = "ANY WAREHOUSE RECEIVING DATE WE PROVIDE IS AN ESTIMATE ONLY AND THEREFORE NOT GUA" & _
    "RANTEED."
        Me.xrCustCdTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify
        Me.xrCustCdTtlTcel.Weight = 1.0R
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell4})
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 1.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell4.Font = New System.Drawing.Font("Tahoma", 9.25!)
        Me.xrTableCell4.Multiline = True
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.StylePriority.UseBorders = False
        Me.xrTableCell4.StylePriority.UseFont = False
        Me.xrTableCell4.StylePriority.UseTextAlignment = False
        Me.xrTableCell4.Text = resources.GetString("xrTableCell4.Text")
        Me.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        Me.xrTableCell4.Weight = 1.28R
        '
        'xrCustTbl
        '
        Me.xrCustTbl.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.xrCustTbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 15.99998!)
        Me.xrCustTbl.Name = "xrCustTbl"
        Me.xrCustTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrCustInfoTrow})
        Me.xrCustTbl.SizeF = New System.Drawing.SizeF(620.1062!, 119.9998!)
        Me.xrCustTbl.StylePriority.UseBorderColor = False
        Me.xrCustTbl.StylePriority.UseBorders = False
        '
        'xrCustInfoTrow
        '
        Me.xrCustInfoTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrShipToTcel})
        Me.xrCustInfoTrow.Name = "xrCustInfoTrow"
        Me.xrCustInfoTrow.Weight = 3.99359795134443R
        '
        'xrShipToTcel
        '
        Me.xrShipToTcel.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel2, Me.xrLabel1, Me.xr_full_name, Me.xr_b_phone, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_addr1, Me.xrShipToLbl, Me.xrCustCdTtlLbl, Me.xrPgHdrLbl1, Me.xr_slsp1, Me.xrDocNumTtlLbl, Me.xr_del_doc_num, Me.xrLabel16, Me.xr_pu_del_dt, Me.xr_pd, Me.xr_slsp2, Me.xrLabel5, Me.xrLabel6})
        Me.xrShipToTcel.Name = "xrShipToTcel"
        Me.xrShipToTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrShipToTcel.StylePriority.UseTextAlignment = False
        Me.xrShipToTcel.Text = "xrShipToTcel"
        Me.xrShipToTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.xrShipToTcel.Weight = 2.5R
        '
        'xrLabel2
        '
        Me.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel2.CanShrink = True
        Me.xrLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 84.99991!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(70.0!, 17.0!)
        Me.xrLabel2.StylePriority.UseBorders = False
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseTextAlignment = False
        Me.xrLabel2.Text = "BUS  #"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLabel1
        '
        Me.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel1.CanShrink = True
        Me.xrLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.7599!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(70.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseBorders = False
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseTextAlignment = False
        Me.xrLabel1.Text = "HOME #"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_full_name
        '
        Me.xr_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_full_name.CanShrink = True
        Me.xr_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_full_name.StylePriority.UseBorders = False
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(70.0!, 84.99991!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseBorders = False
        Me.xr_b_phone.StylePriority.UseFont = False
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_h_phone.CanShrink = True
        Me.xr_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(69.00001!, 67.7599!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseBorders = False
        Me.xr_h_phone.StylePriority.UseFont = False
        '
        'xr_zip
        '
        Me.xr_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(199.6666!, 51.80899!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseBorders = False
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(163.5469!, 51.80899!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseBorders = False
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.56899!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
        Me.xr_city.StylePriority.UseBorders = False
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(239.9999!, 34.75997!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(250.0!, 17.00001!)
        Me.xr_addr2.StylePriority.UseBorders = False
        Me.xr_addr2.StylePriority.UseFont = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.75997!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_addr1.StylePriority.UseBorders = False
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xrCustCdTtlLbl
        '
        Me.xrCustCdTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustCdTtlLbl.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xrCustCdTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(440.1061!, 1.240005!)
        Me.xrCustCdTtlLbl.Name = "xrCustCdTtlLbl"
        Me.xrCustCdTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustCdTtlLbl.SizeF = New System.Drawing.SizeF(49.89377!, 15.75999!)
        Me.xrCustCdTtlLbl.StylePriority.UseBorders = False
        Me.xrCustCdTtlLbl.StylePriority.UseFont = False
        Me.xrCustCdTtlLbl.StylePriority.UseTextAlignment = False
        Me.xrCustCdTtlLbl.Text = "ACCT #:"
        Me.xrCustCdTtlLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrDocNumTtlLbl
        '
        Me.xrDocNumTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDocNumTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.xrDocNumTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(253.0207!, 68.809!)
        Me.xrDocNumTtlLbl.Name = "xrDocNumTtlLbl"
        Me.xrDocNumTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDocNumTtlLbl.SizeF = New System.Drawing.SizeF(54.16669!, 16.99994!)
        Me.xrDocNumTtlLbl.StylePriority.UseBorders = False
        Me.xrDocNumTtlLbl.StylePriority.UseFont = False
        Me.xrDocNumTtlLbl.Text = "TICKET# "
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(307.7083!, 66.3289!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(139.0!, 16.99998!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLabel16
        '
        Me.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel16.ForeColor = System.Drawing.Color.Black
        Me.xrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(253.0207!, 84.99991!)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.SizeF = New System.Drawing.SizeF(40.00012!, 17.0!)
        Me.xrLabel16.StylePriority.UseBorders = False
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseForeColor = False
        Me.xrLabel16.StylePriority.UseTextAlignment = False
        Me.xrLabel16.Text = "REF #"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrLabel16.WordWrap = False
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(294.1667!, 84.99992!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(92.70834!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseBorders = False
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.StylePriority.UseForeColor = False
        Me.xr_pu_del_dt.StylePriority.UseTextAlignment = False
        Me.xr_pu_del_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.Black
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(397.5!, 84.99991!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(30.0!, 17.0!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_pd.WordWrap = False
        '
        'xrLabel5
        '
        Me.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(440.1061!, 50.56899!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(125.0!, 15.75999!)
        Me.xrLabel5.StylePriority.UseBorders = False
        Me.xrLabel5.Text = "DELIVERY / PICKUP"
        '
        'xrLabel6
        '
        Me.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(440.1061!, 84.99992!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(184.8939!, 15.75999!)
        Me.xrLabel6.StylePriority.UseBorders = False
        Me.xrLabel6.Text = "DATE:__________DAY:_____"
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(610.1062!, 115.3289!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(599.9166!, 127.9998!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.083374!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 47.56899!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 125.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(619.0!, 74.32893!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(610.1062!, 101.9999!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 136.0!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(1023.958!, 5.124985!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(490.0001!, 15.99998!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(129.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'xrSoLnTbl
        '
        Me.xrSoLnTbl.BackColor = System.Drawing.Color.RosyBrown
        Me.xrSoLnTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrSoLnTbl.Name = "xrSoLnTbl"
        Me.xrSoLnTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSoLnLine1Trow})
        Me.xrSoLnTbl.SizeF = New System.Drawing.SizeF(1023.958!, 33.0!)
        Me.xrSoLnTbl.StylePriority.UseBackColor = False
        Me.xrSoLnTbl.StylePriority.UseTextAlignment = False
        Me.xrSoLnTbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrSoLnLine1Trow
        '
        Me.xrSoLnLine1Trow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_qty, Me.xrTableCell2, Me.xr_loc, Me.xr_sku, Me.xr_vend, Me.xr_vsn, Me.xr_des, Me.xr_pre_disc_ret, Me.xr_ret, Me.xr_ext})
        Me.xrSoLnLine1Trow.Name = "xrSoLnLine1Trow"
        Me.xrSoLnLine1Trow.Weight = 1.0R
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_qty.Weight = 0.17517674443484527R
        '
        'xrTableCell2
        '
        Me.xrTableCell2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xrTableCell2.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.StylePriority.UseBackColor = False
        Me.xrTableCell2.StylePriority.UseBorderColor = False
        Me.xrTableCell2.StylePriority.UseBorders = False
        Me.xrTableCell2.StylePriority.UseFont = False
        Me.xrTableCell2.Weight = 0.20889529821335887R
        '
        'xr_loc
        '
        Me.xr_loc.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_loc.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_loc.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_loc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.StylePriority.UseBackColor = False
        Me.xr_loc.StylePriority.UseBorderColor = False
        Me.xr_loc.StylePriority.UseBorders = False
        Me.xr_loc.StylePriority.UseFont = False
        Me.xr_loc.StylePriority.UseTextAlignment = False
        Me.xr_loc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_loc.Weight = 0.20259620832969327R
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sku.Weight = 0.31334291330378794R
        '
        'xr_vend
        '
        Me.xr_vend.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vend.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vend.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vend.Name = "xr_vend"
        Me.xr_vend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vend.StylePriority.UseBackColor = False
        Me.xr_vend.StylePriority.UseBorderColor = False
        Me.xr_vend.StylePriority.UseBorders = False
        Me.xr_vend.StylePriority.UseFont = False
        Me.xr_vend.StylePriority.UseTextAlignment = False
        Me.xr_vend.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vend.Weight = 0.2034279881263471R
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vsn.Weight = 0.51905425543200789R
        '
        'xr_des
        '
        Me.xr_des.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_des.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_des.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_des.Name = "xr_des"
        Me.xr_des.StylePriority.UseBackColor = False
        Me.xr_des.StylePriority.UseBorderColor = False
        Me.xr_des.StylePriority.UseBorders = False
        Me.xr_des.StylePriority.UseFont = False
        Me.xr_des.Weight = 1.0387560429775193R
        '
        'xr_pre_disc_ret
        '
        Me.xr_pre_disc_ret.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_pre_disc_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_pre_disc_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_pre_disc_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pre_disc_ret.Name = "xr_pre_disc_ret"
        Me.xr_pre_disc_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pre_disc_ret.StylePriority.UseBackColor = False
        Me.xr_pre_disc_ret.StylePriority.UseBorderColor = False
        Me.xr_pre_disc_ret.StylePriority.UseBorders = False
        Me.xr_pre_disc_ret.StylePriority.UseFont = False
        Me.xr_pre_disc_ret.StylePriority.UseTextAlignment = False
        Me.xr_pre_disc_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pre_disc_ret.Weight = 0.31819666532251273R
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ret.Visible = False
        Me.xr_ret.Weight = 0.00649380161140134R
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ext.Weight = 0.33875016281859438R
        '
        'xr_corp_name
        '
        Me.xr_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_corp_name.Name = "xr_corp_name"
        Me.xr_corp_name.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_disc_amt, Me.xr_void_flag, Me.xrSoLnTbl})
        Me.Detail.HeightF = 33.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(1021.784!, 25.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(800.0!, 8.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Expanded = False
        Me.PageFooter.HeightF = 34.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrBorderPanl, Me.xr_billToSt_hidden, Me.xr_tax_cd, Me.xrLabel14, Me.xrLabel13, Me.xrLabel12, Me.xrLabel11, Me.xrLabel10, Me.xrLabel9, Me.xrLabel8, Me.xrPrtNameTtlLbl, Me.xrBalanceDetailTbl, Me.xrFinalDetailTbl, Me.xrCmntPanl, Me.xr_payments, Me.xrCustSigTtlNLnLbl, Me.xrLabel27, Me.xrTermNCondTtlLbl, Me.xrLine2})
        Me.ReportFooter.HeightF = 350.2308!
        Me.ReportFooter.KeepTogether = True
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrBorderPanl
        '
        Me.xrBorderPanl.BackColor = System.Drawing.Color.Transparent
        Me.xrBorderPanl.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.xrBorderPanl.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrBorderPanl.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrRichText1})
        Me.xrBorderPanl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 73.55042!)
        Me.xrBorderPanl.Name = "xrBorderPanl"
        Me.xrBorderPanl.SizeF = New System.Drawing.SizeF(759.9999!, 96.99997!)
        Me.xrBorderPanl.StylePriority.UseBorders = False
        '
        'xrRichText1
        '
        Me.xrRichText1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRichText1.CanShrink = True
        Me.xrRichText1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(1.999998!, 0.0!)
        Me.xrRichText1.Name = "xrRichText1"
        Me.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString")
        Me.xrRichText1.SizeF = New System.Drawing.SizeF(755.2916!, 96.99994!)
        Me.xrRichText1.StylePriority.UseBorders = False
        Me.xrRichText1.StylePriority.UseFont = False
        '
        'xr_billToSt_hidden
        '
        Me.xr_billToSt_hidden.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_billToSt_hidden.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_billToSt_hidden.Name = "xr_billToSt_hidden"
        Me.xr_billToSt_hidden.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xr_billToSt_hidden.Visible = False
        '
        'xr_tax_cd
        '
        Me.xr_tax_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tax_cd.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_tax_cd.Name = "xr_tax_cd"
        Me.xr_tax_cd.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xr_tax_cd.Visible = False
        '
        'xrLabel14
        '
        Me.xrLabel14.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrLabel14.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.xrLabel14.KeepTogether = True
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 191.8837!)
        Me.xrLabel14.Multiline = True
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(759.9999!, 81.46165!)
        Me.xrLabel14.StylePriority.UseBorders = False
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseTextAlignment = False
        Me.xrLabel14.Text = resources.GetString("xrLabel14.Text")
        Me.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify
        '
        'xrLabel13
        '
        Me.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel13.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.KeepTogether = True
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(526.6664!, 300.4167!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(241.3335!, 17.0!)
        Me.xrLabel13.StylePriority.UseBorders = False
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseTextAlignment = False
        Me.xrLabel13.Text = "X_____________________________"
        Me.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'xrLabel12
        '
        Me.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel12.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel12.KeepTogether = True
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(256.6665!, 300.4167!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(241.3335!, 17.0!)
        Me.xrLabel12.StylePriority.UseBorders = False
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseTextAlignment = False
        Me.xrLabel12.Text = "X_____________________________"
        Me.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'xrLabel11
        '
        Me.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel11.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 300.4167!)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.SizeF = New System.Drawing.SizeF(255.6664!, 17.0!)
        Me.xrLabel11.StylePriority.UseBorders = False
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.StylePriority.UseTextAlignment = False
        Me.xrLabel11.Text = "X_______________________________"
        Me.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'xrLabel10
        '
        Me.xrLabel10.BackColor = System.Drawing.Color.Black
        Me.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.White
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 171.5504!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(759.9999!, 20.33327!)
        Me.xrLabel10.StylePriority.UseBackColor = False
        Me.xrLabel10.StylePriority.UseBorders = False
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.StylePriority.UseTextAlignment = False
        Me.xrLabel10.Text = "LIMITED WARRANTY AND RESOLUTION POLICY"
        Me.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLabel9
        '
        Me.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.Black
        Me.xrLabel9.KeepTogether = True
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(531.6665!, 317.4167!)
        Me.xrLabel9.Multiline = True
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(233.3334!, 31.0!)
        Me.xrLabel9.StylePriority.UseBorders = False
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "As-is Merchandise Inspected by Customer ALL AS-IS SALES ARE FINAL"
        '
        'xrLabel8
        '
        Me.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.ForeColor = System.Drawing.Color.Black
        Me.xrLabel8.KeepTogether = True
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(260.6665!, 317.4167!)
        Me.xrLabel8.Multiline = True
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(229.3334!, 31.0!)
        Me.xrLabel8.StylePriority.UseBorders = False
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.Text = "Customer's Signature to Acknowledge Receipt of the Above in Good Order"
        '
        'xrPrtNameTtlLbl
        '
        Me.xrPrtNameTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPrtNameTtlLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPrtNameTtlLbl.ForeColor = System.Drawing.Color.Red
        Me.xrPrtNameTtlLbl.KeepTogether = True
        Me.xrPrtNameTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(469.4999!, 323.2308!)
        Me.xrPrtNameTtlLbl.Name = "xrPrtNameTtlLbl"
        Me.xrPrtNameTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPrtNameTtlLbl.SizeF = New System.Drawing.SizeF(56.375!, 17.0!)
        Me.xrPrtNameTtlLbl.StylePriority.UseBorders = False
        Me.xrPrtNameTtlLbl.StylePriority.UseFont = False
        Me.xrPrtNameTtlLbl.StylePriority.UseForeColor = False
        Me.xrPrtNameTtlLbl.Text = "OFFICE"
        '
        'xrBalanceDetailTbl
        '
        Me.xrBalanceDetailTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrBalanceDetailTbl.LocationFloat = New DevExpress.Utils.PointFloat(771.78!, 3.9!)
        Me.xrBalanceDetailTbl.Name = "xrBalanceDetailTbl"
        Me.xrBalanceDetailTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrSubTotalTrow, Me.xrTableRow2, Me.xrTableRow3, Me.xrTransChrgsTrow, Me.xrTableRow4, Me.xrTotOrderTrow, Me.xrPmtTrow})
        Me.xrBalanceDetailTbl.SizeF = New System.Drawing.SizeF(250.0!, 188.8837!)
        Me.xrBalanceDetailTbl.StylePriority.UseBorders = False
        '
        'xrSubTotalTrow
        '
        Me.xrSubTotalTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrSubTotalTtlTcel, Me.xr_subtotal})
        Me.xrSubTotalTrow.Name = "xrSubTotalTrow"
        Me.xrSubTotalTrow.Weight = 1.0R
        '
        'xrSubTotalTtlTcel
        '
        Me.xrSubTotalTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrSubTotalTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrSubTotalTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrSubTotalTtlTcel.Name = "xrSubTotalTtlTcel"
        Me.xrSubTotalTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSubTotalTtlTcel.StylePriority.UseBackColor = False
        Me.xrSubTotalTtlTcel.StylePriority.UseBorderColor = False
        Me.xrSubTotalTtlTcel.StylePriority.UseFont = False
        Me.xrSubTotalTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrSubTotalTtlTcel.Text = "SUBTOTAL"
        Me.xrSubTotalTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrSubTotalTtlTcel.Weight = 1.08R
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 1.0899999999999999R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTaxLbl, Me.xr_tax})
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 1.0R
        '
        'xrTaxLbl
        '
        Me.xrTaxLbl.BackColor = System.Drawing.Color.Silver
        Me.xrTaxLbl.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTaxLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTaxLbl.Name = "xrTaxLbl"
        Me.xrTaxLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTaxLbl.StylePriority.UseBackColor = False
        Me.xrTaxLbl.StylePriority.UseBorderColor = False
        Me.xrTaxLbl.StylePriority.UseFont = False
        Me.xrTaxLbl.StylePriority.UsePadding = False
        Me.xrTaxLbl.StylePriority.UseTextAlignment = False
        Me.xrTaxLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTaxLbl.Weight = 1.7855430639038088R
        '
        'xr_tax
        '
        Me.xr_tax.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.StylePriority.UseBorderColor = False
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UsePadding = False
        Me.xr_tax.StylePriority.UseTextAlignment = False
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax.Weight = 1.8021902694295246R
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTaxBaseInfo, Me.xrTableCell8})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.Weight = 1.0R
        '
        'xrTaxBaseInfo
        '
        Me.xrTaxBaseInfo.BackColor = System.Drawing.Color.Silver
        Me.xrTaxBaseInfo.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTaxBaseInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTaxBaseInfo.Name = "xrTaxBaseInfo"
        Me.xrTaxBaseInfo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTaxBaseInfo.StylePriority.UseBackColor = False
        Me.xrTaxBaseInfo.StylePriority.UseBorderColor = False
        Me.xrTaxBaseInfo.StylePriority.UseFont = False
        Me.xrTaxBaseInfo.StylePriority.UsePadding = False
        Me.xrTaxBaseInfo.StylePriority.UseTextAlignment = False
        Me.xrTaxBaseInfo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTaxBaseInfo.Weight = 1.7855429557490299R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.StylePriority.UseBorderColor = False
        Me.xrTableCell8.StylePriority.UseFont = False
        Me.xrTableCell8.Weight = 1.8021900464685807R
        '
        'xrTransChrgsTrow
        '
        Me.xrTransChrgsTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTransChrgsTtlTcel, Me.xr_del})
        Me.xrTransChrgsTrow.Name = "xrTransChrgsTrow"
        Me.xrTransChrgsTrow.Weight = 1.0R
        '
        'xrTransChrgsTtlTcel
        '
        Me.xrTransChrgsTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTransChrgsTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTransChrgsTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTransChrgsTtlTcel.Name = "xrTransChrgsTtlTcel"
        Me.xrTransChrgsTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTransChrgsTtlTcel.StylePriority.UseBackColor = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseFont = False
        Me.xrTransChrgsTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTransChrgsTtlTcel.Text = "DEL. CHARGE"
        Me.xrTransChrgsTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTransChrgsTtlTcel.Weight = 1.08R
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 1.0899999999999999R
        '
        'xrTableRow4
        '
        Me.xrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell9, Me.xr_setup})
        Me.xrTableRow4.Name = "xrTableRow4"
        Me.xrTableRow4.Weight = 1.0R
        '
        'xrTableCell9
        '
        Me.xrTableCell9.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell9.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell9.StylePriority.UseBackColor = False
        Me.xrTableCell9.StylePriority.UseBorderColor = False
        Me.xrTableCell9.StylePriority.UseFont = False
        Me.xrTableCell9.StylePriority.UsePadding = False
        Me.xrTableCell9.StylePriority.UseTextAlignment = False
        Me.xrTableCell9.Text = "ZONE CHARGE"
        Me.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell9.Weight = 1.08R
        '
        'xr_setup
        '
        Me.xr_setup.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_setup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_setup.Name = "xr_setup"
        Me.xr_setup.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_setup.StylePriority.UseBorderColor = False
        Me.xr_setup.StylePriority.UseFont = False
        Me.xr_setup.StylePriority.UsePadding = False
        Me.xr_setup.StylePriority.UseTextAlignment = False
        Me.xr_setup.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_setup.Weight = 1.0899999999999999R
        '
        'xrTotOrderTrow
        '
        Me.xrTotOrderTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTotOrderTtlTcel, Me.xr_grand_total})
        Me.xrTotOrderTrow.Name = "xrTotOrderTrow"
        Me.xrTotOrderTrow.Weight = 1.0R
        '
        'xrTotOrderTtlTcel
        '
        Me.xrTotOrderTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTotOrderTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTotOrderTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTotOrderTtlTcel.Name = "xrTotOrderTtlTcel"
        Me.xrTotOrderTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTotOrderTtlTcel.StylePriority.UseBackColor = False
        Me.xrTotOrderTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTotOrderTtlTcel.StylePriority.UseFont = False
        Me.xrTotOrderTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTotOrderTtlTcel.Text = "TOTAL:  >>"
        Me.xrTotOrderTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTotOrderTtlTcel.Weight = 1.08R
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 1.0899999999999999R
        '
        'xrPmtTrow
        '
        Me.xrPmtTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTotPmtsTtlTcel, Me.xr_pmt_amt, Me.xr_mop})
        Me.xrPmtTrow.Name = "xrPmtTrow"
        Me.xrPmtTrow.Weight = 1.0R
        '
        'xrTotPmtsTtlTcel
        '
        Me.xrTotPmtsTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrTotPmtsTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTotPmtsTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTotPmtsTtlTcel.Name = "xrTotPmtsTtlTcel"
        Me.xrTotPmtsTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.xrTotPmtsTtlTcel.StylePriority.UseBackColor = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseBorderColor = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseFont = False
        Me.xrTotPmtsTtlTcel.StylePriority.UsePadding = False
        Me.xrTotPmtsTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrTotPmtsTtlTcel.Text = "DEPOSIT"
        Me.xrTotPmtsTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTotPmtsTtlTcel.Weight = 0.59268939165683188R
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pmt_amt.CanShrink = True
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.StylePriority.UseBorders = False
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pmt_amt.Weight = 0.36979786834951767R
        '
        'xr_mop
        '
        Me.xr_mop.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_mop.CanShrink = True
        Me.xr_mop.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.StylePriority.UseBorders = False
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_mop.Weight = 0.22837953076513479R
        '
        'xrFinalDetailTbl
        '
        Me.xrFinalDetailTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrFinalDetailTbl.KeepTogether = True
        Me.xrFinalDetailTbl.LocationFloat = New DevExpress.Utils.PointFloat(770.958!, 225.3133!)
        Me.xrFinalDetailTbl.LockedInUserDesigner = True
        Me.xrFinalDetailTbl.Name = "xrFinalDetailTbl"
        Me.xrFinalDetailTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrBalanceTrow, Me.xrTableRow5, Me.xrTableRow6, Me.xrTableRow7, Me.xrTableRow8})
        Me.xrFinalDetailTbl.SizeF = New System.Drawing.SizeF(250.0!, 114.9175!)
        Me.xrFinalDetailTbl.StylePriority.UseBorders = False
        '
        'xrBalanceTrow
        '
        Me.xrBalanceTrow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrBalanceTtlTcel, Me.xr_balance})
        Me.xrBalanceTrow.LockedInUserDesigner = True
        Me.xrBalanceTrow.Name = "xrBalanceTrow"
        Me.xrBalanceTrow.Weight = 1.0R
        '
        'xrBalanceTtlTcel
        '
        Me.xrBalanceTtlTcel.BackColor = System.Drawing.Color.Silver
        Me.xrBalanceTtlTcel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrBalanceTtlTcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrBalanceTtlTcel.LockedInUserDesigner = True
        Me.xrBalanceTtlTcel.Name = "xrBalanceTtlTcel"
        Me.xrBalanceTtlTcel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrBalanceTtlTcel.StylePriority.UseBackColor = False
        Me.xrBalanceTtlTcel.StylePriority.UseBorderColor = False
        Me.xrBalanceTtlTcel.StylePriority.UseFont = False
        Me.xrBalanceTtlTcel.StylePriority.UseTextAlignment = False
        Me.xrBalanceTtlTcel.Text = "BALANCE DUE:"
        Me.xrBalanceTtlTcel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrBalanceTtlTcel.Weight = 1.08R
        '
        'xr_balance
        '
        Me.xr_balance.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_balance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.LockedInUserDesigner = True
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_balance.StylePriority.UseBorderColor = False
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.Weight = 1.0899999999999999R
        '
        'xrTableRow5
        '
        Me.xrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell11})
        Me.xrTableRow5.LockedInUserDesigner = True
        Me.xrTableRow5.Name = "xrTableRow5"
        Me.xrTableRow5.Weight = 1.0R
        '
        'xrTableCell11
        '
        Me.xrTableCell11.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell11.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell11.LockedInUserDesigner = True
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseBackColor = False
        Me.xrTableCell11.StylePriority.UseBorderColor = False
        Me.xrTableCell11.StylePriority.UseFont = False
        Me.xrTableCell11.StylePriority.UsePadding = False
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "TO BE PAID BY:"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell11.Weight = 1.08R
        '
        'xrTableRow6
        '
        Me.xrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_ord_srt_des})
        Me.xrTableRow6.LockedInUserDesigner = True
        Me.xrTableRow6.Name = "xrTableRow6"
        Me.xrTableRow6.Weight = 1.0R
        '
        'xr_ord_srt_des
        '
        Me.xr_ord_srt_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ord_srt_des.LockedInUserDesigner = True
        Me.xr_ord_srt_des.Name = "xr_ord_srt_des"
        Me.xr_ord_srt_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt_des.StylePriority.UseBorderColor = False
        Me.xr_ord_srt_des.StylePriority.UsePadding = False
        Me.xr_ord_srt_des.StylePriority.UseTextAlignment = False
        Me.xr_ord_srt_des.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_ord_srt_des.Weight = 1.08R
        '
        'xrTableRow7
        '
        Me.xrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell15})
        Me.xrTableRow7.LockedInUserDesigner = True
        Me.xrTableRow7.Name = "xrTableRow7"
        Me.xrTableRow7.Weight = 1.0R
        '
        'xrTableCell15
        '
        Me.xrTableCell15.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell15.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell15.LockedInUserDesigner = True
        Me.xrTableCell15.Name = "xrTableCell15"
        Me.xrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell15.StylePriority.UseBorderColor = False
        Me.xrTableCell15.StylePriority.UseBorders = False
        Me.xrTableCell15.StylePriority.UsePadding = False
        Me.xrTableCell15.StylePriority.UseTextAlignment = False
        Me.xrTableCell15.Text = "7 CHECKS _______    MNTG ___"
        Me.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell15.Weight = 1.08R
        '
        'xrTableRow8
        '
        Me.xrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell17})
        Me.xrTableRow8.LockedInUserDesigner = True
        Me.xrTableRow8.Name = "xrTableRow8"
        Me.xrTableRow8.Weight = 1.0R
        '
        'xrTableCell17
        '
        Me.xrTableCell17.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell17.LockedInUserDesigner = True
        Me.xrTableCell17.Name = "xrTableCell17"
        Me.xrTableCell17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell17.StylePriority.UseBorderColor = False
        Me.xrTableCell17.StylePriority.UsePadding = False
        Me.xrTableCell17.StylePriority.UseTextAlignment = False
        Me.xrTableCell17.Text = "INTERSECTION KEYED ___"
        Me.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell17.Weight = 1.08R
        '
        'xrCmntPanl
        '
        Me.xrCmntPanl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCmntPanl.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments})
        Me.xrCmntPanl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
        Me.xrCmntPanl.Name = "xrCmntPanl"
        Me.xrCmntPanl.SizeF = New System.Drawing.SizeF(760.6666!, 44.87181!)
        Me.xrCmntPanl.StylePriority.UseBorders = False
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.xr_comments.KeepTogether = True
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(2.000014!, 1.999982!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(760.0!, 42.87!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'xr_payments
        '
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_payments.Visible = False
        '
        'xrCustSigTtlNLnLbl
        '
        Me.xrCustSigTtlNLnLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustSigTtlNLnLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCustSigTtlNLnLbl.ForeColor = System.Drawing.Color.Black
        Me.xrCustSigTtlNLnLbl.KeepTogether = True
        Me.xrCustSigTtlNLnLbl.LocationFloat = New DevExpress.Utils.PointFloat(6.666597!, 317.4167!)
        Me.xrCustSigTtlNLnLbl.Multiline = True
        Me.xrCustSigTtlNLnLbl.Name = "xrCustSigTtlNLnLbl"
        Me.xrCustSigTtlNLnLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustSigTtlNLnLbl.SizeF = New System.Drawing.SizeF(233.3334!, 31.0!)
        Me.xrCustSigTtlNLnLbl.StylePriority.UseBorders = False
        Me.xrCustSigTtlNLnLbl.StylePriority.UseFont = False
        Me.xrCustSigTtlNLnLbl.StylePriority.UseForeColor = False
        Me.xrCustSigTtlNLnLbl.Text = "Customer's Signature to Acknowledge Terms and Receipt of this Salesticket"
        '
        'xrLabel27
        '
        Me.xrLabel27.BackColor = System.Drawing.Color.Black
        Me.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel27.ForeColor = System.Drawing.Color.White
        Me.xrLabel27.KeepTogether = True
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 52.8718!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(760.0!, 20.33!)
        Me.xrLabel27.StylePriority.UseBackColor = False
        Me.xrLabel27.StylePriority.UseBorders = False
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.Text = "RETURN, PICKUP-UP & DELIVERY POLICIES - PLEASE READ BEFORE SIGNING THIS SALES AGR" & _
    "EEMENT"
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTermNCondTtlLbl
        '
        Me.xrTermNCondTtlLbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTermNCondTtlLbl.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xrTermNCondTtlLbl.KeepTogether = True
        Me.xrTermNCondTtlLbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 273.3454!)
        Me.xrTermNCondTtlLbl.Name = "xrTermNCondTtlLbl"
        Me.xrTermNCondTtlLbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTermNCondTtlLbl.SizeF = New System.Drawing.SizeF(758.6666!, 17.0!)
        Me.xrTermNCondTtlLbl.StylePriority.UseBorders = False
        Me.xrTermNCondTtlLbl.StylePriority.UseFont = False
        Me.xrTermNCondTtlLbl.StylePriority.UseTextAlignment = False
        Me.xrTermNCondTtlLbl.Text = "This sale subject to the above terms & conditions and not final until prices & te" & _
    "rms are approved by management."
        Me.xrTermNCondTtlLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrLine2
        '
        Me.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(1020.958!, 9.0!)
        Me.xrLine2.StylePriority.UseBorders = False
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'winControlContainer1
        '
        Me.winControlContainer1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.winControlContainer1.Name = "winControlContainer1"
        Me.winControlContainer1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.winControlContainer1.WinControl = Nothing
        '
        'richTextBox1
        '
        Me.richTextBox1.Location = New System.Drawing.Point(0, 0)
        Me.richTextBox1.Name = "richTextBox1"
        Me.richTextBox1.Size = New System.Drawing.Size(100, 96)
        Me.richTextBox1.TabIndex = 0
        Me.richTextBox1.Text = ""
        '
        'xrPmtTbl
        '
        Me.xrPmtTbl.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrPmtTbl.Name = "xrPmtTbl"
        Me.xrPmtTbl.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
        '
        'xrPmtTcel
        '
        Me.xrPmtTcel.Name = "xrPmtTcel"
        Me.xrPmtTcel.Weight = 0.0R
        '
        'xr_billToSt
        '
        Me.xr_billToSt.Name = "xr_billToSt"
        Me.xr_billToSt.Weight = 0.0R
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 25.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'RothmanInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(33, 10, 25, 25)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        CType(Me.xrPgHdrTbl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrSoLnTtlTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrDocNumCustCdTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrCustTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrSoLnTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrBalanceDetailTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrFinalDetailTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_setup.DataBindings.Add("Text", DataSource, "SO.SETUP_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_tax_cd.DataBindings.Add("Text", DataSource, "SO.TAX_CD")
        'xr_tax_cd2.DataBindings.Add("Text", DataSource, "SO.TAX_CD")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_corp_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CORP")
        xr_full_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_FULL_NAME")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        '.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_sku.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_vend.DataBindings.Add("Text", DataSource, "ITM.VE_CD")
        xr_des.DataBindings.Add("Text", DataSource, "ITM.DES")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_slsp1.DataBindings.Add("Text", DataSource, "SLSP1_FULL_NAME")
        xr_slsp2.DataBindings.Add("Text", DataSource, "SLSP2_FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_pre_disc_ret.DataBindings.Add("Text", DataSource, "SO_LN.PRE_DISC_PRC", "{0:0.00}")     '  not the typical unit price - is pre-disc
        'xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.ext_pre_disc", "{0:0.00}")                ' not the typical extended - is pre-disc
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_so_store_cd.DataBindings.Add("Text", DataSource, "SO.so_store_cd")
        'xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")
        xr_store_addr1.DataBindings.Add("Text", DataSource, "STORE_ADDR1")
        xr_store_phone.DataBindings.Add("Text", DataSource, "STORE.STORE_PHONE")
        xr_store_city_st_zip.DataBindings.Add("Text", DataSource, "SO.STORE_CITY_ST_ZIP")
        xr_billToSt_hidden.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_ord_srt_des.DataBindings.Add("Text", DataSource, "SRT_DES")
        xr_so_seq_num.DataBindings.Add("Text", DataSource, "SO.SO_SEQ_NUM")

    End Sub

    'Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint

    '    ' SO.tax_chg on line tax is tax for setup and delivery charges; for header tax method it is tax on everything
    '    Dim taxChg As Double = 0
    '    If IsNumeric(sender.text) AndAlso CDbl(sender.text) > 0.0 Then
    '        taxChg = sender.text
    '    End If

    '    If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
    '        Dim conn As New System.Data.OracleClient.OracleConnection
    '        Dim objSql2 As System.Data.OracleClient.OracleCommand
    '        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
    '        Dim sql As String

    '        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '        conn.Open()

    '        'store tax is not charged to customer and is not part of sale balance reported/billed to customer
    '        sql = "select sum(NVL(CUST_TAX_CHG,0)) AS TAX_CHG FROM SO_LN WHERE DEL_DOC_NUM='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '        sql = sql & "AND SO_LN.VOID_FLAG='N'"

    '        'Set SQL OBJECT 
    '        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

    '        Try
    '            'Execute DataReader 
    '            MyDataReader2 = objSql2.ExecuteReader
    '            If MyDataReader2.Read Then
    '                If IsNumeric(MyDataReader2.Item("TAX_CHG").ToString) Then
    '                    taxChg = taxChg + CDbl(MyDataReader2.Item("TAX_CHG").ToString)
    '                End If
    '            End If
    '            MyDataReader2.Close()
    '        Catch
    '            conn.Close()
    '            Throw
    '        End Try
    '    End If

    '    sender.Text = FormatCurrency(taxChg, 2)t as original
    'End Sub

    Private Sub calc_tax()
        ' since balance is calculated before tax and payments, going to calculate tax and payments with balance; if that changes, then break out as original

        ' SO.tax_chg on line tax is tax for setup and delivery charges; for header tax method it is tax on everything
        Dim taxChg As Double = 0
        If IsNumeric(Me.GetCurrentColumnValue("TAX_CHG").ToString) AndAlso CDbl(Me.GetCurrentColumnValue("TAX_CHG").ToString) > 0.0 Then
            taxChg = Me.GetCurrentColumnValue("TAX_CHG").ToString
        End If

        If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            'store tax is not charged to customer and is not part of sale balance reported/billed to customer
            sql = "select sum(NVL(CUST_TAX_CHG,0)) AS TAX_CHG FROM SO_LN WHERE DEL_DOC_NUM='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            sql = sql & "AND SO_LN.VOID_FLAG='N'"

            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read Then
                    If IsNumeric(MyDataReader2.Item("TAX_CHG").ToString) Then
                        taxChg = taxChg + CDbl(MyDataReader2.Item("TAX_CHG").ToString)
                    End If
                End If
                MyDataReader2.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

        'g_TaxChg = FormatCurrency(taxChg, 2)
        xr_tax.Text = FormatCurrency(taxChg, 2)
    End Sub

    Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_setup.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = FormatCurrency(0.0, 2)
        Else
            sender.Text = FormatCurrency(xr_setup.Text, 2)
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = FormatCurrency(0.0, 2)
        Else
            sender.Text = FormatCurrency(xr_del.Text, 2)
        End If
    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        sender.text = FormatNumber(xr_qty.Text * xr_pre_disc_ret.Text, 2)  ' Rothman's displays pre-discount on lines but regular subtotal ?????
        subtotal = subtotal + (xr_qty.Text * xr_ret.Text)
    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text) + CDbl(xr_setup.Text), 2)
    End Sub

    'Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint, xr_payment_holder.BeforePrint

    '    'If processed_payments = False Then
    '    Dim conn As New System.Data.OracleClient.OracleConnection
    '    Dim objSql2 As System.Data.OracleClient.OracleCommand
    '    Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
    '    Dim sql As String

    '    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '    conn.Open()

    '    'sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '    'sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
    '    sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c "
    '    sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
    '    Select Case xr_ord_tp_hidden.Text
    '        Case "SAL"
    '            sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '        Case "CRM"
    '            sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '        Case "MCR"
    '            sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '        Case "MDB"
    '            sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '        Case Else
    '            sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
    '    End Select
    '    sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

    '    'Set SQL OBJECT 
    '    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        'Execute DataReader 
    '        MyDataReader2 = objSql2.ExecuteReader
    '        If MyDataReader2.Read Then
    '            If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
    '                sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
    '            Else
    '                sender.text = FormatNumber(0, 2)
    '                xrPmtTbl.Visible = False
    '                xrLabel27.Visible = False
    '            End If
    '        Else
    '            sender.text = FormatNumber(0, 2)
    '            xrPmtTbl.Visible = False
    '            xrLabel27.Visible = False
    '        End If
    '    Catch
    '        Throw
    '    End Try
    '    'processed_payments = True
    '    'End If

    'End Sub

    'Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

    '    If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
    '        If CDbl(xr_disc_amt.Text) > 0 Then
    '            Dim conn As New System.Data.OracleClient.OracleConnection
    '            Dim objSql2 As System.Data.OracleClient.OracleCommand
    '            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
    '            Dim sql As String

    '            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '            conn.Open()

    '            sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

    '            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

    '            Try
    '                'Execute DataReader 
    '                MyDataReader2 = objSql2.ExecuteReader
    '                If MyDataReader2.Read Then
    '                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
    '                        sender.visible = True
    '                        sender.text = MyDataReader2.Item("DES").ToString & " (" & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
    '                    Else
    '                        sender.visible = False
    '                    End If
    '                End If
    '            Catch
    '                Throw
    '            End Try
    '        End If
    '    End If

    'End Sub

    'Private Sub xr_pmt_amt_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pmt_amt.BeforePrint

    '    'If mop_processed = False Then
    '    Dim conn As New System.Data.OracleClient.OracleConnection
    '    Dim objSql2 As System.Data.OracleClient.OracleCommand
    '    Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
    '    Dim sqlSb As New StringBuilder
    '    Dim merchant_id As String = ""

    '    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '    conn.Open()

    '    sqlSb.Append("select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, TO_CHAR(POST_DT,'MM-DD-YY') AS POST_DT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.MOP_CD ").Append(
    '    "from ar_trn a, mop b, ar_trn_tp c ").Append(
    '    "where  a.trn_tp_cd = c.trn_tp_cd(+) ").Append(
    '    "and  a.MOP_CD=b.MOP_CD(+) AND a.co_cd = '").Append(Me.GetCurrentColumnValue("CO_CD").ToString & "' AND a.cust_cd = '").Append(Me.GetCurrentColumnValue("CUST_CD").ToString).Append("' ")
    '    Select Case xr_ord_tp_hidden.Text
    '        Case "SAL"
    '            sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
    '        Case "CRM"
    '            sqlSb.Append("and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
    '        Case "MCR"
    '            sqlSb.Append("and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
    '        Case "MDB"
    '            sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
    '        Case Else
    '            sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
    '    End Select
    '    sqlSb.Append("and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')")

    '    'Set SQL OBJECT 
    '    objSql2 = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

    '    Try
    '        'Execute DataReader 
    '        MyDataReader2 = objSql2.ExecuteReader
    '        Do While MyDataReader2.Read
    '            If MyDataReader2.Item("MOP_CD").ToString = "WC" Then
    '                xr_ord_tp.Text = "WILL CALL"
    '            End If
    '            'If MyDataReader2.Item("DES").ToString & "" <> "" Then
    '            '    xr_mop.Text = xr_mop.Text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
    '            'Else
    '            '    xr_mop.Text = xr_mop.Text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
    '            'End If
    '            xr_mop.Text = xr_mop.Text & MyDataReader2.Item("MOP_CD").ToString & Constants.vbCrLf
    '            'If String.IsNullOrEmpty(MyDataReader2.Item("POST_DT").ToString) Then
    '            '    xr_pmt_dt.Text = xr_pmt_dt.Text & Constants.vbCrLf
    '            'Else
    '            '    xr_pmt_dt.Text = xr_pmt_dt.Text & MyDataReader2.Item("POST_DT").ToString & Constants.vbCrLf
    '            'End If
    '            sender.Text = sender.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
    '            g_pmtTotal = g_pmtTotal + CDbl(MyDataReader2.Item("PMT_AMT").ToString)
    '        Loop
    '    Catch
    '        Throw
    '    End Try
    '    'xr_balance.Text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_setup.Text) + CDbl(xr_tax.Text)) - pmtTotal, 2)
    '    'processed_payments = True
    '    'End If

    'End Sub

    Private Sub get_pmts_n_mops()

        ' since balance is calculated before tax and payments, going to calculate tax and payments with balance; if that changes, then break out as original
        'If mop_processed = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sqlSb As New StringBuilder
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sqlSb.Append("select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, TO_CHAR(POST_DT,'MM-DD-YY') AS POST_DT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.MOP_CD ").Append(
        "from ar_trn a, mop b, ar_trn_tp c ").Append(
        "where  a.trn_tp_cd = c.trn_tp_cd(+) ").Append(
        "and  a.MOP_CD=b.MOP_CD(+) AND a.co_cd = '").Append(Me.GetCurrentColumnValue("CO_CD").ToString & "' AND a.cust_cd = '").Append(Me.GetCurrentColumnValue("CUST_CD").ToString).Append("' ")
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
            Case "CRM"
                sqlSb.Append("and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
            Case "MCR"
                sqlSb.Append("and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
            Case "MDB"
                sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
            Case Else
                sqlSb.Append("and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' ")
        End Select
        sqlSb.Append("and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')")

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If MyDataReader2.Item("MOP_CD").ToString = "WC" Then
                    xr_ord_tp.Text = "WILL CALL"
                End If
                'If MyDataReader2.Item("DES").ToString & "" <> "" Then
                '    xr_mop.Text = xr_mop.Text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                'Else
                '    xr_mop.Text = xr_mop.Text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
                'End If
                xr_mop.Text = xr_mop.Text & MyDataReader2.Item("MOP_CD").ToString & Constants.vbCrLf
                'If String.IsNullOrEmpty(MyDataReader2.Item("POST_DT").ToString) Then
                '    xr_pmt_dt.Text = xr_pmt_dt.Text & Constants.vbCrLf
                'Else
                '    xr_pmt_dt.Text = xr_pmt_dt.Text & MyDataReader2.Item("POST_DT").ToString & Constants.vbCrLf
                'End If
                xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
                g_pmtTotal = g_pmtTotal + CDbl(MyDataReader2.Item("PMT_AMT").ToString)
            Loop
        Catch
            Throw
        End Try
        'xr_balance.Text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_setup.Text) + CDbl(xr_tax.Text)) - pmtTotal, 2)
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        Const maxCmntLength As Integer = 415

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim cmd As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sqlSb As New StringBuilder

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sqlSb.Append("Select SO_CMNT.DT, SO_CMNT.TEXT, NVL(SO_CMNT.CMNT_TYPE, 'S') as CMNT_TYPE, NVL(PERM, 'N') as PERM, SO_CMNT.DEL_DOC_NUM ").Append(
                         "from SO_CMNT, SO Where SO.DEL_DOC_NUM = :docNum ").Append(
                         " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD ").Append(
                         " AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND NVL(SO_CMNT.CMNT_TYPE,'S') = 'S' ").Append(
                         " AND NVL(so_cmnt.perm,'N') != 'Y' AND (so_cmnt.del_Doc_num IS NULL OR so_cmnt.del_Doc_num = :docNum) ORDER BY SEQ#")

            cmd = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

            cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":docNum").Value = xr_del_doc_num.Text

            Try
                Dim datSet As DataSet = New DataSet
                Dim oraDatA As System.Data.OracleClient.OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                oraDatA.Fill(datSet)
                sender.text = getSoCmnts4View(datSet.Tables(0), False, maxCmntLength)
            Catch
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Public Shared Function getSoCmnts4View(ByRef cmntTab As DataTable, ByVal useCrLF As Boolean, ByVal maxLength As Integer) As String

        Dim cmntRow As DataRow
        Dim cmntSb As New StringBuilder

        If (Not cmntTab Is Nothing) AndAlso cmntTab.Rows.Count > 0 Then

            Dim currDt As String = ""
            Dim currPerm As String = ""
            Dim currDoc As String = ""
            Dim currTp As String = ""
            'Dim currEmp As String = ""
            'Dim currOrig As String = ""
            'currEmp = cmntRow("
            'currOrig = cmntRow("PERM").ToString

            For Each cmntRow In cmntTab.Rows

                If cmntRow("DT").ToString.Equals(currDt) AndAlso
                    cmntRow("PERM").ToString.Equals(currPerm) AndAlso
                    cmntRow("CMNT_TYPE").ToString.Equals(currTp) AndAlso
                    ((currDoc.isEmpty AndAlso
                      cmntRow("DEL_DOC_NUM").ToString.isEmpty) OrElse
                     cmntRow("DEL_DOC_NUM").ToString.Equals(currDoc)) Then
                    ' if a match to previous 

                    cmntSb.Append(cmntRow("TEXT").ToString)

                Else
                    If currDt.isEmpty Then
                        ' if first time thru
                        cmntSb.Append(cmntRow("TEXT").ToString)

                    ElseIf useCrLF Then
                        cmntSb.Append(Constants.vbCrLf).Append(cmntRow("TEXT").ToString)

                    Else
                        cmntSb.Append("; ").Append(cmntRow("TEXT").ToString)
                    End If
                    currDt = cmntRow("DT").ToString
                    currPerm = cmntRow("PERM").ToString
                    currDoc = cmntRow("DEL_DOC_NUM").ToString
                    currTp = cmntRow("CMNT_TYPE").ToString
                End If
            Next

        End If

        Dim len As Integer = cmntSb.ToString.Length
        If len > maxLength Then
            len = maxLength
        End If
        If cmntSb.Length > 0 Then
            Return cmntSb.ToString.Substring(0, len)
        Else
            Return ""
        End If
    End Function

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        If xr_corp_name.Text.isNotEmpty Then
            xr_full_name.Text = xr_corp_name.Text
        End If
        ' xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES TICKET"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT"
        End Select

    End Sub

    Private Sub xr_store_addr1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr1.BeforePrint

        xr_store_addr1.Text = StrConv(xr_store_addr1.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xrTaxLbl_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrTaxLbl.BeforePrint

        If xr_tax_cd.Text & "" <> "" Then
            sender.text = "TAX " & xr_tax_cd.Text
        Else
            sender.text = "TAX "
        End If
    End Sub

    Private Sub xrTaxBaseInfo_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrTaxBaseInfo.BeforePrint

        sender.text = xr_billToSt_hidden.Text & " " & xr_pd.Text & "  = " & xr_tax_cd.Text
    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        calc_tax()  ' for some reason balance is calculated early so need to force calculated values before balance (tax and pmts)
        get_pmts_n_mops()
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        If Not IsNumeric(xr_del.Text) Then
            xr_del.Text = 0.0
        End If
        If Not IsNumeric(xr_setup.Text) Then
            xr_setup.Text = 0.0
        End If
        If Not IsNumeric(xr_tax.Text) Then
            xr_tax.Text = 0.0
        End If
        sender.text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_setup.Text) + CDbl(xr_tax.Text)) - g_pmtTotal, 2)
    End Sub
End Class