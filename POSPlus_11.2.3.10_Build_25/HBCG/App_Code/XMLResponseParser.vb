Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Data

Public Class XMLResponseParser

    Public Sub ParseResponseAndFillDS(ByRef responseXML As XmlDocument, ByRef ResponseDS As ResponseAllianceDataSet)
        Dim rsaDS As New DataSet()
        rsaDS.ReadXml(New XmlNodeReader(responseXML))
        Dim responseAllianceDSDataRow As ResponseAllianceDataSet.ResponseRow
        If (Not rsaDS Is Nothing) And (rsaDS.Tables.Count > 0) Then
            If rsaDS.Tables(0).Rows.Count > 0 Then
                responseAllianceDSDataRow = ResponseDS.Response.NewResponseRow
                Dim dr As DataRow = rsaDS.Tables(2).Rows(0)
                If dr.Table.Columns.Contains("returnCode") Then
                    responseAllianceDSDataRow.returnCode = dr("returnCode")
                End If
                If dr.Table.Columns.Contains("authCode") Then
                    responseAllianceDSDataRow.authCode = dr("authCode")
                End If
                If dr.Table.Columns.Contains("reasonCode") Then
                    responseAllianceDSDataRow.reasonCode = dr("reasonCode")
                End If
                If dr.Table.Columns.Contains("avsResponse") Then
                    responseAllianceDSDataRow.avsResponse = dr("avsResponse")
                End If
                ResponseDS.Response.Rows.Add(responseAllianceDSDataRow)
            End If
        End If
    End Sub

End Class