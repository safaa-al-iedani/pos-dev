Public Class LawrenceInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Dim comments_processed As Boolean
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cause_des As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp_title As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cmnts As DevExpress.XtraReports.UI.XRLabel
    ' abbrev key - add as needed
    ' label      - lbl
    ' table      - tbl
    ' tableCell  - tCel
    ' panel      - panl
    ' tableRow   - tRow
    ' title      - ttl
    Private WithEvents xr_cmnt_lbl As DevExpress.XtraReports.UI.XRLabel  ' same as xrLabel10 in existing
    Private WithEvents xr_cmnt_panl As DevExpress.XtraReports.UI.XRPanel ' same as xrPanel1 in existing
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable5 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable6 As DevExpress.XtraReports.UI.XRTable                        ' title table for lines
    Private WithEvents xrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrQtyTtlTCel As DevExpress.XtraReports.UI.XRTableCell                 ' QTY title
    Private WithEvents xrVsnTtlTCel As DevExpress.XtraReports.UI.XRTableCell                ' VSN title
    Private WithEvents xrUntPrcTtlTCel As DevExpress.XtraReports.UI.XRTableCell              ' Price Each title
    Private WithEvents xrTotPrcTtlTCel As DevExpress.XtraReports.UI.XRTableCell             ' Total title
    Private WithEvents xrItmTtlTCel As DevExpress.XtraReports.UI.XRTableCell                ' SKU title
    Private WithEvents xrItmDesTtlTCel As DevExpress.XtraReports.UI.XRTableCell             ' SKU Description title
    Private WithEvents xrTblSoLines As DevExpress.XtraReports.UI.XRTable                 ' SO_LN lines table
    Private WithEvents xrTblRowOfSoLines As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_SoLn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_fillSoLn1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_itmDes As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTblRowOfSoLines2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_fillSoLn2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_finish As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_cover As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grade As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_size As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable8 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell51 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow18 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell53 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_origDoc_info As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xr_payment_holder As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrPanel3 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrCheckBox3 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrCheckBox2 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrCheckBox1 As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_delivery As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xr_pickup As DevExpress.XtraReports.UI.XRCheckBox
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrRichText3 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrCrossBandBox1 As DevExpress.XtraReports.UI.XRCrossBandBox
    Private WithEvents xrRichText4 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrTable4 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable3 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPmtTbl As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrPmtTRow As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "LawrenceInvoice.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.LawrenceInvoice.ResourceManager
        Me.xr_payment_holder = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPanel3 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrCheckBox3 = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.xrCheckBox2 = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.xrCheckBox1 = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xr_delivery = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.xr_pickup = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xrTblSoLines = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTblRowOfSoLines = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_SoLn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_fillSoLn1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_itmDes = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTblRowOfSoLines2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_fillSoLn2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_finish = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_cover = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grade = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_size = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrRichText3 = New DevExpress.XtraReports.UI.XRRichText()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_bill_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_origDoc_info = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable5 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cause_des = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_title = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cmnts = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cmnt_panl = New DevExpress.XtraReports.UI.XRPanel()
        Me.xr_cmnt_lbl = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable3 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrRichText4 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xrTable8 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell51 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow18 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell53 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPmtTbl = New DevExpress.XtraReports.UI.XRTable()
        Me.xrPmtTRow = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_mop = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xrTable6 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrQtyTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrItmTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrVsnTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrItmDesTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrUntPrcTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTotPrcTtlTCel = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.xrCrossBandBox1 = New DevExpress.XtraReports.UI.XRCrossBandBox()
        CType(Me.xrTblSoLines, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xr_payment_holder
        '
        Me.xr_payment_holder.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_payment_holder.CanGrow = False
        Me.xr_payment_holder.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_payments})
        Me.xr_payment_holder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payment_holder.Name = "xr_payment_holder"
        Me.xr_payment_holder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_payment_holder.StylePriority.UseBorderColor = False
        Me.xr_payment_holder.StylePriority.UseFont = False
        Me.xr_payment_holder.Weight = 0.91977250302411073R
        '
        'xr_payments
        '
        Me.xr_payments.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_payments.CanGrow = False
        Me.xr_payments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.Black
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.00005340576!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(112.3199!, 21.29859!)
        Me.xr_payments.StylePriority.UseBorders = False
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.StylePriority.UseTextAlignment = False
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrPanel3
        '
        Me.xrPanel3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xrPanel3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel14, Me.xrLabel13, Me.xrCheckBox3, Me.xrCheckBox2, Me.xrCheckBox1, Me.xrLabel12, Me.xrLabel8})
        Me.xrPanel3.LocationFloat = New DevExpress.Utils.PointFloat(2.16802!, 144.6389!)
        Me.xrPanel3.LockedInUserDesigner = True
        Me.xrPanel3.Name = "xrPanel3"
        Me.xrPanel3.SizeF = New System.Drawing.SizeF(325.7068!, 77.0833!)
        Me.xrPanel3.StylePriority.UseBorders = False
        '
        'xrLabel14
        '
        Me.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel14.CanGrow = False
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(261.7081!, 50.08335!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(58.83316!, 23.0!)
        Me.xrLabel14.StylePriority.UseBorders = False
        Me.xrLabel14.Text = "________________"
        Me.xrLabel14.WordWrap = False
        '
        'xrLabel13
        '
        Me.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel13.CanGrow = False
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(156.0!, 50.08334!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(58.83316!, 23.0!)
        Me.xrLabel13.StylePriority.UseBorders = False
        Me.xrLabel13.Text = "________________"
        Me.xrLabel13.WordWrap = False
        '
        'xrCheckBox3
        '
        Me.xrCheckBox3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCheckBox3.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCheckBox3.LocationFloat = New DevExpress.Utils.PointFloat(214.8332!, 41.91666!)
        Me.xrCheckBox3.Name = "xrCheckBox3"
        Me.xrCheckBox3.SizeF = New System.Drawing.SizeF(46.87498!, 33.08336!)
        Me.xrCheckBox3.StylePriority.UseBorders = False
        Me.xrCheckBox3.StylePriority.UseFont = False
        Me.xrCheckBox3.Text = "HOLD UNTIL"
        '
        'xrCheckBox2
        '
        Me.xrCheckBox2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCheckBox2.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCheckBox2.LocationFloat = New DevExpress.Utils.PointFloat(86.04165!, 41.91666!)
        Me.xrCheckBox2.Name = "xrCheckBox2"
        Me.xrCheckBox2.SizeF = New System.Drawing.SizeF(69.79167!, 33.08336!)
        Me.xrCheckBox2.StylePriority.UseBorders = False
        Me.xrCheckBox2.StylePriority.UseFont = False
        Me.xrCheckBox2.Text = "CONFIRMED DELIVERY"
        '
        'xrCheckBox1
        '
        Me.xrCheckBox1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCheckBox1.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrCheckBox1.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 41.91666!)
        Me.xrCheckBox1.Name = "xrCheckBox1"
        Me.xrCheckBox1.SizeF = New System.Drawing.SizeF(76.04167!, 33.08336!)
        Me.xrCheckBox1.StylePriority.UseBorders = False
        Me.xrCheckBox1.StylePriority.UseFont = False
        Me.xrCheckBox1.Text = "CUSTOMER WANTS ASAP"
        '
        'xrLabel12
        '
        Me.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel12.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(8.5!, 27.25!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(307.8749!, 14.66667!)
        Me.xrLabel12.StylePriority.UseBorders = False
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseTextAlignment = False
        Me.xrLabel12.Text = "***** PLEASE SEE ""REMARKS"" BELOW *****"
        Me.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel8
        '
        Me.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel8.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(6.999969!, 6.999969!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(307.8749!, 14.66667!)
        Me.xrLabel8.StylePriority.UseBorders = False
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseTextAlignment = False
        Me.xrLabel8.Text = "ESTIMATED AVAILABILITY APPROXIMATELY ______ WEEKS"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrPanel2
        '
        Me.xrPanel2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xrPanel2.BorderColor = System.Drawing.Color.Black
        Me.xrPanel2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_delivery, Me.xr_pickup, Me.xrLabel7, Me.xrLabel6})
        Me.xrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(2.000046!, 88.12494!)
        Me.xrPanel2.LockedInUserDesigner = True
        Me.xrPanel2.Name = "xrPanel2"
        Me.xrPanel2.SizeF = New System.Drawing.SizeF(326.1249!, 56.25!)
        Me.xrPanel2.StylePriority.UseBorderColor = False
        Me.xrPanel2.StylePriority.UseBorders = False
        '
        'xr_delivery
        '
        Me.xr_delivery.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_delivery.LocationFloat = New DevExpress.Utils.PointFloat(238.8332!, 5.083286!)
        Me.xr_delivery.Name = "xr_delivery"
        Me.xr_delivery.SizeF = New System.Drawing.SizeF(76.04167!, 43.95835!)
        Me.xr_delivery.StylePriority.UseBorders = False
        Me.xr_delivery.Text = "Delivery or Ship"
        '
        'xr_pickup
        '
        Me.xr_pickup.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pickup.LocationFloat = New DevExpress.Utils.PointFloat(156.0!, 5.083286!)
        Me.xr_pickup.Name = "xr_pickup"
        Me.xr_pickup.SizeF = New System.Drawing.SizeF(76.04167!, 43.95835!)
        Me.xr_pickup.StylePriority.UseBorders = False
        Me.xr_pickup.Text = "Customer Pick-Up"
        '
        'xrLabel7
        '
        Me.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel7.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(8.000056!, 19.74994!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(141.9999!, 23.29169!)
        Me.xrLabel7.StylePriority.UseBorders = False
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "858-549-3577"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel6
        '
        Me.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel6.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(33.00006!, 5.083286!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 14.66667!)
        Me.xrLabel6.StylePriority.UseBorders = False
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseTextAlignment = False
        Me.xrLabel6.Text = "DELIVERY INFO"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTblSoLines
        '
        Me.xrTblSoLines.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTblSoLines.LocationFloat = New DevExpress.Utils.PointFloat(340.5167!, 80.0!)
        Me.xrTblSoLines.Name = "xrTblSoLines"
        Me.xrTblSoLines.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTblRowOfSoLines})
        Me.xrTblSoLines.SizeF = New System.Drawing.SizeF(708.9827!, 100.0!)
        Me.xrTblSoLines.StylePriority.UseBackColor = False
        Me.xrTblSoLines.StylePriority.UseTextAlignment = False
        Me.xrTblSoLines.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTblRowOfSoLines
        '
        Me.xrTblRowOfSoLines.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_SoLn})
        Me.xrTblRowOfSoLines.Name = "xrTblRowOfSoLines"
        Me.xrTblRowOfSoLines.Weight = 1.0R
        '
        'xr_SoLn
        '
        Me.xr_SoLn.BackColor = System.Drawing.Color.Transparent
        Me.xr_SoLn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_SoLn.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_SoLn.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_SoLn.Multiline = True
        Me.xr_SoLn.Name = "xr_SoLn"
        Me.xr_SoLn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 0, 96.0!)
        Me.xr_SoLn.StylePriority.UseBackColor = False
        Me.xr_SoLn.StylePriority.UseBorderColor = False
        Me.xr_SoLn.StylePriority.UseBorders = False
        Me.xr_SoLn.StylePriority.UseFont = False
        Me.xr_SoLn.StylePriority.UseTextAlignment = False
        Me.xr_SoLn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xr_SoLn.Weight = 0.16420828634461371R
        '
        'xr_fillSoLn1
        '
        Me.xr_fillSoLn1.BackColor = System.Drawing.Color.Transparent
        Me.xr_fillSoLn1.BorderColor = System.Drawing.Color.Transparent
        Me.xr_fillSoLn1.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_fillSoLn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fillSoLn1.Name = "xr_fillSoLn1"
        Me.xr_fillSoLn1.StylePriority.UseBackColor = False
        Me.xr_fillSoLn1.StylePriority.UseBorderColor = False
        Me.xr_fillSoLn1.StylePriority.UseBorders = False
        Me.xr_fillSoLn1.StylePriority.UseFont = False
        Me.xr_fillSoLn1.StylePriority.UseTextAlignment = False
        Me.xr_fillSoLn1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_fillSoLn1.Weight = 0.0R
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.Transparent
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_qty.Weight = 0.0R
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.Transparent
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xr_sku.Weight = 0.39821116478510388R
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.Transparent
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xr_vsn.Weight = 1.2092553138732911R
        '
        'xr_itmDes
        '
        Me.xr_itmDes.BackColor = System.Drawing.Color.Transparent
        Me.xr_itmDes.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_itmDes.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_itmDes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_itmDes.Name = "xr_itmDes"
        Me.xr_itmDes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_itmDes.StylePriority.UseBackColor = False
        Me.xr_itmDes.StylePriority.UseBorderColor = False
        Me.xr_itmDes.StylePriority.UseBorders = False
        Me.xr_itmDes.StylePriority.UseFont = False
        Me.xr_itmDes.StylePriority.UseTextAlignment = False
        Me.xr_itmDes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xr_itmDes.Weight = 1.2092553138732911R
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.Transparent
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xr_ret.Weight = 0.44322874069213869R
        '
        'xrTblRowOfSoLines2
        '
        Me.xrTblRowOfSoLines2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_fillSoLn2, Me.xr_finish, Me.xr_cover, Me.xr_grade, Me.xr_size})
        Me.xrTblRowOfSoLines2.Name = "xrTblRowOfSoLines2"
        Me.xrTblRowOfSoLines2.Weight = 1.0R
        '
        'xr_fillSoLn2
        '
        Me.xr_fillSoLn2.BackColor = System.Drawing.Color.Transparent
        Me.xr_fillSoLn2.BorderColor = System.Drawing.Color.Transparent
        Me.xr_fillSoLn2.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_fillSoLn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fillSoLn2.Name = "xr_fillSoLn2"
        Me.xr_fillSoLn2.StylePriority.UseBackColor = False
        Me.xr_fillSoLn2.StylePriority.UseBorderColor = False
        Me.xr_fillSoLn2.StylePriority.UseBorders = False
        Me.xr_fillSoLn2.StylePriority.UseFont = False
        Me.xr_fillSoLn2.StylePriority.UseTextAlignment = False
        Me.xr_fillSoLn2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_fillSoLn2.Weight = 0.0R
        '
        'xr_finish
        '
        Me.xr_finish.BackColor = System.Drawing.Color.Transparent
        Me.xr_finish.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_finish.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_finish.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_finish.Name = "xr_finish"
        Me.xr_finish.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_finish.StylePriority.UseBackColor = False
        Me.xr_finish.StylePriority.UseBorderColor = False
        Me.xr_finish.StylePriority.UseBorders = False
        Me.xr_finish.StylePriority.UseFont = False
        Me.xr_finish.StylePriority.UseTextAlignment = False
        Me.xr_finish.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_finish.Weight = 1.2092553138732911R
        '
        'xr_cover
        '
        Me.xr_cover.BackColor = System.Drawing.Color.Transparent
        Me.xr_cover.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_cover.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_cover.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cover.Name = "xr_cover"
        Me.xr_cover.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cover.StylePriority.UseBackColor = False
        Me.xr_cover.StylePriority.UseBorderColor = False
        Me.xr_cover.StylePriority.UseBorders = False
        Me.xr_cover.StylePriority.UseFont = False
        Me.xr_cover.StylePriority.UseTextAlignment = False
        Me.xr_cover.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_cover.Weight = 1.2092553138732911R
        '
        'xr_grade
        '
        Me.xr_grade.BackColor = System.Drawing.Color.Transparent
        Me.xr_grade.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grade.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_grade.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grade.Name = "xr_grade"
        Me.xr_grade.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grade.StylePriority.UseBackColor = False
        Me.xr_grade.StylePriority.UseBorderColor = False
        Me.xr_grade.StylePriority.UseBorders = False
        Me.xr_grade.StylePriority.UseFont = False
        Me.xr_grade.StylePriority.UseTextAlignment = False
        Me.xr_grade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_grade.Weight = 1.2092553138732911R
        '
        'xr_size
        '
        Me.xr_size.BackColor = System.Drawing.Color.Transparent
        Me.xr_size.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_size.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_size.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_size.Name = "xr_size"
        Me.xr_size.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_size.StylePriority.UseBackColor = False
        Me.xr_size.StylePriority.UseBorderColor = False
        Me.xr_size.StylePriority.UseBorders = False
        Me.xr_size.StylePriority.UseFont = False
        Me.xr_size.StylePriority.UseTextAlignment = False
        Me.xr_size.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_size.Weight = 1.2092553138732911R
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.Transparent
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xr_ext.Weight = 0.43432807922363281R
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(476.7268!, 4.999987!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.416077!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(482.6436!, 32.58327!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.49939!, 2.083332!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'xrRichText2
        '
        Me.xrRichText2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xrRichText2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrRichText2.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.xrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(1.669995!, 322.725!)
        Me.xrRichText2.LockedInUserDesigner = True
        Me.xrRichText2.Name = "xrRichText2"
        Me.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString")
        Me.xrRichText2.SizeF = New System.Drawing.SizeF(326.6235!, 124.2084!)
        Me.xrRichText2.StylePriority.UseBorders = False
        Me.xrRichText2.StylePriority.UseFont = False
        '
        'xrRichText1
        '
        Me.xrRichText1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xrRichText1.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrRichText1.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.xrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(1.667404!, 225.7222!)
        Me.xrRichText1.LockedInUserDesigner = True
        Me.xrRichText1.Name = "xrRichText1"
        Me.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString")
        Me.xrRichText1.SizeF = New System.Drawing.SizeF(326.4576!, 111.4195!)
        Me.xrRichText1.StylePriority.UseBorders = False
        Me.xrRichText1.StylePriority.UseFont = False
        '
        'xrLabel5
        '
        Me.xrLabel5.BorderColor = System.Drawing.Color.Transparent
        Me.xrLabel5.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(23.04166!, 65.12495!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(291.8333!, 23.0!)
        Me.xrLabel5.StylePriority.UseBorderColor = False
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseTextAlignment = False
        Me.xrLabel5.Text = "Shop online: www.lawrance.com / E-mail us: sales@lawrance.com"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrRichText3
        '
        Me.xrRichText3.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.xrRichText3.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 36.54187!)
        Me.xrRichText3.Name = "xrRichText3"
        Me.xrRichText3.SerializableRtfString = resources.GetString("xrRichText3.SerializableRtfString")
        Me.xrRichText3.SizeF = New System.Drawing.SizeF(326.125!, 111.6667!)
        Me.xrRichText3.StylePriority.UseFont = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2})
        Me.PageHeader.HeightF = 114.9583!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTable2
        '
        Me.xrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable2.BorderWidth = 2
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(1.31038!, 0.0!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow4})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(1048.914!, 114.8612!)
        Me.xrTable2.StylePriority.UseBorders = False
        Me.xrTable2.StylePriority.UseBorderWidth = False
        '
        'xrTableRow4
        '
        Me.xrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell14, Me.xrTableCell15})
        Me.xrTableRow4.Name = "xrTableRow4"
        Me.xrTableRow4.Weight = 1.0R
        '
        'xrTableCell14
        '
        Me.xrTableCell14.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrTableCell14.Multiline = True
        Me.xrTableCell14.Name = "xrTableCell14"
        Me.xrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell14.StylePriority.UseFont = False
        Me.xrTableCell14.Text = resources.GetString("xrTableCell14.Text")
        Me.xrTableCell14.Weight = 2.4954680455728666R
        '
        'xrTableCell15
        '
        Me.xrTableCell15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_bill_corp_name, Me.xr_bill_addr2, Me.xr_bill_addr1, Me.xr_bill_b_phone, Me.xr_origDoc_info, Me.xr_bill_city, Me.xr_bill_h_phone, Me.xr_bill_state, Me.xr_bill_zip, Me.xr_bill_full_name, Me.xrTable5, Me.xr_cust_cd, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname, Me.xr_cause_des, Me.xr_ord_tp_hidden, Me.xr_ord_srt, Me.xr_slsp2_hidden, Me.xr_pd})
        Me.xrTableCell15.Name = "xrTableCell15"
        Me.xrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell15.Text = "xrTableCell15"
        Me.xrTableCell15.Weight = 5.4481483666408739R
        '
        'xr_bill_corp_name
        '
        Me.xr_bill_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_corp_name.CanShrink = True
        Me.xr_bill_corp_name.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 0.0!)
        Me.xr_bill_corp_name.Name = "xr_bill_corp_name"
        Me.xr_bill_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_corp_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_corp_name.StylePriority.UseBorders = False
        Me.xr_bill_corp_name.StylePriority.UseFont = False
        '
        'xr_bill_addr2
        '
        Me.xr_bill_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr2.CanShrink = True
        Me.xr_bill_addr2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_addr2.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 52.49993!)
        Me.xr_bill_addr2.Name = "xr_bill_addr2"
        Me.xr_bill_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr2.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_addr2.StylePriority.UseBorders = False
        Me.xr_bill_addr2.StylePriority.UseFont = False
        '
        'xr_bill_addr1
        '
        Me.xr_bill_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_addr1.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 34.4999!)
        Me.xr_bill_addr1.Name = "xr_bill_addr1"
        Me.xr_bill_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr1.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_addr1.StylePriority.UseBorders = False
        Me.xr_bill_addr1.StylePriority.UseFont = False
        '
        'xr_bill_b_phone
        '
        Me.xr_bill_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_b_phone.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(231.6025!, 88.41664!)
        Me.xr_bill_b_phone.Name = "xr_bill_b_phone"
        Me.xr_bill_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_bill_b_phone.StylePriority.UseBorders = False
        Me.xr_bill_b_phone.StylePriority.UseFont = False
        Me.xr_bill_b_phone.Text = "xr_bill_b_phone"
        '
        'xr_origDoc_info
        '
        Me.xr_origDoc_info.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_origDoc_info.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_origDoc_info.LocationFloat = New DevExpress.Utils.PointFloat(48.60252!, 103.5!)
        Me.xr_origDoc_info.Name = "xr_origDoc_info"
        Me.xr_origDoc_info.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_origDoc_info.SizeF = New System.Drawing.SizeF(387.0002!, 17.00001!)
        Me.xr_origDoc_info.StylePriority.UseBorders = False
        Me.xr_origDoc_info.StylePriority.UseFont = False
        Me.xr_origDoc_info.Text = "xr_origDoc_info"
        '
        'xr_bill_city
        '
        Me.xr_bill_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_city.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_city.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 69.49994!)
        Me.xr_bill_city.Name = "xr_bill_city"
        Me.xr_bill_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_city.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
        Me.xr_bill_city.StylePriority.UseBorders = False
        Me.xr_bill_city.StylePriority.UseFont = False
        '
        'xr_bill_h_phone
        '
        Me.xr_bill_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_h_phone.CanShrink = True
        Me.xr_bill_h_phone.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 86.49995!)
        Me.xr_bill_h_phone.Name = "xr_bill_h_phone"
        Me.xr_bill_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_bill_h_phone.StylePriority.UseBorders = False
        Me.xr_bill_h_phone.StylePriority.UseFont = False
        Me.xr_bill_h_phone.Text = "xr_h_phone"
        '
        'xr_bill_state
        '
        Me.xr_bill_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_state.CanGrow = False
        Me.xr_bill_state.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_state.LocationFloat = New DevExpress.Utils.PointFloat(201.6027!, 69.49994!)
        Me.xr_bill_state.Name = "xr_bill_state"
        Me.xr_bill_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_bill_state.StylePriority.UseBorders = False
        Me.xr_bill_state.StylePriority.UseFont = False
        Me.xr_bill_state.Text = "xr_state"
        '
        'xr_bill_zip
        '
        Me.xr_bill_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_zip.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_zip.LocationFloat = New DevExpress.Utils.PointFloat(231.6025!, 69.49994!)
        Me.xr_bill_zip.Name = "xr_bill_zip"
        Me.xr_bill_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_bill_zip.StylePriority.UseBorders = False
        Me.xr_bill_zip.StylePriority.UseFont = False
        Me.xr_bill_zip.Text = "xr_zip"
        '
        'xr_bill_full_name
        '
        Me.xr_bill_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_full_name.CanShrink = True
        Me.xr_bill_full_name.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_bill_full_name.LocationFloat = New DevExpress.Utils.PointFloat(48.60255!, 17.49996!)
        Me.xr_bill_full_name.Name = "xr_bill_full_name"
        Me.xr_bill_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_full_name.SizeF = New System.Drawing.SizeF(240.0!, 17.00001!)
        Me.xr_bill_full_name.StylePriority.UseBorders = False
        Me.xr_bill_full_name.StylePriority.UseFont = False
        '
        'xrTable5
        '
        Me.xrTable5.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable5.LocationFloat = New DevExpress.Utils.PointFloat(441.125!, 0.5000114!)
        Me.xrTable5.Name = "xrTable5"
        Me.xrTable5.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow9, Me.xrTableRow10})
        Me.xrTable5.SizeF = New System.Drawing.SizeF(278.2487!, 123.8333!)
        Me.xrTable5.StylePriority.UseBorders = False
        '
        'xrTableRow9
        '
        Me.xrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell32, Me.xrTableCell33})
        Me.xrTableRow9.Name = "xrTableRow9"
        Me.xrTableRow9.Weight = 1.0R
        '
        'xrTableCell32
        '
        Me.xrTableCell32.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell32.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel2})
        Me.xrTableCell32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell32.ForeColor = System.Drawing.Color.Black
        Me.xrTableCell32.Name = "xrTableCell32"
        Me.xrTableCell32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell32.StylePriority.UseBackColor = False
        Me.xrTableCell32.StylePriority.UseFont = False
        Me.xrTableCell32.StylePriority.UseForeColor = False
        Me.xrTableCell32.Weight = 1.08R
        '
        'xrLabel2
        '
        Me.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 21.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel2.StylePriority.UseBorders = False
        Me.xrLabel2.Text = "ORDER NO."
        '
        'xrTableCell33
        '
        Me.xrTableCell33.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_del_doc_num})
        Me.xrTableCell33.Name = "xrTableCell33"
        Me.xrTableCell33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell33.Weight = 1.0899999999999999R
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.49996!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(129.0!, 24.0!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrTableRow10
        '
        Me.xrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell34, Me.xrTableCell35})
        Me.xrTableRow10.Name = "xrTableRow10"
        Me.xrTableRow10.Weight = 1.0R
        '
        'xrTableCell34
        '
        Me.xrTableCell34.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell34.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel3})
        Me.xrTableCell34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell34.ForeColor = System.Drawing.Color.Black
        Me.xrTableCell34.Name = "xrTableCell34"
        Me.xrTableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell34.StylePriority.UseBackColor = False
        Me.xrTableCell34.StylePriority.UseFont = False
        Me.xrTableCell34.StylePriority.UseForeColor = False
        Me.xrTableCell34.Weight = 1.08R
        '
        'xrLabel3
        '
        Me.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(1.999898!, 18.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(128.848!, 25.0!)
        Me.xrLabel3.StylePriority.UseBorders = False
        Me.xrLabel3.Text = "DATE OF ORDER"
        '
        'xrTableCell35
        '
        Me.xrTableCell35.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_so_wr_dt})
        Me.xrTableCell35.Name = "xrTableCell35"
        Me.xrTableCell35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell35.Weight = 1.0899999999999999R
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(0.0003864765!, 19.49995!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(128.9997!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseBorders = False
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.StylePriority.UseForeColor = False
        Me.xr_so_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_so_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(307.6027!, 17.99997!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(129.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_cust_cd.Visible = False
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(332.6027!, 84.49993!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(381.1861!, 52.49993!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(361.7924!, 52.49993!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_cause_des
        '
        Me.xr_cause_des.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cause_des.LocationFloat = New DevExpress.Utils.PointFloat(326.6027!, 54.41666!)
        Me.xr_cause_des.Name = "xr_cause_des"
        Me.xr_cause_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cause_des.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_cause_des.StylePriority.UseFont = False
        Me.xr_cause_des.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(381.1861!, 69.49994!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(326.6027!, 69.49994!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(338.547!, 69.49994!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.Black
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(409.1525!, 52.49993!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(12.33337!, 17.0!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pd.Visible = False
        '
        'xr_ord_tp_title
        '
        Me.xr_ord_tp_title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_title.ForeColor = System.Drawing.Color.Black
        Me.xr_ord_tp_title.LocationFloat = New DevExpress.Utils.PointFloat(450.9997!, 5.0!)
        Me.xr_ord_tp_title.Name = "xr_ord_tp_title"
        Me.xr_ord_tp_title.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_title.SizeF = New System.Drawing.SizeF(300.0!, 30.0!)
        Me.xr_ord_tp_title.StylePriority.UseBorders = False
        Me.xr_ord_tp_title.StylePriority.UseFont = False
        Me.xr_ord_tp_title.StylePriority.UseForeColor = False
        Me.xr_ord_tp_title.StylePriority.UseTextAlignment = False
        Me.xr_ord_tp_title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_cmnts
        '
        Me.xr_cmnts.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cmnts.CanShrink = True
        Me.xr_cmnts.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cmnts.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 16.0!)
        Me.xr_cmnts.Multiline = True
        Me.xr_cmnts.Name = "xr_cmnts"
        Me.xr_cmnts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 1, 0, 0, 100.0!)
        Me.xr_cmnts.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_cmnts.SizeF = New System.Drawing.SizeF(709.4832!, 50.0!)
        Me.xr_cmnts.StylePriority.UseBackColor = False
        Me.xr_cmnts.StylePriority.UseBorderColor = False
        Me.xr_cmnts.StylePriority.UseFont = False
        Me.xr_cmnts.StylePriority.UseTextAlignment = False
        '
        'xr_cmnt_panl
        '
        Me.xr_cmnt_panl.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xr_cmnt_panl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_cmnt_panl.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cmnts, Me.xr_cmnt_lbl})
        Me.xr_cmnt_panl.LocationFloat = New DevExpress.Utils.PointFloat(340.7412!, 0.0!)
        Me.xr_cmnt_panl.Name = "xr_cmnt_panl"
        Me.xr_cmnt_panl.SizeF = New System.Drawing.SizeF(709.4832!, 59.0!)
        Me.xr_cmnt_panl.StylePriority.UseBorders = False
        '
        'xr_cmnt_lbl
        '
        Me.xr_cmnt_lbl.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cmnt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cmnt_lbl.ForeColor = System.Drawing.Color.Black
        Me.xr_cmnt_lbl.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 2.0!)
        Me.xr_cmnt_lbl.Name = "xr_cmnt_lbl"
        Me.xr_cmnt_lbl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 1, 0, 0, 100.0!)
        Me.xr_cmnt_lbl.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xr_cmnt_lbl.StylePriority.UseBorders = False
        Me.xr_cmnt_lbl.StylePriority.UseFont = False
        Me.xr_cmnt_lbl.StylePriority.UseForeColor = False
        Me.xr_cmnt_lbl.Text = "COMMENTS:"
        '
        'xrLabel1
        '
        Me.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrLabel1.ForeColor = System.Drawing.Color.Black
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(783.9996!, 5.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseBorders = False
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.StylePriority.UseTextAlignment = False
        Me.xrLabel1.Text = "Salesperson"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_slsp
        '
        Me.xr_slsp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp.ForeColor = System.Drawing.Color.Black
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(875.9997!, 5.0!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(174.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseBorders = False
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseForeColor = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cmnt_panl, Me.xrTable4, Me.xrTable3, Me.xrRichText4, Me.xrTable8, Me.xrTable1, Me.xrPmtTbl, Me.xrRichText3})
        Me.ReportFooter.HeightF = 202.7917!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrTable4
        '
        Me.xrTable4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.xrTable4.KeepTogether = True
        Me.xrTable4.LocationFloat = New DevExpress.Utils.PointFloat(551.3939!, 33.0!)
        Me.xrTable4.Name = "xrTable4"
        Me.xrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow8})
        Me.xrTable4.SizeF = New System.Drawing.SizeF(116.6669!, 127.7917!)
        '
        'xrTableRow8
        '
        Me.xrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell9})
        Me.xrTableRow8.Name = "xrTableRow8"
        Me.xrTableRow8.Weight = 4.4666665649414057R
        '
        'xrTableCell9
        '
        Me.xrTableCell9.CanGrow = False
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Text = "Terms"
        Me.xrTableCell9.Weight = 3.0R
        '
        'xrTable3
        '
        Me.xrTable3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.xrTable3.KeepTogether = True
        Me.xrTable3.LocationFloat = New DevExpress.Utils.PointFloat(668.0608!, 33.0!)
        Me.xrTable3.Name = "xrTable3"
        Me.xrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow5, Me.xrTableRow6, Me.xrTableRow7, Me.xrTableRow19})
        Me.xrTable3.SizeF = New System.Drawing.SizeF(115.8555!, 85.19447!)
        '
        'xrTableRow5
        '
        Me.xrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell10})
        Me.xrTableRow5.Name = "xrTableRow5"
        Me.xrTableRow5.Weight = 1.0R
        '
        'xrTableCell10
        '
        Me.xrTableCell10.CanGrow = False
        Me.xrTableCell10.Name = "xrTableCell10"
        Me.xrTableCell10.Weight = 3.0R
        '
        'xrTableRow6
        '
        Me.xrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell12})
        Me.xrTableRow6.Name = "xrTableRow6"
        Me.xrTableRow6.Weight = 1.0R
        '
        'xrTableCell12
        '
        Me.xrTableCell12.CanGrow = False
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Weight = 3.0R
        '
        'xrTableRow7
        '
        Me.xrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell8})
        Me.xrTableRow7.Name = "xrTableRow7"
        Me.xrTableRow7.Weight = 1.0R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.CanGrow = False
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Weight = 3.0R
        '
        'xrTableRow19
        '
        Me.xrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell13})
        Me.xrTableRow19.Name = "xrTableRow19"
        Me.xrTableRow19.Weight = 1.0R
        '
        'xrTableCell13
        '
        Me.xrTableCell13.CanGrow = False
        Me.xrTableCell13.Name = "xrTableCell13"
        Me.xrTableCell13.Weight = 3.0R
        '
        'xrRichText4
        '
        Me.xrRichText4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRichText4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 149.2085!)
        Me.xrRichText4.Name = "xrRichText4"
        Me.xrRichText4.SerializableRtfString = resources.GetString("xrRichText4.SerializableRtfString")
        Me.xrRichText4.SizeF = New System.Drawing.SizeF(328.125!, 34.58328!)
        Me.xrRichText4.StylePriority.UseBorders = False
        '
        'xrTable8
        '
        Me.xrTable8.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.xrTable8.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable8.KeepTogether = True
        Me.xrTable8.LocationFloat = New DevExpress.Utils.PointFloat(783.9162!, 33.0!)
        Me.xrTable8.Name = "xrTable8"
        Me.xrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow13, Me.xrTableRow14, Me.xrTableRow15, Me.xrTableRow16, Me.xrTableRow17, Me.xrTableRow18})
        Me.xrTable8.SizeF = New System.Drawing.SizeF(265.0007!, 112.2086!)
        Me.xrTable8.StylePriority.UseBorders = False
        '
        'xrTableRow13
        '
        Me.xrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell42, Me.xr_subtotal})
        Me.xrTableRow13.Name = "xrTableRow13"
        Me.xrTableRow13.Weight = 1.0R
        '
        'xrTableCell42
        '
        Me.xrTableCell42.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell42.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell42.CanGrow = False
        Me.xrTableCell42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell42.Name = "xrTableCell42"
        Me.xrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell42.StylePriority.UseBackColor = False
        Me.xrTableCell42.StylePriority.UseBorderColor = False
        Me.xrTableCell42.StylePriority.UseFont = False
        Me.xrTableCell42.StylePriority.UseTextAlignment = False
        Me.xrTableCell42.Text = "AMOUNT OF SALE"
        Me.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell42.Weight = 1.2502274969758891R
        Me.xrTableCell42.WordWrap = False
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.CanGrow = False
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 0.91977250302411073R
        Me.xr_subtotal.WordWrap = False
        '
        'xrTableRow14
        '
        Me.xrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell44, Me.xr_del})
        Me.xrTableRow14.Name = "xrTableRow14"
        Me.xrTableRow14.Weight = 1.0R
        '
        'xrTableCell44
        '
        Me.xrTableCell44.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell44.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell44.CanGrow = False
        Me.xrTableCell44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell44.Name = "xrTableCell44"
        Me.xrTableCell44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell44.StylePriority.UseBackColor = False
        Me.xrTableCell44.StylePriority.UseBorderColor = False
        Me.xrTableCell44.StylePriority.UseFont = False
        Me.xrTableCell44.StylePriority.UseTextAlignment = False
        Me.xrTableCell44.Text = "DELIVERY STOP CHARGE"
        Me.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell44.Weight = 1.2502274969758891R
        Me.xrTableCell44.WordWrap = False
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.CanGrow = False
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 0.91977250302411073R
        Me.xr_del.WordWrap = False
        '
        'xrTableRow15
        '
        Me.xrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell47, Me.xr_tax})
        Me.xrTableRow15.Name = "xrTableRow15"
        Me.xrTableRow15.Weight = 1.0R
        '
        'xrTableCell47
        '
        Me.xrTableCell47.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell47.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell47.CanGrow = False
        Me.xrTableCell47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell47.Name = "xrTableCell47"
        Me.xrTableCell47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell47.StylePriority.UseBackColor = False
        Me.xrTableCell47.StylePriority.UseBorderColor = False
        Me.xrTableCell47.StylePriority.UseFont = False
        Me.xrTableCell47.StylePriority.UseTextAlignment = False
        Me.xrTableCell47.Text = "SALES TAX"
        Me.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell47.Weight = 1.2502274969758891R
        Me.xrTableCell47.WordWrap = False
        '
        'xr_tax
        '
        Me.xr_tax.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax.CanGrow = False
        Me.xr_tax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_tax.StylePriority.UseBorderColor = False
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseTextAlignment = False
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax.Weight = 0.91977250302411073R
        Me.xr_tax.WordWrap = False
        '
        'xrTableRow16
        '
        Me.xrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell49, Me.xr_grand_total})
        Me.xrTableRow16.Name = "xrTableRow16"
        Me.xrTableRow16.Weight = 1.0R
        '
        'xrTableCell49
        '
        Me.xrTableCell49.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell49.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell49.CanGrow = False
        Me.xrTableCell49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell49.Name = "xrTableCell49"
        Me.xrTableCell49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell49.StylePriority.UseBackColor = False
        Me.xrTableCell49.StylePriority.UseBorderColor = False
        Me.xrTableCell49.StylePriority.UseFont = False
        Me.xrTableCell49.StylePriority.UseTextAlignment = False
        Me.xrTableCell49.Text = "TOTAL"
        Me.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell49.Weight = 1.2502274969758891R
        Me.xrTableCell49.WordWrap = False
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.CanGrow = False
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 0.91977250302411073R
        Me.xr_grand_total.WordWrap = False
        '
        'xrTableRow17
        '
        Me.xrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell51, Me.xr_payment_holder})
        Me.xrTableRow17.Name = "xrTableRow17"
        Me.xrTableRow17.Weight = 1.0R
        '
        'xrTableCell51
        '
        Me.xrTableCell51.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell51.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell51.CanGrow = False
        Me.xrTableCell51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell51.Name = "xrTableCell51"
        Me.xrTableCell51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell51.StylePriority.UseBackColor = False
        Me.xrTableCell51.StylePriority.UseBorderColor = False
        Me.xrTableCell51.StylePriority.UseFont = False
        Me.xrTableCell51.StylePriority.UseTextAlignment = False
        Me.xrTableCell51.Text = "PAYMENTS"
        Me.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell51.Weight = 1.2502274969758891R
        Me.xrTableCell51.WordWrap = False
        '
        'xrTableRow18
        '
        Me.xrTableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell53, Me.xr_balance})
        Me.xrTableRow18.Name = "xrTableRow18"
        Me.xrTableRow18.Weight = 1.0R
        '
        'xrTableCell53
        '
        Me.xrTableCell53.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell53.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell53.CanGrow = False
        Me.xrTableCell53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell53.Name = "xrTableCell53"
        Me.xrTableCell53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell53.StylePriority.UseBackColor = False
        Me.xrTableCell53.StylePriority.UseBorderColor = False
        Me.xrTableCell53.StylePriority.UseFont = False
        Me.xrTableCell53.StylePriority.UseTextAlignment = False
        Me.xrTableCell53.Text = "BALANCE"
        Me.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell53.Weight = 1.2502274969758891R
        Me.xrTableCell53.WordWrap = False
        '
        'xr_balance
        '
        Me.xr_balance.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_balance.CanGrow = False
        Me.xr_balance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_balance.StylePriority.UseBorderColor = False
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.Weight = 0.91977250302411073R
        Me.xr_balance.WordWrap = False
        '
        'xrTable1
        '
        Me.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.xrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.xrTable1.KeepTogether = True
        Me.xrTable1.LocationFloat = New DevExpress.Utils.PointFloat(329.8736!, 33.46528!)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow3})
        Me.xrTable1.SizeF = New System.Drawing.SizeF(221.2693!, 18.0!)
        Me.xrTable1.StylePriority.UseBorders = False
        Me.xrTable1.StylePriority.UseFont = False
        Me.xrTable1.StylePriority.UseForeColor = False
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell7, Me.xrTableCell11})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.Weight = 1.0R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell7.CanGrow = False
        Me.xrTableCell7.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.xrTableCell7.ForeColor = System.Drawing.Color.Black
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell7.StylePriority.UseBorders = False
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UseForeColor = False
        Me.xrTableCell7.StylePriority.UseTextAlignment = False
        Me.xrTableCell7.Text = "Payment Type"
        Me.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell7.Weight = 0.26006085495536863R
        '
        'xrTableCell11
        '
        Me.xrTableCell11.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell11.CanGrow = False
        Me.xrTableCell11.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.xrTableCell11.ForeColor = System.Drawing.Color.Black
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseBorders = False
        Me.xrTableCell11.StylePriority.UseFont = False
        Me.xrTableCell11.StylePriority.UseForeColor = False
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "Amount"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell11.Weight = 0.28681414504463132R
        '
        'xrPmtTbl
        '
        Me.xrPmtTbl.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.xrPmtTbl.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrPmtTbl.LocationFloat = New DevExpress.Utils.PointFloat(330.0417!, 52.0!)
        Me.xrPmtTbl.Name = "xrPmtTbl"
        Me.xrPmtTbl.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrPmtTRow})
        Me.xrPmtTbl.SizeF = New System.Drawing.SizeF(221.2693!, 93.19447!)
        Me.xrPmtTbl.StylePriority.UseTextAlignment = False
        '
        'xrPmtTRow
        '
        Me.xrPmtTRow.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_mop, Me.xr_pmt_amt})
        Me.xrPmtTRow.Name = "xrPmtTRow"
        Me.xrPmtTRow.Weight = 1.0R
        '
        'xr_mop
        '
        Me.xr_mop.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_mop.CanGrow = False
        Me.xr_mop.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.StylePriority.UseBorders = False
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_mop.Weight = 0.26006085495536863R
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_pmt_amt.CanGrow = False
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.StylePriority.UseBorders = False
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_pmt_amt.Weight = 0.28681414504463132R
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0.0R
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0.0R
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0.0R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0.0R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0.0R
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0.0R
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0.0R
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable6, Me.xr_slsp, Me.xrLabel1, Me.xr_ord_tp_title, Me.xrPictureBox1, Me.xr_disc_amt, Me.xr_void_flag, Me.xrLabel5, Me.xrPanel3, Me.xrPanel2, Me.xrRichText1, Me.xrRichText2, Me.xrTblSoLines})
        Me.GroupHeader1.HeightF = 446.9334!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xrTable6
        '
        Me.xrTable6.BackColor = System.Drawing.Color.Transparent
        Me.xrTable6.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xrTable6.LocationFloat = New DevExpress.Utils.PointFloat(340.5167!, 41.45834!)
        Me.xrTable6.Name = "xrTable6"
        Me.xrTable6.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow11})
        Me.xrTable6.SizeF = New System.Drawing.SizeF(709.4832!, 37.0!)
        Me.xrTable6.StylePriority.UseBackColor = False
        Me.xrTable6.StylePriority.UseFont = False
        Me.xrTable6.StylePriority.UseTextAlignment = False
        Me.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow11
        '
        Me.xrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrQtyTtlTCel, Me.xrItmTtlTCel, Me.xrTableCell16, Me.xrVsnTtlTCel, Me.xrItmDesTtlTCel, Me.xrUntPrcTtlTCel, Me.xrTotPrcTtlTCel})
        Me.xrTableRow11.Name = "xrTableRow11"
        Me.xrTableRow11.Weight = 1.0R
        '
        'xrQtyTtlTCel
        '
        Me.xrQtyTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrQtyTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrQtyTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrQtyTtlTCel.Name = "xrQtyTtlTCel"
        Me.xrQtyTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrQtyTtlTCel.StylePriority.UseBackColor = False
        Me.xrQtyTtlTCel.StylePriority.UseBorderColor = False
        Me.xrQtyTtlTCel.StylePriority.UseFont = False
        Me.xrQtyTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrQtyTtlTCel.Text = "QTY"
        Me.xrQtyTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrQtyTtlTCel.Weight = 0.25465504348497664R
        '
        'xrItmTtlTCel
        '
        Me.xrItmTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrItmTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrItmTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrItmTtlTCel.Multiline = True
        Me.xrItmTtlTCel.Name = "xrItmTtlTCel"
        Me.xrItmTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrItmTtlTCel.StylePriority.UseBackColor = False
        Me.xrItmTtlTCel.StylePriority.UseBorderColor = False
        Me.xrItmTtlTCel.StylePriority.UseFont = False
        Me.xrItmTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrItmTtlTCel.Text = "SKU"
        Me.xrItmTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrItmTtlTCel.Weight = 0.44491915753471678R
        '
        'xrTableCell16
        '
        Me.xrTableCell16.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell16.Name = "xrTableCell16"
        Me.xrTableCell16.StylePriority.UseBorderColor = False
        Me.xrTableCell16.StylePriority.UseFont = False
        Me.xrTableCell16.Text = "VEND"
        Me.xrTableCell16.Weight = 0.32265335510021814R
        '
        'xrVsnTtlTCel
        '
        Me.xrVsnTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrVsnTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrVsnTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrVsnTtlTCel.Name = "xrVsnTtlTCel"
        Me.xrVsnTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrVsnTtlTCel.StylePriority.UseBackColor = False
        Me.xrVsnTtlTCel.StylePriority.UseBorderColor = False
        Me.xrVsnTtlTCel.StylePriority.UseFont = False
        Me.xrVsnTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrVsnTtlTCel.Text = "VSN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrVsnTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrVsnTtlTCel.Weight = 1.1686179770843865R
        '
        'xrItmDesTtlTCel
        '
        Me.xrItmDesTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrItmDesTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrItmDesTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrItmDesTtlTCel.Name = "xrItmDesTtlTCel"
        Me.xrItmDesTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrItmDesTtlTCel.StylePriority.UseBackColor = False
        Me.xrItmDesTtlTCel.StylePriority.UseBorderColor = False
        Me.xrItmDesTtlTCel.StylePriority.UseFont = False
        Me.xrItmDesTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrItmDesTtlTCel.Text = "DESCRIPTION" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrItmDesTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrItmDesTtlTCel.Weight = 1.1732393638860674R
        '
        'xrUntPrcTtlTCel
        '
        Me.xrUntPrcTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrUntPrcTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrUntPrcTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrUntPrcTtlTCel.Name = "xrUntPrcTtlTCel"
        Me.xrUntPrcTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrUntPrcTtlTCel.StylePriority.UseBackColor = False
        Me.xrUntPrcTtlTCel.StylePriority.UseBorderColor = False
        Me.xrUntPrcTtlTCel.StylePriority.UseFont = False
        Me.xrUntPrcTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrUntPrcTtlTCel.Text = "PRICE EACH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrUntPrcTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrUntPrcTtlTCel.Weight = 0.48899535828425533R
        '
        'xrTotPrcTtlTCel
        '
        Me.xrTotPrcTtlTCel.BackColor = System.Drawing.Color.Transparent
        Me.xrTotPrcTtlTCel.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTotPrcTtlTCel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrTotPrcTtlTCel.Name = "xrTotPrcTtlTCel"
        Me.xrTotPrcTtlTCel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTotPrcTtlTCel.StylePriority.UseBackColor = False
        Me.xrTotPrcTtlTCel.StylePriority.UseBorderColor = False
        Me.xrTotPrcTtlTCel.StylePriority.UseFont = False
        Me.xrTotPrcTtlTCel.StylePriority.UseTextAlignment = False
        Me.xrTotPrcTtlTCel.Text = "TOTAL"
        Me.xrTotPrcTtlTCel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTotPrcTtlTCel.Weight = 0.47917509266071534R
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 4.0!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(324.8749!, 61.45834!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.xrPictureBox1.StylePriority.UseBorders = False
        '
        'PageFooter
        '
        Me.PageFooter.HeightF = 0.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 33.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 10.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'xrCrossBandBox1
        '
        Me.xrCrossBandBox1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.None
        Me.xrCrossBandBox1.EndBand = Me.ReportFooter
        Me.xrCrossBandBox1.EndPointFloat = New DevExpress.Utils.PointFloat(0.0!, 183.0!)
        Me.xrCrossBandBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 114.8612!)
        Me.xrCrossBandBox1.Name = "xrCrossBandBox1"
        Me.xrCrossBandBox1.StartBand = Me.PageHeader
        Me.xrCrossBandBox1.StartPointFloat = New DevExpress.Utils.PointFloat(0.0!, 114.8612!)
        Me.xrCrossBandBox1.WidthF = 330.2083!
        '
        'LawrenceInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.xrCrossBandBox1})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 33, 10)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        CType(Me.xrTblSoLines, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrPmtTbl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_origDoc_info.DataBindings.Add("Text", DataSource, "SO.ORIG_DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_bill_corp_name.DataBindings.Add("Text", DataSource, "CUST.CORP_NAME")
        xr_bill_full_name.DataBindings.Add("Text", DataSource, "CUST.CUST_FULL_NAME")
        xr_bill_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_bill_addr2.DataBindings.Add("Text", DataSource, "CUST.BILL_ADDR2")
        xr_bill_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_bill_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_bill_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_bill_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_bill_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        'xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        'xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC")
        'xr_sku.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        'xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        'xr_finish.DataBindings.Add("Text", DataSource, "SO_LN.FINISH")
        'xr_cover.DataBindings.Add("Text", DataSource, "SO_LN.COVER")
        'xr_grade.DataBindings.Add("Text", DataSource, "SO_LN.GRADE")
        'xr_size.DataBindings.Add("Text", DataSource, "SO_LN.SIZ")
        'xr_itmDes.DataBindings.Add("Text", DataSource, "ITM.DES")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_cause_des.DataBindings.Add("Text", DataSource, "CAUSE_DES")

    End Sub

    Private Sub xr_ord_tp_title_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp_title.BeforePrint

        Select Case xr_ord_tp_hidden.Text   'ord_tp_cd
            Case "SAL"
                sender.text = "Sales Order"
            Case "CRM"
                sender.text = "Credit Memo"
            Case "MCR"
                sender.text = "Miscellaneous Credit"
            Case "MDB"
                sender.text = "Miscellaneous Debit"
        End Select
    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "Pick-Up"
            Case "D"
                sender.text = "Delivery"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint

        If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "select sum(NVL(CUST_TAX_CHG,0))+sum(NVL(STORE_TAX_CHG,0)) AS TAX_CHG FROM SO_LN WHERE DEL_DOC_NUM='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            sql = sql & "AND SO_LN.VOID_FLAG='N'"

            'Set SQL OBJECT 
            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read Then
                    If IsNumeric(MyDataReader2.Item("TAX_CHG").ToString) Then
                        sender.text = FormatNumber(MyDataReader2.Item("TAX_CHG").ToString, 2)
                    End If
                End If
                MyDataReader2.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If

    End Sub

    'Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
    '    If Not IsNumeric(sender.text) Then
    '        sender.text = "0.00"
    '    End If
    'End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint, xr_payment_holder.BeforePrint
        ' produce sum of debits and credits for payments on the lower right corner

        'If processed_payments = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        'sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                    xrTable1.Visible = False
                End If
            Else
                sender.text = FormatNumber(0, 2)
                xrTable1.Visible = False
            End If
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        Dim pmts As Double = 0

        If IsNumeric(xr_payments.Text) Then
            pmts = CDbl(xr_payments.Text)
        End If
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        sender.text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)

    End Sub

    Private Sub xr_mop_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop.BeforePrint

        'If mop_processed = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, TO_CHAR(POST_DT,'MM-DD-YY') AS POST_DT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM, a.MOP_CD from ar_trn a, mop b, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If MyDataReader2.Item("DES").ToString & "" <> "" Then
                    sender.text = sender.text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                Else
                    sender.text = sender.text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
                End If
                xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
            Loop
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_cmnts.BeforePrint

        Const maxCmntLength As Integer = 415

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim cmd As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sqlSb As New StringBuilder

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sqlSb.Append("Select SO_CMNT.DT, SO_CMNT.TEXT, NVL(SO_CMNT.CMNT_TYPE, 'S') as CMNT_TYPE, NVL(PERM, 'N') as PERM, SO_CMNT.DEL_DOC_NUM ").Append(
                         "from SO_CMNT, SO Where SO.DEL_DOC_NUM = :docNum ").Append(
                         " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD ").Append(
                         " AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND NVL(SO_CMNT.CMNT_TYPE,'S') = 'S' ").Append(
                         " AND (so_cmnt.del_Doc_num IS NULL OR so_cmnt.del_Doc_num = :docNum) ORDER BY SEQ#")

            cmd = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

            cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":docNum").Value = xr_del_doc_num.Text

            Try
                Dim datSet As DataSet = New DataSet
                Dim oraDatA As System.Data.OracleClient.OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                oraDatA.Fill(datSet)
                sender.text = getSoCmnts4View(datSet.Tables(0), False, maxCmntLength)
            Catch
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Public Shared Function getSoCmnts4View(ByRef cmntTab As DataTable, ByVal useCrLF As Boolean, ByVal maxLength As Integer) As String

        Dim cmntRow As DataRow
        Dim cmntSb As New StringBuilder

        If (Not cmntTab Is Nothing) AndAlso cmntTab.Rows.Count > 0 Then

            Dim currDt As String = ""
            Dim currPerm As String = ""
            Dim currDoc As String = ""
            Dim currTp As String = ""
            'Dim currEmp As String = ""
            'Dim currOrig As String = ""
            'currEmp = cmntRow("
            'currOrig = cmntRow("PERM").ToString

            For Each cmntRow In cmntTab.Rows

                If cmntRow("DT").ToString.Equals(currDt) AndAlso
                    cmntRow("PERM").ToString.Equals(currPerm) AndAlso
                    cmntRow("CMNT_TYPE").ToString.Equals(currTp) AndAlso
                    ((currDoc.isEmpty AndAlso
                      cmntRow("DEL_DOC_NUM").ToString.isEmpty) OrElse
                     cmntRow("DEL_DOC_NUM").ToString.Equals(currDoc)) Then
                    ' if a match to previous 

                    cmntSb.Append(cmntRow("TEXT").ToString)

                Else
                    If currDt.isEmpty Then
                        ' if first time thru
                        cmntSb.Append(cmntRow("TEXT").ToString)

                    ElseIf useCrLF Then
                        cmntSb.Append(Constants.vbCrLf).Append(cmntRow("TEXT").ToString)

                    Else
                        cmntSb.Append("; ").Append(cmntRow("TEXT").ToString)
                    End If
                    currDt = cmntRow("DT").ToString
                    currPerm = cmntRow("PERM").ToString
                    currDoc = cmntRow("DEL_DOC_NUM").ToString
                    currTp = cmntRow("CMNT_TYPE").ToString
                End If
            Next

        End If

        Dim len As Integer = cmntSb.ToString.Length
        If len > maxLength Then
            len = maxLength
        End If
        If cmntSb.Length > 0 Then
            Return cmntSb.ToString.Substring(0, len)
        Else
            Return ""
        End If
    End Function

    Private Sub xr_sale_last_four_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        If Len(xr_del_doc_num.Text) = 11 Then
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 4)
        Else
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 5)
        End If

    End Sub

    Private Sub xr_origDoc_info_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_origDoc_info.BeforePrint

        If "SAL".Equals(xr_ord_tp_hidden.Text) Then

            sender.text = ""

        Else
            sender.text = "Orig Sales Order: " + xr_origDoc_info.Text + "    Reason: " + xr_cause_des.Text
        End If

    End Sub

    Private Sub xr_slsp1_email_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        If sender.text & "" <> "" Then
            sender.text = sender.text.ToString.ToLower
        End If

    End Sub

    Private Sub xr_SoLn_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_SoLn.BeforePrint
        'Private Sub xr_qty_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmd As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sqlSb As New StringBuilder
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sqlSb.Append("select SO_LN.ITM_CD, NVL(ITM.VSN,' ') as vsn, SO_LN.QTY, NVL(SO_LN.UNIT_PRC,0) AS UNIT_PRC, itm.ve_cd, ").Append(
            " NVL(itm.des,' ') as des, NVL(itm.finish,' ') AS finish, NVL(itm.cover,' ') AS cover, NVL(itm.grade,' ') AS grade, ").Append(
            " NVL(itm.siz,' ') AS siz, NVL((UNIT_PRC*QTY),0) AS EXT_PRC FROM SO_LN, ITM ").Append(
            "where  SO_LN.ITM_CD=ITM.ITM_CD ").Append(
            "and  SO_LN.DEL_DOC_NUM = :docNum ").Append(
            "AND SO_LN.VOID_FLAG='N' ").Append(
            "ORDER BY SO_LN.DEL_DOC_LN#")

        'Set SQL OBJECT 
        cmd = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

        cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
        cmd.Parameters(":docNum").Value = Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString

        Dim qty As String
        Dim itmCd As String
        Dim vsn As String
        Dim des As String
        Dim unitPrc As String
        Dim extPrc As String
        Dim fin As String
        Dim cov As String
        Dim grd As String
        Dim siz As String
        Dim vend As String
        Dim strLen As Integer
        Dim tmpLine As String
        Dim nxtLine As String
        Dim indx As Integer = 1
        Dim maxLine As Integer = 95

        Const lineBreakUnderLine As String = "______________________________________________________________________" +
            "________________________________"

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(cmd)

            xr_SoLn.Text = ""
            Do While MyDataReader2.Read
                ' " | " +
                qty = FormatNumber(MyDataReader2.Item("QTY").ToString, 0)
                itmCd = MyDataReader2.Item("ITM_CD").ToString
                vend = MyDataReader2.Item("VE_CD").ToString
                vsn = MyDataReader2.Item("VSN").ToString
                des = MyDataReader2.Item("DES").ToString
                unitPrc = FormatCurrency(MyDataReader2.Item("UNIT_PRC").ToString, 2)
                extPrc = FormatCurrency(MyDataReader2.Item("EXT_PRC").ToString, 2)
                fin = MyDataReader2.Item("finish").ToString
                cov = MyDataReader2.Item("cover").ToString
                grd = MyDataReader2.Item("grade").ToString
                siz = MyDataReader2.Item("siz").ToString

                ' need font at Courier New so can do this padding
                xr_SoLn.Text = xr_SoLn.Text & qty.PadLeft(5) + " " +
                    itmCd.PadRight(10) + " " +
                    vend.PadRight(7) + " " +
                    vsn.PadRight(26) + " " +
                    des.PadRight(26) + " " +
                    unitPrc.PadLeft(11) + " " +
                    extPrc.PadLeft(11) + Constants.vbCrLf
                tmpLine = fin + cov + grd + siz
                strLen = Len(tmpLine)
                nxtLine = ""
                indx = 1
                If strLen > 0 Then

                    Do Until indx > strLen

                        nxtLine = nxtLine + " ".PadLeft(6) + Mid(tmpLine, indx, maxLine) + Constants.vbCrLf
                        indx = indx + maxLine
                    Loop
                    xr_SoLn.Text = xr_SoLn.Text & nxtLine
                End If
                xr_SoLn.Text = xr_SoLn.Text & lineBreakUnderLine & Constants.vbCrLf
                '+ " ".PadLeft(6) +
                'fin.PadRight(53).Substring(0, 23) + " " +
                'cov.PadRight(53).Substring(0, 23) + " " +
                'grd.PadRight(53).Substring(0, 23) + " " +
                'siz.PadRight(44).Substring(0, 23) + " " + Constants.vbCrLf +
                'lineBreakUnderLine & Constants.vbCrLf ' + Constants.vbCrLf cannot get 10 on page with this
                subtotal = subtotal + CDbl(MyDataReader2.Item("EXT_PRC").ToString)
            Loop
        Catch
            Throw
        End Try

    End Sub

End Class