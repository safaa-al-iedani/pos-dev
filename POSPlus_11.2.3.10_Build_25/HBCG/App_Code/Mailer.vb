Public Class Mailer
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents winControlContainer1 As DevExpress.XtraReports.UI.WinControlContainer
    Private WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents winControlContainer2 As DevExpress.XtraReports.UI.WinControlContainer
    Private WithEvents pictureBox2 As System.Windows.Forms.PictureBox
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Mailer.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.Mailer.ResourceManager
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.winControlContainer1 = New DevExpress.XtraReports.UI.WinControlContainer
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.pictureBox2 = New System.Windows.Forms.PictureBox
        Me.winControlContainer2 = New DevExpress.XtraReports.UI.WinControlContainer
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel3, Me.winControlContainer2, Me.xrTable1, Me.xrLabel2, Me.xrLine1, Me.xrLabel1, Me.winControlContainer1, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_lname, Me.xr_fname, Me.xr_addr1})
        Me.Detail.Height = 908
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_zip
        '
        Me.xr_zip.Location = New System.Drawing.Point(717, 700)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.Size = New System.Drawing.Size(67, 17)
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Location = New System.Drawing.Point(683, 700)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.Size = New System.Drawing.Size(25, 17)
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Location = New System.Drawing.Point(508, 700)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.Size = New System.Drawing.Size(167, 17)
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Location = New System.Drawing.Point(508, 681)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_lname
        '
        Me.xr_lname.Location = New System.Drawing.Point(650, 642)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.Size = New System.Drawing.Size(133, 17)
        Me.xr_lname.Text = "xr_lname"
        '
        'xr_fname
        '
        Me.xr_fname.Location = New System.Drawing.Point(508, 642)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.Size = New System.Drawing.Size(133, 17)
        Me.xr_fname.Text = "xr_fname"
        '
        'xr_addr1
        '
        Me.xr_addr1.Location = New System.Drawing.Point(508, 664)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(800, 456)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBox1.TabIndex = 0
        Me.pictureBox1.TabStop = False
        '
        'winControlContainer1
        '
        Me.winControlContainer1.Location = New System.Drawing.Point(0, 0)
        Me.winControlContainer1.Name = "winControlContainer1"
        Me.winControlContainer1.Size = New System.Drawing.Size(833, 475)
        Me.winControlContainer1.WinControl = Me.pictureBox1
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Snap ITC", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.ForeColor = System.Drawing.Color.White
        Me.xrLabel1.Location = New System.Drawing.Point(417, 425)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.Size = New System.Drawing.Size(417, 50)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "All 5 pieces - $2199!"
        '
        'xrLine1
        '
        Me.xrLine1.ForeColor = System.Drawing.Color.Olive
        Me.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.xrLine1.LineWidth = 3
        Me.xrLine1.Location = New System.Drawing.Point(417, 508)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.Size = New System.Drawing.Size(8, 392)
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLabel2
        '
        Me.xrLabel2.Location = New System.Drawing.Point(8, 508)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.Size = New System.Drawing.Size(367, 42)
        Me.xrLabel2.Text = "Hurry in for this and other spectacular deals!  Offer available while supplies la" & _
            "st."
        '
        'xrTable1
        '
        Me.xrTable1.Location = New System.Drawing.Point(25, 583)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow1})
        Me.xrTable1.Size = New System.Drawing.Size(350, 75)
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell1, Me.xrTableCell2})
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Size = New System.Drawing.Size(350, 75)
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Location = New System.Drawing.Point(0, 0)
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell1.Size = New System.Drawing.Size(158, 75)
        Me.xrTableCell1.Text = "Please visit our showroom at:"
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Location = New System.Drawing.Point(158, 0)
        Me.xrTableCell2.Multiline = True
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell2.Size = New System.Drawing.Size(192, 75)
        Me.xrTableCell2.Text = "Fake Furniture" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2543 Main Street" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Columbus, OH 43082" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "614.555.1256"
        '
        'pictureBox2
        '
        Me.pictureBox2.Image = CType(resources.GetObject("pictureBox2.Image"), System.Drawing.Image)
        Me.pictureBox2.Location = New System.Drawing.Point(0, 0)
        Me.pictureBox2.Name = "pictureBox2"
        Me.pictureBox2.Size = New System.Drawing.Size(128, 184)
        Me.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureBox2.TabIndex = 1
        Me.pictureBox2.TabStop = False
        '
        'winControlContainer2
        '
        Me.winControlContainer2.Location = New System.Drawing.Point(0, 700)
        Me.winControlContainer2.Name = "winControlContainer2"
        Me.winControlContainer2.Size = New System.Drawing.Size(133, 192)
        Me.winControlContainer2.WinControl = Me.pictureBox2
        '
        'xrLabel3
        '
        Me.xrLabel3.Location = New System.Drawing.Point(150, 750)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.Size = New System.Drawing.Size(250, 108)
        Me.xrLabel3.Text = "Accessories are also on sale now through May 23rd!  Everything guaranteed discoun" & _
            "ted between 10-75% off retail price."
        '
        'ThankYou
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(12, 0, 50, 50)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1})
        Me.Version = "8.1"
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_fname.DataBindings.Add("Text", DataSource, "SO.FNAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.LNAME")
        'xr_salutation.DataBindings.Add("Text", DataSource, "SO.FNAME" & " " & "SO.LNAME" & ",")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.ST")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.ZIP_CD")

    End Sub

End Class