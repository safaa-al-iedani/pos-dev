Imports Microsoft.VisualBasic
Imports System.Xml

'Public Class XMLRequestBuilder

'    Private Request As New XmlDataDocument()
'    Private _requestDS As RequestAllianceDataSet
'    Private Const cSale10RootNode As String = "<sales10 xmlns=""http://axes.alldata.net/web-services"" version=""1.2""></sales10>"
'    Public Function BuildAllianceDataRequest(ByRef requestDS As RequestAllianceDataSet) As XmlDataDocument
'        _requestDS = requestDS
'        Request.LoadXml(cSale10RootNode)
'        LoadClientHeader()
'        LoadStoreNumber()
'        LoadAccountNumber()
'        LoadTransactionAmount()
'        LoadSwipeInd()
'        Return Request
'    End Function

'    Private Sub LoadClientHeader()
'        Dim clientHeader As XmlNode = Request.CreateElement("clientHeader", Request.DocumentElement.NamespaceURI)
'        Dim clientName As XmlNode = Request.CreateElement("clientName", Request.DocumentElement.NamespaceURI)
'        Dim txtClientName As XmlText = Request.CreateTextNode(_requestDS.Request(0).ClientName)
'        clientHeader.AppendChild(clientName)
'        clientHeader.LastChild.AppendChild(txtClientName)
'        Dim timestamp As XmlNode = Request.CreateElement("timestamp", Request.DocumentElement.NamespaceURI)
'        Dim txtTimeStamp As XmlText = Request.CreateTextNode(_requestDS.Request(0).timeStamp)
'        clientHeader.AppendChild(timestamp)
'        clientHeader.LastChild.AppendChild(txtTimeStamp)
'        Request.DocumentElement.AppendChild(clientHeader)
'    End Sub

'    Private Sub LoadStoreNumber()
'        Dim storeNumber As XmlNode = Request.CreateElement("storeNumber", Request.DocumentElement.NamespaceURI)
'        Dim txtStoreNumber As XmlText = Request.CreateTextNode(_requestDS.Request(0).storeNumber)
'        Request.DocumentElement.AppendChild(storeNumber)
'        Request.DocumentElement.LastChild.AppendChild(txtStoreNumber)
'    End Sub

'    Private Sub LoadAccountNumber()
'        Dim accountNumber As XmlNode = Request.CreateElement("accountNumber", Request.DocumentElement.NamespaceURI)
'        Dim txtAccountNumber As XmlText = Request.CreateTextNode(_requestDS.Request(0).accountNumber)
'        Request.DocumentElement.AppendChild(accountNumber)
'        Request.DocumentElement.LastChild.AppendChild(txtAccountNumber)
'    End Sub

'    Private Sub LoadTransactionAmount()
'        Dim transactionAmount As XmlNode = Request.CreateElement("transactionAmount", Request.DocumentElement.NamespaceURI)
'        Dim txtTransactionAmount As XmlText = Request.CreateTextNode(_requestDS.Request(0).transactionAmount)
'        Request.DocumentElement.AppendChild(transactionAmount)
'        Request.DocumentElement.LastChild.AppendChild(txtTransactionAmount)
'    End Sub

'    Private Sub LoadSwipeInd()
'        Dim swipeInd As XmlNode = Request.CreateElement("swipeInd", Request.DocumentElement.NamespaceURI)
'        Dim txtSwipeInd As XmlText = Request.CreateTextNode(_requestDS.Request(0).swipeInd)
'        Request.DocumentElement.AppendChild(swipeInd)
'        Request.DocumentElement.LastChild.AppendChild(txtSwipeInd)
'    End Sub
'End Class

Public Class XMLRequestBuilder

    Private Request As New XmlDataDocument()
    Private _requestDS As RequestAllianceDataSet
    Private Const cSale10URI As String = "http://axes.alldata.net/web-services version=1.2"
    Private Const cSoapHeaderNode As String = "<?xml version=""1.0"" encoding=""utf-8"" ?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""></soap:Envelope>"

    Public Function BuildAllianceDataRequest(ByRef requestDS As RequestAllianceDataSet) As XmlDataDocument
        _requestDS = requestDS
        Request.LoadXml(cSoapHeaderNode)

        Dim soapBody As XmlNode = Request.CreateElement("soap:Body", Request.DocumentElement.NamespaceURI)

        Dim salesHeader As XmlNode = Request.CreateElement("sales10", cSale10URI)
        LoadClientHeader(salesHeader)
        LoadStoreNumber(salesHeader)
        LoadAccountNumber(salesHeader)
        LoadTransactionAmount(salesHeader)
        LoadSwipeInd(salesHeader)
        soapBody.AppendChild(salesHeader)
        Request.DocumentElement.AppendChild(soapBody)
        Return Request

    End Function

    Private Sub LoadClientHeader(ByRef salesHeader As XmlNode)

        Dim clientHeader As XmlNode = Request.CreateElement("clientHeader", salesHeader.NamespaceURI)
        Dim clientName As XmlNode = Request.CreateElement("clientName", salesHeader.NamespaceURI)
        Dim txtClientName As XmlText = Request.CreateTextNode(_requestDS.Request(0).ClientName)
        clientHeader.AppendChild(clientName)
        clientHeader.LastChild.AppendChild(txtClientName)

        Dim timestamp As XmlNode = Request.CreateElement("timestamp", salesHeader.NamespaceURI)
        Dim txtTimeStamp As XmlText = Request.CreateTextNode(_requestDS.Request(0).timeStamp)
        clientHeader.AppendChild(timestamp)
        clientHeader.LastChild.AppendChild(txtTimeStamp)
        salesHeader.AppendChild(clientHeader)

    End Sub

    Private Sub LoadStoreNumber(ByRef salesHeader As XmlNode)

        Dim storeNumber As XmlNode = Request.CreateElement("storeNumber", salesHeader.NamespaceURI)
        Dim txtStoreNumber As XmlText = Request.CreateTextNode(_requestDS.Request(0).storeNumber)
        salesHeader.AppendChild(storeNumber)
        salesHeader.LastChild.AppendChild(txtStoreNumber)

    End Sub

    Private Sub LoadAccountNumber(ByRef salesHeader As XmlNode)

        Dim accountNumber As XmlNode = Request.CreateElement("accountNumber", salesHeader.NamespaceURI)
        Dim txtAccountNumber As XmlText = Request.CreateTextNode(_requestDS.Request(0).accountNumber)
        salesHeader.AppendChild(accountNumber)
        salesHeader.LastChild.AppendChild(txtAccountNumber)

    End Sub

    Private Sub LoadTransactionAmount(ByRef salesHeader As XmlNode)

        Dim transactionAmount As XmlNode = Request.CreateElement("transactionAmount", salesHeader.NamespaceURI)
        Dim txtTransactionAmount As XmlText = Request.CreateTextNode(_requestDS.Request(0).transactionAmount)
        salesHeader.AppendChild(transactionAmount)
        salesHeader.LastChild.AppendChild(txtTransactionAmount)

    End Sub

    Private Sub LoadSwipeInd(ByRef salesHeader As XmlNode)

        Dim swipeInd As XmlNode = Request.CreateElement("swipeInd", salesHeader.NamespaceURI)
        Dim txtSwipeInd As XmlText = Request.CreateTextNode(_requestDS.Request(0).swipeInd)
        salesHeader.AppendChild(swipeInd)
        salesHeader.LastChild.AppendChild(txtSwipeInd)

    End Sub
End Class