Public Class Relationship_Prospect
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_addr As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_phone2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_prospect_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_email As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_phone1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_rel_no As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Relationship_Prospect.resx"
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xr_addr = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_phone2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_prospect_total = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_email = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_phone1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_rel_no = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable1})
        Me.Detail.HeightF = 20.83333!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 35.41667!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 52.08333!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2, Me.xrPageInfo1, Me.xrLabel1})
        Me.PageHeader.HeightF = 60.41667!
        Me.PageHeader.Name = "PageHeader"
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(173.9583!, 28.20834!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Prospect Report"
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.xrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(800.0!, 0.0!)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 16.75!)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.StylePriority.UseTextAlignment = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrTable1
        '
        Me.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.xrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow1})
        Me.xrTable1.SizeF = New System.Drawing.SizeF(900.0!, 20.83333!)
        Me.xrTable1.StylePriority.UseBorders = False
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_rel_no, Me.xr_full_name, Me.xr_addr, Me.xr_phone1, Me.xr_phone2, Me.xr_email, Me.xr_prospect_total})
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 1
        '
        'xr_addr
        '
        Me.xr_addr.Name = "xr_addr"
        Me.xr_addr.Weight = 0.65625
        '
        'xr_phone2
        '
        Me.xr_phone2.Name = "xr_phone2"
        Me.xr_phone2.Weight = 0.27083333333333331
        '
        'xr_prospect_total
        '
        Me.xr_prospect_total.Name = "xr_prospect_total"
        Me.xr_prospect_total.Weight = 0.5
        '
        'xr_email
        '
        Me.xr_email.Name = "xr_email"
        Me.xr_email.Weight = 0.5
        '
        'xr_phone1
        '
        Me.xr_phone1.Name = "xr_phone1"
        Me.xr_phone1.Weight = 0.30208333333333337
        '
        'xr_rel_no
        '
        Me.xr_rel_no.Name = "xr_rel_no"
        Me.xr_rel_no.Weight = 0.375
        '
        'xr_full_name
        '
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Weight = 0.39583333333333331
        '
        'xrTable2
        '
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 40.625!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow2})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(900.0!, 19.79167!)
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell1, Me.xrTableCell2, Me.xrTableCell3, Me.xrTableCell4, Me.xrTableCell5, Me.xrTableCell6, Me.xrTableCell7})
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 1
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Text = "Relationship"
        Me.xrTableCell1.Weight = 0.375
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Text = "Customer"
        Me.xrTableCell2.Weight = 0.39583333333333331
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Text = "Address"
        Me.xrTableCell3.Weight = 0.65625
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Text = "Primary"
        Me.xrTableCell4.Weight = 0.30208333333333337
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Text = "Secondary"
        Me.xrTableCell5.Weight = 0.27083333333333331
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Text = "Email"
        Me.xrTableCell6.Weight = 0.5
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Text = "Prospect Total"
        Me.xrTableCell7.Weight = 0.5
        '
        'Relationship_Prospect
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(100, 100, 35, 52)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "11.1"
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_rel_no.DataBindings.Add("Text", DataSource, "AR_TRN.REL_NO")
        xr_addr.DataBindings.Add("Text", DataSource, "AR_TRN.ADDR")
        xr_full_name.DataBindings.Add("Text", DataSource, "AR_TRN.FULL_NAME")
        xr_phone1.DataBindings.Add("Text", DataSource, "AR_TRN.HPHONE")
        xr_phone2.DataBindings.Add("Text", DataSource, "AR_TRN.BPHONE")
        xr_email.DataBindings.Add("Text", DataSource, "AR_TRN.EMAIL_ADDR")
        xr_prospect_total.DataBindings.Add("Text", DataSource, "AR_TRN.PROSPECT_TOTAL")

    End Sub

End Class