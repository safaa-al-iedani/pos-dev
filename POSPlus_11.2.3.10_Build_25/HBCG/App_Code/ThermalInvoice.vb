Public Class ThermalInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Dim comments_processed As Boolean
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrLine6 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payment_details As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine8 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pkg_src_sku As DevExpress.XtraReports.UI.XRLabel
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_financed As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_triggers As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_desc As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "ThermalInvoice.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.ThermalInvoice.ResourceManager
        Dim xrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xr_store_addr = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine8 = New DevExpress.XtraReports.UI.XRLine()
        Me.xr_payment_details = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine6 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_triggers = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_financed = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.xrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.xr_pkg_src_sku = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pkg_src_sku, Me.xr_SKU, Me.xr_disc_desc, Me.xr_disc_amt, Me.xr_void_flag, Me.xr_ext, Me.xr_ret, Me.xr_desc, Me.xr_loc, Me.xr_store, Me.xr_vsn, Me.xr_qty})
        Me.Detail.HeightF = 97.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_SKU
        '
        Me.xr_SKU.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_SKU.LocationFloat = New DevExpress.Utils.PointFloat(40.99999!, 0.0!)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xr_SKU.StylePriority.UseFont = False
        Me.xr_SKU.Text = "SKU"
        '
        'xr_disc_desc
        '
        Me.xr_disc_desc.CanShrink = True
        Me.xr_disc_desc.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_disc_desc.ForeColor = System.Drawing.Color.Red
        Me.xr_disc_desc.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 51.0!)
        Me.xr_disc_desc.Name = "xr_disc_desc"
        Me.xr_disc_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_desc.SizeF = New System.Drawing.SizeF(258.0!, 17.0!)
        Me.xr_disc_desc.StylePriority.UseFont = False
        Me.xr_disc_desc.StylePriority.UseForeColor = False
        Me.xr_disc_desc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 75.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 75.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'xr_ext
        '
        Me.xr_ext.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_ext.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.Text = "EXTENDED"
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ret
        '
        Me.xr_ret.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_ret.LocationFloat = New DevExpress.Utils.PointFloat(124.0!, 0.0!)
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.SizeF = New System.Drawing.SizeF(68.0!, 17.0!)
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.Text = "RETAIL"
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_desc
        '
        Me.xr_desc.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_desc.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 34.0!)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.SizeF = New System.Drawing.SizeF(258.0!, 17.0!)
        Me.xr_desc.StylePriority.UseFont = False
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_loc
        '
        Me.xr_loc.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_loc.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 68.0!)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
        Me.xr_loc.StylePriority.UseFont = False
        Me.xr_loc.Text = "LOC"
        '
        'xr_store
        '
        Me.xr_store.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_store.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 68.0!)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_store.StylePriority.UseFont = False
        Me.xr_store.Text = "ST"
        '
        'xr_vsn
        '
        Me.xr_vsn.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_vsn.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.SizeF = New System.Drawing.SizeF(258.0!, 17.0!)
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.Text = "VSN"
        '
        'xr_qty
        '
        Me.xr_qty.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_qty.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.Text = "QTY"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_store_addr, Me.xr_store_name, Me.xrLine2, Me.xrLabel21, Me.xr_store_phone, Me.xrPictureBox1})
        Me.PageHeader.HeightF = 119.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_addr
        '
        Me.xr_store_addr.Font = New System.Drawing.Font("Calibri", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_addr.ForeColor = System.Drawing.Color.Black
        Me.xr_store_addr.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 68.0!)
        Me.xr_store_addr.Name = "xr_store_addr"
        Me.xr_store_addr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
        Me.xr_store_addr.StylePriority.UseFont = False
        Me.xr_store_addr.StylePriority.UseForeColor = False
        Me.xr_store_addr.StylePriority.UseTextAlignment = False
        Me.xr_store_addr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_store_name
        '
        Me.xr_store_name.Font = New System.Drawing.Font("Calibri", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.Black
        Me.xr_store_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 51.0!)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.StylePriority.UseTextAlignment = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLine2
        '
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LineWidth = 2
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 108.0!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(258.0!, 8.0!)
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'xrLabel21
        '
        Me.xrLabel21.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.xrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(66.0!, 34.0!)
        Me.xrLabel21.Name = "xrLabel21"
        Me.xrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel21.SizeF = New System.Drawing.SizeF(133.0!, 17.0!)
        Me.xrLabel21.StylePriority.UseFont = False
        Me.xrLabel21.StylePriority.UseTextAlignment = False
        Me.xrLabel21.Text = "www.olindes.com"
        Me.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Font = New System.Drawing.Font("Calibri", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_phone.ForeColor = System.Drawing.Color.Black
        Me.xr_store_phone.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 85.0!)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(250.0!, 33.0!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(240.0!, 159.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.StylePriority.UseFont = False
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 108.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "CUSTOMER:"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine3
        '
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 233.0!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(258.0!, 8.0!)
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 160.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(74.0!, 16.0!)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseForeColor = False
        Me.xrLabel2.Text = "HOME TEL:"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 176.0!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel3.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 176.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.StylePriority.UseForeColor = False
        Me.xrLabel3.Text = "WORK TEL:"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 159.0!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 142.0!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(40.00003!, 16.99998!)
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(195.0!, 142.0!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 142.0!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(109.0!, 17.0!)
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(224.0!, 174.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(207.0!, 174.0!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 125.0!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(258.0!, 17.0!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel16, Me.xrLabel15, Me.xrLine8, Me.xr_payment_details, Me.xrLine5, Me.xrLine6, Me.xrLabel27, Me.xr_triggers, Me.xrLine1, Me.xrLabel9, Me.xrPanel1, Me.xrLine7, Me.xrLabel20, Me.xrLabel22, Me.xrLabel23, Me.xrLabel24, Me.xrLabel8, Me.xrLabel25, Me.xr_balance, Me.xr_financed, Me.xr_payments, Me.xr_grand_total, Me.xrLabel19, Me.xr_subtotal, Me.xr_del, Me.xr_tax})
        Me.ReportFooter.HeightF = 540.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'xrLabel16
        '
        Me.xrLabel16.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel16.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(9.999987!, 458.0417!)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.SizeF = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseForeColor = False
        Me.xrLabel16.Text = "X"
        '
        'xrLabel15
        '
        Me.xrLabel15.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!, System.Drawing.FontStyle.Bold)
        Me.xrLabel15.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(8.499988!, 363.5417!)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.SizeF = New System.Drawing.SizeF(258.0!, 68.41663!)
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.StylePriority.UseForeColor = False
        Me.xrLabel15.StylePriority.UseTextAlignment = False
        Me.xrLabel15.Text = "CUSTOMER ACKNOWLEDGES RECEIPT OF THE OLINDES FURNITURE INFORMATION BROCHURE AND A" & _
            "GREES TO THE TERMS AND CONDITIONS STATED IN THE BROCHURE."
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLine8
        '
        Me.xrLine8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine8.LocationFloat = New DevExpress.Utils.PointFloat(17.99998!, 467.0417!)
        Me.xrLine8.Name = "xrLine8"
        Me.xrLine8.SizeF = New System.Drawing.SizeF(247.0!, 8.0!)
        Me.xrLine8.StylePriority.UseForeColor = False
        '
        'xr_payment_details
        '
        Me.xr_payment_details.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payment_details.ForeColor = System.Drawing.Color.DimGray
        Me.xr_payment_details.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 200.0!)
        Me.xr_payment_details.Multiline = True
        Me.xr_payment_details.Name = "xr_payment_details"
        Me.xr_payment_details.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payment_details.SizeF = New System.Drawing.SizeF(258.0!, 25.0!)
        Me.xr_payment_details.StylePriority.UseFont = False
        Me.xr_payment_details.StylePriority.UseForeColor = False
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 192.0!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(258.0!, 2.0!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'xrLine6
        '
        Me.xrLine6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine6.LineWidth = 2
        Me.xrLine6.LocationFloat = New DevExpress.Utils.PointFloat(8.999991!, 497.7499!)
        Me.xrLine6.Name = "xrLine6"
        Me.xrLine6.SizeF = New System.Drawing.SizeF(258.0!, 2.0!)
        Me.xrLine6.StylePriority.UseForeColor = False
        '
        'xrLabel27
        '
        Me.xrLabel27.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!, System.Drawing.FontStyle.Bold)
        Me.xrLabel27.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 258.0!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(258.0001!, 47.58331!)
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.Text = "I AGREE TO PAY TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT (MERCHANT AGREEMEN" & _
            "T IF CREDIT VOUCHER)."
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_triggers
        '
        Me.xr_triggers.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_triggers.ForeColor = System.Drawing.Color.Gray
        Me.xr_triggers.LocationFloat = New DevExpress.Utils.PointFloat(8.999991!, 513.0001!)
        Me.xr_triggers.Multiline = True
        Me.xr_triggers.Name = "xr_triggers"
        Me.xr_triggers.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_triggers.SizeF = New System.Drawing.SizeF(258.0!, 17.0!)
        Me.xr_triggers.StylePriority.UseFont = False
        Me.xr_triggers.StylePriority.UseForeColor = False
        '
        'xrLine1
        '
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 343.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(249.0!, 8.0!)
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLabel9
        '
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 334.0!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "X"
        '
        'xrPanel1
        '
        Me.xrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrLabel10})
        Me.xrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 142.0!)
        Me.xrPanel1.Name = "xrPanel1"
        Me.xrPanel1.SizeF = New System.Drawing.SizeF(258.0!, 42.0!)
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 133.0!)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.SizeF = New System.Drawing.SizeF(258.0!, 2.0!)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xrLabel20
        '
        Me.xrLabel20.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel20.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 17.0!)
        Me.xrLabel20.Name = "xrLabel20"
        Me.xrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel20.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel20.StylePriority.UseFont = False
        Me.xrLabel20.StylePriority.UseForeColor = False
        Me.xrLabel20.Text = "DELIVERY CHARGE:"
        Me.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel22
        '
        Me.xrLabel22.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel22.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 34.0!)
        Me.xrLabel22.Name = "xrLabel22"
        Me.xrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel22.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel22.StylePriority.UseFont = False
        Me.xrLabel22.StylePriority.UseForeColor = False
        Me.xrLabel22.Text = "SALES TAX:"
        Me.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel23
        '
        Me.xrLabel23.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel23.ForeColor = System.Drawing.Color.Black
        Me.xrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 52.0!)
        Me.xrLabel23.Name = "xrLabel23"
        Me.xrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel23.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel23.StylePriority.UseFont = False
        Me.xrLabel23.StylePriority.UseForeColor = False
        Me.xrLabel23.Text = "*TOTAL SALE*:"
        Me.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel24
        '
        Me.xrLabel24.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel24.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 69.0!)
        Me.xrLabel24.Name = "xrLabel24"
        Me.xrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel24.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel24.StylePriority.UseFont = False
        Me.xrLabel24.StylePriority.UseForeColor = False
        Me.xrLabel24.Text = "PAYMENTS:"
        Me.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(58.0!, 86.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.Text = "FINANCED:"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel25
        '
        Me.xrLabel25.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel25.ForeColor = System.Drawing.Color.Black
        Me.xrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 111.0!)
        Me.xrLabel25.Name = "xrLabel25"
        Me.xrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel25.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel25.StylePriority.UseFont = False
        Me.xrLabel25.StylePriority.UseForeColor = False
        Me.xrLabel25.Text = "**BALANCE** :"
        Me.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_balance
        '
        Me.xr_balance.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_balance.ForeColor = System.Drawing.Color.Black
        Me.xr_balance.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 111.0!)
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_balance.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseForeColor = False
        Me.xr_balance.Text = "xr_balance"
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_financed
        '
        Me.xr_financed.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.xr_financed.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_financed.ForeColor = System.Drawing.Color.DimGray
        Me.xr_financed.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 86.0!)
        Me.xr_financed.Name = "xr_financed"
        Me.xr_financed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_financed.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_financed.StylePriority.UseBorders = False
        Me.xr_financed.StylePriority.UseFont = False
        Me.xr_financed.StylePriority.UseForeColor = False
        Me.xr_financed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_payments
        '
        Me.xr_payments.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_payments.ForeColor = System.Drawing.Color.DimGray
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 69.0!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.Text = "xr_payments"
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_grand_total
        '
        Me.xr_grand_total.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_grand_total.ForeColor = System.Drawing.Color.Black
        Me.xr_grand_total.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 52.0!)
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseForeColor = False
        Me.xr_grand_total.Text = "xr_grand_total"
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel19
        '
        Me.xrLabel19.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel19.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 0.0!)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel19.StylePriority.UseFont = False
        Me.xrLabel19.StylePriority.UseForeColor = False
        Me.xrLabel19.Text = "SUB-TOTAL:"
        Me.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_subtotal
        '
        Me.xr_subtotal.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_subtotal.ForeColor = System.Drawing.Color.DimGray
        Me.xr_subtotal.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseForeColor = False
        xrSummary1.FormatString = "{0:#.00}"
        xrSummary1.IgnoreNullValues = True
        Me.xr_subtotal.Summary = xrSummary1
        Me.xr_subtotal.Text = "xr_subtotal"
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_del
        '
        Me.xr_del.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_del.ForeColor = System.Drawing.Color.DimGray
        Me.xr_del.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 17.0!)
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseForeColor = False
        Me.xr_del.Text = "xr_del"
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_tax
        '
        Me.xr_tax.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_tax.ForeColor = System.Drawing.Color.DimGray
        Me.xr_tax.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 34.0!)
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseForeColor = False
        Me.xr_tax.Text = "xr_tax"
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportHeader
        '
        Me.ReportHeader.HeightF = 188.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0.0R
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0.0R
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0.0R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0.0R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0.0R
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0.0R
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0.0R
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xrLabel26
        '
        Me.xrLabel26.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel26.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 125.0!)
        Me.xrLabel26.Name = "xrLabel26"
        Me.xrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel26.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel26.StylePriority.UseFont = False
        Me.xrLabel26.StylePriority.UseForeColor = False
        Me.xrLabel26.Text = "ADDRESS:"
        Me.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel28
        '
        Me.xrLabel28.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel28.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 142.0!)
        Me.xrLabel28.Name = "xrLabel28"
        Me.xrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel28.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xrLabel28.StylePriority.UseFont = False
        Me.xrLabel28.StylePriority.UseForeColor = False
        Me.xrLabel28.Text = "CITY/ST/ZIP:"
        Me.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_slsp2, Me.xrLine4, Me.xrLabel13, Me.xrLabel14, Me.xrLabel11, Me.xrLabel12, Me.xrLabel4, Me.xr_ord_tp, Me.xr_slsp, Me.xrLabel7, Me.xr_pd, Me.xr_pu_del_dt, Me.xrLabel6, Me.xr_cust_cd, Me.xrLabel5, Me.xr_del_doc_num, Me.xr_so_wr_dt, Me.xr_ord_tp_hidden, Me.xr_full_name, Me.xrLabel28, Me.xrLabel26, Me.xr_disc_cd, Me.xrLabel1, Me.xrLine3, Me.xrLabel2, Me.xr_b_phone, Me.xrLabel3, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_lname, Me.xr_addr1, Me.xr_fname, Me.xr_slsp2_hidden})
        Me.GroupHeader1.HeightF = 247.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_slsp2
        '
        Me.xr_slsp2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_slsp2.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 92.00001!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(117.0!, 15.99999!)
        Me.xr_slsp2.StylePriority.UseBorders = False
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseForeColor = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLine4
        '
        Me.xrLine4.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 208.0!)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.SizeF = New System.Drawing.SizeF(259.0!, 8.0!)
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(205.0!, 216.0!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(60.00002!, 16.99998!)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.Text = "EXTENDED"
        Me.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel14
        '
        Me.xrLabel14.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 216.0!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.Text = "SKU"
        '
        'xrLabel11
        '
        Me.xrLabel11.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 216.0!)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.Text = "QTY"
        '
        'xrLabel12
        '
        Me.xrLabel12.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 216.0!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(68.0!, 17.0!)
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.Text = "RETAIL"
        Me.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel4
        '
        Me.xrLabel4.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel4.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 50.0!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(133.3333!, 16.0!)
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseForeColor = False
        Me.xrLabel4.Text = "PICKUP/DELIVERY DATE:"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 0.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES ORDER #:"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_slsp
        '
        Me.xr_slsp.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 75.0!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 75.0!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "SALES ASSOCIATE:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pd
        '
        Me.xr_pd.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_pd.ForeColor = System.Drawing.Color.DimGray
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 50.99996!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(16.00001!, 17.0!)
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(168.75!, 50.99996!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(81.25002!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 34.0!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseForeColor = False
        Me.xrLabel6.Text = "SALES DATE:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 17.0!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 17.0!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseForeColor = False
        Me.xrLabel5.Text = "ACCOUNT #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 34.0!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(215.0!, 176.0!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 108.0!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Franklin Gothic Demi Cond", 10.0!)
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(248.0!, 185.0!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.HeightF = 34.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 10.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 10.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'xr_pkg_src_sku
        '
        Me.xr_pkg_src_sku.LocationFloat = New DevExpress.Utils.PointFloat(57.99999!, 70.00002!)
        Me.xr_pkg_src_sku.Name = "xr_pkg_src_sku"
        Me.xr_pkg_src_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_pkg_src_sku.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_pkg_src_sku.Visible = False
        '
        'ThermalInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(10, 10, 10, 10)
        Me.PageHeight = 2500
        Me.PageWidth = 287
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_financed.DataBindings.Add("Text", DataSource, "SO.ORIG_FI_AMT", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_pkg_src_sku.DataBindings.Add("Text", DataSource, "SO_LN.PKG_SOURCE")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_SKU.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_store.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_desc.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")
        xr_store_addr.DataBindings.Add("Text", DataSource, "STORE.STORE_ADDRESS")
        xr_store_phone.DataBindings.Add("Text", DataSource, "STORE.STORE_PHONE")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "P"
            Case "D"
                sender.text = "D"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2_hidden.BeforePrint

    End Sub


    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint
        sender.text = FormatNumber(xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString, 2)
        subtotal = subtotal + (xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                End If
            End If
        Catch
            Throw
        End Try

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint
        Dim pmts As Double = 0

        If IsNumeric(xr_payments.Text) Then
            pmts = CDbl(xr_payments.Text)
        End If
        If IsNumeric(xr_financed.Text) Then
            pmts = pmts + CDbl(xr_financed.Text)
        End If
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        sender.text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)

    End Sub

    Private Sub xr_financed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_financed.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = ""
        Else
            sender.Text = FormatCurrency(sender.text, 2)
        End If
    End Sub

    Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_disc_desc.BeforePrint

        'only print the discount details for a line that is not a component and has a discount attached
        If Not String.IsNullOrEmpty(xr_disc_cd.Text) AndAlso IsNumeric(xr_disc_amt.Text) AndAlso String.IsNullOrEmpty(xr_pkg_src_sku.Text) Then
            If CDbl(xr_disc_amt.Text) > 0 Then
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
                            sender.visible = True
                            sender.text = MyDataReader2.Item("DES").ToString & " (" & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text), 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
                        Else
                            sender.visible = False
                        End If
                    Else
                        sender.visible = False
                    End If
                Catch
                    Throw
                End Try
            Else
                sender.visible = False
            End If
        Else
            sender.visible = False
        End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='S' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
            Catch
                Throw
            End Try

            sender.text = sender.text & vbCrLf & vbCrLf

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='D' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
            Catch
                Throw
            End Try

            comments_processed = True
        End If

    End Sub

    Private Sub xr_triggers_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_triggers.BeforePrint

        If triggers_processed <> True Then
            Dim conn2 As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            conn.Open()

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','P_D','" & Microsoft.VisualBasic.Strings.Left(xr_pd.Text, 1) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','STORE_CD','" & Microsoft.VisualBasic.Strings.Mid(xr_del_doc_num.Text, 6, 2) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "SELECT c.ITM_TP_CD, b.MJR_CD, c.MNR_CD, a.ITM_CD, c.DROP_DT FROM SO_LN a, INV_MNR b, ITM c "
            sql = sql & "WHERE a.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND c.MNR_CD=b.MNR_CD "
            sql = sql & "AND a.ITM_CD=c.ITM_CD"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_TP_CD','" & MyDataReader2.Item("ITM_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MAJOR_CD','" & MyDataReader2.Item("MJR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MINOR_CD','" & MyDataReader2.Item("MNR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_CD','" & MyDataReader2.Item("ITM_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DROP_DT").ToString) Then
                        sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                        sql = sql & "VALUES('" & xr_del_doc_num.Text & "','DROP_DT','" & MyDataReader2.Item("DROP_DT").ToString & "')"

                        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql2.ExecuteNonQuery()
                    End If
                Loop
            Catch
                Throw
            End Try

            sql = "select trigger_response, TRIGGER_ID "
            sql = sql & "from invoice_triggers, invoice_triggers_temp "
            sql = sql & "where invoice_triggers.trigger_field = invoice_triggers_temp.trigger_field "
            sql = sql & "and invoice_triggers.trigger_value=invoice_triggers_temp.trigger_value "
            sql = sql & "and del_doc_num='" & xr_del_doc_num.Text & "' "
            sql = sql & "GROUP BY TRIGGER_ID, trigger_response"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_triggers.Text = xr_triggers.Text & MyDataReader2.Item("TRIGGER_RESPONSE").ToString & Constants.vbCrLf
                Loop
            Catch
                Throw
            End Try

            If Not String.IsNullOrEmpty(xr_triggers.Text) Then
                xr_triggers.Text = xr_triggers.Text & Constants.vbCrLf
            End If

            sql = "DELETE FROM INVOICE_TRIGGERS_TEMP WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            triggers_processed = True
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES ORDER #:"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO #:"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT #:"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT #:"
        End Select

    End Sub

    Private Sub xr_store_addr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr.BeforePrint

        xr_store_addr.Text = StrConv(xr_store_addr.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = StrConv(xr_store_name.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_payment_details_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payment_details.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String
        Dim merchant_id As String = ""
        Dim html As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select decode(a.TRN_TP_CD,'R',-AMT,AMT) AS PMT_AMT, b.DES, a.DES as MID, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM from ar_trn a, mop b where a.ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP') AND b.MOP_CD=a.MOP_CD"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If Not String.IsNullOrEmpty(MyDataReader2.Item("REF_NUM").ToString) Then
                    html = html & "Payment: " & MyDataReader2.Item("REF_NUM").ToString & Constants.vbCrLf
                Else
                    html = html & "Payment: " & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                End If
                html = html & "Amount: " & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
                If Len(MyDataReader2.Item("BNK_CRD_NUM").ToString) = 19 Then
                    html = html & "Card Number: " & Microsoft.VisualBasic.Strings.Right(MyDataReader2.Item("BNK_CRD_NUM").ToString, 16) & Constants.vbCrLf
                ElseIf MyDataReader2.Item("CHK_NUM").ToString & "" <> "" Then
                    html = html & "Check #: " & MyDataReader2.Item("CHK_NUM").ToString & Constants.vbCrLf
                ElseIf MyDataReader2.Item("BNK_CRD_NUM").ToString & "" <> "" Then
                    html = html & "Card Number: " & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                End If
                If Not String.IsNullOrEmpty(MyDataReader2.Item("EXP_DT").ToString) Then
                    html = html & "Exp Date: " & "xx/xx" & Constants.vbCrLf
                End If
                If MyDataReader2.Item("APP_CD").ToString & "" <> "" Then
                    html = html & "Approval Code: " & MyDataReader2.Item("APP_CD").ToString & Constants.vbCrLf
                End If
                If InStr(MyDataReader2.Item("MID").ToString, "MID:") > 0 Then
                    merchant_id = Replace(MyDataReader2.Item("MID").ToString, "MID:", "")
                    If IsNumeric(merchant_id) Then merchant_id = CDbl(merchant_id)
                    html = html & "Merchant ID: " & merchant_id & Constants.vbCrLf
                End If
                If Not String.IsNullOrEmpty(MyDataReader2.Item("HOST_REF_NUM").ToString) Then
                    html = html & "Reference #: " & MyDataReader2.Item("HOST_REF_NUM").ToString & Constants.vbCrLf
                End If
                html = html & Constants.vbCrLf
                'xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
            Loop
        Catch
            Throw
        End Try

        sender.text = html

    End Sub
End Class