Public Class LazboyWinnipegInvoiceFull
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim subtotal As Double
    Dim processed_payments As Boolean = False
    Dim mop_processed As Boolean = False
    Dim tax_processed As Boolean = False
    Dim pst As Double = 0
    Dim gst As Double = 0
    Dim pst_cd As String = "PST"
    Dim gst_cd As String = ""
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_card_no As DevExpress.XtraReports.UI.XRLabel
    Dim comments_processed As Boolean
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_exp_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_appr_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_merchant As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ref As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Private WithEvents xr_ord_srt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp2_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable3 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable4 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sale_last_four As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell28 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_po_num As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable5 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable7 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_sku As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable8 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax_pst As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell51 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow18 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell53 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vend As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_des As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bill_corp_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payment_holder As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xrTable6 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell36 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell41 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell37 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell39 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_disc_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell31 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_pmt_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow20 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell40 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax_gst As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_tax_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc_cd As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_store_cd As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow21 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell45 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_finance As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow22 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell50 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_rem As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_GST As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_PST As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Private WithEvents xr_setup As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_warranty As DevExpress.XtraReports.UI.XRLabel
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_triggers As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "LazboyWinnipegInvoiceFull.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.LazboyWinnipegInvoiceFull.ResourceManager
        Me.xr_payment_holder = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xr_disc_desc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrTable7 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xr_qty = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sku = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vend = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_store_cd = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_loc_cd = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ret = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_ext = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_des = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_slsp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_addr = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.xr_setup = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.xrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xr_tax_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_tax = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.xr_pmt_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable8 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_del = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax_pst = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow20 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell40 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_tax_gst = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell51 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow18 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell53 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow21 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell45 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_finance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow22 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell50 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_rem = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ref = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_merchant = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_appr_cd = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_exp_dt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.xr_triggers = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_card_no = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_mop = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xr_PST = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable6 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell36 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell41 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell37 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell39 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable5 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_sale_last_four = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_po_num = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable3 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xr_bill_corp_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_b_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_h_phone = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_zip = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_state = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_city = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_addr1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_bill_full_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_slsp2_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_srt = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel()
        Me.xr_GST = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.xr_warranty = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xr_payment_holder
        '
        Me.xr_payment_holder.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_payment_holder.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_payments})
        Me.xr_payment_holder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payment_holder.Name = "xr_payment_holder"
        Me.xr_payment_holder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_payment_holder.StylePriority.UseBorderColor = False
        Me.xr_payment_holder.StylePriority.UseFont = False
        Me.xr_payment_holder.Weight = 1.0899999999999999R
        '
        'xr_payments
        '
        Me.xr_payments.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_payments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.Black
        Me.xr_payments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.SizeF = New System.Drawing.SizeF(126.0!, 21.0!)
        Me.xr_payments.StylePriority.UseBorders = False
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.StylePriority.UseTextAlignment = False
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_disc_desc, Me.xrLine3, Me.xrTable7, Me.xr_disc_amt, Me.xr_void_flag})
        Me.Detail.HeightF = 75.00001!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_disc_desc
        '
        Me.xr_disc_desc.CanShrink = True
        Me.xr_disc_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_desc.ForeColor = System.Drawing.Color.Red
        Me.xr_disc_desc.LocationFloat = New DevExpress.Utils.PointFloat(159.0!, 50.0!)
        Me.xr_disc_desc.Multiline = True
        Me.xr_disc_desc.Name = "xr_disc_desc"
        Me.xr_disc_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_desc.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_disc_desc.SizeF = New System.Drawing.SizeF(641.0!, 17.0!)
        Me.xr_disc_desc.StylePriority.UseFont = False
        Me.xr_disc_desc.StylePriority.UseForeColor = False
        Me.xr_disc_desc.StylePriority.UseTextAlignment = False
        Me.xr_disc_desc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrLine3
        '
        Me.xrLine3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.00001!)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.SizeF = New System.Drawing.SizeF(800.0!, 8.0!)
        '
        'xrTable7
        '
        Me.xrTable7.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTable7.LocationFloat = New DevExpress.Utils.PointFloat(9.99999!, 0.0!)
        Me.xrTable7.Name = "xrTable7"
        Me.xrTable7.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow12, Me.xrTableRow19})
        Me.xrTable7.SizeF = New System.Drawing.SizeF(790.0!, 50.0!)
        Me.xrTable7.StylePriority.UseBackColor = False
        Me.xrTable7.StylePriority.UseTextAlignment = False
        Me.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow12
        '
        Me.xrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xr_qty, Me.xr_sku, Me.xr_vend, Me.xr_vsn, Me.xr_store_cd, Me.xr_loc_cd, Me.xr_ret, Me.xr_ext})
        Me.xrTableRow12.Name = "xrTableRow12"
        Me.xrTableRow12.Weight = 1.0R
        '
        'xr_qty
        '
        Me.xr_qty.BackColor = System.Drawing.Color.Transparent
        Me.xr_qty.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_qty.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_qty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.StylePriority.UseBackColor = False
        Me.xr_qty.StylePriority.UseBorderColor = False
        Me.xr_qty.StylePriority.UseBorders = False
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.StylePriority.UseTextAlignment = False
        Me.xr_qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_qty.Weight = 0.15384619507608535R
        '
        'xr_sku
        '
        Me.xr_sku.BackColor = System.Drawing.Color.Transparent
        Me.xr_sku.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_sku.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sku.Name = "xr_sku"
        Me.xr_sku.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sku.StylePriority.UseBackColor = False
        Me.xr_sku.StylePriority.UseBorderColor = False
        Me.xr_sku.StylePriority.UseBorders = False
        Me.xr_sku.StylePriority.UseFont = False
        Me.xr_sku.StylePriority.UseTextAlignment = False
        Me.xr_sku.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sku.Weight = 0.30769246451462384R
        '
        'xr_vend
        '
        Me.xr_vend.BackColor = System.Drawing.Color.Transparent
        Me.xr_vend.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vend.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vend.Name = "xr_vend"
        Me.xr_vend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_vend.StylePriority.UseBackColor = False
        Me.xr_vend.StylePriority.UseBorderColor = False
        Me.xr_vend.StylePriority.UseBorders = False
        Me.xr_vend.StylePriority.UseFont = False
        Me.xr_vend.StylePriority.UseTextAlignment = False
        Me.xr_vend.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vend.Weight = 0.22596134040929094R
        '
        'xr_vsn
        '
        Me.xr_vsn.BackColor = System.Drawing.Color.Transparent
        Me.xr_vsn.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_vsn.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_vsn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.StylePriority.UseBackColor = False
        Me.xr_vsn.StylePriority.UseBorderColor = False
        Me.xr_vsn.StylePriority.UseBorders = False
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.StylePriority.UseTextAlignment = False
        Me.xr_vsn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_vsn.Weight = 1.2985758544268138R
        '
        'xr_store_cd
        '
        Me.xr_store_cd.BackColor = System.Drawing.Color.Transparent
        Me.xr_store_cd.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_store_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_cd.Name = "xr_store_cd"
        Me.xr_store_cd.StylePriority.UseBackColor = False
        Me.xr_store_cd.StylePriority.UseBorderColor = False
        Me.xr_store_cd.StylePriority.UseFont = False
        Me.xr_store_cd.Weight = 0.11598108806071517R
        '
        'xr_loc_cd
        '
        Me.xr_loc_cd.BackColor = System.Drawing.Color.Transparent
        Me.xr_loc_cd.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_loc_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc_cd.Name = "xr_loc_cd"
        Me.xr_loc_cd.StylePriority.UseBackColor = False
        Me.xr_loc_cd.StylePriority.UseBorderColor = False
        Me.xr_loc_cd.StylePriority.UseFont = False
        Me.xr_loc_cd.Weight = 0.2056350882585R
        '
        'xr_ret
        '
        Me.xr_ret.BackColor = System.Drawing.Color.Transparent
        Me.xr_ret.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ret.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ret.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.StylePriority.UseBackColor = False
        Me.xr_ret.StylePriority.UseBorderColor = False
        Me.xr_ret.StylePriority.UseBorders = False
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.StylePriority.UseTextAlignment = False
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ret.Weight = 0.35355796925397093R
        '
        'xr_ext
        '
        Me.xr_ext.BackColor = System.Drawing.Color.Transparent
        Me.xr_ext.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_ext.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_ext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.StylePriority.UseBackColor = False
        Me.xr_ext.StylePriority.UseBorderColor = False
        Me.xr_ext.StylePriority.UseBorders = False
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.StylePriority.UseTextAlignment = False
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_ext.Weight = 0.33875R
        '
        'xrTableRow19
        '
        Me.xrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell27, Me.xrTableCell43, Me.xr_des, Me.xrTableCell48})
        Me.xrTableRow19.Name = "xrTableRow19"
        Me.xrTableRow19.Weight = 1.0R
        '
        'xrTableCell27
        '
        Me.xrTableCell27.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell27.Name = "xrTableCell27"
        Me.xrTableCell27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell27.StylePriority.UseBackColor = False
        Me.xrTableCell27.Weight = 0.125R
        '
        'xrTableCell43
        '
        Me.xrTableCell43.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell43.Name = "xrTableCell43"
        Me.xrTableCell43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell43.StylePriority.UseBackColor = False
        Me.xrTableCell43.Weight = 0.34250000000000008R
        '
        'xr_des
        '
        Me.xr_des.BackColor = System.Drawing.Color.Transparent
        Me.xr_des.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_des.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xr_des.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_des.Name = "xr_des"
        Me.xr_des.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_des.StylePriority.UseBackColor = False
        Me.xr_des.StylePriority.UseBorderColor = False
        Me.xr_des.StylePriority.UseBorders = False
        Me.xr_des.StylePriority.UseFont = False
        Me.xr_des.StylePriority.UseTextAlignment = False
        Me.xr_des.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_des.Weight = 1.35125R
        '
        'xrTableCell48
        '
        Me.xrTableCell48.BackColor = System.Drawing.Color.Transparent
        Me.xrTableCell48.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell48.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell48.Name = "xrTableCell48"
        Me.xrTableCell48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell48.StylePriority.UseBackColor = False
        Me.xrTableCell48.StylePriority.UseBorderColor = False
        Me.xrTableCell48.StylePriority.UseBorders = False
        Me.xrTableCell48.Weight = 1.18125R
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.LocationFloat = New DevExpress.Utils.PointFloat(817.0!, 8.0!)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.LocationFloat = New DevExpress.Utils.PointFloat(800.0!, 8.0!)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'PageHeader
        '
        Me.PageHeader.HeightF = 0.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTable2
        '
        Me.xrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(266.6667!, 32.99999!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow4})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(525.3334!, 91.99999!)
        Me.xrTable2.StylePriority.UseBorders = False
        '
        'xrTableRow4
        '
        Me.xrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell15})
        Me.xrTableRow4.Name = "xrTableRow4"
        Me.xrTableRow4.Weight = 1.0R
        '
        'xrTableCell15
        '
        Me.xrTableCell15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_slsp2, Me.xr_slsp, Me.xrLabel7, Me.xrLabel21, Me.xr_store_phone, Me.xr_store_addr, Me.xr_store_name})
        Me.xrTableCell15.Name = "xrTableCell15"
        Me.xrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell15.Text = "xrTableCell15"
        Me.xrTableCell15.Weight = 7.92R
        '
        'xr_slsp2
        '
        Me.xr_slsp2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp2.CanShrink = True
        Me.xr_slsp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp2.LocationFloat = New DevExpress.Utils.PointFloat(375.3333!, 74.99999!)
        Me.xr_slsp2.Name = "xr_slsp2"
        Me.xr_slsp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2.SizeF = New System.Drawing.SizeF(148.0!, 17.0!)
        Me.xr_slsp2.StylePriority.UseBorders = False
        Me.xr_slsp2.StylePriority.UseFont = False
        Me.xr_slsp2.StylePriority.UseForeColor = False
        Me.xr_slsp2.StylePriority.UseTextAlignment = False
        Me.xr_slsp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_slsp
        '
        Me.xr_slsp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_slsp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(375.3333!, 57.99998!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(148.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseBorders = False
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseForeColor = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(250.3333!, 57.99998!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(124.0!, 17.0!)
        Me.xrLabel7.StylePriority.UseBorders = False
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "Salesperson:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel21
        '
        Me.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.xrLabel21.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 59.0!)
        Me.xrLabel21.Name = "xrLabel21"
        Me.xrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel21.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xrLabel21.StylePriority.UseBorders = False
        Me.xrLabel21.StylePriority.UseFont = False
        Me.xrLabel21.StylePriority.UseForeColor = False
        Me.xrLabel21.StylePriority.UseTextAlignment = False
        Me.xrLabel21.Text = "http://www.lazboy.com/winnipeg"
        Me.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_phone.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_phone.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 42.0!)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.SizeF = New System.Drawing.SizeF(238.0!, 17.0!)
        Me.xr_store_phone.StylePriority.UseBorders = False
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_addr
        '
        Me.xr_store_addr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_addr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_addr.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_addr.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 25.0!)
        Me.xr_store_addr.Name = "xr_store_addr"
        Me.xr_store_addr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr.SizeF = New System.Drawing.SizeF(517.0!, 17.0!)
        Me.xr_store_addr.StylePriority.UseBorders = False
        Me.xr_store_addr.StylePriority.UseFont = False
        Me.xr_store_addr.StylePriority.UseForeColor = False
        Me.xr_store_addr.StylePriority.UseTextAlignment = False
        Me.xr_store_addr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_name
        '
        Me.xr_store_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.DimGray
        Me.xr_store_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.SizeF = New System.Drawing.SizeF(517.0!, 17.0!)
        Me.xr_store_name.StylePriority.UseBorders = False
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.StylePriority.UseTextAlignment = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.00006357829!, 42.99998!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(266.6666!, 68.79166!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.xrPictureBox1.StylePriority.UseBorders = False
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.SizeF = New System.Drawing.SizeF(417.0!, 33.0!)
        Me.xr_ord_tp.StylePriority.UseBorders = False
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES INVOICE"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(636.0!, 0.0!)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.SizeF = New System.Drawing.SizeF(154.0!, 17.0!)
        Me.xrPageInfo2.StylePriority.UseBorders = False
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.StylePriority.UseForeColor = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 125.0!)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.SizeF = New System.Drawing.SizeF(2.0!, 2.0!)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xr_pd
        '
        Me.xr_pd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.Black
        Me.xr_pd.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pd.StylePriority.UseBorders = False
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.StylePriority.UseTextAlignment = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_pu_del_dt.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 2.0!)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_pu_del_dt.StylePriority.UseBorders = False
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.StylePriority.UseForeColor = False
        Me.xr_pu_del_dt.StylePriority.UseTextAlignment = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cust_cd.CanGrow = False
        Me.xr_cust_cd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.SizeF = New System.Drawing.SizeF(129.0!, 17.0!)
        Me.xr_cust_cd.StylePriority.UseBorders = False
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.StylePriority.UseTextAlignment = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        Me.xr_cust_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.0!)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_b_phone.StylePriority.UseBorders = False
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_h_phone.CanShrink = True
        Me.xr_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.0!)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_h_phone.StylePriority.UseBorders = False
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_zip.LocationFloat = New DevExpress.Utils.PointFloat(147.0!, 69.0!)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_zip.StylePriority.UseBorders = False
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_state.LocationFloat = New DevExpress.Utils.PointFloat(121.0!, 69.0!)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_state.StylePriority.UseBorders = False
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 69.0!)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_city.StylePriority.UseBorders = False
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr2.CanShrink = True
        Me.xr_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.0!)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr2.StylePriority.UseBorders = False
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_del_doc_num.CanGrow = False
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 14.0!)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.SizeF = New System.Drawing.SizeF(129.0!, 24.0!)
        Me.xr_del_doc_num.StylePriority.UseBorders = False
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.StylePriority.UseTextAlignment = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        Me.xr_del_doc_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 125.0!)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 125.0!)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.ForeColor = System.Drawing.Color.Black
        Me.xr_so_wr_dt.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 2.0!)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_so_wr_dt.StylePriority.UseBorders = False
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.StylePriority.UseForeColor = False
        Me.xr_so_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_so_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_addr1.StylePriority.UseBorders = False
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.SizeF = New System.Drawing.SizeF(530.0!, 17.0!)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_warranty, Me.xr_setup, Me.xrRichText2, Me.xrRichText1, Me.xrPictureBox2, Me.xr_tax_cd, Me.xr_tax, Me.xrPageBreak1, Me.xr_pmt_dt, Me.xrLine2, Me.xrLabel4, Me.xrTable8, Me.xrPanel1, Me.xr_ref, Me.xr_merchant, Me.xr_pmt_amt, Me.xr_appr_cd, Me.xr_exp_dt, Me.xrLabel9, Me.xrLine1, Me.xrLine7, Me.xr_triggers, Me.xrLabel27, Me.xrTable1, Me.xrLabel11, Me.xr_card_no, Me.xr_mop, Me.xrLine5})
        Me.ReportFooter.HeightF = 1423.583!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_setup
        '
        Me.xr_setup.LocationFloat = New DevExpress.Utils.PointFloat(536.4584!, 133.0!)
        Me.xr_setup.Name = "xr_setup"
        Me.xr_setup.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_setup.SizeF = New System.Drawing.SizeF(13.54163!, 10.50002!)
        Me.xr_setup.Visible = False
        '
        'xrRichText2
        '
        Me.xrRichText2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(2.53334!, 452.0833!)
        Me.xrRichText2.Name = "xrRichText2"
        Me.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString")
        Me.xrRichText2.SizeF = New System.Drawing.SizeF(390.9999!, 850.0417!)
        Me.xrRichText2.StylePriority.UseBorders = False
        '
        'xrRichText1
        '
        Me.xrRichText1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(402.5333!, 452.0833!)
        Me.xrRichText1.Name = "xrRichText1"
        Me.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString")
        Me.xrRichText1.SizeF = New System.Drawing.SizeF(397.0001!, 826.125!)
        Me.xrRichText1.StylePriority.UseBorders = False
        '
        'xrPictureBox2
        '
        Me.xrPictureBox2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPictureBox2.Image = CType(resources.GetObject("xrPictureBox2.Image"), System.Drawing.Image)
        Me.xrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(535.5333!, 1011.417!)
        Me.xrPictureBox2.Name = "xrPictureBox2"
        Me.xrPictureBox2.SizeF = New System.Drawing.SizeF(245.9583!, 63.54169!)
        Me.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.xrPictureBox2.StylePriority.UseBorders = False
        '
        'xr_tax_cd
        '
        Me.xr_tax_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tax_cd.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 139.25!)
        Me.xr_tax_cd.Name = "xr_tax_cd"
        Me.xr_tax_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax_cd.SizeF = New System.Drawing.SizeF(6.886078!, 4.25!)
        Me.xr_tax_cd.StylePriority.UseBorders = False
        Me.xr_tax_cd.Visible = False
        '
        'xr_tax
        '
        Me.xr_tax.LocationFloat = New DevExpress.Utils.PointFloat(453.4583!, 133.0!)
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.SizeF = New System.Drawing.SizeF(13.54163!, 10.50002!)
        Me.xr_tax.Visible = False
        '
        'xrPageBreak1
        '
        Me.xrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 403.9167!)
        Me.xrPageBreak1.Name = "xrPageBreak1"
        '
        'xr_pmt_dt
        '
        Me.xr_pmt_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pmt_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_dt.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 221.125!)
        Me.xr_pmt_dt.Multiline = True
        Me.xr_pmt_dt.Name = "xr_pmt_dt"
        Me.xr_pmt_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_dt.SizeF = New System.Drawing.SizeF(53.76667!, 17.0!)
        Me.xr_pmt_dt.StylePriority.UseBorders = False
        Me.xr_pmt_dt.StylePriority.UseFont = False
        Me.xr_pmt_dt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLine2
        '
        Me.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(299.0833!, 304.0833!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(458.0!, 9.0!)
        Me.xrLine2.StylePriority.UseBorders = False
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'xrLabel4
        '
        Me.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 296.0833!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(298.9584!, 17.00002!)
        Me.xrLabel4.StylePriority.UseBorders = False
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseForeColor = False
        Me.xrLabel4.Text = "I authorize special purchase of the above merchandise"
        '
        'xrTable8
        '
        Me.xrTable8.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable8.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 4.0!)
        Me.xrTable8.Name = "xrTable8"
        Me.xrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow13, Me.xrTableRow14, Me.xrTableRow15, Me.xrTableRow20, Me.xrTableRow16, Me.xrTableRow17, Me.xrTableRow18, Me.xrTableRow21, Me.xrTableRow22})
        Me.xrTable8.SizeF = New System.Drawing.SizeF(250.0!, 186.0!)
        Me.xrTable8.StylePriority.UseBorders = False
        '
        'xrTableRow13
        '
        Me.xrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell42, Me.xr_subtotal})
        Me.xrTableRow13.Name = "xrTableRow13"
        Me.xrTableRow13.Weight = 1.0R
        '
        'xrTableCell42
        '
        Me.xrTableCell42.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell42.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell42.Name = "xrTableCell42"
        Me.xrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell42.StylePriority.UseBackColor = False
        Me.xrTableCell42.StylePriority.UseBorderColor = False
        Me.xrTableCell42.StylePriority.UseFont = False
        Me.xrTableCell42.StylePriority.UseTextAlignment = False
        Me.xrTableCell42.Text = "SubTotal:"
        Me.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell42.Weight = 1.08R
        '
        'xr_subtotal
        '
        Me.xr_subtotal.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_subtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_subtotal.StylePriority.UseBorderColor = False
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseTextAlignment = False
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_subtotal.Weight = 1.0899999999999999R
        '
        'xrTableRow14
        '
        Me.xrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell44, Me.xr_del})
        Me.xrTableRow14.Name = "xrTableRow14"
        Me.xrTableRow14.Weight = 1.0R
        '
        'xrTableCell44
        '
        Me.xrTableCell44.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell44.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell44.Name = "xrTableCell44"
        Me.xrTableCell44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell44.StylePriority.UseBackColor = False
        Me.xrTableCell44.StylePriority.UseBorderColor = False
        Me.xrTableCell44.StylePriority.UseFont = False
        Me.xrTableCell44.StylePriority.UseTextAlignment = False
        Me.xrTableCell44.Text = "Shipping:"
        Me.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell44.Weight = 1.08R
        '
        'xr_del
        '
        Me.xr_del.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_del.StylePriority.UseBorderColor = False
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseTextAlignment = False
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_del.Weight = 1.0899999999999999R
        '
        'xrTableRow15
        '
        Me.xrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell47, Me.xr_tax_pst})
        Me.xrTableRow15.Name = "xrTableRow15"
        Me.xrTableRow15.Weight = 1.0R
        '
        'xrTableCell47
        '
        Me.xrTableCell47.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell47.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell47.Name = "xrTableCell47"
        Me.xrTableCell47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell47.StylePriority.UseBackColor = False
        Me.xrTableCell47.StylePriority.UseBorderColor = False
        Me.xrTableCell47.StylePriority.UseFont = False
        Me.xrTableCell47.StylePriority.UseTextAlignment = False
        Me.xrTableCell47.Text = "PST Tax:"
        Me.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell47.Weight = 1.08R
        '
        'xr_tax_pst
        '
        Me.xr_tax_pst.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax_pst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax_pst.Name = "xr_tax_pst"
        Me.xr_tax_pst.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_tax_pst.StylePriority.UseBorderColor = False
        Me.xr_tax_pst.StylePriority.UseFont = False
        Me.xr_tax_pst.StylePriority.UseTextAlignment = False
        Me.xr_tax_pst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax_pst.Weight = 1.0899999999999999R
        '
        'xrTableRow20
        '
        Me.xrTableRow20.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell40, Me.xr_tax_gst})
        Me.xrTableRow20.Name = "xrTableRow20"
        Me.xrTableRow20.Weight = 1.0R
        '
        'xrTableCell40
        '
        Me.xrTableCell40.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell40.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell40.Name = "xrTableCell40"
        Me.xrTableCell40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell40.StylePriority.UseBackColor = False
        Me.xrTableCell40.StylePriority.UseBorderColor = False
        Me.xrTableCell40.StylePriority.UseFont = False
        Me.xrTableCell40.StylePriority.UsePadding = False
        Me.xrTableCell40.StylePriority.UseTextAlignment = False
        Me.xrTableCell40.Text = "GST Tax:"
        Me.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell40.Weight = 1.08R
        '
        'xr_tax_gst
        '
        Me.xr_tax_gst.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_tax_gst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax_gst.Name = "xr_tax_gst"
        Me.xr_tax_gst.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax_gst.StylePriority.UseBorderColor = False
        Me.xr_tax_gst.StylePriority.UseFont = False
        Me.xr_tax_gst.StylePriority.UsePadding = False
        Me.xr_tax_gst.StylePriority.UseTextAlignment = False
        Me.xr_tax_gst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tax_gst.Weight = 1.0899999999999999R
        '
        'xrTableRow16
        '
        Me.xrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell49, Me.xr_grand_total})
        Me.xrTableRow16.Name = "xrTableRow16"
        Me.xrTableRow16.Weight = 1.0R
        '
        'xrTableCell49
        '
        Me.xrTableCell49.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell49.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell49.Name = "xrTableCell49"
        Me.xrTableCell49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell49.StylePriority.UseBackColor = False
        Me.xrTableCell49.StylePriority.UseBorderColor = False
        Me.xrTableCell49.StylePriority.UseFont = False
        Me.xrTableCell49.StylePriority.UseTextAlignment = False
        Me.xrTableCell49.Text = "Total Sale:"
        Me.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell49.Weight = 1.08R
        '
        'xr_grand_total
        '
        Me.xr_grand_total.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_grand_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_grand_total.StylePriority.UseBorderColor = False
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseTextAlignment = False
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_grand_total.Weight = 1.0899999999999999R
        '
        'xrTableRow17
        '
        Me.xrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell51, Me.xr_payment_holder})
        Me.xrTableRow17.Name = "xrTableRow17"
        Me.xrTableRow17.Weight = 1.0R
        '
        'xrTableCell51
        '
        Me.xrTableCell51.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell51.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell51.Name = "xrTableCell51"
        Me.xrTableCell51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell51.StylePriority.UseBackColor = False
        Me.xrTableCell51.StylePriority.UseBorderColor = False
        Me.xrTableCell51.StylePriority.UseFont = False
        Me.xrTableCell51.StylePriority.UseTextAlignment = False
        Me.xrTableCell51.Text = "Payments:"
        Me.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell51.Weight = 1.08R
        '
        'xrTableRow18
        '
        Me.xrTableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell53, Me.xr_balance})
        Me.xrTableRow18.Name = "xrTableRow18"
        Me.xrTableRow18.Weight = 1.0R
        '
        'xrTableCell53
        '
        Me.xrTableCell53.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell53.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell53.Name = "xrTableCell53"
        Me.xrTableCell53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell53.StylePriority.UseBackColor = False
        Me.xrTableCell53.StylePriority.UseBorderColor = False
        Me.xrTableCell53.StylePriority.UseFont = False
        Me.xrTableCell53.StylePriority.UseTextAlignment = False
        Me.xrTableCell53.Text = "Balance:"
        Me.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell53.Weight = 1.08R
        '
        'xr_balance
        '
        Me.xr_balance.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_balance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_balance.StylePriority.UseBorderColor = False
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseTextAlignment = False
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_balance.Weight = 1.0899999999999999R
        '
        'xrTableRow21
        '
        Me.xrTableRow21.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell45, Me.xr_finance})
        Me.xrTableRow21.Name = "xrTableRow21"
        Me.xrTableRow21.Weight = 1.0R
        '
        'xrTableCell45
        '
        Me.xrTableCell45.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell45.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell45.Name = "xrTableCell45"
        Me.xrTableCell45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell45.StylePriority.UseBackColor = False
        Me.xrTableCell45.StylePriority.UseBorderColor = False
        Me.xrTableCell45.StylePriority.UseFont = False
        Me.xrTableCell45.StylePriority.UsePadding = False
        Me.xrTableCell45.StylePriority.UseTextAlignment = False
        Me.xrTableCell45.Text = "Financed:"
        Me.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell45.Weight = 1.08R
        '
        'xr_finance
        '
        Me.xr_finance.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_finance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_finance.Name = "xr_finance"
        Me.xr_finance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_finance.StylePriority.UseBorderColor = False
        Me.xr_finance.StylePriority.UseFont = False
        Me.xr_finance.StylePriority.UsePadding = False
        Me.xr_finance.StylePriority.UseTextAlignment = False
        Me.xr_finance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_finance.Weight = 1.0899999999999999R
        '
        'xrTableRow22
        '
        Me.xrTableRow22.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell50, Me.xr_rem})
        Me.xrTableRow22.Name = "xrTableRow22"
        Me.xrTableRow22.Weight = 1.0R
        '
        'xrTableCell50
        '
        Me.xrTableCell50.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell50.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell50.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell50.Name = "xrTableCell50"
        Me.xrTableCell50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell50.StylePriority.UseBackColor = False
        Me.xrTableCell50.StylePriority.UseBorderColor = False
        Me.xrTableCell50.StylePriority.UseFont = False
        Me.xrTableCell50.StylePriority.UsePadding = False
        Me.xrTableCell50.StylePriority.UseTextAlignment = False
        Me.xrTableCell50.Text = "Balance Due:"
        Me.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrTableCell50.Weight = 1.08R
        '
        'xr_rem
        '
        Me.xr_rem.BorderColor = System.Drawing.Color.Gainsboro
        Me.xr_rem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_rem.Name = "xr_rem"
        Me.xr_rem.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_rem.StylePriority.UseBorderColor = False
        Me.xr_rem.StylePriority.UseFont = False
        Me.xr_rem.StylePriority.UsePadding = False
        Me.xr_rem.StylePriority.UseTextAlignment = False
        Me.xr_rem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_rem.Weight = 1.0899999999999999R
        '
        'xrPanel1
        '
        Me.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_comments, Me.xrLabel10})
        Me.xrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
        Me.xrPanel1.Name = "xrPanel1"
        Me.xrPanel1.SizeF = New System.Drawing.SizeF(533.0!, 125.0!)
        Me.xrPanel1.StylePriority.UseBorders = False
        '
        'xrLabel10
        '
        Me.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.xrLabel10.StylePriority.UseBorders = False
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xr_ref
        '
        Me.xr_ref.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_ref.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_ref.LocationFloat = New DevExpress.Utils.PointFloat(733.5334!, 221.125!)
        Me.xr_ref.Multiline = True
        Me.xr_ref.Name = "xr_ref"
        Me.xr_ref.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ref.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
        Me.xr_ref.StylePriority.UseBorders = False
        Me.xr_ref.StylePriority.UseFont = False
        Me.xr_ref.StylePriority.UseTextAlignment = False
        Me.xr_ref.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_merchant
        '
        Me.xr_merchant.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_merchant.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_merchant.LocationFloat = New DevExpress.Utils.PointFloat(591.0667!, 221.125!)
        Me.xr_merchant.Multiline = True
        Me.xr_merchant.Name = "xr_merchant"
        Me.xr_merchant.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_merchant.SizeF = New System.Drawing.SizeF(142.4667!, 17.00002!)
        Me.xr_merchant.StylePriority.UseBorders = False
        Me.xr_merchant.StylePriority.UseFont = False
        Me.xr_merchant.StylePriority.UseTextAlignment = False
        Me.xr_merchant.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_amt.LocationFloat = New DevExpress.Utils.PointFloat(484.9333!, 221.125!)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
        Me.xr_pmt_amt.StylePriority.UseBorders = False
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_appr_cd
        '
        Me.xr_appr_cd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_appr_cd.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_appr_cd.LocationFloat = New DevExpress.Utils.PointFloat(391.7333!, 221.125!)
        Me.xr_appr_cd.Multiline = True
        Me.xr_appr_cd.Name = "xr_appr_cd"
        Me.xr_appr_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_appr_cd.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xr_appr_cd.StylePriority.UseBorders = False
        Me.xr_appr_cd.StylePriority.UseFont = False
        Me.xr_appr_cd.StylePriority.UseTextAlignment = False
        Me.xr_appr_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_exp_dt
        '
        Me.xr_exp_dt.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_exp_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_exp_dt.LocationFloat = New DevExpress.Utils.PointFloat(336.8333!, 221.125!)
        Me.xr_exp_dt.Multiline = True
        Me.xr_exp_dt.Name = "xr_exp_dt"
        Me.xr_exp_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_exp_dt.SizeF = New System.Drawing.SizeF(54.16669!, 17.0!)
        Me.xr_exp_dt.StylePriority.UseBorders = False
        Me.xr_exp_dt.StylePriority.UseFont = False
        Me.xr_exp_dt.StylePriority.UseTextAlignment = False
        Me.xr_exp_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLabel9
        '
        Me.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 351.7501!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xrLabel9.StylePriority.UseBorders = False
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "Merchandise Taken"
        '
        'xrLine1
        '
        Me.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 359.75!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(458.0!, 9.0!)
        Me.xrLine1.StylePriority.UseBorders = False
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.SizeF = New System.Drawing.SizeF(800.0!, 2.0!)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xr_triggers
        '
        Me.xr_triggers.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_triggers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_triggers.ForeColor = System.Drawing.Color.Gray
        Me.xr_triggers.LocationFloat = New DevExpress.Utils.PointFloat(2.533277!, 422.9167!)
        Me.xr_triggers.Multiline = True
        Me.xr_triggers.Name = "xr_triggers"
        Me.xr_triggers.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_triggers.SizeF = New System.Drawing.SizeF(797.0!, 17.0!)
        Me.xr_triggers.StylePriority.UseBorders = False
        Me.xr_triggers.StylePriority.UseFont = False
        Me.xr_triggers.StylePriority.UseForeColor = False
        '
        'xrLabel27
        '
        Me.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel27.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 245.1251!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(800.0!, 33.0!)
        Me.xrLabel27.StylePriority.UseBorders = False
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTable1
        '
        Me.xrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 203.125!)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow3})
        Me.xrTable1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
        Me.xrTable1.StylePriority.UseBorders = False
        Me.xrTable1.StylePriority.UseFont = False
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell31, Me.xrTableCell7, Me.xrTableCell10, Me.xrTableCell8, Me.xrTableCell9, Me.xrTableCell11, Me.xrTableCell12, Me.xrTableCell13})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.Weight = 1.0R
        '
        'xrTableCell31
        '
        Me.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell31.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell31.Name = "xrTableCell31"
        Me.xrTableCell31.StylePriority.UseBorders = False
        Me.xrTableCell31.StylePriority.UseFont = False
        Me.xrTableCell31.StylePriority.UseForeColor = False
        Me.xrTableCell31.Text = "Date"
        Me.xrTableCell31.Weight = 0.067208333333333328R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell7.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell7.StylePriority.UseBorders = False
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UseForeColor = False
        Me.xrTableCell7.StylePriority.UseTextAlignment = False
        Me.xrTableCell7.Text = "Payment Type"
        Me.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell7.Weight = 0.14508329900105793R
        '
        'xrTableCell10
        '
        Me.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell10.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell10.Name = "xrTableCell10"
        Me.xrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell10.StylePriority.UseBorders = False
        Me.xrTableCell10.StylePriority.UseFont = False
        Me.xrTableCell10.StylePriority.UseForeColor = False
        Me.xrTableCell10.StylePriority.UseTextAlignment = False
        Me.xrTableCell10.Text = " Card Number"
        Me.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell10.Weight = 0.20875004386901858R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell8.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell8.StylePriority.UseBorders = False
        Me.xrTableCell8.StylePriority.UseFont = False
        Me.xrTableCell8.StylePriority.UseForeColor = False
        Me.xrTableCell8.StylePriority.UseTextAlignment = False
        Me.xrTableCell8.Text = " Exp Dt"
        Me.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell8.Weight = 0.068624990463256832R
        '
        'xrTableCell9
        '
        Me.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell9.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell9.StylePriority.UseBorders = False
        Me.xrTableCell9.StylePriority.UseFont = False
        Me.xrTableCell9.StylePriority.UseForeColor = False
        Me.xrTableCell9.StylePriority.UseTextAlignment = False
        Me.xrTableCell9.Text = " Approval"
        Me.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell9.Weight = 0.11524999999999999R
        '
        'xrTableCell11
        '
        Me.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell11.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseBorders = False
        Me.xrTableCell11.StylePriority.UseFont = False
        Me.xrTableCell11.StylePriority.UseForeColor = False
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "Amount"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell11.Weight = 0.13391666666666668R
        '
        'xrTableCell12
        '
        Me.xrTableCell12.BorderColor = System.Drawing.SystemColors.ControlText
        Me.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell12.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell12.StylePriority.UseBorderColor = False
        Me.xrTableCell12.StylePriority.UseBorders = False
        Me.xrTableCell12.StylePriority.UseFont = False
        Me.xrTableCell12.StylePriority.UseForeColor = False
        Me.xrTableCell12.StylePriority.UseTextAlignment = False
        Me.xrTableCell12.Text = "Merchant ID"
        Me.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell12.Weight = 0.17933333333333334R
        '
        'xrTableCell13
        '
        Me.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell13.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell13.Name = "xrTableCell13"
        Me.xrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell13.StylePriority.UseBorders = False
        Me.xrTableCell13.StylePriority.UseFont = False
        Me.xrTableCell13.StylePriority.UseForeColor = False
        Me.xrTableCell13.StylePriority.UseTextAlignment = False
        Me.xrTableCell13.Text = "Ref"
        Me.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell13.Weight = 0.081833333333333341R
        '
        'xrLabel11
        '
        Me.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel11.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 405.9167!)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
        Me.xrLabel11.StylePriority.UseBorders = False
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.Text = "TERMS AND CONDITIONS OF SALE:"
        Me.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_card_no
        '
        Me.xr_card_no.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_card_no.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_card_no.LocationFloat = New DevExpress.Utils.PointFloat(169.8333!, 221.125!)
        Me.xr_card_no.Multiline = True
        Me.xr_card_no.Name = "xr_card_no"
        Me.xr_card_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_card_no.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
        Me.xr_card_no.StylePriority.UseBorders = False
        Me.xr_card_no.StylePriority.UseFont = False
        Me.xr_card_no.StylePriority.UseTextAlignment = False
        Me.xr_card_no.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_mop
        '
        Me.xr_mop.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_mop.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop.LocationFloat = New DevExpress.Utils.PointFloat(54.84993!, 221.125!)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.SizeF = New System.Drawing.SizeF(113.6751!, 17.0!)
        Me.xr_mop.StylePriority.UseBorders = False
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 195.1251!)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.SizeF = New System.Drawing.SizeF(800.0!, 2.0!)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2, Me.xr_ord_tp, Me.xrPictureBox1})
        Me.ReportHeader.HeightF = 126.7916!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0.0R
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0.0R
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0.0R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0.0R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0.0R
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0.0R
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0.0R
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_PST, Me.xrTable6, Me.xrTable5, Me.xrTable4, Me.xrTable3, Me.xr_slsp2_hidden, Me.xr_ord_srt, Me.xr_ord_tp_hidden, Me.xr_disc_cd, Me.xr_lname, Me.xr_fname, Me.xr_GST})
        Me.GroupHeader1.HeightF = 242.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_PST
        '
        Me.xr_PST.LocationFloat = New DevExpress.Utils.PointFloat(678.6!, 116.5!)
        Me.xr_PST.Name = "xr_PST"
        Me.xr_PST.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_PST.SizeF = New System.Drawing.SizeF(55.93329!, 28.74003!)
        Me.xr_PST.Visible = False
        '
        'xrTable6
        '
        Me.xrTable6.BackColor = System.Drawing.Color.RosyBrown
        Me.xrTable6.LocationFloat = New DevExpress.Utils.PointFloat(9.99999!, 217.0!)
        Me.xrTable6.Name = "xrTable6"
        Me.xrTable6.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow11})
        Me.xrTable6.SizeF = New System.Drawing.SizeF(790.0!, 25.0!)
        Me.xrTable6.StylePriority.UseBackColor = False
        Me.xrTable6.StylePriority.UseTextAlignment = False
        Me.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow11
        '
        Me.xrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell36, Me.xrTableCell41, Me.xrTableCell37, Me.xrTableCell38, Me.xrTableCell39})
        Me.xrTableRow11.Name = "xrTableRow11"
        Me.xrTableRow11.Weight = 1.0R
        '
        'xrTableCell36
        '
        Me.xrTableCell36.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell36.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell36.Name = "xrTableCell36"
        Me.xrTableCell36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell36.StylePriority.UseBackColor = False
        Me.xrTableCell36.StylePriority.UseBorderColor = False
        Me.xrTableCell36.StylePriority.UseFont = False
        Me.xrTableCell36.StylePriority.UseTextAlignment = False
        Me.xrTableCell36.Text = "QTY"
        Me.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell36.Weight = 0.15384619786189152R
        '
        'xrTableCell41
        '
        Me.xrTableCell41.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell41.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell41.Name = "xrTableCell41"
        Me.xrTableCell41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell41.StylePriority.UseBackColor = False
        Me.xrTableCell41.StylePriority.UseBorderColor = False
        Me.xrTableCell41.StylePriority.UseFont = False
        Me.xrTableCell41.StylePriority.UseTextAlignment = False
        Me.xrTableCell41.Text = "SKU"
        Me.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell41.Weight = 0.30769232823298537R
        '
        'xrTableCell37
        '
        Me.xrTableCell37.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell37.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell37.Name = "xrTableCell37"
        Me.xrTableCell37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell37.StylePriority.UseBackColor = False
        Me.xrTableCell37.StylePriority.UseBorderColor = False
        Me.xrTableCell37.StylePriority.UseFont = False
        Me.xrTableCell37.StylePriority.UseTextAlignment = False
        Me.xrTableCell37.Text = "Description"
        Me.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell37.Weight = 1.8461537111722506R
        '
        'xrTableCell38
        '
        Me.xrTableCell38.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell38.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell38.Name = "xrTableCell38"
        Me.xrTableCell38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell38.StylePriority.UseBackColor = False
        Me.xrTableCell38.StylePriority.UseBorderColor = False
        Me.xrTableCell38.StylePriority.UseFont = False
        Me.xrTableCell38.StylePriority.UseTextAlignment = False
        Me.xrTableCell38.Text = "Unit Price"
        Me.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell38.Weight = 0.35355776273287259R
        '
        'xrTableCell39
        '
        Me.xrTableCell39.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell39.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTableCell39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell39.Name = "xrTableCell39"
        Me.xrTableCell39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell39.StylePriority.UseBackColor = False
        Me.xrTableCell39.StylePriority.UseBorderColor = False
        Me.xrTableCell39.StylePriority.UseFont = False
        Me.xrTableCell39.StylePriority.UseTextAlignment = False
        Me.xrTableCell39.Text = "Total"
        Me.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell39.Weight = 0.33875R
        '
        'xrTable5
        '
        Me.xrTable5.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable5.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 8.0!)
        Me.xrTable5.Name = "xrTable5"
        Me.xrTable5.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow9, Me.xrTableRow10})
        Me.xrTable5.SizeF = New System.Drawing.SizeF(258.0!, 100.0!)
        Me.xrTable5.StylePriority.UseBorders = False
        '
        'xrTableRow9
        '
        Me.xrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell32, Me.xrTableCell33})
        Me.xrTableRow9.Name = "xrTableRow9"
        Me.xrTableRow9.Weight = 1.0R
        '
        'xrTableCell32
        '
        Me.xrTableCell32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell32.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel2})
        Me.xrTableCell32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell32.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell32.Name = "xrTableCell32"
        Me.xrTableCell32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell32.StylePriority.UseBackColor = False
        Me.xrTableCell32.StylePriority.UseFont = False
        Me.xrTableCell32.StylePriority.UseForeColor = False
        Me.xrTableCell32.Weight = 1.08R
        '
        'xrLabel2
        '
        Me.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel2.StylePriority.UseBorders = False
        Me.xrLabel2.Text = " Invoice No.:"
        '
        'xrTableCell33
        '
        Me.xrTableCell33.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_del_doc_num})
        Me.xrTableCell33.Name = "xrTableCell33"
        Me.xrTableCell33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell33.Weight = 1.0899999999999999R
        '
        'xrTableRow10
        '
        Me.xrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell34, Me.xrTableCell35})
        Me.xrTableRow10.Name = "xrTableRow10"
        Me.xrTableRow10.Weight = 1.0R
        '
        'xrTableCell34
        '
        Me.xrTableCell34.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell34.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel3})
        Me.xrTableCell34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell34.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell34.Name = "xrTableCell34"
        Me.xrTableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell34.StylePriority.UseBackColor = False
        Me.xrTableCell34.StylePriority.UseFont = False
        Me.xrTableCell34.StylePriority.UseForeColor = False
        Me.xrTableCell34.Weight = 1.08R
        '
        'xrLabel3
        '
        Me.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 8.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel3.StylePriority.UseBorders = False
        Me.xrLabel3.Text = " Customer ID:"
        '
        'xrTableCell35
        '
        Me.xrTableCell35.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cust_cd})
        Me.xrTableCell35.Name = "xrTableCell35"
        Me.xrTableCell35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell35.Weight = 1.0899999999999999R
        '
        'xrTable4
        '
        Me.xrTable4.BorderColor = System.Drawing.Color.Gainsboro
        Me.xrTable4.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 158.0!)
        Me.xrTable4.Name = "xrTable4"
        Me.xrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow7, Me.xrTableRow8})
        Me.xrTable4.SizeF = New System.Drawing.SizeF(800.0!, 42.0!)
        Me.xrTable4.StylePriority.UseBorderColor = False
        Me.xrTable4.StylePriority.UseBorders = False
        '
        'xrTableRow7
        '
        Me.xrTableRow7.BackColor = System.Drawing.Color.Tan
        Me.xrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell20, Me.xrTableCell25, Me.xrTableCell21, Me.xrTableCell24, Me.xrTableCell22, Me.xrTableCell23})
        Me.xrTableRow7.Name = "xrTableRow7"
        Me.xrTableRow7.StylePriority.UseBackColor = False
        Me.xrTableRow7.Weight = 1.0R
        '
        'xrTableCell20
        '
        Me.xrTableCell20.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell20.Name = "xrTableCell20"
        Me.xrTableCell20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell20.StylePriority.UseBackColor = False
        Me.xrTableCell20.StylePriority.UseFont = False
        Me.xrTableCell20.StylePriority.UseTextAlignment = False
        Me.xrTableCell20.Text = "Date"
        Me.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell20.Weight = 0.5R
        '
        'xrTableCell25
        '
        Me.xrTableCell25.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell25.Name = "xrTableCell25"
        Me.xrTableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell25.StylePriority.UseBackColor = False
        Me.xrTableCell25.StylePriority.UseFont = False
        Me.xrTableCell25.StylePriority.UseTextAlignment = False
        Me.xrTableCell25.Text = "Order No."
        Me.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell25.Weight = 0.5R
        '
        'xrTableCell21
        '
        Me.xrTableCell21.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell21.Name = "xrTableCell21"
        Me.xrTableCell21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell21.StylePriority.UseBackColor = False
        Me.xrTableCell21.StylePriority.UseFont = False
        Me.xrTableCell21.StylePriority.UseTextAlignment = False
        Me.xrTableCell21.Text = "Ship Via"
        Me.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell21.Weight = 0.5R
        '
        'xrTableCell24
        '
        Me.xrTableCell24.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell24.Name = "xrTableCell24"
        Me.xrTableCell24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell24.StylePriority.UseBackColor = False
        Me.xrTableCell24.StylePriority.UseFont = False
        Me.xrTableCell24.StylePriority.UseTextAlignment = False
        Me.xrTableCell24.Text = "Ship Date"
        Me.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell24.Weight = 0.5R
        '
        'xrTableCell22
        '
        Me.xrTableCell22.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell22.Name = "xrTableCell22"
        Me.xrTableCell22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell22.StylePriority.UseBackColor = False
        Me.xrTableCell22.StylePriority.UseFont = False
        Me.xrTableCell22.StylePriority.UseTextAlignment = False
        Me.xrTableCell22.Text = "Terms"
        Me.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell22.Weight = 0.5R
        '
        'xrTableCell23
        '
        Me.xrTableCell23.BackColor = System.Drawing.Color.Silver
        Me.xrTableCell23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell23.Name = "xrTableCell23"
        Me.xrTableCell23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell23.StylePriority.UseBackColor = False
        Me.xrTableCell23.StylePriority.UseFont = False
        Me.xrTableCell23.StylePriority.UseTextAlignment = False
        Me.xrTableCell23.Text = "P.O."
        Me.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell23.Weight = 0.5R
        '
        'xrTableRow8
        '
        Me.xrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell26, Me.xr_sale_last_four, Me.xrTableCell28, Me.xrTableCell29, Me.xrTableCell30, Me.xr_po_num})
        Me.xrTableRow8.Name = "xrTableRow8"
        Me.xrTableRow8.Weight = 1.0R
        '
        'xrTableCell26
        '
        Me.xrTableCell26.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_so_wr_dt})
        Me.xrTableCell26.Multiline = True
        Me.xrTableCell26.Name = "xrTableCell26"
        Me.xrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell26.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrTableCell26.Weight = 0.5R
        '
        'xr_sale_last_four
        '
        Me.xr_sale_last_four.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sale_last_four.Name = "xr_sale_last_four"
        Me.xr_sale_last_four.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_sale_last_four.StylePriority.UseFont = False
        Me.xr_sale_last_four.StylePriority.UseTextAlignment = False
        Me.xr_sale_last_four.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_sale_last_four.Weight = 0.5R
        '
        'xrTableCell28
        '
        Me.xrTableCell28.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pd})
        Me.xrTableCell28.Name = "xrTableCell28"
        Me.xrTableCell28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell28.Weight = 0.5R
        '
        'xrTableCell29
        '
        Me.xrTableCell29.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_pu_del_dt})
        Me.xrTableCell29.Name = "xrTableCell29"
        Me.xrTableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell29.Weight = 0.5R
        '
        'xrTableCell30
        '
        Me.xrTableCell30.Name = "xrTableCell30"
        Me.xrTableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell30.StylePriority.UseTextAlignment = False
        Me.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell30.Weight = 0.5R
        '
        'xr_po_num
        '
        Me.xr_po_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_po_num.Name = "xr_po_num"
        Me.xr_po_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xr_po_num.StylePriority.UseFont = False
        Me.xr_po_num.StylePriority.UseTextAlignment = False
        Me.xr_po_num.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_po_num.Weight = 0.5R
        '
        'xrTable3
        '
        Me.xrTable3.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.xrTable3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
        Me.xrTable3.Name = "xrTable3"
        Me.xrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow5, Me.xrTableRow6})
        Me.xrTable3.SizeF = New System.Drawing.SizeF(500.0!, 142.0!)
        Me.xrTable3.StylePriority.UseBorderColor = False
        Me.xrTable3.StylePriority.UseBorders = False
        '
        'xrTableRow5
        '
        Me.xrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell16, Me.xrTableCell17})
        Me.xrTableRow5.Name = "xrTableRow5"
        Me.xrTableRow5.Weight = 0.55185659411011523R
        '
        'xrTableCell16
        '
        Me.xrTableCell16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell16.Name = "xrTableCell16"
        Me.xrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell16.StylePriority.UseBackColor = False
        Me.xrTableCell16.StylePriority.UseFont = False
        Me.xrTableCell16.StylePriority.UseForeColor = False
        Me.xrTableCell16.Text = " Ship To:"
        Me.xrTableCell16.Weight = 2.5R
        '
        'xrTableCell17
        '
        Me.xrTableCell17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.xrTableCell17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.xrTableCell17.Name = "xrTableCell17"
        Me.xrTableCell17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell17.StylePriority.UseBackColor = False
        Me.xrTableCell17.StylePriority.UseFont = False
        Me.xrTableCell17.StylePriority.UseForeColor = False
        Me.xrTableCell17.Text = " Bill To:"
        Me.xrTableCell17.Weight = 2.5R
        '
        'xrTableRow6
        '
        Me.xrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell18, Me.xrTableCell19})
        Me.xrTableRow6.Name = "xrTableRow6"
        Me.xrTableRow6.Weight = 3.99359795134443R
        '
        'xrTableCell18
        '
        Me.xrTableCell18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_corp_name, Me.xr_b_phone, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_addr1, Me.xr_full_name})
        Me.xrTableCell18.Name = "xrTableCell18"
        Me.xrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell18.Text = "xrTableCell18"
        Me.xrTableCell18.Weight = 2.5R
        '
        'xr_corp_name
        '
        Me.xr_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_corp_name.CanShrink = True
        Me.xr_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_corp_name.Name = "xr_corp_name"
        Me.xr_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_corp_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_corp_name.StylePriority.UseBorders = False
        Me.xr_corp_name.StylePriority.UseFont = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_full_name.CanShrink = True
        Me.xr_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_full_name.StylePriority.UseBorders = False
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'xrTableCell19
        '
        Me.xrTableCell19.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_bill_corp_name, Me.xr_bill_b_phone, Me.xr_bill_h_phone, Me.xr_bill_zip, Me.xr_bill_state, Me.xr_bill_city, Me.xr_bill_addr2, Me.xr_bill_addr1, Me.xr_bill_full_name})
        Me.xrTableCell19.Name = "xrTableCell19"
        Me.xrTableCell19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell19.Text = "xrTableCell19"
        Me.xrTableCell19.Weight = 2.5R
        '
        'xr_bill_corp_name
        '
        Me.xr_bill_corp_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_corp_name.CanShrink = True
        Me.xr_bill_corp_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_corp_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_bill_corp_name.Name = "xr_bill_corp_name"
        Me.xr_bill_corp_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_corp_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_corp_name.StylePriority.UseBorders = False
        Me.xr_bill_corp_name.StylePriority.UseFont = False
        '
        'xr_bill_b_phone
        '
        Me.xr_bill_b_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_b_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_b_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.0!)
        Me.xr_bill_b_phone.Name = "xr_bill_b_phone"
        Me.xr_bill_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_b_phone.SizeF = New System.Drawing.SizeF(107.0!, 17.0!)
        Me.xr_bill_b_phone.StylePriority.UseBorders = False
        Me.xr_bill_b_phone.StylePriority.UseFont = False
        Me.xr_bill_b_phone.Text = "xr_b_phone"
        '
        'xr_bill_h_phone
        '
        Me.xr_bill_h_phone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_h_phone.CanShrink = True
        Me.xr_bill_h_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_h_phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.0!)
        Me.xr_bill_h_phone.Name = "xr_bill_h_phone"
        Me.xr_bill_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_h_phone.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
        Me.xr_bill_h_phone.StylePriority.UseBorders = False
        Me.xr_bill_h_phone.StylePriority.UseFont = False
        Me.xr_bill_h_phone.Text = "xr_h_phone"
        '
        'xr_bill_zip
        '
        Me.xr_bill_zip.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_zip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_zip.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 69.0!)
        Me.xr_bill_zip.Name = "xr_bill_zip"
        Me.xr_bill_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_zip.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_bill_zip.StylePriority.UseBorders = False
        Me.xr_bill_zip.StylePriority.UseFont = False
        Me.xr_bill_zip.Text = "xr_zip"
        '
        'xr_bill_state
        '
        Me.xr_bill_state.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_state.CanGrow = False
        Me.xr_bill_state.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_state.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 69.0!)
        Me.xr_bill_state.Name = "xr_bill_state"
        Me.xr_bill_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_state.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_bill_state.StylePriority.UseBorders = False
        Me.xr_bill_state.StylePriority.UseFont = False
        Me.xr_bill_state.Text = "xr_state"
        '
        'xr_bill_city
        '
        Me.xr_bill_city.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_city.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 69.0!)
        Me.xr_bill_city.Name = "xr_bill_city"
        Me.xr_bill_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_city.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.xr_bill_city.StylePriority.UseBorders = False
        Me.xr_bill_city.StylePriority.UseFont = False
        '
        'xr_bill_addr2
        '
        Me.xr_bill_addr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr2.CanShrink = True
        Me.xr_bill_addr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 52.0!)
        Me.xr_bill_addr2.Name = "xr_bill_addr2"
        Me.xr_bill_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr2.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_addr2.StylePriority.UseBorders = False
        Me.xr_bill_addr2.StylePriority.UseFont = False
        '
        'xr_bill_addr1
        '
        Me.xr_bill_addr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_addr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_addr1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
        Me.xr_bill_addr1.Name = "xr_bill_addr1"
        Me.xr_bill_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_addr1.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_addr1.StylePriority.UseBorders = False
        Me.xr_bill_addr1.StylePriority.UseFont = False
        '
        'xr_bill_full_name
        '
        Me.xr_bill_full_name.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bill_full_name.CanShrink = True
        Me.xr_bill_full_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bill_full_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.xr_bill_full_name.Name = "xr_bill_full_name"
        Me.xr_bill_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bill_full_name.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
        Me.xr_bill_full_name.StylePriority.UseBorders = False
        Me.xr_bill_full_name.StylePriority.UseFont = False
        '
        'xr_slsp2_hidden
        '
        Me.xr_slsp2_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp2_hidden.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 150.0!)
        Me.xr_slsp2_hidden.Name = "xr_slsp2_hidden"
        Me.xr_slsp2_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp2_hidden.SizeF = New System.Drawing.SizeF(10.0!, 8.0!)
        Me.xr_slsp2_hidden.StylePriority.UseFont = False
        Me.xr_slsp2_hidden.Visible = False
        '
        'xr_ord_srt
        '
        Me.xr_ord_srt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_srt.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 150.0!)
        Me.xr_ord_srt.Name = "xr_ord_srt"
        Me.xr_ord_srt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_srt.SizeF = New System.Drawing.SizeF(2.0!, 8.0!)
        Me.xr_ord_srt.StylePriority.UseFont = False
        Me.xr_ord_srt.Visible = False
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 142.0!)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.SizeF = New System.Drawing.SizeF(8.0!, 8.0!)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_GST
        '
        Me.xr_GST.LocationFloat = New DevExpress.Utils.PointFloat(535.0!, 116.5!)
        Me.xr_GST.Name = "xr_GST"
        Me.xr_GST.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_GST.SizeF = New System.Drawing.SizeF(55.93329!, 28.74003!)
        Me.xr_GST.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel29, Me.xrPageInfo2})
        Me.PageFooter.HeightF = 43.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrLabel29
        '
        Me.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel29.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel29.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 16.99994!)
        Me.xrLabel29.Name = "xrLabel29"
        Me.xrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel29.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
        Me.xrLabel29.StylePriority.UseBorders = False
        Me.xrLabel29.StylePriority.UseFont = False
        Me.xrLabel29.StylePriority.UseForeColor = False
        Me.xrLabel29.StylePriority.UseTextAlignment = False
        Me.xrLabel29.Text = "CUSTOMER INITIAL:_____________"
        Me.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 10.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 10.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'xr_warranty
        '
        Me.xr_warranty.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_warranty.CanShrink = True
        Me.xr_warranty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_warranty.ForeColor = System.Drawing.Color.DimGray
        Me.xr_warranty.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 368.75!)
        Me.xr_warranty.Multiline = True
        Me.xr_warranty.Name = "xr_warranty"
        Me.xr_warranty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_warranty.SizeF = New System.Drawing.SizeF(799.0001!, 37.16669!)
        Me.xr_warranty.StylePriority.UseBorders = False
        Me.xr_warranty.StylePriority.UseFont = False
        Me.xr_warranty.StylePriority.UseForeColor = False
        Me.xr_warranty.StylePriority.UseTextAlignment = False
        Me.xr_warranty.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Extended Warranty [ ]  Accepted [ ] Declined    Signature Line_________________" & _
    "________________________"
        Me.xr_warranty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'LazboyWinnipegInvoiceFull
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 10, 10)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "11.2"
        Me.Watermark.ImageTransparency = 209
        Me.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Zoom
        CType(Me.xrTable7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_ord_srt.DataBindings.Add("Text", DataSource, "SO.ORD_SRT_CD")
        xr_po_num.DataBindings.Add("Text", DataSource, "SO.ORDER_PO_NUM")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_setup.DataBindings.Add("Text", DataSource, "SO.SETUP_CHG", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_tax_cd.DataBindings.Add("Text", DataSource, "SO.TAX_CD")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_corp_name.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CORP")
        xr_bill_corp_name.DataBindings.Add("Text", DataSource, "CUST.CORP_NAME")
        xr_bill_full_name.DataBindings.Add("Text", DataSource, "CUST.CUST_FULL_NAME")
        xr_bill_addr1.DataBindings.Add("Text", DataSource, "CUST.ADDR1")
        xr_bill_addr2.DataBindings.Add("Text", DataSource, "CUST.ADDR2")
        xr_bill_city.DataBindings.Add("Text", DataSource, "CUST.CITY")
        xr_bill_state.DataBindings.Add("Text", DataSource, "CUST.ST_CD")
        xr_bill_zip.DataBindings.Add("Text", DataSource, "CUST.ZIP_CD")
        xr_bill_h_phone.DataBindings.Add("Text", DataSource, "CUST.HOME_PHONE")
        xr_bill_b_phone.DataBindings.Add("Text", DataSource, "CUST.BUS_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_sku.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_vend.DataBindings.Add("Text", DataSource, "ITM.VE_CD")
        xr_des.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_slsp2_hidden.DataBindings.Add("Text", DataSource, "SO.SO_EMP_SLSP_CD2")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")
        xr_store_addr.DataBindings.Add("Text", DataSource, "STORE.STORE_ADDRESS")
        xr_store_phone.DataBindings.Add("Text", DataSource, "STORE.STORE_PHONE")
        xr_store_cd.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc_cd.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "Pick-Up"
            Case "D"
                sender.text = "Delivery"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax_pst.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint

        If Not IsNumeric(sender.text) Then
            sender.Text = "0.00"
        End If
        If Not IsNumeric(xr_setup.Text) Then xr_setup.Text = "0.00"
        sender.Text = FormatNumber(CDbl(sender.Text) + CDbl(xr_setup.Text), 2)

    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint

        sender.text = FormatNumber(xr_qty.Text * xr_ret.Text, 2)
        subtotal = subtotal + (xr_qty.Text * xr_ret.Text)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint, xr_payment_holder.BeforePrint

        'If processed_payments = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        'sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        sql = "select sum(nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0))AS PMT_AMT from ar_trn a, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                    xrTable1.Visible = False
                End If
            Else
                sender.text = FormatNumber(0, 2)
                xrTable1.Visible = False
            End If
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint

        Dim grand_total As Double = 0
        Dim payments As Double = 0
        Dim finance As Double = 0
        If IsNumeric(xr_grand_total.Text) Then grand_total = CDbl(xr_grand_total.Text)
        If IsNumeric(xr_payments.Text) Then payments = CDbl(xr_payments.Text)
        If IsNumeric(xr_finance.Text) Then finance = CDbl(xr_finance.Text)

        xr_balance.Text = FormatNumber(grand_total - (payments + finance), 2)

    End Sub

    Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_disc_desc.BeforePrint

        If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
            If CDbl(xr_disc_amt.Text) > 0 Then
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
                            sender.visible = True
                            sender.text = MyDataReader2.Item("DES").ToString & " ( DISCOUNTED " & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
                        Else
                            sender.visible = False
                            sender.text = ""
                        End If
                    Else
                        sender.text = ""
                    End If
                Catch
                    Throw
                End Try
            Else
                sender.text = ""
            End If
        Else
            sender.text = ""
        End If

    End Sub

    Private Sub xr_mop_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop.BeforePrint

        'If mop_processed = False Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String
        Dim merchant_id As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select nvl(DECODE(c.DC_CD,'D',-a.AMT,'C',a.AMT,0),0) AS PMT_AMT, TO_CHAR(POST_DT,'MM-DD-YY') AS POST_DT, b.DES, a.DES as MID, c.DES as trn_des, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM from ar_trn a, mop b, ar_trn_tp c "
        sql = sql & "where  a.trn_tp_cd = c.trn_tp_cd(+) "
        sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If Not String.IsNullOrEmpty(MyDataReader2.Item("REF_NUM").ToString) Then
                    sender.text = sender.text & MyDataReader2.Item("REF_NUM").ToString & Constants.vbCrLf
                ElseIf MyDataReader2.Item("DES").ToString & "" <> "" Then
                    sender.text = sender.text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                Else
                    sender.text = sender.text & MyDataReader2.Item("TRN_DES").ToString & Constants.vbCrLf
                End If
                If String.IsNullOrEmpty(MyDataReader2.Item("HOST_REF_NUM").ToString) Then
                    xr_ref.Text = xr_ref.Text & Constants.vbCrLf
                Else
                    xr_ref.Text = xr_ref.Text & MyDataReader2.Item("HOST_REF_NUM").ToString & Constants.vbCrLf
                End If
                If String.IsNullOrEmpty(MyDataReader2.Item("POST_DT").ToString) Then
                    xr_pmt_dt.Text = xr_pmt_dt.Text & Constants.vbCrLf
                Else
                    xr_pmt_dt.Text = xr_pmt_dt.Text & MyDataReader2.Item("POST_DT").ToString & Constants.vbCrLf
                End If
                xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
                If Len(MyDataReader2.Item("BNK_CRD_NUM").ToString) = 19 Then
                    xr_card_no.Text = xr_card_no.Text & Microsoft.VisualBasic.Strings.Right(MyDataReader2.Item("BNK_CRD_NUM").ToString, 16) & Constants.vbCrLf
                ElseIf MyDataReader2.Item("CHK_NUM").ToString & "" <> "" Then
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("CHK_NUM").ToString & Constants.vbCrLf
                Else
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                End If
                If InStr(MyDataReader2.Item("MID").ToString, "MID:") > 0 Then
                    merchant_id = Replace(MyDataReader2.Item("MID").ToString, "MID:", "")
                    If IsNumeric(merchant_id) Then merchant_id = CDbl(merchant_id)
                    xr_merchant.Text = xr_merchant.Text & merchant_id & Constants.vbCrLf
                Else
                    xr_merchant.Text = xr_merchant.Text & Constants.vbCrLf
                End If
                'xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                If Not String.IsNullOrEmpty(MyDataReader2.Item("EXP_DT").ToString) Then
                    xr_exp_dt.Text = xr_exp_dt.Text & "xx/xx" & Constants.vbCrLf
                Else
                    xr_exp_dt.Text = xr_exp_dt.Text & Constants.vbCrLf
                End If
                xr_appr_cd.Text = xr_appr_cd.Text & MyDataReader2.Item("APP_CD").ToString & Constants.vbCrLf
            Loop
        Catch
            Throw
        End Try
        'processed_payments = True
        'End If

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='S' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString
                Loop
            Catch
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Private Sub xr_triggers_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_triggers.BeforePrint

        If triggers_processed <> True Then
            Dim conn2 As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            If xr_ord_srt.Text & "" <> "" Then
                sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                sql = sql & "VALUES('" & xr_del_doc_num.Text & "','SORT_CODE','" & xr_ord_srt.Text & "')"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
            End If

            If xr_pd.Text & "" <> "" Then
                sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                sql = sql & "VALUES('" & xr_del_doc_num.Text & "','P_D','" & Microsoft.VisualBasic.Strings.Left(xr_pd.Text, 1) & "')"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
            End If

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','STORE_CD','" & Microsoft.VisualBasic.Strings.Mid(xr_del_doc_num.Text, 6, 2) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "SELECT c.ITM_TP_CD, b.MJR_CD, c.MNR_CD, a.ITM_CD, c.DROP_DT FROM SO_LN a, INV_MNR b, ITM c "
            sql = sql & "WHERE a.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND c.MNR_CD=b.MNR_CD "
            sql = sql & "AND a.ITM_CD=c.ITM_CD"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_TP_CD','" & MyDataReader2.Item("ITM_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MAJOR_CD','" & MyDataReader2.Item("MJR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MINOR_CD','" & MyDataReader2.Item("MNR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_CD','" & MyDataReader2.Item("ITM_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DROP_DT").ToString) Then
                        sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                        sql = sql & "VALUES('" & xr_del_doc_num.Text & "','DROP_DT','" & MyDataReader2.Item("DROP_DT").ToString & "')"

                        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql2.ExecuteNonQuery()
                    End If
                Loop
            Catch
                Throw
            End Try

            sql = "select trigger_response, TRIGGER_ID "
            sql = sql & "from invoice_triggers, invoice_triggers_temp "
            sql = sql & "where invoice_triggers.trigger_field = invoice_triggers_temp.trigger_field "
            sql = sql & "and invoice_triggers.trigger_value=invoice_triggers_temp.trigger_value "
            sql = sql & "and del_doc_num='" & xr_del_doc_num.Text & "' "
            sql = sql & "GROUP BY TRIGGER_ID, trigger_response"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_triggers.Text = xr_triggers.Text & MyDataReader2.Item("TRIGGER_RESPONSE").ToString & Constants.vbCrLf
                Loop
            Catch
                Throw
            End Try

            If Not String.IsNullOrEmpty(xr_triggers.Text) Then
                xr_triggers.Text = xr_triggers.Text & Constants.vbCrLf
            End If

            sql = "DELETE FROM INVOICE_TRIGGERS_TEMP WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            triggers_processed = True
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES INVOICE"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT"
        End Select

    End Sub

    Private Sub xr_store_addr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr.BeforePrint

        xr_store_addr.Text = StrConv(xr_store_addr.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = StrConv(xr_store_name.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_slsp2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp2.BeforePrint

        'If xr_slsp2_hidden.Text & "" <> "" Then
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "SELECT EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME "
        sql = sql & "FROM SO, EMP WHERE SO.DEL_DOC_NUM='" & HttpContext.Current.Request("DEL_DOC_NUM") & "' "
        sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD2  "

        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                sender.text = MyDataReader2.Item("FULL_NAME").ToString
            Loop
        Catch
            Throw
        End Try
        'End If

    End Sub

    Private Sub xr_sale_last_four_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_sale_last_four.BeforePrint

        If Len(xr_del_doc_num.Text) = 11 Then
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 4)
        Else
            sender.text = Microsoft.VisualBasic.Right(xr_del_doc_num.Text, 5)
        End If

    End Sub

    Private Sub xr_tax_cd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax_cd.BeforePrint, xr_tax_pst.BeforePrint, xr_tax_gst.BeforePrint

        If tax_processed <> True Then
            xr_tax_gst.Text = "0"
            xr_tax_pst.Text = "0"
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "SELECT TAX_CD$TAT.TAT_CD, TAT.PCT "
            sql = sql & "FROM TAT, TAX_CD$TAT "
            sql = sql & "WHERE TAT.EFF_DT <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
            sql = sql & "AND  TAX_CD$TAT.TAX_CD='" & xr_tax_cd.Text & "' "
            sql = sql & "ORDER BY TAX_CD$TAT.TAT_CD DESC"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    If MyDataReader2.Item("TAT_CD").ToString = "PST" Then
                        If IsNumeric(MyDataReader2.Item("PCT").ToString) Then
                            pst = CDbl(MyDataReader2.Item("PCT").ToString) / 100
                        End If
                    Else
                        gst_cd = MyDataReader2.Item("TAT_CD").ToString
                        If IsNumeric(MyDataReader2.Item("PCT").ToString) Then
                            gst = CDbl(MyDataReader2.Item("PCT").ToString) / 100
                        End If
                    End If
                Loop
                MyDataReader2.Close()
                conn.Close()
                determine_tax()
            Catch
                conn.Close()
                Throw
            End Try
            xr_tax_pst.Text = FormatNumber(CDbl(xr_tax_pst.Text), 2)
            xr_tax_gst.Text = FormatNumber(CDbl(xr_tax_gst.Text), 2)
            xr_PST.Text = xr_tax_pst.Text
            xr_GST.Text = xr_tax_gst.Text
            tax_processed = True
        End If

    End Sub

    Public Function Determine_Taxability(ByVal itm_tp_cd As String, ByVal tat_cd As String)

        Determine_Taxability = "N"

        Dim sql As String
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As System.Data.OracleClient.OracleCommand
        Dim MyDataReader As System.Data.OracleClient.OracleDataReader
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If itm_tp_cd = "PKG" Or itm_tp_cd = "NLN" Then
            Determine_Taxability = "N"
            Exit Function
        End If

        If itm_tp_cd = "INV" Then
            Determine_Taxability = "Y"
            Exit Function
        End If

        'Open Connection 
        conn.Open()

        sql = "select inventory from itm_tp where itm_tp_cd='" & itm_tp_cd & "' and inventory='Y'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                MyDataReader.Close()
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try

        Dim TAT_QUERY As String = ""
        Select Case itm_tp_cd
            Case "LAB"
                TAT_QUERY = "LAB_TAX"
            Case "FAB"
                TAT_QUERY = "FAB_TAX"
            Case "NLT"
                TAT_QUERY = "NLT_TAX"
            Case "WAR"
                TAT_QUERY = "WAR_TAX"
            Case "PAR"
                TAT_QUERY = "PARTS_TAX"
            Case "MIL"
                TAT_QUERY = "MILEAGE_TAX"
            Case Else
                Determine_Taxability = "N"
                conn.Close()
                Exit Function
        End Select

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAT_CD='" & tat_cd & "' "
        sql = sql & "AND TAT." & TAT_QUERY & " = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                MyDataReader.Close()
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Sub determine_tax()

        Dim SQL As String = ""
        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As System.Data.OracleClient.OracleCommand
        Dim MyDataReader As System.Data.OracleClient.OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        SQL = "SELECT SO_LN.QTY, SO_LN.UNIT_PRC, ITM.ITM_TP_CD FROM SO_LN, ITM "
        SQL = SQL & "WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND SO_LN.VOID_FLAG='N' AND SO_LN.ITM_CD=ITM.ITM_CD "

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                If Determine_Taxability(MyDataReader.Item("ITM_TP_CD").ToString, pst_cd) = "Y" Then
                    If IsNumeric(MyDataReader.Item("QTY").ToString) And IsNumeric(MyDataReader.Item("UNIT_PRC").ToString) Then
                        xr_tax_pst.Text = xr_tax_pst.Text + ((CDbl(MyDataReader.Item("QTY").ToString) * CDbl(MyDataReader.Item("UNIT_PRC").ToString)) * pst)
                    End If
                End If
                If Determine_Taxability(MyDataReader.Item("ITM_TP_CD").ToString, gst_cd) = "Y" Then
                    If IsNumeric(MyDataReader.Item("QTY").ToString) And IsNumeric(MyDataReader.Item("UNIT_PRC").ToString) Then
                        xr_tax_gst.Text = xr_tax_gst.Text + ((CDbl(MyDataReader.Item("QTY").ToString) * CDbl(MyDataReader.Item("UNIT_PRC").ToString)) * gst)
                    End If
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        SQL = "SELECT TAX_CD$TAT.TAT_CD, TAT.PCT "
        SQL = SQL & "FROM TAT, TAX_CD$TAT "
        SQL = SQL & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        SQL = SQL & "AND TAX_CD$TAT.TAX_CD='" & xr_tax_cd.Text & "' "
        SQL = SQL & "AND TAT.DEL_TAX = 'Y' "

        'Determine the taxability of the setup and delivery charges
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                If MyDataReader.Item("TAT_CD").ToString = "PST" Then
                    If IsNumeric(MyDataReader.Item("PCT").ToString) And IsNumeric(xr_del.Text) Then
                        xr_tax_pst.Text = xr_tax_pst.Text + (CDbl(xr_del.Text) * pst)
                    End If
                Else
                    If IsNumeric(MyDataReader.Item("PCT").ToString) And IsNumeric(xr_del.Text) Then
                        xr_tax_gst.Text = xr_tax_gst.Text + (CDbl(xr_del.Text) * gst)
                    End If
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub

    Private Sub xr_finance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_finance.BeforePrint

        If Not IsNumeric(xr_finance.Text) Then xr_finance.Text = "0.00"

    End Sub

    Private Sub xr_rem_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_rem.BeforePrint

        Dim balance As Double = 0
        Dim finance As Double = 0
        If IsNumeric(xr_balance.Text) Then balance = CDbl(xr_balance.Text)
        If IsNumeric(xr_finance.Text) Then finance = CDbl(xr_finance.Text)

        xr_rem.Text = FormatNumber(balance + finance, 2)

    End Sub

    Private Sub xr_bill_zip_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_bill_zip.BeforePrint

        sender.text = UCase(sender.text)

    End Sub

    Private Sub xr_zip_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_zip.BeforePrint

        sender.Text = UCase(sender.text)

    End Sub
End Class