Imports System.IO

Public Class HBCGInvoice
    Inherits DevExpress.XtraReports.UI.XtraReport

    Dim subtotal As Double
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_card_no As DevExpress.XtraReports.UI.XRLabel
    Dim comments_processed As Boolean
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents xr_full_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine5 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine6 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine8 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xr_exp_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_appr_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pmt_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_merchant As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ref As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xr_ord_tp_hidden As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ord_tp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_name As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_addr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Private WithEvents xr_sig_pic As DevExpress.XtraReports.UI.XRPictureBox
    Dim triggers_processed As Boolean

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xr_del_doc_num As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_so_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cust_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pu_del_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_pd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ext As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_ret As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_desc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_loc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_SKU As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_vsn As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qty As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine4 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_subtotal As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_del As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tax As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_grand_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_payments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_balance As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_void_flag As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_financed As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_comments As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine7 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_mop As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_triggers As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_desc As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "HBCGInvoice.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.HBCGInvoice.ResourceManager
        Dim xrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_SKU = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_disc_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_disc_amt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_void_flag = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ext = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ret = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_desc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_loc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_vsn = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_qty = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox
        Me.xr_store_addr = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store_name = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.xr_store_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xr_disc_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pu_del_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_cust_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del_doc_num = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_so_wr_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_comments = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xr_sig_pic = New DevExpress.XtraReports.UI.XRPictureBox
        Me.xrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak
        Me.xr_ref = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_merchant = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_pmt_amt = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_appr_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_exp_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox
        Me.xrLabel30 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine8 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine7 = New DevExpress.XtraReports.UI.XRLine
        Me.xr_triggers = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_card_no = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_mop = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel20 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel22 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel23 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel24 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel25 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_financed = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_payments = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_grand_total = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_subtotal = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_del = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tax = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine6 = New DevExpress.XtraReports.UI.XRLine
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.xrLabel26 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel28 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.xr_ord_tp = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_ord_tp_hidden = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_full_name = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xrLabel29 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_SKU, Me.xr_disc_desc, Me.xr_disc_amt, Me.xr_void_flag, Me.xr_ext, Me.xr_ret, Me.xr_desc, Me.xr_loc, Me.xr_store, Me.xr_vsn, Me.xr_qty})
        Me.Detail.Height = 35
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_SKU
        '
        Me.xr_SKU.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_SKU.Location = New System.Drawing.Point(8, 0)
        Me.xr_SKU.Name = "xr_SKU"
        Me.xr_SKU.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_SKU.Size = New System.Drawing.Size(117, 17)
        Me.xr_SKU.StylePriority.UseFont = False
        Me.xr_SKU.Text = "SKU"
        '
        'xr_disc_desc
        '
        Me.xr_disc_desc.CanShrink = True
        Me.xr_disc_desc.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_desc.ForeColor = System.Drawing.Color.Red
        Me.xr_disc_desc.Location = New System.Drawing.Point(417, 17)
        Me.xr_disc_desc.Name = "xr_disc_desc"
        Me.xr_disc_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_desc.Size = New System.Drawing.Size(367, 17)
        Me.xr_disc_desc.StylePriority.UseFont = False
        Me.xr_disc_desc.StylePriority.UseForeColor = False
        Me.xr_disc_desc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_amt
        '
        Me.xr_disc_amt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_amt.Location = New System.Drawing.Point(33, 25)
        Me.xr_disc_amt.Name = "xr_disc_amt"
        Me.xr_disc_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_amt.Size = New System.Drawing.Size(8, 8)
        Me.xr_disc_amt.StylePriority.UseFont = False
        Me.xr_disc_amt.Visible = False
        '
        'xr_void_flag
        '
        Me.xr_void_flag.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_void_flag.Location = New System.Drawing.Point(17, 25)
        Me.xr_void_flag.Name = "xr_void_flag"
        Me.xr_void_flag.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_void_flag.Size = New System.Drawing.Size(2, 2)
        Me.xr_void_flag.StylePriority.UseFont = False
        Me.xr_void_flag.Visible = False
        '
        'xr_ext
        '
        Me.xr_ext.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ext.Location = New System.Drawing.Point(708, 0)
        Me.xr_ext.Name = "xr_ext"
        Me.xr_ext.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ext.Size = New System.Drawing.Size(83, 17)
        Me.xr_ext.StylePriority.UseFont = False
        Me.xr_ext.Text = "EXTENDED"
        Me.xr_ext.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ret
        '
        Me.xr_ret.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ret.Location = New System.Drawing.Point(617, 0)
        Me.xr_ret.Name = "xr_ret"
        Me.xr_ret.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ret.Size = New System.Drawing.Size(83, 17)
        Me.xr_ret.StylePriority.UseFont = False
        Me.xr_ret.Text = "RETAIL"
        Me.xr_ret.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_desc
        '
        Me.xr_desc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_desc.Location = New System.Drawing.Point(200, 17)
        Me.xr_desc.Name = "xr_desc"
        Me.xr_desc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_desc.Size = New System.Drawing.Size(208, 17)
        Me.xr_desc.StylePriority.UseFont = False
        Me.xr_desc.Text = "DESCRIPTION"
        '
        'xr_loc
        '
        Me.xr_loc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_loc.Location = New System.Drawing.Point(158, 0)
        Me.xr_loc.Name = "xr_loc"
        Me.xr_loc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_loc.Size = New System.Drawing.Size(33, 17)
        Me.xr_loc.StylePriority.UseFont = False
        Me.xr_loc.Text = "LOC"
        '
        'xr_store
        '
        Me.xr_store.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store.Location = New System.Drawing.Point(125, 0)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.Size = New System.Drawing.Size(25, 17)
        Me.xr_store.StylePriority.UseFont = False
        Me.xr_store.Text = "ST"
        '
        'xr_vsn
        '
        Me.xr_vsn.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_vsn.Location = New System.Drawing.Point(200, 0)
        Me.xr_vsn.Name = "xr_vsn"
        Me.xr_vsn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_vsn.Size = New System.Drawing.Size(375, 17)
        Me.xr_vsn.StylePriority.UseFont = False
        Me.xr_vsn.Text = "VSN"
        '
        'xr_qty
        '
        Me.xr_qty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qty.Location = New System.Drawing.Point(583, 0)
        Me.xr_qty.Name = "xr_qty"
        Me.xr_qty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qty.Size = New System.Drawing.Size(33, 17)
        Me.xr_qty.StylePriority.UseFont = False
        Me.xr_qty.Text = "QTY"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPictureBox1, Me.xr_store_addr, Me.xr_store_name, Me.xrLine2, Me.xr_store_phone})
        Me.PageHeader.Height = 59
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.Image = CType(resources.GetObject("xrPictureBox1.Image"), System.Drawing.Image)
        Me.xrPictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.Size = New System.Drawing.Size(92, 43)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'xr_store_addr
        '
        Me.xr_store_addr.Font = New System.Drawing.Font("Calibri", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_addr.ForeColor = System.Drawing.Color.Black
        Me.xr_store_addr.Location = New System.Drawing.Point(283, 17)
        Me.xr_store_addr.Name = "xr_store_addr"
        Me.xr_store_addr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_addr.Size = New System.Drawing.Size(517, 17)
        Me.xr_store_addr.StylePriority.UseFont = False
        Me.xr_store_addr.StylePriority.UseForeColor = False
        Me.xr_store_addr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_store_name
        '
        Me.xr_store_name.Font = New System.Drawing.Font("Calibri", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_name.ForeColor = System.Drawing.Color.Black
        Me.xr_store_name.Location = New System.Drawing.Point(283, 0)
        Me.xr_store_name.Name = "xr_store_name"
        Me.xr_store_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_name.Size = New System.Drawing.Size(517, 17)
        Me.xr_store_name.StylePriority.UseFont = False
        Me.xr_store_name.StylePriority.UseForeColor = False
        Me.xr_store_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine2
        '
        Me.xrLine2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine2.LineWidth = 2
        Me.xrLine2.Location = New System.Drawing.Point(0, 51)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.Size = New System.Drawing.Size(800, 8)
        Me.xrLine2.StylePriority.UseForeColor = False
        '
        'xr_store_phone
        '
        Me.xr_store_phone.Font = New System.Drawing.Font("Calibri", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_phone.ForeColor = System.Drawing.Color.Black
        Me.xr_store_phone.Location = New System.Drawing.Point(562, 34)
        Me.xr_store_phone.Name = "xr_store_phone"
        Me.xr_store_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_phone.Size = New System.Drawing.Size(238, 17)
        Me.xr_store_phone.StylePriority.UseFont = False
        Me.xr_store_phone.StylePriority.UseForeColor = False
        Me.xr_store_phone.StylePriority.UseTextAlignment = False
        Me.xr_store_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray
        Me.xrPageInfo2.Format = "Page {0} of {1}"
        Me.xrPageInfo2.Location = New System.Drawing.Point(642, 0)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.Size = New System.Drawing.Size(154, 17)
        Me.xrPageInfo2.StylePriority.UseFont = False
        Me.xrPageInfo2.StylePriority.UseForeColor = False
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_cd
        '
        Me.xr_disc_cd.Location = New System.Drawing.Point(475, 33)
        Me.xr_disc_cd.Name = "xr_disc_cd"
        Me.xr_disc_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_cd.Size = New System.Drawing.Size(2, 2)
        Me.xr_disc_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xr_disc_cd.Visible = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel1.Location = New System.Drawing.Point(0, 0)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseForeColor = False
        Me.xrLabel1.Text = "CUSTOMER:"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine4
        '
        Me.xrLine4.Location = New System.Drawing.Point(0, 117)
        Me.xrLine4.Name = "xrLine4"
        Me.xrLine4.Size = New System.Drawing.Size(800, 8)
        '
        'xrLine3
        '
        Me.xrLine3.Location = New System.Drawing.Point(0, 91)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.Size = New System.Drawing.Size(800, 8)
        '
        'xrLabel18
        '
        Me.xrLabel18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel18.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel18.Location = New System.Drawing.Point(155, 100)
        Me.xrLabel18.Name = "xrLabel18"
        Me.xrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel18.Size = New System.Drawing.Size(33, 17)
        Me.xrLabel18.StylePriority.UseFont = False
        Me.xrLabel18.StylePriority.UseForeColor = False
        Me.xrLabel18.Text = "LOC"
        '
        'xrLabel17
        '
        Me.xrLabel17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel17.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel17.Location = New System.Drawing.Point(129, 100)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.Size = New System.Drawing.Size(25, 17)
        Me.xrLabel17.StylePriority.UseFont = False
        Me.xrLabel17.StylePriority.UseForeColor = False
        Me.xrLabel17.Text = "ST"
        '
        'xrLabel16
        '
        Me.xrLabel16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel16.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel16.Location = New System.Drawing.Point(704, 100)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.Size = New System.Drawing.Size(83, 17)
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseForeColor = False
        Me.xrLabel16.Text = "TOTAL"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel15
        '
        Me.xrLabel15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel15.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel15.Location = New System.Drawing.Point(617, 100)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.Size = New System.Drawing.Size(83, 17)
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.StylePriority.UseForeColor = False
        Me.xrLabel15.Text = "PRICE EACH"
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel14
        '
        Me.xrLabel14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel14.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel14.Location = New System.Drawing.Point(42, 100)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.Size = New System.Drawing.Size(58, 17)
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseForeColor = False
        Me.xrLabel14.Text = "SKU"
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel13.Location = New System.Drawing.Point(317, 100)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseForeColor = False
        Me.xrLabel13.Text = "DESCRIPTION"
        '
        'xrLabel12
        '
        Me.xrLabel12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel12.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel12.Location = New System.Drawing.Point(583, 100)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.Size = New System.Drawing.Size(33, 17)
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseForeColor = False
        Me.xrLabel12.Text = "QTY"
        '
        'xr_slsp
        '
        Me.xr_slsp.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_slsp.Location = New System.Drawing.Point(625, 74)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.Size = New System.Drawing.Size(175, 17)
        Me.xr_slsp.StylePriority.UseFont = False
        Me.xr_slsp.StylePriority.UseTextAlignment = False
        Me.xr_slsp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel7.Location = New System.Drawing.Point(500, 74)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.Size = New System.Drawing.Size(117, 17)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseForeColor = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "SALES ASSOCIATE:"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pd
        '
        Me.xr_pd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pd.ForeColor = System.Drawing.Color.DimGray
        Me.xr_pd.Location = New System.Drawing.Point(592, 51)
        Me.xr_pd.Name = "xr_pd"
        Me.xr_pd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pd.Size = New System.Drawing.Size(100, 17)
        Me.xr_pd.StylePriority.UseFont = False
        Me.xr_pd.StylePriority.UseForeColor = False
        Me.xr_pd.Text = "DELIVERY DATE:"
        Me.xr_pd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_pu_del_dt
        '
        Me.xr_pu_del_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_pu_del_dt.Location = New System.Drawing.Point(692, 51)
        Me.xr_pu_del_dt.Name = "xr_pu_del_dt"
        Me.xr_pu_del_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pu_del_dt.Size = New System.Drawing.Size(100, 17)
        Me.xr_pu_del_dt.StylePriority.UseFont = False
        Me.xr_pu_del_dt.Text = "xr_pu_del_dt"
        Me.xr_pu_del_dt.WordWrap = False
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel6.Location = New System.Drawing.Point(592, 34)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.Size = New System.Drawing.Size(100, 16)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseForeColor = False
        Me.xrLabel6.Text = "SALES DATE:"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_cust_cd
        '
        Me.xr_cust_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cust_cd.Location = New System.Drawing.Point(692, 17)
        Me.xr_cust_cd.Name = "xr_cust_cd"
        Me.xr_cust_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cust_cd.Size = New System.Drawing.Size(100, 17)
        Me.xr_cust_cd.StylePriority.UseFont = False
        Me.xr_cust_cd.Text = "xr_cust_cd"
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel5.Location = New System.Drawing.Point(592, 17)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.StylePriority.UseForeColor = False
        Me.xrLabel5.Text = "ACCOUNT #:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel2.Location = New System.Drawing.Point(0, 52)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.Size = New System.Drawing.Size(74, 16)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.StylePriority.UseForeColor = False
        Me.xrLabel2.Text = "HOME TEL:"
        Me.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_b_phone.Location = New System.Drawing.Point(75, 68)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.Size = New System.Drawing.Size(107, 17)
        Me.xr_b_phone.StylePriority.UseFont = False
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel3.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel3.Location = New System.Drawing.Point(0, 68)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.Size = New System.Drawing.Size(75, 16)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.StylePriority.UseForeColor = False
        Me.xrLabel3.Text = "WORK TEL:"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_h_phone.Location = New System.Drawing.Point(75, 51)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.Size = New System.Drawing.Size(108, 17)
        Me.xr_h_phone.StylePriority.UseFont = False
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_zip.Location = New System.Drawing.Point(289, 34)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.Size = New System.Drawing.Size(67, 17)
        Me.xr_zip.StylePriority.UseFont = False
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.CanGrow = False
        Me.xr_state.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_state.Location = New System.Drawing.Point(258, 34)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.Size = New System.Drawing.Size(25, 17)
        Me.xr_state.StylePriority.UseFont = False
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_city.Location = New System.Drawing.Point(75, 34)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.Size = New System.Drawing.Size(167, 17)
        Me.xr_city.StylePriority.UseFont = False
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr2.Location = New System.Drawing.Point(284, 17)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.Size = New System.Drawing.Size(92, 17)
        Me.xr_addr2.StylePriority.UseFont = False
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_del_doc_num
        '
        Me.xr_del_doc_num.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del_doc_num.Location = New System.Drawing.Point(692, 0)
        Me.xr_del_doc_num.Name = "xr_del_doc_num"
        Me.xr_del_doc_num.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del_doc_num.Size = New System.Drawing.Size(100, 17)
        Me.xr_del_doc_num.StylePriority.UseFont = False
        Me.xr_del_doc_num.Text = "xr_del_doc_num"
        '
        'xr_lname
        '
        Me.xr_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_lname.Location = New System.Drawing.Point(458, 33)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.Size = New System.Drawing.Size(8, 8)
        Me.xr_lname.StylePriority.UseFont = False
        Me.xr_lname.Text = "xr_lname"
        Me.xr_lname.Visible = False
        '
        'xr_fname
        '
        Me.xr_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_fname.Location = New System.Drawing.Point(442, 33)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.Size = New System.Drawing.Size(8, 8)
        Me.xr_fname.StylePriority.UseFont = False
        Me.xr_fname.Text = "xr_fname"
        Me.xr_fname.Visible = False
        '
        'xr_so_wr_dt
        '
        Me.xr_so_wr_dt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_so_wr_dt.Location = New System.Drawing.Point(692, 34)
        Me.xr_so_wr_dt.Name = "xr_so_wr_dt"
        Me.xr_so_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_so_wr_dt.Size = New System.Drawing.Size(100, 17)
        Me.xr_so_wr_dt.StylePriority.UseFont = False
        Me.xr_so_wr_dt.WordWrap = False
        '
        'xr_addr1
        '
        Me.xr_addr1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_addr1.Location = New System.Drawing.Point(75, 17)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.Size = New System.Drawing.Size(208, 17)
        Me.xr_addr1.StylePriority.UseFont = False
        Me.xr_addr1.Text = "xr_addr1"
        '
        'xr_comments
        '
        Me.xr_comments.CanShrink = True
        Me.xr_comments.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_comments.Location = New System.Drawing.Point(8, 25)
        Me.xr_comments.Multiline = True
        Me.xr_comments.Name = "xr_comments"
        Me.xr_comments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_comments.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.xr_comments.Size = New System.Drawing.Size(530, 17)
        Me.xr_comments.StylePriority.UseFont = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_sig_pic, Me.xrPageBreak1, Me.xr_ref, Me.xr_merchant, Me.xr_pmt_amt, Me.xr_appr_cd, Me.xr_exp_dt, Me.xrPictureBox2, Me.xrLabel30, Me.xrLine8, Me.xrLabel9, Me.xrLine1, Me.xrLine7, Me.xr_triggers, Me.xrLabel27, Me.xrTable1, Me.xrLabel11, Me.xr_card_no, Me.xr_mop, Me.xr_comments, Me.xrLabel20, Me.xrLabel22, Me.xrLabel23, Me.xrLabel24, Me.xrLabel8, Me.xrLabel25, Me.xr_balance, Me.xr_financed, Me.xr_payments, Me.xr_grand_total, Me.xrLabel19, Me.xr_subtotal, Me.xr_del, Me.xrLabel10, Me.xr_tax, Me.xrLine5, Me.xrLine6})
        Me.ReportFooter.Height = 500
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_sig_pic
        '
        Me.xr_sig_pic.Location = New System.Drawing.Point(142, 250)
        Me.xr_sig_pic.Name = "xr_sig_pic"
        Me.xr_sig_pic.Size = New System.Drawing.Size(170, 42)
        Me.xr_sig_pic.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'xrPageBreak1
        '
        Me.xrPageBreak1.Location = New System.Drawing.Point(0, 308)
        Me.xrPageBreak1.Name = "xrPageBreak1"
        '
        'xr_ref
        '
        Me.xr_ref.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_ref.Location = New System.Drawing.Point(683, 185)
        Me.xr_ref.Multiline = True
        Me.xr_ref.Name = "xr_ref"
        Me.xr_ref.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ref.Size = New System.Drawing.Size(66, 17)
        Me.xr_ref.StylePriority.UseFont = False
        Me.xr_ref.StylePriority.UseTextAlignment = False
        Me.xr_ref.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_merchant
        '
        Me.xr_merchant.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_merchant.Location = New System.Drawing.Point(542, 185)
        Me.xr_merchant.Multiline = True
        Me.xr_merchant.Name = "xr_merchant"
        Me.xr_merchant.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_merchant.Size = New System.Drawing.Size(141, 17)
        Me.xr_merchant.StylePriority.UseFont = False
        Me.xr_merchant.StylePriority.UseTextAlignment = False
        Me.xr_merchant.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_pmt_amt
        '
        Me.xr_pmt_amt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_pmt_amt.Location = New System.Drawing.Point(434, 185)
        Me.xr_pmt_amt.Multiline = True
        Me.xr_pmt_amt.Name = "xr_pmt_amt"
        Me.xr_pmt_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_pmt_amt.Size = New System.Drawing.Size(106, 17)
        Me.xr_pmt_amt.StylePriority.UseFont = False
        Me.xr_pmt_amt.StylePriority.UseTextAlignment = False
        Me.xr_pmt_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_appr_cd
        '
        Me.xr_appr_cd.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_appr_cd.Location = New System.Drawing.Point(342, 185)
        Me.xr_appr_cd.Multiline = True
        Me.xr_appr_cd.Name = "xr_appr_cd"
        Me.xr_appr_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_appr_cd.Size = New System.Drawing.Size(92, 17)
        Me.xr_appr_cd.StylePriority.UseFont = False
        Me.xr_appr_cd.StylePriority.UseTextAlignment = False
        Me.xr_appr_cd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_exp_dt
        '
        Me.xr_exp_dt.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_exp_dt.Location = New System.Drawing.Point(275, 185)
        Me.xr_exp_dt.Multiline = True
        Me.xr_exp_dt.Name = "xr_exp_dt"
        Me.xr_exp_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_exp_dt.Size = New System.Drawing.Size(66, 17)
        Me.xr_exp_dt.StylePriority.UseFont = False
        Me.xr_exp_dt.StylePriority.UseTextAlignment = False
        Me.xr_exp_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrPictureBox2
        '
        Me.xrPictureBox2.Image = CType(resources.GetObject("xrPictureBox2.Image"), System.Drawing.Image)
        Me.xrPictureBox2.Location = New System.Drawing.Point(0, 362)
        Me.xrPictureBox2.Name = "xrPictureBox2"
        Me.xrPictureBox2.Size = New System.Drawing.Size(800, 104)
        '
        'xrLabel30
        '
        Me.xrLabel30.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel30.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel30.Location = New System.Drawing.Point(0, 342)
        Me.xrLabel30.Multiline = True
        Me.xrLabel30.Name = "xrLabel30"
        Me.xrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel30.Size = New System.Drawing.Size(798, 20)
        Me.xrLabel30.StylePriority.UseFont = False
        Me.xrLabel30.StylePriority.UseForeColor = False
        Me.xrLabel30.Text = "Will My Furniture Fit?  We ask that you follow these guidelines to ensure complet" & _
            "e satisfaction:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLine8
        '
        Me.xrLine8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine8.LineWidth = 2
        Me.xrLine8.Location = New System.Drawing.Point(0, 475)
        Me.xrLine8.Name = "xrLine8"
        Me.xrLine8.Size = New System.Drawing.Size(800, 2)
        Me.xrLine8.StylePriority.UseForeColor = False
        '
        'xrLabel9
        '
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel9.Location = New System.Drawing.Point(0, 267)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.Size = New System.Drawing.Size(133, 17)
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseForeColor = False
        Me.xrLabel9.Text = "CUSTOMER SIGNATURE:"
        '
        'xrLine1
        '
        Me.xrLine1.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine1.Location = New System.Drawing.Point(133, 292)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.Size = New System.Drawing.Size(458, 9)
        Me.xrLine1.StylePriority.UseForeColor = False
        '
        'xrLine7
        '
        Me.xrLine7.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine7.LineWidth = 2
        Me.xrLine7.Location = New System.Drawing.Point(0, 0)
        Me.xrLine7.Name = "xrLine7"
        Me.xrLine7.Size = New System.Drawing.Size(800, 2)
        Me.xrLine7.StylePriority.UseForeColor = False
        '
        'xr_triggers
        '
        Me.xr_triggers.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_triggers.ForeColor = System.Drawing.Color.Gray
        Me.xr_triggers.Location = New System.Drawing.Point(0, 483)
        Me.xr_triggers.Multiline = True
        Me.xr_triggers.Name = "xr_triggers"
        Me.xr_triggers.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_triggers.Size = New System.Drawing.Size(797, 17)
        Me.xr_triggers.StylePriority.UseFont = False
        Me.xr_triggers.StylePriority.UseForeColor = False
        '
        'xrLabel27
        '
        Me.xrLabel27.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel27.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel27.Location = New System.Drawing.Point(0, 217)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel27.Size = New System.Drawing.Size(800, 33)
        Me.xrLabel27.StylePriority.UseFont = False
        Me.xrLabel27.StylePriority.UseForeColor = False
        Me.xrLabel27.StylePriority.UseTextAlignment = False
        Me.xrLabel27.Text = resources.GetString("xrLabel27.Text")
        Me.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrTable1
        '
        Me.xrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable1.Location = New System.Drawing.Point(0, 167)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow3})
        Me.xrTable1.Size = New System.Drawing.Size(750, 18)
        Me.xrTable1.StylePriority.UseBorders = False
        Me.xrTable1.StylePriority.UseFont = False
        '
        'xrTableRow3
        '
        Me.xrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell7, Me.xrTableCell10, Me.xrTableCell8, Me.xrTableCell9, Me.xrTableCell11, Me.xrTableCell12, Me.xrTableCell13})
        Me.xrTableRow3.Name = "xrTableRow3"
        Me.xrTableRow3.Weight = 1
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell7.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell7.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell7.StylePriority.UseBorders = False
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UseForeColor = False
        Me.xrTableCell7.StylePriority.UseTextAlignment = False
        Me.xrTableCell7.Text = "Payment Type"
        Me.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell7.Weight = 0.11066666666666666
        '
        'xrTableCell10
        '
        Me.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell10.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell10.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell10.Name = "xrTableCell10"
        Me.xrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell10.StylePriority.UseBorders = False
        Me.xrTableCell10.StylePriority.UseFont = False
        Me.xrTableCell10.StylePriority.UseForeColor = False
        Me.xrTableCell10.StylePriority.UseTextAlignment = False
        Me.xrTableCell10.Text = " Card Number"
        Me.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell10.Weight = 0.256
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell8.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell8.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell8.StylePriority.UseBorders = False
        Me.xrTableCell8.StylePriority.UseFont = False
        Me.xrTableCell8.StylePriority.UseForeColor = False
        Me.xrTableCell8.StylePriority.UseTextAlignment = False
        Me.xrTableCell8.Text = " Exp Dt"
        Me.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell8.Weight = 0.088
        '
        'xrTableCell9
        '
        Me.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell9.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell9.StylePriority.UseBorders = False
        Me.xrTableCell9.StylePriority.UseFont = False
        Me.xrTableCell9.StylePriority.UseForeColor = False
        Me.xrTableCell9.StylePriority.UseTextAlignment = False
        Me.xrTableCell9.Text = " Approval"
        Me.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell9.Weight = 0.124
        '
        'xrTableCell11
        '
        Me.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell11.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell11.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseBorders = False
        Me.xrTableCell11.StylePriority.UseFont = False
        Me.xrTableCell11.StylePriority.UseForeColor = False
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "Amount"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell11.Weight = 0.14266666666666666
        '
        'xrTableCell12
        '
        Me.xrTableCell12.BorderColor = System.Drawing.SystemColors.ControlText
        Me.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell12.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell12.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell12.StylePriority.UseBorderColor = False
        Me.xrTableCell12.StylePriority.UseBorders = False
        Me.xrTableCell12.StylePriority.UseFont = False
        Me.xrTableCell12.StylePriority.UseForeColor = False
        Me.xrTableCell12.StylePriority.UseTextAlignment = False
        Me.xrTableCell12.Text = "Merchant ID"
        Me.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell12.Weight = 0.18933333333333333
        '
        'xrTableCell13
        '
        Me.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTableCell13.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTableCell13.ForeColor = System.Drawing.Color.DimGray
        Me.xrTableCell13.Name = "xrTableCell13"
        Me.xrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell13.StylePriority.UseBorders = False
        Me.xrTableCell13.StylePriority.UseFont = False
        Me.xrTableCell13.StylePriority.UseForeColor = False
        Me.xrTableCell13.StylePriority.UseTextAlignment = False
        Me.xrTableCell13.Text = "Ref"
        Me.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrTableCell13.Weight = 0.089333333333333334
        '
        'xrLabel11
        '
        Me.xrLabel11.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel11.Location = New System.Drawing.Point(0, 325)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.Size = New System.Drawing.Size(800, 17)
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.Text = "TERMS AND CONDITIONS OF SALE:"
        Me.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_card_no
        '
        Me.xr_card_no.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_card_no.Location = New System.Drawing.Point(83, 185)
        Me.xr_card_no.Multiline = True
        Me.xr_card_no.Name = "xr_card_no"
        Me.xr_card_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_card_no.Size = New System.Drawing.Size(192, 17)
        Me.xr_card_no.StylePriority.UseFont = False
        Me.xr_card_no.StylePriority.UseTextAlignment = False
        Me.xr_card_no.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_mop
        '
        Me.xr_mop.Font = New System.Drawing.Font("Calibri", 8.25!)
        Me.xr_mop.Location = New System.Drawing.Point(0, 185)
        Me.xr_mop.Multiline = True
        Me.xr_mop.Name = "xr_mop"
        Me.xr_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_mop.Size = New System.Drawing.Size(83, 17)
        Me.xr_mop.StylePriority.UseFont = False
        Me.xr_mop.StylePriority.UseTextAlignment = False
        Me.xr_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLabel20
        '
        Me.xrLabel20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel20.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel20.Location = New System.Drawing.Point(550, 24)
        Me.xrLabel20.Name = "xrLabel20"
        Me.xrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel20.Size = New System.Drawing.Size(125, 17)
        Me.xrLabel20.StylePriority.UseFont = False
        Me.xrLabel20.StylePriority.UseForeColor = False
        Me.xrLabel20.Text = "DELIVERY CHARGE:"
        Me.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel22
        '
        Me.xrLabel22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel22.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel22.Location = New System.Drawing.Point(575, 42)
        Me.xrLabel22.Name = "xrLabel22"
        Me.xrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel22.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel22.StylePriority.UseFont = False
        Me.xrLabel22.StylePriority.UseForeColor = False
        Me.xrLabel22.Text = "SALES TAX:"
        Me.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel23
        '
        Me.xrLabel23.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel23.ForeColor = System.Drawing.Color.Black
        Me.xrLabel23.Location = New System.Drawing.Point(558, 59)
        Me.xrLabel23.Name = "xrLabel23"
        Me.xrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel23.Size = New System.Drawing.Size(117, 17)
        Me.xrLabel23.StylePriority.UseFont = False
        Me.xrLabel23.StylePriority.UseForeColor = False
        Me.xrLabel23.Text = "*TOTAL SALE*:"
        Me.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel24
        '
        Me.xrLabel24.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel24.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel24.Location = New System.Drawing.Point(575, 76)
        Me.xrLabel24.Name = "xrLabel24"
        Me.xrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel24.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel24.StylePriority.UseFont = False
        Me.xrLabel24.StylePriority.UseForeColor = False
        Me.xrLabel24.Text = "PAYMENTS:"
        Me.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel8.Location = New System.Drawing.Point(592, 94)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.Size = New System.Drawing.Size(83, 17)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseForeColor = False
        Me.xrLabel8.Text = "FINANCED:"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel25
        '
        Me.xrLabel25.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel25.ForeColor = System.Drawing.Color.Black
        Me.xrLabel25.Location = New System.Drawing.Point(550, 119)
        Me.xrLabel25.Name = "xrLabel25"
        Me.xrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel25.Size = New System.Drawing.Size(125, 17)
        Me.xrLabel25.StylePriority.UseFont = False
        Me.xrLabel25.StylePriority.UseForeColor = False
        Me.xrLabel25.Text = "**BALANCE** :"
        Me.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_balance
        '
        Me.xr_balance.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_balance.ForeColor = System.Drawing.Color.Black
        Me.xr_balance.Location = New System.Drawing.Point(683, 119)
        Me.xr_balance.Name = "xr_balance"
        Me.xr_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_balance.Size = New System.Drawing.Size(117, 17)
        Me.xr_balance.StylePriority.UseFont = False
        Me.xr_balance.StylePriority.UseForeColor = False
        Me.xr_balance.Text = "xr_balance"
        Me.xr_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_financed
        '
        Me.xr_financed.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.xr_financed.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_financed.ForeColor = System.Drawing.Color.DimGray
        Me.xr_financed.Location = New System.Drawing.Point(683, 94)
        Me.xr_financed.Name = "xr_financed"
        Me.xr_financed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_financed.Size = New System.Drawing.Size(117, 17)
        Me.xr_financed.StylePriority.UseBorders = False
        Me.xr_financed.StylePriority.UseFont = False
        Me.xr_financed.StylePriority.UseForeColor = False
        Me.xr_financed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_payments
        '
        Me.xr_payments.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_payments.ForeColor = System.Drawing.Color.DimGray
        Me.xr_payments.Location = New System.Drawing.Point(683, 76)
        Me.xr_payments.Name = "xr_payments"
        Me.xr_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_payments.Size = New System.Drawing.Size(117, 17)
        Me.xr_payments.StylePriority.UseFont = False
        Me.xr_payments.StylePriority.UseForeColor = False
        Me.xr_payments.Text = "xr_payments"
        Me.xr_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_grand_total
        '
        Me.xr_grand_total.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_grand_total.ForeColor = System.Drawing.Color.Black
        Me.xr_grand_total.Location = New System.Drawing.Point(683, 59)
        Me.xr_grand_total.Name = "xr_grand_total"
        Me.xr_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_grand_total.Size = New System.Drawing.Size(117, 17)
        Me.xr_grand_total.StylePriority.UseFont = False
        Me.xr_grand_total.StylePriority.UseForeColor = False
        Me.xr_grand_total.Text = "xr_grand_total"
        Me.xr_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel19
        '
        Me.xrLabel19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel19.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel19.Location = New System.Drawing.Point(575, 7)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel19.StylePriority.UseFont = False
        Me.xrLabel19.StylePriority.UseForeColor = False
        Me.xrLabel19.Text = "SUB-TOTAL:"
        Me.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_subtotal
        '
        Me.xr_subtotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_subtotal.ForeColor = System.Drawing.Color.DimGray
        Me.xr_subtotal.Location = New System.Drawing.Point(683, 7)
        Me.xr_subtotal.Name = "xr_subtotal"
        Me.xr_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_subtotal.Size = New System.Drawing.Size(117, 17)
        Me.xr_subtotal.StylePriority.UseFont = False
        Me.xr_subtotal.StylePriority.UseForeColor = False
        xrSummary1.FormatString = "{0:#.00}"
        xrSummary1.IgnoreNullValues = True
        Me.xr_subtotal.Summary = xrSummary1
        Me.xr_subtotal.Text = "xr_subtotal"
        Me.xr_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_del
        '
        Me.xr_del.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_del.ForeColor = System.Drawing.Color.DimGray
        Me.xr_del.Location = New System.Drawing.Point(683, 24)
        Me.xr_del.Name = "xr_del"
        Me.xr_del.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_del.Size = New System.Drawing.Size(117, 17)
        Me.xr_del.StylePriority.UseFont = False
        Me.xr_del.StylePriority.UseForeColor = False
        Me.xr_del.Text = "xr_del"
        Me.xr_del.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel10.Location = New System.Drawing.Point(8, 8)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.Size = New System.Drawing.Size(125, 17)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseForeColor = False
        Me.xrLabel10.Text = "COMMENTS:"
        '
        'xr_tax
        '
        Me.xr_tax.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tax.ForeColor = System.Drawing.Color.DimGray
        Me.xr_tax.Location = New System.Drawing.Point(683, 42)
        Me.xr_tax.Name = "xr_tax"
        Me.xr_tax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tax.Size = New System.Drawing.Size(117, 17)
        Me.xr_tax.StylePriority.UseFont = False
        Me.xr_tax.StylePriority.UseForeColor = False
        Me.xr_tax.Text = "xr_tax"
        Me.xr_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine5
        '
        Me.xrLine5.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine5.LineWidth = 2
        Me.xrLine5.Location = New System.Drawing.Point(0, 142)
        Me.xrLine5.Name = "xrLine5"
        Me.xrLine5.Size = New System.Drawing.Size(800, 2)
        Me.xrLine5.StylePriority.UseForeColor = False
        '
        'xrLine6
        '
        Me.xrLine6.ForeColor = System.Drawing.Color.DimGray
        Me.xrLine6.LineWidth = 2
        Me.xrLine6.Location = New System.Drawing.Point(0, 317)
        Me.xrLine6.Name = "xrLine6"
        Me.xrLine6.Size = New System.Drawing.Size(800, 2)
        Me.xrLine6.StylePriority.UseForeColor = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Height = 0
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 0
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell1.Weight = 0
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell2.Weight = 0
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell3.Weight = 0
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 0
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell4.Weight = 0
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell5.Weight = 0
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrTableCell6.Weight = 0
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'xrLabel26
        '
        Me.xrLabel26.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel26.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel26.Location = New System.Drawing.Point(0, 17)
        Me.xrLabel26.Name = "xrLabel26"
        Me.xrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel26.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel26.StylePriority.UseFont = False
        Me.xrLabel26.StylePriority.UseForeColor = False
        Me.xrLabel26.Text = "ADDRESS:"
        Me.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel28
        '
        Me.xrLabel28.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel28.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel28.Location = New System.Drawing.Point(0, 34)
        Me.xrLabel28.Name = "xrLabel28"
        Me.xrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel28.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel28.StylePriority.UseFont = False
        Me.xrLabel28.StylePriority.UseForeColor = False
        Me.xrLabel28.Text = "CITY/ST/ZIP:"
        Me.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_ord_tp, Me.xr_ord_tp_hidden, Me.xr_full_name, Me.xrLabel28, Me.xrLabel26, Me.xr_disc_cd, Me.xrLabel1, Me.xrLine4, Me.xrLine3, Me.xrLabel18, Me.xrLabel17, Me.xrLabel16, Me.xrLabel15, Me.xrLabel14, Me.xrLabel13, Me.xrLabel12, Me.xr_slsp, Me.xrLabel7, Me.xr_pd, Me.xr_pu_del_dt, Me.xrLabel6, Me.xr_cust_cd, Me.xrLabel5, Me.xrLabel2, Me.xr_b_phone, Me.xrLabel3, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_del_doc_num, Me.xr_lname, Me.xr_addr1, Me.xr_so_wr_dt, Me.xr_fname})
        Me.GroupHeader1.Height = 125
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'xr_ord_tp
        '
        Me.xr_ord_tp.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp.ForeColor = System.Drawing.Color.DimGray
        Me.xr_ord_tp.Location = New System.Drawing.Point(467, 0)
        Me.xr_ord_tp.Name = "xr_ord_tp"
        Me.xr_ord_tp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp.Size = New System.Drawing.Size(225, 17)
        Me.xr_ord_tp.StylePriority.UseFont = False
        Me.xr_ord_tp.StylePriority.UseForeColor = False
        Me.xr_ord_tp.Text = "SALES ORDER #:"
        Me.xr_ord_tp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_ord_tp_hidden
        '
        Me.xr_ord_tp_hidden.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_ord_tp_hidden.Location = New System.Drawing.Point(450, 50)
        Me.xr_ord_tp_hidden.Name = "xr_ord_tp_hidden"
        Me.xr_ord_tp_hidden.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_ord_tp_hidden.Size = New System.Drawing.Size(8, 8)
        Me.xr_ord_tp_hidden.StylePriority.UseFont = False
        Me.xr_ord_tp_hidden.Visible = False
        '
        'xr_full_name
        '
        Me.xr_full_name.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_full_name.Location = New System.Drawing.Point(75, 0)
        Me.xr_full_name.Name = "xr_full_name"
        Me.xr_full_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_full_name.Size = New System.Drawing.Size(325, 17)
        Me.xr_full_name.StylePriority.UseFont = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel29, Me.xrPageInfo2})
        Me.PageFooter.Height = 34
        Me.PageFooter.Name = "PageFooter"
        '
        'xrLabel29
        '
        Me.xrLabel29.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel29.ForeColor = System.Drawing.Color.DimGray
        Me.xrLabel29.Location = New System.Drawing.Point(616, 17)
        Me.xrLabel29.Name = "xrLabel29"
        Me.xrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel29.Size = New System.Drawing.Size(183, 17)
        Me.xrLabel29.StylePriority.UseFont = False
        Me.xrLabel29.StylePriority.UseForeColor = False
        Me.xrLabel29.StylePriority.UseTextAlignment = False
        Me.xrLabel29.Text = "CUSTOMER INITIAL:_____________"
        Me.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'xrControlStyle2
        '
        Me.xrControlStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.xrControlStyle2.Name = "xrControlStyle2"
        Me.xrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'HBCGInvoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.GroupHeader1, Me.PageFooter})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 10, 10)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1, Me.xrControlStyle2})
        Me.Version = "8.1"
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_del_doc_num.DataBindings.Add("Text", DataSource, "SO.DEL_DOC_NUM")
        xr_del.DataBindings.Add("Text", DataSource, "SO.DEL_CHG", "{0:0.00}")
        xr_financed.DataBindings.Add("Text", DataSource, "SO.ORIG_FI_AMT", "{0:0.00}")
        xr_tax.DataBindings.Add("Text", DataSource, "SO.TAX_CHG", "{0:0.00}")
        xr_so_wr_dt.DataBindings.Add("Text", DataSource, "SO.SO_WR_DT", "{0:MM/dd/yyyy}")
        xr_fname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_F_NAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_L_NAME")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ST_CD")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_ZIP_CD")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_H_PHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.SHIP_TO_B_PHONE")
        xr_cust_cd.DataBindings.Add("Text", DataSource, "SO.CUST_CD")
        xr_pu_del_dt.DataBindings.Add("Text", DataSource, "SO.PU_DEL_DT", "{0:MM/dd/yyyy}")
        xr_pd.DataBindings.Add("Text", DataSource, "SO.PU_DEL")
        xr_disc_cd.DataBindings.Add("Text", DataSource, "SO.DISC_CD")
        xr_disc_amt.DataBindings.Add("Text", DataSource, "SO_LN.DISC_AMT")
        xr_void_flag.DataBindings.Add("Text", DataSource, "SO.VOID_FLAG")
        xr_qty.DataBindings.Add("Text", DataSource, "SO_LN.QTY")
        xr_SKU.DataBindings.Add("Text", DataSource, "SO_LN.ITM_CD")
        xr_store.DataBindings.Add("Text", DataSource, "SO_LN.STORE_CD")
        xr_loc.DataBindings.Add("Text", DataSource, "SO_LN.LOC_CD")
        xr_vsn.DataBindings.Add("Text", DataSource, "SO_LN.VSN")
        xr_slsp.DataBindings.Add("Text", DataSource, "FULL_NAME")
        xr_desc.DataBindings.Add("Text", DataSource, "SO_LN.DES")
        xr_ret.DataBindings.Add("Text", DataSource, "SO_LN.UNIT_PRC", "{0:0.00}")
        xr_ext.DataBindings.Add("Text", DataSource, "SO_LN.EXT_PRC", "{0:0.00}")
        xr_ord_tp_hidden.DataBindings.Add("Text", DataSource, "SO.ORD_TP_CD")
        xr_store_name.DataBindings.Add("Text", DataSource, "STORE.STORE_NAME")
        xr_store_addr.DataBindings.Add("Text", DataSource, "STORE.STORE_ADDRESS")
        xr_store_phone.DataBindings.Add("Text", DataSource, "STORE.STORE_PHONE")

    End Sub

    Private Sub xr_pd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_pd.BeforePrint

        Dim test As String
        test = sender.Text
        Select Case test
            Case "P"
                sender.text = "PICKUP:"
            Case "D"
                sender.text = "DELIVERY:"
        End Select

    End Sub

    Private Sub xr_tax_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tax.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_setup_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_del_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_del.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = "0.00"
        End If
    End Sub

    Private Sub xr_qty_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qty.BeforePrint
        'If Me.GetCurrentColumnValue("VOID_FLAG").ToString = "Y" Then
        '    sender.text = "0.00"
        'End If
    End Sub

    Private Sub xr_ext_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ext.BeforePrint
        sender.text = FormatNumber(xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString, 2)
        subtotal = subtotal + (xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)

    End Sub

    Private Sub xr_subtotal_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_subtotal.BeforePrint
        sender.text = FormatCurrency(subtotal, 2)
    End Sub

    Private Sub xr_grand_total_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_grand_total.BeforePrint
        sender.text = FormatCurrency(CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text), 2)
    End Sub

    Private Sub xr_payments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_payments.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select sum(decode(TRN_TP_CD,'R',-AMT,AMT)) AS PMT_AMT from ar_trn where ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP')"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDataReader2.Read Then
                If IsNumeric(MyDataReader2.Item("PMT_AMT").ToString) Then
                    sender.text = FormatNumber(MyDataReader2.Item("PMT_AMT").ToString, 2)
                Else
                    sender.text = FormatNumber(0, 2)
                End If
            End If
        Catch
            Throw
        End Try

    End Sub

    Private Sub xr_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_balance.BeforePrint
        Dim pmts As Double = 0

        If IsNumeric(xr_payments.Text) Then
            pmts = CDbl(xr_payments.Text)
        End If
        If IsNumeric(xr_financed.Text) Then
            pmts = pmts + CDbl(xr_financed.Text)
        End If
        If Not IsNumeric(xr_subtotal.Text) Then
            xr_subtotal.Text = subtotal
        End If
        sender.text = FormatCurrency((CDbl(xr_subtotal.Text) + CDbl(xr_del.Text) + CDbl(xr_tax.Text)) - pmts, 2)

    End Sub

    Private Sub xr_financed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_financed.BeforePrint
        If Not IsNumeric(sender.text) Then
            sender.text = ""
        Else
            sender.Text = FormatCurrency(sender.text, 2)
        End If
    End Sub

    Private Sub xr_disc_desc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_disc_desc.BeforePrint

        If Not String.IsNullOrEmpty(xr_disc_cd.Text) And IsNumeric(xr_disc_amt.Text) Then
            If CDbl(xr_disc_amt.Text) > 0 Then
                Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql2 As System.Data.OracleClient.OracleCommand
                Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                sql = "select des from disc where disc_cd='" & xr_disc_cd.Text & "'"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read Then
                        If Not String.IsNullOrEmpty(MyDataReader2.Item("DES").ToString) Then
                            sender.visible = True
                            sender.text = MyDataReader2.Item("DES").ToString & " (" & FormatCurrency(xr_disc_amt.Text, 2) & " - ORIGINALLY " & FormatCurrency(CDbl(xr_qty.Text * xr_disc_amt.Text) + CDbl((xr_qty.Text * Me.GetCurrentColumnValue("UNIT_PRC").ToString)), 2) & ")"
                        Else
                            sender.visible = False
                        End If
                    End If
                Catch
                    Throw
                End Try
            End If
        End If

    End Sub

    Private Sub xr_mop_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_mop.BeforePrint

        Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As System.Data.OracleClient.OracleCommand
        Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "select decode(a.TRN_TP_CD,'R',-AMT,AMT) AS PMT_AMT, b.DES, a.DES as MID, a.BNK_CRD_NUM, a.CHK_NUM, a.EXP_DT, a.APP_CD, a.REF_NUM, a.HOST_REF_NUM from ar_trn a, mop b where a.ivc_cd='" & Me.GetCurrentColumnValue("DEL_DOC_NUM").ToString & "' "
        sql = sql & "AND TRN_TP_CD IN('R','PMT','DEP') AND b.MOP_CD=a.MOP_CD"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                If Not String.IsNullOrEmpty(MyDataReader2.Item("REF_NUM").ToString) Then
                    sender.text = sender.text & MyDataReader2.Item("REF_NUM").ToString & Constants.vbCrLf
                Else
                    sender.text = sender.text & MyDataReader2.Item("DES").ToString & Constants.vbCrLf
                End If
                If String.IsNullOrEmpty(MyDataReader2.Item("HOST_REF_NUM").ToString) Then
                    xr_ref.Text = xr_ref.Text & Constants.vbCrLf
                Else
                    xr_ref.Text = xr_ref.Text & MyDataReader2.Item("HOST_REF_NUM").ToString & Constants.vbCrLf
                End If
                xr_pmt_amt.Text = xr_pmt_amt.Text & FormatCurrency(MyDataReader2.Item("PMT_AMT").ToString, 2) & Constants.vbCrLf
                If Len(MyDataReader2.Item("BNK_CRD_NUM").ToString) = 19 Then
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                ElseIf MyDataReader2.Item("CHK_NUM").ToString & "" <> "" Then
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("CHK_NUM").ToString & Constants.vbCrLf
                Else
                    xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                End If
                'xr_card_no.Text = xr_card_no.Text & MyDataReader2.Item("BNK_CRD_NUM").ToString & Constants.vbCrLf
                If InStr(MyDataReader2.Item("MID").ToString, "MID:") > 0 Then
                    xr_merchant.Text = xr_merchant.Text & Replace(MyDataReader2.Item("MID").ToString, "MID:", "") & Constants.vbCrLf
                Else
                    xr_merchant.Text = xr_merchant.Text & Constants.vbCrLf
                End If
                If Not String.IsNullOrEmpty(MyDataReader2.Item("EXP_DT").ToString) Then
                    xr_exp_dt.Text = xr_exp_dt.Text & "xx/xx" & Constants.vbCrLf
                Else
                    xr_exp_dt.Text = xr_exp_dt.Text & Constants.vbCrLf
                End If
                xr_appr_cd.Text = xr_appr_cd.Text & MyDataReader2.Item("APP_CD").ToString & Constants.vbCrLf
            Loop
        Catch
            Throw
        End Try

    End Sub

    Private Sub xr_comments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_comments.BeforePrint

        If comments_processed <> True Then
            Dim conn As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "Select SO_CMNT.DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & xr_del_doc_num.Text & "'"
            sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
            sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.CMNT_TYPE='D' ORDER BY SEQ#"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    sender.text = sender.text & MyDataReader2.Item("TEXT").ToString & Constants.vbCrLf
                Loop
            Catch
                Throw
            End Try
            comments_processed = True
        End If

    End Sub

    Private Sub xr_triggers_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_triggers.BeforePrint

        If triggers_processed <> True Then
            Dim conn2 As System.Data.OracleClient.OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql2 As System.Data.OracleClient.OracleCommand
            Dim MyDataReader2 As System.Data.OracleClient.OracleDataReader
            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim conn As System.Data.OracleClient.OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','P_D','" & Microsoft.VisualBasic.Strings.Left(xr_pd.Text, 1) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
            sql = sql & "VALUES('" & xr_del_doc_num.Text & "','STORE_CD','" & Microsoft.VisualBasic.Strings.Mid(xr_del_doc_num.Text, 6, 2) & "')"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            sql = "SELECT c.ITM_TP_CD, b.MJR_CD, c.MNR_CD, a.ITM_CD, c.DROP_DT, d.ORD_TP_CD FROM SO_LN a, INV_MNR b, ITM c, SO d "
            sql = sql & "WHERE a.DEL_DOC_NUM='" & xr_del_doc_num.Text & "' AND c.MNR_CD=b.MNR_CD "
            sql = sql & "AND a.ITM_CD=c.ITM_CD AND d.DEL_DOC_NUM=a.DEL_DOC_NUM"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_TP_CD','" & MyDataReader2.Item("ITM_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MAJOR_CD','" & MyDataReader2.Item("MJR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','MINOR_CD','" & MyDataReader2.Item("MNR_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ITM_CD','" & MyDataReader2.Item("ITM_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                    sql = sql & "VALUES('" & xr_del_doc_num.Text & "','ORD_TP_CD','" & MyDataReader2.Item("ORD_TP_CD").ToString & "')"

                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql2.ExecuteNonQuery()

                    If Not String.IsNullOrEmpty(MyDataReader2.Item("DROP_DT").ToString) Then
                        sql = "INSERT INTO INVOICE_TRIGGERS_TEMP (DEL_DOC_NUM, TRIGGER_FIELD, TRIGGER_VALUE) "
                        sql = sql & "VALUES('" & xr_del_doc_num.Text & "','DROP_DT','" & MyDataReader2.Item("DROP_DT").ToString & "')"

                        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                        objSql2.ExecuteNonQuery()
                    End If
                Loop
            Catch
                Throw
            End Try

            sql = "select trigger_response, TRIGGER_ID "
            sql = sql & "from invoice_triggers, invoice_triggers_temp "
            sql = sql & "where invoice_triggers.trigger_field = invoice_triggers_temp.trigger_field "
            sql = sql & "and invoice_triggers.trigger_value=invoice_triggers_temp.trigger_value "
            sql = sql & "and del_doc_num='" & xr_del_doc_num.Text & "' "
            sql = sql & "GROUP BY TRIGGER_ID, trigger_response"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read
                    xr_triggers.Text = xr_triggers.Text & MyDataReader2.Item("TRIGGER_RESPONSE").ToString & Constants.vbCrLf
                Loop
            Catch
                Throw
            End Try

            If Not String.IsNullOrEmpty(xr_triggers.Text) Then
                xr_triggers.Text = xr_triggers.Text & Constants.vbCrLf
            End If

            sql = "DELETE FROM INVOICE_TRIGGERS_TEMP WHERE DEL_DOC_NUM='" & xr_del_doc_num.Text & "'"

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.ExecuteNonQuery()

            triggers_processed = True
        End If

    End Sub

    Private Sub xr_full_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_full_name.BeforePrint

        xr_full_name.Text = xr_fname.Text & " " & xr_lname.Text

    End Sub

    Private Sub xr_ord_tp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_ord_tp.BeforePrint

        Select Case xr_ord_tp_hidden.Text
            Case "SAL"
                xr_ord_tp.Text = "SALES ORDER #:"
            Case "CRM"
                xr_ord_tp.Text = "CREDIT MEMO #:"
            Case "MCR"
                xr_ord_tp.Text = "MISCELLANEOUS CREDIT #:"
            Case "MDB"
                xr_ord_tp.Text = "MISCELLANEOUS DEBIT #:"
        End Select

    End Sub

    Private Sub xr_store_addr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_addr.BeforePrint

        xr_store_addr.Text = StrConv(xr_store_addr.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub xr_store_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_store_name.BeforePrint

        xr_store_name.Text = StrConv(xr_store_name.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub HBCGInvoice_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint

        Dim path As String
        path = HttpContext.Current.Request.PhysicalApplicationPath & "\Signatures\" & HttpContext.Current.Session.SessionID.ToString & ".bmp"
        path = Replace(path, "\", "\\")
        If File.Exists(path) Then
            xrLine1.Visible = False
            xr_sig_pic.Image = System.Drawing.Image.FromFile(path)
        End If

    End Sub
End Class