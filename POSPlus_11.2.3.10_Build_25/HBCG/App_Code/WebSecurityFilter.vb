﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Data
Imports System.Configuration
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Namespace WebSecurityFilter
    Public Class SecurityHttpModule
        Implements IHttpModule
        Public Sub Dispose() Implements IHttpModule.Dispose
            ' nothing to do
        End Sub

        Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
            'context.BeginRequest += New EventHandler(AddressOf context_BeginRequest)
            'AddHandler context.PostRequestHandlerExecute, AddressOf context_BeginRequest
            AddHandler context.AcquireRequestState, New EventHandler(AddressOf context_BeginRequest)
        End Sub

        Private Class RegexWithDesc
            Inherits Regex
            Private _errorText As String

            Public ReadOnly Property ErrorText() As String
                Get
                    Return _errorText
                End Get
            End Property
            Public Sub New(regex As String, options As RegexOptions, errorText As String)
                MyBase.New(regex, options)
                _errorText = errorText
            End Sub
        End Class

        ''' <summary>
        ''' error text displayed when security violation is detected
        ''' </summary>
        Private _errorhtml As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.1//EN"" ""http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"">" + "<html xmlns=""http://www.w3.org/1999/xhtml"" >" + "<body style=""background:black;"">" + "<table style=""width:100%"" >" + "<tr><td align=""center"">" + "<div style=""border:3px solid red;text-align:center;width:95%;color:red;padding:10px;text-decoration:blink;"">" + "SECURITY VIOLATION" + "<br/>" + "<br/>" + "{0}" + "<br/>" + "</div>" + "</td></tr>" + "</table>" + "</body>" + "</html>"

        ' regex for default checks
        ' http://www.securityfocus.com/infocus/1768
        Shared _defaultRegexOptions As RegexOptions = RegexOptions.Compiled Or RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace
        Private _regexCollection As RegexWithDesc() = New RegexWithDesc() {New RegexWithDesc("((¼|<)[^\n]+(>|¾)*)|javascript|unescape", _defaultRegexOptions, "XSS 1"), New RegexWithDesc("(=)[^\n]*(\'|(\-\-)|(;))", _defaultRegexOptions, "SQL Injection detected"), New RegexWithDesc("(\')\s*(or|union|insert|delete|drop|update|create|(declare\s+@\w+))", _defaultRegexOptions, "SQL Injection detected"), New RegexWithDesc("exec(((\s|\+)+(s|x)p\w+)|(\s@))", _defaultRegexOptions, "SQL 5")}
#Region "IHttpModule Members"
        Private Sub context_BeginRequest(sender As Object, e As EventArgs)
            Try
                Dim toCheck As New List(Of String)()
                Dim context As HttpContext = DirectCast(sender, HttpApplication).Context
                Dim validateWord As String
                Dim getkeys As String = String.Empty
                Dim keyvalue As String = String.Empty
                Dim str As String = System.Web.HttpContext.Current.Request.Path()
                'Dim configObject As System.Configuration.ConfigurationSection = New ConfigurationSection()
                'Dim context As HttpContext = New HttpContext()
                'Do not validate if the pages are login, default or posted page name is empty
                If str.ToLower().Contains("login.aspx") Or str = String.Empty Or str.ToLower().Contains("default.aspx") Then
                    ' do nothing
                    ' Else

                    For Each key As String In HttpContext.Current.ApplicationInstance.Request.QueryString.AllKeys
                        toCheck.Add(HttpContext.Current.ApplicationInstance.Request(key))
                    Next
                    For Each key As String In HttpContext.Current.ApplicationInstance.Request.Form.AllKeys
                        toCheck.Add(HttpContext.Current.ApplicationInstance.Request.Form(key))
                    Next
                    For Each key As String In HttpContext.Current.Request.Form
                        toCheck.Add(key)
                    Next
                    'Check for form elements that may contain the sql injection script
                    If context.Request.Form IsNot Nothing Then
                        For i As Integer = 0 To context.Request.Form.Count - 1
                            If context.Request.Form.Keys(i) = Nothing Then
                                Continue For
                            End If

                            validateWord = context.Request.Form.Keys(i).ToUpper()
                            If validateWord = "__ASYNCPOST" OrElse validateWord = "__VIEWSTATE" OrElse validateWord = "__PREVIOUSPAGE" OrElse validateWord = "__EVENTARGUMENT" OrElse validateWord = "__EVENTTARGET" OrElse validateWord = "__CLIENTPOSTDATA__" Then
                                Continue For
                            End If
                            keyvalue = context.Server.HtmlDecode(context.Request.Form(i)).Replace("'", "")
                            toCheck.Add(keyvalue)
                        Next
                    End If
                    For Each regex As RegexWithDesc In _regexCollection
                        For Each param As String In toCheck
                            Dim dp As String = HttpUtility.UrlDecode(param)
                            If Not dp = Nothing AndAlso regex.IsMatch(dp) Then
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                                System.Web.HttpContext.Current.Response.Redirect("Error_Handling.aspx", False)
                                Return
                            End If
                        Next
                    Next
                End If
            Catch x As System.Threading.ThreadAbortException
                Throw
            Catch ex As Exception
                HttpContext.Current.ApplicationInstance.Response.Write(String.Format(_errorhtml, "Attack Vector Detected"))
                HttpContext.Current.ApplicationInstance.Response.Write(String.Format(_errorhtml, ex.[GetType]().ToString()))
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End Try
        End Sub

#End Region
    End Class
End Namespace
