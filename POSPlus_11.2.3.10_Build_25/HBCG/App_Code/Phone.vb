Public Class Phone
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xr_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_addr2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_city As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_state As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_zip As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_h_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_b_phone As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents winControlContainer1 As DevExpress.XtraReports.UI.WinControlContainer
    Private WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_wr_dt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_follow_text As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_rel_no As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Phone.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.Phone.ResourceManager
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_follow_text = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_wr_dt = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_b_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_h_phone = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_zip = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_state = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_city = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_addr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.winControlContainer1 = New DevExpress.XtraReports.UI.WinControlContainer
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xr_rel_no = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_rel_no, Me.xrLine1, Me.xrLabel5, Me.xrLabel4, Me.xr_follow_text, Me.xr_wr_dt, Me.xrLabel2, Me.xr_b_phone, Me.xrLabel3, Me.xr_h_phone, Me.xr_zip, Me.xr_state, Me.xr_city, Me.xr_addr2, Me.xr_lname, Me.xr_fname, Me.xr_addr1})
        Me.Detail.Height = 117
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StyleName = "xrControlStyle1"
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_follow_text
        '
        Me.xr_follow_text.Location = New System.Drawing.Point(558, 75)
        Me.xr_follow_text.Name = "xr_follow_text"
        Me.xr_follow_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_follow_text.Size = New System.Drawing.Size(183, 17)
        Me.xr_follow_text.Text = "xr_follow_text"
        '
        'xr_wr_dt
        '
        Me.xr_wr_dt.Location = New System.Drawing.Point(558, 50)
        Me.xr_wr_dt.Name = "xr_wr_dt"
        Me.xr_wr_dt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_wr_dt.Size = New System.Drawing.Size(183, 17)
        Me.xr_wr_dt.StylePriority.UseTextAlignment = False
        Me.xr_wr_dt.Text = "xr_wr_dt"
        Me.xr_wr_dt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_wr_dt.WordWrap = False
        '
        'xrLabel2
        '
        Me.xrLabel2.Location = New System.Drawing.Point(500, 7)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.Size = New System.Drawing.Size(33, 16)
        Me.xrLabel2.Text = "(H) "
        '
        'xr_b_phone
        '
        Me.xr_b_phone.Location = New System.Drawing.Point(533, 25)
        Me.xr_b_phone.Name = "xr_b_phone"
        Me.xr_b_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_b_phone.Size = New System.Drawing.Size(217, 17)
        Me.xr_b_phone.Text = "xr_b_phone"
        '
        'xrLabel3
        '
        Me.xrLabel3.Location = New System.Drawing.Point(500, 25)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.Size = New System.Drawing.Size(33, 16)
        Me.xrLabel3.Text = "(B) "
        '
        'xr_h_phone
        '
        Me.xr_h_phone.Location = New System.Drawing.Point(533, 7)
        Me.xr_h_phone.Name = "xr_h_phone"
        Me.xr_h_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_h_phone.Size = New System.Drawing.Size(217, 17)
        Me.xr_h_phone.Text = "xr_h_phone"
        '
        'xr_zip
        '
        Me.xr_zip.Location = New System.Drawing.Point(217, 61)
        Me.xr_zip.Name = "xr_zip"
        Me.xr_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_zip.Size = New System.Drawing.Size(67, 17)
        Me.xr_zip.Text = "xr_zip"
        '
        'xr_state
        '
        Me.xr_state.Location = New System.Drawing.Point(183, 61)
        Me.xr_state.Name = "xr_state"
        Me.xr_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_state.Size = New System.Drawing.Size(25, 17)
        Me.xr_state.Text = "xr_state"
        '
        'xr_city
        '
        Me.xr_city.Location = New System.Drawing.Point(8, 61)
        Me.xr_city.Name = "xr_city"
        Me.xr_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_city.Size = New System.Drawing.Size(167, 17)
        Me.xr_city.Text = "xr_city"
        '
        'xr_addr2
        '
        Me.xr_addr2.Location = New System.Drawing.Point(8, 43)
        Me.xr_addr2.Name = "xr_addr2"
        Me.xr_addr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr2.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr2.Text = "xr_addr2"
        '
        'xr_lname
        '
        Me.xr_lname.Location = New System.Drawing.Point(142, 8)
        Me.xr_lname.Name = "xr_lname"
        Me.xr_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_lname.Size = New System.Drawing.Size(133, 17)
        Me.xr_lname.Text = "xr_lname"
        '
        'xr_fname
        '
        Me.xr_fname.Location = New System.Drawing.Point(0, 8)
        Me.xr_fname.Name = "xr_fname"
        Me.xr_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_fname.Size = New System.Drawing.Size(133, 17)
        Me.xr_fname.Text = "xr_fname"
        '
        'xr_addr1
        '
        Me.xr_addr1.Location = New System.Drawing.Point(8, 25)
        Me.xr_addr1.Name = "xr_addr1"
        Me.xr_addr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_addr1.Size = New System.Drawing.Size(275, 17)
        Me.xr_addr1.Text = "xr_addr1"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel8, Me.xrLabel1, Me.winControlContainer1, Me.xrPageInfo1})
        Me.PageHeader.Height = 95
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel1
        '
        Me.xrLabel1.Location = New System.Drawing.Point(308, 0)
        Me.xrLabel1.Multiline = True
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.Size = New System.Drawing.Size(258, 50)
        Me.xrLabel1.Text = "5875 Highland Hills Dr" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Westerville, OH 43082" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "614.256.0628"
        '
        'winControlContainer1
        '
        Me.winControlContainer1.Location = New System.Drawing.Point(0, 0)
        Me.winControlContainer1.Name = "winControlContainer1"
        Me.winControlContainer1.Size = New System.Drawing.Size(285, 60)
        Me.winControlContainer1.WinControl = Me.pictureBox1
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(274, 58)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pictureBox1.TabIndex = 0
        Me.pictureBox1.TabStop = False
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.xrPageInfo1.Location = New System.Drawing.Point(592, 0)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrPageInfo1.Size = New System.Drawing.Size(154, 17)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrControlStyle1
        '
        Me.xrControlStyle1.BackColor = System.Drawing.Color.Empty
        Me.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrControlStyle1.Name = "xrControlStyle1"
        Me.xrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPageInfo2})
        Me.PageFooter.Height = 35
        Me.PageFooter.Name = "PageFooter"
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xrLabel8.Location = New System.Drawing.Point(0, 67)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.Size = New System.Drawing.Size(483, 25)
        Me.xrLabel8.Text = "Phone Follow-Up Listing"
        '
        'xrPageInfo2
        '
        Me.xrPageInfo2.Location = New System.Drawing.Point(675, 0)
        Me.xrPageInfo2.Name = "xrPageInfo2"
        Me.xrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo2.Size = New System.Drawing.Size(67, 25)
        Me.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel4
        '
        Me.xrLabel4.Location = New System.Drawing.Point(450, 50)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel4.Text = "Written Date:"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel5
        '
        Me.xrLabel5.Location = New System.Drawing.Point(450, 75)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel5.Text = "Follow Up Date:"
        Me.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLine1
        '
        Me.xrLine1.Location = New System.Drawing.Point(0, 100)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.Size = New System.Drawing.Size(750, 8)
        '
        'xr_rel_no
        '
        Me.xr_rel_no.Location = New System.Drawing.Point(317, 8)
        Me.xr_rel_no.Name = "xr_rel_no"
        Me.xr_rel_no.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_rel_no.Size = New System.Drawing.Size(100, 17)
        '
        'Phone
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.xrControlStyle1})
        Me.Version = "8.1"
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_fname.DataBindings.Add("Text", DataSource, "SO.FNAME")
        xr_lname.DataBindings.Add("Text", DataSource, "SO.LNAME")
        'xr_salutation.DataBindings.Add("Text", DataSource, "SO.FNAME" & " " & "SO.LNAME" & ",")
        xr_addr1.DataBindings.Add("Text", DataSource, "SO.ADDR1")
        xr_addr2.DataBindings.Add("Text", DataSource, "SO.ADDR2")
        xr_city.DataBindings.Add("Text", DataSource, "SO.CITY")
        xr_state.DataBindings.Add("Text", DataSource, "SO.ST")
        xr_zip.DataBindings.Add("Text", DataSource, "SO.ZIP")
        xr_h_phone.DataBindings.Add("Text", DataSource, "SO.HPHONE")
        xr_b_phone.DataBindings.Add("Text", DataSource, "SO.BPHONE")
        xr_follow_text.DataBindings.Add("Text", DataSource, "SO.FOLLOW_UP_DT", "{0:MM/dd/yyyy}")
        xr_wr_dt.DataBindings.Add("Text", DataSource, "SO.WR_DT", "{0:MM/dd/yyyy}")
        xr_rel_no.DataBindings.Add("Text", DataSource, "SO.REL_NO")

    End Sub

End Class