Imports Microsoft.VisualBasic
Imports System.Xml
Imports HBCG_Utils

Public Class World_Gift_Utils

    Public Shared Function WGC_GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            ElseIf IsNumeric(m_node.ChildNodes.Item(0).InnerText) And IsNumeric(reasonCd) Then
                If CDbl(m_node.ChildNodes.Item(0).InnerText) = CDbl(reasonCd) Then
                    desc = m_node.ChildNodes.Item(1).InnerText
                    Exit For
                End If
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Public Shared Function WGC_SendRequestAndGetResponse(ByVal mnum As String, ByVal transaction_type As String, ByVal card_num As String, Optional ByVal amount As Double = 0) 'As XmlDocument

        Dim _requestXML As XmlDataDocument
        Dim _responseXML As New XmlDataDocument()
        Dim oWebReq As System.Net.HttpWebRequest
        oWebReq = CType(System.Net.HttpWebRequest.Create("https://wgchost.com/W3/Service.asmx"), System.Net.HttpWebRequest)

        Dim xml_string As String = ""

        xml_string = "<?xml version=""1.0"" encoding=""utf-8""?>"
        xml_string = xml_string & "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://schemas.xmlsoap.org/soap/envelope/"">"
        xml_string = xml_string & "<soap12:Body>"
        Select Case transaction_type
            Case "balance"
                xml_string = xml_string & Build_WGC_Balance(mnum, card_num)
            Case "addvalue"
                xml_string = xml_string & Build_WGC_AddValue(mnum, card_num, amount)
            Case "sale"
                xml_string = xml_string & Build_WGC_Sale(mnum, card_num, amount)
        End Select
        xml_string = xml_string & "</soap12:Body>"
        xml_string = xml_string & "</soap12:Envelope>"

        _requestXML = New XmlDataDocument
        _requestXML.RemoveAll()
        _requestXML.LoadXml(xml_string)

        Dim reqBytes As Byte()
        reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
        oWebReq.ContentLength = reqBytes.Length
        oWebReq.Method = "POST"
        oWebReq.ContentType = "text/xml"
        oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
        Try
            Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
            oReqStream.Write(reqBytes, 0, reqBytes.Length)
            Dim responseBytes As Byte()
            Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
            If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
                responseBytes = WGC_processResponse(oWebResp)
                Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
                _responseXML.RemoveAll()
                _responseXML.LoadXml(strResp)
            End If
            oWebResp.Close()
        Catch ex As Exception
            If Err.Number = 5 Then
                Return _responseXML
                Exit Function
            Else
                Return Err.Description
            End If
            'Throw
        End Try
        Return _responseXML

    End Function

    Public Shared Function Build_WGC_Balance(ByVal mnum As String, ByVal card_id As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<Balance xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</Balance>"

        Return iString

    End Function

    Public Shared Function Build_WGC_RedeemPoints(ByVal mnum As String, ByVal card_id As String, ByVal amount As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<RedeemPoints xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<amount>" & amount & "</amount>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</RedeemPoints>"

        Return iString

    End Function

    Public Shared Function Build_WGC_AddPoints(ByVal mnum As String, ByVal card_id As String, ByVal amount As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<AddPoints xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<amount>" & amount & "</amount>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</AddPoints>"

        Return iString

    End Function

    Public Shared Function Build_WGC_Sale(ByVal mnum As String, ByVal card_id As String, ByVal amount As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<Sale xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<amount>" & amount & "</amount>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</Sale>"

        Return iString

    End Function

    Public Shared Function Build_WGC_AddValue(ByVal mnum As String, ByVal card_id As String, ByVal amount As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<AddValue xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<amount>" & amount & "</amount>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</AddValue>"

        Return iString

    End Function

    Public Shared Function Build_WGC_GetTransactions(ByVal mnum As String, ByVal card_id As String, ByVal amount As String, ByVal startdate As String, ByVal enddate As String)

        Dim iString As String
        Dim WGC_UserName As String = Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = "<RedeemPoints xmlns=""http://wgchost.com/w3/"">"
        iString = iString & "<mnum>" & mnum & "</mnum>"
        iString = iString & "<id>" & card_id & "</id>"
        iString = iString & "<startdate>" & startdate & "</startdate>"
        iString = iString & "<enddate>" & enddate & "</enddate>"
        iString = iString & "<cn>1234</cn>"
        'iString = iString & "<cn>" & HttpContext.Current.Session("emp_cd") & "</cn>"
        iString = iString & "<amount>" & amount & "</amount>"
        iString = iString & "<login>" & WGC_UserName & "</login>"
        iString = iString & "<pass>" & WGC_Password & "</pass>"
        iString = iString & "</RedeemPoints>"

        Return iString

    End Function

    Private Shared Function WGC_processResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()

        Dim memStream As New System.IO.MemoryStream()
        Const BUFFER_SIZE As Integer = 4096
        Dim iRead As Integer = 0
        Dim idx As Integer = 0
        Dim iSize As Int64 = 0
        memStream.SetLength(BUFFER_SIZE)
        While (True)
            Dim respBuffer(BUFFER_SIZE) As Byte
            Try
                iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
            Catch e As System.Exception
                Throw e
            End Try
            If (iRead = 0) Then
                Exit While
            End If
            iSize += iRead
            memStream.SetLength(iSize)
            memStream.Write(respBuffer, 0, iRead)
            idx += iRead
        End While

        Dim content As Byte() = memStream.ToArray()
        memStream.Close()
        Return content

    End Function

    Public Function WGC_ParseResponseAndFillDS(ByRef responseXML As XmlDocument)

        Dim rsaDS As New DataSet()
        Dim ResponseDS As New ResponseAllianceDataSet
        rsaDS.ReadXml(New XmlNodeReader(responseXML))
        Dim responseAllianceDSDataRow As ResponseAllianceDataSet.ResponseRow
        If (Not rsaDS Is Nothing) And (rsaDS.Tables.Count > 0) Then
            If rsaDS.Tables(0).Rows.Count > 0 Then
                responseAllianceDSDataRow = ResponseDS.Response.NewResponseRow
                Dim dr As DataRow = rsaDS.Tables(2).Rows(0)
                If dr.Table.Columns.Contains("returnCode") Then
                    responseAllianceDSDataRow.returnCode = dr("returnCode")
                End If
                If dr.Table.Columns.Contains("authCode") Then
                    responseAllianceDSDataRow.authCode = dr("authCode")
                End If
                If dr.Table.Columns.Contains("reasonCode") Then
                    responseAllianceDSDataRow.reasonCode = dr("reasonCode")
                End If
                ResponseDS.Response.Rows.Add(responseAllianceDSDataRow)
            End If
        End If

        Return ResponseDS

    End Function

End Class
