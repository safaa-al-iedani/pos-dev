Imports DevExpress.XtraReports.UI

Public Class CashReceipts
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim csh_totals As Double = 0
    Dim bc_totals As Double = 0
    Dim disc_totals As Double = 0
    Dim amex_totals As Double = 0
    Dim check_totals As Double = 0
    Dim other_totals As Double = 0
    Dim rcsh_totals As Double = 0
    Dim rbc_totals As Double = 0
    Dim rdisc_totals As Double = 0
    Dim ramex_totals As Double = 0
    Dim rcheck_totals As Double = 0
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents xr_r_amex As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_r_disc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_r_bc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_r_csh As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_r_check As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_r_other As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_csh_drawer_header As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cdrawer As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Private WithEvents xrLine3 As DevExpress.XtraReports.UI.XRLine
    Dim rother_totals As Double = 0

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_date As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_amt As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_cust As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_mop As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_other As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_nbr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_check As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_appr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_bc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_csh As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_amex As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_tbl_disc As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_emp_csh As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Private WithEvents xr_slsp_fname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_home As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xr_slsp_emp_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_invoice As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bnk_crd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xr_store As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_csh_dwr As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_amex_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_disc_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_bc_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_cash_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_check_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_other_sum As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_group_totals As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "CashReceipts.resx"
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_tbl_cust = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_csh_dwr = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_store = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bnk_crd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_invoice = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_emp_csh = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_amex = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_disc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_bc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_csh = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_appr = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_check = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_nbr = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_other = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_tbl_mop = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_amt = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xr_store_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_date = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.xr_cdrawer = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_csh_drawer_header = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp_emp_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xr_slsp_home = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp_fname = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_amex_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_disc_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_bc_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_cash_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_check_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_other_sum = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_group_totals = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_amex = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_disc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_bc = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_csh = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_check = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_r_other = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule
        Me.xrLine3 = New DevExpress.XtraReports.UI.XRLine
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLine3, Me.xr_tbl_cust, Me.xr_csh_dwr, Me.xr_store, Me.xr_bnk_crd, Me.xr_invoice, Me.xr_emp_csh, Me.xr_tbl_amex, Me.xr_tbl_disc, Me.xr_tbl_bc, Me.xr_tbl_csh, Me.xr_tbl_appr, Me.xr_tbl_check, Me.xr_tbl_nbr, Me.xr_tbl_other, Me.xr_tbl_mop, Me.xr_amt})
        Me.Detail.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detail.Height = 24
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_tbl_cust
        '
        Me.xr_tbl_cust.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_cust.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_cust.Location = New System.Drawing.Point(58, 0)
        Me.xr_tbl_cust.Name = "xr_tbl_cust"
        Me.xr_tbl_cust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_cust.Size = New System.Drawing.Size(120, 16)
        Me.xr_tbl_cust.StylePriority.UseBorders = False
        Me.xr_tbl_cust.StylePriority.UseFont = False
        Me.xr_tbl_cust.StylePriority.UseTextAlignment = False
        Me.xr_tbl_cust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_tbl_cust.WordWrap = False
        '
        'xr_csh_dwr
        '
        Me.xr_csh_dwr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_csh_dwr.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_csh_dwr.Location = New System.Drawing.Point(25, 0)
        Me.xr_csh_dwr.Name = "xr_csh_dwr"
        Me.xr_csh_dwr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_csh_dwr.Size = New System.Drawing.Size(33, 16)
        Me.xr_csh_dwr.StylePriority.UseBorders = False
        Me.xr_csh_dwr.StylePriority.UseFont = False
        Me.xr_csh_dwr.StylePriority.UseTextAlignment = False
        Me.xr_csh_dwr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_csh_dwr.WordWrap = False
        '
        'xr_store
        '
        Me.xr_store.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_store.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store.Location = New System.Drawing.Point(0, 0)
        Me.xr_store.Name = "xr_store"
        Me.xr_store.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store.Size = New System.Drawing.Size(25, 16)
        Me.xr_store.StylePriority.UseBorders = False
        Me.xr_store.StylePriority.UseFont = False
        Me.xr_store.StylePriority.UseTextAlignment = False
        Me.xr_store.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_store.WordWrap = False
        '
        'xr_bnk_crd
        '
        Me.xr_bnk_crd.Location = New System.Drawing.Point(1033, 8)
        Me.xr_bnk_crd.Name = "xr_bnk_crd"
        Me.xr_bnk_crd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bnk_crd.Size = New System.Drawing.Size(8, 2)
        Me.xr_bnk_crd.Visible = False
        '
        'xr_invoice
        '
        Me.xr_invoice.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_invoice.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_invoice.Location = New System.Drawing.Point(180, 0)
        Me.xr_invoice.Name = "xr_invoice"
        Me.xr_invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_invoice.Size = New System.Drawing.Size(80, 16)
        Me.xr_invoice.StylePriority.UseBorders = False
        Me.xr_invoice.StylePriority.UseFont = False
        Me.xr_invoice.StylePriority.UseTextAlignment = False
        Me.xr_invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xr_invoice.WordWrap = False
        '
        'xr_emp_csh
        '
        Me.xr_emp_csh.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_emp_csh.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_emp_csh.Location = New System.Drawing.Point(925, 0)
        Me.xr_emp_csh.Name = "xr_emp_csh"
        Me.xr_emp_csh.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_emp_csh.Size = New System.Drawing.Size(75, 16)
        Me.xr_emp_csh.StylePriority.UseBorders = False
        Me.xr_emp_csh.StylePriority.UseFont = False
        Me.xr_emp_csh.StylePriority.UseTextAlignment = False
        Me.xr_emp_csh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_emp_csh.WordWrap = False
        '
        'xr_tbl_amex
        '
        Me.xr_tbl_amex.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_amex.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_amex.Location = New System.Drawing.Point(848, 0)
        Me.xr_tbl_amex.Name = "xr_tbl_amex"
        Me.xr_tbl_amex.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_amex.Size = New System.Drawing.Size(77, 16)
        Me.xr_tbl_amex.StylePriority.UseBorders = False
        Me.xr_tbl_amex.StylePriority.UseFont = False
        Me.xr_tbl_amex.StylePriority.UseTextAlignment = False
        Me.xr_tbl_amex.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_amex.WordWrap = False
        '
        'xr_tbl_disc
        '
        Me.xr_tbl_disc.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_disc.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_disc.Location = New System.Drawing.Point(771, 0)
        Me.xr_tbl_disc.Name = "xr_tbl_disc"
        Me.xr_tbl_disc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_disc.Size = New System.Drawing.Size(77, 16)
        Me.xr_tbl_disc.StylePriority.UseBorders = False
        Me.xr_tbl_disc.StylePriority.UseFont = False
        Me.xr_tbl_disc.StylePriority.UseTextAlignment = False
        Me.xr_tbl_disc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_disc.WordWrap = False
        '
        'xr_tbl_bc
        '
        Me.xr_tbl_bc.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_bc.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_bc.Location = New System.Drawing.Point(697, 0)
        Me.xr_tbl_bc.Name = "xr_tbl_bc"
        Me.xr_tbl_bc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_bc.Size = New System.Drawing.Size(74, 16)
        Me.xr_tbl_bc.StylePriority.UseBorders = False
        Me.xr_tbl_bc.StylePriority.UseFont = False
        Me.xr_tbl_bc.StylePriority.UseTextAlignment = False
        Me.xr_tbl_bc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_bc.WordWrap = False
        '
        'xr_tbl_csh
        '
        Me.xr_tbl_csh.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_csh.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_csh.Location = New System.Drawing.Point(622, 0)
        Me.xr_tbl_csh.Name = "xr_tbl_csh"
        Me.xr_tbl_csh.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_csh.Size = New System.Drawing.Size(75, 16)
        Me.xr_tbl_csh.StylePriority.UseBorders = False
        Me.xr_tbl_csh.StylePriority.UseFont = False
        Me.xr_tbl_csh.StylePriority.UseTextAlignment = False
        Me.xr_tbl_csh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_csh.WordWrap = False
        '
        'xr_tbl_appr
        '
        Me.xr_tbl_appr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_appr.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_appr.Location = New System.Drawing.Point(547, 0)
        Me.xr_tbl_appr.Name = "xr_tbl_appr"
        Me.xr_tbl_appr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_appr.Size = New System.Drawing.Size(74, 16)
        Me.xr_tbl_appr.StylePriority.UseBorders = False
        Me.xr_tbl_appr.StylePriority.UseFont = False
        Me.xr_tbl_appr.StylePriority.UseTextAlignment = False
        Me.xr_tbl_appr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_tbl_appr.WordWrap = False
        '
        'xr_tbl_check
        '
        Me.xr_tbl_check.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_check.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_check.Location = New System.Drawing.Point(473, 0)
        Me.xr_tbl_check.Name = "xr_tbl_check"
        Me.xr_tbl_check.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_check.Size = New System.Drawing.Size(74, 16)
        Me.xr_tbl_check.StylePriority.UseBorders = False
        Me.xr_tbl_check.StylePriority.UseFont = False
        Me.xr_tbl_check.StylePriority.UseTextAlignment = False
        Me.xr_tbl_check.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_check.WordWrap = False
        '
        'xr_tbl_nbr
        '
        Me.xr_tbl_nbr.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_nbr.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_nbr.Location = New System.Drawing.Point(385, 0)
        Me.xr_tbl_nbr.Name = "xr_tbl_nbr"
        Me.xr_tbl_nbr.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_nbr.Size = New System.Drawing.Size(88, 16)
        Me.xr_tbl_nbr.StylePriority.UseBorders = False
        Me.xr_tbl_nbr.StylePriority.UseFont = False
        Me.xr_tbl_nbr.StylePriority.UseTextAlignment = False
        Me.xr_tbl_nbr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_tbl_nbr.WordWrap = False
        '
        'xr_tbl_other
        '
        Me.xr_tbl_other.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_other.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_other.Location = New System.Drawing.Point(318, 0)
        Me.xr_tbl_other.Name = "xr_tbl_other"
        Me.xr_tbl_other.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_other.Size = New System.Drawing.Size(67, 16)
        Me.xr_tbl_other.StylePriority.UseBorders = False
        Me.xr_tbl_other.StylePriority.UseFont = False
        Me.xr_tbl_other.StylePriority.UseTextAlignment = False
        Me.xr_tbl_other.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.xr_tbl_other.WordWrap = False
        '
        'xr_tbl_mop
        '
        Me.xr_tbl_mop.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_tbl_mop.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_tbl_mop.Location = New System.Drawing.Point(260, 0)
        Me.xr_tbl_mop.Name = "xr_tbl_mop"
        Me.xr_tbl_mop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_tbl_mop.Size = New System.Drawing.Size(58, 16)
        Me.xr_tbl_mop.StylePriority.UseBorders = False
        Me.xr_tbl_mop.StylePriority.UseFont = False
        Me.xr_tbl_mop.StylePriority.UseTextAlignment = False
        Me.xr_tbl_mop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xr_tbl_mop.WordWrap = False
        '
        'xr_amt
        '
        Me.xr_amt.Location = New System.Drawing.Point(1008, 8)
        Me.xr_amt.Name = "xr_amt"
        Me.xr_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_amt.Size = New System.Drawing.Size(8, 2)
        Me.xr_amt.Visible = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel3, Me.xrTable1, Me.xr_store_cd, Me.xr_date, Me.xrLabel2, Me.xrLabel1, Me.xrPageInfo1})
        Me.PageHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageHeader.Height = 75
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseFont = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel3.Location = New System.Drawing.Point(350, 0)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.Size = New System.Drawing.Size(200, 25)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.Text = "DAILY CASH RECEIPTS"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrTable1
        '
        Me.xrTable1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrTable1.Location = New System.Drawing.Point(0, 42)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow1})
        Me.xrTable1.Size = New System.Drawing.Size(1000, 33)
        Me.xrTable1.StylePriority.UseFont = False
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell6, Me.xrTableCell13, Me.xrTableCell14, Me.xrTableCell12, Me.xrTableCell1, Me.xrTableCell11, Me.xrTableCell10, Me.xrTableCell5, Me.xrTableCell9, Me.xrTableCell2, Me.xrTableCell8, Me.xrTableCell4, Me.xrTableCell7, Me.xrTableCell3})
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 1
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell6.StylePriority.UseTextAlignment = False
        Me.xrTableCell6.Text = "St"
        Me.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.xrTableCell6.Weight = 0.025000000000000022
        '
        'xrTableCell13
        '
        Me.xrTableCell13.Name = "xrTableCell13"
        Me.xrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell13.StylePriority.UseTextAlignment = False
        Me.xrTableCell13.Text = "Dwr"
        Me.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell13.Weight = 0.032999999999999988
        '
        'xrTableCell14
        '
        Me.xrTableCell14.Name = "xrTableCell14"
        Me.xrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell14.StylePriority.UseTextAlignment = False
        Me.xrTableCell14.Text = "Customer"
        Me.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell14.Weight = 0.11999999999999997
        '
        'xrTableCell12
        '
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell12.StylePriority.UseTextAlignment = False
        Me.xrTableCell12.Text = "Invoice"
        Me.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.xrTableCell12.Weight = 0.080000000000000016
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell1.StylePriority.UseTextAlignment = False
        Me.xrTableCell1.Text = "MOP Code"
        Me.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell1.Weight = 0.058
        '
        'xrTableCell11
        '
        Me.xrTableCell11.Name = "xrTableCell11"
        Me.xrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell11.StylePriority.UseTextAlignment = False
        Me.xrTableCell11.Text = "Other"
        Me.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell11.Weight = 0.067
        '
        'xrTableCell10
        '
        Me.xrTableCell10.Multiline = True
        Me.xrTableCell10.Name = "xrTableCell10"
        Me.xrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell10.StylePriority.UseTextAlignment = False
        Me.xrTableCell10.Text = "Check|Card" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Number"
        Me.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell10.Weight = 0.092
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell5.StylePriority.UseTextAlignment = False
        Me.xrTableCell5.Text = "Check"
        Me.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell5.Weight = 0.075
        '
        'xrTableCell9
        '
        Me.xrTableCell9.Name = "xrTableCell9"
        Me.xrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell9.StylePriority.UseTextAlignment = False
        Me.xrTableCell9.Text = "Approval"
        Me.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell9.Weight = 0.075
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell2.StylePriority.UseTextAlignment = False
        Me.xrTableCell2.Text = "Cash"
        Me.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell2.Weight = 0.075
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell8.StylePriority.UseTextAlignment = False
        Me.xrTableCell8.Text = "Visa | MC"
        Me.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell8.Weight = 0.075
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell4.StylePriority.UseTextAlignment = False
        Me.xrTableCell4.Text = "Discover"
        Me.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell4.Weight = 0.075
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell7.StylePriority.UseTextAlignment = False
        Me.xrTableCell7.Text = "AMEX"
        Me.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell7.Weight = 0.075
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTableCell3.StylePriority.UseTextAlignment = False
        Me.xrTableCell3.Text = "Emp Init"
        Me.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.xrTableCell3.Weight = 0.075
        '
        'xr_store_cd
        '
        Me.xr_store_cd.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_cd.Location = New System.Drawing.Point(75, 17)
        Me.xr_store_cd.Name = "xr_store_cd"
        Me.xr_store_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_cd.Size = New System.Drawing.Size(100, 17)
        Me.xr_store_cd.StylePriority.UseFont = False
        '
        'xr_date
        '
        Me.xr_date.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_date.Location = New System.Drawing.Point(75, 0)
        Me.xr_date.Name = "xr_date"
        Me.xr_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_date.Size = New System.Drawing.Size(100, 17)
        Me.xr_date.StylePriority.UseFont = False
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.Location = New System.Drawing.Point(0, 17)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.Text = "Store Code:"
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.Location = New System.Drawing.Point(0, 0)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.Size = New System.Drawing.Size(42, 17)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Date:"
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo1.Location = New System.Drawing.Point(842, 0)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo1.Size = New System.Drawing.Size(58, 17)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'PageFooter
        '
        Me.PageFooter.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageFooter.Height = 30
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.Transparent
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_cdrawer, Me.xr_csh_drawer_header, Me.xr_slsp_emp_cd, Me.xrLine2, Me.xrLine1, Me.xr_slsp_home, Me.xr_slsp_lname, Me.xr_slsp_fname})
        Me.GroupHeader1.Height = 51
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.StylePriority.UseBackColor = False
        '
        'xr_cdrawer
        '
        Me.xr_cdrawer.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cdrawer.Location = New System.Drawing.Point(134, 17)
        Me.xr_cdrawer.Name = "xr_cdrawer"
        Me.xr_cdrawer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cdrawer.Size = New System.Drawing.Size(100, 25)
        Me.xr_cdrawer.StylePriority.UseFont = False
        Me.xr_cdrawer.StylePriority.UseTextAlignment = False
        Me.xr_cdrawer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xr_csh_drawer_header
        '
        Me.xr_csh_drawer_header.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_csh_drawer_header.Location = New System.Drawing.Point(8, 17)
        Me.xr_csh_drawer_header.Name = "xr_csh_drawer_header"
        Me.xr_csh_drawer_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_csh_drawer_header.Size = New System.Drawing.Size(125, 25)
        Me.xr_csh_drawer_header.StylePriority.UseFont = False
        Me.xr_csh_drawer_header.StylePriority.UseTextAlignment = False
        Me.xr_csh_drawer_header.Text = "Cash Drawer Code:"
        Me.xr_csh_drawer_header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xr_slsp_emp_cd
        '
        Me.xr_slsp_emp_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_emp_cd.Location = New System.Drawing.Point(567, 17)
        Me.xr_slsp_emp_cd.Name = "xr_slsp_emp_cd"
        Me.xr_slsp_emp_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_emp_cd.Size = New System.Drawing.Size(100, 17)
        Me.xr_slsp_emp_cd.StylePriority.UseFont = False
        Me.xr_slsp_emp_cd.Text = "xr_slsp_emp_cd"
        Me.xr_slsp_emp_cd.Visible = False
        '
        'xrLine2
        '
        Me.xrLine2.Location = New System.Drawing.Point(0, 8)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.Size = New System.Drawing.Size(1000, 8)
        '
        'xrLine1
        '
        Me.xrLine1.Location = New System.Drawing.Point(0, 42)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.Size = New System.Drawing.Size(1000, 8)
        '
        'xr_slsp_home
        '
        Me.xr_slsp_home.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_home.Location = New System.Drawing.Point(817, 17)
        Me.xr_slsp_home.Name = "xr_slsp_home"
        Me.xr_slsp_home.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_home.Size = New System.Drawing.Size(25, 17)
        Me.xr_slsp_home.StylePriority.UseFont = False
        Me.xr_slsp_home.Visible = False
        '
        'xr_slsp_lname
        '
        Me.xr_slsp_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_lname.Location = New System.Drawing.Point(683, 17)
        Me.xr_slsp_lname.Name = "xr_slsp_lname"
        Me.xr_slsp_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_lname.Size = New System.Drawing.Size(25, 17)
        Me.xr_slsp_lname.StylePriority.UseFont = False
        Me.xr_slsp_lname.Visible = False
        '
        'xr_slsp_fname
        '
        Me.xr_slsp_fname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_fname.Location = New System.Drawing.Point(717, 17)
        Me.xr_slsp_fname.Name = "xr_slsp_fname"
        Me.xr_slsp_fname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_fname.Size = New System.Drawing.Size(100, 17)
        Me.xr_slsp_fname.StylePriority.UseFont = False
        Me.xr_slsp_fname.Visible = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel17, Me.xrLabel16, Me.xrLabel15, Me.xrLabel14, Me.xrLabel13, Me.xrLabel12, Me.xr_amex_sum, Me.xr_disc_sum, Me.xr_bc_sum, Me.xr_cash_sum, Me.xr_check_sum, Me.xr_other_sum, Me.xr_group_totals})
        Me.GroupFooter1.Height = 50
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'xrLabel17
        '
        Me.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel17.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel17.Location = New System.Drawing.Point(849, 16)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.Size = New System.Drawing.Size(77, 17)
        Me.xrLabel17.StylePriority.UseBorders = False
        Me.xrLabel17.StylePriority.UseFont = False
        Me.xrLabel17.StylePriority.UseTextAlignment = False
        Me.xrLabel17.Text = "AMEX"
        Me.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel16
        '
        Me.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel16.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel16.Location = New System.Drawing.Point(771, 16)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.Size = New System.Drawing.Size(77, 17)
        Me.xrLabel16.StylePriority.UseBorders = False
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseTextAlignment = False
        Me.xrLabel16.Text = "Discover"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel15
        '
        Me.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel15.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel15.Location = New System.Drawing.Point(697, 16)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.Size = New System.Drawing.Size(74, 17)
        Me.xrLabel15.StylePriority.UseBorders = False
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.StylePriority.UseTextAlignment = False
        Me.xrLabel15.Text = "Visa| MC"
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel14
        '
        Me.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel14.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel14.Location = New System.Drawing.Point(622, 16)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.Size = New System.Drawing.Size(75, 17)
        Me.xrLabel14.StylePriority.UseBorders = False
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseTextAlignment = False
        Me.xrLabel14.Text = "Cash"
        Me.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel13
        '
        Me.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel13.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.Location = New System.Drawing.Point(475, 16)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.Size = New System.Drawing.Size(74, 17)
        Me.xrLabel13.StylePriority.UseBorders = False
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseTextAlignment = False
        Me.xrLabel13.Text = "Check"
        Me.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel12
        '
        Me.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel12.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel12.Location = New System.Drawing.Point(283, 16)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.Size = New System.Drawing.Size(100, 17)
        Me.xrLabel12.StylePriority.UseBorders = False
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseTextAlignment = False
        Me.xrLabel12.Text = "Other"
        Me.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xr_amex_sum
        '
        Me.xr_amex_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_amex_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_amex_sum.Location = New System.Drawing.Point(848, 33)
        Me.xr_amex_sum.Name = "xr_amex_sum"
        Me.xr_amex_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_amex_sum.Size = New System.Drawing.Size(77, 17)
        Me.xr_amex_sum.StylePriority.UseBorders = False
        Me.xr_amex_sum.StylePriority.UseFont = False
        Me.xr_amex_sum.Text = "0.00"
        Me.xr_amex_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_disc_sum
        '
        Me.xr_disc_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_disc_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_disc_sum.Location = New System.Drawing.Point(771, 33)
        Me.xr_disc_sum.Name = "xr_disc_sum"
        Me.xr_disc_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_disc_sum.Size = New System.Drawing.Size(77, 17)
        Me.xr_disc_sum.StylePriority.UseBorders = False
        Me.xr_disc_sum.StylePriority.UseFont = False
        Me.xr_disc_sum.Text = "0.00"
        Me.xr_disc_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_bc_sum
        '
        Me.xr_bc_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_bc_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_bc_sum.Location = New System.Drawing.Point(697, 33)
        Me.xr_bc_sum.Name = "xr_bc_sum"
        Me.xr_bc_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_bc_sum.Size = New System.Drawing.Size(74, 17)
        Me.xr_bc_sum.StylePriority.UseBorders = False
        Me.xr_bc_sum.StylePriority.UseFont = False
        Me.xr_bc_sum.Text = "0.00"
        Me.xr_bc_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_cash_sum
        '
        Me.xr_cash_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_cash_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_cash_sum.Location = New System.Drawing.Point(622, 33)
        Me.xr_cash_sum.Name = "xr_cash_sum"
        Me.xr_cash_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_cash_sum.Size = New System.Drawing.Size(75, 17)
        Me.xr_cash_sum.StylePriority.UseBorders = False
        Me.xr_cash_sum.StylePriority.UseFont = False
        Me.xr_cash_sum.Text = "0.00"
        Me.xr_cash_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_check_sum
        '
        Me.xr_check_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_check_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_check_sum.Location = New System.Drawing.Point(475, 33)
        Me.xr_check_sum.Name = "xr_check_sum"
        Me.xr_check_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_check_sum.Size = New System.Drawing.Size(74, 17)
        Me.xr_check_sum.StylePriority.UseBorders = False
        Me.xr_check_sum.StylePriority.UseFont = False
        Me.xr_check_sum.Text = "0.00"
        Me.xr_check_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_other_sum
        '
        Me.xr_other_sum.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_other_sum.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_other_sum.Location = New System.Drawing.Point(283, 33)
        Me.xr_other_sum.Name = "xr_other_sum"
        Me.xr_other_sum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_other_sum.Size = New System.Drawing.Size(100, 17)
        Me.xr_other_sum.StylePriority.UseBorders = False
        Me.xr_other_sum.StylePriority.UseFont = False
        Me.xr_other_sum.Text = "0.00"
        Me.xr_other_sum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_group_totals
        '
        Me.xr_group_totals.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_group_totals.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_group_totals.Location = New System.Drawing.Point(0, 33)
        Me.xr_group_totals.Name = "xr_group_totals"
        Me.xr_group_totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_group_totals.Size = New System.Drawing.Size(167, 17)
        Me.xr_group_totals.StylePriority.UseBorders = False
        Me.xr_group_totals.StylePriority.UseFont = False
        Me.xr_group_totals.Text = "Cash Drawer Totals:"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel11, Me.xrLabel10, Me.xrLabel9, Me.xrLabel8, Me.xrLabel7, Me.xrLabel6, Me.xr_r_amex, Me.xr_r_disc, Me.xr_r_bc, Me.xr_r_csh, Me.xr_r_check, Me.xr_r_other, Me.xrLabel5})
        Me.ReportFooter.Height = 59
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xrLabel11
        '
        Me.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel11.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel11.Location = New System.Drawing.Point(850, 25)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.Size = New System.Drawing.Size(77, 16)
        Me.xrLabel11.StylePriority.UseBorders = False
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.StylePriority.UseTextAlignment = False
        Me.xrLabel11.Text = "AMEX"
        Me.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel10
        '
        Me.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel10.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.Location = New System.Drawing.Point(771, 25)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.Size = New System.Drawing.Size(77, 16)
        Me.xrLabel10.StylePriority.UseBorders = False
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseTextAlignment = False
        Me.xrLabel10.Text = "Discover"
        Me.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel9
        '
        Me.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel9.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel9.Location = New System.Drawing.Point(697, 25)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.Size = New System.Drawing.Size(74, 16)
        Me.xrLabel9.StylePriority.UseBorders = False
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseTextAlignment = False
        Me.xrLabel9.Text = "Visa| MC"
        Me.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel8
        '
        Me.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel8.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.Location = New System.Drawing.Point(622, 25)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.Size = New System.Drawing.Size(75, 16)
        Me.xrLabel8.StylePriority.UseBorders = False
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseTextAlignment = False
        Me.xrLabel8.Text = "Cash"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel7
        '
        Me.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel7.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.Location = New System.Drawing.Point(475, 25)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.Size = New System.Drawing.Size(74, 16)
        Me.xrLabel7.StylePriority.UseBorders = False
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "Check"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrLabel6
        '
        Me.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel6.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.Location = New System.Drawing.Point(283, 25)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.Size = New System.Drawing.Size(100, 16)
        Me.xrLabel6.StylePriority.UseBorders = False
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseTextAlignment = False
        Me.xrLabel6.Text = "Other"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xr_r_amex
        '
        Me.xr_r_amex.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_amex.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_amex.Location = New System.Drawing.Point(848, 42)
        Me.xr_r_amex.Name = "xr_r_amex"
        Me.xr_r_amex.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_amex.Size = New System.Drawing.Size(77, 17)
        Me.xr_r_amex.StylePriority.UseBorders = False
        Me.xr_r_amex.StylePriority.UseFont = False
        Me.xr_r_amex.Text = "0.00"
        Me.xr_r_amex.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_r_disc
        '
        Me.xr_r_disc.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_disc.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_disc.Location = New System.Drawing.Point(771, 42)
        Me.xr_r_disc.Name = "xr_r_disc"
        Me.xr_r_disc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_disc.Size = New System.Drawing.Size(77, 17)
        Me.xr_r_disc.StylePriority.UseBorders = False
        Me.xr_r_disc.StylePriority.UseFont = False
        Me.xr_r_disc.Text = "0.00"
        Me.xr_r_disc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_r_bc
        '
        Me.xr_r_bc.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_bc.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_bc.Location = New System.Drawing.Point(697, 42)
        Me.xr_r_bc.Name = "xr_r_bc"
        Me.xr_r_bc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_bc.Size = New System.Drawing.Size(74, 17)
        Me.xr_r_bc.StylePriority.UseBorders = False
        Me.xr_r_bc.StylePriority.UseFont = False
        Me.xr_r_bc.Text = "0.00"
        Me.xr_r_bc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_r_csh
        '
        Me.xr_r_csh.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_csh.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_csh.Location = New System.Drawing.Point(622, 42)
        Me.xr_r_csh.Name = "xr_r_csh"
        Me.xr_r_csh.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_csh.Size = New System.Drawing.Size(75, 17)
        Me.xr_r_csh.StylePriority.UseBorders = False
        Me.xr_r_csh.StylePriority.UseFont = False
        Me.xr_r_csh.Text = "0.00"
        Me.xr_r_csh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_r_check
        '
        Me.xr_r_check.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_check.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_check.Location = New System.Drawing.Point(475, 42)
        Me.xr_r_check.Name = "xr_r_check"
        Me.xr_r_check.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_check.Size = New System.Drawing.Size(74, 17)
        Me.xr_r_check.StylePriority.UseBorders = False
        Me.xr_r_check.StylePriority.UseFont = False
        Me.xr_r_check.Text = "0.00"
        Me.xr_r_check.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xr_r_other
        '
        Me.xr_r_other.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xr_r_other.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_r_other.Location = New System.Drawing.Point(283, 42)
        Me.xr_r_other.Name = "xr_r_other"
        Me.xr_r_other.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_r_other.Size = New System.Drawing.Size(100, 17)
        Me.xr_r_other.StylePriority.UseBorders = False
        Me.xr_r_other.StylePriority.UseFont = False
        Me.xr_r_other.Text = "0.00"
        Me.xr_r_other.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrLabel5
        '
        Me.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrLabel5.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.Location = New System.Drawing.Point(0, 42)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.Size = New System.Drawing.Size(167, 17)
        Me.xrLabel5.StylePriority.UseBorders = False
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.Text = "Report Totals:"
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'xrLine3
        '
        Me.xrLine3.Location = New System.Drawing.Point(0, 16)
        Me.xrLine3.Name = "xrLine3"
        Me.xrLine3.Size = New System.Drawing.Size(1000, 8)
        '
        'CashReceipts
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "8.1"
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_tbl_cust.DataBindings.Add("Text", DataSource, "AR_TRN.CUST_CD")
        xr_date.DataBindings.Add("Text", DataSource, "AR_TRN.POST_DT", "{0:MM/dd/yyyy}")
        xr_store_cd.DataBindings.Add("Text", DataSource, "AR_TRN.PMT_STORE")
        xr_tbl_mop.DataBindings.Add("Text", DataSource, "AR_TRN.MOP_CD")
        xr_tbl_nbr.DataBindings.Add("Text", DataSource, "AR_TRN.CHK_NUM")
        xr_bnk_crd.DataBindings.Add("Text", DataSource, "AR_TRN.BNK_CRD_NUM")
        xr_tbl_appr.DataBindings.Add("Text", DataSource, "AR_TRN.APP_CD")
        xr_amt.DataBindings.Add("Text", DataSource, "AR_TRN.AMT", "{0:0.00}")
        xr_emp_csh.DataBindings.Add("Text", DataSource, "AR_TRN.EMP_CD_CSHR")
        xr_slsp_fname.DataBindings.Add("Text", DataSource, "EMP.FNAME")
        xr_slsp_lname.DataBindings.Add("Text", DataSource, "EMP.LNAME")
        xr_store.DataBindings.Add("Text", DataSource, "AR_TRN.PMT_STORE")
        xr_csh_dwr.DataBindings.Add("Text", DataSource, "AR_TRN.CSH_DWR_CD")
        xr_slsp_emp_cd.DataBindings.Add("Text", DataSource, "AR_TRN.EMP_CD_CSHR")
        xr_slsp_home.DataBindings.Add("Text", DataSource, "EMP.HOME_STORE_CD")
        xr_invoice.DataBindings.Add("Text", DataSource, "AR_TRN.IVC_CD")
        xr_cdrawer.DataBindings.Add("Text", DataSource, "AR_TRN.CSH_DWR_CD")

    End Sub

    Private Sub xr_tbl_csh_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_csh.BeforePrint

        If xr_tbl_mop.Text = "CSH" OrElse xr_tbl_mop.Text = "CS" OrElse xr_tbl_mop.Text = "CA" Then
            xr_tbl_csh.Text = xr_amt.Text
            csh_totals = csh_totals + xr_amt.Text
            rcsh_totals = rcsh_totals + xr_amt.Text
        Else
            xr_tbl_csh.Text = ""
        End If

    End Sub

    Private Sub xr_tbl_amex_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_amex.BeforePrint

        If xr_tbl_mop.Text = "AMX" Then
            xr_tbl_amex.Text = xr_amt.Text
            amex_totals = amex_totals + xr_amt.Text
            ramex_totals = ramex_totals + xr_amt.Text
        Else
            xr_tbl_amex.Text = ""
        End If

    End Sub

    Private Sub xr_tbl_bc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_bc.BeforePrint

        If xr_tbl_mop.Text = "BC" Then
            xr_tbl_bc.Text = xr_amt.Text
            bc_totals = bc_totals + xr_amt.Text
            rbc_totals = rbc_totals + xr_amt.Text
        Else
            xr_tbl_bc.Text = ""
        End If

    End Sub

    Private Sub xr_tbl_check_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_check.BeforePrint

        If xr_tbl_mop.Text = "CK" Then
            xr_tbl_check.Text = xr_amt.Text
            check_totals = check_totals + xr_amt.Text
            rcheck_totals = rcheck_totals + xr_amt.Text
        Else
            xr_tbl_check.Text = ""
        End If

    End Sub

    Private Sub xr_tbl_disc_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_disc.BeforePrint

        If xr_tbl_mop.Text = "DIS" Then
            xr_tbl_disc.Text = xr_amt.Text
            disc_totals = disc_totals + xr_amt.Text
            rdisc_totals = rdisc_totals + xr_amt.Text
        Else
            xr_tbl_disc.Text = ""
        End If

    End Sub

    Private Sub xr_tbl_other_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_other.BeforePrint

        If xr_tbl_mop.Text <> "DIS" And xr_tbl_mop.Text <> "CK" And xr_tbl_mop.Text <> "BC" And xr_tbl_mop.Text <> "AMX" And xr_tbl_mop.Text <> "CSH" And xr_tbl_mop.Text <> "CS" And xr_tbl_mop.Text <> "CA" Then
            xr_tbl_other.Text = xr_amt.Text
            other_totals = other_totals + xr_amt.Text
            rother_totals = rother_totals + xr_amt.Text
        Else
            xr_tbl_other.Text = ""
        End If

    End Sub

    Private Sub xr_slsp_emp_cd_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        If sender.text & "" = "" Then
            xr_slsp_lname.Text = "** No Salesperson Listed **"
        End If

    End Sub

    Private Sub xr_slsp_fname_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp_fname.BeforePrint

        If sender.text & "" = "" Then
            xr_slsp_lname.Text = "** No Salesperson Listed **"
        End If

    End Sub

    Private Sub xr_tbl_nbr_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_tbl_nbr.BeforePrint

        If xr_bnk_crd.Text & "" <> "" Then
            xr_tbl_nbr.Text = xr_bnk_crd.Text
        End If

    End Sub

    Private Sub GroupFooter1_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.AfterPrint

        csh_totals = 0.0
        bc_totals = 0.0
        disc_totals = 0.0
        amex_totals = 0.0
        check_totals = 0.0
        other_totals = 0.0

    End Sub

    Private Sub GroupFooter1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles GroupFooter1.BeforePrint

        Dim xr_cash_sum As XRLabel = GroupFooter1.FindControl("xr_cash_sum", True)
        xr_cash_sum.Text = csh_totals
        Dim xr_bc_sum As XRLabel = GroupFooter1.FindControl("xr_bc_sum", True)
        xr_bc_sum.Text = bc_totals
        Dim xr_disc_sum As XRLabel = GroupFooter1.FindControl("xr_disc_sum", True)
        xr_disc_sum.Text = disc_totals
        Dim xr_amex_sum As XRLabel = GroupFooter1.FindControl("xr_amex_sum", True)
        xr_amex_sum.Text = amex_totals
        Dim xr_check_sum As XRLabel = GroupFooter1.FindControl("xr_check_sum", True)
        xr_check_sum.Text = check_totals
        Dim xr_other_sum As XRLabel = GroupFooter1.FindControl("xr_other_sum", True)
        xr_other_sum.Text = other_totals

    End Sub

    Private Sub ReportFooter_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles ReportFooter.BeforePrint

        xr_r_csh.Text = rcsh_totals
        xr_r_bc.Text = rbc_totals
        xr_r_disc.Text = rdisc_totals
        xr_r_amex.Text = ramex_totals
        xr_r_check.Text = rcheck_totals
        xr_r_other.Text = rother_totals

    End Sub
End Class