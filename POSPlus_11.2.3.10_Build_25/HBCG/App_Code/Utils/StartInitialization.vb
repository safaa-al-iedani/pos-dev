﻿Imports HBCG_Utils
Imports System.Data.OracleClient
Imports System.Collections.Generic


Public Class SessionIntializer

    Public Shared Sub IntializeSession()

        Dim theSalesBiz As SalesBiz = New SalesBiz()
        Dim theTranBiz As TransportationBiz = New TransportationBiz()

        '''''This code is moved from start page to this as per JIRA-2964&3923

        System.Web.HttpContext.Current.Session("MP") = System.DBNull.Value
        System.Web.HttpContext.Current.Session("IP") = System.DBNull.Value

        Dim trnsDt As String = FormatDateTime(Today.Date, 2)
        If Not IsNothing(System.Web.HttpContext.Current.Session("isCommited")) AndAlso Not IsNothing(System.Web.HttpContext.Current.Session("tran_dt")) Then
            trnsDt = System.Web.HttpContext.Current.Session("tran_dt")
        End If

        Clear_Variables()

        System.Web.HttpContext.Current.Session("PD") = ConfigurationManager.AppSettings("p_or_d").ToString
        System.Web.HttpContext.Current.Session("tran_dt") = trnsDt

        'this method updates the session("store_cd")
        HBCG_Utils.ActivateCashier(System.Web.HttpContext.Current.Session("EMP_CD"))
        'if the session session("store_cd") is null after calling the activate cashier, then apply Session("HOME_STORE_CD") to session("Store_cd")
        If System.Web.HttpContext.Current.Session("store_cd") & "" = String.Empty Then
            System.Web.HttpContext.Current.Session("store_cd") = System.Web.HttpContext.Current.Session("HOME_STORE_CD")
        End If

        'Alice updated on June 10,2019 for request 81
        If System.Web.HttpContext.Current.Session("str_co_grp_cd").ToString = "BRK" Then
            If SalesUtils.Validate_Slsp1(System.Web.HttpContext.Current.Session("emp_cd")) = True Then
                System.Web.HttpContext.Current.Session("slsp1") = UCase(System.Web.HttpContext.Current.Session("emp_cd"))
            Else
                System.Web.HttpContext.Current.Session("slsp1") = ""
            End If
        Else
            If SalesUtils.Validate_Slsp(System.Web.HttpContext.Current.Session("emp_cd")) = True Then
                System.Web.HttpContext.Current.Session("slsp1") = UCase(System.Web.HttpContext.Current.Session("emp_cd"))
            Else
                System.Web.HttpContext.Current.Session("slsp1") = ""
            End If
        End If


        System.Web.HttpContext.Current.Session("pct_1") = "100"
        System.Web.HttpContext.Current.Session("ord_tp_cd") = "SAL"
        If ConfigurationManager.AppSettings("default_sort_code").ToString & "" <> "" Then
            System.Web.HttpContext.Current.Session("ord_srt") = ConfigurationManager.AppSettings("default_sort_code").ToString
        End If


        'get store info
        Try
            Dim store As StoreData = theSalesBiz.GetStoreInfo(UCase(System.Web.HttpContext.Current.Session("store_cd")))
            Dim puDelStore As String = theTranBiz.GetPuDelStore(SessVar.wrStoreCd, SessVar.puDel, SessVar.zoneCd)
            'set session variable after reset
            SessVar.puDelStoreCd = puDelStore  ' TODO consider saving store object for session; probably written and puDel - at least a subset
            System.Web.HttpContext.Current.Session("CO_CD") = store.coCd
            SessVar.isArsEnabled = store.enableARS ' based on written store code, may change if cshr activation used and written diff home store 
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Shared Sub Clear_Variables()
        '''''This code is moved from start page to this as per JIRA-2964&3923
        Dim emp_cd, fname, lname, store_cd, IPAD, co_cd As String
        emp_cd = System.Web.HttpContext.Current.Session("EMP_CD")
        co_cd = System.Web.HttpContext.Current.Session("CO_CD")
        fname = System.Web.HttpContext.Current.Session("EMP_FNAME")
        lname = System.Web.HttpContext.Current.Session("EMP_LNAME")
        store_cd = System.Web.HttpContext.Current.Session("HOME_STORE_CD")
        IPAD = System.Web.HttpContext.Current.Session("IPAD")

        If Not IsNothing(System.Web.HttpContext.Current.Session.SessionID.ToString.Trim) Then
            Dim theSalesBiz As SalesBiz = New SalesBiz()
            theSalesBiz.DeleteTempOrder(System.Web.HttpContext.Current.Session.SessionID.ToString.Trim)
        End If
        ' Daniela save clientip and emp_init
        Dim client_IP As String = System.Web.HttpContext.Current.Session("clientip")
        Dim emp_init As String = System.Web.HttpContext.Current.Session("emp_init")
        Dim supUser As String = System.Web.HttpContext.Current.Session("str_sup_user_flag")
        Dim coGrpCd As String = System.Web.HttpContext.Current.Session("str_co_grp_cd")
        Dim userType As String = System.Web.HttpContext.Current.Session("USER_TYPE")
        System.Web.HttpContext.Current.Session.Clear()
        System.Web.HttpContext.Current.Session("clientip") = client_IP
        System.Web.HttpContext.Current.Session("EMP_INIT") = emp_init
        System.Web.HttpContext.Current.Session("str_sup_user_flag") = supUser
        System.Web.HttpContext.Current.Session("str_co_grp_cd") = coGrpCd ' Daniela
        System.Web.HttpContext.Current.Session("USER_TYPE") = userType ' Daniela
        System.Web.HttpContext.Current.Session("EMP_CD") = emp_cd
        System.Web.HttpContext.Current.Session("CO_CD") = co_cd
        System.Web.HttpContext.Current.Session("EMP_FNAME") = fname
        System.Web.HttpContext.Current.Session("EMP_LNAME") = lname
        System.Web.HttpContext.Current.Session("HOME_STORE_CD") = store_cd
        System.Web.HttpContext.Current.Session("IPAD") = IPAD
    End Sub
End Class
