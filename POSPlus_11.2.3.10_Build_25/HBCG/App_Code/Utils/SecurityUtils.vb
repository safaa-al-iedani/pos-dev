﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

''' <summary>
''' Class to encapsulate the security constants used in the application as well as
''' any utility methods related to checking securities.
''' </summary>
Public Class SecurityUtils

    '******SECURITY CODES and descriptions stored in table PRIV ****************
    'Security to override the default refund tender type.
    Public Const REFUND_DEFAULT_MOP_OVERRIDE As String = "ARROVRDEF"

    'Access to enter refunds
    Public Const REFUND_ENTRY As String = "ARRI"

    'Security to refund below the minimum deposit amount of the sale:
    Public Const REFUND_MIN_DUE_OVERRIDE As String = "ARROVRMDUE"
    '***************************************************************************

    'OTHER SECURITY CODES IN THE APP THAT HAVE NOT BEEN REFACTORED TO USE THIS CLASS - description stored in table PRIV
    'Access to view gross margin 
    Public Const ACCESS_GROSS_MARGIN As String = "GMP_ACC"

    'Access to system parameters
    Public Const ACCESS_SYS_PARAMS As String = "SYSPM"

    'Security to create a sales order
    Public Const CREATE_SALES As String = "SAL_CREATE"

    'Security to create a CRM
    Public Const CREATE_CRM As String = "CRM_CREATE"

    'Security to create a MCR
    Public Const CREATE_MCR As String = "MCR_CREATE"

    'Security to create a MDB
    Public Const CREATE_MDB As String = "MDB_CREATE"

    'Security to create/generate a PO
    Public Const CREATE_PO As String = "POGEN"

    Public Const SOM_ACCESS As String = "SOM"

    'Security to override Paid in Full for confirmation logic & ARS ONLY
    Public Const OVERRIDE_PAID_IN_FULL As String = "CONFRM_PIF"

    Public Const OVERRIDE_CONFIRMATION As String = "CONFRM_OVR"

    Public Const UPDATE_CONFIRMATION As String = "CONFRM_UPD"

    Public Const OVERRIDE_TAXCD_ENTRY As String = "PSOETAXCD"

    Public Const OVERRIDE_TAXCD_MGMT As String = "SOMTAXC"

    'Discount Securities
    'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line. The line must be deleted or voided and then a new line must be added.
    'And this security will not allow changes to the qty.
    Public Const PKGCMPT_CHG As String = "CMPNTCHG"

    'This security "PKGCMPT_CHG_QTY" will allow to change the qty alone.    
    Public Const PKGCMPT_CHG_QTY As String = "CMPNTQTY"

    'This security "SOE_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
    Public Const SOE_CHG_PRC As String = "SOERET"

    'This security "SOM_CHG_PRC" for SOM+screen, it will allows only to change retail price for any line except package sku if it is explored.
    Public Const SOM_CHG_PRC As String = "SOMRET"

    ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.The user must have both to be able to change the component price.
    Public Const PKGCMPT_CHG_PRC As String = "CMPNTRET"

    ' This security "PKG_CHG_PRC" is to change the retail price of the Package.This is on top of SOERET.The user must have both to be able to change the component price.
    Public Const PKG_CHG_PRC As String = "PKGRET"

    ' MM-7170
    ' This security "SOM_DEL_CHG" is to allow the user to enter the delivery charge field in SOM+ screen.
    Public Const SOM_DEL_CHG As String = "SOMDELC"

    ' This security "SOM_SETUP_CHG" is to allow the user to enter the set up charge field in Sales Exchange screen.
    Public Const SOM_SETUP_CHG As String = "SOMSETUPC"
    'tHIS SECURITY  enables the package headder to void
    Public Const PKG_HEADDER_VOID As String = "PKGVOID"

    Public Const SOMSCLN As String = "SOMSCLN"

    Public Const SOMSCPCT As String = "SOMSCPCT"
    'This security allows the user to add the war sku in to the order
    Public Const WARRSKUPOS As String = "WARRSKUPOS"
    'This security allows the user to add the war sku in to the order
    Public Const WARRSKUSOE As String = "WARRSKUSOE"
    'TODO - other sec codes found in the app that need appropriate names based on function
    '"SOMTAXC"
    '"SOMF"
    '"SOMV"
    '"SOMSCPCT"
    '"SOCTRK"
    '"PSOETAXCD"
    '"WBQ"
    '"ITEM"
    '"CPTR_PCST"
    '"OLPM"
    '"ISTM"
    '"CUSM"
    '"MCM"
    '"CCRC"
    '"SBOB"
    '"CSHA"
    '"ISTM"
    '"POMA"
    '"AIQ"
    '"SORW"
    '"INAV"
    '"SOM"
    '"OCINQ"
    '"IPRT"
    '"EEXI"
    '"POA"
    '"ARUTI"

    ''' <summary>
    ''' Determines if the employee passed-in has the sepcified security or not.  True indicates
    ''' that either the employee has all securities(by virtue of the override flag) or has the
    ''' security passed-in either in a group or individually
    ''' </summary>
    ''' <param name="sec_cd">the security code to check</param>
    ''' <param name="emp_cd">the employee code of the empoyee to find the security for.</param>
    ''' <returns> True to indicate that the employee has the security passed-in.
    '''           false otherwise.</returns>
    Public Shared Function hasSecurity(ByVal sec_cd As String, ByVal emp_cd As String) As Boolean

        Dim hasSec As Boolean = False

        'If user is setup to override all securities
        If (SystemUtils.HasSecurityOverride(emp_cd)) Then

            hasSec = True

        Else

            'first check if sec code exists in sec group for the employee
            Dim sql As String = "SELECT SEC_GRP2EMP.EMP_CD, SEC_GRP2PRIV.SEC_CD " &
                                "FROM SEC_GRP2EMP, SEC_GRP2PRIV, EMP " &
                                "WHERE SEC_GRP2EMP.SEC_GRP_CD = SEC_GRP2PRIV.SEC_GRP_CD " &
                                "AND SEC_GRP2PRIV.SEC_CD = :secCd " &
                                "AND SEC_GRP2EMP.EMP_CD=EMP.EMP_CD AND EMP.EMP_CD=:empCd "

            If (SecurityCodeExists(sql, sec_cd, emp_cd)) Then

                hasSec = True

            Else
                'if the sec code was not found in the sec group, check for individual emp privs
                sql = "SELECT EMP2PRIV.EMP_CD, EMP2PRIV.SEC_CD " &
                          "FROM EMP2PRIV, EMP " &
                          "WHERE EMP2PRIV.SEC_CD= :secCd " &
                           "AND EMP2PRIV.EMP_CD=EMP.EMP_CD " &
                           "AND EMP.EMP_CD= :empCd"

                If (SecurityCodeExists(sql, sec_cd, emp_cd)) Then

                    hasSec = True
                End If
            End If
        End If

        Return hasSec

    End Function

    ''' <summary>
    ''' Determines if the employee passed-in has the sepcified security or not.A "Y" indicates
    ''' that either the employee has all securities(by virtue of the override flag) or has the
    ''' security passed-in.
    ''' </summary>
    ''' <param name="sec_cd">the security code to check</param>
    ''' <param name="emp_cd">the employee code of the empoyee to find the security for.</param>
    ''' <returns> "Y" to indicate that either the employee has the security passed-in.
    '''           "N" otherwise.</returns>
    Private Shared Function Find_Security(ByVal sec_cd As String, ByVal emp_cd As String)

        Find_Security = "N"

        If hasSecurity(sec_cd, emp_cd) Then

            Find_Security = "Y"
        End If

    End Function

    ''' <summary>
    ''' Determines if the employee passed-in has the sepcified security or not. A value of 'true'
    ''' indicates that the employee has the security passed-in.
    ''' </summary>
    ''' <param name="sql">the sql to execute</param>
    ''' <param name="sec_cd">the security code to check</param>
    ''' <param name="emp_cd">the employee code of the empoyee to find the security for.</param>
    ''' <returns>true if the employee has the security passed-in</returns>
    Private Shared Function SecurityCodeExists(ByVal sql As String,
                                               ByVal secCode As String,
                                               ByVal empCode As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim dataRdr As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql)

        Try
            conn.Open()
            cmd.Parameters.Add(":secCd", OracleType.VarChar)
            cmd.Parameters(":secCd").Value = secCode
            cmd.Parameters.Add(":empCd", OracleType.VarChar)
            cmd.Parameters(":empCd").Value = empCode + ""
            cmd.Connection = conn
            dataRdr = DisposablesManager.BuildOracleDataReader(cmd)


            'if the sec code passed-in is found, then return true
            Do While dataRdr.Read
                rtnVal = dataRdr.Item("SEC_CD").ToString.isNotEmpty
            Loop
            dataRdr.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function

End Class
