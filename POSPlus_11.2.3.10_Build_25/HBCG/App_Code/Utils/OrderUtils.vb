﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Linq

Public Class OrderUtils

    ''' <summary>
    ''' object for session variable of order slsp info
    ''' </summary>
    Public Class SlspCommissionInfo
        Property slsp1 As String
        Property slsp2 As String
        Property pct1 As Double
        Property pct2 As Double
    End Class

    ''' <summary>
    ''' Validates and reformats the quantity provided; validates fractionals, and > 1 for warranty and outlet
    ''' </summary>
    ''' <param name="qtyStr">quantity string</param>
    ''' <param name="itmTpCd">item type code</param>
    ''' <param name="warrRowLink">warranty link; if one to one warr syspm, then quantity must be 1; if this is blank, then doesn't matter </param>
    ''' <param name="outId">outlet ID; if outlet entry, then quantity must be 1; if this is blank, then doesn't matter</param>
    ''' <returns>the dataset of extended warranties for the SKU input </returns>
    ''' <remarks>If you need the calendar price, then you must use this method and provide the data for the calendar pricing  </remarks>
    Public Shared Function ValAndFormatQty(ByRef qtyStr As String, ByVal prevQty As String, ByVal itmTpCd As String, ByVal warrRowLink As String, ByVal outId As String) As String
        '  extract and return the basic item information

        Dim qtyValidMsg As String = ""

        If (Not String.IsNullOrEmpty(qtyStr) AndAlso SystemUtils.isNumber(qtyStr)) Then

            Dim qty As Double = CDbl(qtyStr)
            If qty < 0 Then
                qty = qty * -1
                qtyStr = qty.ToString
            End If

            If SkuUtils.ITM_TP_CPT.Equals(itmTpCd) Then

                qtyStr = FormatNumber(qtyStr, 2)

            ElseIf InStr(qtyStr, ".") > 0 Then

                qtyStr = FormatNumber(prevQty, 0)
                qtyValidMsg = "Fractional quantities are only allowed for carpet SKUs.  Quantity reset."

            ElseIf qty = 0 Then
                qtyStr = prevQty
                qtyValidMsg = "Please delete the item; changing quantity to zero will not void the line"

            ElseIf qty > 1 Then

                If (Not String.IsNullOrWhiteSpace(outId)) Then ' if outlet then qty must be 1

                    qtyValidMsg = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    qtyStr = FormatNumber(1, 0)

                ElseIf itmTpCd = SkuUtils.ITM_TP_WAR Then ' warranties still sold at qty 1

                    qtyValidMsg = "Quantity must equal 1 for warranty SKUs. Quantity set to 1."
                    qtyStr = FormatNumber(1, 0)

                ElseIf (Not (SysPms.isOne2ManyWarr OrElse String.IsNullOrWhiteSpace(warrRowLink))) Then ' if 1 to many or not linked, OK

                    qtyValidMsg = "Quantity must equal 1 when a warranty is attached to a SKU. Quantity set to 1."
                    qtyStr = FormatNumber(1, 0)
                Else
                    qtyStr = FormatNumber(qty, 0)
                End If

            End If

        Else
            qtyValidMsg = "Quantity must be a valid number. Quantity restored."
            qtyStr = FormatNumber(prevQty, 0)
        End If

        Return qtyValidMsg

    End Function

    ''' <summary>
    ''' Extracts by SKU, the quantities returned against the input sales document
    ''' </summary>
    ''' <param name="docNum">original del_Doc_num for a return or MCR</param>
    ''' <returns>DataSet containing the set of previously returned SKUs to the original sale order</returns>
    ''' <remarks></remarks>
    Public Shared Function getMaxReturnQtyList4NewCrms(ByVal docNum As String) As DataSet

        Dim datSet As New DataSet
        Dim sqlStmtSb As New StringBuilder

        sqlStmtSb.Append("SELECT SL.ITM_CD, NVL(SUM(NVL(qty, 0)),0) AS crm_qty FROM so_ln sl, so WHERE (sl.del_Doc_num = so.del_Doc_num) AND sl.void_flag != 'Y' ").Append(
             "AND so.stat_cd != 'V' || '' AND so.ord_tp_cd = 'CRM' AND SO.orig_DEL_DOC_NUM= :docNum ")
        sqlStmtSb.Append("GROUP BY ITM_CD ORDER BY ITM_CD ")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
              cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

            cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":docNum").Value = UCase(docNum)
            oDatAdap.Fill(datSet)
        End Using

        Return datSet
    End Function

    ''' <summary>
    ''' Extracts by SKU, the quantities returned and retail credited against the input sales document
    ''' </summary>
    ''' <param name="docNum">original del_Doc_num for a return (CRM), MCR or MDB</param>
    ''' <returns>DataSet containing the set of previously returned SKU quantity and credited retail to the original sale order</returns>
    ''' <remarks>Includes all documents for the original sale; to exclude current doc, use overload with additional parameter</remarks>
    Public Shared Function getMaxCreditListByLn(ByVal docNum As String) As DataSet

        Dim datSet As New DataSet
        Dim sqlStmtSb As New StringBuilder

        'extract available retail by SKU and orig line number for the original document number
        sqlStmtSb.Append("SELECT itm_cd, del_doc_ln# AS lnNum, '' AS salUnitPrc, ")
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.REGEXP) Then
            sqlStmtSb.Append("REGEXP_SUBSTR(sal1, '[^^]+', 1, 1) AS salTot, REGEXP_SUBSTR(crm1, '[^^]+', 1, 1) AS credTot, ").Append(
                    "REGEXP_SUBSTR(sal1, '[^^]+', 1, 2) AS salQty, REGEXP_SUBSTR(crm1, '[^^]+', 1, 2) AS crmQty ")
        Else
            sqlStmtSb.Append("SUBSTR(sal1, 1, INSTR(sal1, '^', 1, 1) -1) salTot, SUBSTR(crm1, 1, INSTR(crm1, '^', 1, 1) -1) credTot, ").Append(
                "SUBSTR(sal1, INSTR(sal1, '^', 1, 1) +1) salQty, SUBSTR(crm1, INSTR(crm1, '^', 1, 1) +1) crmQty ")
        End If
        sqlStmtSb.Append("FROM (SELECT sl.itm_cd,  del_doc_ln#, ").Append(
                                "(SELECT SUM(NVL(unit_prc * qty, 0)) || '^' || SUM(NVL(qty, 0)) sal1  ").Append(
                                     "FROM so_ln slS, so soS ").Append(
                                    "WHERE slS.del_Doc_num = soS.del_Doc_num AND void_flag != 'Y' ").Append(
                                    "AND sl.itm_cd = slS.itm_cd and sl.del_doc_ln# = slS.del_Doc_ln# AND soS.del_doc_num = :docNum GROUP BY ITM_CD, del_doc_ln# ) sal1, ").Append(
                                 "NVL( (SELECT SUM(DECODE(ord_tp_cd, 'MDB', -NVL(unit_prc * qty, 0), NVL(unit_prc * qty, 0))) || '^' || SUM(DECODE(ord_tp_cd, 'CRM', NVL(qty, 0), 0)) crm1 ").Append(
                                        "FROM so_ln slC, so soC ").Append(
                                        "WHERE slC.del_Doc_num = soC.del_Doc_num AND void_flag != 'Y' AND soC.stat_cd != 'V' || '' AND ord_tp_cd IN ('CRM', 'MCR', 'MDB') ").Append(
                                        "AND sl.itm_cd = slC.itm_cd AND sl.del_doc_ln# = slC.alt_doc_ln# and orig_del_doc_num = :docNum GROUP BY ITM_CD, alt_Doc_ln#  ),'0^0') crm1 ").Append(
                            "FROM so_ln sl WHERE del_doc_num = :docNum AND void_flag != 'Y'  GROUP BY sl.itm_cd, del_doc_ln# order by itm_cd, del_Doc_ln# ) ")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
              cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

            cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":docNum").Value = UCase(docNum)
            oDatAdap.Fill(datSet)
        End Using

        Return datSet
    End Function

    ''' <summary>
    ''' This method will return maximum available credit for MCR Item
    ''' </summary>
    ''' <param name="itemCode">item code</param>
    ''' <param name="custCode">customer code</param>
    ''' <param name="originalDocumentNumber">original SALE number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMaxCreditforMCRItem(ByVal itemCode As String, ByVal custCode As String, ByVal originalDocumentNumber As String) As DataSet

        Dim datSet As New DataSet
        Dim oDatAdap As OracleDataAdapter
        Dim sqlStmtSb As New StringBuilder

        Dim dbConnection As OracleConnection
        Dim dbCommand As OracleCommand

        Try
            sqlStmtSb.Append(" SELECT   '' SALUNITPRC, ")
            sqlStmtSb.Append(" SUM(DECODE ( ")
            sqlStmtSb.Append(" ORD_TP_CD, ")
            sqlStmtSb.Append(" 'MDB', ")
            sqlStmtSb.Append(" ROUND (NVL ( (UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'SAL', ")
            sqlStmtSb.Append(" ROUND (NVL ( (UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0))) SALTOT,  ")
            sqlStmtSb.Append(" SUM(DECODE ( ")
            sqlStmtSb.Append(" ORD_TP_CD, ")
            sqlStmtSb.Append(" 'MDB', ")
            sqlStmtSb.Append(" ROUND (NVL ( (UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'SAL', ")
            sqlStmtSb.Append(" ROUND (NVL ( (UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'MCR', ")
            sqlStmtSb.Append(" ROUND(NVL((-1 * UNIT_PRC * QTY), 0), 2) ")
            sqlStmtSb.Append(" - NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'CRM', ")
            sqlStmtSb.Append(" ROUND(NVL((-1 * UNIT_PRC * QTY), 0), 2) ")
            sqlStmtSb.Append(" - NVL (CUST_TAX_CHG, 0))) ")
            sqlStmtSb.Append(" AVAL_CREDIT, ")
            sqlStmtSb.Append(" SUM (DECODE (ORD_TP_CD, 'SAL', NVL (QTY, 0), 0)) SALQTY, ")
            sqlStmtSb.Append(" SUM (DECODE (ORD_TP_CD, 'CRM', NVL (QTY, 0), 0)) CRMQTY ")
            sqlStmtSb.Append(" FROM   SO_LN SL, SO S ")
            sqlStmtSb.Append(" WHERE SL.ITM_CD || '' = :ITM_CD ")
            sqlStmtSb.Append(" AND SL.VOID_FLAG = 'N' ")
            sqlStmtSb.Append(" AND SL.DEL_DOC_NUM = S.DEL_DOC_NUM ")
            sqlStmtSb.Append(" AND S.STAT_CD != 'V' ")
            sqlStmtSb.Append(" AND S.CUST_CD || '' = :CUST_CD ")
            sqlStmtSb.Append(" AND ( (S.DEL_DOC_NUM = :ORIG_DEL_DOC_NUM AND ORD_TP_CD = 'SAL') ")
            sqlStmtSb.Append(" OR (S.ORIG_DEL_DOC_NUM = :ORIG_DEL_DOC_NUM ")
            sqlStmtSb.Append("  AND ORD_TP_CD IN ('MDB', 'MCR', 'CRM'))) ")


            dbConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            dbCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, dbConnection)
            oDatAdap = DisposablesManager.BuildOracleDataAdapter(dbCommand)

            dbCommand.Parameters.Add(":ITM_CD", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":ITM_CD").Value = Convert.ToString(UCase(itemCode))

            dbCommand.Parameters.Add(":CUST_CD", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = Convert.ToString(UCase(custCode))

            dbCommand.Parameters.Add(":ORIG_DEL_DOC_NUM", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":ORIG_DEL_DOC_NUM").Value = Convert.ToString(UCase(originalDocumentNumber))
            oDatAdap.Fill(datSet)
        Catch ex As Exception
            Throw
        Finally
            oDatAdap.Dispose()
            dbConnection.Dispose()
            dbCommand.Dispose()
        End Try

        Return datSet
    End Function

    ''' <summary>
    ''' This method will return other chages like Delivery, setup, taxes for MCR document from the previously created SAL, MDB, CRM, MCR 
    ''' </summary>
    ''' <param name="custCode">customer code</param>
    ''' <param name="originalDocumentNumber">original SALE number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMaxOtherChargesforMCR(ByVal custCode As String, ByVal originalDocumentNumber As String) As Decimal
        Dim sqlStmtSb As New StringBuilder
        Dim dbConnection As OracleConnection
        Dim dbCommand As OracleCommand
        Dim otherCharges As Decimal = 0.0
        Try
            sqlStmtSb.Append(" SELECT NVL(SUM(DECODE ( ")
            sqlStmtSb.Append(" ORD_TP_CD, ")
            sqlStmtSb.Append(" 'MDB', ")
            sqlStmtSb.Append(" NVL (DEL_CHG, 0) + NVL (SETUP_CHG, 0) + NVL (TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'SAL', ")
            sqlStmtSb.Append("  NVL (DEL_CHG, 0) + NVL (SETUP_CHG, 0) + NVL (TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'MCR', -1 ")
            sqlStmtSb.Append(" * (NVL (DEL_CHG, 0) + NVL (SETUP_CHG, 0) + NVL (TAX_CHG, 0)), ")
            sqlStmtSb.Append(" 'CRM', -1 ")
            sqlStmtSb.Append("  * (NVL (DEL_CHG, 0) + NVL (SETUP_CHG, 0) + NVL (TAX_CHG, 0)))),0) OTHER_CHARGES ")
            sqlStmtSb.Append(" FROM   SO S ")
            sqlStmtSb.Append(" WHERE   S.STAT_CD != 'V' AND S.CUST_CD || '' = :CUST_CD ")
            sqlStmtSb.Append(" AND ( (S.DEL_DOC_NUM = :ORIG_DEL_DOC_NUM AND ORD_TP_CD = 'SAL') ")
            sqlStmtSb.Append(" OR (S.ORIG_DEL_DOC_NUM = :ORIG_DEL_DOC_NUM ")
            sqlStmtSb.Append(" AND ORD_TP_CD IN ('MDB', 'MCR', 'CRM'))) ")

            dbConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, dbConnection)

            dbCommand.Parameters.Add(":CUST_CD", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = Convert.ToString(UCase(custCode))

            dbCommand.Parameters.Add(":ORIG_DEL_DOC_NUM", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":ORIG_DEL_DOC_NUM").Value = Convert.ToString(UCase(originalDocumentNumber))

            otherCharges = Convert.ToDecimal(dbCommand.ExecuteScalar())

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Dispose()
            dbCommand.Dispose()
        End Try
        Return otherCharges
    End Function

    ''' <summary>
    ''' This method will return maximum available credit for MCR document
    ''' </summary>
    ''' <param name="custCode">customer code</param>
    ''' <param name="originalDocumentNumber">original SALE number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMaxAvailableCreditforMCR(ByVal custCode As String, ByVal originalDocumentNumber As String) As Decimal
        Dim sqlStmtSb As New StringBuilder
        Dim dbConnection As OracleConnection
        Dim dbCommand As OracleCommand
        Dim availableCredit As Decimal = 0.0
        Try
            sqlStmtSb.Append(" SELECT NVL(SUM(DECODE ( ")
            sqlStmtSb.Append(" ORD_TP_CD, ")
            sqlStmtSb.Append(" 'MDB', ")
            sqlStmtSb.Append(" ROUND (NVL((UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'SAL', ")
            sqlStmtSb.Append("  ROUND (NVL((UNIT_PRC * QTY), 0), 2) + NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'MCR', ")
            sqlStmtSb.Append(" ROUND(NVL((-1 * UNIT_PRC * QTY), 0), 2)- NVL (CUST_TAX_CHG, 0), ")
            sqlStmtSb.Append(" 'CRM', ")
            sqlStmtSb.Append("  ROUND(NVL((-1 * UNIT_PRC * QTY), 0), 2) - NVL (CUST_TAX_CHG, 0))),0) AVAL_CREDIT ")
            sqlStmtSb.Append(" FROM SO_LN SL, SO S ")
            sqlStmtSb.Append(" WHERE SL.VOID_FLAG = 'N' ")
            sqlStmtSb.Append(" AND SL.DEL_DOC_NUM = S.DEL_DOC_NUM ")
            sqlStmtSb.Append(" AND S.STAT_CD != 'V' AND S.CUST_CD || '' = :CUST_CD ")
            sqlStmtSb.Append(" AND ( (S.DEL_DOC_NUM = :ORIG_DEL_DOC_NUM AND ORD_TP_CD = 'SAL') ")
            sqlStmtSb.Append(" OR (S.ORIG_DEL_DOC_NUM = :ORIG_DEL_DOC_NUM AND ORD_TP_CD IN ('MDB', 'MCR', 'CRM'))) ")


            dbConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, dbConnection)

            dbCommand.Parameters.Add(":CUST_CD", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = Convert.ToString(UCase(custCode))

            dbCommand.Parameters.Add(":ORIG_DEL_DOC_NUM", System.Data.OracleClient.OracleType.VarChar)
            dbCommand.Parameters(":ORIG_DEL_DOC_NUM").Value = Convert.ToString(UCase(originalDocumentNumber))

            availableCredit = Convert.ToDecimal(dbCommand.ExecuteScalar())

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Dispose()
            dbCommand.Dispose()
        End Try
        Return availableCredit
    End Function

    ''' <summary>
    ''' Extracts by SKU, the quantities returned and retail credited against the input sales document but separates out the qty and amt for the current document processing
    ''' </summary>
    ''' <param name="docNum">original del_Doc_num for a return (CRM), MCR or MDB</param>
    ''' <returns>DataSet containing the set of previously returned SKU quantity and credited retail to the original sale order</returns>
    ''' <remarks>Used for existing document for say, SOM</remarks>
    ' TODO - try to merge these but no time now
    Public Shared Function getMaxCreditListByLn(ByVal origDocNum As String, ByVal currDocNum As String, Optional ByVal onlyFinal As Boolean = False) As DataSet

        Dim datSet As New DataSet
        Dim sqlStmtSb As New StringBuilder

        Dim statusClause As String = IIf(onlyFinal, " = 'F'", " != 'V' ")

        'extract available retail by SKU and orig line number for the original document number
        sqlStmtSb.Append("SELECT itm_cd, del_doc_ln# AS lnNum, unit_prc As salUnitPrc, ")
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.REGEXP) Then
            sqlStmtSb.Append("REGEXP_SUBSTR(sal1, '[^^]+', 1, 1) AS salTot, REGEXP_SUBSTR(crm1, '[^^]+', 1, 1) AS credTot, ").Append(
                    "REGEXP_SUBSTR(sal1, '[^^]+', 1, 2) AS salQty, REGEXP_SUBSTR(crm1, '[^^]+', 1, 2) AS crmQty, ").Append(
                    "REGEXP_SUBSTR(thisCred, '[^^]+', 1, 1) AS thisCredTot, REGEXP_SUBSTR(thisCred, '[^^]+', 1, 2) AS thisCrmQty ")
        Else
            sqlStmtSb.Append("SUBSTR(sal1, 1, INSTR(sal1, '^', 1, 1) -1) salTot, SUBSTR(crm1, 1, INSTR(crm1, '^', 1, 1) -1) credTot, ").Append(
                "SUBSTR(sal1, INSTR(sal1, '^', 1, 1) +1) salQty, SUBSTR(crm1, INSTR(crm1, '^', 1, 1) +1) crmQty, ").Append(
                "SUBSTR(thisCred, INSTR(thisCred, '^', 1, 1) +1) thisCredTot, SUBSTR(thisCred, INSTR(thisCred, '^', 1, 1) +1) thisCrmQty ")
        End If

        sqlStmtSb.Append("FROM (SELECT sl.itm_cd,  del_doc_ln#, sl.unit_prc, ").Append(
                                "(SELECT SUM(NVL(unit_prc * qty, 0)) || '^' || SUM(NVL(qty, 0)) sal1  ").Append(
                                        "FROM so_ln slS, so soS ").Append(
                                    "WHERE slS.del_Doc_num = soS.del_Doc_num AND void_flag != 'Y' ").Append(
                                        "AND sl.itm_cd = slS.itm_cd and sl.del_doc_ln# = slS.del_Doc_ln# AND soS.del_doc_num = :origDocNum GROUP BY ITM_CD, del_doc_ln# ) sal1, ").Append(
                                 "NVL( (SELECT SUM(DECODE(ord_tp_cd, 'MDB', -NVL(unit_prc * qty, 0), NVL(unit_prc * qty, 0))) || '^' || SUM(DECODE(ord_tp_cd, 'CRM', NVL(qty, 0), 0)) crm1 ").Append(
                                            "FROM so_ln slC, so soC ").Append(
                                        "WHERE slC.del_Doc_num = soC.del_Doc_num AND void_flag != 'Y' AND soC.stat_cd " + statusClause + " || '' AND ord_tp_cd IN ('CRM', 'MCR', 'MDB') ").Append(
                                            "AND sl.itm_cd = slC.itm_cd AND sl.del_doc_ln# = slC.alt_doc_ln# AND orig_del_doc_num = :origDocNum AND soC.del_Doc_num != :currDocNum ").Append(
                                            "GROUP BY ITM_CD, alt_Doc_ln#  ),'0^0') crm1, ").Append(
                                 "NVL( (SELECT SUM( NVL(unit_prc * qty, 0)) || '^' || SUM(qty) thisCred ").Append(
                                            "FROM so_ln slT, so soT ").Append(
                                        "WHERE slT.del_Doc_num = soT.del_Doc_num AND void_flag != 'Y' AND soT.stat_cd " + statusClause + " || '' AND ord_tp_cd = 'CRM' ").Append(
                                            "AND sl.itm_cd = slT.itm_cd AND sl.del_doc_ln# = slT.alt_doc_ln# AND orig_del_doc_num = :origDocNum ").Append(
                                            "AND soT.del_doc_num = :currDocNum GROUP BY ITM_CD, alt_Doc_ln#  ),'0^0') thisCred ").Append(
                               "FROM so_ln sl WHERE del_doc_num = :origDocNum AND void_flag != 'Y'  GROUP BY sl.itm_cd, del_doc_ln#, unit_prc order by itm_cd, del_Doc_ln# ) ")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
              cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

            cmd.Parameters.Add(":origDocNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":origDocNum").Value = UCase(origDocNum)
            cmd.Parameters.Add(":currDocNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":currDocNum").Value = UCase(currDocNum)
            oDatAdap.Fill(datSet)
        End Using

        Return datSet
    End Function

    ''' <summary>
    ''' Extracts by SKU, the quantities returned and retail credited against the input sales document
    ''' </summary>
    ''' <param name="docNum">original del_Doc_num for a return (CRM), MCR or MDB</param>
    ''' <returns>DataSet containing the set of previously returned SKU quantity and credited retail to the original sale order</returns>
    ''' <remarks></remarks>
    Public Shared Function getMaxCreditListBySku(ByVal docNum As String) As DataSet

        Dim datSet As New DataSet
        Dim sqlStmtSb As New StringBuilder

        'extract available retail by SKU and orig line number for the original document number
        sqlStmtSb.Append("SELECT itm_cd, ")
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.REGEXP) Then
            sqlStmtSb.Append("REGEXP_SUBSTR(sal1, '[^^]+', 1, 1) AS salTot, REGEXP_SUBSTR(crm1, '[^^]+', 1, 1) AS credTot, ").Append(
                    "REGEXP_SUBSTR(sal1, '[^^]+', 1, 2) AS salQty, REGEXP_SUBSTR(crm1, '[^^]+', 1, 2) AS crmQty ")
        Else
            sqlStmtSb.Append("SUBSTR(sal1, 1, INSTR(sal1, '^', 1, 1) -1) salTot, SUBSTR(crm1, 1, INSTR(crm1, '^', 1, 1) -1) credTot, ").Append(
                "SUBSTR(sal1, INSTR(sal1, '^', 1, 1) +1) salQty, SUBSTR(crm1, INSTR(crm1, '^', 1, 1) +1) crmQty ")
        End If
        sqlStmtSb.Append("FROM (SELECT sl.itm_cd, ").Append(
                                "(SELECT SUM(NVL(unit_prc * qty, 0)) || '^' || SUM(NVL(qty, 0)) sal1  ").Append(
                                     "FROM so_ln slS, so soS ").Append(
                                    "WHERE slS.del_Doc_num = soS.del_Doc_num AND void_flag != 'Y' ").Append(
                                    "AND sl.itm_cd = slS.itm_cd AND soS.del_doc_num = :docNum GROUP BY ITM_CD) sal1, ").Append(
                                 "NVL( (SELECT SUM(DECODE(ord_tp_cd, 'MDB', -NVL(unit_prc * qty, 0), NVL(unit_prc * qty, 0))) || '^' || SUM(DECODE(ord_tp_cd, 'CRM', NVL(qty, 0), 0)) crm1 ").Append(
                                        "FROM so_ln slC, so soC ").Append(
                                        "WHERE slC.del_Doc_num = soC.del_Doc_num AND void_flag != 'Y' AND soC.stat_cd != 'V' || '' AND ord_tp_cd IN ('CRM', 'MCR', 'MDB') ").Append(
                                        "AND sl.itm_cd = slC.itm_cd and orig_del_doc_num = :docNum GROUP BY ITM_CD  ),'0^0') crm1 ").Append(
                            "FROM so_ln sl WHERE del_doc_num = :docNum AND void_flag != 'Y'  GROUP BY sl.itm_cd  ORDER BY itm_cd ) ")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
              cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

            cmd.Parameters.Add(":docNum", System.Data.OracleClient.OracleType.VarChar)
            cmd.Parameters(":docNum").Value = UCase(docNum)
            oDatAdap.Fill(datSet)
        End Using

        Return datSet
    End Function

    ''' <summary>
    ''' Compares by SKU, the quantities returned and retail credited against the original sales document input
    ''' </summary>
    ''' <param name="docNum">original del_Doc_num for a return (CRM), MCR or MDB</param>
    ''' <param name="ordTpCd">order type code such as CRM or MCR</param>
    ''' <param name="prcColName">name for the price column in the input table, for example, ret_prc for temp_itm and unit_prc for so_ln</param>
    ''' <param name="wipLines">dataset of the work in process information to check quantity, price by SKU</param>
    ''' <returns>error message string which will be empty if there were no errors found</returns>
    ''' <remarks>Input dataset can be any dataset but rows must have columns specifically named QTY, ITM_CD and entry in proColName for the retail price</remarks>
    Public Shared Function confirmMaxCredit(ByVal docNum As String, ByVal ordTpCd As String, ByVal prcColName As String, ByRef wipLines As DataSet) As String

        Dim errMsgSb As New StringBuilder
        errMsgSb.Append("")

        If SystemUtils.dataSetHasRows(wipLines) AndAlso SystemUtils.dataRowIsNotEmpty(wipLines.Tables(0).Rows(0)) Then

            Dim wipCredTbl As DataTable = wipLines.Tables(0)
            Dim wipRowCnt As Double = wipCredTbl.Rows.Count

            ' list by SKU of sale quantity and extended totals and credit total extended amount and CRM quantity (handles multiple of same SKU on the sale and/or credit)
            Dim maxBySku As DataSet = getMaxCreditListBySku(docNum)

            ' get the sales line info by SKU and the credit info by SKU; used to catch those that do not have original lines
            If SystemUtils.dataSetHasRows(maxBySku) AndAlso SystemUtils.dataRowIsNotEmpty(maxBySku.Tables(0).Rows(0)) Then

                Dim skuIndx As Integer = 0
                Dim skuCredTbl As DataTable = maxBySku.Tables(0)
                skuCredTbl.Columns.Add("UsedQty", Type.GetType("System.Double"))
                skuCredTbl.Columns.Add("UsedCred", Type.GetType("System.Double"))
                Dim skuRowCnt As Double = skuCredTbl.Rows.Count
                Dim maxQty, maxCred As Double

                For Each wipRow In wipCredTbl.Rows

                    skuIndx = 0
                    Do While skuIndx <= (skuRowCnt - 1) AndAlso skuCredTbl.Rows(skuIndx)("itm_cd") <> wipRow.item("itm_cd")
                        skuIndx = skuIndx + 1
                    Loop

                    If skuIndx <= (skuRowCnt - 1) AndAlso skuCredTbl.Rows(skuIndx)("itm_cd") = wipRow.item("itm_cd") Then

                        If IsNothing(skuCredTbl.Rows(skuIndx)("UsedQty")) OrElse Not IsNumeric(skuCredTbl.Rows(skuIndx)("UsedQty")) Then
                            skuCredTbl.Rows(skuIndx)("UsedQty") = 0.0
                        End If
                        If IsNothing(skuCredTbl.Rows(skuIndx)("UsedCred")) OrElse Not IsNumeric(skuCredTbl.Rows(skuIndx)("UsedCred")) Then
                            skuCredTbl.Rows(skuIndx)("UsedCred") = 0.0
                        End If
                        skuCredTbl.Rows(skuIndx)("UsedQty") = skuCredTbl.Rows(skuIndx)("UsedQty") + wipRow("qty")
                        skuCredTbl.Rows(skuIndx)("UsedCred") = Math.Round(skuCredTbl.Rows(skuIndx)("UsedCred") + (wipRow.item(prcColName) * wipRow.item("qty")), SystemUtils.CurrencyPrecision)
                    End If
                Next

                For Each skuRow In skuCredTbl.Rows

                    maxQty = skuRow("salQty") - skuRow("CrmQty")
                    maxCred = skuRow("salTot") - skuRow("credTot")
                    If AppConstants.Order.TYPE_CRM.Equals(ordTpCd) AndAlso (Not IsNothing(skuRow("UsedQty"))) AndAlso IsNumeric(skuRow("UsedQty")) AndAlso skuRow("UsedQty") > maxQty Then

                        errMsgSb.Append("SKU " & skuRow("Itm_Cd") & " return quantity exceeded by " & (skuRow("UsedQty") - maxQty) & ", please adjust. " + vbCrLf)
                    End If
                    If (Not IsNothing(skuRow("UsedCred"))) AndAlso IsNumeric(skuRow("UsedCred")) AndAlso skuRow("UsedCred") > maxCred Then

                        errMsgSb.Append("SKU " & skuRow("Itm_Cd") & " retail credit exceeded by " & (skuRow("UsedCred") - maxCred) & ", please adjust. " + vbCrLf)
                    End If
                Next

            End If
        End If

        Return errMsgSb.ToString
    End Function

    Public Class MaxCredResponse

        Property maxUnitRetail As Double
        Property maxQty As Double
    End Class

    ''' <summary>
    ''' Determines the maximum retail that is left to credit for a particular SKU CRM/MCR line
    ''' </summary>
    ''' <param name="itmCd">item code setting max retail for</param>
    ''' <param name="soLnNum">original line number setting max retail unit credit for</param>
    ''' <param name="origUnitPrc">original line retail price per unit</param>
    ''' <param name="ordTpCd">order type of credit being processed (CRM/MCR)</param>
    ''' <param name="credRetDatSet">DataSet containing the set of available credited amounts by SKU to the original sale order; SKU avail retail price is updated to reflect the used credit value</param>
    ''' <returns>the maximum unit retail credit available to return or credit on this line and the maximum quantity to return</returns>
    ''' <remarks></remarks>
    Public Shared Function getLnMaxCredit(ByVal itmCd As String, ByVal origSoLnNum As Integer, ByVal origUnitPrc As Double, ByVal ordTpCd As String, ByRef credRetDatSet As DataSet) As MaxCredResponse

        Dim maxCred As New MaxCredResponse
        maxCred.maxUnitRetail = origUnitPrc ' this will be the remaining credit to use as the unit retail for MCR or new CRM; for existing CRM, from the dataset orig info
        maxCred.maxQty = 0

        ' find the matching original row 
        If SystemUtils.dataSetHasRows(credRetDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(credRetDatSet.Tables(0).Rows(0)) Then

            Dim credRetTbl As DataTable = credRetDatSet.Tables(0)
            Dim rowCnt As Double = credRetTbl.Rows.Count

            Dim indx As Integer = 0
            Do While indx <= (rowCnt - 1) AndAlso credRetTbl.Rows(indx)("lnNum") <> origSoLnNum
                indx = indx + 1
            Loop

            If indx <= (rowCnt - 1) AndAlso credRetTbl.Rows(indx)("lnNum") = origSoLnNum AndAlso credRetTbl.Rows(indx)("itm_cd") = itmCd Then

                If credRetTbl.Rows(indx)("salQty") > 0.00001 Then ' sale with negative qty (<) should not happen

                    If (Not IsNothing(credRetTbl.Rows(indx)("salUnitPrc"))) AndAlso IsNumeric(credRetTbl.Rows(indx)("salUnitPrc")) Then
                        maxCred.maxUnitRetail = credRetTbl.Rows(indx)("salUnitPrc")
                    End If
                    If ordTpCd = AppConstants.Order.TYPE_CRM Then   'credRetTbl.Rows(indx)("crmQty") > 0.00001 AndAlso 

                        maxCred.maxQty = credRetTbl.Rows(indx)("salQty") - credRetTbl.Rows(indx)("crmQty")
                        ' only set if CRM - need to use orig sal prc, not orig CRM prc; could add to line extract but have here from max list so use if have
                        'MM-6114
                    ElseIf ordTpCd = AppConstants.Order.TYPE_MCR Then ' if MCR
                        maxCred.maxQty = credRetTbl.Rows(indx)("salQty") - credRetTbl.Rows(indx)("crmQty")
                    End If
                    If credRetTbl.Rows(indx)("credTot") > 0.00001 Then

                        Dim remCred As Double = credRetTbl.Rows(indx)("salTot") - credRetTbl.Rows(indx)("credTot")  ' set the remaining credit amount
                        ' determine the remaining credit per unit - for CRM, remaining qty units, for MCR, per original qty units
                        If maxCred.maxQty > 0 Then

                            remCred = remCred / maxCred.maxQty
                        End If

                        ' If CRM, don't credit back more per unit than original sale; for instance a previous return at less than full value would make remaining credit exceed; MCR can return all remaining credit but need per here
                        If maxCred.maxUnitRetail > remCred Then

                            maxCred.maxUnitRetail = remCred
                        End If

                        Math.Round(maxCred.maxUnitRetail, SystemUtils.CurrencyPrecision)
                        ' if an MCR with 0 credit, then setting the qty to 0 will make the line entry disabled; Can return qty on CRM w 0 price so cannot disable ba
                        If maxCred.maxUnitRetail < 0.00001 AndAlso ordTpCd = AppConstants.Order.TYPE_MCR Then

                            maxCred.maxQty = 0
                        End If
                    End If
                End If
            End If
        End If

        Return maxCred

    End Function

    ''' <summary>
    ''' Determines the maximum quantity that are left to return for a particular SKU CRM line
    ''' </summary>
    ''' <param name="salLnQty">quantity on the original sale line, possibly to set on the return line</param>
    ''' <param name="qtyRetDatSet">DataSet containing the set of previously returned SKUs to the original sale order; SKU quantity is updated to reflect the used return value</param>
    ''' <returns>the maximum quantity available to return on this line</returns>
    ''' <remarks></remarks>
    Public Shared Function setMaxReturnQty(ByVal itmCd As String, ByVal salLnQty As Double, ByRef qtyRetDatSet As DataSet) As Double

        Dim remCrmQty As Double = salLnQty ' this will be the remaining qty available to return

        ' handle reduction of qty previously returned and maybe same SKU on multiple lines
        If SystemUtils.dataSetHasRows(qtyRetDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(qtyRetDatSet.Tables(0).Rows(0)) Then

            Dim qtyRetTbl As DataTable = qtyRetDatSet.Tables(0)
            Dim qtyRetRowCnt As Double = qtyRetTbl.Rows.Count
            Dim prevCrmQty As Double

            Dim indx As Integer = 0
            Do While indx <= (qtyRetRowCnt - 1) AndAlso qtyRetTbl.Rows(indx)("itm_cd") <> itmCd
                indx = indx + 1
            Loop

            If indx <= (qtyRetRowCnt - 1) AndAlso qtyRetTbl.Rows(indx)("itm_cd") = itmCd AndAlso qtyRetTbl.Rows(indx)("crm_qty") > 0 Then

                prevCrmQty = qtyRetTbl.Rows(indx)("crm_qty")

                ' if the line has qty > or equal to previously return qty for this SKU then reduce qty on this line by the qty of the previously returned
                If remCrmQty >= prevCrmQty Then

                    remCrmQty = remCrmQty - prevCrmQty
                    qtyRetTbl.Rows(indx)("crm_qty") = 0
                    ' if line has qty < previously returned for this SKU, then nothing can be returned from this line so set qty to 0 and reduce previously returned to reflect the removal of return from this line
                    '   probably another line with the same SKU
                Else
                    qtyRetTbl.Rows(indx)("crm_qty") = qtyRetTbl.Rows(indx)("crm_qty") - remCrmQty
                    remCrmQty = 0
                End If
            End If
        End If

        Return remCrmQty

    End Function

    Public Shared Function isCrmWithOrigSal(ByVal ordTpCd As String, ByVal origDeldoc As String) As Boolean

        Dim returnVal As Boolean = False

        If AppConstants.Order.TYPE_CRM.Equals(ordTpCd) AndAlso SystemUtils.isNotEmpty(origDeldoc) Then

            returnVal = True
        End If

        Return returnVal

    End Function

    Public Shared Function isCredDocWithOrigSal(ByVal ordTpCd As String, ByVal origDeldoc As String) As Boolean

        Dim returnVal As Boolean = False

        If (AppConstants.Order.TYPE_CRM.Equals(ordTpCd) OrElse AppConstants.Order.TYPE_MCR.Equals(ordTpCd)) AndAlso SystemUtils.isNotEmpty(origDeldoc) Then

            returnVal = True
        End If

        Return returnVal

    End Function

    Public Enum tempItmSelection
        ' indicates what type of selection based on take-with flag or not to extract from temp_itm table for taxing

        All = 1         ' ignore TW processing selection
        TakeWith = 2    ' select only take-with lines
        NonTakeWith = 4 ' select only non-take-with lines
    End Enum

    Public Shared Function Calc_Hdr_Tax_From_Temp(ByVal session_id As String, ByVal taxCd As String, ByVal taxDt As DateTime,
                                                  ByVal setupChg As Double, ByVal delChg As Double, ByVal twSelect As tempItmSelection) As Double
        ' to calc header tax to match E1 where tax amounts are rounded by tax auth, we need to calculate by tax auth and all
        '    applicable taxable amounts must be included;  gets line amounts from temp_itm table but delivery and setup charges
        '    must be input
        ' if processing a split document and this is the TW, then only extract take-with amounts; otherwise
        ' returns tax charge

        Dim taxAmt As Double = 0.0

        If session_id.isNotEmpty AndAlso (Not IsNothing(taxCd)) AndAlso taxCd.isNotEmpty Then

            Dim sqlStmtSb As New StringBuilder
            sqlStmtSb.Append("SELECT SUM(NVL(ROUND(qty * ret_prc, 2), 0)) AS lnTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'LAB', NVL(ret_prc * qty, 0), 0)) AS labTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'WAR', NVL(ret_prc * qty, 0), 0)) AS warTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'SVC', NVL(ret_prc * qty, 0), 0)) AS svcTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'FAB', NVL(ret_prc * qty, 0), 0)) AS fabTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'PAR', NVL(ret_prc * qty, 0), 0)) AS parTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'MIL', NVL(ret_prc * qty, 0), 0)) AS milTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'NLT', NVL(ret_prc * qty, 0), 0)) AS nltTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'NLN', NVL(ret_prc * qty, 0), 0)) AS nlnTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'DNR', NVL(ret_prc * qty, 0), 0)) AS dnrTot, ").Append(
                             "SUM(DECODE(itm_tp_cd, 'PKG', NVL(ret_prc * qty, 0), 0)) AS pkgTot ").Append(
                             "FROM temp_itm t WHERE session_id = :session_id ")

            If tempItmSelection.TakeWith = twSelect Then
                sqlStmtSb.Append("AND take_with = 'Y' ")
            ElseIf tempItmSelection.NonTakeWith = twSelect Then
                sqlStmtSb.Append("AND (take_with = 'N' OR take_with IS NULL) ")
            End If

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
            Dim datRdr As OracleDataReader
            Dim taxReq As New TaxUtils.taxHdrCalcRequest

            cmd.CommandText = sqlStmtSb.ToString
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("session_id", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("session_id").Value = session_id
            taxReq.taxCd = taxCd
            taxReq.taxDt = taxDt

            Try
                conn.Open()
                datRdr = DisposablesManager.BuildOracleDataReader(cmd)


                taxReq.itmTpAmts = New ItemTypeTaxAmtsDtc()
                taxReq.itmTpAmts.del = delChg
                taxReq.itmTpAmts.setup = setupChg
                taxReq.itmTpAmts.lin = 0
                If datRdr.HasRows AndAlso datRdr.Read AndAlso (Not String.IsNullOrEmpty(datRdr("lnTot").ToString)) AndAlso IsNumeric(datRdr("lnTot").ToString) Then

                    taxReq.itmTpAmts.lab = CDbl(datRdr("labTot").ToString)
                    taxReq.itmTpAmts.war = CDbl(datRdr("warTot").ToString)
                    taxReq.itmTpAmts.svc = CDbl(datRdr("svcTot").ToString)
                    taxReq.itmTpAmts.fab = CDbl(datRdr("fabTot").ToString)
                    taxReq.itmTpAmts.par = CDbl(datRdr("parTot").ToString)
                    taxReq.itmTpAmts.mil = CDbl(datRdr("milTot").ToString)
                    taxReq.itmTpAmts.nlt = CDbl(datRdr("nltTot").ToString)
                    taxReq.itmTpAmts.nln = CDbl(datRdr("nlnTot").ToString)
                    taxReq.itmTpAmts.lin = CDbl(datRdr("lnTot").ToString)
                End If

                If taxReq.itmTpAmts.lin > 0 OrElse taxReq.itmTpAmts.del > 0 OrElse taxReq.itmTpAmts.setup > 0 Then
                    taxAmt = TaxUtils.ComputeHdrTax(taxReq)
                End If
            Finally
                datRdr.Close()
                datRdr.Dispose()
                conn.Close()
            End Try
        End If

        Return taxAmt

    End Function


    ''' <summary>
    ''' Retrieves the order details such as the store code, order type, customer code, salespersons,etc.
    ''' for the order passed_in
    ''' </summary>
    ''' <param name="del_doc_num">the order to retrieve details for</param>
    ''' <returns>A datatable with all the basic order info</returns>
    Public Shared Function GetOrderDetails(ByVal delDocNum As String) As DataTable
        '  extract and return the basic document information

        Dim dtable As New DataTable

        If (delDocNum.isNotEmpty) Then

            Dim datSet As New DataSet
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            'Dim sqlStmt As String = "SELECT so_wr_dt, so_seq_num, so_store_cd, ord_tp_cd, pu_del_store_cd, cust_cd, orig_del_doc_num, " +
            '                        " so_emp_slsp_cd1, so_emp_slsp_cd2, stat_cd, del_chg, setup_chg, disc_cd, pu_del, pu_del_dt " +
            '                        " FROM so " +
            '                        " WHERE del_doc_num = :del_doc_num "
            'Alice update for rquest 4477 on Feb 22, 2019
            Dim sqlStmt As String = "SELECT so_wr_dt, so_seq_num, so_store_cd, ord_tp_cd, pu_del_store_cd, cust_cd, orig_del_doc_num, " +
                                    " so_emp_slsp_cd1, so_emp_slsp_cd2, stat_cd, del_chg, setup_chg, disc_cd, pu_del, pu_del_dt, ord_srt_cd " +
                                    " FROM so " +
                                    " WHERE del_doc_num = :del_doc_num "


            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmt, conn)
            cmd.Parameters.Add(":del_doc_num", OracleType.VarChar)
            cmd.Parameters(":del_doc_num").Value = delDocNum.Trim.ToUpper

            Try
                conn.Open()
                Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                oraDatA.Fill(datSet)
                dtable = datSet.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally
                conn.Close()
            End Try

        End If
        Return dtable

    End Function

    ''' <summary>
    ''' Creates an object of type WrDtStoreSeqKey and populate its details with the ones passed in
    ''' </summary>
    ''' <param name="soKeys">a Sales Order Details to be used when populating the new object</param>
    ''' <returns>theKey</returns>
    Public Shared Function GetWrDtStoreSeqKey(ByVal soKeys As OrderUtils.SalesOrderKeys) As WrDtStoreSeqKey

        Return GetWrDtStoreSeqKey(soKeys.soKey.storeCd, soKeys.soKey.seqNum, soKeys.soKey.wrDt)

    End Function

    ''' <summary>
    ''' Creates an object of type WrDtStoreSeqKey and populate its details with the ones passed in
    ''' </summary>
    ''' <param name="storeCd">the store code where the order was keyed</param>
    ''' <param name="seqNum">a sequence number</param>
    ''' <param name="writtenDt">the order written date</param>
    ''' <returns></returns>
    Public Shared Function GetWrDtStoreSeqKey(ByVal storeCd As String, ByVal seqNum As String, ByVal writtenDt As Date?) As WrDtStoreSeqKey

        Dim theKey = New OrderUtils.WrDtStoreSeqKey
        theKey.storeCd = storeCd
        theKey.seqNum = seqNum
        theKey.wrDt = writtenDt
        Return theKey

    End Function

    ' TODO DSA - this is from SalesGateway
    Public Class WrDtStoreSeqKey

        Property wrDt As Nullable(Of Date)
        Property storeCd As String
        Property seqNum As String

    End Class

    Public Shared Function getNextSoCmntSeq(ByVal soCmntKey As WrDtStoreSeqKey, ByRef cmd As OracleCommand) As Integer
        'Returns the next so_cmnt seq# to use for insertion to an SO_doc_num
        'Use this when inserting rows within a transaction

        Dim retVal As Integer = 0
        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("SELECT (NVL(MAX(cmnt.seq#), 0) + 1) AS seqNum FROM so_cmnt cmnt ").Append(
            "WHERE cmnt.so_wr_dt = :wrDt AND cmnt.so_store_cd = :storeCd AND cmnt.so_seq_num = :soSeqNum ")

        cmd.Parameters.Clear()
        cmd.CommandText = sqlStmtSb.ToString
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add(":wrDt", OracleType.DateTime)
        cmd.Parameters(":wrDt").Value = FormatDateTime(soCmntKey.wrDt, DateFormat.ShortDate)
        cmd.Parameters.Add(":storeCd", OracleType.VarChar)
        cmd.Parameters(":storeCd").Value = soCmntKey.storeCd
        cmd.Parameters.Add(":soSeqNum", OracleType.VarChar)
        cmd.Parameters(":soSeqNum").Value = soCmntKey.seqNum
        Dim datRdr As OracleDataReader

        Try
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.HasRows Then
                datRdr.Read()
                retVal = datRdr("seqNum").ToString
            End If

        Finally
            datRdr.Dispose()
        End Try
        cmd.Parameters.Clear()

        Return retVal
    End Function

    Public Shared Function getNextSoCmntSeq(ByVal soCmntKey As WrDtStoreSeqKey) As Integer
        'Returns the next so_cmnt seq# to use for insertion to an SO_doc_num
        'Use this when inserting rows outside of a transaction

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)

        Dim retVal As Integer = 0

        Try
            conn.Open()

            retVal = getNextSoCmntSeq(soCmntKey, cmd)

        Finally
            cmd.Cancel()
            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return retVal
    End Function

    Public Class SalesOrderNum
        ' currently the 1st 11 characters of the delivery document number 

        Property delDocNum As String
    End Class

    Public Class SalesOrderKeys

        Property soKey As WrDtStoreSeqKey
        Property soDocNum As SalesOrderNum
    End Class

    Public Shared Function Get_Next_DocNumInfo(ByVal store_cd As String, ByVal wr_dt As String) As SalesOrderKeys

        Dim so_key As SalesOrderKeys
        If (Not (store_cd Is Nothing OrElse wr_dt Is Nothing)) AndAlso store_cd.isNotEmpty AndAlso wr_dt.isNotEmpty AndAlso IsDate(wr_dt) Then

            Dim sql As String = "bt_misc_wrapper.autodoc"
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("store_cd_io", OracleType.VarChar).Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add("doc_date_io", OracleType.DateTime).Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add("doc_num_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add("doc_seq_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput

            cmd.Parameters("store_cd_io").Value = store_cd
            cmd.Parameters("doc_date_io").Value = FormatDateTime(wr_dt, DateFormat.ShortDate)

            Try
                conn.Open()

                cmd.ExecuteNonQuery()

                so_key = New SalesOrderKeys
                so_key.soKey = New WrDtStoreSeqKey
                so_key.soKey.wrDt = cmd.Parameters("doc_date_io").Value
                so_key.soKey.storeCd = cmd.Parameters("store_cd_io").Value
                so_key.soKey.seqNum = cmd.Parameters("doc_seq_io").Value
                so_key.soDocNum = New SalesOrderNum
                so_key.soDocNum.delDocNum = cmd.Parameters("doc_num_io").Value

            Finally
                cmd.Cancel()
                cmd.Dispose()
                conn.Close()
                conn.Dispose()
            End Try
        End If

        Return so_key
    End Function

    Public Shared Function Get_Orig_Del_Doc(ByVal docNum As String) As DataRow

        Dim origDelDoc As DataRow
        If (Not (docNum Is Nothing)) AndAlso docNum.isNotEmpty Then

            Dim sql As String = "SELECT tax_cd, so_wr_dt FROM so WHERE del_doc_num = :docNum "
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add("docNum", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("docNum").Value = docNum

            Try
                conn.Open()

                Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                Dim datSet As New DataSet
                oraDatA.Fill(datSet)
                If Not (datSet Is Nothing OrElse datSet.Tables(0) Is Nothing OrElse datSet.Tables(0).Rows.Count < 1) Then
                    origDelDoc = datSet.Tables(0).Rows(0)
                End If

            Finally
                cmd.Cancel()
                cmd.Dispose()
                conn.Close()
                conn.Dispose()
            End Try
        End If

        Return origDelDoc
    End Function
    'Alice added on Feb21,2019 for request 4477
    Public Shared Function Get_Orig_Slsp_forExchangeSo(ByVal docNum As String) As DataRow

        Dim origSlsp As DataRow

        If (Not (docNum Is Nothing)) AndAlso docNum.isNotEmpty Then
            Dim sql As String = String.Empty

            'sql = "select so_emp_slsp_cd1, so_emp_slsp_cd2, pct_of_sale1, pct_of_sale2 from so where ord_tp_cd='SAL' and del_doc_num in "
            'sql = sql & "( select orig_del_doc_num from so where del_doc_num=:docNum ||'A' and origin_cd='EXC' AND ORD_TP_CD='CRM')"

            sql = "select orig_sal.so_emp_slsp_cd1, orig_sal.so_emp_slsp_cd2, orig_sal.pct_of_sale1, orig_sal.pct_of_sale2 from so exc_crm, so orig_sal "
            sql = sql & " where exc_crm.del_doc_num =:docNum ||'A' and exc_crm.origin_cd = 'EXC' and exc_crm.ord_tp_cd = 'CRM' and exc_crm.orig_del_doc_num = orig_sal.del_doc_num and orig_sal.ord_tp_cd = 'SAL' "


            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add("docNum", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("docNum").Value = docNum

            Try
                conn.Open()

                Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                Dim datSet As New DataSet
                oraDatA.Fill(datSet)
                If Not (datSet Is Nothing OrElse datSet.Tables(0) Is Nothing OrElse datSet.Tables(0).Rows.Count < 1) Then
                    origSlsp = datSet.Tables(0).Rows(0)
                End If

            Catch ex As Exception
                origSlsp = Nothing

            Finally
                cmd.Cancel()
                cmd.Dispose()
                conn.Close()
                conn.Dispose()
            End Try
        End If



        Return origSlsp
    End Function


    ''' <summary>
    ''' Returns the original order for the passed-in order which is typically a CRM or MCR.
    ''' </summary>
    ''' <param name="delDocNum">the order(return or misc credit) for which to get the original del doc num</param>
    ''' <returns>the original del doc num on the order passed-in</returns>
    Public Shared Function GetOrginalDelDocNum(ByVal delDocNum As String) As String

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim origDelDocNum As String = String.Empty
        Dim reader As OracleDataReader
        Dim cmd As OracleCommand
        Dim sql As New StringBuilder("Select orig_del_doc_num ")
        sql.Append("FROM SO ")
        sql.Append("WHERE del_doc_num = :DEL_DOC_NUM")

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum.ToUpper
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read() AndAlso Not IsDBNull(reader.Item("ORIG_DEL_DOC_NUM"))) Then
                origDelDocNum = reader.Item("ORIG_DEL_DOC_NUM")
            End If
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return origDelDocNum
    End Function

    ''' <summary>
    ''' Returns the order status, type and whether it is a pickup/delivery for the order specified.
    ''' </summary>
    ''' <param name="delDocNum"></param>
    ''' <returns></returns>
    Public Shared Function GetOrderStatusAndType(ByVal delDocNum As String) As DataSet

        ' TODO - replace the calls to this by getting this data with the original sql extraction and then remove
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String = "SELECT ORD_TP_CD, STAT_CD, PU_DEL " &
                            "FROM SO " &
                            "WHERE so.del_doc_num = :DEL_DOC_NUM "

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(UCase(sql), conn)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "ORDER_STS")

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' returns salesperson list for dropdown selection and includes 'ALL' in list, sequenced by employee initials
    ''' </summary>
    ''' <param name="elimTermd">if true, do not include terminated salespersons; if false, query all - NOT IMPLEMENTED YET</param>
    ''' <returns>data set of all salesperson, term'd or not and including an 'ALL" entry for a dropdown</returns>
    ''' <remarks>output formatted specifically to be able to extract emp_init from full_name text as needed</remarks>
    Public Shared Function getSalespersonList(Optional ByVal elimTermd As Boolean = True, Optional ByVal onlyThisSlsp As String = "") As DataSet

        Dim datSet As New DataSet
        Dim sqlStmtSb As New StringBuilder

        ' Daniela June 18 added es.slsp_dept_cd not in ('TERM','DRIVER','HELPER')

        ' NEED TO KEEP THE ENTRIES AS init || ' - ' || name for the lookup to pull out the emp_init 
        'sqlStmtSb.Append("SELECT  emp.emp_cd, emp.emp_init || ' - ' || fname || ' ' || lname AS full_name ").Append(
        '    "FROM emp$emp_tp et, emp, emp_slsp es ").Append(
        '    "WHERE es.emp_cd = emp.emp_cd AND et.emp_cd = emp.emp_cd AND et.emp_cd = es.emp_cd ").Append(
        '    "AND emp_tp_cd IN ('SLS','SLM') AND (TRUNC(sysdate)) BETWEEN et.eff_dt and et.end_dt ")

        sqlStmtSb.Append("SELECT  emp.emp_cd, emp.emp_init || ' - ' || fname || ' ' || lname AS full_name ").Append(
            "FROM emp$emp_tp et, emp, emp_slsp es ").Append(
            "WHERE es.emp_cd = emp.emp_cd AND et.emp_cd = emp.emp_cd AND et.emp_cd = es.emp_cd ").Append(
            "AND emp_tp_cd IN ('SLS','SLM') AND (TRUNC(sysdate)) BETWEEN et.eff_dt and et.end_dt and es.slsp_dept_cd not in ('TERM','DRIVER','HELPER') ")

        If SystemUtils.isEmpty(onlyThisSlsp) Then

            sqlStmtSb.Append("UNION SELECT 'ALL', 'ALL Salespersons' AS full_name FROM dual ORDER BY 2 ")

        Else
            sqlStmtSb.Append("AND emp.emp_cd = :empCd ")
        End If

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
              cmd As New OracleCommand(sqlStmtSb.ToString, conn), oDatAdap As New OracleDataAdapter(cmd)

            If SystemUtils.isNotEmpty(onlyThisSlsp) Then

                cmd.Parameters.Add(":empCd", OracleType.VarChar)
                cmd.Parameters(":empCd").Value = UCase(onlyThisSlsp)
            End If

            oDatAdap.Fill(datSet)
        End Using

        Return datSet
    End Function

    ''' <summary>
    ''' Creates and populates a SOHeader object with the passed-in data. This is can be used for 
    ''' existing lines in a management case or new lines in order entry.
    ''' </summary>
    ''' <param name="sessionId"></param>
    ''' <param name="slsp1"></param>
    ''' <param name="slsp2"></param>
    ''' <param name="slspPct1"></param>
    ''' <param name="slspPct2"></param>
    ''' <param name="taxExemptCd"></param>
    ''' <param name="taxCd"></param>
    ''' <param name="isStoreARS"></param>
    ''' <param name="zoneCd"></param>
    ''' <param name="pkpDel"></param>
    ''' <param name="pkpDelStoreCd"></param>
    ''' <param name="isCashNcarry"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function GetSoHeader(sessionId As String,
                                slsp1 As String, slsp2 As String,
                                slspPct1 As Double, slspPct2 As Double,
                                taxExemptCd As String, taxCd As String,
                                isStoreARS As Boolean, zoneCd As String,
                                pkpDel As String, pkpDelStoreCd As String,
                                isCashNcarry As Boolean) As SoHeaderDtc

        Dim headerInfo As SoHeaderDtc = New SoHeaderDtc()
        headerInfo.slsp1 = slsp1
        headerInfo.slsp2 = IIf(slsp2.isEmpty, "", slsp2)
        headerInfo.slspPct1 = slspPct1
        headerInfo.slspPct2 = IIf(IsNothing(slspPct2), 0, slspPct2)
        headerInfo.taxExemptCd = IIf(taxExemptCd.isEmpty, "", taxExemptCd)
        headerInfo.taxCd = IIf(taxCd.isEmpty, "", taxCd)
        headerInfo.isARSEnabled = IIf(IsNothing(isStoreARS), False, isStoreARS)
        headerInfo.zoneCd = IIf(zoneCd.isEmpty, "", zoneCd)
        headerInfo.puDel = pkpDel
        headerInfo.puDelStore = pkpDelStoreCd
        headerInfo.isCashNCarry = IIf(IsNothing(isCashNcarry), False, isCashNcarry)

        Return headerInfo
    End Function


    ''' <summary>
    ''' Returns the actual data row that matches the passed-in line number from the the 
    ''' dataset passed-in that has all order lines. This datarow can then be used to read 
    ''' values or modify it's values.   
    ''' </summary>
    Public Shared Function GetOrderLine(ByVal orderLines As DataSet, ByVal lineNum As String, Optional ByVal isSOM As Boolean = True) As DataRow

        Dim row As DataRow = Nothing

        'find the matching line in the order lines table
        Dim strWhereClause As String = String.Empty
        If isSOM Then
            strWhereClause = ("[DEL_DOC_LN#] = " & lineNum)
        Else
            strWhereClause = ("[LINE] = " & lineNum)
        End If

        Dim lines As DataRow() = orderLines.Tables(0).Select(strWhereClause)

        If lines.Count > 0 Then
            row = lines(0)
        End If

        Return row
    End Function
    Public Shared Function isAeroplanPromptAllow(empinit As String) As Boolean
        'Oct 20,2016 - mariam - Aeroplan promotion
        Dim isValid As String = "N"
        Dim sql As String = "STD_AEROPLAN.isPOSPromptAllow"

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
              cmd As New OracleCommand(sql, conn)
            Try
                With cmd
                    .Connection = conn
                    .Connection.Open()
                    .CommandType = CommandType.StoredProcedure
                    'Output value
                    .Parameters.Add(New OracleParameter("p_out", OracleType.VarChar, 1)).Direction = ParameterDirection.ReturnValue

                    .Parameters.Add(New OracleParameter("p_init", OracleType.VarChar, 8)).Direction = ParameterDirection.Input
                    .Parameters("p_init").Value = empinit

                    .ExecuteNonQuery()
                    isValid = .Parameters("p_out").Value
                End With
            Catch ex As Exception
                conn.Close()
            End Try
        End Using
        Return IIf(isValid = "Y", True, False)
    End Function

    Public Shared Function insAeroplanStaging(p_deldocnum As String, p_cardnum As String) As Boolean
        'Oct 20,2016 - mariam - Aeroplan promotion
        Dim affectedRows As Integer = 0
        Dim sql As String = "STD_AEROPLAN.insStaging"

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
              cmd As New OracleCommand(sql, conn)
            Try
                With cmd
                    .Connection = conn
                    .Connection.Open()
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.Add(New OracleParameter("p_doc", OracleType.VarChar, 14)).Direction = ParameterDirection.Input
                    .Parameters("p_doc").Value = p_deldocnum
                    .Parameters.Add(New OracleParameter("p_crd", OracleType.VarChar, 20)).Direction = ParameterDirection.Input
                    .Parameters("p_crd").Value = p_cardnum
                    affectedRows = .ExecuteNonQuery()
                    If affectedRows > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End With
            Catch ex As Exception
                conn.Close()
                Return False
            End Try
        End Using
    End Function
    Public Shared Function isValidAeroplanCardNum(p_cardnum As String) As Boolean
        'Oct 20,2016 - mariam - Aeroplan promotion
        Dim isValidCardNum As Boolean = False
        Dim isValid As String = "N"
        Dim sql As String = "STD_AEROPLAN.validate_crdnum"

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
              cmd As New OracleCommand(sql, conn)
            Try
                With cmd
                    .Connection = conn
                    .Connection.Open()
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.Add(New OracleParameter("p_out", OracleType.VarChar, 1)).Direction = ParameterDirection.ReturnValue

                    .Parameters.Add(New OracleParameter("p_card", OracleType.VarChar, 20)).Direction = ParameterDirection.Input
                    .Parameters("p_card").Value = p_cardnum

                    .ExecuteNonQuery()
                    isValid = .Parameters("p_out").Value
                End With
            Catch ex As Exception
                conn.Close()
            End Try
        End Using
        Return IIf(isValid = "Y", True, False)
    End Function
End Class



