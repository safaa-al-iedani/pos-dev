﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports System.IO

Public Class SystemUtils

    Public Const CurrencyPrecision As Integer = 2  ' at some time during localization, this gets replaced but maybe it will help being here

    Public Class Connection_Constants

        Public Const CONN_ERP As String = "ERP"

    End Class

    ''' <summary>
    ''' Validates if the user has gone thru the login entry
    ''' </summary>
    ''' <param name="None">Uses Session("emp_cd")</param>
    ''' <returns>if the Session("emp_cd") is filled, then valLogin returns true, otherwise returns false</returns>
    Public Shared Function valLogin() As Boolean

        If IsNothing(HttpContext.Current.Session("emp_cd")) OrElse HttpContext.Current.Session("emp_cd").ToString.isEmpty Then

            Return False

        Else
            Return True
        End If
    End Function

    Public Class PageInfo

        Property title As String  ' will be the page title
        Property label As String  ' will be the page label
        Property allowPageEntry As Boolean ' will be true if the user has security to access the page or if there is no security check for that page; 
        ' false if there is a security to access and the user does not have it
        Property hyPrLnk As HyperLink  ' provides the opportunity to return specific hyperlink setting for the specific form requested
    End Class

    ''' <summary>
    '''Returns the title, label and potentially hyperlink information depending on the pageName requested; 
    ''' can also validate the user can access the page requested - this avoids the user accessing links without going thru the menu
    ''' </summary>
    ''' <param name="pageName">the page name being requested</param>
    ''' <param name="empCd">the parameter name</param>
    ''' <returns>PageInfo</returns>
    Public Shared Function GetPageInfo(ByVal pageName As String, ByVal empCd As String) As PageInfo

        Dim pgInfo As New PageInfo
        pgInfo.hyPrLnk = New System.Web.UI.WebControls.HyperLink
        pgInfo.allowPageEntry = True   ' TODO probably should be an IBE to put in the rest of the securities as this is a problem with all pages
        ' the problem being that if the URL is sitting in history or the user knows it, can click and enter even if no security for
        Dim pgName As String = LCase(pageName)
        Dim appTitle As String = ConfigurationManager.AppSettings("app_title").ToString

        Select Case pgName

            Case "newsmaintenance.aspx"

                pgInfo.label = "NEWS AND UPDATES - MAINTENANCE"
                pgInfo.title = appTitle & "News and Updates - Maintenance"
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'lblTabID.Text = "NEWS AND UPDATES - MAINTENANCE"
                'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "News and Updates - Maintenance"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "admin.aspx"

                pgInfo.label = "SYSTEM PARAMETERS"
                pgInfo.title = appTitle & "System Parameters"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "independentpaymentprocessing.aspx"
                'Daniela french
                'pgInfo.label = "PAYMENT PROCESSING"
                'pgInfo.title = appTitle & "Payment Processing"
                pgInfo.label = Resources.LibResources.Label389.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label389
                'ASPxMenu1.RootItem.Items(3).Text = "Quick Screen"
                'ASPxMenu1.RootItem.Items(3).NavigateUrl = "GEQuickCredit.aspx"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "terminal_setup.aspx"

                pgInfo.label = "TERMINAL SETUP"
                pgInfo.title = appTitle & "Terminal Setup"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "terminal_status.aspx"

                pgInfo.label = "TERMINAL STATUS"
                pgInfo.title = appTitle & "Terminal Status"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "invoice_triggers.aspx"

                pgInfo.label = "INVOICE TRIGGER SETUP"
                pgInfo.title = appTitle & "Invoice Trigger Setup"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "inventorycomparison.aspx"

                pgInfo.label = "MERCHANDISE COMPARISON"
                pgInfo.title = appTitle & "Merchandise Comparison"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "relationship_maintenance.aspx"
                'Daniela french
                'pgInfo.label = "RELATIONSHIP MAINTENANCE"
                'pgInfo.title = appTitle & "Relationship Maintenance"
                pgInfo.label = Resources.LibResources.Label456.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label456
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "relationship_calendar.aspx"
                'Dnaiela french
                'pgInfo.label = "RELATIONSHIP CALENDAR"
                'pgInfo.title = appTitle & "Relationship Calendar"
                pgInfo.label = Resources.LibResources.Label455.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label455
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "relationship_reporting.aspx"

                pgInfo.label = "RELATIONSHIP REPORTING"
                pgInfo.title = appTitle & "Relationship Reporting"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "campaign_main.aspx"

                pgInfo.label = "CAMPAIGN"
                pgInfo.title = appTitle & "Launch Campaign"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "paymentonaccount.aspx"

                pgInfo.label = "PAYMENT ON ACCOUNT"
                pgInfo.title = appTitle & "Payment on Account"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "salesexchange.aspx"
                'Alice update on Mar 27,2019 for request 4607
                'pgInfo.label = "EXCHANGE"
                'pgInfo.title = appTitle & "Exchange"
                pgInfo.label = Resources.SalesExchange.Label69.ToUpper()
                pgInfo.title = appTitle & Resources.SalesExchange.Label69.ToUpper()

                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "picture_upload.aspx"

                pgInfo.label = "UTILITIES : UPLOAD IMAGES"
                pgInfo.title = appTitle & "Upload Images"
                pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "utilities.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "discount_lookup.aspx"

                pgInfo.label = "DISCOUNT LOOKUP"
                pgInfo.title = appTitle & "Discount Lookup"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 
                pgInfo.label = Resources.LibResources.Label167.ToUpper   'lucy
                pgInfo.title = appTitle & Resources.LibResources.Label167

            Case "inventorylookup.aspx"
                'Daniela french
                'pgInfo.label = "INVENTORY AVAILABILITY"
                'pgInfo.title = appTitle & "Inventory Availability"
                pgInfo.label = Resources.LibResources.Label245.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label245
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "invoice.aspx"

                pgInfo.label = "PRINT INVOICE"
                pgInfo.title = appTitle & "Print Invoice"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "wgc_balance.aspx"

                pgInfo.label = ""
                pgInfo.title = appTitle & "Gift Card Balance"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "wgc_addvalue.aspx"

                pgInfo.label = ""
                pgInfo.title = appTitle & "Gift Card Add Value"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "wgc_reconcile.aspx"

                pgInfo.label = ""
                pgInfo.title = appTitle & "Gift Card Reconciliation"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "customerinquiry.aspx"
                'Daniela french
                'pgInfo.label = "ACCOUNT INQUIRY"
                'pgInfo.title = appTitle & "Account Inquiry"
                pgInfo.label = Resources.LibResources.Label8.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label8
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "settlement.aspx"

                pgInfo.label = ""
                pgInfo.title = appTitle & "Settlement Options"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "price_tag.aspx"

                pgInfo.label = "PRICE TAGS"
                pgInfo.title = appTitle & "Price Tags"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "main_cust_lookup.aspx"

                pgInfo.label = "CUSTOMER SEARCH"
                pgInfo.title = appTitle & "Customer Search"
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "invoice_admin.aspx"

                pgInfo.label = "INVOICE ASSIGN"
                pgInfo.title = appTitle & "Invoice Assignment"
                pgInfo.hyPrLnk.NavigateUrl = "invoice_admin.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "admin.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "testcc.aspx"

                pgInfo.label = "TEST CC"
                pgInfo.title = appTitle & "Test Credit Cards"
                pgInfo.hyPrLnk.NavigateUrl = "admin.aspx"
                pgInfo.hyPrLnk.Visible = True
                'hpl_exit.NavigateUrl = "admin.aspx"
                'hpl_exit.Visible = True
                'pgInfo.allowPgEntry = SecurityUtils.HasSecurity("") 

            Case "salesconfirmation.aspx"

                pgInfo.label = Resources.LibResources.Label952.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label952
                'pgInfo.allowPageEntry = SecurityUtils.hasSecurity("SBOB", empCd)

            Case "salesbookofbusiness.aspx"

                'pgInfo.label = "SALES BOOK OF BUSINESS"
                'pgInfo.title = appTitle & "Sales Book of Business"
                pgInfo.label = Resources.LibResources.Label508.ToUpper
                pgInfo.title = appTitle & Resources.LibResources.Label508
                pgInfo.allowPageEntry = SecurityUtils.hasSecurity("SBOB", empCd)

            Case "loadehpriceitems.aspx"

                pgInfo.label = "Easy Home Price Upload"
                pgInfo.title = appTitle & "Easy Home Price Upload"
                pgInfo.hyPrLnk.Visible = True


        End Select

        ' TODO (maybe) Got tired of re-doing all here - more some other time

        If LCase(pageName) = "merchant_info.aspx" Then
            'hpl_exit.NavigateUrl = "admin.aspx"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "admin.aspx"
            pgInfo.hyPrLnk.Visible = True
            pgInfo.label = "MERCHANT INFO"
            pgInfo.title = appTitle & "Merchant Information"
            'lblTabID.Text = "MERCHANT INFO"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Merchant Information"
        End If

        If LCase(pageName) = "admin_rights.aspx" Then
            'hpl_exit.NavigateUrl = "admin.aspx"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "admin.aspx"
            pgInfo.hyPrLnk.Visible = True
            'lblTabID.Text = "ADMINISTRATIVE RIGHTS"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Administrative Rights"
            pgInfo.label = "ADMINISTRATIVE RIGHTS"
            pgInfo.title = appTitle & "Administrative Rights"
        End If

        If LCase(pageName) = "help_admin.aspx" Then
            'hpl_exit.NavigateUrl = "admin.aspx"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "admin.aspx"
            pgInfo.hyPrLnk.Visible = True
            'lblTabID.Text = "HELP ADMIN"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Help Page Administration"
            pgInfo.label = "HELP ADMIN"
            pgInfo.title = appTitle & "Help Page Administration"
        End If

        If LCase(pageName) = "utilities.aspx" Then
            'lblTabID.Text = "UTILITIES"
            ' Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Utilities"
            pgInfo.label = "UTILITIES"
            pgInfo.title = appTitle & "Utilities"
        End If

        If LCase(pageName) = "reporting.aspx" Then
            'lblTabID.Text = "REPORTING"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Reporting Functions"
            pgInfo.label = "REPORTING"
            pgInfo.title = appTitle & "Reporting Functions"
        End If

        If LCase(pageName) = "main_customer.aspx" Then
            'lblTabID.Text = "CUSTOMER MAINTENANCE"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Customer Maintenance"
            'Daniela french
            'pgInfo.label = "CUSTOMER MAINTENANCE"
            'pgInfo.title = appTitle & "Customer Maintenance"
            pgInfo.label = Resources.LibResources.Label137.ToUpper
            pgInfo.title = appTitle & Resources.LibResources.Label137
        End If

        If LCase(pageName) = "salesordermaintenance.aspx" Then
            'lblTabID.Text = "SALES ORDER MAINTENANCE"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Maintenance"
            ' Daniela french
            'pgInfo.label = "SALES ORDER MAINTENANCE"
            'pgInfo.title = appTitle & "Sales Order Maintenance"
            pgInfo.label = Resources.LibResources.Label512.ToUpper
            pgInfo.title = appTitle & Resources.LibResources.Label512
        End If

        If LCase(pageName) = "cashcarrysale.aspx" Then
            pgInfo.label = "CASH CARRY SALE"
            pgInfo.title = appTitle & "Cash Carry Sale"
        End If

        If LCase(pageName) = "main_so_lookup.aspx" Then
            'lblTabID.Text = "SALES ORDER SEARCH"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Search"
            ' Daniela french
            'pgInfo.label = "SALES ORDER SEARCH"
            pgInfo.label = Resources.LibResources.Label526.ToUpper() & " " & Resources.LibResources.Label511.ToUpper()
            pgInfo.title = appTitle & Resources.LibResources.Label526 & " " & Resources.LibResources.Label511
        End If

        If LCase(pageName) = "salestrends.aspx" Then
            'lblTabID.Text = "SALES TRENDS"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Trends"
            pgInfo.label = "SALES TRENDS"
            pgInfo.title = appTitle & "Sales Trends"
        End If

        If LCase(pageName) = "salesreport.aspx" Then
            'lblTabID.Text = "REPORTING : SALES ORDER REPORT"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Report"
            pgInfo.label = "REPORTING : SALES ORDER REPORT"
            pgInfo.title = appTitle & "Sales Order Report"
        End If

        If LCase(pageName) = "salesrewrite.aspx" Then
            'lblTabID.Text = "SALES ORDER REWRITE"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Rewrite"
            ' pgInfo.label = "SALES ORDER REWRITE"
            ' pgInfo.title = appTitle & "Sales Order Rewrite"
            pgInfo.label = Resources.LibResources.Label514.ToUpper   'lucy
            pgInfo.title = appTitle & Resources.LibResources.Label514
        End If

        If LCase(pageName) = "cashdrawerreport.aspx" Then
            'lblTabID.Text = "REPORTING : DAILY RECONCILIATION"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Daily Reconciliation"
            pgInfo.label = "REPORTING : DAILY RECONCILIATION"
            pgInfo.title = appTitle & "Daily Reconciliation"
        End If

        If LCase(pageName) = "custom_availability.aspx" Then
            'hpl_exit.NavigateUrl = "custom_availability.aspx"
            'hpl_exit.Text = "Lookup Another"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "custom_availability.aspx"
            pgInfo.hyPrLnk.Visible = True
            pgInfo.hyPrLnk.Text = "Lookup Another"
            'lblTabID.Text = "INVENTORY LOOKUP"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Inventory Lookup"
            pgInfo.label = "NVENTORY LOOKUP"
            pgInfo.title = appTitle & "Inventory Lookup"
        End If

        If LCase(pageName) = "restore_saved_order.aspx" Then
            'lblTabID.Text = "RESTORE SAVED ORDER"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Restore Saved Order"
            pgInfo.label = "RESTORE SAVED ORDER"
            pgInfo.title = appTitle & "Restore Saved Order"
        End If

        If LCase(pageName) = "generalinterest.aspx" Then
            'hpl_exit.NavigateUrl = "utilities.aspx"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
            pgInfo.hyPrLnk.Visible = True
            'lblTabID.Text = "GENERAL INTEREST"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "General Interest Categories"
            pgInfo.label = "GENERAL INTEREST"
            pgInfo.title = appTitle & "General Interest Categories"
        End If

        If LCase(pageName) = "asp_promo_admin.aspx" Then
            'lblTabID.Text = "FINANCE ADDENDUM ADMINISTRATION"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Finance Addendum Selection"
            pgInfo.label = "FINANCE ADDENDUM ADMINISTRATION"
            pgInfo.title = appTitle & "Finance Addendum Selection"
        End If

        If LCase(pageName) = "payment_xref.aspx" Then
            'hpl_exit.NavigateUrl = "utilities.aspx"
            'hpl_exit.Visible = True
            pgInfo.hyPrLnk.NavigateUrl = "utilities.aspx"
            pgInfo.hyPrLnk.Visible = True
            'lblTabID.Text = "PAYMENT SETUP"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payment Setup"
            pgInfo.label = "PAYMENT SETUP"
            pgInfo.title = appTitle & "Payment Setup"
        End If

        If LCase(pageName) = "po.aspx" Then
            'lblTabID.Text = "PURCHASE ORDER INQUIRY"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Purchase Order Inquiry"
            'Daniela french
            'pgInfo.label = "PURCHASE ORDER INQUIRY"
            'pgInfo.title = appTitle & "Purchase Order Inquiry"
            pgInfo.label = Resources.LibResources.Label442.ToUpper()
            pgInfo.title = appTitle & Resources.LibResources.Label442
        End If

        If LCase(pageName) = "main_po_lookup.aspx" Then
            'lblTabID.Text = "PURCHASE ORDER LOOKUP"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Purchase Order Lookup"
            pgInfo.label = "PURCHASE ORDER LOOKUP"
            pgInfo.title = appTitle & "Purchase Order Lookup"
        End If

        If LCase(pageName) = "ist.aspx" Then
            ' lblTabID.Text = "INTER-STORE TRANSFER INQUIRY"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Inter-Store Transfer Inquiry"
            pgInfo.label = "INTER-STORE TRANSFER INQUIRY"
            pgInfo.title = appTitle & "YInter-Store Transfer Inquiry"
        End If

        If LCase(pageName) = "main_ist_lookup.aspx" Then
            'lblTabID.Text = "INTER-STORE TRANSFER LOOKUP"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Inter-Store Transfer Lookup"
            pgInfo.label = "INTER-STORE TRANSFER LOOKUP"
            pgInfo.title = appTitle & "Inter-Store Transfer Lookup"
        End If

        If LCase(pageName) = "inventoryavailable.aspx" Then
            'lblTabID.Text = "AVAILABLE INVENTORY INQUIRY"
            'Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Available Inventory Inquiry"
            'pgInfo.label = "AVAILABLE INVENTORY INQUIRY"
            'pgInfo.title = appTitle & "Available Inventory Inquiry"
            pgInfo.label = Resources.LibResources.Label706.ToUpper()
            pgInfo.title = appTitle & Resources.LibResources.Label706
        End If

        If LCase(pageName) = "franchisepoentry.aspx" Then
            pgInfo.label = "FRANCHISE PURCHASE ORDER ENTRY (FRPOE)"
            pgInfo.title = "Franchise Purchase Order Entry (FRPOE)"
        End If

        If LCase(pageName) = "franchiseshippingschedule.aspx" Then
            pgInfo.label = "FRANCHISE SHIPPING SCHEDULE MAINTENANCE (FSSM)"
            pgInfo.title = "Franchise Shipping Schedule Maintenance (FSSM)"
        End If

        If LCase(pageName) = "websitepromotionalpricingexception.aspx" Then
            pgInfo.label = "WEB-SITE PROMOTIONAL PRICING EXCEPTION (WPPE)"
            pgInfo.title = "Web-site Promotional Pricing Exception (WPPE)"
        End If

        If LCase(pageName) = "loadarpayments.aspx" Then
            pgInfo.label = "AR PAYMENTS LOAD"
            pgInfo.title = "AR Payments Load"
        End If

        If LCase(pageName) = "loadwppe.aspx" Then
            pgInfo.label = "MASS LOAD WPPE"
            pgInfo.title = "Mass Load WPPE"
        End If

        If LCase(pageName) = "loadadvwpromotitems.aspx" Then
            pgInfo.label = "MASS LOAD ADVW PROMO ITEMS"
            pgInfo.title = "Load ADVW Promo Items"
        End If

        If LCase(pageName) = "loadadfp.aspx" Then
            pgInfo.label = "Mass Load ADFP (Advertising Flyer Performance)"
            pgInfo.title = "Mass Load ADFP  (Advertising Flyer Performance)"
        End If

        If LCase(pageName) = "loadadfs.aspx" Then
            pgInfo.label = "Mass Load ADFS (Advertising Flyer SKU )"
            pgInfo.title = "Mass Load ADFS  (Advertising Flyer SKU"
        End If
        If LCase(pageName) = "LoadLead.aspx" Then
            pgInfo.label = "Mass Load Lead "
            pgInfo.title = "Mass Load Lead "
        End If
        If LCase(pageName) = "LoadSalesForecat.aspx" Then
            pgInfo.label = "Mass Load Sales Forecast "
            pgInfo.title = "Mass Load Sales Forecast "
        End If
        If LCase(pageName) = "productattributemaintenance.aspx" Then
            pgInfo.label = "PRODUCT ATTRIBUTE MAINTENANCE"
            pgInfo.title = "Product Attribute Maintenance"
        End If

		
        Return pgInfo
    End Function

    ''' <summary>
    '''Returns the gp_parms value for the key and parm requested
    ''' </summary>
    ''' <param name="key">the key</param>
    ''' <param name="parm">the parameter name</param>
    ''' <param name="updtOnly">if true, then the sql gets appended with additional text</param>
    ''' <returns>the gp_parms valu</returns>
    Private Shared Function allowPgEntry(ByVal secCd As String, ByVal empCd As String) As Boolean

        Dim allow As Boolean = False

        If SecurityUtils.hasSecurity(secCd, empCd) Then

            allow = True
        End If

        Return allow
    End Function

    ''' <summary>
    ''' Returns a new oracle connection based on the type(ERP or LOCAL) of connection specified.
    ''' </summary>
    ''' <param name="conn_tp">the type, whcih can be ERP or LOCAL </param>
    ''' <returns>a new oracle connection </returns>
    Public Shared Function GetConn(ByVal conn_tp As String) As OracleConnection
        ' returns a connection

        Dim conn As OracleConnection

        If ConfigurationManager.ConnectionStrings(conn_tp) Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings(conn_tp).ConnectionString.Trim() = "" Then

            Throw New Exception("Connection Error")

        Else
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings(conn_tp).ConnectionString)
        End If

        Return conn

    End Function

    ''' <summary>
    ''' Returns the full  format of the passed-in, abbreviated expiration date where the day is the
    ''' last day of the month passed-in. e.g "1220" becomes "12/31/20"
    ''' </summary>
    ''' <param name="shortExpDate">the short form of the expiration date without any delimiters e.g. "1220"</param>
    ''' <returns>the full  format of the passed-in, abbreviated expiration date e.g "06/30/2010"</returns>
    Public Shared Function GetFullExpirationDate(ByVal shortExpDate As String) As Date

        Dim fullDt As Date
        If (shortExpDate.isNotEmpty) Then
            Dim parsedDt As String = Left(shortExpDate, 2) & ("/1/") & Right(shortExpDate, 2)
            fullDt = DateSerial(Year(parsedDt), Month(parsedDt) + 1, 0) 'gives the end of the month date
        End If

        Return fullDt

    End Function

    ''' <summary>
    ''' Checks if the user passed-in has been setup to override all securities, essentially
    ''' making the user as if it has all security access. <b>True</b> means that the user
    ''' has all the securities.
    ''' </summary>
    ''' <param name="empCd">the employee code for which to check priveleges</param>
    ''' <returns>a flag if <b>true</b>indicates that the employee has the security override 
    '''          and <b>false</b>if not.</returns>
    Public Shared Function HasSecurityOverride(ByVal empCd As String) As Boolean

        ' TODO - we need to cache this data instead of so many db hits
        Dim rtnVal As Boolean = False

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dataRdr As OracleDataReader
        Dim sql As String = "SELECT PRG FROM EMP WHERE EMP.EMP_CD = :empCd"
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        cmd.Parameters.Add(":empCd", OracleType.VarChar)
        cmd.Parameters(":empCd").Value = empCd + ""

        Try
            conn.Open()
            dataRdr = DisposablesManager.BuildOracleDataReader(cmd)


            If (dataRdr.Read) Then
                rtnVal = (dataRdr.Item("PRG").ToString = "Y")
            End If
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal
    End Function

    ''' <summary>
    ''' Class that can be used to sort two files by comparing their file names.
    ''' </summary>
    Public Class FileSorter
        Implements IComparer

        ''' <summary>
        ''' Compares the passed-in objects(should be of type 'FileInfo') to see if
        ''' their names are the same or not.
        ''' </summary>
        ''' <param name="x">the first FileInfo object</param>
        ''' <param name="y">the second FileInfo object to compare</param>
        ''' <returns>a value to indicate if the names of the two files are the same, less or more than the other</returns>
        ''' <remarks></remarks>
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

            Dim result As Integer = 0
            If ((TypeOf x Is FileInfo) AndAlso (TypeOf y Is FileInfo)) Then
                Dim file1 As FileInfo = CType(x, FileInfo)
                Dim file2 As FileInfo = CType(y, FileInfo)

                result = file1.Name.CompareTo(file2.Name)
            End If

            Return result
        End Function
    End Class

    Public Class StringSorter
        Implements IComparer

        ''' <summary>
        ''' Compares the passed-in objects(should be of type 'String') to see if
        ''' they are the same or not.
        ''' </summary>
        ''' <param name="x">the first string object</param>
        ''' <param name="y">the second string  object to compare</param>
        ''' <returns>a value to indicate if the two strings are the same, less or more than the other,
        '''          in terms of the sort order.</returns>
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

            'Dim result = (Function(a As String, b As String) String.Compare(a, b))
            'Return CType(result, Integer)

            Dim result As Integer = 0
            If ((TypeOf x Is String) AndAlso (TypeOf y Is String)) Then
                Dim s1 As String = CType(x, String)
                Dim s2 As String = CType(y, String)

                result = String.Compare(s1, s2)
            End If

            Return result
        End Function
    End Class

    Public Shared Function isNumber(ByVal str As String) As Boolean

        Dim retVal As Boolean = True

        If IsNothing(str) OrElse str.isEmpty OrElse Not IsNumeric(str) Then

            retVal = False
        End If
        Return retVal
    End Function

    Public Shared Function skipDataSecurity(ByVal empCd As String) As Boolean

        ' TODO some emp info including this should be extracted on login and cached
        Dim skipDatSec As Boolean = False
        Dim sql As String = "SELECT SKIP_DATA_SECURITY FROM EMP WHERE EMP_CD = :empCd "

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
                cmd As New OracleCommand(sql, conn)

            cmd.Parameters.Add(":empCd", OracleType.VarChar)
            cmd.Parameters(":empCd").Value = UCase(empCd)

            conn.Open()
            Dim oraDatRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


            If oraDatRdr.HasRows Then

                oraDatRdr.Read()
                If oraDatRdr.Item("SKIP_DATA_SECURITY").ToString = "Y" Then

                    skipDatSec = True
                End If
            End If
        End Using

        Return skipDatSec
    End Function

    Public Shared Function hasAllStoreAccess(ByVal empCd As String) As Boolean

        Dim hasDatSecAcc As Boolean = False

        If ConfigurationManager.AppSettings("store_based") = "N" OrElse
            (ConfigurationManager.AppSettings("store_based") = "Y" AndAlso skipDataSecurity(empCd)) Then

            hasDatSecAcc = True
        End If

        Return hasDatSecAcc
    End Function

    ''' <summary>
    ''' to determine if a string is nothing, dbNull, empty, white space, null or blank all in one routine; if the input is empty, replaces input with empty string
    ''' </summary>
    ''' <param name="input">an object assumed to have a string value</param>
    ''' <returns>true if the object is nothing, dbNull, empty, white space, null or blank</returns>
    ''' <remarks>Object is input but assumes string</remarks>
    Public Shared Function isEmpty(ByRef input As Object) As Boolean

        If IsNothing(input) OrElse IsDBNull(input) Then
            input = ""
        End If

        Return (String.IsNullOrEmpty(input) Or String.IsNullOrWhiteSpace(input))
    End Function

    ''' <summary>
    ''' to determine if a string is nothing, dbNull, empty, white space, null or blank all in one routine; if the input is empty, replaces input with empty string
    ''' </summary>
    ''' <param name="input">an object assumed to have a string value</param>
    ''' <returns>false if the object is nothing, dbNull, empty, white space, null or blank</returns>
    ''' <remarks>Object is input but assumes string</remarks>
    Public Shared Function isNotEmpty(ByRef input As String) As Boolean

        Return Not (isEmpty(input))
    End Function

    ''' <summary>
    ''' to determine if a datatable is nothing, has at least one row, at least one row has value information
    ''' </summary>
    ''' <param name="datTbl">an object assumed to have a string value</param>
    ''' <returns>true if table has at least one row with information in it; false if table is nothing or no rows or no values</returns>
    Public Shared Function dataTblIsNotEmpty(ByRef datTbl As DataTable, Optional ByVal colNum As Integer = 0) As Boolean

        Dim resp As Boolean = False
        If Not (IsNothing(datTbl) OrElse datTbl.Rows.Count < 1 OrElse IsNothing(datTbl.Rows(0)) OrElse
            IsNothing(datTbl.Rows(0).Item(colNum)) OrElse IsNothing(datTbl.Rows(0).Item(colNum))) Then
            ' OrElse IsdbNull(datTbl.Rows(0).Item(colNum))Then ' this is probably more what we need but means that always have to input primary key or reference a specific column, not now

            resp = True
        End If
        Return resp
    End Function

    Public Shared Function dataSetHasRows(ByRef datSet As DataSet, Optional ByVal tblNum As Integer = 0) As Boolean

        Return ((Not IsNothing(datSet)) AndAlso (Not IsNothing(datSet.Tables(tblNum))) AndAlso datSet.Tables(tblNum).Rows.Count > 0)
        ' TODO - consider adding datset.Tables.Count > 1 before referencing tblNum
    End Function

    Public Shared Function dataRowIsNotEmpty(ByRef datRow As DataRow, Optional ByVal colNum As Integer = 0) As Boolean

        Return ((Not IsNothing(datRow)) AndAlso (Not IsNothing(datRow.Item(colNum))))
    End Function

    Public Shared Function MonthLastDay(ByVal dCurrDate As Date)

        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    ''' <summary>
    ''' Utility function to simplify building queries using a WHERE x IN (...) construct.
    ''' </summary>
    ''' <param name="listOfValues">Values to be used in the list</param>
    ''' <param name="prefix">Start of the parameter name</param>
    ''' <param name="cmd">Oracle command to define and set the parameters for the list.  Modified as a side effect!</param>
    ''' <returns>The sql fragment that goes INSIDE the () </returns>
    ''' <remarks>MM-9935</remarks>
    <Obsolete("This variation tends to crowd the SGA sql area with lots of unique queries that cannot be shared easily")> _
    Public Shared Function BuildValueList(listOfValues As List(Of String), prefix As String, cmd As OracleCommand) As String
        Dim inList As New StringBuilder
        Dim comma As String = ""
        Dim placeholder As String = ""
        For i As Integer = 0 To listOfValues.Count - 1
            placeholder = prefix + i.ToString("0000")
            inList.Append(comma).Append(placeholder)
            comma = ", "
            cmd.Parameters.Add(placeholder, OracleType.VarChar)
            cmd.Parameters(placeholder).Value = listOfValues(i)
        Next
        Return inList.ToString
    End Function

    ''' <summary>
    ''' Utility function to simplify building queries using a WHERE x IN (...) construct.
    ''' This variation makes better use of the SGA / sqlarea by always building queries with a 
    ''' set number of bind variables.  any "execss" are set to null
    ''' </summary>
    ''' <param name="listOfValues">Values to be used in the list</param>
    ''' <param name="prefix">Start of the parameter name</param>
    ''' <param name="cmd">Oracle command to define and set the parameters for the list.  Modified as a side effect!</param>
    ''' <param name="numberOfBindVars">fixed number of parameters, to make sharing the prepared query in sga more likely</param>
    ''' <returns>The sql fragment that goes INSIDE the () </returns>
    ''' <remarks>MM-9935</remarks>
    Public Shared Function BuildAndBindValueList(listOfValues As List(Of String), prefix As String, cmd As OracleCommand, numberOfBindVars As Integer) As String
        Dim inList As New StringBuilder
        Dim comma As String = ""
        Dim placeholder As String = ""
        For i As Integer = 0 To numberOfBindVars - 1
            placeholder = prefix + i.ToString("0000")
            inList.Append(comma).Append(placeholder)
            comma = ", "
            cmd.Parameters.Add(placeholder, OracleType.VarChar)
            If i < listOfValues.Count Then
                cmd.Parameters(placeholder).Value = listOfValues(i)
            Else
                cmd.Parameters(placeholder).Value = System.DBNull.Value
            End If
        Next
        Return inList.ToString
    End Function


    ''' <summary>
    ''' Utility function to simplify building queries using a WHERE x IN (...) construct.
    ''' This variation helps make better use of the SGA / sqlarea by always building queries with a 
    ''' set number of bind variables.  all values are set to dbnull
    ''' </summary>
    ''' <param name="prefix">Start of the parameter name</param>
    ''' <param name="cmd">Oracle command to define and set the parameters for the list.  Modified as a side effect!</param>
    ''' <param name="numberOfBindVars">fixed number of parameters, to make sharing the prepared query in sga more likely</param>
    ''' <returns>The sql fragment that goes INSIDE the () </returns>
    ''' <remarks>MM-9935</remarks>
    Public Shared Function BuildValueList(prefix As String, cmd As OracleCommand, numberOfBindVars As Integer) As String
        Dim inList As New StringBuilder
        Dim comma As String = ""
        Dim placeholder As String = ""
        For i As Integer = 0 To numberOfBindVars - 1
            placeholder = prefix + i.ToString("0000")
            inList.Append(comma).Append(placeholder)
            comma = ", "
            cmd.Parameters.Add(placeholder, OracleType.VarChar)
            cmd.Parameters(placeholder).Value = System.DBNull.Value
        Next
        Return inList.ToString
    End Function

    ''' <summary>
    ''' This is the companion to the "good citizen" variation of BuildValueList: it will fill in the 
    ''' bind variables (parameters) in the command, from the given starting point in the list, and set any
    ''' "extra" parameters to null.
    ''' </summary>
    ''' <param name="listOfValues">Values to be used in the list</param>
    ''' <param name="prefix">Start of the parameter name</param>
    ''' <param name="cmd">Oracle command into which we need to define and set the parameters for the list.  Modified as a side effect!</param>
    ''' <param name="numberOfBindVars">fixed number of parameters, to make sharing the prepared query in sga more likely</param>
    ''' <param name="startIx">first element in the list to apply set into the command</param>
    ''' <remarks>MM-9935</remarks>
    Public Shared Sub FillMore(listOfValues As List(Of String), prefix As String, cmd As OracleCommand, numberOfBindVars As Integer, startIx As Integer)
        Dim parmName As String = ""
        Dim ix As Integer
        For i As Integer = 0 To numberOfBindVars - 1
            parmName = prefix + i.ToString("0000")
            ix = i + startIx
            If ix < listOfValues.Count Then
                cmd.Parameters(parmName).Value = listOfValues(ix)
            Else
                cmd.Parameters(parmName).Value = System.DBNull.Value
            End If
        Next
    End Sub
End Class

