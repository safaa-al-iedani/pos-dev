﻿Imports Microsoft.VisualBasic
Imports System.Web.UI

Public Class POSBasePage
    Inherits System.Web.UI.Page

    ' this will dispose and remove the references to any IDisposable instances
    ' that were built by (or otherwise registered with) the DisposbalesManager
    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        DisposablesManager.Cleanup()
    End Sub

End Class
