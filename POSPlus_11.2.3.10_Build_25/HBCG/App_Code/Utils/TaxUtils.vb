﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class TaxUtils

    Public Class Tax_Constants
        Public Const lineTaxMethod As String = "L"
        Public Const headerTaxMethod As String = "H"
        Public Const taxPrecision As Integer = 5  ' this is the tax precision for calculations; display remains as currency
    End Class

    Public Shared Function useLineTax(ByVal taxCd As String, Optional ByVal tetCd As String = "") As Boolean  ' TODO DSA - test passing string not assigned to tete
        '  set the value based on the POS+ parameter ConfigurationManager.AppSettings("tax_line").ToString
        '   TODO future - change this to be based on the tax code tax method

        If Not String.IsNullOrEmpty(tetCd) Then

            Return False

        ElseIf "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

            Return True

        Else
            Return False
        End If
        ' BELOW IS UNTESTED
        'Dim taxCdInfo As New taxCodeDetails
        'taxCdInfo = getTaxCodeDetails(taxCd)

        'If Tax_Constants.lineTaxMethod.Equals(taxCdInfo.taxMethod) Then
        '    Return True
        'Else
        '    Return False
        'End If

    End Function

    Public Shared Function isHdrTax(ByVal taxCd As String) As Boolean  ' TODO DSA - test passing string not assigned to tete
        '  set the value based on the POS+ parameter ConfigurationManager.AppSettings("tax_line").ToString
        '   TODO future - change this to be based on the tax code tax method

        If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then

            Return False

        Else
            Return True
        End If
        ' BELOW IS UNTESTED
        'Dim taxCdInfo As New taxCodeDetails
        'taxCdInfo = getTaxCodeDetails(taxCd)

        'If Tax_Constants.lineTaxMethod.Equals(taxCdInfo.taxMethod) Then
        '    Return True
        'Else
        '    Return False
        'End If

    End Function

    Public Shared Function Get_Tax_CD(ByVal pd_store_cd As String, ByVal ship_zip As String, ByVal take_with As String, ByVal wr_store_cd As String, ByVal trn_dt As String, ByVal p_d As String)

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim Mydatareader As OracleDataReader
        Dim tax_cd As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn.Open()
        sql = "bt_tax_wrapper.get_tax_cds"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql.CommandType = CommandType.StoredProcedure

        objsql.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("pu_del_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_zip_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("wr_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("trn_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_cd_curtype", OracleType.Cursor).Direction = ParameterDirection.ReturnValue

        objsql.Parameters("pu_del_i").Value = p_d
        objsql.Parameters("pu_del_store_cd_i").Value = pd_store_cd
        objsql.Parameters("ship_to_zip_cd_i").Value = ship_zip
        objsql.Parameters("taken_with_i").Value = take_with
        objsql.Parameters("wr_store_cd_i").Value = wr_store_cd
        objsql.Parameters("trn_dt_i").Value = Today.Date

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Do While Mydatareader.Read
                tax_cd = tax_cd & Mydatareader.Item("tax_cd").ToString & ";"
            Loop
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Get_Tax_CD = ""
            Return Get_Tax_CD
        End Try

        ' Daniela fix error
        If isNotEmpty(tax_cd) Then
            Get_Tax_CD = Left(tax_cd, Len(tax_cd) - 1)
        Else
            Get_Tax_CD = ""
        End If
        ' End Daniela

    End Function

    Public Class taxCodeDetails
        ' details from the TAX_CD table for the tax code specified

        Property taxCd As String
        Property taxMethod As String
        Property useNsp As Boolean
    End Class

    Public Shared Function getTaxCodeDetails(ByVal taxCd As String) As taxCodeDetails
        '  Get the tax code related information
        ' TODO - future use when need tax code tax method - NOT tested

        Dim sqlStmt As String = "SELECT tax_method, use_nsp FROM tax_cd WHERE tax_cd = :taxCd "

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmt, conn)

        cmd.Parameters.Add("taxCd", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("taxCd").Value = taxCd

        Dim taxDet As New taxCodeDetails
        taxDet.taxCd = taxCd

        Try
            conn.Open()

            Dim datRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


            If datRdr.HasRows AndAlso datRdr.Read AndAlso (Not String.IsNullOrEmpty(datRdr("tax_method").ToString + "")) Then

                taxDet.taxMethod = datRdr("tax_method").ToString
                taxDet.useNsp = HBCG_Utils.ResolveYnToBooleanEquiv(datRdr("use_nsp").ToString + "")

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()

        End Try

        Return taxDet

    End Function

    ''' <summary>
    ''' An Object of taxable amounts for the tax code and tax date to submit for header taxing - see Lin comment in itmTpAmounts
    ''' </summary>
    Public Class taxHdrCalcRequest

        Property taxCd As String
        Property taxDt As DateTime
        Property itmTpAmts As New ItemTypeTaxAmtsDtc()
    End Class

    ''' <summary>
    ''' An Object of tax percentages for the tax code and tax date - see Lin comment in itmTpAmounts
    ''' </summary>
    Public Class taxPcts

        Property taxCd As String
        Property taxDt As DateTime
        Property itmTpPcts As New ItemTypeTaxAmtsDtc
    End Class


    ''' <summary>
    ''' return the appropriate tax rate for the input
    ''' <param name="itmTp">the item type code for a tax rate to be provided for</param>    
    ''' <param name="itmTpAmts">object of tax rate percentages, by item type, previously acquired for a specific tax code and tax date
    ''' for the process </param>
    ''' <returns>tax rate for the item type from the list of tax rates provided</returns>
    ''' <remarks>the tax percentages object for a specific tax code and date must have previously be created by a call to TaxUtils.SetTaxPcts; 
    ''' this object of tax rates can be saved across pages until a new process or until a new tax code/tax date is required</remarks>

    ''' </summary>
    Public Shared Function getTaxRate(ByVal itmTp As String, ByVal itmTpAmts As ItemTypeTaxAmtsDtc) As Double

        Dim taxRate As Double = 0

        Select Case itmTp

            Case AppConstants.Sku.TP_INV
                taxRate = itmTpAmts.lin
            Case AppConstants.Sku.TP_FAB
                taxRate = itmTpAmts.fab
            Case AppConstants.Sku.TP_LAB
                taxRate = itmTpAmts.lab
            Case AppConstants.Sku.TP_MIL
                taxRate = itmTpAmts.mil
            Case AppConstants.Sku.TP_NLN
                taxRate = itmTpAmts.nln
            Case AppConstants.Sku.TP_PAR
                taxRate = itmTpAmts.par
            Case AppConstants.Sku.TP_WAR
                taxRate = itmTpAmts.war
            Case AppConstants.Sku.TP_DEL
                taxRate = itmTpAmts.del
            Case AppConstants.Sku.TP_SETUP
                taxRate = itmTpAmts.setup
            Case AppConstants.Sku.TP_NLT
                taxRate = itmTpAmts.nlt
            Case AppConstants.Sku.TP_SVC
                taxRate = itmTpAmts.svc
            Case AppConstants.Sku.TP_PKG
                taxRate = itmTpAmts.pkg
            Case AppConstants.Sku.TP_DNR
                taxRate = itmTpAmts.dnr
            Case Else
                taxRate = itmTpAmts.lin
        End Select

        Return taxRate
    End Function

    Public Shared Function ComputeHdrTax(ByVal taxReq As taxHdrCalcRequest) As Double
        '  call the E1 header tax module to compute the header tax amounts

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("DECLARE tInRec bt_tax.tax_hdr_in_rectype; tOutRec bt_tax.tax_hdr_out_rectype; taxAmt NUMBER := 0; ").Append(
            "BEGIN gers_util.init('POS+ TaxUtils.ComputeHdrTax', 'Beginning'); ").Append(
            " tInRec.tax_cd := :taxcd; tInRec.tax_dt := :taxDt; tInRec.line_total := :lnTot; tInRec.labor_total := :labTot; ").Append(
            " tInRec.warranty_total := :warTot; tInRec.service_total := :svcTot; tInRec.fabric_total := :fabTot; tInRec.parts_total := :parTot; ").Append(
            " tInRec.mileage_total := :milTot; tInRec.nlt_total := :nltTot; tInRec.nln_total := :nlnTot; tInRec.setup_chg := :setupChg; ").Append(
            " tInRec.del_chg := :delChg; tInRec.ord_tp_cd := NULL; tInRec.order_num := NULL; ").Append(
            " bt_tax.compute_hdr_tax(tInRec, tOutRec); :taxAmt := tOutRec.tax_total; END; ")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add("taxcd", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("taxcd").Value = taxReq.taxCd
        cmd.Parameters.Add("taxDt", OracleType.DateTime).Direction = ParameterDirection.Input
        cmd.Parameters("taxDt").Value = taxReq.taxDt
        cmd.Parameters.Add("lnTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("lnTot").Value = taxReq.itmTpAmts.lin
        cmd.Parameters.Add("labTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("labTot").Value = taxReq.itmTpAmts.lab
        cmd.Parameters.Add("warTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("warTot").Value = taxReq.itmTpAmts.war
        cmd.Parameters.Add("svcTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("svcTot").Value = taxReq.itmTpAmts.svc
        cmd.Parameters.Add("fabTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("fabTot").Value = taxReq.itmTpAmts.fab
        cmd.Parameters.Add("parTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("parTot").Value = taxReq.itmTpAmts.par
        cmd.Parameters.Add("milTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("milTot").Value = taxReq.itmTpAmts.mil
        cmd.Parameters.Add("nltTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("nltTot").Value = taxReq.itmTpAmts.nlt
        cmd.Parameters.Add("nlnTot", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("nlnTot").Value = taxReq.itmTpAmts.nln
        cmd.Parameters.Add("setupChg", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("setupChg").Value = taxReq.itmTpAmts.setup
        cmd.Parameters.Add("delChg", OracleType.Number).Direction = ParameterDirection.Input
        cmd.Parameters("delChg").Value = taxReq.itmTpAmts.del

        ' DNR and PKG are actually part of the line total, to be taxed as inventory;  
        ' if PKG SKU is price split, then the price on package line is 0 and the lines will be
        '    taxed on their individual prices and based on their item types;  If the PK SKU is not
        '    price split, then the component line prices are 0 and the package line will be 
        '    taxed as if all inventory type;  this is may or may not be entirely valid.
        cmd.Parameters.Add("taxAmt", OracleType.Number).Direction = ParameterDirection.Output

        Dim returnVal As Double = 0.0
        Try
            conn.Open()

            cmd.ExecuteNonQuery()

            returnVal = CDbl(cmd.Parameters("taxAmt").Value.ToString)

            conn.Close()

        Catch ex As Exception
            conn.Close()
        End Try

        Return returnVal
    End Function


    ''' <summary>
    ''' Calculates line taxes on the item(item is null in the case of a delivery/setup charge) by calling the E1 API.
    ''' </summary>
    ''' <param name="itm_cd"></param>
    ''' <param name="unit_prc"></param>
    ''' <param name="qty"></param>
    ''' <param name="tax_cd"></param>
    ''' <param name="store_cd"></param>
    ''' <param name="p_d"></param>
    ''' <param name="del_doc_num"></param>
    ''' <param name="d_or_s"></param>
    ''' <returns></returns>
    Public Shared Function ComputeLineTax(ByVal itm_cd As String, ByVal unit_prc As String, ByVal qty As Double,
                                              ByVal tax_cd As String, ByVal store_cd As String, ByVal p_d As String,
                                              ByVal del_doc_num As String, Optional d_or_s As String = "N")

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn.Open()
        sql = "bt_tax_wrapper.compute"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql.CommandType = CommandType.StoredProcedure

        objsql.Parameters.Add("build_table_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("del_setup_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("unit_prc_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("continue_if_itm_cd_invalid_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("spec_ord_itm_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("use_hist_tax_basis_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("hist_tax_basis_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("incl_non_taxable_sku_i", OracleType.VarChar, 15).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_resp_i", OracleType.VarChar, 15).Direction = ParameterDirection.Input
        objsql.Parameters.Add("co_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("so_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("orig_del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ord_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("so_wr_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("orig_so_wr_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("final_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("orig_final_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("pu_del_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        objsql.Parameters.Add("pu_del_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("pu_del_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("cust_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_name_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_st_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_county_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_city_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("ship_to_zip_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("bill_to_name_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("bill_to_st_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("bill_to_county_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("bill_to_city_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("bill_to_zip_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tet_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tet_id_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("del_doc_ln_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("se_part_seq_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("itm_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("des_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_chg_i", OracleType.Number).Direction = ParameterDirection.Input
        objsql.Parameters.Add("audit_file_i", OracleType.VarChar).Direction = ParameterDirection.Input
        objsql.Parameters.Add("tax_resp_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("cust_tax_chg_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("store_tax_chg_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("pri_tax_total_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("sec_tax_total_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("tax_basis_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("level_cd_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("compl_cd_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("action_tp_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("msg_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("cust_tax_adj_io", OracleType.Number).Direction = ParameterDirection.InputOutput
        objsql.Parameters.Add("store_tax_adj_io", OracleType.Number).Direction = ParameterDirection.InputOutput

        objsql.Parameters("build_table_i").Value = "N"
        objsql.Parameters("itm_cd_i").Value = itm_cd
        objsql.Parameters("del_setup_i").Value = d_or_s
        objsql.Parameters("unit_prc_i").Value = CDbl(unit_prc)
        objsql.Parameters("qty_i").Value = CDbl(qty)
        objsql.Parameters("tax_cd_i").Value = tax_cd
        objsql.Parameters("tax_dt_i").Value = Today.Date
        objsql.Parameters("store_cd_i").Value = store_cd
        objsql.Parameters("continue_if_itm_cd_invalid_i").Value = "Y"
        objsql.Parameters("spec_ord_itm_tp_cd_i").Value = DBNull.Value
        If del_doc_num & "" = "" Then
            objsql.Parameters("use_hist_tax_basis_i").Value = "N"
        Else
            objsql.Parameters("use_hist_tax_basis_i").Value = "Y"
        End If
        objsql.Parameters("hist_tax_basis_i").Value = DBNull.Value
        objsql.Parameters("incl_non_taxable_sku_i").Value = "Y"
        objsql.Parameters("tax_resp_i").Value = "C"
        objsql.Parameters("co_cd_i").Value = DBNull.Value
        objsql.Parameters("so_store_cd_i").Value = DBNull.Value
        objsql.Parameters("del_doc_num_i").Value = del_doc_num
        objsql.Parameters("orig_del_doc_num_i").Value = DBNull.Value
        objsql.Parameters("ord_tp_cd_i").Value = DBNull.Value
        objsql.Parameters("so_wr_dt_i").Value = DBNull.Value
        objsql.Parameters("orig_so_wr_dt_i").Value = DBNull.Value
        objsql.Parameters("final_dt_i").Value = DBNull.Value
        objsql.Parameters("orig_final_dt_i").Value = DBNull.Value
        objsql.Parameters("pu_del_dt_i").Value = DBNull.Value
        objsql.Parameters("pu_del_i").Value = p_d
        objsql.Parameters("pu_del_store_cd_i").Value = DBNull.Value
        objsql.Parameters("cust_cd_i").Value = DBNull.Value
        objsql.Parameters("ship_to_name_i").Value = DBNull.Value
        objsql.Parameters("ship_to_st_cd_i").Value = DBNull.Value
        objsql.Parameters("ship_to_county_i").Value = DBNull.Value
        objsql.Parameters("ship_to_city_i").Value = DBNull.Value
        objsql.Parameters("ship_to_zip_cd_i").Value = DBNull.Value
        objsql.Parameters("bill_to_name_i").Value = DBNull.Value
        objsql.Parameters("bill_to_st_cd_i").Value = DBNull.Value
        objsql.Parameters("bill_to_county_i").Value = DBNull.Value
        objsql.Parameters("bill_to_city_i").Value = DBNull.Value
        objsql.Parameters("bill_to_zip_cd_i").Value = DBNull.Value
        objsql.Parameters("tet_cd_i").Value = DBNull.Value
        objsql.Parameters("tet_id_i").Value = DBNull.Value
        objsql.Parameters("del_doc_ln_i").Value = DBNull.Value
        objsql.Parameters("se_part_seq_i").Value = DBNull.Value
        objsql.Parameters("itm_tp_cd_i").Value = DBNull.Value
        objsql.Parameters("des_i").Value = DBNull.Value
        objsql.Parameters("taken_with_i").Value = DBNull.Value
        objsql.Parameters("tax_chg_i").Value = DBNull.Value
        objsql.Parameters("audit_file_i").Value = DBNull.Value
        Try
            objsql.ExecuteNonQuery()
        Catch
            conn.Close()
            ComputeLineTax = "C;0;0;0;0;0;"
            Return ComputeLineTax
        End Try

        conn.Close()

        ComputeLineTax = objsql.Parameters("tax_resp_io").Value & ";" & objsql.Parameters("cust_tax_chg_io").Value & ";" & objsql.Parameters("store_tax_chg_io").Value & ";" & objsql.Parameters("pri_tax_total_io").Value & ";" & objsql.Parameters("sec_tax_total_io").Value & ";" & objsql.Parameters("tax_basis_io").Value & ";"

    End Function

    ''' <summary>
    ''' create an object of tax percentages by item type as the taxes are applied by tax date and tax code
    ''' </summary>
    Private Shared Function GetTaxPcts(ByVal tax_dt As String, ByVal tax_cd As String) As taxPcts

        Dim txPcts As New taxPcts
        txPcts.taxCd = tax_cd
        txPcts.taxDt = tax_dt

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim datRdr As OracleDataReader

        Dim sql As New StringBuilder
        sql.Append("SELECT SUM(DECODE( del_tax, 'Y', pct, 0)) AS delPct, SUM(DECODE( fab_tax, 'Y', pct, 0)) AS fabPct, ").Append(
            "SUM(DECODE( lab_tax, 'Y', pct, 0)) AS labPct, SUM(DECODE( mileage_tax, 'Y', pct, 0)) AS milPct, ").Append(
            "SUM(DECODE( nlt_tax, 'Y', pct, 0)) AS nltPct, SUM(DECODE( parts_tax, 'Y', pct, 0)) AS partPct, ").Append(
            "SUM(DECODE( su_tax, 'Y', pct, 0)) AS setPct, SUM(DECODE( war_tax, 'Y', pct, 0)) AS warPct, ").Append(
            "SUM(pct) AS invPct, MAX(TAX_CD) AS TAX_CD ").Append(
            "FROM TAT, TAX_CD$TAT tt ").Append(
            "WHERE TAT.EFF_DT <= NVL(:taxDt, SYSDATE) AND NVL(TAT.END_DT, TO_DATE(:endOfTime, 'MM/DD/YYYY')) >= NVL(:taxDt, TRUNC(SYSDATE)) ").Append(
            "AND TAT.TAT_CD = tt.TAT_CD ").Append(
            "AND tt.TAX_CD = :taxCd ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add("taxCd", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("taxCd").Value = tax_cd
        cmd.Parameters.Add("taxDt", OracleType.DateTime).Direction = ParameterDirection.Input
        cmd.Parameters("taxDt").Value = FormatDateTime(tax_dt, DateFormat.ShortDate)
        cmd.Parameters.Add("endOfTime", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("endOfTime").Value = SysPms.endOfTime

        Try
            conn.Open()
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.Read() AndAlso datRdr.Item("TAX_CD").ToString.isNotEmpty Then

                txPcts.itmTpPcts = New ItemTypeTaxAmtsDtc
                txPcts.itmTpPcts.del = datRdr.Item("delPct").ToString
                txPcts.itmTpPcts.fab = datRdr.Item("fabPct").ToString
                txPcts.itmTpPcts.lab = datRdr.Item("labPct").ToString
                txPcts.itmTpPcts.mil = datRdr.Item("milPct").ToString
                txPcts.itmTpPcts.nlt = datRdr.Item("nltPct").ToString
                txPcts.itmTpPcts.par = datRdr.Item("partPct").ToString
                txPcts.itmTpPcts.setup = datRdr.Item("setPct").ToString
                txPcts.itmTpPcts.war = datRdr.Item("warPct").ToString
                txPcts.itmTpPcts.lin = datRdr.Item("invPct").ToString
                conn.Close()
            End If
            datRdr.Close()
            conn.Close()
        Catch
            datRdr.Close()
            conn.Close()
            Throw
        End Try

        Return txPcts
    End Function

    ''' <summary>
    ''' create an object of tax percentages by item type as the taxes are applied by tax date and tax code; also sets in session variable
    ''' </summary>
    Public Shared Function SetTaxPcts(ByVal tax_dt As String, ByVal tax_cd As String) As taxPcts

        Dim txPcts As taxPcts

        If tax_cd.isNotEmpty AndAlso tax_dt.isNotEmpty AndAlso IsDate(tax_dt) Then

            If IsNothing(HttpContext.Current.Session("taxPcts")) Then

                txPcts = GetTaxPcts(tax_dt, tax_cd)
                HttpContext.Current.Session("taxPcts") = txPcts

            Else
                txPcts = HttpContext.Current.Session("taxPcts")

                If txPcts.taxCd.isNotEmpty AndAlso txPcts.taxCd = tax_cd AndAlso
                     (Not IsNothing(txPcts.taxDt)) AndAlso txPcts.taxDt = CDate(tax_dt) Then

                    txPcts = HttpContext.Current.Session("taxPcts")

                Else
                    txPcts = GetTaxPcts(tax_dt, tax_cd)
                    HttpContext.Current.Session("taxPcts") = txPcts
                End If
            End If
        End If

        Return txPcts
    End Function

    ' TODO DSA - this is most likely used incorrectly in some places; for instance line create in HBCG_UTILs.createItmTemp, copied out of OrderDetail, is
    '   using this to determine if line taxable and then using one rate for all line types - should probably be getting rate back and applying that
    '   but I don't have time to check all usages now so I don't want to change it now.  And we don't really need to know taxable, if routine returned zero tax, then
    '   line would not get taxed
    Public Shared Function Determine_Taxability(ByVal itm_tp_cd As String, ByVal tax_dt As String, ByVal tax_cd As String, Optional invFlg As String = "")

        Determine_Taxability = "N"
        Dim sql As New StringBuilder

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim MyDataReader As OracleDataReader

        If itm_tp_cd = "PKG" Or itm_tp_cd = "NLN" Then  ' TODO - this is assuming that the package will be expanded - how does that work in E1 and how is handled here; what if not broken out
            ' TODO - in other words, this routine may not be generic enough but at the same time, don't want to mess up where called
            Determine_Taxability = "N"
            Exit Function
        End If

        If itm_tp_cd = "INV" OrElse "Y".Equals(invFlg) Then
            Determine_Taxability = "Y"
            Exit Function
        End If

        ' if the inventory flag was not input, then get it
        If invFlg.isEmpty Then

            sql.Append("select inventory from itm_tp where itm_tp_cd= :itmTpCd and inventory='Y'")
            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":itmTpCd", OracleType.VarChar)
            cmd.Parameters(":itmTpCd").Value = itm_tp_cd

            Try
                conn.Open()
                MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

                If MyDataReader.Read() Then

                    Determine_Taxability = "Y"
                    conn.Close()
                    Exit Function
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

        Dim TAT_QUERY As String = ""
        Select Case itm_tp_cd
            Case AppConstants.Sku.TP_LAB
                TAT_QUERY = "LAB_TAX"
            Case AppConstants.Sku.TP_FAB
                TAT_QUERY = "FAB_TAX"
            Case AppConstants.Sku.TP_NLT
                TAT_QUERY = "NLT_TAX"
            Case AppConstants.Sku.TP_WAR
                TAT_QUERY = "WAR_TAX"
            Case AppConstants.Sku.TP_PAR
                TAT_QUERY = "PARTS_TAX"
            Case AppConstants.Sku.TP_MIL
                TAT_QUERY = "MILEAGE_TAX"
            Case Else
                Determine_Taxability = "N"
                conn.Close()
                Exit Function
        End Select

        sql.Clear()
        sql.Append("SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT ").Append(
            "FROM TAT, TAX_CD$TAT ").Append(
            "WHERE TAT.EFF_DT <= NVL(:taxDt, SYSDATE) AND NVL(TAT.END_DT, TO_DATE(:endOfTime, 'MM/DD/YYYY')) >= NVL(:taxDt, TRUNC(SYSDATE)) ").Append(
            "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ").Append(
            "AND TAX_CD$TAT.TAX_CD= :taxCd ").Append(
            "AND TAT." & TAT_QUERY & " = 'Y' ").Append(
            "GROUP BY TAX_CD$TAT.TAX_CD ")
        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)

        cmd.Parameters.Add("taxCd", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("taxCd").Value = tax_cd   'HttpContext.Current.Session("TAX_CD")
        cmd.Parameters.Add("taxDt", OracleType.DateTime).Direction = ParameterDirection.Input
        cmd.Parameters("taxDt").Value = FormatDateTime(tax_dt, DateFormat.ShortDate)
        cmd.Parameters.Add("endOfTime", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("endOfTime").Value = SysPms.endOfTime

        Try
            If Not conn.State = ConnectionState.Open Then
                conn.Open()
            End If
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Shared Function getTaxPct(ByVal taxCd As String, ByVal taxDt As DateTime, ByVal itmTp As String) As Double
        '  Get the tax percent for the tax code and tax Date provided based on the item type requested
        '    Pass the itm_tp_cd for SKU's and 'DEL' for delivery charges and 'SETUP' for setup charges

        Dim taxPct As Double = 0.0

        ' NLN is non-taxable so just return 0.0 tax pcr
        If taxCd.isNotEmpty AndAlso IsDate(taxDt) AndAlso Not "NLN".Equals(itmTp) Then

            Dim sqlStmtSb As New StringBuilder
            sqlStmtSb.Append("SELECT TAX_CD$TAT.TAX_CD as tax_cd, Sum(TAT.PCT) AS SumOfPCT ").Append(
            "FROM TAT, TAX_CD$TAT ").Append(
            "WHERE TAT.EFF_DT <= :taxDt And NVL(TAT.END_DT, TO_DATE(:endOfTime, 'MM/DD/YYYY')) >= :taxDt ").Append(
            "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ").Append(
            "AND TAX_CD$TAT.TAX_CD= :taxCd ")

            Select Case itmTp
                Case "DEL"
                    sqlStmtSb.Append("AND TAT.DEL_TAX = 'Y' ")
                Case "SETUP"
                    sqlStmtSb.Append("AND TAT.SU_TAX = 'Y' ")
                Case "FAB"
                    sqlStmtSb.Append("AND TAT.FAB_TAX = 'Y' ")
                Case "LAB"
                    sqlStmtSb.Append("AND TAT.LAB_TAX = 'Y' ")
                Case "MIL"
                    sqlStmtSb.Append("AND TAT.MILEAGE_TAX = 'Y' ")
                Case "NLT"
                    sqlStmtSb.Append("AND TAT.NLT_TAX = 'Y' ")
                Case "PAR"
                    sqlStmtSb.Append("AND TAT.PARTS_TAX = 'Y' ")
                Case "WAR"
                    sqlStmtSb.Append("AND TAT.WAR_TAX = 'Y' ")
                Case "SVC"
                    sqlStmtSb.Append("AND TAT.SVC_TAX = 'Y' ")
                    'Case "NLN"
                    'Case "PKG" 
                    'Case "DNR"  
                    'Case Else ' inventory types -  CPT, INV, APP, ACC

            End Select

            sqlStmtSb.Append("GROUP BY TAX_CD$TAT.TAX_CD ")

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

            cmd.Parameters.Add("taxCd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("taxCd").Value = taxCd
            cmd.Parameters.Add("taxDt", OracleType.DateTime).Direction = ParameterDirection.Input
            cmd.Parameters("taxDt").Value = taxDt
            cmd.Parameters.Add("endOfTime", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("endOfTime").Value = SysPms.endOfTime

            Try
                conn.Open()

                Dim datRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


                If datRdr.HasRows AndAlso
                    datRdr.Read AndAlso
                    (Not String.IsNullOrEmpty(datRdr("tax_cd").ToString + "")) AndAlso
                    IsNumeric(datRdr("SumOfPct").ToString) Then

                    taxPct = CDbl(datRdr("SumOfPct").ToString) / 100

                End If
                conn.Close()

            Catch ex As Exception
                conn.Close()

            End Try
        End If

        Return taxPct

    End Function


    ' MM-4240
    ''' <summary>
    ''' This method will get the TAX % and Maximum_Tax for the tax code from TAT Table
    ''' </summary>
    ''' <param name="taxCd">Tax Code</param>
    ''' <param name="taxDt">Tax Date</param>
    ''' <param name="itmTp">Item Type</param>
    ''' <param name="max_tax">Maximum Tax</param>
    ''' <param name="isMaxTaxSet">isMaxTaxSet</param>
    ''' <returns>Tax Percentage</returns>
    ''' <remarks> I have written this new method to get the tax %, maximum tax and I didnt disturbed the above existing method </remarks>
    Public Shared Function getTaxPct(ByVal taxCd As String, ByVal taxDt As DateTime, ByVal itmTp As String, ByRef max_tax As Double, ByRef isMaxTaxSet As Boolean) As Double
        '  Get the tax percent for the tax code and tax Date provided based on the item type requested
        '  Pass the itm_tp_cd for SKU's and 'DEL' for delivery charges and 'SETUP' for setup charges

        Dim taxPct As Double = 0.0

        ' NLN is non-taxable so just return 0.0 tax pcr
        If taxCd.isNotEmpty AndAlso IsDate(taxDt) AndAlso Not "NLN".Equals(itmTp) Then

            Dim sqlStmtSb As New StringBuilder

            sqlStmtSb.Append("SELECT SUM(tat.pct) AS SumOfPCT, SUM(tat.UP_TO) AS MaxTax ")
            sqlStmtSb.Append("FROM TAT, TAX_CD$TAT ")
            sqlStmtSb.Append("WHERE TAT.EFF_DT <= :tax_dt And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= :tax_dt ")
            sqlStmtSb.Append("AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD ")
            sqlStmtSb.Append("AND TAX_CD$TAT.TAX_CD= :tax_cd ")

            Select Case itmTp
                Case "DEL"
                    sqlStmtSb.Append("AND TAT.DEL_TAX = 'Y' ")
                Case "SETUP"
                    sqlStmtSb.Append("AND TAT.SU_TAX = 'Y' ")
                Case "FAB"
                    sqlStmtSb.Append("AND TAT.FAB_TAX = 'Y' ")
                Case "LAB"
                    sqlStmtSb.Append("AND TAT.LAB_TAX = 'Y' ")
                Case "MIL"
                    sqlStmtSb.Append("AND TAT.MILEAGE_TAX = 'Y' ")
                Case "NLT"
                    sqlStmtSb.Append("AND TAT.NLT_TAX = 'Y' ")
                Case "PAR"
                    sqlStmtSb.Append("AND TAT.PARTS_TAX = 'Y' ")
                Case "WAR"
                    sqlStmtSb.Append("AND TAT.WAR_TAX = 'Y' ")
                Case "SVC"
                    sqlStmtSb.Append("AND TAT.SVC_TAX = 'Y' ")
                    'Case "NLN"
                    'Case "PKG" 
                    'Case "DNR"  
                    'Case Else ' inventory types -  CPT, INV, APP, ACC

            End Select

            sqlStmtSb.Append("GROUP BY TAX_CD$TAT.TAX_CD ")

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

            cmd.Parameters.Add("tax_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("tax_cd").Value = taxCd
            cmd.Parameters.Add("tax_dt", OracleType.DateTime).Direction = ParameterDirection.Input
            cmd.Parameters("tax_dt").Value = taxDt

            Try
                conn.Open()

                Dim datRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


                If datRdr.Read Then
                    If (datRdr.HasRows) Then
                        'taxPct = CDbl(datRdr("SumOfPct").ToString) / 100
                        If Not IsDBNull((datRdr.Item("SumOfPCT"))) Then
                            If IsNumeric(datRdr.Item("SumOfPCT").ToString) Then
                                taxPct = CDbl(datRdr.Item("SumOfPCT").ToString) / 100
                            Else
                                taxPct = 0
                            End If
                        End If

                        If Not IsDBNull((datRdr.Item("MaxTax"))) Then
                            If IsNumeric(datRdr.Item("MaxTax").ToString) Then
                                max_tax = CDbl(datRdr.Item("MaxTax").ToString)
                                isMaxTaxSet = True
                            End If
                        End If
                    End If
                End If

                conn.Close()

            Catch ex As Exception
                conn.Close()

            End Try
        End If

        Return taxPct

    End Function

    Public Shared Function getTaxAmt(ByVal taxCd As String, ByVal taxDt As DateTime, ByVal charge As Double, ByVal itmTp As String) As Double
        '  Get the tax amount for the tax code, tax Date and charge provided based on the item type for the charge
        '    Pass the itm_tp_cd for SKU's and 'DEL' for delivery charges and 'SETUP' for setup charges

        Dim taxAmt As Double = 0.0

        If taxCd.isNotEmpty AndAlso IsNumeric(charge) AndAlso charge > 0.0 AndAlso IsDate(taxDt) Then

            taxAmt = charge * getTaxPct(taxCd, taxDt, itmTp)
        End If

        Return taxAmt

    End Function

    Public Shared Sub AccumTaxingAmts(ByVal itmTp As String, ByVal amt As Double, ByRef itmTpAmts As ItemTypeTaxAmtsDtc)
        '  Add the amount to the appropriate taxing bucket based on the item type code
        '    Pass the itm_tp_cd for SKU's and 'DEL' for delivery charges and 'SETUP' for setup charges

        ' setup and delivery charges are not included in the total of the lines
        If Not ("DEL".Equals(itmTp) OrElse "SETUP".Equals(itmTp)) Then
            itmTpAmts.lin = itmTpAmts.lin + amt
        End If

        Select Case itmTp
            Case AppConstants.Sku.TP_FAB
                itmTpAmts.fab = itmTpAmts.fab + amt
            Case AppConstants.Sku.TP_LAB
                itmTpAmts.lab = itmTpAmts.lab + amt
            Case AppConstants.Sku.TP_MIL
                itmTpAmts.mil = itmTpAmts.mil + amt
            Case AppConstants.Sku.TP_NLN
                itmTpAmts.nln = itmTpAmts.nln + amt
            Case AppConstants.Sku.TP_NLT
                itmTpAmts.nlt = itmTpAmts.nlt + amt
            Case AppConstants.Sku.TP_PAR
                itmTpAmts.par = itmTpAmts.par + amt
            Case AppConstants.Sku.TP_WAR
                itmTpAmts.war = itmTpAmts.war + amt
            Case AppConstants.Sku.TP_DEL
                itmTpAmts.del = itmTpAmts.del + amt
            Case AppConstants.Sku.TP_SETUP
                itmTpAmts.setup = itmTpAmts.setup + amt
            Case AppConstants.Sku.TP_PKG
                itmTpAmts.pkg = itmTpAmts.pkg + amt
            Case AppConstants.Sku.TP_SVC
                itmTpAmts.svc = itmTpAmts.svc + amt
            Case AppConstants.Sku.TP_DNR
                itmTpAmts.dnr = itmTpAmts.dnr + amt
        End Select


    End Sub

End Class