﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Data
Imports System.Linq

Public Class SalesUtils
    ' Utilities specifically for SALES type documents

    Public Class SoLnWarrDtc

        Property docNum As String
        Property docLnNum As Integer
        Property warrDocNum As String
        Property warrDocLnNum As Integer
    End Class

    ''' <summary>
    ''' Locks the SO table record for update for the del_doc_num supplied
    ''' </summary>
    ''' <param name="delDoc">delivery document number</param>
    ''' <param name="cmd">the command object to use for the insert</param>
    ''' <remarks>Throws errors if record not found or record locked; Command parameters are cleared</remarks>
    Public Shared Function LockSoRec(ByVal docNum As String, ByRef dbCmd As OracleCommand) As String

        Dim soRowid As String = String.Empty

        If Not String.IsNullOrEmpty(docNum) Then

            Dim dbRdr As OracleDataReader
            Try
                Dim dbSql As New StringBuilder
                dbSql.Append("SELECT rowid FROM so WHERE del_doc_num = :docNum FOR UPDATE NOWAIT ")

                dbCmd.CommandText = dbSql.ToString
                dbCmd.CommandType = CommandType.Text
                dbCmd.Parameters.Clear()

                dbCmd.Parameters.Add(":docNum", OracleType.VarChar)
                dbCmd.Parameters(":docNum").Value = docNum

                dbRdr = DisposablesManager.BuildOracleDataReader(dbCmd)

                If dbRdr.Read() Then

                    soRowid = dbRdr.Item("rowid").ToString()

                Else       ' if the record is not found
                    Throw New Exception(String.Format(Resources.POSErrors.ERR0041, docNum))
                End If
                dbCmd.Parameters.Clear()

            Catch oraex As OracleException

                If oraex.Code = AppConstants.OraErr.RECORD_LOCKED Then
                    Throw New Exception(String.Format(Resources.POSErrors.ERR0042, docNum))
                Else
                    Throw oraex
                End If
                'Catch ex As Exception
            Finally
                If Not IsNothing(dbRdr) Then
                    dbRdr.Close()
                End If
            End Try

        End If

        Return soRowid
    End Function    

	Public Shared Function lucy_check_store(ByRef p_emp_cd As String, ByRef p_store_cd As String, ByRef p_valid_flag As String)
        ' Lucy 
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_y_n As String = "N"
        Dim p_y_n As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isvalidstore2"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_store_cd", OracleType.VarChar)).Value = p_store_cd
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_ind").Value) = False Then
                p_valid_flag = myCMD.Parameters("p_ind").Value
            End If
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        Catch x
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
            Throw
        Finally

        End Try

        Return p_valid_flag

    End Function

    Public Shared Sub lucy_check_payment(ByRef p_yn As String, ByRef p_session_id As String)
        'lucy
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        ' Daniela added TDF and GC mop code and DCS code
        sql = "select mop_cd from payment where sessionid='" & p_session_id & "'"
        sql = sql & " and (mop_cd = 'VI' or (mop_cd='FI'  and FI_ACCT_NUM is not null  and APPROVAL_CD is not null ) or mop_cd='MC' or mop_cd='FV' or mop_cd='AMX' or mop_cd='DC' or mop_cd = 'TDF' or mop_cd = 'GC' or mop_cd = 'DCS')"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                p_yn = "NO"

            Else

                p_yn = "YES"
            End If
            MyDataReader.Close()
            conn.Close()
            conn.Dispose()
        Catch x
            conn.Close()
            conn.Dispose()
            Throw

        End Try
    End Sub


    ''THIS METHOD CAN BE MOVED TO POSLIB SALESUTIL ONCE ALL METHODS FROM HERE HAVE BEEN MOVED THERE
    ''' <summary>
    ''' Returns the datatable containing the columns needed for displaying a set of order lines. 
    ''' There is no data being added- only the columns specific to order lines are created.
    ''' </summary>
    ''' <remarks>Currently being used by the summary price change approval window.</remarks>
    Public Shared Function GetOrderLinesTable() As DataTable

        Dim table As DataTable = New DataTable

        ' Create the structure for the summary price change table.
        table.Columns.Add("LN_SEQNUM")
        table.Columns.Add("ITM_CD")
        table.Columns.Add("DES")
        table.Columns.Add("QTY")
        table.Columns.Add("ORIG_PRC", Type.GetType("System.Double"))
        table.Columns.Add("UNIT_PRC", Type.GetType("System.Double"))

        Return table
    End Function
    'Alice added on May 14, 2019 for request 4168
    Public Shared Function GetOrderLinesTableNew() As DataTable

        Dim table As DataTable = New DataTable

        ' Create the structure for the summary price change table.
        table.Columns.Add("LN_SEQNUM")
        table.Columns.Add("ITM_CD")
        table.Columns.Add("DES")
        table.Columns.Add("QTY")
        table.Columns.Add("ORIG_PRC", Type.GetType("System.Double"))
        table.Columns.Add("UNIT_PRC", Type.GetType("System.Double"))
        table.Columns.Add("STYLE_CD")
        Return table
    End Function


    ''THIS METHOD CAN BE MOVED TO POSLIB SALESUTIL ONCE ALL METHODS FROM HERE HAVE BEEN MOVED THERE
    ''' <summary>
    ''' Creates and adds a row to the table passed-in based on the specified values.
    ''' This is specific to the table being used for displaying basic order line info.
    ''' </summary>
    ''' <param name="table">the table to add the row to</param>
    ''' <param name="lnSeqNum">the line sequence number</param>
    ''' <param name="itmCd">the item code </param>
    ''' <param name="des">the item description</param>
    ''' <param name="qty">the item qty</param>
    ''' <param name="origPrc">the original price of the sku</param>
    ''' <param name="unitPrc">the unit price of the sku</param>
    ''' <remarks>Currently being used by the summary price change approval window.</remarks>
    Public Shared Sub AddOrderLineRow(ByRef table As DataTable,
                                      ByVal lnSeqNum As String,
                                      ByVal itmCd As String,
                                      ByVal des As String,
                                      ByVal qty As Double,
                                      ByVal origPrc As Double,
                                      ByVal unitPrc As Double)

        Dim row As DataRow
        row = table.NewRow()
        row("LN_SEQNUM") = lnSeqNum
        row("ITM_CD") = itmCd
        row("DES") = des
        row("QTY") = qty
        row("ORIG_PRC") = origPrc
        row("UNIT_PRC") = unitPrc
        table.Rows.Add(row)

    End Sub
    'Alice added on May 14, 2019 for request 4168
    Public Shared Sub AddOrderLineRow(ByRef table As DataTable,
                                      ByVal lnSeqNum As String,
                                      ByVal itmCd As String,
                                      ByVal des As String,
                                      ByVal qty As Double,
                                      ByVal origPrc As Double,
                                      ByVal unitPrc As Double,
                                      ByVal IsOverMaxDiscountPrc As String)

        Dim row As DataRow
        row = table.NewRow()
        row("LN_SEQNUM") = lnSeqNum
        row("ITM_CD") = itmCd
        row("DES") = des
        row("QTY") = qty
        row("ORIG_PRC") = origPrc
        row("UNIT_PRC") = unitPrc
        row("STYLE_CD") = IsOverMaxDiscountPrc
        table.Rows.Add(row)

    End Sub

    ''' <summary>
    ''' Reads the system configuration for the price override parameter. It translates any old
    ''' answers of 'Y' into the current 'L'ine level answer and if null, defaults to 'N'.Otherwise,
    ''' it returns the defined answer as is.
    ''' </summary>
    ''' <returns>the code('L', 'N', 'O') corresponding to the answer for the price override system parameter</returns>
    Public Shared Function GetPriceMgrOverrideSetting() As String

        Dim answer As String = ConfigurationManager.AppSettings("supervisor_overrides").ToString
        If answer.isEmpty Then
            answer = AppConstants.ManagerOverride.NONE
        ElseIf (answer = "Y") Then
            answer = AppConstants.ManagerOverride.BY_LINE
        End If

        Return answer
    End Function

    'MM-5661
    ''' <summary>
    ''' Reads the system configuration for the price override parameter. It translates any old
    ''' answers of 'Y' into the 'W' - With approval and 'N' - Without approval,
    ''' it returns the defined answer as is.
    ''' </summary>
    ''' <returns>the code('L', 'N', 'O') corresponding to the answer for the price override system parameter</returns>
    Public Shared Function GetPriceMgrOverride() As String
        Dim answer As String = ConfigurationManager.AppSettings("supervisor_overrides").ToString
        If answer.Equals(AppConstants.Discount.NOT_AT_ALL) Then
            answer = AppConstants.ManagerOverride.NONE
        ElseIf (answer = AppConstants.ManagerOverride.BY_LINE) Then
            answer = AppConstants.ManagerOverride.BY_LINE
        ElseIf (answer = AppConstants.Discount.WITHOUT_APPROVAL) Then
            answer = AppConstants.ManagerOverride.BY_ORDER
        End If
        Return answer
    End Function

    ''' <summary>
    ''' Inserts a record into the table that establishes the relationship
    ''' between a warrantable and warranty line, based on the object passed-in.
    ''' </summary>
    ''' <param name="soLnWarr">the object encapsulating the line warranty and warrantable info</param>
    ''' <param name="cmd">the command object to use for the insert</param>
    Public Shared Sub InsertSoLnWarr(ByVal soLnWarr As SoLnWarrDtc, ByRef cmd As OracleCommand)

        If (Not String.IsNullOrEmpty(soLnWarr.docNum)) AndAlso
            (Not String.IsNullOrEmpty(soLnWarr.docLnNum)) AndAlso
            HBCG_Utils.isValidLnNum(soLnWarr.docLnNum) Then

            Try
                Dim sql As New StringBuilder
                sql.Append("INSERT INTO so_ln2warr ")
                sql.Append("(del_doc_num, del_doc_ln#, warr_del_doc_num, warr_del_doc_ln#) VALUES ")
                sql.Append("(:delDoc, :lnNum, :warrDelDoc, :warrLnNum ) ")

                cmd.CommandText = sql.ToString
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Clear()

                cmd.Parameters.Add(":delDoc", OracleType.VarChar)
                cmd.Parameters(":delDoc").Value = soLnWarr.docNum
                cmd.Parameters.Add(":lnNum", OracleType.Number)
                cmd.Parameters(":lnNum").Value = soLnWarr.docLnNum
                cmd.Parameters.Add(":warrDelDoc", OracleType.VarChar)
                cmd.Parameters(":warrDelDoc").Value = soLnWarr.warrDocNum  ' TODO DSA - these need to be tested for not null or handle differently
                ' TODO DSA - TBD - how is warranty w/o warrantable handled
                cmd.Parameters.Add(":warrLnNum", OracleType.Number)
                cmd.Parameters(":warrLnNum").Value = soLnWarr.warrDocLnNum

                cmd.ExecuteNonQuery()

            Catch ex As Exception

            End Try
        End If
    End Sub

    ''' <summary>
    ''' Pricing api request object
    ''' </summary>
    Public Class GetCalRetPrcReq

        Property itmCd As String
        Property storeCd As String   ' used if pricing by store or store group
        Property effDt As String     ' used to extract data from calendar
        Property custTpPrcCd As String  'used if pricing by customer type
        Property cashFlg As String   ' special price if pay cash, future use only
        Property itmRetPrc As Double ' this is the ITM table retail price ITM.RET_PRC called item retail price as opposed to the calendar pricing; if have, avoids an extra db hit to get
        ' item retail price is used if no other pricing is setup
    End Class

    ''' <summary>
    ''' Extracts and returns the extended warranties for the SKU input
    ''' </summary>
    ''' <param name="req">True indicates to also extract the calendar retail price for the warranty skus; false returns the warranty price from itm2itm_warr</param>
    ''' <param name="useCalendar">True indicates to also extract the calendar retail price for the warranty skus; false returns the warranty price from itm2itm_warr</param>
    ''' <returns>the dataset of extended warranties for the SKU input </returns>
    ''' <remarks>If you need the calendar price, then you must use this method and provide the data for the calendar pricing  </remarks>
    Public Shared Function GetItmExtWarrs(ByVal req As GetCalRetPrcReq, Optional ByVal useCalendar As Boolean = True) As DataSet

        Dim datSet As DataSet = New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim useCal As Boolean = useCalendar
        ' TODO DSA - see if store code is empty, will the process just return the item retail price?  if so, do not need this
        'If useCal AndAlso req.storeCd.isEmpty Then
        '    useCal = False
        'End If
        'Note - the special VSN as formatted below is used in the warranty selection list - could parameterize
        Dim sql As New StringBuilder
        sql.Append("SELECT WAR_ITM_CD, war_ret_prc,  ")
        If useCal Then
            sql.Append("RPAD(TO_CHAR(bt_price.current_price(war_itm_cd, :storeCd, :effDt, itm.ret_prc, :custTpPrc, 'N', 'REAL'), '$9999.99'), 14 ) || ") ' REAL or INTEGER (REAL * 100)
        End If
        sql.Append("'   --  ' || ITM.DES  || ' (' || ITM.WARR_DAYS || ' DAYS)' AS DES ")  ' TODO - see hasWarranty possibility of combining
        sql.Append("FROM ITM, ITM2ITM_WARR iw ").Append(
                    "WHERE iw.WAR_ITM_CD = ITM.ITM_CD AND iw.ITM_CD = :itmCd ORDER BY ITM.WARR_DAYS")

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = req.itmCd
        If useCal Then
            cmd.Parameters.Add(":storeCd", OracleType.VarChar)
            cmd.Parameters(":storeCd").Value = req.storeCd
            cmd.Parameters.Add(":effDt", OracleType.DateTime)
            cmd.Parameters(":effDt").Value = req.effDt
            cmd.Parameters.Add(":custTpPrc", OracleType.VarChar)
            cmd.Parameters(":custTpPrc").Value = req.custTpPrcCd
        End If
        Dim oAdp As OracleDataAdapter

        Try
            conn.Open()

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)

        Finally
            oAdp.Dispose()
            conn.Close()
        End Try

        Return datSet
    End Function

    ''' <summary>
    ''' Extracts and returns the extended warranties for the SKU input
    ''' </summary>
    ''' <param name="itmCd">the SKU code to find extended warranties for</param>
    ''' <returns>the dataset of extended warranties for the SKU input </returns>
    Public Shared Function GetItmExtWarrs(ByVal itmCd As String) As DataSet

        Dim warrReq As New GetCalRetPrcReq
        warrReq.itmCd = itmCd

        Return GetItmExtWarrs(warrReq, False)
    End Function

    ''' <summary>
    ''' Extracts and returns the manufacturers warranties for the SKU input
    ''' </summary>
    Public Shared Function GetItmManuWarrs(ByVal itmCd As String) As DataSet

        Dim datSet As DataSet = New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As New StringBuilder
        sql.Append("SELECT ITM.VSN, WARR.DAYS, WARR.DES FROM ITM, WARR, ITM2WARR iw WHERE iw.WARR_ID = WARR.WARR_ID ").Append(
                     "AND iw.ITM_CD=ITM.ITM_CD and ITM.ITM_CD = :itmCd ")

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = itmCd
        Dim oAdp As OracleDataAdapter

        Try
            conn.Open()

            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(datSet)

        Finally
            oAdp.Dispose()
            conn.Close()
        End Try

        Return datSet
    End Function

    ''' <summary>
    ''' Returns a new combination number.
    ''' </summary>
    Public Shared Function GetCombinationNo()

        Dim returnVal As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim sqlstmt As String = "Bt_custom_order.get_new_comb_id"
        Dim objSql As OracleCommand
        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        Dim cmd = DisposablesManager.BuildOracleCommand(sqlstmt, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            conn.Open()
            cmd.Parameters.Add("inv_curtype", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()

            returnVal = cmd.Parameters("inv_curtype").Value

        Catch ex As Exception
            Throw New Exception("Error in get_new_comb_id ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnVal

    End Function

    ''' <summary>
    ''' Validates that the passed-in salesperson code correspond to a valid salesperson
    ''' in the system.
    ''' </summary>
    ''' <param name="slsp">the salesperson employee code</param>
    ''' <returns>Returns 'true' if the salesperson is a valid one. 'False' otherwise.</returns>
    Public Shared Function Validate_Slsp(ByVal slsp As String) As Boolean

        Validate_Slsp = False

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        'SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        'SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        'SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        'SQL = SQL & "AND EMP_SLSP.EMP_CD=:SLSP "
        'SQL = SQL & "ORDER BY FullNAME"
        ' Daniela french date issue

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') "
        SQL = SQL & "AND EMP_SLSP.EMP_CD=:SLSP "
        SQL = SQL & "ORDER BY FullNAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objSql.Parameters.Add(":SLSP", OracleType.VarChar)
        objSql.Parameters(":SLSP").Value = UCase(slsp)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Validate_Slsp = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    'Alice added on June 10, 2019 for request 81, don't default SalesPerson 1 to the login user for CSR sale
    Public Shared Function Validate_Slsp1(ByVal slsp As String) As Boolean

        Validate_Slsp1 = False

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        'SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        'SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        'SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        'SQL = SQL & "AND EMP_SLSP.EMP_CD=:SLSP "
        'SQL = SQL & "ORDER BY FullNAME"
        ' Daniela french date issue

        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') "
        SQL = SQL & "AND EMP_SLSP.EMP_CD=:SLSP and EMP_SLSP.SLSP_DEPT_CD <> 'CSR'  "
        SQL = SQL & "ORDER BY FullNAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objSql.Parameters.Add(":SLSP", OracleType.VarChar)
        objSql.Parameters(":SLSP").Value = UCase(slsp)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Validate_Slsp1 = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function



    Public Class OrderSalespersons

        Public slsp1 As MiscLibrary.EmployeeIdentification
        Public slsp2 As MiscLibrary.EmployeeIdentification
    End Class

    ''' <summary>
    ''' Extract the salesperson information from the sales order header
    ''' </summary>
    ''' <param name="slsp">the salesperson employee code</param>
    ''' <returns>Returns 'true' if the salesperson is a valid one. 'False' otherwise.</returns>
    Public Shared Function GetSoHdrSlsps(ByVal delDoc As String) As OrderSalespersons

        Dim ordSlsps As OrderSalespersons

        If (delDoc + "").isNotEmpty Then

            Dim sqlSb As New StringBuilder
            sqlSb.Append("SELECT so_emp_slsp_cd1, e1.emp_init AS slsp1_init, so_emp_slsp_cd2, e2.emp_init AS slsp2_init ").Append(
                 "FROM emp e1, emp e2, so WHERE e1.emp_cd = so.so_emp_slsp_cd1 AND e2.emp_cd (+) = so.so_emp_slsp_cd2 AND del_doc_num = :delDoc ")
            Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)

            cmd.Parameters.Add(":delDoc", OracleType.VarChar)
            cmd.Parameters(":delDoc").Value = delDoc

            Dim datRdr As OracleDataReader

            Try
                conn.Open()
                datRdr = DisposablesManager.BuildOracleDataReader(cmd)


                If datRdr.HasRows Then

                    datRdr.Read()
                    ordSlsps = New OrderSalespersons
                    ordSlsps.slsp1 = New MiscLibrary.EmployeeIdentification
                    ordSlsps.slsp2 = New MiscLibrary.EmployeeIdentification
                    ordSlsps.slsp1.Cd = datRdr("SO_EMP_SLSP_CD1").ToString
                    ordSlsps.slsp2.Cd = datRdr("SO_EMP_SLSP_CD2").ToString + ""
                    ordSlsps.slsp1.Id = datRdr("SLSP1_INIT").ToString
                    ordSlsps.slsp2.Id = datRdr("SLSP2_INIT").ToString + ""
                End If

            Finally
                datRdr.Dispose()
                conn.Close()
                conn.Dispose()
            End Try
        End If

        Return ordSlsps
    End Function

    ''' <summary>
    ''' Retrieves all employees that are valid salespersons and populates a dataset 
    ''' to store the salespersons.
    ''' </summary>
    ''' <returns> a dataset populates with the salesperson info</returns>
    Public Shared Function RetrieveSalespersons() As DataSet

        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim sqlQueryBuilder As New StringBuilder
        sqlQueryBuilder.Append("SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD  ")
        sqlQueryBuilder.Append(" FROM EMP, EMP_SLSP, EMP$EMP_TP ")
        sqlQueryBuilder.Append(" WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD ")
        sqlQueryBuilder.Append(" AND EMP$EMP_TP.EFF_DT <= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') ")
        sqlQueryBuilder.Append(" AND emp.home_store_cd in(select store_cd from store where co_cd = :str_co_cd) ")
        sqlQueryBuilder.Append(" and EMP_SLSP.slsp_dept_cd not in ('TERM','DRIVER','HELPER') ")
        sqlQueryBuilder.Append(" AND EMP$EMP_TP.END_DT >= TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') ORDER BY FullNAME")

        Try
            With cmd
                .Connection = conn
                .CommandText = sqlQueryBuilder.ToString
                .Parameters.Add(":str_co_cd", OracleType.VarChar)
                .Parameters(":str_co_cd").Value = HttpContext.Current.Session("CO_CD").ToString
            End With
            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)
        Catch ex As Exception
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
                conn.Dispose()
            End If
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If
        End Try

        Return ds
    End Function



    Public Shared Function Get_Finance_Credit_Avail(ByVal invoice As String) As Double

        Dim finCredAvail As Double = 0.0
        Dim sqlStmtSb As New StringBuilder

        sqlStmtSb.Append("SELECT SUM(DECODE(ord_tp_cd, 'SAL', orig_fi_amt, 'MDB', orig_fi_amt, 'CRM', -orig_fi_amt, 'MCR', -orig_fi_amt, 0)) as cred ").Append(
            " FROM SO WHERE DEL_DOC_NUM = :invoice OR ORIG_DEL_DOC_NUM = :invoice ")

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
        Dim datRdr As OracleDataReader

        cmd.Parameters.Add(":invoice", OracleType.VarChar)
        cmd.Parameters(":invoice").Value = invoice

        Try
            conn.Open()
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)


            If datRdr.Read() Then
                finCredAvail = datRdr.Item("cred")
            End If
            datRdr.Close()

        Finally
            conn.Close()
        End Try

        Return finCredAvail

    End Function

    ''' <summary>
    ''' Object representing the BT SO_LN table. Not all columns are 
    ''' currently provided here but may be added as needed
    ''' </summary>
    Public Class OrderLine

        Property delDocNum As String
        Property soDocNum As String
        Property lnNum As Integer
        Property itmCd As String   ' TODO object
        Property idNum As String

        Property addonWrDt As Date
        Property voidDt As Date
        Property voidFlag As String
        Property takenWith As String

        Property unitPrc As Double
        Property prcChgAppCd As String
        Property discAmt As String
        Property discs As DataTable '   ??? List(Of SoLnDisc)
        Property puDiscAmt As Double
        Property extdPuDiscAmt As Double ' TODO
        Property qty As Double
        Property extPrc As Double  ' not base

        Property picked As String
        Property serNum As String
        Property storeCd As String  ' TODO inv object
        Property locCd As String    ' TODO
        Property outId As String
        Property outCd As String
        Property fillDt As Date
        Property fiflDt As Date
        Property fifoDt As Date
        Property fifoCst As Double              ' TODO gets and set sb created so can set to empty or not as desired and not have to know
        Property oocQty As Double

        Property pkgSource As String    ' TODO
        Property treatsLnNum As Integer
        Property treatsItmCd As String
        Property treatedByLnNum As Integer
        Property treatedByItmCd As String
        Property warrByDocNum As String
        Property warrByLnNum As Integer
        Property warrBySku As String
        Property refSerNum As String

        Property cmnt As String
        Property setUp As String
        Property lvInCarton As String
        Property delivPts As Double
        Property extdDelivPts As Double ' not base

        Property commCd As String   ' TODO
        Property spiff As Double
        Property empCdSlsp1 As String   ' TODO object
        Property empCdSlsp2 As String
        Property pctOfSale1 As Double
        Property pctOfSale2 As Double
        Property commOnSetupChg As String
        Property commOnDelChg As String

        Property activDt As Date
        Property activPhone As String ' should phone be an object

        Property taxCd As String   ' TODO object
        Property taxBasis As Double
        Property custTaxChg As Double
        Property storeTaxChg As Double
        Property taxResp As String
        Property custTaxAdj As Double
        Property storeTaxAdj As Double

        'Property mfgWarrs As List(Of MfgWarr)   '   TODO ???
        Property stats As DataTable   '    TODO ???
        Property rfChkCnt As Double ' not base
    End Class

    Public Class soLn_updates
        'for Updt_Soln function - indicates whether update of that type is expected to occur

        Property slsprsnInfo As Boolean = False 'indicates if slsp1/2 or their pcts changed
        Property inv As Boolean = False
        Property prc As Boolean = False
        Property qty As Boolean = False
        Property tax As Boolean = False
        Property void As Boolean = False
        Property move As Boolean = False
        Property split As Boolean = False
        Property trtLinks As Boolean = False
        Property warrLinks As Boolean = False
        Property newLn As Boolean = False ' update flags usually for existing lines but inventory update can occur on newly added lines, use this flag to avoid orig comparison because orig doesn't exist newly added 
    End Class

    Public Shared Sub Throw_SoLn_DatCon_Err(ByVal del_doc_num As String, ByVal lnNum As String)
        ' raises error for SO_LN update process

        Throw New Exception("Document " + del_doc_num + ", line " + lnNum + " has been updated by another.  Please re-query and modify again to save.")
    End Sub

    ''' <summary>
    ''' Checks to see if inventory entry has been updated. Qty on line would 
    ''' impact inventory so qty also checked
    ''' </summary>
    ''' <param name="datRdr"></param>
    ''' <param name="origSoLn">the order line class that encapsulates the BT SO_LN table and represents the original line data </param>
    ''' <returns></returns>
    Public Shared Function Is_Inv_Changed(ByRef datRdr As OracleDataReader, ByVal origSoLn As OrderLine) As Boolean

        Dim inv_updated As Boolean = True

        If (datRdr("store_cd").ToString = origSoLn.storeCd AndAlso
            datRdr("loc_cd").ToString = origSoLn.locCd AndAlso
            datRdr("picked").ToString = origSoLn.picked AndAlso
            datRdr("out_cd").ToString = origSoLn.outCd AndAlso
            datRdr("out_id_cd").ToString = origSoLn.outId AndAlso
            datRdr("ser_num").ToString = origSoLn.serNum AndAlso
            datRdr("lv_in_carton").ToString = origSoLn.lvInCarton AndAlso
            HBCG_Utils.NumbersEqual(datRdr("ooc_qty").ToString, origSoLn.oocQty, True) AndAlso
            HBCG_Utils.NumbersEqual(datRdr("fifo_cst").ToString, origSoLn.fifoCst, True) AndAlso
            HBCG_Utils.DatesEqual(datRdr("fifo_dt").ToString, origSoLn.fifoDt, True) AndAlso
            HBCG_Utils.DatesEqual(datRdr("fifl_dt").ToString, origSoLn.fiflDt, True) AndAlso
            HBCG_Utils.DatesEqual(datRdr("fill_dt").ToString, origSoLn.fillDt, True) AndAlso
            datRdr("qty").ToString = origSoLn.qty) Then

            inv_updated = False
        End If

        Return inv_updated
    End Function

    ''' <summary>
    ''' Deletes ALL so line warranty records for the order passed-in. This is 
    ''' usually when the order is voided and the all links needed to be removed.
    ''' </summary>
    ''' <param name="delDocNum">the order whose warranty links need to be removed</param>
    ''' <param name="cmd">teh command object to use for executing the sql</param>
    Public Shared Sub DeleteSoLnWarranties(ByVal delDocNum As String, ByRef cmd As OracleCommand, ByVal soWRDt As String, Optional ByVal isSplitDoc As Boolean = False)

        Dim sql As New StringBuilder
        Dim dtAdapter As OracleDataAdapter
        Dim objSalesDac As New SalesDac
        Dim dtDelDocNum As DataTable
        Dim dtableWarrLines As New DataTable
        Dim dtable As New DataTable
        Try
            cmd.Parameters.Clear()
            cmd.CommandType = CommandType.Text

            If (delDocNum.isNotEmpty) Then
                If isSplitDoc Then
                    sql.Append("SELECT SLW.DEL_DOC_NUM,SLW.DEL_DOC_LN# ,SL.ITM_CD ")
                    sql.Append("  FROM SO_LN2WARR SLW INNER JOIN SO_LN SL ON SLW.WARR_DEL_DOC_NUM=SL.DEL_DOC_NUM AND SLW.WARR_DEL_DOC_LN#=SL.DEL_DOC_LN# ")
                    sql.Append(" WHERE SLW.WARR_DEL_DOC_NUM=:DEL_DOC_NUM AND SLW.DEL_DOC_NUM !=:DEL_DOC_NUM  ")
                    cmd.CommandText = sql.ToString
                    cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                    cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
                    dtAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                    dtAdapter.Fill(dtableWarrLines)
                    For Each row In dtableWarrLines.Rows
                        RemoveWarrLinkLine(cmd, row("DEL_DOC_NUM"), row("DEL_DOC_LN#"), row("ITM_CD"), Convert.ToDateTime(soWRDt))
                    Next
                    sql.Clear()
                    cmd.Parameters.Clear()
                    sql.Append("SELECT DISTINCT(SLN.WARR_DEL_DOC_NUM) AS WARR_DEL_DOC_NUM ,SLN.WARR_DEL_DOC_LN#,S.STAT_CD  FROM SO_LN2WARR SLN ")
                    sql.Append(" INNER JOIN SO S ON S.DEL_DOC_NUM=SLN.WARR_DEL_DOC_NUM WHERE SLN.DEL_DOC_NUM=:DEL_DOC_NUM")
                    cmd.CommandText = sql.ToString
                    cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                    cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
                    dtAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
                    dtAdapter.Fill(dtable)
                    For Each row In dtable.Rows
                        If row("STAT_CD") <> "F" Then
                            dtDelDocNum = objSalesDac.GetAllWarrantedLines(cmd, row("WARR_DEL_DOC_LN#"), row("Warr_del_doc_num"))
                            If Not IsNothing(dtDelDocNum) AndAlso dtDelDocNum.Rows.Count > 0 Then
                                Dim results = (From dtrow In dtDelDocNum.AsEnumerable()
                                            Select Col1 = dtrow.Field(Of String)("DEL_DOC_NUM")
                                            ).Distinct().ToList
                                If Not IsNothing(results) AndAlso results.Count = 1 Then
                                    If results(0).ToString.ToUpper = delDocNum.ToUpper Then
                                        VoidWarrLine(cmd, row("Warr_del_doc_num"), row("WARR_DEL_DOC_LN#"))
                                    End If
                                End If
                            End If
                        End If
                    Next
                    RemoveAllWarrantyLinkInSale(cmd, delDocNum)
                Else
                    'means that the warranty link was removed, so a delete needs to be done
                    RemoveAllWarrantyLinkInSale(cmd, delDocNum)
                End If
            End If
        Catch oex As OracleException
            Throw New Exception("Warranties for Order " + delDocNum + " could not be deleted. Please requery and try again to save.")

        End Try

    End Sub

    ''' <summary>
    ''' Determines based on the data if an Update or Delete needs
    ''' to occur for the warranty  link data for the passed-in order line.
    ''' </summary>
    ''' <param name="origSoLn">the original order line</param>
    ''' <param name="chngSoLn">the updated order line which may have updated warranty link info.</param>
    ''' <param name="cmd">the commnad object needed to execute the db queries</param>
    Private Shared Sub UpdateSoLnWarranties(ByVal origSoLn As OrderLine,
                                            ByVal chngSoLn As OrderLine,
                                            ByRef cmd As OracleCommand,
                                            Optional isSplitDoc As Boolean = False)

        Dim sql As New StringBuilder
        Dim theSystemBiz As SystemBiz = New SystemBiz()
        Dim objSalesDac As New SalesDac
        Dim objSolnWarr As New SoLineWarranty
        Dim dtDelDocNum As DataTable
        Try

            '**** Clear the sql and prepare it for update
            sql.Clear()
            cmd.Parameters.Clear()
            cmd.CommandType = CommandType.Text

            'now determine if the record needs to be deleted or not
            If (origSoLn.warrBySku.isNotEmpty) Then
                If (chngSoLn.warrBySku.isEmpty OrElse chngSoLn.warrByLnNum = 0) Then
                    'means that the warranty link was removed, so a delete needs to be done
                    If isSplitDoc Then
                        objSolnWarr = objSalesDac.GetSoWarrantyLine(cmd, chngSoLn.lnNum, chngSoLn.delDocNum)
                        dtDelDocNum = objSalesDac.GetAllWarrantedLines(cmd, objSolnWarr.warDelDocLn, objSolnWarr.warDelDocNUM)
                        If Not IsNothing(dtDelDocNum) AndAlso dtDelDocNum.Rows.Count > 0 Then
                            If dtDelDocNum.Rows.Count = 1 Then
                                RemoveWarrLinkLine(cmd, chngSoLn.delDocNum, chngSoLn.lnNum, origSoLn.warrBySku, origSoLn.addonWrDt)
                                If objSolnWarr.statCd <> "F" Then
                                    If dtDelDocNum.Rows(0)(0).ToString.ToUpper <> objSolnWarr.warDelDocNUM.ToUpper Then
                                        VoidWarrLine(cmd, objSolnWarr.warDelDocNUM, objSolnWarr.warDelDocLn)
                                    End If
                                End If
                            Else
                                RemoveWarrLinkLine(cmd, chngSoLn.delDocNum, chngSoLn.lnNum, origSoLn.warrBySku, origSoLn.addonWrDt)
                            End If
                        End If
                    Else
                        RemoveWarrLinkLine(cmd, chngSoLn.delDocNum, chngSoLn.lnNum, origSoLn.warrBySku, origSoLn.addonWrDt)
                    End If
                ElseIf (chngSoLn.warrBySku.isNotEmpty AndAlso origSoLn.warrByLnNum <> chngSoLn.warrByLnNum) Then
                    'if orig line waarByLnMum has value & it's difft than the new one then it's a update
                    ' means that the warranty link was changed, so do an update
                    sql.Append("UPDATE so_ln2warr SET ")
                    sql.Append("warr_del_doc_num = :WARR_DEL_DOC_NUM ")
                    sql.Append("warr_del_doc_ln# = :WARR_DEL_DOC_LNNUM ")
                    sql.Append("WHERE del_doc_num = :DEL_DOC_NUM ")
                    sql.Append("AND del_doc_ln# = :DEL_DOC_LNNUM ")

                    cmd.CommandText = sql.ToString
                    cmd.Parameters.Add(":WARR_DEL_DOC_NUM", OracleType.VarChar)
                    cmd.Parameters(":WARR_DEL_DOC_NUM").Value = chngSoLn.warrByDocNum
                    cmd.Parameters.Add(":WARR_DEL_DOC_LNNUM", OracleType.Number)
                    cmd.Parameters(":WARR_DEL_DOC_LNNUM").Value = chngSoLn.warrByLnNum

                    'add common parameters and then execute
                    cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                    cmd.Parameters(":DEL_DOC_NUM").Value = chngSoLn.delDocNum
                    cmd.Parameters.Add(":DEL_DOC_LNNUM", OracleType.Number)
                    cmd.Parameters(":DEL_DOC_LNNUM").Value = chngSoLn.lnNum
                    cmd.ExecuteNonQuery()
                End If
            Else
                'means that the original did not have a link
                If (chngSoLn.warrBySku.isNotEmpty AndAlso chngSoLn.warrByLnNum > 0) Then
                    ' if the orig line did not have a link but now there is a value- it's an insert
                    Dim soLnWarrObj As New SalesUtils.SoLnWarrDtc
                    soLnWarrObj.docNum = chngSoLn.delDocNum
                    soLnWarrObj.docLnNum = chngSoLn.lnNum
                    soLnWarrObj.warrDocNum = chngSoLn.warrByDocNum
                    soLnWarrObj.warrDocLnNum = chngSoLn.warrByLnNum
                    SalesUtils.InsertSoLnWarr(soLnWarrObj, cmd)
                End If
            End If
        Catch oex As OracleException
            Throw New Exception("Warranties for Order " + origSoLn.delDocNum + ", line " + origSoLn.delDocNum + " could not be updated. Please requery and modify again to save.")

        End Try

    End Sub



    ''' <summary>
    ''' Validates if strategic columns of the original record have not been updated since being retrieved. It queries the line 
    ''' from the db,  locks the row and then updates SO_LN if data is not stale. Values in the 'origSoLn' passed-in are compared
    ''' to values in the database to determine if data is stale for type of process being done (inv, tax, price, qty,slspersons, etc.)
    ''' If this function is processing a new line, then caller must provide an 'origSoLn' with del_Doc_num and del_doc_ln# only. No 
    ''' orig values will be compared so others not required. 
    ''' This routine is only called when adding a new line because line insertion has occurred prior to inventory update. So, only
    ''' need to update inv on SO_LN if adding new line  all values updated from the chngSoLn values.
    ''' </summary>
    ''' <param name="origSoLn">the order line class that encapsulates the BT SO_LN table and represents the original line data </param>
    ''' <param name="chngSoLn">the order line class that encapsulates the BT SO_LN table and represents the updated line data </param>
    ''' <param name="updt">class that reprsents a set of the types of columns to update, inv, tax, price, etc.. It also includes a 
    '''                    'this is a new line' flag</param>
    ''' <param name="cmd">the command object ot use for sql functions</param>
    Public Shared Sub Updt_SoLn(ByVal origSoLn As OrderLine, ByVal chngSoLn As OrderLine, ByVal updt As soLn_updates, ByRef cmd As OracleCommand, Optional ByVal updtWarrExpDt As Boolean = False, Optional isSplitDoc As Boolean = False)

        Dim origDiscAmt As Double = If(IsDBNull(origSoLn.discAmt), 0, CDbl(origSoLn.discAmt))
        Dim chngDiscAmt As Double = If(IsDBNull(chngSoLn.discAmt), 0, CDbl(chngSoLn.discAmt))
        Dim updtDiscAmt As Boolean = origDiscAmt <> chngDiscAmt

        Dim updtLvInCarton As Boolean = False
        If (Not (updt.newLn OrElse origSoLn.lvInCarton.Equals(chngSoLn.lvInCarton))) Then
            updtLvInCarton = True
        End If

        ' TODO - perhaps move some of the other simple comparisons in here entirely - void, qty, price
        If updt.prc OrElse updt.qty OrElse updt.inv OrElse updt.tax OrElse updt.void OrElse updt.move OrElse
           updt.split OrElse updt.warrLinks OrElse updt.trtLinks OrElse updtLvInCarton OrElse updtDiscAmt OrElse
           updt.slsprsnInfo Then

            Dim sqlStmtSb As New StringBuilder
            Dim datRdr As OracleDataReader = Nothing
            sqlStmtSb.Append("SELECT sl.rowid as row_id,sl.itm_cd, sl.unit_prc, sl.qty, sl.void_flag, sl.void_dt, sl.cust_tax_chg, sl.tax_basis, sl.tax_cd, ").Append(
                                    "sl.tax_resp, sl.store_cd, sl.loc_cd, sl.out_cd, sl.out_id_cd, sl.ser_num, sl.picked, sl.ooc_qty, sl.fill_dt, ").Append(
                                    "sl.fifl_dt, sl.fifo_dt, sl.fifo_cst, sl.lv_in_carton, sl.warranted_by_ln#, sl.warranted_by_itm_cd, ").Append(
                                    "sl.ref_del_doc_ln#, sl.ref_itm_cd, sl.treated_by_ln#, sl.treated_by_itm_cd,sl.treats_ln#, sl.treats_itm_cd, ").Append(
                                    "sl.so_emp_slsp_cd1, sl.so_emp_slsp_cd2, sl.pct_of_sale1, sl.pct_of_sale2,i.ITM_TP_CD ").Append(
                            "FROM so_ln sl Inner JOIN ITM i on i.ITM_CD=sl.itm_cd ").Append(
                            "WHERE sl.del_Doc_num = :del_doc_num AND sl.del_doc_ln# = :del_doc_lnNum  ").Append(
                            "FOR UPDATE NOWAIT")

            cmd.CommandText = sqlStmtSb.ToString
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Clear()

            cmd.Parameters.Add(":del_doc_num", OracleType.VarChar)
            cmd.Parameters(":del_doc_num").Value = origSoLn.delDocNum
            cmd.Parameters.Add(":del_doc_lnNum", OracleType.VarChar)
            cmd.Parameters(":del_doc_lnNum").Value = origSoLn.lnNum

            Try
                datRdr = DisposablesManager.BuildOracleDataReader(cmd)


                If datRdr.Read() Then
                    ' if line is already voided or the item code has changed, we are done and need to error
                    If "N".Equals(datRdr("void_flag")) AndAlso
                        datRdr("itm_cd").ToString = origSoLn.itmCd Then ' TODO - need to make sure all calls actually put this value in place

                        '**** Clear the sql and prepare it for update
                        sqlStmtSb.Clear()
                        sqlStmtSb.Append("UPDATE so_ln SET ")
                        Dim updtSoLn As Boolean = False

                        ' NOTE THAT THESE SQL appends are created with COMMAS at the end; LAST comma must be removed- is done by
                        '   length, not char so make sure end in comma, NOT space as "...," 
                        ' Assuming not updating a new line to void - not even testing
                        If updt.void Then

                            If datRdr("void_flag").ToString.Equals(origSoLn.voidFlag) AndAlso
                               datRdr("picked").ToString.Equals(origSoLn.picked) AndAlso
                               HBCG_Utils.DatesEqual(datRdr("fill_dt").ToString, origSoLn.fillDt, True) AndAlso
                               Not Is_Inv_Changed(datRdr, origSoLn) Then

                                sqlStmtSb.Append(" void_flag = 'Y', void_dt = :void_dt, picked = 'V', fill_dt = :fill_dt,")
                                updtSoLn = True

                                If datRdr("ITM_TP_CD").ToString.ToUpper() = "WAR" Then
                                    RemoveAllWarrLink(cmd, origSoLn.delDocNum, origSoLn.lnNum, origSoLn.itmCd, origSoLn.addonWrDt)
                                End If
                            Else
                                Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                            End If

                            ' MM-4240
                            ' When the line has been VOIDED then we suppose to remove tax information on SO_LN table
                            If updt.tax Then
                                If (HBCG_Utils.NumbersEqual(datRdr("cust_tax_chg").ToString, origSoLn.custTaxChg, True) AndAlso
                                    datRdr("tax_resp").ToString = origSoLn.taxResp AndAlso
                                    datRdr("tax_cd").ToString = origSoLn.taxCd AndAlso
                                    HBCG_Utils.NumbersEqual(datRdr("tax_basis").ToString, origSoLn.taxBasis, True)) Then
                                    sqlStmtSb.Append(" tax_cd = :tax_cd, tax_resp = :tax_resp, tax_basis = :tax_basis, cust_tax_chg = :cust_tax_chg,")
                                    updtSoLn = True
                                Else
                                    Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                                End If
                            End If


                        Else ' if voiding, no other updates allowed or necessary
                            ' if new line, then don't compare to orig values because no orig to compare to
                            If updt.prc Then
                                If datRdr("unit_prc").ToString = origSoLn.unitPrc Then  ' TODO DSA - should these be diff compares

                                    sqlStmtSb.Append(" unit_prc = :unit_prc,")

                                    If (chngSoLn.prcChgAppCd.isNotEmpty) Then
                                        sqlStmtSb.Append(" PRC_CHG_APP_CD = :PRC_CHG_APP_CD,")
                                    End If
                                    updtSoLn = True
                                Else
                                    Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                                End If
                            End If

                            If updt.trtLinks Then

                                If datRdr("treated_by_ln#").ToString = origSoLn.treatedByLnNum AndAlso
                                    datRdr("treated_by_itm_cd").ToString = origSoLn.treatedByItmCd AndAlso
                                    datRdr("treats_ln#").ToString = origSoLn.treatsLnNum AndAlso
                                    datRdr("treats_itm_cd").ToString = origSoLn.treatsItmCd AndAlso
                                    datRdr("qty").ToString = origSoLn.qty AndAlso
                                    datRdr("unit_prc").ToString = origSoLn.unitPrc Then

                                    sqlStmtSb.Append(" treated_by_ln# = :treatedByLnNum, treats_ln# = :treatsLnNum,")
                                    updtSoLn = True
                                Else
                                    Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                                End If
                            End If

                            If updt.tax Then

                                If (HBCG_Utils.NumbersEqual(datRdr("cust_tax_chg").ToString, origSoLn.custTaxChg, True) AndAlso
                                    datRdr("tax_resp").ToString = origSoLn.taxResp AndAlso
                                    datRdr("tax_cd").ToString = origSoLn.taxCd AndAlso
                                    HBCG_Utils.NumbersEqual(datRdr("tax_basis").ToString, origSoLn.taxBasis, True)) Then
                                    sqlStmtSb.Append(" tax_cd = :tax_cd, tax_resp = :tax_resp, tax_basis = :tax_basis, cust_tax_chg = :cust_tax_chg,")
                                    updtSoLn = True
                                Else
                                    Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                                End If
                            End If

                            'validate salesperson info
                            If updt.slsprsnInfo Then
                                If datRdr("SO_EMP_SLSP_CD1").ToString = origSoLn.empCdSlsp1 AndAlso
                                    datRdr("SO_EMP_SLSP_CD2").ToString = origSoLn.empCdSlsp2 AndAlso
                                    (HBCG_Utils.NumbersEqual(datRdr("PCT_OF_SALE1").ToString, origSoLn.pctOfSale1, True)) AndAlso
                                    (HBCG_Utils.NumbersEqual(datRdr("PCT_OF_SALE2").ToString, origSoLn.pctOfSale2, True)) Then

                                    sqlStmtSb.Append(" so_emp_slsp_cd1=:so_emp_slsp_cd1, so_emp_slsp_cd2=:so_emp_slsp_cd2, pct_of_sale1=:pct_of_sale1, pct_of_sale2=:pct_of_sale2,")
                                    updtSoLn = True
                                End If
                            End If

                            ' if doing any change that would not want inventory changed behind (and therefore not handled in this process), 
                            '    then that would result in an error
                            ' if updating new line with inventory, then can't check for change against orig since there isn't an orig
                            If (Not updt.newLn) AndAlso
                                ((updt.qty OrElse updt.inv OrElse updt.move OrElse updt.split OrElse updtLvInCarton) AndAlso Is_Inv_Changed(datRdr, origSoLn)) Then

                                Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)

                            Else
                                If updt.qty Then

                                    sqlStmtSb.Append(" qty = :qty,")
                                    updtSoLn = True
                                End If

                                If updt.split Then

                                    sqlStmtSb.Append(" qty = :qty, ooc_qty= :ooc_qty,")
                                    updtSoLn = True
                                End If

                                If updt.inv Then

                                    sqlStmtSb.Append(" store_cd = :store_cd, loc_cd = :loc_cd, picked = :picked, ").Append(
                                        " out_cd = :out_cd, out_id_cd = :out_id_cd, ser_num = :ser_num, ooc_qty = :ooc_qty, fifo_cst = :fifo_cst,").Append(
                                        " fifo_dt = :fifo_dt, fifl_dt = :fifl_dt, fill_dt = :fill_dt,")
                                    updtSoLn = True
                                End If

                                If updtLvInCarton Then
                                    sqlStmtSb.Append(" lv_in_carton = :lv_in_carton,")
                                    updtSoLn = True
                                End If

                                If updtDiscAmt Then
                                    sqlStmtSb.Append(" disc_amt = :disc_amt,")
                                    updtSoLn = True
                                End If

                                If updt.move Then
                                    sqlStmtSb.Append(" del_doc_num = :nu_del_doc_num,")
                                    updtSoLn = True
                                End If
                            End If ' set sql for new line inv updt or non-new line updates
                        End If ' check for line voided

                        If updtWarrExpDt AndAlso SysPms.warrantyStartDate <> "W" Then
                            If chngSoLn.warrByLnNum <> 0 Then
                                sqlStmtSb.Append("WAR_EXP_DT =:WAR_EXP_DT,")
                                cmd.Parameters.Add(":WAR_EXP_DT", OracleType.DateTime)
                                cmd.Parameters(":WAR_EXP_DT").Value = Date.Now
                            End If
                        End If

                        If updtSoLn Then
                            sqlStmtSb.Remove(sqlStmtSb.Length - 1, 1) ' remove the ending comma; entries must end in comma, not comma space
                            sqlStmtSb.Append(" WHERE rowid = :row_id ")

                            cmd.Parameters.Clear()
                            cmd.CommandText = sqlStmtSb.ToString
                            cmd.CommandType = CommandType.Text

                            cmd.Parameters.Add(":row_id", OracleType.RowId)
                            cmd.Parameters(":row_id").Value = datRdr("row_id").ToString

                            ' MM-4240
                            ' Tax information should be updated on SO_LN table regardless
                            If updt.tax Then
                                cmd.Parameters.Add(":tax_cd", OracleType.VarChar)
                                cmd.Parameters(":tax_cd").Value = chngSoLn.taxCd
                                cmd.Parameters.Add(":tax_resp", OracleType.VarChar)
                                cmd.Parameters(":tax_resp").Value = chngSoLn.taxResp
                                cmd.Parameters.Add(":tax_basis", OracleType.Number)
                                cmd.Parameters(":tax_basis").Value = IIf(NumberEmpty(chngSoLn.taxBasis), System.DBNull.Value, chngSoLn.taxBasis)
                                cmd.Parameters.Add(":cust_tax_chg", OracleType.Number)
                                cmd.Parameters(":cust_tax_chg").Value = IIf(NumberEmpty(chngSoLn.custTaxChg), System.DBNull.Value, chngSoLn.custTaxChg)
                            End If

                            If updt.void Then
                                cmd.Parameters.Add(":void_dt", OracleType.DateTime)
                                cmd.Parameters(":void_dt").Value = FormatDateTime(Today.Date, DateFormat.ShortDate) ' TODO - this sb tran dt
                                cmd.Parameters.Add(":fill_dt", OracleType.DateTime)
                                cmd.Parameters(":fill_dt").Value = FormatDateTime(Today.Date, DateFormat.ShortDate) ' TODO - this sb tran dt
                            Else
                                If updt.prc Then
                                    cmd.Parameters.Add(":unit_prc", OracleType.Number)
                                    cmd.Parameters(":unit_prc").Value = chngSoLn.unitPrc
                                    If (chngSoLn.prcChgAppCd.isNotEmpty) Then
                                        cmd.Parameters.Add(":PRC_CHG_APP_CD", OracleType.VarChar)
                                        cmd.Parameters(":PRC_CHG_APP_CD").Value = chngSoLn.prcChgAppCd
                                    End If
                                End If
                                If updt.qty Then
                                    cmd.Parameters.Add(":qty", OracleType.Number)
                                    cmd.Parameters(":qty").Value = chngSoLn.qty
                                End If
                                If updt.split Then
                                    cmd.Parameters.Add(":qty", OracleType.Number)
                                    cmd.Parameters(":qty").Value = chngSoLn.qty
                                    cmd.Parameters.Add(":ooc_qty", OracleType.Number)
                                    cmd.Parameters(":ooc_qty").Value = IIf(NumberEmpty(chngSoLn.oocQty), System.DBNull.Value, chngSoLn.oocQty)
                                End If
                                If updt.move Then
                                    cmd.Parameters.Add(":nu_del_doc_num", OracleType.VarChar)
                                    cmd.Parameters(":nu_del_doc_num").Value = chngSoLn.delDocNum
                                End If
                                If updt.inv Then
                                    cmd.Parameters.Add(":store_cd", OracleType.VarChar)
                                    cmd.Parameters(":store_cd").Value = chngSoLn.storeCd
                                    cmd.Parameters.Add(":loc_cd", OracleType.VarChar)
                                    cmd.Parameters(":loc_cd").Value = chngSoLn.locCd
                                    cmd.Parameters.Add(":picked", OracleType.VarChar)
                                    cmd.Parameters(":picked").Value = chngSoLn.picked
                                    cmd.Parameters.Add(":out_cd", OracleType.VarChar)
                                    cmd.Parameters(":out_cd").Value = IIf(IsNothing(chngSoLn.outCd), System.DBNull.Value, chngSoLn.outCd)
                                    cmd.Parameters.Add(":out_id_cd", OracleType.VarChar)
                                    cmd.Parameters(":out_id_cd").Value = IIf(IsNothing(chngSoLn.outId), System.DBNull.Value, chngSoLn.outId)
                                    cmd.Parameters.Add(":ser_num", OracleType.VarChar)
                                    cmd.Parameters(":ser_num").Value = chngSoLn.serNum
                                    cmd.Parameters.Add(":ooc_qty", OracleType.Number)
                                    cmd.Parameters(":ooc_qty").Value = IIf(NumberEmpty(chngSoLn.oocQty), System.DBNull.Value, chngSoLn.oocQty)
                                    cmd.Parameters.Add(":fifo_cst", OracleType.Number)
                                    cmd.Parameters(":fifo_cst").Value = IIf(NumberEmpty(chngSoLn.fifoCst), System.DBNull.Value, chngSoLn.fifoCst)
                                    cmd.Parameters.Add(":fifo_dt", OracleType.DateTime)
                                    cmd.Parameters(":fifo_dt").Value = IIf(DateEmpty(chngSoLn.fifoDt), System.DBNull.Value, chngSoLn.fifoDt)
                                    cmd.Parameters.Add(":fifl_dt", OracleType.DateTime)
                                    cmd.Parameters(":fifl_dt").Value = IIf(DateEmpty(chngSoLn.fiflDt), System.DBNull.Value, chngSoLn.fiflDt)
                                    cmd.Parameters.Add(":fill_dt", OracleType.DateTime)
                                    cmd.Parameters(":fill_dt").Value = IIf(DateEmpty(chngSoLn.fillDt), System.DBNull.Value, chngSoLn.fillDt)
                                End If
                                If updt.slsprsnInfo Then
                                    cmd.Parameters.Add(":so_emp_slsp_cd1", OracleType.VarChar)
                                    cmd.Parameters(":so_emp_slsp_cd1").Value = chngSoLn.empCdSlsp1
                                    cmd.Parameters.Add(":so_emp_slsp_cd2", OracleType.VarChar)
                                    cmd.Parameters(":so_emp_slsp_cd2").Value = chngSoLn.empCdSlsp2

                                    cmd.Parameters.Add(":pct_of_sale1", OracleType.Number)
                                    cmd.Parameters(":pct_of_sale1").Value = IIf(IsNumeric(chngSoLn.pctOfSale1), CDbl(chngSoLn.pctOfSale1), System.DBNull.Value)
                                    cmd.Parameters.Add(":pct_of_sale2", OracleType.Number)
                                    cmd.Parameters(":pct_of_sale2").Value = IIf(IsNumeric(chngSoLn.pctOfSale2), CDbl(chngSoLn.pctOfSale2), System.DBNull.Value)
                                End If
                                If updt.trtLinks Then
                                    cmd.Parameters.Add(":treated_by_ln#", OracleType.Number)
                                    cmd.Parameters(":treated_by_ln#").Value = IIf(NumberEmpty(chngSoLn.treatedByLnNum), System.DBNull.Value, chngSoLn.treatedByLnNum)
                                    cmd.Parameters.Add(":treats_ln#", OracleType.Number)
                                    cmd.Parameters(":treats_ln#").Value = IIf(NumberEmpty(chngSoLn.treatsLnNum), System.DBNull.Value, chngSoLn.treatsLnNum)
                                End If
                                If updtLvInCarton Then
                                    cmd.Parameters.Add(":lv_in_carton", OracleType.VarChar)
                                    cmd.Parameters(":lv_in_carton").Value = chngSoLn.lvInCarton
                                End If
                                If updtDiscAmt Then
                                    cmd.Parameters.Add(":disc_amt", OracleType.VarChar)
                                    If (chngDiscAmt = 0) Then
                                        cmd.Parameters(":disc_amt").Value = DBNull.Value
                                    Else
                                        cmd.Parameters(":disc_amt").Value = chngDiscAmt
                                    End If
                                End If

                            End If
                            If updtWarrExpDt AndAlso SysPms.warrantyStartDate <> "W" Then
                                If chngSoLn.warrByLnNum <> 0 Then
                                    cmd.Parameters.Add(":WAR_EXP_DT", OracleType.DateTime)
                                    cmd.Parameters(":WAR_EXP_DT").Value = Date.Now
                                End If
                            End If
                            cmd.ExecuteNonQuery()
                            cmd.Parameters.Clear()
                        End If  'end if updtSoLn

                        If updt.warrLinks Then
                            If datRdr("qty").ToString = origSoLn.qty Then
                                UpdateSoLnWarranties(origSoLn, chngSoLn, cmd, isSplitDoc)
                            End If
                        End If

                    Else ' if line has been voided elsewhere or itm code changed
                        Throw_SoLn_DatCon_Err(origSoLn.delDocNum, origSoLn.lnNum)
                    End If
                Else  ' if line was not found
                    Throw New Exception("Document " + origSoLn.delDocNum + ", line " + origSoLn.lnNum + " was not found.  The operation could not be completed.")
                End If

            Catch oex As OracleException
                Throw oex
            Finally
                datRdr.Close()
            End Try
        End If
    End Sub

    Public Shared Function hasWarranty(ByVal itmCds As String) As Boolean

        Dim hasWarr As Boolean = False

        Dim conn As OracleConnection = HBCG_Utils.GetConn(HBCG_Utils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable = New DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet
        Dim loop1 As Integer

        Dim sql As String = "SELECT WAR_ITM_CD, WAR_RET_PRC, ITM.VSN  || ' (' || ITM.WARR_DAYS || ' DAYS) ' AS VSN FROM ITM, ITM2ITM_WARR "
        sql = sql & "WHERE (ITM2ITM_WARR.WAR_ITM_CD = ITM.ITM_CD) AND ITM2ITM_WARR.ITM_CD IN('" & itmCds & "') ORDER BY ITM.WARR_DAYS"

        conn.Open()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        sAdp.Fill(ds)
        MyTable = ds.Tables(0)
        Dim numrows As Integer = MyTable.Rows.Count

        For loop1 = 0 To numrows - 1
            If MyTable.Rows(loop1).Item("war_itm_cd").ToString & "" <> "" Then
                hasWarr = True
                Exit For
            End If
        Next
        conn.Close()

        Return hasWarr
    End Function

    Public Shared Function GetTempItmRowSeqNum(ByVal row_Id As String) As String
        ' Extract and return the TEMP_ITM.ROW_ID for the input rowid 
        '  TEMP_ITM ROW_ID is a 10 digit table column that holds a unique sequential number 
        '  Not to be confused with the Oracle rowid input here to find the table column by
        '  

        Dim row_id_seq_num As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = "SELECT ROW_ID FROM TEMP_ITM WHERE rowid = :row_id "
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Input
        cmd.Parameters(":row_id").Value = row_Id
        Dim datRdr As OracleDataReader

        Try
            conn.Open()
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.Read() Then
                If datRdr.Item("ROW_ID").ToString & "" <> "" Then
                    row_id_seq_num = datRdr.Item("ROW_ID").ToString
                End If
            End If
        Finally
            conn.Close()
        End Try
        Return row_id_seq_num

    End Function

    Public Shared Function GetSoLnNum(ByVal row_Id As String) As String
        ' Extract and return the SO_LN.DEL_DOC_LN# for the input Oracle rowid 
        '  
        Dim lnNum As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = "SELECT del_doc_ln# FROM so_ln WHERE rowid = :row_id "
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Input
        cmd.Parameters(":row_id").Value = row_Id
        Dim datRdr As OracleDataReader

        Try
            conn.Open()
            datRdr = DisposablesManager.BuildOracleDataReader(cmd)

            If datRdr.Read() Then
                If datRdr.Item("ROW_ID").ToString & "" <> "" Then
                    lnNum = datRdr.Item("ROW_ID").ToString
                End If
            End If
        Finally
            conn.Close()
        End Try
        Return lnNum

    End Function

    Private Shared Function DateEmpty(ByVal dt As Date) As Boolean

        Dim dtEmpty As Boolean = False
        Dim emptyDt As New Date
        If IsDate(dt) AndAlso dt = emptyDt Then
            dtEmpty = True
        End If
        Return dtEmpty = True
    End Function

    Private Shared Function NumberEmpty(ByVal numb As Double) As Boolean

        Dim numEmpty As Boolean = False
        If Double.IsNaN(numb) Then
            numEmpty = True
        End If
        Return numEmpty
    End Function

    ''' <summary>
    ''' Delete all the warranty links for the sale when the sale is voided.
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="delDocNum"></param>
    ''' <remarks></remarks>
    Private Shared Sub RemoveAllWarrantyLinkInSale(ByRef cmd As OracleCommand, ByVal delDocNum As String)
        Dim sql As New StringBuilder
        Try
            Sql.Clear()
            cmd.Parameters.Clear()
            Sql.Append("DELETE FROM so_ln2warr ")
            Sql.Append("WHERE del_doc_num = :DEL_DOC_NUM ")
            cmd.CommandText = Sql.ToString
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Delete all the warranty link for the warranty when the warranety is voided.
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="delDocNum"></param>
    ''' <param name="lineNum"></param>
    ''' <param name="warrentedSku"></param>
    ''' <param name="wtDt"></param>
    ''' <remarks></remarks>
    Private Shared Sub RemoveAllWarrLink(ByRef cmd As OracleCommand, ByVal delDocNum As String, ByVal lineNum As Integer, ByVal warrentedSku As String, ByVal wtDt As Date)
        Dim sql As New StringBuilder
        Dim dataAdaptr As OracleDataAdapter
        Dim dtable As New DataTable
        Try
            Sql.Clear()
            cmd.Parameters.Clear()
            Sql.Append("SELECT DEL_DOC_NUM,DEL_DOC_LN# FROM SO_LN2WARR WHERE  WARR_DEL_DOC_NUM =:DEL_DOC_NUM and WARR_DEL_DOC_LN#=:DEL_DOC_LNNUM  AND DEL_DOC_NUM!=:DEL_DOC_NUM")
            cmd.CommandText = Sql.ToString
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.Parameters.Add(":DEL_DOC_LNNUM", OracleType.Number)
            cmd.Parameters(":DEL_DOC_LNNUM").Value = lineNum
            dataAdaptr = DisposablesManager.BuildOracleDataAdapter(cmd)
            dataAdaptr.Fill(dtable)
            If dtable.Rows.Count > 0 Then
                For Each row In dtable.Rows
                    RemoveWarrLinkLine(cmd, row("DEL_DOC_NUM"), row("DEL_DOC_LN#"), warrentedSku, wtDt)
                Next
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Remove the warranty link for the line.
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="delDocNum"></param>
    ''' <param name="lineNum"></param>
    ''' <param name="warrentedSku"></param>
    ''' <param name="wtDt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RemoveWarrLinkLine(ByRef cmd As OracleCommand, ByVal delDocNum As String, ByVal lineNum As Integer, ByVal warrentedSku As String, ByVal wtDt As Date)
        Dim theSystemBiz As New SystemBiz
        Dim sql As New StringBuilder
        Try
            Sql.Clear()
            cmd.Parameters.Clear()
            Sql.Append("DELETE FROM SO_LN2WARR ")
            Sql.Append("WHERE DEL_DOC_NUM = :DEL_DOC_NUM ")
            Sql.Append("AND DEL_DOC_LN# = :DEL_DOC_LNNUM ")
            cmd.CommandText = Sql.ToString

            'add common parameters and then execute
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            cmd.Parameters.Add(":DEL_DOC_LNNUM", OracleType.Number)
            cmd.Parameters(":DEL_DOC_LNNUM").Value = lineNum
            cmd.ExecuteNonQuery()
            HBCG_Utils.SaveAutoComment(cmd, delDocNum, wtDt.ToString, "WLR", lineNum.ToString, warrentedSku, HttpContext.Current.Session("EMP_CD"))
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Void the warranty line when all the links are removed for voided.
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="delDocNum"></param>
    ''' <param name="lineNum"></param>
    ''' <remarks></remarks>
    Private Shared Sub VoidWarrLine(ByRef cmd As OracleCommand, ByVal delDocNum As String, ByVal lineNum As Integer)
        Dim sql As New StringBuilder
        Dim recsAfftected As Integer
        Try
            Sql.Clear()
            cmd.Parameters.Clear()
            sql.Append(" UPDATE SO_LN SET VOID_FLAG='Y',VOID_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') ,PICKED='V',FILL_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND DEL_DOC_LN#=:DEL_DOC_LNNUM")
            cmd.CommandText = Sql.ToString
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum.Trim
            cmd.Parameters.Add(":DEL_DOC_LNNUM", OracleType.Number)
            cmd.Parameters(":DEL_DOC_LNNUM").Value = lineNum
            recsAfftected = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub
    ''' <summary>
    ''' This function returns the maximum available dated from the selected items in tmep_itm table for the session!!
    ''' </summary>
    ''' <param name="sessionID">Session ID</param>
    ''' <returns></returns>
    Public Shared Function GetMaxAvailableDate(ByVal sessionID As String, ByRef Points As String) As String
        Dim sql As String = String.Empty
        Dim maxDeliveredBy As String = String.Empty
        Using conn As OracleConnection = DisposablesManager.BuildOracleConnection, oraCmd As OracleCommand = DisposablesManager.BuildOracleCommand
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            oraCmd.Connection = conn
            conn.Open()
            If SysPms.isDelvByPoints Then
                sql = "SELECT SUM(DEL_PTS*QTY) As Point_CNT, MAX( AVAIL_DT ) AvailDate FROM TEMP_ITM WHERE SESSION_ID=:SESSION_ID"
            Else
                sql = "SELECT '0' as  Point_CNT,  MAX( AVAIL_DT )  AvailDate FROM TEMP_ITM WHERE SESSION_ID=:SESSION_ID"
            End If
            oraCmd.Parameters.Add(":SESSION_ID", OracleType.VarChar)
            oraCmd.Parameters(":SESSION_ID").Value = sessionID
            oraCmd.CommandText = sql
            Dim OraDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(oraCmd)
            Try
                'Store Values in String Variables 
                If (OraDataReader.Read()) Then
                    Points = OraDataReader.Item("Point_CNT").ToString

                    If OraDataReader.Item("AvailDate").ToString <> String.Empty Then
                        maxDeliveredBy = Date.Parse(OraDataReader.Item("AvailDate").ToString)
                    Else
                        maxDeliveredBy = String.Empty
                    End If
                Else
                    Points = "0"
                    'If no rows found, initilize it tp previous day...
                    maxDeliveredBy = String.Empty
                End If
            Catch ex As Exception
                maxDeliveredBy = String.Empty
                Throw
            Finally
                OraDataReader.Close()
                conn.Close()
            End Try
        End Using
        Return maxDeliveredBy
    End Function


    Public Shared Function getStoreZipCode(storeCode As String, customereCode As String) As String

        Dim storeZipCode As String = String.Empty
        Dim SqlQuery As StringBuilder = New StringBuilder()

        If String.IsNullOrEmpty(storeCode) Then Return storeZipCode

        If String.IsNullOrEmpty(customereCode) Then
            Return storeZipCode
        Else
            SqlQuery.Append("SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & storeCode & "'")
        End If

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            conn.Open()
            Using cmd As New OracleCommand(SqlQuery.ToString, conn)
                Using MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)
                    If MyDataReader.Read() Then
                        storeZipCode = MyDataReader.Item("ZIP_CD")
                    End If
                End Using
            End Using

        End Using
        Return storeZipCode
    End Function

    'Alice added on May 14, 2019 for request 4168 -	Allow max discount item to have its price changed if ORDER's sort code is acceptable/approved order sort codes.

    Public Shared Function isAllowMaxDiscountSortCode(ByVal sort_code As String) As String

        Dim v_value As Integer = 0
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "select count(*) as count from MAX_DISC_SRT_CD c inner join ord_srt_maint m on m.ord_srt_cd=c.ord_srt_cd where m.active='Y' and c.ord_srt_cd='" & sort_code & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("count")
            End If
        Catch
            v_value = 0
        End Try
        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function

End Class
