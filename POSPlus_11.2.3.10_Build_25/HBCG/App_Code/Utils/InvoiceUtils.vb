﻿Imports Microsoft.VisualBasic

Public Class InvoiceUtils

    ''' <summary>
    ''' Returns the query used for retrieving the order info that is needed when printing
    ''' certain kinds of invoices.
    ''' </summary>
    ''' <param name="delDocNum">the order for which to get info for</param>
    ''' <returns>the query string for the order details</returns>
    Public Shared Function GetOrderDataQuery(ByVal delDocNum As String) As String

        Dim sql As String
        sql = "SELECT SO.*, ITM.VSN, ITM.DES, SO_LN.PKG_SOURCE, EMP.FNAME || ' ' || EMP.LNAME AS FULL_NAME, "
        sql = sql & "SO_LN.ITM_CD, SO_LN.VOID_FLAG, SO_LN.STORE_CD, SO_LN.LOC_CD, "
        sql = sql & "SO_LN.UNIT_PRC, SO_LN.QTY, ITM.SIZ, SO_LN.DISC_AMT, SO_LN.DEL_DOC_LN#, SO_LN.QTY*SO_LN.UNIT_PRC As EXT_PRC, "
        sql = sql & "CUST.FNAME || ' ' || CUST.LNAME AS CUST_FULL_NAME, CUST.FNAME, CUST.LNAME, CUST.ADDR1, CUST.ADDR2, CUST.CITY, "
        sql = sql & "CUST.CORP_NAME, CUST.ST_CD, CUST.ZIP_CD, CUST.HOME_PHONE, CUST.BUS_PHONE, "
        sql = sql & "STORE.STORE_NAME, 'Tel. ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 || ', ' || STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS STORE_ADDRESS "
        sql = sql & "FROM SO, SO_LN, ITM, EMP, STORE, STORE_PHONE, CUST "
        sql = sql & "WHERE SO.DEL_DOC_NUM='" & delDocNum & "' "
        sql = sql & "AND SO_LN.ITM_CD=ITM.ITM_CD "
        sql = sql & "AND CUST.CUST_CD=SO.CUST_CD "
        sql = sql & "AND EMP.EMP_CD=SO.SO_EMP_SLSP_CD1  "
        sql = sql & "AND SO.SO_STORE_CD=STORE.STORE_CD "
        sql = sql & "AND SO.SO_STORE_CD=STORE_PHONE.STORE_CD(+) "
        sql = sql & "AND STORE_PHONE.PHONE_TP_CD(+)='PHN' "
        sql = sql & "AND SO_LN.VOID_FLAG(+)='N' "
        sql = sql & "AND SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM(+) "
        sql = sql & "ORDER BY SO.DEL_DOC_NUM, SO_LN.DEL_DOC_LN#"

        Return sql

    End Function

End Class
