﻿Imports System
Imports System.Collections.Generic
'Imports Oracle.DataAccess.Client
Imports System.Data.OracleClient
Imports System.Threading
Imports System.Web

''' <summary>
''' This utility class provides a "safety net" for IDisposable OracleClient object instances.
''' 
''' The factory methods here are to be used in place of the constructors for OracleConnection and 
''' OracleCommand instances, and the builder method for OracleDataReader.
''' 
''' These functions "register" each new instance in a "request-local" list in HttpContext.Current.  
''' For an explanation of why the simpler, more-efficient ThreadStatic cannot be used here, 
''' research the issue of "thread agility in ASP.NET".
''' 
''' The sole exception is when the oracle objects are constructed on a Using statement.  In that 
''' case the compiler guarantees they will be disposed, so we do not need to track them, and 
''' prefer not to... so the overloads with the added boolean (set to false) can be used to 
''' accomplish construction without registration.
''' 
''' When all the work of a page is completed, the Cleanup() routine is to be called, and 
''' any of those instances that were not already disposed, will be; and then the additional 
''' references dropped.  
''' </summary>
''' <remarks></remarks>
Public Class DisposablesManager

    Protected Const NameString As String = "DisposablesList"

    Protected Shared Function GetLocalList() As List(Of IDisposable)
        Dim rv As List(Of IDisposable) = Nothing
        If HttpContext.Current IsNot Nothing Then
            rv = HttpContext.Current.Items(NameString)
            If rv Is Nothing Then
                rv = New List(Of IDisposable)
                HttpContext.Current.Items.Add(NameString, rv)
            End If
        End If
        Return rv
    End Function

    Protected Shared Sub ForgetLocalList()
        If HttpContext.Current IsNot Nothing Then
            If HttpContext.Current.Items.Contains(NameString) Then
                HttpContext.Current.Items.Remove(NameString)
            End If
        End If
    End Sub


    Public Shared Sub Register(suspect As IDisposable)
        Dim Suspects As List(Of IDisposable) = GetLocalList()
        If Suspects IsNot Nothing Then
            If suspect IsNot Nothing Then
                Suspects.Add(suspect)
            End If
        Else
            System.Diagnostics.Debug.WriteLine("HttpContext is null, IDisposable object cannot be registered")
        End If
    End Sub

    Public Shared Sub Cleanup()
        Dim Suspects As List(Of IDisposable) = GetLocalList()
        If Suspects Is Nothing Then Return
        Try
            For Each d As IDisposable In Suspects
                Try
                    ' 'in principle' we should not dispose any instances more than once
                    ' but, in practice we find it is safe to do so
                    ' we just catch and ignore any exceptions that might occur
                    d.Dispose()
                Catch
                End Try
            Next
        Finally
            ' regardless, WE don't want to be the reason some instances are pinned
            Suspects.Clear()
            ForgetLocalList()
        End Try
    End Sub

    ' This is to be used with ODP it is not applicable to System.Data.OracleClient
    'Public Shared Sub ApplyCompatibilitySettings(cmd As OracleCommand)
    '    '' compatibility settings
    '    'cmd.BindByName = True
    '    'cmd.CommandTimeout = 0
    'End Sub

    ' Regular builder overloads - register the new instance with the safety net
    Public Shared Function BuildOracleConnection() As OracleConnection
        Return BuildOracleConnection(True)
    End Function

    Public Shared Function BuildOracleConnection(cs As String) As OracleConnection
        Return BuildOracleConnection(cs, True)
    End Function

    Public Shared Function BuildOracleCommand() As OracleCommand
        Return BuildOracleCommand(True)
    End Function

    Public Shared Function BuildOracleCommand(sql As String) As OracleCommand
        Return BuildOracleCommand(sql, True)
    End Function

    Public Shared Function BuildOracleCommand(conn As OracleConnection) As OracleCommand
        Return BuildOracleCommand(conn, True)
    End Function

    Public Shared Function BuildOracleCommand(sql As String, conn As OracleConnection) As OracleCommand
        Return BuildOracleCommand(sql, conn, True)
    End Function

    Public Shared Function BuildOracleDataReader(cmd As OracleCommand) As OracleDataReader
        Return BuildOracleDataReader(cmd, True)
    End Function

    Public Shared Function BuildOracleDataReader(cmd As OracleCommand, cb As CommandBehavior) As OracleDataReader
        Return BuildOracleDataReader(cmd, cb, True)
    End Function

    Public Shared Function BuildOracleDataAdapter() As OracleDataAdapter
        Return BuildOracleDataAdapter(True)
    End Function

    Public Shared Function BuildOracleDataAdapter(cmd As OracleCommand) As OracleDataAdapter
        Return BuildOracleDataAdapter(cmd, True)
    End Function

    Public Shared Function BuildOracleDataAdapter(sql As String, conn As OracleConnection) As OracleDataAdapter
        Return BuildOracleDataAdapter(sql, conn, True)
    End Function

    Public Shared Function BuildOracleDataAdapter(sql As String, cs As String) As OracleDataAdapter
        Return BuildOracleDataAdapter(sql, cs, True)
    End Function

    ' Alternate builders omit the registration
    ' These MUST NOT be used unless the caller is on a Using statement
    Public Shared Function BuildOracleConnection(_register As Boolean) As OracleConnection
        Dim rv As New OracleConnection
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleConnection(cs As String, _register As Boolean) As OracleConnection
        Dim rv As New OracleConnection(cs)
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleCommand(_register As Boolean) As OracleCommand
        Dim rv As New OracleCommand
        If _register Then Register(rv)
        'ApplyCompatibilitySettings(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleCommand(sql As String, _register As Boolean) As OracleCommand
        Dim rv As New OracleCommand(sql)
        If _register Then Register(rv)
        'ApplyCompatibilitySettings(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleCommand(conn As OracleConnection, _register As Boolean) As OracleCommand
        Dim rv As OracleCommand = conn.CreateCommand
        If _register Then Register(rv)
        'ApplyCompatibilitySettings(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleCommand(sql As String, conn As OracleConnection, _register As Boolean) As OracleCommand
        Dim rv As New OracleCommand(sql, conn)
        If _register Then Register(rv)
        'ApplyCompatibilitySettings(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataReader(cmd As OracleCommand, _register As Boolean) As OracleDataReader
        If cmd.Connection.State = ConnectionState.Closed Then
            cmd.Connection.Open()
        End If
        Dim rv As OracleDataReader = cmd.ExecuteReader
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataReader(cmd As OracleCommand, cb As CommandBehavior, _register As Boolean) As OracleDataReader
        If cmd.Connection.State = ConnectionState.Closed Then
            cmd.Connection.Open()
        End If
        Dim rv As OracleDataReader = cmd.ExecuteReader(cb)
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataAdapter(_register As Boolean) As OracleDataAdapter
        Dim rv As New OracleDataAdapter()
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataAdapter(cmd As OracleCommand, _register As Boolean) As OracleDataAdapter
        Dim rv As New OracleDataAdapter(cmd)
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataAdapter(sql As String, conn As OracleConnection, _register As Boolean) As OracleDataAdapter
        Dim rv As New OracleDataAdapter(sql, conn)
        If _register Then Register(rv)
        Return rv
    End Function

    Public Shared Function BuildOracleDataAdapter(sql As String, cs As String, _register As Boolean) As OracleDataAdapter
        Dim rv As New OracleDataAdapter(sql, cs)
        If _register Then Register(rv)
        Return rv
    End Function

End Class
