﻿Option Explicit On
'Option Strict On

Imports System.Web.SessionState
Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

''' <summary>
'''    Encapsulates the details of managing session variables.  
''' </summary>
Public NotInheritable Class SessVar

    Private theSystemBiz As SystemBiz = New SystemBiz()
    ' TODO - we probably still want to group these into objects but objects or individual, they should all be organized here

    ' Necessary here as public to allow reference to remove by name; left original below for consistent usage
    ' GOing forward, maybe a class of public session variable names ???
    Public Const discReCalcVarNm As String = SessionVarCls.discountReCalc   'object used to indicate when to re-calc the discount on adding a line
    Public Const existHdrDiscCdVarNm As String = SessionVarCls.existHdrDiscountCd   'disc code of existing header disc (not the syspm) of a queried order in SOM


    Friend Class SessionVarCls

        ''' <summary>
        '''    Constants for the session variables
        ''' </summary>
        ' THESE ARE IN ALPHA ORDER BY THE LITERAL FOR EASE OF REFERENCE WHEN ADDING NEW; AT SOME POINT ORG'D BY FUNCTION/ALPHA ON CONSTANT NAME WOULD BE BETTER
        Friend Const cashAndCarry As String = "cash_carry"       'TRUE indicates is a cash and carry sale, is not otherwise
        Friend Const custCode As String = "CUST_CD"
        Friend Const custTpPrcCode As String = "cust_tp_prc_cd"  'special pricing code for this customer used in pricing process
        Friend Const custZipCode As String = "CUST_ZIP"         ' customer postal code 
        Friend Const discountCd As String = "DISC_CD"           'Either a Header discCd OR the Syspm MultiDiscCd when Syspm MultiDisc is Enabled
        Friend Const discountReCalc As String = "DISC_RE_CALC"   'object used to indicate when to re-calc the discount on adding a line
        Friend Const existHdrDiscountCd As String = "EXISTING_HDR_DISC"   'disc code of existing header disc (not the syspm) of a queried order in SOM

        Friend Const homeStoreCode As String = "home_store_cd"  ' the store code for the employee on the emp record
        Friend Const skuList2Add As String = "itemid"           'item code or codes comma delimited list usually input to insert_records method
        Friend Const lastLineSeqNum As String = "LAST_LNSEQNUM"  'the last (max) line sequence number existing before add a line used for discount re-calculation
        Friend Const ordTpCode As String = "ord_tp_cd"          ' order type code like SAL, CRM, MDB, MCR, RES, etc.
        Friend Const soDbRow As String = "ORIG_SO"              ' SO record for queried doc in SOM as queried from the db
        Friend Const puDelFlag As String = "PD"                 ' pickup/delivery flag P or D
        Friend Const puDelStoreCode As String = "pd_store_cd"   'pickup and/or delivery store code
        Friend Const sessionId As String = "Sess_ID"
        Friend Const orderLines As String = "DEL_DOC_LN#"        ' the dataset of order lines, most likely SO_LN or SO_LN to be
        Friend Const wrStoreCode As String = "store_cd"         ' maybe this should have been aStoreCd; it is written store code in a sale
        Friend Const wrStoreArsEnabled As String = "STORE_ENABLE_ARS"   ' advanced reservation logic which is based on written store

        Friend Const anyZipCode As String = "ZIP_CD"            ' postal code that is set from whatever and used wherever ???
        Friend Const zoneCode As String = "ZONE_CD"             'zone code used for pickup and/or delivery scheduling and also inventory availability
        Friend Const zoneZipCode As String = "ZONE_ZIP_CD"      ' postal code to be used for zone acquisition

        Friend Const zoneDes As String = "zone_des"                 ' Session variable representing the zone code descrption

        'last date Sales History Update (SHU) ran
        Friend Const shuDate As String = SysPmsConstants.SHU_DATE  '  "LST_SLS_UPDATE_DT"

        Private Sub New()
        End Sub

        ''' <summary>
        '''    Returns the current Session object, if any.
        ''' </summary>
        Friend Shared ReadOnly Property Session As HttpSessionState
            Get
                Return HttpContext.Current.Session
            End Get
        End Property

        ''' <summary>
        '''    Returns True if a valid Session object exists.
        ''' </summary>
        Public Shared ReadOnly Property Exists() As Boolean
            Get
                Return Not Session Is Nothing
            End Get
        End Property

    End Class

    ''' <summary>
    ''' Fetches the store details, for the store code passed in.
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieved details for</param>
    ''' <returns>the store details or an empty object if no matches are found</returns>
    Public Shared Sub Remove(ByVal sessionVarName As String)

        If sessionVarName.isNotEmpty Then

            HttpContext.Current.Session.Remove(sessionVarName)
        End If

    End Sub

    ''' <summary>
    ''' Session variable representing the existing header discount code on a document queried in SOM; not the multiple syspm disc code but the real discount code if found; also
    '''   not a discount code added in SOM but one on the document as queried from the database; needed for SHU processing determination
    ''' </summary>
    Public Shared Property existHdrDiscCd As String
        Get
            Return GetString(SessionVarCls.existHdrDiscountCd)
        End Get
        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.existHdrDiscountCd, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing the order lines - was DEL_DOC_LN#.
    ''' </summary>
    Public Shared Property ordLns As DataSet
        Get
            If IsNothing(HttpContext.Current.Session(SessionVarCls.orderLines)) Then

                Return Nothing

            Else
                Return SessionVarCls.Session(SessionVarCls.orderLines)
            End If
        End Get
        Set(ByVal value As DataSet)
            SetOfType(Of DataSet)(SessionVarCls.orderLines, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing the SHU date for easy reference; must be re-acquired for every reference
    ''' </summary>
    Public Shared Property shuDt As Date
        Get
            Dim tmp As String = GetShuDt()
            If IsDBNull(tmp) Then

                tmp = SysPms.begOfTime 'Return FormatDateTime(SysPms.begOfTime, DateFormat.ShortDate)
            End If
            Return FormatDateTime(tmp, DateFormat.ShortDate)
        End Get
        ' CANNOT BE SET EXCEPT BY Syspm SHU extraction
        Set(ByVal value As Date)
            'SetOfType(Of String)(SessionVarCls.shuDate, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing whether the current order is a cash and carry transaction or not.
    ''' </summary>
    Public Shared Property cashNCarry As String
        Get
            Return GetTrueFalseString(SessionVarCls.cashAndCarry)
        End Get
        ' probably should validate is TRUE or FALSE
        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.cashAndCarry, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing comma delimited item code or codes list to add to order
    ''' </summary>
    Public Shared Property skuListToAdd As String
        Get
            Return GetString(SessionVarCls.skuList2Add)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.skuList2Add, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing whether the customer has special pricing or not
    ''' </summary>
    Public Shared Property custTpPrcCd As String
        Get
            Return GetString(SessionVarCls.custTpPrcCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.custTpPrcCode, value)
        End Set
    End Property

    ''' <summary>
    ''' Session variable representing whether the current order contains a discounts.
    ''' This value represents a HEADER level discount or if SysPm.allowMultDisc is
    ''' enabled and discounts exist, then it represents the SysPm.multiDiscCd value
    ''' </summary>
    Public Shared Property discCd As String
        Get
            Return GetString(SessionVarCls.discountCd)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.discountCd, value)
        End Set
    End Property

    ''' <summary>
    ''' A class variable used within a session to process discount re-calculation requirements for add-on lines; 
    ''' used to avoid re-calculating for each add-on line with possibly adding multiples
    ''' </summary>
    Public Class DiscountReCalcObj

        Property needToReCalc As Boolean   ' true when need to re-calc discounts; false otherwise
        Property maybeWarr As Boolean      ' expecting to go to warr page so maybe a warr will also be added
        Property maybeRelated As Boolean   ' expecting to go to related Sku page so maybe a related sku will also be added
    End Class

    ''' <summary>
    ''' Session variable representing whether the current order contains a discounts.
    ''' This value represents a HEADER level discount or if SysPm.allowMultDisc is
    ''' enabled and discounts exist, then it represents the SysPm.multiDiscCd value
    ''' </summary>
    Public Shared Property discReCalc As Object
        Get
            If IsNothing(HttpContext.Current.Session(SessionVarCls.discountReCalc)) Then
                'create and initialize

                Dim tmpDrc As DiscountReCalcObj = New DiscountReCalcObj
                tmpDrc.needToReCalc = False
                tmpDrc.maybeWarr = False
                tmpDrc.maybeRelated = False
                HttpContext.Current.Session(SessionVarCls.discountReCalc) = tmpDrc
                Return tmpDrc

            Else
                Return SessionVarCls.Session(SessionVarCls.discountReCalc)
            End If
        End Get

        Set(ByVal value As Object)
            SessionVarCls.Session(SessionVarCls.discountReCalc) = value
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing whether the current written store is ARS enabled or not; true if enabled, false otherwise;
    ''' Note that during creation, the written store could be the employee's home store code OR the cashier activated store code
    '''    
    ''' </summary>
    Public Shared Property isArsEnabled As Boolean
        Get
            ' need to use the basic here or will try to call itself and get into an endless loop
            If IsNothing(HttpContext.Current.Session(SessionVarCls.wrStoreArsEnabled)) Then

                If IsNothing(SessVar.wrStoreCd) OrElse SessVar.wrStoreCd.isEmpty Then

                    SessVar.isArsEnabled = GetofType(Of Boolean)(SessionVarCls.wrStoreArsEnabled)

                Else
                    SessVar.isArsEnabled = getEnableArs(SessVar.wrStoreCd)
                    'Dim storeObj As StoreData = StoreDac.GetStoreInfo(SessVar.wrStoreCd)
                    'SessVar.isArsEnabled = storeObj.enableARS
                End If
            End If
            ' need to use the basic here or will try to call itself and get into an endless loop
            Return HttpContext.Current.Session(SessionVarCls.wrStoreArsEnabled) 'SessVar.isArsEnabled
        End Get
        Set(ByVal value As Boolean)
            SetOfType(Of Boolean)(SessionVarCls.wrStoreArsEnabled, value)
        End Set
    End Property

    ''' <summary>
    '''    Session Identifier
    ''' </summary>
    Public Shared ReadOnly Property sessId As String
        Get
            Return SessionVarCls.Session.SessionID.ToString.Trim
        End Get
        ' NO SET for session ID
    End Property

    ''' <summary>
    '''    Session variable representing the pickup/delivery store code 
    ''' </summary>
    Public Shared Property puDelStoreCd As String
        Get
            Return GetString(SessionVarCls.puDelStoreCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.puDelStoreCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the home store code of the employee who logged in
    ''' </summary>
    Public Shared Property homeStoreCd As String
        Get
            Return GetString(SessionVarCls.homeStoreCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.homeStoreCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing a postal code 
    ''' </summary>
    Public Shared Property aZipCd As String
        Get
            Return GetString(SessionVarCls.anyZipCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.anyZipCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing a customer ship-to postal code 
    ''' </summary>
    Public Shared Property custZipCd As String
        Get
            Return GetString(SessionVarCls.custZipCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.custZipCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the zip code used to determine the zone code
    ''' </summary>
    Public Shared Property zipCode4Zone As String
        Get
            Return GetString(SessionVarCls.zoneZipCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.zoneZipCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the customer code
    ''' </summary>
    Public Shared Property custCd As String
        Get
            Return GetString(SessionVarCls.custCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.custCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the zone code
    ''' </summary>
    Public Shared Property zoneCd As String
        Get
            Return GetString(SessionVarCls.zoneCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.zoneCode, value)
        End Set
    End Property
    ''' <summary>
    ''' Session variable representing the zone code descrption
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property ZoneDes As String
        Get
            Return GetString(SessionVarCls.zoneDes)
        End Get
        Set(value As String)
            SetOfType(Of String)(SessionVarCls.zoneDes, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the pickup or delivery flag
    ''' </summary>
    Public Shared Property puDel As String
        Get
            Return GetString(SessionVarCls.puDelFlag)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.puDelFlag, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the written store code
    ''' </summary>
    Public Shared Property wrStoreCd As String
        Get
            Return GetString(SessionVarCls.wrStoreCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.wrStoreCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the order type code
    ''' </summary>
    Public Shared Property ordTpCd As String
        Get
            Return GetString(SessionVarCls.ordTpCode)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.ordTpCode, value)
        End Set
    End Property

    ''' <summary>
    '''    Session variable representing the max (last) line sequence number
    ''' </summary>
    Public Shared Property LastLnSeqNum As String
        Get
            Return GetString(SessionVarCls.lastLineSeqNum)
        End Get

        Set(ByVal value As String)
            SetOfType(Of String)(SessionVarCls.lastLineSeqNum, value)
        End Set
    End Property

    ''' <summary>
    '''    Sets the session variable specified by the key with the value input; generic for all types of session variables
    ''' </summary>
    Private Shared Sub SetOfType(Of T)(ByVal key As String, ByVal value As T)

        SessionVarCls.Session(key) = value
    End Sub

    ''' <summary>
    '''    Returns the session variable value for the specified key; generic for all types of session variables; if the session variable does not exist, returns the default for the type
    ''' </summary>
    Private Shared Function GetofType(Of T)(ByVal key As String) As T

        Dim rtnVal As T
        If IsNothing(SessionVarCls.Session(key)) Then

            Return rtnVal  ' will return whatever the default is for the type

        Else
            rtnVal = SessionVarCls.Session(key)
            Return rtnVal
        End If
    End Function

    ''' <summary>
    '''    Returns the session variable value for the specified key; use for string session variables where the default return value if the session variable does not exist is an empty string
    ''' </summary>
    Private Shared Function GetString(ByVal key As String) As String

        If IsNothing(SessionVarCls.Session(key)) OrElse IsDBNull(SessionVarCls.Session(key)) Then

            Return String.Empty   ' return an empty string instead of nothing

        Else
            Return SessionVarCls.Session(key)
        End If
    End Function

    ''' <summary>
    '''    Returns the session variable value for the specified key; use for string session variables where the default return value if the session variable does not exist is a FALSE string
    ''' </summary>
    Private Shared Function GetTrueFalseString(ByVal key As String) As String

        If IsNothing(SessionVarCls.Session(key)) Then

            Return "FALSE"   ' return a FALSE string instead of nothing

        Else
            Return SessionVarCls.Session(key)
        End If
    End Function

    ''' <summary>
    ''' Fetches the store details, for the store code passed in.
    ''' </summary>
    ''' <param name="storeCd">the store code to retrieved details for</param>
    ''' <returns>the store details or an empty object if no matches are found</returns>
    Private Shared Function getEnableArs(ByVal storeCd As String) As Boolean

        Dim enableARS As Boolean = False
        Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim dbRdr As OracleDataReader
        Dim sql As String = " SELECT enable_ars FROM store WHERE store_cd = :storeCd "

        Try
            dbConn.Open()
            dbCmd.CommandText = sql
            dbCmd.Parameters.Add(":storeCd", OracleType.VarChar)
            dbCmd.Parameters(":storeCd").Value = UCase(storeCd)
            dbRdr = DisposablesManager.BuildOracleDataReader(dbCmd)


            If dbRdr.Read() Then

                enableARS = StringUtils.ResolveYNToBoolean(dbRdr.Item("ENABLE_ARS").ToString())
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dbRdr.Close()
            dbCmd.Dispose()
            dbConn.Dispose()
        End Try

        Return enableARS
    End Function

    ''' <summary>
    ''' 'Returns the value of the system parameter from E1 using SYSPM_REF - is the most
    ''' efficient way to call for system parameter info if the param has a name.
    ''' </summary>
    ''' <param name="paramName">system parameter name</param>
    ''' <returns>the value of the system parameter from E1</returns>
    ''' <remarks>COPIED FROM SystemDac.GetSysPm</remarks>
    Private Shared Function GetShuDt() As String

        Dim sysPmValue As String = String.Empty
        Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim sql As String = "bt_syspm.param_value"

        dbCmd.CommandText = sql
        dbCmd.CommandType = CommandType.StoredProcedure

        dbCmd.Parameters.Add("returnValue", OracleType.VarChar, 50).Direction = ParameterDirection.ReturnValue
        dbCmd.Parameters.Add("param_name_i", OracleType.VarChar).Direction = ParameterDirection.Input

        dbCmd.Parameters("param_name_i").Value = SysPmsConstants.SHU_DATE

        Try
            dbConn.Open()
            dbCmd.ExecuteNonQuery()
            sysPmValue = dbCmd.Parameters("returnValue").Value.ToString

        Catch ex As Exception
            Throw
        Finally
            dbCmd.Dispose()
            dbConn.Dispose()
        End Try

        Return sysPmValue
    End Function

End Class
