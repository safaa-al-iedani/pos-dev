Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Data
Imports System
Imports ErrorManager
Imports System.Xml
Imports System.Web
Imports System.IO
Imports AppExtensions
Imports System.Linq

Public Class HBCG_Utils
    Inherits POSBasePage
    ' Note: this class retained for TEMP_ITM processing and other CRM/POS table logic; E1 stuff sb moved to a more appropriate class

    ''' <summary>
    ''' Create temp_itm record request object
    ''' </summary>
    Public Class CreateTempItmReq
        'TODO - maybe should have just used the TEMP_ITM object below but I forgot about it - another time

        Property itmCd As String = ""    ' when creating a warranty line, this is the warranty itm_cd, vice versa for warrantable
        Property sessId As String = ""
        Property empCd As String = ""    ' emp code of operator, maybe slsp
        Property tranDt As String = ""    ' used to extract price data from calendar and drop date for SKU
        Property defLocCd As String = ""   ' default location code for store if take-with
        Property newSpecOrd As Boolean = False
        Property takeWith As Boolean = False
        Property warrRowLink As Integer = 0
        Property warrItmLink As String = "" ' when creating a warranty line, this is the warrantable itm_cd, vice versa for warrantable
        Property soHdrInf As New SoHeaderDtc()

    End Class

    ''' <summary>
    ''' Create temp_itm record response object
    ''' </summary>
    Public Class CreateTempItmResp

        Property numRows As Integer         ' number of rows inserted
        Property errMsg As String           ' error message from process
        Property selectedSku As String      ' the ITM_CD of the selected SKU (an alternate itm_cd may have been the input)
        Property User_Questions As String
        Property dropped_dialog As String     'messages regarding dropped SKUs and dates
        Property PKG_SKUS As String             'list of package SKUs
        Property CUSTOM_SKUS As String          ' list of frame type SKUS
        Property mnrCd As String                ' minor code for SKU
        Property catCd As String                ' category code for SKU
        Property vsn As String
        Property isInventory As Boolean
        Property isWarrantable As Boolean
        Property isTreatable As Boolean
        Property retPrc As Double            ' item retail price - this is the retail price on the ITEM table, not the calendar pricing
        Property insertedSkuRowId As String   ' the temp_itm unique sequence of the line called row_id which is not the Oracle rowid; used for creating link
        Property rowIdSequenceNumber As Integer
    End Class

    ''' <summary>
    ''' Creates an order line record in temp storage (TEMP_ITM table) for the request information
    ''' </summary>
    ''' <param name="reqCreate">object with input request information used to create the temp_itm record</param>
    ''' <returns>response object</returns>
    ''' <remarks>If you need the calendar price, then you must use this method and provide the data for the calendar pricing  </remarks>
    Public Shared Function CreateTempItm(ByVal reqCreate As CreateTempItmReq) As CreateTempItmResp

        Dim resp As CreateTempItmResp

        If SystemUtils.isNotEmpty(reqCreate.itmCd) AndAlso SystemUtils.isNotEmpty(reqCreate.sessId) Then

            ' TODO - when installing, watch for session variable updates (in orderDetail.insert_records) because currently not going out ???
            Dim sesId As String = reqCreate.sessId.Trim
            Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand
            Dim itmTbl As DataTable
            resp = New CreateTempItmResp
            Dim loop1 As Integer

            ' TODO - handle multiple SKUs in future ????
            'dim itemcd as string = Session("itemid")
            'itemcd = "'" + itemcd.Replace(",", "','") + "'"
            'itemcd = "(" + itemcd + ")"

            Dim reqItm As New SalesUtils.GetCalRetPrcReq
            reqItm.itmCd = reqCreate.itmCd
            reqItm.custTpPrcCd = reqCreate.soHdrInf.custTpPrcCd
            reqItm.effDt = reqCreate.tranDt
            reqItm.storeCd = reqCreate.soHdrInf.writtenStoreCd
            ' TODO maybe - E1 probably does this in the reverse sequence, alt first, then regular
            itmTbl = SkuUtils.GetItemInfo(reqItm)
            resp.numRows = itmTbl.Rows.Count
            resp.errMsg = ""

            'if the ITEM_CD was not found, then try the ALT_ITM_CD
            If resp.numRows = 0 Then
                itmTbl = SkuUtils.GetItemInfo(reqItm, True)
                resp.numRows = itmTbl.Rows.Count
            End If

            If resp.numRows > 0 Then

                resp.selectedSku = ""
                resp.PKG_SKUS = ""
                resp.CUSTOM_SKUS = ""
                resp.mnrCd = ""
                resp.catCd = ""
                resp.retPrc = 0
                resp.dropped_dialog = ""
                resp.User_Questions = ""

                Dim sku As String = ""
                Dim VeCd As String = ""
                Dim point_size As String = "0"
                Dim taxAmt As Double
                Dim cost As Double
                Dim slspPct1 As Double
                Dim slspPct2 As Double
                Dim comm_cd As String = ""
                Dim slspCd1 As String = ""
                Dim spiff As String = ""
                Dim des As String = ""
                Dim siz As String = ""
                Dim finish As String = ""
                Dim cover As String = ""
                Dim grade As String = ""
                Dim stylecd As String = ""
                Dim itm_tp_cd As String = ""
                Dim dropped_dt As String = ""
                Dim tax_cd As String = ""
                Dim tax_pct As String = ""
                Dim taxable_amt As String
                Dim store_cd As String = ""
                Dim loc_cd As String = ""
                Dim trans As OracleTransaction
                Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim sql As StringBuilder
                Dim taxPcts As TaxUtils.taxPcts

                For loop1 = 0 To resp.numRows - 1

                    sku = itmTbl.Rows(loop1).Item("ITM_CD").ToString ' this may or may not be the same itm code as input depending on alternate itm usage so cannot use input
                    resp.selectedSku = sku

                    'Do not allow SKUs that have a prevent sale flag on them
                    If (reqCreate.soHdrInf.orderTpCd = AppConstants.Order.TYPE_SAL AndAlso
                            isSkuForSale(itmTbl.Rows(loop1).Item("prevent_sale").ToString, itmTbl.Rows(loop1).Item("DROP_DT").ToString)) OrElse
                        reqCreate.soHdrInf.orderTpCd <> AppConstants.Order.TYPE_SAL Then

                        resp.isInventory = IIf("Y".Equals(itmTbl.Rows(loop1).Item("INVENTORY").ToString), True, False)
                        'for inventoriable ITEMs if order is Take-with defaults the store/location
                        store_cd = reqCreate.soHdrInf.writtenStoreCd
                        If resp.isInventory AndAlso reqCreate.soHdrInf.isCashNCarry Then
                            loc_cd = reqCreate.defLocCd
                        End If
                        If isEmpty(loc_cd) Then store_cd = ""
                        If IsDate(itmTbl.Rows(loop1).Item("DROP_DT").ToString) Then
                            dropped_dt = FormatDateTime(itmTbl.Rows(loop1).Item("DROP_DT").ToString, DateFormat.ShortDate)
                        Else
                            dropped_dt = ""
                        End If
                        If dropped_dt & "" <> "" Then
                            If FormatDateTime(dropped_dt, DateFormat.ShortDate) < Now() Then
                                resp.dropped_dialog = resp.dropped_dialog & "SKU " & sku & "(" & itmTbl.Rows(loop1).Item("VSN").ToString & ") was dropped on " & dropped_dt & vbCrLf
                            End If
                        End If
                        resp.mnrCd = itmTbl.Rows(loop1).Item("MNR_CD").ToString
                        If IsDBNull(itmTbl.Rows(loop1).Item("CAT_CD")) = False Then
                            resp.catCd = itmTbl.Rows(loop1).Item("CAT_CD").ToString
                        Else
                            resp.catCd = ""
                        End If
                        If IsDBNull(itmTbl.Rows(loop1).Item("COMM_CD")) = False Then
                            comm_cd = itmTbl.Rows(loop1).Item("COMM_CD").ToString
                        Else
                            comm_cd = ""
                        End If
                        If itmTbl.Rows(loop1).Item("REPL_CST").ToString & "" <> "" Then
                            cost = CDbl(itmTbl.Rows(loop1).Item("REPL_CST").ToString)
                        Else
                            cost = 0
                        End If
                        resp.vsn = itmTbl.Rows(loop1).Item("VSN").ToString
                        If itmTbl.Rows(loop1).Item("DES").ToString & "" <> "" Then
                            des = itmTbl.Rows(loop1).Item("DES").ToString
                        End If
                        If itmTbl.Rows(loop1).Item("POINT_SIZE").ToString & "" <> "" Then
                            point_size = itmTbl.Rows(loop1).Item("POINT_SIZE").ToString
                        End If
                        If point_size & "" = "" AndAlso resp.isInventory Then point_size = "0"
                        If itmTbl.Rows(loop1).Item("SPIFF").ToString & "" <> "" AndAlso
                            IsNumeric(itmTbl.Rows(loop1).Item("SPIFF").ToString) Then
                            spiff = itmTbl.Rows(loop1).Item("SPIFF").ToString
                        Else
                            spiff = 0
                        End If
                        itm_tp_cd = itmTbl.Rows(loop1).Item("ITM_TP_CD").ToString
                        resp.isTreatable = IIf("Y".Equals(itmTbl.Rows(loop1).Item("TREATABLE").ToString + ""), True, False)
                        resp.isWarrantable = IIf("Y".Equals(itmTbl.Rows(loop1).Item("warrantable").ToString + ""), True, False)
                        resp.retPrc = CDbl(itmTbl.Rows(loop1).Item("calRetPrc").ToString) ' NEED to calculate this before taxes and packages

                        If String.IsNullOrEmpty(reqCreate.soHdrInf.taxExemptCd) Then

                            tax_cd = reqCreate.soHdrInf.taxCd
                            tax_pct = TaxUtils.getTaxRate(itm_tp_cd, reqCreate.soHdrInf.taxRates)
                            taxable_amt = resp.retPrc
                        Else
                            tax_cd = 0
                            ' TODO - shouldn't this be empty string, and taxable amt sb amt even if not taxable - we should copy E1
                            tax_pct = 0
                            taxable_amt = 0
                        End If
                        If reqCreate.soHdrInf.slsp1 & "" = "" Then
                            slspCd1 = reqCreate.empCd
                            slspPct1 = "100"
                            slspPct2 = "0"
                        Else
                            slspCd1 = reqCreate.soHdrInf.slsp1
                        End If

                        Dim retPrc As Double = resp.retPrc
                        If itm_tp_cd = SkuUtils.ITM_TP_PKG AndAlso ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then retPrc = 0
                        If Not IsNumeric(tax_pct) Then tax_pct = 0
                        If Not IsNumeric(retPrc) Then retPrc = 0
                        If Not IsNumeric(point_size) Then point_size = 0
                        If Not IsNumeric(cost) Then cost = 0
                        If Not IsNumeric(tax_pct) Then tax_pct = 0
                        If Not IsNumeric(taxable_amt) Then taxable_amt = 0
                        If Not IsNumeric(spiff) Then spiff = 0
                        If IsNumeric(reqCreate.soHdrInf.slspPct1) Then
                            slspPct1 = reqCreate.soHdrInf.slspPct1
                        Else
                            slspPct1 = "100"
                        End If
                        If IsNumeric(reqCreate.soHdrInf.slspPct2) Then
                            slspPct2 = reqCreate.soHdrInf.slspPct2
                        Else
                            slspPct2 = "0"
                        End If
                        If slspPct1 + slspPct2 <> 100.0 Then
                            slspPct2 = 100.0 - slspPct1
                        End If
                        Dim tw As String = "N"
                        If reqCreate.takeWith Then
                            tw = "Y"
                        End If
                        Dim special As String = "N"
                        If reqCreate.newSpecOrd Then
                            special = "Y"
                        End If
                        taxAmt = taxable_amt * (tax_pct / 100)

                        ' save the record for the entered/selected SKU
                        sql = New StringBuilder
                        sql.Append("INSERT INTO temp_itm (session_id, ITM_CD, VE_CD, MNR_CD, CAT_CD, RET_PRC, ORIG_PRC, MANUAL_PRC, VSN, DES, SIZ, FINISH, COVER, GRADE, ").Append(
                                    "STYLE_CD, ITM_TP_CD, TREATABLE, DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF, NEW_SPECIAL_ORDER, ").Append(
                                    "SLSP1, SLSP1_PCT, TAX_AMT, WARR_ROW_LINK, WARR_ITM_LINK, inventory, ")
                        If reqCreate.soHdrInf.slsp2 & "" <> "" Then
                            sql.Append("SLSP2, SLSP2_PCT, ")
                        End If
                        sql.Append("BULK_TP_ITM, SERIAL_TP, STORE_CD, LOC_CD, WARRANTABLE) ").Append(
                            "VALUES ('").Append(
                                    sesId & "','" & sku & "','" & itmTbl.Rows(loop1).Item("VE_CD").ToString & "','" & resp.mnrCd & "','" & resp.catCd & "'," & retPrc & " , :origPrc, :modifiedPrc, '").Append(
                                    Replace(resp.vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''")).Append(
                                    "','" & Replace(cover, "'", "''") & "','" & Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & IIf(resp.isTreatable, "Y", "N")).Append(
                                    "'," & point_size & "," & cost & ",'" & comm_cd & "','" & tax_cd & "'," & tax_pct & "," & taxable_amt & "," & spiff & ",'").Append(
                                    special & "'" & ",'" & slspCd1 & "'," & slspPct1 & ",ROUND(" & taxAmt & "," & TaxUtils.Tax_Constants.taxPrecision & ")").Append(
                                    ", :warrRowNum, :warrItmCd, :inv ")
                        If reqCreate.soHdrInf.slsp2 & "" <> "" Then
                            sql.Append(",'" & reqCreate.soHdrInf.slsp2 & "'," & slspPct2)
                        End If
                        sql.Append(", '" & itmTbl.Rows(loop1).Item("BULK_TP_ITM").ToString & "', '" & itmTbl.Rows(loop1).Item("SERIAL_TP").ToString & "'").Append(
                                ", '" & store_cd & "','" & loc_cd & "', :warra ) RETURNING rowid INTO :row_id ")

                        Try
                            conn.Open()
                            cmdInsertItems.Parameters.Clear()
                            trans = conn.BeginTransaction
                            With cmdInsertItems
                                .Transaction = trans
                                .Connection = conn
                                .CommandText = sql.ToString
                                .Parameters.Add(":warra", OracleType.VarChar).Direction = ParameterDirection.Input
                                .Parameters(":warra").Value = IIf(resp.isWarrantable, "Y", "N")
                                .Parameters.Add(":warrRowNum", OracleType.Number)
                                .Parameters(":warrRowNum").Value = IIf(reqCreate.warrRowLink = 0, System.DBNull.Value, reqCreate.warrRowLink)
                                .Parameters.Add(":warrItmCd", OracleType.VarChar)
                                .Parameters(":warrItmCd").Value = reqCreate.warrItmLink
                                .Parameters.Add(":inv", OracleType.VarChar)
                                .Parameters(":inv").Value = itmTbl.Rows(loop1).Item("INVENTORY").ToString
                                .Parameters.Add(":origPrc", OracleType.Number)
                                .Parameters(":origPrc").Value = retPrc
                                .Parameters.Add(":origPrc", OracleType.Number)
                                .Parameters(":origPrc").Value = retPrc
                                .Parameters.Add(":modifiedPrc", OracleType.Number)
                                .Parameters(":modifiedPrc").Value = retPrc
                                .Parameters.Add(":row_id", OracleType.VarChar, 18).Direction = ParameterDirection.Output
                                .Parameters(":row_id").Value = System.DBNull.Value
                            End With
                            cmdInsertItems.ExecuteNonQuery()
                            resp.insertedSkuRowId = cmdInsertItems.Parameters(":row_id").Value.ToString
                            trans.Commit()

                        Catch ex As Exception
                            trans.Dispose()
                            conn.Close()
                            Throw ex
                        End Try

                        ' setup remaining output info for return processing
                        If itmTbl.Rows(loop1).Item("USER_QUESTIONS").ToString & "" <> "" Then
                            resp.User_Questions = resp.User_Questions & itmTbl.Rows(loop1).Item("USER_QUESTIONS").ToString & vbCrLf
                        End If
                        If itmTbl.Rows(loop1).Item("ITM_TP_CD").ToString = "PKG" Then
                            resp.PKG_SKUS = resp.PKG_SKUS & "'" & sku & "',"
                        End If
                        If itmTbl.Rows(loop1).Item("FRAME_TP_ITM").ToString = "Y" Then
                            resp.CUSTOM_SKUS = resp.CUSTOM_SKUS & sku & ","
                        End If
                    End If
                Next

            Else
                resp.errMsg = "An invalid SKU " + reqCreate.itmCd + " has been entered."
            End If
        End If
        Return resp
    End Function

    ''' <summary>
    ''' TO get states.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStates() As DataSet
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As New DataSet
        Dim cmdGetStates As OracleCommand
        cmdGetStates = DisposablesManager.BuildOracleCommand
        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT st_cd, des, st_cd || ' - ' || des as full_desc FROM ST order by st_cd"

        Try
            With cmdGetStates
                .Connection = conn
                .CommandText = SQL
            End With
            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStates)
            oAdp.Fill(ds)
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        Return ds
    End Function

    ''' <summary>
    ''' An object representing a row from the TEMP_WARR table which is used to hold warranty information already
    ''' selected on an order for passing to the warranty page
    ''' </summary>
    Public Class TempWarrDtc

        Property sessId As String
        Property lnNum As Integer
        Property itmCd As String
        Property retPrc As Double
        Property vsn As String
        Property days As Integer
    End Class

    ''' <summary>
    ''' Inserts a record into the temp_warr table based on the custom object passed-in.
    ''' </summary>
    ''' <param name="req">the custom object with the relevant data to be saved</param>
    Public Shared Sub CreateTempWarr(ByVal req As TempWarrDtc)

        If IsNothing(req) OrElse isEmpty(req.sessId) OrElse isEmpty(req.lnNum) OrElse isEmpty(req.itmCd) OrElse isEmpty(req.retPrc) Then
            ' DO NOTHING - NO INPUT - TODO - maybe should error here

        Else
            Dim sql As New StringBuilder
            sql.Append("INSERT INTO temp_warr (session_id, ln_num, itm_cd, ret_prc, vsn, days) ").Append(
                        "VALUES( :sessId, :lnNum, :itmCd, :retPrc, :vsn, :days ) ")

            Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            Try

                Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(UCase(sql.ToString), conn)
                conn.Open()
                Dim trans As OracleTransaction = conn.BeginTransaction
                cmd.Transaction = trans
                cmd.Parameters.Clear()

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                cmd.Parameters(":sessId").Value = req.sessId
                cmd.Parameters.Add(":lnNum", OracleType.Number)
                cmd.Parameters(":lnNum").Value = req.lnNum
                cmd.Parameters.Add(":itmCd", OracleType.VarChar)
                cmd.Parameters(":itmCd").Value = req.itmCd
                cmd.Parameters.Add(":retPrc", OracleType.Number)
                cmd.Parameters(":retPrc").Value = req.retPrc
                cmd.Parameters.Add(":vsn", OracleType.VarChar)
                cmd.Parameters(":vsn").Value = IIf(req.vsn.isEmpty, "", req.vsn)
                cmd.Parameters.Add(":days", OracleType.Number)
                If IsNothing(req.days) OrElse Not IsNumeric(req.days) Then
                    cmd.Parameters(":days").Value = System.DBNull.Value
                Else
                    cmd.Parameters(":days").Value = req.days
                End If

                cmd.ExecuteNonQuery()
                trans.Commit()
            Catch ex As Exception
                conn.Close()

            End Try
        End If

    End Sub

    Public Shared Sub Update_Theme()

        Dim executingPage As Page = CType(HttpContext.Current.Handler, Page)
        If Not executingPage Is Nothing Then
            executingPage.Theme = ConfigurationManager.AppSettings("app_theme").ToString
        End If

    End Sub

    Public Shared Sub ActivateCashier(ByVal empCd As String)
        ' input should be cashier
        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = "SELECT ca.CSHR_INIT, ca.CO_CD, ca.STORE_CD, ca.CSH_DWR_CD, ca.POST_DT " &
              "FROM CSHR_ACTIVATION ca, emp " +
              "WHERE ca.CSHR_INIT = emp.emp_init AND emp.emp_cd = :EMPCD"

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objSql.Parameters.Add(":EMPCD", OracleType.VarChar)
        objSql.Parameters(":EMPCD").Value = UCase(empCd)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            If MyDataReader.Read() Then
                HttpContext.Current.Session("store_cd") = MyDataReader.Item("STORE_CD").ToString
                HttpContext.Current.Session("csh_dwr_cd") = MyDataReader.Item("CSH_DWR_CD").ToString
                HttpContext.Current.Session("CO_CD") = MyDataReader.Item("CO_CD").ToString
                If IsDate(MyDataReader.Item("POST_DT").ToString) Then
                    HttpContext.Current.Session("tran_dt") = FormatDateTime(MyDataReader.Item("POST_DT").ToString, DateFormat.ShortDate)
                End If
                HttpContext.Current.Session("csh_act_init") = MyDataReader.Item("CSHR_INIT").ToString
            Else
                HttpContext.Current.Session("csh_act_init") = "INVALIDCASHIER"
            End If

            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub
    ''' <summary>
    ''' This functions returns the activated store code for the user id passed
    ''' </summary>
    ''' <param name="empCd">Emp code</param>
    ''' <returns>Activated store code</returns>
    ''' <remarks></remarks>
    Public Shared Function GetActivatedStoreCode(ByVal empCd As String) As CashierInfo
        Dim cashierInfo As CashierInfo = New CashierInfo()

        Dim SQL As StringBuilder = New StringBuilder()
        SQL.Append("SELECT ca.CSHR_INIT, ca.CO_CD, ca.STORE_CD, ca.CSH_DWR_CD, ca.POST_DT ")
        SQL.Append("FROM CSHR_ACTIVATION ca, emp ")
        SQL.Append("WHERE ca.CSHR_INIT = emp.emp_init AND emp.emp_cd = :EMPCD")

        Using conn As OracleConnection = DisposablesManager.BuildOracleConnection, objSql As OracleCommand = DisposablesManager.BuildOracleCommand(SQL.ToString(), conn)
            Dim MyDataReader As OracleDataReader
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()
            objSql.Parameters.Add(":EMPCD", OracleType.VarChar)
            objSql.Parameters(":EMPCD").Value = UCase(empCd)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                If MyDataReader.Read() Then
                    'if activated store code is found, then return it
                    cashierInfo.ActivatedStoreCode = MyDataReader.Item("STORE_CD").ToString
                    cashierInfo.ActivatedCashDrawerCode = MyDataReader.Item("CSH_DWR_CD").ToString
                    cashierInfo.CashierInitials = MyDataReader.Item("CSHR_INIT").ToString
                    cashierInfo.CompanyCode = MyDataReader.Item("CO_CD").ToString
                    cashierInfo.Postdate = MyDataReader.Item("POST_DT").ToString
                Else
                    'Do Nothing
                End If
                MyDataReader.Close()
            Catch ex As Exception
                Throw
            Finally
                conn.Close()
            End Try
            Return cashierInfo
        End Using
    End Function

    ''' <summary>
    ''' Determines if SKU is dropped and no longer for sale
    ''' </summary>
    ''' <param name="prevSale">flag from drop_cd table for drop_cd assigned to itm</param>
    ''' <param name="dropDt">drop dt on the Item</param>
    ''' <returns>true if the SKU is for sale, false if it is dropped and prevent sale flag </returns>
    ''' <remarks></remarks>
    Private Shared Function isSkuForSale(ByVal prevSale As String, ByVal dropDt As String) As Boolean

        Dim forSale As Boolean = True

        If prevSale = "Y" AndAlso IsDate(dropDt) AndAlso FormatDateTime(dropDt) <= Today.Date Then
            forSale = False
        End If

        Return forSale
    End Function

    Public Shared Sub Store_Order_Data(Optional ByVal test As String = "")

        Dim sql, sql2 As String
        Dim s As String = Guid.NewGuid().ToString()
        Dim insertTempMaster As StringBuilder = New StringBuilder()
        Dim insertTempMasterValues As StringBuilder = New StringBuilder()

        insertTempMaster.Append("INSERT INTO TEMP_MASTER (SESSION_ID, SAVE_DATE ")
        insertTempMasterValues.Append("VALUES('").Append(s).Append("', TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate)).Append("','mm/dd/RRRR') ")

        If HttpContext.Current.Session("EMP_CD") & "" <> "" Then
            insertTempMaster.Append(",EMP_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("EMP_CD")).Append("'")
        End If
        If HttpContext.Current.Session("PD") & "" <> "" Then
            insertTempMaster.Append(",PD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("PD")).Append("'")

        End If
        If HttpContext.Current.Session("STORE_CD") & "" <> "" Then
            insertTempMaster.Append(",STORE_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("STORE_CD")).Append("'")

        End If
        If HttpContext.Current.Session("SLSP1") & "" <> "" Then
            insertTempMaster.Append(",SLSP1")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("SLSP1")).Append("'")

        End If
        If HttpContext.Current.Session("PCT_1") & "" <> "" Then
            insertTempMaster.Append(",PCT_1")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("PCT_1")).Append("'")

        End If
        If HttpContext.Current.Session("SLSP2") & "" <> "" Then
            insertTempMaster.Append(",SLSP2")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("SLSP2")).Append("'")

        End If
        If HttpContext.Current.Session("PCT_2") & "" <> "" Then
            insertTempMaster.Append(",PCT_2")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("PCT_2")).Append("'")

        End If
        If HttpContext.Current.Session("ORD_TP_CD") & "" <> "" Then
            insertTempMaster.Append(",ORD_TP_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("ORD_TP_CD")).Append("'")

        End If
        If HttpContext.Current.Session("ORD_SRT") & "" <> "" Then
            insertTempMaster.Append(",ORD_SRT_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("ORD_SRT")).Append("'")
        End If
        If HttpContext.Current.Session("PD_STORE_CD") & "" <> "" Then
            insertTempMaster.Append(",PD_STORE_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("PD_STORE_CD")).Append("'")

        End If
        If HttpContext.Current.Session("CO_CD") & "" <> "" Then
            insertTempMaster.Append(",CO_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("CO_CD")).Append("'")

        End If
        If HttpContext.Current.Session("CUST_CD") & "" <> "" Then
            insertTempMaster.Append(",CUST_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("CUST_CD")).Append("'")

        End If
        If HttpContext.Current.Session("TAX_RATE") & "" <> "" Then
            insertTempMaster.Append(",TAX_RATE")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("TAX_RATE")).Append("'")

        End If
        If HttpContext.Current.Session("TAX_CD") & "" <> "" Then
            insertTempMaster.Append(",TAX_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("TAX_CD")).Append("'")

        End If
        If HttpContext.Current.Session("NO_TAX") & "" <> "" Then
            insertTempMaster.Append(",NO_TAX")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("NO_TAX")).Append("'")

        End If
        If HttpContext.Current.Session("TET_CD") & "" <> "" Then
            insertTempMaster.Append(",TET_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("TET_CD")).Append("'")

        End If
        If HttpContext.Current.Session("DEL_CHG") & "" <> "" Then
            insertTempMaster.Append(",DEL_CHG")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("DEL_CHG")).Append("'")

        End If
        If HttpContext.Current.Session("DISC_CD") & "" <> "" Then
            insertTempMaster.Append(",DISC_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("DISC_CD")).Append("'")

        End If
        If HttpContext.Current.Session("DISC_TP") & "" <> "" Then
            insertTempMaster.Append(",DISC_TP")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("DISC_TP")).Append("'")

        End If
        If HttpContext.Current.Session("DISC_AMT") & "" <> "" Then
            insertTempMaster.Append(",DISC_AMT")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("DISC_AMT")).Append("'")

        End If
        If HttpContext.Current.Session("ZIP_CD") & "" <> "" Then
            insertTempMaster.Append(",ZIP_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("ZIP_CD")).Append("'")

        End If
        If HttpContext.Current.Request("LEAD") & "" <> "" Then
            insertTempMaster.Append(",LEAD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("LEAD")).Append("'")

        End If
        If HttpContext.Current.Session("ZONE_CD") & "" <> "" Then
            insertTempMaster.Append(",ZONE_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("ZONE_CD")).Append("'")

        End If
        If HttpContext.Current.Session("SCOMMENTS") & "" <> "" Then
            insertTempMaster.Append(",SCOMMENTS")
            insertTempMasterValues.Append(",'").Append(Left(Replace(HttpContext.Current.Session("SCOMMENTS"), "'", "''"), 250)).Append("'")

        End If
        If HttpContext.Current.Session("DCOMMENTS") & "" <> "" Then
            insertTempMaster.Append(",DCOMMENTS")
            insertTempMasterValues.Append(",'").Append(Left(Replace(HttpContext.Current.Session("DCOMMENTS"), "'", "''"), 250)).Append("'")

        End If
        If HttpContext.Current.Session("ARCOMMENTS") & "" <> "" Then
            insertTempMaster.Append(",ARCOMMENTS")
            insertTempMasterValues.Append(",'").Append(Left(Replace(HttpContext.Current.Session("ARCOMMENTS"), "'", "''"), 250)).Append("'")

        End If
        If HttpContext.Current.Session("DEL_DT") & "" <> "" Then
            insertTempMaster.Append(",DEL_DT")
            insertTempMasterValues.Append(",'").Append(FormatDateTime(HttpContext.Current.Session("DEL_DT"), DateFormat.ShortDate)).Append("'")

        End If
        If HttpContext.Current.Session("SETUP_CHG") & "" <> "" Then
            insertTempMaster.Append(",SETUP_CHG")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("SETUP_CHG")).Append("'")

        End If
        'MM-9227
        If HttpContext.Current.Session("adj_ivc_cd") & "" <> "" Then
            insertTempMaster.Append(",ORIG_DEL_DOC_NUM")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("adj_ivc_cd")).Append("'")

        End If
        If HttpContext.Current.Session("crm_cause_cd") & "" <> "" Then
            insertTempMaster.Append(",CAUSE_CD")
            insertTempMasterValues.Append(",'").Append(HttpContext.Current.Session("crm_cause_cd")).Append("'")

        End If

        sql = UCase(insertTempMaster.ToString & ")" & insertTempMasterValues.ToString & ")")

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Open Connection 
            conn2.Open()
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            'Close Connection 
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        Dim MyDataReader2 As OracleDataReader
        If HttpContext.Current.Session("CUST_CD") & "" = "" Then
            sql = "SELECT CUST_CD FROM CUST_INFO WHERE SESSION_ID='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql)
                'Store Values in String Variables 
                If MyDataReader2.Read() Then
                    HttpContext.Current.Session("cust_cd") = MyDataReader2.Item("CUST_CD").ToString
                    MyDataReader2.Close()
                    objsql.Dispose()
                    sql = "UPDATE TEMP_MASTER SET CUST_CD='" & HttpContext.Current.Session("cust_cd") & "' WHERE SESSION_ID='" & s & "'"
                    objsql = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objsql.ExecuteNonQuery()
                End If
                'Close Connection 
                MyDataReader2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        End If

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        cmdDeleteItems = DisposablesManager.BuildOracleCommand
        With cmdDeleteItems
            .Connection = conn2
            .CommandText = "update cust_info set session_id='" & UCase(s) & "' where session_id='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()
        cmdDeleteItems.Dispose()
        cmdDeleteItems = Nothing
        cmdDeleteItems = DisposablesManager.BuildOracleCommand
        With cmdDeleteItems
            .Connection = conn2
            .CommandText = "update payment set sessionid='" & UCase(s) & "' where sessionid='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()
        cmdDeleteItems.Dispose()
        cmdDeleteItems = Nothing
        cmdDeleteItems = DisposablesManager.BuildOracleCommand
        With cmdDeleteItems
            .Connection = conn2
            .CommandText = "update temp_itm set session_id='" & UCase(s) & "' where session_id='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()
        cmdDeleteItems.Dispose()
        cmdDeleteItems = Nothing
        cmdDeleteItems = DisposablesManager.BuildOracleCommand
        With cmdDeleteItems
            .Connection = conn2
            .CommandText = "update multi_disc set session_id='" & UCase(s) & "' where session_id='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()

        Dim emp_cd, store_cd, fname, lname, IPAD, co_cd As String

        fname = ""
        lname = ""
        emp_cd = HttpContext.Current.Session("EMP_CD")
        co_cd = HttpContext.Current.Session("CO_CD")
        fname = HttpContext.Current.Session("EMP_FNAME")
        lname = HttpContext.Current.Session("EMP_LNAME")
        store_cd = HttpContext.Current.Session("HOME_STORE_CD")
        IPAD = HttpContext.Current.Session("IPAD")

        ' Daniela save clientip and emp_init, super user
        Dim client_IP As String = HttpContext.Current.Session("clientip")
        Dim emp_init As String = HttpContext.Current.Session("EMP_INIT")
        Dim supUser As String = HttpContext.Current.Session("str_sup_user_flag")
        Dim coGrpCd As String = HttpContext.Current.Session("str_co_grp_cd")
	Dim userType As String = HttpContext.Current.Session("USER_TYPE")
        HttpContext.Current.Session.Clear()
        HttpContext.Current.Session("clientip") = client_IP
        HttpContext.Current.Session("EMP_INIT") = emp_init
        HttpContext.Current.Session("str_sup_user_flag") = supUser
        HttpContext.Current.Session("str_co_grp_cd") = coGrpCd ' Daniela
	HttpContext.Current.Session("USER_TYPE") = userType ' Daniela
        HttpContext.Current.Session("EMP_CD") = emp_cd
        HttpContext.Current.Session("IPAD") = IPAD
        HttpContext.Current.Session("CO_CD") = co_cd
        HttpContext.Current.Session("EMP_FNAME") = fname
        HttpContext.Current.Session("EMP_LNAME") = lname
        HttpContext.Current.Session("HOME_STORE_CD") = store_cd
        HttpContext.Current.Session("MP") = "DELIVERY AVAILABILITY"
        HttpContext.Current.Session("IP") = "INVENTORY LOOKUP"

        conn2.Close()

    End Sub

    Public Shared Function Lookup_Merchant(ByVal mop_tp As String, ByVal store_cd As String, ByVal mop_cd As String)

        Select Case mop_tp
            Case "BC"
                If ConfigurationManager.AppSettings("cc") = "N" Then
                    Return ""
                    Exit Function
                End If
            Case "CK"
                If ConfigurationManager.AppSettings("chk_guar") = "N" Then
                    Return ""
                    Exit Function
                End If
            Case "FI"
                If ConfigurationManager.AppSettings("finance") = "N" Then
                    Return ""
                    Exit Function
                End If
        End Select

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        sql = "SELECT MERCHANT_ID FROM PAYMENT_XREF WHERE STORE_CD='" & store_cd & "' AND MOP_TP='" & mop_tp & "' "
        If mop_tp = "GC" Or mop_tp = "SIG" Then
            'Do Nothing
        Else
            sql = sql & "AND MOP_CD='" & mop_cd & "'"
        End If

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)
            If MyDatareader2.Read Then
                Return MyDatareader2.Item("MERCHANT_ID").ToString
            Else
                Return ""
            End If
        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Shared Function Lookup_Transport(ByVal mop_tp As String, ByVal store_cd As String, ByVal mop_cd As String)

        Select Case mop_tp
            Case "BC"
                If ConfigurationManager.AppSettings("cc") = "N" Then
                    Return ""
                    Exit Function
                End If
            Case "CK"
                If ConfigurationManager.AppSettings("chk_guar") = "N" Then
                    Return ""
                    Exit Function
                End If
            Case "FI"
                If ConfigurationManager.AppSettings("finance") = "N" Then
                    Return ""
                    Exit Function
                End If
        End Select

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        sql = "SELECT TRANSPORT FROM PAYMENT_XREF WHERE STORE_CD='" & store_cd & "' AND MOP_TP='" & mop_tp & "' AND MOP_CD='" & mop_cd & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)
            If MyDatareader2.Read Then
                Return MyDatareader2.Item("TRANSPORT").ToString
            Else
                Return ""
            End If
        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    ''' <summary>
    ''' Inserts a record into the PAYMENT_HOLDING table
    ''' </summary>
    ''' <param name="dbCommand">the database command to be used</param>
    ''' <param name="row_id">row number to persist</param>
    ''' <param name="host_resp_msg">the host response message received upon request for authorization</param>
    ''' <param name="host_resp_cd">the host response code received upon request for authorization</param>
    ''' <param name="pb_resp_msg">the protobase response message received upon request for authorization</param>
    ''' <param name="trn_tp_cd">the transaction type code</param>
    ''' <param name="del_doc_num">the document number</param>
    ''' <remarks></remarks>
    Public Shared Sub SavePaymentHolding(ByRef dbCommand As OracleCommand, ByVal rowId As String,
                                         ByVal hostRespMsg As String, ByVal hostRespCd As String,
                                         ByVal pbRespMsg As String, ByVal trnTpCd As String, ByVal delDcoNum As String)

        ' --- Retrieves the additional values required to create the record in the PAYMENT_HOLDING table
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String
        sql = "SELECT ROW_ID, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP "
        sql = sql & ",FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD, CHK_NUM, CHK_TP, TRANS_ROUTE, "
        sql = sql & "CHK_ACCOUNT_NO, PB_RESPONSE_MSG, HOST_RESP_MSG, HOST_RESPONSE_CD, DL_STATE, DL_LICENSE_NO FROM PAYMENT WHERE ROW_ID=" & rowId

        Try
            dbConnection.Open()
            dbCmd = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCmd)
            If dbReader.Read Then

                sql = "INSERT INTO PAYMENT_HOLDING (ROW_ID, DEL_DOC_NUM, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP "
                sql = sql & ",FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD, CHK_NUM, CHK_TP, TRANS_ROUTE, "
                sql = sql & "CHK_ACCOUNT_NO, TRN_TP_CD, PB_RESPONSE_MSG, HOST_RESPONSE_MSG, HOST_RESPONSE_CD, DL_STATE, DL_LICENSE_NO) "
                sql = sql & "VALUES(:ROW_ID, :DEL_DOC_NUM, :MOP_CD, :AMT, :DES, :BC, :EXP_DT, :MOP_TP "
                sql = sql & ",:FIN_CO, :FIN_PROMO_CD, :FI_ACCT_NUM, :APPROVAL_CD, :CHK_NUM, :CHK_TP, :TRANS_ROUTE, "
                sql = sql & ":CHK_ACCOUNT_NO, :TRN_TP_CD, :PB_RESPONSE, :HOST_RESPONSE_MSG, :HOST_RESPONSE_CD, :DL_STATE, :DL_LICENSE_NO) "

                dbCommand.CommandText = sql
                dbCommand.Parameters.Add(":ROW_ID", OracleType.Number)
                dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                dbCommand.Parameters.Add(":MOP_CD", OracleType.VarChar)
                dbCommand.Parameters.Add(":AMT", OracleType.Number)
                dbCommand.Parameters.Add(":DES", OracleType.VarChar)
                dbCommand.Parameters.Add(":BC", OracleType.VarChar)
                dbCommand.Parameters.Add(":EXP_DT", OracleType.VarChar)
                dbCommand.Parameters.Add(":MOP_TP", OracleType.VarChar)
                dbCommand.Parameters.Add(":FIN_CO", OracleType.VarChar)
                dbCommand.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
                dbCommand.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
                dbCommand.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
                dbCommand.Parameters.Add(":CHK_NUM", OracleType.VarChar)
                dbCommand.Parameters.Add(":CHK_TP", OracleType.VarChar)
                dbCommand.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
                dbCommand.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)
                dbCommand.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                dbCommand.Parameters.Add(":PB_RESPONSE", OracleType.VarChar)
                dbCommand.Parameters.Add(":HOST_RESPONSE_MSG", OracleType.VarChar)
                dbCommand.Parameters.Add(":HOST_RESPONSE_CD", OracleType.VarChar)
                dbCommand.Parameters.Add(":DL_STATE", OracleType.VarChar)
                dbCommand.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

                dbCommand.Parameters(":ROW_ID").Value = CDbl(rowId)
                dbCommand.Parameters(":DEL_DOC_NUM").Value = delDcoNum
                dbCommand.Parameters(":MOP_CD").Value = dbReader.Item("MOP_CD").ToString
                dbCommand.Parameters(":AMT").Value = CDbl(dbReader.Item("AMT").ToString)
                dbCommand.Parameters(":DES").Value = dbReader.Item("DES").ToString
                dbCommand.Parameters(":BC").Value = dbReader.Item("BC").ToString
                dbCommand.Parameters(":EXP_DT").Value = dbReader.Item("EXP_DT").ToString
                dbCommand.Parameters(":MOP_TP").Value = dbReader.Item("MOP_TP").ToString
                dbCommand.Parameters(":FIN_CO").Value = dbReader.Item("FIN_CO").ToString
                dbCommand.Parameters(":FIN_PROMO_CD").Value = dbReader.Item("FIN_PROMO_CD").ToString
                dbCommand.Parameters(":FI_ACCT_NUM").Value = dbReader.Item("FI_ACCT_NUM").ToString
                dbCommand.Parameters(":APPROVAL_CD").Value = dbReader.Item("APPROVAL_CD").ToString
                dbCommand.Parameters(":CHK_NUM").Value = dbReader.Item("CHK_NUM").ToString
                dbCommand.Parameters(":CHK_TP").Value = dbReader.Item("CHK_TP").ToString
                dbCommand.Parameters(":TRANS_ROUTE").Value = dbReader.Item("TRANS_ROUTE").ToString
                dbCommand.Parameters(":CHK_ACCOUNT_NO").Value = dbReader.Item("CHK_ACCOUNT_NO").ToString
                dbCommand.Parameters(":TRN_TP_CD").Value = trnTpCd
                dbCommand.Parameters(":PB_RESPONSE").Value = pbRespMsg
                dbCommand.Parameters(":HOST_RESPONSE_MSG").Value = hostRespMsg
                dbCommand.Parameters(":HOST_RESPONSE_CD").Value = hostRespCd
                dbCommand.Parameters(":DL_STATE").Value = dbReader.Item("DL_STATE").ToString
                dbCommand.Parameters(":DL_LICENSE_NO").Value = dbReader.Item("DL_LICENSE_NO").ToString

                dbCommand.ExecuteNonQuery()
                dbCommand.Parameters.Clear()

            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub

    Public Shared Sub Update_Payment_Holding(ByVal row_id As String, ByVal host_resp_msg As String, ByVal host_resp_cd As String, ByVal pb_resp_msg As String, ByVal trn_tp_cd As String, ByVal del_doc_num As String)

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand("", dbConnection)

            SavePaymentHolding(dbCommand, row_id, host_resp_msg, host_resp_cd, pb_resp_msg, trn_tp_cd, del_doc_num)

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub

    Public Shared Function Calculate_Take_With()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd = DisposablesManager.BuildOracleCommand
        cmd.Connection = conn

        Try
            conn.Open()

            Calculate_Take_With = Calculate_Take_With(cmd)

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Shared Function Calculate_Take_With(ByRef cmd As OracleCommand)

        Dim sql As String = "SELECT NVL(SUM(TAXABLE_AMT*QTY),0) as line_total, NVL(SUM(QTY*TAX_AMT),0) As total_ln_tax FROM TEMP_ITM where TAKE_WITH='Y' AND session_id='" & HttpContext.Current.Session.SessionID.ToString.Trim & "'"

        cmd.Parameters.Clear()
        cmd.CommandText = sql
        cmd.CommandType = CommandType.Text
        'Dim conn As New OracleConnection
        'Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        'conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
        'conn.Open()
        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)
            If MyDataReader.Read() Then
                If IsNumeric(MyDataReader.Item("line_total").ToString) Then

                    Calculate_Take_With = MyDataReader.Item("line_total").ToString

                    If ConfigurationManager.AppSettings("tax_line").ToString = "Y" AndAlso
                        IsNumeric(MyDataReader.Item("total_ln_tax").ToString) Then  ' line taxing

                        Calculate_Take_With = Calculate_Take_With + MyDataReader.Item("total_ln_tax").ToString

                    Else ' header taxing
                        Dim wr_Dt As DateTime
                        If IsDate(HttpContext.Current.Session("tran_dt")) Then

                            wr_Dt = FormatDateTime(HttpContext.Current.Session("tran_dt"), DateFormat.ShortDate)
                        Else
                            wr_Dt = FormatDateTime(Today.Date, DateFormat.ShortDate)
                        End If
                        Calculate_Take_With = Calculate_Take_With + OrderUtils.Calc_Hdr_Tax_From_Temp(HttpContext.Current.Session.SessionID.ToString.Trim, HttpContext.Current.Session("tax_cd"), wr_Dt, 0, 0, OrderUtils.tempItmSelection.TakeWith)
                    End If

                Else
                    Calculate_Take_With = 0
                End If
            Else
                Calculate_Take_With = 0
            End If

            MyDataReader.Close()
        Catch ex As Exception
            'conn.Close()
            Throw
        End Try

    End Function

    Public Shared Function Is_Finance_DP(ByVal mopCd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Is_Finance_DP = False

        If mopCd.isNotEmpty Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT MOP_CD FROM SETTLEMENT_MOP WHERE FINANCE_DEPOSIT_TP='Y' AND MOP_CD=:MOP_CD"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
            cmd.Parameters(":MOP_CD").Value = mopCd
            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)
                If reader.Read() Then
                    If reader.Item("MOP_CD") & "" <> "" Then
                        Is_Finance_DP = True
                    End If
                End If
                reader.Close()
            Catch ex As Exception

                Throw
            Finally
                conn.Close()
            End Try
        End If

    End Function

    Public Shared Function Get_next_cust_cmnt_seq(ByVal custCd As String) As Double
        'Returns the next sequence number from the customer comment table

        Const CUST_CMNT_NEXT_SEQ_SQL As String = "SELECT NVL(MAX(seq#),0) + 1 NXT_SEQ FROM cust_cmnt WHERE cust_cd = :custCd "
        Dim returnVal As Integer = 0

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim cmd = DisposablesManager.BuildOracleCommand(CUST_CMNT_NEXT_SEQ_SQL, conn)
        Dim datRdr As OracleDataReader

        Try
            conn.Open()

            cmd.Parameters.Add("custCd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters("custCd").Value = custCd & ""

            datRdr = DisposablesManager.BuildOracleDataReader(cmd)
            If datRdr.Read Then
                returnVal = datRdr.Item("NXT_SEQ").ToString
            End If

        Catch ex As Exception
            Throw New Exception("Error in Get_next_custCmntSeq 1 ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnVal

    End Function

    Public Shared Function DP_String_Creation()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim dp_string As String = ""

        Try

            conn.Open()

            sql = "SELECT MOP_CD FROM SETTLEMENT_MOP WHERE FINANCE_DEPOSIT_TP='Y'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            Do While MyDataReader.Read()
                dp_string = dp_string & "'" & MyDataReader.Item("MOP_CD").ToString & "',"
            Loop
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            'Daniela, no need to throw error
            'Throw
        End Try

        If dp_string & "" = "" Then
            DP_String_Creation = ""
        Else
            DP_String_Creation = Left(dp_string, Len(dp_string) - 1)
        End If

    End Function

    Public Shared Function Terminal_Locked(ByVal IP_Address As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT STATUS FROM TERMINAL_STATUS WHERE TERMINAL_IP='" & IP_Address & "'"

        sql = UCase(sql)

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            If MyDataReader.Read Then
                If MyDataReader.Item("STATUS").ToString = "O" Then
                    Terminal_Locked = "O"
                Else
                    Terminal_Locked = "C"
                End If
            Else
                Terminal_Locked = "C"
            End If
        Catch
            conn.Close()
            Throw
        End Try

        If Terminal_Locked = "C" Then
            sql = "SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP = '" & Left(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), Find_First_IP(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))) & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                If MyDataReader.Read Then
                    Terminal_Locked = "O"
                End If
            Catch
                conn.Close()
                Throw
            End Try

        End If

        conn.Close()

        Return Terminal_Locked

    End Function

    ''' <summar>
    ''' DEPRECATED 
    ''' Determines if the employee passed-in has the sepcified security or not.A "Y" indicates
    ''' that either the employee has all securities(by virtue of the override flag) or has the
    ''' security passed-in.
    ''' </summary>
    ''' <param name="sec_cd">the security code to check</param>
    ''' <param name="emp_cd">the employee code of the empoyee to find the security for.</param>
    ''' <returns> "Y" to indicate that either the employee has the security passed-in.
    '''           "N" otherwise.</returns>
    Public Shared Function Find_Security(ByVal sec_cd As String, ByVal emp_cd As String)

        ' NOTE THAT THIS ROUTINE HAS BEEN DEPRECATED - PLEASE CALL SecurityUtils hasSecurity

        Find_Security = "N"

        If SecurityUtils.hasSecurity(sec_cd, emp_cd) Then

            Find_Security = "Y"
        End If

    End Function

    Public Shared Function Find_Merchandise_Images(ByVal SKU As String)

        Find_Merchandise_Images = ""
        Dim TheFile2 As System.IO.FileInfo = New System.IO.FileInfo(HttpContext.Current.Server.MapPath("\merch_images\") & SKU & ".jpg")
        If TheFile2.Exists Then
            Find_Merchandise_Images = "~/merch_images/" & SKU & ".jpg"
            Return Find_Merchandise_Images
            Exit Function
        End If
        Dim TheFile1 As System.IO.FileInfo = New System.IO.FileInfo(HttpContext.Current.Server.MapPath("\merch_images\") & SKU & ".gif")
        If TheFile1.Exists Then
            Find_Merchandise_Images = "~/merch_images/" & SKU & ".gif"
            Return Find_Merchandise_Images
            Exit Function
        End If
        Dim TheFile3 As System.IO.FileInfo = New System.IO.FileInfo(HttpContext.Current.Server.MapPath("\merch_images\") & SKU & ".png")
        If TheFile3.Exists Then
            Find_Merchandise_Images = "~/merch_images/" & SKU & ".png"
            Return Find_Merchandise_Images
            Exit Function
        End If

    End Function

    Public Shared Function Check_E1_Version()

        Check_E1_Version = "0"

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        sql = sql & "select value from gp_parms where key='VERSION'"
        sql = UCase(sql)

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            If MyDataReader.Read Then
                Check_E1_Version = MyDataReader.Item("VALUE").ToString.Trim
            End If
            MyDataReader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Function

    Public Shared Function Check_Inventory(store_cd As String, loc_cd As String, itm_cd As String, qty As Double)

        Check_Inventory = "N"

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        If LocType(store_cd, loc_cd) = "O" Then
            sql = "select SUM(QTY) from OUT WHERE OUT.STORE_CD=:STORE_CD AND OUT.LOC_CD=:LOC_CD AND ITM_CD=:ITM_CD HAVING SUM(QTY)>=:QTY"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters(":ITM_CD").Value = itm_cd
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters(":STORE_CD").Value = store_cd
            objSql.Parameters.Add(":LOC_CD", OracleType.VarChar)
            objSql.Parameters(":LOC_CD").Value = loc_cd
            objSql.Parameters.Add(":QTY", OracleType.Number)
            objSql.Parameters(":QTY").Value = qty

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                If MyDataReader.Read Then
                    Check_Inventory = "Y"
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
        Else
            sql = "select SUM(QTY) from ITM_FIFL WHERE ITM_FIFL.STORE_CD=:STORE_CD AND ITM_FIFL.LOC_CD=:LOC_CD AND ITM_CD=:ITM_CD HAVING SUM(QTY)>=:QTY"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters(":ITM_CD").Value = itm_cd
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters(":STORE_CD").Value = store_cd
            objSql.Parameters.Add(":LOC_CD", OracleType.VarChar)
            objSql.Parameters(":LOC_CD").Value = loc_cd
            objSql.Parameters.Add(":QTY", OracleType.Number)
            objSql.Parameters(":QTY").Value = qty

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                If MyDataReader.Read Then
                    Check_Inventory = "Y"
                End If
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()

    End Function

    Public Shared Sub calculate_total(ByVal master As MasterPage)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim numrows As Integer
        Dim total As Double
        Dim tax As Double
        Dim Session_String As String = HttpContext.Current.Session.SessionID

        conn.Open()

        If ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED" And HttpContext.Current.Session("PD") = "D" Then

            Dim Delivery_Amount As Double
            Dim objsql As OracleCommand
            sql = "select sum(ret_prc*qty) As Total_Order from temp_itm where itm_tp_cd = 'INV' AND Session_id='" & Session_String & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)
            If MyDataReader.Read Then
                If MyDataReader.Item(0).ToString & "" <> "" Then
                    Delivery_Amount = CDbl(MyDataReader.Item(0).ToString)
                End If
            End If
            MyDataReader.Close()
            sql = "select p_or_d, amount from tiered_delivery where lower_limit < " & Delivery_Amount & " AND upper_limit >= " & Delivery_Amount
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)
            If MyDataReader.Read Then
                If MyDataReader.Item(0) = "D" Then
                    HttpContext.Current.Session("DEL_CHG") = CDbl(MyDataReader.Item(1).ToString)
                Else
                    HttpContext.Current.Session("DEL_CHG") = (Delivery_Amount * (CDbl(MyDataReader.Item(1).ToString) / 100))
                End If
            End If
            MyDataReader.Close()
        End If

        sql = "select sum(ret_prc*qty) As Total_Order, sum(tax_amt*qty) As Total_Tax from temp_itm where session_id='" & Session_String & "'"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        Try
            If IsNumeric(MyTable.Rows(0).Item("Total_Order")) Then
                total = total + MyTable.Rows(0).Item("Total_Order")
            Else
                total = total + 0
            End If
            If IsNumeric(MyTable.Rows(0).Item("Total_Tax")) Then
                tax = tax + MyTable.Rows(0).Item("Total_Tax")
            Else
                tax = tax + 0
            End If

            '
            ' Changed to reflect the session variable which is calculated at the POS
            '
            Dim del_tax As Double = 0
            Dim setup_tax As Double = 0
            Dim del_chg As Double = 0
            Dim setup_chg As Double = 0
            Dim grandtotal As Double = 0

            If HttpContext.Current.Session("sub_total") & "" <> "" Then
                If IsNumeric(HttpContext.Current.Session("DEL_CHG")) Then
                    del_chg = CDbl(HttpContext.Current.Session("DEL_CHG"))
                    If IsNumeric(HttpContext.Current.Session("DEL_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                        del_tax = CDbl(HttpContext.Current.Session("DEL_CHG")) * (HttpContext.Current.Session("DEL_TAX") / 100)
                    End If
                End If
                If IsNumeric(HttpContext.Current.Session("SETUP_CHG")) Then
                    setup_chg = CDbl(HttpContext.Current.Session("SETUP_CHG"))
                    If IsNumeric(HttpContext.Current.Session("SU_TAX")) And IsNumeric(HttpContext.Current.Session("TAX_RATE")) Then
                        setup_tax = CDbl(HttpContext.Current.Session("SETUP_CHG")) * (HttpContext.Current.Session("SU_TAX") / 100)
                    End If
                End If
                ' round to currency for final total and display
                tax = Math.Round(CDbl(tax + del_tax + setup_tax), 2, MidpointRounding.AwayFromZero)
            End If

            If Not String.IsNullOrEmpty(HttpContext.Current.Session("tet_cd")) Then
                tax = 0

                ' TODO - someday this needs to be based on tax code tax method
            ElseIf "N".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                'If Header tax Then
                ' need the written date and for new sales, this is the written date logic from Order Stage; for returns, is written date of original sale if have
                Dim wr_Dt As Date
                If (Not HttpContext.Current.Session("origSoWrDt") Is Nothing) AndAlso (HttpContext.Current.Session("origSoWrDt").ToString + "").isNotEmpty AndAlso IsDate(HttpContext.Current.Session("origSoWrDt")) Then

                    wr_Dt = FormatDateTime(HttpContext.Current.Session("origSoWrDt"), DateFormat.ShortDate)

                ElseIf IsDate(HttpContext.Current.Session("tran_dt")) Then

                    wr_Dt = FormatDateTime(HttpContext.Current.Session("tran_dt"), DateFormat.ShortDate)

                Else
                    wr_Dt = FormatDateTime(Today.Date, DateFormat.ShortDate)
                End If

                ' in order to have the exact same pennies, need to handle exactly as will at OrderStage when split
                ' option 1: figure total, figure one part, then subtract to get total of other
                ' option 2: figure each part (TW and non) and add together
                ' option 2 is chosen since STA will handle separately, handled separately here also
                Try
                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session_String, HttpContext.Current.Session("tax_cd"), wr_Dt, setup_chg, del_chg, OrderUtils.tempItmSelection.NonTakeWith)
                    tax = tax + OrderUtils.Calc_Hdr_Tax_From_Temp(Session_String, HttpContext.Current.Session("tax_cd"), wr_Dt, 0, 0, OrderUtils.tempItmSelection.TakeWith)
                Catch ex As Exception
                    Throw
                End Try
            End If

            Dim footer As New DevExpress.Web.ASPxEditors.ASPxLabel
            footer = master.FindControl("ASPxRoundPanel1").FindControl("lblsubtotal")
            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(total))
            End If
            footer = Nothing

            footer = master.FindControl("ASPxRoundPanel1").FindControl("lblDelivery")
            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(del_chg))
            End If
            footer = Nothing

            footer = master.FindControl("ASPxRoundPanel1").FindControl("lblSetup")
            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(setup_chg))
            End If
            footer = Nothing

            footer = master.FindControl("ASPxRoundPanel1").FindControl("lbltotal")
            grandtotal = FormatCurrency(CDbl(total) + CDbl(tax) + CDbl(del_chg) + CDbl(setup_chg), 2)
            HttpContext.Current.Session("grand_total") = grandtotal

            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(total) + CDbl(tax) + CDbl(del_chg) + CDbl(setup_chg), 2)
            End If

            If IsNumeric(tax) Then
                HttpContext.Current.Session("TAX_CHG") = tax
                footer = Nothing
                footer = master.FindControl("ASPxRoundPanel1").FindControl("lbltax")
                If Not footer Is Nothing Then
                    footer.Text = FormatCurrency(CDbl(tax))
                End If
            End If
            If IsNumeric(total) Then
                HttpContext.Current.Session("sub_total") = CDbl(total)
            End If
            Dim lbl_Payments As DevExpress.Web.ASPxEditors.ASPxLabel = master.FindControl("ASPxRoundPanel1").FindControl("lbl_Payments")
            Dim lbl_balance As DevExpress.Web.ASPxEditors.ASPxLabel = master.FindControl("ASPxRoundPanel1").FindControl("lbl_balance")

            If Not lbl_Payments Is Nothing Then
                If HttpContext.Current.Session("payment") & "" <> "" Then
                    lbl_Payments.Text = FormatCurrency(CDbl(HttpContext.Current.Session("payments")), 2)
                Else
                    lbl_Payments.Text = FormatCurrency(0, 2)
                End If
            End If
            If Not lbl_balance Is Nothing Then
                If IsNumeric(lbl_Payments.Text) And IsNumeric(grandtotal) Then
                    lbl_balance.Text = FormatCurrency(CDbl(grandtotal) - CDbl(lbl_Payments.Text), 2)
                Else
                    lbl_balance.Text = FormatCurrency(0, 2)
                End If
            End If

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
    End Sub

    Public Shared Function ResolveYnToBooleanEquiv(ByVal inpChar As String, Optional ByVal deflt As Boolean = False) As Boolean
        'since BT integration can only handle primitive types, resolve Char Y/N to boolean
        '  can only be used if N equates to False and Y equates to True
        ' default occurs if the input is blank
        Return StringUtils.ResolveYNToBoolean(inpChar, deflt)
    End Function

    Public Shared Function Get_Connection_String(ByVal vendor_name As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Request.ServerVariables("APPL_PHYSICAL_PATH") & "appTConnConfig.xml")
        m_nodelist = m_xmld.SelectNodes(vendor_name)
        For Each m_node In m_nodelist
            If m_node.FirstChild.Name = criteria Then
                desc = m_node.ChildNodes.Item(0).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc
    End Function

    Public Shared Function Get_Version(ByVal vendor_name As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Request.ServerVariables("APPL_PHYSICAL_PATH") & "version.xml")
        m_nodelist = m_xmld.SelectNodes(vendor_name)
        For Each m_node In m_nodelist
            If m_node.FirstChild.Name = criteria Then
                desc = m_node.ChildNodes.Item(0).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc
    End Function

    Public Shared Function Find_First_IP(ByVal IPAddress As String) As Double

        Find_First_IP = 0
        Dim Second_String As String = ""
        Dim Pos1 As Double = 0
        Dim Pos2 As Double = 0

        Pos1 = IPAddress.IndexOf(".") + 1
        Second_String = Right(IPAddress, Len(IPAddress) - Pos1)
        Pos2 = Second_String.IndexOf(".") + 1
        Find_First_IP = Pos1 + Pos2

    End Function


    ''' <summary>
    ''' Saves a comment for the passed-in order based on the type of comment specified.
    ''' </summary>
    ''' <param name="dbAtomicCommand">the database command to be used</param>
    ''' <param name="delDocNum">the current del doc number</param>
    ''' <param name="wrDt">the date of the comment</param>
    ''' <param name="cmntType">the type of comment</param>
    ''' <param name="oldValue">the old value to track</param>
    ''' <param name="newValue">the new value to track</param>
    ''' <param name="empCd">the current employee</param>
    ''' <param name="lineNo">the line for which the comment is being made, if any</param>
    ''' <remarks></remarks>
    Public Shared Sub SaveAutoComment(ByRef dbAtomicCommand As OracleCommand,
                                        ByVal delDocNum As String,
                                        ByVal wrDt As Date,
                                        ByVal cmntType As String,
                                        ByVal oldValue As String,
                                        ByVal newValue As String,
                                        ByVal empCd As String,
                                        Optional lineNo As String = "")
        Dim strComments As String
        Dim lineText As String = If(lineNo.isEmpty, "", " ON LINE " & lineNo)

        'TODO future - all UI must be emp_init, not emp_cd
        'TODO future - need to make these strings into constants

        Select Case cmntType

            Case "DC" ' Delivery Charge Changes
                If SysPms.cmtDelChargeChng Then
                    strComments = "DELIVERY CHARGE CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd
                End If
            Case "FA" ' Finance Amount Changes
                If SysPms.cmtFinAmountChng Then
                    strComments = "FINANCE AMOUNT CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd
                End If
            Case "TC" ' Tax Charge Changes
                If SysPms.cmtTaxChargeChng Then
                    strComments = "TAX CHARGE CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd
                End If
            Case "TCD" ' Tax Code Changes               
                strComments = "TAX CODE CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
            Case "ZC" ' Zone Code Changes
                If SysPms.cmtZoneCdChng Then
                    strComments = "ZONE CODE CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
                End If
            Case "PDF" ' Pickup/Delivery Flag Changes
                If SysPms.cmtPuDelFlagChng Then
                    strComments = "PICKUP/DELIVERY FLAG CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
                End If
            Case "PDD" ' Pickup/Delivery Date Changes
                If SysPms.cmtPuDelDateChng Then
                    strComments = "PICKUP/DELIVERY DATE CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
                End If
            Case "SC" ' Setup Charge Changes
                If SysPms.cmtSetupChargeChng Then
                    strComments = "SETUP CHARGE CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd
                End If
            Case "SQC" ' SKU Quantity Changes
                If SysPms.cmtQtyChng Then
                    strComments = "SKU QUANTITY CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd & lineText
                End If
            Case "SKC" ' SKU Changes
                If SysPms.cmtSkuChng Then
                    strComments = "SKU CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd & lineText
                End If
            Case "SRC" ' SKU Retail Changes
                If SysPms.cmtPriceChng Then
                    strComments = "SKU RETAIL CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd & lineText
                End If
            Case "NLA" ' New Line Added
                If SysPms.cmtNewItems Then
                    strComments = "SKU " & newValue & " ADDED BY " & empCd & lineText
                End If
            Case "FCC" ' Finance Company Changes
                If SysPms.cmtFinCompanyChng Then
                    strComments = "FINANCE COMPANY CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
                End If
            Case "FAC" ' Finance authorization code #ITREQUEST-149
                strComments = "FINANCE AUTHORIZATION CODE CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd

            Case "CON" ' Confirmation Status Code
                strComments = "CONFIRMATION STATUS CODE CHANGED FROM " & oldValue & " TO " & newValue & " BY " & empCd
            Case "WLR"
                strComments = "Line#:" & oldValue & " WARRANTY SKU:" & newValue & " Removed BY:" & empCd
            Case "APPCMNT"  'Alice added for request 294 - add approver comments for price changes
                strComments = oldValue & " BY " & empCd
            Case "DISC"
                If SysPms.cmtPriceChng Then
                    'May 11 fix Infinite loop issue
                    'strComments = "SKU RETAIL CHANGED FROM $" & oldValue & " TO $" & newValue & " BY " & empCd & lineText & "(DISCOUNT) "
                    strComments = "SKU DISCOUNT FROM $" & oldValue & " TO $" & newValue & " BY " & empCd & lineText
                End If
            Case Else
                Exit Sub
        End Select

        Dim lngComments As Integer = Len(strComments)
        If lngComments > 0 Then

            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim strSO_SEQ_NUM As String
            Dim strMidCmnts As String
            Dim lngSEQNUM As Integer = 1
            Dim count As Integer = 1

            sql = "Select MAX(SEQ#), so_seq_num from SO_CMNT where DEL_DOC_NUM = '" & delDocNum & "' GROUP BY SO_SEQ_NUM"
            dbAtomicCommand.Parameters.Clear()
            dbAtomicCommand.CommandText = sql
            dbAtomicCommand.CommandType = CommandType.Text
            dbReader = DisposablesManager.BuildOracleDataReader(dbAtomicCommand)
            If dbReader.Read() AndAlso dbReader.IsDBNull(0) = False Then
                lngSEQNUM = dbReader(0)
                lngSEQNUM = lngSEQNUM + 1
                strSO_SEQ_NUM = dbReader(1)
            End If

            ' TODO this should be passed to avoid extraneous db hits
            If String.IsNullOrEmpty(strSO_SEQ_NUM) Then strSO_SEQ_NUM = GetSoSeqNum(delDocNum)
            Dim so_store_cd = GetSoStore(delDocNum)

            'April 28 prevent infinite loop
            strMidCmnts = Mid(strComments, count, 70)
            strMidCmnts = Replace(strMidCmnts, "'", "")

            If Not strMidCmnts.Trim.Equals(String.Empty) Then
                sql = "insert into SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM, PERM) Values ( " &
                       "TO_DATE('" & FormatDateTime(wrDt, DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & so_store_cd & "','" & strSO_SEQ_NUM & "'," &
                           lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" &
                           strMidCmnts & "','" & delDocNum & "','Y')"
                sql = UCase(sql)
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()
            End If

            ' end April 28

            'Do Until count > lngComments   ' TODO call bt_format_util.split_text to break at spaces
            '    strMidCmnts = Mid(strComments, count, 70)
            '    strMidCmnts = Replace(strMidCmnts, "'", "")
            '    If Not strMidCmnts.Trim.Equals(String.Empty) Then
            '        sql = "insert into SO_CMNT (SO_WR_DT, SO_STORE_CD, SO_SEQ_NUM, SEQ#, DT, TEXT, DEL_DOC_NUM, PERM) Values ( " &
            '            "TO_DATE('" & FormatDateTime(wrDt, DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" & so_store_cd & "','" & strSO_SEQ_NUM & "'," &
            '            lngSEQNUM & ", " & "TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR')" & ",'" &
            '            strMidCmnts & "','" & delDocNum & "','Y')"
            '        sql = UCase(sql)
            '        dbAtomicCommand.CommandText = sql
            '        dbAtomicCommand.ExecuteNonQuery()
            '        count = count + 70
            '        lngSEQNUM = lngSEQNUM + 1
            '    End If
            'Loop

        End If
    End Sub

    Private Shared Function GetSoSeqNum(ByVal del_doc_num As String) As String

        Dim version As String = Check_E1_Version() ' used for so_seq_num setting
        Dim seqNum As String = ""

        'version 11 and 10 can pull from del_doc_num but 12 up may or may not be document - can always get from SO table
        If (Left(version, 2) = "11" OrElse Left(version, 2) = "10") Then
            If del_doc_num.Length > 11 Then

                seqNum = Right(Left(del_doc_num, 11), 4)
            Else
                seqNum = Right(del_doc_num, 4)
            End If
        Else
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim datRdr As OracleDataReader
            Dim sql As String = "SELECT so_seq_num FROM so WHERE del_doc_num = :del_doc_num "
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                conn.Open()

                cmd.Parameters.Add("del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
                cmd.Parameters("del_doc_num").Value = del_doc_num

                datRdr = DisposablesManager.BuildOracleDataReader(cmd)
                If datRdr.Read() Then
                    seqNum = datRdr.Item("SO_SEQ_NUM").ToString
                End If
                datRdr.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            Finally
                conn.Close()
            End Try

        End If
        Return seqNum
    End Function

    Private Shared Function GetSoStore(ByVal del_doc_num As String) As String

        Dim version As String = Check_E1_Version() ' used to get so_store_cd
        Dim seqNum As String = ""

        'version 11 can pull from del_doc_num but 12 up may or may not be document - can always get from SO table
        If (Left(version, 2) = "11" OrElse Left(version, 2) = "10") Then

            If del_doc_num.Length > 11 Then
                seqNum = Mid(Left(del_doc_num, 11), 6, 2)
            Else
                seqNum = Mid(del_doc_num, 6, 2)
            End If
        Else
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim datRdr As OracleDataReader
            Dim sql As String = "SELECT SO_STORE_CD FROM so WHERE del_doc_num = :del_doc_num "
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                conn.Open()

                cmd.Parameters.Add("del_doc_num", OracleType.VarChar).Direction = ParameterDirection.Input
                cmd.Parameters("del_doc_num").Value = del_doc_num

                datRdr = DisposablesManager.BuildOracleDataReader(cmd)
                If datRdr.Read() Then
                    seqNum = datRdr.Item("SO_STORE_CD").ToString
                End If
                datRdr.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            Finally
                conn.Close()
            End Try

        End If
        Return seqNum
    End Function

    Public Shared Function determine_gm(ByVal itm_cd As String, ByVal ret_prc As Double) As Double

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim frt_factor As Double = 0
        Dim repl_cst As Double = 0
        Dim qty As Double = 1
        Dim total_ret_prc As Double = 0
        Dim total_repl_cst As Double = 0
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Try
            sql = "select repl_cst, frt_fac from itm where itm_cd='" & itm_cd & "'"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            If MyDataReader.Read() Then
                If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
            End If
            MyDataReader.Close()
            total_ret_prc = total_ret_prc + (ret_prc * qty)
            total_repl_cst = total_repl_cst + (repl_cst * qty)
        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
        determine_gm = FormatNumber((total_ret_prc - total_repl_cst) / total_ret_prc, 2) * 100

    End Function

    Public Shared Function XDigits(ByVal sNumberString, ByVal nReturnLength)

        Dim x As Integer
        If nReturnLength > Len(sNumberString) Then
            XDigits = "0" & sNumberString
            If nReturnLength - Len(sNumberString) > 1 Then
                For x = 2 To nReturnLength - Len(sNumberString)
                    XDigits = "0" & XDigits
                Next
            End If
        Else
            XDigits = Right(sNumberString, nReturnLength)
        End If
    End Function

    Public Shared Function Create_Relationship_No(ByVal store_cd As String)

        Dim x As Integer
        Dim DateString As String = ""
        Dim REL_NO As String = ""
        Dim GOOD_REL As Boolean
        x = 0

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim sql As String

        'Open Connection 
        conn.Open()

        GOOD_REL = False
        Do While GOOD_REL = False
            x = x + 1
            DateString = XDigits(Today.Month.ToString, 2) & XDigits(Today.Day.ToString, 2) & Right(Today.Year, 1)
            REL_NO = DateString & store_cd & XDigits(x.ToString, 4)

            sql = "SELECT REL_NO FROM RELATIONSHIP WHERE REL_NO='" & REL_NO & "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)
                If (MyDatareader.Read()) Then
                    GOOD_REL = False
                Else
                    GOOD_REL = True
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop
        conn.Close()

        Return REL_NO

    End Function

    Public Shared Function Retrieve_Store_Group_Stores(ByVal store_grp_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim sql, store_cd As String

        store_cd = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select store_cd from store_grp$store where store_grp_cd='" & store_grp_cd & "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Do While MyDatareader.Read
                store_cd = store_cd & "'" & MyDatareader.Item("store_cd").ToString & "',"
            Loop
            MyDatareader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Len(store_cd) > 0 Then
            store_cd = Left(store_cd, Len(store_cd) - 1)
        End If

        Return store_cd

    End Function

    Public Shared Function Skip_Data_Security()

        If SystemUtils.skipDataSecurity(HttpContext.Current.Session("emp_cd")) Then

            Skip_Data_Security = "Y"

        Else
            Skip_Data_Security = "N"
        End If

    End Function

    Public Shared Function Lookup_User_Count()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim sql As String
        Dim UserCount As Integer = 0

        'Open Connection 
        conn.Open()

        sql = "SELECT COUNT(DISTINCT AUDIT_USER) as USER_COUNT FROM AUDIT_LOG WHERE AUDIT_DT > SYSDATE-31 AND AUDIT_COLUMN_ID='LOGIN_SUCCESS'"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)
            If MyDatareader.Read() Then
                If IsNumeric(MyDatareader.Item("USER_COUNT").ToString) Then
                    UserCount = CDbl(MyDatareader.Item("USER_COUNT").ToString)
                End If
            End If
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

        Return UserCount

    End Function

    ' TODO - fix Order_detail so can use related_sku, not this and then remove this
    Public Shared Function Orig_Related_SKU(ByVal SKUS As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim sqlSB As StringBuilder = New StringBuilder
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim itm_string As String = ""

        conn.Open()

        '''''**Changes done for to avoid displaying related code if the itm_CD is prevented from sale**'''''
        sqlSB.Append("SELECT ITM.DES, ITM2RSKU.RELATED_ITM_CD AS rel_sku, ITM.VSN, ITM.RET_PRC FROM ITM")
        sqlSB.Append(" join  ITM2RSKU on ITM2RSKU.RELATED_ITM_CD=ITM.ITM_CD")
        sqlSB.Append(" and ITM2RSKU.ITM_CD  Not in (")
        sqlSB.Append(" select itm_cd from itm i join drop_cd d on i.drop_cd = d.drop_cd ")
        sqlSB.Append(" where i.drop_dt <= sysdate and upper(d.prevent_sale) = 'Y' and i.itm_cd IN (" & SKUS & ") ")
        sqlSB.Append(" )")
        sqlSB.Append(" WHERE ITM2RSKU.RELATED_ITM_CD=ITM.ITM_CD")
        sqlSB.Append(" AND ITM2RSKU.ITM_CD IN (" & SKUS & ") ORDER BY ITM2RSKU.ITM_CD")

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sqlSB.ToString, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            Do While MyDataReader.Read()
                itm_string = itm_string & MyDataReader.Item("REL_SKU").ToString & ","
            Loop
            MyDataReader.Close()
        Catch
            Throw
        End Try

        Dim mnr_string As String = ""
        Dim mjr_string As String = ""

        sql = "SELECT ITM.MNR_CD, INV_MNR.MJR_CD FROM ITM, INV_MNR WHERE ITM.ITM_CD IN (" & SKUS & ") AND INV_MNR.MNR_CD=ITM.MNR_CD GROUP BY INV_MNR.MJR_CD, ITM.MNR_CD"
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            'Store Values in String Variables 
            Do While MyDataReader.Read()
                mnr_string = mnr_string & "'" & MyDataReader.Item("MNR_CD").ToString & "',"
                mjr_string = mjr_string & "'" & MyDataReader.Item("MJR_CD").ToString & "',"
            Loop
            MyDataReader.Close()
        Catch
            Throw
        End Try

        conn.Close()
        sqlSB.Clear()
        If Len(mnr_string) > 3 Then
            sqlSB.Append("SELECT ITM_CD FROM RELATED_SKUS WHERE ITM_CD NOT IN (")
            sqlSB.Append(" select itm_cd from itm i join drop_cd d on i.drop_cd = d.drop_cd where  i.drop_dt <= sysdate and upper(d.prevent_sale) = 'Y'")
            sqlSB.Append(" )AND (REL_TP='MNR' AND REL_CD IN(").Append(Left(mnr_string, Len(mnr_string) - 1)).Append(")")
            sqlSB.Append(" OR REL_TP='MJR' AND REL_CD IN(").Append(Left(mjr_string, Len(mjr_string) - 1)).Append(")) GROUP BY ITM_CD")

            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sqlSB.ToString, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                'Store Values in String Variables 
                Do While MyDataReader.Read()
                    itm_string = itm_string & MyDataReader.Item("ITM_CD").ToString & ","
                Loop
                MyDataReader.Close()
            Catch
                conn.Close()
                Throw
            End Try

            conn.Close()
        End If

        Return itm_string

    End Function

    Public Shared Function Related_SKU(ByVal inpSkus As String) As String

        Dim itm_string As String = ""
        Dim SKUS As String = inpSkus   '" (" + inpSkus + ") "  TODO - unable to implement this everywhere at this time

        Dim sql As New StringBuilder
        sql.Append("SELECT ITM.DES, ITM2RSKU.RELATED_ITM_CD AS rel_sku, ITM.VSN, ITM.RET_PRC FROM ITM, ITM2RSKU ").Append(
            "WHERE ITM2RSKU.RELATED_ITM_CD=ITM.ITM_CD AND ITM2RSKU.ITM_CD IN " & SKUS & " ORDER BY ITM2RSKU.ITM_CD")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
            cmd As New OracleCommand(sql.ToString, conn)

            conn.Open()
            'cmd.Parameters.Add(":storeCd", OracleType.VarChar)
            'cmd.Parameters(":storeCd").Value = UCase(storeCd)

            Dim oraDatRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            Try
                If oraDatRdr.HasRows Then

                    Do While oraDatRdr.Read()

                        itm_string = itm_string & oraDatRdr.Item("REL_SKU").ToString & ","
                    Loop
                End If
                oraDatRdr.Close()
            Catch
                oraDatRdr.Close()
                conn.Close()
                Throw
            End Try

            Dim mnr_string As String = ""
            Dim mjr_string As String = ""

            sql.Clear()
            sql.Append("SELECT ITM.MNR_CD, INV_MNR.MJR_CD FROM ITM, INV_MNR WHERE ITM.ITM_CD IN " & SKUS).Append(
                " AND INV_MNR.MNR_CD=ITM.MNR_CD GROUP BY INV_MNR.MJR_CD, ITM.MNR_CD")

            Try
                cmd.CommandText = sql.ToString
                oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)

                Do While oraDatRdr.Read()

                    mnr_string = mnr_string & "'" & oraDatRdr.Item("MNR_CD").ToString & "',"
                    mjr_string = mjr_string & "'" & oraDatRdr.Item("MJR_CD").ToString & "',"
                Loop
                oraDatRdr.Close()
            Catch
                oraDatRdr.Close()
                conn.Close()
                Throw
            End Try

            If Len(mnr_string) > 3 Then

                sql.Clear()
                sql.Append("SELECT ITM_CD FROM RELATED_SKUS WHERE REL_TP='MNR' AND REL_CD IN (" & Left(mnr_string, Len(mnr_string) - 1) & ") ").Append(
                        "OR REL_TP='MJR' AND REL_CD IN (" & Left(mjr_string, Len(mjr_string) - 1) & ") GROUP BY ITM_CD")

                Try
                    cmd.CommandText = sql.ToString
                    oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)

                    Do While oraDatRdr.Read()

                        itm_string = itm_string & oraDatRdr.Item("ITM_CD").ToString & ","
                    Loop
                    oraDatRdr.Close()
                Catch
                    oraDatRdr.Close()
                    conn.Close()
                    Throw
                End Try
            End If
        End Using

        Return itm_string

    End Function

    ''' <summary>
    ''' Adds 'PEN' records to the INV_XREF table, if the application is setup to update the table and
    ''' also if the SKU is flagged as Non-Bulk
    ''' </summary>
    ''' <param name="delDocNum">the document being processed</param>
    ''' <param name="delDocLn">the line being processed</param>
    ''' <param name="itmCd">the ITEM-CD in the line being processed</param>
    ''' <param name="lnQty">the line quantity, represents the number of records being inserted</param>
    ''' <param name="origDelDoc">the original document number, applies for CRM documents against an existing document</param>
    Public Shared Sub AddPendingInventory(ByVal delDocNum As String, ByVal delDocLn As String, ByVal itmCd As String,
                                          ByVal lnQty As String,
                                          Optional ByVal origDelDoc As String = "")

        ' ----> Update the INV_XREF tables
        If ConfigurationManager.AppSettings("xref") = "Y" Then

            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbSql = "SELECT NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM WHERE ITM_CD = :itmCd"
            Dim isBulk As Boolean
            Try
                dbConnection.Open()
                dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
                dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
                dbCommand.Parameters(":itmCd").Value = itmCd
                isBulk = (dbCommand.ExecuteScalar().ToString() = "Y")
            Catch ex As Exception
                dbCommand.Dispose()
                dbConnection.Close()
                Throw
            End Try

            If (Not isBulk) Then

                Dim count As Integer = 0
                Dim lineQty As Integer = CInt(lnQty)
                Try
                    If (Not String.IsNullOrEmpty(origDelDoc)) Then

                        Dim dbAdp As OracleDataAdapter
                        Dim ds As DataSet = New DataSet

                        dbSql = "SELECT DEL_DOC_NUM, RF_ID_CD FROM INV_XREF_ARC WHERE INV_XREF_ARC.DEL_DOC_NUM = :origDelDoc AND INV_XREF_ARC.ITM_CD = :itmCd"
                        dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
                        dbCommand.Parameters.Add(":origDelDoc", OracleType.VarChar)
                        dbCommand.Parameters(":origDelDoc").Value = origDelDoc
                        dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
                        dbCommand.Parameters(":itmCd").Value = itmCd
                        dbAdp = DisposablesManager.BuildOracleDataAdapter(dbCommand)
                        dbAdp.Fill(ds, "INV_XREF_ARC_DATA")

                        'start a single transaction to save updates to INV_XREF and INV_XREF_ARC together
                        dbCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
                        dbCommand.Transaction = dbConnection.BeginTransaction()
                        For Each dr As DataRow In ds.Tables(0).Rows

                            'needs to copy only as many rows as needed
                            If (count >= lineQty) Then
                                Exit For
                            Else
                                dbSql = "INSERT INTO INV_XREF ("
                                dbSql = dbSql & "RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, "
                                dbSql = dbSql & "IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, "
                                dbSql = dbSql & "LAST_ACTN_EMP_CD, ACTIVITY, DISPOSITION, QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM) "

                                dbSql = dbSql & "SELECT RF_ID_CD, NULL, NULL, ITM_CD, :NEW_DEL_DOC_NUM, :NEW_DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, "
                                dbSql = dbSql & "IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
                                dbSql = dbSql & "TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), LAST_CYCLE_DT, "
                                dbSql = dbSql & ":LAST_ACTN_EMP_CD, ACTIVITY, 'PEN', QTY, 'POS', REPLACED_BY, ORIG_STATUS_CD, SER_NUM "
                                dbSql = dbSql & "FROM INV_XREF_ARC WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND RF_ID_CD=:RF_ID_CD"

                                dbCommand.CommandText = dbSql
                                dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)
                                dbCommand.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
                                dbCommand.Parameters.Add(":NEW_DEL_DOC_LN#", OracleType.Number)
                                dbCommand.Parameters.Add(":LAST_ACTN_EMP_CD", OracleType.VarChar)

                                dbCommand.Parameters(":DEL_DOC_NUM").Value = dr("DEL_DOC_NUM")
                                dbCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")
                                dbCommand.Parameters(":NEW_DEL_DOC_NUM").Value = delDocNum
                                dbCommand.Parameters(":NEW_DEL_DOC_LN#").Value = delDocLn
                                dbCommand.Parameters(":LAST_ACTN_EMP_CD").Value = HttpContext.Current.Session("emp_cd")
                                dbCommand.ExecuteNonQuery()
                                dbCommand.Parameters.Clear()

                                dbSql = "DELETE FROM INV_XREF_ARC WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND RF_ID_CD=:RF_ID_CD"

                                dbCommand.CommandText = dbSql
                                dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)

                                dbCommand.Parameters(":DEL_DOC_NUM").Value = dr("DEL_DOC_NUM")
                                dbCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")
                                dbCommand.ExecuteNonQuery()
                                dbCommand.Parameters.Clear()

                                count = count + 1  'to account for the row just added
                            End If
                        Next
                        dbCommand.Transaction.Commit()
                    End If

                    'adds the missing records. This happens either because there weren't enough records in the
                    'INV_XREF_ARC table or because the CRM line doesn't reference an original document
                    Do While (count < lnQty)
                        dbSql = "INSERT INTO INV_XREF ("
                        dbSql = dbSql & "RF_ID_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, CREATE_DT, LAST_ACTN_DT, "
                        dbSql = dbSql & "LAST_ACTN_EMP_CD, DISPOSITION, QTY, ORIGIN_CD) "
                        dbSql = dbSql & "VALUES(:RF_ID_CD, :ITM_CD, :DEL_DOC_NUM, :DEL_DOC_LN, TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
                        dbSql = dbSql & "TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), :LAST_ACTN_EMP_CD, 'PEN', 1, 'POS') "
                        dbCommand = DisposablesManager.BuildOracleCommand(dbSql, dbConnection)

                        dbCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)
                        dbCommand.Parameters.Add(":ITM_CD", OracleType.VarChar)
                        dbCommand.Parameters.Add(":LAST_ACTN_EMP_CD", OracleType.VarChar)
                        dbCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                        dbCommand.Parameters.Add(":DEL_DOC_LN", OracleType.Number)

                        dbCommand.Parameters(":RF_ID_CD").Value = getRFID()
                        dbCommand.Parameters(":ITM_CD").Value = itmCd
                        dbCommand.Parameters(":LAST_ACTN_EMP_CD").Value = HttpContext.Current.Session("emp_cd")
                        dbCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
                        dbCommand.Parameters(":DEL_DOC_LN").Value = delDocLn

                        dbCommand.ExecuteNonQuery()
                        dbCommand.Parameters.Clear()

                        count = count + 1  'to account for the row just added
                    Loop
                Catch ex As Exception
                    If (Not IsNothing(dbCommand.Transaction)) Then dbCommand.Transaction.Rollback()
                    Throw
                Finally
                    dbCommand.Dispose()
                    dbConnection.Close()
                End Try
            End If
        End If
    End Sub
    Private Shared Function IsBulkItem(ByVal itmCd As String) As Boolean

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = "SELECT NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM WHERE ITM_CD = :itmCd"
        Dim isBulk As Boolean
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd
            isBulk = (dbCommand.ExecuteScalar().ToString() = "Y")
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return isBulk
    End Function
    ''' <summary>
    ''' Adds 'PEN' records to the INV_XREF table, if the application is setup to update the table and
    ''' also if the SKU is flagged as Non-Bulk
    ''' </summary>
    ''' <param name="delDocNum">the document being processed</param>
    ''' <param name="delDocLn">the line being processed</param>
    ''' <param name="itmCd">the ITEM-CD in the line being processed</param>
    ''' <param name="lnQty">the line quantity, represents the number of records being inserted</param>
    ''' <param name="origDelDoc">the original document number, applies for CRM documents against an existing document</param>
    Public Shared Sub AddPendingInventory(ByRef dbAtomicCommand As OracleCommand, ByVal delDocNum As String, ByVal delDocLn As String, ByVal itmCd As String, ByVal lnQty As String, Optional ByVal origDelDoc As String = "")

        ' ----> Update the INV_XREF tables
        If ConfigurationManager.AppSettings("xref") = "Y" Then
            Dim dbSql As StringBuilder = New StringBuilder()

            If (Not IsBulkItem(itmCd)) Then
                Dim count As Integer = 0
                Dim lineQty As Integer = CInt(lnQty)
                Try
                    If (Not String.IsNullOrEmpty(origDelDoc)) Then

                        Dim dbAdp As OracleDataAdapter
                        Dim ds As DataSet = New DataSet

                        dbSql.Clear()
                        dbSql.Append("SELECT DEL_DOC_NUM, RF_ID_CD FROM INV_XREF_ARC WHERE ")
                        dbSql.Append("INV_XREF_ARC.DEL_DOC_NUM='" & origDelDoc & "' ")
                        dbSql.Append("AND INV_XREF_ARC.ITM_CD='" & itmCd & "' ")

                        dbAtomicCommand.CommandText = dbSql.ToString()
                        dbAdp = DisposablesManager.BuildOracleDataAdapter(dbAtomicCommand)
                        dbAdp.Fill(ds, "INV_XREF_ARC_DATA")

                        For Each dr As DataRow In ds.Tables(0).Rows

                            'needs to copy only as many rows as needed
                            If (count >= lineQty) Then
                                Exit For
                            Else
                                dbSql.Clear()
                                dbSql.Append("INSERT INTO INV_XREF (")
                                dbSql.Append("RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ")
                                dbSql.Append("IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, ")
                                dbSql.Append("LAST_ACTN_EMP_CD, ACTIVITY, DISPOSITION, QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM) ")

                                dbSql.Append("SELECT RF_ID_CD, NULL, NULL, ITM_CD, :NEW_DEL_DOC_NUM, :NEW_DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ")
                                dbSql.Append("IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), ")
                                dbSql.Append("TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), LAST_CYCLE_DT, ")
                                dbSql.Append(":LAST_ACTN_EMP_CD, ACTIVITY, 'PEN', QTY, 'POS', REPLACED_BY, ORIG_STATUS_CD, SER_NUM ")
                                dbSql.Append("FROM INV_XREF_ARC WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND RF_ID_CD=:RF_ID_CD")

                                dbAtomicCommand.CommandText = dbSql.ToString()
                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":NEW_DEL_DOC_LN#", OracleType.Number)
                                dbAtomicCommand.Parameters.Add(":LAST_ACTN_EMP_CD", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = dr("DEL_DOC_NUM")
                                dbAtomicCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")
                                dbAtomicCommand.Parameters(":NEW_DEL_DOC_NUM").Value = delDocNum
                                dbAtomicCommand.Parameters(":NEW_DEL_DOC_LN#").Value = delDocLn
                                dbAtomicCommand.Parameters(":LAST_ACTN_EMP_CD").Value = HttpContext.Current.Session("emp_cd")

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()

                                dbSql.Clear()
                                dbSql.Append("DELETE FROM INV_XREF_ARC WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND RF_ID_CD=:RF_ID_CD")
                                dbAtomicCommand.CommandText = dbSql.ToString()

                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = dr("DEL_DOC_NUM")
                                dbAtomicCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()

                                count = count + 1  'to account for the row just added
                            End If
                        Next
                    End If

                    'Used in case of Non-refrenced CRM scenario
                    If count = 0 Then
                        'adds the missing records. This happens either because there weren't enough records in the
                        'INV_XREF_ARC table or because the CRM line doesn't reference an original document
                        Do While (count < CInt(lnQty))
                            dbSql.Clear()
                            dbSql.Append("INSERT INTO INV_XREF (")
                            dbSql.Append("RF_ID_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, CREATE_DT, LAST_ACTN_DT, ")
                            dbSql.Append("LAST_ACTN_EMP_CD, DISPOSITION, QTY, ORIGIN_CD) ")
                            dbSql.Append("VALUES(:RF_ID_CD, :ITM_CD, :DEL_DOC_NUM, :DEL_DOC_LN, TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), ")
                            dbSql.Append("TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), :LAST_ACTN_EMP_CD, 'PEN', 1, 'POS') ")

                            dbAtomicCommand.CommandText = UCase(dbSql.ToString())
                            dbAtomicCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)
                            dbAtomicCommand.Parameters.Add(":ITM_CD", OracleType.VarChar)
                            dbAtomicCommand.Parameters.Add(":LAST_ACTN_EMP_CD", OracleType.VarChar)
                            dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                            dbAtomicCommand.Parameters.Add(":DEL_DOC_LN", OracleType.Number)
                            dbAtomicCommand.Parameters(":RF_ID_CD").Value = getRFID()
                            dbAtomicCommand.Parameters(":ITM_CD").Value = itmCd
                            dbAtomicCommand.Parameters(":LAST_ACTN_EMP_CD").Value = HttpContext.Current.Session("emp_cd")
                            dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
                            dbAtomicCommand.Parameters(":DEL_DOC_LN").Value = delDocLn

                            dbAtomicCommand.ExecuteNonQuery()
                            dbAtomicCommand.Parameters.Clear()

                            count = count + 1  'to account for the row just added
                        Loop
                    End If
                Catch ex As Exception
                    Throw
                Finally

                End Try
            End If
        End If
    End Sub

    Public Shared Sub SubtractInventory(ByRef dbAtomicCommand As OracleCommand, ByVal delDocNum As String, ByVal delDocLn As String, ByVal itmCd As String, ByVal lnQty As String, Optional ByVal origDelDoc As String = "")

        ' ----> Update the INV_XREF tables
        If ConfigurationManager.AppSettings("xref") = "Y" Then
            Dim dbSql As StringBuilder = New StringBuilder()
            Dim OriginalStoreCode As String = String.Empty
            Dim OriginalLocationCode As String = String.Empty

            If (Not IsBulkItem(itmCd)) Then

                Dim count As Integer = 0
                Dim lineQty As Integer = CInt(lnQty)
                Try
                    If (Not String.IsNullOrEmpty(origDelDoc)) Then

                        Dim dbAdp As OracleDataAdapter
                        Dim ds As DataSet = New DataSet
                        dbSql.Clear()

                        dbSql.Append("SELECT RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ")
                        dbSql.Append("IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, ")
                        dbSql.Append("LAST_ACTN_EMP_CD, ACTIVITY, DISPOSITION, QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM, OOC ")
                        dbSql.Append("FROM INV_XREF WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN#")

                        dbAtomicCommand.CommandText = dbSql.ToString()
                        dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                        dbAtomicCommand.Parameters.Add(":DEL_DOC_LN#", OracleType.VarChar)
                        dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
                        dbAtomicCommand.Parameters(":DEL_DOC_LN#").Value = delDocLn

                        dbAdp = DisposablesManager.BuildOracleDataAdapter(dbAtomicCommand)
                        dbAdp.Fill(ds, "INV_XREF_DATA")

                        For Each dr As DataRow In ds.Tables(0).Rows
                            'needs to copy only as many rows as needed
                            If (count >= lineQty) Then
                                Exit For
                            Else

                                dbSql.Clear()
                                dbAtomicCommand.Parameters.Clear()
                                dbSql.Append("SELECT STORE_CD, LOC_CD FROM SO_ln WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN# AND ITM_CD = :ITM_CD")
                                dbAtomicCommand.CommandText = dbSql.ToString()
                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":DEL_DOC_LN#", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":ITM_CD", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = origDelDoc
                                dbAtomicCommand.Parameters(":DEL_DOC_LN#").Value = delDocLn
                                dbAtomicCommand.Parameters(":ITM_CD").Value = dr("ITM_CD")
                                Dim reader As OracleDataReader = DisposablesManager.BuildOracleDataReader(dbAtomicCommand, CommandBehavior.SingleRow)
                                If (reader.HasRows) Then
                                    reader.Read()
                                    OriginalStoreCode = reader("STORE_CD")
                                    OriginalLocationCode = reader("LOC_CD")
                                End If

                                dbSql.Clear()
                                dbSql.Append("INSERT INTO INV_XREF_ARC (")
                                dbSql.Append("RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, ")
                                dbSql.Append("IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, ")
                                dbSql.Append("LAST_ACTN_EMP_CD, ACTIVITY, DISPOSITION, QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM, OOC) ")
                                dbSql.Append(" VALUES( :RF_ID_CD, :STORE_CD, :LOC_CD, :ITM_CD, :DEL_DOC_NUM, :DEL_DOC_LN#, :SE_PART_SEQ, :IST_WR_DT, :IST_STORE_CD, ")
                                dbSql.Append(":IST_SEQ_NUM, :IST_LN#, :NAS_OUT_ID, :NAS_OUT_CD, :CRPT_ID_NUM,  TO_DATE(:CREATE_DT,'mm/dd/RRRR'), TO_DATE(:LAST_ACTN_DT,'mm/dd/RRRR'),:LAST_CYCLE_DT, ")
                                dbSql.Append(":LAST_ACTN_EMP_CD, :ACTIVITY, :DISPOSITION, :QTY, :ORIGIN_CD, :REPLACED_BY, :ORIG_STATUS_CD, :SER_NUM, :OOC) ")

                                dbAtomicCommand.CommandText = dbSql.ToString()
                                dbAtomicCommand.Parameters.Add(":RF_ID_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":STORE_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":LOC_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":ITM_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":DEL_DOC_LN#", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":SE_PART_SEQ", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":IST_WR_DT", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":IST_STORE_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":IST_SEQ_NUM", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":IST_LN#", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":NAS_OUT_ID", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":NAS_OUT_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":CRPT_ID_NUM", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":CREATE_DT", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":LAST_ACTN_DT", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":LAST_CYCLE_DT", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":LAST_ACTN_EMP_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":ACTIVITY", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":DISPOSITION", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":QTY", OracleType.Number)
                                dbAtomicCommand.Parameters.Add(":ORIGIN_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":REPLACED_BY", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":ORIG_STATUS_CD", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":SER_NUM", OracleType.NVarChar)
                                dbAtomicCommand.Parameters.Add(":OOC", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")
                                dbAtomicCommand.Parameters(":STORE_CD").Value = OriginalStoreCode
                                dbAtomicCommand.Parameters(":LOC_CD").Value = OriginalLocationCode
                                dbAtomicCommand.Parameters(":ITM_CD").Value = dr("ITM_CD")
                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = origDelDoc
                                dbAtomicCommand.Parameters(":DEL_DOC_LN#").Value = delDocLn
                                dbAtomicCommand.Parameters(":SE_PART_SEQ").Value = dr("SE_PART_SEQ")
                                dbAtomicCommand.Parameters(":IST_WR_DT").Value = dr("IST_WR_DT")
                                dbAtomicCommand.Parameters(":IST_STORE_CD").Value = dr("IST_STORE_CD")
                                dbAtomicCommand.Parameters(":IST_SEQ_NUM").Value = dr("IST_SEQ_NUM")
                                dbAtomicCommand.Parameters(":IST_LN#").Value = dr("IST_LN#")
                                dbAtomicCommand.Parameters(":NAS_OUT_ID").Value = dr("NAS_OUT_ID")
                                dbAtomicCommand.Parameters(":NAS_OUT_CD").Value = dr("NAS_OUT_CD")
                                dbAtomicCommand.Parameters(":CRPT_ID_NUM").Value = dr("CRPT_ID_NUM")
                                dbAtomicCommand.Parameters(":CREATE_DT").Value = FormatDateTime(dr("CREATE_DT"), DateFormat.ShortDate)
                                dbAtomicCommand.Parameters(":LAST_ACTN_DT").Value = FormatDateTime(dr("LAST_ACTN_DT"), DateFormat.ShortDate)
                                dbAtomicCommand.Parameters(":LAST_CYCLE_DT").Value = dr("LAST_CYCLE_DT")
                                dbAtomicCommand.Parameters(":LAST_ACTN_EMP_CD").Value = HttpContext.Current.Session("emp_cd")
                                dbAtomicCommand.Parameters(":ACTIVITY").Value = dr("ACTIVITY")
                                dbAtomicCommand.Parameters(":DISPOSITION").Value = "RES"
                                dbAtomicCommand.Parameters(":QTY").Value = dr("QTY")
                                dbAtomicCommand.Parameters(":ORIGIN_CD").Value = dr("ORIGIN_CD")
                                dbAtomicCommand.Parameters(":REPLACED_BY").Value = dr("REPLACED_BY")
                                dbAtomicCommand.Parameters(":ORIG_STATUS_CD").Value = dr("ORIG_STATUS_CD")
                                dbAtomicCommand.Parameters(":SER_NUM").Value = dr("SER_NUM")
                                dbAtomicCommand.Parameters(":OOC").Value = dr("OOC")

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()

                                dbSql.Clear()
                                dbSql.Append("DELETE FROM INV_XREF WHERE DEL_DOC_NUM=:DEL_DOC_NUM AND RF_ID_CD=:RF_ID_CD")
                                dbAtomicCommand.CommandText = dbSql.ToString()

                                dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                dbAtomicCommand.Parameters.Add(":RF_ID_CD", OracleType.VarChar)

                                dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = dr("DEL_DOC_NUM")
                                dbAtomicCommand.Parameters(":RF_ID_CD").Value = dr("RF_ID_CD")

                                dbAtomicCommand.ExecuteNonQuery()
                                dbAtomicCommand.Parameters.Clear()

                                count = count + 1  'to account for the row just added
                            End If
                        Next
                    Else
                        dbSql.Clear()
                        dbSql.Append("DELETE FROM INV_XREF WHERE DEL_DOC_NUM=:DEL_DOC_NUM and DEL_DOC_LN# =:DEL_DOC_LN# and rownum <=:rcount")
                        dbAtomicCommand.CommandText = dbSql.ToString()

                        dbAtomicCommand.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                        dbAtomicCommand.Parameters.Add(":DEL_DOC_LN#", OracleType.VarChar)
                        dbAtomicCommand.Parameters.Add(":rcount", OracleType.Number)

                        dbAtomicCommand.Parameters(":DEL_DOC_NUM").Value = delDocNum
                        dbAtomicCommand.Parameters(":DEL_DOC_LN#").Value = delDocLn
                        dbAtomicCommand.Parameters(":rcount").Value = lnQty

                        dbAtomicCommand.ExecuteNonQuery()

                    End If
                Catch ex As Exception
                    Throw
                Finally
                End Try
            End If
        End If
    End Sub

    Public Shared Function PO_Creation(ByVal del_doc As String)

        Dim GOOD_REL As Boolean
        Dim CHAR_SEQ As String = ""

        GOOD_REL = False
        PO_Creation = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim x As Integer = 64
        Dim sql As String
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn2.Open()
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()

        Do While GOOD_REL = False
            x = x + 1

            PO_Creation = del_doc & Chr(x)

            sql = "SELECT PO_CD FROM PO WHERE PO_CD='" & PO_Creation & "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)
                If (MyDatareader.Read()) Then
                    GOOD_REL = False
                Else
                    GOOD_REL = True
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop

        conn.Close()
        conn2.Close()

        Return PO_Creation

    End Function

    Private Shared Function getRFID() As String

        Dim returnVal As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        Dim sql As String = "INV.SP_GEN_RF_ID"
        Dim cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("C_RF_ID_CD", OracleType.VarChar, 50).Direction = ParameterDirection.InputOutput

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            returnVal = cmd.Parameters("C_RF_ID_CD").Value
        Catch ex As Exception
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnVal
    End Function


    ''' <summary>
    ''' Retrieves the cashier activation details
    ''' </summary>
    ''' <param name="cashierId">the ID of the cashier for which to get activation details </param>
    ''' <returns>a DataSet consisting of details such as the company code, cash drawer cdoe, etc. for the specified cashier</returns>
    Public Shared Function GetCashierActivationDetails(ByVal cashierId As String) As DataSet

        Dim returnVal As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String

        'retrieves the cashier activation details
        sql = "SELECT co_cd, csh_dwr_cd, post_dt, store_cd " &
                    " FROM CSHR_ACTIVATION " &
                    "WHERE  cshr_init ='" & cashierId

        Try
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "CSHA_DETAILS")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function


    ''' <summary>
    '''Retrieves the first cash drawer  setup for the store passed-in
    ''' </summary>
    ''' <param name="storeCd">the store code to get the cash drawer for</param>
    ''' <returns>first cash drawer  setup for the store passed-in</returns>
    Public Shared Function GetCashDrawerCd(ByVal storeCd As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim cashDrawerCd As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        'retrieves the first cash drawer setup for the specified store
        sql = "SELECT csh_dwr_cd FROM csh_dwr " &
                    "WHERE STORE_CD='" & storeCd &
                     "' ORDER BY csh_dwr_cd"

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim rdr As OracleDataReader

        Try
            'Execute DataReader 
            rdr = DisposablesManager.BuildOracleDataReader(cmd)
            If rdr.Read() Then
                cashDrawerCd = rdr.Item("CSH_DWR_CD").ToString
            End If
            rdr.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return cashDrawerCd
    End Function

    'NOTE: this method exists in the InventoryBiz class, use that if possible
    '***** DO NOT USE ***** This will be removed eventually
    Private Shared Function LocType(ByVal storeCd As String, locCd As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim LocTp As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        'retrieves the first cash drawer setup for the specified store
        sql = "SELECT loc_tp_cd FROM loc " &
                    "WHERE STORE_CD='" & storeCd &
                     "' and LOC_CD='" & locCd & "'"

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim rdr As OracleDataReader

        Try
            'Execute DataReader 
            rdr = DisposablesManager.BuildOracleDataReader(cmd)
            If rdr.Read() Then
                LocTp = rdr.Item("LOC_TP_CD").ToString
            End If
            rdr.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return LocTp

    End Function

    ''' <summary>
    ''' Retrieves the details such as processor id, terminal id, etc.  for the auth provider passed-in 
    ''' and the specified store and cash drawer
    ''' </summary>
    ''' <param name="providerId">the finance provider for which to get details for</param>
    ''' <param name="storeCd">the store code of the current auth transaction</param>
    ''' <param name="cashDrawerCd">the cash drawer where the auth transaction is occuring</param>
    ''' <returns>a Dataset consisting of the finance terminal id and processor id</returns>
    Public Shared Function GetAuthDetails(ByVal providerId As String, ByVal storeCd As String, ByVal cashDrawerCd As String) As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String =
            "SELECT  terminal_id, processor_id" &
                       " FROM CSH_DWR2ASP_TERM_ID" &
                       " WHERE as_cd = '" & providerId & "' " &
                            " AND store_cd = '" & storeCd & "' " &
                            " AND csh_dwr_cd = '" & cashDrawerCd & "' " &
                            " AND processor_id = " &
                                "(SELECT processor_id_eca FROM ASP where AS_CD = '" & providerId & "') "

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "AUTH_DETAILS")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function


    ''' <summary>
    ''' 
    ''' Retrieves the the details such as terminal id, transport  for the finance auth provider and 
    ''' the store passed-in. This is necessary in order to issue any electronic authorization for 
    ''' this finance provider.
    ''' </summary>
    ''' <param name="financeCoCd">the finance provider for which to get terminal id for.</param>
    ''' <param name="storeCd">the store code of the current auth transaction</param>
    ''' <returns>a Dataset consisting of the auth details corresponding to the finance company that will be 
    '''          used for electronic authorization.
    Public Shared Function GetFinanceAuthDetails(ByVal financeCoCd As String, ByVal storeCd As String) As DataSet

        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim sql As New StringBuilder
        Dim oAdp As OracleDataAdapter
        conn.Open()

        sql.Append(" SELECT A.TERMINAL_ID, c.TRANSPORT ")
        sql.Append("   FROM CSH_DWR2ASP_TERM_ID a, ASP2STORE b, PAYMENT_XREF c ")
        sql.Append("  WHERE     A.AS_CD = b.AS_CD ")
        sql.Append("        AND A.MERCHANT_KEY = B.MERCHANT_ID ")
        sql.Append("        AND A.TERMINAL_ID = C.MERCHANT_ID ")
        sql.Append("        AND A.STORE_CD = C.STORE_CD ")
        sql.Append("        AND b.AS_CD = :AS_CD ")
        sql.Append("        AND b.STORE_CD = :STORE_CD ")
        sql.Append("        AND ROWNUM = 1 ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":AS_CD", OracleType.VarChar)
        cmd.Parameters(":AS_CD").Value = financeCoCd
        cmd.Parameters.Add(":STORE_CD", OracleType.VarChar)
        cmd.Parameters(":STORE_CD").Value = storeCd

        Try
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "FI_AUTH_DETAILS")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds

    End Function

    ''' <summary>
    ''' Retrieves the reponse code details for an approved auth for the auth provider passed-in 
    ''' </summary>
    ''' <param name="providerId">the finance provider for which to get details for</param>
    ''' <returns>a Dataset consisting of the response code and response type code</returns>
    Public Shared Function GetAspResponseCodeDetails(ByVal providerId As String) As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String =
            "SELECT  resp_cd, resp_tp_cd" &
                       " FROM ASP_RESP" &
                       " WHERE as_cd = '" & providerId & "' " &
                            " AND resp_tp_cd = 'A' " &
                            " AND rownum = 1"

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "ASP_RESP_CD")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Saves the asp response code details for the passed-in order and finance provider to the SO_ASP table. This
    ''' is done when an approval is receieved on a successful finance authorization. 
    ''' </summary>
    ''' <param name="dbCommand">the database command to be used</param>
    ''' <param name="providerId">the finance provider used for authorization</param>
    ''' <param name="delDocNum">the order number</param>
    Public Shared Sub SaveAspResponseCodeInfo(ByRef dbCommand As OracleCommand, ByVal providerId As String, ByVal delDocNum As String)

        Dim responseCd, responseTypeCd As String
        Dim sql As String

        ' Check if the provider code is not null. This happens when it is 'DP' type FI pmt, that is not flagged as a true 'FI' in the SO table.
        ' Therefore, the 'fin_cust_cd' which is the FI/Provider id is null, and that value is a not-null col. in the so_asp response insert.
        If (delDocNum.isNotEmpty AndAlso providerId.isNotEmpty) Then
            'retrieve the auth provider response code info from the db
            Dim ds As DataSet = GetAspResponseCodeDetails(providerId)
            For Each dr In ds.Tables(0).Rows
                responseCd = dr("resp_cd")
                responseTypeCd = dr("resp_tp_cd")
            Next
            responseCd = "MANUAL"   'lucy
            sql = "INSERT INTO SO_ASP_RESP  (" &
                            " DEL_DOC_NUM, AS_CD, RESP_CD,  RESP_TP_CD,  RESP_DT_TIME )" &
                            " VALUES('" & delDocNum & "',  '" & providerId & "', '" & responseCd & "',  '" & responseTypeCd & "',   sysdate" & ")"

            dbCommand.CommandText = sql
            dbCommand.ExecuteNonQuery()

        End If
    End Sub

    ''' <summary>
    ''' Saves the asp response code details for the passed-in order and finance provider to the SO_ASP table. This
    ''' is done when an approval is receieved on a successful finance authorization. 
    ''' </summary>
    ''' <param name="providerId">the finance provider used for authorization</param>
    ''' <param name="delDocNum">the order number</param>
    ''' <remarks>NOTE: NO CHANGES SHOULD BE MADE TO THIS METHOD, INSTEAD TO  
    '''          SaveAspResponseCodeInfo(dbCommand, providerId, delDocNum) </remarks>
    Public Shared Sub SaveAspResponseCodeInfo(ByVal providerId As String, ByVal delDocNum As String)

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand("", dbConnection)

            SaveAspResponseCodeInfo(dbCommand, providerId, delDocNum)

        Catch ex As Exception
            Throw
        Finally
            dbConnection.Close()
        End Try

    End Sub

    Public Shared Function Use_New_Logic(ByVal process As Integer)

        Dim version As String = Check_E1_Version()  ' in Use_New_Logic
        Dim use_new As Boolean = True
        Dim versNum As Integer = Make_Numeric_Version(version)

        ' if version is less than that set for the process, cannot use new logic
        If versNum < process Then

            use_new = False
        End If

        Return use_new

    End Function

    Private Shared Function Make_Numeric_Version(ByVal version As String) As Integer
        ' convert the version string into a numeric assuming max 4 separate entries as in 12.1.3.5 or 11.1.9.0 to compare to constants

        Dim versNum As Integer = 0
        Dim multiplier As Integer = 100    ' times 100 to allow for two digit entries

        Dim strArr() As String = version.Split(".")
        Dim strCnt As Integer = strArr.Length
        Dim position As Integer = 0

        Try
            For i As Integer = 0 To strArr.Length - 1

                versNum = versNum * multiplier
                ' unfortunately, the dev environments have 'bt' in them so have to remove it to test in dev
                position = InStr(1, strArr(i).ToLower, "bt", 1)   ' using text comparison
                If position > 0 Then
                    strArr(i) = strArr(i).Substring(0, position - 1)  ' bt should be on the end so should not need more - change as needed
                End If
                versNum = versNum + CInt(strArr(i))

            Next

        Catch ex As Exception
            Throw ex

        End Try

        ' need to multiply out for 4 positions as if there were 4 entries 
        For i As Integer = strArr.Length To 3
            versNum = versNum * multiplier
        Next

        Return versNum

    End Function

    Public Shared Sub Finalize_Inv_Xref(ByVal del_doc_num As String, ByVal del_doc_ln As Integer, ByVal ser_num As String, ByRef cmd As OracleCommand, Optional ByVal disposition As String = "")
        '  moves the inv_xref to inv_xref_arc if rf enabled  

        Dim dbSql As String
        cmd.Parameters.Clear()
        cmd.CommandType = CommandType.Text

        ' ----> Update the INV_XREF tables if RF enabled
        If SysPms.isRFEnabled Then    ' For serialized, inv_xref sb removed but no record to inv_xref_arc
            Dim disp As String = "DISPOSITION"

            If Not String.IsNullOrEmpty(disposition) Then
                disp = ":DISPOSITION"
                cmd.Parameters.Add(":DISPOSITION", OracleType.VarChar)
                cmd.Parameters(":DISPOSITION").Value = disposition
            End If

            dbSql = "INSERT INTO INV_XREF_ARC (" +
                     "RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, " +
                     "IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, " +
                     "LAST_ACTN_EMP_CD, ACTIVITY, DISPOSITION, QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM, OOC) " +
                 "SELECT RF_ID_CD, STORE_CD, LOC_CD, ITM_CD, DEL_DOC_NUM, DEL_DOC_LN#, SE_PART_SEQ, IST_WR_DT, IST_STORE_CD, " +
                     "IST_SEQ_NUM, IST_LN#, NAS_OUT_ID, NAS_OUT_CD, CRPT_ID_NUM, CREATE_DT, LAST_ACTN_DT, LAST_CYCLE_DT, " +
                     "LAST_ACTN_EMP_CD, ACTIVITY, " & disp & ", QTY, ORIGIN_CD, REPLACED_BY, ORIG_STATUS_CD, SER_NUM, OOC " +
                     "FROM INV_XREF WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN#"

            Try
                cmd.CommandText = dbSql
                cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                cmd.Parameters.Add(":DEL_DOC_LN#", OracleType.Number)

                cmd.Parameters(":DEL_DOC_NUM").Value = del_doc_num
                cmd.Parameters(":DEL_DOC_LN#").Value = del_doc_ln

                cmd.ExecuteNonQuery()

                If Not String.IsNullOrEmpty(disposition) Then cmd.Parameters.Remove(cmd.Parameters(":DISPOSITION"))

                dbSql = "DELETE FROM INV_XREF WHERE DEL_DOC_NUM = :DEL_DOC_NUM AND DEL_DOC_LN# = :DEL_DOC_LN#"
                cmd.CommandText = dbSql
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
            Catch ex As Exception
                Throw
            End Try

            ' TODO - what happens when have reserved, no specific serial num, then enter on finalization - does inventory get set correctly  (how about qty decrease - non-serial)
        ElseIf SysPms.isSerialEnabled Then ' if RF not enabled, then inv_xref may not have del_doc/ln

            dbSql = "DELETE FROM INV_XREF WHERE SER_NUM = :ser_num "
            Try
                cmd.CommandText = dbSql
                cmd.Parameters.Add(":ser_num", OracleType.VarChar)
                cmd.Parameters(":ser_num").Value = ser_num

                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    Public Class Connection_Constants

        ' NOTE THAT These CONSTANTS HAVE BEEN DEPRECATED - PLEASE CALL SystemUtils version
        ' will do over time as routines are retested

        Public Const CONN_ERP As String = "ERP"

    End Class

    Public Shared Function GetConn(ByVal conn_tp As String) As OracleConnection
        ' returns a connection

        ' NOTE THAT THIS ROUTINE HAS BEEN DEPRECATED - PLEASE CALL SystemUtils version

        Return SystemUtils.GetConn(conn_tp)

    End Function

    Public Shared Function IfNull(value As Object, Optional nullValue As Object = "") As Object
        ' return empty string if null value for strings
        If IsDBNull(value) Then

            IfNull = nullValue
        Else
            IfNull = value
        End If
    End Function

    Public Shared Sub xferCustLiability(ByVal delDocNum As String, ByVal origDelDocNum As String, ByVal ordTpCd As String,
                           ByVal statCd As String, ByVal finalDt As Date, ByVal storeCd As String, ByVal amt As Double,
                           ByVal custCd As String, ByVal finCustCd As String, ByVal empCdOp As String, ByVal coCd As String,
                           ByVal originCd As String, ByVal arTp As String, ByVal acctNum As String, ByRef cmd As OracleCommand)
        'Calls E1 bt_sales_util.xfer_cust_liability to transfer finance liability to finance company cust cd


        cmd.CommandText = "bt_sales_util.xfer_cust_liability"
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("orig_del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("ord_tp_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("stat_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("trn_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
        cmd.Parameters.Add("so_store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("amt_i", OracleType.Double).Direction = ParameterDirection.Input
        cmd.Parameters.Add("xfer_from_cust_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("xfer_to_finco_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("emp_cd_op_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("co_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("origin_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("ar_tp_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add("bnk_crd_num_i", OracleType.VarChar).Direction = ParameterDirection.Input

        cmd.Parameters("del_doc_num_i").Value = IfNull(delDocNum)
        cmd.Parameters("orig_del_doc_num_i").Value = IfNull(origDelDocNum)
        cmd.Parameters("ord_tp_cd_i").Value = IfNull(ordTpCd)
        cmd.Parameters("stat_cd_i").Value = IfNull(statCd)
        cmd.Parameters("trn_dt_i").Value = IfNull(finalDt, Nothing)
        cmd.Parameters("so_store_cd_i").Value = IfNull(storeCd)
        cmd.Parameters("amt_i").Value = IfNull(amt, Nothing)
        cmd.Parameters("xfer_from_cust_cd_i").Value = IfNull(custCd)
        cmd.Parameters("xfer_to_finco_cd_i").Value = IfNull(finCustCd)
        cmd.Parameters("emp_cd_op_i").Value = IfNull(empCdOp)
        cmd.Parameters("co_cd_i").Value = IfNull(coCd)
        cmd.Parameters("origin_cd_i").Value = IfNull(originCd)
        cmd.Parameters("ar_tp_i").Value = IfNull(arTp)
        cmd.Parameters("bnk_crd_num_i").Value = IfNull(acctNum)

        Try
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        Catch ex As Exception
            Throw ex 'New Exception("Error in xferCustLiability " & ex.Message, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Retrieves the different promotion descriptions, code, etc. setup  for the auth provider 
    ''' and promotion code passed-in 
    ''' </summary>
    ''' <param name="providerId">the finance provider offering the promotion</param>
    ''' <param name="providerPromoCd">the promo code that the user sets up for the promotion. This is not the promotion 
    '''  code that was provided by the service provider. </param>
    ''' <returns>a Dataset consisting of the promotion descriptions, code.
    Public Shared Function GetAspPromoDetails(ByVal providerId As String, ByVal providerPromoCd As String) As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String =
            "SELECT  des, promo_des3, as_promo_cd" &
                       " FROM ASP_PROMO" &
                       " WHERE as_cd = '" & providerId & "' " &
                            " AND promo_cd = '" & providerPromoCd & "' "

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "PROMO_DETAILS")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function

    Public Enum warning

        qtyChangeWarning = 1

    End Enum

    Public Enum temp_itm_key_tp

        tempItmBySessionId = 1
        tempItmByRowIdSeqNum = 2            ' this is the temp_itm row_id column which is really a sequence number
        tempItmBySessAndRowIdSeqNum = 4     ' this is the temp_itm row_id column which is really a sequence number
        tempItmByOraRowId = 8               ' this is the Oracle rowid
    End Enum

    Public Enum temp_itm_where_clause

        none = 1
        qtyGreaterZero = 2
    End Enum

    ''' <summary>
    ''' Extracts from the temp_itm table using a single key; key dynamically set based on key type specified; where clause set dynamically based on
    ''' </summary>
    ''' <param name="key">value of the single key string, where it be the session id or the row_id column or the Oracle rowid</param>
    ''' <param name="keyTp">the type of key to extract temp_itm table entries with - see Enum temp_itm_key_tp</param>
    ''' <param name="where">where clause type to add to the selection</param>
    ''' <returns>DataSet containing the set of temp_itm records using the specified key and the where clause if specified</returns>
    ''' <remarks></remarks>
    Public Shared Function GetTempItmInfoBySingleKey(ByVal key As String,
                                                     ByVal keyTp As temp_itm_key_tp,
                                                     Optional ByVal where As temp_itm_where_clause = temp_itm_where_clause.none) As DataTable
        '  extract and return the basic temp_itm record information based on input and type and where clause

        ' TODO - where clause may be better as a string of comma delimited options or could change this to another level where the output is the sql which is added to
        '   based on the routine called 
        Dim datSet As New DataSet
        Dim conn As OracleConnection = GetConn(Connection_Constants.CONN_ERP)

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("SELECT t.ITM_CD, t.ORIG_SO_LN_SEQ, t.VE_CD, t.VSN, t.DES, t.SLSP1, t.SLSP2, t.STORE_CD, t.LOC_CD, ").Append(
            "t.ROW_ID, NVL(t.RET_PRC,0) AS RET_PRC, NVL(t.ORIG_PRC,0) AS ORIG_PRC, NVL(t.MANUAL_PRC,0) AS MANUAL_PRC, NVL(t.QTY,0) AS QTY, NVL(tax_pct,0) AS tax_pct, ").Append(
            "t.ITM_TP_CD, t.TREATABLE, t.TREATED, t.TAKE_WITH, t.NEW_SPECIAL_ORDER, t.PACKAGE_PARENT, ").Append(
            "NVL(t.TAXABLE_AMT,0) AS TAXABLE_AMT, t.tax_amt, ").Append(
            "t.LEAVE_CARTON, NVL(t.SLSP1_PCT,0) AS SLSP1_PCT, NVL(t.SLSP2_PCT,0) AS SLSP2_PCT, ").Append(
            "t.SERIAL_TP, t.SERIAL_NUM, t.ID_NUM, t.BULK_TP_ITM, NVL(t.OUT_ID, '') AS out_id, ").Append(
            "NVL(t.WARR_ROW_LINK, '') AS warr_row_link, t.WARR_ITM_LINK, NVL(t.warrantable,'N') AS warrantable, ").Append(
            "i.inventory, t.res_id, t.treated_by, t.comm_cd, t.disc_amt, t.spiff, t.del_pts, t.po_cd, t.po_ln, prc_chg_app_cd ").Append(
        "FROM itm i, temp_itm t WHERE i.itm_cd = t.itm_cd AND ")

        If temp_itm_where_clause.qtyGreaterZero = where Then
            sqlStmtSb.Append("QTY > 0 AND ")
        End If

        If temp_itm_key_tp.tempItmByRowIdSeqNum = keyTp Then
            sqlStmtSb.Append("row_id = :key ")

        ElseIf temp_itm_key_tp.tempItmBySessionId = keyTp Then
            sqlStmtSb.Append("session_id = :key ORDER BY t.row_id ")   ' ASCending is default
        End If

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

        cmd.Parameters.Add(":key", OracleType.VarChar)
        cmd.Parameters(":key").Value = key

        Try
            conn.Open()
            Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
            oraDatA.Fill(datSet)

        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try

        ' TODO DSA - what if nothing is returned
        Return datSet.Tables(0)

    End Function

    Public Shared Function GetTempItmIinfoByDoubleKey(ByVal key1 As String, ByVal key2 As String, ByVal keyTp As temp_itm_key_tp) As DataTable
        '  extract and return the basic temp_itm record information based on 

        Dim datSet As New DataSet
        Dim conn As OracleConnection = GetConn(Connection_Constants.CONN_ERP)

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("SELECT treated_by, NVL(ret_prc, 0) as ret_prc, NVL(orig_prc,0) AS orig_prc, NVL(manual_prc, 0) as manual_prc, itm_tp_cd, take_with, ").Append(
                         "itm_cd, NVL(qty, 0) as qty, NVL(max_crm_qty, 0) AS max_crm_qty, ").Append("NVL(taxable_amt, 0) as taxable_amt, NVL(cost, 0) as cost, ").Append(
                         "vsn, package_parent, out_id, row_id, NVL(max_ret_prc, 0) AS max_ret_prc, ").Append(
                         "warr_row_link, store_cd, loc_cd, res_id, prc_chg_app_cd, style_cd  FROM temp_itm WHERE ") 'Alice added style_cd on May 21, 2019 for request 4168

        If temp_itm_key_tp.tempItmBySessAndRowIdSeqNum = keyTp Then

            sqlStmtSb.Append(" session_id = :key1 AND row_id = :key2 ")
        End If

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)

        cmd.Parameters.Add(":key1", OracleType.VarChar)
        cmd.Parameters(":key1").Value = key1
        cmd.Parameters.Add(":key2", OracleType.VarChar)
        cmd.Parameters(":key2").Value = key2

        Try
            conn.Open()
            Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
            oraDatA.Fill(datSet)

        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try

        ' TODO DSA - what if nothing is returned
        Return datSet.Tables(0)

    End Function


    ''' <summary>
    ''' Creates temp_warr table entries for the existing temp_itm warranty records that can be applied to the SKU
    ''' </summary>
    ''' <param name="sessId">Current session ID to extract data from TEMP_ITM table</param>
    ''' <param name="itmCd">Warrantable SKU itm_cd to be used to only make warranty SKUs available that are setup as valid for this warrantable SKU</param>
    Public Shared Sub CreateTempWarrFromTempItm(ByVal sessId As String, ByVal itmCd As String)

        ' TODO - check constraints on these tables to make sure don't need outer joins - here and salesUtils
        Dim sql As New StringBuilder
        sql.Append("DELETE FROM temp_warr WHERE session_id = :sessId ")

        Try
            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP),
                cmd As New OracleCommand(UCase(sql.ToString), conn)

                conn.Open()

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                cmd.Parameters(":sessId").Value = sessId

                cmd.ExecuteNonQuery()

                sql.Clear()
                sql.Append("INSERT INTO temp_warr (session_id, ln_num, itm_cd, ret_prc, vsn, days) ").Append(
                           "SELECT ti.session_id, ti.row_id, ti.itm_cd, ti.ret_prc, ti.vsn, itm.warr_days ").Append(
                           "FROM itm, temp_itm ti ").Append(
                           "WHERE itm.itm_cd = ti.itm_cd ").Append(
                           "AND ti.itm_cd = (SELECT iiw.war_itm_cd FROM ITM2ITM_WARR iiw WHERE iiw.itm_cd = :itmCd AND iiw.war_itm_cd = ti.itm_cd ) ").Append(
                           "AND session_id = :sessId ORDER BY warr_days ")

                cmd.CommandText = sql.ToString
                ' keep session id parameter from 1st parameter setting
                cmd.Parameters.Add(":itmCd", OracleType.VarChar)
                cmd.Parameters(":itmCd").Value = itmCd

                cmd.ExecuteNonQuery()
            End Using
        Finally
        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="delDocNum"></param>
    ''' <param name="itmCd"></param>
    ''' <param name="sessionId"></param>
    ''' <remarks></remarks>
    Public Shared Sub CreateTempWarrFromSoLn(ByVal delDocNum As String, ByVal itmCd As String, ByVal sessionId As String)

        Dim sql As New StringBuilder
        sql.Append("DELETE FROM temp_warr WHERE session_id = :sessId ")

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Try
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(UCase(sql.ToString), conn)
            conn.Open()

            cmd.Parameters.Add(":sessId", OracleType.VarChar)
            cmd.Parameters(":sessId").Value = delDocNum
            cmd.ExecuteNonQuery()

            sql.Clear()
            sql.Append("INSERT INTO temp_warr (session_id, ln_num, itm_cd, ret_prc, vsn, days) ").Append(
                       "SELECT sl.del_doc_num, sl.del_doc_ln#, sl.itm_cd, sl.unit_prc, itm.vsn, NVL(itm.warr_days, w.days) ").Append(
                       "FROM warr w, itm2warr i2w, itm, so_ln sl ").Append(
                       "WHERE itm.itm_cd = sl.itm_cd AND i2w.itm_cd = sl.itm_cd AND i2w.warr_id = w.warr_id ").Append(
                       "AND sl.itm_cd = (SELECT iiw.war_itm_cd FROM ITM2ITM_WARR iiw WHERE iiw.itm_cd = :itmCd AND iiw.war_itm_cd = sl.itm_cd ) ").Append(
                       "AND del_doc_num = :delDocNum  ORDER BY days ")


            cmd.CommandText = sql.ToString
            cmd.Parameters.Clear()
            cmd.Parameters.Add(":itmCd", OracleType.VarChar)
            cmd.Parameters(":itmCd").Value = itmCd
            cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
            cmd.Parameters(":delDocNum").Value = delDocNum
            cmd.ExecuteNonQuery()

            'End Using
        Catch
            Throw

        Finally
            conn.Close()
        End Try

    End Sub

    Public Class TempItm
        ' object representing a row of the TEMP_ITM table
        ' Keys
        Property rowId As String            ' this is the Oracle rowid
        Property rowIdSeqNum As Integer      ' this is the temp_itm row_id column which is really a sequence number
        Property sessionId As String

        Property itmCd As String
        Property qty As Double
        Property veCd As String
        Property mnrCd As String
        Property catCd As String
        Property retPrc As Double
        Property vsn As String
        Property des As String
        Property siz As String
        Property finish As String
        Property cover As String
        Property grade As String
        Property styleCd As String
        Property discAmt As Double
        Property itmTpCd As String
        Property treatable As String
        Property treated As String
        Property relatedComplete As String
        Property treatedBy As String
        Property storeCd As String
        Property locCd As String
        Property takeWith As String
        Property availDt As Date
        Property delPoints As Double
        Property cost As Double
        Property commCd As String
        Property soLnNo As Integer
        Property taxCd As String
        Property taxableAmt As Double
        Property taxPct As Double
        Property spiff As Double
        Property newSpecOrd As String
        Property pkgParent As String
        Property lvInCarton As String
        Property slsp1 As String
        Property slsp2 As String
        Property slsp1Pct As Double
        Property slsp2Pct As String
        Property serialNum As String
        Property serialTp As String
        Property bulkTpItm As String
        Property poCd As String
        Property taxAmt As Double
        Property idNum As String
        Property outId As String
        Property warrantable As String
        Property warrRowLink As Integer
        Property warrItmLink As String

    End Class

    Public Class TempItmUpdateRequest
        ' object representing a list of flags to indicate the key info and to indicate which columns to update

        Property UpdtBy As temp_itm_key_tp

        Property qtyUpdt As Boolean
        Property retPrcUpdt As Boolean
        Property sizUpdt As Boolean
        Property finishUpdt As Boolean
        Property coverUpdt As Boolean
        Property gradeUpdt As Boolean
        Property discAmtUpdt As Boolean
        Property treatedUpdt As Boolean
        Property relatedCompleteUpdt As Boolean
        Property treatedByUpdt As Boolean
        Property storeCdUpdt As Boolean
        Property locCdUpdt As Boolean
        Property takeWithUpdt As Boolean
        Property availDtUpdt As Boolean
        'Property delPoints As Double - extended or not ???
        Property costUpdt As Boolean
        Property commCdUpdt As Boolean
        'Property soLnNo As Integer
        Property taxCdUpdt As Boolean
        'Property taxableAmt As Double
        'Property taxPct As Double
        'Property spiff As Double
        'Property newSpecOrd As String
        'Property pkgParent As String
        Property lvInCartonUpdt As Boolean
        Property slsp1Updt As Boolean
        Property slsp2Updt As Boolean
        Property slsp1PctUpdt As Boolean
        Property slsp2PctUpdt As Boolean
        Property serialNumUpdt As Boolean
        Property poCdUpdt As Boolean
        Property taxAmtUpdt As Boolean
        Property idNumUpdt As Boolean
        Property outIdUpdt As Boolean
        Property warrRowLinkUpdt As Boolean
        Property warrItmLinkUpdt As Boolean

        Property tmpItm As TempItm

    End Class

    ''' <summary>
    ''' Updates the avail_dt column on the temp_itm table for the line number (temp_itm.row_id) specified; if the availDt is not a valid date the update will
    ''' not occur so currently does not clear the column
    ''' </summary>
    ''' <param name="availDt">available date to set on the row specified</param>
    ''' <param name="lnNum">line number (temp_itm.row_id) to update</param>
    ''' <returns>the numnber of records updated</returns>
    ''' <remarks></remarks>
    Public Shared Function UpdtTempItmAvailDtByLnNum(ByVal availDt As String, ByVal lnNum As Integer) As Integer

        Dim updtCount As Integer = 0

        If IsNumeric(lnNum) AndAlso IsDate(availDt) Then

            Dim tempUpdtReq As New TempItmUpdateRequest
            tempUpdtReq.tmpItm = New TempItm
            tempUpdtReq.tmpItm.availDt = FormatDateTime(availDt, DateFormat.ShortDate)
            tempUpdtReq.availDtUpdt = True
            tempUpdtReq.tmpItm.rowIdSeqNum = lnNum
            tempUpdtReq.UpdtBy = temp_itm_key_tp.tempItmByRowIdSeqNum
            updtCount = HBCG_Utils.UpdtTempItm(tempUpdtReq)
        End If

        Return updtCount
    End Function


    ''' <summary>
    ''' Updates the temp_itm table based on the input request
    ''' </summary>
    ''' <param name="updtReq">object containing both the tempItm oject and the object of indicator as to which columns to update</param>
    ''' <remarks></remarks>
    Public Shared Function UpdtTempItm(ByVal updtReq As TempItmUpdateRequest) As Integer
        '  Update temp_itm table based on input request; return # records updated

        Dim updtCount As Integer = 0
        Dim tmpData As TempItm = updtReq.tmpItm
        Dim tmpStr As String
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("UPDATE temp_itm SET ")
        Dim baseLen As Integer = sqlStmtSb.ToString.Length

        If updtReq.availDtUpdt Then
            sqlStmtSb.Append(" avail_dt = :availDt,")
            cmd.Parameters.Add(":availDt", OracleType.DateTime)
            If Not IsNothing(tmpData.availDt) AndAlso IsDate(tmpData.availDt) Then
                cmd.Parameters(":availDt").Value = tmpData.availDt
            Else
                cmd.Parameters(":availDt").Value = System.DBNull.Value
            End If
        End If
        If updtReq.warrItmLinkUpdt Then
            sqlStmtSb.Append(" warr_itm_link = :warrItm,")
            cmd.Parameters.Add(":warrItm", OracleType.VarChar)
            cmd.Parameters(":warrItm").Value = tmpData.warrItmLink
        End If
        If updtReq.warrRowLinkUpdt Then
            tmpStr = tmpData.warrRowLink
            sqlStmtSb.Append(" warr_row_link = :warrRow,")
            cmd.Parameters.Add(":warrRow", OracleType.Number)
            cmd.Parameters(":warrRow").Value = IIf(convStrToNum(tmpStr) = 0, System.DBNull.Value, CDbl(tmpStr))     'tmpData.warrRowLink
        End If
        If updtReq.treatedByUpdt Then
            tmpStr = tmpData.treatedBy
            sqlStmtSb.Append(" treated_by = :trtRowIdSeqNum,")
            cmd.Parameters.Add(":trtRowIdSeqNum", OracleType.Number)
            cmd.Parameters(":trtRowIdSeqNum").Value = IIf(convStrToNum(tmpStr) = 0, System.DBNull.Value, CDbl(tmpStr))
        End If
        If updtReq.treatedUpdt Then
            sqlStmtSb.Append(" treated = :trtd,")
            cmd.Parameters.Add(":trtd", OracleType.VarChar)
            cmd.Parameters(":trtd").Value = tmpData.treated
        End If

        If sqlStmtSb.ToString.Length > baseLen Then

            sqlStmtSb.Remove(sqlStmtSb.Length - 1, 1) ' remove the ending comma; entries must end in comma, not comma space
            sqlStmtSb.Append(" WHERE ")

            If updtReq.UpdtBy = temp_itm_key_tp.tempItmByRowIdSeqNum Then

                sqlStmtSb.Append(" row_id = :rowIdFld ")
                cmd.Parameters.Add(":rowIdFld", OracleType.VarChar)
                cmd.Parameters(":rowIdFld").Value = tmpData.rowIdSeqNum

            ElseIf updtReq.UpdtBy = temp_itm_key_tp.tempItmByOraRowId Then

                sqlStmtSb.Append(" rowid = :rowIdFld ")
                cmd.Parameters.Add(":rowIdFld", OracleType.VarChar)
                cmd.Parameters(":rowIdFld").Value = tmpData.rowId
            End If

            cmd.CommandText = sqlStmtSb.ToString

            Try
                Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)

                    conn.Open()
                    Dim trans As OracleTransaction = conn.BeginTransaction
                    cmd.Connection = conn
                    cmd.Transaction = trans

                    updtCount = cmd.ExecuteNonQuery()
                    trans.Commit()
                    trans.Dispose()
                    conn.Close()
                End Using
            Finally
            End Try
        End If

        Return updtCount

    End Function

    ''' <summary>
    ''' Creates temp_itm table entry for the warranty record and update the link to the warranty on the applicable warrantable SKU 
    ''' </summary>
    ''' <param name="req">requestion object information to create warranty SKU line</param>
    ''' <param name="itmRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid) for the warrantable item to link</param>
    Public Shared Function CreateWarrantyLn(ByVal req As HBCG_Utils.CreateTempItmReq) As String
        Dim warrRowIdSeqNum As String = String.Empty

        Dim resp As HBCG_Utils.CreateTempItmResp = HBCG_Utils.CreateTempItm(req)

        If (Not IsNothing(resp)) AndAlso resp.insertedSkuRowId.isNotEmpty Then
            '  Update the warranty link information (itm_cd, temp_itm.row_id) on the warrantable SKU
            warrRowIdSeqNum = SalesUtils.GetTempItmRowSeqNum(resp.insertedSkuRowId)
            UpdtWarrLnk(req.itmCd, warrRowIdSeqNum, req.warrRowLink) ' not checking for update count at this time ???
        End If

        Return warrRowIdSeqNum
    End Function

    ''' <summary>
    ''' update the warranty link information on the applicable warrantable SKU in the temp_itm table
    ''' </summary>
    ''' <param name="warrItmCd">warranty SKU</param>
    ''' <param name="warrRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid) for the warranty item to link</param>
    ''' <param name="itmRowIdSeqNum">TEMP_ITM unique sequence number (ROW_ID column but not Oracle rowid) for the warrantable item to link</param>
    ''' 
    ''' TODO DSA  - this input may depend whether 1 to many or 1 to 1 and whether updating warranty or warrantable at this time
    ''' 

    Public Shared Sub UpdtWarrLnk(ByVal warrItmCd As String, ByVal warrRowIdSeqNum As String, ByVal itmRowIdSeqNum As String)

        'If Not IsNothing(resp)) AndAlso resp.insertedSkuRowId.isNotEmpty Then   ' TODO - maybe further validation

        ' TODO - make sure 1 to 1 still works
        Dim updtWarrReq As New TempItmUpdateRequest
        updtWarrReq.tmpItm = New TempItm
        updtWarrReq.tmpItm.warrItmLink = warrItmCd
        updtWarrReq.tmpItm.warrRowLink = warrRowIdSeqNum
        updtWarrReq.warrItmLinkUpdt = True
        updtWarrReq.warrRowLinkUpdt = True
        updtWarrReq.UpdtBy = temp_itm_key_tp.tempItmByRowIdSeqNum
        updtWarrReq.tmpItm.rowIdSeqNum = itmRowIdSeqNum

        UpdtTempItm(updtWarrReq) ' not checking for update count at this time ???

    End Sub

    Public Shared Function deleteTempItmByRowId(ByVal LnNum As Integer) As Integer
        '  Update temp_itm table based on input request; return # records updated

        Dim updtCount As Integer = 0

        Dim conn As OracleConnection = GetConn(Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("DELETE FROM temp_itm WHERE row_id = :rowIdFld ")
        Dim baseLen As Integer = sqlStmtSb.ToString.Length

        'If temp_itm_key_tp.selectByRowId = keyTp Then
        '    sqlStmtSb.Append("row_id = :key ")

        'ElseIf temp_itm_key_tp.selectBySessionId = keyTp Then
        '    sqlStmtSb.Append("session_id = :key ")
        'End If

        cmd.CommandText = sqlStmtSb.ToString
        cmd.Connection = conn
        cmd.Parameters.Add(":rowIdFld", OracleType.VarChar)
        cmd.Parameters(":rowIdFld").Value = LnNum

        Try
            conn.Open()
            updtCount = cmd.ExecuteNonQuery()
            conn.Close()

        Finally
            conn.Close()
        End Try

        Return updtCount

    End Function

    ''' <summary>
    ''' Deletes a line from temp_itm based on user hitting delete button on order create screens; takes care of deleting attached lines and updating links
    ''' </summary>
    ''' <param name="rowIdSeqNum">this is the temp_itm table ROW_ID column which is a unique sequence number, not to be confused with the Oracle rowid</param>
    ''' <returns>error message if any</returns>
    ''' <remarks></remarks>
    Public Shared Function DeleteTempItmAndLinked(ByVal delRowIdSeqNum As String, ByVal sessId As String) As String
        'ByVal req As TempItmDeleteRequest) As String
        ' TODO - CC Express seems to have to acquire most of the fields for this so just get here - maybe can overload for 
        '    order_details

        Dim conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim cmdSelect As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim datRdr As OracleDataReader
        'Dim deleteRowId As String
        Dim sql As String
        Dim warnMsg As String = ""

        Dim delTbl As DataTable = GetTempItmInfoBySingleKey(delRowIdSeqNum, temp_itm_key_tp.tempItmByRowIdSeqNum)
        Dim delRow As DataRow = delTbl.Rows(0)
        Dim treated_by As String = delRow("treated_by") + ""
        Dim itmTpCd As String = delRow("itm_tp_cd")
        Dim pkgParentSeqNum As String = delRow("package_parent") + ""
        ' TODO:  this is scary, so the original item unit price is on the - taxable_amt ?????????????????????????????? line taxable amt is something different
        Dim cmpntPrc As String = delRow("taxable_amt")
        Dim warrable As String = delRow("warrantable") + "" ' do not need here but better safe than sorry
        Dim warrRowLink As String = delRow("warr_row_link") & ""  ' because it is a number being returned

        conn.Open()
        'Delete corresponding treatment record
        If treated_by & "" <> "" Then

            With cmd
                .Connection = conn
                .CommandText = "delete from temp_itm where row_id = " & treated_by
            End With
            cmd.ExecuteNonQuery()
        End If

        ' delete relationship records for the deleting line
        With cmd
            .Connection = conn
            .CommandText = "delete from relationship_lines_desc where line='" & delRowIdSeqNum & "' and rel_no='" & sessId & "'"
        End With
        cmd.ExecuteNonQuery()

        ' if SKU to be deleted, was a treatment, then remove the links to it
        With cmd
            .Connection = conn
            .CommandText = "update temp_itm set treated ='N', treated_by='' where treated_by = '" & delRowIdSeqNum & "'"
        End With
        cmd.ExecuteNonQuery()

        ' delete the row itself
        With cmd
            .Connection = conn
            .CommandText = "delete from temp_itm where row_id = " & delRowIdSeqNum
        End With
        cmd.ExecuteNonQuery()

        'If it is a package SKU, then we need to delete the treatment on any package component 
        If itmTpCd = SkuUtils.ITM_TP_PKG Then

            sql = "SELECT TREATED_BY FROM TEMP_ITM WHERE PACKAGE_PARENT=" & delRowIdSeqNum & " AND TREATED_BY IS NOT NULL"
            cmdSelect = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                datRdr = DisposablesManager.BuildOracleDataReader(cmdSelect)
                Do While datRdr.Read()
                    With cmd
                        .Connection = conn
                        .CommandText = "delete from temp_itm where row_id = " & datRdr.Item("TREATED_BY").ToString
                    End With
                    cmd.ExecuteNonQuery()
                Loop
                datRdr.Close()
            Catch
                datRdr.Close()
                Throw
            End Try

            ' delete the package components
            With cmd
                .Connection = conn
                .CommandText = "delete from temp_itm where package_parent = " & delRowIdSeqNum
            End With
            cmd.ExecuteNonQuery()

        End If
        Dim ret_prc As Double = 0
        ' if the deleting SKU is a component of a package, then 
        If IsNumeric(pkgParentSeqNum) Then

            ' TODO:  this is scary, so the original item unit price is on the - taxable_amt ??????????????????????????????
            If IsNumeric(cmpntPrc) Then

                With cmd
                    .Connection = conn
                    .CommandText = "update temp_itm set ret_prc = ret_prc-" & cmpntPrc & " where ret_prc > 0 and row_id=" & pkgParentSeqNum
                End With
                cmd.ExecuteNonQuery()

                ' TODO:   NEED TO CREATE GENERIC VERSION OF MODIFY_PACKAGE_PRICE BEFORE CAN ADD THIS IN SO LEFT THAT PIECE IN CALLING CODE  - ANOTHER TIME
                'sql = "SELECT RET_PRC FROM TEMP_ITM WHERE row_id=" & req.pkgSeqNum
                'cmdSelect = DisposablesManager.BuildOracleCommand(sql, conn)
                'Try
                '    datRdr = cmdSelect.ExecuteReader
                '    If datRdr.Read Then

                '        Modify_Package_Price(req.pkgSeqNum, req.itmCd, datRdr.Item("RET_PRC").ToString)
                '    End If
                '    datRdr.Close()
                'Catch
                '    datRdr.Close()
                '    Throw
                'End Try
            End If
        End If

        ' if warranty, remove links from warrantables; if warrantable, if linked to warranty and is last on warranty, then delete warranty.
        If itmTpCd = SkuUtils.ITM_TP_WAR Then

            With cmd
                .Connection = conn
                .CommandText = "update temp_itm set warr_itm_link = NULL, warr_row_link = NULL WHERE warr_row_link = :delId "
                .Parameters.Add(":delId", OracleType.VarChar)
                .Parameters(":delId").Value = delRowIdSeqNum
            End With
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

        ElseIf "Y".Equals(warrable) AndAlso warrRowLink <> "" Then

            warnMsg = RemoveWarrLink(warrRowLink, delRowIdSeqNum, sessId)
        End If

        cmdSelect.Dispose()
        cmd.Dispose()
        conn.Close()

        Return warnMsg
    End Function

    ''' <summary>
    ''' Determines if the warranty line attached has any other warrantable lines attached (one to many) and if not or one to one, then the warranty line is also deleted
    ''' </summary>
    ''' <param name="warrRowLink">for the warranty linked, this is the temp_itm table ROW_ID column which is a unique sequence number, not to be confused with the Oracle rowid</param>
    ''' <param name="rowIdSeqNum">for the warrantable having link removed, this is the temp_itm table ROW_ID column which is a unique sequence number, not to be confused with the Oracle rowid</param>
    ''' <param name="sessId">session ID</param>
    ''' <returns>error message if any</returns>
    ''' <remarks></remarks>
    Public Shared Function RemoveWarrLink(ByVal warrRowLink As String, ByVal rowIdSeqNum As String, ByVal sessId As String) As String

        Dim warnMsg As String = ""
        Dim currWarrLnNum As Integer = 0
        ' can have a warranty SKU without a Warranty line# (if doc was split or the warranty added later, just means warrantable not on this doc)
        If warrRowLink.isNotEmpty AndAlso warrRowLink <> "&nbsp;" Then

            currWarrLnNum = warrRowLink
        End If

        Dim hasOtherLinks As Boolean = False
        Dim warrLnNumList As New ArrayList

        Dim soLnDatTbl As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(sessId, temp_itm_key_tp.tempItmBySessionId)

        Dim soLnRow As DataRow
        For Each soLnRow In soLnDatTbl.Rows

            If soLnRow("ROW_ID") = rowIdSeqNum Then
                'finds the warrantied row and remove the warranty link from it; do not add to list

                Dim updtReq As New TempItmUpdateRequest
                updtReq.tmpItm = New TempItm
                updtReq.tmpItm.warrItmLink = ""
                updtReq.tmpItm.warrRowLink = convStrToNum("")
                updtReq.warrRowLinkUpdt = True
                updtReq.warrItmLinkUpdt = True
                updtReq.tmpItm.rowIdSeqNum = rowIdSeqNum
                updtReq.UpdtBy = temp_itm_key_tp.tempItmByRowIdSeqNum
                HBCG_Utils.UpdtTempItm(updtReq)

                If Not SysPms.isOne2ManyWarr Then

                    Exit For 'no need to keep on iterating for single warr, updated the correct row already but for one to many, will need to go thru all
                End If

            ElseIf SysPms.isOne2ManyWarr AndAlso soLnRow("WARR_ROW_LINK").ToString.isNotEmpty AndAlso "Y".Equals(soLnRow("WARRANTABLE").ToString) Then

                ' track the warrantied lines that are not the one being removed
                warrLnNumList.Add(soLnRow("WARR_ROW_LINK").ToString)
            End If
        Next

        If SysPms.isOne2ManyWarr AndAlso currWarrLnNum > 0 AndAlso (Not warrLnNumList Is Nothing) AndAlso warrLnNumList.Count > 0 Then

            For Each warrLnNum In warrLnNumList
                'finds if any other warrantables are linked to the warranty
                If warrLnNum = currWarrLnNum Then

                    hasOtherLinks = True
                End If
            Next
        End If

        If (Not hasOtherLinks) AndAlso currWarrLnNum > 0 Then

            ' now iterate to find the warranty row to void/remove it
            For Each soLnRow In soLnDatTbl.Rows

                If soLnRow("ROW_ID") = currWarrLnNum Then

                    HBCG_Utils.deleteTempItmByRowId(currWarrLnNum)
                    warnMsg = "Removing the referenced warranty SKU " + soLnRow("ITM_CD")   ' TODO - could add line # but not NOW 'sku x on line y'
                    ' TODO not sure if this will show, it might get overwritten - we need better POS+ messaging

                    Exit For 'no need to keep on iterating, updated the correct row already
                End If
            Next
        End If

        Return warnMsg
    End Function

    Public Shared Function convStrToDt(ByVal str2conv As String) As Date

        If IsDate(str2conv) Then

            Return FormatDateTime(str2conv, DateFormat.ShortDate) ' TODO - if needed, could paramterize the format 

        Else
            Return Nothing
        End If
    End Function

    Public Shared Function convStrToNum(ByVal str2Conv As String) As Double

        If IsNumeric(str2Conv) Then

            Return CDbl(str2Conv)

        Else
            Return Nothing
        End If
    End Function

    Public Shared Function NumbersEqual(ByVal numStr1 As String, ByVal numStr2 As String, ByVal emptyIsEquivZero As Boolean) As Boolean
        ' empty string, nothing and system.dbnull are equivalent to zero if emptyIsNumeric = true; false otherwise
        ' Call with true if a null/blank/empty/nothing value would default to or mean the same as zero

        Dim numsEqual As Boolean = False

        If IsNumeric(numStr1) Then

            If IsNumeric(numStr2) Then

                If numStr1 = numStr2 Then

                    numsEqual = True

                Else '1.5' is not equal to '1.50' but we'll convert and retest; it may be this should be done by doing diff and compare to very small number
                    Dim num1 As Double = CDbl(numStr1)
                    Dim num2 As Double = CDbl(numStr2)
                    If num1 = num2 Then

                        numsEqual = True
                    End If
                End If

            ElseIf emptyIsEquivZero AndAlso numStr2.isEmpty AndAlso CDbl(numStr1) = 0 Then   ' probably should convert equalities (2) to diff with .0000001 or something

                numsEqual = True
            End If

        ElseIf IsNumeric(numStr2) AndAlso emptyIsEquivZero AndAlso numStr1.isEmpty AndAlso CDbl(numStr2) = 0 Then

            numsEqual = True

        ElseIf emptyIsEquivZero AndAlso numStr1.isEmpty AndAlso numStr2.isEmpty Then

            numsEqual = True
        End If

        Return numsEqual
    End Function

    Public Shared Function DatesEqual(ByVal dtStr1 As String, ByVal dtStr2 As String, ByVal emptyIsValid As Boolean, Optional ByVal shortDtOnly As Boolean = True) As Boolean
        ' empty string, nothing and system.dbnull are valid if emptyIsValid = true; false otherwise
        ' Call with true if a null/blank/empty/nothing value is valid to compare
        '  most E1 dates are truncated (without time) so shortDtOnly means to only compare truncated dates (needed for most E1 date processing)

        Dim dtsEqual As Boolean = False
        Dim emptyDt As New Date

        If IsDate(dtStr1) Then

            If IsDate(dtStr2) Then

                If dtStr1 = dtStr2 Then

                    dtsEqual = True

                ElseIf shortDtOnly AndAlso FormatDateTime(dtStr1, DateFormat.ShortDate) = FormatDateTime(dtStr2, DateFormat.ShortDate) Then

                    dtsEqual = True
                End If

            ElseIf emptyIsValid AndAlso dtStr2.isEmpty AndAlso dtStr1 = emptyDt Then

                dtsEqual = True
            End If

        ElseIf IsDate(dtStr2) AndAlso emptyIsValid AndAlso dtStr1.isEmpty AndAlso dtStr2 = emptyDt Then

            dtsEqual = True

        ElseIf emptyIsValid AndAlso dtStr1.isEmpty AndAlso dtStr2.isEmpty Then

            dtsEqual = True
        End If

        Return dtsEqual
    End Function

    Public Shared Function isValidLnNum(ByVal lnNum As String) As Boolean

        Dim validLnNum As Boolean = False

        If IsNumeric(lnNum) AndAlso CInt(lnNum) > 0.000001 AndAlso Integer.TryParse(lnNum, 0) Then

            validLnNum = True
        End If

        Return validLnNum
    End Function

    'MM-6305
    Public Shared Function FormatOracleException(ByVal errorMessage As String) As String
        ' Searchtext to split
        Dim stringSeparators As String() = New String() {"ORA-"}

        ' Split errormessage
        Dim OracleErrors As String() = errorMessage.Split(stringSeparators, StringSplitOptions.None)

        ' Show the first message to user
        Dim ErrorText As String = stringSeparators(0) + OracleErrors(1)
        Dim outPutString As String() = ErrorText.Split(":".ToCharArray())
        Dim count As Integer = 0
        For Each message As String In outPutString
            count += 1
            ' When we find the ":" then we need to get the remaining error message to dispaly on the screen
            If (count = 3) Then
                ErrorText = message
                Exit For
            End If
        Next
        Return ErrorText & " -- Please try again."
    End Function

    Public Shared Function Get_Curr_Pricing(ByVal itm_cd As String, ByVal store_cd As String, ByVal eff_date As String, ByVal itm_ret_prc As Double, ByVal cust_tp_prc_cd As String)
        ' May 25 added customization
        If store_cd & "" = "" Then store_cd = HttpContext.Current.Session("store_cd")
        If store_cd & "" = "" Then store_cd = HttpContext.Current.Session("home_store_cd")
        If cust_tp_prc_cd & "" = "" Then cust_tp_prc_cd = ""

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn.Open()

        Try

            sql = "bt_price.current_price"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.CommandType = CommandType.StoredProcedure

            objsql.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("cash_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("eff_date_i", OracleType.DateTime).Direction = ParameterDirection.Input
            objsql.Parameters.Add("itm_ret_prc_i", OracleType.Number).Direction = ParameterDirection.Input
            objsql.Parameters.Add("cust_tp_prc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("err_msg_io", OracleType.VarChar, 250).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("return_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("v_price", OracleType.Number).Direction = ParameterDirection.ReturnValue

            objsql.Parameters("itm_cd_i").Value = itm_cd
            objsql.Parameters("store_cd_i").Value = store_cd
            objsql.Parameters("cash_flag_i").Value = "N"
            objsql.Parameters("eff_date_i").Value = eff_date
            objsql.Parameters("itm_ret_prc_i").Value = itm_ret_prc
            objsql.Parameters("cust_tp_prc_cd_i").Value = cust_tp_prc_cd
            objsql.Parameters("err_msg_io").Value = DBNull.Value
            objsql.Parameters("return_type_i").Value = DBNull.Value

            objsql.ExecuteNonQuery()

            Get_Curr_Pricing = objsql.Parameters("v_price").Value

        Catch ex As Exception ' Daniela added
            Throw
        Finally
            objsql.Cancel()
            objsql.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

    End Function

    Public Shared Sub InsertSOComment(ByRef cmd As OracleCommand, comment As String, soWrittenDate As String, soStoreCode As String, soSequenceNo As String, commentType As String, employeeCode As String, delDocNum As String)


        Dim sqlQueryBuilder As StringBuilder = New StringBuilder

        sqlQueryBuilder.Append(" DECLARE ")
        sqlQueryBuilder.Append(" k_max_len CONSTANT     NUMBER := 70; ")

        'Alice added on April 30, 2019 for request 4648
        'Start
        sqlQueryBuilder.Append("  NL        constant varchar2(2)  := chr(13); ")
        sqlQueryBuilder.Append("  LF        constant varchar2(2)  := chr(10); ")
        sqlQueryBuilder.Append("  SP        constant varchar2(2)  := chr(32); ")
        'End

        sqlQueryBuilder.Append(" v_curr_cmnt            VARCHAR2(2000) := NULL; ")
        sqlQueryBuilder.Append(" v_result_cmnt          VARCHAR2(2000) := NULL; ")
        sqlQueryBuilder.Append(" v_rem_cmnt             VARCHAR2(2000) := NULL; ")
        sqlQueryBuilder.Append(" v_delimiter            VARCHAR2(01) := ' '; ")
        sqlQueryBuilder.Append(" v_trn_dt               so.so_wr_dt%TYPE := NULL; ")
        sqlQueryBuilder.Append(" origin_cd_i            so_cmnt.origin_cd%TYPE := 'POS'; ")
        sqlQueryBuilder.Append(" so_cmnt_rec            btt_so_cmnt.socmnt_recsubtype; ")
        sqlQueryBuilder.Append(" v_err_rec              gers_exc.err_rectype; ")
        sqlQueryBuilder.Append(" BEGIN ")
        sqlQueryBuilder.Append("    v_trn_dt := TRUNC(SYSDATE); ")
        sqlQueryBuilder.Append(" v_rem_cmnt := :text_i; ")

        'Alice added on April 30, 2019 for request 4648
        'Start
        sqlQueryBuilder.Append("  v_rem_cmnt := replace (v_rem_cmnt, NL, SP); ")
        sqlQueryBuilder.Append("  v_rem_cmnt := replace (v_rem_cmnt, LF, SP); ")
        'End

        sqlQueryBuilder.Append(" so_cmnt_rec.so_wr_dt := to_date(:sowrdt,'MM/DD/YYYY'); ")
        sqlQueryBuilder.Append(" so_cmnt_rec.so_store_cd := :soStoreCode; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.so_seq_num := :soSequenceNo; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.dt := v_trn_dt; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.cmnt_type := :commentType; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.origin_cd := origin_cd_i; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.emp_cd    := :emp_cd_i; ")
        sqlQueryBuilder.Append(" so_cmnt_rec.del_doc_num := :delDocNum; ")
        sqlQueryBuilder.Append(" While (v_rem_cmnt Is Not NULL) ")
        sqlQueryBuilder.Append(" Loop ")
        sqlQueryBuilder.Append(" v_curr_cmnt := v_rem_cmnt; ")
        sqlQueryBuilder.Append(" bt_format_util.split_text(v_curr_cmnt, ")
        sqlQueryBuilder.Append("                              v_result_cmnt, ")
        sqlQueryBuilder.Append("                              v_rem_cmnt, ")
        sqlQueryBuilder.Append("                              k_max_len, ")
        sqlQueryBuilder.Append("                              v_delimiter); ")
        sqlQueryBuilder.Append("   so_cmnt_rec.text := v_result_cmnt; ")
        sqlQueryBuilder.Append("   so_cmnt_rec.seq# := btt_so_cmnt.get_next_seq(so_cmnt_rec.so_wr_dt,so_cmnt_rec.so_store_cd,so_cmnt_rec.so_seq_num); ")
        sqlQueryBuilder.Append("   btt_so_cmnt.insert_rec(so_cmnt_rec, v_err_rec); ")
        sqlQueryBuilder.Append("   :err_cd := v_err_rec.err_cd; ")
        sqlQueryBuilder.Append("   :err_msg := v_err_rec.err_msg; ")
        sqlQueryBuilder.Append("   End Loop; ")
        sqlQueryBuilder.Append("   End; ")

        comment = Replace(comment, "'", "")

        'alice added on April 30, 2019 for request 4648
        comment = Replace(comment, "|", "")
        comment = comment.Trim()

        cmd.Parameters.Clear()
        cmd.CommandText = UCase(sqlQueryBuilder.ToString)
        cmd.CommandType = CommandType.Text

        cmd.Parameters.Add("err_cd", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        cmd.Parameters.Add("err_msg", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        cmd.Parameters.Add("text_i", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("text_i").Value = comment
        cmd.Parameters.Add("sowrdt", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("sowrdt").Value = soWrittenDate
        cmd.Parameters.Add("soStoreCode", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("soStoreCode").Value = soStoreCode
        cmd.Parameters.Add("soSequenceNo", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("soSequenceNo").Value = soSequenceNo
        cmd.Parameters.Add("commentType", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("commentType").Value = IIf(commentType Is Nothing, DBNull.Value, commentType)
        cmd.Parameters.Add("EMP_CD_I", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("EMP_CD_I").Value = IIf(employeeCode Is Nothing, DBNull.Value, employeeCode)
        cmd.Parameters.Add("delDocNum", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters("delDocNum").Value = IIf(delDocNum Is Nothing, DBNull.Value, delDocNum)

        cmd.ExecuteNonQuery()

        Dim errCode = cmd.Parameters("err_cd").Value
        Dim errMessage = cmd.Parameters("err_msg").Value
        cmd.Parameters.Clear()
        If Not "0".Equals(errCode) Then
            Throw New Exception(errMessage)
        End If

    End Sub



    Public Shared Sub InsertCustomerComment(ByRef cmd As OracleCommand, customerCode As String, employeeCode As String, length As Int32, comment As String)

        Dim sql As String = ""
        Dim lngSEQNUM As Integer
        Dim strMidCmnts As String
        Dim commentlst As New System.Collections.Generic.List(Of String)
        lngSEQNUM = 0

        If Not comment Is Nothing AndAlso comment.Length > 0 Then

            Dim sqlQueryBuilder As StringBuilder = New StringBuilder
            cmd.CommandType = CommandType.Text
            Dim hasInformation As Boolean = False

            sql = "Select Max(SEQ#) from CUST_CMNT where CUST_CD = '" & customerCode & "'"

            cmd.CommandText = sql
            cmd.Parameters.Clear()
            Dim myDatareader As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If myDatareader.HasRows Then
                myDatareader.Read()
                If False.Equals(myDatareader.IsDBNull(0)) Then
                    lngSEQNUM = myDatareader(0)
                    lngSEQNUM = lngSEQNUM + 1
                Else
                    lngSEQNUM = 1
                End If
            Else
                lngSEQNUM = 1
            End If

            commentlst = HBCG_Utils.CommentList(comment, length)

            sqlQueryBuilder.Append(" INSERT ALL ")

            For Each splitcomment As String In commentlst
                If splitcomment IsNot Nothing AndAlso Not String.IsNullOrEmpty(splitcomment) Then

                    Dim seqNum As Integer = 1
                    strMidCmnts = splitcomment.Trim
                    strMidCmnts = Replace(strMidCmnts, "'", "")
                    sqlQueryBuilder.Append(" into CUST_CMNT (CUST_CD, SEQ#, CMNT_DT, TEXT, EMP_CD_OP) Values ('")
                    sqlQueryBuilder.Append(customerCode).Append("','").Append(lngSEQNUM).Append("',").Append("TO_DATE('").Append(Now()).Append("','MM/DD/RRRR HH:MI:SS PM')").Append(" ,'")
                    sqlQueryBuilder.Append(strMidCmnts).Append("','").Append(employeeCode).Append("')")
                    hasInformation = True
                    lngSEQNUM = lngSEQNUM + 1
                End If
            Next

            sqlQueryBuilder.Append(" SELECT * FROM dual ")
            sql = UCase(sqlQueryBuilder.ToString)
            If hasInformation Then
                cmd.CommandText = sql
                cmd.ExecuteNonQuery()
            End If
        End If
    End Sub

    Public Shared Function CommentList(comment As String, length As Integer) As System.Collections.Generic.List(Of String)

        Dim commentlst As New System.Collections.Generic.List(Of String)
        Dim obj As New SalesUtils()

        For Each item As String In comment.Split(vbLf)

            Dim processedlength As Int32 = 0
            Dim Itemlength As Int32 = 0
            Dim len As Int32 = 0
            Itemlength = item.Length
            While Itemlength > processedlength
                Dim splititemlength As Int32 = 0
                If item Is Nothing OrElse String.IsNullOrEmpty(item) OrElse item.Length < length OrElse item.LastIndexOf(" ", length) = -1 Then

                    If item.Length > length Then
                        commentlst.Add(item.Substring(0, length))
                        splititemlength = item.Substring(0, length).Length
                    Else
                        commentlst.Add(item)
                        splititemlength = item.Length
                        Exit While
                    End If
                Else
                    commentlst.Add(item.Substring(0, item.LastIndexOf(" ", length)))
                    splititemlength = item.Substring(0, item.LastIndexOf(" ", length)).Length
                End If
                len = len + splititemlength
                processedlength = len
                item = String.Join(String.Empty, item.Skip(splititemlength))
                item = item.Trim
            End While
        Next
        Return commentlst
    End Function

End Class
