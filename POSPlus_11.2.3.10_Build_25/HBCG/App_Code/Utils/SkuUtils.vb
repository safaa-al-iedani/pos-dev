﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class SkuUtils

    ' itm type codes from the base table ITM_TP 
    Public Const ITM_TP_CPT As String = "CPT"   ' carpet
    Public Const ITM_TP_PKG As String = "PKG"   ' package
    Public Const ITM_TP_WAR As String = "WAR"   ' warranty

    ''' <summary>
    ''' Extracts and returns a few pieces of SKU information for the requested item code.
    ''' NOTE: the details returned here are mostly used during the save process.
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve some details for</param>
    ''' <returns>a table with a few details for the SKU passed in</returns>
    Public Shared Function GetItemInfo(ByVal itmCd As String) As DataRow

        Dim drSkuInfo As DataRow = Nothing
        Dim ds As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim adp As OracleDataAdapter
        Dim dbSql As String = "SELECT INVENTORY, REPL_CST, NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM WHERE ITM_CD = :itmCd"

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(dbSql, conn)
            cmd.Parameters.Add(":itmCd", OracleType.VarChar)
            cmd.Parameters(":itmCd").Value = itmCd
            adp = DisposablesManager.BuildOracleDataAdapter(cmd)
            adp.Fill(ds)

            'if nothing found, looks using the alternate ID
            If (ds.Tables(0).Rows.Count() = 0) Then
                dbSql = "SELECT INVENTORY, REPL_CST, NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM WHERE ITM_CD = "
                dbSql = dbSql & "(SELECT ITM_CD FROM ALT_ITM_ID WHERE ALT_ITM_ID_CD = :itmCd)"
                cmd.CommandText = dbSql
                adp.Fill(ds)
            End If
            'if a record is found, returns the details
            If (ds.Tables(0).Rows.Count() > 0) Then drSkuInfo = ds.Tables(0).Rows(0)

            adp.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            cmd.Dispose()
            conn.Close()
        End Try

        Return drSkuInfo
    End Function

    ''' <summary>
    ''' Extracts and returns the SKU information for the requested item code
    ''' </summary>
    ''' <param name="itm_cd"></param>
    ''' <param name="useAltItmCd">True indicates to use the alternate item code; false extracts using the item code directly</param>
    ''' <returns>the dataset of extended warranties for the SKU input </returns>
    ''' <remarks>If you need the calendar price, then you must use this method and provide the data for the calendar pricing  </remarks>
    Public Shared Function GetItemInfo(ByVal req As SalesUtils.GetCalRetPrcReq, Optional ByVal useAltItmCd As Boolean = False) As DataTable
        '  extract and return the basic item information

        Dim datSet As New DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")
        Dim sql As New StringBuilder

        sql.Append(" select sp.spiff_AMT,T.ITM_CD, COMM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF, ")
        sql.Append(" VE_CD, VSN, T.DES, REPL_CST, ITM_TP_CD, RET_PRC, ADV_PRC, bulk_tp_itm, inventory, ")
        sql.Append(" NVL(SERIAL_TP,'') AS SERIAL_TP, mnr_cd, cat_cd, siz, finish, cover, grade, style_cd, treatable, frame_tp_itm, t.drop_cd, drop_dt, ")
        sql.Append(" user_questions, point_size,  NVL(warrantable,'N') AS warrantable, prevent_sale  ")
        If req.storeCd.isNotEmpty Then
            sql.Append(" ,calRetPrc ")
        End If
        sql.Append("from (")

        sql.Append(" SELECT   ITM.ITM_CD, COMM_CD, SPIFF, VE_CD, VSN, itm.DES, REPL_CST, ITM_TP_CD, RET_PRC, ADV_PRC, bulk_tp_itm, inventory, ")
        sql.Append(" NVL(SERIAL_TP,'') AS SERIAL_TP, mnr_cd, cat_cd, siz, finish, cover, grade, style_cd, treatable, frame_tp_itm, itm.drop_cd, drop_dt, ")
        sql.Append(" user_questions, point_size,  NVL(warrantable,'N') AS warrantable, prevent_sale  ")
        If req.storeCd.isNotEmpty Then
            sql.Append(", bt_price.current_price(ITM.itm_cd, :storeCd, :effDt, itm.ret_prc, :custTpPrc, 'N', 'REAL') as calRetPrc ")
        End If
        sql.Append(" FROM  drop_cd dr, ITM  WHERE  itm.drop_cd = dr.drop_cd (+) AND ITM.ITM_CD = ")
        If useAltItmCd Then
            sql.Append("(SELECT ITM_CD FROM ALT_ITM_ID WHERE ALT_ITM_ID_CD = :itmCd )")
        Else
            sql.Append(":itmCd ")
        End If
        sql.Append(" ) T ")
        sql.Append(" left join  " + spiffTable + "  sp on t.itm_cd = sp.itm_cd and TO_DATE(sysdate) BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT) ")
        sql.Append(If(SysPms.isSpiffBySKU, " ", " AND STORE_CD = :storeCd "))


        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = req.itmCd
        cmd.Parameters.Add(":storeCd", OracleType.VarChar)
        cmd.Parameters(":storeCd").Value = req.storeCd

        If req.storeCd.isNotEmpty Then
            cmd.Parameters.Add(":effDt", OracleType.DateTime)
            cmd.Parameters(":effDt").Value = FormatDateTime(req.effDt, DateFormat.ShortDate)
            cmd.Parameters.Add(":custTpPrc", OracleType.VarChar)
            If IsNothing(req.custTpPrcCd) Then
                cmd.Parameters(":custTpPrc").Value = System.DBNull.Value
            Else
                cmd.Parameters(":custTpPrc").Value = req.custTpPrcCd
            End If
        End If
        Try
            conn.Open()
            Dim oraDatA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)
            oraDatA.Fill(datSet)

        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try

        Return datSet.Tables(0)
    End Function

    ''' <summary>
    ''' Extracts and returns the treatment SKU for the minor and category code input
    ''' </summary>
    ''' <param name="mnrCd">minor code</param>
    ''' <param name="catCd">catory code</param>
    ''' <returns>treatment SKU for the minor and category codes provided</returns>
    ''' <remarks></remarks>
    Public Shared Function GetTreatmentItmCd(ByVal mnrCd As String, ByVal catCd As String) As String

        Dim trtItmCd As String = ""

        Dim sql As New StringBuilder
        sql.Append("SELECT treatment_itm_cd FROM inv_mnr_cat WHERE mnr_cd = :mnrCd AND cat_cd = :catCd ")

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
            cmd As New OracleCommand(sql.ToString, conn)

            conn.Open()
            cmd.Parameters.Add(":mnrCd", OracleType.VarChar)
            cmd.Parameters(":mnrCd").Value = mnrCd
            cmd.Parameters.Add(":catCd", OracleType.VarChar)
            cmd.Parameters(":catCd").Value = catCd

            Dim oraDatRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


            Try
                If oraDatRdr.HasRows Then

                    oraDatRdr.Read()

                    trtItmCd = oraDatRdr.Item("treatment_itm_cd").ToString
                End If
                oraDatRdr.Close()
            Catch
                oraDatRdr.Close()
                conn.Close()
                Throw
            End Try
        End Using
        Return trtItmCd
    End Function    
    

End Class
