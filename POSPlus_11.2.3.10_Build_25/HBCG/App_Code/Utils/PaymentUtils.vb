﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class PaymentUtils

    ''' <summary>
    ''' Payment constants
    ''' </summary>
    Public Class Payment
        Public Const TP_FINANCE As String = "FI"
        Public Const TP_CCARD As String = "BC"
        Public Const TP_GCARD As String = "GC"
        Public Const TP_CHECK As String = "CK"

        Public Const CD_CASH As String = "CS"

        Public Const CD_FINANCE_GE As String = "GE"
        Public Const CD_FINANCE_WELLS As String = "WF"

        Public Const CD_CCARD_VISA As String = "VI"
        Public Const CD_CCARD_AMEX As String = "AX"
        Public Const CD_CCARD_MASTERCARD As String = "MC"
        Public Const CD_CCARD_DISCOVER As String = "DS"
    End Class

    Public Class Bank_MOP_CD
        Public Const ALI As String = "ZAP"
        Public Const WECHAT As String = "ZWC"
        Public Const UNION As String = "ZUP"
        Public Const FLEXITI As String = "FLX"
        Public Const DESJARDINS As String = "DCS"
        Public Const TDFinancial As String = "TDF"
        Public Const MCFinancial As String = "FM"
    End Class

    ''' <summary>
    ''' Gets all the original payments on the order passed-in. It will sum up the net amounts
    ''' of all alike payments and return other associated details such as the description, exp dt, etc.
    ''' </summary>
    ''' <param name="delDocNum">the order for which to get original payments for</param>
    ''' <param name="companyCd">the company code on the order</param>
    ''' <param name="customerCd">the customer code on the order</param>
    ''' <returns></returns>
    Public Shared Function GetOriginalPayments(ByVal delDocNum As String,
                                               ByVal companyCd As String,
                                               ByVal customerCd As String) As DataSet

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        'sabrina GC - added union
        ' Daniela add TD plan to DES and decrypt TDF bank card
        ' sabrina R1999
        Dim sql As String = "SELECT acct_num,  des, bnk_crd_num, exp_dt, mop_cd, mop_tp, amt " &
                                     "FROM " &
                                         "(SELECT decode(a.MOP_CD, 'TDF', (m.DES || ' - ' || a.des),'DCS',(m.DES ||' - '||a.des),'FLX',(m.DES ||' - '||a.des),'FM',(m.DES ||' - '||a.des), m.des) des,  max(decode(a.trn_tp_cd, 'DEP', acct_num, '')) as acct_num, " &
                                         " decode(a.MOP_CD, 'TDF', std_des3.decryptCrdNum(BNK_CRD_NUM), BNK_CRD_NUM) BNK_CRD_NUM, " &
                                             "TO_CHAR(EXP_DT, 'MM/DD/YYYY') as EXP_DT, a.MOP_CD, m.MOP_TP, " &
                                             "SUM( nvl(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0),0) ) AS AMT " &
                                         "FROM ar_trn_tp t, ar_trn a, mop m " &
                                         "WHERE  a.trn_tp_cd = t.trn_tp_cd " &
                                         "AND a.MOP_CD = m.MOP_CD " &
                                         "AND a.ivc_cd = :DEL_DOC_NUM " &
                                         "AND a.cust_cd = :CUST_CD " &
                                         "AND a.co_cd = :CO_CD " &
                                         "AND a.ar_tp   = 'O' " &
                                         "AND a.MOP_CD <> 'GC' " &
                                         "AND a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER', 'DTC', 'DTD') " &
                                         "GROUP BY decode(a.MOP_CD, 'TDF', (m.DES || ' - ' || a.des), 'DCS',(m.DES || ' - ' || a.des),'FLX',(m.DES ||' - '||a.des),'FM',(m.DES ||' - '||a.des), m.des), BNK_CRD_NUM, EXP_DT, a.MOP_CD, m.MOP_TP) " &
                                     "WHERE amt != 0 " &
         " union all " &
         " SELECT acct_num,  des, bnk_crd_num, exp_dt, mop_cd, mop_tp, sum(amt) " &
                "FROM " &
                "(SELECT m.DES,  null as acct_num, null BNK_CRD_NUM, " &
                " null as EXP_DT, a.MOP_CD, m.MOP_TP, " &
                "SUM( nvl(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0),0) ) AS AMT " &
                "FROM ar_trn_tp t, ar_trn a, mop m " &
                "WHERE  a.trn_tp_cd = t.trn_tp_cd " &
                "AND a.MOP_CD = m.MOP_CD " &
                "AND a.ivc_cd = :DEL_DOC_NUM " &
                "AND a.cust_cd = :CUST_CD " &
                "AND a.co_cd = :CO_CD " &
                "AND a.ar_tp   = 'O' " &
                "AND a.MOP_CD = 'GC' " &
                "AND a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER', 'DTC', 'DTD') " &
                "GROUP BY m.DES, 2,3,4, a.MOP_CD, m.MOP_TP " &
                " union " &
                "select 'GIFT CARD      (PREPAID PURCHASE CARD)',null,null,null,'GC','GC',0 FROM DUAL) " &
                " group by acct_num,des,bnk_crd_num,exp_dt,mop_cd,mop_tp "

        If (delDocNum.isNotEmpty) Then
            ds = New DataSet()
            Try
                conn.Open()
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
                cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum.Trim.ToUpper
                cmd.Parameters.Add(":CUST_CD", OracleType.VarChar).Direction = ParameterDirection.Input
                cmd.Parameters(":CUST_CD").Value = customerCd
                cmd.Parameters.Add(":CO_CD", OracleType.VarChar).Direction = ParameterDirection.Input
                cmd.Parameters(":CO_CD").Value = companyCd
                oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
                oAdp.Fill(ds, "ORIG_PMTS")

                'TODO- AXK -WIP add back/revisit when finance is introduced

                '*** the table above DOES NOT include FINANCE pmts- they have to be retrieved separately
                '*** from SO, and then its data combined with the origPmts.
                'Dim orderStatus As String = String.Empty
                'Dim orderType As String = String.Empty

                ''retrieve the order status, type, etc. from the db
                'Dim dTable As DataTable = OrderUtils.GetOrderDetails(delDocNum)
                'If dTable.Rows.Count > 0 Then
                '    Dim dRow As DataRow = dTable.Rows(0)
                '    orderType = dRow("ORD_TP_CD")
                '    orderStatus = dRow("STAT_CD")
                'End If

                'TODO- AXK -WIP add back/revisit when finance is introduced
                ''**** Deposit type(DP) finance are always retrieved and are refundable.  
                'Dim dr As DataRow
                'Dim origPmtsTable As DataTable = ds.Tables(0)

                ''*** First retrieve the DP type finance payments
                'Dim financeDPPmtDs As DataSet = GetFinanceDPPayments(delDocNum)
                '' now add each record in the DP finance datatable to the orig pmts
                'For Each dr In financeDPPmtDs.Tables(0).Rows
                '    origPmtsTable.ImportRow(dr)
                'Next

                ''**** Based on the order status & type, regular finance may or may not be included in the list of MOPs that can be refunded or not.
                'Dim retrieveNonDPFinance As Boolean = (AppConstants.Order.TYPE_CRM = orderType AndAlso
                '                                       (AppConstants.Order.STATUS_OPEN = orderStatus Or AppConstants.Order.STATUS_VOID = orderStatus))

                'If retrieveNonDPFinance Then
                '    '**** retrieve the non-DP/regular finance payments
                '    Dim financePmtDs As DataSet = GetFinancePayments(delDocNum)

                '    'add each record in the Non-DP finance datatable to the orig pmts
                '    For Each dr In financePmtDs.Tables(0).Rows
                '        origPmtsTable.ImportRow(dr)
                '    Next
                'End If
            Catch ex As Exception
                Throw
            Finally
                conn.Close()
            End Try
        End If

        Return ds

    End Function

    ''' <summary>
    ''' Retrieves the regular, NON-Deposit Type/Down Payment (or DP) Finance MOPs that were originally made on the order passed-in.
    ''' </summary>
    ''' <param name="delDocNum">the order for which to get Finance payment for</param>
    ''' <returns></returns>
    Private Shared Function xxGetFinancePayments(ByVal delDocNum As String) As DataSet

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String = "SELECT m.des, ar.acct_num, ar.bnk_crd_num, ar.exp_dt, ar.mop_cd, 'FI' AS mop_tp, " &
                            "NVL(DECODE(ar.trn_tp_cd, 'SAL', so.orig_fi_amt, 'MDB', so.orig_fi_amt, -so.orig_fi_amt), 0) AS amt " &
                            "FROM  ar_trn ar, store s, mop m, so " &
                            "WHERE(ar.ivc_cd = so.del_doc_num)  " &
                            "AND m.mop_cd = ar.mop_cd " &
                            "AND s.store_cd = so.so_store_cd  " &
                            "AND ar.co_cd = s.co_cd  " &
                            "AND ar.trn_tp_cd IN ('SAL', 'CRM', 'MDB', 'MCR') " &
                            "AND so.stat_cd ='F' || '' " &
                            "AND ar.cust_cd = so.cust_cd " &
                            "AND so.del_doc_num = :DEL_DOC_NUM " &
                            "AND so.fin_cust_cd IS NOT NULL  " &
                            "AND so.orig_fi_amt > 0"

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "ORIG_FIN")

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Determines if there is an open MCR/MDB/CRM against the order passed-in. 
    ''' If there is, then that order number is returned, otheewise a "N" is returned.
    ''' The 'N" is an indicator that will typically be used in a refund scenario.
    ''' </summary>
    ''' <param name="delDocNum">the order to check agaisnt </param>
    ''' <param name="companyCd">the company code on the order</param>
    ''' <param name="customerCd">the customer code on the order</param>
    Public Shared Function GetLinkedDocForSale(ByVal delDocNum As String,
                                                   ByVal companyCd As String,
                                                   ByVal customerCd As String) As String

        Dim sql As New StringBuilder
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim reader As OracleDataReader
        Dim cmd As OracleCommand
        Dim rtnVal As String = String.Empty

        sql.Append(" SELECT nvl(a.adj_ivc_cd,'N') ")
        sql.Append(" FROM ar_trn a, ar_Trn_tp b ")
        sql.Append(" WHERE a.trn_tp_cd = b.trn_tp_cd ")
        sql.Append(" AND  CO_CD     = :CO_CD  ")
        sql.Append(" AND  CUST_CD   = :CUST_CD  ")
        sql.Append(" AND  IVC_CD = :DEL_DOC_NUM  ")
        sql.Append(" GROUP BY adj_ivc_cd ")
        sql.Append(" HAVING SUM(DECODE(dc_cd,'D',amt,'C',-amt)) != 0 ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add(":CUST_CD", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add(":CO_CD", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum.Trim
        cmd.Parameters(":CUST_CD").Value = customerCd
        cmd.Parameters(":CO_CD").Value = companyCd
        Try
            conn.Open()
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                rtnVal = reader.Item(0).ToString()
            End If
            reader.Close()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal

    End Function

    ''' <summary>
    ''' Gets refund details such as the refundable amount, order total, etc. for the order passed-in.
    ''' This will be used for amount validation when doing refunds against a sale order. 
    ''' </summary>
    ''' <param name="delDocNum">the order for which to get amount details for</param>
    ''' <param name="companyCd">the company code on the order</param>
    ''' <param name="customerCd">the customer code on the order</param>
    Public Shared Function GetRefundDetails(ByVal delDocNum As String,
                                            ByVal companyCd As String,
                                            ByVal customerCd As String) As DataTable
        Dim dtable As New DataTable
        Dim sql As New StringBuilder
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand

        sql.Append(" SELECT a.ivc_cd AS DOC_NUM, SUM(DECODE(A.TRN_TP_CD,'SAL',A.AMT, 'MDB', A.AMT, 'CRM', -A.AMT, 'MCR', -A.AMT,0))  ORDER_TOTAL,  ")
        sql.Append("                            SUM(DECODE(A.TRN_TP_CD,'PMT', A.AMT,  ")
        sql.Append("                                      'DEP',A.AMT,'DED', -A.AMT,    'MRC',A.AMT,'MRD', -A.AMT,  ")
        sql.Append("                                      'BDR',A.AMT,'NSF', -A.AMT, 'R', -A.AMT,  'DTC',A.AMT,'DTD', -A.AMT,0)) as REFUNDABLE_AMT, 'N' as link_fnd ")
        sql.Append("                     FROM    AR_TRN A, AR_TRN_TP T ")
        sql.Append("                     WHERE   A.CO_CD     = :CO_CD ")
        sql.Append("                     AND     A.CUST_CD   = :CUST_CD  ")
        sql.Append("                     AND     (A.IVC_CD = :DEL_DOC_NUM  or A.ADJ_IVC_CD = :DEL_DOC_NUM)  ")
        sql.Append("                     AND     A.AR_TP = 'O'  ")
        sql.Append("                     AND     A.TRN_TP_CD IN ('PMT','DEP','DED', 'MRC','MRD', 'BDR','NSF',  ")
        sql.Append("                                             'R','DTC','DTD', 'SAL','CRM','MDB','MCR')  ")
        sql.Append("                     AND     A.TRN_TP_CD = T.TRN_TP_CD  ")
        sql.Append("                     GROUP BY A.ivc_cd ")
        sql.Append(" UNION ALL ")
        sql.Append(" SELECT a.ivc_cd AS DOC_NUM, SUM(DECODE(A.TRN_TP_CD,'SAL',A.AMT, 'MDB', A.AMT, 'CRM', -A.AMT, 'MCR', -A.AMT,0))  ORDER_TOTAL,  ")
        sql.Append("                            SUM(DECODE(A.TRN_TP_CD,'PMT', A.AMT,  ")
        sql.Append("                                      'DEP',A.AMT,'DED', -A.AMT,    'MRC',A.AMT,'MRD', -A.AMT,  ")
        sql.Append("                                      'BDR',A.AMT,'NSF', -A.AMT, 'R', -A.AMT,  'DTC',A.AMT,'DTD', -A.AMT,0)) as REFUNDABLE_AMT, 'N' as link_fnd  ")
        sql.Append("                     FROM    AR_TRN A, AR_TRN_TP T ")
        sql.Append("                     WHERE   A.CO_CD     = :CO_CD ")
        sql.Append("                     AND     A.CUST_CD   = :CUST_CD  ")
        sql.Append("                     AND     (A.IVC_CD = :DEL_DOC_NUM  or A.ADJ_IVC_CD = :DEL_DOC_NUM)  ")
        sql.Append("                     AND     a.ivc_cd is NULL ")
        sql.Append("                     AND     A.AR_TP = 'O'  ")
        sql.Append("                     AND     A.TRN_TP_CD IN ('PMT','DEP','DED', 'MRC','MRD', 'BDR','NSF',  ")
        sql.Append("                                             'R','DTC','DTD', 'SAL','CRM','MDB','MCR')  ")
        sql.Append("                     AND     A.TRN_TP_CD = T.TRN_TP_CD  ")
        sql.Append("                     GROUP BY A.ivc_cd ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add(":CUST_CD", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters.Add(":CO_CD", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":DEL_DOC_NUM").Value = delDocNum
        cmd.Parameters(":CUST_CD").Value = customerCd
        cmd.Parameters(":CO_CD").Value = companyCd
        Try
            conn.Open()
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)
            dtable = ds.Tables(0)
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return dtable

    End Function

    ''' <summary>
    ''' Returns the balance on the order passed-in. It includes any finance amounts as appropriate for the status and order type.
    ''' </summary>
    ''' <param name="balAmt">the balance for a document from AR, typically from bt_ar_trn.bal_due</param>
    ''' <param name="fiAmt">the finance amount on the document</param>
    ''' <param name="appCd">the finance approval on the document</param>
    ''' <param name="finCoCd">the finance company code (as_cd) on the document</param>
    ''' <param name="statCd">the status code of the document</param>
    ''' <param name="ordTpCd">the status code of the document</param>
    ''' <returns>the net balance for the information provided</returns>
    Public Shared Function CalcBal(ByVal balAmt As Double, ByVal fiAmt As Double, ByVal appCd As String, ByVal finCoCd As String, ByVal statCd As String, ByVal ordTpCd As String) As Double

        Dim balance As Double = 0

        If SystemUtils.isNumber(balAmt) Then

            balance = balAmt

            If balAmt >= 0 Then   ' TODO - confirm that this is applicable for finalized doc and also 

                If SystemUtils.isNumber(fiAmt) AndAlso fiAmt > 0 AndAlso SystemUtils.isNotEmpty(appCd) AndAlso SystemUtils.isNotEmpty(finCoCd) Then

                    'For a voided order, finance should be NOT included in the balance
                    If (statCd <> AppConstants.Order.STATUS_VOID) Then

                        If AppConstants.Order.TYPE_SAL = ordTpCd OrElse
                            AppConstants.Order.TYPE_MDB = ordTpCd Then

                            balance = balAmt - fiAmt

                            'Else  ' TODO - need this for finalized credits, open credits ??
                            '    balance = balAmt + fiAmt
                        End If
                    End If
                End If
            End If
        End If

        Return balance

    End Function

    ''' <summary>
    ''' Returns the balance on the order passed-in. It excludes any finance amounts.
    ''' </summary>
    ''' <param name="custCd">the current customer</param>
    ''' <param name="docNum">the current order for which to find the balance</param>
    ''' <returns>the net balance of the order</returns>
    Public Shared Function GetBalance(ByVal custCd As String, ByVal delDocNum As String, Optional ByVal includeAll As Boolean = True) As Double

        Dim balance As Double = 0
        Dim ds As New DataSet
        Dim sqlStmtSb As New StringBuilder
        Dim cmd As OracleCommand
        Dim adp As OracleDataAdapter
        Dim includeAllCd As String = IIf(includeAll = False, "ICD", "")
        sqlStmtSb.Append("SELECT NVL(orig_fi_amt, 0) AS fi_amt, approval_cd, fin_cust_cd, ord_tp_cd, stat_cd, ").Append(
                         "bt_ar_trn.bal_due(so.del_doc_num, so.cust_cd, so.so_store_cd, '' , so.ord_tp_cd) ar_bal ").Append(
                         "FROM so WHERE del_doc_num = :docNum ")
        Try

            Using conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

                cmd = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
                adp = DisposablesManager.BuildOracleDataAdapter(cmd)
                cmd.Parameters.Add(":docNum", OracleType.VarChar)
                cmd.Parameters(":docNum").Value = delDocNum
                'cmd.Parameters.Add(":includeAll", OracleType.VarChar)
                'cmd.Parameters(":includeAll").Value = includeAllCd
                adp.Fill(ds)

            End Using

            If SystemUtils.dataSetHasRows(ds) Then
                Dim dRow As DataRow = ds.Tables(0).Rows(0)

                balance = CalcBal(CDbl(dRow("ar_bal")), CDbl(dRow("fi_amt")), (dRow("approval_cd") + "").ToString, (dRow("fin_cust_cd") + "").ToString, dRow("stat_cd").ToString, dRow("ORD_TP_CD").ToString)

                ' TODO - make sure the above works for refunds - this was code before split out
                'balance = CDbl(dRow("ar_bal"))
                'If balance >= 0 Then
                '    If CDbl(dRow("fi_amt")) > 0 AndAlso dRow("approval_cd").ToString.isNotEmpty AndAlso dRow("fin_cust_cd").ToString.isNotEmpty Then

                '        Dim isVoidSale As Boolean = (dRow("ORD_TP_CD").ToString = AppConstants.Order.TYPE_SAL AndAlso
                '                                     dRow("stat_cd").ToString = AppConstants.Order.STATUS_VOID)

                '        'Except for a voided order, finance should be NOT included in the balance
                '        If Not isVoidSale Then balance = balance - CDbl(dRow("fi_amt"))
                '    End If
                'End If
            End If
        Catch ex As Exception
            Throw
        End Try

        Return balance

    End Function


    ''' <summary>
    ''' Gets the sum of all payments with the specified MOP code(if specified) that have
    ''' been made in the current session.
    ''' </summary>
    ''' <param name="sessionId">the current session id</param>
    ''' <param name="mopCd">the mop code to group by, if any</param>
    ''' <returns>the total amount of all in-process payments or uncommitted payments. This may be grouped by mop code or not.</returns>
    Public Shared Function GetInProcessPaymentAmt(ByVal sessionId As String, Optional ByVal mopCd As String = "") As Double

        Dim rtnVal As Double = 0

        Dim dataReader As OracleDataReader
        Dim cmd As OracleCommand
        Dim ds As DataSet
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim sql As String = "SELECT sum(AMT) As total " &
                            "FROM Payment WHERE sessionid = :SESSIONID "

        If mopCd.isNotEmpty Then
            sql = sql & "AND mop_cd =:MOPCD "
        End If

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":SESSIONID", OracleType.VarChar)
        cmd.Parameters(":SESSIONID").Value = sessionId
        If mopCd.isNotEmpty Then
            cmd.Parameters.Add(":MOPCD", OracleType.VarChar)
            cmd.Parameters(":MOPCD").Value = mopCd
        End If

        Try
            Dim sAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            Dim dset As New DataSet
            sAdp.Fill(dset)
            If dset.Tables(0).Rows(0).Item("total").ToString & "" <> "" Then
                rtnVal = CDbl(dset.Tables(0).Rows(0).Item("total").ToString)
            End If

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return rtnVal

    End Function

    ''' <summary>
    ''' Retrieves the list of all 'Active', finance providers for the specific store,
    ''' defined in the system and that have a medium code and valid promotion also setup for them. 
    ''' </summary>
    ''' <param name="storeCd">the store code for which to get the providers for</param>
    ''' <returns>dataset consisting of all active, finance providers that have a medium code and valid promotion defined. </returns>
    Private Shared Function GetActiveFinanceProviders(ByVal storeCd As String) As DataSet

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As New DataSet

        sql.Append(" SELECT NAME, AS_CD ")
        sql.Append("   FROM ASP_MEDIUM2MOP am, asp ")
        sql.Append("  WHERE     EXISTS ")
        sql.Append("               (SELECT 'X' ")
        sql.Append("                  FROM ASP_PROMO ap ")
        sql.Append("                 WHERE     AP.AS_CD = ASP.AS_CD ")
        sql.Append("                       AND TRUNC (SYSDATE) BETWEEN ap.eff_dt AND ap.end_dt) ")
        sql.Append("        AND ACTIVE = 'Y' ")
        sql.Append("        AND AS_TP_CD = 'F' ")
        sql.Append("        AND am.store_cd = :STORE_CD ")
        sql.Append("        AND am.medium_tp_cd = asp.medium_tp_cd ")

        conn.Open()
        cmd = DisposablesManager.BuildOracleCommand(sql.ToString(), conn)
        cmd.Parameters.Add(":STORE_CD", OracleType.VarChar)
        cmd.Parameters(":STORE_CD").Value = storeCd

        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)

        Return ds

    End Function


    ''' <summary>
    ''' Retrieves the list of all 'Active', finance providers for the specific store, defined in the
    ''' system and populates the passed-in drop down with those values.
    ''' </summary>
    ''' <param name="storeCd">the store code for which to get the providers for</param>
    ''' <param name="ddFinanceProviders">the drop down to populate with finance companies</param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateFinanceProviders(ByVal storeCd As String,
                                               ByRef ddFinanceProviders As DropDownList)

        Dim ds As DataSet = PaymentUtils.GetActiveFinanceProviders(storeCd)
        ddFinanceProviders.ClearSelection()
        If (ds.Tables(0).Rows.Count > 0) Then
            ddFinanceProviders.DataSource = ds
            ddFinanceProviders.DataValueField = "AS_CD"
            ddFinanceProviders.DataTextField = "NAME"
            ddFinanceProviders.DataBind()
        Else
            ddFinanceProviders.Items.Insert(0, "No finance company found in the system")
            ddFinanceProviders.Items.FindByText("No finance company found in the system").Value = ""
        End If

        ddFinanceProviders.SelectedIndex = 0

    End Sub

    ''' <summary>
    ''' Retrieves all the non expired promotions defined in the system for the specified finance company.
    ''' </summary>
    ''' <param name="mopCd">the mop code, if any</param>
    ''' <param name="finCoCd">the finance company code </param>
    ''' <returns>dataset consisting of all active promotions setup for the finance company specified</returns>
    Private Shared Function GetPromotions(ByVal mopCd As String, ByVal financeCoCd As String) As DataSet

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim cmd As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet
        Dim reader As OracleDataReader

        sql = "select NVL(SHORT_DESC,LONG_DESC) AS DES, PROMO_CD "
        sql = sql & "FROM PROMO_ADDENDUMS "
        sql = sql & "WHERE PLAN_ACTIVE='Y' "
        sql = sql & "AND AS_CD=:FIN_CO "
        sql = sql & "AND EFF_DT <= SYSDATE "
        If HBCG_Utils.Is_Finance_DP(mopCd) = True Then
            sql = sql & "AND WDR_CODE = 'Y' "
        Else
            sql = sql & "AND WDR_CODE = 'N' "
        End If
        sql = sql & "AND END_DT >= SYSDATE "
        sql = sql & "ORDER BY DES"

        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":FIN_CO", OracleType.VarChar)
        cmd.Parameters(":FIN_CO").Value = financeCoCd
        oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
        oAdp.Fill(ds)

        'if no promo info found in the local Db, retrieve from E1 tables
        If ds.Tables(0).Rows.Count < 1 Then
            cmd = DisposablesManager.BuildOracleCommand

            cmd.CommandText = "SELECT PROMO_CD AS PROMO_CD, DES " &
                               "FROM ASP_PROMO WHERE AS_CD=:FIN_CO " &
                               "AND EFF_DT <= SYSDATE AND END_DT >= SYSDATE"
            cmd.Parameters.Add(":FIN_CO", OracleType.VarChar)
            cmd.Parameters(":FIN_CO").Value = financeCoCd
            conn.ConnectionString = ConfigurationManager.ConnectionStrings(SystemUtils.Connection_Constants.CONN_ERP).ConnectionString
            cmd.Connection = conn

            ds.Clear()
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds)
        End If

        Return ds
    End Function


    ''' <summary>
    ''' Retrieves all the non expired promotions defined in the system for the specified finance company and
    ''' populates the passed-in drop down with those values.
    ''' </summary>
    ''' <param name="mopCd">the mop code, if any</param>
    ''' <param name="finCoCd">the finance company code for which to get promotions</param>
    ''' <returns>dataset consisting of all active promotions setup for the finance company specified</returns>
    ''' <param name="ddPromo"> the drop down to populate with promotions</param>
    ''' <param name="addDefaultEntry">flag to indicate if a default entry to select a value should be added as the 
    '''                               as the first entry in the drop-down or not. If not passed-in, it will be added.
    ''' </param>
    ''' <remarks></remarks>
    Public Shared Sub PopulatePromotions(ByVal mopCd As String,
                                         ByVal financeCoCd As String,
                                         ByRef ddPromo As DropDownList,
                                         Optional ByVal addDefaultEntry As Boolean = True)

        Dim ds As DataSet = PaymentUtils.GetPromotions(mopCd, financeCoCd)
        ddPromo.Items.Clear()
        If (ds.Tables(0).Rows.Count > 0) Then
            ddPromo.DataSource = ds
            ddPromo.DataValueField = "PROMO_CD"
            ddPromo.DataTextField = "DES"
            ddPromo.DataBind()
        End If

        If addDefaultEntry Then
            ddPromo.Items.Insert(0, Resources.LibResources.Label810)
            ddPromo.Items.FindByText(Resources.LibResources.Label810).Value = ""
            ddPromo.SelectedIndex = 0
        End If
    End Sub

End Class
