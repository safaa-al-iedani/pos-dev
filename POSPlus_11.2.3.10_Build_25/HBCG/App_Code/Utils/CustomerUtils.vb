﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class CustomerUtils

    Private Shared Function GenCustCd(ByRef custCd As String, ByVal storeCd As String, ByVal addr As String,
                          ByVal lName As String, ByVal fName As String) As Boolean
        'Returns the customer code as generated using the input, format dictated by the AR system parameters

        Dim returnVal As String = ""
        Dim returnBool As Boolean = False
        Dim errMsg As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        Dim funcCallStmt = "gers_cust_cd.gen_cust_cd(:custCd, :storeCd, :lName, :fName, :addr, :errMsg)"
        Dim sqlstmt As String = "BEGIN :returnVal := CASE WHEN " & funcCallStmt & " THEN 'T' ELSE 'F' END; END; "
        Dim cmd = DisposablesManager.BuildOracleCommand(sqlstmt, conn)
        cmd.CommandType = CommandType.Text

        Try
            conn.Open()

            cmd.Parameters.Add("returnVal", OracleType.VarChar, 1).Direction = ParameterDirection.Output
            cmd.Parameters.Add("custCd", OracleType.VarChar, 10).Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add("errMsg", OracleType.VarChar, 80).Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add("fName", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("lName", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("storeCd", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters.Add("addr", OracleType.VarChar).Direction = ParameterDirection.Input

            cmd.Parameters("custCd").Value = custCd & ""
            cmd.Parameters("errMsg").Value = ""
            cmd.Parameters("fName").Value = fName & ""
            cmd.Parameters("lName").Value = lName & ""
            cmd.Parameters("storeCd").Value = storeCd & ""
            cmd.Parameters("addr").Value = addr & ""
            cmd.ExecuteNonQuery()

            returnVal = cmd.Parameters("returnVal").Value.ToString
            custCd = cmd.Parameters("custCd").Value.ToString
            errMsg = cmd.Parameters("errMsg").Value.ToString

            If "T".Equals(returnVal) AndAlso String.IsNullOrEmpty(errMsg) Then
                returnBool = True
            End If

        Catch ex As Exception
            Throw New Exception("Error in GenCustCd ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return returnBool

    End Function

    Private Shared Function CreateCustCodeLocal(ByVal cust_cd As String, ByVal format As String)

        Dim randObj As New Random
        Dim x As Integer
        Dim Start_Number As Integer
        Dim End_Number As Integer
        Dim test As Integer
        Dim CUSTOMER As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        CUSTOMER = "FALSE"

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        Do While CUSTOMER = "FALSE"
            If format = "A" Then    ' alpha-numeric
                If Len(cust_cd) <> 10 Then
                    Start_Number = 1
                    End_Number = 0
                    For x = 1 To (10 - Len(cust_cd))
                        Start_Number = Start_Number * 10
                    Next
                    test = 10 - Len(cust_cd)
                    End_Number = Replace(Start_Number, 0, 9)
                    End_Number = Microsoft.VisualBasic.Right(End_Number, test)
                    Start_Number = Microsoft.VisualBasic.Left(Start_Number, test)
                    CUSTOMER = UCase(cust_cd & (randObj.Next(Start_Number, End_Number).ToString()))
                Else : CUSTOMER = cust_cd
                End If
            Else
                Start_Number = 100000000
                End_Number = 0
                'For x = 1 To 10
                '    Start_Number = Start_Number * 10
                'Next
                test = 10
                End_Number = Replace(Start_Number, 0, 9)
                End_Number = Right(End_Number, test)
                Start_Number = Left(Start_Number, test)
                CUSTOMER = UCase((randObj.Next(Start_Number, End_Number).ToString()))
            End If
            sql = "SELECT CUST_CD FROM CUST WHERE CUST_CD='" & CUSTOMER & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If Len(cust_cd) = 10 Then
                        'CUSTOMER = txt_Cust_cd.Text
                        Exit Do
                    End If
                    CUSTOMER = "FALSE"
                Else
                    CUSTOMER = CUSTOMER
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop
        Return CUSTOMER

    End Function

    Public Shared Function Create_CUSTOMER_CODE(ByVal cust_cd As String, ByVal store_cd As String, ByVal addr1 As String,
                                                ByVal l_name As String, ByVal f_name As String)

        Dim cust_cd_new As String = ""
        Dim custCd As String = cust_cd
        Dim custGenApproach As String = ConfigurationManager.AppSettings("cust_cd_creation").ToString

        ' if set to use MM, then only can use if 11.1.8 and above, so default to 'A'lphaNum instead then
        If custGenApproach = "M" Then

            Dim Use_API As Boolean = False
            If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.CUST_GEN_API) Then
                Use_API = True
            End If

            If Not Use_API Then

                custGenApproach = "A" 'set to alpha-num - cannot use E1 api
            End If
        End If

        If custGenApproach = "M" Then

            Dim genSuccess As Boolean = GenCustCd(custCd, store_cd, addr1, l_name, f_name)

            If genSuccess Then
                cust_cd_new = custCd
            Else
                cust_cd_new = CreateCustCodeLocal(Left(l_name, 4) & Left(f_name, 1), "A")
            End If

        Else
            cust_cd_new = CreateCustCodeLocal(Left(l_name, 4) & Left(f_name, 1), custGenApproach) 'TODO what happens if feed in existing cust_cd
        End If

        Return UCase(cust_cd_new)

    End Function
End Class
