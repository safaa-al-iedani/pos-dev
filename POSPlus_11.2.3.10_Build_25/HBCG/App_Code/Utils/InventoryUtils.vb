﻿Imports Microsoft.VisualBasic
Imports System.Data.OracleClient
Imports System.Collections.Generic

Public Class InventoryUtils

    ''' <summary>
    ''' Makes sure that the data provided is complete to attempt a reservation of inventory
    ''' </summary>
    ''' <param name="resRequest">custom object with the data to be used when making a reservation call</param>
    ''' <returns>an empty string when data is valid; a message indicating the error found</returns>
    Public Shared Function AllowReservations(ByVal zoneCd As String, ByVal puDelStoreCd As String) As String

        Dim valMessage As String = String.Empty

        ' if priority fill is by zone, then the api will error w/o a zone code 
        If (SysPms.isPriorityFillByZone AndAlso zoneCd.isEmpty()) Then
            valMessage = Resources.POSErrors.ERR0004

        ElseIf (puDelStoreCd.isEmpty()) Then
            valMessage = Resources.POSErrors.ERR0010
        End If

        Return valMessage
    End Function

    ''' <summary>
    ''' Counts the column 'QTY' in the dataset passed in
    ''' </summary>
    ''' <param name="dataToCount">a dataset that includes a 'QTY' column to be counted</param>
    ''' <returns>sum of the 'QTY' column, or zero if no rows are found in the dataset</returns>
    Public Shared Function GetQtyCount(ByVal dataToCount As DataSet) As Integer

        Dim qtyCount As Integer = 0

        If SystemUtils.dataSetHasRows(dataToCount) AndAlso SystemUtils.dataRowIsNotEmpty(dataToCount.Tables(0).Rows(0)) Then

            Dim dTable As DataTable = dataToCount.Tables(0)
            For Each dRow In dTable.Rows
                qtyCount = qtyCount + CDbl(dRow("QTY").ToString)
            Next
        End If
        Return qtyCount

    End Function

    ''' <summary>
    ''' Substracts from the Inventory, the pending reservations (soft-reserve) and 
    ''' returns a dataset with the details of the available inventory
    ''' </summary>
    ''' <param name="inventorySet">the inventory counts for a given item</param>
    ''' <param name="reservationSet">reservations found for a given item</param>
    ''' <param name="invType">current types 'W' or 'S'</param>
    ''' <returns>A new dataset including only the inventory available</returns>
    Public Shared Function RemoveResCounts(ByVal inventorySet As DataSet,
                                           ByVal reservationSet As DataSet,
                                           ByVal invType As String) As DataSet
        'if NO reservations, no need to process
        If (reservationSet.Tables.Count = 0 OrElse reservationSet.Tables(0).Rows.Count = 0) Then
            Return inventorySet
        End If

        'if here, then reservations have to be processed
        Dim adjustedTable As DataTable = inventorySet.Tables(0).Clone
        Dim reservationRows() As DataRow
        Dim where As String = String.Empty
        Dim qtyAvailable As Double

        For Each invRow In inventorySet.Tables(0).Rows
            qtyAvailable = CDbl(invRow("QTY"))

            'Filters the Reservations based on the inventory type being adjusted
            If (AppConstants.Loc.TYPE_CD_W = invType) Then
                where = "locTp = 'W' AND store_cd = '" + invRow("STORE_CD") + "' AND loc_cd = '" + invRow("LOC_CD") + "'"
            ElseIf (AppConstants.Loc.TYPE_CD_S = invType) Then
                where = "locTp = 'S' AND store_cd = '" + invRow("STORE_CD") + "' AND loc_cd = '" + invRow("LOC_CD") + "'"
            End If
            reservationRows = reservationSet.Tables(0).Select(where)
            For i As Integer = 0 To reservationRows.GetUpperBound(0)
                Dim dr As DataRow = (reservationRows(i))
                qtyAvailable = qtyAvailable - CDbl(dr("QTY"))
            Next

            'if there is inventory left, after substracting the reservations
            If (qtyAvailable > 0) Then
                invRow("QTY") = qtyAvailable   'adjusts the qty
                adjustedTable.ImportRow(invRow)
            End If
        Next
        inventorySet.Tables.RemoveAt(0)           'removes the original table
        inventorySet.Tables.Add(adjustedTable)

        Return inventorySet
    End Function

    ''' <summary>
    ''' Creates an request object with the details needed to remove a SOFT reservation
    ''' </summary>
    ''' <param name="resId">the reservation id to delete records for</param>
    ''' <param name="itemCd">the item to remove the reservation for</param>
    ''' <param name="storeCd">the store code where the reservation exists</param>
    ''' <param name="locCd">the location code where the reservation exists</param>
    ''' <param name="qty">number of pieces to unreserve, if no number is given then unreserves full qty</param>
    ''' <returns>a custom object needed to remove a soft reservation</returns>
    Public Shared Function GetRequestForUnreserve(ByVal resId As String,
                                                  ByVal itemCd As String,
                                                  ByVal storeCd As String,
                                                  ByVal locCd As String,
                                                  Optional ByVal qty As String = "0.0") As UnResRequestDtc
        Dim request As UnResRequestDtc = New UnResRequestDtc
        request.resId = resId
        request.itemCd = itemCd
        request.qty = IIf(IsNumeric(qty) AndAlso CDbl(qty) > 0, CDbl(qty), 0)
        request.storeCd = storeCd
        request.locCd = locCd
        Return request

    End Function

    ''' <summary>
    ''' Creates an request object with the details needed to create a AUTO SOFT reservation.
    ''' </summary>
    ''' <param name="itemCd">the item code for the item being reserved</param>
    ''' <param name="qty">the number of items to be reserved</param>
    ''' <param name="puDel">either P or D to indicate the transportation method</param>
    ''' <param name="puDelStore">the pickup or delivery store</param>
    ''' <param name="zoneCd">zone code used for priority fill by zone</param>
    ''' <returns>a custom object needed to create soft reservations</returns>
    Public Shared Function GetRequestForAutoRes(ByVal itemCd As String,
                                                ByVal qty As String,
                                                ByVal puDel As String,
                                                ByVal puDelStore As String,
                                                ByVal zoneCd As String) As ResRequestDtc
        Dim request As ResRequestDtc = New ResRequestDtc
        request.isAutoSoftRes = True
        request.itemCd = UCase(itemCd)
        request.qty = CDbl(qty)
        request.puDel = UCase(puDel)
        request.puDelStoreCd = puDelStore
        request.zoneCd = zoneCd
        Return request

    End Function

    ''' <summary>
    ''' Creates an request object with the details needed to create a DIRECTED SOFT reservation.
    ''' </summary>
    ''' <param name="itemCd">the item code for the item being reserved</param>
    ''' <param name="qty">the number of items to be reserved</param>
    ''' <param name="storeCd">the store code where the reservation is requested</param>
    ''' <param name="locCd">the location code where the reservation is requested from</param>
    ''' <param name="puDel">either P or D to indicate the transportation method from</param>
    ''' <returns>a custom object needed to create soft reservations</returns>
    Public Shared Function GetRequestForDirectedRes(ByVal itemCd As String,
                                                    ByVal qty As String,
                                                    ByVal storeCd As String,
                                                    ByVal locCd As String,
                                                    ByVal puDel As String) As ResRequestDtc
        Dim request As New ResRequestDtc
        request.itemCd = UCase(itemCd)
        request.qty = CDbl(qty)
        request.storeCd = storeCd
        request.locCd = locCd
        request.puDel = UCase(puDel)
        Return request

    End Function

    ''' <summary>
    ''' Creates the object needed to Calculate availability with the details passed in
    ''' </summary>
    ''' <param name="itemCd">item code to calculate availability for</param>
    ''' <param name="qty">the item quantity</param>
    ''' <param name="reqDate">the P/D date where the item is scheduled</param>
    ''' <param name="storeCd">the written store code</param>
    ''' <param name="puDelStoreCd">the P/D store code</param>
    ''' <param name="zoneCd">the zone code</param>
    ''' <param name="puDel">either P or D, indicates if item is on Pickup or Delivery</param>
    ''' <param name="delDocNum">document number where this item is placed</param>
    ''' <param name="delDocLnNum">line number were this item is placed</param>
    ''' <returns>custom object populated with the details passed in</returns>
    Public Shared Function GetRequestForCalcAvail(ByVal itemCd As String,
                                                  ByVal qty As String,
                                                  ByVal reqDate As String,
                                                  ByVal storeCd As String,
                                                  ByVal puDelStoreCd As String,
                                                  ByVal zoneCd As String,
                                                  ByVal puDel As String,
                                                  ByVal delDocNum As String,
                                                  ByVal delDocLnNum As Integer) As CalcAvailRequestDtc
        Dim request As New CalcAvailRequestDtc
        request.itemCd = itemCd
        request.qty = CDbl(qty)
        request.reqDate = FormatDateTime(reqDate, DateFormat.ShortDate)
        request.storeCd = storeCd
        request.puDelStoreCd = puDelStoreCd
        request.puDel = puDel
        request.zoneCd = zoneCd
        request.delDocNum = delDocNum
        request.delDocLnNum = delDocLnNum
        Return request

    End Function

    ''' <summary>
    ''' Finds out the inventory type that corresponds to the serial type passed in
    ''' </summary>
    ''' <param name="serialTp">the serial type to be evaluated</param>
    ''' <returns> Y if value is a serial type;  N when not </returns>
    ''' <remarks></remarks>
    Public Shared Function GetRFInventoryType(ByVal serialTp As String) As String
        Return (IIf(IsSerialType(serialTp), "Y", "N"))
    End Function

    ''' <summary>
    ''' Finds out if the serial type passed in, corresponds to a valid serial type
    ''' </summary>
    ''' <param name="serialTp">the serial type to be evaluated</param>
    ''' <returns>TRUE if it is a valid serial type; FALSE otherwise</returns>
    Public Shared Function IsSerialType(ByVal serialTp As String) As Boolean

        Dim rtnVal As Boolean = False
        If (serialTp.isNotEmpty()) Then

            serialTp = UCase(serialTp)
            If (AppConstants.Inv.SERIAL_TP_F.Equals(serialTp) OrElse
                AppConstants.Inv.SERIAL_TP_L.Equals(serialTp) OrElse
                AppConstants.Inv.SERIAL_TP_S.Equals(serialTp) OrElse
                AppConstants.Inv.SERIAL_TP_T.Equals(serialTp)) Then

                rtnVal = True
            End If
        End If
        Return rtnVal

    End Function





    ''' <summary>
    ''' PO linke response for requesting PO link information for a sales document
    ''' </summary>
    ''' <remarks></remarks>
    Public Class PoLinkResponse

        Property poCd As String
        Property lnNum As String
    End Class

    ''' <summary>
    ''' Determines if the sales document has a line on a PO with quantity to be received yet (or specifically the line if provided)
    ''' If the line number is not provided and any line on the invoice is linked to a PO and there is still non-received qty, then PO key info
    '''    otherwise returns empty PO key
    ''' TESTING with lnNUM not done
    ''' <param name="docNum"></param>
    ''' <param name="oneRecReqd">Set to true if need only existance of one single row (for performance); false if need all potential return rows</param>
    ''' <param name="lnNum"></param>
    ''' <returns></returns>
    ''' </summary>
    Public Shared Function getPoInfo(ByVal docNum As String, Optional ByVal oneRecReqd As Boolean = False, Optional ByVal lnNum As String = "") As PoLinkResponse

        Dim poLink As PoLinkResponse

        If docNum.isNotEmpty Then

            Dim sqlStmtSb As New StringBuilder

            '--  get (qty ordered - qty order changed) minus (qty received - qty
            '--      received changed) (QOC and QRC values are positive or negative)
            sqlStmtSb.Append("SELECT sp.po_cd ")
            If SystemUtils.isNotEmpty(lnNum) Then
                sqlStmtSb.Append(", sp.ln# ")
            End If
            sqlStmtSb.Append("FROM so_ln$po_ln sp WHERE del_doc_num = :docNum ").Append(
                                "AND 0.0001 < (SELECT SUM(DECODE(po_actn_tp_cd, 'INI', NVL(qty,0), 'QOC', NVL(qty,0), 0)) ").Append(
                                                "- SUM(DECODE(po_actn_tp_cd, 'RCV', NVL(qty,0), 'QRC', NVL(qty,0), 0)) ").Append(
                                                "FROM po_ln$actn_hst pa WHERE pa.po_cd = sp.po_cd AND pa.ln# = sp.ln# ").Append(
                                                    "AND pa.po_actn_tp_cd IN ('INI', 'QOC', 'RCV', 'QRC') ) ")
            If SystemUtils.isNotEmpty(lnNum) Then
                sqlStmtSb.Append("AND del_doc_ln# = :lnNum ")
            End If
            If oneRecReqd Then
                sqlStmtSb.Append("AND rownum < 2 ")
            End If

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(sqlStmtSb.ToString, conn)

                cmd.Parameters.Add(":docNum", OracleType.VarChar)
                cmd.Parameters(":docNum").Value = UCase(docNum)
                If SystemUtils.isNotEmpty(lnNum) Then
                    cmd.Parameters.Add(":lnNum", OracleType.VarChar)
                    cmd.Parameters(":lnNum").Value = CInt(lnNum)
                End If
                Dim oraDatRdr As OracleDataReader

                Try
                    conn.Open()
                    oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)


                    If oraDatRdr.HasRows Then

                        oraDatRdr.Read()
                        poLink = New PoLinkResponse
                        poLink.poCd = oraDatRdr("PO_CD")
                        If SystemUtils.isNotEmpty(lnNum) Then
                            poLink.lnNum = oraDatRdr("LN#")
                        End If
                    End If

                Finally
                    oraDatRdr.Close()
                    oraDatRdr.Dispose()
                End Try

            End Using
        End If

        Return poLink
    End Function

    ' ''' <summary>
    ' ''' Determines for each SO number if the sales document has a line on a PO with quantity to be received yet (or specifically the line if provided)
    ' ''' This variation queries a whole list of docNums at once, to reduce roundtrips to Oracle and save time.  
    ' ''' It does not support the options to return more than one record nor to include the line number.
    ' ''' </summary>
    ' ''' <param name="docNums">List of del_doc_num</param>
    ' ''' <returns>A dictionary by del_doc_num with a PoLinkResponse for every SO that has such PO</returns>
    ' ''' <remarks></remarks>
    'Public Shared Function getPoInfo(ByVal docNums As List(Of String)) As Dictionary(Of String, PoLinkResponse)

    '    Dim poLinks As New Dictionary(Of String, PoLinkResponse)

    '    If docNums Is Nothing OrElse docNums.Count = 0 Then Return poLinks

    '    Dim poLink As PoLinkResponse

    '    If docNums.Count > 0 Then

    '        Dim inList As String
    '        Dim sqlStmtSb As New StringBuilder

    '        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
    '            cmd As New OracleCommand()

    '            inList = SystemUtils.BuildValueList(docNums, ":ddn", cmd)

    '            '--  get (qty ordered - qty order changed) minus (qty received - qty
    '            '--      received changed) (QOC and QRC values are positive or negative)
    '            sqlStmtSb.Append("SELECT sp.po_cd, del_doc_num ")
    '            sqlStmtSb.Append("FROM so_ln$po_ln sp WHERE del_doc_num IN ( ").Append(inList).Append(") ").Append(
    '                                "AND 0.0001 < (SELECT SUM(DECODE(po_actn_tp_cd, 'INI', NVL(qty,0), 'QOC', NVL(qty,0), 0)) ").Append(
    '                                                "- SUM(DECODE(po_actn_tp_cd, 'RCV', NVL(qty,0), 'QRC', NVL(qty,0), 0)) ").Append(
    '                                                "FROM po_ln$actn_hst pa WHERE pa.po_cd = sp.po_cd AND pa.ln# = sp.ln# ").Append(
    '                                                    "AND pa.po_actn_tp_cd IN ('INI', 'QOC', 'RCV', 'QRC') ) ").Append("AND rownum < 2 ")

    '            cmd.Connection = conn
    '            cmd.CommandText = sqlStmtSb.ToString

    '            Dim oraDatRdr As OracleDataReader

    '            Try
    '                conn.Open()
    '                oraDatRdr = cmd.ExecuteReader

    '                If oraDatRdr.HasRows Then

    '                    While oraDatRdr.Read()
    '                        poLink = New PoLinkResponse
    '                        poLink.poCd = oraDatRdr("PO_CD")
    '                        poLinks.Add(oraDatRdr("DEL_DOC_NUM"), poLink)
    '                    End While
    '                End If

    '            Finally
    '                oraDatRdr.Close()
    '                oraDatRdr.Dispose()
    '            End Try

    '        End Using
    '    End If

    '    Return poLinks
    'End Function

    ''' <summary>
    ''' Determines for each SO number if the sales document has a line on a PO with quantity to be received yet (or specifically the line if provided)
    ''' This variation queries a whole list of docNums at once, to reduce roundtrips to Oracle and save time.  
    ''' It cannot return more than one record/doc and does not support the option to include the line number.
    ''' </summary>
    ''' <param name="docNums">list of SO numbers to check</param>
    ''' <param name="nbrBoundParameters">how many bound parms to use in the IN ( ) list.  helps enable Oracle sharing. </param>
    ''' <returns>A SPARSE dictionary by del_doc_num with a PoLinkResponse for every SO that has such PO</returns>
    ''' <remarks>MM-9935</remarks>
    Public Shared Function getPoInfo(ByVal docNums As List(Of String), nbrBoundParams As Integer) As Dictionary(Of String, PoLinkResponse)

        Dim poLinks As New Dictionary(Of String, PoLinkResponse)

        If docNums IsNot Nothing AndAlso docNums.Count > 0 Then

            Dim poLink As PoLinkResponse
            Dim inList As String
            Dim sqlStmtSb As New StringBuilder

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand()

                inList = SystemUtils.BuildValueList(":ddn", cmd, nbrBoundParams)

                '--  get (qty ordered - qty order changed) minus (qty received - qty
                '--      received changed) (QOC and QRC values are positive or negative)
                sqlStmtSb.Append("SELECT sp.po_cd, del_doc_num ")
                sqlStmtSb.Append("FROM so_ln$po_ln sp WHERE del_doc_num IN ( ").Append(inList).Append(") ").Append(
                                    "AND 0.0001 < (SELECT SUM(DECODE(po_actn_tp_cd, 'INI', NVL(qty,0), 'QOC', NVL(qty,0), 0)) ").Append(
                                                    "- SUM(DECODE(po_actn_tp_cd, 'RCV', NVL(qty,0), 'QRC', NVL(qty,0), 0)) ").Append(
                                                    "FROM po_ln$actn_hst pa WHERE pa.po_cd = sp.po_cd AND pa.ln# = sp.ln# ").Append(
                                                        "AND pa.po_actn_tp_cd IN ('INI', 'QOC', 'RCV', 'QRC') ) ")

                cmd.Connection = conn
                cmd.CommandText = sqlStmtSb.ToString

                Dim oraDatRdr As OracleDataReader
                Dim a_doc_num As String
                Try
                    conn.Open()

                    For ix As Integer = 0 To docNums.Count - 1 Step nbrBoundParams
                        SystemUtils.FillMore(docNums, ":ddn", cmd, nbrBoundParams, ix)
                        oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)

                        If oraDatRdr.HasRows Then
                            While oraDatRdr.Read()
                                a_doc_num = oraDatRdr("DEL_DOC_NUM")
                                If Not poLinks.ContainsKey(a_doc_num) Then
                                    poLink = New PoLinkResponse
                                    poLink.poCd = oraDatRdr("PO_CD")
                                    poLinks.Add(a_doc_num, poLink)
                                Else
                                    ' if a need arises to return multiple results per doc, we could change 
                                    ' the return to a Dictionary(Of String, List(PoLinkResponse)), 
                                    ' and this is where we would extend an exisiting list rather 
                                    ' than adding a new entry to the results.
                                    ' or extend PoLinkResponse as a linked list
                                End If
                            End While
                        End If
                        oraDatRdr.Close()
                        oraDatRdr.Dispose()
                        oraDatRdr = Nothing
                    Next

                Finally
                    If oraDatRdr IsNot Nothing Then
                        oraDatRdr.Close()
                        oraDatRdr.Dispose()
                    End If
                End Try

            End Using
        End If

        Return poLinks
    End Function

    ''' <summary>
    ''' This is a basic E1 key format used for sales order, so_cmnt, IST's, etc.;  This info or a subset is used to make the del_doc_nums
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WrDtStoreSeqKey

        Property wrDt As Nullable(Of Date)
        Property storeCd As String
        Property seqNum As String
    End Class

    ''' <summary>
    ''' Determines if the IST has been scheduled on a truck
    ''' Returns true if the IST is scheduled on a truck (in TDM), false otherwise
    ''' </summary>
    Public Shared Function isIstSchedOnTruck(ByVal istKey As WrDtStoreSeqKey) As Boolean

        Dim istSched As Boolean = False

        If SystemUtils.isNotEmpty(istKey.seqNum) AndAlso
            SystemUtils.isNotEmpty(istKey.storeCd) AndAlso SystemUtils.isNotEmpty(istKey.wrDt.ToString) Then

            Dim sqlStmtSb As New StringBuilder
            sqlStmtSb.Append("SELECT 1 FROM trk_stop ts WHERE ts.ist_seq_num = :seqNum AND ts.ist_store_cd = :storeCd AND ts.ist_wr_dt = :wrDt ")

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(sqlStmtSb.ToString, conn)

                cmd.Parameters.Add(":seqNum", OracleType.VarChar)
                cmd.Parameters(":seqNum").Value = istKey.seqNum
                cmd.Parameters.Add(":storeCd", OracleType.VarChar)
                cmd.Parameters(":storeCd").Value = istKey.storeCd
                cmd.Parameters.Add(":wrDt", OracleType.DateTime)
                cmd.Parameters(":wrDt").Value = istKey.wrDt

                Dim oraDatRdr As OracleDataReader
                Try
                    conn.Open()
                    oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)


                    If oraDatRdr.HasRows AndAlso oraDatRdr.Read() Then

                        istSched = True
                    End If

                Finally
                    oraDatRdr.Close()
                    oraDatRdr.Dispose()
                End Try
            End Using

        End If

        Return istSched
    End Function

    ''' <summary>
    ''' Determines if the sales document has a line on an IST 
    ''' If the line number is not provided and any line on the invoice is linked to an IST, then returns the IST key
    '''    otherwise returns an empty key object
    ''' TESTING WITH lnNUM not done
    ''' </summary>
    ''' <param name="docNum"></param>
    ''' <param name="oneRecReqd">Set to true if need only existance of one single row (for performance); false if need all potential return rows</param>
    ''' <param name="lnNum"></param>
    Public Shared Function isOnIst(ByVal docNum As String, Optional ByVal oneRecReqd As Boolean = False, Optional ByVal lnNum As String = "") As WrDtStoreSeqKey

        Dim istKey As WrDtStoreSeqKey

        If SystemUtils.isNotEmpty(docNum) Then

            Dim sqlStmtSb As New StringBuilder

            sqlStmtSb.Append("SELECT i.ist_wr_dt, i.ist_store_cd, i.ist_seq_num, i.stat_cd ")
            If SystemUtils.isNotEmpty(lnNum) Then
                sqlStmtSb.Append(", il.ln# ")
            End If
            sqlStmtSb.Append(" FROM ")
            If SystemUtils.isNotEmpty(lnNum) Then
                sqlStmtSb.Append("ist_ln il, ")
            End If
            sqlStmtSb.Append(" ist i, ist_ln$so_ln iso WHERE ").Append(
                "i.ist_wr_dt = iso.ist_wr_dt AND i.ist_store_cd = iso.ist_store_cd AND i.ist_seq_num = iso.ist_seq_num ").Append(
                "AND iso.del_doc_num = :docNum ")
            If SystemUtils.isNotEmpty(lnNum) Then
                sqlStmtSb.Append("AND il.ist_wr_dt = iso.ist_wr_dt AND il.ist_store_cd = iso.ist_store_cd AND il.ist_seq_num = iso.ist_seq_num ").Append(
                    "AND i.ist_wr_dt = il.ist_wr_dt AND i.ist_store_cd = il.ist_store_cd AND i.ist_seq_num = il.ist_seq_num ").Append(
                    "AND il.ln# = iso.ist_ln# AND iso.del_doc_ln# = :lnNum ")
            End If
            If oneRecReqd Then
                sqlStmtSb.Append("AND rownum < 2 ")
            End If

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(sqlStmtSb.ToString, conn)

                cmd.Parameters.Add(":docNum", OracleType.VarChar)
                cmd.Parameters(":docNum").Value = docNum
                If SystemUtils.isNotEmpty(lnNum) Then
                    cmd.Parameters.Add(":lnNum", OracleType.VarChar)
                    cmd.Parameters(":lnNum").Value = CInt(lnNum)
                End If

                Dim oraDatRdr As OracleDataReader
                Try
                    conn.Open()
                    oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)


                    If oraDatRdr.HasRows Then
                        oraDatRdr.Read()

                        istKey = New WrDtStoreSeqKey
                        istKey.seqNum = oraDatRdr("IST_SEQ_NUM")
                        istKey.storeCd = oraDatRdr("IST_STORE_CD")
                        istKey.wrDt = oraDatRdr("IST_WR_DT")
                    End If
                Finally
                    oraDatRdr.Close()
                    oraDatRdr.Dispose()
                End Try
            End Using
        End If

        Return istKey
    End Function

    ''' <summary>
    ''' Determines for each id in the list, if the sales document has a line on an IST 
    ''' This variation is more efficient when there are many to check
    ''' But it does not support the options for multiple results or line number
    ''' </summary>
    ''' <param name="docNums">list of SO numbers to check</param>
    ''' <param name="nbrBoundParameters">how many use in the IN ( ) list.  helps Oracle sharing</param>
    ''' <returns>A SPARSE dictionary by del_doc_num with a WrDtStoreSeqKey for every SO that has such IST</returns>
    ''' <remarks>MM-9935</remarks>
    Public Shared Function isOnIst(ByVal docNums As List(Of String), nbrBoundParameters As Integer) As Dictionary(Of String, WrDtStoreSeqKey)

        Dim istKeys As New Dictionary(Of String, WrDtStoreSeqKey)

        If docNums IsNot Nothing AndAlso docNums.Count > 0 Then

            Dim istKey As WrDtStoreSeqKey
            Dim sqlStmtSb As New StringBuilder

            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand()

                Dim inList As String = SystemUtils.BuildValueList(":ddn", cmd, nbrBoundParameters)

                sqlStmtSb.Append("SELECT i.ist_wr_dt, i.ist_store_cd, i.ist_seq_num, i.stat_cd, iso.del_doc_num ")
                sqlStmtSb.Append(" FROM ")
                sqlStmtSb.Append(" ist i, ist_ln$so_ln iso WHERE ").Append(
                    "i.ist_wr_dt = iso.ist_wr_dt AND i.ist_store_cd = iso.ist_store_cd AND i.ist_seq_num = iso.ist_seq_num ").Append(
                    "AND iso.del_doc_num IN ( ").Append(inList).Append(")")

                cmd.Connection = conn
                cmd.CommandText = sqlStmtSb.ToString

                Dim oraDatRdr As OracleDataReader
                Dim a_doc_num As String
                Try
                    conn.Open()
                    For ix As Integer = 0 To docNums.Count - 1 Step nbrBoundParameters
                        SystemUtils.FillMore(docNums, ":ddn", cmd, nbrBoundParameters, ix)
                        oraDatRdr = DisposablesManager.BuildOracleDataReader(cmd)

                        If oraDatRdr.HasRows Then
                            While oraDatRdr.Read()
                                a_doc_num = oraDatRdr("DEL_DOC_NUM")
                                If Not istKeys.ContainsKey(a_doc_num) Then
                                    istKey = New WrDtStoreSeqKey
                                    istKey.seqNum = oraDatRdr("IST_SEQ_NUM")
                                    istKey.storeCd = oraDatRdr("IST_STORE_CD")
                                    istKey.wrDt = oraDatRdr("IST_WR_DT")
                                    istKeys.Add(a_doc_num, istKey)
                                Else
                                    ' if a need arises to return multiple results per doc, we could change 
                                    ' the return to a Dictionary(Of String, List(WrDtStoreSeqKey)), 
                                    ' and this is where we would extend an exisiting list rather 
                                    ' than adding a new entry to the results.
                                    ' or, extent the WrDtStoreSeqKey as a linked list itself
                                End If
                            End While
                        End If
                        oraDatRdr.Close()
                        oraDatRdr.Dispose()
                        oraDatRdr = Nothing
                    Next
                Finally
                    If oraDatRdr IsNot Nothing Then
                        oraDatRdr.Close()
                        oraDatRdr.Dispose()
                    End If
                End Try
            End Using
        End If

        Return istKeys
    End Function

    Public Class ItmTrnInsertReq  ' TODO or should this be the object?

        Property itmCd As String
        Property idCd As String
        Property crptIdNum As String
        Property serNum As String
        Property rfIdNew As String

        Property ittCd As String
        Property trnDt As DateTime
        Property qty As Double
        Property cst As Double
        Property fifoDt As String
        Property fiflDt As String

        Property oldStoreCd As String
        Property oldLocCd As String
        Property newStoreCd As String
        Property newLocCd As String
        Property delDocNum As String
        Property lnNum As String

        Property oldOutCd As String
        Property newOutCd As String
        Property postedToGl As String
        Property confLabels As String
        Property originCd As String
        Property empCdOp As String

    End Class

    Public Shared Sub InsertItmTrn(ByVal insReq As ItmTrnInsertReq, ByRef cmdx As OracleCommand)
        'Create the finalization transaction in the ITM_TRN table

        ' TODO - this process need to protect itself from invalid input

        Dim sqlStmtSb As New StringBuilder
        sqlStmtSb.Append("INSERT INTO ITM_TRN( itm_cd, id_cd, itt_cd, crpt_id_num, ser_num, rf_id_new, trn_dt, qty, cst, old_store_cd, old_loc_cd, ").Append(
                         "new_store_cd, new_loc_cd, del_doc_num, ln#, old_out_cd, new_out_cd, posted_to_gl, conf_labels, origin_cd, emp_cd_op ) ").Append(
                         "VALUES(:itmCd, :idCd, :ittCd, :crptIdNum, :serNum, :rfIdNew, :trnDt, :qty, :cst, :oldStoreCd, :oldLocCd, ").Append(
                         ":newStoreCd, :newLocCd, :delDocNum, :lnNum, :oldOutCd, :newOutCd, :postGl, :confLab, :origCd, :empCd ) ")

        Dim atomicTrn As Boolean = True
        Dim conn As OracleConnection
        Dim cmd As OracleCommand
        'If cmd Is Nothing Then
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        cmd = DisposablesManager.BuildOracleCommand(sqlStmtSb.ToString, conn)
        conn.Open()
        atomicTrn = False
        'Else  ' TODO - this is not tested
        'cmd.CommandText = sqlStmtSb.ToString
        'cmd.CommandType = CommandType.Text
        'cmd.Parameters.Clear()
        'End If

        cmd.Parameters.Add(":itmCd", OracleType.VarChar)
        cmd.Parameters(":itmCd").Value = insReq.itmCd
        cmd.Parameters.Add(":idCd", OracleType.VarChar)
        cmd.Parameters(":idCd").Value = IIf(insReq.idCd.isNotEmpty, insReq.idCd, System.DBNull.Value)

        cmd.Parameters.Add(":ittCd", OracleType.VarChar)
        cmd.Parameters(":ittCd").Value = insReq.ittCd
        cmd.Parameters.Add(":crptIdNum", OracleType.VarChar)
        cmd.Parameters(":crptIdNum").Value = IIf(insReq.crptIdNum.isNotEmpty, insReq.crptIdNum, System.DBNull.Value)
        cmd.Parameters.Add(":serNum", OracleType.VarChar)
        cmd.Parameters(":serNum").Value = IIf(insReq.serNum.isNotEmpty, insReq.serNum, System.DBNull.Value)
        cmd.Parameters.Add(":rfIdNew", OracleType.VarChar)
        cmd.Parameters(":rfIdNew").Value = IIf(insReq.rfIdNew.isNotEmpty, insReq.rfIdNew, System.DBNull.Value)

        cmd.Parameters.Add(":trnDt", OracleType.DateTime)
        cmd.Parameters(":trnDt").Value = insReq.trnDt
        cmd.Parameters.Add(":qty", OracleType.Number)
        cmd.Parameters(":qty").Value = insReq.qty
        cmd.Parameters.Add(":cst", OracleType.Number)
        cmd.Parameters(":cst").Value = insReq.cst

        cmd.Parameters.Add(":oldStoreCd", OracleType.VarChar)
        cmd.Parameters(":oldStoreCd").Value = IIf(insReq.oldStoreCd.isNotEmpty, insReq.oldStoreCd, System.DBNull.Value)
        cmd.Parameters.Add(":oldLocCd", OracleType.VarChar)
        cmd.Parameters(":oldLocCd").Value = IIf(insReq.oldLocCd.isNotEmpty, insReq.oldLocCd, System.DBNull.Value)
        cmd.Parameters.Add(":newStoreCd", OracleType.VarChar)
        cmd.Parameters(":newStoreCd").Value = IIf(insReq.newStoreCd.isNotEmpty, insReq.newStoreCd, System.DBNull.Value)
        cmd.Parameters.Add(":newLocCd", OracleType.VarChar)
        cmd.Parameters(":newLocCd").Value = IIf(insReq.newLocCd.isNotEmpty, insReq.newLocCd, System.DBNull.Value)
        cmd.Parameters.Add(":delDocNum", OracleType.VarChar)
        cmd.Parameters(":delDocNum").Value = IIf(insReq.delDocNum.isNotEmpty, insReq.delDocNum, System.DBNull.Value)
        cmd.Parameters.Add(":lnNum", OracleType.Number)
        cmd.Parameters(":lnNum").Value = IIf(insReq.lnNum.isNotEmpty, CInt(insReq.lnNum), System.DBNull.Value)

        cmd.Parameters.Add(":oldOutCd", OracleType.VarChar)
        cmd.Parameters(":oldOutCd").Value = IIf(insReq.oldOutCd.isNotEmpty, insReq.oldOutCd, System.DBNull.Value)
        cmd.Parameters.Add(":newOutCd", OracleType.VarChar)
        cmd.Parameters(":newOutCd").Value = IIf(insReq.newOutCd.isNotEmpty, insReq.newOutCd, System.DBNull.Value)
        cmd.Parameters.Add(":postGl", OracleType.VarChar)
        cmd.Parameters(":postGl").Value = IIf(insReq.postedToGl.isNotEmpty, insReq.postedToGl, System.DBNull.Value)
        cmd.Parameters.Add(":confLab", OracleType.VarChar)
        cmd.Parameters(":confLab").Value = IIf(insReq.confLabels.isNotEmpty, insReq.confLabels, System.DBNull.Value)
        cmd.Parameters.Add(":origCd", OracleType.VarChar)
        cmd.Parameters(":origCd").Value = IIf(insReq.originCd.isNotEmpty, insReq.originCd, System.DBNull.Value)
        cmd.Parameters.Add(":empCd", OracleType.VarChar)
        cmd.Parameters(":empCd").Value = IIf(insReq.empCdOp.isNotEmpty, insReq.empCdOp, System.DBNull.Value)

        Try
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()
        Finally
            If Not atomicTrn Then
                conn.Close()
            End If
        End Try
    End Sub

    Public Class Inv_Reserve_Req

        Property itm_Cd As String
        Property store_Cd As String
        Property loc_Cd As String
        Property qty As Double
        Property ser_Num As String
        Property out_Id As String
        Property out_Cd As String

        Property del_Doc_Num As String
        Property ln_Num As Integer
        Property pu_Del As String
        Property fifo_Cst As Double

    End Class

    Public Shared Sub Debit_Inventory(ByRef invResReq As Inv_Reserve_Req, ByRef cmd As OracleCommand)
        '    Public Shared Sub Debit_Inventory(ByVal qty As Double, ByVal del_doc_num As String, ByVal itm_cd As String, ByVal store_cd As String, ByVal loc_cd As String, ByVal ln_num As Double, pu_del As String, Optional ByVal fifo_cst As Double = 0, Optional ByVal ser_num As String = "", Optional ByVal out_id As String = "")
        '   Output out_cd

        Dim Use_API As Boolean = False
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.INV_API) Then
            Use_API = True
        End If

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim Mydatareader As OracleDataReader
        Dim tax_cd As String = ""
        Dim out_cd As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        If Use_API = True Then

            If invResReq.out_Id & "" <> "" Then
                If String.IsNullOrEmpty(invResReq.out_Cd + "") Then
                    out_cd = GetOutCd(invResReq.out_Id)
                    invResReq.out_Cd = out_cd
                Else
                    out_cd = invResReq.out_Cd
                End If
            End If

            cmd.Parameters.Clear()
            cmd.CommandText = "bt_order_inv_wrapper.reserve_inv"
            cmd.CommandType = CommandType.StoredProcedure
            objsql = cmd
            'sql = "bt_order_inv_wrapper.reserve_inv"
            'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            'objsql.CommandType = CommandType.StoredProcedure

            objsql.Parameters.Add("pu_del_flg_i", OracleType.VarChar, 1).Direction = ParameterDirection.Input
            objsql.Parameters.Add("tran_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            objsql.Parameters.Add("origin_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("emp_cd_op_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("src_tp_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("del_doc_ln#_i", OracleType.Number).Direction = ParameterDirection.Input
            objsql.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
            objsql.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("out_id_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("out_cd_io", OracleType.VarChar, 3).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("id_num_io", OracleType.VarChar, 10).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("ser_num_io", OracleType.VarChar, 30).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("ooc_qty_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("fifo_cst_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("comm_cd_io", OracleType.VarChar, 3).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("spiff_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("picked_o", OracleType.VarChar, 1).Direction = ParameterDirection.Output
            objsql.Parameters.Add("fill_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            objsql.Parameters.Add("fifl_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            objsql.Parameters.Add("fifo_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            objsql.Parameters.Add("process_stat_o", OracleType.Number).Direction = ParameterDirection.Output
            objsql.Parameters.Add("err_cd_o", OracleType.Number).Direction = ParameterDirection.Output
            objsql.Parameters.Add("err_msg_o", OracleType.VarChar, 512).Direction = ParameterDirection.Output

            objsql.Parameters("pu_del_flg_i").Value = invResReq.pu_Del
            objsql.Parameters("tran_dt_i").Value = FormatDateTime(Today.Date, DateFormat.ShortDate)
            objsql.Parameters("origin_cd_i").Value = "POS"
            objsql.Parameters("emp_cd_op_i").Value = HttpContext.Current.Session("emp_cd")
            objsql.Parameters("src_tp_i").Value = "S"
            objsql.Parameters("del_doc_num_i").Value = invResReq.del_Doc_Num
            objsql.Parameters("del_doc_ln#_i").Value = CInt(invResReq.ln_Num)
            objsql.Parameters("itm_cd_i").Value = invResReq.itm_Cd
            objsql.Parameters("qty_i").Value = invResReq.qty
            objsql.Parameters("taken_with_i").Value = "N"  ' TODO - this should reflect the line info of taken-with for split with TW or cash-and-carry so doesn't err if no inv
            objsql.Parameters("store_cd_i").Value = invResReq.store_Cd
            objsql.Parameters("loc_cd_i").Value = invResReq.loc_Cd
            objsql.Parameters("out_id_cd_i").Value = invResReq.out_Id
            objsql.Parameters("out_cd_io").Value = out_cd
            objsql.Parameters("id_num_io").Value = DBNull.Value
            objsql.Parameters("ser_num_io").Value = invResReq.ser_Num
            objsql.Parameters("ooc_qty_io").Value = 0
            objsql.Parameters("fifo_cst_io").Value = invResReq.fifo_Cst
            objsql.Parameters("comm_cd_io").Value = DBNull.Value
            objsql.Parameters("spiff_io").Value = 0
            objsql.Parameters("picked_o").Value = System.DBNull.Value
            objsql.Parameters("fill_dt_o").Value = System.DBNull.Value
            objsql.Parameters("fifl_dt_o").Value = System.DBNull.Value
            objsql.Parameters("fifo_dt_o").Value = System.DBNull.Value
            objsql.Parameters("process_stat_o").Value = System.DBNull.Value
            objsql.Parameters("err_cd_o").Value = System.DBNull.Value
            objsql.Parameters("err_msg_o").Value = System.DBNull.Value

            Try
                objsql.ExecuteNonQuery()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If objsql.Parameters("err_cd_o").Value.ToString & "" <> "" Then
                Dim int_err As String = objsql.Parameters("err_cd_o").Value.ToString   'lucy
                Dim str_err As String = Left(objsql.Parameters("err_msg_o").Value, 50) 'lucy
                ' TODO - change this so that every inventory message does not change to insufficient inventory
                '   BE AWARE THAT THE ORDER_STAGE IS USING 'Insufficient' to validate inventory update succeeded - argh!
                Dim viewer As String = objsql.Parameters("err_cd_o").Value.ToString & ""
                'lucy comment ' HttpContext.Current.Session("err") = HttpContext.Current.Session("err") & "Insufficient inventory for SKU " & invResReq.itm_Cd & " in location " & invResReq.store_Cd & "/" & invResReq.loc_Cd & "." & vbCrLf
                HttpContext.Current.Session("err") = str_err & " " & invResReq.itm_Cd & " in location " & invResReq.store_Cd & "/" & invResReq.loc_Cd & "." & vbCrLf   'lucy
                'lucy comment above  Session("err") origin one and add new one to show real err
                'conn.Close()  TODO DSA - test this
                Exit Sub
            End If

            invResReq.fifo_Cst = Replace(FormatNumber(objsql.Parameters("fifo_cst_io").Value, 2), ",", "")
            '
            'Update the SO_LN table
            '
            Try
                sql = "UPDATE SO_LN SET STORE_CD='" & invResReq.store_Cd & "', "
                sql = sql & "PICKED='F', " & " SER_NUM='" & invResReq.ser_Num & "', "
                sql = sql & "LOC_CD='" & invResReq.loc_Cd & "', "
                If Not IsNumeric(invResReq.fifo_Cst) Then
                    sql = sql & "FIFO_CST=0, "
                Else
                    sql = sql & "FIFO_CST=" & invResReq.fifo_Cst & ", "
                End If
                If invResReq.out_Id & "" <> "" Then
                    sql = sql & "OUT_ID_CD='" & invResReq.out_Id & "', "
                    sql = sql & "OUT_CD='" & out_cd & "', "
                End If
                sql = sql & "FIFO_DT=TO_DATE('" & objsql.Parameters("fifo_dt_o").Value & "','mm/dd/RRRR'), "
                sql = sql & "FIFL_DT=TO_DATE('" & objsql.Parameters("fifl_dt_o").Value & "','mm/dd/RRRR'), "
                sql = sql & "OOC_QTY=" & objsql.Parameters("ooc_qty_io").Value & ", "
                sql = sql & "FILL_DT=TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
                sql = sql & "WHERE DEL_DOC_LN#=" & invResReq.ln_Num & " AND "
                sql = sql & "DEL_DOC_NUM='" & invResReq.del_Doc_Num & "' "

                cmd.Parameters.Clear()
                cmd.CommandText = sql
                cmd.CommandType = CommandType.Text
                objsql = cmd
                'objsql= DisposablesManager.BuildOracleCommand(sql, conn)
                objsql.ExecuteNonQuery()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            conn.Close()

        Else

            Dim Rcv_Date As String = ""
            Dim FIFO_Rcv_Date As String = ""
            Dim itm_cst As Double = 0
            Dim itm_qty As Double = 0
            Dim fifo_qty As Double = 0
            Dim ser_tp As String = ""
            Dim bulk_tp_itm As String = ""

            invResReq.store_Cd = UCase(invResReq.store_Cd)
            invResReq.loc_Cd = UCase(invResReq.loc_Cd)

            sql = "SELECT NVL(SERIAL_TP,'N') AS SERIAL_TP, NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM WHERE ITM_CD='" & invResReq.itm_Cd & "'"
            sql = UCase(sql)

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    ser_tp = Mydatareader.Item("SERIAL_TP").ToString
                    bulk_tp_itm = Mydatareader.Item("BULK_TP_ITM").ToString
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            ser_tp = GetRFInventoryType(ser_tp)

            sql = "SELECT ITM_FIFL.ITM_CD, ITM_FIFL.QTY, ITM_FIFL.STORE_CD, ITM_FIFL.RCV_DT "
            sql = sql & "FROM ITM_FIFL WHERE ITM_FIFL.STORE_CD='" & invResReq.store_Cd & "' "
            sql = sql & "AND ITM_FIFL.QTY >=" & invResReq.qty & " "
            sql = sql & "AND ITM_FIFL.ITM_CD='" & invResReq.itm_Cd & "' "
            sql = sql & "AND ITM_FIFL.LOC_CD='" & invResReq.loc_Cd & "' "
            sql = sql & "ORDER BY ITM_FIFL.RCV_DT ASC"
            sql = UCase(sql)

            'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Clear()
            cmd.CommandText = sql
            cmd.CommandType = CommandType.Text

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(cmd)

                If Mydatareader.Read Then
                    If IsDate(Mydatareader.Item("RCV_DT").ToString) Then
                        Rcv_Date = FormatDateTime(Mydatareader.Item("RCV_DT").ToString, DateFormat.ShortDate)
                    End If
                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        itm_qty = Mydatareader.Item("QTY").ToString
                    End If
                Else
                    conn.Close()
                    HttpContext.Current.Session("err") = HttpContext.Current.Session("err") & "Insufficient inventory for SKU " & invResReq.itm_Cd & " in location " & invResReq.store_Cd & "/" & invResReq.loc_Cd & "." & vbCrLf
                    Exit Sub
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update ITM_FIFL
            '
            If itm_qty > invResReq.qty Then
                Try
                    'Update the FIFL table
                    sql = "UPDATE ITM_FIFL SET QTY=QTY-" & invResReq.qty & " WHERE "
                    sql = sql & "ITM_FIFL.STORE_CD='" & invResReq.store_Cd & "' "
                    sql = sql & "AND ITM_FIFL.RCV_DT=TO_DATE('" & Rcv_Date & "','mm/dd/RRRR') "
                    sql = sql & "AND ITM_FIFL.ITM_CD='" & invResReq.itm_Cd & "' "
                    sql = sql & "AND ITM_FIFL.LOC_CD='" & invResReq.loc_Cd & "' "
                    'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    'objsql.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            ElseIf itm_qty = invResReq.qty Then
                Try
                    'Update the FIFL table
                    sql = "DELETE FROM ITM_FIFL WHERE "
                    sql = sql & "ITM_FIFL.STORE_CD='" & invResReq.store_Cd & "' "
                    sql = sql & "AND ITM_FIFL.RCV_DT=TO_DATE('" & Rcv_Date & "','mm/dd/RRRR') "
                    sql = sql & "AND ITM_FIFL.ITM_CD='" & invResReq.itm_Cd & "' "
                    sql = sql & "AND ITM_FIFL.LOC_CD='" & invResReq.loc_Cd & "' "
                    'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    'objsql.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Else
                'Not enough inventory, we need to EXIT
                conn.Close()
                Exit Sub
            End If

            '
            'UPDATE FIFO...CHANGE HERE
            '
            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                sql = "SELECT ITM_FIFO.ITM_CD, ITM_FIFO.QTY, ITM_FIFO.CST, ITM_FIFO.RCV_DT "
                sql = sql & "FROM ITM_FIFO "
                sql = sql & "WHERE ITM_FIFO.STORE_CD='" & invResReq.store_Cd & "' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & invResReq.itm_Cd & "' "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT ASC, CST ASC"
            Else
                sql = "SELECT ITM_FIFO.ITM_CD, ITM_FIFO.QTY, ITM_FIFO.CST, ITM_FIFO.RCV_DT "
                sql = sql & "FROM ITM_FIFO "
                sql = sql & "WHERE ITM_FIFO.STORE_CD='00' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & invResReq.itm_Cd & "' "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT ASC, CST ASC"
            End If
            sql = UCase(sql)

            'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Clear()
            cmd.CommandText = sql
            cmd.CommandType = CommandType.Text

            Dim FIFO_QTY_ONGOING As Double = 0
            Dim FIFO_COST_ONGOING As Double = 0
            Dim Loop_qty As Double = invResReq.qty
            FIFO_Rcv_Date = ""

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(cmd)

                Do While Mydatareader.Read
                    itm_cst = 0
                    fifo_qty = 0
                    itm_qty = 0

                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        fifo_qty = Mydatareader.Item("QTY").ToString
                    End If
                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        itm_qty = Mydatareader.Item("QTY").ToString
                    End If

                    If Loop_qty > 0 Then
                        If IsNumeric(Mydatareader.Item("CST").ToString) Then
                            itm_cst = Mydatareader.Item("CST").ToString
                        End If
                        If IsDate(Mydatareader.Item("RCV_DT").ToString) Then
                            FIFO_Rcv_Date = FormatDateTime(Mydatareader.Item("RCV_DT").ToString, DateFormat.ShortDate)
                        End If

                        If Loop_qty < itm_qty Then itm_qty = Loop_qty
                        FIFO_COST_ONGOING = FIFO_COST_ONGOING + (itm_cst * itm_qty)
                        FIFO_QTY_ONGOING = FIFO_QTY_ONGOING + itm_qty

                        If fifo_qty > Loop_qty Then
                            sql = "UPDATE ITM_FIFO SET QTY=QTY-" & Loop_qty & " WHERE "
                            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                                sql = sql & "ITM_FIFO.STORE_CD='" & invResReq.store_Cd & "' "
                            Else
                                sql = sql & "ITM_FIFO.STORE_CD='00' "
                            End If
                            sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & FIFO_Rcv_Date & "','mm/dd/RRRR') "
                            sql = sql & "AND ITM_FIFO.ITM_CD='" & invResReq.itm_Cd & "' "
                            sql = sql & "AND ITM_FIFO.CST=" & itm_cst & " "
                            'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                            'objsql.ExecuteNonQuery()
                            cmd.Parameters.Clear()
                            cmd.CommandText = sql
                            cmd.CommandType = CommandType.Text
                            cmd.ExecuteNonQuery()

                        ElseIf fifo_qty <= Loop_qty Then
                            sql = "DELETE FROM ITM_FIFO WHERE "
                            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                                sql = sql & "ITM_FIFO.STORE_CD='" & invResReq.store_Cd & "' "
                            Else
                                sql = sql & "ITM_FIFO.STORE_CD='00' "
                            End If
                            sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & FIFO_Rcv_Date & "','mm/dd/RRRR') "
                            sql = sql & "AND ITM_FIFO.ITM_CD='" & invResReq.itm_Cd & "' "
                            sql = sql & "AND ITM_FIFO.CST=" & itm_cst & " "
                            'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                            'objsql.ExecuteNonQuery()
                            cmd.Parameters.Clear()
                            cmd.CommandText = sql
                            cmd.CommandType = CommandType.Text
                            cmd.ExecuteNonQuery()
                        End If
                        Loop_qty = Loop_qty - fifo_qty
                    Else
                        Exit Do
                    End If
                Loop
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try


            '
            'Update the SO_LN table
            '
            Try
                sql = "UPDATE SO_LN SET STORE_CD='" & invResReq.store_Cd & "', "
                sql = sql & "PICKED='F', "
                sql = sql & "LOC_CD='" & invResReq.loc_Cd & "', "
                sql = sql & "SER_NUM='" & invResReq.ser_Num & "', "
                If Not IsNumeric(Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")) Or Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "") = "NaN" Then
                    sql = sql & "FIFO_CST=0, "
                Else
                    sql = sql & "FIFO_CST=" & Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "") & ", "
                End If
                sql = sql & "FIFO_DT=TO_DATE('" & FIFO_Rcv_Date & "','mm/dd/RRRR'), "
                sql = sql & "FIFL_DT=TO_DATE('" & Rcv_Date & "','mm/dd/RRRR'), "
                sql = sql & "FILL_DT=TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
                sql = sql & "WHERE DEL_DOC_LN#=" & invResReq.ln_Num & " AND "
                sql = sql & "DEL_DOC_NUM='" & invResReq.del_Doc_Num & "' "
                'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                'objsql.ExecuteNonQuery()
                cmd.Parameters.Clear()
                cmd.CommandText = sql
                cmd.CommandType = CommandType.Text
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update the ITM_TRN table
            '
            Try
                sql = "INSERT INTO ITM_TRN(ITT_CD, ITM_CD, TRN_DT, OLD_STORE_CD, OLD_LOC_CD, QTY, CST, DEL_DOC_NUM, "
                sql = sql & "POSTED_TO_GL, CONF_LABELS, ORIGIN_CD, EMP_CD_OP) "
                sql = sql & "VALUES('OOR','" & invResReq.itm_Cd & "',TO_DATE('" & Today.Date & "','mm/dd/RRRR')"
                sql = sql & ",'" & invResReq.store_Cd & "','" & invResReq.loc_Cd & "'," & invResReq.qty & ","
                If Not IsNumeric(Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")) Or Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "") = "NaN" Then
                    sql = sql & "0"
                Else
                    sql = sql & Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")
                End If
                sql = sql & ",'" & invResReq.del_Doc_Num & "','N','N','POS'"
                sql = sql & ",'" & HttpContext.Current.Session("EMP_CD") & "')"
                'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                'objsql.ExecuteNonQuery()
                cmd.Parameters.Clear()
                cmd.CommandText = sql
                cmd.CommandType = CommandType.Text
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update the INV_XREF tables
            '
            If ConfigurationManager.AppSettings("enable_serialization") = "Y" And ser_tp = "Y" Then
                Try
                    sql = "SELECT INV_XREF.RF_ID_CD, INV_XREF.SER_NUM "
                    sql = sql & "FROM INV_XREF, ITM WHERE "
                    sql = sql & "INV_XREF.STORE_CD='" & invResReq.store_Cd & "' "
                    sql = sql & "AND INV_XREF.ITM_CD='" & invResReq.itm_Cd & "' "
                    sql = sql & "AND INV_XREF.LOC_CD='" & invResReq.loc_Cd & "' "
                    sql = sql & "AND INV_XREF.DISPOSITION='FIF' "
                    If invResReq.ser_Num & "" <> "" Then
                        sql = sql & "AND INV_XREF.SER_NUM='" & invResReq.ser_Num & "' "
                    End If
                    sql = sql & "ORDER BY INV_XREF.CREATE_DT "

                    'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    cmd.Parameters.Clear()
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()

                    Mydatareader = DisposablesManager.BuildOracleDataReader(cmd)

                    If Mydatareader.Read Then
                        If invResReq.ser_Num & "" = "" And Mydatareader.Item("SER_NUM").ToString & "" <> "" Then
                            invResReq.ser_Num = Mydatareader.Item("SER_NUM").ToString
                        End If

                        sql = "UPDATE INV_XREF SET DISPOSITION='RES', "
                        sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                        sql = sql & "DEL_DOC_NUM='" & invResReq.del_Doc_Num & "', "
                        sql = sql & "DEL_DOC_LN#=" & invResReq.ln_Num & ", "
                        sql = sql & "LAST_ACTN_DT=SYSDATE "
                        sql = sql & "WHERE RF_ID_CD='" & Mydatareader.Item("RF_ID_CD").ToString & "'"
                        'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                        'objsql.ExecuteNonQuery()
                        cmd.Parameters.Clear()
                        cmd.CommandText = sql
                        cmd.CommandType = CommandType.Text
                        cmd.ExecuteNonQuery()
                    End If

                    Mydatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            ElseIf ConfigurationManager.AppSettings("xref") = "Y" Then
                If bulk_tp_itm <> "Y" Then
                    Dim x As Integer = 0
                    sql = "SELECT INV_XREF.RF_ID_CD "
                    sql = sql & "FROM INV_XREF, ITM WHERE "
                    sql = sql & "INV_XREF.STORE_CD='" & invResReq.store_Cd & "' "
                    sql = sql & "AND INV_XREF.ITM_CD='" & invResReq.itm_Cd & "' "
                    sql = sql & "AND INV_XREF.LOC_CD='" & invResReq.loc_Cd & "' "
                    sql = sql & "AND INV_XREF.DISPOSITION='FIF' "
                    sql = sql & "AND ITM.ITM_CD=INV_XREF.ITM_CD "
                    sql = sql & "AND NVL(ITM.BULK_TP_ITM,'N')='N' "
                    sql = sql & "ORDER BY INV_XREF.CREATE_DT "

                    'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    cmd.Parameters.Clear()
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()

                    Try
                        Mydatareader = DisposablesManager.BuildOracleDataReader(cmd)

                        Do While Mydatareader.Read
                            If x < invResReq.qty Then
                                sql = "UPDATE INV_XREF SET DISPOSITION='RES', "
                                sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                                sql = sql & "DEL_DOC_NUM='" & invResReq.del_Doc_Num & "', "
                                sql = sql & "DEL_DOC_LN#=" & invResReq.ln_Num & ", "
                                sql = sql & "LAST_ACTN_DT=SYSDATE "
                                sql = sql & "WHERE RF_ID_CD='" & Mydatareader.Item("RF_ID_CD").ToString & "'"
                                'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                                'objsql.ExecuteNonQuery()
                                cmd.Parameters.Clear()
                                cmd.CommandText = sql
                                cmd.CommandType = CommandType.Text
                                cmd.ExecuteNonQuery()
                            Else
                                Exit Do
                            End If
                            x = x + 1
                        Loop
                    Catch
                        conn.Close()
                        Throw
                    End Try
                End If

            End If
        End If

        conn.Close()

    End Sub

    ''THIS METHOD CAN BE MOVED TO INVENTORY-DAC ONCE DEBIT_INVENTORY IS MOVED TO THE BIZ-DAC classes
    Public Shared Function GetOutCd(ByVal out_id As String) As String

        Dim sql As String = "SELECT out_cd FROM out " &
            "WHERE ID_CD='" & out_id & "'"

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim rdr As OracleDataReader

        Try
            conn.Open()

            rdr = DisposablesManager.BuildOracleDataReader(cmd)

            If rdr.Read() Then
                GetOutCd = rdr.Item("OUT_CD").ToString
            End If
            rdr.Close()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return GetOutCd

    End Function


End Class
