Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Data.OracleClient

Public Class Email_Functions

    Public Shared Sub SendMailMessage(ByVal filename As String)

        ' Instantiate a new instance of MailMessage
        Dim mMailMessage As New MailMessage()
        Dim from As String = ""
        Dim recepient As String = ""
        Dim cc As String = ""
        Dim bcc As String = ""
        Dim subject As String = ""
        Dim body As String = ""

        If filename & "" <> "" Then
            mMailMessage.Attachments.Add(New System.Net.Mail.Attachment(filename))
        End If

        ' Create a connection.
        Dim Connection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Connection.Open()

        Dim sql As String
        sql = "SELECT * FROM EMAIL_TEMPLATES WHERE SESSION_ID='" & HttpContext.Current.Session.SessionID.ToString & "'"

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        ' Create a data adapter and a dataset.
        objSql = DisposablesManager.BuildOracleCommand(sql, Connection)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read() Then
                from = MyDataReader.Item("EMAIL_FROM").ToString
                recepient = MyDataReader.Item("EMAIL_TO").ToString
                cc = MyDataReader.Item("EMAIL_CC").ToString
                bcc = MyDataReader.Item("EMAIL_BCC").ToString
                subject = MyDataReader.Item("EMAIL_SUBJECT").ToString
                body = MyDataReader.Item("EMAIL_BODY").ToString
            Else
                Connection.Close()
                Exit Sub
            End If

            'Close Connection 
            MyDataReader.Close()
            sql = "DELETE FROM EMAIL_TEMPLATES WHERE SESSION_ID='" & HttpContext.Current.Session.SessionID.ToString & "'"

            ' Create a data adapter and a dataset.
            objSql = DisposablesManager.BuildOracleCommand(sql, Connection)
            objSql.ExecuteNonQuery()

            Connection.Close()
        Catch ex As Exception
            Connection.Close()
            Throw
        End Try

        Try
            If Right(from, 1) = "," Then from = Left(from, Len(from) - 1)
            If Right(recepient, 1) = "," Then recepient = Left(recepient, Len(recepient) - 1)
            If Right(cc, 1) = "," Then cc = Left(cc, Len(cc) - 1)
            If Right(bcc, 1) = "," Then bcc = Left(bcc, Len(bcc) - 1)

            Dim x As Double

            ' Set the sender address of the mail message
            mMailMessage.From = New MailAddress(from)

            ' Set the recepient address of the mail message
            recepient = Replace(recepient, ";", ",").Trim
            If InStr(recepient, ",") > 0 Then
                Dim MsgArray As Array
                MsgArray = Split(recepient, ",")
                For x = LBound(MsgArray) To UBound(MsgArray)
                    mMailMessage.To.Add(New MailAddress(MsgArray(x)))
                Next
            Else
                mMailMessage.To.Add(New MailAddress(recepient))
            End If

            ' Check if the bcc value is nothing or an empty string
            If Not bcc Is Nothing And bcc <> String.Empty Then
                ' Set the Bcc address of the mail message
                bcc = Replace(bcc, ";", ",").Trim
                If InStr(bcc, ",") > 0 Then
                    Dim MsgArray As Array
                    MsgArray = Split(bcc, ",")
                    For x = LBound(MsgArray) To UBound(MsgArray)
                        mMailMessage.Bcc.Add(New MailAddress(MsgArray(x)))
                    Next
                Else
                    mMailMessage.Bcc.Add(New MailAddress(bcc))
                End If
            End If

            ' Check if the cc value is nothing or an empty value
            If Not cc Is Nothing And cc <> String.Empty Then
                ' Set the CC address of the mail message
                cc = Replace(cc, ";", ",").Trim
                If InStr(cc, ",") > 0 Then
                    Dim MsgArray As Array
                    MsgArray = Split(cc, ",")
                    For x = LBound(MsgArray) To UBound(MsgArray)
                        mMailMessage.CC.Add(New MailAddress(MsgArray(x)))
                    Next

                Else
                    mMailMessage.CC.Add(New MailAddress(cc))
                End If
            End If

            ' Set the subject of the mail message
            mMailMessage.Subject = subject
            ' Set the body of the mail message
            mMailMessage.Body = Replace(body, Chr(10), "<br />")

            ' Set the format of the mail message body as HTML
            mMailMessage.IsBodyHtml = True
            ' Set the priority of the mail message to normal
            mMailMessage.Priority = MailPriority.Normal

            ' Instantiate a new instance of SmtpClient
            Dim mSmtpClient As New SmtpClient()
            ' Send the mail message
            mSmtpClient.Send(mMailMessage)
        Catch ex As Exception
            Log_Email_Transaction(from, recepient, bcc, cc, subject, body, Err.Description)
            Throw
            Exit Sub
        End Try

        Log_Email_Transaction(from, recepient, bcc, cc, subject, body)

    End Sub

    Public Shared Sub Log_Email_Transaction(ByVal from As String, ByVal recepient As String, ByVal bcc As String, ByVal cc As String, ByVal subject As String, ByVal body As String, Optional ByVal err_msg As String = "")

        Try
            Err.Clear()
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            Dim objSql As OracleCommand
            sql = "INSERT INTO EMAIL_AUDIT (EMP_CD, EMAIL_TO, EMAIL_FROM, EMAIL_CC, EMAIL_BCC, EMAIL_SUBJECT, EMAIL_BODY, ERR_MESSAGE) "
            sql = sql & "VALUES(:EMP_CD, :EMAIL_TO, :EMAIL_FROM, :EMAIL_CC, :EMAIL_BCC, :EMAIL_SUBJECT, :EMAIL_BODY, :ERR_MESSAGE)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":EMP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_TO", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_FROM", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_CC", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_BCC", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_SUBJECT", OracleType.VarChar)
            objSql.Parameters.Add(":EMAIL_BODY", OracleType.Clob)
            objSql.Parameters.Add(":ERR_MESSAGE", OracleType.VarChar)

            objSql.Parameters(":EMP_CD").Value = HttpContext.Current.Session("emp_cd")
            objSql.Parameters(":EMAIL_TO").Value = recepient
            objSql.Parameters(":EMAIL_FROM").Value = from
            objSql.Parameters(":EMAIL_CC").Value = cc
            objSql.Parameters(":EMAIL_BCC").Value = bcc
            objSql.Parameters(":EMAIL_SUBJECT").Value = subject
            objSql.Parameters(":EMAIL_BODY").Value = body
            objSql.Parameters(":ERR_MESSAGE").Value = err_msg
            objSql.ExecuteNonQuery()

            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

End Class
