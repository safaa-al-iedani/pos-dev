Imports System
Imports System.Data.OracleClient
Imports System.Net.Sockets
Imports HBCG_Utils

Public Class SD_Utils

    'Third-party authorization types
    Public Const AUTH_ONLY As String = "01"     'used only by PL 
    Public Const AUTH_SALE As String = "02"     'used for PL & CC
    Public Const AUTH_PRIORAUTH As String = "07"
    Public Const AUTH_REFUND As String = "09"
    Public Const AUTH_VOID As String = "11" 'used for a CC partial auth only
    Public Const AUTH_ECA As String = "21"
    Public Const AUTH_INQUIRY As String = "22"
    Public Const AUTH_OTB As String = "24"

    Public Shared Sub Update_QS_Status(ByVal cust_cd As String, ByVal status As String, ByVal auth_no As String, ByVal store_cd As String)

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand
        Dim mydatareader1 As OracleDataReader
        Dim sql As String

        objConnection.Open()

        sql = "SELECT CUST_CD FROM QS_AUDIT WHERE CUST_CD='" & cust_cd & "' AND EXPIRATION_DT > TO_DATE('" & FormatDateTime(Now.Date, DateFormat.ShortDate)
        sql = sql & "','mm/dd/RRRR')"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
        mydatareader1 = DisposablesManager.BuildOracleDataReader(objsql2)

        If mydatareader1.HasRows Then
            sql = "UPDATE QS_AUDIT SET QS_STATUS=:QS_STATUS, EMP_CD=:EMP_CD, LAST_UPDATE=SYSDATE WHERE CUST_CD='" & cust_cd & "'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)

            objsql2.Parameters.Add(":QS_STATUS", OracleType.VarChar)
            objsql2.Parameters(":QS_STATUS").Value = status
            objsql2.Parameters.Add(":EMP_CD", OracleType.VarChar)
            objsql2.Parameters(":EMP_CD").Value = HttpContext.Current.Session("EMP_CD")
        Else
            sql = "INSERT INTO QS_AUDIT (CUST_CD, EMP_CD, TRAN_DT, QS_STATUS, EXPIRATION_DT, AUTH_NO, STORE_CD) "
            sql = sql & "VALUES(:CUST_CD, :EMP_CD, :TRAN_DT, :QS_STATUS, :EXPIRATION_DT, :AUTH_NO, :STORE_CD)"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)

            objsql2.Parameters.Add(":CUST_CD", OracleType.VarChar)
            objsql2.Parameters(":CUST_CD").Value = cust_cd
            objsql2.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objsql2.Parameters(":STORE_CD").Value = store_cd
            objsql2.Parameters.Add(":EMP_CD", OracleType.VarChar)
            objsql2.Parameters(":EMP_CD").Value = HttpContext.Current.Session("EMP_CD")
            objsql2.Parameters.Add(":TRAN_DT", OracleType.DateTime)
            objsql2.Parameters(":TRAN_DT").Value = FormatDateTime(Now.Date.ToString, DateFormat.ShortDate)
            objsql2.Parameters.Add(":QS_STATUS", OracleType.VarChar)
            objsql2.Parameters(":QS_STATUS").Value = status
            objsql2.Parameters.Add(":EXPIRATION_DT", OracleType.DateTime)
            objsql2.Parameters(":EXPIRATION_DT").Value = FormatDateTime(Now.Date.AddDays(60).ToString, DateFormat.ShortDate)
            objsql2.Parameters.Add(":AUTH_NO", OracleType.VarChar)
            If auth_no & "" = "" Then
                objsql2.Parameters(":AUTH_NO").Value = DBNull.Value
            Else
                objsql2.Parameters(":AUTH_NO").Value = auth_no
            End If
        End If
        objsql2.ExecuteNonQuery()
        objConnection.Close()

    End Sub

    Public Shared Function Retrieve_QS_Status(ByVal cust_cd As String) As String

        Dim rtnVal As String = ""
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim Mydatareader1 As OracleDataReader
        Dim sql As String
        Dim exp_days As Double = 0
        Dim Curr_Date As Date = Now.Date.ToString
        Dim Exp_Date As Date

        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        sql = "SELECT * FROM QS_AUDIT WHERE CUST_CD='" & cust_cd & "' ORDER BY EXPIRATION_DT DESC"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
        Mydatareader1 = DisposablesManager.BuildOracleDataReader(objsql2)

        If Mydatareader1.Read Then
            If IsDate(Mydatareader1.Item("EXPIRATION_DT").ToString) Then
                Exp_Date = FormatDateTime(Mydatareader1.Item("EXPIRATION_DT").ToString, DateFormat.ShortDate)
                exp_days = DateDiff(DateInterval.Day, Curr_Date, Exp_Date)
                'If FormatDateTime(Mydatareader1.Item("EXPIRATION_DT").ToString, DateFormat.ShortDate) < FormatDateTime(Now.Date.ToString, DateFormat.ShortDate) Then
                If exp_days < 1 Then
                    rtnVal = ""
                Else
                    rtnVal = Mydatareader1.Item("QS_STATUS").ToString
                End If
            End If
        Else
            rtnVal = ""
        End If
        objsql2.ExecuteNonQuery()
        objConnection.Close()

        Return rtnVal
    End Function

    Public Shared Function Retrieve_Auth_No(ByVal cust_cd As String) As String

        Dim rtnVal As String = ""
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand
        Dim Mydatareader1 As OracleDataReader
        Dim sql As String

        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        sql = "SELECT * FROM QS_AUDIT WHERE CUST_CD='" & cust_cd & "' ORDER BY EXPIRATION_DT DESC"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
        Mydatareader1 = DisposablesManager.BuildOracleDataReader(objsql2)

        If Mydatareader1.Read Then
            rtnVal = Mydatareader1.Item("AUTH_NO").ToString
        Else
            rtnVal = ""
        End If
        objsql2.ExecuteNonQuery()
        objConnection.Close()

        Return rtnVal
    End Function


    Public Shared Function SD_Submit_Query_CC(ByVal paymentId As Integer,
                                            ByVal terminalId As String,
                                            ByVal tranType As String,
                                            Optional ByVal delDoc As String = "") As String

        delDoc = IIf(String.IsNullOrEmpty(delDoc), "TEST", delDoc)
        Dim inputInfo As String = Build_Credit_Card_Transaction(paymentId, terminalId, delDoc, tranType)

        Return SD_Submit_Query(tranType, inputInfo)

    End Function


    ''' <summary>
    ''' Processes a Private Label authorization requests based on the transaction type being
    ''' requested such as an auth only, sale auth, or a refund auth, etc. It retrieves the 
    ''' relevant data needed to build the auth request in the format expected by the authorizer,
    ''' and builds and submits the request for authorization.
    ''' </summary>
    ''' <param name="paymentId">teh unique payment id to use for retrieving data</param>
    ''' <param name="terminalId">the terminal id </param>
    ''' <param name="tranType">the transaction type such as an auth only, sale auth, etc.</param>
    ''' <param name="delDoc">a unique identifier such as the current order/document number. It will be null ONLY
    '''                      in the case of a ECA and for or OTB request, it is the SSN.</param>
    ''' <param name="acctNo">the account number</param>
    ''' <param name="processorId">the processor id to be used for authorization</param>
    ''' <returns>the response string received after the request for authorization is made.</returns>
    Public Shared Function SD_Submit_Query_PL(ByVal paymentId As Integer,
                                                ByVal terminalId As String,
                                                ByVal tranType As String,
                                                Optional ByVal delDoc As String = "",
                                                Optional ByVal acctNo As String = "",
                                                Optional ByVal processorId As String = "") As String

        Dim inputInfo As String = ""
        delDoc = IIf(String.IsNullOrEmpty(delDoc), "TEST", delDoc)

        If tranType = AUTH_OTB Then
            inputInfo = Build_OTB_Transaction(IIf(paymentId = 0, "", paymentId), terminalId, delDoc, acctNo, processorId)
        ElseIf (tranType = AUTH_ONLY Or tranType = AUTH_SALE Or tranType = AUTH_REFUND) Then
            inputInfo = ProcessPLAuthTransaction(paymentId, terminalId, delDoc, tranType)
        ElseIf tranType = AUTH_INQUIRY Then
            inputInfo = ProcessPLAuthTransaction(paymentId, terminalId, delDoc, tranType)
        ElseIf tranType = AUTH_ECA Then
            inputInfo = Build_ECA_Transaction(terminalId, paymentId, acctNo, processorId)
        End If

        Return SD_Submit_Query(tranType, inputInfo)

    End Function


    Public Shared Function SD_Submit_Query(ByVal tranType As String, ByVal authRequestData As String) As String

        Dim rtnVal As String = ""   'this is the value that gets returned
        Dim tcpClient As New System.Net.Sockets.TcpClient()

        Try
            Dim SDC_Connection_String As String = Get_Connection_String("/HandleCode/SDC/PrimaryConnection", "IP_Address")
            Dim SDC_Connection_Port As String = Get_Connection_String("/HandleCode/SDC/Port_Number", "Port_Number")

            tcpClient.ReceiveTimeout = 30000        '30 seconds timeout
            tcpClient.Connect(SDC_Connection_String, SDC_Connection_Port)
            Dim networkStream As NetworkStream = tcpClient.GetStream()
            'Using (networkStream As NetworkStream = tcpClient.GetStream() )

            If networkStream.CanWrite And networkStream.CanRead Then

                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(authRequestData)
                networkStream.Write(sendBytes, 0, sendBytes.Length)

                ' Read the NetworkStream into a byte buffer.
                Dim bytes(tcpClient.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))

                ' Output the data received from the host to the console.
                Dim returndata As String = Encoding.ASCII.GetString(bytes)
                Dim SDC_Fields As Array
                Dim sep(3) As Char
                sep(0) = Chr(10)
                sep(1) = Chr(12)
                SDC_Fields = returndata.Split(sep)
                Dim s As String
                Dim MsgArray As Array
                Dim sdc_msg As Boolean = False
                Dim sdc_pb_cd As String

                'iterate through each of the line entries received in the response/out file from PB to check for errors
                For Each s In SDC_Fields
                    If InStr(s, ",") > 0 Then
                        MsgArray = Split(s, ",")
                        If (MsgArray(0) = "1003") Then  'this has the PB response code
                            sdc_pb_cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)

                            'check if the response matches any of these error codes - that would indicate a problem
                            If (sdc_pb_cd = "3" Or sdc_pb_cd = "4" Or sdc_pb_cd = "1" Or
                                sdc_pb_cd = "15" Or sdc_pb_cd = "16" Or sdc_pb_cd = "17" Or
                                sdc_pb_cd = "155" Or sdc_pb_cd = "0003" Or sdc_pb_cd = "0004" Or
                                sdc_pb_cd = "0001" Or sdc_pb_cd = "0015" Or sdc_pb_cd = "0016" Or
                                sdc_pb_cd = "0017" Or sdc_pb_cd = "0155") Then

                                sdc_msg = True
                            End If

                            'when auth-request fails and the submission is NOT a tran 22, then 
                            'a tran-22 will have to be issued, therefore informs caller
                            If (sdc_msg = True And tranType <> AUTH_INQUIRY) Then
                                Write_Audit_Transaction(returndata)
                                rtnVal = AUTH_INQUIRY
                                Return rtnVal
                            End If
                            Exit For  'only evaluates field 1003, no need to keep iterating
                        End If
                    End If
                Next s
                rtnVal = returndata
            Else
                rtnVal = IIf((Not networkStream.CanRead), "Cannot read data from this stream", "Cannot write data to this stream")
            End If
        Catch
            ' If tranType = "02" Or tranType = "09" Or tranType = "11" Or tranType = "GEAUTH09" Or tranType = "GEAUTH02" Or tranType = "GEAUTH01" Then
            If tranType = AUTH_ONLY Or tranType = AUTH_REFUND Or tranType = AUTH_VOID Or tranType = AUTH_SALE Then
                If rtnVal = "Protobase is not responding" Then
                    Return rtnVal
                Else
                    Write_Audit_Transaction(rtnVal)
                    rtnVal = AUTH_INQUIRY
                    Return rtnVal
                End If
            Else
                rtnVal = "Protobase is not responding"
            End If
        Finally
            tcpClient.Close()
        End Try

        ' Write the response out to the Audit Log
        Write_Audit_Transaction(rtnVal)

        Return Replace(rtnVal, ControlChars.NullChar, "")

    End Function


    Public Shared Sub Write_Audit_Transaction(ByVal SDC_Response As String)

        Dim SDC_Fields As String() = Nothing
        Dim SDC_BreakOut As String() = Nothing
        Dim Auth_No As String = ""
        Dim Trans_Dt As String = ""
        Dim Trans_Id As String = ""
        Dim PB_Response As String = ""
        Dim Host_Response_Msg As String = ""
        Dim Host_Response_Cd As String = ""
        Dim Protobase_Response_Msg As String = ""

        Dim sep(3) As Char
        sep(0) = Chr(10)
        sep(1) = Chr(12)
        SDC_Fields = SDC_Response.Split(sep)
        Dim s As String
        Dim MsgArray As Array
        For Each s In SDC_Fields
            If InStr(s, ",") > 0 Then
                MsgArray = Split(s, ",")
                Select Case MsgArray(0)
                    Case "0006"
                        Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "0032"
                        Trans_Dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "0003"
                        Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1003"
                        PB_Response = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1004"
                        Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1009"
                        Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                    Case "1010"
                        Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                End Select
            End If
        Next s

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand


        Dim sql As String
        sql = "INSERT INTO SDC_AUDIT_DATA "
        sql = sql & "(SDC_TRAN_ID, AUTH_NO, TRANS_DT, PB_RESP, HOST_RESP_MSG, HOST_RESP_CD, " &
                                "PB_RESP_MSG, GERS_DEL_DOC_NUM, SDC_TRAN_DETAILS) "
        sql = sql & "VALUES("
        sql = sql & "'" & Trans_Id & "',"
        sql = sql & "'" & Auth_No & "',"
        sql = sql & "'" & Trans_Dt & "',"
        sql = sql & "'" & PB_Response & "',"
        sql = sql & "'" & Host_Response_Msg & "',"
        sql = sql & "'" & Host_Response_Cd & "',"
        sql = sql & "'" & Protobase_Response_Msg & "'"
        If HttpContext.Current.Session("del_doc_num") & "" <> "" Then
            sql = sql & ",'" & HttpContext.Current.Session("del_doc_num") & "'"
        Else
            sql = sql & ",'NOT FOUND'"
        End If
        sql = sql & ",'" & Replace(SDC_Response.Replace(ControlChars.NullChar, ""), "'", "''") & "'"
        sql = sql & ")"

        'Set SQL OBJECT 
        With objsql
            .Connection = objConnection
            .CommandText = sql.Replace(Chr(4), "")
        End With
        objsql.ExecuteNonQuery()

        objConnection.Close()

    End Sub

    Private Shared Function Build_Credit_Card_Transaction(ByVal paymentId As Integer, ByVal terminalId As String,
                                                                                                                    ByVal delDoc As String, ByVal tranType As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim amt As Double = 0
        Dim bnk_crd_num As String = ""
        Dim exp_dt As String = ""
        Dim app_cd As String = ""
        Dim security_cd As String = ""
        Dim tax_chg As String = ""
        Dim tet_cd As String = ""
        Dim zip_cd As String = ""
        Dim cust_cd As String = ""

        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader

        Dim sql As String = ""
        sql = "SELECT AMT, BC, EXP_DT, APPROVAL_CD, SECURITY_CD FROM PAYMENT " &
                    " WHERE SESSIONID='" & HttpContext.Current.Session.SessionID &
                    "' AND ROW_ID=" & paymentId

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If (MyDataReader.Read()) Then
                amt = Replace(FormatNumber(CDbl(MyDataReader.Item("AMT").ToString), 2), ",", "")
                bnk_crd_num = AppUtils.Decrypt(MyDataReader.Item("BC").ToString, "CrOcOdIlE")
                exp_dt = MyDataReader.Item("EXP_DT").ToString
                app_cd = MyDataReader.Item("APPROVAL_CD").ToString
                security_cd = MyDataReader.Item("SECURITY_CD").ToString
            Else
                MyDataReader.Close()
                sql = "SELECT AMT, BC, EXP_DT, APPROVAL_CD, SECURITY_CD FROM PAYMENT_HOLDING WHERE ROW_ID=" & paymentId
                'Set SQL OBJECT 
                objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                If (MyDataReader.Read()) Then
                    amt = Replace(FormatNumber(CDbl(MyDataReader.Item("AMT").ToString), 2), ",", "")
                    bnk_crd_num = AppUtils.Decrypt(MyDataReader.Item("BC").ToString, "CrOcOdIlE")
                    exp_dt = MyDataReader.Item("EXP_DT").ToString
                    app_cd = MyDataReader.Item("APPROVAL_CD").ToString
                    security_cd = MyDataReader.Item("SECURITY_CD").ToString
                End If
            End If

            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            Throw
        End Try
        objConnection.Close()

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        If delDoc <> "TEST" Then
            sql = "SELECT a.TAX_CHG, a.TET_CD, a.CUST_CD, b.ZIP_CD " &
                        " FROM SO a, CUST b " &
                        " WHERE a.DEL_DOC_NUM='" & delDoc &
                        "' AND a.CUST_CD=b.CUST_CD"
            objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                If (MyDataReader.Read()) Then
                    tax_chg = MyDataReader.Item("TAX_CHG").ToString
                    tet_cd = MyDataReader.Item("TET_CD").ToString
                    cust_cd = MyDataReader.Item("CUST_CD").ToString
                    zip_cd = MyDataReader.Item("ZIP_CD").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                Throw
            End Try
        End If

        If Len(exp_dt) = 9 Then exp_dt = "0" & exp_dt
        exp_dt = Replace(exp_dt, Mid(exp_dt, 3, 6), "")

        If Not String.IsNullOrEmpty(app_cd) Then
            tranType = AUTH_PRIORAUTH
        End If

        Dim iString As String
        Dim x As Integer = 1

        '* Sale Transaction
        iString = "1," & tranType & ControlChars.Cr + ControlChars.Lf
        iString = iString & "2," & Replace(FormatNumber(CDbl(amt), 2), ",", "") & ControlChars.Cr + ControlChars.Lf
        iString = iString & "3," & bnk_crd_num & ControlChars.Cr + ControlChars.Lf
        If InStr(bnk_crd_num, "^") > 0 Then
            'iString = iString & "4, " & ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "4," & exp_dt & ControlChars.Cr + ControlChars.Lf
        End If
        If tranType = AUTH_PRIORAUTH And Not String.IsNullOrEmpty(app_cd) Then
            iString = iString & "6," & app_cd & ControlChars.Cr + ControlChars.Lf
        End If
        iString = iString & "7," & paymentId & ControlChars.Cr + ControlChars.Lf
        If InStr(bnk_crd_num, "^") = 0 And tranType <> AUTH_REFUND And tranType <> AUTH_VOID Then
            iString = iString & "50," & security_cd & ControlChars.Cr + ControlChars.Lf
        End If
        If String.IsNullOrEmpty(cust_cd) Then
            iString = iString & "70,NA" & ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "70," & cust_cd & ControlChars.Cr + ControlChars.Lf
        End If
        If Not String.IsNullOrEmpty(tet_cd) Then
            iString = iString & "71,2" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72,0.00" & ControlChars.Cr + ControlChars.Lf
        ElseIf IsNumeric(tax_chg) Then
            iString = iString & "71,1" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72," & Replace(FormatNumber(CDbl(tax_chg), 2), ",", "") & ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "71,1" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72,0.00" & ControlChars.Cr + ControlChars.Lf
        End If

        iString = iString & "105,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "106,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "109," & terminalId + ControlChars.Cr + ControlChars.Lf
        If HttpContext.Current.Session("EMP_CD") & "" <> "" Then
            iString = iString & "110," + Left(UCase(HttpContext.Current.Session("EMP_CD")), 8) + ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "110,PBADMIN" + ControlChars.Cr + ControlChars.Lf
        End If
        iString = iString & "115,010" + ControlChars.Cr + ControlChars.Lf

        If tranType <> "11" Then
            iString = iString & "401," & paymentId & ControlChars.Cr + ControlChars.Lf

            sql = "SELECT SO_LN.ITM_CD, ITM.VSN FROM SO_LN, ITM " &
            " WHERE DEL_DOC_NUM='" & delDoc &
            "' AND SO_LN.ITM_CD = ITM.ITM_CD AND SO_LN.VOID_FLAG='N' " &
            " ORDER BY DEL_DOC_LN#"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

            Dim itm_cd As String = ""
            Dim iString2 As String = ""
            Dim rgPattern = "[\\\/:\*\?""'<>|,.#()-]"
            Dim objRegEx As New Regex(rgPattern)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                Do While MyDataReader.Read() And x < 6
                    itm_cd = MyDataReader.Item("ITM_CD").ToString
                    If IsNumeric(Left(itm_cd, 4)) Then
                        itm_cd = Left(itm_cd, 4)
                    Else
                        itm_cd = "1111"
                    End If
                    If Left(objRegEx.Replace(MyDataReader.Item("VSN").ToString, " "), 40) & "" = "" Then
                        iString = iString & 400 + (x + 5) & ",NO DESCRIPTION" & ControlChars.Cr + ControlChars.Lf
                    Else
                        iString = iString & 400 + (x + 5) & "," & Left(objRegEx.Replace(MyDataReader.Item("VSN").ToString, " "), 40) & ControlChars.Cr + ControlChars.Lf
                    End If
                    iString2 = iString2 & 410 + (x + 1) & "," & itm_cd & ControlChars.Cr + ControlChars.Lf

                    x = x + 1
                Loop
                iString = iString + iString2

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                Throw
            End Try

            If ConfigurationManager.AppSettings("partial_pay").ToString = "Y" Then
                iString = iString & "647,1" + ControlChars.Cr + ControlChars.Lf
            Else
                iString = iString & "647,0" + ControlChars.Cr + ControlChars.Lf
            End If

        End If

        objConnection.Close()

        If InStr(bnk_crd_num, "^") = 0 And tranType <> AUTH_REFUND And tranType <> AUTH_VOID Then
            iString = iString & "700," & zip_cd & ControlChars.Cr + ControlChars.Lf
        End If

        iString = iString & "1008,ID:" & ControlChars.Cr & ControlChars.Lf
        iString = iString & "8002," & ControlChars.Cr & ControlChars.Lf
        iString = iString & "8006," & ControlChars.Cr & ControlChars.Lf & Chr(4)

        Return iString

    End Function


    ''' <summary>
    ''' Builds the auth request string that is needed for authorization of a Finance/Private Label
    ''' transaction based on the data passed-in. It does NOT make the actual authorization call.
    ''' </summary>
    ''' <param name="paymentId">the unique payment/reference id to use for uniquely identifying the payment transaction.</param>
    ''' <param name="terminalId">the terminal id specific for the Finance provider setup in the system. </param>
    ''' <param name="delDoc">a unique identifier such as the current order/document number. It will be null ONLY
    '''                      in the case of a ECA and for or OTB request, it is the SSN.</param>
    ''' <param name="tranType">the transaction type such as an auth only, sale auth, etc.</param>
    ''' <param name="paymentId"></param>
    ''' <param name="terminalId"></param>
    ''' <param name="delDoc"></param>
    ''' <param name="tranType"></param>
    ''' <param name="acctNumber">the account number</param>
    ''' <param name="amount">the amount to be authorized</param>
    ''' <param name="expDt">the expiration date on the Private Label card</param>
    ''' <param name="approvalCd">the approval code(if any) when a manual/voice auth is done</param>
    ''' <param name="securityCd">the security code on the Private Label card</param>
    ''' <param name="promoCd">the promotion used for the Private Label provider</param>
    ''' <returns>the response string received after the request for authorization is made.</returns>
    Public Shared Function BuildPLAuthTransaction(ByVal paymentId As String, ByVal terminalId As String,
                                                      ByVal delDoc As String, ByVal tranType As String,
                                                      ByVal acctNumber As String, ByVal amount As String,
                                                      ByVal expDt As String, ByVal approvalCd As String,
                                                      ByVal securityCd As String, ByVal promoCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim sql As New StringBuilder
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim tax_chg As String = ""
        Dim tet_cd As String = ""
        Dim zip_cd As String = ""
        Dim cust_cd As String = ""
        Dim promo_cd As String = ""

        If delDoc <> "TEST" Then

            sql = sql.Append(" SELECT a.TAX_CHG, a.TET_CD, a.CUST_CD, b.ZIP_CD  ")
            sql.Append("  FROM SO a, CUST b  ")
            sql.Append("  WHERE a.DEL_DOC_NUM= :DEL_DOC_NUM  AND a.CUST_CD=b.CUST_CD ")

            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
            cmd.Parameters(":DEL_DOC_NUM").Value = delDoc
            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                If (reader.Read()) Then
                    tax_chg = reader.Item("TAX_CHG").ToString
                    tet_cd = reader.Item("TET_CD").ToString
                    cust_cd = reader.Item("CUST_CD").ToString
                    zip_cd = reader.Item("ZIP_CD").ToString
                End If

                reader.Close()
            Catch ex As Exception
                Throw
            Finally
                conn.Close()
            End Try
        End If

        'If for a sale tran there is an approval code, meaning it was manually approved, change the tran type to a '07'
        If Not String.IsNullOrEmpty(approvalCd) And tranType <> AUTH_REFUND Then
            tranType = AUTH_PRIORAUTH
        End If

        Dim iString As String
        Dim x As Integer = 1

        '* Sale Transaction
        iString = "1," & tranType & ControlChars.Cr + ControlChars.Lf
        iString = iString & "2," & Replace(FormatNumber(CDbl(amount), 2), ",", "") & ControlChars.Cr + ControlChars.Lf
        iString = iString & "3," & acctNumber & ControlChars.Cr + ControlChars.Lf

        If tranType = AUTH_ONLY Then
            '***** For finance auth only case - fix to address the case where the auth's exp dt does not match that of the PAuth done in E1.
            ' There is now a new GP_PARMS(MM-1879) record via PTS form that will be used by the E1 asp_store_forward table trigger. Instead of
            '  being hardcoded to 12/31/2017, it has been made configurable. If the PTS value is not filled or is invalid, the 31-DEC-2099 
            ' date will be used and also a default date if none supplied. So, fetch this new GP parms value
            If (SysPms.maxExpDt.isNotEmpty) Then
                Dim convertedDate As Date = Date.Parse(SysPms.maxExpDt) 'converts to a date format like 12/30/2099
                expDt = convertedDate.ToString.Substring(0, 2) + convertedDate.ToString.Substring(8, 2)
            Else
                expDt = "1299"
            End If
        End If

        iString = iString & "4," & expDt & ControlChars.Cr + ControlChars.Lf
        If tranType = AUTH_PRIORAUTH And Not String.IsNullOrEmpty(approvalCd) Then
            iString = iString & "6," & approvalCd & ControlChars.Cr + ControlChars.Lf
        End If

        iString = iString & "7," & paymentId & ControlChars.Cr + ControlChars.Lf
        If securityCd & "" <> "" And tranType = AUTH_ONLY Or securityCd & "" <> "" And tranType = AUTH_SALE Then
            iString = iString & "50," & securityCd & ControlChars.Cr + ControlChars.Lf
        End If
        If String.IsNullOrEmpty(cust_cd) Then
            iString = iString & "70,NA" & ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "70," & cust_cd & ControlChars.Cr + ControlChars.Lf
        End If
        If Not String.IsNullOrEmpty(tet_cd) Then
            iString = iString & "71,2" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72,0.00" & ControlChars.Cr + ControlChars.Lf
        ElseIf IsNumeric(tax_chg) Then
            iString = iString & "71,1" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72," & Replace(FormatNumber(CDbl(tax_chg), 2), ",", "") & ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "71,1" & ControlChars.Cr + ControlChars.Lf
            iString = iString & "72,0.00" & ControlChars.Cr + ControlChars.Lf
        End If

        iString = iString & "105,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "106,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "109," & terminalId + ControlChars.Cr + ControlChars.Lf
        If HttpContext.Current.Session("EMP_CD") & "" <> "" Then
            iString = iString & "110," + Left(UCase(HttpContext.Current.Session("EMP_CD")), 8) + ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "110,PBADMIN" + ControlChars.Cr + ControlChars.Lf
        End If
        iString = iString & "115,025" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "127,D" + ControlChars.Cr + ControlChars.Lf

        sql.Clear()
        sql.Append(" SELECT SO_LN.ITM_CD, SO_LN.UNIT_PRC FROM SO_LN  ")
        sql.Append("  WHERE DEL_DOC_NUM= :DEL_DOC_NUM ")
        sql.Append(" AND SO_LN.VOID_FLAG='N'  ")
        sql.Append("  ORDER BY DEL_DOC_LN# ")
        conn.Open()
        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar).Direction = ParameterDirection.Input
        cmd.Parameters(":DEL_DOC_NUM").Value = delDoc

        Dim iString2 As String = ""
        Dim rgPattern = "[\\\/:\*\?""'<>|,.#()-]"
        Dim objRegEx As New Regex(rgPattern)

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            Do While reader.Read() And x < 8
                If IsNumeric(reader.Item("UNIT_PRC").ToString) Then
                    iString = iString & 607 + x & "," & Replace(FormatNumber(reader.Item("UNIT_PRC").ToString, 2), ",", "") & ControlChars.Cr + ControlChars.Lf
                Else
                    iString = iString & 607 + x & ",0.00" & ControlChars.Cr + ControlChars.Lf
                End If
                iString2 = iString2 & 637 + x & "," & objRegEx.Replace(reader.Item("ITM_CD").ToString, " ") & ControlChars.Cr + ControlChars.Lf
                x = x + 1
            Loop
            iString = iString + "615," + promo_cd + ControlChars.Cr + ControlChars.Lf
            iString = iString + "637," + delDoc + ControlChars.Cr + ControlChars.Lf + iString2

            'Close Connection 
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        iString = iString & "1008,ID:" & ControlChars.Cr & ControlChars.Lf
        iString = iString & "8002," & ControlChars.Cr & ControlChars.Lf
        iString = iString & "8006," & ControlChars.Cr & ControlChars.Lf & Chr(4)
        rtnVal = iString

        Return rtnVal

    End Function


    ''' <summary>
    ''' Pre-processes a Private Label authorization requests for an auth only case. It retrieves the 
    ''' data from the payment table and passes it on so it can be used to build the auth request and
    ''' submit the request for authorization.
    ''' </summary>
    ''' <param name="paymentId">the unique payment id to use for retrieving payment data</param>
    ''' <param name="terminalId">the terminal id specific for the Finance provider setup in the system. </param>
    ''' <param name="delDoc">a unique identifier such as the current order/document number. It will be null ONLY
    '''                      in the case of a ECA and for or OTB request, it is the SSN.</param>
    ''' <param name="tranType">the transaction type such as an auth only, sale auth, etc.</param>
    ''' <returns>the response string received after the request for authorization is made.</returns>
    Private Shared Function ProcessPLAuthTransaction(ByVal paymentId As Integer,
                                                     ByVal terminalId As String,
                                                     ByVal delDoc As String,
                                                     ByVal tranType As String) As String

        Dim authInput As String = String.Empty
        Dim amt As Double = 0
        Dim bnk_crd_num As String = ""
        Dim exp_dt As String = ""
        Dim app_cd As String = ""
        Dim security_cd As String = ""
        Dim tax_chg As String = ""
        Dim promo_cd As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim reader As OracleDataReader
        Dim sql As New StringBuilder
        sql.Append(" SELECT AMT, FI_ACCT_NUM, EXP_DT, APPROVAL_CD, SECURITY_CD, FIN_PROMO_CD  ")
        sql.Append(" FROM PAYMENT WHERE SESSIONID=:SESSION_ID ")
        sql.Append(" AND ROW_ID= :PAYMENT_ID ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":SESSION_ID", OracleType.VarChar)
        cmd.Parameters(":SESSION_ID").Value = HttpContext.Current.Session.SessionID
        cmd.Parameters.Add(":PAYMENT_ID", OracleType.VarChar)
        cmd.Parameters(":PAYMENT_ID").Value = paymentId

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                amt = Replace(FormatNumber(CDbl(reader.Item("AMT").ToString), 2), ",", "")
                bnk_crd_num = AppUtils.Decrypt(reader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                exp_dt = reader.Item("EXP_DT").ToString
                app_cd = reader.Item("APPROVAL_CD").ToString
                promo_cd = GetAspPromoCd(reader.Item("FIN_PROMO_CD").ToString)
                security_cd = reader.Item("SECURITY_CD").ToString
            Else
                reader.Close()
                sql.Clear()
                sql.Append(" SELECT AMT, FI_ACCT_NUM, EXP_DT, APPROVAL_CD, SECURITY_CD, FIN_PROMO_CD  ")
                sql.Append(" FROM PAYMENT_HOLDING WHERE ROW_ID= :PAYMENT_ID ")
                cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
                cmd.Parameters.Add(":PAYMENT_ID", OracleType.VarChar)
                cmd.Parameters(":PAYMENT_ID").Value = paymentId

                reader = DisposablesManager.BuildOracleDataReader(cmd)

                If (reader.Read()) Then
                    amt = Replace(FormatNumber(CDbl(reader.Item("AMT").ToString), 2), ",", "")
                    bnk_crd_num = AppUtils.Decrypt(reader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
                    exp_dt = reader.Item("EXP_DT").ToString
                    app_cd = reader.Item("APPROVAL_CD").ToString
                    promo_cd = GetAspPromoCd(reader.Item("FIN_PROMO_CD").ToString)
                    security_cd = reader.Item("SECURITY_CD").ToString
                End If
            End If

            authInput = BuildPLAuthTransaction(paymentId, terminalId,
                                                delDoc, tranType,
                                                bnk_crd_num, amt,
                                                exp_dt, app_cd,
                                                security_cd, promo_cd)

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return authInput

    End Function


    Private Shared Function Build_OTB_Transaction(ByVal zipCd As String, ByVal terminalId As String,
                                                  ByVal ssn As String, ByVal acctNo As String,
                                                  ByVal processorId As String) As String

        Dim iString As String

        iString = "1,24" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "2,0.00" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "3," & acctNo & ControlChars.Cr + ControlChars.Lf
        iString = iString & "4,1249" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "7,0002" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "105,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "106,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "109," & terminalId + ControlChars.Cr + ControlChars.Lf
        If HttpContext.Current.Session("EMP_CD") & "" <> "" Then
            iString = iString & "110," + Left(UCase(HttpContext.Current.Session("EMP_CD")), 8) + ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "110,PBADMIN" + ControlChars.Cr + ControlChars.Lf
        End If
        iString = iString & "112," & processorId + ControlChars.Cr + ControlChars.Lf
        iString = iString & "115,025" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "3010," & ssn + ControlChars.Cr + ControlChars.Lf
        iString = iString & "3028," & zipCd + ControlChars.Cr + ControlChars.Lf
        iString = iString & "8002," & ControlChars.Cr + ControlChars.Lf
        iString = iString & "8006," & ControlChars.Cr + ControlChars.Lf & Chr(4)
        Return iString

    End Function


    Private Shared Function Build_ECA_Transaction(ByVal terminalId As String, ByVal appId As String,
                                                                                                    ByVal appType As String, ByVal processorId As String) As String

        Dim iString As String

        iString = "1,21" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "7,0002" & ControlChars.Cr + ControlChars.Lf
        iString = iString & "105,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "106,.\" + ControlChars.Cr + ControlChars.Lf
        iString = iString & "109," & terminalId + ControlChars.Cr + ControlChars.Lf
        If HttpContext.Current.Session("EMP_CD") & "" <> "" Then
            iString = iString & "110," + Left(UCase(HttpContext.Current.Session("EMP_CD")), 8) + ControlChars.Cr + ControlChars.Lf
        Else
            iString = iString & "110,PBADMIN" + ControlChars.Cr + ControlChars.Lf
        End If
        iString = iString & "112," & processorId + ControlChars.Cr + ControlChars.Lf
        iString = iString & "115,025" + ControlChars.Cr + ControlChars.Lf

        Dim connString As String
        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader

        Dim sql As String = ""
        sql = "SELECT * FROM EC_APPLICATION WHERE ROW_ID=" & appId
        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)
        Dim ecaAppType As String = "0"
        Dim prime_dob As String = ""
        Dim joint_dob As String = ""

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If MyDataReader.Read() Then
                iString = iString & "3002," & MyDataReader.Item("PRIME_FNAME").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & FormatField("3003", MyDataReader.Item("PRIME_MIDDLE_INIT").ToString)

                iString = iString & "3005," & MyDataReader.Item("PRIME_LNAME").ToString + ControlChars.Cr + ControlChars.Lf
                If IsDate(MyDataReader.Item("PRIME_DOB").ToString) Then
                    prime_dob = CreateDate(MyDataReader.Item("PRIME_DOB").ToString)
                    iString = iString & "3008," & prime_dob + ControlChars.Cr + ControlChars.Lf
                Else
                    iString = iString & "3008," + ControlChars.Cr + ControlChars.Lf
                End If
                iString = iString & "3010," & MyDataReader.Item("PRIME_SSN").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & FormatField("3014", MyDataReader.Item("PRIME_CELL").ToString)
                iString = iString & "3018," & MyDataReader.Item("PRIME_ADDR1").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & FormatField("3019", MyDataReader.Item("PRIME_ADDR2").ToString)
                iString = iString & "3026," & MyDataReader.Item("PRIME_CITY").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & "3027," & MyDataReader.Item("PRIME_STATE").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & "3028," & MyDataReader.Item("PRIME_ZIP_CD").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & "3029," & MyDataReader.Item("PRIME_HOME_PHONE").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & "3032," & MyDataReader.Item("PRIME_TIME_AT_RES").ToString + ControlChars.Cr + ControlChars.Lf
                iString = iString & "3033," & MyDataReader.Item("PRIME_HOME_STATUS").ToString + ControlChars.Cr + ControlChars.Lf
                'build primary applicant's employment info, if present
                Dim occupation As String = MyDataReader.Item("PRIME_OCCUPATION").ToString
                iString = iString & FormatField("3061", IIf(occupation = "1", occupation, "0"))      'self employed = 1, anything else = 0
                iString = iString & FormatField("3063", MyDataReader.Item("PRIME_EMP_NAME").ToString)
                iString = iString & FormatField("3064", MyDataReader.Item("PRIME_EMP_CITY").ToString)
                iString = iString & FormatField("3065", MyDataReader.Item("PRIME_EMP_STATE").ToString)
                iString = iString & FormatField("3066", MyDataReader.Item("PRIME_EMP_TIME").ToString)
                iString = iString & FormatField("3067", MyDataReader.Item("PRIME_EMP_PHONE").ToString)
                iString = iString & FormatNumberField("3069", MyDataReader.Item("PRIME_MON_INCOME").ToString)
                iString = iString & FormatNumberField("3070", MyDataReader.Item("PRIME_YR_INCOME").ToString)

                'Now check if joint applicant info exists
                Dim jointApp As String = MyDataReader.Item("JOINT_APPLICATION").ToString
                If (jointApp.isNotEmpty AndAlso (jointApp = "2")) Then
                    ecaAppType = "2"
                    iString = iString & "3102," & MyDataReader.Item("JOINT_FNAME").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & FormatField("3103", MyDataReader.Item("JOINT_MIDDLE_INIT").ToString)
                    iString = iString & "3105," & MyDataReader.Item("JOINT_LNAME").ToString + ControlChars.Cr + ControlChars.Lf
                    If IsDate(MyDataReader.Item("JOINT_DOB").ToString) Then
                        joint_dob = CreateDate(MyDataReader.Item("JOINT_DOB").ToString)
                        iString = iString & "3108," & joint_dob + ControlChars.Cr + ControlChars.Lf
                    Else
                        iString = iString & "3108," + ControlChars.Cr + ControlChars.Lf
                    End If
                    iString = iString & "3109," & MyDataReader.Item("JOINT_SSN").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & "3115," & MyDataReader.Item("JOINT_CITY").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & "3116," & MyDataReader.Item("JOINT_STATE").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & "3117," & MyDataReader.Item("JOINT_ZIP").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & "3119," & MyDataReader.Item("JOINT_ADDR1").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & FormatField("3120", MyDataReader.Item("JOINT_ADDR2").ToString)
                    iString = iString & "3122," & MyDataReader.Item("JOINT_HOME").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & FormatField("3125", MyDataReader.Item("JOINT_CELL").ToString)
                    iString = iString & "3126," & MyDataReader.Item("JOINT_HOME_STATUS").ToString + ControlChars.Cr + ControlChars.Lf
                    iString = iString & "3127," & MyDataReader.Item("JOINT_TIME_AT_RES").ToString + ControlChars.Cr + ControlChars.Lf

                    'build joint applicant employment info, if present
                    iString = iString & FormatField("3131", MyDataReader.Item("JOINT_EMP_NAME").ToString)
                    iString = iString & FormatField("3132", MyDataReader.Item("JOINT_EMP_CITY").ToString)
                    iString = iString & FormatField("3133", MyDataReader.Item("JOINT_EMP_STATE").ToString)
                    iString = iString & FormatField("3134", MyDataReader.Item("JOINT_EMP_TIME").ToString)
                    iString = iString & FormatField("3135", MyDataReader.Item("JOINT_WORK").ToString)

                    iString = iString & FormatNumberField("3136", MyDataReader.Item("JOINT_MON_INCOME").ToString)
                    iString = iString & FormatField("3137", MyDataReader.Item("JOINT_DL").ToString)
                    iString = iString & FormatField("3138", MyDataReader.Item("JOINT_DL_ST").ToString)
                End If

                iString = iString & FormatField("3146", MyDataReader.Item("RELATIVE_PHONE").ToString)
                iString = iString & "3164,0" + ControlChars.Cr + ControlChars.Lf       'pass a hard-coded '0' to reperesent 'N' for credit protection
                iString = iString & FormatNumberField("3177", MyDataReader.Item("REQUESTED_AMOUNT").ToString)
                iString = iString & FormatField("3185", MyDataReader.Item("PRIME_DL").ToString)
                iString = iString & FormatField("3186", MyDataReader.Item("PRIME_DL_ST").ToString)
                iString = iString & "3193," & ecaAppType + ControlChars.Cr + ControlChars.Lf
                Dim langInd As String = MyDataReader.Item("LANGUAGE_INDICATOR").ToString
                iString = iString & "3207," & IIf(langInd = "E", "0", "1") + ControlChars.Cr + ControlChars.Lf
                iString = iString & FormatField("3317", MyDataReader.Item("AUTHORIZATION").ToString)
            End If
        Catch
            Throw
        End Try

        iString = iString & "8002," & ControlChars.Cr + ControlChars.Lf
        iString = iString & "8006," & ControlChars.Cr + ControlChars.Lf & Chr(4)

        Return iString

    End Function


    ''' <summary>
    ''' Creates a string date from the passed-in date in the either a 'MMDDYY' or 'MMDDYYYY'
    ''' format depending on the flag for the 2 or 4 digit  year. 
    ''' </summary>
    ''' <param name="anyDate">a valid date in a string format</param>
    ''' <param name="useFourDigitYear">indicates whether to use  2 or 4 digit  year format. If none is specified, a 2-digit year is used.</param>
    ''' <returns> a date of the format of 'MMDDYY' or 'MMDDYYYY'</returns>
    Public Shared Function CreateDate(ByVal anyDate As String, Optional ByVal useFourDigitYear As Boolean = False) As String

        Dim rtnVal As String = anyDate

        If (anyDate.isNotEmpty) Then
            Dim dt As Date = DateTime.Parse(anyDate)
            rtnVal = IIf(useFourDigitYear, String.Format("{0:MMddyyyy}", dt), String.Format("{0:MMddyy}", dt))
        End If

        Return rtnVal
    End Function

    Public Shared Function MaskChars(ByVal strCardNumber As String, Optional ByVal DisplayChars As Integer = 4, Optional ByVal CardNumberMask As String = "X") As String

        Dim strRight As String
        Dim strLeft As String
        Dim iTemp As Integer
        Dim strLeftTemp As String = ""
        Dim I As Byte
        Dim strFull As String
        If (Len(strCardNumber) = 0) Or (Len(strCardNumber) < DisplayChars) Then
            Return ""
        End If
        strRight = Right(strCardNumber, DisplayChars)
        iTemp = Len(strCardNumber) - DisplayChars
        strLeft = Left(strCardNumber, iTemp)
        strLeftTemp = ""
        For I = 1 To Len(strLeft)
            strLeftTemp = strLeftTemp & CardNumberMask
        Next I
        strFull = strLeftTemp & strRight
        MaskChars = strFull

    End Function


    ''' <summary>
    ''' Returns a flag indicating if the account number passed-in was swiped or manually keyed
    ''' </summary>
    ''' <param name="accountNumber">the account number which could be the whole track data or just the acct #</param>
    ''' <returns>True if the account number was captured as part of a swipe. False otherwise</returns>
    Public Shared Function IsSwiped(ByVal accountData As String) As Boolean

        Return (accountData.isNotEmpty AndAlso InStr(accountData, "^") > 0)

    End Function

    ''' <summary>
    ''' This method attempts to retrieve the account number from the "trackInfo" of type Track1
    ''' If found, it returns the account number, otherwise an empty value<p>
    ''' </summary>
    ''' <param name="trackInfo">a string value containing information of type Track1</param>
    ''' <returns> the parsed account number as a string value</returns>
    Public Shared Function GetAccountNumber(ByVal trackInfo As String) As String
        Dim parsedVal As String = ""

        If InStr(trackInfo, "^") > 0 Then
            'means it is swiped and if track1 data it will be like '%B5499740000000776^PUBLIC JR/JOHN Q.MR.^15121015432112345678'

            'Create an array of the swipe data separated by the '^' delimiter
            Dim sCC As Array
            sCC = Split(trackInfo, "^")

            'The acct # is the first token in track 1. Ignore the first 2 characters(e.g. %B) at the beginning and extract the rest  of the acct# 
            parsedVal = Right(sCC(0).Trim, Len(sCC(0)) - 2)

            If Left(trackInfo, 1) = "B" Then
                parsedVal = Right(trackInfo, Len(trackInfo) - 1)
            End If
        End If

        Return parsedVal
    End Function


    ''' <summary>
    ''' This method attempts to retrieve the expiration date from "trackInfo" of type Track1.
    '''  If found, it returns the parsed expiration date, otherwise the default value of "1217"(MMYY)
    ''' </summary>
    ''' <param name="trackInfo">a string value containing information of type Track1 </param>
    ''' <returns>an expiration date as a string value</returns>

    Public Shared Function GetExpirationDate(ByVal trackInfo As String) As String

        Dim parsedVal As String = "1217"    'this matches the BT asp_store_forward table trigger value
        If InStr(trackInfo, "^") > 0 Then
            'means it is swiped and if track1 data it will be like '%B5499740000000776^PUBLIC JR/JOHN Q.MR.^15121015432112345678'

            'Create an array of the swipe data separated by the '^' delimiter
            Dim sCC As Array
            sCC = Split(trackInfo, "^")

            'The acct # is the third  token in track 1. expiration date comes in YYMM format and we need it to be MMYY. 
            Dim tempDt As String = Left(sCC(2).Trim, 4)
            parsedVal = Right(tempDt, 2) + Left(tempDt, 2)
        End If

        Return parsedVal
    End Function

    ''' <summary>
    ''' Formats the field passed-in if it is not null by building a string consisting of it's PB field Id (e.g 3003)
    ''' and the actual value of the field. If the field is null, an empty string is returned
    ''' </summary>
    ''' <param name="fieldId">the field id e.g. 3001</param>
    ''' <param name="fieldValue"> the field value e.g. "Mr."</param>
    ''' <returns></returns>
    Private Shared Function FormatField(ByVal fieldId As String, ByVal fieldValue As String) As String

        Dim rtnVal As String = ""
        If (fieldValue.isNotEmpty) Then
            rtnVal = fieldId & "," & fieldValue + ControlChars.Cr + ControlChars.Lf
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' Formats the number field passed-in by formatting it to a 2-decimal number, if not null.  It builds a 
    ''' string consisting of it's PB field Id (e.g 3003) and the actual value of the field. If the field is null or zero, an
    ''' empty string is returned.
    ''' </summary>
    ''' <param name="fieldId">the field id e.g. 3001</param>
    ''' <param name="fieldValue"> the field value e.g. "1500"</param>
    ''' <returns>an empty string OR  the field # and the number field value with decimals padded at the end. e.g. 3001,1500.00</returns>
    Private Shared Function FormatNumberField(ByVal fieldId As String, ByVal fieldValue As String) As String

        Dim rtnVal As String = ""
        If (fieldValue.isNotEmpty AndAlso (CDbl(fieldValue) > 0)) Then
            'format the number and remove any commas added due to number formatting
            Dim num As String = Replace(FormatNumber(fieldValue, 2), ",", "")
            rtnVal = fieldId & "," & num & ControlChars.Cr & ControlChars.Lf
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' Retrieves the actual promotion code provided by the service auth provider that has been
    ''' setup in the system. This should correspond to a user specified promo code that has been 
    ''' defined for the same ASP in the system.
    ''' </summary>
    ''' <param name="promoCd">the user specified promo code for which to get the ASP promo code</param>
    ''' <returns>the ASP promo code</returns>
    Private Shared Function GetAspPromoCd(ByVal promoCd As String) As String

        Dim rtnVal As String = String.Empty
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Open Connection 
        conn2.Open()

        sql = "Select as_promo_cd FROM asp_promo WHERE promo_cd='" & promoCd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            Do While MyDataReader2.Read
                If MyDataReader2.Item("AS_PROMO_CD").ToString & "" = "" Then
                    rtnVal = ""
                Else
                    rtnVal = MyDataReader2.Item("AS_PROMO_CD").ToString
                End If
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            Throw
        Finally
            conn2.Close()
        End Try

        Return rtnVal
    End Function

    ''' <summary>
    ''' Masks the account number passed-in to display only the last 4 digits prepended with asteriks.
    ''' </summary>
    ''' <param name="acctNumber">the ccount number to mask</param>
    ''' <returns>the masked account number </returns>
    Public Shared Function MaskAccountNumber(ByVal acctNumber As String) As String
        Dim rtnVal As String = acctNumber

        'Pad last -four digits with "*"
        If (acctNumber.isNotEmpty) Then
            rtnVal = acctNumber.Substring(acctNumber.Length - 4).PadLeft(acctNumber.Length, "*")
        End If

        Return rtnVal
    End Function

    Public Class VerifyCC
        Public Structure TypeValid
            Public CCType As String
            Public CCValid As Boolean
        End Structure


        Public Function GetCardInfo(ByVal CCN As String) As TypeValid
            Dim i, w, x, y
            y = 0
            CCN = Replace(Replace(Replace(CStr(CCN), "-", ""), " ", ""), ".", "")

            w = 2 * (Len(CCN) Mod 2)
            For i = Len(CCN) - 1 To 1 Step -1
                x = Mid(CCN, i, 1)
                If IsNumeric(x) Then
                    Select Case (i Mod 2) + w
                        Case 0, 3
                            y = y + CInt(x)
                        Case 1, 2
                            x = CInt(x) * 2
                            If x > 9 Then

                                y = y + (x \ 10) + (x - 10)
                            Else
                                y = y + x
                            End If
                    End Select
                End If
            Next

            y = 10 - (y Mod 10)
            If y > 9 Then y = 0
            GetCardInfo.CCValid = (CStr(y) = Right(CCN, 1))
            If GetCardInfo.CCValid = True Then
                Select Case Left(CCN, 1)
                    Case "3"
                        GetCardInfo.CCType = "AMEX/Diners Club/JCB"
                    Case "4"
                        GetCardInfo.CCType = "VISA"
                    Case "5"
                        GetCardInfo.CCType = "MasterCard"
                    Case "6"
                        GetCardInfo.CCType = "Discover"
                    Case Else
                        GetCardInfo.CCType = "Unknown"
                End Select
            Else
                GetCardInfo.CCType = "Unknown"
            End If

        End Function
    End Class

End Class



