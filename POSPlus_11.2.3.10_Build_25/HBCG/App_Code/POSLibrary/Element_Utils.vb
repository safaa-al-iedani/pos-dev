﻿Imports Microsoft.VisualBasic
Imports System.Xml

Public Class Element_Utils

    Public Shared Function EX_SendRequestAndGetResponse(ByVal mnum As String, ByVal transaction_type As String, ByVal card_num As String, Optional ByVal amount As Double = 0) 'As XmlDocument

        Dim _requestXML As XmlDataDocument
        Dim _responseXML As New XmlDataDocument()
        Dim oWebReq As System.Net.HttpWebRequest
        oWebReq = CType(System.Net.HttpWebRequest.Create("https://certtransaction.elementexpress.com/"), System.Net.HttpWebRequest)

        Dim xml_string As String = ""

        Select Case transaction_type
            Case "creditcardauth"
                xml_string = xml_string & Build_EX_CreditCardSale(mnum, card_num)
        End Select

        _requestXML = New XmlDataDocument
        _requestXML.RemoveAll()
        _requestXML.LoadXml(xml_string)

        Dim reqBytes As Byte()
        reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
        oWebReq.ContentLength = reqBytes.Length
        oWebReq.Method = "POST"
        oWebReq.ContentType = "text/xml"
        oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
        Try
            Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
            oReqStream.Write(reqBytes, 0, reqBytes.Length)
            Dim responseBytes As Byte()
            Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
            If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
                responseBytes = EX_processResponse(oWebResp)
                Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
                _responseXML.RemoveAll()
                _responseXML.LoadXml(strResp)
            End If
            oWebResp.Close()
        Catch ex As Exception
            If Err.Number = 5 Then
                Return _responseXML
                Exit Function
            Else
                Return Err.Description
            End If
            'Throw
        End Try
        Return _responseXML

    End Function

    Private Shared Function EX_processResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()

        Dim memStream As New System.IO.MemoryStream()
        Const BUFFER_SIZE As Integer = 4096
        Dim iRead As Integer = 0
        Dim idx As Integer = 0
        Dim iSize As Int64 = 0
        memStream.SetLength(BUFFER_SIZE)
        While (True)
            Dim respBuffer(BUFFER_SIZE) As Byte
            Try
                iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
            Catch e As System.Exception
                Throw e
            End Try
            If (iRead = 0) Then
                Exit While
            End If
            iSize += iRead
            memStream.SetLength(iSize)
            memStream.Write(respBuffer, 0, iRead)
            idx += iRead
        End While

        Dim content As Byte() = memStream.ToArray()
        memStream.Close()
        Return content

    End Function

    Private Shared Function Build_EX_CreditCardSale(ByVal mnum As String, ByVal card_id As String)

        Dim iString As String
        Dim WGC_UserName As String = HBCG_Utils.Get_Connection_String("/HandleCode/WGC/WGC_UserName", "UserName")
        Dim WGC_Password As String = HBCG_Utils.Get_Connection_String("/HandleCode/WGC/WGC_Password", "Password")

        iString = iString & "<CreditCardSale xmlns=""https://transaction.elementexpress.com""> "
        iString = iString & "<Application>"
        iString = iString & "<ApplicationID>" & "1391" & "</ApplicationID>"
        iString = iString & "<ApplicationName>" & "RedPrairie" & "</ApplicationName>"
        iString = iString & "<ApplicationVersion>" & "2012.1.8" & "</ApplicationVersion>"
        iString = iString & "</Application>"
        iString = iString & "<Credentials>"
        iString = iString & "<AccountID>" & "1009648" & "</AccountID>"
        iString = iString & "<AccountToken>" & "FB3EAAD9DF3CB219D2A7D36AAE4943E72362D2F3B4EBC455311841E59DC52F3D96EDA601" & "</AccountToken>"
        iString = iString & "<AcceptorID>" & "3928907" & "</AcceptorID>"
        iString = iString & "</Credentials>"
        iString = iString & "<Terminal>"
        iString = iString & "<TerminalID>" & "678-456-1200" & "</TerminalID>"
        iString = iString & "<CardholderPresentCode>" & "2" & "</CardholderPresentCode>"
        iString = iString & "<CardInputCode>" & "2" & "</CardInputCode>"
        iString = iString & "<TerminalCapabilityCode>" & "3" & "</TerminalCapabilityCode>"
        iString = iString & "<TerminalEnvironmentCode>" & "2" & "</TerminalEnvironmentCode>"
        iString = iString & "<CVVPresenceCode>" & "1" & "</CVVPresenceCode>"
        iString = iString & "<CardPresentCode>" & "2" & "</CardPresentCode>"
        iString = iString & "<MotoECICode>" & "1" & "</MotoECICode>"
        iString = iString & "<TerminalType>1</TerminalType>"
        iString = iString & "</Terminal>"
        iString = iString & "<Transaction>"
        iString = iString & "<ReferenceNumber>1348172369889-1348172369889</ReferenceNumber>"
        iString = iString & "<TransactionAmount>" & "10.00" & "</TransactionAmount>"
        iString = iString & "</Transaction>"
        iString = iString & "<Card>"
        iString = iString & "<EncryptedTrack2Data>CEAFC2FD0BA3E76E0C062DD2DD4196E111A71424C52561E943142AD271FC0D86C45CC365B8D7E292</EncryptedTrack2Data> "
        iString = iString & "<CardDataKeySerialNumber>A08B000C000003000024</CardDataKeySerialNumber>"
        iString = iString & "<EncryptedFormat>2</EncryptedFormat>"
        iString = iString & "</Card>"
        iString = iString & "</CreditCardSale>"

        Return iString

    End Function

End Class
