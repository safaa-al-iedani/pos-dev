﻿Imports Microsoft.VisualBasic

Public Class CreditInfoDtc

#Region "Default Constructor!"

    ''' <summary>
    ''' Default constructor
    ''' </summary>
    Public Sub New()
    End Sub

#End Region

#Region "Private Members!"

    Private itmCode As String
    Private salUnitPrice As DBNull
    Private salTotal As Double
    Private avalCredit As Double
    Private salQuantity As Integer
    Private crmQuantity As Integer

#End Region

#Region "Properties!"

    Public Property ItemCode() As String
        Get
            Return itmCode
        End Get
        Set(value As String)
            itmCode = value
        End Set
    End Property

    Public Property SalUnitPrc() As DBNull
        Get
            Return salUnitPrice
        End Get
        Set(value As DBNull)
            salUnitPrice = value
        End Set
    End Property

    Public Property SalTot() As Double
        Get
            Return salTotal
        End Get
        Set(value As Double)
            salTotal = value
        End Set
    End Property

    Public Property AvalCred() As Double
        Get
            Return avalCredit
        End Get
        Set(value As Double)
            avalCredit = value
        End Set
    End Property

    Public Property SalQty() As Integer
        Get
            Return salQuantity
        End Get
        Set(value As Integer)
            salQuantity = value
        End Set
    End Property

    Public Property CrmQty() As Integer
        Get
            Return crmQuantity
        End Get
        Set(value As Integer)
            crmQuantity = value
        End Set
    End Property

#End Region

End Class
