﻿Imports Microsoft.VisualBasic


''' <summary>
''' Response object from requesting the zone for a zip code;  returns the zone code if only one found 
'''  and returns the number of rows found; if multiple rows found, then need to redirect to 'Delivery.aspx'
'''  for user selection of zone
''' </summary>
Public Class ZoneFromZipResp

    Property zoneCd As String       'code representing a pickup/delivery and/or inventory availability area
    Property numRows As Integer     ' number of Rows in ZIP2ZONE table for the requested zip code
    Property zoneDes As String
End Class

