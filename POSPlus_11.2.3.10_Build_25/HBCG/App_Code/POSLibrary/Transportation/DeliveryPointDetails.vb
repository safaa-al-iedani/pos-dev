﻿Imports Microsoft.VisualBasic

''' <summary>
''' property for the delivery points details.
''' </summary>
''' <remarks></remarks>
Public Class DeliveryPointDetails
    Property ActualStops As Integer
    Property RemainingStops As Integer
    Property ExmptDelCd As String

End Class
