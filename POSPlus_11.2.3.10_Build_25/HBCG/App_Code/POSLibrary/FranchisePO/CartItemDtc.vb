﻿Imports Microsoft.VisualBasic
'For FranchisePOEntry Line Items
'To insert into FRPO_LN
'Feb 06, 2018 - mariam
Public Class CartItemDtc
    Property ID As Int64 = 0
    Property LineNo As Int64 = 0
    Property ItemCode As String = ""
    Property ItemVSN As String = ""
    Property Qty As Int64 = 0
    Property PO_CST As Double = 0.0
    Property PO_Ext_CST As Double = 0.0
    Property SO_CST As Double = 0.0
    Property SO_Ext_CST As Double = 0.0
    Property Arrival_Date As Date
    Property isPackage As Boolean = False
End Class
