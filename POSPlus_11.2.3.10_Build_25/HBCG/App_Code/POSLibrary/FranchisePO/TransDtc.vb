﻿Imports Microsoft.VisualBasic

'For FranchisePOEntry Tramsaction Details
'To insert into FRPO
'Feb 06, 2018 - mariam
Public Class TransDtc
    Property ID As Int64 = 0
    Property Del_Doc_Num As String = String.Empty
    Property Create_Date As Object
    Property FR_Store_CD As String = String.Empty
    Property FR_Cust_CD As String = String.Empty
    Property FR_Store_Num As String = String.Empty
    Property Buyer_Init As String = String.Empty
    Property VE_CD As String = String.Empty
    Property Ship_Via As String = String.Empty
    Property PO_SRT_CD As String = String.Empty
    Property Corp_Store_CD As String = String.Empty
    Property Corp_Zone_CD As String = String.Empty
    Property PO_CD As String = String.Empty

    Property Comments1 As String = String.Empty
    Property Comments2 As String = String.Empty
    Property Comments3 As String = String.Empty
    Property Comments4 As String = String.Empty
    Property Comments5 As String = String.Empty

    Property PO_Sub_Total As Double = 0.0
    Property PO_TAX_CD As String = String.Empty
    Property PO_TAX_AMT As Double = 0.0
    Property PO_Total As Double = 0.0

    Property SO_Sub_Total As Double = 0.0
    Property SO_TAX_CD As String = String.Empty
    Property SO_TAX_AMT As Double = 0.0
    Property SO_Total As Double = 0.0

    Property Approval_User_Init As String = String.Empty
    Property Approval_Pswd As String = String.Empty
    Property Confirm_Num As String = String.Empty
End Class
