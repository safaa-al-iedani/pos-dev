﻿Imports Microsoft.VisualBasic

''' <summary>
''' Holds all the details to represent an Address
''' </summary>
Public Class AddressDtc

    Property addr1 As String
    Property addr2 As String
    Property city As String
    Property stCd As String    'this is the state code
    Property county As String
    Property country As String
    Property postalCd As String
    Property addrTpCd As String
    Property lastChngDt As Date

End Class
