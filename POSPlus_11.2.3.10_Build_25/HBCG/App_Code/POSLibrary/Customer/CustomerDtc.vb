﻿Imports Microsoft.VisualBasic

''' <summary>
''' Holds the details for a CUSTOMER 
''' </summary>

Public Class CustomerDtc

    Property fname As String = ""
    Property lname As String = ""
    Property addr1 As String = ""
    Property addr2 As String = ""
    Property city As String = ""
    Property state As String = ""
    Property zip As String = ""
    Property hphone As String = ""
    Property bphone As String = ""

    Property billFname As String = ""
    Property billLname As String = ""
    Property billAddr1 As String = ""
    Property billAddr2 As String = ""
    Property billCity As String = ""
    Property billState As String = ""
    Property billZip As String = ""
    Property billHphone As String = ""
    Property billBphone As String = ""

    Property custCd As String = ""
    Property email As String = ""
    Property custTp As String = ""
    Property custTpCd As String = ""
    Property corpName As String = ""

End Class
