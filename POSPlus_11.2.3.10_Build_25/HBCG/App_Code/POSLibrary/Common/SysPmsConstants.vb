﻿Imports Microsoft.VisualBasic

''' <summary>
''' This class to contain ALL system parameters used in the POS application
''' </summary>
''' <remarks></remarks>
Public Class SysPmsConstants

    'the following indicate if auto-comments should be added upon different actions
    Public Const NEW_ITEMS_ADDED As String = "NEW_LINE_ITEMS_ADDED"
    Public Const SKU_CHNG As String = "SKU_CHNG"
    Public Const QTY_CHNG As String = "QTY_CHNG"
    Public Const PRICE_CHNG As String = "RETAIL_CHNG"
    Public Const TAX_CHARGE_CHNG As String = "TAX_CHARGE_CHNG"
    Public Const DELV_CHARGE_CHNG As String = "DEL_CHARGE_CHNG"
    Public Const SETUP_CHARGE_CHNG As String = "SETUP_CHARGE_CHNG"
    Public Const PU_DEL_FLAG_CHNG As String = "PD_FLAG_CHNG"
    Public Const PU_DEL_DATE_CHNG As String = "PU_DT_CHNG"
    Public Const ZONE_CD_CHNG As String = "ZONE_CD_CHNG"

    Public Const FIN_COMPANY_CHNG As String = "FINANCE_COMP_CHNG"
    Public Const FIN_AMOUNT_CHNG As String = "FINANCED_AMT_CHNG"

    Public Const SPIFF_ENABLED As String = "CHK_RET_SPIFF_CLNDR"
    Public Const SPIFF_BY_SKU As String = "SPIFF_BY_SKU"

    'last date Sales History Update (SHU) ran
    Public Const SHU_DATE As String = "LST_SLS_UPDATE_DT"

    'number of days that a transaction can be PRE/POST dated (minimum/maximum date)
    Public Const NUM_DAYS_POST_TRANSDT As String = "TRANS_DAYS_AHEAD"

    'Y indicates to not allow the user to override the default transaction date (effective business date);
    '   the default transaction date is the system date 
    Public Const SKIP_TRANS_DT As String = "SKIP_TRANS_DT"

    'Y indicates commission should be paid on SETUP charges
    Public Const COMMISSION_ON_SETUP_CHARGE As String = "PAY_COMM_CHGS"

    'Y indicates commission should be paid on DELIVERY charges
    Public Const COMMISSION_ON_DELV_CHARGE As String = "PAY_COMM_DEL_CHGS"

    'In POS+, when 'Y' it means that LINE discounts can be applied; when 'N' only HEADER discounts
    Public Const ALLOW_MULT_DISC As String = "ALLOW_MULT_DISC_CD"

    'the discount code to be used on the SO that indicates the orders may have multiple discounts
    'NOTE: this will only be relevant when ALLOW_LINE_DISCOUNT = 'Y'
    Public Const MULTI_DISC_CD As String = "MULT_DISC_CD"

    'value of the drop code to be used for special order SKUs
    Public Const SPECIAL_ORDER_DROP_CD As String = "SPEC_ORD_DROP_CD"

    'Number of days to keep special order SKUs in the system
    Public Const SPECIAL_ORDER_NUM_DAYS As String = "SPEC_ORD_#DAYS"

    'delivery scheduling enabled in sales 
    Public Const DELV_SCHED_ENABLED As String = "DELM"

    'calculate delivery (and/pickup if applicable) by stops or points; 
    '  valid values are 'S' and 'P' for stops/points respectively
    Public Const DELV_STOPS_POINTS As String = "STOPS_OR_POINTS"

    'When DELV_STOPS_POINTS is set to points then POINT_OVER_SCHEDULING will have the values.
    'Valid values are W(Warning),P(Prevention) and N(Neither)
    Public Const POINT_OVER_SCHEDULING As String = "OVR_SCH_WARNING"

    'Y indicates a Sales Order CANNOT be finalized if linked to an IST
    Public Const PREVENT_FINALIZE_SO_LINKEDTO_IST As String = "FINAL_SO_IST"

    'Y indicates RF is enabled in the system, S indicates RF setup is occuring
    Public Const RF_ENABLED As String = "RF_WAREHOUSE"

    'Y indicates serialization of SKUS should be tracked in the system
    Public Const SERIAL_ENABLED As String = "ITEM_SERIAL_TRACKING"

    'if set to Y, Inventory Availability is obtained using Priority Fill
    Public Const PRIORITY_FILL_FOR_INV_AVAIL = "PRI_FILL_FIND_AVAIL"

    'if set to S, priority fill is by store, if set to Z then priority fill is by zone
    Public Const PRIORITY_FILL_BY = "PRIORITY_FILL"

    'Y indicates one warranty can be linked to a multiple skus
    Public Const ONE_WARR_TO_MANY As String = "ONE_WARR_SKU"

    'number of days before system date allowed to confirm sales order
    Public Const CONFIRMATION_DAYS As String = "CONFIRMATION_DAYS"

    'NOTE: this will only be relevant when UPDATE_TRUCK_DATE = 'Y'
    Public Const UPDATE_TRUCK_DATE As String = "UPDATE_TRUCK_DATE"

    'CRM affected delivery availablility
    Public Const CRM_AFFECT_DEL_AVAIL As String = "CRM_AFFECT_DEL_AVAIL"

    'NOTE: this will only be relevant when ENABLE_PICKUP = 'Y'
    Public Const ENABLE_PICKUP As String = "ENABLE_PICKUP"

    '==========================================================================
    '====== THE FOLLOWING VALUES ARE CURRENTLY DEFINED AS GP-PARMs IN E1. =====
    ' Later, these may change to be System Parameters, at that point, only the
    ' way these values are read will have to change (in the SystemBiz class).
    '==========================================================================
    ' Next two next values, are used in the custom_paid package;  here for documentation only
    Private Const PTS_DEBUG_KEY As String = "PTS_DEBUG"
    ' indicates that custom paid in full logic (without customer logic in place) 
    'will indicate paid in full (Y) or not (N); entirely for application testing
    Private Const PAID_IN_FULL_CUSTOM_TEST As String = "PAID_IN_FULL_CUSTOM_TEST"
    '========
    'Keys to access the different GP PARMS 
    Public Const PTS_POS_KEY As String = "PTS_POS+"
    Public Const PTS_ARASM_KEY As String = "PTS_ARASM"

    'time elapsed, before user is logged out of POS+  (E1 setting represent minutes)
    Public Const APP_TIMEOUT As String = "TIMEOUT_MINUTES"

    '** For finance auth only case - to address case where auth's exp dt doesn't match the PAuth done in E1.
    'This value is used by the E1 asp_store_forward table trigger, instead of being hardcoded to 12/31/2017
    Public Const DEF_MAX_EXP_DT As String = "DEF_MAX_EXP_DT"

    'defaults to Not search - checkbox question 'Do not search for dropped items' checked by default
    Public Const SEARCH_DROP_ITM_DEFAULT As String = "SEARCH_DROP_ITM_DEFAULT"

    ' number of days to display relationships
    Public Const RELATIONSHIP_DISPLAY_DAYS As String = "RELATIONSHIP_DISPLAY_DAYS"

    ' special process for an SBOB row
    Public Const SBOB_ROW_PROCESS As String = "SBOB_SPECIAL_ROW_PROCESS"
    Public Const SBOB_ROW_HGHLGHT_SPECORD_DROPPED As String = "HIGHLIGHT_SYSPM_SPECORD_DROPPED"
    Public Const SBOB_ROW_HGHLGHT_ITMSRT_MTO_PLUS As String = "HIGHLIGHT_ITMSRT_MTO_PLUS"
    Public Const SBOB_ROW_HGHLGHT_ORDSRT As String = "HIGHLIGHT_ORDSRT"
    Public Const SBOB_ROW_HGHLGHT As String = "HIGH_LIGHT"

    ' indicates to use RCM for PO link confirmation in PO/IST column 
    Public Const SBOB_PO_RCM_CONFIRM As String = "SBOB_PO_RCM_CONFIRM"

    ' indicates to use Custom paid in full logic or standard; default is standard; 
    ' if value = 'CUSTOM', then call custom_paid logic
    Public Const PAID_IN_FULL_CALC As String = "PAID_IN_FULL_CALC"
    Public Const PAID_IN_FULL_CALC_CUSTOM As String = "CUSTOM"

    Public Const BEG_OF_TIME As String = "BEG_OF_TIME"      'represents the beginning of time
    Public Const END_OF_TIME As String = "END_OF_TIME"      'represents the end of time

    Public Const WARRANTY_START_DATE As String = "WARR_BASE_CD"
    'referes the parameter " Number of pad days for receiving POs and ISTs when calculating availability?" Under MERCH, POSITION 23
    Public Const AVAIL_LEAD_DAYS As String = "AVAIL_LEAD_DAYS"
    '==========================================================================
    '=============== END OF VALUES DEFINED AS GP-PARMs IN E1. =================
    '==========================================================================

End Class
