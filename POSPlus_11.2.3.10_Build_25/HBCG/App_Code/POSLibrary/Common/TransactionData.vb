﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to represent any data that is related to a transaction. It is present
''' from the time of login to logout.
''' </summary>
Public Class TransactionData

    'this gets created as soon as a user logs in
    Property emp As New EmployeeDtc

    'this is applicable only when the system parameter to use 'cashier activation' is enabled
    Property cshDwrCd As String
    Property coCd As String
    Property tranDt As String 'is the cashier activation/post date
    Property cshInit As String


End Class


