﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

''' <summary>
''' Class that encapsulates any custom extension methods needed by the application.
''' </summary>
Public Module AppExtensions

    ''' <summary>
    '''  Extension method that validates if a string is NOT 'empty' implying that it is
    ''' not null, not empty(zero-length string) and does not consist of  just whitespaces.
    ''' </summary>
    ''' <param name="input">the string to check</param>
    ''' <returns>True to indciate that the string is truly not 'empty'.  False otherwise.</returns>
    <Extension()> _
    Public Function isNotEmpty(ByVal input As String) As Boolean
        Return (Not (isEmpty(input)))
    End Function

    ''' <summary>
    ''' Extension method that validates if a string is 'empty' implying that could be 
    ''' null, empty(zero-length string) or consists of just whitespaces.
    ''' </summary>
    ''' <param name="input">the string to check</param>
    ''' <returns>True to indciate that the string is 'empty'.  False otherwise.</returns> 
    <Extension()> _
    Public Function isEmpty(ByVal input As String) As Boolean
        Return (String.IsNullOrEmpty(input) OrElse String.IsNullOrWhiteSpace(input) OrElse input = "&nbsp;")
    End Function


    ''' <summary>
    ''' Extension method that substrings or trims the given string based on the start and end positions
    ''' passed-in. This method will NOT throw an exception if trying to extract too much from the input string.
    ''' It will always truncate longer strings while leaving shorter ones unchanged.
    ''' </summary>
    ''' <param name="input">the string to be trimmed</param>
    ''' <param name="start">this starting position to begin extracting the string</param>
    ''' <param name="length">the desired length of the string</param>
    ''' <returns>a new string that is the input string trimmed from the left</returns>
    <Extension()> _
    Public Function trimString(ByVal input As String, ByVal start As Integer, ByVal length As Integer) As String

        Dim rtnVal As String = String.Empty

        If (input.isNotEmpty) Then
            'if  the starting position is less than zero, OR greater than the length of the string,
            ' reset it to zero to avoid an index out of range error.
            If (start < 0 Or start > input.Length()) Then
                start = 0
            End If

            'truncate or return the input string as is based on length
            If (input.Length() > length AndAlso input.Length() > start) Then
                rtnVal = input.Substring(start, length)
            Else
                rtnVal = input
            End If
        End If

        Return rtnVal

    End Function


End Module
