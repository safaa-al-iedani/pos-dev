﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to represent any data that is related to an employee.
''' </summary>
Public Class EmployeeDtc

    Property homeStore As String
    Property cd As String
    Property init As String
    Property fName As String
    Property lName As String

    'if 'Y' implies that all securities are ignored thereby making the user a 'super' user. If 'N' securities are considered
    Property skipSecurity As Boolean

    'if 'Y', then user has access to all stores and store groups. If 'N' and store sec. is enabled, user has limited store access
    Property skipDataSecurity As Boolean

End Class
