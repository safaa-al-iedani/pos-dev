﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

''' <summary>
''' Class that encapsulates any custom extension methods needed by the application
''' that pertains specifically to UI components like textboxes, etc..
''' </summary>
Public Module UIExtensions

    ''' <summary>
    ''' Selects and highlights the contents of the speficied textbox. It injects the appropriate 
    ''' JavaScript into the web page being invoked and invokes the select function for the rendered textbox.
    ''' </summary>
    ''' <param name="txtBox">the text box whose text to select/highlight</param>
    <Extension()> _
    Public Sub SelectText(ByVal txtBox As TextBox)
        'Is there a ScriptManager on the page?
        If ScriptManager.GetCurrent(txtBox.Page) IsNot Nothing AndAlso ScriptManager.GetCurrent(txtBox.Page).IsInAsyncPostBack Then
            'Set ctrlToSelect
            ScriptManager.RegisterStartupScript(txtBox.Page,
                                                txtBox.Page.GetType(),
                                                "SetFocusInUpdatePanel-" & txtBox.ClientID,
                                                String.Format("ctrlToSelect='{0}';", txtBox.ClientID),
                                                True)
        Else
            txtBox.Page.ClientScript.RegisterStartupScript(txtBox.Page.GetType(),
                                                        "Select-" & txtBox.ClientID,
                                                        String.Format("document.getElementById('{0}').select();", txtBox.ClientID),
                                                        True)
        End If
    End Sub


End Module