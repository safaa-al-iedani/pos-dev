﻿<Serializable()>
Public Class AccessControlDtc
    Private _employeeInitials As String
    Private _screenName As String
    Private _browserId As String

    Public Property EmployeeInitials() As String
        Get
            Return _employeeInitials
        End Get
        Set(ByVal value As String)
            _employeeInitials = value
        End Set
    End Property

    Public Property ScreenName() As String
        Get
            Return _screenName
        End Get
        Set(ByVal value As String)
            _screenName = value
        End Set
    End Property

    Public Property BrowserId() As String
        Get
            Return _browserId
        End Get
        Set(ByVal value As String)
            _browserId = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal employeeInitials As String, ByVal screenName As String, ByVal browserId As String)
        _employeeInitials = employeeInitials
        _screenName = screenName
        _browserId = browserId
    End Sub

End Class
