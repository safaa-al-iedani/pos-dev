﻿Imports Microsoft.VisualBasic

Public Class ZoneInfo
    Property ZoneCode As String
    'Selected Store code
    Property ZoneDescription As String
    'Selected Store description
    Property DeliveryStoreCode As String
    'Selected Store Code
    Property SetupCharges As String
    'Selected Setup charges
    Property Deliverycharges As String
    'Selected Delivery charges
    Property ZipCode As String
    ''' <summary>
    ''' Initilize the members..
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()   'constructor
        ZoneCode = String.Empty
        ZoneDescription = String.Empty
        DeliveryStoreCode = String.Empty
        SetupCharges = String.Empty
        Deliverycharges = String.Empty
        ZipCode = String.Empty
    End Sub
End Class
