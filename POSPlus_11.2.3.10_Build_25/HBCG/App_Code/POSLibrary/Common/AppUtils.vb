﻿Imports Microsoft.VisualBasic
Imports System.Security.Cryptography

Public Class AppUtils
    ''' <summary>
    ''' Encrypts the string value passed in, using the key provided.
    ''' </summary>
    ''' <param name="strValue">the value to be encrypted</param>
    ''' <param name="strKey">the key to be used for encrypting</param>
    ''' <returns>the encrypted value</returns>
    Public Shared Function Encrypt(ByVal strValue As String, ByVal strKey As String) As String

        Dim rtnVal As String = String.Empty

        If (strValue.isEmpty()) Then
            rtnVal = strValue
        Else
            Dim cryptMD5Hash As New MD5CryptoServiceProvider()
            Dim cryptDES3 As New TripleDESCryptoServiceProvider()
            cryptDES3.Mode = CipherMode.ECB
            cryptDES3.Key = cryptMD5Hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strKey))

            Dim bytValue() As Byte = ASCIIEncoding.ASCII.GetBytes(strValue)
            Dim encryptor As ICryptoTransform = cryptDES3.CreateEncryptor()

            rtnVal = Convert.ToBase64String(encryptor.TransformFinalBlock(bytValue, 0, bytValue.Length))
            rtnVal = rtnVal.Replace(" ", "@@@").Replace("+", "$$$")
        End If

        Return rtnVal
    End Function

    ''' <summary>
    ''' Decrypts the string value passed in, using the key provided.
    ''' </summary>
    ''' <param name="strValue">the value to be decrypted</param>
    ''' <param name="strKey">the key to be used for decrypting</param>
    ''' <returns>the decrypted value</returns>
    Public Shared Function Decrypt(ByVal strValue As String, ByVal strKey As String) As String

        Dim rtnVal As String = String.Empty

        If (strValue.isEmpty()) Then
            rtnVal = strValue
        Else
            Dim cryptMD5Hash As New MD5CryptoServiceProvider()
            Dim cryptDES3 As New TripleDESCryptoServiceProvider()
            cryptDES3.Mode = CipherMode.ECB
            cryptDES3.Key = cryptMD5Hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strKey))

            strValue = strValue.Replace("@@@", " ").Replace("$$$", "+")
            Dim bytValue() As Byte = Convert.FromBase64String(strValue)

            Dim decryptor As ICryptoTransform = cryptDES3.CreateDecryptor()
            rtnVal = ASCIIEncoding.ASCII.GetString(decryptor.TransformFinalBlock(bytValue, 0, bytValue.Length))
        End If
        Return rtnVal

    End Function

End Class
