﻿Imports System.Collections.Generic
Imports Microsoft.VisualBasic

'This class represents the Package Item which has components and the package price as well
Public Class PackageItem
    'Package SKU
    Public Property PackageSKU As String
    Public Property PackagePrice As Double

    'this has the list of package components
    Public Property Components As List(Of PackageComponent)

    Sub New()
        Components = New List(Of PackageComponent)
        PackageSKU = ""
        PackagePrice = 0.0
    End Sub
    Sub New(ByVal PackageSku As String, ByVal price As Double)
        Components = New List(Of PackageComponent)
        PackageSku = PackageSku
        PackagePrice = price
    End Sub
End Class
