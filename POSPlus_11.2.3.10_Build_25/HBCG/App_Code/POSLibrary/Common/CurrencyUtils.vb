﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Globalization
Imports System.Linq

Public NotInheritable Class CurrencyUtils

    Private Sub New()
    End Sub

    Private Shared ReadOnly SymbolsByCode As Dictionary(Of String, String)

    Public Shared Function GetSymbol(code As String) As String
        Dim currSymbol As String = String.Empty
        Try
            currSymbol = SymbolsByCode(code)
        Catch ex As Exception

        End Try
        Return currSymbol
    End Function

    Shared Sub New()
        SymbolsByCode = New Dictionary(Of String, String)()

        Dim regions = CultureInfo.GetCultures(CultureTypes.SpecificCultures).[Select](Function(x) New RegionInfo(x.LCID))

        For Each region As Object In regions
            If Not SymbolsByCode.ContainsKey(region.ISOCurrencySymbol) Then
                SymbolsByCode.Add(region.ISOCurrencySymbol, region.CurrencySymbol)
            End If
        Next
    End Sub
End Class