﻿Imports Microsoft.VisualBasic
''' <summary>
''' Class that encapsulates the details needed when Schedule deliveries by (P)oints or (S)tops 
''' </summary>
Public Class TransportationPoints
    Property origPointTot As Double
    Property currPointTot As Double
    Property unschedPointTot As Double
    Property schedPointTot As Double

    Public Sub TransportationPoints()
        origPointTot = 0
        currPointTot = 0
        unschedPointTot = 0
        schedPointTot = 0
    End Sub
End Class
