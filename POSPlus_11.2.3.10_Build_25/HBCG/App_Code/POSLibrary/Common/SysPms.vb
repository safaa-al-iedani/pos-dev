﻿Imports Microsoft.VisualBasic

''' <summary>
''' Module for caching system parameter values to eliminate many db hits
'''  -These are loaded upon Application-Start in the Global.asax; add to as needed
''' </summary>
Public Module SysPms

    Property isCashierActiv As Boolean      'indicates if Cashier Activation is Enabled
    Property isPrcByStoreGrp As Boolean     'TRUE pricing is by Store Group; False otherwise

    Property isRFEnabled As Boolean         'indicates if the RF functionality is enabled
    Property isSerialEnabled As Boolean     'indicates if Serialization is enabled in the system
    Property isOne2ManyWarr As Boolean      'indicates whether one warranty can be linked to a single or multiple skus

    Property isDelvSchedEnabled As Boolean  'TRUE if delivery scheduling enabled, FALSE otherwise
    Property isDelvByPoints As Boolean      'TRUE delivery is by POINTS; FALSE deliv ery is by STOPS

    Property pointOverScheduling As String   'Returns either W(Warning),P(Prevention) and N(Neither)

    Property preventFinalSOonIST As Boolean         'TRUE an order CANNOT be finalized when LINKED to an IST
    Property priorityFillForInvAvail As Boolean     'Use priority fill when searching Inventory Available
    Property isPriorityFillByZone As Boolean        'true if priority fill by zone, false otherwise (by store)
    Property isSpiffEnabled As Boolean              'true if SPIFF pricing should be used, false otherwise
    Property isSpiffBySKU As Boolean                'true if SPIFF SKU should be used, false for SPIFF by store

    'MM-5693
    Property isAutoFinalizeMCRMDB As Boolean

    Property skipTransDate As Boolean

    Property commissionOnDelvCharge As Boolean      'pay commissions on DELIVERY charges
    Property commissionOnSetupCharge As Boolean     'pay commissions on SETUP charges

    Property reapplyDisc As Boolean         'indicates if discounts should be recalculated during a price override
    Property allowMultDisc As Boolean       'Indicates if LINE discounts can be applied to an order
    Property multiDiscCd As String          'Discount code to be used for multiple discounts
    'MM-5661
    Property discWithAppSec As String       'represents discount security
    Property specialOrderDropCd As String   'the drop code to be used for Special Order SKUs
    Property specialOrderNumDays As Integer 'number of days to keep special order SKUs in the system

    'Property shuDate As String              'last time Sales History ran  - DO NOT USE HERE - SALES HISTORY DATE MUST ALWAYS USE CURRENT DATABASE VALUE - CANNOT BE LOADED ONCE
    Property numDaysPostDtTrans As Integer  'number of days to allow PRE/POST date transactions
    Property numDaysToConfirmSO As Integer  'number of days to confirm a Sales/CRM Order 

    'the following used for Adding AUTO-COMMENTS in the application
    Property cmtNewItems As Boolean
    Property cmtSkuChng As Boolean
    Property cmtQtyChng As Boolean
    Property cmtPriceChng As Boolean
    Property cmtTaxChargeChng As Boolean
    Property cmtDelChargeChng As Boolean
    Property cmtSetupChargeChng As Boolean
    Property cmtPuDelFlagChng As Boolean
    Property cmtPuDelDateChng As Boolean
    Property cmtZoneCdChng As Boolean
    Property cmtFinCompanyChng As Boolean
    Property cmtFinAmountChng As Boolean
    Property deliveryChargeApproval As String 'Syspm Reference 10 .Sale> 31  Require approval to override default delivery charges in PSOE and SOE?
    Property allowCODBalance As String

    '==========================================================================
    '====== THE FOLLOWING VALUES ARE CURRENTLY DEFINED AS GP-PARMs IN E1. =====
    ' Later, these may change to be System Parameters, at that point, only the
    ' way these values are read will have to change (in the SystemBiz class).
    '==========================================================================
    Property appTimeOut As String               'represents SECONDS elapsed before the app times out
    Property paidInFullCalc As String           'indicates if custom logic is used to determine if 'Paid in Full'
    Property sbobRowProcess As String           'indicates the special attributes to apply to a row
    Property sbobPoRcmConfirm As String         'indicates to use RCM for PO link confirmation
    Property numDaysRelship As Integer          'number of days to display Relationships
    Property searchDropSkus As Boolean          'indicates if search should include DROPPED SKUs by default
    Property maxExpDt As String                 'value used by E1 asp_store_forward table trigger, instead of hardcoded to 12/31/2017
    Property updateTruckDate As Boolean         'indicates if Update the delivery/transfer/truck date in SOM/ISTM/TDM?
    Property crmAffectDelAvail As Boolean       'indicates to use CRM affected delivery availablility
    Property enablePickup As Boolean            'indicates if ENABLE_PICKUP?
    '==========================================================================
    '====== THE FOLLOWING VALUES ARE CURRENTLY DEFINED AS GP-PARMs IN E1. =====
    ' These are E1 gp_parms from the century project and are integrated tightly
    ' as gp_parms in the entire E1 application so if move to syspm, then
    ' need to modify E1 also
    '==========================================================================
    Property begOfTime As String                'value assigned as the beginning of time
    Property endOfTime As String                'value assigned as the end of time

    '==========================================================================
    '=============== END OF VALUES DEFINED AS GP-PARMs IN E1. =================
    '==========================================================================
    Property warrantyStartDate As String        'Represents the warranty start date

    Property PadDaysForReceivingPOandIST As Int32

End Module
