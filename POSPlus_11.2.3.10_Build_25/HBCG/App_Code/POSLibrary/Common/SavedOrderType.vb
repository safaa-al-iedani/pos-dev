﻿Imports Microsoft.VisualBasic
''' <summary>
''' This Class is used to store the OrderTypeCode,OrderNumber,CauseCode where saved order Retrieving from the SavedOrders link in the application 
''' </summary>
''' <remarks></remarks>
Public Class SavedOrderType
    Property OrderTypeCode As String = ""
    Property OrderNumber As String = ""
    Property CauseCode As String = ""
End Class
