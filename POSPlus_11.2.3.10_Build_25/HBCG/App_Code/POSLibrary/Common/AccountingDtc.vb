﻿Imports Microsoft.VisualBasic

Public Class AccountingDtc
    Property Amount As Double
    Property AccountKey As String
    Property MopCode As String
    Property TransactionTypeCode As String
    Property StatusCode As String
    Property OriginCode As String
    Property IvcCode As String
    Property AdjIvcCode As String
    Property Description As String
    Property PostDate As String
    Property DocumentSeqNumber As String
End Class
