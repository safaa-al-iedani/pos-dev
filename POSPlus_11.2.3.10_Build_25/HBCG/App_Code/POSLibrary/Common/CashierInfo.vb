﻿Imports Microsoft.VisualBasic
''' <summary>
''' this class holds the details of the cashier activation 
''' </summary>
''' <remarks></remarks>
''' 

Public Class CashierInfo
    'Cashier initials
    Property CashierInitials As String
    'Activated Store code
    Property ActivatedStoreCode As String
    'Activated Cash drawe
    Property ActivatedCashDrawerCode As String
    'Company Code
    Property CompanyCode As String
    'post Date
    Property Postdate As String
    ''' <summary>
    ''' Initilize the members..
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()   'constructor
        CashierInitials = String.Empty
        ActivatedStoreCode = String.Empty
        ActivatedCashDrawerCode = String.Empty
        CompanyCode = String.Empty
        Postdate = String.Empty
    End Sub
End Class
