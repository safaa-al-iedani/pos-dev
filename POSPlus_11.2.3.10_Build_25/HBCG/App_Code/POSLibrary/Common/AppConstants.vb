﻿Imports Microsoft.VisualBasic

Public Class AppConstants


    ''' <summary>
    ''' These constants reprsent the message codes that get returned by the
    ''' 'bt_adv_res.check_inventory' API that gets called to validate inventory
    ''' </summary>
    ''' <remarks>Currently used for an ARS enabled store.</remarks>
    Public Const NOT_PAID_IN_FULL As Integer = 100
    Public Const TO_BE_ORDERED As Integer = 101
    Public Const RESERVE_TO_PO As Integer = 102
    Public Const RESERVE_TO_INV As Integer = 103
    Public Const CHANGE_PU_DATE As Integer = 104
    Public Const MAKE_TO_ORDER As Integer = 105
    Public Const CANNOT_BE_ORDERED As Integer = 106


    ' ***  TRANSPORTATION METHOD *** 
    Public Const DELIVERY As String = "D"
    Public Const PICKUP As String = "P"

    ''' <summary>
    ''' Used to determine the logic that needs to be executed, based on the E1 APP version
    ''' These values are set to the lowest version available for the new logic.
    ''' The new logic is to be used if version > or = to this.   Also, it needs to be 
    ''' 8 digits for v11.1 and v12.x, and 10 digits for 2012
    ''' </summary>
    Public Class NewLogic
        Public Const CUST_GEN_API As Integer = 11010800 ' "11.1.8"
        Public Const INV_API As Integer = 11010900      ' "11.1.9"   
        Public Const ITM_INV As Integer = 12010000      ' "12.1"   
        Public Const REGEXP As Integer = 12010000       ' "12.1"  ' REGEXP was new in Oracle 10g so not available in 11.1 which is 8i
    End Class

    ''' <summary>
    ''' Represent the Order types as well as the different status on an order
    ''' </summary>
    Public Class Order
        Public Const STATUS_VOID As String = "V"
        Public Const STATUS_FINAL As String = "F"
        Public Const STATUS_OPEN As String = "O"

        Public Const TYPE_CRM As String = "CRM"
        Public Const TYPE_SAL As String = "SAL"
        Public Const TYPE_MCR As String = "MCR"
        Public Const TYPE_MDB As String = "MDB"
    End Class

    ''' <summary>
    ''' Discounts related constants
    ''' </summary>
    Public Class Discount
        Public Const LEVEL_HEADER As String = "H"
        Public Const LEVEL_LINE As String = "L"
        Public Const LEVEL_EITHER As String = "E"

        Public Const TP_PCT As String = "P"
        Public Const TP_AMT As String = "A"

        Public Const TP_CD_DOLLAR As String = "D"

        ' indicates table minor extracted from; order by to get Condtional first; need to maintain conditional first
        Public Const MNR_TP_COND As String = "C"   ' indicates that the minor is extracted from the COND_MNR table
        Public Const MNR_TP_DISC As String = "D"   ' indicates that the minor is extracted from the MNR_DISC table

        'MM-5661
        Public Const WITH_APPROVAL As String = "W"
        Public Const WITHOUT_APPROVAL As String = "O"
        Public Const NOT_AT_ALL As String = "N"
    End Class

    ''' <summary>
    ''' Package Split constants
    ''' </summary>
    Public Class PkgSplit
        'timing of the package price split
        Public Const ON_ENTRY As String = "Y"
        Public Const ON_COMMIT As String = "C"
        Public Const NOT_REALTIME As String = "N"

        Public Const METHOD_PERCENT As String = "P"  ' Percent is the default
        Public Const METHOD_ABSOLUTE As String = "A"
        Public Const METHOD_GROSS As String = "G"
    End Class

    ''' <summary>
    ''' Inventory related constants
    ''' </summary>
    Public Class Inv
        Public Const ITM_RES_RES_ID_LEN As Integer = 13
        ' Item transaction types from the ITT_TRN_TP table
        Public Const ITM_TRN_TP_SOF As String = "SOF"   ' Sales order finalization
        Public Const ITM_TRN_TP_CMF As String = "CMF"   ' Credit Memo finalization

        Public Const PICK_STATUS_CHECKED As String = "Checked"
        Public Const PICK_STATUS_PICKED As String = "Picked"
        Public Const PICK_STATUS_EXCEPTION As String = "Exception"
        Public Const PICK_STATUS_SCHEDULED As String = "Scheduled"

        Public Const SERIAL_TP_F As String = "F"
        Public Const SERIAL_TP_S As String = "S"
        Public Const SERIAL_TP_T As String = "T"
        Public Const SERIAL_TP_L As String = "L"
    End Class

    ''' <summary>
    ''' SKU related constants
    ''' </summary>
    Public Class Sku
        Public Const TP_INV As String = "INV"
        Public Const TP_FAB As String = "FAB"
        Public Const TP_LAB As String = "LAB"
        Public Const TP_MIL As String = "MIL"
        Public Const TP_NLN As String = "NLN"
        Public Const TP_PAR As String = "PAR"
        Public Const TP_WAR As String = "WAR"
        Public Const TP_NLT As String = "NLT"
        Public Const TP_SVC As String = "SVC"
        Public Const TP_PKG As String = "PKG"
        Public Const TP_DNR As String = "DNR"
        Public Const TP_DEL As String = "DEL"
        Public Const TP_SETUP As String = "SETUP"
    End Class

    ''' <summary>
    ''' Location related constants
    ''' </summary>
    Public Class Loc
        Public Const TYPE_CD_W As String = "W"  'warehouse
        Public Const TYPE_CD_S As String = "S"  'showroom
        Public Const TYPE_CD_O As String = "O"  'outlet 
    End Class

    ''' <summary>
    ''' Manager override constants
    ''' </summary>
    Public Class ManagerOverride
        Public Const NONE As String = "N"
        Public Const BY_LINE As String = "L"
        Public Const BY_ORDER As String = "O"
    End Class

    ''' <summary>
    ''' Constants used in building certain queries
    ''' </summary>
    ''' <remarks>MM-9935</remarks>
    Public Class AdvancedQueryConstants
        ' when building quasi-bulk queries, how many bound vars to use?  this constant keeps them consistent and thus re-usable in sqlarea
        Public Const NormalNumberOfBoundParameters = 100
        ' if there is a reason to use a shorter list in some query, we still want to keep it consistent to enable re-use
        Public Const LimitedNumberOfBoundParameters = 20
    End Class

    ''' <summary>
    ''' Oracle Error codes
    ''' </summary>
    Public Class OraErr
        Public Const RECORD_LOCKED As Integer = 54
    End Class
End Class
