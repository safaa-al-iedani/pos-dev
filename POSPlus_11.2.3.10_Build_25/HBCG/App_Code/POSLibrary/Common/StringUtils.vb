﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Text
Imports System.Linq

Public Class StringUtils

    ''' <summary>
    ''' Convert Y and N values to true or false respectively, since BT integration can only handle primitive types.
    ''' Resolve Char Y/N to boolean for better readability; specifically can only be used if N equates to False 
    ''' and Y equates to True.   Default occurs if the input is blank 
    ''' </summary>
    ''' <param name="inpChar">Input Y or N</param>
    ''' <param name="deflt">default value to return if empty input</param>
    ''' <returns>true if the input value is a Y; default value as input or false if the input value is empty; false otherwise</returns>
    Public Shared Function ResolveYNToBoolean(ByVal inpChar As String, Optional ByVal deflt As Boolean = False) As Boolean

        If String.IsNullOrEmpty(inpChar) Then
            Return deflt
        Else
            Return (If("Y".Equals(inpChar), True, False))
        End If

    End Function

    ''' <summary>
    ''' Retrieves the substring till the specified occurence of a sub string. 
    ''' Example, if given the string "a,b,c,d,e,f and want to read the substring
    ''' up to the 4th occurence of "," (ie, we need the substring "a,b,c,d") 
    ''' then call this method as GetSubString("a,b,c,d,e,f",",",4)
    ''' </summary>
    ''' <param name="source">The source string</param>
    ''' <param name="delimeter">the delimeter in use</param>
    ''' <param name="upToOccurence">number of delimiters before extracting the substring</param>
    ''' <returns>the extracted substring or the passed in source if it cannot substring</returns>
    Public Shared Function GetSubString(ByVal source As String,
                                        ByVal delimeter As String,
                                        ByVal upToOccurence As Integer) As String
        Dim rtnVal As String = source
        If (Not String.IsNullOrEmpty(source)) Then
            Dim sourceArray() = source.Split(delimeter)
            If sourceArray.Length > upToOccurence Then
                Dim resultString As StringBuilder = New StringBuilder(source.Length())
                For cnt As Integer = 0 To upToOccurence - 1
                    resultString.Append(sourceArray(cnt).ToString())
                    If (cnt <> upToOccurence - 1) Then
                        resultString.Append(delimeter)
                    Else
                        'Do Nothing
                    End If
                Next
                rtnVal = resultString.ToString()
            End If
        End If
        Return rtnVal

    End Function

    ''' <summary>
    ''' Replaces each character in the given string, up to the length specified with the character passed in. 
    ''' The number of characters to be replaced can be limited by passing in the number of characters from 
    ''' the end of the string to leave unreplaced.  
    ''' Example:
    '''           maskString("5551233331234", '#', 12', 4)   will produce the following:  ########1234
    ''' </summary>
    ''' <param name="str">any string</param>
    ''' <param name="c">the character to replace with</param>
    ''' <param name="len">the length of the string</param>
    ''' <param name="charToLeave">the number of characters from the end of the string to leave untouched</param>
    ''' <returns>given string masked to the specified length with the given character. If the length of
    '''          the string is less than the 'charToLeave' value passed in then the given string is 
    '''          returned unmodified
    ''' </returns>
    Public Shared Function MaskString(ByVal str As String, ByVal c As Char,
                                      ByVal len As Integer, ByVal charToLeave As Integer) As String

        Dim maskedString As String = String.Empty

        If (str.isNotEmpty) Then
            If (str.Length >= charToLeave) Then
                Dim last4 As String = Right(str, charToLeave)
                maskedString = last4.PadLeft(len, c)
            Else
                maskedString = str
            End If
        End If

        Return maskedString

    End Function


    ''' <summary>
    ''' Masks all but the last four characters of the given string with the character passed in. 
    ''' <b>Example:</b>
    ''' 
    '''   maskString("5551233331234", '#')    would produce the following:
    ''' 
    ''' <b> #########1234</b>
    ''' </summary>
    ''' <param name="str">any string</param>
    ''' <param name="c">the character to replace with</param>
    ''' <returns>given string with all but the last 4 characters replaced by the given character.
    '''          If the length of the string is less than or equal to 4, then the passed-in string 
    '''          is returned unmodified
    '''  </returns>
    Public Shared Function MaskExceptLastFour(ByVal str As String, ByVal c As Char) As String
        Return (MaskString(str, c, str.Length, 4))
    End Function

    ''' <summary>
    ''' Makes sure the value passed in, is a numeric, and if so, formats it as a phone number;
    ''' otherwise return the same value given to this method
    ''' </summary>
    ''' <param name="phoneNum">phone number to be formatted</param>
    ''' <returns>formatted phone number or same value if not numeric</returns>
    Public Shared Function FormatPhoneNumber(ByVal phoneNum As String) As String

        Dim formattedPhone As String = phoneNum
        If IsNumeric(Replace(phoneNum, "-", "")) Then
            phoneNum = Replace(phoneNum, "-", "")
            If phoneNum.Length = 10 Then
                formattedPhone = String.Format("{0}-{1}-{2}", phoneNum.Substring(0, 3), phoneNum.Substring(3, 3), phoneNum.Substring(6))
            ElseIf phoneNum.Length = 7 Then
                formattedPhone = String.Format("{0}-{1}", phoneNum.Substring(0, 3), phoneNum.Substring(3))
            End If
        End If
        Return formattedPhone

    End Function
    ''' <summary>
    '''this function eliminates the repeated commas in a string to a singe comma.
    ''' </summary>
    ''' <param name="str">Input string which may have a comma's like a,,,b and returns a,b</param>

    Public Shared Function EliminateCommas(ByVal str As String)
        'replace repeated "Comma" in the column
        str = Regex.Replace(str, "(\,)\1{1,}", "$1").Trim()
        str = If(str.EndsWith(","), str.Substring(0, str.Length - 1), str)
        Return str
    End Function

    ''' <summary>
    ''' Works same as Oracle Null Value function (NVL) except just for strings (at this point)
    ''' Oracle function: if obj1 is nothing or null or empty, then return obj2 but this is a bit diff
    ''' in that if obj2 is also nothing, then return an empty string
    ''' </summary>
    Public Shared Function NullValStr(ByVal obj1 As Object, ByVal obj2 As String) As String
        'if obj1 is not null then return obj1
        If Not (IsNothing(obj1) OrElse IsDBNull(obj1) OrElse String.IsNullOrEmpty(obj1)) Then
            Return obj1
        ElseIf Not (IsNothing(obj2)) Then
            'If obj1 is null return obj2
            Return obj2
        End If
        ' if get here, return an empty string
        Return String.Empty
    End Function

    ''' <summary>
    ''' Returns a comma-delimited list of the elements in the passed-in array list. It also
    ''' adds single quotes around each list element. e.g. 'Red','Blue', etc.
    ''' </summary>
    ''' <param name="aList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDelimitedString(ByVal aList As ArrayList) As String

        Dim rtnVal As String = ""
        If aList IsNot Nothing AndAlso aList.Count > 0 Then

            rtnVal = String.Join("','", aList.ToArray())

            If Not rtnVal.EndsWith("'") Then
                rtnVal = rtnVal + "'"
            End If

            If Not rtnVal.StartsWith("'") Then
                rtnVal = "'" + rtnVal
            End If
        End If
        Return rtnVal

    End Function

    ''' <summary>
    ''' Remove the aplha charater from the input struing
    ''' </summary>
    ''' <param name="query_val"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Query_String_Prepare(ByVal query_val As String)

        Query_String_Prepare = ""
        Dim x As Integer
        For x = 1 To Len(query_val)
            If Mid(query_val, x, 1) = "%" Then
                Query_String_Prepare = Query_String_Prepare & Mid(query_val, x, 1)
            Else
                Query_String_Prepare = Query_String_Prepare & Regex.Replace(Mid(query_val, x, 1), "\D", String.Empty)
            End If
        Next

        Return Query_String_Prepare

    End Function

    ''' <summary>
    ''' Appending the wildchar “%” at the end of phone number if the input does ’t have that character "%" and lenth is less not equal to 10
    ''' </summary>
    ''' <param name="phoneNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AppendWildChar(ByVal phoneNum As String)
        If Not phoneNum.Contains("%") AndAlso Regex.Replace(phoneNum, "\D", String.Empty).Count <> 10 Then
            Return phoneNum + "%"
        Else
            Return phoneNum
        End If
    End Function

    ''' <summary>
    ''' returns like operator or = operator for building the sql query 
    ''' </summary>
    ''' <param name="inp"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function equalOrLike(ByVal inp As String) As String

        Dim eqOrLike As String = "="

        If InStr(inp, "%") > 0 Then

            eqOrLike = "LIKE"
        End If

        Return eqOrLike
    End Function

    ''' <summary>
    ''' returns like operator or = operator for building the sql query for phone number
    ''' </summary>
    ''' <param name="phoneNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function equalOrLikeForPhone(ByVal phoneNum As String) As String
        Dim equalOrLike As String = "LIKE"
        If Regex.Replace(phoneNum, "\D", String.Empty).Count = 10 Then
            equalOrLike = "="
        End If
        Return equalOrLike
    End Function

End Class
