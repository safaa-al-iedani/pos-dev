﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to represent any data that is returned as part of a login attempt.
''' </summary>
Public Class LoginResponseDtc
    Inherits BaseResponseDtc

    Property emp As New EmployeeDtc

End Class
