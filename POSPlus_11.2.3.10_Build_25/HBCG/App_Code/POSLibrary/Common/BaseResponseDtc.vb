﻿Imports Microsoft.VisualBasic

''' <summary>
''' Base class for all 'response' objects used.  It ontains the basic details
''' of a response returned to a client, after a server request is made.
''' NOTE: this class should be extended to fit specific scenarios
''' </summary>
Public Class BaseResponseDtc

    'represents the error/warning/info message (if any) that was generated
    'after applying corresponding 'business' or 'application' rules, 
    'e.g. if user name was invalid ,etc. 
    'It does not represent any inherent 'system' type of errors (like DB errors)
    Property message As String = String.Empty

End Class
