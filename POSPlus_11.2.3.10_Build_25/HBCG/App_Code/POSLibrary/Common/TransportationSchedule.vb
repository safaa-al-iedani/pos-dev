﻿Imports Microsoft.VisualBasic
''' <summary>
''' Class that encapsulates the Zone,Pickup/Delivery,and Pickup/Delivery Date details needed when Schedule deliveries by (P)oints or (S)tops 
''' </summary>
Public Class TransportationSchedule
    Property zoneCd As String
    Property puDelCd As String
    Property puDelDt As Date

    Public Sub TransportationSchedule()
        zoneCd = ""
        puDelCd = ""
        puDelDt = FormatDateTime(SysPms.begOfTime, DateFormat.ShortDate)
    End Sub
End Class
