﻿Imports Microsoft.VisualBasic

''' <summary>
''' This class represents the component. 
''' </summary>
Public Class PackageComponent
    Public Property SKU As String
    Public Property Price As Double

    Public OrderOfOccurance As Integer

    Public Property Quantity As Double

    'holds the item type code, INV etc..
    Public Property ItemType As String


    ''' <summary>
    ''' Constructor
    ''' </summary>
    Sub New()
        Price = 0
        Quantity = 1
        OrderOfOccurance = 0
    End Sub


End Class