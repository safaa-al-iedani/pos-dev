﻿Imports Microsoft.VisualBasic

''' <summary>
''' Holds all the details to represent a Store
''' 
''' *******************************************************************************************
'''   ----------- MEF 02/21/2014 ---------------
'''  NOTE: this class had to be named StoreData to avoid the conflicts due to the StoreDtc
'''        located within the CashDrawerBalancing libraries
''' >>> AT SOME POINT COMBINE THIS WITH THAT ONE. TOO MANY CHANGES TO DO IT RIGHT NOW. 
''' *******************************************************************************************
''' 
''' </summary>
Public Class StoreData

    Property cd As String           ' storeCd
    Property coCd As String         ' Not Null in E1 STORE table so use to indicate have data or not
    Property name As String
    Property addr As AddressDtc
    'Property eAddr As String       ' doesn't exist till 2012 something
    Property lastBalDt As Date
    Property shipToStoreCd As String
    Property puStoreCd As String
    Property defaultLoc As String
    Property zoneCd As String
    Property enableARS As Boolean   ' indicates if Advanced Reservation is enabled for this Store.

End Class
