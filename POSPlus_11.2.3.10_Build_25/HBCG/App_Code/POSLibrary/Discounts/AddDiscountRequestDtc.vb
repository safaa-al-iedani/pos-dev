﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details needed to apply discounts to an order
''' </summary>
Public Class AddDiscountRequestDtc

    Property isSOE As Boolean = True

    ' **** REQUIRED ATTRIBUTES REGARDLESS OF SOE / SOM PROCESSING ****
    Property pkpDel As String
    Property discount As DiscountDtc        'discount to be applied

    'An array, where each value represents a LnSeqNum for the line where discount are to be applied
    'NOTE: NOT needed when the discount is being applied at the Header level
    Property lnSeqNums As ArrayList

    ' **** REQUIRED for ORDER ENTRY processing ****
    Property sessionId As String            'to retrieve ALL lines.   Needed for HEADER discount

    ' **** REQUIRED for ORDER MANAGEMENT processing ****
    Property orderLines As DataTable = Nothing   'ALL lines in the current order ( only applicable when in SOM+ ) 
    Property discApplied As DataTable = Nothing  'ALL discounts applied so far in the current order ( only applicable when in SOM+ ) 

    Protected Sub New()
        ' has to be added otherwise, the default parameterless constructor is overriden
    End Sub

    ''' <summary>
    ''' Creates a request for an SOE scenario, when applying a HEADER discount
    ''' </summary>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="disc">the discount object that will be applied</param>
    ''' <param name="sessId">the session to retrieve the oder lines for the order</param>
    ''' <remarks>*** USE THIS WHEN APPLYING A HEADER DISCOUNT in ORDER ENTRY *** </remarks>
    Public Sub New(ByVal pdFlag As String, ByVal disc As DiscountDtc, ByVal sessId As String)
        pkpDel = pdFlag
        discount = disc
        sessionId = sessId
    End Sub

    ''' <summary>
    ''' Creates a request for an SOE scenario, when applying a LINES discount
    ''' </summary>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="disc">the discount object that will be applied</param>
    ''' <param name="sessId">the session to retrieve the oder lines for the order</param>
    ''' <param name="lnNums">a list of LnSeqNums of the lines that the discount needs to be applied to<param>
    ''' <remarks>*** USE THIS WHEN APPLYING A LINES DISCOUNT in ORDER ENTRY *** </remarks>
    Public Sub New(ByVal pdFlag As String, ByVal disc As DiscountDtc, ByVal sessId As String, ByVal lnNums As ArrayList)
        pkpDel = pdFlag
        discount = disc
        sessionId = sessId
        lnSeqNums = lnNums
    End Sub

    ''' <summary>
    ''' Creates a request for an SOM scenario, when applying a HEADER discount
    ''' </summary>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="disc">the discount object that will be applied</param>
    ''' <param name="orderLns">contains all lines in the current order</param>
    ''' <param name="orderDiscs">contains all the discounts applied so far to the order</param>
    ''' <remarks>*** USE THIS WHEN APPLYING A HEADER DISCOUNT in ORDER MANAGEMENT *** </remarks>
    Public Sub New(ByVal pdFlag As String,
                   ByVal disc As DiscountDtc,
                   ByVal orderLns As DataTable,
                   ByVal orderDiscs As DataTable)
        isSOE = False
        pkpDel = pdFlag
        discount = disc
        orderLines = orderLns
        discApplied = orderDiscs
    End Sub

    ''' <summary>
    ''' Creates a request for an SOM scenario, when applying a LINES discount
    ''' </summary>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="disc">the discount object that will be applied</param>
    ''' <param name="orderLns">contains all lines in the current order</param>
    ''' <param name="orderDiscs">contains all the discounts applied so far to the order</param>
    ''' <param name="lnNums">a list of LnSeqNums of the lines that the discount needs to be applied to<param>
    ''' <remarks>*** USE THIS WHEN APPLYING A LINE DISCOUNT in ORDER MANAGEMENT *** </remarks>
    Public Sub New(ByVal pdFlag As String,
                   ByVal disc As DiscountDtc,
                   ByVal orderLns As DataTable,
                   ByVal orderDiscs As DataTable,
                   ByVal lnNums As ArrayList)
        isSOE = False
        pkpDel = pdFlag
        discount = disc
        orderLines = orderLns
        discApplied = orderDiscs
        lnSeqNums = lnNums
    End Sub


End Class
