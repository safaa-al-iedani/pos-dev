﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details resulting from updating an order line with the updated discount info
''' </summary>
Public Class LineDiscountUpdateResponseDtc
    Inherits BaseResponseDtc

    'indicates if the discount was successfully applied or not, by giving an ERROR/WARNIING/SUCCESS value
    Property status As DiscountUtils.DiscountStatus

    Property isUpdated As Boolean   ' true if record was updated or false if not; false if SHU'd and discount amt not zero

    ' **** Only applicable for ORDER MANAGEMENT processing ****
    Property orderLine As DataRow = Nothing   'Current line being processed

End Class
