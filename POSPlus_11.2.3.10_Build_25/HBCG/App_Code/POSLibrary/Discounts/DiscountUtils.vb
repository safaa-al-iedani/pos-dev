﻿Imports Microsoft.VisualBasic
Imports System.Linq

Public Class DiscountUtils

    ''' <summary>
    ''' Used to indicate the status of a discount after calling the E1 APIs
    ''' to have the discount be applied
    ''' </summary>
    Public Enum DiscountStatus
        Success
        Warning
        HasError
    End Enum

    ''' <summary>
    ''' Finds out if the datarow passed in, which contains order line details, qualifies to have the
    ''' discount passed in, applied to it or not, based on the discount properties.
    ''' IMPORTANT: the datarow passed in is expected to have columns: TYPECODE and MNR_CD, as they are
    ''' populated upon retrieval of an order in SOM+
    ''' </summary>
    ''' <param name="drLine">the datarow that contains the line details to be checked</param>
    ''' <param name="discount">the discount that needs to be applied</param>
    ''' <param name="minorsList">a list of the minors corresponding to the Discount passed in and the associated conditional or discounted flag</param>
    ''' <returns>TRUE, if the line can be discounted; FALSE otherwise</returns>
    ''' <remarks>Used during SOM processing to gather the discountable lines, as well as during add discounts</remarks>
    Public Shared Function IsDiscountableLine(ByVal drLine As DataRow, ByVal discount As DiscountDtc,
                                              ByVal minors As DataTable, ByVal isSOM As Boolean) As Boolean

        Dim isDiscountable As Boolean = True
        Dim skuTpCd As String = drLine.Item("TYPECODE")
        Dim mnrCd As String = If(IsNothing(drLine.Item("MNR_CD")), String.Empty, drLine.Item("MNR_CD"))
        Dim shuD As Boolean = True
        Dim voidFlag As String = String.Empty
        If isSOM Then
            shuD = (If(IsDBNull(drLine.Item("SHU")), False, drLine.Item("SHU")) = "Y")
            voidFlag = drLine("VOID_FLAG")
        Else
            shuD = False
            voidFlag = "F"
        End If

        Dim mnrs As DataRow()
        'Dim isCondMnr As Boolean = False
        'Dim isDiscMnr As Boolean = False
        If SystemUtils.dataTblIsNotEmpty(minors) Then

            mnrs = minors.Select("mnr_cd = '" & mnrCd & "'", "")   ' sort 'C'onditional minors first, before 'D'iscounted to determine if conditional for SHU comparison
            'mnrs = minors.Select("mnr_cd = '" & mnrCd & "'", "condOrDisc")   ' sort 'C'onditional minors first, before 'D'iscounted to determine if conditional for SHU comparison
            '    If mnrs.Length > 0 Then
            '        isCondMnr = mnrs(0)("condOrDisc") = AppConstants.Discount.MNR_TP_COND
            '        isDiscMnr = If(isCondMnr, mnrs.Length > 1, True)
            '    End If
        End If

        'When line is SHU'd, it is not modifiable so cannot be discounted at this point but conditional minor SKUs have to be included so that even if SHU'd, they will be 
        '   included for checking for non-SHU'd discounted minor items; have to pass conditional and SHU'd so can work either way - if SHU'd line returns discount, then
        '   the line will not be updated/discounted and a message will be provided to indicate why
        'When Discount is NOT to be applied to packages, excludes the Package and its components, as well as
        'When Discount is to be applied to certain minors only, excludes those lines whose minor doesn't match
        If "Y".Equals(voidFlag) OrElse (shuD AndAlso (Not discount.isByMinor)) OrElse
            (((Not discount.applyToPkg AndAlso ("PKG".Equals(skuTpCd) OrElse "CMP".Equals(skuTpCd))) OrElse
            (discount.isByMinor AndAlso minors.Rows.Count > 0 AndAlso mnrs.Length < 1))) Then

            isDiscountable = False
        End If
        Return isDiscountable
    End Function

    ''' <summary>
    ''' Returns the datatable containing the columns needed for the discounts for an order.
    ''' There is no data being added- only the columns specific to discounts table are created.
    ''' </summary>
    Public Shared Function GetLineDiscountTable() As DataTable

        Dim table As DataTable = New DataTable

        ' Create the structure for the discounts table.
        Dim rowIdx As DataColumn = table.Columns.Add("ROW_IDX", GetType(Int32))
        rowIdx.AutoIncrement = True
        rowIdx.AutoIncrementSeed = 1
        rowIdx.ReadOnly = True

        table.Columns.Add("DISC_CD")
        table.Columns.Add("DISC_DES")
        table.Columns.Add("DISC_LEVEL")
        table.Columns.Add("DISC_AMT", Type.GetType("System.Double"))
        table.Columns.Add("DISC_SEQ_NUM", Type.GetType("System.Int32"))
        table.Columns.Add("DISC_PCT", Type.GetType("System.Double"))
        table.Columns.Add("DISC_IS_DELETED", Type.GetType("System.Boolean"))
        table.Columns.Add("DISC_IS_EXISTING", Type.GetType("System.Boolean"))
        table.Columns.Add("ITM_CD")
        table.Columns.Add("ITM_DES")
        table.Columns.Add("ITM_RETAIL", Type.GetType("System.Double"))
        table.Columns.Add("QTY")
        table.Columns.Add("LNSEQ_NUM", Type.GetType("System.Int32"))
        table.Columns.Add("TYPE")
        table.Columns.Add("AMT", Type.GetType("System.Double"))
        table.Columns.Add("SEQ_NUM", Type.GetType("System.Int32"))
        'MM-9870
        'Adding Approved column to the discount table
        table.Columns.Add("Approved")
        Return table
    End Function

    ''' <summary>
    ''' Creates and adds a row to the table passed-in based on the specified values.
    ''' This is specific to the table being used for showing the order discounts.
    ''' </summary>
    ''' <param name="table"></param>
    ''' <param name="discCd"></param>
    ''' <param name="discDes"></param>
    ''' <param name="discLevel">as applied to the order, so either 'H' or 'L'</param>
    ''' <param name="discAmt"></param>
    ''' <param name="discSeqNum"></param>
    ''' <param name="discPct"></param>
    ''' <param name="sku"></param>
    ''' <param name="skuDes"></param>
    ''' <param name="retail"></param>
    ''' <param name="qty">the line quantity where the discount was applied to</param>
    ''' <param name="lnSeqNum"></param>
    ''' <param name="isDiscDeleted"></param>
    ''' <param name="isDiscExisting"></param>   
    ''' <remarks></remarks>
    Public Shared Sub AddDiscountRow(ByRef table As DataTable,
                                    ByVal discCd As String,
                                    ByVal discDes As String,
                                    ByVal discLevel As String,
                                    ByVal discAmt As Double,
                                    ByVal discSeqNum As Integer,
                                    ByVal discPct As Double,
                                    ByVal sku As String,
                                    ByVal skuDes As String,
                                    ByVal qty As Integer,
                                    ByVal retail As Double,
                                    ByVal lnSeqNum As Integer,
                                    ByVal seqNum As Integer,
                                    Optional ByVal type As String = "",
                                    Optional ByVal discAmt1 As Double = 0.0,
                                    Optional ByVal isDiscDeleted As Boolean = False,
                                    Optional ByVal isDiscExisting As Boolean = False)


        Dim row As DataRow
        row = table.NewRow()
        row("ROW_IDX") = GetRowIndex(table)
        row("DISC_CD") = discCd
        row("DISC_DES") = discDes
        row("DISC_LEVEL") = discLevel
        row("DISC_AMT") = discAmt
        row("DISC_SEQ_NUM") = discSeqNum
        row("DISC_PCT") = discPct
        row("DISC_IS_DELETED") = isDiscDeleted
        row("DISC_IS_EXISTING") = isDiscExisting
        row("ITM_CD") = sku
        row("ITM_DES") = skuDes
        row("ITM_RETAIL") = retail
        row("QTY") = qty
        row("LNSEQ_NUM") = lnSeqNum
        row("TYPE") = type
        row("AMT") = discAmt1
        row("SEQ_NUM") = seqNum
        'By default adding A flag to the Approved Column
        row("Approved") = "A"
        table.Rows.Add(row)
    End Sub

    'MM-9484
    ''' <summary>
    ''' GetRowIndex to get RowIndex
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetRowIndex(ByVal dt As DataTable)
        Dim seqNum As Integer
        If (dt.Rows.Count() = 0) Then
            seqNum = 1
        Else
            seqNum = (From u In dt.AsEnumerable() _
                                Select u.Field(Of Integer)("ROW_IDX")).Max()
            seqNum = seqNum + 1
        End If
        Return seqNum
    End Function
    ''' <summary>
    ''' Creates and returns a new instance of a discount object based on the passed-in values
    ''' </summary>
    ''' <param name="cd">the code</param>
    ''' <param name="des">the discount description</param>
    ''' <param name="amt">discount amt</param>
    ''' <param name="type">the type of discount - pct or amt</param>
    ''' <param name="level">the level - header or line</param>
    ''' <param name="isByMinor">indicates if it is to be applied based on a minor code</param>
    ''' <param name="isCondDisc">is a conditional discount</param>
    ''' <param name="applyToPkg">indicates if this discount can be applied to a package or not.</param>
    ''' <param name="expDt">expiration date of the discount, inclusive</param>
    ''' <returns>a new and populated discount object</returns>
    Public Shared Function GetDiscountDtc(ByVal cd As String,
                                          ByVal des As String,
                                          ByVal amt As Double,
                                          ByVal type As String,
                                          ByVal level As String,
                                          ByVal isByMinor As Boolean,
                                          ByVal isCondDisc As Boolean,
                                          ByVal applyToPkg As Boolean,
                                          ByVal expDt As String) As DiscountDtc
        Dim discount As New DiscountDtc()
        discount.cd = cd
        discount.desc = des
        discount.amt = amt
        discount.tpCd = type
        discount.level = level
        discount.isByMinor = isByMinor
        discount.isCondDisc = isCondDisc
        discount.applyToPkg = applyToPkg
        discount.expDt = expDt
        Return discount
    End Function

    ''' <summary>
    ''' Creates and returns a new instance of a discount object based on the passed-in values and handles the data
    '''     checking and conversion
    ''' </summary>
    ''' <param name="discRow">datarow from the discount table, specifically from DiscountDac, either GetDiscount or GetDiscounts 
    '''    as assumes des is not blank</param>
    ''' <returns>a new and populated discount object</returns>
    Public Shared Function GetDiscountDtc(ByVal discRow As DataRow) As DiscountDtc

        Dim discount As New DiscountDtc()
        discount.cd = discRow("cd")
        discount.desc = If(IsDBNull(discRow("des")), String.Empty, discRow("des"))
        discount.amt = If(IsDBNull(discRow("AMT")), 0, CDbl(discRow("AMT")))
        discount.tpCd = If(IsDBNull(discRow("TP_CD")), "", (discRow("TP_CD")))
        discount.level = If(IsDBNull(discRow("DISC_LEVEL")), "", (discRow("DISC_LEVEL")))
        discount.isByMinor = If(IsDBNull(discRow("BY_MNR")), False, ("Y" = (discRow("BY_MNR"))))
        discount.isCondDisc = If(IsDBNull(discRow("COND_DISC")), False, ("Y" = (discRow("COND_DISC"))))
        discount.applyToPkg = If(IsDBNull(discRow("APPLY_PACKAGE")), False, ("Y" = (discRow("APPLY_PACKAGE"))))
        discount.expDt = If(IsDBNull(discRow("exp_dt")), String.Empty, discRow("exp_dt"))

        Return discount
    End Function
End Class
