﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details resulting from a call to ADD discounts to an order
''' </summary>
Public Class AddDiscountResponseDtc
    Inherits BaseResponseDtc

    'indicates if the discount was successfully applied or not, by giving an ERROR/WARNIING/SUCCESS value
    Property status As DiscountUtils.DiscountStatus

    'represents the number of lines that were discounted during the 'Add Discount' request
    Property numLnsDiscounted As Integer = 0

    'contains ALL the discounts applied to the current order
    Property discountsApplied As New DataTable

    ' **** Only applicable for ORDER MANAGEMENT processing ****
    Property orderLines As DataTable = Nothing   'ALL lines in the current order ( only applicable when in SOM+ ) 

End Class
