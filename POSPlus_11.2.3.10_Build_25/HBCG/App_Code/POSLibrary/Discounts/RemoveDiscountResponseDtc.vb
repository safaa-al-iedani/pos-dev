﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details resulting from a call to REMOVE discounts from an order
''' </summary>
Public Class RemoveDiscountResponseDtc
    Inherits BaseResponseDtc

    'indicates if the discount was successfully applied or not, by giving an ERROR/WARNIING/SUCCESS value
    Property status As DiscountUtils.DiscountStatus

    'contains ALL the discounts applied to the current order
    Property discountsApplied As New DataTable

    ' **** Only applicable for ORDER MANAGEMENT processing ****
    Property orderLines As DataTable = Nothing   'ALL lines in the current order ( only applicable when in SOM+ ) 

End Class
