﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details needed to recalculate ALL discounts applied to an order
''' </summary>
Public Class ReapplyDiscountRequestDtc

    ' **** REQUIRED ATTRIBUTES REGARDLESS OF SOE / SOM PROCESSING ****
    Property lnAction As LnChng
    Property pkpDel As String

    Property lnSeqNum As String     'the line that was changed

    ' **** REQUIRED for ORDER ENTRY processing ****
    Property sessionId As String            'to retrieve ALL lines.   Needed for HEADER discount

    ' **** REQUIRED for ORDER MANAGEMENT processing ****
    Property orderLines As DataTable = Nothing   'ALL lines in the current order ( only applicable when in SOM+ ) 
    Property discApplied As DataTable = Nothing  'ALL discounts applied so far in the current order ( only applicable when in SOM+ ) 
    Property soWrDt As Date                      ' Sales Order written (create) date (SO.SO_WR_DT)

    Protected Sub New()
        ' has to be added otherwise, the default parameterless constructor is overriden
    End Sub

    ''' <summary>
    ''' Creates a request for an SOE scenario
    ''' </summary>
    ''' <param name="action">one of the values in the LnChng enumeration</param>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="sessId">the session to retrieve the oder lines for the order</param>
    ''' <param name="lnSeqNum">the line number that got changed</param>
    ''' <remarks>*** USE THIS WHEN A LINE IS CHANGED in ORDER ENTRY *** </remarks>
    Public Sub New(ByVal action As LnChng, ByVal pdFlag As String, ByVal sessId As String, ByVal lnSeq As String)
        lnAction = action
        pkpDel = pdFlag
        lnSeqNum = lnSeq
        sessionId = sessId
    End Sub

    ''' <summary>
    ''' Creates a request for an SOM scenario 
    ''' </summary>
    ''' <param name="action">one of the values in the LnChng enumeration</param>
    ''' <param name="pdFlag">indicates if the order is pickup or delivery</param>
    ''' <param name="orderLns">contains all lines in the current order</param>
    ''' <param name="orderDiscs">contains all the discounts applied so far to the order</param>
    ''' <param name="lnSeq">the line number that got changed</param>
    ''' <param name="soWrDate">sales order written (create) date from SO.SO_WR_DT</param>
    ''' <remarks>*** USE THIS WHEN A LINE IS CHANGED in ORDER MANAGEMENT *** </remarks>
    Public Sub New(ByVal action As LnChng,
                   ByVal pdFlag As String,
                   ByVal orderLns As DataTable,
                   ByVal orderDiscs As DataTable,
                   ByVal lnSeq As String,
                   ByVal soWrDate As Date)
        lnAction = action
        pkpDel = pdFlag
        orderLines = orderLns
        discApplied = orderDiscs
        lnSeqNum = lnSeq
        soWrDt = soWrDate
    End Sub

    ''' <summary>
    ''' Used to indicate the action that triggered the application to check if discounts should
    ''' be recalculated or not
    ''' </summary>
    Public Enum LnChng
        Prc
        Qty
        Void
        Add
    End Enum

End Class
