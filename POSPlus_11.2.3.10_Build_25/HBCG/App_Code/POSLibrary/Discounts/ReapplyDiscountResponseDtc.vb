﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a details resulting from a call to REAPPLY discount when something changes in an order
''' </summary>
Public Class ReapplyDiscountResponseDtc
    Inherits BaseResponseDtc

    Property rtnVal As Boolean = False

    ' **** Only applicable for ORDER MANAGEMENT processing ****
    'contains ALL the discounts applied to the current order
    Property discountsApplied As New DataTable

    'contains ALL the lines in the current order
    Property orderLines As DataTable = Nothing   'ALL lines in the current order ( only applicable when in SOM+ ) 

End Class
