﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class that encapsulates the details needed when removing a discount
''' </summary>
Public Class RemoveDiscountDtc

    Property lnSeqNum As String         'represents the line from which discount is being removed
    Property discCd As String           'the discount code being removed
    Property discAmt As String          'the actual discount amount
    Property seqNum As Integer        'the RowId of the MULTI_DISC table

    Protected Sub New()
        ' has to be added otherwise, the default parameterless constructor is overriden
    End Sub

    ''' <summary>
    '''    Creates a request for removing a discount.
    ''' </summary>
    ''' <param name="discountCd">the discount code to be removed</param>
    ''' <param name="discountAmt">the discount amount being removed, used to update the order line amounts</param>
    ''' <param name="lineSeqNum">line seq number of the line whose discount has to be removed</param>
    Public Sub New(ByVal lineSeqNum As String, ByVal discountCd As String, ByVal discountAmt As String, ByVal descSeqNum As Integer)
        lnSeqNum = lineSeqNum
        discCd = discountCd
        discAmt = discountAmt
        seqNum = descSeqNum
    End Sub

End Class
