﻿Imports Microsoft.VisualBasic

''' <summary>
''' Encapsulates a discount object
''' </summary>
Public Class DiscountDtc

    Property cd As String
    Property desc As String
    Property tpCd As String
    Property amt As Double
    Property level As String
    Property isCondDisc As Boolean
    Property isByMinor As Boolean
    Property applyToPkg As Boolean
    Property expDt As String

End Class
