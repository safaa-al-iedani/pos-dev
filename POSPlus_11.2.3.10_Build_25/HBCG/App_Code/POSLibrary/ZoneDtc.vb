﻿Imports Microsoft.VisualBasic

''' <summary>
''' holds the details to represent a zone which is an area designated for delivery/pickup scheduling and/or inventory availability
''' </summary>
Public Class ZoneDtc

    Property zoneCd As String   ' code representing the zone
    Property des As String      ' description of the zone
    Property delStoreCd As String    ' delivery store code
    Property defltDelChg As Double   ' default delivery charge
    Property defltSetupChg As Double ' default setup charge
    Property leadDays As Integer     ' lead days for inventory availability for ARS
End Class
