﻿Imports Microsoft.VisualBasic

''' <summary>
''' Contains the details required to perform a reservation.
''' Object used for SOFT (ITM_RES) and HARD (inventory tables) reservations
''' </summary>
Public Class ResRequestDtc

    Property isAutoSoftRes As Boolean = False

    Property qty As Double                      'the number of pieces to be reserved
    Property itemCd As String                   'the code for the item being reserved
    Property puDel As String                    'indicates if reserving for a 'P'ickup or 'D'elivery
    Property puDelStoreCd As String             'store code for the pickup/delivery this item is scheduled to
    Property zoneCd As String                   'zone code for zone priority fill auto_res

    Property storeCd As String                  'store code where reservation is being performed
    Property locCd As String                    'the location where to reserve to
    Property locTpCd As String = String.Empty   'W/S/O for warehouse, showroom, outlet respectively
    Property serNum As String                   'the serial number of the item
    Property outId As String                    'outlet id 
    Property outCd As String                    'outlet code

    Property delDocNum As String                'Document number where item being reserved belongs to
    Property lineNum As Integer                 'Line number for the item being reserved
    Property fifoCost As Double                 'the FIFO cost

End Class
