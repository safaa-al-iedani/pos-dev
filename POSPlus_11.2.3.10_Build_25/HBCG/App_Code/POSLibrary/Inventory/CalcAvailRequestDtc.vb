﻿Imports Microsoft.VisualBasic

''' <summary>
''' Contains the details required to Calculate Availability.
''' </summary>
Public Class CalcAvailRequestDtc

    Property itemCd As String                   'the code for the item to get availability for
    Property qty As Double                      'the number of pieces needed
    Property reqDate As Date                    'the date needed

    Property puDel As String                    'indicates if item is for a 'P'ickup or 'D'elivery
    Property puDelStoreCd As String             'store code for the pickup/delivery this item is scheduled to
    Property zoneCd As String                   'zone code for zone priority fill logic

    Property storeCd As String                  'the written store code 
    Property delDocNum As String                'the document where item belong to
    Property delDocLnNum As Integer             'the line number where item is placed
    Property combId As String = ""              'frame combination number.  This value is optional, so defaults value

End Class
