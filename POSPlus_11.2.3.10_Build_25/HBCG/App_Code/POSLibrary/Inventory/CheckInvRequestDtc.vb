﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to encapsulate all the data required to check inventory.
''' </summary>
''' <remarks>Called and used right now for a store that is ARS enabled.</remarks>
Public Class CheckInvRequestDtc

    Property isPaidInFull As Boolean            'indicates if the order is paid in full or not

    Property puDel As String                    'indicates if item is for a 'P'ickup or 'D'elivery
    Property puDelStoreCd As String             'store code for the pickup/delivery this item is scheduled to
    Property puDelDt As Date                    'the date the pickup/delivery is scheduled to
    Property zoneCd As String                   'the zone code of the pickup/delivery for zone priority fill

    Property itemCd As String                   'the code for the item being checked
    Property storeCd As String                  'the store from where the line is being reserved
    Property locCd As String                    'the location (like stock, warehouse, et.c) from where the line is being reserved
    Property qty As Double                      'the line qty

End Class
