﻿Imports Microsoft.VisualBasic

''' <summary>
''' Contains the details obtained after a reservation is performed
''' At this point this object is used for SOFT reservations only (ITM_RES)
''' </summary>
Public Class ResResponseDtc
    Inherits BaseResponseDtc

    'this ID corresponds to ALL reservations in the dataset
    Property resId As String = String.Empty

    'Reservations in the DataTable include one record per Store-Location-Qty combination
    'the columns included in this dataset are: 
    '  STORE_CD,  LOC_CD,  LOC_TP_CD,  QTY,  OOC_QTY,  CRPT_ID_NUM
    Property reservations As DataTable
    'Is reservation is for ARS enabled Store
    Property isForARSStore As Boolean
    'Is the lines got Split
    Property isLineSplitted As Boolean



End Class
