﻿Imports Microsoft.VisualBasic

''' <summary>
''' Contains the details required to request priority fill information.
''' </summary>
Public Class PriFillReq

    Property itmCd As String                    'the code for the item to get availability for
    Property locTpCd As String                  ' code for type of inventory location requesting, warehouse(W), showroom(S), etc.

    Property puDel As String                    'indicates if item is for a 'P'ickup or 'D'elivery
    Property puDelStoreCd As String             'store code for the pickup/delivery this item is scheduled to
    Property zoneCd As String                   'zone code for zone priority fill logic
    Property zoneZipCd As String                'zone zip code for zone priority fill logic
    Property homeStoreCd As String              'the home store code for employee
    Property takeWith As String                 'take with Y or N
End Class
