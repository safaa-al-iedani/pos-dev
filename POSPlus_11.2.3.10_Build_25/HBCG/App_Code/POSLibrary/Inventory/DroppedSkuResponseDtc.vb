﻿Imports Microsoft.VisualBasic

Public Class DroppedSkuResponseDtc
    Inherits BaseResponseDtc

    Property totalCount As Integer = 0      'number of items with DROP_DT <= SYSDATE
    Property prevSaleCount As Integer = 0   'number of items with PREVENT_SALE = Y
    Property prevPOCount As Integer = 0     'number of items with PREVENT_PO = Y

    Property prevSaleSkus As String = String.Empty  'comma delimited list of SKU with PREVENT_SALE = Y
    Property prevPOSkus As String = String.Empty    'comma delimited list of SKU with PREVENT_PO = Y

End Class
