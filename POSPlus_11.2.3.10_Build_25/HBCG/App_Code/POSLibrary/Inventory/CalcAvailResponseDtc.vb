﻿Imports Microsoft.VisualBasic

''' <summary>
''' Contains the details obtained after a request to Calculate Availabilty is performed
''' </summary>
Public Class CalcAvailResponseDtc

    Property commentOut As String

    'This datatable includes the following columns: 
    '  ITM_CD,  FILL_TYPE,  FILL_QTY,  AVAIL_DT,  POS_IST_DOC,  ADDITIONAL_QTY
    Property calcAvailData As DataTable

End Class
