﻿Imports Microsoft.VisualBasic

Public Class ResUnresException
    Inherits System.ApplicationException

    'used to identify specifically errors that occur during
    'the reserve or unreserve of inventory

     Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

End Class
