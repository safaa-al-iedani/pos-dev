﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to encapsulate the data returned as a result of Check inventory request.
''' </summary>
''' <remarks>Called and used right now for a store that is ARS enabled.</remarks>
Public Class CheckInvResponseDtc

    Property invStatusCode As Integer       'represents the inventory status message code
    Property invStatusText As String        'represents the inventory status message
    Property availDt As Date                'represents the incoming inv available date
    Property poCd As String                 'represents the incoming PO code
    Property poLnNum As Double              'represents the incoming PO line#
    Property poQty As Double                'represents the incoming PO available quantity

End Class
