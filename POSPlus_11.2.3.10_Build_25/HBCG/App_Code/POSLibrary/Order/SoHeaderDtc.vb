﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to represent any data that is related to an order.  This is present
''' from the time of creating an order to saving/voiding/escaping it.
''' </summary>
Public Class SoHeaderDtc

    Property delDocNum As String = ""

    'Used for taxing, may be same or different from tran dt depending on calling process
    Property writtenDt As String = ""

    'Used if pricing by store or store group; used for default inventory store if take-with
    Property writtenStoreCd As String

    'Property soSeqNum As String  ' TODO - maybe these (seq_num/doc_num) sb in wrapper for existingSoHdrInfo ????
    Property isCashNCarry As Boolean = False

    Property orderTpCd As String = ""
    Property discCd As String = ""
    Property puDel As String = ""   'whether this is a pickup/delivery
    Property puDelStore As String = ""
    Property puDelDt As String = ""
    Property zoneCd As String = ""

    'used when an orig order is referenced by a CRM/MCR/MDB
    Property origDelDocNum As String = ""
    Property origSoWrDt As String = ""

    'Property soSeqNum As String  ' TODO - maybe these (seq_num/doc_num) sb in wrapper for existingSoHdrInfo ????

    'customer related info
    Property custCd As String = ""
    Property custTpPrcCd As String = ""

    'salesperson 1,2 info 
    Property slsp1 As String = ""
    Property slsp2 As String = ""
    Property slspPct1 As Double = 100
    Property slspPct2 As Double = 0

    Property taxCd As String = ""
    Property taxMethod As String = ""
    Property taxExemptCd As String = ""
    Property taxRates As New ItemTypeTaxAmtsDtc()

    'used to indicate if a store is setup to use Adavanced Reservations
    Property isARSEnabled As Boolean = False

End Class
