﻿Imports Microsoft.VisualBasic

''' <summary>
''' Holds the values resulting from applying the Confirmation Status rules
''' upon save of an order in Order Management
''' </summary>
Public Class ConfStatusResponseDtc
    Inherits BaseResponseDtc

    Property allowSaveOrder As Boolean    'TRUE if order can be saved, FALSE to stop from saving
    Property confCode As String           'the confirmation code that needs to be set before save

    Property confirmationErrorMessage As StringBuilder 'this will hold the error message for confirmation status code.

    Property IsConfirmationValidationPassed As Boolean

    Public Sub New()
        confirmationErrorMessage = New StringBuilder()
    End Sub
End Class
