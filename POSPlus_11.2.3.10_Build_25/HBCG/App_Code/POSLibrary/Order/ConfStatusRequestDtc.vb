﻿Imports Microsoft.VisualBasic
Imports System.Data.DataTable

''' <summary>
''' Holds the data needed to apply the rules for the Confirmation Status Code value
''' upon saving of an order in SOM+
''' </summary>
Public Class ConfStatusRequestDtc

    Property orderType As String                'SAL, CRM
    Property isPaidInFull As Boolean = False    'TRUE if order should be considered paid in full
    Property pkpDelDate As Date                 'the pickup/delivery date
    Property pkpDelZone As String               'the pickup/delivery zone

    Property origConfCode As String             'the original confirmation code
    Property currConfCode As String             'the new confirmation code
    Property confirmationCodes As DataTable     'the list of Active confirmation codes
    Property lnsData As DataSet                 'the order lines to check if all reserved
    Property hasConfirmPIFSecurity As Boolean = False    ' User has the Confirm PIF override security

End Class
