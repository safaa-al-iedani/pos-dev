﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

''' <summary>
''' Class to encapsulate the data results after an ARS store validation is performed.
''' </summary>
Public Class ArsValidationResponseDtc
    Inherits BaseResponseDtc

    Property isValid As Boolean = False             'TRUE if no errors found, FALSE otherwise

    'the following will contain IDs, only when during the ARS validation the E1 API indicates a line
    'should be reserved, the caller of the ARS validation initiates the reservation calls
    Property lnsToReserve As New List(Of String)        'Line IDs of those lines that need to be reserved

    'next is specific for SOM+, represents the dataset of order lines
    Property lnsDataset As DataSet

End Class
