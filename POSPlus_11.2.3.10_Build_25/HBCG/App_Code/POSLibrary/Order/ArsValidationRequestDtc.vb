﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

''' <summary>
''' Class to encapsulate all the data required to perform validations for an ARS store.
''' </summary>
Public Class ArsValidationRequestDtc

    Property checkInvReq As New CheckInvRequestDtc      'required to perform the E1 API call for inventory validation
    Property maxAvailDate As Date                       'the maximum date based on the SO Lines in the document
    Property droppedItemsFlaggedPreventPO As List(Of String)  'those items that are dropped and PREVENT_PO = 'Y'

End Class
