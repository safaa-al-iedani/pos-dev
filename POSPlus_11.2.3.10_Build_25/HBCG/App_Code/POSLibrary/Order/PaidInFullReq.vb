﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to encapsulate the information for a request to the paid in full api.
''' </summary>
Public Class PaidInFullReq

    ' Currently this is all the data needed by existing custom_paid and bt_sales_util packages; 
    '    no doubt more will be needed as the custom_paid module logic is completed for changing SOM or new orders

    Property delDocNum As String = ""
    Property custCd As String = ""
    Property writtenStoreCd As String = ""
    Property ordTpCd As String = ""

    Property origFinAmt As Double = 0.0     ' so.orig_fi_amt
    Property finCoCd As String = ""          ' so.fin_cust_cd = asp.as_cd, not cust.cust_cd
    Property finApp As String = ""          ' so.approval_cd
    'These properties are used to determine paid in full in SOE
    Property grandTotal As Double = 0.0
    Property paidAmount As Double = 0.0
    Property requestFromSOE As Boolean = False
End Class
