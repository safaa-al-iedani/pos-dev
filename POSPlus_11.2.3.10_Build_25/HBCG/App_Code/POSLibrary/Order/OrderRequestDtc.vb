﻿Imports Microsoft.VisualBasic
' this is the set of query criteria AND the employee code if testing for store based security 
Public Class OrderRequestDtc
    '  Selection requirements beyond the basic returned set
    Property shipToName As Boolean = False      ' include ship-to l_name, f_name
    Property shipToHPhone As Boolean = False    ' include ship-to home phone
    Property shipToZone As Boolean = False      ' include ship-to zone code and ship_to_zip_cd
    Property slspCds As Boolean = False '		' include the salesperons codes
    Property slspPcts As Boolean = False '		' include the salesperons percentages
    Property puDelInfo As Boolean = False       ' include pu_del and pu_del_dt
    Property billToCustName As Boolean = False  ' include bill-to l_name, f_name
    Property delSetupChgs As Boolean = False    ' include setup and delivery charges
    Property taxChgs As Boolean = False         ' include tax charge
    Property finInfo As Boolean = False         ' include SO finance details: fin_cust_cd (as_cd), orig_fi_amt, approval_cd

    ' Operator employee code for store based security 
    Property empCdOp As String = ""

    '  Order Query criteria, the non-null values will be used to setup the WHERE clause
    '     if the entry includes a '%', then the clause will be 'LIKE'; if not, then '='
    Property docNum As String = ""
    Property wrDt As String = ""
    Property soStore As String = ""
    Property ordTp As String = ""
    Property statCd As String = ""
    Property puDel As String = ""
    Property puDelDt As String = ""
    Property slspInit1 As String = ""   ' converted to code for WHERE, selects SO's with this specific slsp as slsp1 in header
    Property slspInit2 As String = ""   ' converted to code for WHERE, selects SO's with this specific slsp as slsp2 in header
    Property allForSlspCd As String = ""  ' selects SO's with this specfic slsp code as any slsp on the document, line or header, unless 'ALL'

    Property custCd As String = ""
    Property custTpCd As String = ""
    Property corpName As String = ""
    Property shipFName As String = ""
    Property shipLName As String = ""
    Property shipHPhone As String = ""
    Property shipBPhone As String = ""
    Property shipAddr1 As String = ""
    Property shipAddr2 As String = ""
    Property shipcity As String = ""
    Property shipState As String = ""
    Property shipZip As String = ""
    Property orderQueryRequestFrom As String = "0"
    Property orderQueryRequestTo As String = "0"
    Property poNumber As String = ""
    Property emailAddr As String = ""
    'Added these properties as part of JIRA - MM -4073, these fields are used to query the database 
    Property orderZone As String = ""
    Property orderUserField1 As String = ""
    Property orderUserField2 As String = ""
    Property orderUserField3 As String = ""
    Property orderUserField4 As String = ""
    Property orderUserField5 As String = ""
    Property queryOrderDateRange As Boolean = False 'this property is used to identify whether date range validation is required, by default, set to false
    Property queryPaidInFull As Boolean = False  'indicates if the PAID-IN-FULL value should be calculated or not
    Property orderDateTo As String = ""
    Property confStatusCd As String = String.Empty
End Class
