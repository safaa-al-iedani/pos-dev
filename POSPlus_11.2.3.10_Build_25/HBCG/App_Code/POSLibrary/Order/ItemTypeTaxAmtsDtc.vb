﻿Imports Microsoft.VisualBasic

''' <summary>
''' Class to encapsulate the tax rate percentage amount, or tax amounts by item type.
''' </summary>
Public Class ItemTypeTaxAmtsDtc

    ' lin: for header taxing input, total of ALL lines (not setup or delivery charges) amounts
    ' lin: for tax rates by type for line taxing, is tax rate on inventory = Y items
    Property lin As Double = 0.0
    Property fab As Double = 0.0
    Property lab As Double = 0.0
    Property mil As Double = 0.0
    Property nlt As Double = 0.0 ' not locatable, taxable
    Property nln As Double = 0.0 ' not locatable, not taxable
    Property par As Double = 0.0
    Property war As Double = 0.0
    Property svc As Double = 0.0
    Property pkg As Double = 0.0 ' not used by taxing
    Property dnr As Double = 0.0 ' not used by taxing, do not report
    Property del As Double = 0.0
    Property setup As Double = 0.0

End Class
