﻿Imports Microsoft.VisualBasic

''' <summary>
''' Object used to process the creation of lines, upon line split
''' </summary>
''' <remarks></remarks>
Public Class SplitLineRequestDtc
    Property soDiscCd As String
    Property origDelDocNum As String
    Property origLnNum As String            ' for TEMP_ITM table, is line id (aka row_id)

    Property newDelDocNum As String         ' for TEMP_ITM table is session Id
    Property newLnNum As String
    Property newLnQty As Double
    Property newLnStoreCd As String
    Property newLnLocCd As String
    Property newLnTax As String
    Property newLnWarrByLine As String      'warranty line seq # that the new line needs to be linked to
    Property newLnWarrBySku As String       'warranty sku# that the new line needs to be linked to
    Property newLnWarrByDocNum As String    'warranty doc# that the new line needs to be linked to
End Class
