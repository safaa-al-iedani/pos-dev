﻿Imports Microsoft.VisualBasic

''' <summary>
''' Object that represents a payment, specifically an orginal payment as in what was 
''' originally on a transaction.
''' </summary>
<Serializable()>
Public Class OriginalPaymentDtc

    ' A parameter-less constructor is required in order to serialize a class
    Public Sub New()

    End Sub

    Property companyCd As String
    Property custCd As String
    Property mopDesc As String
    Property amt As Double
    Property acctNum As String      'is populated only for CC - it is the token returned by PB e.g. ID:Cg8r7QuZYfYc5KIUVF
    Property bankCardNum As String  'is populated for CC(masked) & FI(plain text)
    Property expDate As String
    Property mopCd As String
    Property mopTp As String
    Property mopDescAmt As String   'used specifically for displaying the description and amt

End Class
