Imports Microsoft.VisualBasic
Imports System.Xml

Public Class ProcessFITrancationBC

    Public Sub New(ByRef requestDS As RequestAllianceDataSet)
        _RequestDataSet = requestDS
    End Sub

    Dim _ResponseDataSet As ResponseAllianceDataSet
    Dim _RequestDataSet As RequestAllianceDataSet
    Dim requestBuilder As New XMLRequestBuilder()
    Dim responseParser As New XMLResponseParser()

    Public Property ResponseDataSet() As ResponseAllianceDataSet
        Get
            If _ResponseDataSet Is Nothing Then
                _ResponseDataSet = New ResponseAllianceDataSet()
            End If
            Return _ResponseDataSet
        End Get
        Set(ByVal value As ResponseAllianceDataSet)
            _ResponseDataSet = value
        End Set
    End Property

    Public Property RequestDataSet() As RequestAllianceDataSet
        Get
            Return _RequestDataSet
        End Get
        Set(ByVal value As RequestAllianceDataSet)
            _RequestDataSet = value
        End Set
    End Property

    Public Sub Execute()
        Dim validator As New ProcessFITransactionValidator(RequestDataSet)
        Dim bValid As Boolean = validator.Validate
        If bValid Then
            Dim requestXML, responseXML As XmlDataDocument
            requestXML = requestBuilder.BuildAllianceDataRequest(RequestDataSet)
            Dim dac As New ProcessFITransactionDAC(requestXML)
            responseXML = dac.SendRequestAndGetResponse(RequestDataSet)
            responseParser.ParseResponseAndFillDS(responseXML, ResponseDataSet)
        End If
    End Sub
End Class
