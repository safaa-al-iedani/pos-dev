Imports Microsoft.VisualBasic
Imports System.Data.OracleClient

Public Class onBase

    Public Shared Sub Import_To_Onbase(ByVal filename As String, ByVal doc_type As String, ByVal del_doc As String, Optional ByVal archive As String = "N")

        Dim index_output As String = ""
        Try
            Dim TheFile As System.IO.FileInfo = New System.IO.FileInfo("c:\Invoices\" & filename)
            Dim randObj As New Random
            If TheFile.Exists Then
                System.IO.File.Copy("c:\Invoices\" & filename, ConfigurationManager.AppSettings("onbase_dir").ToString & randObj.Next(50000) & filename)
            End If
            If System.IO.File.Exists(ConfigurationManager.AppSettings("onbase_dir").ToString & doc_type & ".txt") Then
                Dim StreamReader As System.IO.StreamReader = New System.IO.StreamReader(ConfigurationManager.AppSettings("onbase_dir").ToString & doc_type & ".txt")
                index_output = StreamReader.ReadToEnd()
                StreamReader.Close()
            End If

            Dim StreamWriter As System.IO.StreamWriter = New System.IO.StreamWriter(ConfigurationManager.AppSettings("onbase_dir").ToString & doc_type & ".txt")
            StreamWriter.WriteLine(index_output & Write_Indexes(del_doc, randObj.Next(50000) & filename, doc_type))
            StreamWriter.Close()
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Shared Function Write_Indexes(ByVal del_doc As String, ByVal filename As String, ByVal doc_type As String)

        Write_Indexes = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        Dim objsql As OracleCommand
        Dim MydataReader As OracleDataReader
        Dim file_output As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                       ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()
        sql = "SELECT SO_WR_DT, CUST_CD, DEL_DOC_NUM, SO_STORE_CD, ORD_TP_CD, SO_EMP_SLSP_CD1, PU_DEL_DT FROM SO WHERE DEL_DOC_NUM='" & del_doc & "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            'Execute DataReader 
            MydataReader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If MydataReader.Read() Then
                file_output = FormatDateTime(MydataReader.Item("SO_WR_DT").ToString, DateFormat.ShortDate) & " | "
                file_output = file_output & MydataReader.Item("CUST_CD").ToString & " | "
                file_output = file_output & MydataReader.Item("DEL_DOC_NUM").ToString & " | "
                file_output = file_output & MydataReader.Item("SO_STORE_CD").ToString & " | "
                file_output = file_output & MydataReader.Item("ORD_TP_CD").ToString & " | "
                file_output = file_output & FormatDateTime(Now.Date.ToString, DateFormat.ShortDate) & " " & FormatDateTime(String.Format("{0:T}", DateTime.Now), DateFormat.ShortTime) & " | "
                file_output = file_output & MydataReader.Item("SO_EMP_SLSP_CD1").ToString & " | "
                If doc_type = "invoice" Then
                    file_output = file_output & MydataReader.Item("PU_DEL_DT").ToString & " | "
                End If
                file_output = file_output & filename & Chr(10) & Chr(13)
            End If
            MydataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()
        Write_Indexes = file_output

    End Function

End Class
