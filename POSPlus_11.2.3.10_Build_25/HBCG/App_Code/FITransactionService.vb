Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class FITransactionService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function InvokeFITransaction(ByRef requestDS As RequestAllianceDataSet) As ResponseAllianceDataSet
        Dim fiTransactionBC As New ProcessFITrancationBC(requestDS)
        fiTransactionBC.Execute()
        Return fiTransactionBC.ResponseDataSet
    End Function

End Class
