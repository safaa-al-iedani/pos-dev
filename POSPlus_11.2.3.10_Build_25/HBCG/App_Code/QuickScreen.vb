Imports DevExpress.XtraReports.UI
Imports System.Data.OracleClient

Public Class QuickScreen
    Inherits DevExpress.XtraReports.UI.XtraReport
    Dim preapproved As Double = 0
    Dim declined As Double = 0
    Dim rejected As Double = 0
    Dim accepted As Double = 0
    Dim a_approved As Double = 0
    Dim a_declined As Double = 0
    Dim a_pending As Double = 0
    Dim t_preapproved As Double = 0
    Dim t_declined As Double = 0
    Dim t_rejected As Double = 0
    Dim t_accepted As Double = 0
    Dim t_a_approved As Double = 0
    Dim t_a_declined As Double = 0
    Dim t_a_pending As Double = 0
    Private WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Private WithEvents xr_preapproved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_rejected As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_declined As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_accepted As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_qs_Status As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_edate As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLine2 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_total_preapproved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_total_preapproved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_accepted As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_declined As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_rejected As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_preapproved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_total As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_a_approved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_a_declined As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_a_pending As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_a_approved As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_a_declined As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_t_a_pending As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents xr_qs_count As DevExpress.XtraReports.UI.XRLabel

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents xrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_sdate As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_store_cd As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Private WithEvents xr_slsp_lname As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_home As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xr_slsp_emp_cd As DevExpress.XtraReports.UI.XRLabel

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "QuickScreen.resx"
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xr_store_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp_emp_cd = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_qs_Status = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_qs_count = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_accepted = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_declined = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_rejected = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_preapproved = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_edate = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_sdate = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.xr_slsp_home = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp_lname = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.xr_a_pending = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_a_declined = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_a_approved = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_slsp = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_total_preapproved = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_total = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xr_t_a_pending = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_a_declined = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_a_approved = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_total_preapproved = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_accepted = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_declined = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_rejected = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_preapproved = New DevExpress.XtraReports.UI.XRLabel
        Me.xr_t_total = New DevExpress.XtraReports.UI.XRLabel
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_store_cd, Me.xr_slsp_emp_cd, Me.xr_qs_Status, Me.xr_qs_count})
        Me.Detail.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detail.HeightF = 17.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xr_store_cd
        '
        Me.xr_store_cd.CanShrink = True
        Me.xr_store_cd.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_store_cd.LocationFloat = New DevExpress.Utils.PointFloat(142.0!, 0.0!)
        Me.xr_store_cd.Name = "xr_store_cd"
        Me.xr_store_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_store_cd.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_store_cd.StylePriority.UseFont = False
        Me.xr_store_cd.Visible = False
        '
        'xr_slsp_emp_cd
        '
        Me.xr_slsp_emp_cd.CanShrink = True
        Me.xr_slsp_emp_cd.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_emp_cd.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_slsp_emp_cd.Name = "xr_slsp_emp_cd"
        Me.xr_slsp_emp_cd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_emp_cd.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_slsp_emp_cd.StylePriority.UseFont = False
        Me.xr_slsp_emp_cd.Text = "xr_slsp_emp_cd"
        Me.xr_slsp_emp_cd.Visible = False
        '
        'xr_qs_Status
        '
        Me.xr_qs_Status.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qs_Status.LocationFloat = New DevExpress.Utils.PointFloat(767.0!, 0.0!)
        Me.xr_qs_Status.Name = "xr_qs_Status"
        Me.xr_qs_Status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qs_Status.SizeF = New System.Drawing.SizeF(42.0!, 17.0!)
        Me.xr_qs_Status.StylePriority.UseFont = False
        Me.xr_qs_Status.Visible = False
        '
        'xr_qs_count
        '
        Me.xr_qs_count.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_qs_count.LocationFloat = New DevExpress.Utils.PointFloat(825.0!, 0.0!)
        Me.xr_qs_count.Name = "xr_qs_count"
        Me.xr_qs_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_qs_count.SizeF = New System.Drawing.SizeF(42.0!, 17.0!)
        Me.xr_qs_count.StylePriority.UseFont = False
        Me.xr_qs_count.Visible = False
        '
        'xr_accepted
        '
        Me.xr_accepted.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_accepted.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_accepted.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 0.0!)
        Me.xr_accepted.Name = "xr_accepted"
        Me.xr_accepted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_accepted.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_accepted.StylePriority.UseBackColor = False
        Me.xr_accepted.StylePriority.UseFont = False
        Me.xr_accepted.StylePriority.UseTextAlignment = False
        Me.xr_accepted.Text = "0"
        Me.xr_accepted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel10
        '
        Me.xrLabel10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 50.0!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(67.0!, 50.0!)
        Me.xrLabel10.StylePriority.UseFont = False
        Me.xrLabel10.StylePriority.UseTextAlignment = False
        Me.xrLabel10.Text = "Customer Accepted Offer"
        Me.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_declined
        '
        Me.xr_declined.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_declined.LocationFloat = New DevExpress.Utils.PointFloat(289.0!, 0.0!)
        Me.xr_declined.Name = "xr_declined"
        Me.xr_declined.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_declined.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xr_declined.StylePriority.UseFont = False
        Me.xr_declined.StylePriority.UseTextAlignment = False
        Me.xr_declined.Text = "0"
        Me.xr_declined.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel8
        '
        Me.xrLabel8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(289.0!, 50.0!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(58.0!, 33.0!)
        Me.xrLabel8.StylePriority.UseFont = False
        Me.xrLabel8.StylePriority.UseTextAlignment = False
        Me.xrLabel8.Text = "Declined by GE"
        Me.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_rejected
        '
        Me.xr_rejected.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_rejected.LocationFloat = New DevExpress.Utils.PointFloat(447.0!, 0.0!)
        Me.xr_rejected.Name = "xr_rejected"
        Me.xr_rejected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_rejected.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_rejected.StylePriority.UseFont = False
        Me.xr_rejected.StylePriority.UseTextAlignment = False
        Me.xr_rejected.Text = "0"
        Me.xr_rejected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(445.0!, 50.0!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(67.0!, 50.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.StylePriority.UseTextAlignment = False
        Me.xrLabel6.Text = "Customer Rejected Offer"
        Me.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_preapproved
        '
        Me.xr_preapproved.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_preapproved.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 0.0!)
        Me.xr_preapproved.Name = "xr_preapproved"
        Me.xr_preapproved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_preapproved.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_preapproved.StylePriority.UseFont = False
        Me.xr_preapproved.StylePriority.UseTextAlignment = False
        Me.xr_preapproved.Text = "0"
        Me.xr_preapproved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel4
        '
        Me.xrLabel4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.StylePriority.UseTextAlignment = False
        Me.xrLabel4.Text = "SLSP"
        Me.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel16, Me.xrLabel15, Me.xrLabel14, Me.xrLabel9, Me.xrLabel12, Me.xrLabel10, Me.xrLabel8, Me.xrLabel13, Me.xrLabel11, Me.xrLabel6, Me.xrLabel7, Me.xrLabel4, Me.xrLine2, Me.xrLine1, Me.xrLabel5, Me.xr_edate, Me.xrLabel3, Me.xr_sdate, Me.xrLabel1, Me.xrPageInfo1})
        Me.PageHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageHeader.HeightF = 120.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseFont = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel16
        '
        Me.xrLabel16.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xrLabel16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Italic)
        Me.xrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 50.0!)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel16.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
        Me.xrLabel16.StylePriority.UseBackColor = False
        Me.xrLabel16.StylePriority.UseFont = False
        Me.xrLabel16.StylePriority.UseTextAlignment = False
        Me.xrLabel16.Text = "Customer Accepted Offer:"
        Me.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel15
        '
        Me.xrLabel15.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xrLabel15.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(792.0!, 75.0!)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel15.StylePriority.UseBackColor = False
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.StylePriority.UseTextAlignment = False
        Me.xrLabel15.Text = "PENDING"
        Me.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel14
        '
        Me.xrLabel14.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xrLabel14.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 75.0!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel14.StylePriority.UseBackColor = False
        Me.xrLabel14.StylePriority.UseFont = False
        Me.xrLabel14.StylePriority.UseTextAlignment = False
        Me.xrLabel14.Text = "DECLINED"
        Me.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel9
        '
        Me.xrLabel9.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xrLabel9.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 75.0!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
        Me.xrLabel9.StylePriority.UseBackColor = False
        Me.xrLabel9.StylePriority.UseFont = False
        Me.xrLabel9.StylePriority.UseTextAlignment = False
        Me.xrLabel9.Text = "APPROVED"
        Me.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel12
        '
        Me.xrLabel12.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xrLabel12.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 50.0!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(92.0!, 42.0!)
        Me.xrLabel12.StylePriority.UseBackColor = False
        Me.xrLabel12.StylePriority.UseFont = False
        Me.xrLabel12.StylePriority.UseTextAlignment = False
        Me.xrLabel12.Text = "Pre-Approved by GE"
        Me.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(211.0!, 50.0!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(75.0!, 33.0!)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.StylePriority.UseTextAlignment = False
        Me.xrLabel13.Text = "Total QS Processsed"
        Me.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel11
        '
        Me.xrLabel11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 50.0!)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel11.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xrLabel11.StylePriority.UseFont = False
        Me.xrLabel11.StylePriority.UseTextAlignment = False
        Me.xrLabel11.Text = "Last Name"
        Me.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 50.0!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 50.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.StylePriority.UseTextAlignment = False
        Me.xrLabel7.Text = "UNHANDLED Pre-Approvals"
        Me.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLine2
        '
        Me.xrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 108.0!)
        Me.xrLine2.Name = "xrLine2"
        Me.xrLine2.SizeF = New System.Drawing.SizeF(1000.0!, 8.0!)
        '
        'xrLine1
        '
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 42.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(1000.0!, 8.0!)
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.Text = "through"
        '
        'xr_edate
        '
        Me.xr_edate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_edate.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 0.0!)
        Me.xr_edate.Name = "xr_edate"
        Me.xr_edate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_edate.SizeF = New System.Drawing.SizeF(87.83333!, 17.0!)
        Me.xr_edate.StylePriority.UseFont = False
        '
        'xrLabel3
        '
        Me.xrLabel3.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(200.0!, 25.0!)
        Me.xrLabel3.StylePriority.UseFont = False
        Me.xrLabel3.Text = "Quick Screen"
        Me.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_sdate
        '
        Me.xr_sdate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_sdate.LocationFloat = New DevExpress.Utils.PointFloat(56.25!, 0.0!)
        Me.xr_sdate.Name = "xr_sdate"
        Me.xr_sdate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_sdate.SizeF = New System.Drawing.SizeF(85.75!, 17.0!)
        Me.xr_sdate.StylePriority.UseFont = False
        '
        'xrLabel1
        '
        Me.xrLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(42.0!, 17.0!)
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Date:"
        '
        'xrPageInfo1
        '
        Me.xrPageInfo1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(933.0!, 0.0!)
        Me.xrPageInfo1.Name = "xrPageInfo1"
        Me.xrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageInfo1.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrPageInfo1.StylePriority.UseFont = False
        Me.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'PageFooter
        '
        Me.PageFooter.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PageFooter.HeightF = 0.0!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.Gainsboro
        Me.GroupHeader1.HeightF = 0.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.StylePriority.UseBackColor = False
        '
        'xr_slsp_home
        '
        Me.xr_slsp_home.BackColor = System.Drawing.Color.Transparent
        Me.xr_slsp_home.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_home.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
        Me.xr_slsp_home.Name = "xr_slsp_home"
        Me.xr_slsp_home.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_home.SizeF = New System.Drawing.SizeF(25.0!, 17.0!)
        Me.xr_slsp_home.StylePriority.UseBackColor = False
        Me.xr_slsp_home.StylePriority.UseFont = False
        '
        'xr_slsp_lname
        '
        Me.xr_slsp_lname.BackColor = System.Drawing.Color.Transparent
        Me.xr_slsp_lname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp_lname.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 0.0!)
        Me.xr_slsp_lname.Name = "xr_slsp_lname"
        Me.xr_slsp_lname.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp_lname.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_slsp_lname.StylePriority.UseBackColor = False
        Me.xr_slsp_lname.StylePriority.UseFont = False
        Me.xr_slsp_lname.WordWrap = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_a_pending, Me.xr_a_declined, Me.xr_a_approved, Me.xr_slsp_home, Me.xr_slsp, Me.xr_total_preapproved, Me.xr_slsp_lname, Me.xr_accepted, Me.xr_declined, Me.xr_rejected, Me.xr_preapproved, Me.xr_total})
        Me.GroupFooter1.HeightF = 17.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'xr_a_pending
        '
        Me.xr_a_pending.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_a_pending.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xr_a_pending.LocationFloat = New DevExpress.Utils.PointFloat(792.0!, 0.0!)
        Me.xr_a_pending.Name = "xr_a_pending"
        Me.xr_a_pending.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_a_pending.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_a_pending.StylePriority.UseBackColor = False
        Me.xr_a_pending.StylePriority.UseFont = False
        Me.xr_a_pending.StylePriority.UseTextAlignment = False
        Me.xr_a_pending.Text = "0"
        Me.xr_a_pending.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_a_declined
        '
        Me.xr_a_declined.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_a_declined.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xr_a_declined.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
        Me.xr_a_declined.Name = "xr_a_declined"
        Me.xr_a_declined.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_a_declined.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_a_declined.StylePriority.UseBackColor = False
        Me.xr_a_declined.StylePriority.UseFont = False
        Me.xr_a_declined.StylePriority.UseTextAlignment = False
        Me.xr_a_declined.Text = "0"
        Me.xr_a_declined.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_a_approved
        '
        Me.xr_a_approved.BackColor = System.Drawing.Color.Transparent
        Me.xr_a_approved.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xr_a_approved.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 0.0!)
        Me.xr_a_approved.Name = "xr_a_approved"
        Me.xr_a_approved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_a_approved.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_a_approved.StylePriority.UseBackColor = False
        Me.xr_a_approved.StylePriority.UseFont = False
        Me.xr_a_approved.StylePriority.UseTextAlignment = False
        Me.xr_a_approved.Text = "0"
        Me.xr_a_approved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_slsp
        '
        Me.xr_slsp.BackColor = System.Drawing.Color.Transparent
        Me.xr_slsp.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xr_slsp.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xr_slsp.Name = "xr_slsp"
        Me.xr_slsp.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_slsp.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_slsp.StylePriority.UseBackColor = False
        Me.xr_slsp.StylePriority.UseFont = False
        '
        'xr_total_preapproved
        '
        Me.xr_total_preapproved.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_total_preapproved.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xr_total_preapproved.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
        Me.xr_total_preapproved.Name = "xr_total_preapproved"
        Me.xr_total_preapproved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_total_preapproved.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xr_total_preapproved.StylePriority.UseBackColor = False
        Me.xr_total_preapproved.StylePriority.UseFont = False
        Me.xr_total_preapproved.StylePriority.UseTextAlignment = False
        Me.xr_total_preapproved.Text = "0"
        Me.xr_total_preapproved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_total
        '
        Me.xr_total.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.xr_total.LocationFloat = New DevExpress.Utils.PointFloat(211.0!, 0.0!)
        Me.xr_total.Name = "xr_total"
        Me.xr_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_total.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xr_total.StylePriority.UseFont = False
        Me.xr_total.StylePriority.UseTextAlignment = False
        Me.xr_total.Text = "0"
        Me.xr_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xr_t_a_pending, Me.xr_t_a_declined, Me.xr_t_a_approved, Me.xrLabel2, Me.xr_t_total_preapproved, Me.xr_t_accepted, Me.xr_t_declined, Me.xr_t_rejected, Me.xr_t_preapproved, Me.xr_t_total})
        Me.ReportFooter.HeightF = 19.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'xr_t_a_pending
        '
        Me.xr_t_a_pending.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_t_a_pending.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_a_pending.LocationFloat = New DevExpress.Utils.PointFloat(792.0!, 0.0!)
        Me.xr_t_a_pending.Name = "xr_t_a_pending"
        Me.xr_t_a_pending.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_a_pending.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_t_a_pending.StylePriority.UseBackColor = False
        Me.xr_t_a_pending.StylePriority.UseFont = False
        Me.xr_t_a_pending.StylePriority.UseTextAlignment = False
        Me.xr_t_a_pending.Text = "0"
        Me.xr_t_a_pending.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_a_declined
        '
        Me.xr_t_a_declined.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_t_a_declined.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_a_declined.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
        Me.xr_t_a_declined.Name = "xr_t_a_declined"
        Me.xr_t_a_declined.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_a_declined.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_t_a_declined.StylePriority.UseBackColor = False
        Me.xr_t_a_declined.StylePriority.UseFont = False
        Me.xr_t_a_declined.StylePriority.UseTextAlignment = False
        Me.xr_t_a_declined.Text = "0"
        Me.xr_t_a_declined.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_a_approved
        '
        Me.xr_t_a_approved.BackColor = System.Drawing.Color.Transparent
        Me.xr_t_a_approved.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_a_approved.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 0.0!)
        Me.xr_t_a_approved.Name = "xr_t_a_approved"
        Me.xr_t_a_approved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_a_approved.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_t_a_approved.StylePriority.UseBackColor = False
        Me.xr_t_a_approved.StylePriority.UseFont = False
        Me.xr_t_a_approved.StylePriority.UseTextAlignment = False
        Me.xr_t_a_approved.Text = "0"
        Me.xr_t_a_approved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(136.0!, 0.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.Text = "Totals:"
        '
        'xr_t_total_preapproved
        '
        Me.xr_t_total_preapproved.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_t_total_preapproved.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_total_preapproved.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
        Me.xr_t_total_preapproved.Name = "xr_t_total_preapproved"
        Me.xr_t_total_preapproved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_total_preapproved.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.xr_t_total_preapproved.StylePriority.UseBackColor = False
        Me.xr_t_total_preapproved.StylePriority.UseFont = False
        Me.xr_t_total_preapproved.StylePriority.UseTextAlignment = False
        Me.xr_t_total_preapproved.Text = "0"
        Me.xr_t_total_preapproved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_accepted
        '
        Me.xr_t_accepted.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.xr_t_accepted.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_accepted.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 0.0!)
        Me.xr_t_accepted.Name = "xr_t_accepted"
        Me.xr_t_accepted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_accepted.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_t_accepted.StylePriority.UseBackColor = False
        Me.xr_t_accepted.StylePriority.UseFont = False
        Me.xr_t_accepted.StylePriority.UseTextAlignment = False
        Me.xr_t_accepted.Text = "0"
        Me.xr_t_accepted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_declined
        '
        Me.xr_t_declined.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_declined.LocationFloat = New DevExpress.Utils.PointFloat(289.0!, 0.0!)
        Me.xr_t_declined.Name = "xr_t_declined"
        Me.xr_t_declined.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_declined.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
        Me.xr_t_declined.StylePriority.UseFont = False
        Me.xr_t_declined.StylePriority.UseTextAlignment = False
        Me.xr_t_declined.Text = "0"
        Me.xr_t_declined.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_rejected
        '
        Me.xr_t_rejected.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_rejected.LocationFloat = New DevExpress.Utils.PointFloat(447.0!, 0.0!)
        Me.xr_t_rejected.Name = "xr_t_rejected"
        Me.xr_t_rejected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_rejected.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
        Me.xr_t_rejected.StylePriority.UseFont = False
        Me.xr_t_rejected.StylePriority.UseTextAlignment = False
        Me.xr_t_rejected.Text = "0"
        Me.xr_t_rejected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_preapproved
        '
        Me.xr_t_preapproved.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_preapproved.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 0.0!)
        Me.xr_t_preapproved.Name = "xr_t_preapproved"
        Me.xr_t_preapproved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_preapproved.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.xr_t_preapproved.StylePriority.UseFont = False
        Me.xr_t_preapproved.StylePriority.UseTextAlignment = False
        Me.xr_t_preapproved.Text = "0"
        Me.xr_t_preapproved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xr_t_total
        '
        Me.xr_t_total.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xr_t_total.LocationFloat = New DevExpress.Utils.PointFloat(211.0!, 0.0!)
        Me.xr_t_total.Name = "xr_t_total"
        Me.xr_t_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xr_t_total.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.xr_t_total.StylePriority.UseFont = False
        Me.xr_t_total.StylePriority.UseTextAlignment = False
        Me.xr_t_total.Text = "0"
        Me.xr_t_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 50.0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 50.0!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        '
        'QuickScreen
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.topMarginBand1, Me.bottomMarginBand1})
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "10.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand

#End Region

    Public Sub BindSOVariables()

        'Bind the database fields to their corresponding report labels
        xr_store_cd.DataBindings.Add("Text", DataSource, "AR_TRN.REP_ST_CD")
        xr_sdate.DataBindings.Add("Text", DataSource, "AR_TRN.ST_DT")
        xr_edate.DataBindings.Add("Text", DataSource, "AR_TRN.END_DT")
        xr_slsp_emp_cd.DataBindings.Add("Text", DataSource, "AR_TRN.EMP_CD")
        'xr_slsp_home.DataBindings.Add("Text", DataSource, "EMP.HOME_STORE_CD")
        xr_qs_Status.DataBindings.Add("Text", DataSource, "AR_TRN.QS_STATUS")
        xr_qs_count.DataBindings.Add("Text", DataSource, "AR_TRN.QS_COUNT")

    End Sub

    Private Sub GroupFooter1_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.AfterPrint

        preapproved = 0.0
        declined = 0.0
        rejected = 0.0
        accepted = 0.0
        a_approved = 0.0
        a_declined = 0.0
        a_pending = 0.0

    End Sub

    Private Sub GroupFooter1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles GroupFooter1.BeforePrint

        Dim xr_preapproved As XRLabel = GroupFooter1.FindControl("xr_preapproved", True)
        xr_preapproved.Text = preapproved
        Dim xr_accepted As XRLabel = GroupFooter1.FindControl("xr_accepted", True)
        xr_accepted.Text = accepted
        Dim xr_rejected As XRLabel = GroupFooter1.FindControl("xr_rejected", True)
        xr_rejected.Text = rejected

        Dim xr_a_approved As XRLabel = GroupFooter1.FindControl("xr_a_approved", True)
        xr_a_approved.Text = accepted + a_declined + a_pending
        Dim xr_a_declined As XRLabel = GroupFooter1.FindControl("xr_a_declined", True)
        xr_a_declined.Text = a_declined
        Dim xr_a_pending As XRLabel = GroupFooter1.FindControl("xr_a_pending", True)
        xr_a_pending.Text = a_pending

        Dim xr_declined As XRLabel = GroupFooter1.FindControl("xr_declined", True)
        xr_declined.Text = declined
        Dim xr_total_preapproved As XRLabel = GroupFooter1.FindControl("xr_total_preapproved", True)
        xr_total_preapproved.Text = preapproved + accepted + rejected + a_declined + a_pending
        Dim xr_slsp As XRLabel = GroupFooter1.FindControl("xr_slsp", True)
        xr_slsp.Text = xr_slsp_emp_cd.Text

        Dim xr_total As XRLabel = GroupFooter1.FindControl("xr_total", True)
        xr_total.Text = declined + rejected + accepted + preapproved

    End Sub

    Private Sub ReportFooter_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles ReportFooter.BeforePrint

        xr_t_preapproved.Text = t_preapproved
        xr_t_accepted.Text = t_accepted
        xr_t_rejected.Text = t_rejected
        xr_t_declined.Text = t_declined
        xr_t_total_preapproved.Text = t_preapproved + t_accepted + t_rejected + t_a_pending + t_a_declined
        xr_t_total.Text = t_declined + t_rejected + t_accepted + t_preapproved + t_a_pending + t_a_declined
        xr_t_a_approved.Text = t_a_pending + t_accepted + t_a_declined
        xr_t_a_declined.Text = t_a_declined
        xr_t_a_pending.Text = t_a_pending

    End Sub

    Private Sub xr_qs_Status_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_qs_Status.BeforePrint

        Dim qs_count As Double = 0
        If IsNumeric(xr_qs_count.Text) Then qs_count = CDbl(xr_qs_count.Text)
        Select Case xr_qs_Status.Text
            Case "P"
                preapproved = preapproved + qs_count
                t_preapproved = t_preapproved + qs_count
            Case "A"
                accepted = accepted + qs_count
                t_accepted = t_accepted + qs_count
            Case "R"
                rejected = rejected + qs_count
                t_rejected = t_rejected + qs_count
            Case "D"
                declined = declined + qs_count
                t_declined = t_declined + qs_count
            Case "N"
                a_pending = a_pending + qs_count
                t_a_pending = t_a_pending + qs_count
            Case "X"
                a_declined = a_declined + qs_count
                t_a_declined = t_a_declined + qs_count
        End Select
    End Sub

    Private Sub xr_slsp_emp_cd_BeforePrint1(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xr_slsp_emp_cd.BeforePrint

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & xr_slsp_emp_cd.Text & "'"

        Try
            'Open Connection 
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                xr_slsp_lname.Text = MyDataReader.Item("LNAME").ToString
                xr_slsp_home.Text = MyDataReader.Item("HOME_STORE_CD").ToString
                xr_store_cd.Text = MyDataReader.Item("HOME_STORE_CD").ToString
            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            Throw
            conn.Close()
        End Try

    End Sub
End Class