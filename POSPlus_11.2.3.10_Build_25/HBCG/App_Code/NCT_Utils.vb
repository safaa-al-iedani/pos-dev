Imports System
Imports System.IO
Imports System.Net
Imports System.Data.OracleClient


Public Class NCT_Utils

    Public Shared Function NCT_Submit_Query(ByVal payment_id As Integer, ByVal merchant_id As String, Optional ByVal del_doc As String = "", Optional ByVal tran_tp As String = "2")

        Dim WebReq As HttpWebRequest
        Dim WebResp As HttpWebResponse
        Dim StrmRdr As StreamReader
        Dim URLString As String = "https://connect.nctinc.net/echo/jobpages/Linked_Transactions.aspx?" & Build_Basic_Transaction(payment_id, del_doc, merchant_id)
        Dim HTML As String

        WebReq = CType(WebRequest.Create(New Uri(URLString)), HttpWebRequest)
        WebReq.Credentials = CredentialCache.DefaultCredentials
        WebResp = WebReq.GetResponse
        StrmRdr = New StreamReader(WebResp.GetResponseStream)
        HTML = StrmRdr.ReadToEnd.Trim
        StrmRdr.Close()
        WebResp.Close()

        HTML = Left(HTML, HTML.IndexOf("<!DOCTYPE"))
        HTML = Replace(HTML, vbCrLf, "")
        Return HTML

    End Function

    Private Shared Function Build_Basic_Transaction(ByVal payment_id As Integer, ByVal del_doc As String, ByVal merchant_id As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim amt As Double = 0
        Dim chk_num As String = ""
        Dim chk_tp As String = ""
        Dim TransRoute As String = ""
        Dim Acct_No As String = ""
        Dim dl_state As String = ""
        Dim dl_license_no As String = ""
        Dim Guaranteed As String = ""
        Dim ACH As String = ""

        '
        ' **REMOVE THIS AFTER TESTING
        '
        Guaranteed = "1"
        ACH = "0"

        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()

        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader

        Dim sql As String = ""
        sql = "SELECT AMT, CHK_NUM, CHK_TP, TRANS_ROUTE, CHK_ACCOUNT_NO, DL_STATE, DL_LICENSE_NO FROM PAYMENT WHERE SESSIONID='" & HttpContext.Current.Session.SessionID & "' AND ROW_ID=" & payment_id
        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If (MyDataReader.Read()) Then
                amt = Replace(FormatNumber(CDbl(MyDataReader.Item("AMT").ToString), 2), ",", "")
                chk_num = MyDataReader.Item("CHK_NUM").ToString
                chk_tp = MyDataReader.Item("CHK_TP").ToString
                TransRoute = MyDataReader.Item("TRANS_ROUTE").ToString
                Acct_No = MyDataReader.Item("CHK_ACCOUNT_NO").ToString
                dl_state = MyDataReader.Item("DL_STATE").ToString
                dl_license_no = MyDataReader.Item("DL_LICENSE_NO").ToString
            End If

            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            Throw
        End Try
        objConnection.Close()

        Dim iString As String
        Dim x As Integer = 1

        '* Create Check Processing String 21228018
        iString = "MerchantId=" & merchant_id
        iString = iString & "&Transdate=" & FormatDateTime(Now, DateFormat.ShortDate)
        iString = iString & "&SettleDate="
        iString = iString & "&fName="
        iString = iString & "&lName="
        iString = iString & "&Address1="
        iString = iString & "&Address2="
        iString = iString & "&City="
        iString = iString & "&State="
        iString = iString & "&Zip="
        iString = iString & "&Home_Phone="
        iString = iString & "&Other_Phone="
        iString = iString & "&Memo="
        iString = iString & "&BName="
        iString = iString & "&Business_Phone="
        iString = iString & "&drState=" & dl_state
        iString = iString & "&drLicense=" & dl_license_no
        iString = iString & "&Amount=" & amt
        iString = iString & "&CheckNo=" & chk_num
        iString = iString & "&Type=" & chk_tp
        iString = iString & "&TransRoute=" & TransRoute
        iString = iString & "&AccountNo=" & Acct_No
        iString = iString & "&RefID=" & del_doc
        iString = iString & "&Guaranteed=" & Guaranteed
        iString = iString & "&ACH=" & ACH

        Return iString

    End Function

End Class



