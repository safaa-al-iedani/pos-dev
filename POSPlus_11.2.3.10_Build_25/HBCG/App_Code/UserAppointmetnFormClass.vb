Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports DevExpress.Web.ASPxScheduler
Imports DevExpress.Web.ASPxScheduler.Internal
Imports DevExpress.XtraScheduler
Imports DevExpress.Web.ASPxEditors

#Region "#userappointmentformtemplatecontainer"

Public Class UserAppointmentFormTemplateContainer
    Inherits AppointmentFormTemplateContainer
    Public Sub New(ByVal control As ASPxScheduler)
        MyBase.New(control)
    End Sub
    Public ReadOnly Property CORP_NAME() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("Corporation"))
        End Get
    End Property
    Public ReadOnly Property PROSPECT_TOTAL() As Double
        Get
            Dim val As Object = Appointment.CustomFields("Prospect Total")
            If (val Is DBNull.Value) Then
                Return FormatCurrency(0, 2)
            Else
                Return FormatCurrency(Convert.ToDouble(val), 2)
            End If
        End Get
    End Property
    Public ReadOnly Property FIRST_NAME() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("First Name"))
        End Get
    End Property
    Public ReadOnly Property LAST_NAME() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("Last Name"))
        End Get
    End Property
    Public ReadOnly Property CITY() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("City"))
        End Get
    End Property
    Public ReadOnly Property ST() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("State"))
        End Get
    End Property
    Public ReadOnly Property ZIP() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("Zip"))
        End Get
    End Property
    Public ReadOnly Property ADDR1() As String
        Get
            Return Convert.ToString(Appointment.CustomFields("Address"))
        End Get
    End Property
End Class

#End Region ' #userappointmentformtemplatecontainer

#Region "#userappointmentformcontroller"

Public Class UserAppointmentFormController
    Inherits AppointmentFormController
    Public Sub New(ByVal control As ASPxScheduler, ByVal apt As Appointment)
        MyBase.New(control, apt)
    End Sub
    Public Property CORP_NAME() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("Corporation"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("Corporation") = value
        End Set
    End Property
    Public Property PROSPECT_TOTAL() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("Prospect Total"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("Prospect Total") = value
        End Set
    End Property
    Public Property FIRST_NAME() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("First Name"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("First Name") = value
        End Set
    End Property
    Public Property LAST_NAME() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("Last Name"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("Last Name") = value
        End Set
    End Property
    Public Property ADDR1() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("Address"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("Address") = value
        End Set
    End Property
    Public Property CITY() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("City"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("City") = value
        End Set
    End Property
    Public Property ST() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("State"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("State") = value
        End Set
    End Property
    Public Property ZIP() As String
        Get
            Return CStr(EditedAppointmentCopy.CustomFields("Zip"))
        End Get
        Set(ByVal value As String)
            EditedAppointmentCopy.CustomFields("Zip") = value
        End Set
    End Property

    Private Property SourceField1() As String
        Get
            Return CDbl(SourceAppointment.CustomFields("Corporation"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("Corporation") = value
        End Set
    End Property
    Private Property SourceField2() As String
        Get
            Return CStr(SourceAppointment.CustomFields("First Name"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("First Name") = value
        End Set
    End Property
    Private Property SourceField3() As String
        Get
            Return CStr(SourceAppointment.CustomFields("Last Name"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("Last Name") = value
        End Set
    End Property
    Private Property SourceField4() As String
        Get
            Return CStr(SourceAppointment.CustomFields("Prospect Total"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("Prospect Total") = FormatCurrency(value, 2)
        End Set
    End Property
    Private Property SourceField5() As String
        Get
            Return CStr(SourceAppointment.CustomFields("Address"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("Address") = value
        End Set
    End Property
    Private Property SourceField6() As String
        Get
            Return CStr(SourceAppointment.CustomFields("City"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("City") = value
        End Set
    End Property
    Private Property SourceField7() As String
        Get
            Return CStr(SourceAppointment.CustomFields("State"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("State") = value
        End Set
    End Property
    Private Property SourceField8() As String
        Get
            Return CStr(SourceAppointment.CustomFields("Zip"))
        End Get
        Set(ByVal value As String)
            SourceAppointment.CustomFields("Zip") = value
        End Set
    End Property

    Public Overrides Function IsAppointmentChanged() As Boolean
        If MyBase.IsAppointmentChanged() Then
            Return True
        End If
        Return SourceField1 <> CORP_NAME OrElse SourceField2 <> FIRST_NAME OrElse SourceField3 <> LAST_NAME OrElse SourceField4 <> PROSPECT_TOTAL OrElse SourceField6 <> CITY OrElse SourceField7 <> ST OrElse SourceField8 <> ZIP OrElse SourceField5 <> ADDR1
    End Function
    Protected Overrides Sub ApplyCustomFieldsValues()
        SourceField1 = CORP_NAME
        SourceField2 = FIRST_NAME
        SourceField3 = LAST_NAME
        SourceField4 = PROSPECT_TOTAL
        SourceField5 = ADDR1
        SourceField6 = CITY
        SourceField7 = ST
        SourceField8 = ZIP
    End Sub
End Class
#End Region ' #userappointmentformcontroller

#Region "#userappointmentsavecallbackcommand"

Public Class UserAppointmentSaveCallbackCommand
    Inherits AppointmentFormSaveCallbackCommand
    Public Sub New(ByVal control As ASPxScheduler)
        MyBase.New(control)
    End Sub
    Protected Friend Shadows ReadOnly Property Controller() As UserAppointmentFormController
        Get
            Return CType(MyBase.Controller, UserAppointmentFormController)
        End Get
    End Property

    Protected Overrides Sub AssignControllerValues()
        Dim tbField1 As ASPxTextBox = CType(FindControlByID("tbField1"), ASPxTextBox)
        Dim tbField2 As ASPxTextBox = CType(FindControlByID("tbField2"), ASPxTextBox)
        Dim tbField3 As ASPxTextBox = CType(FindControlByID("tbField3"), ASPxTextBox)
        Dim tbField4 As ASPxLabel = CType(FindControlByID("tbField4"), ASPxLabel)
        Dim tbField5 As ASPxTextBox = CType(FindControlByID("tbField5"), ASPxTextBox)
        Dim tbField6 As ASPxTextBox = CType(FindControlByID("tbField6"), ASPxTextBox)
        Dim tbField7 As ASPxTextBox = CType(FindControlByID("tbField7"), ASPxTextBox)
        Dim tbField8 As ASPxTextBox = CType(FindControlByID("tbField8"), ASPxTextBox)

        Controller.CORP_NAME = Convert.ToString(tbField1.Text)
        Controller.FIRST_NAME = tbField2.Text
        Controller.LAST_NAME = tbField3.Text
        Controller.PROSPECT_TOTAL = tbField4.Text
        Controller.ADDR1 = tbField5.Text
        Controller.CITY = tbField6.Text
        Controller.ST = tbField7.Text
        Controller.ZIP = tbField8.Text

        MyBase.AssignControllerValues()
    End Sub
    Protected Overrides Function CreateAppointmentFormController(ByVal apt As Appointment) As AppointmentFormController
        Return New UserAppointmentFormController(Control, apt)
    End Function
End Class

#End Region ' #userappointmentsavecallbackcommand  
