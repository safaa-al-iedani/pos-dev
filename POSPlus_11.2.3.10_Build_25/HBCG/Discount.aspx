﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Regular.master" AutoEventWireup="false" CodeFile="Discount.aspx.vb" Inherits="Discount" %>
<%@ Register Src="~/Usercontrols/Discount.ascx" TagPrefix="ucDisc" TagName="Discount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ucDisc:Discount runat="server" ID="ucDiscount" />
    <asp:HiddenField ID="mgrApproval" ClientIDMode="Static" runat="server" />
    <script language="javascript" type="text/javascript">
        // MM-5661        
        function pageLoad(sender, args) {
            $('a').click(function (event) {
                var rowcount = 0;

                if ($('#grdDiscItems').length > 0) {
                    rowcount = $get("grdDiscItems").rows.length;
                }               

                var mgrApproval = $('#mgrApproval').val();
                var _hidItems = $('#hidItmSeq').val();
                if (('<% = SysPms.discWithAppSec %>' == 'W') && (mgrApproval != 'Y') && ('<% =AppConstants.ManagerOverride.BY_ORDER %>' == '<% =SalesUtils.GetPriceMgrOverride() %>') && (rowcount > 0) && (_hidItems > 0)) {
                    __doPostBack('popupMgrOverride', "");
                    event.preventDefault();
                    
                }
            });
        };
    </script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TaxIconHolder" runat="Server">
</asp:Content>
