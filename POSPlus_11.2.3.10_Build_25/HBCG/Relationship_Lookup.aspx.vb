Imports System.Data
Imports System.Data.OracleClient

Partial Class Relationship_Lookup
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack = True Then
            GridView1_Binddata()
        End If

    End Sub

    Public Sub GridView1_Binddata()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet
        Dim LikeQry As String

        Dim FOLLOW_UP_DT As String = ""

        FOLLOW_UP_DT = Request("follow_up")
        If String.IsNullOrEmpty(FOLLOW_UP_DT) Then
            FOLLOW_UP_DT = Request("LEAD_DT")
        End If
        GridView3.DataSource = ""
        'CustTable.Visible = False
        CatchMe = False
        If Request("del_doc_num") & "" = "" And FOLLOW_UP_DT & "" = "" And Request("ord_tp_cd") & "" = "" _
            And Request("cust_cd") & "" = "" And Request("stat_cd") & "" = "" And Request("f_name") & "" = "" _
            And Request("l_name") & "" = "" And Request("h_phone") & "" = "" And Request("bphone") & "" = "" _
            And Request("addr2") & "" = "" And Request("slsp1") & "" = "" And Request("city") & "" = "" _
            And Request("state") & "" = "" And Request("zip") & "" = "" And Request("slsp2") & "" = "" _
            And Request("so_wr_dt") & "" = "" And Request("corp_name") & "" = "" And Request("rel_status") & "" = "" Then
            CatchMe = True
        End If
        GridView3.Visible = True
        If CatchMe = False Then

            Dim str_co_grp_cd As String
            Dim str_co_cd As String
            Dim str_sup_user_flag As String
            Dim str_flag As String
            Dim p_emp_cd As String = Session("emp_cd")
            'str_flag = LeonsBiz.lucy_check_sup_user(UCase(p_emp_cd), str_co_grp_cd, str_co_cd, str_sup_user_flag)
            'March 09 Franchise changes
            str_co_grp_cd = Session("str_co_grp_cd")
            str_co_cd = Session("CO_CD")

            sql = "SELECT NVL(CORP_NAME,LNAME ||', ' || FNAME) AS CORP_NAME, REL_NO, WR_DT, LNAME, CITY, HPHONE, PROSPECT_TOTAL, STORE_CD, REL_STATUS FROM RELATIONSHIP WHERE "

            If Request("del_doc_num") & "" <> "" Then
                If InStr(Request("del_doc_num"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "REL_NO " & LikeQry & " '" & Request("del_doc_num") & "' "
                proceed = True
            End If
            ' Daniela multi company logic
            'Franchise changes
            If str_co_grp_cd & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If str_co_grp_cd = "LEO" Then
                    sql = sql & "store_cd in (select store_cd from store where co_cd in ('" & str_co_cd & "')) "
                Else
                    sql = sql & "store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd= '" & str_co_grp_cd & "')) "
                End If
                proceed = True
            End If
            If IsDate(Request("so_wr_dt")) Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("so_wr_dt"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "WR_DT " & LikeQry & " TO_DATE('" & Request("so_wr_dt") & "','mm/dd/RRRR') "
                proceed = True
            End If
            If IsDate(FOLLOW_UP_DT) Then
                If proceed = True Then sql = sql & "AND "
                If InStr(FOLLOW_UP_DT, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "FOLLOW_UP_DT BETWEEN TO_DATE('" & FOLLOW_UP_DT & " 00:00','mm/dd/yyyy hh24:mi') AND TO_DATE('" & FOLLOW_UP_DT & " 23:59','mm/dd/yyyy hh24:mi') "
                proceed = True
            End If
            If Request("cust_cd") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("cust_cd"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CUST_CD " & LikeQry & " '" & Request("cust_cd") & "' "
                proceed = True
            End If
            If Request("slsp1") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("slsp1"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "SLSP1 " & LikeQry & " '" & Request("slsp1") & "' "
                proceed = True
            End If
            If Request("corp_name") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("corp_name"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CORP_NAME " & LikeQry & " '" & Replace(Request("corp_name"), "'", "''") & "' "
                proceed = True
            End If
            If Request("f_name") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("f_name"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "FNAME " & LikeQry & " '" & Replace(Request("f_name"), "'", "''") & "' "
                proceed = True
            End If
            If Request("l_name") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("l_name"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "LNAME " & LikeQry & " '" & Replace(Request("l_name"), "'", "''") & "' "
                proceed = True
            End If
            If Request("addr1") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("addr1"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ADDR1 " & LikeQry & " '" & Replace(Request("addr1"), "'", "''") & "' "
                proceed = True
            End If
            If Request("addr2") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("addr2"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ADDR2 " & LikeQry & " '" & Replace(Request("addr2"), "'", "''") & "' "
                proceed = True
            End If
            If Request("city") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("city"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CITY " & LikeQry & " '" & Replace(Request("city"), "'", "''") & "' "
                proceed = True
            End If
            If Request("state") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("state"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ST " & LikeQry & " '" & Request("state") & "' "
                proceed = True
            End If
            If Request("zip") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("zip"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "ZIP " & LikeQry & " '" & Request("zip") & "' "
                proceed = True
            End If
            If Request("emp_cd") & "" <> "" And ConfigurationManager.AppSettings("rel_access").ToString = "Y" And Not Is_Manager() Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("emp_cd"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "EMP_CD" & LikeQry & " '" & Request("emp_cd") & "' "
                proceed = True
            End If
            If Request("h_phone") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("h_phone"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "HPHONE " & LikeQry & " '" & Request("h_phone") & "' "
                proceed = True
            End If
            If Request("bphone") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(Request("bphone"), "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "BPHONE " & LikeQry & " '" & Request("bphone") & "' "
                proceed = True
            End If

            If Request("rel_status") & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                sql = sql & " REL_STATUS ='" & Request("rel_status") & "' "
                proceed = True
            End If

            If Right(sql, 6) = "WHERE " Then
                sql = Replace(sql, "WHERE", "")
            End If
            sql = UCase(sql)

            sql = Replace(sql, "*", "%")
            sql = Replace(sql, "SELECT %", "SELECT *")

            conn.Open()

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Dim MyTable As DataTable
            Dim numrows As Integer
            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    If numrows = 1 Then
                        Response.Redirect("Relationship_Maintenance.aspx?del_doc_num=" & MyDataReader.Item("REL_NO") & "&query_returned=Y")
                    Else
                        'GridView1.DataSource = dv
                        'GridView1.DataBind()
                        GridView3.DataSource = dv
                        GridView3.DataBind()
                    End If
                Else
                    GridView3.Visible = False
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Else
            Label1.Text = "No search criteria were found.  Please, <a href=""Relationship_Maintenance.aspx"">search again</a>"
            Label1.Visible = True
        End If
        lbl_pageinfo.Text = "Page " + CStr(GridView3.PageIndex + 1) + " of " + CStr(GridView3.PageCount)
        ' make all the buttons visible if page count is more than 0
        If GridView3.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If GridView3.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView3.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView3.PageIndex = (GridView3.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView3.PageCount = 0 Then
            ' Label1.Text = "Sorry, no relationship records found.  Please, <a href=""Relationship_Maintenance.aspx"">search again</a>"
            Label1.Text = Resources.LibResources.Label773 & Resources.LibResources.Label775 & " <a href=""Relationship_Maintenance.aspx"">" & Resources.LibResources.Label774
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView3.PageIndex < (GridView3.PageCount - 1) Then
                    GridView3.PageIndex += 1
                End If
            Case "Prev"
                If GridView3.PageIndex > 0 Then
                    GridView3.PageIndex -= 1
                End If
            Case "Last"
                GridView3.PageIndex = GridView3.PageCount - 1
            Case Else
                GridView3.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    'Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

    '    Dim row As GridViewRow = GridView1.SelectedRow

    '    ' Display the customer name from the selected row.
    '    Response.Redirect("Relationship_Maintenance.aspx?del_doc_num=" & row.Cells(0).Text & "&query_returned=Y")

    'End Sub
    Public Function Is_Manager() As Boolean

        Is_Manager = False
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim conn As New OracleConnection

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                             ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.EMP_CD='" & UCase(Session("EMP_CD")) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='SLM'"
        SQL = UCase(SQL)

        objSql = New OracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = objSql.ExecuteReader
            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Is_Manager = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function
    Protected Sub btn_search_again_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search_again.Click

        Response.Redirect("Relationship_Maintenance.aspx")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView3_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView3.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If Not IsNothing(e.GetValue("REL_NO").ToString()) Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridView3.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)
            If Not IsNothing(ASPxMenu2) Then
                ASPxMenu2.RootItem.Items(0).NavigateUrl = "Relationship_Maintenance.aspx?del_doc_num=" & e.GetValue("REL_NO").ToString() & "&query_returned=Y"
            End If
        End If

    End Sub
End Class
