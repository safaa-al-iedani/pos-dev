<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Terminal_Setup.aspx.vb" Inherits="Terminal_Setup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
    </div>
    <table width="100%" border="0">
        <tr>
            <td>
                Filter by Store: &nbsp;<asp:DropDownList ID="cbo_filter_store" runat="server" AutoPostBack="True"
                    CssClass="style5" Width="272px">
                </asp:DropDownList>
            </td>
            <td align="right">
                <asp:Button ID="btn_ping" runat="server" CssClass="style5" Text="Show Terminal Status"
                    Width="151px" OnClick="btn_ping_Click" />
            </td>
        </tr>
    </table>
    Add New: &nbsp;&nbsp;<asp:Label ID="lbl_warnings" runat="server" Text="" CssClass="style5"
        Width="579px" ForeColor="Red"></asp:Label><br />
    <asp:TextBox ID="txt_new_IP" runat="server" CssClass="style5" Width="78px">
    </asp:TextBox>
    &nbsp;
    <asp:TextBox ID="txt_new_terminal" runat="server" CssClass="style5" Width="150px">
    </asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="cbo_new_store" runat="server" CssClass="style5" Width="150px"
        AutoPostBack="True">
    </asp:DropDownList>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="cbo_new_cash" runat="server" CssClass="style5" Width="150px">
    </asp:DropDownList>
    &nbsp;
    <asp:Button ID="btn_add" runat="server" Text="Add" CssClass="style5"></asp:Button><br />
    <asp:DataGrid Style="position: relative; top: 9px" ID="Gridview1" runat="server"
        CssClass="style5" Width="668px" Height="156px" Font-Underline="False" Font-Strikeout="False"
        Font-Overline="False" Font-Italic="False" Font-Bold="False" DataKeyField="TERMINAL_IP"
        CellPadding="2" AutoGenerateColumns="False">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
            Font-Bold="True" ForeColor="Black"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="store_cd" ReadOnly="True" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="CASH_DWR_CD" ReadOnly="True" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="terminal_ip" HeaderText="IP Address" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Terminal Description">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" />
                <ItemTemplate>
                    <asp:TextBox ID="terminal_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERMINAL_NAME") %>'
                        Width="150" CssClass="style5"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label573 %>">
                <ItemTemplate>
                    <asp:DropDownList ID="cbo_store_cd" Width="150px" runat="server" DataTextField="store_cd"
                        DataValueField="store_cd" CssClass="style5" AutoPostBack="true" OnTextChanged="Store_Update">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Cash Drawer">
                <ItemTemplate>
                    <asp:DropDownList ID="cbo_csh_dwr" Width="150px" runat="server" DataTextField="CASH_DWR_CD"
                        DataValueField="CASH_DWR_CD" CssClass="style5">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Alive?">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/tick.png" Height="20"
                        Width="20" />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
