<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ptag_Multi_Item_Popup.aspx.vb"
    Inherits="Ptag_Multi_Item_Popup" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multiple Items</title>
</head>
<body>
    <form id="form1" runat="server" >
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="lbl_itm_cd" runat="server" meta:resourcekey="lbl_itm_cd"></dx:ASPxLabel>
                            </td>
                            <td align="left">
                                <dx:ASPxTextBox ID="txt_itm_cd" runat="server" Width="170px">
                                <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btn_add_item" runat="server" meta:resourcekey="lbl_btn_add_item"></dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="3">
                    <dx:ASPxGridView ID="GV_MultiItem" runat="server" AutoGenerateColumns="False" Width="98%"
                        KeyFieldName="itm_No"
                        OnRowDeleting="GV_MultiItem_DeleteRow">
                        <Columns>
                             <dx:GridViewDataTextColumn  FieldName="itm_No" Visible="False" VisibleIndex="1" meta:resourcekey="gv_col_item_code" Width="5%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="itm_code" Visible="True" VisibleIndex="1" meta:resourcekey="gv_col_item_code" Width="25%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="itm_desc" VisibleIndex="2" meta:resourcekey="gv_col_item_description" Width="60%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewCommandColumn  VisibleIndex="3" ShowDeleteButton="true" Width="15%" meta:resourcekey="gv_col_delete">
                            </dx:GridViewCommandColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="center">
                    <dx:ASPxButton ID="btn_done" runat="server"  AutoPostBack="True" ClientInstanceName="btn_done" meta:resourcekey="lbl_done">
                         <ClientSideEvents Click="function(s, e) {          
                             var parentWindow = window.parent;
                             parentWindow.multiitempopupclose();
                          }" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red">
    </dx:ASPxLabel>
    </form>
</body>
</html>
