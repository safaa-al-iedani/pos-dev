<%@ Page Language="VB" AutoEventWireup="false" CodeFile="newmain.aspx.vb" Inherits="newmain"%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome | Main Menu</title>
    <script language="javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        Window.name = "ActiveTab"
    </script>

    <meta http-equiv='cache-control' content='no-cache'/>
    <meta http-equiv='expires' content='0'/>
    <meta http-equiv='pragma' content='no-cache'/>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>&nbsp;
                </td>
                <td width="694" align="left" valign="middle" style="text-align: center">
                    <table width="694" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
                        <tr>
                            <td height="56">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" width="60%">
                                            <div style="padding-left: 8px; padding-top: 8px;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                             <img src="<% = ConfigurationManager.AppSettings("company_logo").ToString %>" />
                                                           <%--Daniela french logo  <img runat="server" src="<%$ Resources:LibResources, Label751 %>" /> --%>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_header" runat="server" Text="<%$ Resources:LibResources, Label515 %>">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="localeLink" runat="server" Text="French" visible="false"/>                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                        </td>
                                        <td align="right" valign="top" style="width: 222px">
                                            <div style="padding-right: 13px; padding-top: 8px">
                                                <dx:ASPxLabel ID="lbl_Header1" runat="server" Text="">
                                                </dx:ASPxLabel>
                                                <br />
                                                <dx:ASPxLabel ID="lbl_Header2" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </div>
                                        </td>
                                        <td align="right" valign="top">
                                            <div style="padding-right: 13px; padding-top: 8px">
                                                <dx:ASPxButton ID="ASPxButton1" runat="server" PostBackUrl="Logout.aspx" Text="<%$ Resources:LibResources, Label273 %>"
                                                    AllowFocus="False">
                                                    <Image Height="25px" Url="~/images/login_icon.gif" Width="25px">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="32" align="center">
                                <%--<dx:ASPxMenu ID="ASPxMenu1" runat="server" Width="100%" AppearAfter="0" 
                                EnableAnimation="False" EnableViewState="False" GutterWidth="0px" 
                                ShowSubMenuShadow="False">
                                    <Items>
                                        <dx:MenuItem Text="Home" NavigateUrl="~/newmain.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Reporting" NavigateUrl="~/Reporting.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Utilities" NavigateUrl="~/Utilities.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                    </Items>
                                </dx:ASPxMenu>--%>
                                <table width="90%">
                                    <tr>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton2" runat="server" PostBackUrl="~/newmain.aspx" 
                                                AllowFocus="False" Width="130px" Text="<%$ Resources:LibResources, Label228 %>">
                                            </dx:ASPxButton>
                                        </td>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton3" runat="server" PostBackUrl="~/Reporting.aspx" Text="<%$ Resources:LibResources, Label469 %>" AllowFocus="False"
                                                Width="130px">
                                            </dx:ASPxButton>
                                        </td>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton4" runat="server" PostBackUrl="~/Utilities.aspx" Text="<%$ Resources:LibResources, Label639 %>"
                                                AllowFocus="False" Width="130px">
                                            </dx:ASPxButton>
                                        </td>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton5" runat="server" PostBackUrl="" Text=""
                                                AllowFocus="False" Width="130px">
                                            </dx:ASPxButton>
                                        </td>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton6" runat="server" PostBackUrl="" Text=""
                                                AllowFocus="False" Width="130px">
                                            </dx:ASPxButton>
                                        </td>
                                        <td width="17%" align="center">
                                            <dx:ASPxButton ID="ASPxButton7" runat="server" PostBackUrl="" Text=""
                                                AllowFocus="False" Width="130px">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <div style="padding-left: 13px; padding-top: 0px">
                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" Width="99%">
                                        <PanelCollection>
                                            <dx:PanelContent runat="server">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td valign="middle" width="8%" align="right">
                                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/startup.aspx?ISSOInitialization=yes">                                                              
                                                                    <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/images/pc-lcd-monitor.png"
                                                                        Height="45px">
                                                                    </dx:ASPxImage>                                                             
                                                            </asp:HyperLink>
                                                        </td>
                                                        <%--<td valign="middle" width="12%">
                                                            <dx:aspxhyperlink id="hyperlink2" runat="server" navigateurl="~/startup.aspx?ISSOInitialization=yes" text="Create" cssclass="dxehyperlink_office2010silver">
                                                            </dx:aspxhyperlink>                                                           
                                                            <br />
                                                            <dx:ASPxHyperLink ID="ASPxHyperLink2" runat="server" NavigateUrl="~/Startup.aspx?ISSOInitialization=yes" Text="Sales Order">
                                                            </dx:ASPxHyperLink>                                                           
                                                        </td>--%>
                                                        <td valign="middle" width="12%">
                                                             <dx:ASPxHyperLink ID="hyperlink2" runat="server" NavigateUrl="~/startup.aspx?ISSOInitialization=yes"
                                                                Text="<%$ Resources:LibResources, Label119 %>">
                                                            </dx:ASPxHyperLink>           
                                                            <dx:ASPxHyperLink ID="ASPxHyperLink2" runat="server" NavigateUrl="~/Startup.aspx?ISSOInitialization=yes" Text="">
                                                            </dx:ASPxHyperLink>                                           
                                                        </td>
                                                        <td valign="middle" width="8%" align="right">
                                                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/SalesorderMaintenance.aspx">
                                                                <dx:ASPxImage ID="ASPxImage2" runat="server" ImageUrl="~/images/My-Documents.png"
                                                                    Height="45px">
                                                                </dx:ASPxImage>
                                                            </asp:HyperLink>
                                                        </td>
                                                        <td valign="middle" width="12%">
                                                            <dx:ASPxHyperLink ID="HyperLink4" runat="server" NavigateUrl="~/SalesorderMaintenance.aspx"
                                                                Text="<%$ Resources:LibResources, Label512 %>">
                                                            </dx:ASPxHyperLink>
                                                        </td>
                                                        <td valign="middle" width="8%" align="right">
                                                            <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/relationship_calendar.aspx">
                                                                <dx:ASPxImage ID="ASPxImage5" runat="server" ImageUrl="~/images/Calendar.png" Height="42px">
                                                                </dx:ASPxImage>
                                                            </asp:HyperLink>
                                                        </td>
                                                        <td valign="middle" width="12%">
                                                            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" NavigateUrl="~/relationship_calendar.aspx"
                                                                Text="<%$ Resources:LibResources, Label455 %>">
                                                            </dx:ASPxHyperLink>
                                                        </td>
                                                        <td valign="middle" width="8%" align="right">
                                                            <asp:HyperLink ID="HyperLink7" NavigateUrl="~/Startup.aspx?LEAD=TRUE&ISSOInitialization=yes" runat="server">                                                                
                                                                <dx:ASPxImage ID="ASPxImage4" runat="server" ImageUrl="~/images/Folder_blue.png"
                                                                    Width="45px">
                                                                </dx:ASPxImage>                                                                   
                                                            </asp:HyperLink>
                                                        </td>
                                                        <td valign="middle" width="12%">
                                                            <dx:ASPxHyperLink ID="HyperLink8" NavigateUrl="~/Startup.aspx?LEAD=TRUE&ISSOInitialization=yes" runat="server"
                                                                Text="<%$ Resources:LibResources, Label118 %>">
                                                            </dx:ASPxHyperLink>                                                          
                                                        </td>
                                                        <td valign="middle" width="8%" align="right">
                                                            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/relationship_maintenance.aspx">
                                                                <dx:ASPxImage ID="ASPxImage3" runat="server" ImageUrl="~/images/Memo.png" Width="45px">
                                                                </dx:ASPxImage>
                                                            </asp:HyperLink>
                                                        </td>
                                                        <td valign="middle" width="12%">
                                                            <dx:ASPxHyperLink ID="HyperLink6" runat="server" NavigateUrl="~/relationship_maintenance.aspx"
                                                                Text="<%$ Resources:LibResources, Label456 %>">
                                                            </dx:ASPxHyperLink>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:PanelContent>
                                        </PanelCollection>
                                        <HeaderTemplate>
                                            &nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label213 %>" />
                                        </HeaderTemplate>
                                        <HeaderStyle Font-Bold="False" />
                                    </dx:ASPxRoundPanel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <br />
                                <table width="694" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="left" valign="top">
                                        <td width="350px" height="100%">
                                            <table width="350px" height="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div style="padding-left: 13px; padding-top: 0px">
                                                            <dx:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="99%" Height="461px">
                                                                <PanelCollection>
                                                                    <dx:PanelContent runat="server">
                                                                        <iframe frameborder="no" marginheight="0" marginwidth="0" src="FrontNews.aspx" style="width: 100%; height: 415px;"></iframe>
                                                                    </dx:PanelContent>
                                                                </PanelCollection>
                                                                <HeaderTemplate>
                                                                    &nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label334 %>" />
                                                                </HeaderTemplate>
                                                                <HeaderStyle Font-Bold="False" />
                                                            </dx:ASPxRoundPanel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="460" height="100%">
                                            <table width="460" height="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table width="460" border="0" cellspacing="0" cellpadding="0">
                                                            <tr align="left" valign="top">
                                                                <td>
                                                                    <table border="0" cellspacing="0" cellpadding="0" style="width: 490px">
                                                                        <tr>
                                                                            <td align="left" valign="top" style="width: 100%">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td height="22" align="left" valign="top">
                                                                                            <div style="padding-left: 5px;">
                                                                                                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="99%">
                                                                                                    <PanelCollection>
                                                                                                        <dx:PanelContent runat="server">
                                                                                                            <div style="padding-left: 8px; padding-top: 7px; padding-bottom: 5px">
                                                                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                                                    <tr align="left" valign="top">
                                                                                                                        <td>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="Sales_Top" runat="server">
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:aspxhyperlink id="hpl_sls_order" navigateurl="startup.aspx?ISSOInitialization=yes" runat="server" Text="<%$ Resources:LibResources, Label119 %>"
                                                                                                                                            visible="true">
                                                                                                                                        </dx:aspxhyperlink>                                                                                                                                      
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <%--Daniela <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_cash_carry" NavigateUrl="CashCarrySale.aspx" runat="server"
                                                                                                                                             Text="Create &quot;Take With&quot; Sales Order" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>--%>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_cash_carry" NavigateUrl="CashCarrySale.aspx" runat="server"
                                                                                                                                             Text="<%$ Resources:LibResources, Label117 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                         <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_cash_carry_express" NavigateUrl="CashCarryExpress.aspx" runat="server"
                                                                                                                                            Text="Cash and Carry Express" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px;">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink5" NavigateUrl="SalesOrderMaintenance.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label512 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink6" NavigateUrl="Main_Customer.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label137 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink7" NavigateUrl="CustomerInquiry.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label8 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">-->                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink8" NavigateUrl="Invoices/Invoice.aspx" runat="server"
                                                                                                                                            Text="Print Invoice" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->

                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink3" NavigateUrl="SalesExchange.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label185 %>" Visible="true" Enabled="True">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink32" NavigateUrl="InventoryAvailable.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label41 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="8">&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td height="8">&nbsp;
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="CRM_Top" runat="server">
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink9" NavigateUrl="Startup.aspx?LEAD=TRUE&ISSOInitialization=yes" runat="server"
                                                                                                                                           Text="<%$ Resources:LibResources, Label118 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>                                                                                                                                        
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink10" NavigateUrl="Relationship_Maintenance.aspx"
                                                                                                                                            runat="server" Text="<%$ Resources:LibResources, Label456 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink11" NavigateUrl="Relationship_Calendar.aspx"
                                                                                                                                            runat="server" Text="<%$ Resources:LibResources, Label455 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="8">&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td height="8">&nbsp;
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="Payment" runat="server">
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink13" NavigateUrl="IndependentPaymentProcessing.aspx"
                                                                                                                                            runat="server" Text="<%$ Resources:LibResources, Label386 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                              
                                                                                                                               <%--Daniela move to Leons Custom <%--sabrina add Payment Reversal --%>
                                                                                                                               <%-- <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink34" NavigateUrl="paymentreversal.aspx"
                                                                                                                                            runat="server" Text="Brick Payment Reversal" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>--%>--%>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink14" NavigateUrl="newmain.aspx?payment_hold=TRUE"
                                                                                                                                            runat="server" Text="Payment Hold" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->    
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                         <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink33" NavigateUrl="settlement.aspx" runat="server"
                                                                                                                                            Text="PL Settlement" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="Table1" runat="server">
                                                                                                                                <tr runat="server">
                                                                                                                                    <td width="12" height="16" id="Td1">&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td height="16" id="Td2">&nbsp;
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr runat="server">
                                                                                                                                    <td width="12" height="16" id="Td3">
                                                                                                                         <!-- PY 15Sep2014, As per req from Dave
                                                                                                                         <img src="images/point_1.jpg" width="6" height="5"> -->                                                                                                                                    </td>
                                                                                                                                    <td height="16" id="Td4">
<!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink29" NavigateUrl="price_tag.aspx" runat="server"
                                                                                                                                            Text="Price Tags" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="Sales_Top_Right" runat="server">
                                                                                                                                <%--Daniela moved up  --%>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink36" NavigateUrl="LeonsCustom.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label141 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                  <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                 </td>
                                                                                                                                    <td height="16">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <%-- <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"></td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_poa" NavigateUrl="PaymentonAccount.aspx" runat="server"
                                                                                                                                            Text="Payment on Account" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>--%>
                                                                                                                                <%--<tr align="left" valign="middle">--%>
                                                                                                                                   <%-- <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>--%>
                                                                                                                                    <%-- DB moved external links to Customization --%>
                                                                                                                                <%-- DB link to external page --%>
                                                                                                                                  <%--  <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_credit_app" NavigateUrl="ElectronicCreditApp.aspx"
                                                                                                                                            runat="server" Text="<%$ Resources:LibResources, Label127 %>" Visible="true" Target="_blank">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>  --%>                                                                                                                                  
                                                                                                                                <%--</tr>--%>
                                                                                                                                <%--<tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                       <asp:Image runat="server" src="images/point_1.jpg" width="6" height="5" visible="false" id="img1"/>
                                                                                                                                    </td>--%>
                                                                                                                                <%-- DB link to external page --%>
                                                                                                                               <%--     <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_ez_finance" NavigateUrl="http://leons-training-portal-uat.herokuapp.com"
                                                                                                                                            runat="server" Text="Easy Finance" Visible="false" Target="_blank">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>                                                                                                                                                                                                                                                                 
                                                                                                                                </tr>--%>
                                                                                                                                 <%-- sabrina - add inventory check --%>
                                                                                                                                <%-- DB moved external links to Customization --%>
                                                                                                                                 <%--<tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLinksy" NavigateUrl="http://10.60.99.10/mobile/"
                                                                                                                                            runat="server" Text="<%$ Resources:LibResources, Label246 %>" Visible="true" Target="_blank">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                 </tr>--%>
                                                                                                                                <%--<tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                               <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                         <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_otb" NavigateUrl="newmain.aspx?otb_app=TRUE" runat="server"
                                                                                                                                            Text="Open To Buy" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>--%>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink15" NavigateUrl="SalesRewrite.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label514 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                         <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink16" NavigateUrl="CashCarrySale.aspx?swatch=TRUE"
                                                                                                                                            runat="server" Text="Swatch Check Out" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_saved_orders" NavigateUrl="restore_saved_order.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label523 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="hplSBOB" NavigateUrl="SalesBookOfBusiness.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label508 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="Shared_Functions" runat="server">
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_inventory" NavigateUrl="InventoryLookup.aspx?referrer=MAINMENU" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label247 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="hpl_csha" NavigateUrl="cashieractivation.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label66 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink17" NavigateUrl="Delivery.aspx?D=D" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label152 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" style="height: 16px">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 16px">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink18" NavigateUrl="Discount_Lookup.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label167 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                   <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink19" NavigateUrl="InventoryComparison.aspx" runat="server"
                                                                                                                                            Text="Merchandise Compare" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink30" NavigateUrl="IST.aspx" runat="server" Text="<%$ Resources:LibResources, Label242 %>"
                                                                                                                                            Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink31" NavigateUrl="PO.aspx" runat="server" Text="<%$ Resources:LibResources, Label443 %>"
                                                                                                                                            Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                              <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- PY 15Sep2014, As per req from Dave
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink21" NavigateUrl="File_Manager/index.aspx" runat="server"
                                                                                                                                            Text="File Manager" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img alt="CDB" src="images/point_1.jpg" width="6" height="5" />
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink26" NavigateUrl="CashDrawerBalancing.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label63 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td height="16">&nbsp;
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0" id="gift_card" runat="server">
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink4" NavigateUrl="WGC_Balance.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label220 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                 <%-- sabrina add gift card blance transfer --%>
                                                                                                                                 <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink8a" NavigateUrl="WGC_transfer.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label691 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
 <%-- sabrina add gift card blance transfer end --%>
                                                                                                                                <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <!-- DB 21Oct2014, Not needed
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5"> -->
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <!-- DB 21Oct2014, Not needed
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink12" NavigateUrl="WGC_AddValue.aspx" runat="server"
                                                                                                                                            Text="Gift Card Add Value" Visible="true"> 
                                                                                                                                        </dx:ASPxHyperLink> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                 <tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink35" NavigateUrl="WGC_PurchaseGiftCard.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label440 %>" Visible="true">                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                 <!-- DB Jan 07, 2015, moved up-->
                                                                                                                                <%--<tr align="left" valign="middle">
                                                                                                                                    <td width="12" height="16">
                                                                                                                                        <img src="images/point_1.jpg" width="6" height="5">
                                                                                                                                    </td>
                                                                                                                                    <td height="16">
                                                                                                                                        <dx:ASPxHyperLink ID="ASPxHyperLink36" NavigateUrl="LeonsCustom.aspx" runat="server"
                                                                                                                                            Text="<%$ Resources:LibResources, Label141 %>" Visible="true">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>--%>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </dx:PanelContent>
                                                                                                    </PanelCollection>
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label142 %>" />
                                                                                                    </HeaderTemplate>
                                                                                                    <HeaderStyle Font-Bold="False" />
                                                                                                </dx:ASPxRoundPanel>
                                                                                                &nbsp;
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="6" align="left" valign="top">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="100%" align="left" valign="top">
                                                        <table width="491" height="100%" border="0" cellpadding="0" cellspacing="0" id="CRM_Summary"
                                                            runat="server">
                                                            <tr>
                                                                <td height="22" align="left" valign="middle">
                                                                    <div style="padding-left: 5px; padding-top: 2px">
                                                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="99%">
                                                                            <PanelCollection>
                                                                                <dx:PanelContent runat="server">
                                                                                    <div style="padding-left: 0px; padding-top: 10px" id="main_body" runat="server">
                                                                                    </div>
                                                                                </dx:PanelContent>
                                                                            </PanelCollection>
                                                                            <HeaderTemplate>
                                                                                &nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label460 %>" />
                                                                            </HeaderTemplate>
                                                                            <HeaderStyle Font-Bold="False" />
                                                                        </dx:ASPxRoundPanel>
                                                                    </div>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                              <%-- Daniela commented   <table width="491" height="100%" border="0" cellpadding="0" cellspacing="0" id="Kiosk_Summary"
                                                            runat="server">
                                                            <tr>
                                                                <td height="22" align="left" valign="middle">
                                                                    <div style="padding-left: 13px; padding-top: 2px">
                                                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="99%">
                                                                            <PanelCollection>
                                                                                <dx:PanelContent runat="server">
                                                                                    <iframe class="style5" src="KioskMain.aspx" style="width: 99%" frameborder="0"></iframe>
                                                                                </dx:PanelContent>
                                                                            </PanelCollection>
                                                                            <HeaderTemplate>
                                                                                &nbsp;Kiosk Summary
                                                                            </HeaderTemplate>
                                                                            <HeaderStyle Font-Bold="False" />
                                                                        </dx:ASPxRoundPanel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            &nbsp;
                                                        </table>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="46" align="center" valign="top">
                                <div style="padding-left: 0px; padding-top: 18px">
                                    <dx:ASPxHyperLink ID="ASPxHyperLink22" NavigateUrl="newmain.aspx" runat="server"
                                        Text="<%$ Resources:LibResources, Label228 %>">
                                    </dx:ASPxHyperLink>
                                    &nbsp;:&nbsp;<dx:ASPxHyperLink ID="ASPxHyperLink23" NavigateUrl="reporting.aspx"
                                        runat="server" Text="<%$ Resources:LibResources, Label469 %>" Visible="false">
                                    </dx:ASPxHyperLink>
                                    &nbsp;:&nbsp;<dx:ASPxHyperLink ID="ASPxHyperLink24" NavigateUrl="utilities.aspx"
                                        runat="server" Text="<%$ Resources:LibResources, Label639 %>" Visible="false">
                                    </dx:ASPxHyperLink>
                                    &nbsp;:&nbsp;<dx:ASPxHyperLink ID="ASPxHyperLink25" NavigateUrl="merch_images\POS_Plus_UserGuide.pdf"
                                        runat="server" Text="<%$ Resources:LibResources, Label637 %>" Target="_new">
                                    </dx:ASPxHyperLink>
                                    &nbsp;:&nbsp;<dx:ASPxHyperLink ID="ASPxHyperLink27" NavigateUrl="logout.aspx"
                                        runat="server" Text="<%$ Resources:LibResources, Label273 %>">
                                    </dx:ASPxHyperLink>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="center" valign="middle">
                                <dx:ASPxHyperLink ID="ASPxHyperLink28" NavigateUrl="http://www.redprairie.com" runat="server"
                                    Text="Copyright
                                    &copy; 2011-2013 RedPrairie">
                                </dx:ASPxHyperLink>
                                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <dx:ASPxLabel ID="lbl_version" runat="server">
                            </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
                <td width="694" align="left" valign="middle">&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
            HeaderText="<%$ Resources:LibResources, Label524 %>" Height="300px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="550px" ClientInstanceName="ASPxPopupControl1">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>

        <br />
        <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
            HeaderText="Terminal Locked!" Height="196px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="425px" ClientInstanceName="ASPxPopupControl1">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <table width="100%">
                        <tr>
                            <td width="50%" align="left" style="text-align: center">
                                <img src="images/icons/Symbol-Exclamation.png" style="width: 171px; height: 171px" />
                            </td>
                            <td class="style5">This terminal has been closed for the day. You will not be able to process payments
                                or sales on this terminal.
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl13" runat="server" CloseAction="CloseButton"
            HeaderText="<%$ Resources:LibResources, Label361 %>" Height="400px" PopupAction="None"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="499px"
            ClientInstanceName="ASPxPopupControl13"
            ContentUrl="~/OTB.aspx">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" CloseAction="CloseButton"
            HeaderText="Payment Warnings" Height="475px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="608px" ContentUrl="~/Payment_Hold.aspx"
            AllowDragging="True">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
    </form>
</body>
</html>
