Imports System.Data
Imports System.Data.OracleClient
Imports DevExpress.XtraScheduler
Imports DevExpress.Web.ASPxScheduler

Partial Class Relationship_Calendar
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            store_cd_populate()
            cbo_store.SelectedItem = cbo_store.Items.FindByValue(Session("HOME_STORE_CD"))
            emp_cd_populate()
            cbo_slsp.SelectedItem = cbo_slsp.Items.FindByValue(UCase(Session("EMP_CD")))
            If Not Is_Manager() Then
                cbo_store.Enabled = False
                cbo_slsp.Enabled = False
            End If
            If ConfigurationManager.AppSettings("rel_access") = "N" Then
                cbo_store.Enabled = True
                cbo_slsp.Enabled = True
            End If
            ASPxScheduler1.Start = Today.Date.AddDays(-14)
        End If

    End Sub

    Private ReadOnly Property Storage() As ASPxSchedulerStorage
        Get
            Return Me.ASPxScheduler1.Storage
        End Get
    End Property

    Public Shared RandomInstance As New Random()

#Region "#appointmentformshowing"

    Protected Sub ASPxScheduler1_AppointmentFormShowing(ByVal sender As Object, ByVal e As AppointmentFormEventArgs)
        e.Container = New UserAppointmentFormTemplateContainer(CType(sender, ASPxScheduler))

    End Sub

#End Region ' #appointmentformshowing

    Protected Sub ASPxScheduler1_AppointmentInserting(ByVal sender As Object, _
                ByVal e As PersistentObjectCancelEventArgs)
        Dim storage As ASPxSchedulerStorage = CType(sender, ASPxSchedulerStorage)
        Dim apt As Appointment = CType(e.Object, Appointment)
        storage.SetAppointmentId(apt, "a" & apt.GetHashCode())
    End Sub

#Region "#beforeexecutecallbackcommand"

    Protected Sub ASPxScheduler1_BeforeExecuteCallbackCommand(ByVal sender As Object, ByVal e As SchedulerCallbackCommandEventArgs)
        If e.CommandId = SchedulerCallbackCommandId.AppointmentSave Then
            e.Command = New UserAppointmentSaveCallbackCommand(CType(sender, ASPxScheduler))
        End If
    End Sub

#End Region ' #beforeexecutecallbackcommand


    Public Function Is_Manager() As Boolean

        Is_Manager = False
        Dim SQL As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                             ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT EMP.HOME_STORE_CD, EMP.FNAME, EMP.LNAME, EMP.EMP_CD FROM EMP, EMP$EMP_TP WHERE "
        SQL = SQL & "EMP.EMP_CD='" & UCase(Session("EMP_CD")) & "' "
        SQL = SQL & "AND EMP$EMP_TP.EMP_CD = EMP.EMP_CD AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Date.Today, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP$EMP_TP.EMP_TP_CD='SLM'"
        SQL = UCase(SQL)

        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Is_Manager = True
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        ' MCCL and Franchise added
        'SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"
        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store WHERE "
        If isNotEmpty(Session("str_co_grp_cd")) Then
            'Feb 23 add MCCL logic Franchise
            Dim str_co_grp_cd As String = Session("str_co_grp_cd")
            If str_co_grp_cd = "BRK" Then   '  the Brick 
                SQL = SQL & " store_cd in (select store_cd from store where co_cd in (select co_cd from co_grp where co_grp_cd= '" & str_co_grp_cd & "' )) "
            Else
                SQL = SQL & " store_cd in (select store_cd from store where co_cd = nvl('" & Session("CO_CD") & "', co_cd)) "
            End If
        End If
        SQL = SQL & " order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub emp_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT EMP_CD, LNAME || ', ' || FNAME as full_desc FROM EMP WHERE TERMDATE IS NULL AND HOME_STORE_CD='" & cbo_store.Value & "' order by LNAME, FNAME"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_slsp
                .DataSource = ds
                .ValueField = "EMP_CD"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub cbo_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_store.SelectedIndexChanged

        emp_cd_populate()
        Try
            cbo_slsp.SelectedIndex = 0
        Catch
        End Try

    End Sub
End Class
