Imports System.Data.OracleClient
Imports HBCG_Utils
Imports AppExtensions

Partial Class Lead_Stage
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Sub Create_Customer()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim sql2 As String
        Dim objSql As OracleCommand
        Dim corp_name As String = ""
        Dim fname As String = ""
        Dim lname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim city As String = ""
        Dim state As String = ""
        Dim zip As String = ""
        Dim hphone As String = ""
        Dim bphone As String = ""
        Dim cust_tp_cd As String = ""
        Dim email As String = ""
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader

        sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
        'Open Connection 
        conn2.Open()

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then
                fname = MyDataReader2.Item("FNAME").ToString.Trim
                lname = MyDataReader2.Item("LNAME").ToString.Trim
                cust_tp_cd = MyDataReader2.Item("CUST_TP").ToString.Trim
                addr1 = MyDataReader2.Item("ADDR1").ToString.Trim
                addr2 = MyDataReader2.Item("ADDR2").ToString.Trim
                city = MyDataReader2.Item("CITY").ToString.Trim
                state = MyDataReader2.Item("ST").ToString.Trim
                zip = MyDataReader2.Item("ZIP").ToString.Trim
                hphone = StringUtils.FormatPhoneNumber(MyDataReader2.Item("HPHONE").ToString.Trim)
                bphone = StringUtils.FormatPhoneNumber(MyDataReader2.Item("BPHONE").ToString.Trim)
                email = MyDataReader2.Item("EMAIL").ToString.Trim
                corp_name = MyDataReader2.Item("CORP_NAME").ToString.Trim
            End If

            'Close Connection 
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        If Session("cust_cd") = "NEW" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"), addr1, Left(UCase(lname), 20),
                                                      Left(UCase(fname), 15))

            sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, CUST_TP_CD, FNAME, LNAME "
            sql2 = "VALUES('" & Session("CUST_CD").ToString.Trim & "',TO_DATE('" & FormatDateTime(Now, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
            sql2 = sql2 & "'" & cust_tp_cd & "','" & fname & "','" & lname & "'"
            If corp_name & "" <> "" Then
                sql = sql & ",CORP_NAME"
                sql2 = sql2 & ",'" & corp_name & "'"
            End If
            If addr1 & "" <> "" Then
                sql = sql & ",ADDR1"
                sql2 = sql2 & ",'" & addr1 & "'"
            End If
            If addr2 & "" <> "" Then
                sql = sql & ",ADDR2"
                sql2 = sql2 & ",'" & addr2 & "'"
            End If
            If city & "" <> "" Then
                sql = sql & ",CITY"
                sql2 = sql2 & ",'" & city & "'"
            End If
            If state & "" <> "" Then
                sql = sql & ",ST_CD"
                sql2 = sql2 & ",'" & state & "'"
            End If
            If zip & "" <> "" Then
                sql = sql & ",ZIP_CD"
                sql2 = sql2 & ",'" & zip & "'"
            End If
            If hphone & "" <> "" Then
                sql = sql & ",HOME_PHONE"
                sql2 = sql2 & ",'" & hphone & "'"
            End If
            If bphone & "" <> "" Then
                sql = sql & ",BUS_PHONE"
                sql2 = sql2 & ",'" & bphone & "'"
            End If
            If email & "" <> "" Then
                sql = sql & ",EMAIL_ADDR"
                sql2 = sql2 & ",'" & email & "'"
            End If
            sql = sql & ")" & sql2 & ")"
            sql = UCase(sql)
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                conn.Open()
                objSql.ExecuteNonQuery()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                conn2.Close()
                Throw
            End Try

            'Update the local record to allow for insert into the HBCG database
            sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "'"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            objsql2.ExecuteNonQuery()

        End If
        conn2.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim custcd As String = ""
            Dim summary_txt As String = ""

            custcd = Session("CUST_CD")

            If custcd & "" <> "" Then
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

                sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & custcd & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        summary_txt = Resources.LibResources.Label677 & " " & Resources.LibResources.Label407 & "<br /><br />"
                        If MyDataReader.Item("CORP_NAME").ToString & "" <> "" Then
                            summary_txt = summary_txt & MyDataReader.Item("CORP_NAME").ToString & "<br />"
                        End If
                        summary_txt = summary_txt & MyDataReader.Item("FNAME").ToString & " " & MyDataReader.Item("LNAME").ToString & "<br />"
                        summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("ADDR1").ToString & "<br />"
                        summary_txt = summary_txt & "&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("CITY").ToString & ", " & MyDataReader.Item("ST").ToString & "   " & MyDataReader.Item("ZIP").ToString & "<br />"
                        summary_txt = summary_txt & Resources.LibResources.Label999 & MyDataReader.Item("HPHONE").ToString & "<br />"  ' lucy 23-Jul-18
                        summary_txt = summary_txt & "(C) " & MyDataReader.Item("BPHONE").ToString & "<br />"
                        If String.IsNullOrEmpty(MyDataReader.Item("EMAIL").ToString) And ConfigurationManager.AppSettings("email_prompt").ToString = "Y" Then
                            ASPxPopupControl2.ShowOnPageLoad = True
                        Else
                            summary_txt = summary_txt & MyDataReader.Item("EMAIL").ToString & "<br />"
                        End If
                        If String.IsNullOrEmpty(MyDataReader.Item("FNAME").ToString) Or String.IsNullOrEmpty(MyDataReader.Item("LNAME").ToString) Then
                            summary_txt = summary_txt & "You must enter a customer first and last name." & vbCrLf
                            btn_commit.Enabled = False
                            CheckBox1.Enabled = False
                        End If
                    End If

                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                conn.Dispose()
            Else
                'Daniela french
                'summary_txt = summary_txt & "<font color=red><b>You must select or create a customer to continue.</b></font><br />"
                summary_txt = summary_txt & "<font color=red><b>" & Resources.LibResources.Label679 & "</b></font><br />"
                btn_commit.Enabled = False
                CheckBox1.Enabled = False
            End If
            If Session("tran_dt") & "" = "" Then
                summary_txt = summary_txt & "<font color=red><b>A follow-up date is required.</b></font><br />"
            End If
            If Session("slsp1") & "" = "" Then
                'Daniela french
                'summary_txt = summary_txt & "<font color=red><b>A valid salesperson is required.</b></font><br />"
                summary_txt = summary_txt & "<font color=red><b>" & Resources.LibResources.Label39 & "</b></font><br />"
            End If
            If Session("store_cd") & "" = "" Then
                summary_txt = summary_txt & "<font color=red><b>A valid store code is required.</b></font><br />"
            End If
            lbl_Summary.Text = summary_txt

            Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objsql2 As OracleCommand
            Dim MyDatareader2 As OracleDataReader

            sql = "SELECT SUM(QTY) FROM TEMP_ITM WHERE session_id='" & Session.SessionID.ToString() & "' AND ITM_TP_CD='INV'"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Open Connection 
                conn2.Open()
                'Execute DataReader 
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If (MyDatareader2.Read()) Then
                    If MyDatareader2(0).ToString & "" <> "" Then
                        ' lbl_itm_summary.Text = MyDatareader2(0).ToString & " item(s) have been included in this relationship."
                        lbl_itm_summary.Text = MyDatareader2(0).ToString & " " & Resources.LibResources.Label768
                    Else
                        lbl_itm_summary.Text = Resources.LibResources.Label346
                    End If
                Else
                    lbl_itm_summary.Text = Resources.LibResources.Label346
                End If

                'Close Connection 
                MyDatareader2.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try
        End If

        ASPxPopupControl1.ContentUrl = "Email_Templates.aspx?ord_tp_cd=SAL&slsp1=" & Session("slsp1") & "&slsp2=" & Session("slsp2") & "&cust_cd=" & Session("cust_cd")

    End Sub

    Protected Sub btn_commit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_commit.Click

        If Session("cust_cd") = "NEW" Then
            Create_Customer()
        End If

        Dim x As Integer
        Dim DateString As String = ""
        Dim REL_NO As String = ""
        Dim GOOD_REL As Boolean
        x = 0

        btn_commit.Enabled = False
        CheckBox1.Enabled = False
        chk_email.Enabled = False
        ImageButton1.Enabled = False

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim MyDatareader2 As OracleDataReader
        Dim sql As String

        conn.Open()

        GOOD_REL = False
        Do While GOOD_REL = False
            x = x + 1
            DateString = XDigits(Today.Month.ToString, 2) & XDigits(Today.Day.ToString, 2) & Right(Today.Year, 1)
            REL_NO = DateString & Session("store_cd").ToString & XDigits(x.ToString, 4)

            sql = "SELECT REL_NO FROM RELATIONSHIP WHERE REL_NO='" & REL_NO & "'"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If (MyDatareader.Read()) Then
                    GOOD_REL = False
                Else
                    'Added DISC_CD
                    Dim discCD As String = If(String.IsNullOrEmpty(SessVar.discCd), String.Empty, SessVar.discCd)
                    GOOD_REL = True
                    sql = "INSERT INTO RELATIONSHIP (REL_NO, EMP_CD, STORE_CD, SLSP1, WR_DT,DISC_CD ) VALUES('" & REL_NO & "',"
                    sql = sql & "'" & Session("emp_cd").ToString & "','" & Session("store_cd").ToString & "','" & Session("slsp1").ToString & "'"
                    sql = sql & ",TO_DATE('" & Today.Date & "','mm/dd/RRRR'), '" & discCD & "' )"

                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql.ExecuteNonQuery()
                    'Daniela french
                    'lbl_Status.Text = "<br /><b>Created relationship " & REL_NO & "</b>"
                    lbl_Status.Text = "<br /><b>" & Resources.LibResources.Label120 & " " & REL_NO & "</b>"

                    theSystemBiz.SaveAuditLogComment("RELATIONSHIP_CREATED", REL_NO & " was created via Relationship Entry")

                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Loop

        Dim sub_total, delivery, setup, discount, tax, grand_total, del_chg, del_tax, setup_tax As Double
        Dim ttax As String = ""
        Dim ttax_cd As String = ""

        sql = "SELECT NVL(sum(qty*ret_prc),0) as ord_sum, NVL(sum(qty*(taxable_amt*(tax_pct/100))),0) as tax_sum FROM TEMP_ITM WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND TAKE_WITH IS NULL"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If (MyDatareader.Read()) Then
                If IsNumeric(MyDatareader.Item("tax_sum").ToString) Then
                    ttax = MyDatareader.Item("tax_sum").ToString
                    ttax_cd = Session("tax_cd").ToString
                End If
            End If
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If IsNumeric(Session("DEL_CHG")) Then
            del_chg = CDbl(Session("DEL_CHG"))
            If IsNumeric(Session("DEL_TAX")) And IsNumeric(Session("TAX_RATE")) Then
                del_tax = FormatNumber(CDbl(Session("DEL_CHG")) * (Session("DEL_TAX") / 100), 2)
            End If
        Else
            del_chg = FormatNumber(0, 2)
        End If
        ' TODO - this is a mess  - del_chg vs delivery - format and reformat  ???- is double, CDbl
        If IsNumeric(Session("SETUP_CHG")) Then
            setup = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
            If IsNumeric(Session("SU_TAX")) And IsNumeric(Session("TAX_RATE")) Then
                setup_tax = FormatNumber(CDbl(Session("SETUP_CHG")) * (Session("SU_TAX") / 100), 2)
            End If
        Else
            setup = FormatNumber(0, 2)
        End If
        If Not IsNumeric(ttax) Then ttax = 0
        ttax = ttax + del_tax + setup_tax
        If Session("sub_total") & "" <> "" Then
            sub_total = FormatCurrency(CDbl(Session("sub_total")))
            If IsNumeric(Session("DEL_CHG")) Then
                delivery = FormatNumber(CDbl(Session("DEL_CHG")), 2)
            Else
                delivery = FormatNumber(0, 2)
            End If
            If IsNumeric(ttax) Then
                tax = CDbl(ttax)
            Else
                tax = 0
            End If

            If Session("tet_cd") & "" <> "" Then
                tax = 0
            ElseIf ConfigurationManager.AppSettings("tax_line").ToString = "N" Then  ' header taxing
                ' TODO - someday when LeadStage is also single transaction, this can be pulled and just pass atomic command to the tax calc
                Try
                    tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), Session("tran_dt"), setup, del_chg, OrderUtils.tempItmSelection.All)
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
                'tax = OrderUtils.Calc_Hdr_Tax_From_Temp(Session.SessionID.ToString.Trim, Session("TAX_CD"), Session("tran_dt"), setup, del_chg, OrderUtils.tempItmSelection.All)
            End If

            grand_total = FormatCurrency((CDbl(Session("sub_total")) - CDbl(discount)) + FormatNumber(CDbl(tax)) + FormatNumber(CDbl(delivery)), 2)
        End If

        Dim corp_name As String = ""
        Dim fname As String = ""
        Dim lname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim city As String = ""
        Dim state As String = ""
        Dim zip As String = ""
        Dim hphone As String = ""
        Dim bphone As String = ""
        Dim email As String = ""

        sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If (MyDatareader.Read()) Then
                corp_name = MyDatareader.Item("CORP_NAME").ToString.Trim
                fname = MyDatareader.Item("FNAME").ToString.Trim
                lname = MyDatareader.Item("LNAME").ToString.Trim
                addr1 = MyDatareader.Item("ADDR1").ToString.Trim
                addr2 = MyDatareader.Item("ADDR2").ToString.Trim
                city = MyDatareader.Item("CITY").ToString.Trim
                state = MyDatareader.Item("ST").ToString.Trim
                zip = MyDatareader.Item("ZIP").ToString.Trim
                hphone = StringUtils.FormatPhoneNumber(MyDatareader.Item("HPHONE").ToString.Trim)
                bphone = StringUtils.FormatPhoneNumber(MyDatareader.Item("BPHONE").ToString.Trim)
                email = MyDatareader.Item("EMAIL").ToString.Trim
            End If

            'Close Connection 
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Not String.IsNullOrEmpty(email) Then
            sql = "UPDATE CUST SET EMAIL_ADDR='" & email & "' WHERE CUST_CD='" & Session("cust_cd") & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.ExecuteNonQuery()
        End If

        sql = "UPDATE RELATIONSHIP SET P_D='" & Session("PD") & "',"
        sql = sql & "TAX_CHG=" & CDbl(tax.ToString) & ","
        sql = sql & "TAX_CD='" & Session("TAX_CD") & "',"
        sql = sql & "CORP_NAME='" & Replace(corp_name, "'", "''") & "',"
        sql = sql & "FNAME='" & Replace(fname, "'", "''") & "',"
        sql = sql & "LNAME='" & Replace(lname, "'", "''") & "',"
        sql = sql & "ADDR1='" & Replace(addr1, "'", "''") & "',"
        sql = sql & "ADDR2='" & Replace(addr2, "'", "''") & "',"
        sql = sql & "CITY='" & Replace(city, "'", "''") & "',"
        sql = sql & "ST='" & state & "',"
        sql = sql & "ZIP='" & zip & "',"
        sql = sql & "HPHONE='" & hphone & "',"
        sql = sql & "BPHONE='" & bphone & "',"
        sql = sql & "EMAIL_ADDR='" & email & "',"
        If Session("tet_cd") & "" <> "" Then
            sql = sql & "TET_CD='" & Session("TET_CD") & "',"
            If Session("exempt_id") & "" <> "" Then
                sql = sql & "EXEMPT_ID='" & Session("exempt_id") & "',"
            End If
        ElseIf IsNumeric(Session("TAX_RATE")) Then
            sql = sql & "TAX_RATE=" & Session("TAX_RATE") & ","
        End If
        If IsDate(Session("tran_dt")) Then
            Session("tran_dt") = FormatDateTime(Session("tran_dt"), DateFormat.ShortDate)
        Else
            Session("tran_dt") = FormatDateTime(Today.Date, DateFormat.ShortDate)
        End If
        sql = sql & "FOLLOW_UP_DT=TO_DATE('" & Session("tran_dt") & "','mm/dd/RRRR'),"
        sql = sql & "ZONE_CD='" & Session("ZONE_CD") & "',"
        sql = sql & "CUST_CD='" & Session("CUST_CD") & "',"
        If Session("appt_time") & "" <> "" Then
            sql = sql & "APPT_TIME='" & Session("appt_time") & Session("am_or_pm") & "',"
            sql = sql & "APPT_TYPE='" & Session("call_type") & "',"
        End If
        sql = sql & "DEL_CHG=" & CDbl(delivery.ToString) & ","
        sql = sql & "PROSPECT_TOTAL=" & CDbl(grand_total.ToString) & ","
        sql = sql & "DISCOUNTS=" & CDbl(discount.ToString) & ", "
        sql = sql & "COMMENTS='" & Left(Replace(Session("scomments"), "'", "''"), 254) & "' "
        sql = sql & "WHERE REL_NO='" & REL_NO & "'"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql.ExecuteNonQuery()

        ' EXTRACT TEMP_ITM information for storage
        sql = "SELECT * FROM temp_itm where session_id='" & Session.SessionID.ToString.Trim & "' order by row_id"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim relLnNum As Integer = 1
        Dim firstRowId As Integer = 0  ' used to create offset for warranty line pointers; will only work as long as order is by row_id asc
        Dim sqlSb As New StringBuilder
        Dim sqlValSb As New StringBuilder
        Dim tmpItmDatset As New DataSet
        Dim where As String = ""
        Dim tmpItm() As DataRow    ' internal SQL result - array of datarows
        Dim offSet As Integer = 0

        Try
            Dim oadp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objsql)
            oadp.Fill(tmpItmDatset)
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Dim pkgParentLine As String = String.Empty

            Do While MyDatareader.Read()
                If firstRowId = 0 Then
                    firstRowId = CInt(MyDatareader.Item("ROW_ID").ToString)
                End If

                sqlSb.Clear()
                sqlValSb.Clear()
                sqlSb.Append("INSERT INTO RELATIONSHIP_LINES (REL_NO, LINE, ITM_CD, QTY, RET_PRC, TREATED, VE_CD, VSN, DES, ITM_TP_CD, TREATABLE, PRC_CHG_APP_CD ")

                If IsDBNull(MyDatareader.Item("PACKAGE_PARENT")) Then
                    pkgParentLine = String.Empty
                ElseIf Not String.IsNullOrEmpty(pkgParentLine) Then
                    sqlValSb.Append(",PACKAGE_PARENT ")
                End If
                sqlValSb.Append(",DISC_AMT ")
                sqlValSb.Append(") VALUES('" & REL_NO & "'," & relLnNum & ",'" & MyDatareader.Item("ITM_CD").ToString & "'")
                sqlValSb.Append("," & MyDatareader.Item("QTY").ToString & "," & MyDatareader.Item("RET_PRC") & ",'")
                sqlValSb.Append(MyDatareader.Item("TREATED").ToString & "','" & MyDatareader.Item("VE_CD").ToString & "','")
                sqlValSb.Append(Replace(MyDatareader.Item("VSN").ToString, "'", "''") & "','" & Replace(MyDatareader.Item("DES").ToString, "'", "''") & "','")
                sqlValSb.Append(MyDatareader.Item("ITM_TP_CD").ToString & "','" & MyDatareader.Item("TREATABLE").ToString & "','" & MyDatareader.Item("PRC_CHG_APP_CD").ToString & "'")

                If MyDatareader.Item("warr_row_link").ToString.isNotEmpty AndAlso
                    IsNumeric(MyDatareader.Item("warr_row_link").ToString) AndAlso
                    MyDatareader.Item("warr_row_link").ToString > 0 AndAlso
                    MyDatareader.Item("warr_itm_link").ToString.isNotEmpty AndAlso
                    MyDatareader.Item("warrantable").ToString = "Y" Then

                    where = "row_id >= " + firstRowId.ToString + " AND row_id <= " + MyDatareader.Item("warr_row_link").ToString
                    tmpItm = tmpItmDatset.Tables(0).Select(where)
                    offSet = tmpItm.GetUpperBound(0)

                    ' Warranty links are the same in relationship_lines as temp_itm so just copy the links over, no difference between warrable or warranty lines
                    '  except do not need links on warr and the line numbers are changing on save when deleted lines removed
                    sqlSb.Append(", warr_row_link, warr_itm_link ")
                    sqlValSb.Append(", " & offSet + 1 & ", '" & MyDatareader.Item("warr_itm_link").ToString & "' ")
                End If

                If Not IsDBNull(MyDatareader.Item("PACKAGE_PARENT")) AndAlso Not String.IsNullOrEmpty(pkgParentLine) Then
                    sqlValSb.Append(", " & pkgParentLine)
                End If
                sqlValSb.Append(", :discAmt ")

                sqlValSb.Append(")")
                sqlSb.Append(sqlValSb.ToString)

                objsql = DisposablesManager.BuildOracleCommand(sqlSb.ToString, conn)
                objsql.Parameters.Add(":discAmt", OracleType.VarChar)
                objsql.Parameters(":discAmt").Value = MyDatareader.Item("DISC_AMT")
                objsql.ExecuteNonQuery()

                If String.Equals(MyDatareader.Item("ITM_TP_CD").ToString.ToUpper, "PKG") Then
                    pkgParentLine = relLnNum
                End If

                objsql.Parameters.Clear()

                sql = "SELECT * FROM RELATIONSHIP_LINES_DESC where REL_NO='" & Session.SessionID.ToString.Trim & "' AND LINE='" & MyDatareader.Item("ROW_ID").ToString & "'"
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDatareader2.Read() Then
                    sql = "UPDATE RELATIONSHIP_LINES_DESC SET REL_NO='" & REL_NO & "', LINE='" & relLnNum & "' WHERE REL_NO='" & Session.SessionID.ToString & "' AND LINE='" & MyDatareader.Item("ROW_ID").ToString & "'"
                    sql = sql & "AND LINE='" & MyDatareader2.Item("LINE").ToString & "'"
                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql.ExecuteNonQuery()
                End If
                MyDatareader2.Close()

                relLnNum = relLnNum + 1
            Loop
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    CREATE RELATIONSHIP_LN2DISC INFO - multiple discounts    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        If SysPms.allowMultDisc Then
            theSalesBiz.ProcessRelnDiscounts(REL_NO, Session.SessionID.ToString.Trim)
        End If
        ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   END   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim lngSEQNUM As Integer, lngComments As Integer
        Dim strComments As String, strMidCmnts As String

        If txt_Follow_up.Text & "" <> "" Then
            lngSEQNUM = 1
            strComments = txt_Follow_up.Text.ToString
            lngComments = Len(strComments)

            If lngComments > 0 Then
                x = 1
                lngSEQNUM = 1

                Do Until x > lngComments
                    strMidCmnts = Mid(strComments, x, 254)
                    strMidCmnts = Replace(strMidCmnts, "'", "")

                    sql = "INSERT INTO RELATIONSHIP_COMMENTS (REL_NO, FOLLOW_UP_DT, COMMENTS) "
                    sql = sql & "VALUES('" & REL_NO & "',SYSDATE,'"
                    sql = sql & Replace(strMidCmnts, """", """""") & "')"

                    sql = UCase(sql)

                    'Perform ERP update
                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objsql.ExecuteNonQuery()

                    x = x + 254
                    lngSEQNUM = lngSEQNUM + 1
                Loop
            End If
        End If

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        If CheckBox1.Checked = True Then
            Dim default_invoice As String = ""
            If default_invoice & "" = "" Then
                sql = "select store_cd, quote_file, default_file from quote_admin where store_cd='" & Session("STORE_CD") & "'"

                objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    Do While MyDatareader.Read
                        If MyDatareader.Item("quote_file").ToString & "" = "" Then
                            default_invoice = MyDatareader.Item("DEFAULT_FILE").ToString
                        Else
                            default_invoice = MyDatareader.Item("quote_file").ToString
                        End If
                    Loop
                    MyDatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                If String.IsNullOrEmpty(default_invoice) Then
                    sql = "select store_cd, quote_file, default_file from quote_admin"

                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    Try
                        MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                        If MyDatareader.Read Then
                            default_invoice = MyDatareader.Item("DEFAULT_FILE").ToString
                        End If
                        MyDatareader.Close()
                    Catch ex As Exception
                        conn.Close()
                        Throw
                    End Try
                End If
            End If

            'since this is a quote, nothing changes as far as printing goes- it will not use the new 'repx' files
            If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
                ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "Rel_Prospect", "window.open('Invoices/" & default_invoice & "?DEL_DOC_NUM=" & REL_NO & "&EMAIL=" & chk_email.Checked.ToString & "','frmQuote345432543" & "','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
            End If

        End If

        txt_Follow_up.Enabled = False

        Dim emp_cd, store_cd, IPAD, co_cd As String

        emp_cd = Session("EMP_CD")
        co_cd = Session("CO_CD")
        IPAD = Session("IPAD")
        fname = Session("EMP_FNAME")
        lname = Session("EMP_LNAME")
        store_cd = Session("HOME_STORE_CD")
        Dim transDate As String = Session("tran_dt")
        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
            cmdDeleteItems.Dispose()
            cmdDeleteItems = Nothing
            cmdDeleteItems = DisposablesManager.BuildOracleCommand

            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from temp_itm where session_id='" & Session.SessionID.ToString.Trim & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
        End If
        ' Daniela save clientip
        Dim client_IP = Session("clientip")
        Dim empInit = Session("EMP_INIT")
        Dim supUser As String = Session("str_sup_user_flag")
        Dim coGrpCd As String = Session("str_co_grp_cd")
        Dim userType As String = Session("USER_TYPE")
        Session.Clear()
        Session("clientip") = client_IP
        Session("str_sup_user_flag") = supUser ' Daniela
        Session("str_co_grp_cd") = coGrpCd ' Daniela
        Session("USER_TYPE") = userType ' Daniela
        Session("EMP_INIT") = empInit
        Session("EMP_CD") = emp_cd
        Session("IPAD") = IPAD
        Session("EMP_CD") = emp_cd
        Session("CO_CD") = co_cd
        Session("EMP_FNAME") = fname
        Session("EMP_LNAME") = lname
        Session("HOME_STORE_CD") = store_cd
        Session("tran_dt") = transDate
        Session("isCommited") = True

        conn.Close()

    End Sub

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As String
        Dim approval_found As Boolean = False

        conn2.Open()
        sql = "UPDATE CUST_INFO SET EMAIL='" & txt_email.Text & "' WHERE CUST_CD='" & Session("CUST_CD") & "' AND SESSION_ID='" & Session.SessionID.ToString.Trim & "' "

        With cmdDeleteItems
            .Connection = conn2
            .CommandText = sql
        End With
        cmdDeleteItems.ExecuteNonQuery()
        conn2.Close()

        ASPxPopupControl2.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        ASPxPopupControl2.ShowOnPageLoad = False

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub chk_email_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If chk_email.Checked = True Then
            ImageButton1.Enabled = True
            ASPxPopupControl1.ShowOnPageLoad = True
        Else
            ImageButton1.Enabled = False
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub ImageButton1_Click1(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

End Class
