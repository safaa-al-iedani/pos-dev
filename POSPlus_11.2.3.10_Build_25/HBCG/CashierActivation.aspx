<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CashierActivation.aspx.vb"
    Inherits="CashierActivation" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="admin_form" runat="server">
        <dx:ASPxLabel ID="lbl_title" runat="server" Text="<%$ Resources:LibResources, Label66 %>"
            Font-Bold="True">
        </dx:ASPxLabel><dx:ASPxLabel ID="lbl_new" runat="server" Text="* NEW ADD *"
            Font-Bold="True" Visible="false">
        </dx:ASPxLabel>
        <br />
        <br />
         <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label423 %>">
        </dx:ASPxLabel>
        <br />
        <dx:ASPxCalendar ID="txt_post_dt" runat="server" AutoPostBack="True">
        </dx:ASPxCalendar>
        <br />
        <dx:ASPxLabel ID="lblStoreCode" runat="server" Text="<%$ Resources:LibResources, Label573 %>">
        </dx:ASPxLabel>
        <br />
        <dx:ASPxComboBox ID="store_cd" Width="280px" AutoPostBack="True" runat="server" SelectedIndex="0"
            OnValueChanged="UpdateStoreInfo" IncrementalFilteringMode ="StartsWith">
        </dx:ASPxComboBox>
        <br />
        <dx:ASPxLabel ID="lbl_cash_drw" runat="server" Text="<%$ Resources:LibResources, Label64 %>" Visible="True">
        </dx:ASPxLabel>
        <br />
        <dx:ASPxComboBox ID="cbo_csh_drw" Width="280px" AutoPostBack="True" runat="server"
            IncrementalFilteringMode ="StartsWith">
        </dx:ASPxComboBox>
        <br />
        <dx:ASPxCheckBox ID="chk_soft_close" runat="server" Text="Soft close the previous drawer" Visible="false">
        </dx:ASPxCheckBox>
        <br />
        <dx:ASPxLabel ID="lbl_error" runat="server" Text="" Width="100%" ForeColor=Red>
        </dx:ASPxLabel>
        <dx:ASPxButton ID="btn_save" runat="server" Text="<%$ Resources:LibResources, Label9 %>" Width="135px">
        </dx:ASPxButton>
        <dx:ASPxLabel ID="lbl_co_cd" runat="server" Text="" Width="100%" Visible="false">
        </dx:ASPxLabel>
        <br />
        <br />
        <dx:ASPxLabel ID="lbl_store_orig" runat="server" Text="" Visible="false">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lbl_csh_orig" runat="server" Text="" Visible="false">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lbl_post_orig" runat="server" Text="" Visible="false">
        </dx:ASPxLabel>
    </div>
</asp:Content>
