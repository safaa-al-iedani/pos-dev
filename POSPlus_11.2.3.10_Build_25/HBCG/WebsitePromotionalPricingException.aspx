﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" CodeFile="WebsitePromotionalPricingException.aspx.vb" 
    Inherits="WebsitePromotionalPricingException" culture="auto"  meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
       
        function pageLoad(sender, args) {
            

            $('input[type=submit], a, button').click(function (event) {

                if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {

                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {

                        $('#hidDestUrl').val($.trim($(this).attr('href')));

                        event.preventDefault();
                    }
                }
            });
        }

        function disableButton(id) {
            // Daniela B 20140723 prevent double click
            try {
                var a = document.getElementById(id);
                a.style.display = 'none';
            } catch (err) {
                alert('Error in disableButton ' + err.message);
                return false;
            }
            return true;
        }

       
    </script>

 
    
   

    <table id="FranchiseShippingSchedule" style="position: relative; width: 1000px; left:0px; top:0px;" cellpadding="2px">

     
           
          <tr >
                <td style="width:13%">
                    <asp:Label runat="server" ID="lblCreate" Text="Create/Query"></asp:Label>
                </td>
                <td colspan ="3">
                    <asp:RadioButton ID="rdbCreate" runat="server" Text="Create" Checked="true" GroupName="CreateQuery" OnCheckedChanged="rdbCreate_OnCheckedChanged" AutoPostBack="true" /> &nbsp;
                    <asp:RadioButton ID="rdbQuery" runat="server" Text="Query"  GroupName="CreateQuery" OnCheckedChanged="rdbQuery_OnCheckedChanged" AutoPostBack="true"/>
                </td>
                  
           </tr>       
       <!--Show only when Query is selected -->
        <tr>
                <td style="width:13%">
                        <asp:Label runat="server" ID="lblQuery_CompanyCode" Text="Company Code" Visible="false" ></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:TextBox runat="server" ID="txtQuery_CompanyCode" MaxLength="3" Width="100px" Visible="false"></asp:TextBox>
                </td>
                <td style="width:17%">
                        <asp:Label runat="server" ID="lblQuery_promotionCode" Text="Promotion Code" Visible="false" ></asp:Label>
                </td>
                <td >
                     <asp:TextBox runat="server" ID="txtQuery_PromotionCode" MaxLength="3" Width="100px" Visible="false"></asp:TextBox>
                </td>
        </tr>
         <tr>
                
        </tr>
         <tr>
                <td style="width:13%">
                        <asp:Label runat="server" ID="lblQuery_sku" Text="SKU" Visible="false" ></asp:Label>
                </td>
                <td style="width:35%">
                     <asp:TextBox runat="server" ID="txtQuery_sku" MaxLength="9" Width="100px" Visible="false"></asp:TextBox>
                </td>
              <td style="width:17%">
                        <asp:Label runat="server" ID="lblQuery_vsn" Text="VSN#" Visible="false" ></asp:Label>
                </td>
                <td>
                     <asp:TextBox runat="server" ID="txtQuery_vsn" MaxLength="30" Width="195px" Visible="false"></asp:TextBox>
                </td>
        </tr>
        <tr>
               
        </tr>
        <tr>
                <td style="width:13%">
                        <asp:Label runat="server" ID="lblQuery_eff_dt" Text="Effective Date" Visible="false" ></asp:Label>
                </td>
                <td style="width:35%">
                    <dx:ASPxDateEdit ID="txtQuery_eff_dt" runat="server"  Width="106px" AllowUserInput="false" Visible="false" DisplayFormatString="dd-MMM-yyyy"  EditFormatString="dd-MMM-yyyy" style="border:solid;border-width:thin;border-color:steelblue" >
                    </dx:ASPxDateEdit>
                </td>
                <td style="width:17%">
                        <asp:Label runat="server" ID="lblQuery_end_dt" Text="Ending Date" Visible="false" ></asp:Label>
                </td>
                <td>
                     <dx:ASPxDateEdit ID="txtQuery_end_dt" runat="server"  Width="100px" Visible="false" AllowUserInput="false" DisplayFormatString="dd-MMM-yyyy"  EditFormatString="dd-MMM-yyyy" style="border:solid;border-width:thin;border-color:steelblue" >
                             </dx:ASPxDateEdit>
                </td>
        </tr>
        <tr>
               
        </tr>
       
        </table>

      <div style="width:950px;width:95%;text-align:center; margin-top:10px" >

           <dx:ASPxButton ID="btnSearch" runat="server" Text="Search"  OnClick="btnSearch_Click" Visible="false" >
             </dx:ASPxButton>
            <dx:ASPxButton ID="btnClear" runat="server" OnClick="btnClear_Click"  Text="Clear" Visible="false">
           </dx:ASPxButton>
       </div>

     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
      
    <hr />

    <div id="dvCreate" runat="server">
       
    <asp:Label ID="litMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    
      
    <!-- GRIDVIEW FOR ITEMS -->
    <div id="dvGridItems" runat="server">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
           <ContentTemplate>
        <asp:GridView ID="grvItems" runat="server" ShowFooter="True" AutoGenerateColumns="false"  OnRowDataBound="grvItems_OnRowDataBound" 
                    CellPadding="4" ForeColor="#333333"   GridLines="None" OnRowDeleting="grvItems_RowDeleting" >
            <Columns>
                <asp:BoundField DataField="RowNumber"  HeaderText="LineNo" />
                <asp:TemplateField HeaderText="Company Code">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_co_cd" runat="server" MaxLength="3" Width="50px" AutoPostBack="true" OnTextChanged="txtCoCd_TextChanged"></asp:TextBox><br />
                   </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Promotion Code">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_prc_cd" runat="server" MaxLength="3" Width="50px" AutoPostBack="true" OnTextChanged="txt_prc_cd_TextChanged"></asp:TextBox><br />
                   </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_Desc" runat="server"  MaxLength="30" Width="200px" Enabled ="false" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SKU">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_SKU" runat="server"  MaxLength="9" Width="100px"  AutoPostBack="true" OnTextChanged="txtCorpSKU_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VSN#">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_VSN" runat="server"  MaxLength="30" Width="200px" Enabled ="false"  ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Corporate">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_corporate" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_corp_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Franchise(using Corporate Pricing)">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_franchise_corporate" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_corp_fran_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Franchise">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_franchise" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_fran_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Quebec">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_quebec" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_que_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Quebec Franchise">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_que_fran" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_que_fran_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Other">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_Other" runat="server"  MaxLength="10" Width="100px" AutoPostBack="true" OnTextChanged="txt_other_TextChanged" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Effective Date">
                    <ItemTemplate>
                     <dx:ASPxDateEdit ID="txt_eff_date" runat="server"  Width="100px" AllowUserInput="false" DisplayFormatString="dd-MMM-yyyy"  EditFormatString="dd-MMM-yyyy" style="border:solid;border-width:thin;border-color:steelblue" >
                    </dx:ASPxDateEdit>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ending Date">
                    <ItemTemplate>
                      
                        <dx:ASPxDateEdit ID="txt_end_date" runat="server"  Width="100px"  AllowUserInput="false" DisplayFormatString="dd-MMM-yyyy"  EditFormatString="dd-MMM-yyyy" style="border:solid;border-width:thin;border-color:steelblue" >
                             </dx:ASPxDateEdit>
                    </ItemTemplate>

                     <FooterStyle HorizontalAlign="Right" />
                    <FooterTemplate>

                        <asp:Button ID="ButtonAdd" runat="server" 

                                Text="Add New Row" OnClick="btnAdd_Click" />
                    </FooterTemplate>

                </asp:TemplateField>
               
                   <asp:TemplateField HeaderText="Associate" Visible ="false" >
                    <ItemTemplate>
                        <asp:TextBox ID="txt_associate" runat="server"  MaxLength="10" Width="100px" Enabled ="false"  ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Updated Date" Visible ="false" >
                    <ItemTemplate>
                        <asp:TextBox ID="txt_upd_date" runat="server"  MaxLength="11" Width="100px" Enabled ="false"  ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Updated Time" Visible ="false" >
                    <ItemTemplate>
                        <asp:TextBox ID="txt_upd_time" runat="server"  MaxLength="5" Width="50px" Enabled ="false"  ></asp:TextBox>
                    </ItemTemplate>

                </asp:TemplateField>
    
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>

            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>

        </ContentTemplate>
    </asp:UpdatePanel>
    </div>

  
    <hr />

   <!-- APPROVING AREA -->
    <table style="width:100%" cellpadding="2px">
        <tr>
            <td style="width:10%">
                <asp:Label runat="server" ID="Label9" Text="Approval Password:"></asp:Label>
            </td>
            <td style="width:25%">
                <asp:TextBox runat="server" ID="txtApprover_Pswd" MaxLength="17" Width="300px" AutoPostBack="true" OnTextChanged="txtApprover_Pswd_TextChanged" ></asp:TextBox>
            </td>

            <td style="width:45%">
                 <asp:Button ID="btnConfirmASP" runat="server" Text="COMMIT" Click="btnConfirm_Click" Enabled="true" CausesValidation="False" />
            </td>  
        </tr>
    </table>

    </div>

     <div id="dvQuery" runat="server" >
      
         <!-- Gridview Expand -->
         <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="false" CssClass="Grid"
             PageSize="10"  AllowSorting="true"  >
            <Columns>
                                 
                <asp:BoundField ItemStyle-Width="100px" DataField="CO_CD" HeaderText="Company Code" />
                <asp:BoundField ItemStyle-Width="100px" DataField="PRC_CD" HeaderText="Promotion Code" />
                <asp:BoundField ItemStyle-Width="300px" DataField="DES" HeaderText="Description" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="150px" DataField="ITM_CD" HeaderText="SKU" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="300px" DataField="VSN" HeaderText="VSN#" />
                <asp:BoundField ItemStyle-Width="120px" DataField="CORP" HeaderText="Corporate" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="120px" DataField="FRAN_CORP" HeaderText="Franchise(using Corporate Pricing)" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="120px" DataField="FRAN" HeaderText="Franchise" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="120px" DataField="QUEBEC" HeaderText="Quebec" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="120px" DataField="QUE_FRAN" HeaderText="Quebec Franchise" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="120px" DataField="OTHER" HeaderText="Other" DataFormatString="{0:0.00}" />
                <asp:BoundField ItemStyle-Width="150px" DataField="EFF_DT" HeaderText="Effective Date" />
                <asp:BoundField ItemStyle-Width="150px" DataField="END_DT" HeaderText="Ending Date" />
                <asp:BoundField ItemStyle-Width="130px" DataField="EMP_CD" HeaderText="Associate" />
                <asp:BoundField ItemStyle-Width="150px" DataField="UPD_DT" HeaderText="Updated Date" />
                 <asp:BoundField ItemStyle-Width="130px" DataField="UPD_TIME" HeaderText="Updated Time" />
            </Columns>
        </asp:GridView>

           <table width="98%">
            <tr>
                <td align="center">
                    <table width="35%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>

                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>


     </div>
  
   
</asp:Content>