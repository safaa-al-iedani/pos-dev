Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class InventoryLookup
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private setSOsHyperlink As Boolean = False
    Private setPOsHyperlink As Boolean = False
    Private setInvHyperlink As Boolean = False

    Private LeonsBiz As LeonsBiz = New LeonsBiz() 'Daniela
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lbl_warnings.Text = ""

            If Request("exitWOserial") = "Y" Then
                Response.Write("<script language='javascript'>" & vbCrLf)
                Response.Write("var parentWindow = window.top; ")
                Response.Write("parentWindow.SelectAndClosePopup3();")
                If Request("referrer") = "SOM" Then
                    Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=Y&store_cd=" & Request("STORE_CD") & "&LOC_CD=" & Request("LOC_CD") & "&ROW_ID=" & Request("ROW_ID") & "';")
                Else
                    Response.Write("parentWindow.location.href='order_detail.aspx';")
                End If
                Response.Write("</script>")
                Response.End()
            End If

            If Not IsPostBack Then

                Dim rInvAvail As String = Request("INV_SELECTION") & ""
                Dim rRowId As String = Request("ROW_ID") & ""
                Dim rItmCd As String = Request("SKU") & ""
                rItmCd = UCase(rItmCd)

                If (rItmCd.isEmpty) Then
                    main_body.Visible = False
                    div_lookup_form.Visible = True
                    ' lbl_script.Text = "Please enter the SKU you would like to view Availability for:"
                    lbl_script.Text = Resources.LibResources.Label404
                    lbl_script.Visible = True
                    txt_SKU.Focus()
                    Exit Sub
                Else
                    main_body.Visible = True
                    div_lookup_form.Visible = False
                    rItmCd = rItmCd.Trim ' Daniela trim blanks                
                End If
                ' Daniela SKU validation start
                If (InStr(rItmCd, ",") = 0) Then
                    Dim usr_flag = LeonsBiz.sec_check_itm(rItmCd, Session("emp_cd"))
                    If usr_flag = "N" Then  'multi co
                        txt_SKU.Text = ""
                        main_body.Visible = False
                        div_lookup_form.Visible = True
                        ' lbl_script.Text = "Please enter the SKU you would like to view Availability for:"
                        lbl_script.Text = Resources.LibResources.Label404
                        'lbl_warnings.Text = "You are not authorized to select that SKU"
                        lbl_warnings.Text = Resources.LibResources.Label673
                        lbl_script.Visible = True
                        lbl_warnings.Visible = True
                        txt_SKU.Focus()
                        Exit Sub
                    End If
                End If
                ' Daniela SKU validation end                
				btn_refresh.Visible = Not ("Y" = rInvAvail)

                ' ==== ALL REQUESTS FLAGGED AS 'Process = TRUE' ARE INTERNAL TO THIS CLASS ====
                ' ** They are initiated by the Hyperlinks in the multiple grids on this window **
                If (Request("Process") = "TRUE") Then

                    'When user changes the inventory selection, IF a soft-res is present, it needs to be removed
                    'NOTE: during "ProcessSerialNumberUpdate" or "ProcessStockUpdate", a new soft-res gets created
                    UnreserveInventory(rItmCd)

                    If Request("PO_CD") & "" <> "" Then
                        ' ===== Process POs, lands here when the hyperlink in the PO grid is clicked
                        ProcessPOUpdate()

                    ElseIf (Request("STORE_CD") & "" <> "" AndAlso Request("LOC_CD") & "" <> "") Then

                        If (Request("OUT_ID") & "" <> "") Then
                            ' ===== Process OUTLET, lands here when the hyperlink in the Outlet grid is clicked
                            ProcessOutletUpdate()

                        ElseIf (ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" AndAlso
                                Request("SER_TP") & "" <> "" AndAlso InventoryUtils.IsSerialType(Request("SER_TP"))) Then
                            ' ===== Process SERIAL, lands here when
                            ' 1) the hyperlink in the Serial #s grid is clicked, 
                            ' 2) warehouse/showroom item is selected and it is flagged as serialized
                            If Request("SER_NUM") & "" <> "" Then
                                ProcessSerialNumberUpdate()
                            Else
                                'Serial number hasn't been captured, it needs to prompt
                                PromptForSerialNumber(Request("ROW_ID"), rItmCd, Request("STORE_CD"), Request("LOC_CD"))
                            End If
                        Else
                            ' ===== Process STOCK, lands here when the hyperlink in the Warehouse grid is clicked
                            ' and the ITEM is not serializable 
                            ProcessStockUpdate()
                        End If
                    End If
                End If

                ' ================  Sets the ITEM details to be displayed in this window  ================
                If InStr(rItmCd, ",") > 0 Then
                    Dim skusArray As Array = Split(rItmCd, ",")
                    rItmCd = skusArray(0).ToString
                    lbl_warnings.Text = "Only the first SKU will be displayed"
                End If
                If Len(rItmCd) = 10 Then rItmCd = Right(rItmCd, 9)

                ' -- sets the item image
                Try
                    ' Daniela June 11
                    'Dim image_URL As String = HBCG_Utils.Find_Merchandise_Images(rItmCd)
                    If isEmpty(Session("ImgZipCd")) Then
                        Session("ImgZipCd") = LeonsBiz.GetPostalCode()
                    End If
                    If isEmpty(Session("ImgLangCode")) Then
                        Session("ImgLangCode") = LeonsBiz.GetLangCode()
                    End If
                    ' Daniela save URL for each item
                    ' Feb 28 check if url is already in temp table
                    Dim image_URL As String = LeonsBiz.GetURL(rItmCd)

                    'Dim image_URL As String = LeonsBiz.Find_Merchandise_Images(rItmCd, Session("ImgZipCd"), Session("ImgLangCode"))
                    If Not image_URL.isEmpty Then
                        img_picture.ImageUrl = image_URL
                    Else
                        img_picture.ImageUrl = "~/images/image_not_found.jpg"
                    End If
                Catch

                End Try

                ' ================  If the item is NOT found, stops processing
                If Not GetItemDetails(rItmCd) Then Exit Sub

                ' ========== Sets Class level variables that will be used during the BINDING of the grids =========
                setInvHyperlink = AllowInventoryHyperlink()
                setPOsHyperlink = AllowPurchaseOrderHyperlink()
                setSOsHyperlink = AllowSalesOrderHyperlink()

                ' ================  Gets the data for the multiple grids displayed on this window  ================
                Dim softRes As DataSet = Nothing
                Dim resultSet As DataSet = Nothing

                ' ---- Get Warehouse Inventory
                Dim pfReq As PriFillReq
                If SysPms.priorityFillForInvAvail And Session("ord_tp_cd") & "" <> "" Then

                    pfReq = New PriFillReq
                    pfReq.itmCd = rItmCd
                    pfReq.homeStoreCd = SessVar.homeStoreCd
                    pfReq.puDel = SessVar.puDel
                    pfReq.puDelStoreCd = SessVar.puDelStoreCd
                    pfReq.takeWith = "N"
                    pfReq.zoneCd = SessVar.zoneCd
                    pfReq.zoneZipCd = SessVar.zipCode4Zone
                    pfReq.locTpCd = AppConstants.Loc.TYPE_CD_W
                    resultSet = theInvBiz.GetInvPriFill(pfReq)
                Else
                    resultSet = theInvBiz.GetStoreData(rItmCd, AppConstants.Loc.TYPE_CD_W)
                    'NOTE: for WAREHOUSE and SHOWROOM counts, soft res counts need to be subtracted, but only
                    '      when SYSPM priority fill is off; when on, the API takes into account the soft-res
                    If (softRes Is Nothing) Then softRes = theInvBiz.GetReservations(rItmCd)
                    resultSet = InventoryUtils.RemoveResCounts(resultSet, softRes, AppConstants.Loc.TYPE_CD_W)
                End If
                Dim totalWhse As Integer = InventoryUtils.GetQtyCount(resultSet)
                '  lbl_total_whs.Text = "Total Open Available: " & totalWhse
                lbl_total_whs.Text = Resources.LibResources.Label617 & ": " & totalWhse
                If ConfigurationManager.AppSettings("avail_details") = "Y" Then
                    GridView4.DataSource = resultSet
                    GridView4.DataBind()
                End If

                ' ---- Get Vendor Inventory  Alice added on Nov 22, 2018 for request 4350
                resultSet = GetVendorData(rItmCd)
                ASPxGridView_vendor.DataSource = resultSet
                ASPxGridView_vendor.DataBind()

                ' ---- Get Showroom Inventory
                If SysPms.priorityFillForInvAvail And Session("ord_tp_cd") & "" <> "" Then

                    pfReq = New PriFillReq
                    pfReq.itmCd = rItmCd
                    pfReq.homeStoreCd = SessVar.homeStoreCd
                    pfReq.puDel = SessVar.puDel
                    pfReq.puDelStoreCd = SessVar.puDelStoreCd
                    pfReq.takeWith = "N"
                    pfReq.zoneCd = SessVar.zoneCd
                    pfReq.zoneZipCd = SessVar.zipCode4Zone
                    pfReq.locTpCd = AppConstants.Loc.TYPE_CD_S
                    resultSet = theInvBiz.GetInvPriFill(pfReq)
                Else
                    resultSet = theInvBiz.GetStoreData(rItmCd, AppConstants.Loc.TYPE_CD_S)
                    'NOTE: for WAREHOUSE and SHOWROOM counts, soft res counts need to be subtracted, but only
                    '      when SYSPM priority fill is off; when on, the API takes into account the soft-res
                    If (softRes Is Nothing) Then softRes = theInvBiz.GetReservations(rItmCd)
                    resultSet = InventoryUtils.RemoveResCounts(resultSet, softRes, AppConstants.Loc.TYPE_CD_S)
                End If
                Dim totalShow As Integer = InventoryUtils.GetQtyCount(resultSet)
                'lbl_total_store.Text = "Total Store Inventory: " & totalShow
                lbl_total_store.Text = Resources.LibResources.Label622 & totalShow
                If ConfigurationManager.AppSettings("avail_details") = "Y" Then
                    GridView3.DataSource = resultSet
                    GridView3.DataBind()
                End If

                ' ---- SALES Information
                resultSet = theInvBiz.GetSalesData(rItmCd)
                GridView1.DataSource = resultSet
                GridView1.DataBind()

                Dim totalSales As Integer = InventoryUtils.GetQtyCount(resultSet)
                'lbl_total_so.Text = "Total Pending Sales Orders: " & totalSales
                lbl_total_so.Text = Resources.LibResources.Label621 & " " & totalSales


                ' ---- Get Purchase Orders Information
                resultSet = theInvBiz.GetPoData(rItmCd)

                ' Add new column into data table #ITREQUEST-337
                resultSet.Tables(0).Columns.Add(New DataColumn("BOOKING_QTY"))

                ' Add new column into data table #ITREQUEST-337
                resultSet.Tables(0).Columns.Add(New DataColumn("BOOKING_STATUS"))

                ' Process po details to get booking status #ITREQUEST-337
                If resultSet.Tables(0).Rows.Count > 0 Then
                    For Each dataRow As DataRow In resultSet.Tables(0).Rows
                        ' Get purchase order line booking quantity #ITREQUEST-377
                        Dim bookingQty As Integer = theSalesBiz.getPOLineBookingQty(dataRow("PO_CD").ToString, dataRow("LN#").ToString)
                        If bookingQty > 0 Then
                            dataRow("BOOKING_QTY") = bookingQty.ToString
                        End If

                        ' Get purchase order line booking status #ITREQUEST-377
                        dataRow("BOOKING_STATUS") = theSalesBiz.getPOLineBookingStatus(dataRow("PO_CD").ToString, dataRow("LN#").ToString)
                    Next
                End If

                GridView2.DataSource = resultSet
                GridView2.DataBind()

                'Checks to see if there is enough inventory to cover the open sales, 
                'otherwise collecting the PODates is a highly consuming time process
                'when no Sales and no inventory, the PODates is all there is, so collects them
                Dim poDatesNeeded As Boolean = (totalWhse <= totalSales OrElse (totalWhse = 0 AndAlso totalSales = 0))
                Dim poDates As String = String.Empty
                Dim totalPo As Integer = 0
                Dim totalIncoming As Integer = 0
                Dim cnt As Integer
                For Each poRow In resultSet.Tables(0).Rows
                    If poRow("Arrival_dt") & "" <> "" Then
                        totalIncoming = CInt(poRow("Total_Incoming"))

                        If (totalIncoming > 0) Then
                            totalPo = totalPo + totalIncoming

                            'this concatenation sends the app to last 10+ minutes before screen displays
                            'for highly used SKUs, and if this ends up not used, then it is time wasted
                            If (poDatesNeeded) Then
                                '--- creates a '|' delimited list of dates
                                For cnt = 1 To totalIncoming
                                    poDates = poDates & "|" + poRow("Arrival_dt")
                                Next
                            End If
                        End If
                    End If
                Next
                'lbl_total_po.Text = "Total Available Incoming: " & totalPo
                lbl_total_po.Text = Resources.LibResources.Label616 & " " & totalPo

                ' ---- IST Information
                resultSet = theInvBiz.GetIstData(rItmCd)
                ASPxGridView2.DataSource = resultSet
                ASPxGridView2.DataBind()

                Dim totalIst As Integer = InventoryUtils.GetQtyCount(resultSet)
                'lbl_total_ist.Text = "Total Pending ISTs: " & totalIst
                lbl_total_ist.Text = Resources.LibResources.Label620 & ": " & totalIst


                ' ---- OUTLET Information
                resultSet = theInvBiz.GetOutletData(rItmCd)
                ASPxGridView3.DataSource = resultSet
                ASPxGridView3.DataBind()

                Dim totalOut As Integer = InventoryUtils.GetQtyCount(resultSet)
                'lbl_out.Text = "Total Outlet: " & totalOut
                lbl_out.Text = Resources.LibResources.Label618 & ": " & totalOut
                

                ' ---- DETERMINES STOCK AVAILABILITY ----
                Dim stockAvailability As String = String.Empty

                If (totalWhse - totalSales) > 0 Then
                    stockAvailability = Resources.POSMessages.MSG0027

                ElseIf (totalWhse + totalPo) - totalSales > 0 Then

                    'if here means there is stock and POs to cover the Sales and more
                    Dim poDatesSplit = Split(poDates, "|")
                    Dim availDt As String = poDatesSplit((totalSales - totalWhse) + 1)
                    stockAvailability = String.Format(Resources.POSMessages.MSG0025, availDt)

                    If (IsDate(availDt)) Then
                        Dim availableDt As Date = FormatDateTime(availDt, DateFormat.ShortDate)
                        Dim currentDt As Date = Now.Date.ToString
                        If DateDiff(DateInterval.Day, availableDt, currentDt) > 0 Then
                            Dim paddedCurDate As Date = DateAdd(DateInterval.Day, 30, currentDt)
                            stockAvailability = String.Format(Resources.POSMessages.MSG0025,
                                                              FormatDateTime(paddedCurDate, DateFormat.ShortDate))
                        End If
                    End If

                Else    ' if here means NO STOCK IS AVAILABLE 
                    If (totalSales > 0 AndAlso totalSales = totalWhse + totalPo) Then
                        stockAvailability = Resources.POSMessages.MSG0028
                    Else
                        stockAvailability = CheckForItemAvailability(rItmCd)
                    End If

                End If
                lbl_availability.Text = stockAvailability

                resultSet = Nothing
            End If
        Catch oracleException As OracleException
            Select Case oracleException.Code
                Case 20101                    
                    Session("oracleException") = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
            End Select
            RouteRequest()           
        Catch ex As Exception
            Throw        
        End Try
    End Sub

    ''' <summary>
    ''' When no available product exists in Inventory Priority fill and in IST/PO priority fill, 
    ''' the app should display the date corresponding to the SKU's lead days and any EAD Date 
    ''' if greater than today�s system date.
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve the Lead days for</param>
    ''' <returns>a string representing the item availability, or a 'NO STOCK AVAILABLE' message</returns>
    ''' <remarks></remarks>
    Private Function CheckForItemAvailability(ByVal itmCd As String) As String

        Dim itemAvail As String = Resources.POSMessages.MSG0026
        Dim store As String = If(IsNothing(Session("home_store_cd")), String.Empty, Session("home_store_cd"))
        If (store.isNotEmpty()) Then
            Try
                Dim storeData As StoreData = theSalesBiz.GetStoreInfo(store)
                If (storeData.enableARS) Then
                    Dim leadDays As Integer = theInvBiz.GetItemLeadDays(itmCd, store)
                    If leadDays > 0 Then
                        Dim dtCurrent As Date = Now.Date.ToString
                        itemAvail = String.Format(Resources.POSMessages.MSG0025,
                                                  FormatDateTime(DateAdd(DateInterval.Day, leadDays, dtCurrent).ToString, DateFormat.ShortDate))
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End If
        Return itemAvail
    End Function

    ''' <summary>
    ''' Retrieves the item and displays its details accordingly
    ''' </summary>
    ''' <param name="itmCd">the item to retrieve details for</param>
    ''' <returns>TRUE if item was found in DB; FALSE otherwise</returns>
    Private Function GetItemDetails(ByVal itmCd As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
        Dim dbReader As OracleDataReader
        Dim sql As StringBuilder = New StringBuilder()
        Dim ret_prc As Double = 0

        sql.Append("SELECT ITM.ITM_CD, ITM.SERIAL_TP, ITM.VE_CD, ITM.RET_PRC, ITM.REPL_CST, ")
        sql.Append("       ITM.IVC_CST, ITM.ADV_PRC, ITM.VSN, ITM.DES, ITM.FINISH, ITM.ITM_TP_CD ")
        sql.Append("FROM ITM WHERE ITM.ITM_CD = :itmCd")

        Try
            dbConnection.Open()
            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)


            If dbReader.Read Then
                If IsNumeric(dbReader.Item("RET_PRC").ToString) Then ret_prc = CDbl(dbReader.Item("RET_PRC").ToString)

                If ConfigurationManager.AppSettings("show_cost") = "Y" AndAlso
                   Find_Security("CPTR_PCST", Session("emp_cd")) = "Y" Then

                    If IsNumeric(dbReader.Item("REPL_CST").ToString) Then
                        txt_cost.Text = FormatCurrency(CDbl(dbReader.Item("REPL_CST").ToString), 2)
                    Else
                        txt_cost.Text = ""
                    End If
                Else
                    ASPxLabel3.Visible = False
                    txt_cost.Enabled = False
                    txt_cost.Text = ""
                End If
                lbl_vsn.Text = dbReader.Item("VSN").ToString
                lbl_ser_tp.Text = dbReader.Item("SERIAL_TP").ToString
                lbl_sku.Text = dbReader.Item("ITM_CD").ToString
                lbl_vendor.Text = dbReader.Item("VE_CD").ToString
                lbl_vsn.Text = lbl_vsn.Text & vbCrLf & dbReader.Item("DES").ToString
                Session("tran_dt") = FormatDateTime(Today.Date, 2)
                Session("store_cd") = Session("HOME_STORE_CD")
                Dim ShuDt As String = SessVar.shuDt
                If SysPms.isCashierActiv Then
                ActivateCashier(Session("emp_cd"))
                If FormatDateTime(Session("tran_dt")) < Today.Date.AddDays(-SysPms.numDaysPostDtTrans) Then
                    Session("csh_act_init") = "INVALIDCASHIER"
                ElseIf FormatDateTime(Session("tran_dt")) > Today.Date.AddDays(SysPms.numDaysPostDtTrans) Then
                    Session("csh_act_init") = "INVALIDCASHIER"
                ElseIf IsDate(ShuDt) AndAlso DateDiff("d", FormatDateTime(Session("tran_dt"), DateFormat.ShortDate), ShuDt) >= 0 Then
                    Session("csh_act_init") = "INVALIDCASHIER"
                End If
                If Session("csh_act_init") = "INVALIDCASHIER" Then
                    Session("tran_dt") = FormatDateTime(Today.Date, 2)
                    If Session("store_cd") = Nothing Then
                        Session("store_cd") = Session("HOME_STORE_CD")
                    End If
                End If
                End If
                'Daniela start
                Dim custTpPrcCd As String = ""

                If (isNotEmpty(SessVar.custTpPrcCd)) Then
                    custTpPrcCd = SessVar.custTpPrcCd
                End If
                ' Daniela end
                'Dim currentPrice As Double = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                '                                                         SessVar.wrStoreCd,
                '                                                         SessVar.custTpPrcCd,
                '                                                         Session("tran_dt"),
                '                                                         itmCd, ret_prc)
                Dim currentPrice As Double = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                                        SessVar.wrStoreCd,
                                                                        custTpPrcCd,
                                                                        Session("tran_dt"),
                                                                        itmCd, ret_prc)
                If currentPrice = ret_prc Then
                    lbl_retail.Text = FormatCurrency(ret_prc, 2)
                    lblRetailPrice.Visible = False
                    lblRetailPriceValue.Visible = False
                Else
                    ' Daniela hide retail price
                    'lblRetailPrice.Visible = True
                    'lblRetailPriceValue.Visible = True
                    lbl_retail.Text = FormatCurrency(currentPrice, 2)
                    'lblRetailPriceValue.Text = FormatCurrency(ret_prc, 2)
                End If
                rtnVal = True
            Else
                lbl_warnings.Text = itmCd & " was not found."
                main_body.Visible = False
                div_lookup_form.Visible = True
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        Return rtnVal

    End Function

    ''' <summary>
    ''' Updates the Store and Location selected in the current line being processed.
    ''' </summary>
    Private Sub ProcessStockUpdate()
        Try
            ReserveInventory(GetQtyToReserve())
            RouteRequest()
        Catch oracleException As OracleException
            Throw
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RouteRequest()
        If Request("referrer") = "SOM" Then
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.parent; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "';")
            Response.Write("</script>")
            Response.End()
        Else
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.parent; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            Response.Write("parentWindow.location.href='order_detail.aspx';")
            Response.Write("</script>")
            Response.End()
        End If
    End Sub

    ''' <summary>
    ''' Updates the Serial# selected and its associated details in the current line being processed.
    ''' NOTE: Attempts to create a SOFT-RESERVATION for the store/location selected
    ''' </summary>
    Private Sub ProcessSerialNumberUpdate()
        Try
            ReserveInventory("1")   'for a serial number, the quantity is ALWAYS one

            If Request("referrer") = "SOM" Then
                Response.Write("<script language='javascript'>" & vbCrLf)
                Response.Write("var parentWindow = window.top; ")
                Response.Write("parentWindow.SelectAndClosePopup3();")
                Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=Y&ROW_ID=" & Request("ROW_ID") & "';")
                Response.Write("</script>")
                Response.End()
            Else
                Response.Write("<script language='javascript'>" & vbCrLf)
                Response.Write("var parentWindow = window.top; ")
                Response.Write("parentWindow.SelectAndClosePopup3();")
                Response.Write("parentWindow.location.href='order_detail.aspx';")
                Response.Write("</script>")
                Response.End()
            End If
        Catch oracleException As OracleException
            Throw
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Updates the Outlet ID and associated values in the current line being processed.
    ''' </summary>
    Private Sub ProcessOutletUpdate()

        ' if outlet and qty > 1, reset to 1 and warn
        Dim qtyReset As Boolean = False

        If Request("referrer") = "SOM" Then
            Dim lines As DataSet = Session("DEL_DOC_LN#")
            For Each dr In lines.Tables(0).Rows
                If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                    dr("STORE_CD") = UCase(Request("STORE_CD"))
                    dr("LOC_CD") = UCase(Request("LOC_CD"))
                    dr("SERIAL_NUM") = IIf((Request("SER_NUM").ToString.isEmpty), System.DBNull.Value, UCase(Request("SER_NUM")))
                    dr("OUT_ID") = UCase(Request("OUT_ID"))
                    dr("COMM_CD") = UCase(Request("COMM_CD"))
                    dr("SPIFF") = UCase(Request("SPIFF"))
                    Exit For
                End If
            Next
            Session("DEL_DOC_LN#") = lines
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.top; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=Y&ROW_ID=" & Request("ROW_ID") & "';")
            Response.Write("</script>")
            Response.End()
        Else
            Dim tempItmTab As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Request("ROW_ID"), HBCG_Utils.temp_itm_key_tp.tempItmByRowIdSeqNum)
            If (Not IsNothing(tempItmTab)) AndAlso tempItmTab.Rows.Count > 0 Then
                Dim tempItmRow As DataRow = tempItmTab.Rows(0)
                If CDbl(tempItmRow("QTY").ToString) > 1 Then
                    ' warning = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    qtyReset = True
                End If
            End If

            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
            Dim sql As StringBuilder = New StringBuilder()

            sql.Append("UPDATE TEMP_ITM SET STORE_CD = :storeCd, LOC_CD = :locCd, SERIAL_NUM = :serialNum, ")
            sql.Append("            OUT_ID = :outId, COMM_CD = :commCd, SPIFF = :spiff ")
            If qtyReset Then sql.Append(", QTY = 1 ")
            sql.Append(" WHERE ROW_ID = :theRowId")
            Try
                dbConnection.Open()
                dbCommand.CommandText = sql.ToString
                dbCommand.Parameters.Add(":theRowId", OracleType.VarChar)
                dbCommand.Parameters.Add(":storeCd", OracleType.VarChar)
                dbCommand.Parameters.Add(":locCd", OracleType.VarChar)
                dbCommand.Parameters.Add(":serialNum", OracleType.VarChar)
                dbCommand.Parameters.Add(":outId", OracleType.VarChar)
                dbCommand.Parameters.Add(":commCd", OracleType.VarChar)
                dbCommand.Parameters.Add(":spiff", OracleType.VarChar)

                dbCommand.Parameters(":theRowId").Value = Request("ROW_ID")
                dbCommand.Parameters(":storeCd").Value = Request("STORE_CD")
                dbCommand.Parameters(":locCd").Value = Request("LOC_CD")
                dbCommand.Parameters(":serialNum").Value = IIf((Request("SER_NUM").ToString.isEmpty), System.DBNull.Value, UCase(Request("SER_NUM")))
                dbCommand.Parameters(":outId").Value = Request("OUT_ID")
                dbCommand.Parameters(":commCd").Value = Request("COMM_CD")
                dbCommand.Parameters(":spiff").Value = IIf(String.IsNullOrWhiteSpace(Request("SPIFF")), System.DBNull.Value, CDbl(Request("SPIFF")))
                dbCommand.ExecuteNonQuery()
            Catch ex As Exception
                Throw
            Finally
                dbCommand.Dispose()
                dbConnection.Close()
            End Try
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.top; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            If qtyReset Then
                Response.Write("parentWindow.location.href='order_detail.aspx?warning=" & HBCG_Utils.warning.qtyChangeWarning & "';")
            Else
                Response.Write("parentWindow.location.href='order_detail.aspx';")
            End If
            Response.Write("</script>")
            Response.End()
        End If
    End Sub

    ''' <summary>
    ''' Updates the PO and associated values in the current line being processed.
    ''' </summary>
    Private Sub ProcessPOUpdate()

        If Request("referrer") = "SOM" Then
            Dim lines As DataSet = Session("DEL_DOC_LN#")
            Dim poDes As StringBuilder = New StringBuilder()
            poDes.Append("PO: ").Append(Request("PO_CD")).Append(" - (").Append(Request("PO_DT")).Append(")")
            For Each dr In lines.Tables(0).Rows
                If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                    dr("STORE_CD") = System.DBNull.Value
                    dr("LOC_CD") = System.DBNull.Value
                    dr("OUT_ID") = System.DBNull.Value
                    dr("PO_CD") = Request("PO_CD")
                    dr("PO_DESC") = poDes.ToString()
                    dr("PO_LN") = Request("PO_LN")
                    Exit For
                End If
            Next
            Session("DEL_DOC_LN#") = lines

            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.top; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "';")
            Response.Write("</script>")
            Response.End()
        Else
            Dim sql As String = "UPDATE TEMP_ITM SET store_cd = null, loc_cd = null, out_id = null, po_cd = :poCd, po_ln = :poLn WHERE ROW_ID = :theRowId"
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            Try
                dbConnection.Open()
                dbCommand.Parameters.Add(":theRowId", OracleType.VarChar)
                dbCommand.Parameters.Add(":poCd", OracleType.VarChar)
                dbCommand.Parameters.Add(":poLn", OracleType.Number)

                dbCommand.Parameters(":theRowId").Value = Request("ROW_ID")
                dbCommand.Parameters(":poCd").Value = Request("PO_CD")
                dbCommand.Parameters(":poLn").Value = Request("PO_LN")
                dbCommand.ExecuteNonQuery()
            Catch ex As Exception
                Throw
            Finally
                dbCommand.Dispose()
                dbConnection.Close()
            End Try
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.top; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            Response.Write("parentWindow.location.href='order_detail.aspx';")
            Response.Write("</script>")
            Response.End()
        End If
    End Sub

    ''' <summary>
    ''' Finds out if the item being processed has been soft-reserved, and if so,
    ''' removes the reservation accordingly
    ''' </summary>
    ''' <param name="itmCd">the item that will be unreserved (if applicable)</param>
    Private Sub UnreserveInventory(ByVal itmCd As String)
        Try
            Dim resId As String = String.Empty
            Dim storeCd As String = String.Empty
            Dim locCd As String = String.Empty

            If Request("referrer") = "SOM" Then
                Dim lines As DataSet = Session("DEL_DOC_LN#")
                For Each dr In lines.Tables(0).Rows
                    If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                        resId = If(IsDBNull(dr("RES_ID")), String.Empty, dr("RES_ID").ToString)
                        storeCd = If(IsDBNull(dr("STORE_CD")), String.Empty, dr("STORE_CD").ToString)
                        locCd = If(IsDBNull(dr("LOC_CD")), String.Empty, dr("LOC_CD").ToString)
                        Exit For
                    End If
                Next
            Else
                Dim tempItmTab As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Request("ROW_ID"), HBCG_Utils.temp_itm_key_tp.tempItmByRowIdSeqNum)
                If (Not IsNothing(tempItmTab)) AndAlso tempItmTab.Rows.Count > 0 Then
                    Dim tempItmRow As DataRow = tempItmTab.Rows(0)
                    resId = If(IsDBNull(tempItmRow("RES_ID")), String.Empty, tempItmRow("RES_ID").ToString)
                    storeCd = If(IsDBNull(tempItmRow("STORE_CD")), String.Empty, tempItmRow("STORE_CD").ToString)
                    locCd = If(IsDBNull(tempItmRow("LOC_CD")), String.Empty, tempItmRow("LOC_CD").ToString)
                End If
            End If

            ' Remove the SOFT-Reservations
            If (Not String.IsNullOrEmpty(resId)) Then
                Dim theRequest As UnResRequestDtc = InventoryUtils.GetRequestForUnreserve(resId, itmCd, storeCd, locCd)
                Try
                    theInvBiz.RemoveSoftRes(theRequest)
                Catch ex As Exception
                    'nothing to do at this time
                End Try
            End If
        Catch oracleException As OracleException
            Throw
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Creates a new reservation, with the values available in the Request object
    ''' </summary>
    ''' <param name="qtyToRes">the quantity being reserved</param>
    Private Sub ReserveInventory(ByVal qtyToRes As String)
        Dim sbError As StringBuilder = New StringBuilder()

        Dim resResponse As New ResResponseDtc()
        Dim resRequest As ResRequestDtc = InventoryUtils.GetRequestForDirectedRes(Request("SKU"),
                                                                                  qtyToRes,
                                                                                  Request("STORE_CD"),
                                                                                  Request("LOC_CD"),
                                                                                  Session("PD"))
        If Request("referrer") = "SOM" Then
            resResponse = theInvBiz.RequestSoftReserve(resRequest)
            Dim qtyReserved As Double = CDbl(qtyToRes)  'may change if lines split
            Dim resId As String = String.Empty
            If (Not resResponse Is Nothing AndAlso resResponse.resId.isNotEmpty) Then
                resId = resResponse.resId
                qtyReserved = CDbl(resResponse.reservations.Rows(0)("QTY").ToString)

                Dim lines As DataSet = Session("DEL_DOC_LN#")
                For Each dr In lines.Tables(0).Rows
                    If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                        dr("STORE_CD") = Request("STORE_CD")
                        dr("LOC_CD") = Request("LOC_CD")
                        dr("PO_CD") = System.DBNull.Value
                        dr("PO_DESC") = System.DBNull.Value
                        dr("RES_ID") = IIf(resId.isEmpty, System.DBNull.Value, resId)
                        dr("OUT_ID") = System.DBNull.Value
                        dr("SERIAL_NUM") = System.DBNull.Value
                        dr("QTY") = qtyReserved
                        Exit For
                    End If
                Next
                Session("DEL_DOC_LN#") = lines
            End If
        Else
            resRequest.lineNum = Request("ROW_ID")
            resResponse = theSalesBiz.ReserveLine(Session.SessionID.ToString.Trim(), SessVar.discCd, resRequest, sbError)
            Session("InvError") = sbError
        End If
    End Sub

    ''' <summary>
    ''' Finds out the quantity that needs to be soft-reserved
    ''' </summary>
    ''' <returns>the quantity to create the reservation for</returns>
    Private Function GetQtyToReserve() As String
        Dim qty As String = String.Empty
        If Request("referrer") = "SOM" Then
            Dim lines As DataSet = Session("DEL_DOC_LN#")
            For Each dr In lines.Tables(0).Rows
                If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                    qty = dr("QTY")
                    Exit For
                End If
            Next
        Else
            Dim tempItmTab As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Request("ROW_ID"),
                                                                               HBCG_Utils.temp_itm_key_tp.tempItmByRowIdSeqNum)
            If (Not IsNothing(tempItmTab)) AndAlso tempItmTab.Rows.Count > 0 Then
                Dim tempItmRow As DataRow = tempItmTab.Rows(0)
                qty = tempItmRow("QTY")
            End If
        End If
        Return qty
    End Function

    ''' <summary>
    ''' Finds out the Outlet IDs that have already been used in the current order being processed
    ''' Done so that these IDs get excluded from selection.
    ''' </summary>
    ''' <returns>A comma delimited list of Outlet IDs or an empty string if none found</returns>
    Private Function GetOutletIDsInUse() As String

        Dim outIds As String = String.Empty
        If Request("referrer") = "SOM" Then
            Dim lines As DataSet = Session("DEL_DOC_LN#")
            outIds = theSalesBiz.GetOutletIdsInUse(lines)
        Else
            outIds = theSalesBiz.GetOutletIdsInUse(Session.SessionID.ToString.Trim)
        End If

        Return outIds
    End Function

    ''' <summary>
    ''' Generic rules to be applied when determining if the grids (POs, SHOWROOM, WAREHOUSE, OUTLET)
    ''' should display hyperlinks rather than plain text
    ''' </summary>
    ''' <returns>TRUE if a hyperlink is allowed; FALSE otherwise</returns>
    Private Function AllowHyperlink() As Boolean

        Dim isCallerMainMenu As Boolean = IsReferrerAvailable() AndAlso "MAINMENU" = Request("referrer")

        'ONLY Sales Order Hyperlinks are allowed when coming from the Main Menu page
        Return (Request("LEAD") <> "TRUE" AndAlso
                Not isCallerMainMenu AndAlso
                Request("ROW_ID") <> "NEW" AndAlso
                Session("IP") & "" = "")
    End Function

    ''' <summary>
    ''' Finds out if the inventory grids (SHOWROOM, WAREHOUSE, OUTLET) should display hyperlinks 
    ''' rather than plain text
    ''' </summary>
    ''' <returns>TRUE if a hyperlink is allowed; FALSE otherwise</returns>
    Private Function AllowInventoryHyperlink() As Boolean

        Dim allowLinksInSOM As Boolean = True
        'In SOM, when working with an order that has an ARS enabled written store, 
        'hyperlinks should be allowed only if
        ' - Order has been paid in full
        ' - Order not been paid in full but user has security to override the paid in full condition
        If (IsReferrerAvailable() AndAlso "SOM" = (Request("referrer")) AndAlso
            Not IsNothing(Request("allowres")) AndAlso
            (Request("allowres") + "").isNotEmpty() AndAlso
             Request("allowres") = "False") Then

            allowLinksInSOM = False
        End If
        Return (allowLinksInSOM AndAlso AllowHyperlink())

    End Function

    ''' <summary>
    ''' Finds out if the POs grid should display hyperlinks rather than plain texts
    ''' </summary>
    ''' <returns>TRUE if a hyperlink is allowed; FALSE otherwise</returns>
    Private Function AllowPurchaseOrderHyperlink() As Boolean

        Return ("Y" = ConfigurationManager.AppSettings("link_po") AndAlso AllowHyperlink())
    End Function

    ''' <summary>
    ''' Finds out if the Sales Order grid should display hyperlinks rather than plain text
    ''' ---- Sales Orders hyperlinks will only be allowed when this page opens from the Main Menu ----
    ''' </summary>
    ''' <returns>TRUE if a hyperlink is allowed; FALSE otherwise</returns>
    Private Function AllowSalesOrderHyperlink() As Boolean

        'ONLY when window opened from Main Menu and User has SOM security
        Return (IsReferrerAvailable() AndAlso "MAINMENU" = (Request("referrer")) AndAlso
                ("Y" = Find_Security("SOM", Session("emp_cd"))))
    End Function

    ''' <summary>
    ''' Finds out if the page was called with a 'Referrer' value in the query string.
    ''' </summary>
    ''' <returns>TRUE, if a referrer value is present; FALSE otherwise</returns>
    Private Function IsReferrerAvailable() As Boolean
        Return (Not IsNothing(Request("referrer")) AndAlso (Request("referrer") + "").isNotEmpty())
    End Function

    ''' <summary>
    ''' Retrieves the Serial numbers and binds the results to the grid component displayed in the Serial Numbers popup
    ''' </summary>
    ''' <param name="selRowId">the ID corresponding to the line being modified, indicates the row being processed</param>
    ''' <param name="itemCd">the item code being processed</param>
    ''' <param name="storeCd">store code where item is being allocated from</param>
    ''' <param name="locCd">location code where item is being allocated from</param>
    ''' <returns>TRUE if serial numbers are found for the store and location entered; FALSE otherwise</returns>
    Private Function PromptForSerialNumber(ByVal selRowId As String, ByVal itemCd As String,
                                           ByVal storeCd As String, ByVal locCd As String) As Boolean

        Dim sNumbersFound As Boolean = True
        'makes sure to display only those Serial #s that have not been assigned in the current order
        Dim sNumbersInUse As String = String.Empty
        Dim sNumbersDataSet As DataSet

        If Request("referrer") = "SOM" Then
            Dim lines As DataSet = Session("DEL_DOC_LN#")
            sNumbersInUse = theSalesBiz.GetSerialNumbersInUse(lines)
        Else
            sNumbersInUse = theSalesBiz.GetSerialNumbersInUse(Session.SessionID.ToString.Trim)
        End If
        sNumbersDataSet = theInvBiz.GetSerialNumbersAvailable(itemCd, storeCd, locCd, sNumbersInUse)

        lbl_ser_row_id.Text = selRowId
        lbl_ser_itm_cd.Text = itemCd
        lbl_ser_loc_cd.Text = locCd
        lbl_ser_store_cd.Text = storeCd
        ASPxPopupControl7.ShowOnPageLoad = True
        ASPxGridView1.DataSource = sNumbersDataSet
        ASPxGridView1.ValueField = "SER_NUM"
        ASPxGridView1.TextField = "SER_NUM"
        ASPxGridView1.DataBind()

        If sNumbersDataSet.Tables(0).Rows.Count < 1 Then
            lbl_ser_err.Text = "No items were found for the selected store and location."
            sNumbersFound = False
        End If
        Return sNumbersFound

    End Function

    ' === Called when the button 'View Availability' is pressed
    Protected Sub btn_view_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Response.Redirect("InventoryLookup.aspx?referrer=" & Request("referrer") & "&SKU=" & txt_SKU.Text)

    End Sub

    ' === Called when the button 'Lookup Another' is pressed
    Protected Sub btn_refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Response.Redirect("InventoryLookup.aspx?referrer=" & Request("referrer"))

    End Sub

    ' === Called when the button 'Exit Without Selecting Serial #' is pressed
    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ASPxPopupControl7.ShowOnPageLoad = False
        Response.Redirect("InventoryLookup.aspx?exitWOserial=Y&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&store_cd=" & Request("STORE_CD") & "&LOC_CD=" & Request("LOC_CD") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer"))

    End Sub

    ' === Called when the button 'Use Selected Serial #' is pressed
    Protected Sub ASPxButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Response.Redirect("InventoryLookup.aspx?ser_num=" & ASPxGridView1.Value & "&process=TRUE&store_cd=" & Request("STORE_CD") & "&LOC_CD=" & Request("LOC_CD") & "&DEL_DOC_NUM=" & Request("del_doc_num") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & "&ser_tp=" & Request("ser_tp"))

    End Sub

    ' === Called when a row gets created for the SALES ORDER Grid
    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView1.HtmlRowCreated
        'May 03 disable link for Franchise
        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return
        If e.GetValue("DEL_DOC_NUM").ToString() & "" <> "" Then
            Dim hpl_del_doc As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_del_doc"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            Dim lbl_del_doc As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_del_doc"), DevExpress.Web.ASPxEditors.ASPxLabel)

            hpl_del_doc.Visible = False
            lbl_del_doc.Visible = True
            lbl_del_doc.Text = e.GetValue("DEL_DOC_NUM").ToString()
        End If
        ' End Daniela
        'If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return

        'If e.GetValue("DEL_DOC_NUM").ToString() & "" <> "" Then
        '    Dim hpl_del_doc As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_del_doc"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
        '    Dim lbl_del_doc As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView1.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_del_doc"), DevExpress.Web.ASPxEditors.ASPxLabel)

        '    If (Not IsNothing(hpl_del_doc) AndAlso setSOsHyperlink) Then
        '        lbl_del_doc.Visible = False
        '        hpl_del_doc.Visible = True
        '        hpl_del_doc.Text = e.GetValue("DEL_DOC_NUM").ToString()
        '        hpl_del_doc.NavigateUrl = "~/SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & HttpUtility.UrlEncode(e.GetValue("DEL_DOC_NUM").ToString())

        '    ElseIf Not IsNothing(lbl_del_doc) Then
        '        hpl_del_doc.Visible = False
        '        lbl_del_doc.Visible = True
        '        lbl_del_doc.Text = e.GetValue("DEL_DOC_NUM").ToString()
        '    End If
        'End If
    End Sub

    ' === Called when a row gets created for the POs Grid
    Protected Sub GridView2_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return

        If IsNumeric(e.GetValue("TOTAL_INCOMING").ToString()) Then
            Dim hpl_po_reserve As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_po_reserve"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            Dim lbl_po_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_po_cd"), DevExpress.Web.ASPxEditors.ASPxLabel)

            If (Not IsNothing(hpl_po_reserve) AndAlso setPOsHyperlink) Then
                lbl_po_cd.Visible = False
                hpl_po_reserve.Visible = True
                hpl_po_reserve.Text = e.GetValue("PO_CD").ToString()
                hpl_po_reserve.NavigateUrl = "InventoryLookup.aspx?process=TRUE&po_cd=" & e.GetValue("PO_CD").ToString() & "&po_ln=" & e.GetValue("LN#").ToString() & "&po_dt=" & e.GetValue("ARRIVAL_DT").ToString & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & "&DEL_DOC_NUM=" & Request("del_doc_num")
            ElseIf Not IsNothing(lbl_po_cd) Then
                hpl_po_reserve.Visible = False
                lbl_po_cd.Visible = True
                lbl_po_cd.Text = e.GetValue("PO_CD").ToString()
            End If
        End If
    End Sub

    ' === Called when a row gets created for the SHOWROOM (aka Store) Grid
    Protected Sub GridView3_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView3.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return

        If IsNumeric(e.GetValue("QTY").ToString()) Then
            Dim hpl_store_reserve As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(GridView3.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_store_reserve"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            Dim lbl_store_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView3.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_store_cd"), DevExpress.Web.ASPxEditors.ASPxLabel)

            If (Not IsNothing(hpl_store_reserve) AndAlso setInvHyperlink) Then
                lbl_store_cd.Visible = False
                hpl_store_reserve.Visible = True
                hpl_store_reserve.Text = e.GetValue("STORE_CD").ToString()
                hpl_store_reserve.NavigateUrl = "InventoryLookup.aspx?process=TRUE&store_cd=" & e.GetValue("STORE_CD").ToString() & "&LOC_CD=" & e.GetValue("LOC_CD").ToString() & "&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & "&ser_tp=" & lbl_ser_tp.Text

            ElseIf Not IsNothing(lbl_store_cd) Then
                hpl_store_reserve.Visible = False
                lbl_store_cd.Visible = True
                lbl_store_cd.Text = e.GetValue("STORE_CD").ToString()
            End If
        End If

    End Sub

    ' === Called when a row gets created for the WAREHOUSE (aka Stock Available) Grid
    Protected Sub GridView4_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView4.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return

        If IsNumeric(e.GetValue("QTY").ToString()) Then
            Dim hpl_store_reserve As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(GridView4.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_store_reserve"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            Dim lbl_store_cd As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView4.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_store_cd"), DevExpress.Web.ASPxEditors.ASPxLabel)

            If (Not IsNothing(hpl_store_reserve) AndAlso setInvHyperlink) Then
                lbl_store_cd.Visible = False
                hpl_store_reserve.Visible = True
                hpl_store_reserve.Text = e.GetValue("STORE_CD").ToString()
                hpl_store_reserve.NavigateUrl = "InventoryLookup.aspx?process=TRUE&store_cd=" & e.GetValue("STORE_CD").ToString() & "&LOC_CD=" & e.GetValue("LOC_CD").ToString() & "&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & "&ser_tp=" & lbl_ser_tp.Text

            ElseIf Not IsNothing(lbl_store_cd) Then
                hpl_store_reserve.Visible = False
                lbl_store_cd.Visible = True
                lbl_store_cd.Text = e.GetValue("STORE_CD").ToString()
            End If
        End If

    End Sub

    ' === Called when a row gets created for the OUTLET Grid
    Protected Sub ASPxGridView3_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView3.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return

        If IsNumeric(e.GetValue("QTY").ToString()) Then
            Dim hpl_outlet_reserve As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView3.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_outlet_reserve"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            Dim lbl_outlet_id As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(ASPxGridView3.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_outlet_id"), DevExpress.Web.ASPxEditors.ASPxLabel)

            If (Not IsNothing(hpl_outlet_reserve) AndAlso setInvHyperlink) Then

                Dim outletIdToExclude As String = GetOutletIDsInUse()
                If InStr(outletIdToExclude, e.GetValue("ID_CD").ToString()) > 0 Then
                    hpl_outlet_reserve.Visible = False
                    lbl_outlet_id.Visible = True
                    lbl_outlet_id.Text = e.GetValue("ID_CD").ToString()
                Else
                    lbl_outlet_id.Visible = False
                    hpl_outlet_reserve.Visible = True
                    hpl_outlet_reserve.Text = e.GetValue("ID_CD").ToString()
                    hpl_outlet_reserve.NavigateUrl = "InventoryLookup.aspx?process=TRUE&store_cd=" & e.GetValue("STORE_CD").ToString() & "&LOC_CD=" & e.GetValue("LOC_CD").ToString() & "&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & "&OUT_ID=" & e.GetValue("ID_CD").ToString() & "&SER_NUM=" & e.GetValue("SER_NUM").ToString() & "&COMM_CD=" & e.GetValue("OUT_COMM_CD").ToString() & "&SPIFF=" & e.GetValue("SPIFF").ToString()
                End If

            ElseIf Not IsNothing(lbl_outlet_id) Then
                hpl_outlet_reserve.Visible = False
                lbl_outlet_id.Visible = True
                lbl_outlet_id.Text = e.GetValue("ID_CD").ToString()

            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Request("INV_SELECTION") <> "Y" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        Else
            Me.MasterPageFile = "~/Blank.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Public Function GetVendorData(ByVal itmCd As String) As DataSet

        Dim VendorRecords As New DataSet
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim connString As String = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand()
        dbCommand.Connection = objConnection

        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()
        Dim emp_cd As String = Session("emp_init") ' in order not to add emp_init which gives error
        ' Dim emp_init As String = Session("emp_init")

        If isSuperUser(emp_cd) = "Y" Then
            sql.Append(" SELECT distinct vsg.whs_id RDC, e.whs_qty_dt as_of_dt, e.whs_qty_oh as Qty")
            sql.Append(" FROM VE_INV2STORE_GRP vsg,  edi_846ib e")
            sql.Append(" WHERE vsg.whs_id = e.whs_id And e.itm_cd=:itmCd And ops$daemon.std_multi_co.isvalidstrgrp( :emp_cd ,vsg.store_grp_cd)='Y' And nvl(e.qty_on_hnd,0) > 0")

        Else
            sql.Append(" SELECT distinct vsg.whs_id RDC, e.whs_qty_dt as_of_dt, e.whs_qty_oh as Qty")
            sql.Append(" FROM VE_INV2STORE_GRP vsg,  edi_846ib e")
            sql.Append(" WHERE vsg.whs_id = e.whs_id And e.itm_cd=:itmCd And ops$daemon.std_multi_co.isvalidstrgrp(:emp_cd,vsg.store_grp_cd)='Y'")
            sql.Append(" And vsg.whs_id in( select v.whs_id from emp e inner join store_grp$store g on e.home_store_cd=g.store_cd inner join VE_INV2STORE_GRP v on g.store_grp_cd = v.store_grp_cd where e.emp_cd='" & Session("emp_cd") & "')")
            sql.Append(" And nvl(e.qty_on_hnd,0) > 0")

        End If

        Try

            dbCommand.CommandText = UCase(sql.ToString)
            dbCommand.CommandType = CommandType.Text
            dbCommand.Parameters.Add(":itmCd", OracleType.VarChar)
            dbCommand.Parameters(":itmCd").Value = itmCd
            dbCommand.Parameters.Add(":emp_cd", OracleType.VarChar)
            dbCommand.Parameters(":emp_cd").Value = emp_cd
            '  dbCommand.Parameters.Add(":emp_init", OracleType.VarChar)
            '  dbCommand.Parameters(":emp_init").Value = emp_init

            dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCommand)
            dbAdapter.Fill(VendorRecords)
            dbAdapter.Dispose()

        Catch ex As Exception
            Throw
        Finally
            dbCommand.Cancel()
            dbCommand.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try
        Return VendorRecords

    End Function

    Public Shared Function isSuperUser(ByRef p_emp_init As String) As String
        Dim p_sup_flag As String = "N"
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_role.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_init", OracleType.VarChar)).Value = p_emp_init
        '  myCMD.Parameters.Add("p_co_grp_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        ' myCMD.Parameters.Add("p_co_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        ' myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue




        Try

            myCMD.ExecuteNonQuery()

            If Not IsDBNull(myCMD.Parameters("p_ind").Value) Then
                p_sup_flag = myCMD.Parameters("p_ind").Value
            End If

        Catch x As Exception
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_sup_flag
    End Function
End Class

