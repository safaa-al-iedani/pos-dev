<%@ Page Language="VB" Explicit="True" Buffer="True" Debug="True" Trace="False" Inherits="InventoryResults"
    CodeFile="InventoryResults.aspx.vb" EnableSessionState="True" MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:DataGrid ID="MyDataGrid" runat="server" Width="99%" BorderColor="Black" CellPadding="3"
        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanged="MyDataGrid_Page"
        OnSortCommand="MyDataGrid_Sort" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
        DataKeyField="itm_cd" Height="16px" PageSize="10" AlternatingItemStyle-BackColor="Beige">
        <Columns>
            <asp:TemplateColumn>
                <%--<HeaderTemplate>
                    <asp:CheckBox ID="CheckAll" runat="server" CssClass="style5" />
                    <font face="Webdings" color="white" size="4">a</font>
                </HeaderTemplate>--%>
                <ItemTemplate>
                    <asp:CheckBox ID="SelectThis" runat="server" CssClass="style5" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label550 %>" SortExpression="itm_cd asc" DataField="itm_cd" />
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label640 %>" SortExpression="ve_cd asc" DataField="ve_cd" />
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label293 %>" SortExpression="mnr_cd asc" DataField="mnr_cd" />
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label70 %>" SortExpression="cat_cd asc" DataField="cat_cd"
                Visible="False" />
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label427 %>" SortExpression="ret_prc asc" DataField="ret_prc"
                DataFormatString="{0:N2}" />
            <asp:BoundColumn HeaderText="<%$ Resources:LibResources, Label653 %>" SortExpression="VSN asc" DataField="VSN" />
            <asp:BoundColumn DataField="des" HeaderText="<%$ Resources:LibResources, Label158 %>" SortExpression="des asc"></asp:BoundColumn>
            <asp:BoundColumn DataField="inventory" HeaderText="ITM_TP_CD" Visible="False"></asp:BoundColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    WHS
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl_avail_qty" runat="server" Text="0" CssClass="style5"></asp:Label><br />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
                <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    SHW
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl_shw_qty" runat="server" Text="0" CssClass="style5"></asp:Label><br />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
                <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label418 %>" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Image runat="server" src="images/icons/tick.png" height="20" width="20" ID="po_img"></asp:Image><br />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
                <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
        <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" />
        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" ForeColor="Black" />
    </asp:DataGrid>
    <dx:ASPxLabel ID="lbl_warning" runat="server" Text="" Width="100%">
    </dx:ASPxLabel>
    <br />
    <table>
        <tr>
            <td valign="top">
                <dx:ASPxButton ID="Confirm" OnClick="movenextorder" runat="server" Text="Move To Order Screen">
                </dx:ASPxButton>
            </td>
            <td valign="top">
                <dx:ASPxButton ID="ClearAll" OnClick="ClearDataGrid" runat="server" Text="New Search">
                </dx:ASPxButton>
            </td>
            <td valign="top">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label451 %>">
                </dx:ASPxLabel>
            </td>
            <td valign="top">
                <dx:ASPxComboBox ID="cbo_records" AutoPostBack="True" runat="server" OnSelectedIndexChanged="Update_Record_Count" 
                    IncrementalFilteringMode ="StartsWith" Width="57px">
                    <Items>
                        <dx:ListEditItem Selected="True" Text="10" Value="10" />
                        <dx:ListEditItem Text="25" Value="25" />
                        <dx:ListEditItem Text="50" Value="50" />
                        <dx:ListEditItem Text="100" Value="100" />
                        <dx:ListEditItem Text="9999" Value="9999" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <span id="OutputMsg" runat="server" enableviewstate="false" />
    <br />
</asp:Content>
