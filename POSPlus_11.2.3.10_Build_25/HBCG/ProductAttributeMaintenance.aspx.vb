﻿Imports HBCG_Utils
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Text

Partial Class ProductAttributeMaintenance
    Inherits POSBasePage
'    Inherits System.Web.UI.Page

    Dim footer_message As String
    Public Const gs_version As String = "Version 1.0"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbl_versionInfo.Text = gs_version
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Search button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click

        lbl_message.Text = ""
        footer_message = ""

        GV_product_attribute.DataBind()
        GV_product_attribute.CancelEdit()
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To Bind Product Attribute Grid whenever required
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GV_Product_Attribute_DataBind()
        Dim conn As New OracleConnection
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("SELECT rowid, ")
        sqlString.Append("attribute_code, ")
        sqlString.Append("attribute_description, ")
        sqlString.Append("attribute_description_fr ")
        sqlString.Append("FROM product_attributes ")

        If String.IsNullOrEmpty(txt_attr_cd.Text) = False Then
            sqlString.Append("WHERE attribute_code LIKE :p_attribute_code ")
        End If

        Dim ds As New DataSet
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()

        If String.IsNullOrEmpty(txt_attr_cd.Text) = False Then
            objcmd.Parameters.Add(":p_attribute_code", OracleType.VarChar)
            objcmd.Parameters(":p_attribute_code").Value = txt_attr_cd.Text.ToUpper
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)

        Try
            conn.Open()
            objAdaptor.Fill(ds)
            GV_product_attribute.DataSource = ds
            conn.Close()
            GV_product_attribute.Enabled = True
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Created by    : KUMARAN
    ''' Dreated Date  : Nov 2015
    ''' Description   : To show success message in Grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_Footer_label_init(sender As Object, e As EventArgs)
        Dim gv_footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = CType(sender, DevExpress.Web.ASPxEditors.ASPxLabel)
        gv_footer_label.Text = footer_message
    End Sub

    ''' <summary>
    ''' Created by    : KUMARAN
    ''' Dreated Date  : Nov 2015
    ''' Description   : Grid Row Update Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_row_update(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        'Dim objResult As Object

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If String.IsNullOrEmpty(e.NewValues("ATTRIBUTE_CODE")) Then
            Throw New Exception("Attribute code must be entered")
        End If

        If e.OldValues("ATTRIBUTE_CODE").ToString().ToUpper() <> e.NewValues("ATTRIBUTE_CODE").ToString().ToUpper() Then
            check_duplicate(e.NewValues("ATTRIBUTE_CODE").ToString().ToUpper())
        End If

        conn.Open()

        sqlString = New StringBuilder("UPDATE product_attributes SET ")
        sqlString.Append("attribute_code = :p_attribute_code, ")
        sqlString.Append("attribute_description = :p_english_description, ")
        sqlString.Append("attribute_description_fr = UPPER(:p_french_description) ")
        sqlString.Append("WHERE rowid = :p_rowid ")

        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_attribute_code", OracleType.VarChar)
        objCmd.Parameters(":p_attribute_code").Value = e.NewValues("ATTRIBUTE_CODE").ToString().ToUpper()

        objCmd.Parameters.Add(":p_english_description", OracleType.VarChar)
        If IsNothing(e.NewValues("ATTRIBUTE_DESCRIPTION")) Then
            objCmd.Parameters(":p_english_description").Value = " "
        Else
            objCmd.Parameters(":p_english_description").Value = e.NewValues("ATTRIBUTE_DESCRIPTION").ToString().ToUpper()
        End If

        objCmd.Parameters.Add(":p_french_description", OracleType.VarChar)
        If IsNothing(e.NewValues("ATTRIBUTE_DESCRIPTION_FR")) Then
            objCmd.Parameters(":p_french_description").Value = " "
        Else
            objCmd.Parameters(":p_french_description").Value = e.NewValues("ATTRIBUTE_DESCRIPTION_FR").ToString().ToUpper()
        End If

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Product Attribute updated successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try


        GV_product_attribute.CancelEdit()
        e.Cancel = True
        GV_product_attribute.DataBind()
    End Sub

    ''' <summary>
    ''' Created by    : KUMARAN
    ''' Dreated Date  : Nov 2015
    ''' Description   : Grid Row Insert Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_row_insert(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If String.IsNullOrEmpty(e.NewValues("ATTRIBUTE_CODE")) Then
            Throw New Exception("Attribute code must be entered")
        End If

        check_duplicate(e.NewValues("ATTRIBUTE_CODE").ToString().ToUpper())

        conn.Open()

        sqlString = New StringBuilder("INSERT INTO product_attributes (attribute_code, attribute_description, attribute_description_fr) VALUES ( ")
        sqlString.Append(":p_attribute_code, :p_english_description, UPPER(:p_french_description) ) ")


        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_attribute_code", OracleType.VarChar)
        objCmd.Parameters(":p_attribute_code").Value = e.NewValues("ATTRIBUTE_CODE").ToString().ToUpper()

        objCmd.Parameters.Add(":p_english_description", OracleType.VarChar)
        If IsNothing(e.NewValues("ATTRIBUTE_DESCRIPTION")) Then
            objCmd.Parameters(":p_english_description").Value = ""
        Else
            objCmd.Parameters(":p_english_description").Value = e.NewValues("ATTRIBUTE_DESCRIPTION").ToString().ToUpper()
        End If

        objCmd.Parameters.Add(":p_french_description", OracleType.VarChar)
        If IsNothing(e.NewValues("ATTRIBUTE_DESCRIPTION_FR")) Then
            objCmd.Parameters(":p_french_description").Value = ""
        Else
            objCmd.Parameters(":p_french_description").Value = e.NewValues("ATTRIBUTE_DESCRIPTION_FR").ToString().ToUpper()
        End If

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Product Attribute Added Successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_product_attribute.CancelEdit()
        e.Cancel = True
        GV_product_attribute.DataBind()
    End Sub

    ''' <summary>
    '''  Created by    : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Grid Row Delete Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GV_row_delete(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)

        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("DELETE FROM product_attributes WHERE ROWID = :p_rowid ")

        objCmd = DisposablesManager.BuildOracleCommand()
        objCmd.Connection = conn

        conn.Open()
        objCmd.CommandText = sqlString.ToString()
        objCmd.CommandType = CommandType.Text
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_rowid", OracleType.VarChar)
        objCmd.Parameters(":p_rowid").Value = e.Keys(0)

        Try
            objCmd.ExecuteNonQuery()
            conn.Close()
            footer_message = "Product Attribute deleted successfully"
        Catch ex As Exception
            conn.Close()
            footer_message = "ERRORS WERE ENCOUNTERED!"
            Throw
        End Try

        GV_product_attribute.CancelEdit()
        e.Cancel = True
        GV_product_attribute.DataBind()
    End Sub

    ''' <summary>
    ''' Created by    : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : Grid Row New/Edit Event Handler.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GVCellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        'If GV_product_attribute.IsNewRowEditing Then
        'Return
        'End If
        If e.Column.FieldName = "ATTRIBUTE_CODE" Then
            e.Editor.ReadOnly = False
        End If
        If e.Column.FieldName = "ATTRIBUTE_DESCRIPTION" Then
            e.Editor.ReadOnly = False
        End If
        If e.Column.FieldName = "ATTRIBUTE_DESCRIPTION_FR" Then
            e.Editor.ReadOnly = False
        End If
    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To validate attribute code
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub check_duplicate(attr_cd As String)
        Dim conn As New OracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim objValidate As Object
        Dim sqlString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        conn.Open()
        objCmd.Connection = conn
        objCmd.CommandType = CommandType.Text

        sqlString = New StringBuilder(" SELECT attribute_code FROM product_attributes ")
        sqlString.Append("WHERE attribute_code = UPPER(:p_attribute_code)")

        objCmd.CommandText = sqlString.ToString()
        objCmd.Parameters.Clear()

        objCmd.Parameters.Add(":p_attribute_code", OracleType.VarChar)
        objCmd.Parameters(":p_attribute_code").Value = attr_cd

        objValidate = objCmd.ExecuteScalar()
        If objValidate <> Nothing Then
            Throw New Exception("Attribute code must be unique")
        End If
        conn.Close()
    End Sub

End Class



