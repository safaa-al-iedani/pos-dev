<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Related_Maint.aspx.vb" Inherits="Related_Maint" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
    </div>
    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Add New" /><br />
    <asp:DataGrid Style="position: relative; top: 9px" ID="Gridview1" runat="server"
        Width="668px" Height="156px" Font-Underline="False" Font-Strikeout="False" Font-Overline="False"
        Font-Italic="False" Font-Bold="False" DataKeyField="REL_CD" CellPadding="2" AutoGenerateColumns="False">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
            Font-Bold="True" ForeColor="Black"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="rel_tp" HeaderText="Type" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="rel_cd" HeaderText="Major/Minor Code" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="itm_cd" HeaderText="SKU" ReadOnly="True">
                <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" />
            </asp:BoundColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="Delete">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
        </Columns>
    </asp:DataGrid>&nbsp;<br />
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" Height="129px" Width="366px"
        CloseAction="CloseButton" HeaderText="Add New Related SKU" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                &nbsp;Add New:<br />
                <table width="100%">
                    <tr>
                        <td>
                            Major/Minor:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_mjr_mnr" runat="server" Enabled="True">
                                <asp:ListItem Text="Major Code" Value="MJR"></asp:ListItem>
                                <asp:ListItem Text="Minor Code" Value="MNR"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Major/Minor Code:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_mjr_mnr" runat="server" Width="129px" MaxLength="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SKU:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_sku" runat="server" Width="129px" MaxLength="9"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add" />
                            <asp:Label ID="lbl_warnings" runat="server" Width="287px"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
