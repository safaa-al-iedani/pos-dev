<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" CodeFile="ElectronicCreditApp.aspx.vb" Inherits="ElectronicCreditApp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <div style="margin-top: 1px; margin-left: 30px">
            &nbsp;<br />
                
            <table style="position: relative; top: 0px" class="style5">
               <tr>
                   <td  style="width: 120px">
                        <asp:Label ID="lblProvider" runat="server" Text="Provider:"></asp:Label>
                    </td>
                    <td  style="height: 21px; width: 165px;">                                               
                        <asp:DropDownList ID="cboProviders" runat="server" AutoPostBack="True"   CssClass="style5" 
                            AppendDataBoundItems="True" Width="134px" TabIndex="1">
                        </asp:DropDownList>
                    </td>
               </tr>

               <tr>
                    <td  style="width: 120px">
                        <asp:Label ID="lblCustCode" runat="server" Text="Customer Code:" ></asp:Label>
                    </td>
                    <td  style="width: 165px">                       
                         <asp:TextBox ID="txt_cust_cd" runat="server" Width="129px" CssClass="style5" MaxLength="12" TabIndex="2"></asp:TextBox>
                    </td>
                     <td colspan="3" style="width: 165px">
                        <asp:Button ID="btn_lookup" runat="server" Text="Lookup Customer" Width="129px" CssClass="style5"  TabIndex="3" CausesValidation="False" />
                      </td>  
                </tr>

               <tr>
                    <td  style="width: 120px">
                        <asp:Label ID="lblAppType" runat="server" Text="Application Type:"></asp:Label>
                    </td>
                    <td  style="width: 165px">
                        <asp:DropDownList ID="cbo_app_type" runat="server" Width ="134px" CssClass="style5"  TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="cbo_app_type_SelectedIndexChanged">
                            <asp:ListItem Value="1">Single</asp:ListItem>
                            <asp:ListItem Value="2">Joint</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>

               <tr>
                    <td  style="width: 120px">
                        <asp:Label ID="lblAmt" runat="server" Text="Requested Amt:"></asp:Label>
                    </td>
                    <td  style="width: 165px">                         
                         <asp:TextBox ID="txt_req_amt" runat="server" CssClass="style5" MaxLength="6" Width="129px" TabIndex="5"></asp:TextBox>
                         <br /><asp:CompareValidator ID="CurrencyValidator"  runat="server" ControlToValidate="txt_req_amt"   Operator="DataTypeCheck"  Type="Integer" ErrorMessage="Please enter an amount without a decimal." Display="Dynamic"/> 
                         <asp:RequiredFieldValidator ID="RequiredAmtValidator" runat="server" ErrorMessage=" Amount is required." ControlToValidate="txt_req_amt"  Display="Dynamic" ></asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                          <td nowrap style="width: 120px"> &nbsp;</td>
               </tr>

                <tr>
                    <td nowrap style="width: 120px">
                        &nbsp;
                    </td>
                    <td colspan="3" style="width: 165px; text-align: left;">
                        <span style="color: #000000">Primary Applicant </span>
                    </td>
                    <td colspan="3" style="width: 165px; text-align: left;">
                        <span style="color: #000000">Co-Applicant</span>
                    </td>
                </tr>
                <tr>
                    <td nowrap style="width: 120px">
                        First Name:
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_fname" runat="server" Width="129px" CssClass="style5" MaxLength="30" TabIndex="6"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredFNameFieldValidator" runat="server" ErrorMessage="First Name is required" ControlToValidate="txt_fname"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_fname" runat="server" CssClass="style5" MaxLength="15" Width="129px" TabIndex="40"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredCoFNameValidator" runat="server" ErrorMessage="First Name is required." ControlToValidate="txt_co_fname"  Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                </tr>
                <tr>
                    <td nowrap style="width: 120px">
                        Middle Init:
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_middle_init" runat="server" Width="13px" CssClass="style5" MaxLength="30" TabIndex="7"></asp:TextBox></td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_middle_init" runat="server" CssClass="style5" MaxLength="15" Width="13px" TabIndex="41"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Last Name:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_lname" runat="server" Width="129px" CssClass="style5" MaxLength="30" TabIndex="8"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredLNameFieldValidator" runat="server" ErrorMessage="Last Name is required." ControlToValidate="txt_lname"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_lname" runat="server" CssClass="style5" MaxLength="30" Width="129px" TabIndex="42"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredCoLNameFieldValidator" runat="server" ErrorMessage="Last Name is required." ControlToValidate="txt_co_lname"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Address1:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_addr1" runat="server" Width="128px" CssClass="style5" MaxLength="30" TabIndex="9" ></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredAddr1FieldValidator" runat="server" ErrorMessage="Address is required." ControlToValidate="txt_addr1"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_addr1" runat="server" CssClass="style5" MaxLength="30" Width="128px" TabIndex="43"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredCoAddr1FieldValidator" runat="server" ErrorMessage="Address is required." ControlToValidate="txt_co_addr1"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px; height: 23px;">
                        Address2:</td>
                    <td colspan="3" style="width: 165px; height: 23px;">
                        <asp:TextBox ID="txt_addr2" runat="server" Width="128px" CssClass="style5" MaxLength="30"  TabIndex="10"></asp:TextBox></td>
                    <td colspan="3" style="width: 165px; height: 23px;">
                        <asp:TextBox ID="txt_co_addr2" runat="server" CssClass="style5" MaxLength="30" Width="128px"  TabIndex="44"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        City:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_city" runat="server" Width="129px" CssClass="style5" MaxLength="30" TabIndex="11"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredCityFieldValidator" runat="server" ErrorMessage="City is required." ControlToValidate="txt_city"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_City" runat="server" CssClass="style5" MaxLength="30" Width="129px" TabIndex="45"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredCoCityFieldValidator" runat="server" ErrorMessage="City is required." ControlToValidate="txt_co_City"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        State:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_state" runat="server" Width="20px" CssClass="style5" MaxLength="5" TabIndex="12"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredStateFieldValidator" runat="server" ErrorMessage="State is required." ControlToValidate="txt_state"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>

                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_state" runat="server" CssClass="style5" MaxLength="5" Width="20px" TabIndex="46"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredCoStateFieldValidator" runat="server" ErrorMessage="State is required." ControlToValidate="txt_co_state"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Zip Code:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_zip" runat="server" Width="55px" CssClass="style5" MaxLength="5" TabIndex="13"></asp:TextBox>
                        <asp:RegularExpressionValidator id="ZipCodeExpressionValidator" runat="server" ValidationExpression="\d{5}"  ErrorMessage="ZIP code must be 5 digits" ControlToValidate="txt_zip" Display="Dynamic" />
                        <asp:RequiredFieldValidator ID="RequiredZipFieldValidator" runat="server" ErrorMessage="ZIP is required." ControlToValidate="txt_zip"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_zip" runat="server" CssClass="style5" MaxLength="5" Width="55px" TabIndex="47"></asp:TextBox>    
                         <asp:RegularExpressionValidator id="CoZipCodeExpressionValidator" runat="server" ValidationExpression="\d{5}"  ErrorMessage="ZIP code must be 5 digits" ControlToValidate="txt_co_zip" Display="Dynamic" />
                         <asp:RequiredFieldValidator ID="RequiredCoZipFieldValidator" runat="server" ErrorMessage="ZIP is required." ControlToValidate="txt_co_zip"  Display="Dynamic"></asp:RequiredFieldValidator>                
                   </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Home Phone:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_home_phone" runat="server" CssClass="style5" MaxLength="10" Width="131px" TabIndex="14"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredHPhoneFieldValidator" runat="server" ErrorMessage="Home Phone is required." ControlToValidate="txt_home_phone"  Display="Dynamic"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="PhoneExpressionValidator" runat="server" ErrorMessage="Phone must be 10 digits." ValidationExpression="\d{10}" ControlToValidate="txt_home_phone" Display="Dynamic" />
                     </td>
                   
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_home_phone" runat="server" CssClass="style5" MaxLength="10" Width="131px" TabIndex="48"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredCoHPhoneFieldValidator" runat="server" ErrorMessage="Home Phone is required." ControlToValidate="txt_co_home_phone"  Display="Dynamic"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="CoPhoneExpressionValidator" runat="server" ErrorMessage="Phone must be 10 digits" ValidationExpression="\d{10}" ControlToValidate="txt_co_home_phone" Display="Dynamic" />
                        </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Alt Phone:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_alt_phone" runat="server" CssClass="style5" MaxLength="10" Width="131px"   TabIndex="15"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="AltPhoneExpressionValidator" runat="server" ErrorMessage="Phone must be 10 digits" ValidationExpression="\d{10}" ControlToValidate="txt_alt_phone" Display="Dynamic" />
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_alt_phone" runat="server" CssClass="style5" MaxLength="10" Width="131px" TabIndex="49"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="CoAltPhoneExpressionValidator" runat="server" ErrorMessage="Phone must be 10 digits" ValidationExpression="\d{10}" ControlToValidate="txt_co_alt_phone" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Birth Date:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_month" runat="server" CssClass="style5" MaxLength="2" Width="16px" TabIndex="16"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_day" runat="server" CssClass="style5" MaxLength="2" Width="16px" TabIndex="17"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_year" runat="server" CssClass="style5" MaxLength="4" Width="32px" TabIndex="18"></asp:TextBox></td>
                    <td colspan="3" style="width: 165px">
                            <asp:TextBox ID="txt_co_month" runat="server" CssClass="style5" MaxLength="2" Width="16px" TabIndex="50"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_co_day" runat="server" CssClass="style5" MaxLength="2" Width="16px" TabIndex="51"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_co_year" runat="server" CssClass="style5" MaxLength="4" Width="32px" TabIndex="52"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 23px; width: 120px;">
                        SSN:</td>
                    <td colspan="3" style="height: 23px; width: 165px;">
                        <asp:TextBox ID="txt_ssn1" runat="server" Width="29px" CssClass="style5" MaxLength="3" TabIndex="19"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_ssn2" runat="server" CssClass="style5" MaxLength="2" Width="21px" TabIndex="20"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_ssn3" runat="server" CssClass="style5" MaxLength="4" Width="46px" TabIndex="21"></asp:TextBox></td>
                    <td colspan="3" style="height: 23px; width: 165px;">
                        <asp:TextBox ID="txt_co_ssn1" runat="server" CssClass="style5" MaxLength="3" Width="29px" TabIndex="53"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_co_ssn2" runat="server" CssClass="style5" MaxLength="2" Width="21px" TabIndex="54"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_co_ssn3" runat="server" CssClass="style5" MaxLength="4" Width="46px" TabIndex="55"></asp:TextBox>                        
                        </td>
                </tr>

                <tr>
                    <td style="width: 120px">
                        Occupation:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_occupation" runat="server" Width ="110px" CssClass="style5" TabIndex="22">
                            <asp:ListItem Value="0">Retired</asp:ListItem>
                            <asp:ListItem Value="1">Self Employed</asp:ListItem>
                            <asp:ListItem Value="2">Other</asp:ListItem>
                        </asp:DropDownList></td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_coapp_occupation" runat="server"  Width ="110px" CssClass="style5" TabIndex="56">
                            <asp:ListItem Value="0">Retired</asp:ListItem>
                            <asp:ListItem Value="1">Self Employed</asp:ListItem>
                            <asp:ListItem Value="2">Other</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>

                <tr>
                  <td style="width: 120px">
                        Employer Name:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_name" runat="server" Width="129px" MaxLength="20" CssClass="style5" TabIndex="23"></asp:TextBox></td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_emp_name" runat="server"  Width="129px" MaxLength="20" CssClass="style5" TabIndex="57"></asp:TextBox></td>
                </tr>

                <tr>
                    <td style="width: 120px">
                        Employer City:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_city" runat="server" Width="129px" MaxLength="20" CssClass="style5" TabIndex="24"></asp:TextBox></td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_emp_city" runat="server"  Width="129px" MaxLength="20" CssClass="style5" TabIndex="58"></asp:TextBox></td>
                </tr>

                <tr>
                    <td style="width: 120px">
                        Employer State:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_emp_state" runat="server" Width="20px" CssClass="style5" MaxLength="2" TabIndex="25"></asp:TextBox>
                    </td>

                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_emp_state" runat="server" CssClass="style5" MaxLength="2" Width="20px" TabIndex="59"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                       Time at Employer:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_time_years" runat="server" Width="19px" CssClass="style5"  MaxLength="2" TabIndex="26"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_emp_time_months" runat="server" CssClass="style5" MaxLength="2" Width="19px" TabIndex="27"></asp:TextBox>
                        months
                        <asp:RangeValidator ID="EmpMonthRangeValidator" runat="server" ErrorMessage="Month must be between 0 and 11." MinimumValue="0" MaximumValue="11" ControlToValidate="txt_emp_time_months" Display="Dynamic" Type="Integer"></asp:RangeValidator>
                    </td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_emp_time_years" runat="server" CssClass="style5" MaxLength="2"  Width="19px" TabIndex="60"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_co_emp_time_months" runat="server" CssClass="style5" MaxLength="2" Width="19px" TabIndex="61"  ControlToValidate="txt_co_emp_time_months"></asp:TextBox>
                        months 
                        <asp:RangeValidator ID="CoEmpMonthRangeValidator" runat="server" ErrorMessage="Month must be between 0 and 11." MinimumValue="0" MaximumValue="11" ControlToValidate="txt_co_emp_time_months" Display="Dynamic" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Employer Phone:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_phone" runat="server" Width="92px" CssClass="style5" MaxLength="10" TabIndex="28"></asp:TextBox></td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_emp_phone" runat="server" CssClass="style5" MaxLength="10" Width="92px" TabIndex="62"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Net Annual Income:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_annual_income" runat="server" Width="70px" CssClass="style5" MaxLength="7" TabIndex="29"></asp:TextBox>
                        <br /><asp:CompareValidator ID="AnnualIncomeValidator"  runat="server" ControlToValidate="txt_annual_income"   Operator="DataTypeCheck"  Type="Integer" ErrorMessage="Please enter an amount without a decimal." Display="Dynamic"/> 
                     </td>                     
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_annual_income"  runat="server"  Width="70px" CssClass="style5" MaxLength="7" TabIndex="63"></asp:TextBox>
                         <br /><asp:CompareValidator ID="CoAppAnnualIncomeValidator"  runat="server" ControlToValidate="txt_co_annual_income"   Operator="DataTypeCheck"  Type="Integer" ErrorMessage="Please enter an amount without a decimal." Display="Dynamic"/> 
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Residence:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_res_status" runat="server" Width ="80px" CssClass="style5" TabIndex="30">
                            <asp:ListItem Value="0">Own</asp:ListItem>
                            <asp:ListItem Value="1">Rent</asp:ListItem>
                            <asp:ListItem Value="2">Parents</asp:ListItem>
                        </asp:DropDownList></td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_co_res_status" runat="server"  Width ="80px" CssClass="style5" TabIndex="64">
                            <asp:ListItem Value="0">Own</asp:ListItem>
                            <asp:ListItem Value="1">Rent</asp:ListItem>
                            <asp:ListItem Value="2">Parents</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Time at Residence:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_time_at_res_years" runat="server" Width="19px" CssClass="style5" MaxLength="2" TabIndex="31"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_time_at_res_months" runat="server" CssClass="style5" MaxLength="2"  Width="19px" TabIndex="32"></asp:TextBox>
                        months</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_time_at_res_years" runat="server" CssClass="style5" MaxLength="2" Width="19px" TabIndex="65"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_co_time_at_res_months" runat="server" CssClass="style5" MaxLength="2" Width="19px"  TabIndex="66"></asp:TextBox>
                        months</td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Total Monthly Income:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_monthly_income" runat="server" Width="70px" CssClass="style5" MaxLength="30" TabIndex="33"></asp:TextBox>
                        <br /><asp:CompareValidator ID="MonthlyIncomeValidator"  runat="server" ControlToValidate="txt_monthly_income"   Operator="DataTypeCheck"  Type="Integer" ErrorMessage="Please enter an amount without a decimal." Display="Dynamic"/> 
                    </td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_co_monthly_income" runat="server" CssClass="style5" MaxLength="30" Width="70px" TabIndex="67"></asp:TextBox>
                        <br /><asp:CompareValidator ID="CoAppMonthlyIncomeValidator"  runat="server" ControlToValidate="txt_co_monthly_income"   Operator="DataTypeCheck"  Type="Integer" ErrorMessage="Please enter an amount without a decimal." Display="Dynamic"/> 
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        DL # / State:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_dl_license_no" runat="server" CssClass="style5" MaxLength="10" Width="94px" TabIndex="34"></asp:TextBox>&nbsp;
                        <asp:TextBox ID="txt_dl_license_state" runat="server" CssClass="style5" MaxLength="2" Width="20px"  TabIndex="35"></asp:TextBox></td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_co_dl_license_no" runat="server" CssClass="style5" MaxLength="10" Width="94px"  TabIndex="68"></asp:TextBox>&nbsp;
                        <asp:TextBox ID="txt_co_dl_license_state" runat="server" CssClass="style5" MaxLength="2"   TabIndex="69" Width="20px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Credit Protection:</td>
                    <td colspan="6" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_credit_protection" runat="server" Width ="80px" CssClass="style5" TabIndex="36">
                             <asp:ListItem Value="Y">Yes</asp:ListItem>
                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Relative Phone:</td>
                    <td colspan="6" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_rel_phone" runat="server" Width="92px" CssClass="style5" MaxLength="10" TabIndex="37"></asp:TextBox>
                        <br /> <asp:RegularExpressionValidator ID="RelPhoneExpressionValidator" runat="server" ErrorMessage="Phone must be 10 digits" ValidationExpression="\d{10}" ControlToValidate="txt_rel_phone" />
                        </td>
                </tr>
            </table>
        </div>
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btn_save_exit" runat="server" TabIndex="70" Text="Submit Application"
            Width="144px" CssClass="style5" /><br />
        &nbsp;
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <asp:Label ID="lbl_response" runat="server" Width="452px"></asp:Label><br />
        <br />



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

