﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vd_inspmt.aspx.vb" Inherits="vd_inspmt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <%-- sabrina R1999 --%>
    <script language="javascript" type="text/javascript">

        function disableButton(id) {
            // Daniela B 20140723 prevent double click
            try {
                var a = document.getElementById(id);
                //a.disabled = true;
                a.style.display = 'none';
            } catch (err) {
                alert('Error in disableButton ' + err.message);
                return false;
            }
            return true;
        }
</script>

<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 344px; width: 837px">
    <table style="height: 243px; width: 608px">
        <tr>
             <td>
             <asp:Label ID="lbl_space" runat="server" Text="" Width="20"></asp:Label>
          </td>
             <td>
         <asp:Label ID="lbl_title" runat="server" Text="VISA DESJARDINS PAYMENT DETAIL" Width="160"></asp:Label>
               </td> 
            
            
             <td>
                  <%-- <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Customer.aspx">Click To Find A Customer</asp:HyperLink> sabrina-french Dec14--%>
                 <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Customer.aspx"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:LibResources, Label88 %>" /></asp:HyperLink>
                  
                 </td>
             </tr>
         <tr>
            <td>
                <%--<asp:Label ID="Label1" runat="server" Text="Customer Code"></asp:Label> sabrina-French Dec14--%>
                <asp:Label ID="Label1" runat="server" text="<%$ Resources:LibResources, Label132 %>"></asp:Label>
        
             </td>
                  <td>
                                
        <asp:TextBox ID="Text_cust_cd" runat="server" AutoPostBack="True"></asp:TextBox>
                 </td>
             </tr>
        
        <tr>
            <td>
      <%--  <asp:Label ID="lbl_acct_num" runat="server" Text="VISA Desjardins Account Number "></asp:Label>--%>
                  <asp:Label ID="lbl_acct_num" runat="server" text="<%$ Resources:LibResources, Label649 %>"></asp:Label>
                </td>
            <td>
          <asp:TextBox ID="Text_acct_num" runat="server" AutoPostBack="True" ></asp:TextBox> 
                </td>
            <%-- sabrina R1999 add button --%>
            <td>
                 <%-- <asp:Button ID="Button3" runat="server" Font-Bold="True" Text="Click To Swipe" usesubmitbehavior="false"/>--%>
                 <%--<asp:Button ID="Button3" runat="server" Font-Bold="True" Text="Click To Swipe" OnClientClick="return disableButton(this.id);"/> sabrina-french Dec14--%>
                <asp:Button ID="Button3" runat="server" Font-Bold="True" Text="<%$ Resources:LibResources, Label839 %>" OnClientClick="return disableButton(this.id);"/>
            </td>
        </tr>
        <tr>
            <td>
            <%--<asp:Label ID="lbl_fname" runat="server" Text="Main Cardholder's First Name"></asp:Label> sabrina-french dec14--%>
                <asp:Label ID="lbl_fname" runat="server" Text="<%$ Resources:LibResources, Label282 %>"></asp:Label>
                </td>
            <td>
        <asp:TextBox ID="Text_fname" runat="server" AutoPostBack="True"></asp:TextBox>
                </td>
         </tr>
        <tr>
            <td>
         <%--<asp:Label ID="lbl_lname" runat="server" Text="Main Cardholder's Last Name"></asp:Label> sabrina-french dec14--%>
                <asp:Label ID="lbl_lname" runat="server" Text="<%$ Resources:LibResources, Label283 %>">"</asp:Label>
                </td>
            <td>
        <asp:TextBox ID="Text_lname" runat="server" AutoPostBack="True"></asp:TextBox>
                </td>
        </tr>
          
        <tr>
            <td>
       <%-- <asp:Label ID="lbl_plan_id" runat="server" Text="Finance Plan Being Paid"></asp:Label> sabrina-french dec14--%>
                 <asp:Label ID="lbl_plan_id" runat="server" Text="<%$ Resources:LibResources, Label206 %>"></asp:Label>
                </td>
            <td>
        <asp:TextBox ID="Text_plan_id" runat="server" AutoPostBack="True"></asp:TextBox>
                </td>
            </tr>
       <tr>
           <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </td>
            <td>

         <asp:Label ID="lbl_err" runat="server" Text=" " ForeColor="#FF3300"></asp:Label>
   </td>
            </tr>
    </table>
       
       <%-- <asp:Button ID="Button1" runat="server" ForeColor="#CC00FF" Text="Exit With the Detail Information" onclick="Button1_Click" OnClientClick="return disableButton(this.id);"/>
        sabrina-french dec14--%>
         <asp:Button ID="Button1" runat="server" ForeColor="#CC00FF" Text="<%$ Resources:LibResources, Label189 %>" onclick="Button1_Click" OnClientClick="return disableButton(this.id);"/>
        
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <%--<asp:Button ID="Button2" runat="server" usesubmitbehavior="false" Text="Exit Without Information" /> sabrina-french dec14--%>
        <asp:Button ID="Button2" runat="server" usesubmitbehavior="false" Text="<%$ Resources:LibResources, Label190 %>" />
        
    </div>
    </form>
</body>
</html>
