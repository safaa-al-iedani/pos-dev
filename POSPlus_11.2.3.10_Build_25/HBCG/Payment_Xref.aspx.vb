Imports System.Data.OracleClient
Imports System.Net.NetworkInformation
Imports HBCG_Utils

Partial Class Payment_Xref
    Inherits POSBasePage
    Dim ds As New DataSet

    Public Sub bindgrid(Optional ByVal store_cd As String = "ALL")
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds2 = New DataSet

        Gridview1.DataSource = ""
        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT STORE_CD, MOP_TP, TRANSPORT, MERCHANT_ID, MOP_CD FROM PAYMENT_XREF "
        If store_cd <> "ALL" Then
            sql = sql & "WHERE store_cd='" & store_cd & "' "
        End If
        sql = sql & "ORDER BY STORE_CD, MOP_TP"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds2)
        mytable = New DataTable
        mytable = ds2.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds2
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
                Gridview1.DataSource = ""
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_filter_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            With cbo_new_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            cbo_filter_store.Items.Insert(0, "ALL STORES")
            cbo_filter_store.Items.FindByText("ALL STORES").Value = "ALL"
            cbo_filter_store.SelectedIndex = 0

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub mop_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT DES || ' (' || MOP_CD || ')' AS DES, MOP_CD FROM MOP ORDER BY DES"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_mop_filter
                .DataSource = ds
                .DataValueField = "mop_cd"
                .DataTextField = "des"
                .DataBind()
            End With
            cbo_mop_filter.Items.Insert(0, "ALL MOP")
            cbo_mop_filter.Items.FindByText("ALL MOP").Value = "ALL"
            cbo_mop_filter.SelectedIndex = 0

            With cbo_mop_cd
                .DataSource = ds
                .DataValueField = "mop_cd"
                .DataTextField = "des"
                .DataBind()
            End With

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("MCM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            Button1.Enabled = False
            cbo_pmt_proc.Enabled = False
            cbo_pmt_tp.Enabled = False
            cbo_new_store.Enabled = False
            cbo_filter_store.Enabled = False
            Exit Sub
        Else
            Button1.Enabled = True
            cbo_pmt_proc.Enabled = True
            cbo_pmt_tp.Enabled = True
            cbo_new_store.Enabled = True
            cbo_filter_store.Enabled = True
        End If

        If Not IsPostBack Then
            bindgrid()
            store_cd_populate()
            mop_cd_populate()
        End If

    End Sub

    Protected Sub cbo_filter_store_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_filter_store.SelectedIndexChanged

        bindgrid(cbo_filter_store.SelectedValue.ToString)

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If txt_merchant_id.Text & "" = "" Then
            lbl_warnings.Text = "You must enter a Merchant ID"
            Exit Sub
        End If
        lbl_warnings.Text = ""

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT MOP_CD FROM PAYMENT_XREF WHERE STORE_CD=:STORE_CD AND MOP_CD=:MOP_CD"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)

        objSql.Parameters(":STORE_CD").Value = cbo_new_store.SelectedValue
        objSql.Parameters(":MOP_CD").Value = cbo_mop_cd.SelectedValue

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                If MyDataReader.Item("MOP_CD").ToString & "" <> "" Then
                    lbl_warnings.Text = "An entry already exists for this MOP Code."
                    conn.Close()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        sql = "INSERT INTO PAYMENT_XREF (STORE_CD, MOP_CD, MOP_TP, TRANSPORT, MERCHANT_ID) "
        sql = sql & "VALUES(:STORE_CD, :MOP_CD, :MOP_TP, :TRANSPORT, :MERCHANT_ID)"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
        objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
        objSql.Parameters.Add(":TRANSPORT", OracleType.VarChar)
        objSql.Parameters.Add(":MERCHANT_ID", OracleType.VarChar)

        objSql.Parameters(":STORE_CD").Value = cbo_new_store.SelectedValue
        objSql.Parameters(":MOP_CD").Value = cbo_mop_cd.SelectedValue
        objSql.Parameters(":MOP_TP").Value = cbo_pmt_tp.SelectedValue
        objSql.Parameters(":TRANSPORT").Value = cbo_pmt_proc.SelectedValue.ToString
        objSql.Parameters(":MERCHANT_ID").Value = txt_merchant_id.Text.ToString

        objSql.ExecuteNonQuery()
        conn.Close()
        bindgrid()

    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String

        conn.Open()
        'deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
        deletecd = e.Item.Cells(0).Text.ToString

        'We need to find any fabric protection if linked to ensure that we delete those records as well.

        With cmdDeleteItems
            .Connection = conn
            .CommandText = "delete from payment_xref where store_cd='" & e.Item.Cells(0).Text.ToString & "' and mop_cd='" & e.Item.Cells(1).Text.ToString & "'"
        End With
        cmdDeleteItems.ExecuteNonQuery()

        cmdDeleteItems.Dispose()
        conn.Close()

        Response.Redirect("payment_xref.aspx")

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Update_MOP_TP()
        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub cbo_mop_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Update_MOP_TP()

    End Sub

    Protected Sub Update_MOP_TP()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim SQL As String
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT MOP_TP FROM MOP WHERE MOP_CD='" & cbo_mop_cd.SelectedValue & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                If MyDataReader.Item("MOP_TP").ToString & "" <> "" Then
                    cbo_pmt_tp.SelectedValue = MyDataReader.Item("MOP_TP").ToString
                End If
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
