<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Email_TransactionLogs.aspx.vb" Inherits="Email_TransactionLogs" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="">
    </dx:ASPxLabel>
    <table>
        <tr>
            <td>
                <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" AutoPostBack="True">
                </dx:ASPxDateEdit>
            </td>
            <td>
                &nbsp;<dx:ASPxLabel ID="ASPxLabel1" runat="server" Text=" through ">
                </dx:ASPxLabel>
                &nbsp;
            </td>
            <td>
                <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" AutoPostBack="true">
                </dx:ASPxDateEdit>
            </td>
        </tr>
    </table>
    <br />
    &nbsp;<br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" Width="220px" DataSourceID="SqlDataSource1">
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ERP %>"
        ProviderName="<%$ ConnectionStrings:ERP.ProviderName %>" SelectCommand='SELECT "EMP_CD", "DEL_DOC_NUM", "EMAIL_TO", "TIMESTAMP", "STORE_CD", "EMAIL_FROM", "EMAIL_CC", "EMAIL_BCC", "EMAIL_SUBJECT", "EMAIL_BODY", "ERR_MESSAGE" FROM "EMAIL_AUDIT" WHERE ("TIMESTAMP" BETWEEN :START_TIMESTAMP AND :END_TIMESTAMP+1) ORDER BY "TIMESTAMP"'>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxDateEdit1" Name="START_TIMESTAMP" PropertyName="Value"
                Type="DateTime" />
            <asp:ControlParameter ControlID="ASPxDateEdit2" Name="END_TIMESTAMP" PropertyName="Value"
                Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
