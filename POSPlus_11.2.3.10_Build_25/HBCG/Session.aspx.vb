
Partial Class Session
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        On Error Resume Next

        Response.Write("<b><u>Session ID:</u></b>  " & Session.SessionID.ToString & "<br /><br />")
        Dim Sname
        For Each Sname In Session.Contents
            Response.Write(Sname & " - " & Session.Contents(Sname) & "<br />")
        Next

        Response.Write(Session("zip_cd") & "- ZIP CODE")
    End Sub

    
End Class
