<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Campaign_Main.aspx.vb" Inherits="Campaign_Campaign_Main" Title="Campaign Selection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <strong><span style="text-decoration: underline">Launch Campaign </span></strong>
    <br />
    <br />
    Please select a campaign: &nbsp;&nbsp;
    <asp:DropDownList ID="cbo_campaign" runat="server" Width="292px" AppendDataBoundItems="true"
        CssClass="style5">
    </asp:DropDownList>
    <br />
    <br />
    <br />
    <asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="style5" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
</asp:Content>
