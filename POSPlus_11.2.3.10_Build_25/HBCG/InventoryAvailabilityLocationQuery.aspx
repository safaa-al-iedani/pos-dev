<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts --%>
<%-- <%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="InventoryAvailabilityLocationQuery.aspx.vb" Inherits="InventoryAvailabilityLocationQuery" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %> --%>
<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="InventoryAvailabilityLocationQuery.aspx.vb" Inherits="InventoryAvailabilityLocationQuery" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends --%>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%--    <br /> --%>
    <table border="0" style="position: relative; width: 1000px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_sku" runat="server" meta:resourcekey="lbl_sku">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_sku" runat="server" Style="position: relative" Width="80px" MaxLength="9"></asp:TextBox>
            </td>

            
            <td class="class_tdlbl" colspan="1" align="right">
              <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdddlb" colspan="1">
                <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true" Width ="340">
                </asp:DropDownList>
            </td>
            <td class="class_tdlbl" style="width: 80px" align="right">
                <dx:ASPxLabel ID="lbl_identification_code" runat="server" Text ="Identification Codes:">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 115px">
                <asp:ListBox ID="lst_identification_code" runat="server" Width="100px" MaxLength="20"  Height ="55"></asp:ListBox>
            </td>
            <td class="class_tdbtn" colspan="1" align="right">
                <dx:ASPxButton ID="btn_Search" runat="server" meta:resourcekey="lbl_btn_search">
                </dx:ASPxButton>
            </td>
            <td class="class_tdbtn" colspan="1" align="right">
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear">
                </dx:ASPxButton>
            </td>
        </tr>
    
        <tr><td colspan="8">
        <hr />
        </td></tr>

        <tr>
            <td class="class_tdlbl" style="width: 75px" align="right">
                <dx:ASPxLabel ID="lbl_vendor_code" runat="server" meta:resourcekey="lbl_vendor_code">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 95px">
                <asp:TextBox ID="txt_vendor_code" runat="server" Width="80px" MaxLength="5"></asp:TextBox>
            </td>         

            <td class="class_tdlbl" style="width: 85px" align="right">
                <dx:ASPxLabel ID="lbl_vsn" runat="server" meta:resourcekey="lbl_vsn">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 365px">
                <asp:TextBox ID="txt_vsn" runat="server" Width="335px" MaxLength="30"></asp:TextBox>
            </td> 

            <td class="class_tdlbl" style="width: 80px" align="right">
                <dx:ASPxLabel ID="lbl_related_itm_cd" runat="server" meta:resourcekey="lbl_related_itm_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 115px">
                <asp:TextBox ID="txt_related_itm_cd" runat="server" Width="100px" MaxLength="20"></asp:TextBox>
            </td>

    
            <td class="class_tdtxt "style="width: 115px" align="right">
                <dx:ASPxLabel ID="lbl_itm_srt_cd" runat="server" meta:resourcekey="lbl_itm_srt_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 70px">
                <asp:DropDownList ID="txt_itm_srt_cd" runat="server" Style="position: relative" AppendDataBoundItems="False">
                </asp:DropDownList>
            </td> 
        </tr>

        <tr>
            <td class="class_tdlbl" colspan="1" align="right"> 
                <dx:ASPxLabel ID="lbl_itm_tp_cd" runat="server" meta:resourcekey="lbl_itm_tp_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_itm_tp_cd" runat="server" Width="80px" MaxLength="5"></asp:TextBox>
            </td> 
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_des" runat="server" meta:resourcekey="lbl_des">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_des" runat="server" Width="335px" MaxLength="30"></asp:TextBox>
            </td> 


            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_comm_cd" runat="server" meta:resourcekey="lbl_comm_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_comm_cd" runat="server" Width="50px" MaxLength="50"></asp:TextBox>
            </td>

            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_drop_cd" runat="server" meta:resourcekey="lbl_drop_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_drop_cd" runat="server" Width="50px" MaxLength="20"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td colspan="8" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>

        <tr><td colspan="8">
        <hr />
        </td></tr>
    </table>

    <table border="0" style="position: center; width: 1000px; left: 0px; top: 0px;">
        <tr>
        <td class="class_tdlbl" style="width: 85px" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" style="width: 85px" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" style="width: 120px" align="center">
            <dx:ASPxLabel ID="lbl_hdr_empty" runat="server" meta:resourcekey="lbl_hdr_empty">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" style="width: 140px" align="center">
            <dx:ASPxLabel ID="lbl_hdr_regular" runat="server" meta:resourcekey="lbl_hdr_regular" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" style="width: 140px" align="center">
            <dx:ASPxLabel ID="lbl_hdr_outlet" runat="server" meta:resourcekey="lbl_hdr_outlet" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" style="width: 140px" align="center">
            <dx:ASPxLabel ID="lbl_hdr_floormodel" runat="server" meta:resourcekey="lbl_hdr_floormodel" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" style="width: 140px" align="center">
            <dx:ASPxLabel ID="lbl_hdr_NAS" runat="server" meta:resourcekey="lbl_hdr_NAS" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" style="width: 150px" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_showroom" runat="server" meta:resourcekey="lbl_showroom" Font-Bold="true" Font-Underline="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_total_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_total_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_total_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_total_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_reserved_showroom" runat="server" meta:resourcekey="lbl_reserved">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_reserved_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_reserved_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_reserved_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_reserved_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_available_showroom" runat="server" meta:resourcekey="lbl_available">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_avail_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_avail_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_avail_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_avail_showroom" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_warehouse" runat="server" meta:resourcekey="lbl_warehouse" Font-Bold="true" Font-Underline="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_total_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_total_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_total_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_total_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_reserved_warehouse" runat="server" meta:resourcekey="lbl_reserved">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_reserved_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_reserved_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_reserved_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_reserved_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_available_warehouse" runat="server" meta:resourcekey="lbl_available">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_avail_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_avail_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_avail_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_avail_warehouse" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr><td colspan="7">
        &nbsp;
        </td></tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_total_store" runat="server" meta:resourcekey="lbl_total_store" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_total_store" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_total_store" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_total_store" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_total_store" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_total_reserved" runat="server" meta:resourcekey="lbl_total_reserved" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_total_reserved" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_total_reserved" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_total_reserved" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_total_reserved" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="right">
            <dx:ASPxLabel ID="lbl_total_available" runat="server" meta:resourcekey="lbl_total_available" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_total_available" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_total_available" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_fm_total_available" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_nas_total_available" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        </tr>

        <tr><td colspan="8">
        <hr />
        </td></tr>

        <tr>
<%--        <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
        <hr />
        </td>
--%>
        <td class="class_tdlbl" colspan="3" align="center">
        &nbsp;
        </td>
        <td class="class_tdlbl" colspan="2" align="center">
        <hr />
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_purchase_orders" runat="server" meta:resourcekey="lbl_hdr_purchase_orders" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="2" align="center">
        <hr />
        </td>
<%--    <td class="class_tdlbl" colspan="1" align="center">
            &nbsp;
        </td>
--%>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_lead_time" runat="server" meta:resourcekey="lbl_hdr_lead_time" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_po_total_reserved" runat="server" meta:resourcekey="lbl_total_reserved" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_po_total_available" runat="server" meta:resourcekey="lbl_total_available" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_po_arrival_date" runat="server" meta:resourcekey="lbl_hdr_arrival_date" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_po_available" runat="server" meta:resourcekey="lbl_hdr_available" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_po_destination_store_cd" runat="server" meta:resourcekey="lbl_hdr_po_destination_store_cd" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_lead_time_to_store" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_po_total_reserved" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_po_total_available" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_po_arrival_date_soonest" runat="server" Width="80px" MaxLength="10"></asp:TextBox>
            <asp:TextBox ID="txt_po_buffer_days" runat="server" Width="20px" MaxLength="10" Visible="false"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_po_available_soonest" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <asp:TextBox ID="txt_po_dest_store_cd" runat="server" Width="30px" MaxLength="5" Visible="true"></asp:TextBox>
        </td>
        </tr>
        
        <!--Alice added-->

          <tr><td colspan="8">
        <hr />
        </td></tr>

         <tr>

        <td class="class_tdlbl" colspan="3" align="center">
        &nbsp;
        </td>
        <td class="class_tdlbl" colspan="2" align="center">
        <hr />
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_hdr_vendor" runat="server" meta:resourcekey="lbl_hdr_vendor" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="2" align="center">
        <hr />
        </td>

        </tr>

        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_vendor_in_stock" runat="server" meta:resourcekey="lbl_vendor_in_stock" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_in_stock_date" runat="server" meta:resourcekey="lbl_available_to_ship" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_vendor_on_order" runat="server" meta:resourcekey="lbl_vendor_on_order" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_on_order_date" runat="server" meta:resourcekey="lbl_available_to_ship" Font-Bold="true">
            </dx:ASPxLabel>
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
               
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
        
        </td>
        </tr>
        <tr>
        <td class="class_tdlbl" colspan="2" align="center">
            &nbsp;
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_reg_avail_vendor" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
          <asp:Literal ID="lit_reg_date_vendor" runat="server"></asp:Literal>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
            <asp:TextBox ID="txt_outlet_avail_vendor" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
             <asp:Literal ID="lit_outlet_date_vendor" runat="server"></asp:Literal>
        </td>
        <td class="class_tdtxt" colspan="1" align="center">
          
        </td>
        <td class="class_tdlbl" colspan="1" align="center">
           
        </td>
        </tr>

    </table>
    
    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>