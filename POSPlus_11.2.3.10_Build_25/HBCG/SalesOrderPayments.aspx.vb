Imports System.Data
Imports System.Data.OracleClient
Imports System.IO

Partial Class SalesOrderPayments
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim x As Double
        Dim del_doc_num As String = ""
        Dim ord_tp_cd As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                        ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        del_doc_num = Request("del_doc_num")
        ord_tp_cd = Request("ord_tp_cd")

        Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Payments (" & del_doc_num & ")"

        Dim payment_data As String
        ' Daniela add plan cd to TDF mop cd
        'sql = "SELECT AR_TRN.MOP_CD, BNK_CRD_NUM, EXP_DT, SUM(DECODE(AR_TRN_TP.DC_CD,'C',AR_TRN.AMT,-AR_TRN.AMT)) AS AMT, MOP.DES, AR_TRN.AR_TRN_PK FROM AR_TRN, MOP, AR_TRN_TP WHERE AR_TRN.MOP_CD=MOP.MOP_CD AND AR_TRN.TRN_TP_CD=AR_TRN_TP.TRN_TP_CD AND IVC_CD = '" & del_doc_num & "' AND AR_TRN.TRN_TP_CD NOT IN('SAL','CRM','MDB','MCR','SER') GROUP BY AR_TRN.MOP_CD, BNK_CRD_NUM, EXP_DT, MOP.DES, AR_TRN.AR_TRN_PK"
        'sql = "select a.MOP_CD, BNK_CRD_NUM, EXP_DT, nvl(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0),0) AS AMT, t.des as tran_des, b.DES, a.AR_TRN_PK "
        sql = "select a.MOP_CD, BNK_CRD_NUM, EXP_DT, nvl(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0),0) AS AMT, t.des as tran_des, b.DES || decode(a.mop_cd,'TDF', ' - ' || a.des, '') des, a.AR_TRN_PK "
        sql = sql & "from ar_trn_tp t, ar_trn a, mop b "
        sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
        sql = sql & "and  a.MOP_CD=b.MOP_CD(+) "
        Select Case ord_tp_cd
            Case "SAL"
                sql = sql & "and  a.ivc_cd = '" & del_doc_num & "' "
            Case "CRM"
                sql = sql & "and  a.adj_ivc_cd = '" & del_doc_num & "' "
            Case "MCR"
                sql = sql & "and  a.adj_ivc_cd = '" & del_doc_num & "' "
            Case "MDB"
                sql = sql & "and  a.ivc_cd = '" & del_doc_num & "' "
            Case Else
                sql = sql & "and  a.ivc_cd = '" & del_doc_num & "' "
        End Select
        sql = sql & "and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER')"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        payment_data = "<br /><table width=""95%"" cellpadding=0 cellspacing=0 bgcolor=white>"
        payment_data = payment_data & "<tr>"
        payment_data = payment_data & "<td>&nbsp;</td>"
        payment_data = payment_data & "<td align=left><b><u>Payment Type:</u></b></td>"
        payment_data = payment_data & "<td align=left><b><u>Account #:</u>&nbsp;&nbsp;</b></td>"
        payment_data = payment_data & "<td align=left><b><u>Exp. Date:</u></b></td>"
        payment_data = payment_data & "<td align=left><b><u>Amount:</u></b></td>"
        payment_data = payment_data & "</tr>"

        Dim deposit_totals As Double
        deposit_totals = 0

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td>&nbsp;</td>"
                If MyDataReader.Item("DES").ToString & "" <> "" Then
                    payment_data = payment_data & "<td align=left>" & MyDataReader.Item("DES").ToString & "</td>"
                Else
                    payment_data = payment_data & "<td align=left>" & MyDataReader.Item("tran_des").ToString & "</td>"
                End If
                If MyDataReader.Item("BNK_CRD_NUM").ToString & "" <> "" Then
                    payment_data = payment_data & "<td align=left>Card Ending In: " & Right(MyDataReader.Item("BNK_CRD_NUM").ToString, 4) & "</td>"
                Else
                    payment_data = payment_data & "<td align=left>&nbsp;</td>"
                End If
                If IsDate(MyDataReader.Item("EXP_DT").ToString) Then
                    payment_data = payment_data & "<td align=left>" & FormatDateTime(MyDataReader.Item("EXP_DT").ToString, DateFormat.ShortDate) & "</td>"
                Else
                    payment_data = payment_data & "<td align=left>&nbsp;</td>"
                End If
                payment_data = payment_data & "<td align=left>" & FormatCurrency(MyDataReader.Item("AMT").ToString, 2) & "</td>"
                payment_data = payment_data & "</tr>"
                deposit_totals = deposit_totals + FormatCurrency(MyDataReader.Item("AMT").ToString, 2)
                x = x + 1
            Loop
            payment_data = payment_data & "</table>"
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        main_body.InnerHtml = payment_data
        conn.Close()
    End Sub

End Class
