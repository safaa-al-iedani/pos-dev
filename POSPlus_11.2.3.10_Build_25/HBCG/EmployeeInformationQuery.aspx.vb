Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'JAM added 20Oct2014
Imports DevExpress.Web.ASPxEditors
Imports System.Data


Partial Class EmployeeInformationQuery
    Inherits POSBasePage

    Public Const gn_maxRecordsRetrieved As Integer = 100000
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. Basic employee data query.
    '
    '   2.0:    POS+ French conversion
    '   Labels, tooltips, button captions are converted into French strings.
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetAllFields()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Resets/clears all fields.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        lbl_resultsInfo.Visible = False
        lbl_resultsInfo.Text = ""

        txt_EMP_employee_code.Text = ""
        txt_EMP_employee_init.Text = ""
        txt_EMP_first_name.Text = ""
        txt_EMP_last_name.Text = ""
        txt_QUERY_store_code.Text = ""

        txt_EMP_home_store_code.Text = ""
        txt_EMP_termdate.Text = ""
        txt_EMP_default_printer_queue.Text = ""
        txt_EMP_default_printer_group.Text = ""

        Session("ds_queryResults") = New DataSet
        Session("POSITION_INDEX") = 0
        Session("ROW_NUM") = ""

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveEmployeeInformation()
    '   Created by:     John Melenko
    '   Date:           Mar2015
    '   Description:    Main procedure which populates the employee information based on query parameters entered by the user.
    '   Called by:      "Lookup Prices" button - btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveEmployeeInformation(p_employeeCode As String, p_employeeInitials As String, p_queryStoreCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        'Dim s_employeeCode As String = txt_EMP_employee_code.Text.ToString.ToUpper
        Dim s_employeeCode As String = p_employeeCode.ToString.ToUpper
        'Dim s_employeeInitials As String = txt_EMP_employee_init.Text.ToString.ToUpper
        Dim s_employeeInitials As String = p_employeeInitials.ToString.ToUpper
        Dim s_firstName As String = txt_EMP_first_name.Text.ToString.ToUpper
        Dim s_lastName As String = txt_EMP_last_name.Text.ToString.ToUpper
        'Dim s_queryStoreCode As String = txt_QUERY_store_code.Text.ToString.ToUpper
        Dim s_queryStoreCode As String = p_queryStoreCode.ToString.ToUpper

        'MsgBox("empcode: " + s_employeeCode + "; " + s_employeeInitials + "; " + s_firstName + "; " + s_lastName + "; " + s_queryStoreCode + "; " + txt_EMP_employee_code.Text)

        Dim b_Continue As Boolean = False

        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(s_employeeCode.ToString())) AndAlso String.IsNullOrEmpty(Trim(s_employeeInitials.ToString())) AndAlso String.IsNullOrEmpty(Trim(s_firstName.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(s_lastName.ToString())) AndAlso String.IsNullOrEmpty(Trim(s_queryStoreCode.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = Resources.CustomMessages.MSG0013 & vbCrLf & Resources.CustomMessages.MSG0014 & vbCrLf & Resources.CustomMessages.MSG0015
            s_errorMessage = String.Format(Resources.CustomMessages.MSG0013, vbCrLf)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            txt_EMP_employee_code.Text = Trim(txt_EMP_employee_code.Text)
            txt_EMP_employee_init.Text = Trim(txt_EMP_employee_init.Text)
            txt_EMP_first_name.Text = Trim(txt_EMP_first_name.Text)
            txt_EMP_last_name.Text = Trim(txt_EMP_last_name.Text)

            Return
        End If

        ''STORE CODE REQUIRES at least ONE of the other fields ?????
        'If (Not (String.IsNullOrEmpty(Trim(s_queryStoreCode.ToString()))) AndAlso _
        '    (String.IsNullOrEmpty(Trim(s_employeeCode.ToString())) AndAlso String.IsNullOrEmpty(Trim(s_employeeInitials.ToString())) _
        '    AndAlso String.IsNullOrEmpty(Trim(s_firstName.ToString())) AndAlso String.IsNullOrEmpty(Trim(s_lastName.ToString())))) Then

        '    s_errorMessage = "Please provide more criteria with store code."
        '    'MsgBox(s_errorMessage, MsgBoxStyle.Exclamation)
        '    'lbl_resultsInfo.Text = s_errorMessage
        '    'lbl_resultsInfo.ForeColor = Color.Red
        '    DisplayMessage(s_errorMessage, "LABEL", "ERROR")
        '    Return
        'End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_userCompanyCode As String = Session("CO_CD")
        'Dim s_employee_code As String = Session("EMP_CD")
        Dim s_userEmployeeCode = Session("EMP_CD")

        'for debugging
        'MsgBox("company code [" + s_userCompanyCode + "] for employee code [" + s_userEmployeeCode + "]")

        'If String.IsNullOrEmpty(Trim(s_userCompanyCode.ToString()))) Then
        '    s_errorMessage = "Company code is required to proceed. Please logout and try again."
        '    'MsgBox(s_errorMessage, MsgBoxStyle.Exclamation)
        '    'lbl_resultsInfo.Text = s_errorMessage
        '    'lbl_resultsInfo.ForeColor = Color.Red
        '    DisplayMessage(s_errorMessage, "LABEL", "ERROR")
        '    Return
        'End If

        '--------------------------------------------------------------------------
        '-- data retrieval
        '--------------------------------------------------------------------------

        'conn.Open()

        'Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT e.emp_cd, e.emp_init, e.fname, e.lname, e.home_store_cd, e.home_store_cd || ' - ' || s.store_name || ' (' || s.co_cd || ')' as home_store_cd_desc, "
        SQL = SQL & "TO_CHAR(e.termdate,'DD-MON-YYYY') as TERMDATE, e.lptq_nm as DFLT_PRINTER_QUEUE, e.print_grp_con_cd as DFLT_PRINTER_GROUP "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM emp e, store s, co_grp cg "
        SQL = SQL & "WHERE e.home_store_cd = s.store_cd "
        SQL = SQL & "AND s.co_cd = cg.co_cd "
        SQL = SQL & "AND cg.co_grp_cd = (SELECT DISTINCT co_grp_cd FROM co_grp WHERE co_cd = :x_userCompanyCode) "

        'AND clauses to add based on parameters passed in
        Dim s_clauseEmployeeCode = "AND e.emp_cd LIKE UPPER(:x_employeeCode) "
        Dim s_clauseEmployeeInitials = "AND e.emp_init LIKE UPPER(:x_employeeInitials) "
        Dim s_clauseEmployeeFirstName = "AND e.fname LIKE UPPER(:x_firstName) "
        Dim s_clauseEmployeeLastName = "AND e.lname LIKE UPPER(:x_lastName) "
        Dim s_clauseStoreCode = "AND e.home_store_cd = UPPER(:x_queryStoreCode) "


        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(s_employeeCode.ToString())))) Then
            SQL = SQL & s_clauseEmployeeCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_employeeInitials.ToString())))) Then
            SQL = SQL & s_clauseEmployeeInitials
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_firstName.ToString())))) Then
            SQL = SQL & s_clauseEmployeeFirstName
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_lastName.ToString())))) Then
            SQL = SQL & s_clauseEmployeeLastName
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_queryStoreCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If

        'Set SQL object
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'bind variables based on parameters passed in
        '        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        '       objsql.Parameters(":x_co_cd").Value = s_co_cd

        objsql.Parameters.Add(":x_userCompanyCode", OracleType.VarChar)
        objsql.Parameters(":x_userCompanyCode").Value = s_userCompanyCode

        If (Not (String.IsNullOrEmpty(Trim(s_employeeCode.ToString())))) Then
            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
            objsql.Parameters(":x_employeeCode").Value = s_employeeCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_employeeInitials.ToString())))) Then
            objsql.Parameters.Add(":x_employeeInitials", OracleType.VarChar)
            objsql.Parameters(":x_employeeInitials").Value = s_employeeInitials
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_firstName.ToString())))) Then
            objsql.Parameters.Add(":x_firstName", OracleType.VarChar)
            objsql.Parameters(":x_firstName").Value = s_firstName
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_lastName.ToString())))) Then
            objsql.Parameters.Add(":x_lastName", OracleType.VarChar)
            objsql.Parameters(":x_lastName").Value = s_lastName
        End If
        If (Not (String.IsNullOrEmpty(Trim(s_queryStoreCode.ToString())))) Then
            objsql.Parameters.Add(":x_queryStoreCode", OracleType.VarChar)
            objsql.Parameters(":x_queryStoreCode").Value = s_queryStoreCode
        End If

        Dim ds As New DataSet
        Dim dv As DataView

        Try
            With objsql
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = New DataView(ds.Tables(0))

            Dim n_positionCounter As Integer = 1
            Dim n_queryResultsCount As Integer = dv.Count

            Session("ds_queryResults") = ds
            Session("POSITION_INDEX") = n_positionCounter
            Session("ROW_NUM") = ""

            If (n_queryResultsCount > 0) Then
                Session("ROW_NUM") = dv(0)("ROW_NUM").ToString
            End If

            'for debugging
            'MsgBox("number of records retrieved: " + n_queryResultsCountToString)

            If (n_queryResultsCount > gn_maxRecordsRetrieved) Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = '"" + n_queryResultsCount.ToString + "records found (maximum" + gn_maxRecordsRetrieved.ToString + "exceeded). Please refine your search."
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0016, n_queryResultsCount.ToString, gn_maxRecordsRetrieved.ToString)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                'MsgBox(s_errorMessage, MsgBoxStyle.Exclamation)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                'lbl_resultsInfo.Visible = True
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")
                'DisplayMessage(s_errorMessage, "MSGBOX", "ERROR")

                Session("ds_queryResults") = New DataSet
                Session("POSITION_INDEX") = 0
                Session("ROW_NUM") = ""

                conn.Close()

                Return
            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'bind dataview data to fields on screen based on user's position in the data (in this case the first record)
        BindDataViewData()

    End Sub

    Private Sub BindDataViewData()

        btnFirst.Visible = False
        btnPrev.Visible = False
        btnNext.Visible = False
        btnLast.Visible = False
        'btn_ToggleToGridResults.Visible = False
        'btn_ToggleToGridResults.Enabled = False

        lbl_resultsInfo.Visible = True
        lbl_resultsInfo.ForeColor = Color.Green

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView(ds.Tables(0))
        Dim n_positionCounter As Integer = Session("POSITION_INDEX")
        Dim n_queryResultsCount As Integer = dv.Count

        If (n_queryResultsCount > 0) Then
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'lbl_resultsInfo.Text = Resources.CustomMessages.MSG0019 + n_positionCounter.ToString + Resources.CustomMessages.MSG0020 + n_queryResultsCount.ToString + Resources.CustomMessages.MSG0021
            lbl_resultsInfo.Text = String.Format(Resources.CustomMessages.MSG0019, n_positionCounter.ToString, n_queryResultsCount.ToString)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
        Else
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'lbl_resultsInfo.Text = "No results were found using your search criteria. Please note - you can only view employees within your own company group.
            lbl_resultsInfo.Text = Resources.CustomMessages.MSG0018
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            lbl_resultsInfo.ForeColor = Color.Red
        End If

        ' make all the buttons visible if page count is more than 0
        If (n_queryResultsCount > 0) Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            'btn_ToggleToGridResults.Visible = True
            'btn_ToggleToGridResults.Enabled = True
            '''lbl_resultsInfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If (n_queryResultsCount = 1) Then
            ' only 1 record
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If (n_positionCounter = 1) Then
                ' we are sitting at first record
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If (n_positionCounter = n_queryResultsCount) Then
                    ' we are sitting at last record
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If

        End If

        'JAM - to use a filter, but using position counter instead
        'If (ds Is Nothing OrElse Not ds.IsInitialized) Then
        '    'do nothing
        'Else
        '    dv.RowFilter = ""
        '    'Dim n_row_num = Session("n_row_num")
        '    Dim n_ROW_NUM = Session("ROW_NUM")
        '    If (Not (String.IsNullOrEmpty(Trim(n_ROW_NUM.ToString())))) Then

        '        dv.RowFilter = "ROW_NUM = " & n_ROW_NUM.ToString

        '    End If
        'End If

        Dim n_current_position = 0

        For Each drv As DataRowView In dv

            n_current_position += 1
            If (n_current_position = n_positionCounter) Then


                txt_EMP_employee_code.Text = drv("EMP_CD").ToString
                txt_EMP_employee_init.Text = drv("EMP_INIT").ToString
                txt_EMP_first_name.Text = drv("FNAME").ToString
                txt_EMP_last_name.Text = drv("LNAME").ToString

                txt_EMP_home_store_code.Text = drv("HOME_STORE_CD_DESC").ToString
                txt_EMP_termdate.Text = drv("TERMDATE").ToString
                txt_EMP_default_printer_queue.Text = drv("DFLT_PRINTER_QUEUE").ToString
                txt_EMP_default_printer_group.Text = drv("DFLT_PRINTER_GROUP").ToString

                Session("ROW_NUM") = drv("ROW_NUM").ToString

                Exit For
            End If
        Next

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView(ds.Tables(0))
        Dim n_positionCounter As Integer = Session("POSITION_INDEX")
        Dim n_queryResultsCount As Integer = dv.Count

        Select Case strArg
            Case "Next"
                If (n_positionCounter < n_queryResultsCount) Then
                    n_positionCounter = n_positionCounter + 1
                End If

            Case "Prev"
                If (n_positionCounter > 1) Then
                    n_positionCounter = n_positionCounter - 1
                End If

            Case "Last"
                n_positionCounter = n_queryResultsCount

            Case "First"
                n_positionCounter = 1

        End Select

        Session("POSITION_INDEX") = n_positionCounter
        BindDataViewData()

    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           IsValidEmployeeSecurityCheck()
    '   Created by:     John Melenko
    '   Date:           Mar2015
    '   Description:    Procedure which validates if current user is allowed to view the employees -- can't build this
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function IsValidEmployeeSecurityCheck() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim s_result As String = ""

        'conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'conn.Open()

        'Dim Mydatareader As OracleDataReader
        'Dim objsql As OracleCommand

        'SQL = "SELECT s.emp_cd_op as SO_emp_cd_op "
        'SQL = SQL & "FROM so s "
        'SQL = SQL & "WHERE s.del_doc_num = UPPER(:x_invoice) "

        ''Set SQL OBJECT 
        'objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
        'objsql.Parameters(":x_invoice").Value = p_invoice

        'Try
        '    'Execute DataReader 
        '    Mydatareader = objsql.ExecuteReader
        '    'Store Values in String Variables 
        '    If Mydatareader.Read() Then
        '        s_result = Mydatareader.Item("SO_EMP_CD_OP").ToString
        '    End If
        '    Mydatareader.Close()
        'Catch
        '    Throw
        '    conn.Close()
        'End Try
        'conn.Close()

        'End If

        Return s_result

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty Then

            'Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store order by store_cd"
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc "
            SQL = SQL & "FROM store "
            SQL = SQL & "WHERE UPPER(co_cd) = UPPER(:x_co_cd) "
            SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)
            cmdGetCodes.Parameters.Add(":x_co_cd", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_co_cd").Value = s_co_cd
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'txt_QUERY_store_code.Items.Insert(0, "Select a Store Code")
            'txt_QUERY_store_code.Items.FindByText("Select a Store Code").Value = ""
            txt_QUERY_store_code.Items.Insert(0, GetLocalResourceObject("combo_select_store_code.Text").ToString())
            txt_QUERY_store_code.Items.FindByText(GetLocalResourceObject("combo_select_store_code.Text").ToString()).Value = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            txt_QUERY_store_code.SelectedIndex = 0
            'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            btnFirst.ToolTip = GetLocalResourceObject(("tool_btnFirst.Text").ToString())
            btnLast.ToolTip = GetLocalResourceObject(("tool_btnLast.Text").ToString())
            btnPrev.ToolTip = GetLocalResourceObject(("tool_btnPrev.Text").ToString())
            btnNext.ToolTip = GetLocalResourceObject(("tool_btnNext.Text").ToString())
            'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_QUERY_store_code
                    .DataSource = ds
                    .DataValueField = "store_cd"
                    .DataTextField = "full_store_desc"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation, Title:="Error")
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_resultsInfo.Visible = False

        If Not IsPostBack Then

            ''A query has already been initiated
            'If Request("selected_row") = "Y" Then

            '    'for debugging
            '    'MsgBox("row # is: " + Request("ROW_NUM").ToString)

            '    Session("ROW_NUM") = Request("ROW_NUM")
            '    Session("POSITION_INDEX") = Request("POSITION_INDEX")

            '    Session("PSQGRID_INDEX") = 1 'reset the gridview index variable just in case
            '    BindDataViewData()

            '    Session("ROW_NUM") = "" 'reset the row number

            'Else


            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then

                btn_Lookup.Enabled = False

            Else

                'JAM: populate the store codes drop-down list
                PopulateStoreCodes()

            End If
        End If

        'txt_store_cd.SelectedValue = ConfigurationManager.AppSettings("default_store_code").ToString

        lbl_versionInfo.Text = gs_version

        'txt_EMP_employee_code.ReadOnly = True
        'txt_EMP_employee_init.ReadOnly = True
        'txt_EMP_first_name.ReadOnly = True
        'txt_EMP_last_name.ReadOnly = True
        'txt_QUERY_store_code.ReadOnly = True

        txt_EMP_home_store_code.ReadOnly = True
        txt_EMP_termdate.ReadOnly = True
        txt_EMP_default_printer_queue.ReadOnly = True
        txt_EMP_default_printer_group.ReadOnly = True
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'lbl_EMP_employee_code.ToolTip = "Search via employee code"
        'lbl_EMP_employee_init.ToolTip = "Search via employee initials"
        'lbl_EMP_first_name.ToolTip = "Search via employee first name"
        'lbl_EMP_last_name.ToolTip = "Search via employee last name"
        'lbl_QUERY_store_code.ToolTip = "Search via store code"
        lbl_EMP_employee_code.ToolTip = GetLocalResourceObject("tool_EMP_employee_code.Text").ToString()
        lbl_EMP_employee_init.ToolTip = GetLocalResourceObject("tool_EMP_employee_initials.Text").ToString()
        lbl_EMP_first_name.ToolTip = GetLocalResourceObject("tool_EMP_employee_first_name.Text").ToString()
        lbl_EMP_last_name.ToolTip = GetLocalResourceObject("tool_EMP_employee_last_name.Text").ToString()
        lbl_QUERY_store_code.ToolTip = GetLocalResourceObject("tool_EMP_employee_store_code.Text").ToString()
        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_QUERY_store_code.Enabled = True

        Response.Redirect("EmployeeInformationQuery.aspx")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Lookup_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        'ResetAllFields()

        RetrieveEmployeeInformation(txt_EMP_employee_code.Text, txt_EMP_employee_init.Text, txt_QUERY_store_code.Text)

        'Session("IPQ_SEARCH") = "ON"

        'If (isValidItem(txt_sku.Text)) Then

        '    Dim s_isValidItemSecurityCheck = IsValidItemSecurityCheck(txt_sku.Text)

        '    If (s_isValidItemSecurityCheck = "Y") Then

        '        PopulateItem(txt_sku.Text)

        '    ElseIf (s_isValidItemSecurityCheck = "N") Then

        '        Dim s_errorMessage As String = ""
        '        Dim s_employeeCode = Session("EMP_CD")

        '        'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
        '        s_errorMessage = "Security Violation for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
        '        'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
        '        'lbl_resultsInfo.Text = s_errorMessage
        '        'lbl_resultsInfo.ForeColor = Color.Red
        '        DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        '    Else

        '        Dim s_errorMessage As String = ""
        '        Dim s_employeeCode = Session("EMP_CD")

        '        'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
        '        s_errorMessage = "Cannot determine security information for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
        '        'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
        '        'lbl_resultsInfo.Text = s_errorMessage
        '        'lbl_resultsInfo.ForeColor = Color.Red
        '        DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        '    End If

        'End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class
