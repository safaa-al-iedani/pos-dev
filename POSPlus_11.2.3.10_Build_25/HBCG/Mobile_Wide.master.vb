Imports System.Data.OracleClient
Imports ErrorManager

Partial Class Mobile_Wide
    Inherits System.Web.UI.MasterPage

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            'mm- sep 18,2016 - back button concern
            ClearCache()
            Response.Redirect("login.aspx")
        Else
            'mm -sep 16,2016 - browser backbutton concern
            Response.ClearHeaders()
            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
            Response.AddHeader("Pragma", "no-cache")
            'mm-end
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "<font color=red>* TRAIN MODE *</font>"
        End If

        If Not IsPostBack Then
            Dim sql As String
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            If Session("EMP_FNAME") & "" = "" Or Session("EMP_LNAME") & "" = "" Then
                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If

                sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("EMP_CD") = MyDataReader.Item("EMP_CD").ToString
                        Session("EMP_FNAME") = MyDataReader.Item("FNAME").ToString
                        Session("EMP_LNAME") = MyDataReader.Item("LNAME").ToString
                        Session("HOME_STORE_CD") = MyDataReader.Item("HOME_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                sql = "SELECT SHIP_TO_STORE_CD FROM STORE WHERE STORE_CD='" & UCase(Session("HOME_STORE_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("pd_store_cd") = MyDataReader.Item("SHIP_TO_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            End If
            If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
                lbl_Header1.Text = "Welcome " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            End If
            If Session("HOME_STORE_CD") & "" <> "" Then
                lbl_Header2.Text = "Store Code: " & Session("HOME_STORE_CD")
            End If
            If Request("D") = "D" Then
                lblTabID.Text = Session("MP")
            End If
            If Request("D") = "I" Then
                lblTabID.Text = Session("IP")
            End If

            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink1").ToString) Then
                HyperLink1.Visible = True
                HyperLink1.NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink1").ToString
                HyperLink1.Text = ConfigurationManager.AppSettings("link1_desc").ToString
            End If
            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink2").ToString) Then
                HyperLink2.Visible = True
                HyperLink2.NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink2").ToString
                HyperLink2.Text = ConfigurationManager.AppSettings("link2_desc").ToString
            End If

            Dim strURL As String
            Dim arrayURL As Array
            Dim pagename As String

            strURL = Request.ServerVariables("SCRIPT_NAME")
            arrayURL = Split(strURL, "/", -1, 1)
            pagename = arrayURL(UBound(arrayURL))

            Dim pgInfo As SystemUtils.PageInfo = SystemUtils.GetPageInfo(pagename, Session("emp_cd"))

            If pgInfo.allowPageEntry Then

                lblTabID.Text = pgInfo.label
                Page.Title = pgInfo.title
                If pgInfo.hyPrLnk.NavigateUrl.isNotEmpty Then

                    hpl_exit.NavigateUrl = pgInfo.hyPrLnk.NavigateUrl
                    hpl_exit.Visible = pgInfo.hyPrLnk.Visible

                    If pgInfo.hyPrLnk.Text.isNotEmpty Then

                        hpl_exit.Text = pgInfo.hyPrLnk.Text
                    End If
                End If

            Else
                Response.Redirect("newmain.aspx")
            End If

        End If

    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

     Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton concern
        Session.Abandon()
        Response.ClearHeaders()
        Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
        Response.AddHeader("Pragma", "no-cache")

        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
    End Sub
End Class

