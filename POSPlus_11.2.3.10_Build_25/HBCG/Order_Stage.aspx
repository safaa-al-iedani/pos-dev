<%@ Page Language="VB" MasterPageFile="~/Regular.master" AutoEventWireup="false"
    CodeFile="Order_Stage.aspx.vb" Inherits="Order_Stage" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript">
        function SelectAndClosePopupEmailTemplate() {
            ASPxPopupControl5.Hide();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function ShowBilling() {
            document.getElementById('BillingInfo').style.display = '';
            document.getElementById('DownArrow1').style.display = 'none';
            document.getElementById('UpArrow1').style.display = '';
            document.getElementById('UpArrow1').style.cursor = 'hand';
        }
        function HideBilling() {
            document.getElementById('BillingInfo').style.display = 'none';
            document.getElementById('DownArrow1').style.display = '';
            document.getElementById('DownArrow1').style.cursor = 'hand';
            document.getElementById('UpArrow1').style.display = 'none';
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else {
                return true;
            }
        }
    </script>

    <asp:Label ID="lblErrorMsg" runat="server" Visible="false" Text="<%$ Resources:LibResources, Label973 %>" style="font-weight:bold" ></asp:Label>    <!-- Oct 26,2016 - mariam - message after Aeroplan card purchase -->
    <br />

    <asp:Label ID="lbl_Summary" runat="server" Text=""></asp:Label>
    <asp:Label ID="email" runat="server" Text="" visible="False"></asp:Label>
    
    <asp:Label ID="lblErrorEmail" runat="server" Visible="false" Text="" style="color:red;font-weight:bold" ></asp:Label>    <!-- June 12,2018 - email warning -->
    
    <br />
    <!-- Oct 20, 2016 - mariam - Aeroplam promo -->
    <br />
    <div> 
       <dx:ASPxButton runat="server" ID="btnPromoCard" Text="<%$ Resources:LibResources, Label976 %>"  Visible="false" style="font-weight:bold"  OnClick="btnPromoCard_Click">
    </dx:ASPxButton>
    </div>
    <br />

     
    <!--END- Oct 20, 2016 - mariam - Aeroplam promo -->
    <asp:Label ID="lbl_itm_summary" runat="server" Text=""></asp:Label>
    <br />
    <asp:Label ID="Label5" runat="server" ForeColor="Maroon" Text="<%$ Resources:LibResources, Label532 %>"></asp:Label>
    <br />
    <table>
        <tr>
            <%-- sabrina added --%>
             <td>              
          <dx:ASPxComboBox ID="printer_drp" runat="server" Width="150px" DropDownRows="5">               
          </dx:ASPxComboBox>
      </td>         
            <td>                
				<dx:ASPxButton ID="btn_commit" runat="server" Text="<%$ Resources:LibResources, Label99 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_convert_to_rel" runat="server" Text="<%$ Resources:LibResources, Label108 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <%--' daniela nov 11 hide email--%>
                <dx:ASPxButton ID="ImageButton1" runat="server" Text="Configure Email" OnClick="ImageButton1_Click1" Visible="false">
                </dx:ASPxButton>
            </td>
            <td>
                <%--' daniela nov 11 hide email--%>
                <asp:Literal ID="emailLbl" runat="server" Text="<%$ Resources:LibResources, Label181 %>" />
                <dx:ASPxCheckBox ID="chk_email" runat="server" AutoPostBack="true" OnCheckedChanged="chk_email_CheckedChanged" Enabled="False" Checked="False">
                </dx:ASPxCheckBox>               
            </td>
            <td>
                <asp:CheckBox ID="chk_picr" runat="server" Text="Print Picking Report?" />
            </td>
            <td>
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Re-Print Invoice(s)" Visible="False"
                    OnClick="ASPxButton1_Click">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxButton ID="btn_generate_po" runat="server" Text="Generate PO" OnClick="btn_generate_po_Click" Visible="False">
    </dx:ASPxButton>
    <asp:DropDownList ID="cbo_finance_addendums" runat="server" CssClass="style5" Visible="False"
        Width="3px">
    </asp:DropDownList>
    <asp:Label ID="lbl_fin_co" runat="server" Visible="False" Width="1px"></asp:Label>
    <asp:Label ID="lbl_default_invoice" runat="server" Visible="False" Width="1px"></asp:Label>
    <asp:Label ID="lbl_del_doc_num" runat="server" Visible="False" Width="1px"></asp:Label>
    <asp:Label ID="lbl_split" runat="server" Visible="False" Width="1px"></asp:Label>
    <asp:Label ID="lbl_fin" runat="server" Visible="False" Width="1px"></asp:Label>
    <asp:Label ID="lbl_wdr" runat="server" Visible="False" Width="1px"></asp:Label>
<br />
    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label636 %>" />:
    <img id="DownArrow1" title="Click here to expand Billing Information" style="cursor: hand"
        onclick="ShowBilling()" src="Images/icons/down_arrow.png">&nbsp;<img id="UpArrow1"
            title="Click here to hide billing information" style="display: none; cursor: hand"
            onclick="HideBilling()" src="Images/icons/up_arrow.png">
    <div id="BillingInfo" style="display: none">
        <iframe src="user_Defined_Fields.aspx" style="width: 461px; height: 121px" frameborder="0"></iframe>
    </div>
    <br />
    <br />
    <asp:Label ID="lbl_Status" runat="server" Width="100%"></asp:Label>
    <br />
    <asp:Label ID="lbl_Follow_Up" runat="server" Width="100%"></asp:Label>
    <asp:TextBox ID="txt_Follow_up" runat="server" Height="51px" Width="98%" Rows="3"
        TextMode="MultiLine" Font-Names="tahoma" CssClass="style5">
    </asp:TextBox>
    <br />
    <asp:DataGrid ID="Gridview1" runat="server" AutoGenerateColumns="False" CellPadding="2"
        DataKeyField="row_id" Font-Bold="False" Font-Italic="False" Font-Overline="False"
        Font-Strikeout="False" Font-Underline="False" Height="156px" Width="98%">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="itm_cd" HeaderText="SKU" ReadOnly="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="VSN">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vsn") %>'></asp:Label>
                    -
                    <br />
                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="row_id" HeaderText="Row_ID" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Price">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
                <ItemTemplate>
                    <asp:TextBox ID="ret_prc" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.ret_prc"),2) %>'
                        Width="45" CssClass="style5" Enabled="False"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Qty">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
                <ItemTemplate>
                    <asp:TextBox ID="qty" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.qty"),0) %>'
                        Width="23" CssClass="style5" Enabled="False"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="ITM_TP_CD" HeaderText="itm_tp_cd" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TREATABLE" HeaderText="treatable" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TREATED" HeaderText="TREATED" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TAKE_WITH" HeaderText="TAKE_WITH" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Top" />
            </asp:BoundColumn>
        </columns>
    </asp:DataGrid>
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label180 %>" Height="64px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="494px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
 
                 <%-- MM-3179 - Enter key kicks out of session --%>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_Proceed">
                    <table class="style5">
                        <tr>
                            <td valign="middle" align="center">
                                <img src="images/icons/Symbol-Stop.png" />
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td valign="middle">
                                <dx:ASPxLabel ID="lbl_warning" runat="server" Text="<%$ Resources:LibResources, Label29 %>">
                                </dx:ASPxLabel>
                                <br />
                                <br />
                                <asp:TextBox ID="txt_email" runat="server" Height="16px" CssClass="style5" Width="293px"></asp:TextBox>
                             <%--   <asp:RequiredFieldValidator ID="email_validation" runat="server" controltovalidate="txt_email"
                                    ErrorMessage="<%$ Resources:LibResources, Label998 %>" ForeColor="Red"> 
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ForeColor="Red"  
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ControlToValidate="txt_email" ErrorMessage="<%$ Resources:LibResources, Label998 %>">
                                </asp:RegularExpressionValidator>--%>
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            <dx:ASPxButton ID="btn_Proceed" runat="server" Text="<%$ Resources:LibResources, Label12 %>">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btn_Cancel" runat="server" Text="<%$ Resources:LibResources, Label350 %>">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
        HeaderText="Payment Warnings" Height="475px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="608px" ContentUrl="~/Payment_Hold.aspx"
        AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" CloseAction="CloseButton"
        HeaderText="Signature Capture" Height="327px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="575px" AllowDragging="True" ContentUrl="~/SigCapture.aspx">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table class="style5">
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Stop.png" />
                        </td>
                        <td>&nbsp;&nbsp;
                        </td>
                        <td valign="middle">
                            <asp:Label ID="Label4" runat="server" Text="You have processed a credit memo, would you like to issue a refund?"
                                Font-Bold="True"></asp:Label>
                            <br />
                            <br />
                            <asp:Button ID="Button1" runat="server" Text="Process Refund" CssClass="style5" />
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" CloseAction="CloseButton"
        HeaderText="Process Refund?" Height="64px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="494px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <table class="style5">
                    <tr>
                        <td valign="middle" align="center">
                           <img src="images/icons/Symbol-Stop.png" />
                        </td>
                        <td>&nbsp;&nbsp;
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label3" runat="server" Text="You have processed a Miscellaneous Credit, would you like to issue a refund?"
                                Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <table width="100%">
                                <tr>
                        <td align="center"><asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="style5" /></td>
                        <td align="left"><asp:Button ID="btnNo" runat="server" Text="No" CssClass="style5" /></td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl5" runat="server" ClientInstanceName="ASPxPopupControl5" AllowDragging="True"
        AllowResize="True" ContentUrl="~/Email_Templates.aspx" HeaderText="Update Email Template"
        Height="375px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="546px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
     <!-- Oct 21,2016 - mariam - Aeroplan Promo -->
    <dxpc:ASPxPopupControl ID="ASPxPopupAeroplanPromo" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label976 %>" Height="64px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="494px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupCCCAeroplan" runat="server">
                <table class="style5">
                        <tr>
                            <td valign="middle" align="center">
                                <img src="images/icons/Symbol-Stop.png" />
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td valign="middle">
                                <dx:ASPxLabel ID="lblAeroplan" runat="server" Text="<%$ Resources:LibResources, Label974  %>">
                                </dx:ASPxLabel>
                                 <asp:TextBox ID="txtAerplanNum" runat="server" Height="16px" CssClass="style5"  MaxLength="20" value="" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <br />
                                <br />
                                <table style="width:225px" >
                                    <tr align="right">
                                        <td align="right" style="width:150px">
                                            <dx:ASPxButton ID="btnSubmitAeroplan" runat="server" Text="<%$ Resources:LibResources, Label975 %>" Width="70px" >
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnCanelAeroplan" runat="server" Text="<%$ Resources:LibResources, Label55 %>" Width="70px">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblDisclaimer" runat="server" Text="<%$ Resources:LibResources, Label971 %>" ></asp:Label>
                        </td>
                    </tr>
                  </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <asp:Label ID="lbl_store_cd" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lbl_cust_cd" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lbl_ord_total" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lbl_ivc_cd" runat="server" Text="" Visible="False"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
