Imports System.Collections.Generic
Imports System.Data.OracleClient

Partial Class Generate_PO
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("DEL_DOC_NUM") & "" <> "" Then
                ASPxTextBox1.Text = Request("DEL_DOC_NUM")
            End If

            lbl_PO_MSG.Text = Session("PO_MSG")
            Session("PO_MSG") = ""

            Load_Grid()
            store_cd_populate()

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT PU_DEL_STORE_CD FROM SO WHERE DEL_DOC_NUM='" & ASPxTextBox1.Text & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    store_cd.Value = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                End If

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()

            po_cd_populate()
        End If

    End Sub

    Public Sub Load_Grid()

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim sql As String
        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        Try
            conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn2.Open()

            sql = "select b.DEL_DOC_NUM, b.DEL_DOC_LN#, b.itm_cd, c.ve_cd, c.vsn, b.QTY, c.des from so a, so_ln b, itm c "
            sql = sql & "where a.STAT_CD='O' and a.del_doc_num=b.del_doc_num and b.ITM_CD=c.ITM_CD and b.STORE_CD IS NULL "
            sql = sql & "and b.VOID_FLAG='N' and b.DEL_DOC_LN# NOT IN (SELECT DEL_DOC_LN# FROM SO_LN$PO_LN "
            sql = sql & "where DEL_DOC_NUM='" & ASPxTextBox1.Text & "') and b.DEL_DOC_NUM='" & ASPxTextBox1.Text & "' ORDER BY c.VE_CD, b.DEL_DOC_LN#"

            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            ASPxGridView1.DataSource = ds
            ASPxGridView1.DataBind()
            conn2.Close()
        Catch

        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        lbl_PO_MSG.Text = ""

        If ASPxTextBox1.Text.isEmpty Then
            lbl_PO_MSG.Text = "You must enter a Sales Order to generate a PO."
            Exit Sub
        End If
        If cbo_po_sort.Value & "" = "" Then
            lbl_PO_MSG.Text = "You must select a PO Sort Code."
            Exit Sub
        End If

        Dim ln_no As List(Of Object) = ASPxGridView1.GetSelectedFieldValues("DEL_DOC_LN#")
        Dim ln_no_list As String = ""

        Dim i As Integer
        For i = 0 To ln_no.Count - 1
            ln_no_list = ln_no_list & ln_no(i).ToString() & ","
        Next i

        Dim itm As List(Of Object) = ASPxGridView1.GetSelectedFieldValues("ITM_CD")
        Dim itm_list As String = ""

        For i = 0 To itm.Count - 1
            itm_list = itm_list & itm(i).ToString() & ","
        Next i


        Dim qty As List(Of Object) = ASPxGridView1.GetSelectedFieldValues("QTY")
        Dim qty_list As String = ""

        For i = 0 To qty.Count - 1
            qty_list = qty_list & qty(i).ToString() & ","
        Next i

        Dim VE_Values As List(Of Object) = ASPxGridView1.GetSelectedFieldValues("VE_CD")
        Dim VE_list As String = ""
        Dim VE_List_Unique As String = ""

        Dim k As Integer
        For k = 0 To VE_Values.Count - 1
            VE_list = VE_list & VE_Values(k).ToString() & ","
            If InStr(VE_List_Unique, VE_Values(k).ToString() & ",") = 0 Then
                VE_List_Unique = VE_List_Unique & VE_Values(k).ToString() & ","
            End If
        Next k

        ' TODO these lists must be limited to inventory type (ITM.INVENTORY = Y) SKUs and their vendors
        '  TODO - do not want to create PO with no lines and need to 
        '  either redo the above or maybe determine the itm list first and then use it to drive based on match of other lists
        Generate_PO(ln_no_list, itm_list, qty_list, VE_list, VE_List_Unique)

    End Sub

    Public Sub Generate_PO(ByVal ln_no_list As String, ByVal itm_list As String, ByVal qty_list As String, ByVal VE_list As String, ByVal VE_List_Unique As String)

        lbl_PO_MSG.Text = ""

        If ASPxTextBox1.Text & "" = "" Or store_cd.Value & "" = "" Or cbo_po_sort.Value & "" = "" Then
            lbl_PO_MSG.Text = "You must complete entries for Sales Order, Destination Store Code and PO Sort Code."
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim objsql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim objsql3 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim sql As String
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim VEArray As Array
        Dim ITMArray As Array
        Dim QTYArray As Array
        Dim LN_NO_Array As Array
        Dim po_ln_no As Double = 1
        Dim po_cd As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            Dim MsgArray As Array
            MsgArray = Split(VE_List_Unique, ",")
            For x = LBound(MsgArray) To UBound(MsgArray)
                po_ln_no = 1
                '
                'Create the Purchase Order Header
                '
                sql = "SELECT VE_CD, NVL(TERMS_PCT,0) AS TERMS_PCT, NVL(TERMS_DAYS,0) AS TERMS_DAYS, " &
                            " NVL(DAYS_NET,0) AS DAYS_NET, NVL(EDI,'N') AS EDI, SHIP_VIA, CURRENCY_CD" &
                            " FROM VE WHERE VE_CD='" & MsgArray(x) & "'"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDataReader.Read() Then
                        po_cd = HBCG_Utils.PO_Creation(ASPxTextBox1.Text)

                        Dim currencyCd As String = MyDataReader.Item("CURRENCY_CD").ToString
                        Dim shipVia As String = MyDataReader.Item("SHIP_VIA").ToString
                        Dim cubeRate, exchangeRate As Decimal
                        If currencyCd.isEmpty Then
                            cubeRate = -1
                            exchangeRate = -1
                        Else
                            'get the currency details to save into PO table
                            Dim ds As DataSet = GetCurrencyRates(currencyCd)
                            For Each dr In ds.Tables(0).Rows
                                If ((dr("cube_rate").ToString).isNotEmpty) Then
                                    cubeRate = CDbl(dr("cube_rate"))
                                End If
                                If ((dr("ex_rate").ToString).isNotEmpty) Then
                                    exchangeRate = CDbl(dr("ex_rate"))
                                End If
                            Next
                        End If

                        sql = "INSERT INTO PO (PO_CD, STORE_CD, VE_CD, TERMS_PCT, TERMS_DAYS, DAYS_NET, SEND_VIA_EDI, STAT_CD, " &
                                    "PO_SRT_CD, WR_DT, LST_CHNG_DT, EMP_CD_OP, CURRENCY_CD,  CUBE_RATE, EX_RATE) VALUES "
                        sql = sql & "(:PO_CD, :STORE_CD, :VE_CD, :TERMS_PCT, :TERMS_DAYS, :DAYS_NET, :SEND_VIA_EDI, :STAT_CD, " &
                                    " :PO_SRT_CD, :WR_DT, SYSDATE, :EMP_CD_OP, :CURRENCY_CD,  :CUBE_RATE, :EX_RATE) "

                        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                        objsql.Parameters.Add(":PO_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":STORE_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":VE_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":TERMS_PCT", OracleType.Number)
                        objsql.Parameters.Add(":TERMS_DAYS", OracleType.Number)
                        objsql.Parameters.Add(":DAYS_NET", OracleType.Number)
                        objsql.Parameters.Add(":SEND_VIA_EDI", OracleType.VarChar)
                        objsql.Parameters.Add(":STAT_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":PO_SRT_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                        objsql.Parameters.Add(":WR_DT", OracleType.DateTime)
                        objsql.Parameters.Add(":CURRENCY_CD", OracleType.VarChar)
                        objsql.Parameters.Add(":CUBE_RATE", OracleType.Number)
                        objsql.Parameters.Add(":EX_RATE", OracleType.Number)

                        objsql.Parameters(":PO_CD").Value = po_cd
                        objsql.Parameters(":STORE_CD").Value = store_cd.Value
                        objsql.Parameters(":VE_CD").Value = MyDataReader.Item("VE_CD").ToString
                        objsql.Parameters(":TERMS_PCT").Value = MyDataReader.Item("TERMS_PCT").ToString
                        objsql.Parameters(":TERMS_DAYS").Value = MyDataReader.Item("TERMS_DAYS").ToString
                        objsql.Parameters(":DAYS_NET").Value = MyDataReader.Item("DAYS_NET").ToString
                        objsql.Parameters(":SEND_VIA_EDI").Value = MyDataReader.Item("EDI").ToString
                        objsql.Parameters(":STAT_CD").Value = "O"
                        objsql.Parameters(":PO_SRT_CD").Value = cbo_po_sort.Value
                        objsql.Parameters(":EMP_CD_OP").Value = Session("emp_cd")
                        objsql.Parameters(":WR_DT").Value = FormatDateTime(Today.Date, DateFormat.ShortDate)
                        objsql.Parameters(":CURRENCY_CD").Value = currencyCd
                        objsql.Parameters(":CUBE_RATE").Value = IIf(cubeRate < 0, System.DBNull.Value, cubeRate)
                        objsql.Parameters(":EX_RATE").Value = IIf(exchangeRate < 0, System.DBNull.Value, exchangeRate)

                        objsql.ExecuteNonQuery()
                        objsql.Parameters.Clear()

                        lbl_PO_MSG.Text = lbl_PO_MSG.Text & "PO " & po_cd & " created." & vbCrLf

                        VEArray = Split(VE_list, ",")
                        ITMArray = Split(itm_list, ",")
                        QTYArray = Split(qty_list, ",")
                        LN_NO_Array = Split(ln_no_list, ",")
                        For y = LBound(VEArray) To UBound(VEArray)
                            If VEArray(y) = MsgArray(x) Then
                                sql = "SELECT NVL(REPL_CST,0) AS REPL_CST, NVL(IVC_CST,0) AS IVC_CST, NVL(FGN_REPL_CST,0) AS FGN_UNIT_CST, NVL(FGN_DUTY_RATE,0) AS FGN_DUTY_RATE" &
                                    " FROM ITM WHERE ITM_CD='" & ITMArray(y) & "'"

                                objsql3 = DisposablesManager.BuildOracleCommand(sql, conn)
                                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql3)


                                If MyDataReader2.Read() Then
                                    sql = "INSERT INTO PO_LN (PO_CD, LN#, ITM_CD, DEST_STORE_CD, QTY_ORD, UNIT_CST, REPL_CST, IVC_CST, ARRIVAL_DT, INV_MATCHED, FGN_UNIT_CST, FGN_DUTY_RATE, SHIP_VIA) VALUES "
                                    sql = sql & "(:PO_CD, :LN#, :ITM_CD, :DEST_STORE_CD, :QTY_ORD, :UNIT_CST, :REPL_CST, :IVC_CST, '31-DEC-2049', 'N', :FGN_UNIT_CST, :FGN_DUTY_RATE, :SHIP_VIA) "

                                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                                    objsql.Parameters.Add(":PO_CD", OracleType.VarChar)
                                    objsql.Parameters.Add(":LN#", OracleType.Number)
                                    objsql.Parameters.Add(":ITM_CD", OracleType.VarChar)
                                    objsql.Parameters.Add(":DEST_STORE_CD", OracleType.VarChar)
                                    objsql.Parameters.Add(":QTY_ORD", OracleType.Number)
                                    objsql.Parameters.Add(":UNIT_CST", OracleType.Number)
                                    objsql.Parameters.Add(":REPL_CST", OracleType.Number)
                                    objsql.Parameters.Add(":IVC_CST", OracleType.Number)
                                    objsql.Parameters.Add(":FGN_UNIT_CST", OracleType.Number)
                                    objsql.Parameters.Add(":FGN_DUTY_RATE", OracleType.Number)
                                    objsql.Parameters.Add(":SHIP_VIA", OracleType.VarChar)

                                    objsql.Parameters(":PO_CD").Value = po_cd
                                    objsql.Parameters(":LN#").Value = po_ln_no
                                    objsql.Parameters(":ITM_CD").Value = ITMArray(y)
                                    objsql.Parameters(":DEST_STORE_CD").Value = store_cd.Value
                                    objsql.Parameters(":QTY_ORD").Value = QTYArray(y)
                                    objsql.Parameters(":UNIT_CST").Value = MyDataReader2.Item("REPL_CST").ToString
                                    objsql.Parameters(":REPL_CST").Value = MyDataReader2.Item("REPL_CST").ToString
                                    objsql.Parameters(":IVC_CST").Value = MyDataReader2.Item("IVC_CST").ToString
                                    objsql.Parameters(":FGN_UNIT_CST").Value = MyDataReader2.Item("FGN_UNIT_CST").ToString
                                    objsql.Parameters(":FGN_DUTY_RATE").Value = MyDataReader2.Item("FGN_DUTY_RATE").ToString
                                    objsql.Parameters(":SHIP_VIA").Value = shipVia

                                    objsql.ExecuteNonQuery()
                                    objsql.Parameters.Clear()

                                    '
                                    'Create link to Sales Order
                                    '
                                    sql = "INSERT INTO SO_LN$PO_LN (PO_CD, LN#, DEL_DOC_NUM, DEL_DOC_LN#) VALUES "
                                    sql = sql & "(:PO_CD, :LN#, :DEL_DOC_NUM, :DEL_DOC_LN#) "

                                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                                    objsql.Parameters.Add(":PO_CD", OracleType.VarChar)
                                    objsql.Parameters.Add(":LN#", OracleType.Number)
                                    objsql.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                                    objsql.Parameters.Add(":DEL_DOC_LN#", OracleType.Number)

                                    objsql.Parameters(":PO_CD").Value = po_cd
                                    objsql.Parameters(":LN#").Value = po_ln_no
                                    objsql.Parameters(":DEL_DOC_NUM").Value = ASPxTextBox1.Text
                                    objsql.Parameters(":DEL_DOC_LN#").Value = LN_NO_Array(y)

                                    objsql.ExecuteNonQuery()
                                    objsql.Parameters.Clear()

                                    '
                                    'Create Action History Record
                                    '
                                    sql = "INSERT INTO PO_LN$ACTN_HST (PO_CD, LN#, PO_ACTN_TP_CD, DT_TIME, QTY, AMT, RCV_LABELS) VALUES "
                                    sql = sql & "(:PO_CD, :LN#, 'INI', SYSDATE, :QTY, :AMT, 'Y') "

                                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                                    objsql.Parameters.Add(":PO_CD", OracleType.VarChar)
                                    objsql.Parameters.Add(":LN#", OracleType.Number)
                                    objsql.Parameters.Add(":QTY", OracleType.Number)
                                    objsql.Parameters.Add(":AMT", OracleType.Number)

                                    objsql.Parameters(":PO_CD").Value = po_cd
                                    objsql.Parameters(":LN#").Value = po_ln_no
                                    objsql.Parameters(":QTY").Value = QTYArray(y)
                                    objsql.Parameters(":AMT").Value = MyDataReader2.Item("REPL_CST").ToString()

                                    objsql.ExecuteNonQuery()
                                    objsql.Parameters.Clear()
                                    po_ln_no = po_ln_no + 1
                                End If
                                MyDataReader2.Close()
                            End If
                        Next
                    End If
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Next
            Session("PO_MSG") = lbl_PO_MSG.Text
            Response.Redirect("Generate_PO.aspx?del_doc_num=" & ASPxTextBox1.Text)
        Catch
            Throw
        End Try

    End Sub


    ''' <summary>
    ''' Retrieves the details such as exchange reate, etc. for the currency code passed-in 
    ''' </summary>
    ''' <param name="currencyCd">the currency code for which to get exchange rate, etc. for,  e.g. 'US, CAN, etc.'</param>
    ''' <returns>a Dataset consisting of the cube and exchange rate</returns>
    Public Shared Function GetCurrencyRates(ByVal currencyCd As String) As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As DataSet = New DataSet()
        Dim oAdp As OracleDataAdapter
        Dim cmd As OracleCommand
        Dim sql As String =
            "SELECT cube_rate, ex_rate" &
                       " FROM CURRENCY_RATES" &
                       " WHERE currency_cd = '" & currencyCd & "' "
        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(cmd)
            oAdp.Fill(ds, "CURRENCY_RATES")
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return ds
    End Function




    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Public Sub po_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT po_srt_cd, po_srt_cd || ' - ' || des as full_desc FROM PO_SRT order by PO_SRT_CD"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_po_sort
                .DataSource = ds
                .ValueField = "po_srt_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub ASPxTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If ASPxTextBox1.Text & "" <> "" Then
            Load_Grid()
        End If

    End Sub
End Class
