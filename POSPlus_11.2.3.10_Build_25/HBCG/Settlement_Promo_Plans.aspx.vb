Imports System.Data.OracleClient
Imports System.Data
Imports System.Security.Cryptography
Imports HBCG_Utils
Imports AppExtensions
Imports System.Xml

Partial Class Settlement_Promo_Plans
    Inherits POSBasePage
    Private Const cResponseCriteria As String = "/merchantCode20Response/response/merchantData"
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        ds = New DataSet

        GridView1.DataSource = ""
        GridView1.Visible = True

        conn.Open()

        sql = "SELECT PROMO_CD, DEFER_MONTHS, INVOICE_NAME, INVOICE_NAME_WDR, PROMO_DES, LONG_DESC, SHORT_DESC, " &
                    " PLAN_ACTIVE, EFF_DT, END_DT, DEFER_DT, WDR_CODE " &
                    " FROM PROMO_ADDENDUMS WHERE AS_CD='" & cbo_finance_company.Value & "' ORDER BY PROMO_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        GridView1.Visible = True
        GridView1.DataSource = dv
        GridView1.DataBind()
        conn.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Add_Finance_Company()
            bindgrid()
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound


        Dim lbl_plan_status As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_plan_status"), DevExpress.Web.ASPxEditors.ASPxLabel)
        If Not IsNothing(lbl_plan_status) Then

            If IsDate(e.Item.Cells(3).Text) And IsDate(e.Item.Cells(4).Text) Then
                If FormatDateTime(e.Item.Cells(3).Text, DateFormat.ShortDate) <= Now.Date And FormatDateTime(e.Item.Cells(4).Text, DateFormat.ShortDate) >= Now.Date Then
                    lbl_plan_status.Text = "Available"
                ElseIf FormatDateTime(e.Item.Cells(4).Text, DateFormat.ShortDate) <= Now.Date Then
                    lbl_plan_status.Text = "Expired"
                ElseIf FormatDateTime(e.Item.Cells(3).Text, DateFormat.ShortDate) >= Now.Date Then
                    lbl_plan_status.Text = "Pending"
                End If
            End If
        End If

        Dim CheckBox1 As CheckBox = CType(e.Item.FindControl("CheckBox1"), CheckBox)
        Dim CheckBox2 As CheckBox = CType(e.Item.FindControl("CheckBox2"), CheckBox)

        If Not IsNothing(CheckBox1) And Not IsNothing(CheckBox2) Then
            If e.Item.Cells(9).Text = "Y" Then
                CheckBox1.Checked = True
            Else
                CheckBox1.Checked = False
            End If

            If e.Item.Cells(10).Text = "Y" Then
                CheckBox2.Checked = True
            Else
                CheckBox2.Checked = False
            End If

            If Not IsNothing(lbl_plan_status) Then
                If lbl_plan_status.Text <> "Available" Then
                    CheckBox2.Enabled = False
                Else
                    CheckBox2.Enabled = True
                End If
            End If

            If DropDownList1.Value = "Active" Then
                If CheckBox2.Checked = False Then
                    e.Item.Visible = False
                Else
                    e.Item.Visible = True
                End If
            ElseIf DropDownList1.Value = "Pending" Then
                If e.Item.Cells(11).Text <> "Pending" Then
                    e.Item.Visible = False
                End If
            ElseIf DropDownList1.Value = "Available" Then
                If e.Item.Cells(11).Text <> "Available" Then
                    e.Item.Visible = False
                End If
            ElseIf DropDownList1.Value = "Expired" Then
                If e.Item.Cells(11).Text <> "Expired" Then
                    e.Item.Visible = False
                End If
            End If

        End If

    End Sub

    Public Sub Update_invoice_wdr(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck1 As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxTextBox)

        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        sql = "UPDATE PROMO_ADDENDUMS SET INVOICE_NAME_WDR='" & ck1.Text.ToString &
                    "' WHERE PROMO_CD='" & dgItem.Cells(14).Text &
                    "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_invoice_no_wdr(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck1 As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxTextBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        sql = "UPDATE PROMO_ADDENDUMS SET INVOICE_NAME='" & ck1.Text.ToString &
                    "' WHERE PROMO_CD='" & dgItem.Cells(14).Text &
                    "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_Text(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        'Dim ck1 As TextBox = CType(sender, TextBox)
        Dim ck1 As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxTextBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)


        sql = "UPDATE PROMO_ADDENDUMS SET SHORT_DESC='" & ck1.Text.ToString &
                    "' WHERE PROMO_CD='" & dgItem.Cells(14).Text &
                    "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Shared Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT EMP_CD FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("EMP_CD") & "" <> "" Then
                    Authorize_User = True
                Else
                    Authorize_User = False
                End If
            Else
                Authorize_User = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Public Sub Update_Active(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE PROMO_ADDENDUMS SET PLAN_ACTIVE='" & ck_value &
                    "' WHERE PROMO_CD='" & dgItem.Cells(14).Text &
                    "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_WDR(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE PROMO_ADDENDUMS SET WDR_CODE='" & ck_value & "' WHERE PROMO_CD='" & dgItem.Cells(14).Text & "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_defer_mths(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        'Dim ck1 As TextBox = CType(sender, TextBox)
        Dim ck1 As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxTextBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        sql = "UPDATE PROMO_ADDENDUMS SET DEFER_MONTHS='" & ck1.Text &
                    "' WHERE PROMO_CD='" & dgItem.Cells(14).Text &
                    "' AND AS_CD='" & cbo_finance_company.Value & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Add_Finance_Company()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' ORDER BY NAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.SelectedIndex = 0

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.ValueField = "AS_CD"
                cbo_finance_company.TextField = "NAME"
                cbo_finance_company.DataBind()
                'cbo_promo_fin.DataSource = ds
                'cbo_promo_fin.DataValueField = "AS_CD"
                'cbo_promo_fin.DataTextField = "NAME"
                'cbo_promo_fin.DataBind()
            End If

            cbo_finance_company.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label415, ""))
            cbo_finance_company.Items.FindByText(Resources.LibResources.Label415).Value = ""
            cbo_finance_company.SelectedIndex = 0
            'cbo_promo_fin.Items.Insert(0, "Please select finance company")
            'cbo_promo_fin.Items.FindByText("Please select finance company").Value = ""
            'cbo_promo_fin.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

    '    Dim response As New XmlDocument
    '    response = SendRequestAndGetResponse()
    '    ParseResponseAndFillDS(response)

    'End Sub

    'Public Function SendRequestAndGetResponse() As XmlDocument

    '    Dim oWebReq As System.Net.HttpWebRequest
    '    oWebReq = CType(System.Net.HttpWebRequest.Create("https://retailuat.alldata.net/axes103/web-services/merchantCode"), System.Net.HttpWebRequest)

    '    Dim xml_string As String = ""

    '    xml_string = "<?xml version=""1.0"" encoding=""utf-8""?>"
    '    xml_string = xml_string & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
    '    xml_string = xml_string & "<soap:Body>"
    '    xml_string = xml_string & "<ns1:merchantCode20 xmlns:ns1=""http://axes.alldata.net/web-services"" version=""2.0"">"
    '    xml_string = xml_string & "<request>"
    '    xml_string = xml_string & "<clientHeader>"
    '    xml_string = xml_string & "<clientName>LEVIN</clientName>"
    '    xml_string = xml_string & "<timestamp>" & FormatDateTime(Now.Date.ToString, DateFormat.ShortDate) & " " & FormatDateTime(Now.TimeOfDay.ToString, DateFormat.LongTime) & "</timestamp>"
    '    xml_string = xml_string & "</clientHeader>"
    '    xml_string = xml_string & "</request>"
    '    xml_string = xml_string & "</ns1:merchantCode20>"
    '    xml_string = xml_string & "</soap:Body>"
    '    xml_string = xml_string & "</soap:Envelope>"

    '    _requestXML = New XmlDataDocument
    '    _requestXML.RemoveAll()
    '    _requestXML.LoadXml(xml_string)

    '    Dim reqBytes As Byte()
    '    reqBytes = System.Text.UTF8Encoding.UTF8.GetBytes(_requestXML.InnerXml)
    '    oWebReq.ContentLength = reqBytes.Length
    '    oWebReq.Method = "POST"
    '    oWebReq.ContentType = "text/xml"
    '    oWebReq.Credentials = System.Net.CredentialCache.DefaultCredentials
    '    Try
    '        Dim oReqStream As System.IO.Stream = oWebReq.GetRequestStream()
    '        oReqStream.Write(reqBytes, 0, reqBytes.Length)
    '        Dim responseBytes As Byte()
    '        Dim oWebResp As System.Net.HttpWebResponse = CType(oWebReq.GetResponse(), System.Net.HttpWebResponse)
    '        If oWebResp.StatusCode = Net.HttpStatusCode.OK Then
    '            responseBytes = processResponse(oWebResp)
    '            BuildResponseDocument(responseBytes)
    '        End If
    '        oWebResp.Close()
    '    Catch ex As Exception
    '        If Err.Number = 5 Then
    '            Return _responseXML
    '            Exit Function
    '        End If
    '        Throw
    '    End Try
    '    Return _responseXML

    'End Function

    'Public Sub BuildResponseDocument(ByRef responseBytes As Byte())

    '    Dim strResp As String = System.Text.UTF8Encoding.UTF8.GetString(responseBytes)
    '    _responseXML.RemoveAll()
    '    _responseXML.LoadXml(strResp)

    'End Sub

    'Private Function processResponse(ByRef oWebResp As System.Net.HttpWebResponse) As Byte()
    '    Dim memStream As New System.IO.MemoryStream()
    '    Const BUFFER_SIZE As Integer = 4096
    '    Dim iRead As Integer = 0
    '    Dim idx As Integer = 0
    '    Dim iSize As Int64 = 0
    '    memStream.SetLength(BUFFER_SIZE)
    '    While (True)
    '        Dim respBuffer(BUFFER_SIZE) As Byte
    '        Try
    '            iRead = oWebResp.GetResponseStream().Read(respBuffer, 0, BUFFER_SIZE)
    '        Catch e As System.Exception
    '            Throw e
    '        End Try
    '        If (iRead = 0) Then
    '            Exit While
    '        End If
    '        iSize += iRead
    '        memStream.SetLength(iSize)
    '        memStream.Write(respBuffer, 0, iRead)
    '        idx += iRead
    '    End While

    '    Dim content As Byte() = memStream.ToArray()
    '    memStream.Close()
    '    Return content
    'End Function


    'Public Sub ParseResponseAndFillDS(ByRef responseXML As XmlDocument)

    '    Dim conn As New OracleConnection
    '    Dim ds As New DataSet
    '    Dim cmdGetCodes As OracleCommand
    '    cmdGetCodes = DisposablesManager.BuildOracleCommand

    '    Dim SQL As String

    '    SQL = "SELECT PROMO_CD FROM PROMO_ADDENDUMS WHERE AS_CD='WORLDFINAN' ORDER BY PROMO_CD"

    '    conn.ConnectionString = ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString

    '    Try
    '        With cmdGetCodes
    '            .Connection = conn
    '            .CommandText = SQL
    '        End With

    '        conn.Open()
    '        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
    '        oAdp.Fill(ds)
    '    Catch
    '        conn.Close()
    '        Throw
    '    End Try

    '    Dim dt As DataTable = ds.Tables(0)
    '    Dim dr As DataRow

    '    Dim modifyxml As String = responseXML.InnerXml.ToString
    '    modifyxml = Replace(modifyxml, "<S:Envelope xmlns:S=""http://schemas.xmlsoap.org/soap/envelope/""><S:Body>", "")
    '    modifyxml = Replace(modifyxml, "</S:Body></S:Envelope>", "")
    '    modifyxml = Replace(modifyxml, "ns2:", "")
    '    modifyxml = Replace(modifyxml, ":ns2", "")
    '    modifyxml = Replace(modifyxml, "xmlns=""http://axes.alldata.net/web-services""", "")
    '    modifyxml = Replace(modifyxml, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")
    '    responseXML.RemoveAll()
    '    responseXML.InnerXml = modifyxml

    '    Dim wf_promo_cd As String = ""
    '    Dim match_found As Boolean = False
    '    Dim m_nodelist As XmlNodeList
    '    Dim m_node As XmlNode
    '    m_nodelist = responseXML.SelectNodes(cResponseCriteria)
    '    For Each m_node In m_nodelist
    '        match_found = False
    '        wf_promo_cd = m_node.ChildNodes.Item(0).InnerText
    '        wf_promo_cd = wf_promo_cd.PadLeft(3, "0")
    '        For Each dr In ds.Tables(0).Rows
    '            If dr("PROMO_CD") = wf_promo_cd Then
    '                match_found = True
    '                Exit For
    '            End If
    '        Next
    '        If match_found = True Then
    '            SQL = "UPDATE PROMO_ADDENDUMS SET LONG_DESC='" & m_node.ChildNodes.Item(1).InnerText & "', "
    '            If IsNumeric(Microsoft.VisualBasic.Left(m_node.ChildNodes.Item(1).InnerText, 2)) Then
    '                SQL = SQL & "DEFER_MONTHS=" & Microsoft.VisualBasic.Left(m_node.ChildNodes.Item(1).InnerText, 2) & ", "
    '            End If
    '            SQL = SQL & "EFF_DT=TO_DATE('" & Process_Date(m_node.ChildNodes.Item(2).InnerText) & "','mm/dd/yyyy'), "
    '            SQL = SQL & "END_DT=TO_DATE('" & Process_Date(m_node.ChildNodes.Item(3).InnerText) & "','mm/dd/yyyy'), "
    '            SQL = SQL & "DEFER_DT=TO_DATE('" & Process_Date(m_node.ChildNodes.Item(4).InnerText) & "','mm/dd/yyyy') "
    '            SQL = SQL & "WHERE PROMO_CD='" & wf_promo_cd & "' AND AS_CD='WORLDFINAN'"
    '        Else
    '            SQL = "INSERT INTO PROMO_ADDENDUMS (LONG_DESC, "
    '            If IsNumeric(Microsoft.VisualBasic.Left(m_node.ChildNodes.Item(1).InnerText, 2)) Then
    '                SQL = SQL & "DEFER_MONTHS, "
    '            End If
    '            SQL = SQL & "EFF_DT, END_DT, DEFER_DT, PROMO_CD, AS_CD) VALUES('" & m_node.ChildNodes.Item(1).InnerText & "', "
    '            If IsNumeric(Microsoft.VisualBasic.Left(m_node.ChildNodes.Item(1).InnerText, 2)) Then
    '                SQL = SQL & Microsoft.VisualBasic.Left(m_node.ChildNodes.Item(1).InnerText, 2) & ", "
    '            End If
    '            SQL = SQL & "TO_DATE('" & Process_Date(m_node.ChildNodes.Item(2).InnerText) & "','mm/dd/yyyy'), "
    '            SQL = SQL & "TO_DATE('" & Process_Date(m_node.ChildNodes.Item(3).InnerText) & "','mm/dd/yyyy'), "
    '            SQL = SQL & "TO_DATE('" & Process_Date(m_node.ChildNodes.Item(4).InnerText) & "','mm/dd/yyyy'), "
    '            SQL = SQL & "'" & wf_promo_cd & "','WORLDFINAN')"
    '        End If
    '        cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
    '        cmdGetCodes.ExecuteNonQuery()
    '    Next

    '    'Update records in GERS
    '    Dim ds2 As New DataSet
    '    SQL = "SELECT PROMO_CD FROM PROMO_ADDENDUMS WHERE AS_CD='WORLDFINAN' ORDER BY PROMO_CD"

    '    Try
    '        With cmdGetCodes
    '            .Connection = conn
    '            .CommandText = SQL
    '        End With

    '        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
    '        oAdp.Fill(ds2)
    '    Catch
    '        conn.Close()
    '        Throw
    '    End Try

    '    dt = ds2.Tables(0)
    '    conn.Close()

    '    Dim MyDatareader As OracleDataReader
    '    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '    conn.Open()

    '    SQL = "SELECT PROMO_CD FROM ASP_PROMO WHERE AS_CD='WORLDFINAN' ORDER BY PROMO_CD"

    '    'Set SQL OBJECT 
    '    cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
    '    Dim long_desc As String = ""
    '    Dim eff_dt As String = ""
    '    Dim end_dt As String = ""
    '    Dim promo_cd As String = ""
    '    Try
    '        MyDataReader = cmdGetCodes.ExecuteReader
    '        Do While MyDatareader.Read()
    '            long_desc = ""
    '            eff_dt = ""
    '            end_dt = ""
    '            promo_cd = ""
    '            For Each dr In ds2.Tables(0).Rows
    '                long_desc = dr("LONG_DESC")
    '                eff_dt = dr("EFF_DT")
    '                end_dt = dr("END_DT")
    '                promo_cd = dr("PROMO_CD")
    '                If dr("PROMO_CD") = MyDatareader.Item("PROMO_CD").ToString Then
    '                    match_found = True
    '                    Exit For
    '                End If
    '            Next
    '            If match_found = True Then
    '                SQL = "UPDATE ASP_PROMO SET DES='" & long_desc & "', "
    '                If IsNumeric(Microsoft.VisualBasic.Left(long_desc, 2)) Then
    '                    SQL = SQL & "DEFER_MONTHS=" & Microsoft.VisualBasic.Left(long_desc, 2) & ", "
    '                End If
    '                SQL = SQL & "EFF_DT=TO_DATE('" & eff_dt & "','mm/dd/yyyy'), "
    '                SQL = SQL & "END_DT=TO_DATE('" & end_dt & "','mm/dd/yyyy'), "
    '                SQL = SQL & "WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='WORLDFINAN'"
    '            Else
    '                SQL = "INSERT INTO ASP_PROMO (DES, EFF_DT, END_DT, PROMO_CD, AS_CD "
    '                If IsNumeric(Microsoft.VisualBasic.Left(long_desc, 2)) Then
    '                    SQL = SQL & ",DEFER_MONTHS"
    '                End If
    '                SQL = SQL & ") VALUES('" & long_desc & "', "
    '                SQL = SQL & "TO_DATE('" & eff_dt & "','mm/dd/yyyy'), "
    '                SQL = SQL & "TO_DATE('" & end_dt & "','mm/dd/yyyy'), "
    '                SQL = SQL & "'" & promo_cd & "','WORLDFINAN'"
    '                If IsNumeric(Microsoft.VisualBasic.Left(long_desc, 2)) Then
    '                    SQL = SQL & "," & Microsoft.VisualBasic.Left(long_desc, 2)
    '                End If
    '                SQL = SQL & ")"
    '            End If
    '            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
    '            cmdGetCodes.ExecuteNonQuery()
    '        Loop

    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    '    bindgrid()
    '    conn.Close()

    'End Sub

    Public Function Process_Date(ByVal incoming_dt As String)

        Process_Date = incoming_dt.Substring(5, 2) & "/" & Microsoft.VisualBasic.Right(incoming_dt, 2) & "/" & Microsoft.VisualBasic.Left(incoming_dt, 4)

    End Function


    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        resetUpdateText()
        bindgrid()
    End Sub

    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged
        resetUpdateText()
        bindgrid()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As New DataSet
        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String
        Dim match_found As Boolean = False

        Dim MyDatareader As OracleDataReader
        Dim MyDatareader2 As OracleDataReader

        conn.Open()

        conn2.Open()

        SQL = "SELECT AS_CD, PROMO_CD, EFF_DT, END_DT, DES FROM ASP_PROMO ORDER BY PROMO_CD"

        'Set SQL OBJECT 
        cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
        Dim long_desc As String = ""
        Dim short_desc As String = ""
        Dim promo_desc As String = ""
        Dim eff_dt As String = ""
        Dim end_dt As String = ""
        Dim promo_cd As String = ""
        Dim as_cd As String = ""
        Dim updateCount As Integer = 0
        resetUpdateText()

        Try
            MyDatareader = DisposablesManager.BuildOracleDataReader(cmdGetCodes)

            Do While MyDatareader.Read()
                match_found = False
                long_desc = ""
                short_desc = ""
                promo_desc = ""
                eff_dt = ""
                end_dt = ""
                promo_cd = ""
                as_cd = ""
                SQL = "SELECT AS_CD, PROMO_CD FROM PROMO_ADDENDUMS WHERE AS_CD='" & MyDatareader.Item("AS_CD").ToString & "' " &
                             "AND PROMO_CD='" & MyDatareader.Item("PROMO_CD").ToString & "'"
                objsql = DisposablesManager.BuildOracleCommand(SQL, conn2)
                MyDatareader2 = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDatareader2.Read() Then
                    If MyDatareader2.Item("AS_CD").ToString & "" <> "" Then
                        match_found = True
                    End If
                End If
                MyDatareader2.Close()

                'the descriptions from ASP_Promo are larger and need to be trimmed before saving to the promo_addendum table
                Dim desc As String = MyDatareader.Item("DES").ToString
                promo_desc = desc.trimString(0, 150)
                long_desc = desc.trimString(0, 50)
                short_desc = desc.trimString(0, 30)

                If IsDate(MyDatareader.Item("EFF_DT").ToString) Then
                    eff_dt = FormatDateTime(MyDatareader.Item("EFF_DT").ToString, DateFormat.ShortDate)
                End If
                If IsDate(MyDatareader.Item("END_DT").ToString) Then
                    end_dt = FormatDateTime(MyDatareader.Item("END_DT").ToString, DateFormat.ShortDate)
                End If
                promo_cd = MyDatareader.Item("PROMO_CD").ToString
                as_cd = MyDatareader.Item("AS_CD").ToString

                If match_found = True Then
                    SQL = "UPDATE PROMO_ADDENDUMS SET PROMO_DES='" & Replace(promo_desc, "'", "''") & "', SHORT_DESC='" & Replace(short_desc, "'", "''") & "', LONG_DESC='" & Replace(long_desc, "'", "''") & "',  "
                    If IsNumeric(Microsoft.VisualBasic.Left(desc, 2)) Then
                        SQL = SQL & "DEFER_MONTHS=" & Microsoft.VisualBasic.Left(desc, 2) & ", "
                    End If
                    SQL = SQL & "EFF_DT=TO_DATE('" & eff_dt & "','mm/dd/yyyy'), "
                    SQL = SQL & "END_DT=TO_DATE('" & end_dt & "','mm/dd/yyyy') "
                    SQL = SQL & "WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & as_cd & "'"
                    updateCount = updateCount + 1
                Else
                    SQL = "INSERT INTO PROMO_ADDENDUMS (PROMO_DES, SHORT_DESC, LONG_DESC, EFF_DT, END_DT, PROMO_CD, AS_CD "
                    If IsNumeric(Microsoft.VisualBasic.Left(desc, 2)) Then
                        SQL = SQL & ",DEFER_MONTHS"
                    End If
                    SQL = SQL & ") VALUES('" & Replace(promo_desc, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(short_desc, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(long_desc, "'", "''") & "', "
                    SQL = SQL & "TO_DATE('" & eff_dt & "','mm/dd/yyyy'), "
                    SQL = SQL & "TO_DATE('" & end_dt & "','mm/dd/yyyy'), "
                    SQL = SQL & "'" & promo_cd & "','" & as_cd & "'"
                    If IsNumeric(Microsoft.VisualBasic.Left(long_desc, 2)) Then
                        SQL = SQL & "," & Microsoft.VisualBasic.Left(long_desc, 2)
                    End If
                    SQL = SQL & ")"
                    updateCount = updateCount + 1
                End If

                cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn2)
                cmdGetCodes.ExecuteNonQuery()
            Loop

            'Close Connection 
            MyDatareader.Close()
            conn.Close()
            conn2.Close()
            If (updateCount > 0) Then
                ASPxLabelUpdateStatus.Text = String.Format("Update Complete.")
            Else
                resetUpdateText()
            End If

        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        bindgrid()
        conn.Close()

    End Sub

    Private Sub resetUpdateText()
        ASPxLabelUpdateStatus.Text = ""
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class

