<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="CPHQ.aspx.vb" Inherits="CPHQ" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                </dx:ASPxLabel>
            </td>
            <td  style="width: 109px">
                <asp:TextBox ID="txt_Cust_cd" runat="server" Style="position: relative" CssClass="style5"></asp:TextBox>&nbsp;</td>

            <td style="width: 109px">
                <dx:ASPxLabel ID="lbl_tax_exempt" runat="server" Text="Tax Exempt">
                </dx:ASPxLabel>
            </td>

             <td  >
                <asp:TextBox ID="txt_tax_exempt" runat="server" Width="109px" CssClass="style5" MaxLength="20" >
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="lbl_alt_cust_cd" runat="server" Text="Alt_cust_cd">
                </dx:ASPxLabel>
            </td>

           
            <td style="width: 109px">
                <asp:TextBox ID="txt_alt_cust_cd" runat="server" Style="position: relative" CssClass="style5"
                    ></asp:TextBox>&nbsp;</td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label229 %>">
                </dx:ASPxLabel>
            </td>
                        <td>
                <asp:TextBox ID="txt_h_phone" runat="server" Width="90px" CssClass="style5" MaxLength="12"></asp:TextBox></td>
        </tr>
         <tr>
              <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel_crop_nm" runat="server" Text=" Corp Name">
                </dx:ASPxLabel>
            </td>
         <td style="width: 109px">
              <asp:TextBox ID="txt_corp_name" runat="server" ></asp:TextBox>

         </td>
             <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label49 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_b_phone" runat="server" Style="left: 0px; position: relative"
                    Width="90px" CssClass="style5" MaxLength="12"></asp:TextBox></td>
             </tr>
         
             
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label211 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_Fname" runat="server" Style="position: relative" CssClass="style5"
                    MaxLength="15"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                  <dx:ASPxLabel ID="ASPxLabel21" visible="false" runat="server" Text="<%$ Resources:LibResources, Label633 %>">
                </dx:ASPxLabel>
                
                &nbsp;&nbsp;&nbsp;<asp:DropDownList
                        ID="cbo_cust_tp" visible="false" Style="position: relative;" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="I" Text="<%$ Resources:LibResources, Label236 %>"></asp:ListItem>
                        <asp:ListItem Value="C" Text="<%$ Resources:LibResources, Label112 %>"></asp:ListItem>
                    </asp:DropDownList>
            </td>
           
             
             
            <td>
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label195 %>">
                </dx:ASPxLabel>
            </td>
        
             
            <td>
                <asp:TextBox ID="txt_Ext" runat="server" Style="left: 0px; position: relative" Width="40px"
                    CssClass="style5" MaxLength="4"></asp:TextBox>

            </td>
        </tr>
         
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label263 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_Lname" runat="server" Style="position: relative" CssClass="style5"
                    MaxLength="20"></asp:TextBox></td>
            
            <td style="width: 10px">
                <dx:ASPxLabel ID="ASPxLabel_cust_tp" runat="server" Text=" Cust Tp">
                </dx:ASPxLabel>
            </td>
             
             <td>

             <asp:TextBox ID="txt_cust_tp" runat="server" Style="left: 0px; position: relative"
                    Width="8px" CssClass="style5" MaxLength="1"></asp:TextBox> 
               
                  <asp:TextBox ID="txt_tp_nm" runat="server" Style="left: 0px; position: relative"
                    Width="80px" CssClass="style5" MaxLength="20"></asp:TextBox> 
             

            </td>
         </tr>

            <tr>

            <td >
               
               <dx:ASPxLabel ID="lbl_srt_cd" runat="server" Text="<%$ Resources:LibResources, Label560 %>"></dx:ASPxLabel> 
                  </td>

                 <td >
               <asp:TextBox ID="txt_srt_cd" runat="server" Style="left: 0px; position: relative"
                    Width="30px" CssClass="style5" MaxLength="3"></asp:TextBox>
                          
               <asp:TextBox ID="txt_srt_desc" runat="server" Style=" position: relative"
                    Width="120px" CssClass="style5" MaxLength="40"></asp:TextBox>
               </td>
             </tr>
        </tr>
    </table>
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Width="99%" ActiveTabIndex="0"
        AutoPostBack="True" EnableCallBacks="False">
        <TabPages>
            <dx:TabPage Text="<%$ Resources:LibResources, Label135 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%" class="style5">
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label20 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_addr1" runat="server" Style="position: relative" Width="320px"
                                        CssClass="style5" MaxLength="30"></asp:TextBox></td>
                                <td colspan="2" rowspan="9" valign="top" align="left" bgcolor="beige" bordercolor="#000000">
                                    <asp:Label ID="lbl_summary" runat="server" visible="true"  CssClass ="style5"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label21 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_addr2" runat="server" Style="position: relative; top: 0px; left: 0px; height: 25px;" Width="320px"
                                        CssClass="style5" MaxLength="30"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label80 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_City" runat="server" Style="position: relative; left: 0px; top: 0px;"
                                        CssClass="style5" MaxLength="20" Width="122px"></asp:TextBox>
                                    &nbsp;
                                     <asp:TextBox ID="txt_prv" Width="100px" runat="server" CssClass="style5"></asp:TextBox>
                                     <asp:DropDownList ID="cbo_State" Width="100px" runat="server" CssClass="style5" visible="false"> </asp:DropDownList>
                                     <asp:TextBox ID="txt_Zip" runat="server" Style="position: relative; top: 0px; left: 0px;"
                                        Width="58px" CssClass="style5" MaxLength="10" AutoPostBack="True" OnTextChanged="txt_Zip_TextChanged"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label115 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_county" runat="server" Style="left: 0px; position: relative"
                                        CssClass="style5" MaxLength="20"></asp:TextBox>
                                    /
                                    <asp:TextBox ID="txt_country" runat="server" Style="left: 0px; position: relative"
                                        CssClass="style5" MaxLength="20" Width="153px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label179 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_Email" runat="server" Style="position: relative" Width="320px"
                                        CssClass="style5" MaxLength="60"></asp:TextBox></td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

           

            <dx:TabPage Text="<%$ Resources:LibResources, Label441 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div class="style5" width="100%">
                            <dx:ASPxLabel ID="ASPxLabel17" runat="server"  visible="false" Text="<%$ Resources:LibResources, Label6 %>">
                            </dx:ASPxLabel>
                            &nbsp; &nbsp;<asp:TextBox ID="txt_acct_balance" visible="false" runat="server" ReadOnly="True" CssClass="style5"
                                Width="102px" Text="$0.00"></asp:TextBox>
                            <br />
                            <br />
                            <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3" DataKeyField="sales_order" Height="16px"
                                OnSortCommand="MyDataGrid_Sort" PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages"
                                Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label511 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                     <asp:BoundColumn DataField="REFERENCE" HeaderText="Reference" SortExpression="REFERENCE asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ORD_TP" HeaderText="<%$ Resources:LibResources, Label944 %>"  SortExpression="ORD_TP asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FINAL_DATE" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label202 %>"
                                        SortExpression="FINAL_DATE asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                              
                                    <asp:BoundColumn DataField="ITM_CD" HeaderText="Sku" SortExpression="ITM_CD asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="QTY" HeaderText="<%$ Resources:LibResources, Label447 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="UNIT_PRC" HeaderText="<%$ Resources:LibResources, Label427 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    
                                    <asp:BoundColumn DataField="OUT_ASIS" HeaderText="<%$ Resources:LibResources, Label750 %>"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="WTY" HeaderText="Wty"  >
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SALES_ORDER" HeaderText="<%$ Resources:LibResources, Label511 %>" visible= false SortExpression="SALES_ORDER asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                                                                                        
                                   <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal ID="Literal1" runat="server"   Visible="false"  Text="<%$ Resources:LibResources, Label44 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_balance" runat="server" Visible="false"  CssClass="style5" Text="0"></asp:Label><br />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                      
                                           
                                      
                                    <asp:BoundColumn DataField="sales_order" HeaderText="" Visible="false"></asp:BoundColumn>

                                     <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:LibResources, Label509 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateColumn>
                                    
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="Black" CssClass="style5" />
                            </asp:DataGrid>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            
           
            <dx:TabPage Text="<%$ Resources:LibResources, Label95 %>">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server">
                        <table width="100%" height="100%" class="style5">
                            <%-- 
                            <tr>
                                <td valign="top" align="left" width="50%">
                                    <asp:Image ID="Image1" runat="server"  visible="false" ImageUrl="~/images/icons/comments.gif"   />
                                </td>
                            </tr>
                               
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_cust_comments" runat="server" visible ="false" Height="1px" Rows="3" TextMode="MultiLine"
                                        Width="624px" CssClass="style5"></asp:TextBox><br />
                                    <br />
                                        
                                 --%>
                                    <asp:TextBox ID="txt_cust_past" runat="server" Height="200px" Rows="3" TextMode="MultiLine"
                                        Width="624px" CssClass="style5"></asp:TextBox>
                                    <br />
                                    <br />
                            
                                    <asp:Button ID="btn_save_comments" runat="server" visible="false" Text="<%$ Resources:LibResources, Label521 %>" Width="119px"
                                        CssClass="style5" />  
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
         

              <dx:TabPage Text="Account Query">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl3" runat="server">
                        <table width="100%" height="100%" class="style5">
                            <tr>
                                <td valign="top" align="left" width="50%">
                                    <asp:Image ID="Image2" runat="server" visible="false" ImageUrl="~/images/icons/comments.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                   
                                   
                                    <br />
                                    <asp:Button ID="Button1" runat="server"  visible="false" Text="<%$ Resources:LibResources, Label521 %>" Width="119px"
                                        CssClass="style5" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>

    </dx:ASPxPageControl>
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label275 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Submit" runat="server" visible="false" Text="<%$ Resources:LibResources, Label702 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>">
                </dx:ASPxButton>
            </td>

            <td>
                <dx:ASPxButton ID="btn_print_sls_hst" runat="server" Text="Print Sales History">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="ASPxLabel" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Customer Code is for query purposes only.">
    </dx:ASPxLabel>
</asp:Content>
