<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SalesOrderMaintenance.aspx.vb" Inherits="SalesOrderMaintenance"%>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<%@ Register Src="~/Usercontrols/SummaryPriceChangeApprovalWithComment.ascx" TagPrefix="ucPC" TagName="SummaryPriceChangeApproval" %>
<%@ Register Src="~/Usercontrols/MultiWarranties.ascx" TagPrefix="ucWar" TagName="Warranties" %>
<%@ Register Src="~/Usercontrols/RelatedSKU.ascx" TagPrefix="ucRelatedSku" TagName="RelatedSKUs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     
    <script language="javascript" type="text/javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup2() {
            ASPxPopupControl2.Hide();
        }
        function SelectAndClosePopup3() {
            ASPxPopupControl3.Hide();
        }
        function SelectAndClosePopupEmailTemplate() {
            ASPxPopupControl6.Hide();
        }
        function SelectAndClosePopup6() {
            popupSaleRW.Hide();
        }
        //Alice added on Jan 08, 2019 for request 2260
        function SelectAndClosePopup7() {
            popupSaleEEX.Hide();
        }

        function pageLoad(sender, args) {
            <%--var browserID = '<%= Session("browserID") %>';
            var provideAccess = '<%= Session("provideAccess") %>';
            //Below line is added to identify, check this condition only if you have query string parameters
            //document.location.search.length

            if (browserID.length > 0 && provideAccess == '0' && window.name.length == 0 && document.location.search.length > 0) {
                $("#SOMDetails").find('input,textarea,button,select,a').prop("disabled", true);
                $("#SOMTotal").find('input,textarea,button,select,a').prop("disabled", true);
                ddlConfStaCd.SetEnabled(false);
                cbo_tax_exempt.SetEnabled(false);
                cboOrdSrt.SetEnabled(false);
                ASPxPageControl1.SetEnabled(false);
                //$("#ddlConfStaCd").prop("disabled", true);
                //$("#cbo_tax_exempt").prop("disabled", true);
                //$("#cboOrdSrt").prop("disabled", true);
                //$("#ASPxPageControl1").prop("disabled", true);
                window.alert("Document is being edited in other tab.You are not allowed to edit the document.")
                return;
            }--%>

            $('input[type=submit], a, button').click(function (event) {

                if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {

                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {

                        $('#hidDestUrl').val($.trim($(this).attr('href')));

                        if ('<% =AppConstants.ManagerOverride.BY_ORDER %>' == '<% =SalesUtils.GetPriceMgrOverrideSetting() %>') {
                            __doPostBack('SummaryPriceChangeApproval', "");
                        }

                        event.preventDefault();
                    }
                }
            });
        }

        function disableButton(id) {
            // Daniela B 20140723 prevent double click
            try {
                var a = document.getElementById(id);
                a.style.display = 'none';
            } catch (err) {
                alert('Error in disableButton ' + err.message);
                return false;
            }
            return true;
        }


        function PriceUpdatedRow(rowNo) {
            if (('<% =AppConstants.ManagerOverride.NONE %>' != '<% =SalesUtils.GetPriceMgrOverrideSetting() %>')) {
                        $('#hidUpdLnNos').val($('#hidUpdLnNos').val() + ';' + rowNo);
                    }
        }

          function DelChgUpdated() {
            $("#<%= hidUpdatedDelChg.ClientID %>").val($('#<%= lblDelivery.ClientID %>').val());
        }
      
        function _getKeyCode(evt) {
            return (typeof (evt.keyCode) != "undefined" && evt.keyCode != 0) ?
                evt.keyCode : evt.charCode;
        }

   
 </script>

      <%-- MM-19550 --%>
<script type="text/javascript" language="javascript">
    window.onload = function () {
        var GetDocumentScrollTop = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageYOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollTop || document.body.scrollTop;
                return document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        };
        var _aspxGetDocumentScrollTop = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageYOffset;
                else
                    return document.documentElement.scrollTop || document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        }
        if (window._aspxGetDocumentScrollTop) {
            window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
        } else {
            window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
        }
        /* Begin -> Correct ScrollLeft  */
        var GetDocumentScrollLeft = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageXOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
                return document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        };
        var _aspxGetDocumentScrollLeft = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageXOffset;
                else
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        }
        if (window._aspxGetDocumentScrollLeft) {
            window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
        } else {
            window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
        }
        /* End -> Correct ScrollLeft  */
    };
</script>


    <asp:HiddenField ID="hidDestUrl" runat="server" ClientIDMode="Static" />

    <table id="SOMDetails" width="100%">
        <tr>
            <td align="left">
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server"  ClientInstanceName="ASPxRoundPanel1" ClientIDMode="Static" ShowHeader="False" Width="99%">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <div runat="server" id="frm_finalize">
                                <asp:DropDownList ID="cbo_stat_cd" runat="server" Width="147px" AutoPostBack="True">
                                    <asp:ListItem Selected="True" Text="<%$ Resources:LibResources, Label72 %>" Value="C"></asp:ListItem>
                                    <asp:ListItem Value="F">Finalize</asp:ListItem>
                                    <asp:ListItem Value="V" Text="<%$ Resources:LibResources, Label651 %>">Void</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label200 %>" />:
                                <asp:DropDownList ID="final_store_cd" runat="server" Width="170px" CssClass="style5">
                                </asp:DropDownList>
                                &nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label623 %>" />:
                                <asp:TextBox ID="txt_fv_dt" runat="server" Width="75px" CssClass="style5"></asp:TextBox>
                                &nbsp;<asp:DropDownList ID="cbo_void_cause_cd" runat="server" Width="197px" CssClass="style5">
                                </asp:DropDownList>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px; text-align: left;">
                <table width="100%">                   
                    <tr>
                        <td style="width: 12%;">
                            <dx:ASPxLabel ID="lbl_changes_found" runat="server" Visible="false" Text="">
                            </dx:ASPxLabel>
                            <%--<dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Sales Order:">--%>
                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label511 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_del_doc_num" runat="server" Style="position: relative" Enabled="False"
                                CssClass="style5" Width="110px">
                            </asp:TextBox></td>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="cbo_related_docs" runat="server" AutoPostBack="True" Visible="False"
                                            Width="129px" CssClass="style5">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 100px;">
                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label668 %>">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_wr_dt" runat="server" Style="position: relative" Width="88px"
                                            CssClass="style5">
                                        </asp:TextBox></td>
                                    <td align="right">
                                        <asp:TextBox ID="txt_ord_tp_cd" runat="server" Style="position: relative" Width="26px"
                                            CssClass="style5">
                                        </asp:TextBox>
                                        <label>-</label>
                                        <asp:TextBox ID="txt_store_cd" runat="server" Style="position: relative" Width="19px"
                                            CssClass="style5">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label130 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_Cust_cd" runat="server" Style="position: relative; left: 0px; top: 0px;"
                                CssClass="style5" Width="110px">
                            </asp:TextBox>
                            <asp:Label ID="lbl_so_doc_num" runat="server" Visible="False"></asp:Label></td>
                        <td style="width: 14%; padding-left: 10px;">
                            <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label571 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_Stat_cd" runat="server" Style="position: relative; left: 1px; top: 0px;"
                                Width="16px" CssClass="style5">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_Stat_dt" runat="server" Width="88px" CssClass="style5">
                            </asp:TextBox>
                        </td>
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="<%$ Resources:LibResources, Label377 %>">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_pd_display" runat="server" Width="16px">
                                        </asp:TextBox></td>
                                    <td>
                                        <dx:ASPxLabel ID="lblDatefrom" runat="server" Text="<%$ Resources:LibResources, Label217 %>">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_pd_dt_display" runat="server" Width="88px" CssClass="js-date-picker-From">
                                        </asp:TextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="lblDateTo" runat="server" Text="<%$ Resources:LibResources, Label611 %>">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPDDateTodate" runat="server" Width="88px" CssClass="js-date-picker-To">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="lblSearchZone" runat="server" Text="<%$ Resources:LibResources, Label685 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearchZone" runat="server" Style="position: relative"
                                CssClass="style5" Width="110px" MaxLength="5">
                            </asp:TextBox></td>
                        <td style="padding-left: 10px;">
                            <dx:ASPxLabel ID="ASPxLabel26" runat="server" Text="<%$ Resources:LibResources, Label104 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="ddlConfStaCd" runat="server"  ClientInstanceName="ddlConfStaCd" ClientIDMode="Predictable" IncrementalFilteringMode="StartsWith">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxCheckBox ID="ChkBoxViewSlspersons" runat="server"  ClientInstanceName="ChkBoxViewSlspersons" ClientIDMode="Static" Text="<%$ Resources:LibResources, Label544 %>"
                                AutoPostBack="true" TextAlign="Left" OnCheckedChanged="ChkBoxViewSlspersons_CheckedChanged" />
                        </td>
                        <td colspan="2">
                            <dx:ASPxCheckBox ID="chk_reserve_all" runat="server" ClientIDMode="Static" Text="<%$ Resources:LibResources, Label473 %>"
                                AutoPostBack="true" TextAlign="Left" OnCheckedChanged="chk_reserve_all_CheckedChanged" />
                        </td>
                        <td>
                            <dx:ASPxCheckBox ID="chk_calc_avail" runat="server" ClientIDMode="Static" Text="<%$ Resources:LibResources, Label53 %>"
                                AutoPostBack="true" TextAlign="Left" OnCheckedChanged="chk_calc_avail_CheckedChanged" />
                        </td>
                    </tr>
                            
                </table>
                <table width="100%" >
                    <dx:ASPxLabel ID="lbl_warning" runat="server" Font-bold="true" ForeColor="Red" Width="684px" EncodeHtml="false">
                    </dx:ASPxLabel>                     
                </table>
            </td>
        </tr>        
                       
        <tr>
            <td>
                <asp:Label ID="lbl_PD" runat="server" Visible="False"></asp:Label>
                &nbsp;
                <asp:Label ID="lbl_tax_cd" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="lbl_tax_rate" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="lbl_hdr_slsp1_cd" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="lbl_hdr_slsp2_cd" runat="server" Visible="False"></asp:Label>
                <asp:HiddenField ID="HidslsComm1" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="HidslsComm2" runat="server" ClientIDMode="Static" />
            </td>
        </tr>       
        <tr>
            <td align="left">
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="99%"  ClientIDMode="Static"
                    AutoPostBack="True" ClientInstanceName="ASPxPageControl1">
                    <TabPages>
                        <dx:TabPage Text="<%$ Resources:LibResources, Label364 %>">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <asp:Panel  ID="pnlOrderHeader" runat="server">
                                    <table width="100%" class="style5">
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:LibResources, Label110 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txt_corp_name" runat="server" Width="233px" CssClass="style5" MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td style="text-align: right">
                                                <dx:ASPxLabel ID="lbl_po" runat="server" Text="<%$ Resources:LibResources, Label418 %>"></dx:ASPxLabel>
                                                &nbsp;&nbsp;
                                                <asp:TextBox ID="txt_po" runat="server" Width="99px" MaxLength="15"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="<%$ Resources:LibResources, Label196 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txt_f_name" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                                <asp:TextBox ID="txt_l_name" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                            </td>
                                           <td  style="text-align : right" valign="bottom"  >
                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="<%$ Resources:LibResources, Label225 %>">
                                                </dx:ASPxLabel>
                                                &nbsp;&nbsp;
                                                <span style="float:right;"  >
                                                   <%-- keycode == 37 FOR %
                                                        keycode == 45 FOR -
                                                        keycode == 46 FOR DELETE
                                                        keycode == 8 FOR BACKSPACE
                                                        keycode == 13 FOR Enter key--%>
                                               <dx:ASPxTextBox ID="txt_h_phone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX" CssClass="style5" >
                                                    <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                    <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" /> </dx:ASPxTextBox>
                                                   <dx:ASPxLabel ID="lblHPhoneNumberFormat" runat="server"   Text="xxx-xxx-xxxx" Enabled="false"  style="padding-right:18px"  ></dx:ASPxLabel>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="<%$ Resources:LibResources, Label20 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txt_addr1" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right" valign="bottom">
                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="<%$ Resources:LibResources, Label2 %>">
                                                </dx:ASPxLabel>
                                                &nbsp;&nbsp;
                                                 <span style="float:right;" >
                                                <dx:ASPxTextBox ID="txt_b_phone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX" >
                                                    <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                    <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" /> </dx:ASPxTextBox>
                                                      <dx:ASPxLabel ID="lblBPhoneNumberFormat" runat="server" Text="xxx-xxx-xxxx" Enabled="false" style="padding-right:18px"   ></dx:ASPxLabel>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="<%$ Resources:LibResources, Label21 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txt_addr2" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                                            </td>
                                            <td colspan="1" align="right">
                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="<%$ Resources:LibResources, Label553 %>">
                                                </dx:ASPxLabel>
                                                &nbsp;<asp:TextBox ID="txt_slsp1" runat="server" Width="62px" CssClass="style5" OnTextChanged="txt_slsp1_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <asp:TextBox ID="txt_comm1" runat="server" Width="42px" CssClass="style5" AutoPostBack="True"></asp:TextBox>
                                            </td>
                                            <%--<asp:HyperLink ID="hpl_comments" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/comments.gif" BorderStyle="None" /></asp:HyperLink>--%>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="<%$ Resources:LibResources, Label81 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txt_city" runat="server" Width="136px" CssClass="style5"></asp:TextBox>&nbsp;
                                    <asp:DropDownList ID="cbo_State" Width="150px" runat="server" CssClass="style5">
                                    </asp:DropDownList>&nbsp;
                                    <asp:TextBox ID="txt_zip" runat="server" Width="53px" CssClass="style5" AutoPostBack="True"
                                        OnTextChanged="txt_zip_TextChanged"></asp:TextBox>
                                            </td>
                                            <td colspan="1" align="right">
                                                <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="<%$ Resources:LibResources, Label554 %>">
                                                </dx:ASPxLabel>
                                                &nbsp;<asp:TextBox ID="txt_slsp2" CssClass="style5" runat="server" Width="62px" AutoPostBack="true"
                                                    OnTextChanged="txt_slsp2_TextChanged"></asp:TextBox>
                                                <asp:TextBox ID="txt_comm2" runat="server" Width="42px" CssClass="style5" AutoPostBack="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="<%$ Resources:LibResources, Label178 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_email" runat="server" Width="322px" CssClass="style5"></asp:TextBox>
                                                <asp:TextBox ID="txt_SHU" runat="server" Visible="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="top">
                                                <br />
                                                <asp:Image ID="img_warn" runat="server" Height="20px" ImageUrl="images/warning.jpg"
                                                    Width="26px" Visible="False" />&nbsp;
                                    <dx:ASPxLabel ID="lbl_cust_warn" runat="server" Text="" Visible="false">
                                    </dx:ASPxLabel>
                                                <br />
                                                <br />
                                            </td>
                                            <td colspan="2" align="right" valign="top">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxLabel ID="lblTaxCd" runat="server" Text="<%$ Resources:LibResources, Label587 %>">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtTaxCd" runat="server" Enabled="false" Width="160px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <dx:ASPxLabel ID="AspxLabelExempt" runat="server" Text="<%$ Resources:LibResources, Label588 %>">
                                                </dx:ASPxLabel>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cbo_tax_exempt" runat="server"  ClientInstanceName="cbo_tax_exempt" ClientIDMode="Predictable"
                                                                IncrementalFilteringMode="StartsWith" ValueType="System.String" Width="305px" AutoPostBack="True">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txt_exempt_id" runat="server" Width="103px" MaxLength="20">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <dx:ASPxLabel ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label366 %>">
                                                </dx:ASPxLabel>
                                                <br />
                                                <dx:ASPxComboBox ID="cboOrdSrt" runat="server" ValueType="System.String" Width="250px" ClientInstanceName="cboOrdSrt"  ClientIDMode="Static"
                                                    IncrementalFilteringMode="StartsWith">
                                                </dx:ASPxComboBox>
                                                <br />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="<%$ Resources:LibResources, Label268 %>">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <asp:Panel ID="pnlItemDetails" runat="server">
                                    <dx:ASPxLabel ID="lblARSMsg" runat="server" Text="" Visible="false" ForeColor="Red"></dx:ASPxLabel>
                                    <br />
                                    <br />
                                    <asp:DataGrid ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="2"
                                        DataKeyField="del_doc_ln#" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" Height="156px" Width="100%" ShowFooter="True">
                                        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="del_doc_ln#">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" CssClass="style5" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="itm_cd" Visible="False">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ve_cd" HeaderText="Vendor" ReadOnly="True" Visible="False">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label653 %>">
                                                <ItemTemplate>
                                                    <table width="100%" class="style5">
                                                        <tr>
                                                            <td align="left" valign="top" width="150">
                                                                <%-- <asp:Image ID="img_picture" runat="server" Width="133px" Height="100px" ImageUrl="~/images/image_not_found.jpg" />--%>
                                                                <%-- Daniela commented <asp:Image ID="img_picture" runat="server" Height="100px" ImageUrl="~/images/image_not_found.jpg"
                                                                    Width="133px" />--%>
                                                                <asp:Image ID="img_picture" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.itm_url") %>'
                                                                    />
                                                                <%--ToolTip='<%# If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", ","+Environment.NewLine) = "-","",DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", ","+Environment.NewLine)) %>'--%>
                                                                <br />
                                                                <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label561 %>" />:<br />
                                                                <div runat="server" id="divSortCodes" style="overflow: auto; height: 30px; text-align: left; width: 95%; padding-top: 5px;">
                                                                    <label><%# Replace(StringUtils.EliminateCommas(If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", "," + Environment.NewLine) = "-", "", DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper())),",",", ")%></label>
                                                                </div>
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vsn") %>'></asp:Label>
                                                                &nbsp;&nbsp;
                                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'></asp:Label>
                                                                <br />
                                                                <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label550 %>" />:
                                                    <asp:Label ID="lbl_SKU" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itm_cd") %>'></asp:Label>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label640 %>" />:
                                                    <asp:Label ID="lbl_ve_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ve_cd") %>'></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lbl_sernum" runat="server" Text='Serial Number:'></asp:Label>
                                                                <asp:Label ID="lbl_sernumval" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.serial_num") %>'></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lbl_outnum" runat="server" Text='Outlet ID:'></asp:Label>
                                                                 <asp:Label ID="lbl_max" runat="server" Text ="" Visible ="false"  ></asp:Label>
                                                               
                                                                <asp:Label ID="lbl_outnumval" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.out_id") %>'></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lbl_PO" runat="server" Text=''></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lbl_IST" runat="server" Text=''></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lbl_warr" runat="server" Text='Warr SKU:'></asp:Label>
                                                                <asp:Label ID="lbl_warr_sku" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.warr_by_sku")%>'></asp:Label>
                                                                <br />
                                                                <asp:ImageButton ID="img_gm" runat="server" ImageUrl="images/icons/money.gif" Height="20"
                                                                    Width="20" CommandName="find_gm" Visible="True" />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="lblMaxCred" runat="server" Text=''></asp:Label>
                                                                <br />
                                                               <%--Daniela Nov 11 hide Leave in Carton--%>
                                                                <asp:Label ID="lbl_carton" runat="server" Text='Leave In Carton:' Visible="false"></asp:Label>
                                                                <asp:CheckBox ID="chk_carton" AutoPostBack="True" OnCheckedChanged="Carton_Update"
                                                                    runat="server" Visible="false"/>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="lblPickStat" runat="server" Text=''></asp:Label>
                                                                <div id="slsp_section" runat="server" visible="False">
                                                                    <asp:Label ID="lblSlps1" runat="server" Text='Slsp1:'></asp:Label>
                                                                    <asp:DropDownList ID="cbo_slsp1" OnSelectedIndexChanged="LineSalespersonChanged"
                                                                        AutoPostBack="true" runat="server" CssClass="style5" Width="140px" Enabled="false">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtLineSlsp1Pct" runat="server" AutoPostBack="true" CssClass="style5"
                                                                        OnTextChanged="LineSlsp1PctChanged" Text='<%# DataBinder.Eval(Container, "DataItem.ln_slsp1") %>'
                                                                        Width="25" Enabled="false"></asp:TextBox>%
                                                        <br />
                                                                    <asp:Label ID="lblSlps2" runat="server" Text='Slsp2:'></asp:Label>
                                                                    <asp:DropDownList ID="cbo_slsp2" OnSelectedIndexChanged="LineSalespersonChanged"
                                                                        AutoPostBack="true" runat="server" CssClass="style5" Width="140px" Enabled="false">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtLineSlsp2Pct" runat="server" AutoPostBack="true" CssClass="style5"
                                                                        OnTextChanged="LineSlsp2PctChanged" Text='<%# DataBinder.Eval(Container, "DataItem.ln_slsp2") %>'
                                                                        Width="25" Enabled="false"></asp:TextBox>%
                                                                </div>
                                                                <br />
                                                                <asp:Label ID="lblAvailResults" runat="server" Text='' CssClass="style5"></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblReplace" Visible="false" runat="server" Text='Replace Component: ' CssClass="style5"></asp:Label>
                                                                <asp:ImageButton ID="btnReplace" Visible="false" runat="server" ImageUrl="~/images/icons/replace.png"
                                                                    Height="15px" Width="15px" ToolTip="Replace" CommandName="Replace" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <%--Daniela msg red and bigger as per Donna <asp:Label ID="lbl_itm_warning" runat="server" Font-Size="Smaller" ForeColor="#C0C000"></asp:Label>--%>
                                                                <asp:Label ID="lbl_itm_warning" runat="server" ForeColor="#FF0066"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                        <asp:TextBox ID="txt_new_itm_cd" runat="server" Width="170px" AutoPostBack="False"
                                                            CssClass="style5"></asp:TextBox>
                                                        <asp:HyperLink ID="hpl_find_merch" runat="server" OnLoad="FindMerchandise">
                                                            <img src="images/icons/find.gif" style="width: 15px; height: 15px" border="0" alt="Merchandise Lookup" />
                                                        </asp:HyperLink>                                                
												</FooterTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Image3" runat="server" ImageUrl="images/icons/DocumentsBlack.PNG"
                                                        Height="20" Width="20" CommandName="inventory_lookup" AlternateText="SKU Details" />
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Image1" runat="server" ImageUrl="images/icons/compare_inventory.gif"
                                                                    Height="20" Width="20" CommandName="find_inventory" ToolTip="Inventory Lookup" AlternateText="Inventory Lookup" />
                                                            </td>
                                                        </tr>
                                                        <tr></tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbl_invcount" runat="server" Visible="false" Text=""></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label572 %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="store_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.store_cd") %>'
                                                        Width="30" AutoPostBack="true" OnTextChanged="Store_Update" CssClass="style5"></asp:TextBox>
                                                        <br />
                                                          <asp:Label ID="lbl_invxrefStore" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.InvXrefStore") %>'></asp:Label>
												</ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_new_store_cd" runat="server" Text='' Width="30" AutoPostBack="False"
                                                        CssClass="style5"></asp:TextBox>
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label271 %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="loc_cd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.loc_cd") %>'
                                                        Width="30" AutoPostBack="True" OnTextChanged="Loc_Update" CssClass="style5"></asp:TextBox>
                                                        <br />
                                                          <asp:Label ID="lbl_invxrefLocation" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.InvXrefLocation") %>'></asp:Label>                                                
												</ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_new_loc_cd" runat="server" Text='' Width="30" AutoPostBack="False"
                                                        CssClass="style5"></asp:TextBox>
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <b><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label197 %>" /></b>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="SelectFab" runat="server" Enabled="false" AutoPostBack="false"
                                                        CssClass="style5" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:CheckBox ID="chk_fab" runat="server" Enabled="false" CssClass="style5" />
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <b>Warr</b>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkBoxWarr" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="CheckWarrClicked"
                                                        CssClass="style5" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:CheckBox ID="chk_warr" runat="server" Enabled="false" CssClass="style5" />
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label427 %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="unit_prc" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.unit_prc"),2) %>'
                                                        Width="55px" AutoPostBack="True" OnTextChanged="Price_Update" CssClass="style5"
                                                        onchange='<%#"PriceUpdatedRow("+DataBinder.Eval(Container, "DataItem.del_doc_ln#").ToString()+");" %>'></asp:TextBox>
                                                    <asp:HiddenField ID="hidMaxDisc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.MAX_DISC_PCNT")%>'></asp:HiddenField>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_new_unit_prc" runat="server" Text='' Width="55px" AutoPostBack="False"></asp:TextBox>
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label447 %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="qty" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.qty"),0) %>'
                                                        Width="23" AutoPostBack="True" OnTextChanged="Qty_Update" CssClass="style5"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_new_qty" runat="server" Text='' Width="23" AutoPostBack="False"
                                                        CssClass="style5"></asp:TextBox>
                                                </FooterTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" Wrap="False" />
                                                <HeaderTemplate>
                                                    <b><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label651 %>" /></b>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkBoxVoid" AutoPostBack="true" OnCheckedChanged="ChkBoxVoid_CheckedChanged" runat="server"
                                                        CssClass="style5" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button runat="server" ID="btn_add_item" Text="<%$ Resources:LibResources, Label10 %>" Width="40px"
                                                        OnClick="Add_SKU_to_Order"></asp:Button>
                                                </FooterTemplate>
                                                <FooterStyle HorizontalAlign="center" />
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" Wrap="False" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ITM_TP_CD" HeaderText="itm_tp_cd" Visible="False">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TREATABLE" HeaderText="treatable" Visible="False">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TREATED_BY_ITM_CD" HeaderText="TREATED" Visible="False">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Top" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="VOID_FLAG" HeaderText="void_flag" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NEW_LINE" HeaderText="new_line" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ADDON_WR_DT" HeaderText="add_on" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="SERIAL_TP" HeaderText="serial_tp" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OUT_ID" HeaderText="outlet_id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OUT_CD" HeaderText="out_cd" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="INVENTORY" HeaderText="inventory" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LV_IN_CARTON" HeaderText="lv_in_carton" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CUST_TAX_CHG" HeaderText="cust_tax_chg" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LN_SLSP1" HeaderText="LN_SLSP1" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LN_SLSP2" HeaderText="LN_SLSP2" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LN_SLSP1_PCT" HeaderText="LN_SLSP1_PCT" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LN_SLSP2_PCT" HeaderText="LN_SLSP2_PCT" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="warr_by_sku" HeaderText="WARR_BY_SKU" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="warrantable" HeaderText="WARRANTABLE" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="warr_by_ln" HeaderText="WARR_BY_LN" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="warr_by_doc_num" HeaderText="WARR_BY_DOC_NUM" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="invPickVoid" HeaderText="invPickVoid" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="max_crm_qty" HeaderText="max_crm_qty" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="max_ret_prc" HeaderText="max_ret_prc" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ORIG_SO_LN_NUM" HeaderText="ORIG_SO_LN_NUM" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RES_ID" HeaderText="RES_ID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="IsStoreFromInvXref" HeaderText="IsStoreFromInvXref" Visible="False"></asp:BoundColumn>
                                        </Columns>
                                        <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" />
                                        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                            Font-Bold="True"></HeaderStyle>
                                    </asp:DataGrid>
                                        </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="<%$ Resources:LibResources, Label164 %>">
                            <ContentCollection>
                                <dx:ContentControl ID="ccDiscounts" runat="server">
                                    <asp:Panel ID="pnlDiscounts" runat="server">
                                    <ucDisc:Discount runat="server" ID="ucDiscount" />
                                </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                            <dx:TabPage Text="<%$ Resources:LibResources, Label95 %>">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <asp:Panel ID="pnlComments" runat="server">
                                    <table width="100%" height="100%" class="style5">
                                        <tr>
                                            <td valign="top" align="left">
                                                <br />
                                      <%-- DB Oct 22, 2014 commented New Sales Order Comments: ; add Lucy changes--%>
                                        <table>
                                        <tr>
                                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label333 %>" />    </td>
                                           <td><asp:Button ID="lucy_btn_cmnt" runat="server" Text="<%$ Resources:LibResources, Label18 %>"  ForeColor ="#9933FF" /></td>
                                        </tr>
                                        </table>
                                    <br />
                                                  <%-- DB Oct 22, 2014 added to all 3: MaxLength="4000" onkeydown="ValidateTextboxLength(this, 4000, ('You may enter a maximum of 4000 characters'));" onkeyup="ValidateTextboxLength(this, 4000, ('You may enter a maximum of 4000 characters'));" --%>
                                                <asp:TextBox ID="txt_comments" AutoPostBack="True" runat="server" Rows="2" TextMode="MultiLine"
                                                    Height="70px" Width="99%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label332 %>" />:
                                    <br />
                                                <asp:TextBox ID="txt_del_comments" AutoPostBack="True" runat="server" Height="70px"
                                                    Rows="2" TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label329 %>" />:
                                    <br />
                                                <asp:TextBox ID="txt_ar_comments" AutoPostBack="True" runat="server" Rows="2" TextMode="MultiLine"
                                                    Height="70px" Width="99%" CssClass="style5"></asp:TextBox>
                                                <br />
                                                <br />
                                                <dx:ASPxGridView ID="ASPxGridView1" EnableCallBacks="False" runat="server" Width="99%">
                                                    <SettingsPager Visible="False" Mode="ShowAllRecords">
                                                    </SettingsPager>
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Date" FieldName="DT" VisibleIndex="0" CellStyle-HorizontalAlign="Left">
                                                            <PropertiesTextEdit>
                                                                <MaskSettings Mask="MM/dd/yyyy" />
                                                            </PropertiesTextEdit>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Comments" FieldName="TEXT" VisibleIndex="1" CellStyle-HorizontalAlign="Left">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Type" FieldName="CMNT_TYPE" VisibleIndex="2"
                                                            CellStyle-HorizontalAlign="Left">
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                                <br />
                                                <br />
                                                <dx:ASPxGridView ID="ASPxGridView2" runat="server" Width="99%">
                                                    <SettingsPager Visible="False" Mode="ShowAllRecords">
                                                    </SettingsPager>
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Date" FieldName="DT" VisibleIndex="0" CellStyle-HorizontalAlign="Left">
                                                            <PropertiesTextEdit>
                                                                <MaskSettings Mask="MM/dd/yyyy" />
                                                            </PropertiesTextEdit>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Account Comments" FieldName="TEXT" VisibleIndex="1"
                                                            CellStyle-HorizontalAlign="Left">
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                   </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>   
                        <dx:TabPage Text="<%$ Resources:LibResources, Label294 %>">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <asp:Panel ID="pnlMiscellaneous" runat="server">
                                    <asp:DataGrid ID="MyDataGrid" runat="server" Width="461px" BorderColor="Black" CellPadding="3"
                                        AutoGenerateColumns="False" DataKeyField="FLD_NAME" Height="16px" PageSize="7"
                                        AlternatingItemStyle-BackColor="Beige" ShowHeader="False">
                                        <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                        <Columns>
                                            <asp:BoundColumn HeaderText="FLD_NAME" DataField="FLD_NAME" Visible="False" />
                                            <asp:BoundColumn HeaderText="FLD_LENGTH" DataField="FLD_LENGTH" Visible="False" />
                                            <asp:BoundColumn DataField="FLD_TITLE" />
                                            <asp:TemplateColumn>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txt_fld_value" runat="server" Width="300" AutoPostBack="true" OnTextChanged="UDF_Update"
                                                        Visible="False"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <EditItemStyle CssClass="style5" />
                                        <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                        <ItemStyle CssClass="style5" />
                                    </asp:DataGrid>
                                    </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="<%$ Resources:LibResources, Label395 %>">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server">
                                    <asp:Panel ID="pnlPickDel" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="height: 119px">
                                                <br />
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cbo_PD" Enabled="False" AutoPostBack="True" runat="server" Width="100px"  ClientIDMode="Static"
                                                                IncrementalFilteringMode="StartsWith" ValueType="System.String">
                                                                <Items>
                                                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label151 %>" Value="D" />
                                                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label392 %>" Value="P" />
                                                                </Items>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cbo_pd_store_cd" Enabled="False" Width="247px" AutoPostBack="True"  ClientIDMode="Static"
                                                                runat="server" OnTextChanged="PD_Store_Update" IncrementalFilteringMode="StartsWith">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="<%$ Resources:LibResources, Label73 %>"  ClientIDMode="Static">
                                                            </dx:ASPxButton>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <table id="trk_details" runat="server">
                                                    <tr>
                                                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label154 %>" />:
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_del_dt" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label629 %>" />:
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_trk_stop" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arrival (Est/Actual):
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_arrival_time" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="height: 119px">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="5" bgcolor="lightgrey" align="center">
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label129 %>" /> </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label633 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label572 %>" /> <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label377 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label684 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label143 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label74 %>" /></strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbl_curr_tp" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_curr_pd_store" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_curr_zone" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_curr_dt" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Label ID="lbl_curr_charges" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" bgcolor="lightgrey" align="center">
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label328 %>" /> </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label633 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label572 %>" /> <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label377 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label684 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label143 %>" /></strong>
                                                        </td>
                                                        <td>
                                                            <strong><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label74 %>" /></strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbl_new_tp" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_new_pd_store" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_new_zone" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl_new_dt" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Label ID="lbl_new_charges" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="lblTMError" runat="server" Text="" ForeColor="Red"></dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <dx:ASPxCalendar ID="Calendar1" runat="server" AutoPostBack="True" Visible="false">
                                    </dx:ASPxCalendar>
                                    <br />
                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Height="192px"
                                        Width="473px" Font-Size="XX-Small" AllowPaging="True" PageSize="7" BorderStyle="None"
                                        BorderColor="White" BorderWidth="0px" PagerSettings-Visible="false" CaptionAlign="Left">
                                        <AlternatingRowStyle BackColor="Beige" BorderColor="Green"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:BoundField DataField="ZONE_CD" SortExpression="Zone Code" HeaderText="Zone">
                                                <ControlStyle Font-Size="X-Small"></ControlStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DES" SortExpression="Description" HeaderText="Description">
                                                <ControlStyle Font-Size="X-Small"></ControlStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DELIVERY_STORE_CD" SortExpression="Store" HeaderText="Store">
                                                <ControlStyle Font-Size="X-Small"></ControlStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEFAULT_DEL_CHG" HeaderText="Delivery">
                                                <ControlStyle Font-Size="X-Small"></ControlStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEFAULT_SETUP_CHG" HeaderText="Setup">
                                                <ControlStyle Font-Size="X-Small"></ControlStyle>
                                            </asp:BoundField>
                                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" ItemStyle-CssClass="style5">
                                                <ControlStyle CssClass="style5" />
                                            </asp:CommandField>
                                        </Columns>
                                        <PagerSettings Visible="False"></PagerSettings>
                                    </asp:GridView>
                                    <table id="nav_table" runat="server">
                                        <tr>
                                            <td style="width: 473px; height: 35px" align="center">
                                                <asp:Button ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                                    ToolTip="Go to first page" CommandArgument="First" Visible="false" CssClass="style5"></asp:Button>
                                                &nbsp;&nbsp;<asp:Button ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false" CssClass="style5"></asp:Button>&nbsp;&nbsp;<asp:Button ID="btnNext" OnClick="PageButtonClick" runat="server"
                                                        Text="Next >" ToolTip="Go to the next page" CommandArgument="Next" Visible="False"
                                                        CssClass="style5"></asp:Button>&nbsp;&nbsp;<asp:Button ID="btnLast" OnClick="PageButtonClick"
                                                            runat="server" Text="Last >>" ToolTip="Go to the last page" CommandArgument="Last"
                                                            Visible="false" CssClass="style5"></asp:Button>
                                                <br />
                                                <asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                                <td align="right">
                                                    <dx:ASPxLabel ID ="lblFilterDate" runat="server" Text ="Search By Date" />&nbsp;
                                                    <dx:ASPxDateEdit ID="cboTargetDate" runat="server" EnableViewState="true" AutoPostBack="true" OnDateChanged="cboTargetDatechanged" ToolTip="Select a date to filter the delivery">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                            </tr>
                                            <tr>                                            
												<td>
                                                <asp:DataGrid ID="grid_del_date" runat="server" AutoGenerateColumns="False" Height="192px"
                                                    Width="470px" Font-Size="XX-Small" AllowPaging="True" PageSize="7" BorderStyle="None"
                                                    BorderColor="White" BorderWidth="0px" CaptionAlign="Left" CssClass="style5">
                                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:BoundColumn DataField="DEL_DT" SortExpression="Delivery Date" HeaderText="Date"
                                                            DataFormatString="{0:MM/dd/yyyy}">
                                                            <ItemStyle Font-Size="X-Small"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DAY" SortExpression="Day" HeaderText="<%$ Resources:LibResources, Label144 %>">
                                                            <ItemStyle Font-Size="X-Small"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DEL_STOPS" SortExpression="Max" HeaderText="Max">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ACTUAL_STOPS" SortExpression="Sched" HeaderText="Sched">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="REMAINING_STOPS" SortExpression="Avail" HeaderText="<%$ Resources:LibResources, Label40 %>">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:ButtonColumn ButtonType="PushButton" Text="<%$ Resources:LibResources, Label528 %>" ItemStyle-CssClass="style5"></asp:ButtonColumn>
                                                        <asp:BoundColumn DataField="EXMPT_DEL_CD" Visible="false" HeaderText="ExceptionCode"></asp:BoundColumn>
                                                    </Columns>
                                                    <EditItemStyle CssClass="style5" />
                                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                                    <ItemStyle CssClass="style5" />
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="ASPlblError" runat="server" Text="" ForeColor="Red">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>

                                    <table id="navdt_table" runat="server" style="width: 480px">
                                        <tr>
                                            <td style="width: 345px; height: 35px" align="center">
                                                <asp:Button ID="btnfirst2" OnClick="PageButtonClick2" runat="server" Text="<%$ Resources:LibResources, Label210 %>"
                                                    ToolTip="Go to first page" CommandArgument="First" Visible="false" CssClass="style5"></asp:Button>
                                                &nbsp;&nbsp;<asp:Button ID="btnprev2" OnClick="PageButtonClick2" runat="server" Text="<%$ Resources:LibResources, Label425 %>"
                                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false" CssClass="style5"></asp:Button>&nbsp;&nbsp;<asp:Button ID="btnnext2" OnClick="PageButtonClick2" runat="server"
                                                        Text="<%$ Resources:LibResources, Label337 %>" ToolTip="Go to the next page" CommandArgument="Next" Visible="False"
                                                        CssClass="style5"></asp:Button>&nbsp;&nbsp;<asp:Button ID="btnlast2" OnClick="PageButtonClick2"
                                                            runat="server" Text="<%$ Resources:LibResources, Label261 %>" ToolTip="Go to the last page" CommandArgument="Last"
                                                            Visible="false" CssClass="style5"></asp:Button>
                                                <br />
                                                <asp:Label ID="lbldesc2" runat="server" Text="" Visible="false"></asp:Label><asp:Label
                                                    ID="lblDesc" runat="server" Height="8px" Width="300px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label277 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_Save" ClientInstanceName="btn_Save" runat="server" Text="Save Changes" ClientIDMode="Static"> 
                                <ClientSideEvents Click="function(s,e){  s.SendPostBack('Click'); s.SetEnabled(false);}" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_payments" runat="server" Text="<%$ Resources:LibResources, Label16 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                        <td>
                             <%--'Daniela Nov 10 hide view invoice--%>
                           <dx:ASPxButton ID="btn_invoice" runat="server" Text="<%$ Resources:LibResources, Label646 %>" Visible="false">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnPrintPickReport" runat="server" OnClick="btnPrintPickReport_Click" Text="Print Picking Report"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSORW" runat="server" Text="<%$ Resources:LibResources, Label565 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                     
                           <%--Alice added on Jan 08, 2019 for request 2260--%>
                        <td>
                            <dx:ASPxButton ID="btnEEX" runat="server" Text="<%$ Resources:LibResources, Label185 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>

                         <%--Lucy added on Nov, 2019 for request 214--%>
                        <td>
                            <dx:ASPxButton ID="btn_add_spc" runat="server" Text="Add_SPC#"  ClientIDMode="Static"    >
                            </dx:ASPxButton>
                        </td>

                        <td>
                            <dx:ASPxButton ID="btn_addendum" runat="server" Text="<%$ Resources:LibResources, Label644 %>"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_return" runat="server" Text="Return" ToolTip="Go to the previous page"  ClientIDMode="Static">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                         <%--'Daniela Dec 5 reprint invoice--%>
                         <td>   
                               <dx:ASPxComboBox ID="printer_drp" runat="server" Width="150px" DropDownRows="5">
                                </dx:ASPxComboBox>
                        </td>      
                        <td>
                          <%--'Daniela Dec 5 reprint invoice--%>
                            <dx:ASPxButton ID="btn_reprint" runat="server" Text="<%$ Resources:LibResources, Label471 %>">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnConfigureEmail" runat="server" Text="<%$ Resources:LibResources, Label102 %>" Visible="False" >
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label181 %>" />
                            <dx:ASPxCheckBox ID="chk_email" runat="server" OnCheckedChanged="chk_email_CheckedChanged"
                                AutoPostBack="true" Visible="True" Enabled="false" Checked="false">
                            </dx:ASPxCheckBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                 <%--Daniela hide email message
                <dx:ASPxLabel ID="lbl_email_msg" runat="server" Text='To email a quote, you must click "View Invoice" after you configure your message.'>
                </dx:ASPxLabel>--%>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text=''>
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_desc" runat="server" ForeColor="Red" Width="684px" EncodeHtml="false">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_promo_cd" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_final" runat="server" ForeColor="Red" Width="684px" EncodeHtml="false">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="ASPxPopupControl1" CloseAction="CloseButton"
        HeaderText="Sales Order Comments" Height="600px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="430px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
              
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    

    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="ASPxPopupControl2" CloseAction="CloseButton"
        HeaderText="Payment Information" Height="200px" PopupAction="MouseOver" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="572px" PopupElementID="hpl_payments"
        AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" CloseAction="CloseButton"
        HeaderText="SKU Details" Height="680px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="680px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" ClientInstanceName="ASPxPopupControl3" CloseAction="CloseButton"
        HeaderText="Inventory Lookup" Height="500px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="678px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupModifyFinance" runat="server" CloseAction="None" HeaderText="<%$ Resources:LibResources, Label205%>"
        Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="444px" ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table class="style5">
                    <%--Daniela B added: OnClientClick="return disableButton(this.id);"/>     --%>
                
                     <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label806%>" CausesValidation="true" ValidationGroup="OnSaveFinGrp" ForeColor="#CC00FF" OnClientClick="return disableButton(this.id);"/>
                          &nbsp;&nbsp;
                          <asp:Button ID="btn_no_save_fi_co" runat="server" CssClass="style5"  ForeColor="#333333" Text="<%$ Resources:LibResources, Label807%>" />
                          &nbsp;&nbsp;
                          <asp:Button ID="btn_lucy_save" runat="server" ForeColor="#009933" Text="<%$ Resources:LibResources, Label808%>" />
                          <tr>
                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:</td>
                        <td>
                        <asp:DropDownList ID="ddFinanceType" runat="server" CssClass="style5">
                                      <asp:ListItem Value="O">Open</asp:ListItem>
                                      <asp:ListItem Value="R">Revolving</asp:ListItem>
                                      <asp:ListItem Value="I">Installment</asp:ListItem>
                                  </asp:DropDownList>
                              </td>
                          </tr>
                          <tr>
                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label809 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_promo" runat="server" CssClass="style5" Width="318px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                              <td>
                                 <%-- Daniela mask card number <asp:TextBox ID="txt_fi_acct" runat="server" CssClass="style5" MaxLength="20"></asp:TextBox>--%>
                                  <asp:TextBox ID="txt_fi_acct" runat="server" CssClass="style5" MaxLength="16"></asp:TextBox>
                                <%--  <asp:RegularExpressionValidator id="RegularExpressionValidator1"
                                        ControlToValidate="txt_fi_acct"
                                        ValidationExpression="^[0-9*#]+$"
                                        Display="Static"
                                        EnableClientScript="true"
                                        ErrorMessage="Please enter numbers only"
                                        runat="server"/>--%>
                              Exp&nbspDt:
                              <asp:TextBox ID="txt_exp_dt" runat="server" CssClass="style5" MaxLength="4" Width="40px"></asp:TextBox>MMYY
                                  </td>  
                          </tr>
                          <tr>
                              <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label811 %>" />: </td>
                              <td>
                                  <asp:TextBox ID="txt_fi_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                          <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label812 %>" />: </td>
                          <td>
                            <asp:TextBox ID="txt_fi_amt" runat="server" CssClass="style5" MaxLength="10" Text="0.00"
                                AutoPostBack="True" Width="85px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="FinAmtRequiredValidator" runat="server" ErrorMessage="You must enter a value, even if it is zero."
                                ControlToValidate="txt_fi_amt" Display="Dynamic" ValidationGroup="OnSaveFinGrp">
                            </asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="FinAmtRangeValidator" runat="server" Display="Dynamic" ControlToValidate="txt_fi_amt" MinimumValue="0" MaximumValue="999999" Type="Double"
                                ValidationGroup="OnSaveFinGrp" ErrorMessage="Value must be between 0 and order total."></asp:RangeValidator>
                        </td>
                    </tr>
                </table>
                 &nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_lucy_rmv_fi" runat="server" ForeColor="#FF9933" Text="<%$ Resources:LibResources, Label813 %>" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <br/><asp:RegularExpressionValidator id="RegularExpressionValidator1"
                                        ControlToValidate="txt_fi_acct"
                                        ValidationExpression="^[0-9*#]{16,16}$"
                                        Display="Static"
                                        EnableClientScript="true"
                                        ErrorMessage="Please enter 16 digits Account #"
                                        runat="server" ForeColor="Red"/>
                              <br/><asp:RegularExpressionValidator id="RegularExpressionValidator2"
                                        ControlToValidate="txt_exp_dt"
                                        ValidationExpression="((0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01]))"
                                        Display="Static"
                                        EnableClientScript="true"
                                        ErrorMessage="Invalid Expiry Date"
                                        runat="server" ForeColor="Red"/>
                <br />
                 
                <br />
                <br />
                <asp:Label ID="lblErrorMsg" runat="server" Width="415px" ForeColor="Red" Text=" Invalid info.  Please fix the issue to continue."></asp:Label>
                <br />
                <asp:Label ID="lbl_pmt_tp" runat="server" Visible="False" Width="1px"></asp:Label>
                <br />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl6" runat="server" ClientInstanceName="ASPxPopupControl6" AllowDragging="True"
        AllowResize="True" ContentUrl="~/Email_Templates.aspx" HeaderText="Update Email Template"
        Height="384px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="546px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl7" runat="server" CloseAction="CloseButton"
        HeaderText="Gross Margin" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <asp:Label ID="lbl_gm" runat="server"></asp:Label>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl10" runat="server" CloseAction="None"
        HeaderText="Select Serial Number" Height="179px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="370px"
        ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <dx:ASPxLabel ID="lbl_ser_store_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_loc_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_row_id" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exit Without Selecting Serial #">
                </dx:ASPxButton>
                <dx:ASPxLabel ID="lbl_ser_itm_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxGridView ID="ASPxGridView4" runat="server" AutoGenerateColumns="False" Width="346px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="SER_NUM" VisibleIndex="0" Visible="False" Caption="Serial Number">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="0" Caption="">
                            <DataItemTemplate>
                                &nbsp;
                                <dx:ASPxHyperLink ID="hpl_serial" runat="server" Text="Select" Visible="True">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager Visible="False" Mode="ShowAllRecords" RenderMode="Lightweight">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
                <dx:ASPxLabel ID="lbl_ser_err" runat="server" Text="" Visible="True" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl12" runat="server" CloseAction="None"
        HeaderText="Select Outlet ID" Height="179px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="370px"
        ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl12" runat="server">
                <dx:ASPxLabel ID="lbl_out_store_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_loc_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_row_id" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_itm_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxGridView ID="ASPxGridView5" runat="server" AutoGenerateColumns="False" Width="346px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="ID_CD" VisibleIndex="0" Visible="False" Caption="Outlet ID">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Outlet ID" VisibleIndex="0">
                            <DataItemTemplate>
                                &nbsp;<dx:ASPxHyperLink ID="hpl_out" runat="server" Text=" Select" Visible="True">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OUT_CD" VisibleIndex="0" Visible="True" Caption="Outlet Code">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SPIFF" VisibleIndex="0" Visible="False" Caption="Outlet Code">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager Visible="False" Mode="ShowAllRecords" RenderMode="Lightweight">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
                <dx:ASPxLabel ID="lbl_out_err" runat="server" Text="" Visible="True" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl14" runat="server" CloseAction="None"
        HeaderText="Select Outlet Reason Code" Height="179px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="370px"
        ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl14" runat="server">
                <dx:ASPxLabel ID="lbl_out_cd" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_out_cd_row_id" runat="server" Text="" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxGridView ID="ASPxGridView14" runat="server" AutoGenerateColumns="False" Width="346px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="OUT_CD" VisibleIndex="0" Visible="False" Caption="Outlet Code">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Outlet Code" VisibleIndex="0">
                            <DataItemTemplate>
                                &nbsp;<dx:ASPxHyperLink ID="hpl_outCd" runat="server" Text=" Select" Visible="True">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OUT_DES" VisibleIndex="0" Visible="True" Caption="Outlet Code Description">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager Visible="False" Mode="ShowAllRecords" RenderMode="Lightweight">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
                <dx:ASPxLabel ID="lbl_out_cd_err" runat="server" Text="" Visible="True" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="popupSaleRW" runat="server" ClientInstanceName="popupSaleRW" CloseAction="CloseButton"
        HeaderText="Navigate to Sales Order Rewrite" Height="240px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="430px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
  
      <%-- Alice added on Jan 08, 2019 for request 2260--%>
     <dxpc:ASPxPopupControl ID="popupSaleEEX" runat="server" ClientInstanceName="popupSaleEEX" CloseAction="CloseButton"
        HeaderText="Navigate to Sales Exchange" Height="240px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="430px" AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupControlTax" runat="server" ClientInstanceName="PopupTax" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
   <%--  MM-7366--%>

  <dxpc:ASPxPopupControl ID="PopupControlSalespersonConform" runat="server" CloseAction="CloseButton"
        HeaderText="Sales Persons Split" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="30px" Width="460px"
        ClientInstanceName="ASPxPopupControl1">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="pcccConformation" runat="server" Height="30px" Width="550px">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2" align="center" style="padding-bottom: 20px;">
                            <dx:ASPxLabel ID="lblSalessplit" runat="server"></dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 50%;">
                            <dx:ASPxButton ID="ASPxButton_Yes" runat="server" Text="Yes" OnClick="ASPxButton_Yes_Click">
                            </dx:ASPxButton>
                        </td>
                        <td style="width: 50%;">
                            <dx:ASPxButton ID="ASPxButton_No" runat="server" Text="No" OnClick="ASPxButton_No_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <%--Lucy added on Nov, 2019  Req-214--%>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl_spc_num" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label1202 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="30px" Width="460px"
        ClientInstanceName="ASPxPopupControl1">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server" Height="30px" Width="550px">
               <table>


                 <tr>    
                   <td align="left">
                      <asp:TextBox ID="txt_spc_num" Width="280px" runat="server"   ToolTip="SPC Card number must be 16-digits in length"   > </asp:TextBox>
                   </td>
                 </tr> 
              </table>
          
             <table>
               <tr>                 
                 <td align="left">
                    <dx:ASPxButton ID="btn_spc_save" runat="server" Text="<%$ Resources:LibResources, Label520 %>"  Width="80px" Visible="true" OnClick="btn_spc_save_Click"   ></dx:ASPxButton>
                 </td>
                 <td align="left">
                    <dx:ASPxButton ID="btn_spc_clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>"  Width="80px" Visible="true" OnClick="btn_spc_clear_Click"   > </dx:ASPxButton>
                 </td>
                 <td align="left">
                  <dx:ASPxButton ID="btn_spc_exit" runat="server" Text="<%$ Resources:LibResources, Label1201 %>"  Width="80px" Visible="true" OnClick="btn_spc_exit_Click"  ></dx:ASPxButton>
                 </td>
                </tr>
                <tr>  
                  <td align="left" colspan="3" >
                     <asp:Label ID="lbl_msg" runat="server" BackColor="#FF6600" Width="180px" ForeColor="#333333"  ></asp:Label>
                  </td>
                </tr>
              </table>
           </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
        HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="pcccMgrOverride" runat="server">
                <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <asp:HiddenField ID="hidUpdLnNos" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidEnteredSKUs" runat="server" ClientIDMode="Static" />
     <asp:HiddenField ID="hidUpdatedDelChg" runat="server" ClientIDMode="Static" />
    <ucPC:SummaryPriceChangeApproval runat="server" ID="ucSummaryPrcChange" ClientIDMode="Static" />
    <ucWar:Warranties runat="server" ID="ucWarranties" ClientIDMode="Static" />
    <ucRelatedSku:RelatedSKUs runat="server" ID="ucRelatedSku" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="SOMTotal" align="right" style="float: right; padding-right: 12px;">
        <table border="0">
            <tr>
                <td align="right">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label578 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblSubtotal" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label151 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <asp:TextBox ID="lblDelivery" runat="server" Text="$0.00" AutoPostBack="True" Width="40px"
                        Style="text-align: right;" Height="12px" CssClass="style5">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label541 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <asp:TextBox ID="lblSetup" runat="server" Text="$0.00" AutoPostBack="True" Width="40px"
                        Style="text-align: right;" Height="12px" CssClass="style5">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; white-space: nowrap; vertical-align: middle" valign="top">
                    <table cellpadding="0" cellspacing="0" style="float: right;">
                        <tr>
                            <td>
                                <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label585 %>">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <table id="tblTaxCd" runat="server" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="(">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkModifyTax" runat="server" ToolTip="Click to modify the tax code." CssClass="dxeBase_Office2010Blue">
                                                <%= If(IsNothing(Session("TAX_CD")), "None", If(String.IsNullOrEmpty(Session("TAX_CD").ToString), "None", Session("TAX_CD").ToString))%>
                                            </asp:LinkButton></td>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text=")">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <dx:ASPxLabel runat="server" Text=":">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>

                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblTax" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    <hr />
                </td>
            </tr>
            <tr>
                <td align="right" style="white-space: nowrap">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label224 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblTotal" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right" nowrap="nowrap" valign="middle">
                    <asp:ImageButton ID="pmt_btn" runat="server" Height="12px" ImageUrl="~/images/icons/payment.gif"
                        Width="12px" />&nbsp;
                    <dx:ASPxLabel ID="lblPmts" runat="server" Text="<%$ Resources:LibResources, Label385 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lbl_Payments" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right" nowrap="nowrap" valign="middle">
                    <asp:ImageButton ID="btnModifyFinance" runat="server" Height="12px" ImageUrl="~/images/icons/invoice.gif"
                        Width="12px" OnClick="btnModifyFinance_Click" />&nbsp;
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label207 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lbl_financed" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right" nowrap="nowrap">
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label44 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lbl_balance" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>

    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />

    <dxpc:ASPxPopupControl ID="ASPxPopupSKU" runat="server" ClientInstanceName="ASPxPopupSKU"
        HeaderText="Replace Component" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl13" runat="server">
                <uc1:SKU runat="server" ID="SKU1" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
   <asp:HiddenField runat="server" ID="hidDeliveryCharges" />
</asp:Content>

