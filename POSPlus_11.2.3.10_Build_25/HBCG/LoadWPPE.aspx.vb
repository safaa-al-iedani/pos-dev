﻿Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class LoadWPPE
    Inherits System.Web.UI.Page

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
#Region "Load WPPE data to the external table which was set up under unix env"
    Protected Sub btnUpload_Click(ByVal sender As Object,
      ByVal e As System.EventArgs)
        Try
            If FileUpload1.HasFile Then

                Dim fileSize As Integer = FileUpload1.PostedFile.ContentLength
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "WPPE_Load.csv"

                ' Allow only files less than 1,100,000 bytes (approximately 1 MB) to be uploaded.
                If (fileSize > 1100000) Then
                    Label1.Text = "File size exceeds the allowable limit: " &
                              FileUpload1.PostedFile.ContentLength
                    Exit Sub
                End If

                If (fileExt <> ".csv") Then
                    Label1.Text = "Only .csv files allowed!"
                    Exit Sub
                End If

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live"
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                Try

                    FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadWPPE\done\" & FileUpload1.FileName)
                    FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadWPPE\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                    Label1.Text = "File name: " &
                          FileUpload1.PostedFile.FileName & "<br>" &
                          "File Size: " &
                          FileUpload1.PostedFile.ContentLength

                    Dim userId As String = Nothing
                    Dim password As String = Nothing
                    Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                    Dim dsn_text As String = ""
                    Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                    Dim x As Integer
                    For x = LBound(conStr) To UBound(conStr) - 1
                        If LCase(Left(conStr(x), 3)) = "uid" Then
                            userId = conStr(x)
                            userId = userId.Substring(userId.IndexOf("=") + 1)
                        End If
                        If LCase(Left(conStr(x), 3)) = "pwd" Then
                            password = conStr(x)
                            password = password.Substring(password.IndexOf("=") + 1)
                        End If
                    Next

                    'Upload file using FTP
                    Try
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadWPPE\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                    Catch ex As Exception
                        Label1.Text = Label1.Text & "<br>" &
                                            "System Error: " & ex.Message
                        Exit Sub
                    End Try

                    ' Log the file name and size in AUDIT_LOG
                    theSystemBiz.SaveAuditLogComment("LOAD_WPPE", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                    Label1.Text = Label1.Text & "<br>" &
                            "Upload Successful."

                    btnUpload.Visible = False
                    btn_clear.Visible = True

                    ' Display data from external table
                    Load_WPPE(sender, e)

                Catch ex As Exception
                    Label1.Text = "ERROR: " & ex.Message.ToString()
                    Exit Sub
                End Try

            Else
                Label1.Text = "You have not specified a file."
                End If



        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_WPPE", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" &
                            "Upload failed. Please check the errors."
        End Try
    End Sub


    Protected Sub Load_WPPE(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""
            lbl_msg.Visible = True
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '4046 - load WPPE - create external table WPPE_GENERATION_EXT - July 25,2018

            objSql.CommandText = "select distinct co_cd,prc_cd,itm_cd,corp,fran_corp, fran, quebec,que_fran,other,eff_dt, end_dt," &
                                 "decode(co_cd,null,'Company Code cannot be null; ', ' ') || " &
                                 "decode(prc_cd,null,'Promotion Code cannot be null; ', ' ') || " &
                                 "decode(itm_cd,null,'SKU cannot be null; ', ' ') || " &
                                 "decode(eff_dt,null,'Effective Date cannot be null; ', ' ') || " &
                                 "decode(end_dt,null,'Ending Date cannot be null; ', ' ') err_msg " &
                                 "from WPPE_GENERATION_EXT"


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Gridview1.DataSource = ds
            Gridview1.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No data found. Please upload the file."
            Else
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                        err_cnt = 1
                    End If
                Next
            End If

            If err_cnt = 0 Then
                btn_process.Visible = False
                btn_validate.Visible = True
                btnUpload.Visible = False
                lbl_msg.Text = "No Error Found - Please Validate the file."

            Else
                btn_process.Visible = False
                btn_validate.Visible = False
                btnUpload.Visible = True
                lbl_msg.Text = "Errors found. Please correct the data And upload file again."
            End If

            conn.Close()
        Catch ex As Exception
            btn_process.Visible = False
            btn_validate.Visible = False
            btnUpload.Visible = True
            lbl_msg.Text = "Errors found. Please correct the data And upload file again."
            Throw
        End Try

    End Sub
#End Region


#Region "validate csv file detail, apple validation rule to each field"

    Protected Sub btn_validate_Click(sender As Object, e As EventArgs)

        lbl_msg.Visible = True
        Dim co_grp_cd = Session("str_co_grp_cd")
        If isEmpty(co_grp_cd) Then
            lbl_msg.Text = "System error. Please log out and try again."
            Exit Sub
        End If
        Dim result = validate_WPPE()


        If result = "Y" Then
            Load_ValidationResults()
        End If

        If result = "N" Then
            btn_process.Visible = False
            btn_validate.Visible = False
            btnUpload.Visible = True
            lbl_msg.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Public Function validate_WPPE() As String

        Dim emp_init As String = Session("emp_init")

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "UPLOAD_WPPE.validate_WPPE"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.ExecuteNonQuery()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function
    Protected Sub Load_ValidationResults()

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""
            lbl_msg.Visible = True
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select * from WPPE_GENERATION"
            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Gridview1.DataSource = ds
            Gridview1.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "Errors found. Please correct the data and upload file again."
            Else
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If isNotEmpty(IIf(ds.Tables(0).Rows(i)("err_msg") Is System.DBNull.Value, "", ds.Tables(0).Rows(i)("err_msg"))) Then
                        err_cnt = 1
                    End If
                Next
            End If

            If err_cnt = 0 Then
                btn_process.Visible = True
                btn_validate.Visible = False
                btnUpload.Visible = False
                lbl_msg.Text = "No Errors found. Ready to Process."
            Else
                btn_process.Visible = False
                btn_validate.Visible = False
                btnUpload.Visible = True
                Label1.Text = String.Empty
                lbl_msg.Text = "Errors found. Please correct the data And upload file again."
            End If

            conn.Close()
        Catch ex As Exception
            btn_process.Visible = False
            btn_validate.Visible = False
            btnUpload.Visible = True
            Label1.Text = String.Empty
            lbl_msg.Text = "Errors found. Please correct the data And upload file again."
            Throw
        End Try

    End Sub
#End Region

#Region "Process WPPE load, insert records into WPPE table"

    Protected Sub btn_process_Click(sender As Object, e As EventArgs)
        ' process
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim err_cnt As Integer = 0

        ds = New DataSet

        Gridview1.DataSource = ""
        btn_process.Visible = False

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "UPLOAD_WPPE.process_WPPE"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.ExecuteNonQuery()
            lbl_msg.Text = "Load Successful"

        Catch ex As Exception
            objConnection.Close()
            objConnection.Dispose()
            lbl_msg.Text = "System Error. " & ex.Message
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
#End Region

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs)

        Dim co_grp_cd = Session("str_co_grp_cd")
        If isEmpty(co_grp_cd) Then
            lbl_msg.Text = "System error. Please log out and try again."
            Exit Sub
        End If

        btn_validate.Visible = False
        btn_process.Visible = False
        lbl_msg.Visible = False
        btnUpload.Visible = True
        btn_clear.Visible = False
        Label1.Text = String.Empty
        Gridview1.DataSource = Nothing
        Gridview1.DataBind()
    End Sub


End Class
