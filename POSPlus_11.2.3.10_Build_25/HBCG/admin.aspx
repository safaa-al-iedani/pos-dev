<%@ Page Language="VB" AutoEventWireup="false" CodeFile="admin.aspx.vb" Inherits="admin"
    MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script type="text/javascript">

        $(document).on("click", "a[id='AnchorE1syspm']", function (e) {
            $.ajax({
                type: "POST",
                async: false,
                url: "Services/HBCG_Services.asmx/btn_reloadE1Syspm_Click",
                data: {},
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    window.location.href = "login.aspx";
                }, error: function (error) {
                    alert("hmmm")
                    alert(error.responseText);
                },
            });
            return false;
        });
    </script>
    <div id="admin_form" runat="server">
        <br />
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Width="98%" ActiveTabIndex="0">
            <TabPages>
                <dx:TabPage Text="Systems">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table width="100%">
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;System Mode:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cbo_system_mode" runat="server" Style="position: relative">
                                            <asp:ListItem Value="TRAIN">TRAIN</asp:ListItem>
                                            <asp:ListItem Value="LIVE">LIVE</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="20%" align="center">
                                                    <dx:ASPxCheckBox ID="chk_pos" runat="server" Text="POS" ReadOnly="True">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td width="20%" align="center">
                                                    <dx:ASPxCheckBox ID="chk_CRM" runat="server" Text="CRM" ReadOnly="True">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td width="20%" align="center">
                                                    <dx:ASPxCheckBox ID="chk_payment" runat="server" Text="Payment" ReadOnly="True">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td width="20%" align="center">
                                                    <dx:ASPxCheckBox ID="chk_kiosk" runat="server" Text="Kiosk" ReadOnly="True">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td width="20%" align="center">
                                                    <dx:ASPxCheckBox ID="chk_ptag" runat="server" Text="Price Tags" ReadOnly="True">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Application Title:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_App_Title" runat="server" Style="left: 0px; position: relative"
                                            Width="272px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Licensed User Count</td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txt_user_count" runat="server" Style="position: relative" Width="48px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btn_update_users" runat="server" Text="Update User Count    "
                                                        OnClick="btn_update_users_Click">
                                                        <Image Height="12px" Url="~/images/login_icon.gif" Width="12px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btn_activate_ptag" runat="server" Text="Activate Price Tags    "
                                                        OnClick="btn_activate_ptag_Click">
                                                        <Image Height="12px" Url="~/images/tag.ico" Width="12px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Current User Count</td>
                                    <td>
                                        &nbsp;<asp:TextBox ID="txt_current_users" runat="server" Style="position: relative"
                                            Width="48px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Default Company Code</td>
                                    <td>
                                        <asp:TextBox ID="txt_co_cd" runat="server" Style="position: relative" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Company Logo</td>
                                    <td>
                                        <asp:TextBox ID="txt_logo_image" runat="server" Style="position: relative" Width="272px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Database Type:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_db_type" runat="server" Style="position: relative">
                                            <asp:ListItem Value="ORACLE">Oracle</asp:ListItem>
                                            <asp:ListItem Value="SQL">SQL</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enable Store Based Securities?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_store_based" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enable Terminal Lock Down?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_term_lock_down" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Main Menu Link 1</td>
                                    <td>
                                        <asp:TextBox ID="txt_hyperlink1" runat="server" Style="left: 0px; position: relative"
                                            Width="272px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Link 1 Description</td>
                                    <td>
                                        <asp:TextBox ID="link1_desc" runat="server" Style="left: 0px; position: relative"
                                            Width="80px" MaxLength="18"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Main Menu Link 2</td>
                                    <td>
                                        <asp:TextBox ID="txt_hyperlink2" runat="server" Style="left: 0px; position: relative"
                                            Width="272px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Link 2 Description</td>
                                    <td>
                                        <asp:TextBox ID="link2_desc" runat="server" Style="left: 0px; position: relative"
                                            Width="80px" MaxLength="18"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="hpl_admin_rights" runat="server" NavigateUrl="admin_rights.aspx">Change Administrative Access Rights</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        Change System Theme
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbo_app_theme" runat="server" AutoPostBack="true" SelectedIndex="0"
                                            IncrementalFilteringMode ="StartsWith" ValueType="System.String">
                                            <Items>
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Aqua.jpg" Selected="True" Text="Aqua"
                                                    Value="Aqua" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/BlackGlass.jpg" Text="Black Glass" Value="BlackGlass" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Default.jpg" Text="Default" Value="Default" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/DevExpress.png" Text="Devexpress" Value="DevEx" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Glass.jpg" Text="Glass" Value="Glass" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2003Blue.jpg" Text="Office 2003 Blue"
                                                    Value="Office2003Blue" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2003Green.jpg" Text="Office 2003 Olive"
                                                    Value="Office2003Olive" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2003Silver.jpg" Text="Office 2003 Silver"
                                                    Value="Office2003Silver" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2010Black.png" Text="Office 2010 Black"
                                                    Value="Office2010Black" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2010Blue.png" Text="Office 2010 Blue"
                                                    Value="Office2010Blue" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Office2010Silver.png" Text="Office 2010 Silver"
                                                    Value="Office2010Silver" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/PlasticBlue.jpg" Text="Plastic Blue" Value="PlasticBlue" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/RedWine.jpg" Text="Red Wine" Value="RedWine" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/SoftOrange.jpg" Text="Soft Orange" Value="SoftOrange" />
                                                <dx:ListEditItem ImageUrl="~/images/Themes/Youthful.jpg" Text="Youthful" Value="Youthful" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        <br />
                                        <a id="AnchorE1syspm" href="#">Reload E1 system parameters</a>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Delivery">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;View all zones when none available?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_open_zones" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;Delivery Charges:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Delivery_Options" runat="server" Style="position: relative">
                                            <asp:ListItem Value="ZONE">By Delivery Zone</asp:ListItem>
                                            <asp:ListItem Value="TIERED">Tiered Rates</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                               
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;Delivery/Pickup Default:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_p_or_d" runat="server" Style="position: relative">
                                            <asp:ListItem Value="D">Delivery</asp:ListItem>
                                            <asp:ListItem Value="P">Pickup</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Utilize Mass Finalization?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_mass" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Inventory">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enable Serialization:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_serialization" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Inventory Availability:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_inv_avail" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="STANDARD">Standard</asp:ListItem>
                                            <asp:ListItem Value="CUSTOM">Custom</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Inventory Availability Find Show:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_inv_avail_find" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Link PO:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_link_po" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Print Picking Report:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_picr" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Show PO:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_show_po" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;SKU Generation Link:</td>
                                    <td>
                                        <asp:TextBox ID="txt_sku_generation" runat="server" Style="left: 0px; position: relative" Width="380px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Track Inventory By:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_sys_type" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="SYSTEM">System</asp:ListItem>
                                            <asp:ListItem Value="STORE">Store</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Utilize Inventory XREF?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_xref" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Utilize Priority Fill?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_PFM" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Interfaces">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Email</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Allow Free Form Email?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_email_freeform" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Check Guarantee (NCT)</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable Check Guarantee Module?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_chk_guar" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="NCT_Test.aspx">Live Test</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Credit Card</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable Credit Card Module?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_cc" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HyperLink ID="hpl_cc" runat="server" NavigateUrl="Payment_Xref.aspx">Credit Card Maintenance</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Private Label</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable Private Label Module?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_finance" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="ADS_Test.aspx">Live Test</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable GE Quick Screen?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_qs" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Gift Card</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable World Gift Card?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_w_gift_card" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txt_gift_sku" runat="server" Style="left: 0px; position: relative"
                                            Width="80px" MaxLength="9"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;onBase</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enable onBase?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_onbase" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txt_onbase_dir" runat="server" Style="left: 0px; position: relative"
                                            Width="160px" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Relationships">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Lock Salespeople on Converted Relationships?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_lock_converted" runat="server">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Restrict View to Owner?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_rel_access" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Show Existing Relationship Popup on Customer Lookup?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_show_cust_relationships" runat="server">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Sales">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Allow Cash and Carry?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_allow_cash_carry" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Allow COD?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Allow_COD" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Allow Special Orders?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Special_Orders" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Auto Populate Fabric Protection?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_fab_populate" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Break-out package pricing?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_package" runat="server" Style="position: relative">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="Y">Upon Entry</asp:ListItem>
                                            <asp:ListItem Value="C">Upon Commit</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Cash & Carry Customer Code:</td>
                                    <td>
                                        <asp:TextBox ID="txt_cash_carry" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Create Customer Code As:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_cust_creation" runat="server" Style="position: relative">
                                            <asp:ListItem Value="A">Alpha-Numeric</asp:ListItem>
                                            <asp:ListItem Value="N">Numeric Only</asp:ListItem>
                                            <asp:ListItem Value="M">Use MM Logic</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Customer Type Default:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_cust_tp" runat="server" Style="position: relative">
                                            <asp:ListItem Value="I">Individual</asp:ListItem>
                                            <asp:ListItem Value="C">Corporation</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Default Salesperson 2 %</td>
                                    <td>
                                        <asp:TextBox ID="txt_slsp2" runat="server" MaxLength="3" Width="42px"></asp:TextBox>
                                        <asp:Label ID="lbl_slsp_err" runat="server" Width="289px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Default Sort Code</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Sort_Code" runat="server" Style="position: relative" AppendDataBoundItems="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Drop Code for Special Orders:</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_special_drop_required" runat="server" Style="position: relative">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp; &nbsp;Code:&nbsp;
                                        <asp:TextBox ID="txt_drop_cd" runat="server" Style="left: 0px; position: relative"
                                            Width="48px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enable Promotions?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_promotions" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enable Signature Capture?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_sig_cap" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px; height: 21px;">
                                        &nbsp;
                                    </td>
                                    <td style="height: 21px">
                                        <asp:HyperLink ID="hpl_invoice" runat="server" NavigateUrl="invoice_admin.aspx">Invoice Selection/Assignment</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Enforce Margins?</td>
                                    <td>
                                        <asp:TextBox ID="txt_margin" runat="server" Style="left: 0px; position: relative"
                                            Width="48px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Special Order SKU Size</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_special_sku_limit" runat="server">
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Minimum Deposit Requirements</td>
                                    <td>
                                        <asp:TextBox ID="txt_COD_MIN" runat="server" Style="left: 0px; position: relative"
                                            Width="27px" Text="0"></asp:TextBox>
                                        Percentage, must be between 0 and 100
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_dep_req" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="REQ">Required</asp:ListItem>
                                            <asp:ListItem Value="WARN">Warning</asp:ListItem>
                                        </asp:DropDownList>
                                        Only with deposit requirements
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Prompt for email if missing?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_email" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Require Driver's License Verification?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_dl_verify" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Require Order Sort Code?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_sort_cd" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Require Supervisor for Price Overrides?</td>
                                    <td>
                                        <asp:DropDownList ID="ddlMgrOverride" runat="server" Style="position: relative;">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Require Primary Phone #?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_primary_phone" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Require Secondary Phone #?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_secondary_phone" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Show Availability Details?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_avail_details" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Show Marketing?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_show_marketing" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Swatch Check-Out SKU</td>
                                    <td>
                                        <asp:TextBox ID="txt_swatch_sku" runat="server" Width="67px" AutoPostBack="True"
                                            MaxLength="9"></asp:TextBox><asp:Label ID="lbl_swatch_sku" runat="server" Text="If filled, enables swatch check-out feature"
                                                CssClass="style3"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Tax by Line Item</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_tax_line" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Use Generic Login?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Generic" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Use Special Order Forms?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_Special_Form" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;&nbsp;Validate Zip Codes?</td>
                                    <td>
                                        <asp:DropDownList ID="cbo_zip" runat="server" Style="position: relative;">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Help">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table>
                                <tr>
                                    <td style="width: 253px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="hpl_help" runat="server" NavigateUrl="help_admin.aspx">Help Screen Maintenance</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
        </dx:ASPxPageControl>
        <br />
        <dx:ASPxButton ID="btn_System" runat="server" Text="Save Settings">
        </dx:ASPxButton>
        <br />

    </div>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <div id="admin2" runat="server">
        <dxpc:ASPxPopupControl ID="ASPxPopupControl7" runat="server" CloseAction="CloseButton"
            HeaderText="Update User Licenses" Height="84px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="452px" AllowDragging="True">
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    Please enter a valid authorization code:
                    <br />
                    <dx:ASPxTextBox ID="txt_user_pwd" runat="server" Width="170px" AutoPostBack="True"
                        OnTextChanged="txt_user_pwd_TextChanged" Password="True">
                    </dx:ASPxTextBox>
                    <asp:Label ID="lbl_warnings" runat="server" Width="419px"></asp:Label>
                    <br />
                    <asp:Label ID="lbl_response" runat="server" Visible="False" Text="Please enter the new user count"
                        Width="419px"></asp:Label>
                    <br />
                    <dx:ASPxTextBox ID="txt_new_user_count" Visible="False" runat="server" Width="170px"
                        Enabled="False">
                    </dx:ASPxTextBox>
                    &nbsp;<br />
                    <dx:ASPxButton ID="btn_update" runat="server" Enabled="False" OnClick="btn_update_Click"
                        Text="Update Users">
                    </dx:ASPxButton>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <br />
        <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="CloseButton"
            HeaderText="Activate Price Tags" Height="84px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="452px" AllowDragging="True">
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    Please enter a valid authorization code:
                    <br />
                    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px" AutoPostBack="True"
                        Password="True" OnTextChanged="ASPxTextBox1_TextChanged">
                    </dx:ASPxTextBox>
                    <asp:Label ID="lbl_ptag_warning" runat="server" Width="419px"></asp:Label>
                    &nbsp;<br />
                    &nbsp;<br />
                    <dx:ASPxButton ID="btn_activate" runat="server" Enabled="False" Text="Activate Now"
                        OnClick="btn_activate_Click">
                    </dx:ASPxButton>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        &nbsp;<br />
        <strong><span style="color: #ff0066"></span></strong>&nbsp;</div>

    <div> 
        <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
    </div>

</asp:Content>
