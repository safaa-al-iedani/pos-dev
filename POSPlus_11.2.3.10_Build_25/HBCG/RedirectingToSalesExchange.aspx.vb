﻿
Partial Class RedirectingToSalesExchange
    Inherits POSBasePage

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        'If the current user has write access then perform the below operation
        'If the current has has read-only permission then no need to perform below activity            
        If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
            Dim accessInfo As New AccessControlBiz()
            accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
        End If

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup7();")
        If Request("RELATED_SO") <> "" Then
            Response.Write("parentWindow.location.href='SalesExchange.aspx?RELATED_SO=" & Request("RELATED_SO") & "';" & vbCrLf)
        Else
            Response.Write("close();")
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup7();")
        Response.Write("close();")
        Response.Write("</script>")

    End Sub
End Class
