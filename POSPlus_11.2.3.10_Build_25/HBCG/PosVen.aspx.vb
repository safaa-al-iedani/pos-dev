Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTreeList
Partial Class PosVen
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_name As String = ""
        lbl_msg.Text = ""
        lbl_msg2.Text = ""
        Session("PosVen") = "Y"

        If Not IsPostBack Then
            populate_co_cd()
            populate_ve_cd()
            Populate_supplier_no()
            ASPxPopupPosVen.ShowOnPageLoad() = False
            ASPxPopupPosVen2.ShowOnPageLoad() = False
        Else
            If ASPxPopupPosVen.ShowOnPageLoad = True Or
                ASPxPopupPosVen2.ShowOnPageLoad = True Then
                
                    populate_gridview()
                    If isAuthToUpd() = "Y" Then
                        ASPxPopupPosVen.ShowOnPageLoad() = True
                    Else
                        ASPxPopupPosVen2.ShowOnPageLoad() = True
                    End If
                
            End If
        End If

   
       

    End Sub
    Public Sub Populate_co_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_co_cd.Items.Clear()

        sql = " select co_cd co_cd, co_cd||' - '||des name from co "
        sql = sql & " where co_cd in ('LFL','BW1','LSS') "
        sql = sql & " order by 1 "

        srch_co_cd.Items.Insert(0, "-")
        srch_co_cd.Items.FindByText("-").Value = ""
        srch_co_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_co_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_ve_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_ve_cd.Items.Clear()

        sql = " select ve_cd ve_cd, ve_cd||' - '||ve_name name from ve "
        sql = sql & " order by 1 "

        srch_ve_cd.Items.Insert(0, "-")
        srch_ve_cd.Items.FindByText("-").Value = ""
        srch_ve_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_ve_cd
                .DataSource = ds
                .DataValueField = "ve_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_supplier_no()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_supplier_no.Items.Clear()

        sql = " select distinct supplier_no supplier_no, supplier_no||' - '||supplier_nm name from orafin_vendor_map "
        sql = sql & " order by 1 "

        srch_supplier_no.Items.Insert(0, "-")
        srch_supplier_no.Items.FindByText("-").Value = ""
        srch_supplier_no.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_supplier_no
                .DataSource = ds
                .DataValueField = "supplier_no"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Function isAuthToUpd() As String

        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','PosVen.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function
    Protected Sub clear_the_screen()

        ASPxPopupPosVen.ShowOnPageLoad() = False
        ASPxPopupPosVen2.ShowOnPageLoad() = False

        GridView1.DataSource = ""
        GridView1.DataBind()
        GridView1.PageIndex = 0

        GridView9.DataSource = ""
        GridView9.DataBind()
        GridView9.PageIndex = 0

        srch_co_cd.SelectedIndex = 0
        srch_ve_cd.SelectedIndex = 0
        srch_supplier_no.SelectedIndex = 0


    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click
        clear_the_screen()

    End Sub
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim v_msg As String = ""

        GridView1.PageIndex = 0
        GridView9.PageIndex = 0
        populate_gridview()

        If isAuthToUpd() = "Y" Then
            ASPxPopupPosVen.ShowOnPageLoad() = True
            If GridView1.VisibleRowCount < 1 Then
                lbl_msg2.Text = "No Data to display"
            End If
        Else
            ASPxPopupPosVen2.ShowOnPageLoad() = True
            If GridView9.VisibleRowCount < 1 Then
                lbl_msg2.Text = "No Data to display"
            End If
        End If


    End Sub
    Protected Sub populate_gridview()



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_upd_usr As String = isAuthToUpd()

        GridView1.DataSource = ""
        GridView9.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = " select a.co_cd CO_CD " &
                             " ,a.ve_cd VE_CD " &
                             " ,a.supplier_no SUPPLIER_NO " &
                             " ,a.supplier_nm SUPPLIER_NM " &
                             " ,a.supplier_site SUPPLIER_SITE " &
                             " ,a.payment_terms PAYMENT_TERMS " &
                             " ,a.whs_tp WHS_TP, a.tax_cd TAX_CD, rownum ROW_ID" &
                             " ,a.last_update LAST_UPDATE" &
                             " ,a.changed_by CHANGED_BY " &
                             " FROM ORAFIN_VENDOR_MAP A" &
                             " where a.co_cd = decode('" & srch_co_cd.SelectedItem.Value.ToString() & "',null,a.co_cd,'" & srch_co_cd.SelectedItem.Value.ToString() & "')" &
                             " and a.ve_cd = decode('" & srch_ve_cd.SelectedItem.Value.ToString() & "',null,a.ve_cd,'" & srch_ve_cd.SelectedItem.Value.ToString() & "')" &
                             " and a.supplier_no = decode('" & srch_supplier_no.SelectedItem.Value.ToString() & "',null,a.supplier_no,'" & srch_supplier_no.SelectedItem.Value.ToString() & "')" &
                             " order by a.co_cd,a.ve_cd,a.supplier_no "

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub
    
    
    Function IsValidVe(ByVal p_ve As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from ve "
        sql = sql & " where ve_cd = '" & p_ve & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value < 1 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function AlreadyExists(ByVal p_co_cd As String, ByVal p_ve_cd As String, ByVal p_whs_tp As String) As String

        Dim v_value As Integer = 0
        Dim v_whs_sql As String = ""

        If p_whs_tp & "" = "" Then
            v_whs_sql = " and whs_tp is null "
        Else
            v_whs_sql = " and upper(nvl(whs_tp,'x')) = '" & UCase(p_whs_tp) & "' "
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from orafin_vendor_map " &
                            " where co_cd = '" & p_co_cd & "' " &
                            "   and ve_cd = '" & p_ve_cd & "' " & v_whs_sql

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(Sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Y"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function isValid_whs_tp(ByVal p_whs_tp As String) As String

        Dim v_value As Integer = 0

        If p_whs_tp & "" = "" Then
            Return "Y"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from store_grp$store " &
                            " where store_grp_cd  in ('LSS', 'BDC') " &
                            "   and store_cd = '" & p_whs_tp & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function isValid_tax_cd(ByVal p_tax_cd As String) As String

        Dim v_value As Integer = 0

        If p_tax_cd & "" = "" Then
            Return "Y"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from orafin_prv2tax " &
                            " where tax_verify_cd  = '" & p_tax_cd & "'"


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value > 0 Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Function isValid_co_cd(ByVal p_co_cd As String) As String

        Dim v_IND As String = ""

        If p_co_cd & "" = "" Then
            Return "N"
        End If

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' ind from dual " &
                            " where std_multi_co2.isvalidco('" & Session("EMP_INIT") & "','" & p_co_cd & "') = 'Y' "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_IND = dbReader.Item("ind")
            Else
                v_IND = "N"
            End If
        Catch
            v_IND = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_IND & "" = "" Or isEmpty(v_IND) Then
            Return "N"
        Else
            Return v_IND
        End If

    End Function
    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

      
        If e.Column.FieldName = "CHANGED_BY" Or
                e.Column.FieldName = "LAST_UPDATE" Then
            e.Editor.ReadOnly = True
            e.Editor.ClientEnabled = False
        Else
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        End If
       

    End Sub
    Protected Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("CO_CD") = ""
        e.NewValues("VE_CD") = ""
        e.NewValues("SUPPLIER_NO") = ""
        e.NewValues("SUPPLIER_NM") = ""
        e.NewValues("SUPPLIER_SITE") = ""
        e.NewValues("PAYMENT_TERMS") = ""
        e.NewValues("WHS_TP") = ""
        e.NewValues("TAX_CD") = ""
    
        e.NewValues("LAST_UPDATE") = FormatDateTime(DateTime.Now, DateFormat.ShortDate)
        e.NewValues("CHANGED_BY") = Session("emp_init")



    End Sub
    
    Protected Sub ASPxGridView1_RowValidating(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        e.RowError = ""
        Dim v_stat As String = ""
        Dim v_new_whs_tp As String = ""
        Dim v_old_whs_tp As String = ""

        If e.NewValues("CO_CD") & "" = "" Or isEmpty(e.NewValues("CO_CD")) Then
            e.RowError = "Co Code is a required field - Please enter a value"
            Exit Sub
        ElseIf e.NewValues("CO_CD").ToString.ToUpper() = "LFL" Or
               e.NewValues("CO_CD").ToString.ToUpper() = "BW1" Or
               e.NewValues("CO_CD").ToString.ToUpper() = "LSS" Then
            'If isValid_co_cd(e.NewValues("CO_CD").ToString.ToUpper()) <> "Y" Then
            '    e.RowError = "Invalid CO_CD - please try again"
            '    Exit Sub
            'End If
            'do nothing
        Else
            e.RowError = "Invalid CO_CD - please try again"
            Exit Sub
        End If

        If e.NewValues("VE_CD") & "" = "" Or isEmpty(e.NewValues("VE_CD")) Then
            e.RowError = "Vendor Code is a required field - Please enter a value"
            Exit Sub
        ElseIf isValidVe(e.NewValues("VE_CD")) <> "Y" Then
            e.RowError = "Invalid VE_CD - please try again"
            Exit Sub
        End If

        If e.NewValues("SUPPLIER_NO") & "" = "" Or isEmpty(e.NewValues("SUPPLIER_NO")) Then
            e.RowError = "Supplier No. is a required field - please enter a value"
            Exit Sub
        End If

        If e.NewValues("SUPPLIER_NM") & "" = "" Or isEmpty(e.NewValues("SUPPLIER_NM")) Then
            e.RowError = "Supplier Name is a required field - please enter a value"
            Exit Sub
        ElseIf Len(e.NewValues("SUPPLIER_NM")) > 240 Then
            e.RowError = "Supplier Name is invalid - max. length is 240 characters"
            Exit Sub
        End If

        If e.NewValues("SUPPLIER_SITE") & "" = "" Or isEmpty(e.NewValues("SUPPLIER_SITE")) Then
            e.RowError = "Supplier Site is a required field - please enter a value"
            Exit Sub
        ElseIf Len(e.NewValues("SUPPLIER_SITE")) > 15 Then
            e.RowError = "Supplier Site is invalid - max. length is 15 characters"
            Exit Sub
        End If

        If e.NewValues("PAYMENT_TERMS") & "" = "" Or isEmpty(e.NewValues("PAYMENT_TERMS")) Then
            e.RowError = "Payment Terms is a required field - Please enter a value"
            Exit Sub
        ElseIf Len(e.NewValues("PAYMENT_TERMS")) > 40 Then
            e.RowError = "Payment Terms is invalid - max. length is 40 characters"
            Exit Sub
        End If

        If e.NewValues("WHS_TP") & "" <> "" Then
            If isValid_whs_tp(e.NewValues("WHS_TP") & "") <> "Y" Then
                e.RowError = "WHS TP is invalid"
                Exit Sub
            End If
        End If

        If e.NewValues("TAX_CD") & "" <> "" Then
            If isValid_tax_cd(e.NewValues("TAX_CD") & "") <> "Y" Then
                e.RowError = "TAX CD is invalid"
                Exit Sub
            End If
        End If

        If e.NewValues("WHS_TP") & "" = "" Then
            v_new_whs_tp = "NULL"
        Else
            v_new_whs_tp = e.NewValues("WHS_TP").ToString()
        End If
        If e.OldValues("WHS_TP") & "" = "" Then
            v_old_whs_tp = "NULL"
        Else
            v_old_whs_tp = e.OldValues("WHS_TP").ToString()
        End If

        If e.IsNewRow Then
            If AlreadyExists(e.NewValues("CO_CD").ToString(), e.NewValues("VE_CD"), e.NewValues("WHS_TP")) = "Y" Then
                e.RowError = "A Record with CoCd " & e.NewValues("CO_CD").ToString() & " ,VE " & e.NewValues("VE_CD").ToString() & " ,WHS-TP " & v_new_whs_tp & " already exists-please try again"
                Exit Sub
            End If
        Else
            If e.NewValues("CO_CD").ToString.ToUpper() <> e.OldValues("CO_CD").ToString.ToUpper() Or
               e.NewValues("VE_CD").ToString.ToUpper() <> e.OldValues("VE_CD").ToString.ToUpper() Or
                v_new_whs_tp <> v_old_whs_tp Then
                If AlreadyExists(e.NewValues("CO_CD").ToString(), e.NewValues("VE_CD"), e.NewValues("WHS_TP")) = "Y" Then
                    e.RowError = "A Record with CoCd " & e.NewValues("CO_CD").ToString() & " ,VE " & e.NewValues("VE_CD").ToString() & " ,WHS-TP " & v_new_whs_tp & " already exists-please try again"
                    Exit Sub
                End If
            End If
        End If

        e.RowError = ""
        e.Keys.Clear()

    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim v_whs_sql As String = ""
        Dim v_new_whs_sql As String = ""
        Dim v_tax_sql As String = ""
        Dim v_new_tax_sql As String = ""
        Dim v_payment_sql As String = ""
        Dim v_site_sql As String = ""

        If e.OldValues("WHS_TP") & "" = "" Then
            v_whs_sql = " and whs_tp is null"
        Else
            v_whs_sql = " and upper(whs_tp) = '" & e.OldValues("WHS_TP").ToString.ToUpper() & "'"
        End If

        If e.OldValues("TAX_CD") & "" = "" Then
            v_tax_sql = " and tax_cd is null"
        Else
            v_tax_sql = " and upper(tax_cd) = '" & e.OldValues("TAX_CD").ToString.ToUpper() & "'"
        End If

        If e.OldValues("PAYMENT_TERMS") & "" = "" Then
            v_payment_sql = " and payment_terms is null"
        Else
            v_payment_sql = " and upper(payment_terms) = '" & e.OldValues("PAYMENT_TERMS").ToString.ToUpper() & "'"
        End If
        If e.OldValues("SUPPLIER_SITE") & "" = "" Then
            v_site_sql = " and supplier_site is null"
        Else
            v_site_sql = " and upper(supplier_site) = '" & e.OldValues("SUPPLIER_SITE").ToString.ToUpper() & "'"
        End If
        If e.NewValues("WHS_TP") & "" = "" Then
            v_new_whs_sql = " ,whs_tp = null "
        Else
            v_new_whs_sql = " ,whs_tp = '" & Replace(e.NewValues("WHS_TP").ToString.ToUpper(), "'", "''") & "'"
        End If

        If e.NewValues("TAX_CD") & "" = "" Then
            v_new_tax_sql = " ,tax_cd = null "
        Else
            v_new_tax_sql = " ,tax_cd = '" & Replace(e.NewValues("TAX_CD").ToString.ToUpper(), "'", "''") & "'"
        End If
        '--------------------------------------------------------------------
        ' update 
        '--------------------------------------------------------------------
        conn.Open()
        Try
            sql = "update orafin_vendor_map set " &
                  "  co_cd = '" & e.NewValues("CO_CD").ToString().ToUpper() & "' " &
                  " ,ve_cd = '" & e.NewValues("VE_CD").ToString().ToUpper() & "' " &
                  " ,supplier_no= '" & e.NewValues("SUPPLIER_NO").ToString().ToUpper() & "' " &
                  " ,supplier_nm = '" & Replace(e.NewValues("SUPPLIER_NM").ToString(), "'", "''") & "'" &
                  " ,supplier_site = '" & Replace(e.NewValues("SUPPLIER_SITE").ToString(), "'", "''") & "'" &
                  " ,payment_terms = '" & Replace(e.NewValues("PAYMENT_TERMS").ToString(), "'", "''") & "'" &
                  v_new_whs_sql &
                  v_new_tax_sql &
                  " ,last_update = trunc(sysdate) " &
                  " ,changed_by = '" & Session("emp_init") & "'" &
                  " where co_cd = '" & e.OldValues("CO_CD").ToString().ToUpper() & "'" &
                  "   and ve_cd = '" & e.OldValues("VE_CD").ToString().ToUpper() & "'" &
                  "   and upper(supplier_no) = '" & e.OldValues("SUPPLIER_NO").ToString().ToUpper() & "'" &
                  v_site_sql &
                  v_payment_sql &
                  v_whs_sql &
                  v_tax_sql
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        GridView1.CancelEdit()
        populate_gridview()
        GridView1.DataBind()
        e.Cancel = True

        GridView1.Settings.ShowTitlePanel = True
        GridView1.SettingsText.Title = "** RECORD IS UPDATED **"


    End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_whs_sql As String = ""
        Dim v_tax_sql As String = ""

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- insert new orafin_vendor_map ----------------------------
        '---------------------------------------------------------------------------------------------

        If e.NewValues("WHS_TP") & "" = "" Then
            v_whs_sql = "null "
        Else
            v_whs_sql = " '" & e.NewValues("WHS_TP").ToString.ToUpper() & "' "
        End If


        If e.NewValues("TAX_CD") & "" = "" Then
            v_tax_sql = "null "
        Else
            v_tax_sql = " '" & e.NewValues("TAX_CD").ToString.ToUpper() & "' "
        End If
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into orafin_vendor_map " &
              "(co_cd,ve_cd,supplier_no,supplier_nm,supplier_site,payment_terms,whs_tp,tax_cd,last_update,changed_by) " &
              " values ('" & e.NewValues("CO_CD").ToString().ToUpper() & "'," &
              " '" & e.NewValues("VE_CD").ToString().ToUpper() & "', " &
              " '" & e.NewValues("SUPPLIER_NO").ToString().ToUpper() & "', " &
              " '" & Replace(e.NewValues("SUPPLIER_NM").ToString(), "'", "''") & "', " &
              " '" & Replace(e.NewValues("SUPPLIER_SITE").ToString(), "'", "''") & "'," &
              " '" & Replace(e.NewValues("PAYMENT_TERMS").ToString(), "'", "''") & "', " &
              v_whs_sql & "," &
               v_tax_sql &
              ", trunc(sysdate), '" & Session("emp_init") & "')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record - Record is not added.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        e.Cancel = True
        populate_gridview()
        GridView1.CancelEdit()
        GridView1.DataBind()

        GridView1.Settings.ShowTitlePanel = True
        GridView1.SettingsText.Title = "*** RECORD IS ADDED"

    End Sub
    Protected Sub ASPxGridView1_Rowdeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim v_whs_sql As String = ""
        Dim v_tax_sql As String = ""
        Dim v_payment_sql As String = ""
        Dim v_site_sql As String = ""


        If e.Values.Item("WHS_TP") & "" = "" Then
            v_whs_sql = " and whs_tp is null"
        Else
            v_whs_sql = " and upper(whs_tp) = '" & e.Values.Item("WHS_TP").ToString.ToUpper() & "'"
        End If
        If e.Values.Item("TAX_CD") & "" = "" Then
            v_tax_sql = " and tax_cd is null"
        Else
            v_tax_sql = " and upper(tax_cd) = '" & e.Values.Item("TAX_CD").ToString.ToUpper() & "'"
        End If
        If e.Keys.Item("PAYMENT_TERMS") & "" = "" Then
            v_payment_sql = " and payment_terms is null"
        Else
            v_payment_sql = " and upper(payment_terms) = '" & e.Keys.Item("PAYMENT_TERMS").ToString.ToUpper() & "'"
        End If
        If e.Keys.Item("SUPPLIER_SITE") & "" = "" Then
            v_site_sql = " and supplier_site is null"
        Else
            v_site_sql = " and upper(supplier_site) = '" & e.Keys.Item("SUPPLIER_SITE").ToString.ToUpper() & "'"
        End If
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " delete orafin_vendor_map " &
              " where co_cd = '" & e.Keys.Item("CO_CD") & "' " &
              "  and VE_CD = '" & e.Keys.Item("VE_CD") & "' " &
              "  and upper(supplier_no) = '" & e.Keys.Item("SUPPLIER_NO") & "' " &
              v_site_sql &
              v_payment_sql &
              v_whs_sql &
              v_tax_sql

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw New Exception("Error deleting record. Please try again.")
        End Try
        conn.Close()


        e.Cancel = True
        GridView1.CancelEdit()
        populate_gridview()
        GridView1.DataBind()

        GridView1.Settings.ShowTitlePanel = True
        GridView1.SettingsText.Title = "* RECORD IS DELETED"

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    
    

End Class
