﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SPIFFCalendarLookup.aspx.vb" Inherits="SPIFFCalendarLookup" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    

    <table border="0" style="position: relative; width: 930px; left: 0px; top: 0px;">
        <tr align ="center"  >
            <td>
                <dx:ASPxLabel ID="lbl_screen_title" runat="server" meta:resourcekey="lbl_title_spiff_details" Font-Bold="true"  >
                </dx:ASPxLabel>
            </td>
        </tr>
    </table> 
    &nbsp;&nbsp;
    <table border="0" style="position: relative; width: 930px; left: 0px; top: 0px;">
        <tr>
            <td  style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd" Width="130px">
                </dx:ASPxLabel>
            </td>
            <td  style="width: 400px">
                     <asp:DropDownList ID="ddlist_store_cd" runat="server" Width ="380" Style="position: relative; top: 0px; left: 0px;" >
                     </asp:DropDownList>
           </td>
        </tr>
        <tr>
           <td style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_product_group" runat="server" meta:resourcekey="lbl_product_group" Width="130px">
                </dx:ASPxLabel>
           </td>
           <td style="width: 400px">
                     <asp:DropDownList ID="ddlist_product_group" runat="server" Width ="380" Style="position: relative; top: 0px; left: 0px;" AutoPostBack ="true" >
                     </asp:DropDownList>
           </td>
        </tr>
        <tr>
           <td style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_product_major" runat="server" meta:resourcekey="lbl_product_major" Width="130px">
                </dx:ASPxLabel>
           </td>
           <td style="width: 400px">
                     <asp:DropDownList ID="ddlist_product_major" runat="server" Width ="380" Style="position: relative; top: 0px; left: 0px;" >
                     </asp:DropDownList>
           </td>
            
            <td style="width: 290px" align="right">
                <dx:ASPxButton ID="btn_Search" runat="server" meta:resourcekey="lbl_btn_search"  Width="100px">
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear"  Width="100px">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <hr />
    <div runat="server" id="div_GridViews">
        <dx:ASPxGridView ID="GridViewSpiffSummary" runat="server" AutoGenerateColumns="False" Width="930px"  KeyFieldName="SKU#" 
            ClientInstanceName="GridViewSpiffSummary" OnDataBinding="GridViewSpiffSummary_DataBinding">
            <SettingsPager Position="Top" >
                
        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
        </SettingsPager>
            <Columns>
            <dx:GridViewDataTextColumn FieldName="SKU#" VisibleIndex="1" CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_sku" Width="100px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DESCRIPTION" VisibleIndex="2" CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_description" Width="250px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="VSN" VisibleIndex="3"  CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_vsn" Width="250px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="COMM_CD" VisibleIndex="4" CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_comm_cd" Width="70px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="EFF_DATE" VisibleIndex="5" CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_eff_date" Width="85px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="END_DATE" VisibleIndex="6" CellStyle-HorizontalAlign="Left"  meta:resourcekey="gv_col_end_date" Width="85px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SPIFF$" VisibleIndex="7" CellStyle-HorizontalAlign="Right"    meta:resourcekey="gv_col_spiff" Width="100px" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MIN_RETAIL" VisibleIndex="8" CellStyle-HorizontalAlign="Right"  meta:resourcekey="gv_col_min_retail" Width="100px" >
            </dx:GridViewDataTextColumn>
            </Columns> 
            <SettingsBehavior AllowSort="False" />        
        </dx:ASPxGridView>
        </div> 
    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
    <br />
    <br />
    <div runat="server" id="div_version">
        <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>