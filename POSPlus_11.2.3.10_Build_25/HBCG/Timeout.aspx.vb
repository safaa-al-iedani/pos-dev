
Partial Class Timeout
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Removes extra time added for app processing in SystemBiz.LoadSysPms, not accounted to user
        Dim elapsed As String = ((CInt(SysPms.appTimeOut) - 45) / 60).ToString
        'ASPxLabel2.Text = String.Format("You have been inactive for {0} minutes. You will be automatically logged out in 2 minutes unless you begin working again.", elapsed)
        'French
        ASPxLabel2.Text = String.Format(Resources.LibResources.Label797, elapsed)

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", "120;url=logout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", "120;url=logout.aspx")
        End If

        theSystemBiz.SaveAuditLogComment("TIMEOUT_WARNING", Session("EMP_CD") & " - Timeout Message Received")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.Theme = ConfigurationManager.AppSettings("app_theme").ToString()
    End Sub

End Class
