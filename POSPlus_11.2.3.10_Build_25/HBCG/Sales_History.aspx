<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Sales_History.aspx.vb" Inherits="Sales_History" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Use this process to update the Sales History for certain Reports.">
    </dx:ASPxLabel>
    <br />
    <br />
    <dx:ASPxButton ID="Button1" runat="server" Text="Update Sales History">
    </dx:ASPxButton>
    <br />
    <br />
    <dx:ASPxLabel ID="lbl_status" runat="server" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
