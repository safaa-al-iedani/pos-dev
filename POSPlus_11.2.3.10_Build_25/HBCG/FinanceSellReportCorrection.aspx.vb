Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils


Partial Class FinanceSellReportCorrection
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_old_crd_num As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_msg.Text = ""

        If IsPostBack Then

            If (Not GridView2.DataSource Is Nothing) Then
                GridView2.DataBind()
            End If

        Else
            srch_co_cd.Items.Clear()
            srch_str_cd.Items.Clear()

            populateCoCd()
            populateStrCd()

            srch_str_cd.Enabled = True
            srch_co_cd.Enabled = True

        End If



    End Sub
    Protected Sub Populate_Results(ByVal p_co_cd As String, ByVal p_str_cd As String, ByVal p_post_dt As String)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "select s.del_doc_num IVC_CD,s.cust_cd CUST_CD,substr(c.fname||' '||c.lname,1,30) CUST_NAME "
        sql = sql & " , BNK_CRD_NUM,null NEW_CRD_NUM "
        sql = sql & " ,s.orig_fi_amt ORIG_FI_AMT, s.fin_cust_cd FIN_CUST_CD "
        sql = sql & " ,s.approval_cd APPROVAL_CD, null UPD_EMP_INIT, null APP_EMP_INIT "
        sql = sql & " ,nvl(std_des3.decryptCrdNum(bnk_crd_num),'x') HOLD_BNK_CRD_NUM, a.rowid ROW_ID "
        sql = sql & " from ar_trn a, so s, cust c "
        sql = sql & " where a.ivc_cd = s.del_doc_num "
        sql = sql & " and nvl(s.cust_cd,'x') = c.cust_cd "
        sql = sql & " and nvl(a.mop_cd,'x') = 'FI' "
        sql = sql & " and nvl(a.stat_cd,'x') in ('P', 'T') "
        sql = sql & " and nvl(s.stat_cd,'x') = 'F' "
        sql = sql & " and nvl(a.co_cd,'x') = '" & UCase(p_co_cd) & "'"
        sql = sql & " and nvl(s.so_store_cd,'x') = '" & UCase(p_str_cd) & "'"
        sql = sql & " and std_multi_co2.isvalidco('" & Session("emp_init") & "',nvl(a.co_cd,'x')) = 'Y' "
        sql = sql & " and a.post_dt = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & " and STD_mULTI_CO2.ISVALIDSTR2('" & Session("emp_init") & "',S.SO_STORE_CD) = 'Y' "
        sql = sql & " and not exists (select 'x' from finance_selling_correction f "
        sql = sql & "                  where f.ivc_cd = s.del_doc_num "
        sql = sql & "                    and f.cust_cd = s.cust_cd  "
        sql = sql & "                    and ( (nvl(f.bnk_crd_num,'x') = nvl(a.bnk_crd_num,'x')) or"
        sql = sql & "                          (nvl(f.new_crd_num,'x') = nvl(a.bnk_crd_num,'x')) )"
        sql = sql & "                 ) "
        sql = sql & " union "
        sql = sql & " select f1.ivc_cd IVC_CD, f1.cust_cd CUST_CD, substr(c.fname||' '||c.lname,1,30) CUST_NAME, "
        sql = sql & "  BNK_CRD_NUM, std_des3.decryptCrdNum(NEW_CRD_NUM) NEW_CRD_NUM "
        sql = sql & " ,ORIG_FI_AMT, FIN_CUST_CD FIN_CUST_CD, APPROVAL_CD, upd_emp_init UPD_EMP_INIT, app_emp_init APP_EMP_INIT "
        sql = sql & " ,nvl(std_des3.decryptCrdNum(BNK_CRD_NUM),'x') HOLD_BNK_CRD_NUM, f1.rowid ROW_ID "
        sql = sql & " from finance_selling_correction f1, cust c "
        sql = sql & " where c.cust_cd = f1.cust_cd "
        sql = sql & "   and f1.co_cd = '" & UCase(p_co_cd) & "'"
        sql = sql & "   and f1.store_cd  = '" & UCase(p_str_cd) & "'"
        sql = sql & "   and STD_multi_co2.ISVALIDSTR2('" & Session("emp_init") & "',f1.store_cd) = 'Y' "
        sql = sql & "   and std_multi_co2.isvalidco('" & Session("emp_init") & "',f1.co_cd) = 'Y' "
        sql = sql & "   and f1.post_dt = to_date('" & FormatDateTime(p_post_dt.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & "   and f1.app_emp_init is null "
        sql = sql & " order by 1,2"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            If MyDataReader.Read Then
                Try
                    GridView2.DataSource = ds
                    GridView2.DataBind()

                Catch ex As Exception
                    conn.Close()
                End Try

                MyDataReader.Close()
                srch_str_cd.Enabled = False
                srch_co_cd.Enabled = False

            Else
                srch_str_cd.Enabled = True
                srch_co_cd.Enabled = True


            End If
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw

        End Try

    End Sub
    Private Sub populateCoCd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct co_cd co_cd from store " &
                  " where std_multi_co2.isvalidstr3('" & Session("emp_init") & "', store_cd) = 'Y' "

        srch_co_cd.Items.Insert(0, Resources.LibResources.Label943)
        srch_co_cd.Items.FindByText(Resources.LibResources.Label943).Value = ""
        srch_co_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_co_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "co_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populateStrCd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct store_cd str_cd from store " &
                  " where std_multi_co2.isvalidstr3('" & Session("emp_init") & "', store_cd) = 'Y' "

        srch_str_cd.Items.Insert(0, Resources.LibResources.Label942)
        srch_str_cd.Items.FindByText(Resources.LibResources.Label942).Value = ""
        srch_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "str_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try



    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        GridView2.DataSource = ""
        GridView2.DataBind()

        srch_str_cd.SelectedIndex = 0
        srch_co_cd.SelectedIndex = 0

        srch_str_cd.Items.Clear()
        srch_co_cd.Items.Clear()

        populateCoCd()
        populateStrCd()

        srch_str_cd.Enabled = True
        srch_co_cd.Enabled = True

    End Sub
    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click


        If post_dt.SelectedDate.Date = DateTime.MinValue Then
            'lbl_msg.Text = "Please select a Date to proceed"
            lbl_msg.Text = Resources.LibResources.Label893
            Exit Sub
            'ElseIf FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) > DateTime.Today.AddDays(-1) Then
            '    'lbl_msg.Text = "Invalid Date - please try again"
            '    lbl_msg.Text = Resources.LibResources.Label894
            '    Exit Sub
            'ElseIf FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) < DateTime.Today.AddDays(-3) Then
            '    'lbl_msg.Text = "Invalid Date - please try again"
            '    lbl_msg.Text = Resources.LibResources.Label894
            '    Exit Sub
        End If

        If srch_co_cd.SelectedIndex < 1 Or srch_str_cd.SelectedIndex < 1 Then
            'Please enter Company code and Store to continue "
            lbl_msg.Text = Resources.LibResources.Label929
            Exit Sub
        End If


        Populate_Results(HttpUtility.UrlEncode(srch_co_cd.Text), HttpUtility.UrlEncode(srch_str_cd.Text), FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate))



    End Sub

    Protected Function isValidPlan(ByVal p_FIN_CUST_CD As String) As String

        Dim v_ind As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' as AAA FROM asp " &
                            " where as_cd = '" & p_FIN_CUST_CD & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub GridView2_ParseValue(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxParseValueEventArgs)

        Dim hasError As Boolean = "false"
        Dim errorMsg As String = ""
        Dim v_item_idx As String = ""

        If e.FieldName = "BNK_CRD_NUM" Then
            g_old_crd_num = e.Value
        End If

        If e.FieldName = "ORIG_FI_AMT" Then
            If e.Value & "" = "" Then
                errorMsg = Resources.LibResources.Label802 'amount must be greater than 0
                hasError = True
            ElseIf Not IsNumeric(e.Value) Then
                errorMsg = Resources.LibResources.Label834 'amount is invalid
                hasError = True
            ElseIf e.Value < 1 Then
                errorMsg = Resources.LibResources.Label802 'amount must be greater than 0
                hasError = True
            End If
        ElseIf e.FieldName = "FIN_CUST_CD" Then
            If e.Value & "" = "" Then
                'PLAN number is missing
                errorMsg = Resources.LibResources.Label930
                hasError = True
            ElseIf isValidPlan(UCase(e.Value)) <> "Y" Then
                'Invalid PLAN number
                errorMsg = Resources.LibResources.Label931
                hasError = True
            End If
        ElseIf e.FieldName = "APPROVAL_CD" Then
            If e.Value & "" = "" Then
                errorMsg = Resources.LibResources.Label836
                hasError = True
            ElseIf Len(e.Value) < 6 Then
                'Invalid Auth. number - must be 6 or 7 digits
                errorMsg = Resources.LibResources.Label932
                hasError = True
            ElseIf Len(e.Value) > 7 Then
                'Invalid Auth. number - must be 6 or 7 digits
                errorMsg = Resources.LibResources.Label932
                hasError = True
            End If
        ElseIf e.FieldName = "APP_EMP_INIT" Then
            If e.Value & "" <> "" Then
                'This is already approved - no updates allowed
                errorMsg = Resources.LibResources.Label933
                hasError = True
            End If
        ElseIf e.FieldName = "NEW_CRD_NUM" Then
            If e.Value & "" = "" Then
                If g_old_crd_num & "" = "" Then
                    'card number is required
                    errorMsg = Resources.LibResources.Label940
                    hasError = True
                End If
                g_old_crd_num = ""
            End If
        End If

        If hasError Then
            Throw New Exception(errorMsg)
        End If

    End Sub

    Protected Sub GridView2_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)

        Dim v_err_found As String = ""
        Dim v_invalid_return As Integer = 0
        Dim v_msg As String = ""
        Dim v_updatecount As Integer = e.UpdateValues.Count

        If e.UpdateValues.Count = 0 Then
            If security_to_approve() = "Y" Then
                Update_ArTrn_SO()
            Else
                'You are not authorized to Approve
                Throw New Exception(Resources.LibResources.Label927)
            End If
        End If

        For Each args In e.UpdateValues
            If DoUpdate(args.Keys, args.NewValues, args.OldValues) <> True Then
                v_invalid_return = v_invalid_return + 1
            End If
        Next args

        If v_updatecount > 0 Then
            If v_invalid_return = 0 Then
                If security_to_approve() = "Y" Then
                    Update_ArTrn_SO()
                End If
            End If
        End If

        e.Handled = True


        If v_invalid_return = 0 And v_updatecount > 0 Then
            Throw New Exception(Resources.LibResources.Label890) 'update is done
        ElseIf v_updatecount = 0 And security_to_approve() = "Y" Then
            'All Approved
            Throw New Exception(Resources.LibResources.Label934)
        End If

    End Sub
    Function security_to_approve() As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoInsert('" & Session("emp_cd") & "','FinanceSellReportCorrection.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function
    Function AlreadyExists(ByVal p_ivc_cd As String, ByVal p_cust_cd As String, ByVal p_bnk_crd_num As String) As String

        Dim v_bnk_crd_sql As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String

        If p_bnk_crd_num & "" = "" Then
            v_bnk_crd_sql = " and bnk_crd_num is null "
        ElseIf p_bnk_crd_num = "x" Then
            v_bnk_crd_sql = " and bnk_crd_num is null "
        Else
            v_bnk_crd_sql = " and nvl(std_des3.decryptCrdNum(bnk_crd_num),'x') = nvl('" & p_bnk_crd_num & "' ,'x') "
        End If
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select 'Y' Y from finance_selling_correction  "
        sql = sql & " where co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ivc_cd = '" & p_ivc_cd & "'"
        sql = sql & " and store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "'"
        sql = sql & " and post_dt  = to_date('" & FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & " and cust_cd = '" & p_cust_cd & "'"
        sql = sql & v_bnk_crd_sql

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("Y").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()


    End Function

    Protected Sub ApproveCorrections()

        Dim v_card_sql As String = ""
        Dim v_upd_emp_init_sql As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        conn.Open()
        sql = " update finance_selling_correction  "
        sql = sql & " set app_emp_init = '" & Session("emp_init") & "' "
        sql = sql & " ,app_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'mm/dd/RRRR')"
        sql = sql & " where co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "'"
        sql = sql & " and post_dt = to_date('" & FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & " and app_emp_init is null "
        sql = sql & " and fin_cust_cd is not null and approval_cd is not null and orig_fi_amt is not null "
        sql = sql & " and (bnk_crd_num is not null or new_crd_num is not null) "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            'Error updating the record. Please try again
            Throw New Exception(Resources.LibResources.Label925)
            conn.Close()
        End Try
        conn.Close()

    End Sub
    Protected Sub Update_ArTrn_SO()

        Dim v_card_sql As String = ""
        Dim v_upd_emp_init_sql As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'call this routine to update AR_TRN, SO, CUST_CMNT
        conn.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = conn
            myCMD.CommandText = "std_finance_correction.perform_approve_rtn"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_usr", OracleType.VarChar)).Value = Session("emp_init")
            myCMD.Parameters.Add(New OracleParameter("p_co_cd", OracleType.VarChar)).Value = srch_co_cd.SelectedItem.Value.ToString()
            myCMD.Parameters.Add(New OracleParameter("p_post_dt", OracleType.VarChar)).Value = FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate)
            myCMD.Parameters.Add(New OracleParameter("p_str_cd", OracleType.VarChar)).Value = srch_str_cd.SelectedItem.Value.ToString()
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 60).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            If myCMD.Parameters("p_res").Value.ToString() & "" = "" Then    'update for AR_TRN and SO is ok
                ApproveCorrections()
            Else
                conn.Close()
                Throw New Exception(myCMD.Parameters("p_res").Value.ToString())
            End If
        Catch
            Throw 'New Exception("Error updating AR_TRN, SO - Please try again.")
            conn.Close()
        End Try

        conn.Close()


    End Sub

    Protected Function DoUpdate(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary, ByVal oldValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim v_new_card As String = ""
        Dim v_old_card As String = ""

        If newValues("NEW_CRD_NUM") & "" = "" Then
            v_new_card = "''"
        Else
            v_new_card = "std_des3.encryptCrdNum('" & newValues("NEW_CRD_NUM").ToString() & "')"
        End If


        If oldValues("BNK_CRD_NUM") & "" = "" Then
            v_old_card = "''"
        Else
            ' ** sy note ** added to key feild - otherwise no value is kept for invisible columns 
            v_old_card = "std_des3.encryptCrdNum('" & keys.Item("HOLD_BNK_CRD_NUM").ToString() & "')"
        End If

        If AlreadyExists(oldValues("IVC_CD").ToString(), oldValues("CUST_CD").ToString, keys.item("HOLD_BNK_CRD_NUM").ToString()) = "Y" Then
            sql = "update finance_selling_correction "
            'sql = sql & " set new_crd_num = std_des3.encryptCrdNum('" & newValues("NEW_CRD_NUM").ToString() & "') "
            sql = sql & " set new_crd_num = " & v_new_card
            sql = sql & " ,orig_fi_amt = " & newValues("ORIG_FI_AMT").ToString()
            sql = sql & " ,fin_cust_cd = '" & newValues("FIN_CUST_CD").ToString().ToUpper & "'"
            sql = sql & " ,approval_cd = '" & newValues("APPROVAL_CD").ToString().ToUpper & "'"
            sql = sql & " ,upd_emp_init = '" & Session("emp_init") & "' "
            sql = sql & " ,upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'mm/dd/RRRR')"
            sql = sql & " where co_cd = '" & Session("CO_CD") & "'"
            sql = sql & " and store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "'"
            sql = sql & " and post_dt = to_date('" & FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
            sql = sql & " and cust_cd = '" & oldValues("CUST_CD").ToString() & "'"
            sql = sql & " and nvl(bnk_crd_num,'x') = nvl(std_des3.encryptCrdNum('" & oldValues("BNK_CRD_NUM").ToString() & "'),'x')"


            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Mydatareader.Close()
            Catch
                'Error updating the record. Please try again
                Throw New Exception(Resources.LibResources.Label925)
                conn.Close()
            End Try
            conn.Close()

        Else

            sql = " insert into finance_selling_correction  "
            sql = sql & " (co_cd,post_dt,store_cd,ivc_cd,cust_cd,bnk_crd_num,new_crd_num,orig_fi_amt,fin_cust_cd,approval_cd,upd_emp_init,app_emp_init,upd_dt,app_dt) "
            sql = sql & " values ('" & Session("CO_CD") & "'"
            sql = sql & " ,to_date('" & FormatDateTime(post_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
            sql = sql & " ,'" & srch_str_cd.SelectedItem.Value.ToString.ToUpper & "'"
            sql = sql & " ,'" & oldValues("IVC_CD").ToString() & "','" & oldValues("CUST_CD").ToString() & "'"
            sql = sql & " ," & v_old_card
            sql = sql & " ," & v_new_card
            sql = sql & " ," & newValues("ORIG_FI_AMT").ToString
            sql = sql & " ,'" & newValues("FIN_CUST_CD").ToString().ToUpper & "'"
            sql = sql & " ,'" & newValues("APPROVAL_CD").ToString().ToUpper & "'"
            sql = sql & " ,'" & Session("emp_init") & "',null "
            sql = sql & " ,to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "' ,'mm/dd/RRRR'),null)"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                If ex.ToString.Contains("unique constraint") Then
                    'Duplicate record. To update existing record please enter search criteria and click Search
                    Throw New Exception(Resources.LibResources.Label926)
                Else
                    'Error inserting the record. please try again
                    Throw New Exception(Resources.LibResources.Label925)
                End If
            End Try
            conn.Close()

        End If


        Return True

    End Function

End Class
