
Partial Class SigCapture
    Inherits POSBasePage

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        'Put user code to initialize the page here
        Dim sigObj As New SIGPLUSLib.SigPlus()
        sigObj.InitSigPlus()
        sigObj.AutoKeyStart()

        'use the same data to decrypt signature
        sigObj.AutoKeyData = Request("SigText")

        sigObj.AutoKeyFinish()
        sigObj.SigCompressionMode = 1
        sigObj.EncryptionMode = 2

        'Now, get sigstring from client
        'Sigstring can be stored in a database if 
        'a biometric signature is desired rather than an image
        sigObj.SigString = Request("hidden")
        If (sigObj.NumberOfTabletPoints() > 0) Then
            sigObj.ImageFileFormat = 0
            sigObj.ImageXSize = 500
            sigObj.ImageYSize = 150
            sigObj.ImagePenWidth = 8
            sigObj.SetAntiAliasParameters(1, 600, 700)
            sigObj.JustifyX = 5
            sigObj.JustifyY = 5
            sigObj.JustifyMode = 5
            Dim size As Long
            Dim byteValue As Byte()
            sigObj.BitMapBufferWrite()
            size = sigObj.BitMapBufferSize()
            ReDim byteValue(size)
            byteValue = sigObj.GetBitmapBufferBytes()
            sigObj.BitMapBufferClose()
            Dim ms As System.IO.MemoryStream
            ms = New System.IO.MemoryStream(byteValue)
            Dim img As System.Drawing.Image
            img = System.Drawing.Image.FromStream(ms)
            Dim path As String
            path = HttpContext.Current.Request.PhysicalApplicationPath & "\Signatures\" & HttpContext.Current.Session.SessionID.ToString & ".bmp"
            'path = "C:\\mySig.bmp"
            img.Save(path, System.Drawing.Imaging.ImageFormat.Bmp)
            lbl_status.Text = "Image saved successfully."
            Session("signature") = HttpContext.Current.Session.SessionID.ToString
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.parent; ")
            Response.Write("parentWindow.location.href='Order_Stage.aspx';")
            Response.Write("</script>")
        Else

            'signature has not been returned successfully!
        End If

    End Sub

End Class
