Imports System.Xml
Imports NCT_Utils
Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class NCT_Test
    Inherits POSBasePage

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim rowid As String = ""
        conn.Open()

        sql = "INSERT INTO PAYMENT (SESSIONID, MOP_CD, AMT, DES, MOP_TP, CHK_NUM, CHK_TP, TRANS_ROUTE, CHK_ACCOUNT_NO) "
        sql = sql & "VALUES(:SESSIONID, :MOP_CD, :AMT, :DES, :MOP_TP, :CHK_NUM, :CHK_TP, :TRANS_ROUTE, :CHK_ACCOUNT_NO)"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
        objSql.Parameters.Add(":AMT", OracleType.Number)
        objSql.Parameters.Add(":DES", OracleType.VarChar)
        objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_TP", OracleType.VarChar)
        objSql.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
        objSql.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)

        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString
        objSql.Parameters(":MOP_CD").Value = "CHK"
        objSql.Parameters(":AMT").Value = txt_amt.Text.ToString
        objSql.Parameters(":DES").Value = "NATIONAL CHECK TRUST"
        objSql.Parameters(":MOP_TP").Value = "CK"
        objSql.Parameters(":CHK_NUM").Value = TextBox3.Text.ToString
        objSql.Parameters(":CHK_TP").Value = DropDownList1.SelectedValue.ToString
        objSql.Parameters(":TRANS_ROUTE").Value = TextBox1.Text.ToString
        objSql.Parameters(":CHK_ACCOUNT_NO").Value = TextBox2.Text.ToString
        objSql.ExecuteNonQuery()

        sql = "select row_id from payment where sessionid='" & Session.SessionID.ToString & "' order by row_id DESC"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                rowid = MyDataReader.Item("row_id").ToString
            End If
        Catch
            conn.Close()
            Throw
        End Try

        Dim merchant_id As String = ""

        'Check to see if there is a merchant key for the store that the sale was written under
        merchant_id = Lookup_Merchant("CK", cbo_store.SelectedValue.ToString, "CHK")

        NCT_Response.Text = NCT_Submit_Query(rowid, merchant_id)

        objSql.Dispose()
        conn.Close()

    End Sub

    Private Function GetDesc(ByVal reasonCd As String, ByVal criteria As String) As String

        Dim m_xmld As XmlDocument
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        Dim desc As String = String.Empty
        m_xmld = New XmlDocument()
        m_xmld.Load(HttpContext.Current.Server.MapPath("~/App_Data/CodeDescription.xml"))
        m_nodelist = m_xmld.SelectNodes(criteria)
        For Each m_node In m_nodelist
            If m_node.ChildNodes.Item(0).InnerText = reasonCd Then
                desc = m_node.ChildNodes.Item(1).InnerText
                Exit For
            End If
        Next
        If desc = String.Empty Then
            desc = "NOT Found"
        End If
        Return desc

    End Function

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox1.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            NCT_Response.Text = ""
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "NCT Check Service Test"
            TextBox1.Focus()
            store_cd_populate()
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

End Class
